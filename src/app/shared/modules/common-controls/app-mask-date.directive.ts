import { Directive, ElementRef, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';
import { MatDatepickerInput } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { NgControl } from '@angular/forms';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[appMaskDate]'
})
export class AppMaskDateDirective implements  AfterViewInit, OnDestroy {

 // '^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$'
  mask = [/[0-1]/, /\d/, '/', /[0-3]/, /\d/, '/', /[1-2]/, /\d/, /\d/, /\d/]; // mm/dd/yyyy
  maskedInputController;
  @ViewChild(MatDatepickerInput) datepickerInput: MatDatepickerInput<any>;
  eventSubscription: Subscription;
  constructor(
    private elementRef: ElementRef,
    private control: NgControl
  ) {

    this.maskedInputController = textMask.maskInput({
      inputElement: this.elementRef.nativeElement,
      mask: this.mask
    });
  }

  ngAfterViewInit() {
    this.eventSubscription = fromEvent(this.elementRef.nativeElement, 'input').subscribe(_ => {
   //   this.datepickerInput._onInput(this.elementRef.nativeElement.value);
   console.log(this.elementRef.nativeElement.value);
   this.control.control.setValue(this.elementRef.nativeElement.value);
    });
  }

  ngOnDestroy() {
    this.maskedInputController.destroy();
  }

}
