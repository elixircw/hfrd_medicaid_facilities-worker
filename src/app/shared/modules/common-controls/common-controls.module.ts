import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonDatePickerComponent } from './common-date-picker/common-date-picker.component';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule

} from '@angular/material';
import { AppMaskDateDirective } from './app-mask-date.directive';
import { SignatureFieldComponent } from '../../../pages/provider-management/license-management/license-change-request/signature-field/signature-field.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { CurrencyInputComponent } from './currency-input/currency-input.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    SignaturePadModule

  ],
  declarations: [CommonDatePickerComponent, AppMaskDateDirective, SignatureFieldComponent, CurrencyInputComponent],
  exports: [CommonDatePickerComponent, AppMaskDateDirective, SignatureFieldComponent, CurrencyInputComponent]
})
export class CommonControlsModule { }
