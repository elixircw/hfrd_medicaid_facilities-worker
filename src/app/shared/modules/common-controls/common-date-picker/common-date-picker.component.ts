import { AfterViewInit, Component, forwardRef, Input, OnChanges, Output, OnDestroy, OnInit, ViewChild, HostListener,EventEmitter } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroupDirective, NgForm, NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';
import { MatDatepicker } from '@angular/material';
import { ErrorStateMatcher } from '@angular/material/core';
import * as moment from 'moment';
import { AlertService } from '../../../../@core/services';
import { Moment } from 'moment';


export function validateDate() {
  return (c: FormControl) => {
    console.log('validateDate date' + c.value);
    if (c.value) {
      const momentDate = moment(c.value, [moment.ISO_8601, 'MM/DD/YYYY']);
      if (!momentDate.isValid()) {
        console.log('invalid date' + c.value);
        return 'Invalid Date ' + c.value;
      }
    }
    return null;
  };
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'common-date-picker',
  templateUrl: './common-date-picker.component.html',
  styleUrls: ['./common-date-picker.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CommonDatePickerComponent), multi: true },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => CommonDatePickerComponent), multi: true }
  ]
})

export class CommonDatePickerComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges, ControlValueAccessor {



  @Input()
  _dateValue: string;
  maskedDate: string;
  pickerDate: Date;
  @Input() placeholder: string;
  @Input() required: boolean;
  @Input() min: Date;
  @Input() max: Date;
  @Input() disabled?: boolean;
  @Input() commonDatePickerFilter: any;
  @Input() ngClass: string;
  @Output() valueChange = new EventEmitter();
  @Output() valueClick = new EventEmitter();
  dateFormat = 'MM/DD/YYYY';


  @ViewChild('picker') datePicker: MatDatepicker<Date>;


  constructor(private _alertService: AlertService) {

  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    if (this.dateValue) {
      this.setValueToControls(this.dateValue);
    } else {
      console.log('reset the date');
    }
  }

  onDateClick() {
    this.valueClick.emit(this.maskedDate);
  }

  setValueToControls(value: any) {
    if (value) {
      this.pickerDate = this.formatStringToDate(value);
      this.maskedDate = this.formatDateToString(this.pickerDate);
    } else {
      this.resetControlValues();
    }

  }

  resetControlValues() {
    this.pickerDate = null;
    this.maskedDate = null;
  }

  ngOnChanges(changes: any) {
    console.log('changes', changes, this.maskedDate);
    this.validateDateFn = validateDate();

  }
  onMaskDateChanged(event: any) {
    console.log('blur', event, this.maskedDate);
    this.valueChange.emit(this.maskedDate);
    if (!this.maskedDate) {
      this.resetDate();
      return null;
    }
    const momentDate = moment(this.maskedDate, this.dateFormat);
    if (!this.checkForValidation(momentDate)) {
      return null;
    }
    this.pickerDate = new Date(this.maskedDate);
    this.dateValue = this.maskedDate;
    this.valueChange.emit(this.dateValue);

  }

  checkForValidation(momentDate: Moment) {
    if (!momentDate.isValid()) {
      this._alertService.error(this.maskedDate + ' is not valid date');
      this.resetDate();
      return false;

    }
    if (this.min && momentDate.isBefore(this.min)) {
      const message = this.placeholder ? this.placeholder  : 'Selected date ';
      this._alertService.error(message + ' should be greater than ' + this.formatDateToString(this.min));
      this.resetDate();
      return false;
    }
    if (this.max && momentDate.isAfter(this.max)) {
      this._alertService.error(this.placeholder + ' should be less than ' + this.formatDateToString(this.max));
      this.resetDate();
      return false;
    }

    return true;
  }

  onMaskDateChange() {
    console.log(this.datePicker);
    if (this.datePicker.opened) {
      this.datePicker.close();
    }
  }

  onDatePickerClosed() {
    console.log('on dp cloed', this.maskedDate, this.pickerDate);
  }

  onPickerDateChange() {
    this.valueClick.emit(this.maskedDate);
    const momentDate = moment(this.pickerDate, moment.ISO_8601);
    if (!this.checkForValidation(momentDate)) {
      return null;
    }
    this.maskedDate = this.formatDateToString(this.pickerDate);
    this.dateValue = this.maskedDate;
    this.valueChange.emit(this.dateValue);
  }

  ngOnDestroy() {

  }

  get dateValue() {
    return this._dateValue;
  }

  set dateValue(val) {
    this._dateValue = val;
    this.propagateChange(this._dateValue);
  }

  validateDateFn: any = () => { };
  propagateChange = (_: any) => { };

  writeValue(obj: any): void {
    this.dateValue = obj;
    this.setValueToControls(this.dateValue);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {

  }

  formatDateToString(date: Date): string {
    let convertedDate;
    convertedDate = moment(date);
    if (convertedDate.isValid()) {
      return convertedDate.format('MM/DD/YYYY');
    } else {
      return null;
    }
  }
  formatStringToDate(dateString: string): Date {
    console.log('formatStringToDate', dateString);
    let convertedDate;
    convertedDate = moment(dateString, moment.ISO_8601);
    if (convertedDate.isValid()) {
      return moment(dateString, moment.ISO_8601).toDate();
    } else {
      return null;
    }


  }

  setDisabledState?(isDisabled: boolean): void {
    console.log('cdp setDisabledState', isDisabled );
    this.disabled = isDisabled;
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    console.log('key down');
    if (this.datePicker.opened) {
      this.datePicker.close();
    }
  }

  validate(c: FormControl) {
    return this.validateDateFn(c);
  }

  resetDate() {
    this.pickerDate = null;
    this.maskedDate = null;
    this.dateValue = null;
  }

}
