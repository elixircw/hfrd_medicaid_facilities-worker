import { Component, OnInit, forwardRef, AfterViewInit, OnChanges, Input, Output, ViewChild, OnDestroy, HostListener, ChangeDetectorRef, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { EventEmitter } from 'events';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'currency-input',
  templateUrl: './currency-input.component.html',
  styleUrls: ['./currency-input.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CurrencyInputComponent), multi: true }
  ]
})
export class CurrencyInputComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges, ControlValueAccessor {



  @Input()
  _curValue: string;
  formattedCurValue: string;
  hidden: any = false;
  @Input() placeholder: string;
  @Input() required: boolean;
  @Input() min: number;
  @Input() max: number;
  @Input() disabled?: boolean;
  @Input() commonDatePickerFilter: any;
  @Input() ngClass: string;
  @Output() valueChange = new EventEmitter();
  dateFormat = 'MM/DD/YYYY';

  @ViewChildren('formatted') formattedInput:QueryList<ElementRef>;
  @ViewChildren('original') originalInput:QueryList<ElementRef>;


  constructor(private currencyPipe: CurrencyPipe, private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
    console.log(this.originalInput, this.formattedInput);
    if (this.curValue) {
      this.setValueToControls(this.curValue);
    }
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  setValueToControls(value: any) {
   // if (value) {
      this.formattedCurValue = this.currencyPipe.transform(value);
    // } else {
    //   this.resetControlValues();
    // }

  }

  onFocus(event: any) {
    console.log('focused');
    console.log(this.curValue, this.formattedCurValue);
    // setTimeout(() => {
      this.hidden = true;      
    // }, 100);
    // this.originalInput.nativeElement.focus();
  }

  onFocusOut(event: any) {
    console.log('focusedOut');
    this.hidden = false;
    console.log(this.curValue, this.formattedCurValue);
    const val = event.target.value;
    this.setValueToControls(val);
  }

  resetControlValues() {
    this.formattedCurValue = null;
  }

  ngOnChanges(changes: any) {
    console.log('changes');
  }
  onMaskCurChanged(event: any) {
    console.log('blur', event, this.formattedCurValue);
    this.valueChange.emit(this.formattedCurValue);
    this.curValue = this.formattedCurValue;
    this.valueChange.emit(this.curValue);

  }

  onValueChange(event: any) {
    const val = event.target.value;
    this.setValueToControls(val);
  }

  onMaskDateChange() {
    
  }

  ngOnDestroy() {

  }

  get curValue() {
    return this._curValue;
  }

  set curValue(val) {
    this._curValue = val;
    this.propagateChange(this._curValue);
  }

  propagateChange = (_: any) => { };

  writeValue(obj: any): void {
    this.curValue = obj;
    this.setValueToControls(this.curValue);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {

  }

  resetDate() {
    this.formattedCurValue = null;
    this.curValue = null;
  }
}
