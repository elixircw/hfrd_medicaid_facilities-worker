import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProviderIncidentReportRoutingModule } from './provider-incident-report/provider-incident-report-routing.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ProviderIncidentReportRoutingModule
  ],
  declarations: []
})
export class ProviderIncidentModule { }
