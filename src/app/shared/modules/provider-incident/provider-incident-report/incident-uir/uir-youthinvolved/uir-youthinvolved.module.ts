import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirYouthinvolvedRoutingModule } from './uir-youthinvolved-routing.module';
import { UirYouthinvolvedComponent } from './uir-youthinvolved.component';

@NgModule({
  imports: [
    CommonModule,
    UirYouthinvolvedRoutingModule
  ],
  declarations: [UirYouthinvolvedComponent]
})
export class UirYouthinvolvedModule { }
