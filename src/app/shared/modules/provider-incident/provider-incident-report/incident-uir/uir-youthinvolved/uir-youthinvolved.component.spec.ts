import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirYouthinvolvedComponent } from './uir-youthinvolved.component';

describe('UirYouthinvolvedComponent', () => {
  let component: UirYouthinvolvedComponent;
  let fixture: ComponentFixture<UirYouthinvolvedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirYouthinvolvedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirYouthinvolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
