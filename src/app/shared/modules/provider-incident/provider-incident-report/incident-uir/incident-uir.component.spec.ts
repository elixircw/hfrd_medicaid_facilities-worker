import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentUirComponent } from './incident-uir.component';

describe('IncidentUriComponent', () => {
  let component: IncidentUirComponent;
  let fixture: ComponentFixture<IncidentUirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentUirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentUirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
