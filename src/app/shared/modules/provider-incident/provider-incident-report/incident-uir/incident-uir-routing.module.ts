import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncidentUirComponent } from './incident-uir.component';
import { IncidentUirResolverService } from './incident-uir-resolver.service';

const routes: Routes = [
  {
    path : '',
    component: IncidentUirComponent,
    resolve : {
      complaintDetail : IncidentUirResolverService
    },
    children: [
      {
        path: 'person',
        loadChildren: './uir-person/uir-person.module#UirPersonModule'
      },
      {
        path:'program',
        loadChildren:'./uir-program/uir-program.module#UirProgramModule'
      },
      {
        path:'incident',
        loadChildren:'./uir-incident/uir-incident.module#UirIncidentModule'
      },
      {
        path:'narrative',
        loadChildren:'./uir-narrative/uir-narrative.module#UirNarrativeModule'
      },
      {
        path:'notification',
        loadChildren:'./uir-notification/uir-notification.module#UirNotificationModule'
      },
      {
        path:'supervisory',
        loadChildren:'./uir-supervisory/uir-supervisory.module#UirSupervisoryModule'
      },
      {
        path:'witness',
        loadChildren:'./uir-witness/uir-witness.module#UirWitnessModule'
      },
      {
        path:'staff',
        loadChildren:'./uir-staff/uir-staff.module#UirStaffModule'
      },
      {
        path:'documents',
        loadChildren:'./uir-documents/uir-documents.module#UirDocumentsModule'
      }
  ]

  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidentUirRoutingModule { }
