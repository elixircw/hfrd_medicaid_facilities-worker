import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirHeaderComponent } from './uir-header.component';

describe('UirHeaderComponent', () => {
  let component: UirHeaderComponent;
  let fixture: ComponentFixture<UirHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
