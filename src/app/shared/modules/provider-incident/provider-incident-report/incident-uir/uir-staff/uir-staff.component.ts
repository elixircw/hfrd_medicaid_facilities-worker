import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'uir-staff',
  templateUrl: './uir-staff.component.html',
  styleUrls: ['./uir-staff.component.scss']
})
export class UirStaffComponent implements OnInit {
  assignedStaff:any[];

  staffForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _dataStore: DataStoreService) { } 
    
    ngOnInit() {
    this.addStaffForm();
    this.getassignedstaff();
  } 
  addStaffForm() {
    this.staffForm = this.formBuilder.group({     
      is_staff_assaulted: [null],
      is_injury_sustained:[null],
      injury_severity_level:[null],
      is_primary_staff:[null],

    });
  }
  getassignedstaff(){
		this._commonHttpService.create(
			{
			  // where: { provider_applicant_id: this.applicantId },
			  where: { provider_applicant_id: 'A2019005000542' },
        method: 'post'
			  //applicant_id:this.applicantNumber
			},
			'providerapplicantportal/getassignedstaff'
		
		  ).subscribe(response => {		
			this.assignedStaff = response;
			console.log(JSON.stringify('assigned staff',this.assignedStaff));
			//console.log("rahul");
		  },
			(error) => {
			  this._alertService.error('Unable to get assigned staffs, please try again.');
			  console.log('get contact Error', error);
			  return false;
			}
		  );
	}
}
