import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirGangComponent } from './uir-gang.component';

describe('UirGangComponent', () => {
  let component: UirGangComponent;
  let fixture: ComponentFixture<UirGangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirGangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirGangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
