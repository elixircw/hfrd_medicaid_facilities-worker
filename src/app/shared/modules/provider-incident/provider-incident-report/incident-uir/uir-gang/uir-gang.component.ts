import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { UirDetail } from '../_entities/uir-model';

@Component({
  selector: 'uir-gang',
  templateUrl: './uir-gang.component.html',
  styleUrls: ['./uir-gang.component.scss']
})
export class UirGangComponent implements OnInit {

  gangForm: FormGroup;
  providerUirId: string;
  providerUirDetails: UirDetail;
  gangInfo = [];

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _incidentService: IncidentUirService,
    private _providerIncinent: ProviderIncidentReportService,
    private _dataStore: DataStoreService) {
    this.providerUirId = (this._dataStore.getData('provider_uir_id')) ? this._dataStore.getData('provider_uir_id') : '';
    this.providerUirDetails = (this._dataStore.getData('provider_uir_details')) ? this._dataStore.getData('provider_uir_details') : '';
    if (this.providerUirDetails) {
      this.providerUirDetails = this.providerUirDetails[0];
    }
  }

  ngOnInit() {
    this.initGangForm();
    this.getGangInformation();
  }

  loadGangForm(gangInfo) {
    this.gangForm.patchValue({
      gang_related_explain: gangInfo.gang_related_explain,
      gang_incident_videotapped: gangInfo.gang_incident_videotapped,
      gang_support_evidence: gangInfo.gang_support_evidence,
    });
  }

  initGangForm() {
    this.gangForm = this.formBuilder.group({
      gang_related_explain: [null],
      gang_incident_videotapped: [null, Validators.required],
      gang_support_evidence: [null],
    });
  }

  patchGang() {
    const gangData = this.gangForm.value;
    gangData.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      gangData.provider_uir_id = this.providerUirId;
    }

    this._providerIncinent.saveUirDetails(gangData).subscribe(response => {
      this.resetForm();
      this._dataStore.setData('provider_uir_details', response);
      this.loadGangForm(response);
      this._alertService.success('Information saved successfully!');
    });

  }

  getGangInformation() {
    const providerIdKeyVal = (this.providerUirId) ? { provider_uir_id: this.providerUirId } : {};
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: providerIdKeyVal,
      },
      'provider_uir/list?filter'
    ).subscribe(
      (response) => {
        if (response) {
          this.gangInfo = response[0];
          this.loadGangForm(this.gangInfo);
        }
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  resetForm() {
    this.gangForm.reset();
    this.initGangForm();
  }

}
