import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirGangRoutingModule } from './uir-gang-routing.module';
import { UirGangComponent } from './uir-gang.component';
import { FormMaterialModule } from '../../../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    UirGangRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirGangComponent]
})
export class UirGangModule { }
