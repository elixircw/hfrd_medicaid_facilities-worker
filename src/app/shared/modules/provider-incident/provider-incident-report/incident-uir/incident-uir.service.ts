import { Injectable } from '@angular/core';
import { ProviderPortalUrlConfig  } from '../../../../../provider-portal/provider-portal-url.config';
import { CommonHttpService, AuthService } from '../../../../../@core/services';
import { Router } from '@angular/router';
import { UirDetail } from './_entities/uir-model';


@Injectable()
export class IncidentUirService {
  uirid: string;
  uirDetail: UirDetail;
  providerUirId: string;
  constructor(private _router: Router, private _commonHttpService:CommonHttpService,
    private authService: AuthService) { }

  openIncidentReport(){
    
    this._commonHttpService.getArrayList({}, ProviderPortalUrlConfig.EndPoint.Uir.GetNextNumberUrl).subscribe(result => {
      const url = '/provider-portal/incident-report/uir/' + result['nextNumber'] + '/program';
      this._router.navigate([url]);
    });
    
   // const url = '/provider-portal/incident-report/uir/PC201900200061/person';
    //this._router.navigate([url]);
  }
  getIncidentUirDetails(uir_id: string) {

    return this._commonHttpService.getArrayList({
      method: 'get',
      where: { uir_id: uir_id }
    }, ProviderPortalUrlConfig.EndPoint.Uir.DETAIL_URL);
  }

  getMetaData(uirid: string): any {
    const uirDetatail = {
      uir_number: uirid,
      inserted_on: new Date(),
      created_by: this.authService.getCurrentUser().user.securityusersid,
      displayname: this.authService.getCurrentUser().user.userprofile.fullname
    };
    return uirDetatail;
  }

  getProgramType() {
   return this._commonHttpService.getArrayList(
    {
      method: 'post',
      nolimit: true,
      providertype: 'public'
    },
    ProviderPortalUrlConfig.EndPoint.Uir.LIST_PROGRAMMES_URL )
  }

  getProgramNames() {
    return this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
           provider_id: '201900128'
          }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.PROVIDER_PROGRAM_NAMES_URL )
  }

  getSelectedProgramInformation(selectedSiteId: string) {
    return this._commonHttpService.create(
      {
        method:'post',
        where: 
        {
          objectid : selectedSiteId
        }        
      },
      ProviderPortalUrlConfig.EndPoint.Uir.GET_SELECTED_PROGRAM_INFO )
  }

}
