import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirProgramComponent } from './uir-program.component';

const routes: Routes = [ 
  {
    path:'',
    component:UirProgramComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirProgramRoutingModule { }
