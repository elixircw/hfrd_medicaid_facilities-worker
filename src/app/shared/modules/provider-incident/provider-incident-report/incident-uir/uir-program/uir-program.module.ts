import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirProgramRoutingModule } from './uir-program-routing.module';
import { UirProgramComponent } from './uir-program.component';
import { FormMaterialModule } from '../../../../../../@core/form-material.module';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';

@NgModule({
  imports: [
    CommonModule,
    UirProgramRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirProgramComponent],
  providers: [ProviderIncidentReportService]
})
export class UirProgramModule { }
