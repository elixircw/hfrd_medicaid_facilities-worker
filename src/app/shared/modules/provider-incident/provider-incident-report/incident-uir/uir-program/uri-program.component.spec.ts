import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirProgramComponent } from './uir-program.component';

describe('UirProgramComponent', () => {
  let component: UirProgramComponent;
  let fixture: ComponentFixture<UirProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
