import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../@core/services';
//import { ProviderPortalUrlConfig } from '../../../provider-portal-url.config';
import { IncidentUirService } from '../incident-uir.service';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';

@Component({
  selector: 'uir-program',
  templateUrl: './uir-program.component.html',
  styleUrls: ['./uir-program.component.scss']
})
export class UirProgramComponent implements OnInit {

  programNames$: Observable<DropdownModel[]>;
  programNamesDropdown: any[];
  selectedSiteId: string;
  selectedLicenseNo: string;
  licenseInformation: any;
  selectedSiteLicenseInformation: any;
  uirProgramForm: FormGroup;


  constructor(private _commonHttpService: CommonHttpService,
    private _incidentService: IncidentUirService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private _providerIncidentService: ProviderIncidentReportService,
    private _dataStore: DataStoreService) { }

  ngOnInit() {
    this.initForms();
    this.loadDropdowns();
    //console.log(this._dataStore.getData('provider_uir_id'));
  }

  loadDropdowns() {
    const source = forkJoin([
      this._incidentService.getProgramNames()
    ])
      .map((result) => {
        return {
          programNames: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.provider_nm,
                value: res.site_id
              })
          )
        }
      })
      .share();

    this.programNames$ = source.pluck('programNames');
  }


  initForms() {
    this.uirProgramForm = this.formBuilder.group({
      incident_no: [''],
      provider_nm: [''],
      program_nm: [''],
      program: [''],
      program_type: [''],
      license_no: [''],
      site_id: [null],
      phone_no: [null],
      uir_adr_street_no: [''],
      uir_adr_street_nm: [''],
      uir_adr_city_nm: [''],
      uir_adr_state_cd: [''],
      uir_adr_zip_no: [''],
      formatted_address: [null],
    });
  }


  changeSiteId(event) {
    this.selectedSiteId = event.value;
    this.getSiteLicenseInformation();
  }

  getSiteLicenseInformation() {
    this.selectedSiteLicenseInformation = {}
    this._incidentService.getSelectedProgramInformation(this.selectedSiteId)
      .subscribe(response => {
        this.selectedSiteLicenseInformation = response;
        this.selectedSiteLicenseInformation = this.selectedSiteLicenseInformation.data[0];
        this.selectedLicenseNo = this.selectedSiteLicenseInformation.license_no;
        this.patchSiteInformation();
      },
        (error) => {
          this._alertService.error('Unable to get license information, please try again.');
          console.log('get license information Error', error);
          return false;
        }
      );
  }

  patchSiteInformation() {
    this.uirProgramForm.patchValue({
      site_id: this.selectedSiteId,
      formatted_address: this.selectedSiteLicenseInformation.formatted_address,
      // uir_adr_street_no: this.selectedSiteLicenseInformation.adr_street_no,
      // uir_adr_street_nm: this.selectedSiteLicenseInformation.adr_street_nm,
      // uir_adr_city_nm: this.selectedSiteLicenseInformation.adr_city_nm,
      // uir_adr_state_cd: this.selectedSiteLicenseInformation.adr_state_cd,
      // uir_adr_zip_no: this.selectedSiteLicenseInformation.adr_zip5_no,
      program: this.selectedSiteLicenseInformation.license_level,
      phone_no: this.selectedSiteLicenseInformation.adr_work_phone_tx,
      program_type: this.selectedSiteLicenseInformation.license_type
      //license_no: this.selectedSiteLicenseInformation.license_no
    });
  }

  submitProgram() {
    if (this.uirProgramForm.valid) {
      const uirDetails = this.uirProgramForm.getRawValue();
      uirDetails.license_type = this.uirProgramForm.getRawValue().program_nm;

      if (this._dataStore.getData('provider_uir_id')) {
        uirDetails.provider_uir_id = this._dataStore.getData('provider_uir_id');
      }

      uirDetails.uir_no = this._incidentService.uirid;
      this._providerIncidentService.saveUirDetails(uirDetails).subscribe(response => {
        if (response.provider_uir_id) {
          this._dataStore.setData('provider_uir_id', response.provider_uir_id);
        }
      });
    }
  }
}
