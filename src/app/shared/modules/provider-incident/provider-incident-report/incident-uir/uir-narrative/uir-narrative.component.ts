import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'uir-narrative',
  templateUrl: './uir-narrative.component.html',
  styleUrls: ['./uir-narrative.component.scss']
})
export class UirNarrativeComponent implements OnInit {

  narrativeForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.addNarrativeForm();
    // this.loadDropDowns();
  }

  addNarrativeForm() {
    this.narrativeForm = this.formBuilder.group({
      narrative_before_incident: [null],
      narrative_incident_occur: [null],
      narrative_during_incident: [null],
      narrative_after_incident: [null],
      role_incident: [null],
      restraint_typekey: [null],
      duration_restraint: [null],
      duration_restraint_other: [null],
      is_de_escalation: [null],
      is_seen_medical: [null],
      is_injury_sustained: [null],
      injury_severity_rating: [null],
      is_injury_result: [null],
      is_seclusion: [null],
      duration_seclusion: [null]
    });
  }
  

}
