import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { IncidentUirService } from './incident-uir.service';

@Injectable()
export class IncidentUirResolverService implements Resolve<any> {

  constructor(private _service: IncidentUirService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const uirid = route.paramMap.get('uirid');
    this._service.uirid = uirid;
    return this._service.getIncidentUirDetails(uirid);
  }
}
