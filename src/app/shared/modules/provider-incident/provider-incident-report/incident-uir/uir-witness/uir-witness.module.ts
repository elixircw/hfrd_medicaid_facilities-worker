import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormMaterialModule } from '../../../../../../@core/form-material.module';

import { UirWitnessRoutingModule } from './uir-witness-routing.module';
import { UirWitnessComponent } from './uir-witness.component';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';


@NgModule({
  imports: [
    CommonModule,
    UirWitnessRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirWitnessComponent],
  providers: [ProviderIncidentReportService]
})
export class UirWitnessModule { }
