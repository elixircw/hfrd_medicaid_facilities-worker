import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';


@Component({
  selector: 'uir-witness',
  templateUrl: './uir-witness.component.html',
  styleUrls: ['./uir-witness.component.scss']
})
export class UirWitnessComponent implements OnInit {
  witnessForm: FormGroup;
  witnessInformation = [];
  currentSelectedWitnessId: string;
  providerUirId: string;
  isEditWitness: boolean;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _incidentService: IncidentUirService,
    private _providerIncinent: ProviderIncidentReportService,
    private _dataStore: DataStoreService) {

      this.providerUirId = (this._dataStore.getData('provider_uir_id')) ? this._dataStore.getData('provider_uir_id') : '';

    }

  ngOnInit() {
    this.getWitnessInformation();
    this.initWitnessForm();
  }

  getWitnessType(id) {
    return (id === 1) ? 'Youth' : 'Staff';
  }

  initWitnessForm() {
    this.witnessForm = this.formBuilder.group({
      first_name: [null],
      last_name: [null],
      actor_type_id: [null],
      other_actor_type: [null],
      witness_statement: [null]
    });
  }

  getWitnessInformation() {
    const providerIdKeyVal = (this.providerUirId) ? { provider_uir_id: this.providerUirId} : {};
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : providerIdKeyVal,
        order : 'provider_witness_id desc'
      },
      'provider_uir_witness/list?filter'
    ).subscribe(
      (response) => {
        this.witnessInformation = response;
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }
  resetForm() {
    this.witnessForm.reset();
    this.initWitnessForm();
  }

  patchWitness() {
    const witnessData = this.witnessForm.value;
    witnessData.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      witnessData.provider_uir_id = this.providerUirId;
    }

    if (this.isEditWitness) {
      witnessData.provider_uir_witness_id = this.currentSelectedWitnessId;
    }

    this._providerIncinent.saveWitnessDetails(witnessData).subscribe(response => {
        this.getWitnessInformation();
        this.resetForm();
        this.currentSelectedWitnessId = '';
        this._alertService.success('Information saved successfully!');
    });

    (<any>$('#add-witness')).modal('hide');
  }

  deleteWitness(witnessInfo) {
    this.currentSelectedWitnessId = witnessInfo.provider_uir_witness_id;
    (<any>$('#delet-witness')).modal('show');
  }

  editWitness(witnessInfo) {
    this.isEditWitness = true;
    this.currentSelectedWitnessId = witnessInfo.provider_uir_witness_id;
    (<any>$('#add-witness')).modal('show');
    this.witnessForm.patchValue({
      provider_uir_id: this.providerUirId,
      first_name: witnessInfo.first_name,
      last_name: witnessInfo.last_name,
      actor_type_id: witnessInfo.actor_type_id,
      other_actor_type: (witnessInfo.actor_type_id === '3') ? witnessInfo.other_actor_type : '',
      witness_statement: witnessInfo.witness_statement
    });
  }

  openAddWitness() {
    this.isEditWitness = false;
    this.currentSelectedWitnessId = '';
  }

  confirmDelete() {
    this._commonHttpService.deleteByPost(
      '',
      {
       provider_uir_witness_id: this.currentSelectedWitnessId
      },
      'provider_uir_witness/deletewitness').subscribe(
      (response) => {
        this.getWitnessInformation();
      },
      (error) => {
        this._alertService.error('Unable to delete Witness');
        return false;
      }
    );
  (<any>$('#delet-witness')).modal('hide');
  }

}
