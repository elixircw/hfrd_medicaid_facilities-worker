import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirWitnessComponent } from './uir-witness.component';

const routes: Routes = [
  {
    path: '',
    component: UirWitnessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirWitnessRoutingModule { }
