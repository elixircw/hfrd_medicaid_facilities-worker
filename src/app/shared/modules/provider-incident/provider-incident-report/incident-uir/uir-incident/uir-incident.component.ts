import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { IncidentUirService } from '../incident-uir.service';
import { CommonDropdownsService, AlertService, DataStoreService } from '../../../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { MatRadioButton, MatRadioChange } from '@angular/material';
import { UirDetail } from '../_entities/uir-model';

@Component({
  selector: 'uir-incident',
  templateUrl: './uir-incident.component.html',
  styleUrls: ['./uir-incident.component.scss']
})
export class UirIncidentComponent implements OnInit {

  incidentForm: FormGroup;
  locationSource$: any;
  locationSource1$: any;
  locationSource2$: any;
  class1IncidentSource$: any;
  class2IncidentSource$: any;
  class3IncidentSource$: any;
  class1Selected: any = [];
  class2Selected: any = [];
  class3Selected: any = [];
  class3Other = false;
  classMerge: any = [];
  precipitate = '';
  providerUirId: string;
  providerUirDetails: UirDetail;
  providerClassIncident: any = [];

  constructor(private formBuilder: FormBuilder,
    private _service: ProviderIncidentReportService,
    private _incidentService: IncidentUirService,
    private commonDropdownService: CommonDropdownsService,
    private _dataStore: DataStoreService) {
       this.providerUirId = (this._dataStore.getData('provider_uir_id')) ? this._dataStore.getData('provider_uir_id') : '';
  }

  ngOnInit() {
    this.providerUirDetails = this._dataStore.getData('provider_uir_details')[0];
    if (this._dataStore.getData('provider_uir_classincident')) {
      this.providerClassIncident = this._dataStore.getData('provider_uir_classincident');

      console.log(this.providerClassIncident);

      this.class1IncidentSource$ = this.providerClassIncident.filter(x => {
        return x.ref_key = x.class1_typekey;
      });

      this.class2IncidentSource$ = this.providerClassIncident.filter(x => {
        return x.ref_key = x.class2_typekey;
      });

      this.class3IncidentSource$ = this.providerClassIncident.filter(x => {
        return x.ref_key = x.class3_typekey;
      });
    }
    // console.log(this.class1IncidentSource$);
    // console.log(this.class2IncidentSource$);
    // console.log(this.class3IncidentSource$);

    this.addIncidentForm();
    this.loadDropDowns();
    setTimeout(() => this.loadFormValues(), 1000);
  }

  loadFormValues() {
    // HTML - as-investigation-plan.components.ts

    // <mat-selection-list (selectionChange)="getIndicatorsID($event)" formControlName="indicators" dense class="indicator-list">
    //     <mat-list-option *ngFor="let item of filteredAllegationItems[0]?.indicators" [selected]="item.selected" 
    //         [value]="item.indicatorid">

    //         <h3 matLine>{{item.indicatorname}} </h3>

    //     </mat-list-option>
    // </mat-selection-list>

    // this.filteredAllegationItems = this.allegationItems.filter(item => {
    //   return item.allegationid === selectedAllegation;
    // });

    this.incidentForm.patchValue({
      level_supervision: this.providerUirDetails.level_supervision,
      location_incident: this.providerUirDetails.location_incident,
      location_area: this.providerUirDetails.location_area,
      location_area_other: this.providerUirDetails.location_area_other,
      incident_date: this.providerUirDetails.incident_datetime,
      incident_time: moment(this.providerUirDetails.incident_datetime, 'HH:mm:ss').format('HH:mm:ss'),
      discovered_date: this.providerUirDetails.discovered_datetime,
      discovered_time: moment(this.providerUirDetails.discovered_datetime, 'HH:mm:ss').format('HH:mm:ss'),
    });
  }

  addIncidentForm() {
    this.incidentForm = this.formBuilder.group({
      level_supervision: [null],
      location_incident: [null],
      location_area: [null],
      location_area_other: [null],
      incident_date: [null],
      incident_time: [null],
      discovered_date: [null, Validators.required],
      discovered_time: [null, Validators.required],
      class3_Other: [null]
    });
  }

  onChangeLocationType($event: MatRadioChange) {
    this.locationSource$ = ($event.value === '1') ? this.locationSource1$ : this.locationSource2$;
  }

  loadDropDowns() {
    Observable.forkJoin([
      this.commonDropdownService.getDropownsByTable('onsite_location_area', 'OLM'),
      this.commonDropdownService.getDropownsByTable('offsite_location_area', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_I_Incidents', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_II_Incidents', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_III_Incidents', 'OLM'),
    ]).subscribe(
      ([locationSource1, locationSource2, class1IncidentSource, class2IncidentSource, class3IncidentSource]) => {
        this.locationSource1$ = locationSource1;
        this.locationSource2$ = locationSource2;
        this.locationSource$ = (this.providerUirDetails.location_incident === '1') ? this.locationSource1$ : this.locationSource2$;
        this.class1IncidentSource$ = class1IncidentSource;
        this.class2IncidentSource$ = class2IncidentSource;
        this.class3IncidentSource$ = class3IncidentSource;
      });

  }

  onChange(event, index, item, classType) {
    if (event.checked) {
      this.classMerge.push(item);
    } else {
      const itemMergeIndex =  this.classMerge.findIndex( x => (x.ref_key === item.ref_key && x.ref_key !== 'Other') );
      this.classMerge.splice(itemMergeIndex, 1);
    }
    switch (classType) {
      case 'class1':
        if (event.checked) {
          this.class1Selected.push({class1_typekey: item.ref_key});
        } else {
          const itemIndex = this.class1Selected.findIndex( x => x.class1_typekey === item.ref_key );
          this.class1Selected.splice(itemIndex, 1);
        }
        break;
      case 'class2':
        if (event.checked) {
          this.class2Selected.push({class2_typekey: item.ref_key});
        } else {
          const itemIndex = this.class2Selected.findIndex( x => x.class2_typekey === item.ref_key );
          this.class2Selected.splice(itemIndex, 1);
        }
        break;
      case 'class3':
        if (event.checked) {
          this.class3Selected.push({class3_typekey: item.ref_key});
        } else {
          const itemIndex = this.class3Selected.findIndex( x => x.class3_typekey === item.ref_key );
          this.class3Selected.splice(itemIndex, 1);
        }
        const isOther = this.class3Selected.findIndex( x => x.class3_typekey === 'Other' );
        this.class3Other = (isOther !== -1) ? true : false;
        break;
    }
  }

  onChangePrecipitate(item) {
    this.precipitate = item.ref_key;
  }

  selectPrecipitaeEvent() {
    (<any>$('#precipitate-event')).modal('show');
  }

  closeDeleteModal() {
    (<any>$('#precipitate-event')).modal('hide');
  }

  submitIncident() {
    if (this.incidentForm.valid) {
      const uirDetails = this.incidentForm.getRawValue();
      const date1 = moment(this.incidentForm.getRawValue().discovered_date).format('MM/DD/YYYY') + ' ' + this.incidentForm.getRawValue().discovered_time;
      const date2 = moment(this.incidentForm.getRawValue().incident_date).format('MM/DD/YYYY') + ' ' + this.incidentForm.getRawValue().incident_time;
      uirDetails.discovered_datetime = moment(date1).format();
      uirDetails.incident_datetime = moment(date2).format();
      uirDetails.uir_no = this._incidentService.uirid;
      uirDetails.class1_typekey = this.class1Selected;
      uirDetails.class2_typekey = this.class2Selected;
      uirDetails.class3_typekey = this.class3Selected;
      if (this.providerUirId) {
        uirDetails.provider_uir_id = this.providerUirId;
      }
      uirDetails.class3Other = (this.class3Other) ? this.incidentForm.getRawValue().class3_Other : null;
      uirDetails.precipitate_event = this.precipitate;
      this._service.saveIncidentUirDetails(uirDetails).subscribe(response => {
        (<any>$('#precipitate-event')).modal('hide');
        this.incidentForm.reset();
      });
    }
  }
}
