import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirIncidentComponent } from './uir-incident.component';

const routes: Routes = [ 
  {
    path:'',
    component:UirIncidentComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirIncidentRoutingModule { }
