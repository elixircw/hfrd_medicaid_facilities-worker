import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirDocumentsComponent } from './uir-documents.component';

describe('ProviderComplaintDocumentsComponent', () => {
  let component: UirDocumentsComponent;
  let fixture: ComponentFixture<UirDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
