import { Component, OnInit } from '@angular/core';
//import { ComplaintDetailsService } from '../complaint-details.service';
import { AttachmentService } from '../../../../../../provider-portal/provider-portal-utils/attachment/attachment.service';
import { AttachmentConfig } from '../../../../../../provider-portal/provider-portal-utils/attachment/_entities/attachment.data.models';
import { Router, ActivatedRoute } from '@angular/router';
import { IncidentUirService } from '../incident-uir.service';
import { DataStoreService } from '../../../../../../@core/services';



@Component({
  selector: 'uir-documents',
  templateUrl: './uir-documents.component.html',
  styleUrls: ['./uir-documents.component.scss']
})
export class UirDocumentsComponent implements OnInit {
  uir_number: string;

  constructor(private attachService: AttachmentService,
    private _router: Router, private route: ActivatedRoute,
    private _service: IncidentUirService
    ) {
      this.uir_number = _service.uirid; //route.snapshot.params['id'];
      const attachmentConfig: AttachmentConfig = new AttachmentConfig();      
      attachmentConfig.uniqueNumber = this.uir_number;
      this.attachService.setAttachmentConfig(attachmentConfig);
      }

  ngOnInit() {

    }

}
