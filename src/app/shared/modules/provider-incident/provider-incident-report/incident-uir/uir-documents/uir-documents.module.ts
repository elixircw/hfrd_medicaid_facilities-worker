import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirDocumentsRoutingModule } from './uir-documents-routing.module';
import { UirDocumentsComponent } from './uir-documents.component';
import { FormMaterialModule } from '../../../../../../@core/form-material.module';
import { AttachmentModule } from '../../../../../../provider-portal/provider-portal-utils/attachment/attachment.module';

 

@NgModule({
  imports: [
    CommonModule,
    UirDocumentsRoutingModule,
    FormMaterialModule,
    AttachmentModule
  ],
  declarations: [UirDocumentsComponent]
})
export class UirDocumentsModule { }
