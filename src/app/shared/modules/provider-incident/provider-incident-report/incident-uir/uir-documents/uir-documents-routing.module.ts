import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirDocumentsComponent } from './uir-documents.component';



const routes: Routes = [{
  path: '',
  component: UirDocumentsComponent,
  
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirDocumentsRoutingModule { }
