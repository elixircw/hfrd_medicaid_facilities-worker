import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirYouthInvolvedComponent } from './uir-youth-involved.component';

const routes: Routes = [
  {
    path: '',
    component: UirYouthInvolvedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirYouthInvolvedRoutingModule { }
