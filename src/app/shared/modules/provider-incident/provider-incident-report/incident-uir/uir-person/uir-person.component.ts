import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../@core/services';
import {MatTableDataSource} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { IncidentUirService } from '../incident-uir.service';
export interface StaffInfoTable {
  firstName: string;
  lastName: string;
  affiliationType:string;
  jobTitle:string;
  employeeType:string;
 // provider_staff_id:string;

  }
@Component({
  selector: 'uir-person',
  templateUrl: './uir-person.component.html',
  styleUrls: ['./uir-person.component.scss']
})
export class UirPersonComponent implements OnInit {
 youthForm: FormGroup;
 staffForm: FormGroup;
 foasterForm: FormGroup;
 affiliationType = [];
 employeeType = [];
 reasonForLeaving = [];
 rccCertificateType = [];
 showCertification: boolean;
 currentSelectedProviderStaffId: string;
 currentSelectedYouthId: string;
 staffInformation = [];
 youthInformation = [];
 isEditStaff: boolean;
 isEndDate: boolean;
 isPhoneValid: boolean;
 currenbtStaffInfo = [];
 dataSource: any;
 maxDate = new Date();
 displayedColumns: string[] = ['select', 'firstName', 'lastName', 'affiliationType', 'jobTitle', 'employeeType'];
 selection = new SelectionModel < StaffInfoTable > (true, []);
 staffArray: any[];
 assignedStaff: any[];
 providerUirId: string;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _dataStore: DataStoreService,
    private _incidentService: IncidentUirService,
    private _providerIncinent: ProviderIncidentReportService,
    ) { }

  ngOnInit() {
    this.providerUirId = (this._dataStore.getData('provider_uir_id')) ? this._dataStore.getData('provider_uir_id') : '';
    console.log(this.providerUirId);
    this.initializeYouthForm();
    this.initializeStaffForm();
    this.getStaffInformation();
    this.getYouthInformation();
    this.affiliationType = ["Employee","Board Member", "Volunteer","Intern Information","Temp Employee"];
    this.employeeType = ["Program Administrator","Residential Child and youth care practitioner","N/A"];
    this.reasonForLeaving = ["Resigned","Retired","Death","Terminated"];
    this.rccCertificateType = ["Certified Program Administrator","Residential Child and youth care practitioner certification"];
    this.initializeFosterForm();
    this.getassignedstaff();
  }

  private initializeYouthForm() {
    this.youthForm = this.formBuilder.group({
      first_name: [null],
      last_name: [null],
      address: [null],
      phone_no: [null],
      gender: [null],
      dob: [null],
      youth_identifier: [null],
      placing_agency: [null]
    });
  }

  private initializeStaffForm() {
    this.staffForm = this.formBuilder.group({
      provider_id: 201900413,
      employee_first_nm: [''],
      employee_last_nm: [''],
      affiliation_type: [''],
      job_title: [''],
      employee_type: null,
      personnel_start_date: null,
      personnel_end_date: null,
      reason_for_leaving: [''],
      behavioral_interventions_training: [''],
      cps_clearance_request_date: null,
      cps_clearance_result_date: null,
      cps_rcc_compliant: [''],
      cps_cpa_compliant: [''],
      federal_clearance_request_date: null,
      federal_clearance_result_date: null,
      federal_rcc_compliant: [''],
      federal_cpa_compliant: [''],
      state_clearance_request_date: null,
      state_clearance_result_date: null,
      state_rcc_compliant: [''],
      state_cpa_compliant: [''],
      rcc_certificate_type: [''],
      rcc_certificate_due_date: null,
      rcc_date_application_mailed: null,
      rcc_date_tested: null,
      rcc_certification_effective_date: null,
      rcc_certification_number: [''],
      rcc_certification_renewal_date: null,
      comments: [''],
    });
  }

  private initializeFosterForm() {
    this.foasterForm = this.formBuilder.group({
      provider_id: 201900413,
      foaster_first_nm: [''],
      foaster_last_nm: [''],
      foaster_address: [''],
      foaster_phone_no: ['']
    });
  }

  // getStaffInformation() {
  //   this._commonHttpService.getArrayList(
  //     {
  //       method: 'get',
  //       nolimit: true,
  //       where : {provider_id : 201900413},
  //       order : 'provider_staff_id desc'
  //     },
  //     'providerstaff?filter'
  //   ).subscribe(
  //     (response) => {
  //       this.staffInformation = response;
  //       console.log(this.staffInformation);
  //       // this.staffForm.patchValue(response);
  //     },
  //     (error) => {
  //       this._alertService.error('Unable to retrieve information');
  //     }
  //   );
  // }
  getStaffInformation() {
	
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : {provider_uir_id : this.providerUirId},
        order : 'provider_staff_id desc'
      },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.staffInformation = response;
        this.dataSource = new MatTableDataSource < StaffInfoTable > (this.staffInformation);
        console.log("Rahul get Staff info" + this.staffInformation);

      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }
  
  isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}
	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}
	assignStaff() {
		this.staffArray = this.selection.selected;		
		var selectedstaffs = [];		
	//	selectedstaffs.push({provider_applicant_id:this.applicantId});
		for(var i=0; i<this.staffArray.length;i++){
			selectedstaffs.push({provider_staff_id:this.staffArray[i].provider_staff_id});
		}
		var payload = {}
    // payload['applicant_id'] =this.applicantId ;
    payload['applicant_id'] ='A2019005000542';
		payload['staff'] = selectedstaffs;
		console.log('payload staff',payload);
		this._commonHttpService.create(
			payload,
			'providerapplicantportal/addapplicantstaff'
		).subscribe(
			(response) => {
			this._alertService.success("Staffs assigned successfully!");
			this.getassignedstaff();
			( < any > $('#assign-staff')).modal('hide');
		},
			(error) => {
			this._alertService.error("Unable to assign staffs");
			});
		
		this._alertService.success("Staffs assigned successfully!");    

	}
	getassignedstaff(){
		this._commonHttpService.create(
			{
			  // where: { provider_applicant_id: this.applicantId },
			  where: { provider_applicant_id: 'A2019005000542' },
        method: 'post'
			  //applicant_id:this.applicantNumber
			},
			'providerapplicantportal/getassignedstaff'
		
		  ).subscribe(response => {		
			this.assignedStaff = response;
			console.log(JSON.stringify('assigned staff',this.assignedStaff));
			//console.log("rahul");
		  },
			(error) => {
			  this._alertService.error('Unable to get assigned staffs, please try again.');
			  console.log('get contact Error', error);
			  return false;
			}
		  );
	}


  openAddStaffMembers() {
    this.isEditStaff = false;
    this.showCertification = false;
    this.isEndDate = false;
  }

  isEmployeeType(employeeType){
    if(employeeType === "Residential Child and youth care practitioner" || employeeType=="Program Administrator" ){
      this.showCertification=true;
    }
    else if(employeeType=="N/A"){
      this.showCertification=false;
    }
  }

  editStaffForm(editStaff) {
    console.log(editStaff);
    this.currentSelectedProviderStaffId = editStaff.provider_staff_id;
    (<any>$('#add-staff')).modal('show');
    this.isEditStaff = true;
    this.staffForm.patchValue({
        provider_id: 201900413,
        employee_first_nm: editStaff.employee_first_nm,
        employee_last_nm: editStaff.employee_last_nm,
        affiliation_type: editStaff.affiliation_type,
        job_title: editStaff.job_title,
        employee_type: editStaff.employee_type,
        personnel_start_date: editStaff.personnel_start_date,
        personnel_end_date: editStaff.personnel_end_date,
        reason_for_leaving: editStaff.reason_for_leaving,
        behavioral_interventions_training: editStaff.behavioral_interventions_training,
        cps_clearance_request_date: editStaff.cps_clearance_request_date,
        cps_clearance_result_date: editStaff.cps_clearance_result_date,
        cps_rcc_compliant: editStaff.cps_rcc_compliant,
        cps_cpa_compliant: editStaff.cps_cpa_compliant,
        federal_clearance_request_date: editStaff.federal_clearance_request_date,
        federal_clearance_result_date: editStaff.federal_clearance_result_date,
        federal_rcc_compliant: editStaff.federal_rcc_compliant,
        federal_cpa_compliant: editStaff.federal_cpa_compliant,
        state_clearance_request_date: editStaff.state_clearance_request_date,
        state_clearance_result_date: editStaff.state_clearance_result_date,
        state_rcc_compliant: editStaff.state_rcc_compliant,
        state_cpa_compliant: editStaff.state_cpa_compliant,
        rcc_certificate_type: editStaff.rcc_certificate_type,
        rcc_certificate_due_date: editStaff.rcc_certificate_due_date,
        rcc_date_application_mailed: editStaff.rcc_date_application_mailed,
        rcc_date_tested: editStaff.rcc_date_tested,
        rcc_certification_effective_date: editStaff.rcc_certification_effective_date,
        rcc_certification_number: editStaff.rcc_certification_number,
        rcc_certification_renewal_date: editStaff.rcc_certification_renewal_date,
        comments: editStaff.comments
 
    });
  }

  resetStaffForm() {
    this.staffForm.reset();
    this.initializeStaffForm();
  }

  resetYouthForm() {
    this.youthForm.reset();
    this.initializeYouthForm();
  }

  personnelEndDate(){
    this.isEndDate= true;
  }


  createProviderStaff() {
    console.log(this.staffForm.value);
    this._commonHttpService.create(
      this.staffForm.value,
      'providerstaff'
    ).subscribe(
      (response) => {
                this._alertService.success("Information saved, Please add clearance info!");
        this.currentSelectedProviderStaffId = response.provider_staff_id;
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
  }

  patchProviderStaff() {
    this._commonHttpService.patch(
      this.currentSelectedProviderStaffId,
      this.staffForm.value,
      'providerstaff'
    ).subscribe(
      (response) => {
        this.getStaffInformation();
        this.resetStaffForm();
        this._alertService.success('Information saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
    (<any>$('#add-staff')).modal('hide');
  }

  patchYouthForm() {
    const youthInfo = this.youthForm.value;
    youthInfo.uir_actor_type = 'youth';
    youthInfo.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      youthInfo.provider_uir_id = this.providerUirId;
    }

    // if (this.isEditWitness) {
    //   witnessData.provider_uir_witness_id = this.currentSelectedWitnessId;
    // }

    this._providerIncinent.saveYouthDetails(youthInfo).subscribe(response => {
        this.getYouthInformation();
        this.resetYouthForm();
        //this.currentSelectedWitnessId = '';
        this._alertService.success('Information saved successfully!');
    });
    (<any>$('#add-youth')).modal('hide');
  }

  getYouthInformation() {
    console.log(111)
    console.log(this.providerUirId);
    const whereClause = (this.providerUirId) ? { provider_uir_id : this.providerUirId} : '';
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : whereClause,
        order : 'provider_youth_id desc'
      },
      'provider_uir_actor_detail/list?filter'
    ).subscribe(
      (response) => {
        this.youthInformation = response;
        //this.dataSource = new MatTableDataSource < youthInfoTable > (this.youthInformation);
        console.log("Rahul get youth info" + this.youthInformation);

      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  deleteStaffForm(staffInfo) {
    this.currentSelectedProviderStaffId = staffInfo.provider_staff_id;
    (<any>$('#delet-staff')).modal('show');
  }

  confirmDelete() {
    this._commonHttpService.remove(
      this.currentSelectedProviderStaffId,
      {
        where: {provider_staff_id: this.currentSelectedProviderStaffId}
      },
      'providerstaff').subscribe(
      (response) => {
        this.getStaffInformation();
      },
      (error) => {
        this._alertService.error('Unable to delete Staff');
        return false;
      }
    );
  (<any>$('#delet-staff')).modal('hide');
  }

  phoneValidation(event: any) {
    this.isPhoneValid = false;
    const pattern = /^\d{10}$/;
    if (!pattern.test(event.target.value)) {
      this.isPhoneValid = false;
    } else {
      this.isPhoneValid = true;
    }
  }

  resetFoasterForm() {

  }
}
