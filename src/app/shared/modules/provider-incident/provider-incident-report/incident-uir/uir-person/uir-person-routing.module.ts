import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirPersonComponent } from './uir-person.component';

const routes: Routes = [
  {
    path:'',
    component:UirPersonComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirPersonRoutingModule { }
