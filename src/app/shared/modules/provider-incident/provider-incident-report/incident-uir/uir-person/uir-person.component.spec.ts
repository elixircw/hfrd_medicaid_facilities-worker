import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirPersonComponent } from './uir-person.component';

describe('UirPersonComponent', () => {
  let component: UirPersonComponent;
  let fixture: ComponentFixture<UirPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
