import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UirPersonRoutingModule } from './uir-person-routing.module';
import { UirPersonComponent } from './uir-person.component';
import { FormMaterialModule } from '../../../../../../@core/form-material.module';
import { IncidentUirService } from '../incident-uir.service';

@NgModule({
  imports: [
    CommonModule,
    UirPersonRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirPersonComponent],
  providers: [IncidentUirService]
})
export class UirPersonModule { }
