import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirNotificationComponent } from './uir-notification.component';

const routes: Routes = [ 
  {
    path:'',
    component:UirNotificationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirNotificationRoutingModule { }
