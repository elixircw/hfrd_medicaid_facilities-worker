import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderIncidentReportComponent } from './provider-incident-report.component';

describe('ProviderIncidentReportComponent', () => {
  let component: ProviderIncidentReportComponent;
  let fixture: ComponentFixture<ProviderIncidentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderIncidentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderIncidentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
