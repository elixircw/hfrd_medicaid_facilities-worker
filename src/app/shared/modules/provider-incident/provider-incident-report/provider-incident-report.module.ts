import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderIncidentReportRoutingModule } from './provider-incident-report-routing.module';
import { ProviderIncidentReportComponent } from './provider-incident-report.component';
import { ProviderIncidentReportService } from './provider-incident-report.service';
import { IncidentReportDashboardComponent } from './incident-report-dashboard/incident-report-dashboard.component';
import { IncidentReportDashboardService } from './incident-report-dashboard/incident-report-dashboard.service';
import { IncidentUirService } from './incident-uir/incident-uir.service';
import { UirDashboardComponent } from './incident-report-dashboard/uir-dashboard/uir-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    ProviderIncidentReportRoutingModule,
  ],
  declarations: [ProviderIncidentReportComponent, IncidentReportDashboardComponent, UirDashboardComponent],
  providers: [ProviderIncidentReportService, IncidentReportDashboardService, IncidentUirService],
  exports: [ProviderIncidentReportComponent, IncidentReportDashboardComponent, UirDashboardComponent]
})
export class ProviderIncidentReportModule { }
