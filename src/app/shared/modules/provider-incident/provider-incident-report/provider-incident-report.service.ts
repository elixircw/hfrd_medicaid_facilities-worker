import { Injectable } from '@angular/core';
import { ProviderPortalUrlConfig  } from '../../../../provider-portal/provider-portal-url.config';
import { CommonHttpService } from '../../../../@core/services';
import { Router } from '@angular/router';
import { UirDetail, YouthInvolved } from './incident-uir/_entities/uir-model';


@Injectable()
export class ProviderIncidentReportService {
  constructor(private _router: Router, private _commonHttpService: CommonHttpService) { }

  openIncidentReport() {
    this._commonHttpService.getArrayList({}, ProviderPortalUrlConfig.EndPoint.Uir.GetNextNumberUrl).subscribe(result => {
      console.log(this._router.url);
      const url = '../uir/' + result['nextNumber'] + '/program';
      this._router.navigate([url]);
    });
  }

  saveUirDetails(uirDetails: UirDetail) {
    return this._commonHttpService.create(uirDetails, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_URL);
  }

  saveIncidentUirDetails(uirDetails: UirDetail) {
    return this._commonHttpService.create(uirDetails, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_INCIDENT);
  }

  saveWitnessDetails(witness) {
    return this._commonHttpService.create(witness, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_WITNESS);
  }

  saveYouthDetails(youth) {
    return this._commonHttpService.create(youth, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_YOUTHFOSTER);
  }
  saveYouthInvolvedDetails(youthInvolvedDetail: YouthInvolved) {
    return this._commonHttpService.create(youthInvolvedDetail, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_YOUTHINVOLVED);
  }
  getIncidentDropList(filter: any) {
    return this._commonHttpService.getAllFilter(filter, ProviderPortalUrlConfig.EndPoint.Uir.LIST_URL);
  }

  getIncidentRecordDashboard(filter: any) {
    return this._commonHttpService.getAllFilter(filter, ProviderPortalUrlConfig.EndPoint.Uir.LIST_URL);
  }

}


