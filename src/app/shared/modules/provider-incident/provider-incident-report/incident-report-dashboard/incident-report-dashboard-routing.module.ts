import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncidentReportDashboardComponent } from './incident-report-dashboard.component';
import { UirDashboardComponent } from './uir-dashboard/uir-dashboard.component';


const routes: Routes = [
  {
    path : '',
    component: IncidentReportDashboardComponent,
    children: [
      // { path: 'audio-record', component: AudioRecordComponent },
      // { path: 'video-record', component: VideoRecordComponent },
      // { path: 'image-record', component: ImageRecordComponent },
      // { path: 'scan-attachment', component: ScanAttachmentComponent},
      { path: 'dashboard', component: UirDashboardComponent }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidentReportDashboardRoutingModule { }
