import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncidentReportDashboardRoutingModule } from './incident-report-dashboard-routing.module';
import { IncidentReportDashboardComponent } from './incident-report-dashboard.component';
import { FormArrayName } from '@angular/forms';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { ProviderIncidentReportService } from '../provider-incident-report.service';
import { UirDashboardComponent } from './uir-dashboard/uir-dashboard.component';
import { IncidentReportDashboardService } from './incident-report-dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    IncidentReportDashboardRoutingModule,
    FormMaterialModule
  ],
  declarations: [IncidentReportDashboardComponent, UirDashboardComponent],
  exports: [ IncidentReportDashboardComponent, UirDashboardComponent],
  providers: [IncidentReportDashboardService, ProviderIncidentReportService]
})
export class IncidentReportDashboardModule { }
