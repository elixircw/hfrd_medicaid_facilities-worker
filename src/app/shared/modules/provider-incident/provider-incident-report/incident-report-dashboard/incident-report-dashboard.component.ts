import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { ProviderIncidentReportService } from '../provider-incident-report.service';
import { CommonHttpService, CommonDropdownsService, DataStoreService } from '../../../../../@core/services';
//import { ProviderPortalUrlConfig } from '../../provider-portal-url.config';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'incident-report-dashboard',
  templateUrl: './incident-report-dashboard.component.html',
  styleUrls: ['./incident-report-dashboard.component.scss']
})
export class IncidentReportDashboardComponent implements OnInit {

  uirList: any = [];
  constructor(private _router: Router, private _incidentService: ProviderIncidentReportService,
    private _commonHttpService: CommonHttpService, private commonDropdownService: CommonDropdownsService, 
    private _dataStore: DataStoreService) { }

  ngOnInit() {
    this.getIncidentRecordDashboard();
  }

  onNewIncidentReport() {
    this._incidentService.openIncidentReport();
    this._dataStore.setData('provider_uir_id', '');
  }

  getIncidentRecordDashboard() {
    // alert(2)
    // const param = {"count":-1,"limit":10,"method":"post","where":{"status":"pending"}};
    // this.uirList = this._incidentService.getIncidentRecordDashboard(param);
     // this.commonDropdownService.getDropownsByTable('offsite_location_area', 'OLM');

    // const data = {
    //   "provider_uir_id":"4abba0c0-9917-431a-a624-00d5842b54a3",
    //   "provider_staff_id":1

    // };
    // return this._commonHttpService.getArrayList(
    //   data,
    //   'provider_uir_actor_detail/addupdate'
    // );

    // const param = {'count': -1, 'limit': 10, 'method': 'post', 'where': {'status': 'pending'}};
    // this._incidentService.getIncidentRecordDashboard(param);
    // this._incidentService.openIncidentReport();
// console.log(ProviderPortalUrlConfig.EndPoint.Uir.LIST_URL);
// console.log({
//   limit: this.paginationInfo.pageSize,
//   order: '',
//   page: this.paginationInfo.pageNumber,
//   count: this.paginationInfo.total
//   //where: {'status': 'pending'}
// });
    // const source = this._commonHttpService
    // .getAllFilter(
    //     {
    //         limit: this.paginationInfo.pageSize,
    //         order: '',
    //         page: this.paginationInfo.pageNumber,
    //         count: this.paginationInfo.total
    //         //where: {'status': 'pending'}
    //     },
    //     ProviderPortalUrlConfig.EndPoint.Uir.LIST_URL
    // )
    // .map((result) => {
    //   console.log(1111);
    //   console.log(result);
    //     // return {
    //     //     data: result.data,
    //     //     count: result.count,
    //     //     canDisplayPager: result.count > this.paginationInfo.pageSize
    //     // };
    // })
    // .share();

  }

  openExistingComplaint() {}
}
