import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DynamicObject, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { DataStoreService, CommonHttpService } from '../../../../../../@core/services';
import { PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { IncidentReportDashboardService } from '../incident-report-dashboard.service';



@Component({
  // tslint:disable-next-line:component-selector
  selector: 'uir-dashboard',
  templateUrl: './uir-dashboard.component.html',
  styleUrls: ['./uir-dashboard.component.scss']
})
export class UirDashboardComponent implements OnInit {

  totalRecords: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  dynamicObject: DynamicObject = {};
  private pageStream$ = new Subject<number>();
  private searchTermStream$ = new Subject<DynamicObject>();
  previousPage: number;
  uirList = [];
  tabs = [];
  status: string;
  constructor(private _service: IncidentReportDashboardService,
    private _dataStore: DataStoreService,
    private _commonHttpService: CommonHttpService
    ) { }

  ngOnInit() {
    this.paginationInfo.sortBy = 'desc';
    this.paginationInfo.sortColumn = 'updateddate';
    this.status = this.tabs.length ? this.tabs[0].filterName : '';
    this.loadDashboardList(1, this.status);
  }

  // getPage(selectPageNumber: number, status: string) {
  //   this.status = status;
  //   const pageSource = this.pageStream$.map(pageNumber => {
  //     if (this.paginationInfo.pageNumber !== 1) {
  //       this.previousPage = pageNumber;
  //     } else {
  //       this.previousPage = this.paginationInfo.pageNumber;
  //     }
  //     return { search: this.dynamicObject, page: this.previousPage };
  //   });
  //   return this._service.loadDashboardList(this.paginationInfo.pageSize, this.previousPage, { status: this.status, complaint_number: this.search_complaint_number }).subscribe(result => {
  //     this.uirList = result.data;
  //     this.totalRecords = result.count;
  //   });
  // }

  loadDashboardList(selectPageNumber: number, status: string) {
    this.status = status;
    const pageSource = this.pageStream$.map(pageNumber => {
      if (this.paginationInfo.pageNumber !== 1) {
        this.previousPage = pageNumber;
      } else {
        this.previousPage = this.paginationInfo.pageNumber;
      }
      return { search: this.dynamicObject, page: this.previousPage };
    });
    this._service.getDashboardList(this.paginationInfo.pageSize, this.previousPage, { status: this.status }).subscribe(result => {
      this.uirList = result.data;
      this.totalRecords = result.count;
    });
  }

  setProviderUirId(providerUirId) {
    this._dataStore.setData('provider_uir_id', providerUirId);
    this._commonHttpService.getSingle(new PaginationRequest({
      where: {
        provider_uir_id: providerUirId
      },
      method: 'get',
      count: -1
    }), 'provider_uir/list?filter').subscribe((data) => {
      this._dataStore.setData('provider_uir_details', data);
    });

    this._commonHttpService.getSingle(new PaginationRequest({
      where: {
        provider_uir_id: providerUirId
      },
      method: 'get',
      count: -1
    }), 'provider_uir_classincident/list?filter').subscribe((data) => {
      this._dataStore.setData('provider_uir_classincident', data);
    });

  }
}
