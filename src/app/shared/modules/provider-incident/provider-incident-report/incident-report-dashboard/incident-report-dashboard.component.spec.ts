import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentReportDashboardComponent } from './incident-report-dashboard.component';

describe('IncidentReportDashboardComponent', () => {
  let component: IncidentReportDashboardComponent;
  let fixture: ComponentFixture<IncidentReportDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentReportDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentReportDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
