import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderIncidentReportComponent} from './provider-incident-report.component';
import { IncidentReportDashboardComponent } from './incident-report-dashboard/incident-report-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: ProviderIncidentReportComponent,
    children: [
      // { path: 'audio-record', component: AudioRecordComponent },
      // { path: 'video-record', component: VideoRecordComponent },
      // { path: 'image-record', component: ImageRecordComponent },
      // { path: 'scan-attachment', component: ScanAttachmentComponent},
      { path: 'dashboard', component: IncidentReportDashboardComponent }
     ]
  },
  // {
  //   path: 'dashboard',
  //   loadChildren: './incident-report-dashboard/incident-report-dashboard.module#IncidentReportDashboardModule'
  // },
  // {
  //   path: 'uir/:uirid',
  //   loadChildren: './incident-uir/incident-uir.module#IncidentUirModule'
  // }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderIncidentReportRoutingModule { }
