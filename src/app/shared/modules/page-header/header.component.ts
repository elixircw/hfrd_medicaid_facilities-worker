import { Component, OnInit, AfterViewInit, Directive, HostListener, EventEmitter, Output, ElementRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../../environments/environment';

import { Location } from '@angular/common';
import { AppConstants } from '../../../@core/common/constants';
import { hasMatch } from '../../../@core/common/initializer';
import { AppUser, UserProfile } from '../../../@core/entities/authDataModel';
import { AuthService, CommonHttpService, DataStoreService, SessionStorageService, AlertService } from '../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../pages/case-worker/case-worker-url.config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { FileError, NgxfUploaderService, UploadStatus } from 'ngxf-uploader';
import { config } from '../../../../environments/config';
import { AppConfig } from '../../../app.config';
import { HttpHeaders } from '@angular/common/http';
import { UserRoleProfile } from '../../../pages/admin/user-security-profile/_entites/user-security-profile.data.modal';

import * as appSettingsJson from '../../../../environments/version.json';

@Directive({ selector: 'div[clickoutside]' })
export class ClickOutside {
    @Output() outsidetrigger = new EventEmitter<MouseEvent>();
    constructor(private elementRef: ElementRef) { }
    @HostListener('document:click', ['$event']) clickedOutside(event: MouseEvent): void {
        const targetElement = event.target as HTMLElement;
        if (targetElement && !this.elementRef.nativeElement.contains(targetElement)) {
            this.outsidetrigger.emit(event);
        }
    }
}
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
    pushRightClass = 'push-right';
    userInfo: AppUser;
    today = Date.now();
    role = '';
    agency = '';
    totalNotificationCount = 10;
    showNotification = false;
    isPreIntake = false;
    dashBoardLink = '';
    isDjs = false;
    ROLES = AppConstants.ROLES;
    feedbackUser: UserProfile;
    curDate: Date;
    feedbackForm: FormGroup;
    supportNo: number;
    fileUploaded: any;
    reports: any;
    nytdreport: any;
    modulecoll: boolean = false;
    activeModule: string = '';
    activeModuleNavAccess = [];
    allowedModules = [];
    imagefile;
    public imagePath;
    imgURL: any;
    isUploading = false;
    uploadedFile = [];
    isAttachType = '';
    isCate = 'CW-CJAMS';
    issubCate= 'CW-Other Document';
    attachmenttype= 'case';
    addUpdateUserProfile: UserRoleProfile = new UserRoleProfile();
    user: { userphoto: any; };
    appSettings = appSettingsJson;

    // rolesColl = [{
    //     moduleName: 'Intake',
    //     rolename: 'Intake Worker'
    // }, {
    //     moduleName: 'Case Work',
    //     rolename: 'field'
    // }, {
    //     moduleName: 'Approve',
    //     rolename: 'apcs'
    // }, {
    //     moduleName: 'Provider',
    //     rolename: 'LDSS_DIRECTOR'
    // }, {
    //     moduleName: 'Finance',
    //     rolename: 'FW'
    // }, {
    //     moduleName: 'Payment',
    //     rolename: 'FW'
    // }, {
    //     moduleName: 'Reports',
    //     rolename: 'apcs'
    // }, {
    //     moduleName: 'Find',
    //     rolename: 'Intake Worker'
    // }, {
    //     moduleName: '4E',
    //     rolename: 'IV-E Specialist'
    // }, {
    //     moduleName: 'Admin',
    //     rolename: 'superuser'
    // }]
    constructor(
        public router: Router,
        private _authService: AuthService,
        private _service: CommonHttpService,
        private _dataStoreService: DataStoreService,
        private _sessionStorage: SessionStorageService,
        private _uploadService: NgxfUploaderService,
        private _formBuild: FormBuilder,
        private _alert: AlertService,
        private location: Location
    ) {
        this.router.events.subscribe(val => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        const workEnv = config.workEnvironment;
        this.userInfo = this._authService.getCurrentUser();
        console.log(this.userInfo);
        this.isDjs = this._authService.isDJS();
        if (this.userInfo && this.userInfo.user && this.userInfo.user.userprofile) {
        if (workEnv === 'state') {
        this.imgURL = AppConfig.baseUrl + '/attachment/v1' + this.userInfo.user.userprofile.userphoto;
        } else {
            this.imgURL = this.userInfo.user.userprofile.userphoto;
        }
        console.log(this.imgURL, 'this.imgURLthis.imgURL');
        }
        if (!this._sessionStorage.getItem('activeModuleNav')) {
            this._sessionStorage.setItem('activeModuleNav', this.activeModule);
            this.initializeForm();
            if (this.userInfo && this.userInfo.role) {
                if (this.userInfo.role) {
                    this.role = this.userInfo.role.name;
                }
                if (this.userInfo.user && this.userInfo.user.userprofile) {
                    this.agency = this.userInfo.user.userprofile.teamtypekey;
                    this._sessionStorage.setItem('activeModuleAgency', this.agency);
                }
                this.getNotificationCount();
            }
        } else {
            this.activeModule = this._sessionStorage.getItem('activeModuleNav');
            this.role = this._sessionStorage.getItem('activeModuleRole');
            this.agency = this._sessionStorage.getItem('activeModuleAgency');
        }
        this.setActiveModules();

        //  else {
        //     this.router.navigate(['login']);
        // }
        if (this.role === 'SCRNW') {
            this.isPreIntake = true;
        }

        this._dataStoreService.currentStore.subscribe(notify => {
            if (notify['notification'] === true) {
                this.getNotificationCount();
            }
        });
        this.setDashBoardLink();
        this.reports = environment.reports;
        this.nytdreport = '#/pages/reports/nytd';

    }
    setActiveModules() {

        //@Simar: This was already hardcoded here, just adding the rolekey as just rolename is not enough when switching to different module context
        // We should move this module access logic to backend as doing it on the header component is neither secure nor re-usable
        if (this.hasModuleAccess('Help') || this.hasModuleAccess('New')) {
            this.allowedModules.push({ modulename: 'Intake', image: 'intake.png', rolename: 'Intake Worker', rolekey: 'CWIW' });
            this.allowedModules.push({ modulename: 'Find', image: 'find.png', rolename: 'Intake Worker', rolekey: 'CWIW' });
        }
        if (this.hasModuleAccess('Validation') || this.hasModuleAccess('CaseWork Dashboard')) {
            this.allowedModules.push({ modulename: 'Case Work', image: 'case-work.png', rolename: 'field', rolekey: 'CWCW' });
        }
        if (this.hasModuleAccess('Approval Inbox')) {
            this.allowedModules.push({ modulename: 'Approve', image: 'approve.png', rolename: 'apcs', rolekey: 'CWSP' });
            this.allowedModules.push({ modulename: 'Reports', image: 'report.png', rolename: 'apcs', rolekey: 'CWSP' });
        }
        if (this.hasModuleAccess('Resource Home')) {
            this.allowedModules.push({ modulename: 'Resource Home', image: 'resource-home.png', rolename: this.role });
        }
        if (this.hasModuleAccess('Home Study')) {
            this.allowedModules.push({ modulename: 'Home Study', image: 'home-study.png', rolename: this.role });
        }
        if (this.hasModuleAccess('Recruiter Trainer')) {
            this.allowedModules.push({ modulename: 'Recruiter Trainer', image: 'trainer.png', rolename: this.role });
        }
        if (this.hasModuleAccess('Provider')) {
            this.allowedModules.push({ modulename: 'Provider', image: 'provide.png', rolename: this.role });
        }
        if (this.hasModuleAccess('Finance')) {
            this.allowedModules.push({ modulename: 'Finance', image: 'finance.png', rolekey: 'FNSFW', rolename: AppConstants.ROLES.FINANCE_WORKER });
        }
        if (this.hasModuleAccess('Finance Approval')) {
            this.allowedModules.push({ modulename: 'Finance Approval', image: 'payment.png', rolekey: 'FNSFS', rolename: AppConstants.ROLES.LDSS_FISCAL_SUPERVISOR });
        }
        if (this.hasModuleAccess('Title IV-E')) {
            this.allowedModules.push({ modulename: '4E', image: '4e.png', rolename: 'IV-E Specialist' });
        }
        if (this.hasModuleAccess('LDSS_PROVIDER')) {
            this.allowedModules.push({ modulename: 'LDSS_PROVIDER', image: 'provide.png', rolename: this.role });
        }
        if (this.hasModuleAccess('Admin')) {
            this.allowedModules.push({ modulename: 'Admin', image: 'admin.png', rolename: this.role });
        }

        let activemodlen = this.allowedModules.filter(c => c.rolename === this.role);
        if (activemodlen.length) {
            this.activeModule = activemodlen[0].modulename;
        } else {
            this.activeModule = '';
        }
        this._sessionStorage.setItem('activeModuleNav', this.activeModule);
        this._sessionStorage.setItem('activeModuleRole', this.role);
    }

    ngAfterViewInit() {
        // (<any>window).Feedback({
        //     h2cPath: '../../../../assets/js/html2canvas.js',
        //     url: environment.apiHost + '/supportlog/add',
        //     getPersonUrl: environment.apiHost + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList
        // });
    }
    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        this._authService.logout();
    }

    hasModuleAccess(key: string): boolean {
        const user = this._authService.getCurrentUser();
        if (key && user.resources.length > 0) {
            const resources = user.resources.filter(menu => menu.isallowed === true);
            if (hasMatch([key], resources.map(item => (item ? item.name : '')))) {
                return true;
            }
            return false;
        }
        return false;
    }
    hasAccess(key: string): Observable<boolean> {
        return this._authService.currentUser.map(user => {
            if (key && user.resources.length > 0) {
                const resources = user.resources.filter(menu => menu.isallowed === true);
                if (hasMatch([key], resources.map(item => (item ? item.name : '')))) {
                    return true;
                }
                return false;
            }
            return true;
        });
    }
    hasSubMenuAccess(mainmenu: string, submenu: string ): boolean {
        let isAllowed: boolean = true;
        if (this._authService && this._authService.currentUser) {
            this._authService.currentUser.map(user => user['resources'].filter(res => res.resourceid === submenu && res.parentkey === mainmenu)).subscribe((item) => {
                if (item.length) {
                    item.forEach((activeMenu) => {
                        if (activeMenu.resourceid === submenu) {
                            isAllowed = activeMenu.isallowed === true;
                        } else {
                            isAllowed = true;
                        }
                    });
                }
            });
        }
        return isAllowed;
    }
    hasMainMenuAccess(key, menu): boolean {
        let isAllowed: boolean = true;
        this._authService.currentUser.map(user => user['resources'].filter(res => res.resourceid === menu && res.modulekey === key)).subscribe((item) => {
            if (item.length) {
                item.forEach((activeMenu) => {
                    if (activeMenu.resourceid === menu) {
                        isAllowed = activeMenu.isallowed === true;
                    } else {
                        isAllowed = true;
                    }
                });
            }
        });
        return isAllowed;
    }
    setActiveNav(module) {
        this.activeModule = module.modulename;
        this._sessionStorage.setItem('activeModuleNav', this.activeModule);
        this.role = module.rolename;
        console.log('SIMAR ---> Role name after switch -->', module);
        this._authService.changeUserRole(module);
        // this.activeModuleNavAccess = this.rolesColl.find(v => v.name === module.modulename).roleaccess;
        this.setDashBoardLink();
        if (module.modulename === 'Resource Home' || module.modulename === 'Home Study' || module.modulename === 'Recruiter Trainer') {
            this.router.navigate(['/pages/provider-dashboard']);
        } else if (module.modulename === 'Finance' || module.modulename === 'Finance Approval' ) {
            this.router.navigate(['/pages/finance/finance-dashboard']);
            this._authService.dashboardConfig$.next('REFRESH-DASHBORAD');
        } else {
            this._authService.roleBasedRoute(this.role.toLowerCase());
        }
        this.modulecoll = false;
        if (this.activeModule==="Find"){
            this.router.navigate(['/pages/default-dashboard']);
        }
    }
    clearCount() {
        this.showNotification = false;
    }

    getNotificationCount() {
        this._service.endpointUrl = 'Usernotifications/getUserNotificationCount';
        this._service.getArrayList({}).subscribe(data => {
            this.totalNotificationCount = data['count'];
            if (data['count'] !== '0') {
                this.showNotification = true;
            }
        });
    }

    setDashBoardLink() {
        if (this.agency === 'AS') {
            if (this.role === AppConstants.ROLES.SUPERVISOR || this.role === AppConstants.ROLES.PROVIDER) {
                this.dashBoardLink = '/pages/cjams-dashboard';
            } else if (this.role === 'MHCP' || this.role === 'PHCP' || this.role === 'RNC' || this.role === 'Provider Applicant') {
                this.dashBoardLink = '/pages/practitioners';
            } else if (this.role === AppConstants.ROLES.CASE_WORKER) {
                this.dashBoardLink = '/pages/home-dashboard';
            } else if (this.role === AppConstants.ROLES.IHAS_SUPERVISOR) {
                this.dashBoardLink = '/pages/as-service-plan-approval';
            } else {
                this.dashBoardLink = '/pages/newintake/new-saveintake';
            }
        } else if (this.agency === 'DJS') {
            if (this.role === AppConstants.ROLES.SUPERVISOR || this.role === AppConstants.ROLES.PROVIDER) {
                this.dashBoardLink = '/pages/cjams-dashboard';
            } else if (this.role === AppConstants.ROLES.COURT_WORKER) {
                this.dashBoardLink = '/pages/sao-dashboard';
            } else if (this.role === AppConstants.ROLES.INTAKE_WORKER || this.role === AppConstants.ROLES.OFFICE_PROFFESSIONAL) {
                this.dashBoardLink = '/pages/newintake/new-saveintake';
            } else if (this.role === AppConstants.ROLES.RESTITUTION_SUPERVISOR) {
                this.dashBoardLink = '/pages/restitution/djs-finance-dashboard/changeform';
            } else if (this.role === AppConstants.ROLES.FINANCE_WORKER_DJS) {
                this.dashBoardLink = '/pages/restitution/djs-finance-dashboard/accounts';
            } else if (this.role === AppConstants.ROLES.DJS_PLACEMENT_WORKER) {
                this.dashBoardLink = '/pages/cjams-dashboard/djsplacement-dashboard';
            } else if (this.role === AppConstants.ROLES.DJS_ATD_PLACEMENT_WORKER) {
                this.dashBoardLink = '/pages/cjams-dashboard/djsplacement-dashboard';
            } else if (this.role === AppConstants.ROLES.TRANSPORTATION_OFFICER) {
                this.dashBoardLink = '/pages/transport-dboard';
            } else {
                this.dashBoardLink = '/pages/home-dashboard';
            }
        } else {
            if (this.role === AppConstants.ROLES.PROVIDER) {
                this.dashBoardLink = '/pages/cjams-dashboard';
            } else if (this.role === AppConstants.ROLES.INTAKE_WORKER || this.role === AppConstants.ROLES.KINSHIP_INTAKE_WORKER) {
                this.dashBoardLink = '/pages/newintake/new-saveintake';
            } else if (this.role === AppConstants.ROLES.KINSHIP_SUPERVISOR || this.role === AppConstants.ROLES.SUPERVISOR) {
                this.dashBoardLink = '/pages/cjams-dashboard/cw-intake-referals';
            } else if (this.role === AppConstants.ROLES.FINANCE_WORKER || this.role === AppConstants.ROLES.LDSS_FISCAL_SUPERVISOR || this.role === AppConstants.ROLES.DIRECTOR_OF_FINANCE) {
                this.dashBoardLink = '/pages/finance/finance-dashboard';
            } else  if (this.activeModule === 'Case Work' || this.activeModule === 'Resource Home' || this.activeModule === 'Home Study' || this.activeModule === 'Recruiter Trainer') {
                this.dashBoardLink = '/pages/home-dashboard';
            } else if (this.role === AppConstants.ROLES.LICENSING_ADMINISTRATOR) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.QUALITY_ASSURANCE) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.PROGRAM_MANAGER) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            }  else if (this.role === AppConstants.ROLES.EXECUTIVE_DIRECTOR) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.DEPUTY_DIRECTOR) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.DESIGNEE) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.PRIVATE_SUPERVISOR) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.PROVIDER_DJS_QA) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.PROVIDER_DJS_PROGRAMDIRECTOR) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.PROVIDER_DJS_RESOURCE_SUPERVISOR) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.PROVIDER_DJS_RESOURCE) {
                this.dashBoardLink = '/pages/provider-applicant/existing-applicant';
            } else if (this.role === AppConstants.ROLES.APPEAL_USER) {
                this.dashBoardLink = '/pages/home-dashboard';
            } else {
                this.dashBoardLink = '/pages/default-dashboard';
            }
        }
    }
    fillFeedback() {
        this.curDate = new Date();
        let caseid = 'Dashboard';
        this.feedbackForm.patchValue({ clientid: 'Dashboard' });
        if (this.location.path().indexOf('/case-worker/') !== -1) {
            caseid = this.location.path().split('/')[4];
            this.getInvolvedPerson();
        } else if (this.location.path().indexOf('/my-newintake/') !== -1) {
            caseid = this._sessionStorage.getObj('intake') ? this._sessionStorage.getObj('intake').number : '';
            this.feedbackForm.patchValue({ clientid: 'Intake' });
        }
        this.feedbackForm.patchValue({ supportlogdate: this.curDate, caseid: caseid, pageurl: this.location.path() });
        (<any>$('#user-feedback')).modal('show');
    }
    onAttachmentChange(event) {
        console.log(event);
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            this.fileUploaded = event.target.files[0];
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.feedbackForm.patchValue({
                    filedata: reader.result
                });
            };
        }
    }
    sendFeedback() {
        this._service.create(this.feedbackForm.value, 'supportlog/add').subscribe(
            res => {
                this._alert.success('Feedback sent successfully');
                this.resetFeedback();
                this.supportNo = res.supportno;
                (<any>$('#supportNo')).modal('show');
            },
            err => {
                this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    resetFeedback() {
        (<any>$('#user-feedback')).modal('hide');
        this.feedbackForm.reset();
        this.fileUploaded = Object.assign({});
        (<any>$('#filedata')).val(null);
        if (this.userInfo.user && this.userInfo.user.userprofile) {
            this.feedbackForm.patchValue({
                displayname: this.userInfo.user.userprofile.displayname,
                cjamspid: this.userInfo.user.userprofile.cjamspid,
                frommailid: this.userInfo.user.userprofile.email,
                userrole: this.userInfo.role.description,
                severity: ''
            });
        }
    }
    private initializeForm() {
        this.feedbackForm = this._formBuild.group({
            displayname: [''],
            cjamspid: [''],
            frommailid: [''],
            ldssregion: [''],
            supportlogdate: [null],
            officelocation: [''],
            clientid: [''],
            subject: [''],
            notes: ['', [Validators.required]],
            userrole: [''],
            caseid: [''],
            filedata: [null],
            severity: [''],
            pageurl: ['']
        });
        if (this.userInfo.user && this.userInfo.user.userprofile) {
            this.feedbackForm.patchValue({
                displayname: this.userInfo.user.userprofile.displayname,
                cjamspid: this.userInfo.user.userprofile.cjamspid,
                frommailid: this.userInfo.user.userprofile.email,
                userrole: this.userInfo.role.description
            });
        }
    }
    private getInvolvedPerson() {
        const intakeServiceId = this.location.path().split('/')[3] ? this.location.path().split('/')[3] : null;
        this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeserviceid: intakeServiceId }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
            .subscribe(res => {
                const person = res.data.filter(child => child.rolename === 'RC')[0];
                this.feedbackForm.patchValue({ clientid: person.cjamspid });
            });
    }

    uploadFile(file: File | FileError): void {
        this.preview(file);
        const fileExt = file['name'].toLowerCase()
            .split('.')
            .pop();
        if (fileExt === 'jpeg' ||
        fileExt === 'jpg' ||
        fileExt === 'png') {
        this.uploadedFile.push(file);
        this.uploadedFile[0].attachmenttypekey = 'Document';
        this.isAttachType = this.uploadedFile[0].attachmenttypekey;
        } else {
            this._alert.error(fileExt + 'format can\'t be uploaded');
            return;
        }
        const workEnv = config.workEnvironment;
        let uploadUrl = '';
        const dynam  = this.isAttachType + '|' + this.isCate + '|' + this.issubCate ;
        if (workEnv === 'state') {
            uploadUrl =  AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
            + '?access_token=' + this.userInfo.id + '&' + 'srno=' +  this.userInfo.userId + '&' + 'docsInfo=' + dynam + '&attachmenttype=' + this.attachmenttype
            + '&personid=' + this.userInfo.user.securityusersid;
            console.log('state', uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.userInfo.id +
            '&' + 'srno='+  this.userInfo.userId + '&attachmenttype=' + this.attachmenttype + '&personid=' + this.userInfo.user.securityusersid ;
            console.log('local', uploadUrl);
        }
        this.isUploading = true;
        if (!(file instanceof File)) {
          this.isUploading = false;
          return;
        }
        this._uploadService.upload({
          url: uploadUrl,
          headers: new HttpHeaders().set('access_token', this.userInfo.id).set('ctype', 'file'),
          filesKey: ['file'],
                    files: this.uploadedFile[0],
                    process: true
        }).subscribe(
          (response) => {
            if (response.status) {
                this.uploadedFile[0].percentage = response.percent;
            }
            if ( response.status === UploadStatus.Completed) {
                console.log(response.data, '(response.data(response.data');
                console.log(this.userInfo, 'this.userInfothis.userInfo');
                this.user = {
                    userphoto: response.data.s3bucketpathname
                };
                this._service.create(this.user, 'admin/userprofile/updatephoto').subscribe((result) => {
                    if (result) {
                        console.log(result, 'resultlkhkjhkjh');
                        this._alert.success('User photo updated successfully.');
                    }
                });
            }
          },
          (err) => {
            console.log(err);
          },
          () => {
            this.isUploading = false;
            console.log('complete');
          });
      }

      preview(files) {
        console.log(files);
        const mimeType = files.type;
        if (mimeType.match(/image\/*/) == null) {
          return;
        }
        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files);
        reader.onload = (_event) => {
          console.log(reader.result);
          this.imgURL = reader.result;
        };
      }
}
