import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CpsDocLetterComponent } from '../../pages/newintake/my-newintake/intake-document-creator/cps-doc-letter/cps-doc-letter.component';
import { SharedDirectivesModule } from '../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    SharedDirectivesModule
  ],
  declarations: [CpsDocLetterComponent],
  exports: [CpsDocLetterComponent]
})
export class SharedComponentsModule { }
