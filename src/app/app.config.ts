import { config } from '../environments/config';
import { environment } from '../environments/environment';

export class AppConfig {
    public static siteTitle = 'CJAMS';
    public static authTokenUrl = 'users/login?include=["user"]';
	//public static authTokenUrl = 'admin/userprofile/generateAccessToken';
    public static roleProfileUrl = 'Authorizes/getroleprofile';
    public static logoutUrl = 'users/logout';
    public static logoutOpenAm = 'users/logoutEcms';
    public static pageProfile = 'Authorizes/getPageProfile';

    public static get baseUrl(): string {
        return (config.workEnvironment === 'state') ? (window.location.origin + environment.contextPath) : environment.apiHost;
    }

    public static getModuleMapName(modname: string = '') {
        let result = '';

        // console.log("modname", modname);
        switch (modname.toLowerCase()) {
            case 'users':
            case 'users/getuser':
                result = 'user';
                break;
            case 'authorizes':
            case 'authorizes/getpageprofile':
                // console.log('Authorize', result);
                result = 'Authorize';
                break;
            case 'intake/activitytaskdispositiontype':
                result = 'Activitytaskdispositiontype';
                break;
            case 'daconfig/accesslist':
                result = 'accesslist';
                break;
            case 'accesstkns':
                result = 'accessTkn';
                break;
            case 'accesstokens':
                result = 'AccessToken';
                break;
            case 'acls':
                result = 'ACL';
                break;
            case 'activities':
                result = 'Activity';
                break;
            case 'intake/activitygoal':
                result = 'Activitygoal';
                break;
            case 'intake/activitygoaldispositiontype':
                result = 'Activitygoaldispositiontype';
                break;
            case 'intake/activitygoalstatustype':
                result = 'Activitygoalstatustype';
                break;
            case 'intake/activitygoaltype':
                result = 'Activitygoaltype';
                break;
            case 'admin/activityprioritytype':
                result = 'Activityprioritytype';
                break;
            case 'intake/activitytask':
                result = 'Activitytask';
                break;
            case 'intake/activitytaskdispositiontype':
                result = 'Activitytaskdispositiontype';
                break;
            case 'intake/activitytaskstatustype':
                result = 'Activitytaskstatustype';
                break;
            case 'intake/activitytasktypestatusdispositionmap':
                result = 'Activitytasktypestatusdispositionmap';
                break;
            case 'intake/activitytasktypestatusdispositionmap/investigationdisposition':
                result = 'Activitytasktypestatusdispositionmap';
                break;
            case 'admin/activitytasktype':
                result = 'Activitytasktype';
                break;
            case 'admin/activitytype':
                result = 'Activitytype';
                break;
            case 'actors':
                result = 'Actor';
                break;
            case 'actorrelationships':
                result = 'Actorrelationship';
                break;
            case 'admin/actortype':
                result = 'Actortype';
                break;
            case 'actortypeagencies':
                result = 'Actortypeagency';
                break;
            case 'addresskeydetaillkups':
                result = 'Addresskeydetaillkup';
                break;
            case 'addresskeylkups':
                result = 'Addresskeylkup';
                break;
            case 'admin/agency':
                result = 'Agency';
                break;
            case 'agencyaddresses':
                result = 'Agencyaddress';
                break;
            case 'admin/agencyaddresstype':
                result = 'Agencyaddresstype';
                break;
            case 'agencyaliases':
                result = 'Agencyalias';
                break;
            case 'admin/agencycategory':
                result = 'Agencycategory';
                break;
            case 'agencyconfig':
                result = 'Agencyconfig';
                break;
            case 'admin/agencyphonenumbertype':
                result = 'Agencyphonenumbertype';
                break;
            case 'admin/agencyroletype':
                result = 'Agencyroletype';
                break;
            case 'agencyservcassessmenttemplatemaps':
                result = 'Agencyservcassessmenttemplatemap';
                break;
            case 'agencyservices':
                result = 'Agencyservice';
                break;
            case 'agencystatustypes':
                result = 'Agencystatustype';
                break;
            case 'agencysubtypes':
                result = 'Agencysubtype';
                break;
            case 'admin/agencytype':
                result = 'Agencytype';
                break;
            case 'admin/alerteventtype':
                result = 'Alerteventtype';
                break;
            case 'aliases':
                result = 'Alias';
                break;
            case 'admin/allegation':
            case 'admin/allegation/getcasesubtype':
            case 'admin/allegation/getallegationsindicators':
                result = 'Allegation';
                break;
            case 'allegationstatestatutes':
                result = 'Allegationstatestatutes';
                break;
            case 'admin/amactivity':
                result = 'Amactivity';
                break;
            case 'admin/amgoal':
                result = 'Amgoal';
                break;
            case 'intake/amgoaldisposition':
                result = 'Amgoaldisposition';
                break;
            case 'intake/amgoalstatus':
                result = 'Amgoalstatus';
                break;
            case 'admin/ammapping':
                result = 'Ammapping';
                break;
            case 'admin/ammappinggoal':
                result = 'Ammappinggoal';
                break;
            case 'admin/ammappingtask':
                result = 'Ammappingtask';
                break;
            case 'admin/amtask':
                result = 'Amtask';
                break;
            case 'announcement':
                result = 'Announcement';
                break;
            case 'areateammemberservicerequests':
                result = 'Areateammemberservicerequest';
                break;
            case 'admin/assessment':
            case 'admin/assessment/createassessmentinternal':
            case 'admin/assessment/generateassessmentpdf':
                result = 'Assessment';
                break;
            case 'assessmentcomments':
                result = 'Assessmentcomments';
                break;
            case 'admin/assessmentscoretype':
                result = 'Assessmentscoretype';
                break;
            case 'admin/assessmentscoringmethod':
                result = 'Assessmentscoringmethod';
                break;
            case 'admin/assessmentstatustype':
                result = 'Assessmentstatustype';
                break;
            case 'admin/assessmenttemplate':
                result = 'Assessmenttemplate';
                break;
            case 'admin/assessmenttemplatecategory':
                result = 'Assessmenttemplatecategory';
                break;
            case 'admin/assessmenttemplatecategoryfilter':
                result = 'Assessmenttemplatecategoryfilter';
                break;
            case 'admin/assessmenttemplatecategoryfiltermap':
                result = 'Assessmenttemplatecategoryfiltermap';
                break;
            case 'admin/assessmenttemplatescoremapping':
                result = 'Assessmenttemplatescoremapping';
                break;
            case 'admin/assessmenttemplatesubcategory':
                result = 'Assessmenttemplatesubcategory';
                break;
            case 'admin/assessmenttemplatetarget':
                result = 'Assessmenttemplatetarget';
                break;
            case 'admin/assessmenttextpositiontype':
                result = 'Assessmenttextpositiontype';
                break;
            case 'attachments':
                result = 'attachment';
                break;
            case 'intake/attachmentclassificationtype':
                result = 'Attachmentclassificationtype';
                break;
            case 'intake/attachmenttype':
                result = 'Attachmenttype';
                break;
            case 'auditlogs':
            case 'auditlog':
                result = 'Auditlog';
                break;
            case 'auditlogtypes':
                result = 'Auditlogtype';
                break;
            case 'admin/caregiveraddresstype':
                result = 'Caregiveraddresstype';
                break;
            case 'admin/caregiverphonetype':
                result = 'Caregiverphonetype';
                break;
            case 'childcharacteristictype':
                result = 'Childcharacteristictype';
                break;
            case 'conditiontype':
                result = 'conditiontype';
                break;
            case 'manage/configurablelinks':
                result = 'Configurablelinks';
                break;
            case 'admin/configurationsettings':
                result = 'Configurationsetting';
                break;
            case 'contactroletypes':
                result = 'Contactroletype';
                break;
            case 'admin/county':
                result = 'County';
                break;
            case 'admin/countyareateammember':
                result = 'Countyareateammember';
                break;
            case 'courtactiontype':
                result = 'Courtactiontype';
                break;
            case 'dentalspecialtytype':
                result = 'Dentalspecialtytype';
                break;
            case 'admin/dispositioncode':
                result = 'Dispositioncode';
                break;
            case 'documentattachments':
                result = 'Documentattachment';
                break;
            case 'documentproperties':
            case 'documentproperties/getintakeattachments':
            case 'documentproperties/getpersonattachments':
            case 'documentproperties/updatepersonattachments':
            case 'documentproperties/delete':
            case 'documentproperties/getcaseworkerattachments':
            case 'documentproperties/getattachments':
            case 'documentproperties/ecmsclientdetails':
                result = 'Documentproperties';
                break;
            case 'documenttypes':
                result = 'Documenttype';
                break;
            case 'documenttemplate':
                result = 'Documenttemplate';
                break;
            case 'educationtype':
                result = 'Educationtype';
                break;
            case 'admin/employeetype':
                result = 'Employeetype';
                break;
            case 'admin/equipment':
                result = 'Equipment';
                break;
            case 'admin/equipmenttype':
                result = 'Equipmenttype';
                break;
            case 'admin/ethnicgrouptype':
                result = 'Ethnicgrouptype';
                break;
            case 'evaluationsources':
            case 'evaluationsources/list':
                result = 'Evaluationsource';
                break;
            case 'exitreasontype':
                result = 'Exitreasontype';
                break;
            case 'familyinvolvementmeetings':
                result = 'Familyinvolvementmeeting';
                break;
            case 'familyinvolvementmeetingactors':
                result = 'Familyinvolvementmeetingactor';
                break;
            case 'familymeetingsubtypes':
                result = 'Familymeetingsubtype';
                break;
            case 'familymeetingtypes':
                result = 'Familymeetingtype';
                break;
            case 'admin/gendertype':
                result = 'Gendertype';
                break;
            case 'getdocdetails':
                result = 'getdocdetails';
                break;
            case 'getdocumentsearchdtls':
                result = 'Getdocumentsearchdtls';
                break;
            case 'getsameracrossrefdas':
                result = 'GetSameRACrossRefDA';
                break;
            case 'getusersearches':
                result = 'getusersearch';
                break;
            case 'gloabalagencysearches':
                result = 'gloabalagencysearch';
                break;
            case 'globalpersonsearches':
                result = 'globalpersonsearch';
                break;
            case 'gradetype':
                result = 'Gradetype';
                break;
            case 'helptexts':
                result = 'Helptext';
                break;
            case 'healthassessmenttype':
                result = 'Healthassessmenttype';
                break;
            case 'healthdomaintype':
                result = 'Healthdomaintype';
                break;
            case 'healthprofessiontype':
                result = 'Healthprofessiontype';
                break;
            case 'hearingstatustype':
                result = 'Hearingstatustype';
                break;
            case 'householdtype':
                result = 'Householdtype';
                break;
            case 'incidentlocationtype':
                result = 'Incidentlocationtype';
                break;
            case 'admin/incometype':
                result = 'Incometype';
                break;
            case 'admin/indicator':
                result = 'Indicator';
                break;
            case 'informationsourcetype':
                result = 'Informationsourcetype';
                break;
            case 'injurycharactersticstype':
                result = 'Injurycharactersticstype';
                break;
            case 'injurytype':
                result = 'Injurytype';
                break;
            case 'intakeagencies':
                result = 'Intakeagency';
                break;
            case 'intakeagencypurposes':
                result = 'Intakeagencypurpose';
                break;
            case 'intakeagencyrequesttypes':
                result = 'Intakeagencyrequesttype';
                break;
            case 'intakeagencyservs':
                result = 'Intakeagencyserv';
                break;
            case 'intakeappeal':
            case 'intakeappeals':
                result = 'Intakeappeal';
                break;
            case 'intakedastagings':
                result = 'intakedastaging';
                break;
            case 'intakedocument':
                result = 'Intakedocument';
                break;
            case 'admin/intakeserreqstatustype':
                result = 'Intakeserreqstatustype';
                break;
            case 'intakeservs':
                result = 'Intakeserv';
                break;
            case 'intakeserviceagencyroletypes':
                result = 'Intakeserviceagencyroletype';
                break;
            case 'intakeservicerequests':
            case 'intakeservicerequests/getdsdsactionsummarydtls':
            case 'intakeservicerequests/getdatimelinedjs':
            case 'intakeservicerequests/getdatimeline':
            case 'intakeservicerequests/reportsummary/list':
            case 'intakeservicerequests/investigationactivities/list':
            case 'intakeservicerequests/prepopasmtprepostdiacharge':
            case 'intakeservicerequests/updatefolderchange':
            case 'intakeservicerequests/getcaseconnected':
            case 'intakeservicerequests/getadoptionsummarydtls':   
                result = 'Intakeservicerequest';
                break;
            case 'intakeservicerequestactors':
            case 'intakeservicerequestactors/getpersonlist':
            case 'intakeservicerequestactors/deletewithintakeserreqnumber':
                result = 'Intakeservicerequestactor';
                break;
            case 'intakeservicerequestagencies':
                result = 'Intakeservicerequestagency';
                break;
            case 'intakeservicerequestappeal':
                result = 'Intakeservicerequestappeal';
                break;
            case 'intakeservicerequestclearingdata':
                result = 'Intakeservicerequestclearingdata';
                break;
            case 'intakeservicerequestcourtaction':
                result = 'Intakeservicerequestcourtaction';
                break;
            case 'intakeservicerequestcourtactiontype':
                result = 'Intakeservicerequestcourtactiontype';
                break;
            case 'intakeservicerequestcourtconditiontypeconfig':
                result = 'Intakeservicerequestcourtconditiontypeconfig';
                break;
            case 'intakeservicerequestcourthearing':
            case 'intakeservicerequestcourthearing/updatehearing':
                result = 'Intakeservicerequestcourthearing';
                break;
            case 'intakeservicerequestcourthearingconfig':
                result = 'Intakeservicerequestcourthearingconfig';
                break;
            case 'intakeservicerequestcourtordertypeconfig':
                result = 'Intakeservicerequestcourtordertypeconfig';
                break;
            case 'intakeservicerequestcrossreferences':
                result = 'Intakeservicerequestcrossreference';
                break;
            case 'intakeservicerequestcrossreferencereasontypes':
                result = 'Intakeservicerequestcrossreferencereasontype';
                break;
            case 'intakeservicerequestdispositioncodes':
                result = 'Intakeservicerequestdispositioncode';
                break;
            case 'intakeservicerequestevaluationconfig':
                result = 'Intakeservicerequestevaluationconfig';
                break;
            case 'admin/intakeservicerequestgroup':
                result = 'Intakeservicerequestgroup';
                break;
            case 'intakeservicerequestgroupdetails':
                result = 'Intakeservicerequestgroupdetails';
                break;
            case 'intake/intakeservicerequestillegalactivity':
                result = 'Intakeservicerequestillegalactivity';
                break;
            case 'intakeservicerequestillegalactivitytypes':
                result = 'Intakeservicerequestillegalactivitytype';
                break;
            case 'intakeservicerequestinputsources':
                result = 'Intakeservicerequestinputsource';
                break;
            case 'intakeservicerequestinputtypes':
                result = 'Intakeservicerequestinputtype';
                break;
            case 'admin/intakeservicerequestplantype':
                result = 'Intakeservicerequestplantype';
                break;
            case 'intakeservicerequestpurposes':
                result = 'Intakeservicerequestpurpose';
                break;
            case 'intake/intakeservicerequestreferral':
                result = 'Intakeservicerequestreferral';
                break;
            case 'admin/intakeservicerequestreferraldetail':
                result = 'Intakeservicerequestreferraldetail';
                break;
            case 'intakeservicerequestsdm':
                result = 'Intakeservicerequestsdm';
                break;
            case 'admin/intakeservicerequesttype':
                result = 'Intakeservicerequesttype';
                break;
            case 'intake/intakeservicerequestweaver':
                result = 'Intakeservicerequestweaver';
                break;
            case 'intake/intakeservicerequestweavertype':
                result = 'Intakeservicerequestweavertype';
                break;
            case 'intakeservreqchildremoval':
            case 'intakeservreqchildremoval/add':
                result = 'Intakeservreqchildremoval';
                break;
            case 'intakeservreqchildremovalreason':
                result = 'Intakeservreqchildremovalreason';
                break;
            case 'investigations':
                result = 'Investigation';
                break;
            case 'investigationallegations':
                result = 'Investigationallegation';
                break;
            case 'investigationallegationactors':
                result = 'Investigationallegationactor';
                break;
            case 'investigationallegationcharacterstics':
                result = 'Investigationallegationcharacterstics';
                break;
            case 'investigationallegationindicator':
                result = 'Investigationallegationindicator';
                break;
            case 'investigationallegationinjury':
                result = 'Investigationallegationinjury';
                break;
            case 'investigationallegationinjurycharacterstics':
                result = 'Investigationallegationinjurycharacterstics';
                break;
            case 'investigationallegationmaltreators':
                result = 'Investigationallegationmaltreators';
                break;
            case 'investigationallegationstatustypes':
                result = 'Investigationallegationstatustype';
                break;
            case 'investigationfindings':
                result = 'Investigationfinding';
                break;
            case 'investigationfindingtype':
                result = 'Investigationfindingtype';
                break;
            case 'investigationmaltreatment':
                result = 'Investigationmaltreatment';
                break;
            case 'investigationmaltreatmentactor':
                result = 'Investigationmaltreatmentactor';
                break;
            case 'investigationmappings':
                result = 'Investigationmapping';
                break;
            case 'investigationreviewtypes':
                result = 'Investigationreviewtype';
                break;
            case 'intake/investigationtask':
                result = 'Investigationtask';
                break;
            case 'admin/itemstable':
            case 'itemstables':
                result = 'Itemstable';
                break;
            case 'admin/languagetype':
                result = 'Languagetype';
                break;
            case 'admin/livingarrangementtype':
                result = 'Livingarrangementtype';
                break;
            case 'localstorage':
                result = 'localstorage';
                break;
            case 'maltreatmentcharactersticstype':
                result = 'Maltreatmentcharactersticstype';
                break;
            case 'admin/maritalstatustype':
                result = 'Maritalstatustype';
                break;
            case 'medicalconditiontype':
                result = 'Medicalconditiontype';
                break;
            case 'medicationtype':
                result = 'Medicationtype';
                break;
            case 'meetingfimdetails':
                result = 'Meetingfimdetails';
                break;
            case 'meetingparticipants':
                result = 'Meetingparticipants';
                break;
            case 'meetingrecordings':
                result = 'Meetingrecording';
                break;
            case 'meetingrecordingactors':
                result = 'Meetingrecordingactor';
                break;
            case 'meetingtypes':
                result = 'Meetingtype';
                break;
            case 'manage/news':
                result = 'News';
                break;
            case 'nextnumbers':
                result = 'Nextnumber';
                break;
            case 'notificationconfig':
                result = 'notificationconfig';
                break;
            case 'notificationtype':
                result = 'notificationtype';
                break;
            case 'admin/objecttype':
                result = 'Objecttype';
                break;
            case 'offensecategories':
                result = 'Offensecategory';
                break;
            case 'participantsubtypes':
                result = 'Participantsubtype';
                break;
            case 'participanttypes':
                result = 'Participanttype';
                break;
            case 'permanencyplan':
            case 'permanencyplan/list':
                result = 'Permanencyplan';
                break;
            case 'permanencyplandetail':
                result = 'Permanencyplandetail';
                break;
            case 'permanencyplansubtype':
                result = 'Permanencyplansubtype';
                break;
            case 'permanencyplantypes':
            case 'permanencyplantypes/list':
                result = 'Permanencyplantype';
                break;
            case 'permissiongroups':
                result = 'Permissiongroup';
                break;
            case 'personabusehistory':
                result = 'Personabusehistory';
                break;
            case 'personabusesubstance':
                result = 'Personabusesubstance';
                break;
            case 'people':
            case 'people/getpersonbasicdetails':
                result = 'Person';
                break;
            case 'personaccomplishment':
                result = 'Personaccomplishment';
                break;
            case 'personaddresses':
            case 'personaddresses/personaddressdelete':
                result = 'Personaddress';
                break;
            case 'admin/personaddresstype':
                result = 'Personaddresstype';
                break;
            case 'personbehavioralhealth':
                result = 'Personbehavioralhealth';
                break;
            case 'personclearinginfos':
                result = 'Personclearinginfo';
                break;
            case 'persondentalinfo':
                result = 'Persondentalinfo';
                break;
            case 'personeducation':
            case 'personeducation/personeducationdelete':
                result = 'Personeducation';
                break;
            case 'personeducationtesting':
                result = 'Personeducationtesting';
                break;
            case 'personeducationvocation':
                result = 'Personeducationvocation';
                break;
            case 'personemails':
                result = 'Personemail';
                break;
            case 'admin/personemailtype':
                result = 'Personemailtype';
                break;
            case 'personemployerdetail':
                result = 'Personemployerdetail';
                break;
            case 'personhealthexamination':
                result = 'Personhealthexamination';
                break;
            case 'personhealthinsurance':
                result = 'Personhealthinsurance';
                break;
            case 'personidentifiers':
                result = 'Personidentifier';
                break;
            case 'admin/personidentifiertype':
                result = 'Personidentifiertype';
                break;
            case 'personmedicalcondition':
                result = 'Personmedicalcondition';
                break;
            case 'personmedicpshychotropic':
                result = 'Personmedicpshychotropic';
                break;
            case 'personphonenumbers':
            case 'personphonenumbers/personphonenumberdelete':
                result = 'Personphonenumber';
                break;
            case 'admin/personphonetype':
                result = 'Personphonetype';
                break;
            case 'personphycisianinfo':
                result = 'Personphycisianinfo';
                break;
            case 'personrelations':
                result = 'Personrelation';
                break;
            case 'personrelationtypes':
                result = 'Personrelationtype';
                break;
            case 'personresultfields':
                result = 'Personresultfield';
                break;
            case 'personservicetype':
                result = 'Personservicetype';
                break;
            case 'petitiontype':
                result = 'Petitiontype';
                break;
            case 'pgresources':
                result = 'Pgresource';
                break;
            case 'physicalattributetypes':
                result = 'Physicalattributetype';
                break;
            case 'physicianspecialtytype':
                result = 'Physicianspecialtytype';
                break;
            case 'placement':
            case 'placement/add':
                result = 'Placement';
                break;
            case 'prescriptionreasontype':
                result = 'Prescriptionreasontype';
                break;
            case 'priorities':
                result = 'Priority';
                break;
            case 'admin/progressnote':
            case 'admin/progressnote/getalldarecording':
            case 'admin/progressnote/getdarecordingdetails':
            case 'admin/progressnote/updaterecordings':
            case 'admin/progressnote/getcontactslogreport':
                result = 'Progressnote';
                break;
            case 'admin/progressnoteclassificationtype':
                result = 'Progressnoteclassificationtype';
                break;
            case 'admin/progressnotedetail':
                result = 'Progressnotedetail';
                break;
            case 'admin/progressnotetype':
                result = 'Progressnotetype';
                break;
            case 'provider':
                result = 'Provider';
                break;
            case 'provideraddress':
                result = 'Provideraddress';
                break;
            case 'provideraddresstype':
                result = 'Provideraddresstype';
                break;
            case 'providerservice':
                result = 'Providerservice';
                break;
            case 'admin/provideragreement':
                result = 'Provideragreement';
                break;
            case 'admin/provideragreementdocumentruleconfig':
                result = 'Provideragreementdocumentruleconfig';
                break;
            case 'admin/provideragreementdocumenttype':
                result = 'Provideragreementdocumenttype';
                break;
            case 'admin/provideragreementtype':
                result = 'Provideragreementtype';
                break;
            case 'providerchildcharacteristic':
                result = 'Providerchildcharacteristic';
                break;
            case 'providercontract':
                result = 'Providercontract';
                break;
            case 'providercontracttype':
                result = 'Providercontracttype';
                break;
            case 'providernonagreementdetails':
                result = 'Providernonagreementdetail';
                break;
            case 'providernonagreementmedicaiddetails':
                result = 'Providernonagreementmedicaiddetail';
                break;
            case 'admin/providernonagreementtype':
                result = 'Providernonagreementtype';
                break;
            case 'admin/racetype':
                result = 'Racetype';
                break;
            case 'admin/referralorgtype':
                result = 'Refferalorgtype';
                break;
            case 'admin/referredtotype':
                result = 'Refferedtotype';
                break;
            case 'admin/region':
            case 'admin/region/list':
                result = 'Region';
                break;
            case 'relationshiptypes':
                result = 'Relationshiptype';
                break;
            case 'admin/religiontype':
                result = 'Religiontype';
                break;
            case 'removalreasontype':
                result = 'removalreasontype';
                break;
            case 'serviceplan/repeatdaytype':
                result = 'Repeatdaytype';
                break;
            case 'resources':
                result = 'Resource';
                break;
            case 'resourcenarratives':
                result = 'Resourcenarrative';
                break;
            case 'admin/reviewresulttemplate':
                result = 'Reviewresulttemplate';
                break;
            case 'role':
                result = 'role';
                break;
            case 'roles':
                result = 'Role';
                break;
            case 'rolemappings':
                result = 'Rolemapping';
                break;
            case 'roleresources':
                result = 'Roleresource';
                break;
            case 'roletype':
                result = 'Roletype';
                break;
            case 'routing':
                result = 'Routing';
                break;
            case 'admin/rule':
                result = 'Rule';
                break;
            case 'saoresponse':
                result = 'Saoresponse';
                break;
            case 'saoresponseconditiontype':
                result = 'Saoresponseconditiontype';
                break;
            case 'saoresponsestatustype':
                result = 'Saoresponsestatustype';
                break;
            case 'securityusers':
                result = 'Securityusers';
                break;
            case 'services':
                result = 'Service';
                break;
            case 'serviceplan':
                result = 'Serviceplan';
                break;
            case 'serviceplan/activity':
                result = 'Serviceplanactivity';
                break;
            case 'serviceplan/activitystatustype':
                result = 'Serviceplanactivitystatustype';
                break;
            case 'serviceplan/goal':
                result = 'Serviceplangoal';
                break;
            case 'serviceplan/occurence':
                result = 'Serviceplanoccurence';
                break;
            case 'serviceplan/repeat':
                result = 'Serviceplanrepeat';
                break;
            case 'serviceplan/scheduleexemption':
                result = 'Serviceplanscheduleexemption';
                break;
            case 'serviceplan/statustype':
                result = 'Serviceplanstatustype';
                break;
            case 'manage/servicerequestincidenttype':
                result = 'Servicerequestincidenttype';
                break;
            case 'servicerequestsearches':
                result = 'servicerequestsearch';
                break;
            case 'admin/servicerequestsubtype':
                result = 'Servicerequestsubtype';
                break;
            case 'daconfig/servicerequesttypeconfig':
                result = 'Servicerequesttypeconfig';
                break;
            case 'daconfig/servicerequesttypeconfigalert':
                result = 'Servicerequesttypeconfigalert';
                break;
            case 'daconfig/servicerequesttypeconfigdispositioncode':
                result = 'Servicerequesttypeconfigdispositioncode';
                break;
            case 'daconfig/servicerequesttypeconfigrole':
                result = 'Servicerequesttypeconfigrole';
                break;
            case 'servicesubtype':
                result = 'Servicesubtype';
                break;
            case 'servicetype':
                result = 'Servicetype';
                break;
            case 'serviceplanlog':
                result = 'Serviceplanlog';
                break;
            case 'servicerequestappointment':
                result = 'Servicerequestappointment';
                break;
            case 'servicerequestappointmentactor':
                result = 'Servicerequestappointmentactor';
                break;
            case 'submissioncollection':
                result = 'Submissioncollection';
                break;
            case 'states':
                result = 'State';
                break;
            case 'school':
                result = 'School';
                break;
            case 'statementofdeficiencies':
                result = 'Statementofdeficiency';
                break;
            case 'manage/statestatutes':
                result = 'Statestatutes';
                break;
            case 'manage/team':
            case 'manage/team/details':
                result = 'Team';
                break;
            case 'admin/teammember':
            case 'admin/teammember/list':
                result = 'Teammember';
                break;
            case 'admin/teammemberassignment':
            case 'admin/teammemberassignment/investigationplanassignto/list':
            case 'admin/teammemberassignment/investigationplanassignto':
                result = 'Teammemberassignment';
                break;
            case 'admin/teammemberequipment':
                result = 'Teammemberequipment';
                break;
            case 'teammemberrolecategories':
                result = 'Teammemberrolecategory';
                break;
            case 'teammemberrolecategoryteammemberroletypemaps':
                result = 'Teammemberrolecategoryteammemberroletypemap';
                break;
            case 'admin/teammemberroletype':
            case 'admin/teammemberroletype/list':
            case 'admin/teammemberroletype/teampositionlist':
                result = 'Teammemberroletype';
                break;
            case 'admin/teamtype':
                result = 'Teamtype';
                break;
            case 'typesagencymapping':
                result = 'Typesagencymapping';
                break;
            case 'testingtype':
                result = 'Testingtype';
                break;
            case 'titleive':
            case 'titleive/fc/get-periods/{clientid}/{removalid}':
            case 'titleive/fc/eligibility-worksheet':
            case 'titleive/ive':
            case 'titleive/ive/routing':
            case 'titleive/fc':
            case 'titleive/gap/gap-eligibility-worksheet':
            case 'titleive/gap/audit':
                result = 'TitleIVE';
                break;
            case 'userannouncement':
                result = 'Userannouncement';
                break;
            case 'usernotifications':
                result = 'Usernotification';
                break;
            case 'admin/usernotificationgroup':
                result = 'Usernotificationgroup';
                break;
            case 'admin/usernotificationgroupdetail':
                result = 'Usernotificationgroupdetail';
                break;
            case 'usernotificationmaps':
                result = 'Usernotificationmap';
                break;
            case 'userpersonresultfields':
                result = 'Userpersonresultfield';
                break;
            case 'admin/userprofile':
                result = 'Userprofile';
                break;
            case 'userprofileaddresses':
                result = 'Userprofileaddress';
                break;
            case 'admin/userprofileidentifier':
                result = 'Userprofileidentifier';
                break;
            case 'userprofilephonenumbers':
                result = 'Userprofilephonenumber';
                break;
            case 'userprofilephonetypes':
                result = 'Userprofilephonetype';
                break;
            case 'admin/usertype':
                result = 'Usertype';
                break;
            case 'admin/userworkstatustype':
                result = 'Userworkstatustype';
                break;
            case 'welfarelog':
                result = 'Welfarelog';
                break;
            case 'evaluationsourcetypes':
                result = 'Evaluationsourcetype';
                break;
            case 'evaluationsourceagency':
                result = 'Evaluationsourceagency';
                break;
            case 'findingtype':
                result = 'findingtype';
                break;
            case 'hearingtype':
                result = 'Hearingtype';
                break;
            case 'progressnoteroletypes':
                result = 'Progressnoteroletype';
                break;
            case 'intakeservicerequestevaluations':
                result = 'Intakeservicerequestevaluation';
                break;
            case 'intakeservicerequestpetition':
                result = 'Intakeservicerequestpetition';
                break;
            case 'intakeservicerequestsafetyplans':
                result = 'Intakeservicerequestsafetyplan';
                break;
            case 'intakeservicerequestsafetyplanaction':
                result = 'Intakeservicerequestsafetyplanaction';
                break;
            case 'progressnotepurposetype':
                result = 'Progressnotepurposetype';
                break;
            case 'courtordertype':
                result = 'Courtordertype';
                break;
            case 'adjudicateddecisiontype':
                result = 'Adjudicateddecisiontype';
                break;
            case 'caseclosuresummary':
                result = 'Caseclosuresummary';
                break;
            case 'responsibilitytype':
                result = 'Responsibilitytype';
                break;
            case 'closuresubtype':
                result = 'Closuresubtype';
                break;
            case 'closuretype':
                result = 'Closuretype';
                break;
            case 'caseclosureparticipant':
                result = 'Caseclosureparticipant';
                break;
            case 'gapdisclosure':
                result = 'Gapdisclosure';
                break;
            case 'gapannualreview':
                result = 'Gapannualreview';
                break;
            case 'gapsuspension':
                result = 'Gapsuspension';
                break;
            case 'suspensionreasontype':
                result = 'Suspensionreasontype';
                break;
            case 'gapagreement':
                result = 'Gapagreement';
                break;
            case 'offencelocationtype':
                result = 'Offencelocationtype';
                break;
            case 'foldertype':
                result = 'Foldertype';
                break;
            case 'folderreasontype':
                result = 'Folderreasontype';
                break;
            case 'finance':
                result = 'Finance';
                break;
            case 'providerapplicant':
                result = 'Providerapplicant';
                break;
            case 'intakerecomendationtype':
                result = 'Intakerecomendationtype';
                break;
            case 'investigationfindingassessors':
                result = 'Investigationfindingassessors';
                break;
            case 'contactparticipant':
                result = 'Contactparticipant';
                break;
            case 'professiontype':
                result = 'Professiontype';
                break;
            case 'closingcodetype':
                result = 'Closingcodetype';
                break;
            case 'sstastatustype':
                result = 'Sstastatustype';
                break;
            case 'taskcommunicationtype':
                result = 'Taskcommunicationtype';
                break;
            case 'evaluationdocument':
            case 'evaluationdocument/generateintakedocument':
                result = 'Evaluationdocument';
                break;
            case 'intakeservicerequestconsultreview':
                result = 'Intakeservicerequestconsultreview';
                break;
            case 'updatechecklisttasks':
                result = 'Updatechecklisttask';
                break;
            case 'provprogramtypes':
                result = 'provprogramtype';
                break;
            case 'safetyplans':
                result = 'Safetyplan';
                break;
            case 'providerreferral':
            case 'providerreferral/approveproviderreferral':
                result = 'Providerreferral';
                break;
            case 'assignedassessments':
                result = 'Assignedassessment';
                break;
            case 'supportlog':
                result = 'Supportlog';
                break;
            case 'admin/teammemberassignment':
            case 'admin/teammemberassignment/investigationplanassignto/list':
                result = 'Teammemberassignment';
                break;
            case 'contacttrialvisit':
                result = 'Contacttrialvisit';
                break;
            case 'allegationprovidermaltreatment':
                result = 'Allegationprovidermaltreatment';
                break;
            case 'providermaltreatmenttype':
                result = 'Providermaltreatmenttype';
                break;
            case 'intakeservsubtypes':
                result = 'Intakeservsubtype';
                break;
            case 'kinshipcare':
                result = 'Kinshipcare';
                break;
            case 'kinshipcarechecklist':
                result = 'Kinshipcarechecklist';
                break;
            case 'kinshipcaredocuments':
                result = 'Kinshipcaredocuments';
                break;
            case 'checklist':
                result = 'Checklist';
                break;
            case 'personnicknames':
                result = 'Personnickname';
                break;
            case 'personworkcarrergoal':
                result = 'Personworkcarrergoal';
                break;
            case 'personfamilyinfo':
                result = 'Personfamilyinfo';
                break;
            case 'personalerttype':
                result = 'Personalerttype';
                break;
            case 'persontransportation':
            case 'persontransportation/updatetransport':
                result = 'Persontransportation';
                break;
            case 'locationtotype':
                result = 'Locationtotype';
                break;
            case 'locationfromtype':
                result = 'Locationfromtype';
                break;
            case 'providerapplicantportal':
            case 'providerapplicantportal/updateapplprograminfo':
                result = 'Providerapplicantportal';
                break;
            case 'applicantstaffs':
                result = 'Applicantstaff';
                break;
            case 'providerlicense':
                result = 'Providerlicense';
                break;
            case 'referencetype':
                result = 'Referencetype';
                break;
            case 'intakeservreqcourtorder':
                result = 'Intakeservreqcourtorder';
                break;
            case 'intakeservreqcourtorderdetails':
                result = 'Intakeservreqcourtorderdetails';
                break;
            case 'kinshipcareprogram':
                result = 'Kinshipcareprogram';
                break;
            case 'foldertypeproviderconfig':
                result = 'Foldertypeproviderconfig';
                break;
            case 'placementleavereturn':
                result = 'Placementleavereturn';
                break;
            case 'providerinfo':
                result = 'Providerinfo';
                break;
            case 'placementrelease':
                result = 'Placementrelease';
                break;
            case 'tprrecommendationchecklist':
                result = 'Tprrecommendationchecklist';
                break;
            case 'tprrecommendation':
                result = 'Tprrecommendation';
                break;
            case 'tprdetails':
                result = 'Tprdetails';
                break;
            case 'legalcustody':
                result = 'Legalcustody';
                break;
            case 'tb_picklist_values':
                result = 'Tb_picklist_values';
                break;
            case 'focuspersoncasestatus':
                result = 'Focuspersoncasestatus';
                break;
            case 'oasactionletter':
                result = 'Oasactionletter';
                break;
            case 'actionletterprogramconfig':
                result = 'Actionletterprogramconfig';
                break;
            case 'tb_client_account':
            case 'tb_client_account/updatechildaccounts':
                result = 'Tb_client_account';
                break;
            case 'tb_service_purchase_authorization':
                result = 'Tb_service_purchase_authorization';
                break;
            case 'nationalitytypes':
                result = 'Nationalitytype';
                break;
            case 'personprimaryincometype':
            case 'personprimaryincometype/getvalues':
                result = 'Personprimaryincometype';
                break;
            case 'actionletterheaderconfig':
                result = 'Actionletterheaderconfig';
                break;
            case 'personalert':
                result = 'Personalert';
                break;
            case 'servicelogs':
            case 'servicelogs/update':
            case 'servicelogs/editvendorservicelog':
            case 'servicelogs/deletevendorservice':
                result = 'serviceLog';
                break;
            case 'picklistvalues':
                result = 'Picklistvalues';
                break;
            case 'tbagencyprogramareas':
                result = 'TbAgencyProgramArea';
                break;
            case 'tbservices':
                result = 'TbServices';
                break;
            case 'providerlistsearches':
                result = 'providerListSearch';
                break;
            case 'purchaseauthorizations':
            case 'purchaseauthorizations/updatepurchaseauthorize':
                result = 'purchaseAuthorization';
                break;
            case 'fiscalcodes':
                result = 'fiscalCode';
                break;
            case 'progressnotereasontypes':
                result = 'Progressnotereasontype';
                break;
            case 'consultreviewusertype':
                result = 'Consultreviewusertype';
                break;
            case 'personabusesubstancefrequencytype':
                result = 'Personabusesubstancefrequencytype';
                break;
            case 'tb_services/getbundledplacementserviceslist':
            case 'tb_services/getplacementstructureslist':
            case 'tb_services':
                result = 'Tb_services';
                break;
            case 'tb_placement/fostercarereferallist':
            case 'tb_placement':
            case 'tb_placement/approverejectedplacement':
            case 'tb_placement/addplacement':
                result = 'Tb_placement';
                break;
            case 'tb_provider/fostercareprovidersearch':
            case 'tb_provider':
                result = 'Tb_provider';
                break;
            case 'adoptionchecklistdetails':
                result = 'Adoptionchecklistdetails';
                break;
            case 'adoptionchecklist':
                result = 'Adoptionchecklist';
                break;
            case 'adoptionemotional':
                result = 'Adoptionemotional';
                break;
            case 'adoptionemotionaldetails':
                result = 'Adoptionemotionaldetails';
                break;
            case 'adoptionplanning':
                result = 'Adoptionplanning';
                break;
            case 'intakeservreqadultscreentool':
                result = 'Intakeservreqadultscreentool';
                break;
            case 'intakeservreqchildremoval':
                result = 'Intakeservreqchildremoval';
                break;
            case 'ihasprovidermonthlyreport':
                result = 'Ihasprovidermonthlyreport';
                break;
            case 'publicprovider':
                result = 'Publicprovider';
                break;
            case 'publicproviderapplicant':
                result = 'Publicproviderapplicant';
                break;
            case 'publicproviderreferral':
                result = 'Publicproviderreferral';
                break;
            case 'tb_commingled_account':
                result = 'Tb_commingled_account';
                break;
            case 'tb_account_transaction':
            case 'tb_account_transaction/updateclienttransaction':
                result = 'Tb_account_transaction';
                break;
            case 'intakeserreqrestitution':
                result = 'Intakeserreqrestitution';
                break;
            case 'adoptionbreakthelink':
                result = 'Adoptionbreakthelink';
                break;
            case 'providerassignmentownership':
                result = 'providerassignmentownership';
                break;
            case 'providerstaff':
                result = 'Providerstaff';
                break;
            case 'tb_provider_complaint/list':
            case 'tb_provider_complaint':
                result = 'Tb_provider_complaint';
                break;
            case 'providerlicensesanctions':
                result = 'Providerlicensesanctions';
                break;
            case 'providercontractprogram':
                result = 'Providercontractprogram';
                break;
            case 'restitutionpaymentflatfile':
                result = 'Restitutionpaymentflatfile';
                break;
            case 'providerincident':
                result = 'Providerincident';
                break;
            case 'provideryouthinfo':
                result = 'Provideryouthinfo';
                break;
            case 'tb_receivable_collection_status':
                result = 'Tb_receivable_collection_status';
                break;
            case 'tb_payment_plan':
                result = 'Tb_payment_plan';
                break;
            case 'tb_receivable_detail':
            case 'tb_receivable_detail/updateprovideroverpaymentlist':
                result = 'Tb_receivable_detail';
                break;
            case 'tb_comm_acct_transactions':
                result = 'Tb_comm_acct_transactions';
                break;
            case 'tb_provider_complaint_notes':
                result = 'Tb_provider_complaint_notes';
                break;
            case 'tb_provider_complaint_contacts':
                result = 'Tb_provider_complaint_contacts';
                break;
            case 'serviceplanvendor':
                result = 'Serviceplanvendor';
                break;
            case 'tb_payment_receipt':
                result = 'Tb_payment_receipt';
                break;
            case 'tb_provider_complaint_deficiency':
                result = 'Tb_provider_complaint_deficiency';
                break;
            case 'investigations':
                result = 'Investigation';
                break;
            case 'tb_placement_revision':
                result = 'Tb_placement_revision';
                break;
            case 'intakeservreqinputtypeagencies':
                result = 'Intakeservreqinputtypeagency';
                break;
            case 'tb_placement_validation':
            case 'tb_placement_validation/updateplacementvalidation':
                result = 'Tb_placement_validation';
                break;
            case 'tb_foster_care_rate':
                result = 'Tb_foster_care_rate';
                break;
            case 'servicecase':
                result = 'Servicecase';
                break;
            case 'intakeserreqrestitutionpayment':
                result = 'Intakeserreqrestitutionpayment';
                break;
            case 'providerportalrequest':
                result = 'Providerportalrequest';
                break;
            case 'providerportalrequestnarrative':
                result = 'Providerportalrequestnarrative';
                break;
            case 'assessmentactor':
                result = 'Assessmentactor';
                break;
            case 'adoptionagreement':
                result = 'Adoptionagreement';
                break;
            case 'adoptionagreementrate':
                result = 'Adoptionagreementrate';
                break;
            case 'livingarrangement':
                result = 'Livingarrangement';
                break;
            case 'tb_fiscal_category_master':
                result = 'Tb_fiscal_category_master';
                break;
            case 'tb_placement_stru_category_link':
                result = 'Tb_placement_stru_category_link';
                break;
            case 'tb_payment_header':
            case 'tb_payment_header/updatepaymentcheckstatus':
                result = 'Tb_payment_header';
                break;
            case 'referencevalues':
                result = 'Referencevalues';
                break;
            case 'stateoffice':
                result = 'Stateoffice';
                break;
            case 'persondisability':
                result = 'Persondisability';
                break;
            case 'servicedictionary':
                result = 'Servicedictionary';
                break;
            case 'caseclosureservice':
                result = 'Caseclosureservice';
                break;
            case 'serviceagreement':
                result = 'ServiceAgreement';
                break;
            case 'tb_account_fast_entry':
                result = 'Tb_account_fast_entry';
                break;
            case 'tb_child_account_disbursement':
                result = 'Tb_child_account_disbursement';
                break;
            case 'caseevaluation':
                result = 'CaseEvaluation';
                break;
            case 'gapagreementrate':
                result = 'Gapagreementrate';
                break;
            case 'tb_payment_status':
                result = 'Tb_payment_status';
                break;
            case 'adoptioncase':
                result = 'Adoptioncase';
                break;
            case 'cinapetition':
                result = 'Cinapetition';
                break;
            case 'adoptionemotional':
                result = 'Adoptionemotional';
                break;
            case 'adoptionsuspensionrevision':
                result = 'Adoptionsuspensionrevision';
                break;
            case 'caseevaluationservice':
                result = 'CaseEvaluationService';
                break;
            case 'petitionwitness':
                result = 'Petitionwitness';
                break;
            case 'servicecaserequests':
                result = 'Servicecaserequest';
                break;
            case 'servicecasedisposition':
                result = 'Servicecasedisposition';
                break;
            case 'publicproviderapplicanthouseholdbgchecks':
                result = 'Publicproviderapplicanthouseholdbgchecks';
                break;
            case 'providerstaffconfig':
                result = 'Providerstaffconfig';
                break;
            case 'tb_foster_care_rate_stg':
                result = 'Tb_foster_care_rate_stg';
                break;
            case 'providertraininginstructor':
                result = 'Providertraininginstructor';
                break;
            case 'publicproviderreconsideration':
                result = 'Publicproviderreconsideration';
                break;
            case 'publicproviderstatusmanagement':
                result = 'Publicproviderstatusmanagement';
                break;
            case 'pubprovapphouseholdbgchecks':
                result = 'Pubprovapphouseholdbgchecks';
                break;
            case 'publicproviderreferencecheck':
                result = 'Publicproviderreferencecheck';
                break;
            case 'publicproviderhomeplacementspecification':
                result = 'Publicproviderhomeplacementspecification';
                break;
            case 'publicproviderapplicanthousehold':
            case 'publicproviderapplicanthousehold/updatehousehold':
                result = 'Publicproviderapplicanthousehold';
                break;
            case 'publicproviderhomestudyhouseholdmapping':
                result = 'Publicproviderhomestudyhouseholdmapping';
                break;
            case 'publicproviderhomestudyvisit':
                result = 'Publicproviderhomestudyvisit';
                break;
            case 'providerorientationtrainingattendance':
                result = 'Providerorientationtrainingattendance';
                break;
            case 'providerorientationtraining':
                result = 'Providerorientationtraining';
                break;
            case 'publicproviderhouseholdchecklist':
                result = 'Publicproviderhouseholdchecklist';
                break;
            case 'publicproviderhousehold':
            case 'publicproviderhousehold/addchecklist':
                result = 'Publicproviderhousehold';
                break;
            case 'publicproviderhouseholdmember':
                result = 'Publicproviderhouseholdmember';
                break;
            case 'publicproviderhouseholdmember':
                result = 'Publicproviderhouseholdmember';
                break;
            case 'publicproviderhomeinfo':
                result = 'Publicproviderhomeinfo';
                break;
            case 'agencyprogramarea':
                result = 'Agencyprogramarea';
                break;
            case 'personprogramareas':
                result = 'Personprogramarea';
                break;
            case 'serviceplanfocus':
                result = 'Serviceplanfocus';
                break;
            case 'serviceplanstrength':
                result = 'Serviceplanstrength';
                break;
            case 'serviceplanpersoninvolved':
                result = 'Serviceplanpersoninvolved';
                break;
            case 'serviceplanneed':
                result = 'Serviceplanneed';
                break;
            case 'serviceplanaction':
                result = 'Serviceplanaction';
                break;
            case 'serviceplanoutcome':
                result = 'Serviceplanoutcome';
                break;
            case 'serviceplanchild':
                result = 'Serviceplanchild';
                break;
            case 'tb_ive_gapaudit':
                result = 'Tb_ive_gapaudit';
                break;
            case 'youthtransitionplan':
                result = 'YouthTransitionPlan';
                break;
            case 'caseplan1':
                result = 'Caseplan1';
                break;
            case 'tb_ive_adoption_audit':
                result = 'Tb_ive_adoption_audit';
                break;
            case 'caseassignments':
                result = 'Caseassignment';
                break;
            case 'personexamination':
                result = 'Personexamination';
                break;
            case 'clientunder5yearsinfo':
                result = 'Clientunder5yearsinfo';
                break;
            case 'personsexualinfo':
                result = 'Personsexualinfo';
                break;
            case 'personhospitalization':
                result = 'Personhospitalization';
                break;
            case 'personimmunizations':
                result = 'Personimmunization';
                break;
            case 'provider_uir_staff_config':
                result = 'Provider_uir_staff_config';
                break;
            case 'personmilitaryservices':
                result = 'Personmilitaryservices';
                break;
            case 'attorneyaddress':
                result = 'Attorneyaddress';
                break;
            case 'personimmunizationconfig':
                result = 'Personimmunizationconfig';
                break;
            case 'socialhistory':
                result = 'Socialhistory';
                break;
            case 'restricteditems':
                result = 'Restricteditems';
                break;
            case 'placementrevision':
                result = 'Placementrevision';
                break;
            case 'gapratesrevision':
                result = 'Gapratesrevision';
                break;
            case 'adoptionrevision':
                result = 'Adoptionrevision';
                break;
            case 'supportlogfiles':
                result = 'Supportlogfiles';
                break;
            case 'gapapplication':
                result = 'Gapapplication';
                break;
            case 'gapsuspensionrevision':
                result = 'Gapsuspensionrevision';
                break;
            case 'adoptionsuspension':
                result = 'Adoptionsuspension';
                break;
            case 'expungement':
                result = 'Expungement';
                break;
            case 'providerinfoconfig':
                result = 'Providerinfoconfig';
                break;
            case 'providerapprovetypeconfig':
                result = 'Providerapprovetypeconfig';
                break;
            case 'providerapproval':
                result = 'Providerapproval';
                break;
            case 'restitutionupload':
                result = 'restitutionupload';
                break;
            case 'residentialdischargereview':
                result = 'Residentialdischargereview';
                break;
            case 'Appointmenttitletypeconfigs':
                result = 'Appointmenttitletypeconfig';
                break;
            case 'city':
                result = 'City';
                break;
            case 'visitor':
                result = 'Visitor';
                break;
            case 'trackcommunitymonitoring':
                result = 'trackcommunitymonitoring';
                break;
            case 'Usernotificationtagconfigs':
                result = 'Usernotificationtagconfig';
                break;
            case 'intakeservicerequesticpc':
                result = 'Intakeservicerequesticpc';
                break;
            case 'tb_vendor_applicant':
                result = 'Tb_vendor_applicant';
                break;
            case 'tb_vendor_addresses':
                result = 'Tb_vendor_addresses';
                break;
            case 'tb_vendor_phone':
                result = 'Tb_vendor_phone';
                break;
            case 'tb_vendor_email':
                result = 'Tb_vendor_email';
                break;
            case 'permanencyplanhistory':
                result = 'Permanencyplanhistory';
                break;
            case 'splanobjective':
                result = 'Splanobjective';
                break;
            case 'splangoal':
                result = 'Splangoal';
                break;
            case 'snapshothist':
                result = 'Snapshothist';
                break;
            case 'serviceplanvisitationplanmapping':
                result = 'Serviceplanvisitationplanmapping';
                break;
            case 'visitationplan':
                result = 'Visitationplan';
                break;
            case 'visitationplanclients':
                result = 'Visitationplanclients';
                break;
            case 'userreference':
                result = 'Userreference';
                break;
            case 'placementadmissionclassificationtype':
                result = 'Placementadmissionclassificationtype';
                break;
            case 'investigationcopreferral':
                result = 'Investigationcopreferral';
                break;
            case 'privatetransportation':
                result = 'Privatetransportation';
                break;
            case 'trackcommunitymonitoring':
                result = 'trackcommunitymonitoring';
                break;
            case 'investigationform':
                result = 'Investigationform';
                break;
            case 'trackcommunitymonitoringviolations':
                result = 'trackcommunitymonitoringviolations';
                break;
            case 'appointmenttitletypeconfigs':
                result = 'Appointmenttitletypeconfig';
                break;
            case 'activitytaskprogressnoteconfigs':
                result = 'Activitytaskprogressnoteconfig';
                break;
            case 'restitutionccudocuments':
                result = 'Restitutionccudocument';
                break;
            case 'communitymonitoringviolationalertconfigs':
                result = 'Communitymonitoringviolationalertconfig';
                break;
            case 'communitymonitoringviolations':
                result = 'Communitymonitoringviolations';
                break;
            case 'intakeserreqinterstateresidingconfigs':
                result = 'Intakeserreqinterstateresidingconfig';
                break;
            case 'intakeservicerequestweaver':
                result = 'Intakeservicerequestweaver';
                break;
            case 'intakeservicerequestweavertypes':
                result = 'Intakeservicerequestweavertype';
                break;
            case 'residentialdischargereviewdocuments':
                result = 'Residentialdischargereviewdocuments';
                break;
            case 'residentialdischargereview':
                result = 'Residentialdischargereview';
                break;
            case 'residentialdischargereviewparticipants':
                result = 'Residentialdischargereviewparticipants';
                break;
            case 'usernotificationtagconfigs':
                result = 'Usernotificationtagconfig';
                break;
            case 'copreferraldocumentconfig':
                result = 'Copreferraldocumentconfig';
                break;
            case 'copreferralproviderconfig':
                result = 'Copreferralproviderconfig';
                break;
            case 'iepdisabilitycodes':
                result = 'iepdisabilitycode';
                break;
            case 'iepeducationalservices':
                result = 'iepeducationalservices';
                break;
            case 'iepspecialeducations':
                result = 'iepspecialeducation';
                break;
            case 'investigationcopreferral':
                result = 'Investigationcopreferral';
                break;
            case 'workeremails':
                result = 'Workeremail';
                break;
            case 'casereview':
                result = 'Casereview';
                break;
            case 'reviewparticipants':
                result = 'Reviewparticipants';
                break;
            case 'reviewrecommendations':
                result = 'Reviewrecommendations';
                break;
            case 'publicproviderpetinfo':
                result = 'Publicproviderpetinfo';
                break;
            case 'clearancesearch':
                result = 'Clearancesearch';
                break;
            case 'providerapprovalphaserecord':
                result = 'Providerapprovalphaserecord';
                break;
            case 'providerformconfig':
                result = 'Providerformconfig';
                break;
            case 'providerformdetails':
                result = 'Providerformdetails';
                break;
            case 'caseplan':
                result = 'Caseplan';
                break;
            case 'adoptionemotionalties':
                result = 'Adoptionemotionalties';
                break;
            case 'warrantrequestoutcome':
                result = 'Warrantrequestoutcome';
                break;
            case 'intakecaseconfig':
                result = 'Intakecaseconfig';
                break;
            case 'intakeserreqinterstate':
                result = 'Intakeserreqinterstate';
                break;
            case 'fmis_payment_vendor_date':
                result = 'Fmis_payment_vendor_date';
                break;
            case 'tb_provider_services':
                result = 'Tb_provider_services';
                break;
            case 'appealallegationconfig':
                result = 'Appealallegationconfig';
                break;
            case 'appealcomplaintconfig':
                result = 'Appealcomplaintconfig';
                break;
            case 'personhealthinsuranceserviceconfig':
                result = 'Personhealthinsuranceserviceconfig';
                break;
            case 'placementauthadmissionconfig':
                result = 'Placementauthadmissionconfig';
                break;
            case 'provider_uir':
                result = 'Provider_uir';
                break;
            case 'tb_receivable_detail_history':
                result = 'Tb_receivable_detail_history';
                break;
            case 'provider_uir_contact_detail':
                result = 'Provider_uir_contact_detail';
                break;
            case 'provider_uir_incident_detail':
                result = 'Provider_uir_incident_detail';
                break;
            case 'provider_uir_lawenforcement_detail':
                result = 'provider_uir_lawenforcement_detail';
                break;
            case 'details':
                result = 'Detail';
                break;
            case 'surveys':
                result = 'Survey';
                break;
            case 'supervisorreport/getfieldsforreports':
                result = 'Supervisorreport';
                break;
            case 'restitutionreport/generate/{type}':
                result = 'Restitutionreport';
                break;
            case 'withhold_eft_config':
                result = 'Withhold_eft_config';
                break;
            case 'accountreceivabledocuments':
                result = 'Accountreceivabledocuments';
                break;
            case 'collateral':
                result = 'Collateral';
                break;
            case 'collateraladdress':
                result = 'Collateraladdress';
                break;
            case 'provider_uir_youth_detail':
                result = 'Provider_uir_youth_detail';
                break;
            case 'collateralroleconfig':
                result = 'Collateralroleconfig';
                break;
            case 'caseassignmentactor':
                result = 'Caseassignmentactor';
                break;
            case 'programfacility':
                result = 'Tb_prov_program_facility';
                break;
            case 'adoptioncaseagreementrate':
                result = 'Adoptioncaseagreementrate';
                break; 
            case 'tb_placement_cpa_homes':
                result = 'Tb_placement_cpa_homes';
                break; 
            case 'external/persons': 
                    result = 'PersonProgram';
                    break;   
            case 'adoptioncaserevision':
                result = 'Adoptioncaserevision';
                break;
            case 'adoptioncasesuspension':
                result = 'Adoptioncasesuspension';
                break;
            case 'adoptioncasesuspensionrevision':
                result = 'Adoptioncasesuspensionrevision';
                break;   
            case 'reports':   
                result = 'Report';
                break;
            case 'adoptionannualreview':
                result = 'Adoptionannualreview';
                break;
            case 'adoptioncasedisposition':
                result = 'Adoptioncasedisposition';
                break;  
            case 'ldsslocations':
                result = 'Ldsslocations';
                break; 
            case 'adoptioniverenewal':
                result = 'Adoptioniverenewal';
                break; 
            case 'tb_provider_userconfig':
                result = 'Tb_provider_userconfig';
                break;         
        }
        const workEnv = config.workEnvironment;
        //  console.log('modname :' + modname + '     reslt', result + '/v1/');
        //     console.log('workEnvironment', workEnv);
        if (workEnv === 'state') {

            return result + '/v1/';
        } else {
            return '';
        }
    }
}
