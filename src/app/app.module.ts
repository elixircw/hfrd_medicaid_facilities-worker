import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatTooltipModule,
} from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { CookieService } from 'ngx-cookie-service';
import { NgxPaginationModule } from 'ngx-pagination';

import { CoreModule } from './@core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './auth/login/login.module';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formatting
import { ContactlogComponent } from './contactlog/contactlog.component';
@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatCheckboxModule,
        MatTabsModule,
        MatTooltipModule,
        HttpModule,
        AppRoutingModule,
        LoginModule,
        HttpClientModule,
        TimepickerModule.forRoot(),
        CoreModule.forRoot(),
        // FakeCoreModule.forRoot(),
        PaginationModule.forRoot(),
        FormsModule,
        A2Edatetimepicker,
        ReactiveFormsModule,
        MomentModule,
        NgIdleKeepaliveModule.forRoot(),
        NgxPaginationModule
    ],
    declarations: [AppComponent, ContactlogComponent],
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }, CookieService],
    bootstrap: [ AppComponent, ContactlogComponent ]
})
export class AppModule {}
