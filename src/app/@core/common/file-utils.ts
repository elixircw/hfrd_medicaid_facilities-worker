export class FileUtils {
    public static getFileName(fileExtension) {
        const currentDate = new Date();
        return (
            'DHS-' +
            currentDate.getUTCFullYear() +
            currentDate.getUTCMonth() +
            currentDate.getUTCDate() +
            '-' +
            this.getRandomString() +
            '.' +
            fileExtension
        );
    }

    public static getRandomString() {
        return (Math.random() * new Date().getTime())
            .toString(36)
            .replace(/\./g, '');
    }

    public static generateAccountNo() {
        return Math.floor(100000000 + Math.random() * 90000000)
        //  return "P" + Math.floor(100000000 + Math.random() * 90000000)
      }

    public static generateAccountNumber() {
        const numbers = [];
        let randomnumber;
        do {
            randomnumber = Math.floor(Math.random() * 999999) + 100000;
        } while (numbers.includes(randomnumber));
        numbers.push(randomnumber);
        console.log(randomnumber);
        return randomnumber;
       //  return Math.floor(100000000 + Math.random() * 90000000)
        //  return "P" + Math.floor(100000000 + Math.random() * 90000000)
      }

      static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }
}
