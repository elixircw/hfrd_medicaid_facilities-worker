import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldPermissionDirective } from './directives/field-permission.directive';

const DIRECTIVES = [
    FieldPermissionDirective
];

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [...DIRECTIVES],
  providers: [
    ...DIRECTIVES,
  ],
})
export class DirectiveModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DirectiveModule,
      providers: [
        ...DIRECTIVES,
      ],
    };
  }
}

