import { Injectable } from '@angular/core';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { hasMatch } from '../common/initializer';
import { AuthService } from '../services/auth.service';
import { CommonHttpService, DataStoreService } from '../services';
import { AppConfig } from '../../app.config';
import 'rxjs/add/operator/filter';
import { ResourceAccess } from '../entities/authDataModel';
import { PaginationRequest } from '../entities/common.entities';
// import { ResourceAccess } from '../entities/authDataModel';
@Injectable()
export class RoleGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router, private _store: DataStoreService, private _commonService: CommonHttpService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.checkActivation(route, state);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.checkActivation(route, state);
    }

    private checkActivation(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const currentUser = this.authService.getCurrentUser();
        if (route.data && route.data.screen && route.data.screen.key) {
            return this._commonService.getSingle(new PaginationRequest({where: {modulekey: route.data.screen.key}, method: 'get'}), AppConfig.pageProfile + '?arg').map((result: ResourceAccess) => {
                // if (result && result.parentresource) {
                    this._store.setData('PERMISSION', result.resources);
                //     const screen = result.parentresource.find(item => item.resourceid === route.data.screen.key);
                //     if (screen && screen.isallowed) {
                //         return true;
                //     } else {
                //         return false;
                //     }
                // }
                // return false;
                return true;
            });
        }
        return Observable.of(true);
    }
}
