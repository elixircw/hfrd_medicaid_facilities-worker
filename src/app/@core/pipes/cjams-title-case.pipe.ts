import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cjamsTitleCase'
})
export class CjamsTitleCasePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value){
     /* if(value.length > 20){
       let slicedStr = value.substring(0,20);
       let slicedArr = slicedStr.split(" ");
       if(slicedArr.length ==2){
         return    slicedArr[0].charAt(0).toUpperCase() + slicedArr[0].slice(1) +" "+
         slicedArr[1].charAt(0).toUpperCase() + slicedArr[1].slice(1)+"...";
       }else{
         return    slicedArr[0].charAt(0).toUpperCase() + slicedArr[0].slice(1)
       }
      }else{
        let slicedArr = value.split(" ");
       if(slicedArr.length ==2){
         return    slicedArr[0].charAt(0).toUpperCase() + slicedArr[0].slice(1) +" "+
         slicedArr[1].charAt(0).toUpperCase() + slicedArr[1].slice(1);
       }else{
         return    slicedArr[0].charAt(0).toUpperCase() + slicedArr[0].slice(1)
       }
      } */
      return value.charAt(0).toUpperCase() + value.slice(1);

    }
    return null;
  }

}
