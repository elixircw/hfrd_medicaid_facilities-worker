import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';

@Pipe({
    name: 'countDown'
})
export class CountDownTimer implements PipeTransform {
    currentTime: string;
    countDown: any;
    timeDiff: number;
    transform(value: any, hours: number = 2): Observable<any> {
        return Observable.interval(1000)
            .map((res) => {
                if (new Date().getTime() < new Date(value).getTime() + hours * 60 * 60 * 1000) {
                    this.timeDiff = Math.floor((new Date(value).getTime() + hours * 60 * 60 * 1000 - new Date().getTime()) / 1000) - 1;
                } else {
                    this.timeDiff = Math.floor((new Date().getTime() - new Date(value).getTime()) / 1000);
                }
                return this.timeDiff;
            })
            .map((res) => {
                const timeLeft = moment.duration(res, 'seconds');
                this.currentTime = '';
                // Days.
                if (timeLeft.days()) {
                    this.currentTime += timeLeft.days() + ' Days';
                }
                // Hours.
                if (timeLeft.hours()) {
                    this.currentTime += ' ' + timeLeft.hours() + ' Hours';
                }
                // Minutes.
                if (timeLeft.minutes()) {
                    this.currentTime += ' ' + timeLeft.minutes() + ' Mins';
                }
                // Seconds.
                // this.currentTime += ' ' + timeLeft.seconds() + ' Sec';

                if (new Date().getTime() < new Date(value).getTime() + hours * 60 * 60 * 1000) {
                    this.countDown = {
                        time: this.currentTime,
                        color: 'green'
                    };
                } else {
                    this.countDown = {
                        time:  this.currentTime + ' Overdue' ,
                        color: 'red'
                    };
                }
                return this.countDown;
            });
    }
}
