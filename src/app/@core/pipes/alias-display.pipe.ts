import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'aliasDisplay'
})
export class AliasDisplayPipe implements PipeTransform {

  transform(aliases: any, args?: any): string {
      let alias = "";
      if (aliases) {
        if (Array.isArray(aliases)) {
          alias = aliases.map(item => {
            let result = "";
            const fname = (item.firstname ? item.firstname : '');
            const lname = (item.lastname ? item.lastname : '');
            if (fname == '' && lname == '') {
              return;
            }
            result =  (fname ? fname + ' ' : '') + lname;
            return result;
          })
          .filter( item => {return typeof item != 'undefined'})
          .join(", ");
  
        } else {
          switch (typeof aliases) {
            case 'object':
              alias = (aliases.firstname ? aliases.firstname : '') + ' ' + (aliases.lastname ? aliases.lastname : '');
              break;
            case 'string':
              alias = aliases;
              break;
          }
        }
      }
      return alias;
  }

}
