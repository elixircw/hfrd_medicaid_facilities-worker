import { Directive, ElementRef, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[noDirty]'
})
export class NoDirtyDirective {
    constructor(private control: NgControl) {}

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {
        this.control.valueChanges.subscribe((v) => {
            if (this.control.dirty) {
                this.control.control.markAsPristine();
            }
        });
    }
}
