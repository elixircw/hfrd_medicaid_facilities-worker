import { Directive, Input, OnInit, TemplateRef, ViewContainerRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { DataStoreService } from '../services';
import { UserResource } from '../entities/authDataModel';
import { NgControl } from '@angular/forms';
// tslint:disable-next-line:directive-selector
@Directive({ selector: '[securityKey]' })
export class FieldPermissionDirective implements OnInit {
    constructor(private templateRef: TemplateRef<any>,
        private dataStore: DataStoreService,
        private viewContainer: ViewContainerRef,
        private cdr: ChangeDetectorRef) {}
    _securityKey: any;
    @Input() set securityKey(key: any) {
        this._securityKey = key;
    }
    ngOnInit() {
        if (this._securityKey) {
            this.cdr.detectChanges();
            const screenPermission = <UserResource[]>this.dataStore.getData('PERMISSION');
            if (screenPermission && screenPermission.length) {
                const fieldPermission = screenPermission.find(item => item.resourceid === this._securityKey);
                if (!fieldPermission) {
                    this.viewContainer.createEmbeddedView(this.templateRef);
                } else {
                    if (fieldPermission.resourcetype === 3) {
                        if (fieldPermission.isallowed) {
                            this.viewContainer.clear();
                        } else {
                            this.viewContainer.createEmbeddedView(this.templateRef);
                        }
                    } else if (fieldPermission.resourcetype === 4) {
                        if (fieldPermission.isvisible) {
                            this.viewContainer.createEmbeddedView(this.templateRef);
                        } else {
                            this.viewContainer.clear();
                        }
                    } else {
                        this.viewContainer.clear();
                    }
                }
            } else {
                // this.viewContainer.clear();
                 this.viewContainer.createEmbeddedView(this.templateRef);
            }
        } else {
            this.viewContainer.createEmbeddedView(this.templateRef);
        }
    }
}
