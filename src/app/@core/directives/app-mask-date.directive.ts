import { Directive, ElementRef, OnDestroy } from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[appMaskDate]'
})
export class AppMaskDateDirective implements  OnDestroy {

  private el: HTMLInputElement;
  mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]; // mm/dd/yyyy
  maskedInputController;

  constructor(
    private elementRef: ElementRef
  ) {

    /* this.maskedInputController = textMask.maskInput({
      inputElement: this.elementRef.nativeElement,
      mask: this.mask
    }); */
  }

  ngOnDestroy() {
   // this.maskedInputController.destroy();
  }

}
