import { Injectable, OnInit } from '@angular/core';
import { UserProfilePhoneNumber } from '../../pages/admin/user-security-profile/_entites/user-security-profile.data.modal';
declare var $: any;

export interface Token {
    id: string;
    ttl: number;
    created: Date;
    userId: string;
}

@Injectable()
export class AppToken implements Token {
    id: string;
    ttl: number;
    created: Date;
    userId: string;
    user: UserInfo;
}

@Injectable()
export class AppUser extends AppToken {
    role: AppRole;
    resources: AppResource[];
}

export class UserLogin {
    public email: string;
    public password: string;
}

export class AppRole {
    id: string;
    name: string;
    description: string;
    key: string;
}

export class AppResource {
    id: string;
    parentid: string;
    name: string;
    isallowed: boolean;
    resourcetype: number;
    parentkey?: string;
    resourceid?: string;
    modulekey?: string;
}
export class UserInfo {
    realm: string;
    username: string;
    email: string;
    securityusersid: string;
    emailVerified: boolean;
    id: number;
    userphoto: string;
    userprofile: UserProfile;
    ismanualrouting: boolean;
}

export class UserProfile {
    securityusersid: string;
    firstname: string;
    lastname: string;
    displayname: string;
    fullname: string;
    activeflag: number;
    otherfields: string;
    onprobation: boolean;
    expirationdate: Date;
    email: string;
    oldId: string;
    title: string;
    isavailable: boolean;
    userworkstatustypekey: string;
    voidedby: string;
    voidedon: Date;
    voidreasonid: string;
    calendarkey: string;
    orgname: string;
    orgnumber: string;
    usertypekey: string;
    autonotification: boolean;
    userphoto: string;
    gendertypekey: string;
    middlename: string;
    dob: Date;
    teammemberassignment: TeamMemberAssignment;
    teamtypekey: string;
    ismanualrouting: boolean;
    cjamspid: string;
    userprofilephonenumber: UserProfilePhoneNumber[];
    userprofileaddress: UserProfileAddress[];
    viewpreference: string;
}

export class TeamMemberAssignment {
    teammemberid: string;
    securityusersid: string;
    teammember: TeamMember;
}

export class TeamMember {
    teammemberid: string;
    teamid: string;
    loadnumber: string;
    roletypekey?: string;
    team: Team;
    teammemberroletype: any;
}
export class Team {
    id: string;
    name: string;
    teamtypekey: string;
    teamType: TeamType;
    parentteamid?: string;
    countyid?: string;
}
export class TeamType {
    teamtypekey: string;
    description: string;
}

export interface UserRole {
    id: number;
    name: string;
    description: string;
}

export interface UserResource {
    id: string;
    parentid?: any;
    name: string;
    resourceid: string;
    resourcetype: number;
    isSelected: boolean;
    isallowed: boolean;
    isvisible: boolean;
    isenabled: boolean;
}

export interface ResourceAccess {
    role: UserRole;
    resources: UserResource[];
    parentresource: UserResource[];
}
export class UserProfileAddress {
    activeflag: string;
    address: string;
    city: string;
    country: string;
    county: string;
    countyid: string;
    effectivedate: string;
    expirationdate: string;
    oldId: string;
    pobox: string;
    securityusersid: string;
    state: string;
    userprofileaddressid: string;
    userprofileaddresstypekey: string;
    voidedby: string;
    voidedon: string;
    voidreasonid: string;
    zipcode: string;
    zipcodeplus: string;
}
