import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { HttpService } from './http.service';
import { AlertService } from './alert.service';
import { RouterTestingModule } from '@angular/router/testing';
import { config } from '../../../environments/config';

fdescribe('HttpService against STATE environment configuration', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpService, AlertService],
      imports: [HttpClientTestingModule, RouterTestingModule]
    });
    //TODO: make environment variables an injectable service 
    config.workEnvironment = 'state';
  });
  afterEach(() => {
    //clean up: TODO make environment variables an injectable service 
    config.workEnvironment = 'local';
  });

  it('should be created', inject([HttpService], (service: HttpService) => {
    expect(service).toBeTruthy();
  }));
  it('url check for admin/region/list', inject([HttpService], (service: HttpService) => {
    expect(service['get_formatted_url']('admin/region/list')).toBe('Region/v1/admin/region/list');
  }));
  it('url check for admin/region/', inject([HttpService], (service: HttpService) => {
    expect(service['get_formatted_url']('admin/region/')).toBe('Region/v1/admin/region/');
  }));
  it('url check for personphonenumbers/personphonenumberdelete', inject([HttpService], (service: HttpService) => {
    expect(service['get_formatted_url']('personphonenumbers/personphonenumberdelete')).toBe('Personphonenumber/v1/personphonenumbers/personphonenumberdelete');
  }));

});
