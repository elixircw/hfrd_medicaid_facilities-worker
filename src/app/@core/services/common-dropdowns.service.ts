import { Injectable } from '@angular/core';
import { DropdownModel } from '../entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { CommonUrlConfig } from '../common/URLs/common-url.config';
import { CommonHttpService } from './common-http.service';
import { NewUrlConfig } from '../../pages/newintake/newintake-url.config';
import { AuthService } from './auth.service';
import { NullTemplateVisitor } from '@angular/compiler';
import { DataStoreService } from './data-store.service';
import { CASE_STORE_CONSTANTS } from '../../pages/case-worker/_entities/caseworker.data.constants';


@Injectable()
export class CommonDropdownsService {


  _personRelations;
  constructor(private dropdownService: CommonHttpService, private authService: AuthService,
    private _dataStoreService: DataStoreService) { }

  getGenders(): Observable<any[]> {
    return this.dropdownService
      .getArrayList({
        where: { activeflag: 1 },
        method: 'get',
        nolimit: true
      },
        CommonUrlConfig.EndPoint.Listing.GenderTypeUrl + '?filter').map(res => {

          return res;
        }
        );
  }

  getEtinicity(): Observable<any[]> {
    return this.dropdownService
      .getArrayList({
        where: { activeflag: 1 },
        method: 'get',
        nolimit: true
      },
        CommonUrlConfig.EndPoint.Listing.EthnicGroupTypeUrl + '?filter').map(res => {

          return res;
        }
        );
  }

  getStateList(): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true
      },
      CommonUrlConfig.EndPoint.Listing.StateListUrl + '?filter'
    );
  }

  getNationalOriginList(): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        order: 'description'
      },
      CommonUrlConfig.EndPoint.Listing.NationalOriginList
    );
  }

  getCountyList(state: string): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        where: { state: state },
        order: 'countyname',
        method: 'get',
        nolimit: true
      },
      CommonUrlConfig.EndPoint.Listing.CountyListUrl + '?filter'
    );
  }

  getRelations(): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { 'personrelationship': true }
      },
      CommonUrlConfig.EndPoint.Listing.RelationshipTypesUrl + '?filter'
    );
  }

  getReligions(): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        order: 'typedescription'
      },
      CommonUrlConfig.EndPoint.Intake.ReligionTypeUrl + '?filter'
    );
  }

  getRaces(): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        activeflag: 1,
        order: 'typedescription'
      },
      CommonUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
    );
  }

  getMaritalStatus(): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        order: 'typedescription'
      },
      CommonUrlConfig.EndPoint.Intake.MaritalStatusUrl + '?filter'
    );
  }

  getAlertType(): Observable<any[]> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true
      },
      NewUrlConfig.EndPoint.Intake.AlertType + '?filter'
    );
  }

  getFolderTypes(): Observable<any[]> {
    return this.dropdownService
      .getArrayList(
        {}, CommonUrlConfig.EndPoint.Intake.folderTypeList
      )
      .map(result => {
        return result.map(
          res =>
            new DropdownModel({
              text: res.description,
              value: res.foldertypekey
            })
        );
      });
  }

  getAddressType(): Observable<any> {
    return this.dropdownService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        order: 'typedescription'
      }, CommonUrlConfig.EndPoint.Intake.AddressTypeUrl);
  }

  getFrequencyDetails(): Observable<any> {
    return this.dropdownService.getArrayList({
      method: 'get'
    }, CommonUrlConfig.EndPoint.Intake.substanceAbuseFrequencyList);
  }

  get personRelations() {
    return this._personRelations;
  }

  set personRelations(personRelations) {
    this._personRelations = personRelations;
  }

  getDropownsByTable(tableName, teamTypeKey = '') {
    const teamTypKeyVal = (teamTypeKey) ? teamTypeKey : this.authService.getAgencyName();
    return this.dropdownService.getArrayList({
      method: 'get',
      where: { tablename: tableName, teamtypekey: teamTypKeyVal }
    }, 'referencetype/gettypes?filter');
  }

  getCommunicationTypes(teamTypKeyVal?) {
    return this.dropdownService.getArrayList({
      method: 'get',
      where: { teamtypekey: teamTypKeyVal, activeflag: 1 },
      order : 'description'
    }, `Intakeservicerequestinputtypes/list?filter`);
  }

  getListByTableID(tableId) {
    return this.dropdownService.getArrayList(
      {
        where: { referencetypeid: tableId, teamtypekey: this.authService.getAgencyName(), order: 'description asc' },
        method: 'get'
      },
      'referencetype/gettypes?filter'
    );
  }

  getValidDate(givenDate: string) {
    if (givenDate) {
      const processedDate = new Date(givenDate);
      processedDate.setMinutes(processedDate.getMinutes() + processedDate.getTimezoneOffset());
      return processedDate;
    } else {
      return null;
    }
  }

  getPickListByName(tableName) {

    return this.dropdownService.getArrayList({
      method: 'get',
      where: { tablename: tableName, teamtypekey: null }
    }, 'referencetype/gettypes?filter');
  }

  getPickList(picklistid) {
    return this.dropdownService.getArrayList({
      where: {
        'picklist_type_id': picklistid
      },
      nolimit: true,
      method: 'get'
    }, 'tb_picklist_values/getpicklist?filter');
  }

  getPickListByMdmcode(code) {

    return this.dropdownService.getArrayList({
      method: 'get',
      nolimit: true,
      order: 'description',
      where: { referencetypeid: 306, mdmcode: code }
    }, 'referencevalues?filter');
  }

  getStoredCaseUuid() {
    console.log('current store', this._dataStoreService.getCurrentStore());
    const caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    if (caseID) {
      return caseID;
    }
    const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
    let caseUUID = null;
    if (caseInfo) {
      caseUUID = caseInfo.intakeserviceid;
    }
    console.log('stored caseuid', caseUUID);
    return caseUUID;
  }

  getStoredCaseNumber() {
    console.log('current store', this._dataStoreService.getCurrentStore());
    const daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    if (daNumber) {
      return daNumber;
    }
    const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
    let ldaNumber = null;
    if (caseInfo) {
      ldaNumber = caseInfo.da_number;
    }
    console.log('stored da_number', ldaNumber);
    return ldaNumber;
  }

  formatPhoneNumber(phoneNumberString: string) {
    if (!phoneNumberString) {
      return null;
    }
    const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
    const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      return '(' + match[1] + ') ' + match[2] + '-' + match[3];
    }
    return null;
  }

  /**
   * This gets reference values using the type id directly
   * Preference shound be to use the getReferenveValuesByTypeName
   * @param typeid the reference type id
   */
  getReferenveValuesByTypeId(typeid) {
    return this.dropdownService.getArrayList(
      {
        nolimit: true,
        where: { referencetypeid: typeid},
        method: 'get'
      },
      'referencevalues?filter'
    );
  }

  /**
   * This gets reference values using the type id and teamtypekey
   * @param typeid the reference type id
   * @param teamtypekey the reference team type key
   */
  getReferenveValuesByTypeIdandTeam(typeid, teamkey) {
    return this.dropdownService.getArrayList(
      {
        nolimit: true,
        where: { referencetypeid: typeid, teamtypekey: teamkey},
        method: 'get'
      },
      'referencevalues?filter'
    );
  }

  /**
   * 
   * @param tableName The reference type name is called 'tablename' in referencetype table
   */
  getReferenveValuesByTypeName(typename) {
    return this.dropdownService.getArrayList({
      method: 'get',
      where: { tablename: typename, teamtypekey: null }
    }, 
    'referencetype/gettypes?filter');
  }

}
