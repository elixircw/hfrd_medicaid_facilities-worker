import 'rxjs/Rx';

import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { NavigationEnd, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { distinctUntilChanged } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { AppConfig } from '../../app.config';
import { AppUser } from '../entities/authDataModel';
import { DataStoreService } from './data-store.service';
import { HttpService } from './http.service';
import { SessionStorageService, LocalStorageService } from './storage.service';
import { config } from '../../../environments/config';
import { environment } from '../../../environments/environment';
import { AppConstants } from '../common/constants';
import { hasMatch } from '../common/initializer';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class AuthService {
    private currentUserSubject = new BehaviorSubject<AppUser>({} as AppUser);
    public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();
    userProfile: any;

    //@Simar - Multi-role hack: use this to hold the original role that the user logged-in with
    //This way on switching context to different role we have a placeholder for the primary role
    primaryRole: any;
    public dashboardConfig$ = new Subject<any>();

    constructor(
        private location: Location,
        private storage: SessionStorageService,
        private localstore: LocalStorageService,
        private http: HttpService,
        private router: Router,
        private _storeService: DataStoreService,
        private _CookieService: CookieService,
        private _http: Http
    ) { }

    public callAPIToResetCokiesBackFromOpenAm(){
        const token = this.storage.getObj('token') as AppUser;
        this.http.get(AppConfig.roleProfileUrl).subscribe((user: AppUser) => {
            token.role = user.role;
            token.resources = user.resources;
            this.isAuthenticatedSubject.next(true);
            this.currentUserSubject.next(Object.assign(token, user));            
        });
    }

    // Verify JWT in localstorage with server & load user's info.
    // This runs once on application startup.
    public populate(returnUrl: string = '') {
        if (this.location.path().indexOf('/devices/') !== -1) {
            if (this.storage.getObj('isDevice')) {
                const token = this.storage.getObj('token') as AppUser;
                this.http.get(AppConfig.roleProfileUrl).subscribe((user: AppUser) => {
                    token.role = user.role;
                    token.resources = user.resources;
                    this.isAuthenticatedSubject.next(true);
                    const tokenInfo = Object.assign(token, user);
                    this.currentUserSubject.next(tokenInfo);
                    this.storage.setObj('token', tokenInfo);
                    this.router.navigate([returnUrl]);
                });
            }
        } else {
            const token = this.storage.getObj('token') as AppUser;
            if (!token) {
                if (location.pathname.indexOf('external-assessment') === -1 && location.hash.indexOf('external-assessment') === -1) {
                    this.currentUserSubject.next({} as AppUser);
                    // Set auth status to false
                    this.isAuthenticatedSubject.next(false);
                    // Remove any potential remnants of previous auth states
                    this.router.navigate(['/login']);
                } else {
                    if (location.pathname.indexOf('external-assessment') !== -1) {
                        this.router.navigate([location.pathname], { queryParams: { access_token: location.search.replace('?access_token=', '') } });
                    } else {
                        this.router.navigate([location.hash.replace('#', '')]);
                    }
                }
                // }
            } else {
                const currentUser = this.getCurrentUser();
                if (Object.keys(currentUser).length === 0) {
                    this.http.get(AppConfig.roleProfileUrl).subscribe((user: AppUser) => {
                        token.role = user.role;
                        token.resources = user.resources;
                        this.isAuthenticatedSubject.next(true);
                        this.currentUserSubject.next(Object.assign(token, user));
                        if (this.location.path() !== '') {
                            this.router.navigate([this.location.path()]);
                        } else {
                            if (user && user.id && user.role) {
                                console.log('user', user);
                                this.roleBasedRoute(user.role.name.toLowerCase());
                            }
                        }
                    });
                }
            }
        }
    }

    public openAMlogin(email: string, password: string): Observable<AppUser> {
        return this.http.post('admin/userprofile/generateAccessToken', JSON.stringify({ email: email.toLowerCase(), password: password, fromdevice: 1 })).flatMap((token: AppUser) => {
            // const token = appuser.UserToken.token;
            // token.user = appuser.UserToken.user;
            // token.user.userprofile = appuser.UserToken.userprofile;
            // token.userId = appuser.UserToken.user.id;
            this.storage.setObj('token', token);
            console.log('Accesstoken', token);
            this.isAuthenticatedSubject.next(true);
            return this.http.get(AppConfig.roleProfileUrl).map((user: AppUser) => {
                token.role = user.role;
                token.resources = user.resources;
                this.savePrimaryRole(user.role);
                this.currentUserSubject.next(Object.assign(token, user));
                return token;
            });
        });
    }

    public login(email: string, password: string): Observable<AppUser> {
        return this.http.post(AppConfig.authTokenUrl, JSON.stringify({ email: email.toLowerCase(), password: password, fromdevice: 1 })).flatMap((token: AppUser) => {
            this.storage.setObj('token', token);
            if(token.user.userprofile.teamtypekey) {
                this.storage.setItem('userTeamType', token.user.userprofile.teamtypekey);
            }           
            this.isAuthenticatedSubject.next(true);
            return this.http.get(AppConfig.roleProfileUrl).map((user: AppUser) => {
                token.role = user.role;
                token.resources = user.resources;
                this.savePrimaryRole(user.role);
                this.currentUserSubject.next(Object.assign(token, user));
                return token;
            });
        });
    }

    public logout() {
        // tslint:disable-next-line:prefer-const
        let workEnv = config.workEnvironment;
        return this.http.post(AppConfig.logoutUrl).subscribe(
            response => {
                console.log(response);
                this.storage.removeItem('token');
                this.storage.removeItem('fbToken');
                this.storage.removeItem('PERSON_NAVIGATION_INFO');
                this.storage.removeItem('intake');
                this.storage.removeItem('activeModuleNav');
                this.router.routeReuseStrategy.shouldReuseRoute = function () {
                    return false;
                };
                if (workEnv === 'state') {
                    window.location.reload(true);
                    this.storage.removeItem('PERSON_NAVIGATION_INFO');
                    this.storage.removeItem('intake');
                    // clear session storage properties in log out
                    this.storage.removeItem('ISSERVICECASE');
                    this.storage.removeItem('activeModuleNav');
                    const currentUrl = environment.logoutDHSURL;
                    // console.log("currentUrl",currentUrl, "env", environment.envName);
                    window.location.replace(currentUrl);
                } else {
                    const currentUrl = 'login';
                    this.router.navigateByUrl(currentUrl).then(() => {
                        this.isAuthenticatedSubject.next(false);
                        this.currentUserSubject.next({} as AppUser);
                        this._storeService.clearStore();
                        this.storage.clear();
                        this.localstore.clear();
                        this.router.navigated = false;
                        this.router.navigate([currentUrl], { replaceUrl: true });
                        this.router.events.subscribe(event => {
                            if (event instanceof NavigationEnd) {
                                location.reload();
                            }
                        });
                    });
                }
            },
            error => {
                console.log(error);
                // Some time we lost header information of Open AM 
                this.clearLogout();
            }
        );
    }
    // Some time we lost header information of Open AM so the post call to clear user logged in information may not be
    // execute
    public clearLogout() {
        const workEnv = config.workEnvironment;
        this.storage.removeItem('token');
        this.storage.removeItem('fbToken');
        this.storage.removeItem('PERSON_NAVIGATION_INFO');
        this.storage.removeItem('intake');
        this.storage.removeItem('userTeamType');
        if (workEnv === 'state') {
            window.location.reload(true);
            this.storage.removeItem('PERSON_NAVIGATION_INFO');
            this.storage.removeItem('intake');
            // clear session storage properties in log out
            this.storage.removeItem('ISSERVICECASE');
            this.storage.removeItem('activeModuleNav');
            const currentUrl = environment.logoutDHSURL;
            // console.log("currentUrl",currentUrl, "env", environment.envName);
            if ( currentUrl) {
                window.location.replace(currentUrl);
            } else {
                location.reload();
            }

        } else {
            location.reload();
        }
    }

    private savePrimaryRole(userRole) {
        this.primaryRole = userRole;
    }

    public getCurrentUser(): AppUser {
        return this.currentUserSubject.value;
    }

    private clearSessionroleswitching(newRole) {
        const temptoken = this.storage.getItem('token');
        const tempactiveModuleAgency = this.storage.getItem('activeModuleAgency');
        const tempfbtoken = this.storage.getItem('fbToken');
        this.storage.clear();
        this.storage.setItem('token', temptoken);
        this.storage.setItem('activeModuleAgency', tempactiveModuleAgency);
        this.storage.setItem('activeModuleNav', newRole.modulename);
        this.storage.setItem('activeModuleRole', newRole.rolename);
        this.storage.setItem('fbToken', tempfbtoken);
        }

    public changeUserRole(newRole) {
        //Clearing session storage for user role change and restoring the session tokens for authentication.
        this.clearSessionroleswitching(newRole);
        this.getCurrentUser().role.name = newRole.rolename;
        this.getCurrentUser().role.key = newRole.rolekey;
        this.currentUserSubject.next(this.getCurrentUser());
    }

    public isDevice(): boolean {
        return this.storage.getObj('isDevice');
    }

    public roleBasedRoute(roleName: string) {
        console.log(roleName);
        if (roleName === 'superuser') {
            this.router.navigate(['/pages/newintake/new-saveintake']);
            // this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.CASE_WORKER) {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.SUPERVISOR || roleName === AppConstants.ROLES.KINSHIP_SUPERVISOR.toLowerCase()) {
            // this.router.navigate(['/pages/case-worker']);
            this.router.navigate(['/pages/cjams-dashboard']);
        } else if (roleName === AppConstants.ROLES.KINSHIP_SUPERVISOR.toLowerCase()) {
            // this.router.navigate(['/pages/case-worker']);
            this.router.navigate(['/pages/cjams-dashboard/cw-intake-referals']);
        } else if (roleName === AppConstants.ROLES.PROVIDER) {
            // this.router.navigate(['/pages/case-worker']);
            this.router.navigate(['/pages/cjams-dashboard']);
        } else if (roleName === 'ihassp') {
            this.router.navigate(['/pages/as-service-plan-approval']);
        } else if (roleName === 'cru') {
            this.router.navigate(['/pages/newintake/new-saveintake']);
        } else if (roleName === 'ASCW') {
            this.router.navigate(['/pages/newintake/new-saveintake']);
        } else if (roleName === AppConstants.ROLES.COURT_WORKER.toLowerCase()) {
            this.router.navigate(['/pages/sao-dashboard']);
        } else if (roleName === 'case worker') {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === 'intake worker,olm') {
            this.router.navigate(['/pages/provider-referral/existing-referral']);
        } else if (roleName === 'mhcp') {
            this.router.navigate(['/pages/practitioners']);
        } else if (roleName === 'phcp') {
            this.router.navigate(['/pages/practitioners']);
        } else if (roleName === 'rnc') {
            this.router.navigate(['/pages/practitioners']);
        } else if (roleName === AppConstants.ROLES.IV_E_WORKER) {
            this.router.navigate(['/pages/notification']);
            // } else if (roleName === 'ivew') {
            //     this.router.navigate(['/pages/title4e/worker4e']);
        } else if (roleName === 'provider applicant') {
            this.router.navigate(['/pages/practitioners']);
        } else if (roleName === 'dssa') {
            this.router.navigate(['/pages/as-dss-admin']);
        } else if (roleName === 'cwfs') {
            this.router.navigate(['/pages/cjams-dashboard']);
        } else if (roleName === 'iv-e specialist') {
            this.router.navigate(['/pages/title4e/worker4e']);
        } else if (roleName === 'iv-e supervisor') {
            this.router.navigate(['/pages/title4e/supervisor4e']);
        } else if (roleName === AppConstants.ROLES.RESTITUTION_COORDINATOR.toLowerCase()) {
            this.router.navigate(['/pages/cjams-dashboard/restitution/review']);
        } else if (roleName === AppConstants.ROLES.FINANCE_WORKER.toLowerCase()) {
            this.router.navigate(['/pages/finance/finance-dashboard']);
        } else if (roleName === AppConstants.ROLES.LDSS_FISCAL_SUPERVISOR.toLowerCase()) {
            this.router.navigate(['/pages/finance/finance-dashboard']);
        } else if (roleName === AppConstants.ROLES.DIRECTOR_OF_FINANCE.toLowerCase()) {
            this.router.navigate(['/pages/finance/finance-dashboard/director-approval']);
        } else if (roleName === AppConstants.ROLES.LICENSING_ADMINISTRATOR.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.QUALITY_ASSURANCE.toLowerCase()) {
            this.router.navigate(['/pages/provider/complaints/dashboard']);
        } else if (roleName === AppConstants.ROLES.PROGRAM_MANAGER.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.FINANCE_WORKER_DJS.toLowerCase()) {
            // DJS Financial Worker role
            this.router.navigate(['/pages/restitution/djs-finance-dashboard/accounts']);
        } else if (roleName === AppConstants.ROLES.RESTITUTION_SUPERVISOR.toLowerCase()) {
            // DJS Restitution Supervisor
            this.router.navigate(['/pages/restitution/djs-finance-dashboard/changeform']);
        } else if (roleName === AppConstants.ROLES.DJS_PLACEMENT_WORKER.toLowerCase()) {
            // DJS Detention worker
            this.router.navigate(['/pages/cjams-dashboard/djsplacement-dashboard']);
        } else if (roleName === AppConstants.ROLES.DJS_ATD_PLACEMENT_WORKER.toLowerCase()) {
            // DJS ATD Detention worker
            this.router.navigate(['/pages/cjams-dashboard/djsplacement-dashboard']);
        }
        // LDSS Public Provider roles
        else if (roleName === AppConstants.ROLES.LDSS_DIRECTOR.toLowerCase()) {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.LDSS_RESOURCE_WORKER.toLowerCase()) {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.LDSS_SUPERVISOR.toLowerCase()) {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.LDSS_HOMESTUDY_WORKER.toLowerCase()) {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.LDSS_RECRUITER_TRAINER.toLowerCase()) {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.TRANSPORTATION_OFFICER.toLowerCase()) {
            this.router.navigate(['/pages/transport-dboard']);
        } else if (roleName.toLowerCase() === 'intake worker') {
            this.router.navigate(['/pages/newintake/new-saveintake']);
        } else if (roleName === AppConstants.ROLES.EXECUTIVE_DIRECTOR.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.DEPUTY_DIRECTOR.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.DESIGNEE.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.PRIVATE_SUPERVISOR.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.PROVIDER_DJS_QA.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.PROVIDER_DJS_PROGRAMDIRECTOR.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.PROVIDER_DJS_RESOURCE_SUPERVISOR.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.PROVIDER_DJS_RESOURCE.toLowerCase()) {
            this.router.navigate(['/pages/provider-applicant/existing-applicant']);
        } else if (roleName === AppConstants.ROLES.APPEAL_USER.toLowerCase()) {
            this.router.navigate(['/pages/home-dashboard']);
        } else if (roleName === AppConstants.ROLES.DHS_LEGAL_ATTORNEY.toLowerCase()) {
            this.router.navigate(['/pages/case-search/list']);
        } else {
            this.router.navigate(['/pages/default-dashboard']);
        }

    }

    public getAgencyName() {
        return this.getCurrentUser().user.userprofile.teamtypekey;
    }

    public isDJS() {
        return this.getAgencyName() === 'DJS';
    }

    public isCW() {
        return this.getAgencyName() === 'CW';
    }

    public isAS() {
        return this.getAgencyName() === 'AS';
    }

    public selectedRoleIs(roleName: string): boolean {
        let roleFound = false;
        const userInfo = this.getCurrentUser();
        if (userInfo && userInfo.role && userInfo.role.name) {
            roleFound = userInfo.role.name === roleName;
        }
        return roleFound;
    }

    public isLoggedIn() {
        return this.storage.getItem('token') ? true : false;
    }

    public routeToProviderPortal() {
        this.router.navigate(['/provider-portal/dashboard']);
    }


    hasModuleAccess(key: string): boolean {
        const user = this.getCurrentUser();
        if (key && user.resources.length > 0) {
            const resources = user.resources.filter(menu => menu.isallowed === true);
            if (hasMatch([key], resources.map(item => (item ? item.name : '')))) {
                return true;
            }
            return false;
        }
        return false;
    }

    hasAccess(key: string): Observable<boolean> {
        return this.currentUser.map(user => {
            if (key && user.resources.length > 0) {
                const resources = user.resources.filter(menu => menu.isallowed === true);
                if (hasMatch([key], resources.map(item => (item ? item.name : '')))) {
                    return true;
                }
                return false;
            }
            return true;
        });
    }
}
