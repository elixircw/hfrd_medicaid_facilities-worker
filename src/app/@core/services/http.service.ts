import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AppConfig } from '../../app.config';
import { AlertService } from './alert.service';

@Injectable()
export class HttpService {
    baseUrl = '';
    overrideUrl = false;

    errorData: HttpErrorResponse;

    public headers = new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'application/json');

    constructor(private http: HttpClient,
        private _alertService: AlertService) {
        this.baseUrl = AppConfig.baseUrl;
    }

    private get_formatted_url(url: string): string {

        let ulen = url.length ;
        if (url.lastIndexOf('?') > 0 ) { ulen = url.lastIndexOf('?'); }
        
        let modname = url.substring(0, ulen);

        // Check for / before substring to it
        if (modname.includes('/')) {
            modname = modname.substring(0, url.lastIndexOf('/'));
        }

        if (modname === '' || modname === 'Intake' || modname === 'admin') {
            modname = url.substring(0, url.lastIndexOf('?'));
        }
        if (modname.includes('Intakeservicerequests/getdsdsactionsummarydtls')) {
            modname = 'intakeservicerequests/getdsdsactionsummarydtls';
        }
        if (modname.includes('Intakeservicerequests/updatefolderchange')) {
            modname = 'intakeservicerequests/updatefolderchange';
        }
        if (modname.includes('Intakeservicerequests/getcaseconnected')) {
            modname = 'intakeservicerequests/getcaseconnected';
        }
        if (modname.includes('tb_account_transaction')) {
            modname = 'tb_account_transaction';
        }
        if (modname.includes('tb_commingled_account')) {
            modname = 'tb_commingled_account';
        }
        if (url.includes('tb_comm_acct_transactions')) {
            modname = 'tb_comm_acct_transactions';
        }
        if (modname.includes('titleive')) {
            modname = 'titleive';
        }
        if (modname.includes('serviceLogs/editVendorServiceLog')) {
            modname = 'servicelogs/editvendorservicelog';
        }
        if (modname.includes('Documentproperties/getpersonattachments')) {
            modname = 'documentproperties/getpersonattachments';
        }
        if (modname.includes('Documentproperties/updatepersonattachments')) {
            modname = 'documentproperties/updatepersonattachments';
        }
        if (modname.includes('Documentproperties/ecmsclientdetails')) {
            modname = 'documentproperties/ecmsclientdetails';
        }
        if (modname.includes('admin/assessment/getassessmentform')) {
            modname = 'admin/assessment';
        }
        if (modname.includes('restricteditems')) {
            modname = 'restricteditems';
        }
        if (url.includes('manage/team')) {
            modname = 'manage/team';
        }
        if (url.includes('serviceplanneed')) {
            modname = 'serviceplanneed';
        }
        if (url.includes('serviceplanstrength')) {
            modname = 'serviceplanstrength';
        }
        if (url.includes('snapshothist')) {
            modname = 'snapshothist';
        }
        if (url.includes('personmilitaryservices')) {
            modname = 'Personmilitaryservices';
        }
        if (url.includes('admin/assessment/createassessmentinternal')) {
            modname = 'admin/assessment/createassessmentinternal';
        }
        if (url.includes('tb_child_account_disbursement')) {
            modname = 'tb_child_account_disbursement';
        }
        if (modname.includes('Personemails')) {
            modname = 'personemails';
        }
        if (url.includes('tb_payment_header')) {
            modname = 'tb_payment_header';
        }
        if (modname.includes('Intakeservicerequests/getadoptionsummarydtls')) {
            modname = 'intakeservicerequests/getadoptionsummarydtls';
        }
        if (url.includes('external/persons')) {
            modname = 'external/persons';
        }
        return AppConfig.getModuleMapName(modname) + url;
    }

    public get(url: string, params = {}): Observable<any> {
        // console.log(url);
        //  headers.set('Access-Control-Allow-Methods','*');
        const res = this.request('GET', this.get_formatted_url(url), {}, params);
        return res.catch(
            (err: HttpErrorResponse) => {
                // this._alertService.error('A server error has occurred. Please try again or contact support.');
                // @Simar - On request, hiding the error pop-up, but logging on the console for dev debugging
                console.log('CONSOLE LOGGING SERVER ERROR', err);
                this.errorData = err;
                //@Dharmendra - below error codes need to change based on error received in get requests
                if (err.error && err.error.error && err.error.error.code) {
                    if (err.error.error.code === 'LOGIN_FAILED') {
                        this._alertService.warn('Invalid email or password.');
                    } else if (err.error.error.code === 'MAX_LENGTH_EXCEEDED') {
                        this._alertService.warn('Exceeded the maximum character limit allowed for certain fields');
                    }
                    else{
                        this._alertService.error('A server error has occurred. Please try again or contact support.');
                    }
                }

                // (<any>$('#service-error-confirm-action')).modal('show');
                return Observable.empty<any>();
            }
        );
    }

    public post(url: string, body: any = {}, params = {}): Observable<any> {
        // console.log(url);
        const response = this.request('POST', this.get_formatted_url(url), body, params);
        return response.catch(
            (err: HttpErrorResponse) => {
                // this._alertService.error('A server error has occurred. Please try again or contact support.');
                // @Simar - On request, hiding the error pop-up, but logging on the console for dev debugging
                console.log('CONSOLE LOGGING SERVER ERROR', err);
                this.errorData = err;
                //@Dharmendra - below error codes need to extended based on error received in post requests
                if (err.error && err.error.error && err.error.error.code) {
                    if (err.error.error.code === 'LOGIN_FAILED') {
                        this._alertService.warn('Invalid email or password.');
                    } else if (err.error.error.code === 'MAX_LENGTH_EXCEEDED') {
                        this._alertService.warn('Exceeded the maximum character limit allowed for certain fields.');
                    } else if (err.error.error.code === 'INTEGER_EXPECTED') {
                        this._alertService.warn('Enter integer value in amount fields.');
                    }
                    else{
                        this._alertService.error('A server error has occurred. Please try again or contact support.');
                    }
                }
                // (<any>$('#service-error-confirm-action')).modal('show');
                return Observable.empty<any>();
            }
        );
    }

    public download(url: string, body: any = {}, params = {}, responseType: string): Observable<any> {
        // console.log(url);
        return this.request('POST', this.get_formatted_url(url), body, params, responseType);
    }

    public put(url: string, body: any = {}, params = {}): Observable<any> {
        // console.log(url);
        return this.request('PUT', this.get_formatted_url(url), body, params);
    }

    public patch(url: string, body: any = {}, params = {}): Observable<any> {
        // console.log(url);
        return this.request('PATCH', this.get_formatted_url(url), body, params);
    }

    public delete(url: string, body: any = {}, params = {}): Observable<any> {
        // console.log(url);
        return this.request('DELETE', this.get_formatted_url(url), body, params);
    }

    upload(fileItem: File, url: string, extraData?: object): Observable<any> {
        // console.log(url);
        const formData: FormData = new FormData();

        formData.append('fileItem', fileItem, fileItem.name);
        if (extraData) {
            // tslint:disable-next-line:forin
            for (const key in extraData) {
                // iterate and set other form data
                formData.append(key, extraData[key]);
            }
        }

        const req = new HttpRequest(
            'POST',
            `${this.baseUrl}/${url}`,
            {
                body: formData,
                headers: new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'multipart/form-data'),
                reportProgress: true
            },
            { reportProgress: true }
        );
        return this.http.request(req);
    }

    public downloadXml(url) {
        // resolve url with conditions
        const fullUrl = this.get_formatted_url(url);

        // change and reset headers around request for DL
        this.setHeader('Content-Type', 'application/xml');
        const response = this.request('GET', fullUrl, {}, {}, 'blob');
        this.resetHeaders();
        
        return response;
    }

    public request(method: string, url, body: any = {}, params = {}, responseType = null) {
        // console.log(url);
        if (!this.overrideUrl) {
            url = `${this.baseUrl}/${url}`;
        }

        const paramsBuild: any = this.buildParams(params);
        const options: any = {
            body: body,
            headers: this.headers,
            params: paramsBuild,
            responseType: 'json'
        };
        if (responseType) {
            options.responseType = responseType;
        }
        return this.http.request(method, url, options);
    }

    public buildParams(paramsObj: any): HttpParams {
        let params = new HttpParams();
        Object.keys(paramsObj).forEach((key) => {
            params = params.set(key, paramsObj[key]);
        });
        return params;
    }

    public resetHeaders(): void {
        this.headers = new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'application/json');
    }

    public setHeader(key: string, value: string): void {
        this.headers = this.headers.set(key, value);
    }

    public deleteHeader(key: string): void {
        this.headers = this.headers.delete(key);
    }
}
