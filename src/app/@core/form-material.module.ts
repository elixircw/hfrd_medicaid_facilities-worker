import { NgModule } from '@angular/core';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatStepperModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatProgressSpinnerModule,
  MatGridListModule,
  MatDividerModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedDirectivesModule } from './directives/shared-directives.module';
import { CommonControlsModule } from '../shared/modules/common-controls/common-controls.module';
@NgModule({
  imports: [
  ],
  exports: [   FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatTooltipModule,
    SharedDirectivesModule,
    CommonControlsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatStepperModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatDividerModule
    ]
})
export class FormMaterialModule { }
