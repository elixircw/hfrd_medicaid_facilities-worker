import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from '../@core/services/storage.service';
import { environment } from '../../environments/environment';
import { HttpService } from '../@core/services/http.service';
import { AlertService, CommonHttpService } from '../@core/services';
import { ActivatedRoute } from '@angular/router';
import { CaseWorkerUrlConfig } from '../pages/case-worker/case-worker-url.config';
declare var $: any;
declare var Formio: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'external-assessment',
    templateUrl: './external-assessment.component.html',
    styleUrls: ['./external-assessment.component.scss']
})
export class ExternalAssessmentComponent implements OnInit {
    assessmmentName: string;
    private servicereqid: string;
    private templateId: string;
    private submissionId: string;
    access_token: string;
    isApproved: boolean;
    intakeservicerequestactorid: string;
    // tslint:disable-next-line:max-line-length
    constructor(private storage: SessionStorageService, private route: ActivatedRoute, private _http: HttpService, private _alertService: AlertService, private _commonService: CommonHttpService) {
        this.servicereqid = route.snapshot.params['servicereqid'];
        if (route.snapshot.params['isApproved'] === 'review') {
            this.isApproved = false;
        } else if (route.snapshot.params['isApproved'] === 'approved') {
            this.isApproved = true;
        }
        this.templateId = route.snapshot.params['templateId'];
        this.submissionId = route.snapshot.params['submissionId'];
        this.access_token = route.snapshot.queryParams['access_token'];
        this.intakeservicerequestactorid = route.snapshot.params['intakeservicerequestactorid'];
        if (this.access_token) {
            this.storage.setObj('token', { id: this.access_token });
        }
    }

    ngOnInit() {
        this.openAssessment();
    }
    private openAssessment() {
        setTimeout(() => {
            Formio.setToken(this.storage.getObj('fbToken'));
            // Formio.setToken(this.access_token);
            Formio.baseUrl = environment.formBuilderHost;
            const _self = this;
            let formioUrl = '';
            if (this.submissionId === 'survey') {
                formioUrl = environment.formBuilderHost + `/form/${this.templateId}`;
             } else {
                formioUrl = environment.formBuilderHost + `/form/${this.templateId}/submission/${this.submissionId}`;
             }

            Formio.createForm(document.getElementById('assessmentForm'), formioUrl, {
                readOnly: _self.isApproved
            }).then(function(form) {
                const formData = form.data;
                formData.userrole = 'Intake Worker';
                if (_self.submissionId === 'survey') {
                    formData.isCjamsId = _self.route.snapshot.params['isApproved'];
                }
                form.submission = {
                    data: formData
                };
                form.on('submit', submission => {
                    if (_self.submissionId === 'survey') {
                        _self.surveyFormSave(submission);
                    } else {
                        _self.transportationFormSave(submission);
                    }
                });
            });
        }, 3000);
    }

    private transportationFormSave(submission) {
        const formContent = document.getElementById('assessmentForm').innerHTML;
        const attachment = window.btoa(String(formContent));
        const schoolEmail = [
            {
                email: submission.data['sendemailid'],
                objectid: this.submissionId,
                objecttypekey: 'Assessment',
                attachment: attachment,
                message: ''
            }
        ];
        this._commonService.create(schoolEmail, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.EmailSchoolNotification).subscribe(res => {
            this._alertService.success(this.assessmmentName + ' saved successfully.');
        });
    }

    private surveyFormSave(submission) {
        this._http
            .post('admin/assessment/Add', {
                externaltemplateid: this.templateId,
                objectid: this.servicereqid,
                submissionid: submission._id,
                submissiondata: submission.data ? submission.data : null,
                form: submission.form ? submission.form : null,
                score: submission.data.score ? submission.data.score : 0,
                intakeservicerequestactorid: this.intakeservicerequestactorid
            })
            .subscribe(response => {
                this._alertService.success('Survey form saved successfully.');
            });
    }
}
