import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute, RouteConfigLoadEnd, RouteConfigLoadStart, Router } from '@angular/router';

import { environment } from '../environments/environment';
import { AppUser, UserInfo } from './@core/entities/authDataModel';
import { AuthService } from './@core/services';
import { CommonHttpService } from './@core/services/common-http.service';
import { SessionStorageService } from './@core/services/storage.service';

import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import { ContactlogComponent } from './contactlog/contactlog.component';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    currentlyLoadingCount = this.router.events.scan((c, e) => this.countLoads(c, e), 0);
    idleState = 'Not started.';
    timedOut = false;
    lastPing?: Date = null;
    timer: Number;
    isLoggedin: boolean;
    iscountdown: boolean;

    @ViewChild(ContactlogComponent) contactLogComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private _storage: SessionStorageService,
        private _authService: AuthService,
        private _http: HttpClient,
        private location: Location,
        private _service: CommonHttpService,
        private idle: Idle, private keepalive: Keepalive,
        private zone: NgZone
    ) {
        this.zone.runOutsideAngular(() => {
            const IdleTime = environment.IdleTimeOut;
            const PopupTime = environment.PopupTimeOut;
            this.timer = PopupTime;
            this.iscountdown = false;
            // sets an idle timeout of 780 seconds - 13 min
            idle.setIdle(IdleTime);
            // sets an idle timeout of 60 seconds - 1 min
            idle.setTimeout(PopupTime);
            // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
            idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
            idle.onIdleEnd.subscribe(() => {
                this.idleState = 'No longer idle.';
                console.log('idle.onIdleEnd >> No longer idle.');
            });
            idle.onTimeout.subscribe(() => {
                this.idleState = 'Timed out!';
                this.timedOut = true;
                this.iscountdown = false;
                console.log('idle.onTimeout >> ');
            });
            idle.onIdleStart.subscribe(() => {
                this.idleState = 'You\'ve gone idle!';
                console.log('idle.onIdleStart >> You\'ve gone idle!');
            });
            idle.onTimeoutWarning.subscribe((countdown) => {
                if (this._authService.isLoggedIn() && this.router.url !== '/login') {
                    (<any>$('#session-time-out')).modal('show');
                    this.idleState = 'You will time out in ' + countdown + ' seconds!';
                    console.log('You will time out in ' + countdown + ' seconds!');
                    if ( countdown ===  PopupTime && !this.iscountdown ) {
                        this.iscountdown = true;
                        this.startTimer(countdown);
                    }
                }
            });
            keepalive.interval(PopupTime);

            keepalive.onPing.subscribe(() => {
                this.lastPing = new Date();
                // console.log('keepalive.onPing>> ' + this.lastPing);
            });

            this.reset();
        });
    }
    reset() {
        const PopupTime = environment.PopupTimeOut;
        this.timer = PopupTime;
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
    }

    startTimer(duration) {
        let timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = timer / 60;
            seconds = timer % 60;
            minutes = parseInt(minutes + '', 10);
            seconds = parseInt(seconds + '', 10);
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;
            this.timer = minutes + ':' + seconds;
            console.log('this.timer :', this.timer );
            if (--timer < 0) {
                timer = duration;
            }
            if (this.timer === '00:00' ) {
               this.timedOut = true;
             }
             document.getElementById('time').innerHTML = this.timer;
        }, 1000);
    }

    private countLoads(counter: number, event: any): number {
        if (this._storage.getItem('isDevice')) {
            return 0;
        }
        if (event instanceof RouteConfigLoadStart) {
            return counter + 1;
        }
        if (event instanceof RouteConfigLoadEnd) {
            return counter - 1;
        }
        return counter;
    }
    continueSession() {
        this._authService.callAPIToResetCokiesBackFromOpenAm();
        this.reset();
        location.reload();
        (<any>$('#session-time-out')).modal('hide');
    }
    logOutSession() {
        this._authService.clearLogout();
        (<any>$('#session-time-out')).modal('hide');
    }
    ngOnInit() {
        const id = this.route.snapshot.params['token'];
        const returnUrl = this.route.snapshot.params['returnUrl'];
        if (id && returnUrl) {
            const appUser = new AppUser();
            appUser.id = id;
            appUser.user = new UserInfo();
            this._storage.setObj('token', appUser);
            this._storage.setObj('isDevice', true);
            const encodedReturnUrl = decodeURIComponent(returnUrl);
            this._authService.populate(encodedReturnUrl);
        } else {
            // const fbToken = this._storage.getObj('fbToken');
            // if (!fbToken) {
            //     const templateUrl = environment.formBuilderHost + `/user/login`;
            //     this._http
            //         .post(
            //             templateUrl,
            //             {
            //                 data: {
            //                     email: environment.formBuilderUserId,
            //                     password: environment.formBuilderPassword
            //                 }
            //             },
            //             { observe: 'response' }
            //         )
            //         .subscribe(res => {
            //             this._storage.setObj('fbToken', res.headers.get('x-jwt-token'));
            //         });
            // }
            this._authService.populate();
        }
    }

    refreshPage() {
        window.location.reload();
    }

    contactSupport() {
        this.contactLogComponent.launchContactlog();
    }
}
