import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactlogComponent } from './contactlog.component';

describe('ContactlogComponent', () => {
  let component: ContactlogComponent;
  let fixture: ComponentFixture<ContactlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
