import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../pages/provider-applicant/new-public-applicant/public-applicant-add-document/_entities/attachmnt.model';
import { AlertService, DataStoreService, SessionStorageService, CommonHttpService, AuthService } from '../@core/services';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import * as _ from 'lodash';
import { PaginationRequest } from '../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../pages/case-worker/case-worker-url.config';
import { AppConstants } from '../@core/common/constants';
import { AppUser } from '../@core/entities/authDataModel';
import { GLOBAL_MESSAGES } from '../@core/entities/constants';
import { AppConfig } from '../app.config';

@Component({
    selector: 'contactlog',
    templateUrl: './contactlog.component.html',
    styleUrls: ['./contactlog.component.scss']
})
export class ContactlogComponent implements OnInit {

    @ViewChild('canvas') canvas: any;

    pushRightClass = 'push-right';
    userInfo: AppUser;
    today = Date.now();
    role = '';
    agency = '';
    totalNotificationCount = 10;
    showNotification = false;
    isPreIntake = false;
    dashBoardLink = '';
    isDjs = false;
    ROLES = AppConstants.ROLES;
    feedbackUser: UserProfile;
    curDate: Date;
    feedbackForm: FormGroup;
    searchForm: FormGroup;
    supportNo: number;
    fileUploaded: any;
    screenshot: any;
    isLoggedIn: boolean;
    ticketsList: any = [];
    ticketsCount: number;
    maxPageSize = 10;
    query = {};
    showProgress = false;
    downloadMessage: string;
    downloadAttachments: boolean = false;
    enableDownload: boolean = false;
    severityTypes:any;
    issueTypes:any;

    constructor(
        public router: Router,
        private _authService: AuthService,
        private _service: CommonHttpService,
        private _dataStoreService: DataStoreService,
        private _sessionStorage: SessionStorageService,
        private _formBuild: FormBuilder,
        private _alert: AlertService,
        private location: Location) {

        this.isLoggedIn = false;
    }

    private newMethod() {
        return this;
    }

    ngOnInit() {

        this.userInfo = this._authService.getCurrentUser();
        this.severityTypes = [
            {   key: "C",
                value: "Critical"
            },
            {   key: "H",
                value: "High"
            },
            {   key: "M",
                value: "Medium"
            },
            {   key: "L",
                value: "Low"
            },
        ];
        this.issueTypes = [
            {   key: "E",
                value: "Enhancement"
            },
            {   key: "B",
                value: "Bug/Issue/Defect"
            },
            {   key: "Q",
                value: "Question/Policy"
            },
            {   key: "G",
                value: "Gap/Missing from Legacy System"
            },
        ];
        this.initializeForm();
        this.initilizeSearchForm();
    }

    getSeverityType(key){
        return _.get(_.filter(this.severityTypes, {key}), "0.value");
    }

    getIssueType(key){
        return _.get(_.filter(this.issueTypes, {key}), "0.value");
    }

    launchContactlog() {
        const _self = this;
        this.userInfo = this._authService.getCurrentUser();
        this.resetFeedback();
        html2canvas(document.body).then(function (canvas) {
            _self.screenshot = canvas.toDataURL('image/png');
            // tslint:disable-next-line: comment-format
            //console.log(_self.screenshot);
            const canvasEl: HTMLCanvasElement = _self.canvas.nativeElement;
            const cx: CanvasRenderingContext2D = canvasEl.getContext('2d');

            // set the width and height
            //   canvasEl.width = 200;
            //   canvasEl.height = 100;

            // set some default properties about the line
            cx.lineWidth = 3;
            cx.lineCap = 'round';
            cx.strokeStyle = '#000';

            let image = new Image();
            image.onload = function () {
                cx.drawImage(image, 0, 0, 200, 140);
            };
            image.src = _self.screenshot;

            _self.fillFeedback();

        });
    }

    fillFeedback() {

        this.curDate = new Date();
        let caseid = 'Dashboard';
        this.feedbackForm.patchValue({ clientid: 'Dashboard' });
        if (this.location.path().indexOf('/case-worker/') !== -1) {
            caseid = this.location.path().split('/')[4];
            //this.getInvolvedPerson();
            this.feedbackForm.patchValue({ clientid: 'CaseWorker' });
        } else if (this.location.path().indexOf('/my-newintake/') !== -1) {
            caseid = this._sessionStorage.getObj('intake') ? this._sessionStorage.getObj('intake').number : '';
            this.feedbackForm.patchValue({ clientid: 'Intake' });
        }
        this.feedbackForm.patchValue({ supportlogdate: this.curDate, caseid: caseid, pageurl: this.location.path() });
        this.feedbackForm.patchValue({ filedata: this.screenshot });
        (<any>$('#user-feedback')).modal('show');
    }

    forceDownload(fileId){
        const wso2Module = AppConfig.getModuleMapName('supportlogfiles');
        window.open(`${AppConfig.baseUrl}/${wso2Module}supportlogfiles/download?id=${fileId}&access_token=${this.userInfo.id}`, '_blank');
    }

    viewTickets() {
        this.userInfo = this._authService.getCurrentUser();
        this.getDownloadAccessRole();
        this.searchForm.patchValue({ searchText: this.userInfo.user.email });
        this.searchTickets();
        (<any>$('#user-tickets')).modal('show');
    }

    hideTickets() {
        this.clearSearchTickets();
        (<any>$('#user-tickets')).modal('hide');
    }

    pageChanged(event: any) {
        this.getSupportTickets(event - 1);
        return event;
    }

    searchTickets() {
        this.ticketsCount = 0
        const { searchType , searchText } = this.searchForm.value;
        let query = {};

        if(searchType === 'effectivedate') {
            query = {
                and: [
                    {'effectivedate': {gt: new Date(searchText.trim()+ " 00:00:00")}},
                    {'effectivedate': {lt: new Date(searchText.trim()+ " 23:59:59")}}
                ],
            }
        } else {
            query[searchType] = {like: `%${searchText.trim()}%`};
        }

        this.query = query;
        this.getSupportTickets(0);
    }

    clearSearchTickets() {
        this.query = {};
        this.ticketsList = [];
        this.searchForm.reset();
        this.initilizeSearchForm();
        this.getSupportTickets(0);
    }

    downloadTickets() {
        const { searchType, searchText } = this.searchForm.value;
        this.downloadMessage = '';
        if (searchType === 'effectivedate' && !isNaN(Date.parse(searchText))) {
            const filter = {
                where: {
                    and: [
                        { 'effectivedate': { gt: new Date(searchText.trim() + " 00:00:00") } },
                        { 'effectivedate': { lt: new Date(searchText.trim() + " 23:59:59") } }
                    ],
                }
            };
            const wso2Module = AppConfig.getModuleMapName('supportlogfiles');
            window.open(`${AppConfig.baseUrl}/${wso2Module}supportlogfiles/downloadall?filter=${JSON.stringify(filter)}&access_token=${this.userInfo.id}`, '_blank');
        } else {
            this.downloadMessage = 'Please select "Date" filter with valid date';
        }
    }

    onAttachmentChange(event) {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            this.fileUploaded = event.target.files[0];
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.feedbackForm.patchValue({
                    filedata: reader.result
                });
            };
        }
    }
    sendFeedback() {
        this._service.create(this.feedbackForm.value, 'supportlog/add').subscribe(
            res => {
                this._alert.success('Feedback sent successfully');
                this.resetFeedback();
                this.supportNo = res.supportno;
                (<any>$('#supportNo')).modal('show');
            },
            err => {
                this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    resetFeedback() {
        (<any>$('#user-feedback')).modal('hide');
        this.feedbackForm.reset();
        this.fileUploaded = Object.assign({});
        (<any>$('#filedata')).val(null);
        if (this.userInfo.user && this.userInfo.user.userprofile) {
            this.feedbackForm.patchValue({
                displayname: this.userInfo.user.userprofile.displayname,
                cjamspid: this.userInfo.user.userprofile.cjamspid,
                frommailid: this.userInfo.user.userprofile.email,
                userrole: this.userInfo.role.description,
                severity: ''
            });
        }
    }
    private initializeForm() {
        this.feedbackForm = this._formBuild.group({
            displayname: [''],
            cjamspid: [''],
            frommailid: [''],
            ldssregion: [''],
            supportlogdate: [null],
            officelocation: [''],
            clientid: [''],
            subject: [''],
            notes: ['', [Validators.required]],
            userrole: [''],
            caseid: [''],
            filedata: [null],
            severity: [''],
            issuetype: [''],
            pageurl: ['']
        });
        if (this.userInfo.user && this.userInfo.user.userprofile) {
            this.feedbackForm.patchValue({
                displayname: this.userInfo.user.userprofile.displayname,
                cjamspid: this.userInfo.user.userprofile.cjamspid,
                frommailid: this.userInfo.user.userprofile.email,
                userrole: this.userInfo.role.description
            });
        }
    }

    private initilizeSearchForm(){
        this.downloadMessage = '';
        this.searchForm = this._formBuild.group({
            searchText: [''],
            searchType: ['frommailid']
        });

        this.searchForm.valueChanges.subscribe((result)=>{
            this.enableDownload = false;
        });
    }

    private getDownloadAccessRole(){
        const cache = Date.now();
        this._service
            .getAll(`Authorizes/getPageProfile?cache=${cache}&arg={"count":-1,"where":{"modulekey":"contact-support"},"method":"get"}`)
            .subscribe(res => {
                const data: any = res;
                this.downloadAttachments = _.includes(_.map(data.resources, 'resourceid'), 'can-download-defects');
            });
    }

    private getInvolvedPerson() {
        const intakeServiceId = this.location.path().split('/')[3] ? this.location.path().split('/')[3] : null;
        this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeserviceid: intakeServiceId }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
            .subscribe(res => {
                const person = res.data.filter(child => child.rolename === 'RC')[0];
                this.feedbackForm.patchValue({ clientid: person.cjamspid });
            });
    }

    private getSupportTickets(pageNumber = 0) {
        const pageSize = pageNumber * this.maxPageSize;
        const filter: any = {
            limit: this.maxPageSize,
            skip: pageSize,
            where: this.query,
            fields: {
                frommailid: true,
                notes: true,
                effectivedate: true,
                clientid: true,
                subject: true,
                supportno: true,
                supportlogid: true,
                caseid: true,
                severity: true,
                issuetype: true,
            },
            include:{
                relation:'supportlogfiles',              
                scope:{
                    fields: ["supportlogfilesid"]
                }
            },
            order: 'effectivedate desc'
        };
        this.showProgress = true;

        if (this.ticketsCount) {
            this.ticketsList = [];
            if (this.ticketsCount) {
                this.getTickets(filter);
            }
        } else {
            this._service
                .getAll('supportlog/count')
                .subscribe(res => {
                    const data: any = res;
                    this.ticketsCount = data.count;
                    this.getTickets(filter);
                });
        }
    }

    private getTickets(filter) {
        this._service
            .getAll('supportlog?filter=' + encodeURIComponent(JSON.stringify(filter)))
            .subscribe(res => {
                if(this.searchForm['controls'].searchType.value === 'effectivedate' && !isNaN(Date.parse(this.searchForm['controls'].searchText.value))){
                    this.enableDownload = true;
                }
                this.showProgress = false;
                this.ticketsList = res;
            }, err => {
                this.showProgress = false;
            });
    }
}
