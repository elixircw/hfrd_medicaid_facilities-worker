import { AppConfig } from './app.config';
import { config } from '../environments/config';

fdescribe('AppConfig against STATE environment configuration', () => {
    let appConfig: AppConfig;

    beforeEach(() => {
        appConfig = new AppConfig();
        //TODO: make environment variables an injectable service 
        config.workEnvironment = 'local';
    });
    afterEach(() => {
        //clean up: TODO make environment variables an injectable service 
        config.workEnvironment = 'local';
    });

    it('should create an instance', () => {
        expect(appConfig).toBeTruthy();
    });

    it('admin/region', () => {
        expect(AppConfig.getModuleMapName('admin/region')).toBe('Region/v1/');
    });
    it('admin/county', () => {
        expect(AppConfig.getModuleMapName('admin/county')).toBe('County/v1/');
    });
    it('personphonenumbers/personphonenumberdelete', () => {
        expect(AppConfig.getModuleMapName('personphonenumbers/personphonenumberdelete')).toBe('Personphonenumber/v1/');
    });

});
