import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import { PlacementValidationsRoutingModule } from './placement-validations-routing.module';
import { PlacementValidationsComponent } from './placement-validations.component';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { PendingPlacementComponent } from './pending-placement/pending-placement.component';
import { ApprovedPlacementComponent } from './approved-placement/approved-placement.component';
import { IntakeUtils } from '../_utils/intake-utils.service';
import { FormMaterialModule } from '../../@core/form-material.module';
import { SortTableModule } from './../../shared/modules/sortable-table/sortable-table.module';

@NgModule({
  imports: [
    CommonModule,
    PlacementValidationsRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    FormMaterialModule,
    SortTableModule
  ],
  declarations: [PlacementValidationsComponent, PendingPlacementComponent, ApprovedPlacementComponent],
  providers: [IntakeUtils]
})
export class PlacementValidationsModule { }
