import { Component, OnInit } from '@angular/core';
import { AuthService, CommonHttpService, AlertService } from '../../@core/services';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserProfile } from '../../@core/entities/authDataModel';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'placement-validations',
  templateUrl: './placement-validations.component.html',
  styleUrls: ['./placement-validations.component.scss']
})
export class PlacementValidationsComponent implements OnInit {
  tabDetails = [];
  loggedInUser: UserProfile;
  totalcount: number;

  constructor(private _commonService: CommonHttpService, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private _alertService: AlertService, private _authService: AuthService) { }

  ngOnInit() {
    this.loggedInUser = this._authService.getCurrentUser().user.userprofile;
    this.initTabs();
  }

  initTabs() {
    this.tabDetails = [
        {
            id: 'pending',
            title: 'pending',
            name: 'pending',
            route: 'pending'
        },
        {
          id: 'approved',
          title: 'approved',
          name: 'approved',
          route: 'approved'
      }
    ];
  }
}
