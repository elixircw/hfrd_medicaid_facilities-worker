import { Component, OnInit } from '@angular/core';
import { AuthService, CommonHttpService, AlertService, SessionStorageService, DataStoreService } from '../../../@core/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationRequest, PaginationInfo } from '../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { Observable } from 'rxjs/Observable';
import { PlacementValidationUrlConfig } from './../_constants/placement-validation-url.config';
import { UserProfile, AppUser } from '../../../@core/entities/authDataModel';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'pending-placement',
  templateUrl: './pending-placement.component.html',
  styleUrls: ['./pending-placement.component.scss']
})
export class PendingPlacementComponent implements OnInit {
  placementList = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  pickList: any[];
  selectedPlacement: any;
  placementForm: FormGroup;
  loggedInUser: UserProfile;
  totalcount: number;
  savePlacement: any[];
  bulkReceipt: any[] = [];
  enableBulk: boolean;
  selectTransBulk: boolean;
  dupResponse: any;
  id: any;
  daNumber: any;
  enableSave: boolean;
  placementDis: boolean;
  clientSearch: any;
  userProfile: AppUser;
  isSupervisor: boolean;
  activeModule: any;
  roletype: string;
  // validateChecked: number;


  constructor(private _commonService: CommonHttpService, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private _alertService: AlertService, private _authService: AuthService,
    private _router: Router,
    private _intakeUtils: IntakeUtils,
    private _session: SessionStorageService,
    private _dataStoreService: DataStoreService,
    private _sessionStorage: SessionStorageService) { }

  ngOnInit() {
    this.loadInfo();
    this._authService.dashboardConfig$.subscribe(_ => {
      this.loadInfo();
    });
  }

  loadInfo() {
    this.loggedInUser = this._authService.getCurrentUser().user.userprofile;
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    if (this.activeModule === 'Case Work') {
      this.isSupervisor = false;
      this.roletype = 'CWCW';
    } else if (this.activeModule === 'Approve') {
      this.isSupervisor = true;
      this.roletype = 'CWSP';
    }
    this.userProfile = this._authService.getCurrentUser();
    if (this.userProfile) {
      this.isSupervisor = this.userProfile.role.name === 'apcs' ? true : false;
    }
    this.initPlacementForm();
    this.getPlacementList();
    this.getPlacementValidationType();
    this.paginationInfo.sortColumn = 'validation_id';
    this.paginationInfo.sortBy = 'desc'
  }


  initPlacementForm() {
    this.placementForm = this.formBuilder.group({
      client_id: [null],
      clientname: [null],
      entry_dt: [null],
      exit_dt: [null],
      placement_entry_dt: [null],
      placement_exit_dt: [null],
      placement_id: [null],
      contract_program_nm: [null],
      provider_nm: [null],
      service_nm: [null],
      servicerequestnumber: [null],
      totalcount: [null],
      validation_end_dt: [null],
      validation_start_dt: [null],
      placement_structure_nm: [null],
      validation_month: [null],
      comment_tx: [null],
      cis_id: [null],
      validation_status_cd: [null],
      address: [null]
    });
    this.placementForm.disable();
    this.placementForm.get('comment_tx').enable();
    this.placementForm.get('validation_status_cd').enable();
    this.placementForm.get('validation_status_cd').setValidators(Validators.required);
    this.placementForm.get('validation_status_cd').updateValueAndValidity();
  }
  onSearchClient() {
    this.paginationInfo.pageNumber = 1;
    if (this.clientSearch) {
      this.clientSearch = this.clientSearch;
    } else {
      this.clientSearch = null;
    }
    this.getPlacementList();
  }
  getPlacementList() {
    const source = this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: this.paginationInfo.pageNumber,
          limit: this.paginationInfo.pageSize,
          where: {
            'validationstatus': 'Pending',
            roletypekey: this.roletype,
            client_id: this.clientSearch ? this.clientSearch : null,
            sortcolumn: this.paginationInfo.sortColumn,
            sortorder: this.paginationInfo.sortBy
          },
          method: 'get'
        }),
        PlacementValidationUrlConfig.EndPoint.placement.FcReferalListURL + '?filter'
      ).subscribe(res => {
        if (res) {
          this.placementList = res.data;
          this.dupResponse = res.data;
          this.totalcount = res.count;
          this.placementDis = !(res && res.data && res.data.length > 0);
        }
      });
  }

  pageChanged(pageNumber: number) {
    this.paginationInfo.pageNumber = pageNumber;
    this.getPlacementList();
  }

  getPlacementValidationType() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '146'
      },
      nolimit: true,
      method: 'get'
    }), PlacementValidationUrlConfig.EndPoint.placement.PlacementValidationTypeUrl + '?filter').subscribe(result => {
      this.pickList = result;
    });
  }

  saveValidation() {
    this.savePlacement = [];
    const placement = this.placementForm.getRawValue();
    this._commonService.endpointUrl = PlacementValidationUrlConfig.EndPoint.placement.UpdatePlacementValidation;
    this.savePlacement.push({
      validation_status_cd: placement.validation_status_cd ? 1750 : 1749,
      update_ts: new Date(),
      placement_validation_id: this.selectedPlacement.placement_validation_id

    });
    /* const body = {
       // validation_status_cd: placement.validation_status_cd,
       validation_status_cd: placement.validation_status_cd ?  1750 : 1749,
       // validation_status_cd: this.validateCheck,
       update_ts: new Date(),
       this.selectedPlacement.placement_validation_id

     }; */
    this._commonService.create({ placement: this.savePlacement }).subscribe(result => {
      this._alertService.success('Placement Updated Successfully.');
      this.getPlacementList();
      (<any>$('#placement-view')).modal('hide');
    });
  }


  editPlacement(item: any) {
    this.id = item.servicecaseid;
    this.daNumber = item.case_id;
    this.selectedPlacement = item;
    this.placementForm.reset();
    item.validation_status_cd = (item.validation_status_cd === 1750);
    this.placementForm.patchValue(item);
    this.placementForm.patchValue({
      clientname: item.client_first_nm + ' ' + item.client_last_nm
    });
    (<any>$('#placement-view')).modal('show');
  }

  viewPlacementReDirect() {
    (<any>$('#placement-view')).modal('hide');
   // this._intakeUtils.redirectToCase(this.id, this.daNumber, 'edit');
    this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
    if (this.id && this.daNumber) {
      this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, 'servicecase');
    }
    const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + this.id + '/casetype';
    this._commonService.getAll(url).subscribe((response) => {
      const dsdsActionsSummary = response[0];
      if (dsdsActionsSummary) {
        this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
        this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
        const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-placements/list';
        this._router.navigate([currentUrl]);
      }
    });
    // this._router.navigate(['../pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-placements/list']);
  }

  getBulk(event) {
    this.bulkReceipt = [];
    if (event) {
      this.enableBulk = true;
      if (this.dupResponse && this.dupResponse.length) {
        for (let i = 0; i < this.dupResponse.length; i++) {

          this.dupResponse[i].update_ts = new Date();
          (<any>$('#trans-id-' + i)).prop('checked', true);
          this.selectTransBulk = true;
          this.bulkReceipt.push({
            placement_validation_id: this.dupResponse[i].placement_validation_id,
            update_ts: this.dupResponse[i].update_ts,
            validation_status_cd: 1750
          });
        }
      }
    } else {
      this.enableBulk = false;
      this.selectTransBulk = false;
      (<any>$('.trans')).prop('checked', false);

    }
  }

  transChanged(checked, placementdata) {
    const receipt = JSON.parse(JSON.stringify(placementdata));
    const selectedReceipt = (this.bulkReceipt && this.bulkReceipt.length > 0) ? this.bulkReceipt.find(rec => rec.placement_validation_id === receipt.placement_validation_id) : null;
    // receipt.validation_status_cd = placement.validation_status_cd ?  1750 : 1749,
    receipt.update_ts = new Date();
    if (checked) {
      if (!selectedReceipt) {
        this.bulkReceipt.push({
          validation_status_cd: 1750,
          update_ts: new Date(),
          placement_validation_id: placementdata.placement_validation_id ? placementdata.placement_validation_id : null
        });
      }
    } else {
      this.selectTransBulk = false;
      // if (selectedReceipt) {
      this.bulkReceipt = this.bulkReceipt.filter(rec => rec.placement_validation_id !== receipt.placement_validation_id);
      // }
    }
    this.enableBulk = !!this.bulkReceipt.length;
    const receptlist = this.placementList;
    if (this.bulkReceipt.length !== 0) {
      const bulkcheckboxselect = (receptlist.length === this.bulkReceipt.length);
      if (bulkcheckboxselect) {
        this.selectTransBulk = true;
      } else {
        this.selectTransBulk = false;
      }
    } else {
      this.selectTransBulk = false;
    }
  }

  saveBulkValidation() {
    this._commonService.endpointUrl = PlacementValidationUrlConfig.EndPoint.placement.UpdatePlacementValidation;
    this._commonService.create({ placement: this.bulkReceipt }).subscribe(
      (response) => {
        this._alertService.success('Selected Placement Validation Saved Successfully');
        this.getPlacementList();
        this.selectTransBulk = false;
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  validateCheck(checked) {
    if (checked === true) {
      this.enableSave = true;
    } else {
      this.enableSave = false;
    }
  }

  onChildListSort($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.getPlacementList();
}
}
