import { Component, OnInit, Input } from '@angular/core';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';
import { IntakeStore } from '../../_utils/intake-utils.service';
import { FormGroup, FormBuilder, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { PaginationInfo } from '../../../@core/entities/common.entities';
import { Observable, Subject } from 'rxjs';
import { CaseSearchService } from '../case-search.service';
import { CommonHttpService, DataStoreService, AlertService, SessionStorageService, AuthService } from '../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { DSDSActionDetails } from '../../case-worker/_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { AppConstants } from '../../../@core/common/constants';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';

@Component({
  selector: 'case-search-list',
  templateUrl: './case-search-list.component.html',
  styleUrls: ['./case-search-list.component.scss']
})
export class CaseSearchListComponent implements OnInit {

  servicerequestnumber: any = '';
  caseTypeInp: any = '';
  searchResult: any = [];
  caseWorker: any;
  caseType: any;
  myirForm: FormGroup;
  caseSearchForm: FormGroup;
  @Input() paginationInfo: PaginationInfo;
  caseList$: Observable<any[]>;
  totalRecords$: Observable<number>;
  canDisplayPager$: Observable<boolean>;
  pageStream$ = new Subject<number>();
  caseworkerList: any[] = [];
  disableFind: boolean;
  isAudtLogVisible = false;
  constructor(private _caseSearchService: CaseSearchService,
    private _formBuilder: FormBuilder,
    private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _session: SessionStorageService,
    private _authService: AuthService) { this.paginationInfo = new PaginationInfo(); }

  ngOnInit() {
    this.isAudtLogVisible = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this.caseType = 'Empty';
    this.getCaseWorkerList();
    this.myirForm = this._formBuilder.group({
      filterIR: ['All']
    });
    this.caseSearchForm = this._formBuilder.group({
      caseWorker: null,
      servicerequestnumber: null,
      caseTypeInp: null
    }, { validator: this.atLeastOne(Validators.required) });
    this.pageStream$.subscribe(data => {
      this.paginationInfo.pageNumber = data;
    });
    // this.triggerCaseSearch(1);
    this.caseTypeInp = '';
    this.caseWorker = null;
    this.loadFormCache();
  }

  atLeastOne = (validator: ValidatorFn) => (
    group: FormGroup,
  ): ValidationErrors | null => {
    const hasAtLeastOne = group && group.controls && Object.keys(group.controls)
      .some(k => !validator(group.controls[k]));
    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo;
    this.paginationInfo.pageSize = 10;
    this.triggerCaseSearch(pageInfo);
    // this.pageStream$.next(this.paginationInfo.pageNumber);
  }

  resetSearchResult() {
    this.searchResult = null;
    this.caseType = null;
  }

  triggerCaseSearch(page) {
    // this.searchResult = [];
    if (this.caseSearchForm.valid) {
      const reqData = {
        'actiontype': this.caseTypeInp,
        'servicerequestnumber': this.servicerequestnumber.trim(), 'workername': this.caseWorker
      };
      this._commonService
        .getSingle(
          {

            limit: 10,
            sortorder: this.paginationInfo.sortBy,
            sortcolumn: this.paginationInfo.sortColumn,
            page: page,
            count: -1,
            where: reqData,
            method: 'get'
          },
          'servicerequestsearches/myCaseSearch?data'
        ).subscribe(data => {
          if (data && data.count) {
            this.totalRecords$ = (page === 1) ? Observable.of(data.count) : this.totalRecords$;
          }
          if (data && data.data) {
            this.searchResult = data.data;
            this.disableFind = false;
            // const list = (Array.isArray(data.data)) ? data.data : [];
            // list.forEach(element => {
            //   element.caseType = 'CPS-AR';
            //   this.searchResult.push(element);
            // });
            this.caseList$ = Observable.of(this.searchResult);
          }
        });
    }
  }


  tagCase(caseInfo) {
      const request = {
      objectid: caseInfo.intakeserviceid,
      objecttypekey: caseInfo.srtype,
      casenumber: caseInfo.servicerequestnumber,
      legalguardian: caseInfo.legalguardian && caseInfo.legalguardian.length ? caseInfo.legalguardian[0].personname : null ,
      worker: caseInfo.workername
    };
    
  

    this._commonService.create(request, 'userreference/addreference').subscribe((item) => {
      if (item) {
        this._alertService.success(item);
      }
    });
    this.triggerCaseSearch(1);

  }

  clear() {
    this.servicerequestnumber = '';
    this.caseTypeInp = '';
    this.caseWorker = null;
    this.saveSearchRequest();
    this.searchResult = [];
    this.totalRecords$ = Observable.of(0);
  }

  getCaseWorkerList() {
    this._commonService
      .getPagedArrayList(
        {
          where: { appevent: 'ALL' },
          method: 'post'
        },
        'Intakedastagings/getroutingusers'
      ).subscribe(result => {
        this.caseworkerList = result.data;
      });
  }

  onSearchCase() {
    const caseId = this.servicerequestnumber;
    if (!caseId) {
      return;
    }
    this.searchResult = [];
    this._caseSearchService.searchCase(caseId, 'AR', 'servicerequestsearches/usersservicerequestwithrestricteduser?data').subscribe(ardata => {
      console.log('AR Search', ardata);
      if (ardata && ardata.count) {
        ardata.data.forEach(element => {
          element.caseType = 'CPS-AR';
          this.searchResult.push(element);
        });
        //  this.searchResult = ardata;
        //  this.caseType = 'CPS-AR';
        //  console.log('Search Result', this.searchResult);
        // return;
      } else {
        // this.resetSearchResult();
      }
    });
    this._caseSearchService.searchCase(caseId, 'servicecase', 'servicerequestsearches/getservicecase?data').subscribe(scdata => {
      console.log('Service Search', scdata);
      if (scdata && scdata.count) {
        scdata.data.forEach(element => {
          element.caseType = 'Service Case';

          this.searchResult.push(element);
        });
        //  this.searchResult =  scdata;
        //  this.caseType = 'Service Case';
        //  console.log('Search Result', this.searchResult);
        // return;
      } else {
        // this.resetSearchResult();
      }
    });
    this._caseSearchService.searchCase(caseId, null, 'servicerequestsearches/usersservicerequestwithrestricteduser?data').subscribe(cpsdata => {
      console.log('NON CPS', cpsdata);
      if (cpsdata && cpsdata.count) {
        cpsdata.data.forEach(element => {
          element.caseType = 'NON CPS';

          this.searchResult.push(element);
        });
        //  this.searchResult =  cpsdata;
        //  this.caseType = 'NON CPS';
        //  console.log('Search Result', this.searchResult);
        // return;
      } else {
        // this.resetSearchResult();
      }
    });
    this._caseSearchService.searchCase(caseId, 'IR', 'servicerequestsearches/usersservicerequestwithrestricteduser?data').subscribe(irdata => {
      console.log('IR Search', irdata);
      if (irdata && irdata.count) {
        irdata.data.forEach(element => {
          element.caseType = 'CPS-IR';

          this.searchResult.push(element);
        });
        // this.searchResult = irdata;
        // this.caseType = 'CPS-IR';
        // console.log('Search Result', this.searchResult);
        // return;
      } else {
        // this.resetSearchResult();
      }
    });


    this._caseSearchService.searchCase(caseId, 'servicecase', 'servicerequestsearches/getadoptioncase?data').subscribe(adoptiondata => {
      console.log('Adoption', adoptiondata);
      if (adoptiondata && adoptiondata.count) {
        adoptiondata.data.forEach(element => {
          element.caseType = 'Adoption';

          this.searchResult.push(element);
        });
        // this.searchResult = irdata;
        // this.caseType = 'CPS-IR';
        // console.log('Search Result', this.searchResult);
        // return;
      } else {
        // this.resetSearchResult();
      }
    });


    //  this._caseSearchService.searchCase(caseId, 'Adoption',  'servicerequestsearches/getadoptioncase?data').subscribe(adoptiondata => {
    //    console.log('Adoption', cpsdata);
    //    if (adoptiondata && adoptiondata.count) {
    //     this.searchResult =  adoptiondata;
    //     this.caseType = 'Adoption Case';
    //     console.log('Search Result', this.searchResult);
    //     return;
    //    }
    // });
    //      });
    //    });
    //  });
    // });


  }

  routeToCase(item) {
    this.clearSession();
    if (item.srtype === 'Service') {
      this.routToServiceCase(item);
    } else if (item.srtype === 'Adoption') {
      this.routToAdoptionCase(item);
    } else if (item.srtype === 'Referral') {
      this.routToIntake(item);
    } else if (item.srtype === 'CPS AR') {
      this.routToCpsCase(item);
    } else if (item.srtype === 'CPS IR') {
      this.routToCpsCase(item);
    }
  }

  routToServiceCase(item: DSDSActionDetails) {
    this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
    if (item) {
      this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, item.objecttypekey);
    }
    const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.caseid + '/casetype';
    this._commonService.getAll(url).subscribe((response) => {
      const dsdsActionsSummary = response[0];
      if (dsdsActionsSummary) {
        this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
        this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
        const currentUrl = '#/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/report-summary';
        // this._router.navigate([currentUrl]);
        window.open(currentUrl);
      }
    });
  }

  routToCpsCase(item: DSDSActionDetails) {
    this._commonService
      .getSingle(
        {
          intakeserviceid: item.intakeserviceid,
          isaccepted: true,
          isrejected: false,
          rejectreason: 'Accepted',
          method: 'post'
        },
        'Areateammemberservicerequests/updateassignedstatus'
      )
      .subscribe(
        (result) => {
          // this._alertService.success('Accepted Successfully');
          // this.getIR(1, this.currentStatus);
          this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
              this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
              this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
              const currentUrl = '#/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/report-summary';
              // this._router.navigate([currentUrl]);
              window.open(currentUrl);
            }
          });
        },
        (error) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
  }

  routToAdoptionCase(item: DSDSActionDetails) {
    this._session.setItem(CASE_STORE_CONSTANTS.CASE_TYPE, CASE_TYPE_CONSTANTS.ADOPTION);
    this._session.setItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID, item.adoptionplanningid);
    this._session.setItem(CASE_STORE_CONSTANTS.Adoption_START_DATE, item.startdate);
    this._session.setObj(CASE_STORE_CONSTANTS.CASE_DASHBOARD, item);
    if (item) {
      this._dataStoreService.setData(CASE_STORE_CONSTANTS.CASE_DASHBOARD, item);
    }
    this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
      const dsdsActionsSummary = response[0];
      if (dsdsActionsSummary) {
        this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
        this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
        // TODO : DEB As discussed with management we are parking this link but we are working on separate branch after extensive testing this will
        // be available for user
        const currentUrl = '#/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/adoption-persons';
        // this._router.navigate([currentUrl]);
        window.open(currentUrl);
      }
    });
  }

  routToIntake(item: any) {
    this._session.removeItem('intake');
    const intake = Object.create(IntakeStore);
    intake.number = item.servicerequestnumber;
    intake.action = 'edit';
    this._session.setObj('intake', intake);
   // const url = '/pages/newintake/my-newintake/';
    this._dataStoreService.clearStore();
   // this._router.navigate([url], { skipLocationChange: true });
    window.open('#/pages/newintake/my-newintake');
  }

  clearSession() {
    this._session.removeItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    this._session.removeItem('intake');
    this._session.removeItem(CASE_STORE_CONSTANTS.CASE_TYPE);
    this._session.removeItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    this._session.removeItem(CASE_STORE_CONSTANTS.Adoption_START_DATE);
  }

  openAuditLog(item) {
    const caseId = item.intakeserviceid;
    this._session.setObj('caseSearchItem', item);
    this.saveSearchRequest();
    this._router.navigate(['../case-log/' + caseId], { relativeTo: this.route });
  }

  saveSearchRequest() {
    const caseSearchRequest = {
      servicerequestnumber: this.servicerequestnumber,
      caseWorker: this.caseWorker,
      caseTypeInp: this.caseTypeInp
    };
    this._session.setObj('caseSearchRequest', caseSearchRequest);
  }

  loadFormCache() {
    const caseSearchRequest = this._session.getObj('caseSearchRequest');
    if (caseSearchRequest) {
      this.servicerequestnumber = caseSearchRequest.servicerequestnumber;
      this.caseWorker = caseSearchRequest.caseWorker;
      this.caseTypeInp = caseSearchRequest.caseTypeInp;
      this.triggerCaseSearch(1);
    }
  }

  onSorted($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.triggerCaseSearch(1);
}

}
