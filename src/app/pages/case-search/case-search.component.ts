import { Component, OnInit, Input } from '@angular/core';
import { CaseSearchService } from './case-search.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, DataStoreService, AlertService, SessionStorageService } from '../../@core/services';
import { DSDSActionDetails } from '../case-worker/_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../case-worker/case-worker-url.config';
import { Router } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../@core/entities/constants';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../case-worker/_entities/caseworker.data.constants';
import { PaginationInfo } from '../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

export class IntakeStore {
  number: string;
  action: string;
}

@Component({
  selector: 'case-search',
  templateUrl: './case-search.component.html',
  styleUrls: ['./case-search.component.scss']
})
export class CaseSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }


}
