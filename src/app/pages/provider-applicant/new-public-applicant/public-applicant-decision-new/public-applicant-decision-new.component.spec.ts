import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantDecisionNewComponent } from './public-applicant-decision-new.component';

describe('PublicApplicantDecisionNewComponent', () => {
  let component: PublicApplicantDecisionNewComponent;
  let fixture: ComponentFixture<PublicApplicantDecisionNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantDecisionNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantDecisionNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
