import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApplicationDecision } from '../_entities/newApplicantModel';
import { AuthService, CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { RoutingUser } from '../_entities/existingreferralModel';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as moment from 'moment';
const CPS_OS_ERROR_MESSAGE = 'Director‘s Approval details is required for CPS history/ Out of State CPS history';
const LEAST_REFERECE_COUNT = 3;
const FACE_TO_FACE_COUNT = 2;
const NON_RELATIVE_COUNT = 2;
@Component({
  selector: 'public-applicant-decision-new',
  templateUrl: './public-applicant-decision-new.component.html',
  styleUrls: ['./public-applicant-decision-new.component.scss']
})
export class PublicApplicantDecisionNewComponent implements OnInit {
  decisionFormGroup: FormGroup;
  currentDecision: ApplicationDecision;
  roleId: AppUser;
  decisions = [];
  user: AppUser;
  applicantId: string;
  cpsErrorMessage = CPS_OS_ERROR_MESSAGE;

  // Assigning / Routing
  getUsersList: RoutingUser[];
  // originalUserList: RoutingUser[];
  selectedPerson: any;
  eventCode: string;
  assignedUsersLog: any[] = [];

  // Flags
  isSupervisor: boolean;
  isGroup = false;
  isFinalDecisoin: boolean;
  inapplicationphase: boolean;

  //statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  isApplicationPhase: boolean;
  personsBGChecks = [];
  providerApplicationData: any;
  approveResponse: any = [];

  referenceInfo = [];
  hasSatisfiedValidCount = false;
  hasFacetoFaceValidCount = false;
  hasNonRelativeValidCount = false;
  NonRepetingArray: any[] = [];
  PrideDuration = 0;
  providerProgramType: any;
  recommendationDropDowns = [];
  acknowledgementMsg: string;
  isProvisionApprove: boolean = false;
  homeStudyVisitInfo: any[] = [];
  allStatusDropdownItems =  [
    { 'text': 'Accept', 'value': 'Accepted', 'routingstatustypeid': 600 },
    { 'text': 'Approve', 'value': 'Approved', 'routingstatustypeid': 601 },
    { 'text': 'Reject', 'value': 'Rejected', 'routingstatustypeid': 602 },
    { 'text': 'Return', 'value': 'Incomplete', 'routingstatustypeid': 603 },
    { 'text': 'Close', 'value': 'Closed', 'routingstatustypeid': 604 },
    { 'text': 'Submit Application and Continue', 'value': 'Submitted', 'routingstatustypeid': 605 },
    { 'text': 'Submit for Approval', 'value': 'Approval Submitted', 'routingstatustypeid': 606 },
    { 'text': 'Provisionally Accept', 'value': 'Provisionally Accepted', 'routingstatustypeid': 607 },
    { 'text': 'Provisionally Approve', 'value': 'Provisionally Approved', 'routingstatustypeid': 608 },
  ];
  canproviderapproval: boolean;
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService
  ) {
    this.applicantId = route.parent.snapshot.params['id'];
    this.providerApplicationData = route.parent.snapshot.data.appData;
    this.inapplicationphase = this._dataStoreService.getData('inapplicationphase');
  }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    this.initializeDecisionForm();

    this.roleId = this._authService.getCurrentUser();
    
    const resourcePermission = this._dataStoreService.getData('PERMISSION');
    this.canproviderapproval = (resourcePermission.filter(data => data.name === 'manage_provider_approval').length > 0);
   
    this.providerProgramType = this._dataStoreService.getData('providerProgramType');
    if (this.inapplicationphase === false && this.user.role.name !== 'LDSS_SUPERVISOR' && this.canproviderapproval === false) {
      this.statusDropdownItems = [
        /*   { 'text': 'Deny Application and Close', 'value': 'Rejected' }, */
        { 'text': 'Submit Application and Continue', 'value': 'Submitted', 'routingstatustypeid': 605 },
      ];
      this.recommendationDropDowns = [
        { 'text': 'Accept', 'value': 'Accepted', 'routingstatustypeid': 600 },
        { 'text': 'Reject', 'value': 'Rejected', 'routingstatustypeid': 602 },
        { 'text': 'Return', 'value': 'Incomplete', 'routingstatustypeid': 603 },
        { 'text': 'Close', 'value': 'Closed', 'routingstatustypeid': 604 },
      ];
      if (this.providerProgramType === 'Restricted Home') {
        this.statusDropdownItems.push({ 'text': 'Provisionally Accept', 'value': 'Provisionally Accepted', 'routingstatustypeid': 607 });
      }
    } else if (this.inapplicationphase === false && (this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true )) {
      this.statusDropdownItems = [
        { 'text': 'Accept', 'value': 'Accepted', 'routingstatustypeid': 600 },
        { 'text': 'Reject', 'value': 'Rejected', 'routingstatustypeid': 602 },
        { 'text': 'Return', 'value': 'Incomplete', 'routingstatustypeid': 603 },
        { 'text': 'Close', 'value': 'Closed', 'routingstatustypeid': 604 },
      ];

      if (this.providerProgramType === 'Restricted Home') {
        this.statusDropdownItems.push({ 'text': 'Provisionally Approve', 'value': 'Provisionally Approved', 'routingstatustypeid': 608 });
      }

    } else if (this.inapplicationphase === true && this.user.role.name !== 'LDSS_SUPERVISOR' &&  this.canproviderapproval === false) {
      this.statusDropdownItems = [
        { 'text': 'Submit for Approval', 'value': 'Approval Submitted', 'routingstatustypeid': 606 }
      ];

      this.recommendationDropDowns = [
        { 'text': 'Approve', 'value': 'Approved', 'routingstatustypeid': 601 },
        { 'text': 'Provisionally Approve', 'value': 'Provisionally Approved', 'routingstatustypeid': 608 },
        { 'text': 'Reject', 'value': 'Rejected', 'routingstatustypeid': 602 },
        { 'text': 'Return', 'value': 'Incomplete', 'routingstatustypeid': 603 },
        { 'text': 'Close', 'value': 'Closed', 'routingstatustypeid': 604 },
      ];

    } else if (this.inapplicationphase === true && (this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true)) {
      this.statusDropdownItems = [
        { 'text': 'Approve', 'value': 'Approved', 'routingstatustypeid': 601 },
        { 'text': 'Provisionally Approve', 'value': 'Provisionally Approved', 'routingstatustypeid': 608 },
        { 'text': 'Reject', 'value': 'Rejected', 'routingstatustypeid': 602 },
        { 'text': 'Return', 'value': 'Incomplete', 'routingstatustypeid': 603 },
        { 'text': 'Close', 'value': 'Closed', 'routingstatustypeid': 604 },
      ];
    }
   
    console.log(this.statusDropdownItems);
    //this.roleId.user.userprofile.teamtypekey;

    if (this.roleId.role.name == 'Executive Director') {
      this.isFinalDecisoin = true;
    } else {
      this.isFinalDecisoin = false;
    }

    this.getDecisions();
    this.getAssignedUsersLog();
    this.loadPersonBGAndHouseHolds();
    this.getRefrenceChecks();
    this.getHomeStudyVisits();
    this.getassignedTrainingSessions();
    this.isApplicationPhase = this._dataStoreService.getData('inapplicationphase');
  }

  checkForCPSHistoryValidation(backgroundCheckInfo) {
    let cpsValidation = false;
    if (backgroundCheckInfo) {
      if (backgroundCheckInfo.submissiondata.childProtectiveServ.mainQuestion === '1' &&
        backgroundCheckInfo.submissiondata.childProtectiveServ.approval_info.directorsApproval === '1' &&
        backgroundCheckInfo.submissiondata.childProtectiveServ.approval_info.approval_date) {
        cpsValidation = true;
      } else if (backgroundCheckInfo.submissiondata.childProtectiveServ.mainQuestion === '0') {
        cpsValidation = true;
      }

    }
    return cpsValidation;

  }

  checkForOutOfStateValidation(backgroundCheckInfo) {
    let oosValidation = true;
    if (backgroundCheckInfo && backgroundCheckInfo.submissiondata.outOfStateClearences.length) {
      if (backgroundCheckInfo.submissiondata.outOfStateClearences[0].mainQuestion === '1') {
          if ((!backgroundCheckInfo.submissiondata.outOfStateClearences[0].approval_info.directorsApproval) ||
            (backgroundCheckInfo.submissiondata.outOfStateClearences[0].approval_info.directorsApproval === '1' &&
            !backgroundCheckInfo.submissiondata.outOfStateClearences[0].approval_info.approval_date)) {
            oosValidation = false;
          }
      }
    }
    return oosValidation;

  }

  getCPSHistoryInfo(backgroundCheckInfo) {
    let cpsInfo = 'N/A';
    if (backgroundCheckInfo) {
      if (backgroundCheckInfo.submissiondata.childProtectiveServ.mainQuestion === '1') {
        cpsInfo = 'Yes';
      } else if (backgroundCheckInfo.submissiondata.childProtectiveServ.mainQuestion === '0') {
        cpsInfo = 'No';
      }

    }
    return cpsInfo;

  }

  getOutOfStateInfo(backgroundCheckInfo) {
    let outOfStateInfo = 'N/A';
    if (backgroundCheckInfo && backgroundCheckInfo.submissiondata.outOfStateClearences.length) {
      if (backgroundCheckInfo.submissiondata.outOfStateClearences[0].mainQuestion === '1') {
        outOfStateInfo = 'Yes';
      } else if (backgroundCheckInfo.submissiondata.outOfStateClearences[0].mainQuestion === '0') {
        outOfStateInfo = 'No';
      }

    }
    return outOfStateInfo;
  }

  getCPSHistoryApprovalInfo(backgroundCheckInfo) {
    let cpsInfo = 'N/A';
    if (backgroundCheckInfo) {
      if (backgroundCheckInfo.submissiondata.childProtectiveServ.mainQuestion === '1' &&
        backgroundCheckInfo.submissiondata.childProtectiveServ.approval_info.directorsApproval === '1') {
        cpsInfo = 'Yes';
      } else if (backgroundCheckInfo.submissiondata.childProtectiveServ.mainQuestion === '1' &&
        backgroundCheckInfo.submissiondata.childProtectiveServ.approval_info.directorsApproval === '0') {
        cpsInfo = 'No';
      }

    }
    return cpsInfo;

  }

  getOutOfStatApprovalInfo(backgroundCheckInfo) {
    let outOfStateInfo = 'N/A';
    if (backgroundCheckInfo && backgroundCheckInfo.submissiondata.outOfStateClearences.length) {
      if ( backgroundCheckInfo.submissiondata.outOfStateClearences[0].mainQuestion === '1' &&
           backgroundCheckInfo.submissiondata.outOfStateClearences[0].approval_info.directorsApproval === '1') {
        outOfStateInfo = 'Yes';
      } else if (backgroundCheckInfo.submissiondata.outOfStateClearences[0].mainQuestion === '1' &&
        backgroundCheckInfo.submissiondata.outOfStateClearences[0].approval_info.directorsApproval === '0') {
        outOfStateInfo = 'No';
      }

    }
    return outOfStateInfo;
  }

  loadPersonBGAndHouseHolds() {
    forkJoin([this.getHouseHolds(), this.getPersonBGInfo()]).subscribe(([houseHolds, bgChecks]) => {
      console.log('households', houseHolds);
      console.log('bg', bgChecks);
      if (houseHolds && houseHolds.data && houseHolds.data.length) {
        this.personsBGChecks = houseHolds.data.map(houseHold => {
          const houseHoldInfo = houseHold;
          houseHoldInfo.bgCheck = bgChecks.find(bgCheck => bgCheck.personid === houseHold.personid);

          houseHoldInfo.CPSHistoryInfo = 'N/A';
          houseHoldInfo.cpsHistoryValidated = false;
          houseHoldInfo.outOfStateInfo = 'N/A';
          houseHoldInfo.outOfStateValidated = true;

          if (houseHoldInfo.bgCheck) {
            houseHoldInfo.cpsHistoryValidated = this.checkForCPSHistoryValidation(houseHoldInfo.bgCheck);
            houseHoldInfo.CPSHistoryInfo = this.getCPSHistoryInfo(houseHoldInfo.bgCheck);
            houseHoldInfo.cpsApprovalInfo = this.getCPSHistoryApprovalInfo(houseHoldInfo.bgCheck);
            houseHoldInfo.CPSHistoryApprovalDate = (houseHoldInfo.bgCheck.submissiondata.childProtectiveServ) ?
                                                  houseHoldInfo.bgCheck.submissiondata.childProtectiveServ.approval_info.approval_date : null;
            houseHoldInfo.outOfStateValidated = this.checkForOutOfStateValidation(houseHoldInfo.bgCheck);
            houseHoldInfo.outOfStateInfo = this.getOutOfStateInfo(houseHoldInfo.bgCheck);
            houseHoldInfo.outOfStateApprovalInfo = this.getOutOfStatApprovalInfo(houseHoldInfo.bgCheck);
            houseHoldInfo.outOfStateApprovalDate = null;
            if (houseHoldInfo.bgCheck.submissiondata.outOfStateClearences.length) {
              houseHoldInfo.outOfStateApprovalDate = houseHoldInfo.bgCheck.submissiondata.outOfStateClearences[0].approval_info.approval_date;
            }
          }
          return houseHoldInfo;
        });
        console.log('pbc', this.personsBGChecks);
      }
    });
  }

  initializeDecisionForm() {
    this.decisionFormGroup = this.formBuilder.group({
      status: [''],
      reason: [''],
      home_study_completion_date: [''],
      home_study_completed_by: [''],
      provisionstrtdate: [''],
      provisionenddate: [''],
      recommendation_status: ['']
    });
    this.decisionFormGroup.patchValue({
      home_study_completed_by: this.user.user.username
    });
    console.log(this.providerApplicationData);
    if (this.providerApplicationData) {
      this.decisionFormGroup.patchValue({
        home_study_completion_date: this.providerApplicationData.home_study_completion_date,
        home_study_completed_by: (this.providerApplicationData.home_study_completed_by) ? this.providerApplicationData.home_study_completed_by : this.user.user.username,
        provisionstrtdate: (this.providerApplicationData.provisionstrtdate) ? this.providerApplicationData.provisionstrtdate : null,
        provisionenddate: (this.providerApplicationData.provisionenddate) ? this.providerApplicationData.provisionenddate : null
      });
    }

  }

  getPersonBGInfo() {
    return this._commonHttpService.getSingle(
      {
        method: 'get',
        nolimit: true,
        where: {
          objectid: this.applicantId
        }
      },
      'pubprovapphouseholdbgchecks?filter'
    );
  }

  getHouseHolds() {

    const inputRequest = {
      intakenumber: this.applicantId
    };
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: inputRequest
        }),
        'People/getpersondetailcw?filter'
      );


  }

  checkForValidation() {
    this.hasFacetoFaceValidCount = this.getValidFaceToRefferences().length >= FACE_TO_FACE_COUNT;
    this.hasNonRelativeValidCount = this.getNonRelativeRefferences().length >= NON_RELATIVE_COUNT;
    this.hasSatisfiedValidCount = this.getValidRefferences().length >= LEAST_REFERECE_COUNT;
  }

  getValidRefferences() {
    return this.referenceInfo.filter(item => !item.is_school_recommends);
  }

  getValidFaceToRefferences() {
    return this.getValidRefferences().filter(item => item.type_of_contact === 'Face-to-face')
  }

  getNonRelativeRefferences() {
    return this.getValidRefferences().filter(item => !item.is_relative);
  }

  private getRefrenceChecks() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: { object_id: this.applicantId }
      },
      'publicproviderreferencecheck?filter'
    ).subscribe(response => {
      this.referenceInfo = response;
      this.checkForValidation();

    });
  }
  private getHomeStudyVisits() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: { object_id: this.applicantId }
      },
      'publicproviderhomestudyhouseholdmapping/gethomestudyhousehold'
    ).subscribe(response => {
      this.homeStudyVisitInfo = response;
      console.log('this.homeStudyVisitInfo', this.homeStudyVisitInfo);
    });
  }

  getDecisions() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          eventcode: 'PTA',
          objectid: this.applicantId
        }
      },
      'providerapplicant/gettierdecisions'
      //ApplicantUrlConfig.EndPoint.Applicant.getappointmentdetails
    ).subscribe(decisionsarray => {
      this.decisions = decisionsarray.data;
      console.log('Public Provider Tier Decisions')
      console.log(JSON.stringify(this.decisions));
    },
      (error) => {
        this._alertService.error('Unable to get decisions, please try again.');
        console.log('get decisions Error', error);
        return false;
      }
    );
  }


  // listUser(assigned: string) {
  //   this.selectedPerson = '';
  //   this.getUsersList = [];
  //   this.getUsersList = this.originalUserList;
  //   if (assigned === 'TOBEASSIGNED') {
  //     this.getUsersList = this.getUsersList.filter(res => {
  //       if (res.issupervisor === false) {
  //         this.isSupervisor = false;
  //         return res;
  //       }
  //     });
  //   }
  // }

  // getRoutingUser() {
  //   //this.getServicereqid = modal.provider_referral_id;
  // //  this.getServicereqid = 'R201800200374';
  //   // console.log(modal.provider_referral_id);
  //   // this.zipCode = modal.incidentlocation;
  //   this.getUsersList = [];
  //   this.originalUserList = [];
  //   // this.isGroup = modal.isgroup;
  //   this._commonHttpService
  //     .getPagedArrayList(
  //       new PaginationRequest({
  //         where: { appevent: 'PTA' },
  //         method: 'post'
  //       }),
  //       'Intakedastagings/getroutingusers'
  //     )
  //     .subscribe(result => {
  //       this.getUsersList = result.data;
  //       this.originalUserList = this.getUsersList;
  //       this.listUser('TOBEASSIGNED');
  //     });
  // }

  // assignUser() {
  //   // Doing the update of Status and assigning at the same time
  //   // In the same flow, first 'Submit' then 'Assign' will do them both
  //   // TODO: SIMAR - Put validations that status value has been selected before submitting
  //   console.log('In assignUser');
  //   console.log(this.isFinalDecisoin);

  //   if (this.selectedPerson) {
  //     this._commonHttpService
  //       .getPagedArrayList(
  //         new PaginationRequest({
  //           where: {
  //             eventcode: 'PTA',
  //             objectid: this.applicantId,
  //             fromroleid: this.roleId.role.name,
  //             tosecurityusersid: this.selectedPerson.userid,
  //             toroleid: this.selectedPerson.userrole,
  //             remarks: this.decisionFormGroup.value.reason, //.get('reason'),
  //             status: this.decisionFormGroup.value.status, //.get('status')
  //             isfinalapproval: this.isFinalDecisoin
  //           },
  //           method: 'post'
  //         }),
  //         'providerapplicant/submitdecision'
  //         //'Providerreferral/routereferralda'
  //       )
  //       .subscribe(result => {
  //         console.log('Inside subscribe', result);
  //         this._alertService.success('Application assigned successfully!');
  //       //  this.getAssignCase(1, false);
  //         this.closePopup();

  //         //Reset the decision table and get all new results
  //         this.decisions.length = 0;
  //         this.getDecisions();
  //       });
  //   } else {
  //     this._alertService.warn('Please select a person');
  //   }
  // }


  submitDecision() {
    let decision = this.decisionFormGroup.getRawValue().status;
    let recommendStatus = this.decisionFormGroup.getRawValue().recommendation_status;
    const reason = this.decisionFormGroup.getRawValue().reason;
    var payload = {}

    // Do the setting up as before
    if (this.user.role.name !== 'LDSS_SUPERVISOR' &&  this.canproviderapproval !== true) {
     // if (this.isApplicationPhase) {
        decision = 'For Acceptance';
        if (this.decisionFormGroup.getRawValue().status === 'Rejected' || recommendStatus === 'Rejected') {
          decision = 'For Rejection';
        }
      // } else {
      //   decision = 'For Acceptance';
      // }
    }
    payload['application_status'] = decision;
    payload['application_decision'] = decision;
    payload['applicant_id'] = this.applicantId;
    payload['reason'] = reason;
    payload['recommendation_status'] = this.decisionFormGroup.getRawValue().recommendation_status;
    if (this.isApplicationPhase) {
      const home_study_completion_date = this.decisionFormGroup.get('home_study_completion_date').value;
      const home_study_completed_by = this.decisionFormGroup.get('home_study_completed_by').value;
      payload['home_study_completion_date'] = home_study_completion_date;
      payload['home_study_completed_by'] = home_study_completed_by;
    }

    if (payload['recommendation_status'] === 'Provisionally Approved' ||
      this.decisionFormGroup.getRawValue().status === 'Provisionally Approved') {
      payload['provisionenddate'] = this.decisionFormGroup.get('provisionenddate').value;
      payload['provisionstrtdate'] = this.decisionFormGroup.get('provisionstrtdate').value;
    }

    if ((this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true) && this.inapplicationphase === false ) {
      if (this.decisionFormGroup.getRawValue().status === 'Accepted') {
        payload['phase'] = 'Application';
      } else {
        payload['phase'] = 'Pre-App';
      }
    }

    this._commonHttpService.patch(
      '',
      payload,
      'publicproviderapplicant'
    ).subscribe(
      (response) => {
        this._alertService.success('Application Submitted successfully!');
      },
      (error) => {
        this._alertService.error('Unable to submit application');
      }

    );

    // Final decision from Executive director is Approved
    if (this.inapplicationphase === true && (decision === 'Approved'
        || decision === 'Provisionally Approved' || decision === 'Rejected')) {

      this._commonHttpService.create(
        payload,
        'publicproviderapplicant/approvepublicproviderapplication')
        .subscribe(
          (response) => {
            this._alertService.success('Application approved successfully!');
            (<any>$('#approve-application-ackmt')).modal('show');
            if (response && Array.isArray(response)) {
              this.approveResponse = response;
            }

          },
          (error) => {
            this._alertService.error('Unable to approve application');
          }
        );
    }

    if ((this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true ) && this.decisionFormGroup.getRawValue().status === 'Rejected') {
      this.returnToWorker();
    }
  }

  returnToWorker() {
    let lastDecisionData = '';
    if (this.decisions && this.decisions.length) {
      lastDecisionData = this.decisions[0].tosecurityusersid;
    }

    var payload = {
      eventcode: 'PRASS',
      tosecurityusersid: lastDecisionData,
      objectid: this.applicantId,
      typeofobj: this.inapplicationphase ? 'Application' : 'Pre-App',
      status: 'Rejected',
      comments: this.decisionFormGroup.get('reason').value,
      routingstatustypeid: 602,
    }
    this._commonHttpService.create(
      payload,
      'publicproviderapplicant/publicproviderapplicantrouting'
    ).subscribe(
      (response) => {

        this.acknowledgementMsg = 'Returned Successfully';
        (<any>$('#application-assign-success')).modal('show');

        // this._alertService.success('Returned successfully!');

        // this.getAssignedUsersLog();
        // (<any>$('#intake-caseassignnew')).modal('hide');
        // (<any>$('#application-assign-success')).modal('show');
      },
      (error) => {
        this._alertService.error('Unable to save ownership');
      });
  }

  /**
   * Public provider assignment & Routing
   */
  getRoutingUser(appevent, assigntype: string) {

    (<any>$('#intake-caseassignnew')).modal('show');
    this.eventCode = 'PRASS';
    if (assigntype === 'rw-assign') {
      this.eventCode = 'PRWS';
    }
    if (assigntype === 'hsw-assign') {
      this.eventCode = 'PHSWS';
    }

    this.getUsersList = [];
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: this.eventCode },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.getUsersList = this.getUsersList.filter(
          users => users.userid !== this._authService.getCurrentUser().user.securityusersid
        );

      });

    if ((this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true ) && assigntype === 'SUBMIT-REVIEW') {
      this.eventCode = 'PRWS';
      this.assignNewUser();
    }


  }

  selectPerson(row) {
    this.selectedPerson = row;
  }

  assignNewUser() {
    let routingstatustypeid = null;
    if (this.decisionFormGroup.getRawValue().status !== '') {
      const selectedStatus = this.allStatusDropdownItems.find(status => (status.value === this.decisionFormGroup.getRawValue().status));
      routingstatustypeid = selectedStatus.routingstatustypeid;
    } else {
      const selectedStatus = this.allStatusDropdownItems.find(status => (status.value === this.decisionFormGroup.getRawValue().recommendation_status));
      routingstatustypeid = selectedStatus.routingstatustypeid;
    }
    if (this.selectedPerson) {
      let status = '';
      if (this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true ) {
        status = this.decisionFormGroup.getRawValue().status;
      } else {
        status = this.decisionFormGroup.getRawValue().recommendation_status;
      }
      var payload = {
        eventcode: this.eventCode,
        tosecurityusersid: this.selectedPerson.userid,
        objectid: this.applicantId,
        typeofobj: this.inapplicationphase ? 'Application' : 'Pre-App',
        status: status,
        comments: this.decisionFormGroup.get('reason').value,
        routingstatustypeid: routingstatustypeid
      }
      this._commonHttpService.create(
        payload,
        'publicproviderapplicant/publicproviderapplicantrouting'
      ).subscribe(
        (response) => {
          this._alertService.success('Assignment successful!');
          this.getAssignedUsersLog();
          (<any>$('#intake-caseassignnew')).modal('hide');
          this.acknowledgementMsg = 'Application assigned successfully.';
          (<any>$('#application-assign-success')).modal('show');
        },
        (error) => {
          this._alertService.error('Unable to save ownership');
        });
    }

  }

  /**
   * Assigned users history / log
   */
  getAssignedUsersLog() {
    this._commonHttpService.getArrayList(
      {
        where: {
          requesttype: 'assigneduserslog',
          objectid: this.applicantId
        },

        method: 'post'
      },
      'publicproviderapplicant/getassignedlist'
    ).subscribe(
      (response: any) => {
        this.assignedUsersLog = response.data;
      }
    );

  }

  closePopup() {
    (<any>$('#intake-caseassign')).modal('hide');
    (<any>$('#assign-preintake')).modal('hide');
    (<any>$('#reopen-intake')).modal('hide');
  }

  submitApplication() {
    const bgNotValidUsers = this.personsBGChecks.filter(person => (person.cpsHistoryValidated === false || person.outOfStateValidated === false));
    if (this.decisionFormGroup.invalid) {
      this._alertService.error('Please fill mandatory fields');
      return false;
    }
    const status = this.decisionFormGroup.getRawValue().status;
    const recommendStatus = this.decisionFormGroup.getRawValue().recommendation_status;

    if (this.isApplicationPhase) {

      if (status === 'Approved' || recommendStatus === 'Approved') {
        if (bgNotValidUsers.length) {
          this._alertService.error(CPS_OS_ERROR_MESSAGE);
          return false;
        }
        if (this.homeStudyVisitInfo && this.homeStudyVisitInfo.length === 0) {
          this._alertService.error('Atleast one Home study visit information required');
          return false;
        }
        if (!this.hasSatisfiedValidCount || !this.hasFacetoFaceValidCount || !this.hasNonRelativeValidCount) {
          this._alertService.error('Please validate reference check COMAR Regulations');
          return false;
        }
      }

    }
    if (status === 'Approved' || recommendStatus === 'Approved') {
      if ((this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true)
        && this.inapplicationphase === true
        && this.PrideDuration < 27
      ) {
        this._alertService.error('Applicants need to attend at least 27 hours of PRIDE Training');
        return false;
      }
    }
    this.submitDecision();
    if (this.user.role.name !== 'LDSS_SUPERVISOR' &&  this.canproviderapproval !== true) {
      this.getRoutingUser('', 'SUBMIT-REVIEW');
    }
  }

  approveApplicationAcknowledged() {
    (<any>$('#approve-application-ackmt')).modal('hide');
    this._router.navigate(['/pages/provider-management/existing-provider']);
  }

  getassignedTrainingSessions() {
    this._commonHttpService.create(
      {
        where: {
          object_id: this.applicantId,
          training_type: 'Pre-Service Training'
        },
        method: 'post'
      },
      'providerorientationtrainingattendance/getassignedorientations'

    ).subscribe(trainingList => {
      if (trainingList && Array.isArray(trainingList) && trainingList.length) {
        this.calculateDuration(trainingList);
      }

    },
      (error) => {
        this._alertService.error('Unable to get assigned sessions, please try again.');
        console.log('get contact Error', error);
        return false;
      }
    );
  }
  calculateDuration(trainingList) {
    for (let i = 0; i < trainingList.length; i++) {
      const curr = trainingList[i];
      if (curr.session_type === 'PRIDE' && curr.is_attended) {
        if (!this.NonRepetingArray.includes(curr.session_no)) {
          this.NonRepetingArray.push(curr.session_no);
          const y = parseFloat(curr.duration);
          this.PrideDuration = this.PrideDuration + y;
          console.log('this.PrideDuration', this.NonRepetingArray);
        }
      }
    }

  }

  redirectAssign() {
    this._router.navigate(['/pages/home-dashboard']);
  }

  getStatus(type) {
    let status = '';
    this.isProvisionApprove = false;
    if (type === 'Status') {
      status = this.decisionFormGroup.getRawValue().status;
    } else {
      status = this.decisionFormGroup.getRawValue().recommendation_status;
    }
    if (status === 'Provisionally Approved') {
      this.isProvisionApprove = true;
    }
  }

  endDateCalculate() {
    const startDate = this.decisionFormGroup.getRawValue().provisionstrtdate;
    if (startDate) {
      const endDate = moment(startDate).add(120, 'days').format('YYYY-MM-DD');
      this.decisionFormGroup.patchValue({ provisionenddate: endDate });
    }
  }
}
