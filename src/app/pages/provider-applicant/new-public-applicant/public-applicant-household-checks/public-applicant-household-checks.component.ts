import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, DataStoreService, AlertService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonDropdownsService } from '../../../../@core/services/common-dropdowns.service'
@Component({
  selector: 'pub-app-house-hold-checks',
  templateUrl: './public-applicant-household-checks.component.html',
})
export class PubAppHouseHoldChecksComp implements OnInit {

  bgChecksForm: FormGroup;
  bgCriminalCheckForm: FormGroup;
  household_member_id: string;
  household_bg_id: string;
  states: any[];
  uploadedFiles = [];
  personRole: any;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute, private dropDownService: CommonDropdownsService) {
  }

  ngOnInit() {
    this.personRole = this._dataStoreService.getData('ROLE');
    this.bgChecksForm = this.formBuilder.group({
      mvaClearance: this.getSimpleFormGroup(),
      fiaClearance: this.getSimpleFormGroup(),
      mdSexOffRegClearance: this.getSimpleFormGroup(),
      natSexOffRegClearance: this.getSimpleFormGroup(),
      judSearchClearance: this.getSimpleFormGroup(),
      childSupportClearance: this.getSubQuestionFormGroup(),
      childProtectiveServ: this.getApprovalFormGroup(),
      homeHealthReport: this.getSimpleFormGroup(),
      respiteCare: this.getSimpleFormGroup(),
      outOfStateClearences: new FormArray([]),
    });

    this.bgCriminalCheckForm = this.formBuilder.group({
      mdClearance: this.getSimpleFormGroup(),
      fbiClearance: this.getSimpleFormGroup(),
      outOfStateClearences: new FormArray([]),
      mdClearanceCheck: [''],
      fbiClearanceCheck: ['']
    });

   /*  this.bgChecksForm.valueChanges.subscribe(item =>
      console.log('Background check form ' + item.getRawValue())
    ) */

    this.dropDownService.getStateList().subscribe(item => {
     //  console.log('state code' + item);
      this.states = item;
      // item.forEach(i => console.log(JSON.stringify(i)));
    });
    this.loadPersonBGInfo();
  }

  loadPersonBGInfo() {
    this._commonHttpService.getSingle(
      {
        method: 'get',
        nolimit: true,
        where: { objectid: this._dataStoreService.getData('OBJECT_ID'),
        personid: this._dataStoreService.getData('SELECTED_PERSON_ID') }
      },
      'pubprovapphouseholdbgchecks/getlist?filter'
    ).subscribe(
      (response) => {
        const houseHoldbgChecks = response;
        console.log("Backgroud clearance data:" + JSON.stringify(houseHoldbgChecks));


        if (houseHoldbgChecks.length === 0) {
          this.bgChecksForm.reset();
          this.bgCriminalCheckForm.reset();
          this.uploadedFiles = [];
          // this.houseHoldChecksComp.household_member_id = null;
          const submissionOCControl = <FormArray>this.bgChecksForm.controls.outOfStateClearences;
          submissionOCControl.push(this.createOutOfStateClearance());
          const criminalOCControl = <FormArray>this.bgCriminalCheckForm.controls.outOfStateClearences;
          criminalOCControl.push(this.createOutOfStateClearance());
        } else {
          this.household_bg_id =  houseHoldbgChecks[0].household_bg_id;
          if ( houseHoldbgChecks[0].submission_data) {
            console.log(houseHoldbgChecks[0].submission_data.outOfStateClearences);
            this.bgChecksForm.patchValue({
              fiaClearance: houseHoldbgChecks[0].submission_data.fiaClearance,
              mvaClearance: houseHoldbgChecks[0].submission_data.mvaClearance,
              judSearchClearance: houseHoldbgChecks[0].submission_data.judSearchClearance,
              childProtectiveServ: houseHoldbgChecks[0].submission_data.childProtectiveServ,
              mdSexOffRegClearance: houseHoldbgChecks[0].submission_data.mdSexOffRegClearance,
              childSupportClearance: houseHoldbgChecks[0].submission_data.childSupportClearance,
              natSexOffRegClearance: houseHoldbgChecks[0].submission_data.natSexOffRegClearance,
              // outOfStateClearences: houseHoldbgChecks[0].submission_data.outOfStateClearences
            });
            this.setoutOfStateClearences(houseHoldbgChecks[0].submission_data.outOfStateClearences, 'submission');

            if (this.personRole === 'BCKP') {
              this.bgChecksForm.patchValue({
                homeHealthReport: (houseHoldbgChecks[0].submission_data.homeHealthReport) ? houseHoldbgChecks[0].submission_data.homeHealthReport : null,
                respiteCare: (houseHoldbgChecks[0].submission_data.respiteCare) ? houseHoldbgChecks[0].submission_data.respiteCare : null,
              });
            }
            this.uploadedFiles = houseHoldbgChecks[0].submission_data.uploadedFiles ? houseHoldbgChecks[0].submission_data.uploadedFiles : [] ;
          } else {
            const submissionOCControl = <FormArray>this.bgChecksForm.controls.outOfStateClearences;
            submissionOCControl.push(this.createOutOfStateClearance());
          }

          if (houseHoldbgChecks[0].criminal_history_check_data) {
            this.bgCriminalCheckForm.patchValue({
              mdClearance: houseHoldbgChecks[0].criminal_history_check_data.mdClearance,
              fbiClearance: houseHoldbgChecks[0].criminal_history_check_data.fbiClearance,
              // outOfStateClearences: houseHoldbgChecks[0].criminal_history_check_data.outOfStateClearences,
              mdClearanceCheck: houseHoldbgChecks[0].criminal_history_check_data.mdClearanceCheck,
              fbiClearanceCheck: houseHoldbgChecks[0].criminal_history_check_data.fbiClearanceCheck
            });
            this.setoutOfStateClearences(houseHoldbgChecks[0].criminal_history_check_data.outOfStateClearences, 'criminal');
          } else {
            const criminalOCControl = <FormArray>this.bgCriminalCheckForm.controls.outOfStateClearences;
            criminalOCControl.push(this.createOutOfStateClearance());
          }
        }

      },
      (error) => {
        this._alertService.error('Unable to retrieve Household member bgChecks');
        this.bgChecksForm.reset();
        this.bgCriminalCheckForm.reset();
      }
    );
  }

  setoutOfStateClearences(clearanceList, type) {
    if (clearanceList) {
      let control = (type === 'submission') ? <FormArray>this.bgChecksForm.controls.outOfStateClearences :
                                              <FormArray>this.bgCriminalCheckForm.controls.outOfStateClearences;
      clearanceList.forEach(x => {
        control.push(this.formBuilder.group({
          narrative: x.narrative,
          state: x.state,
          clearance_date: x.clearance_date,
          mainQuestion: x.mainQuestion,
          approval_info: this.formBuilder.group({
            directorsApproval: x.approval_info.directorsApproval,
            uploaded: x.approval_info.uploaded,
            approval_date: x.approval_info.approval_date,
          })
        }));
      });
    }
    // if (controls) {
    //   let osFormArray = new FormArray([])
    //   controls.forEach(data => {
    //     osFormArray.push(this.formBuilder.group({
    //       narrative: data.narrative,
    //       state: data.state,
    //       clearance_date: data.clearance_date,
    //       mainQuestion: data.mainQuestion,
    //       approval_info: this.formBuilder.group({
    //         directorsApproval: data.directorsApproval,
    //         uploaded: data.uploaded,
    //         approval_date: data.approval_date,
    //       })
    //     }));
    //   });
    //   return osFormArray;
    // }
    // return new FormArray([this.createOutOfStateClearance()]);
  }

  getSimpleFormGroup() {
    return this.formBuilder.group({
      narrative: '',
      clearance_date: '',
      mainQuestion: '',
    });
  }

  save() {
    console.log('Form submission ' + this.bgChecksForm.getRawValue());
    const bgFormData = {};
    bgFormData['household_member_id'] = null;
    bgFormData['personid'] = this._dataStoreService.getData('SELECTED_PERSON_ID');
    bgFormData['objectid'] = this._dataStoreService.getData('OBJECT_ID');
    const submissionData = this.bgChecksForm.getRawValue();
    submissionData['uploadedFiles'] = this.uploadedFiles;
    if (submissionData.uploadedFiles) {
      submissionData.uploadedFiles.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }
    bgFormData['submissiondata'] = submissionData;
    if (this.household_bg_id) {
      this._commonHttpService.patch(
        this.household_bg_id,
        bgFormData,
        'pubprovapphouseholdbgchecks'
      ).subscribe(
        (response) => {
          this.household_bg_id = response.household_bg_id;
          console.log('Household member checks updated...' + response);
          this._alertService.success('Household member background checks updated successfully!');
        },
        (error) => {
          this._alertService.error('Unable to update Household member background checks');
        }
      );
    } else {
      this._commonHttpService.create(

        bgFormData,
        'pubprovapphouseholdbgchecks/'
      ).subscribe(
        (response) => {
          this.household_bg_id = response.household_bg_id;
          console.log('Household member checks updated...' + response)
          this._alertService.success('Household member background checks updated successfully!');
        },
        (error) => {
          this._alertService.error('Unable to update Household member background checks');
        }
      );
    }
    (<any>$('#add-household-person-security')).modal('hide');
  }


  saveCriminalHistory() {
    console.log('Form submission ' + this.bgCriminalCheckForm.getRawValue());
    const bgCriminalFormData = {};
    bgCriminalFormData['household_member_id'] = null;
    bgCriminalFormData['personid'] = this._dataStoreService.getData('SELECTED_PERSON_ID');
    bgCriminalFormData['objectid'] = this._dataStoreService.getData('OBJECT_ID');
    bgCriminalFormData['criminalhistorycheckdata'] = this.bgCriminalCheckForm.value;
    if (this.household_bg_id) {
      this._commonHttpService.patch(
        this.household_bg_id,
        bgCriminalFormData,
        'pubprovapphouseholdbgchecks'
      ).subscribe(
        (response) => {
          this.household_bg_id = response.household_bg_id;
          console.log('Household member checks updated...' + response)
          this._alertService.success('Household member background checks updated successfully!');
        },
        (error) => {
          this._alertService.error('Unable to update Household member background checks');
        }
      );
    } else {
      this._commonHttpService.create(

        bgCriminalFormData,
        'pubprovapphouseholdbgchecks/'
      ).subscribe(
        (response) => {
          this.household_bg_id = response.household_bg_id;
          console.log('Household member checks updated...' + response)
          this._alertService.success('Household member background checks updated successfully!');
        },
        (error) => {
          this._alertService.error('Unable to update Household member background checks');
        }
      );
    }
    (<any>$('#add-household-person-security')).modal('hide');
  }

  getApprovalFormGroup() {
    return this.formBuilder.group({
      narrative: '',
      clearance_date: '',
      mainQuestion: '',
      approval_info: this.formBuilder.group({
        directorsApproval: '',
        uploaded: '',
        approval_date: ''
      })
    });
  }

  getSubQuestionFormGroup() {
    return this.formBuilder.group({
      narrative: '',
      clearance_date: '',
      mainQuestion: '',
      subQuestion: ''
    });
  }

  addOutofStateClearance() {
    (<FormArray>this.bgChecksForm.get('outOfStateClearences')).push(this.createOutOfStateClearance())
  }

  addOutofStateCriminalClearance() {
    (<FormArray>this.bgCriminalCheckForm.get('outOfStateClearences')).push(this.createOutOfStateClearance())
  }

  createOutOfStateClearance() {
    return this.formBuilder.group({
      narrative: '',
      state: '',
      clearance_date: '',
      mainQuestion: '',
      approval_info: this.formBuilder.group({
        directorsApproval: '',
        uploaded: '',
        approval_date: ''
      })
    });
  }
}