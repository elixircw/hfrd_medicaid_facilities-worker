import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantPlacementStructuresComponent } from './public-applicant-placement-structures.component';

describe('PublicApplicantPlacementStructuresComponent', () => {
  let component: PublicApplicantPlacementStructuresComponent;
  let fixture: ComponentFixture<PublicApplicantPlacementStructuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantPlacementStructuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantPlacementStructuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
