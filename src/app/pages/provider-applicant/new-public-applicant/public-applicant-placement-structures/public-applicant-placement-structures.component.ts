import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import {  AlertService } from '../../../../@core/services';
import { ProviderService } from '../../../provider-management/new-public-provider/_entities/newApplicantModel';

// import { ProviderService } from '../_entities/newApplicantModel';

@Component({
  selector: 'public-applicant-placement-structures',
  templateUrl: './public-applicant-placement-structures.component.html',
  styleUrls: ['./public-applicant-placement-structures.component.scss']
})
export class PublicApplicantPlacementStructuresComponent implements OnInit {

  serviceClassification = [];
  serviceUnit = [];
  serviceStatus = [];
  servicesObj:any = {};
  availableTasks: any[] = [];
  currentData =[];
  FormMode:string;
  applicantId:any;
  servicePaid=[];
  placementStructuresList = [];
  providerPlacementStructuresDataList: ProviderService[] = [];
  
    minDate = new Date();
    
    programId: string;
    applicantServiceForm: FormGroup;

  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute,private formBuilder: FormBuilder,private _alertService: AlertService) { 
    this.applicantId = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.initServiceForm();
    this.getPlacementList();  
    this.getProviderservices();
    this.servicePaid = ['DJS','SSA','LDSS','MSDE','Medicaid','DJS-SSA'];
    this.serviceClassification = ['Educational','Residential'];
    this.serviceUnit = ['Annually','Daily','Hourly'];
    this.serviceStatus = ['Active', 'Expired'];
  }
  
  private initServiceForm() {
    this.FormMode="Add";
    this.applicantServiceForm = this.formBuilder.group({
      applicant_service_id :0,
      applicant_id :this.applicantId,
      service_id :null,
      start_dt :new Date(),
      end_dt:new Date(),
      service_description :[''],
      structure_service_cd: [''],
    });
  }

  private getProviderservices() {
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            applicant_id: this.applicantId
          }
      },
      'publicproviderapplicant/getproviderapplicantservices')
      .subscribe(
        (response: any) => 
        {
          let responseData = response.data;
          this.providerPlacementStructuresDataList = responseData.filter(x => x.structure_service_cd === 'P');
        });      
  }

  private deletepublicproviderservices(obj){
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          applicant_service_id: obj.applicant_service_id
        }
      },
      'Publicprovider/deletepublicproviderservices'
    ).subscribe(res => {
      this.servicesObj = res;
      this.getProviderservices()
    });
  }

  addServices(){
    this.FormMode="Add";
  }

  createNewService(obj){
    console.log("@@@ "+obj);
    this._commonHttpService.create(obj, "publicproviderapplicant/addproviderapplicantservices").subscribe(
      (response) => {
        this._alertService.success("Placement Structure saved successfully");
        this.getProviderservices();
        (<any>$('#add-placement-structure')).modal('hide');
      },
      (error) => {
        this._alertService.error("Unable to save Placement Structure");
      }
    );
  }

    // this._commonHttpService.create(obj, "publicprovider/addpublicproviderservices").subscribe(
    //   (response) => {
    //     this.getProviderservices();
    //     this._alertService.success("Placement Structure saved successfully");
    //     (<any>$('#add-placement-structure')).modal('hide');
    //   },
    //   (error) => {
    //     this._alertService.error("Unable to save Placement Structure");
    //   }
    // );

  getPlacementList() {

    this._commonHttpService.getArrayList({
      where: {structure_service_cd: 'P'},
      method: 'get',
      nolimit: true
    },
      'tb_services?filter'
    ).subscribe(placementStructuresList => {
      console.log("Placement", placementStructuresList);
      this.placementStructuresList = placementStructuresList;
    },
      (error) => {
        return false;
      }
    );

  }

}
