import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'public-applicant-check-list',
  templateUrl: './public-applicant-check-list.component.html',
  styleUrls: ['./public-applicant-check-list.component.scss']
})
export class PublicApplicantCheckListComponent implements OnInit {
  private checklistTypeChange$ = new Subject<any>();

  constructor() {
  }

  ngOnInit() {
  }

  tabChange(tab) {
    this.checklistTypeChange$.next(tab);
  }

}
