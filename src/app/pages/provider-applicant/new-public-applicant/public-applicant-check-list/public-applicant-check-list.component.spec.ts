import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantCheckListComponent } from './public-applicant-check-list.component';

describe('PublicApplicantCheckListComponent', () => {
  let component: PublicApplicantCheckListComponent;
  let fixture: ComponentFixture<PublicApplicantCheckListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantCheckListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantCheckListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
