import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseHoldMemberChecklistComponent } from './house-hold-member-checklist.component';

describe('HouseHoldMemberChecklistComponent', () => {
  let component: HouseHoldMemberChecklistComponent;
  let fixture: ComponentFixture<HouseHoldMemberChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseHoldMemberChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseHoldMemberChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
