import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ChangeDetectorRef, Input } from '@angular/core';
import { AlertService, AuthService, CommonHttpService, SessionStorageService, DataStoreService,GenericService } from './../../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaginationRequest, DynamicObject } from '../../../../../@core/entities/common.entities';
import { Subject } from 'rxjs';

@Component({
  selector: 'house-hold-member-checklist',
  templateUrl: './house-hold-member-checklist.component.html',
  styleUrls: ['./house-hold-member-checklist.component.scss']
})
export class HouseHoldMemberChecklistComponent implements OnInit {
  applicantId: string;
  id: string;
  taskList = [];
  tasktypestatus = ["Yes","No","N/A"];
  tmpAccordionKey = [];
  activityList = [];
  activityCompleteStatus = 'Completed';
  houseHoldMemberChecklistFormGroup: FormGroup;
  config = {
    panels: [

    ]
  };
  programType: string;
  persons = [];
  selectedPerson: any;
  selectedPersonId: any;
  @Input() parentSubject:Subject<any>;
  memberEnabled: any;
  publicProviderHouseholdMemberChecklist: any[];
  generatememberchecklist: boolean = true;
  statusList: any=["Yes","No","N/A"];
  appInfo: any;
  appStatus: any;

  constructor(private _cfr: ComponentFactoryResolver,
    private route: ActivatedRoute,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _changeDetect: ChangeDetectorRef,
    private _dataStoreService: DataStoreService) { 
      this.applicantId = route.parent.snapshot.params['id'];
      this.appInfo = this._dataStoreService.getData('publicProviderApplicantInfo');
      this.appStatus = (this.appInfo) ? this.appInfo.application_status : null;
  }

  ngOnInit() {
    this.houseHoldMemberChecklistFormGroup = this._formBuilder.group({
      checklist: this._formBuilder.array([])
    });

    this.getHouseHolds(1, 20).subscribe(response => {
      console.log('response', response);
      if (response && response.data && response.data.length) {
        this.persons = response.data;
      }
    });

    this.parentSubject.subscribe(data => {
      this.memberEnabled = (data === 'hhm') ? true : false;
      this.selectedPerson = null;
      this.selectedPersonId = null;
    });
  }

  getHouseHolds(page: number, limit: number) {
    const inputRequest = {
      intakenumber: this.applicantId
    };
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: page,
          limit: limit,
          method: 'get',
          where: inputRequest
        }),
        'People/getpersondetailcw?filter'
      );
  }

  selectPerson(person) {
    this.houseHoldMemberChecklistFormGroup = this._formBuilder.group({
      checklist: this._formBuilder.array([])
    });
    this.selectedPerson = null;
    this.selectedPersonId = null;
    setTimeout(() => {
      this.selectedPerson = person;
      this.selectedPersonId = person.personid;
      this.getpublicproviderhouseholdmember();
      this.getpublicproviderhouseholdchecklist();
    }, 1000);
  }

  getpublicproviderhouseholdchecklist(){
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 10,
        where: { checklist_type: 'HHM' }
      },'publicproviderhouseholdchecklist/list?filter'
    ).subscribe(res => {
      if (res && res.length>0) {
        this.publicProviderHouseholdMemberChecklist = res;
      }
    });
  }

  generateCheckList(){
   
    let req = {
      checklist: this.publicProviderHouseholdMemberChecklist,
      applicant_id: this.applicantId,
      personid: this.selectedPersonId,
      householdMemberId: null
    }
    
    this._commonHttpService.create(req, 'publicproviderhouseholdmember/addchecklist').subscribe(
      (response) => {
        this.generatememberchecklist = false;
        this.getpublicproviderhouseholdmember();
        this._alertService.success('CheckList Added successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        return false;
      }
    );
  }

  getpublicproviderhouseholdmember(){
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 20,
        where: { applicant_id: this.applicantId, personid: this.selectedPersonId }
      },'publicproviderhouseholdmember/memberchecklist?filter'
    ).subscribe(res => {
      // if (res && res.length>0) {
      //   this.taskList = res;
      //   this.setFormValues();
      // }
      if (res && res.length>0) {
        this.generatememberchecklist = false;
        const control = <FormArray>this.houseHoldMemberChecklistFormGroup.controls.checklist;
        res.forEach(x => {
            control.push(this.buildTaskForm(x));
        });
      }else{
        this.generatememberchecklist = true;
      }
    });
  }

  createTaskForm() {
    return this._formBuilder.group({
      household_member_first_name: [''],
      household_member_middle_name: [''],
      household_member_last_name: [''],
      applicant_id: [''],
      personid : this.selectedPersonId,
      household_member_id: [''],
      checklist_task: [''],
      comment: [''],
      review_date: [''],
      date_type: [''],
      status: ['']
    });
  }

  private buildTaskForm(x): FormGroup {
    return this._formBuilder.group({
      // household_member_first_name: x.household_member_first_name,
      // household_member_middle_name: x.household_member_middle_name,
      // household_member_last_name: x.household_member_last_name,
      // applicant_id: x.applicant_id,
      // personid: this.selectedPersonId,
      // household_member_id: x.household_member_id,
      // checklist_task: x.checklist_task,
      // comment: x.comment,
      // review_date: x.review_date,
      // date_type: x.date_type,
      // status: x.status
      applicant_id: x.applicant_id ? x.applicant_id : '',
      personid: this.selectedPersonId,
      checklist_task: x.checklist_task ? x.checklist_task : '',
      comment: x.comment ? x.comment : '',
      review_date: x.review_date ? x.review_date : '',
      date_type: x.date_type ? x.date_type : '',
      status: x.status ? x.status : ''
    });
  }



  setFormValues() {
    let newObj = {};

    this.taskList.forEach((x) => {
      if (!newObj[x.household_member_id]) {
        newObj[x.household_member_id] = [x];
      } else {
        newObj[x.household_member_id].push(x);
      }
    }
    );

    Object.keys(newObj).forEach((key, index) => {
      var first_name  = newObj[key][0].household_member_first_name === null ? '' : newObj[key][0].household_member_first_name ;
      var middle_name  = newObj[key][0].household_member_middle_name === null ? '' : newObj[key][0].household_member_middle_name ;
      var last_name  = newObj[key][0].household_member_last_name === null ? '' : newObj[key][0].household_member_last_name ;

      var name = first_name + ' '+ middle_name +' '+last_name;

      if (!this.tmpAccordionKey.includes(key)) {
        this.houseHoldMemberChecklistFormGroup.setControl('task' + this.selectedPersonId, this._formBuilder.array([]));
        this.config.panels.push({ name: key, text: name, description: this.activityCompleteStatus });
        this.tmpAccordionKey.push(key);
      }

      const control = <FormArray>this.houseHoldMemberChecklistFormGroup.controls['task' + this.selectedPersonId];

      newObj[key].forEach((x) => {
        control.push(this.buildTaskForm(x));
        if (x.status === '' ||  x.status === null) {
          this.config.panels[index].description = 'Incomplete';
        }
      });


    });
  }



  // saveTask(index) {
  //   const control = <FormArray>this.houseHoldMemberChecklistFormGroup.controls['task' + index];
  //   const  req = {
  //     checklist: control.getRawValue(),
  //     applicant_id: this.applicantId,
  //     personid : this.selectedPersonId
  //   };

  //   this._commonHttpService.create(req, 'publicproviderhouseholdmember/updatememberchecklist').subscribe(
  //     (response) => {
  //       this._alertService.success('CheckList Updated successfully!');
  //     },
  //     (error) => {
  //       this._alertService.error('Unable to save Checklist, please try again.');
  //       return false;
  //     }
  //   );

  // }

  saveTask(){
    let req = {
      checklist: this.houseHoldMemberChecklistFormGroup.getRawValue().checklist,
      applicant_id: this.applicantId
    };
    
    this._commonHttpService.create(req, 'publicproviderhouseholdmember/updatememberchecklist').subscribe(
      (response) => {
        this.generatememberchecklist = false;
        this._alertService.success('CheckList Updated successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        return false;
      }
    );
  }

}
