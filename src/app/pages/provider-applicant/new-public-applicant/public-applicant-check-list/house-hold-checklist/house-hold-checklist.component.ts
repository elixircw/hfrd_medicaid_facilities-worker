import { Component, OnInit, Input } from '@angular/core';
import { AlertService, AuthService, CommonHttpService, SessionStorageService, DataStoreService,GenericService } from './../../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'house-hold-checklist',
  templateUrl: './house-hold-checklist.component.html',
  styleUrls: ['./house-hold-checklist.component.scss']
})
export class HouseHoldChecklistComponent implements OnInit {
  applicantId: string;
  generatechecklist: boolean = true;
  checkListFormGroup: FormGroup;
  publicproviderhouseholdchecklist: any;
  statusList: any=["Yes","No","N/A"];
  appInfo: any;
  appStatus: any;
  constructor(private _commonHttpService: CommonHttpService, 
    private _alertService: AlertService, 
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService
    ) { 
    this.applicantId = route.parent.snapshot.params['id'];
    this.appInfo = this._dataStoreService.getData('publicProviderApplicantInfo');
    this.appStatus = (this.appInfo) ? this.appInfo.application_status : null;

  }

  ngOnInit() {
    this.initializeForm();
    this.getpublicproviderhousehold();
    this.getpublicproviderhouseholdchecklist();
  }

  initializeForm(){
    this.checkListFormGroup = this._formBuilder.group({
      checklist: this._formBuilder.array([])
    });
  }

  generateTastBtn() {
    this.generatechecklist = false;
  }

  getpublicproviderhousehold(){
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 20,
        where: { applicant_id: this.applicantId}
      },'publicproviderhousehold/list?filter'
    ).subscribe(res => {
      if (res && res.length>0) {
        this.generatechecklist = false;
        this.initializeForm();
        const control = <FormArray>this.checkListFormGroup.controls.checklist;
        res.forEach(x => {
            control.push(this.buildCheckListForm(x));
        });
      }else{
        this.generatechecklist = true;
      }
    });
  }


  private buildCheckListForm(x): FormGroup {
    return this._formBuilder.group({
        applicant_id: x.applicant_id ? x.applicant_id : '',
        checklist_task: x.checklist_task ? x.checklist_task : '',
        comment: x.comment ? x.comment : '',
        review_date: x.review_date ? x.review_date : '',
        date_type: x.date_type ? x.date_type : '',
        status: x.status ? x.status : ''
    });
} 

  getpublicproviderhouseholdchecklist(){
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 10,
        where: { checklist_type: 'HH' }
      },'publicproviderhouseholdchecklist/list?filter'
    ).subscribe(res => {
      if (res && res.length > 0) {
        this.publicproviderhouseholdchecklist = res;
      }
    });
  }


  generateCheckList(){
   
    let req = {
      checklist: this.publicproviderhouseholdchecklist,
      applicant_id: this.applicantId
    }
    
    this._commonHttpService.create(req, 'publicproviderhousehold/addchecklist').subscribe(
      (response) => {
        this.generatechecklist = false;
        this.getpublicproviderhousehold();
        this._alertService.success('CheckList Added successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        return false;
      }
    );
  }


  saveChecklist(){
    this._commonHttpService.create(this.checkListFormGroup.getRawValue(), 'publicproviderhousehold/updatechecklist').subscribe(
      (response) => {
        this.generatechecklist = false;
        this._alertService.success('CheckList Updated successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        return false;
      }
    );
  }

 
}
