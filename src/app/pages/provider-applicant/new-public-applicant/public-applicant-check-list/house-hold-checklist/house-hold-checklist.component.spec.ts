import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseHoldChecklistComponent } from './house-hold-checklist.component';

describe('HouseHoldChecklistComponent', () => {
  let component: HouseHoldChecklistComponent;
  let fixture: ComponentFixture<HouseHoldChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseHoldChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseHoldChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
