import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantBasicInfoComponent } from './public-applicant-basic-info.component';

describe('PublicApplicantBasicInfoComponent', () => {
  let component: PublicApplicantBasicInfoComponent;
  let fixture: ComponentFixture<PublicApplicantBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
