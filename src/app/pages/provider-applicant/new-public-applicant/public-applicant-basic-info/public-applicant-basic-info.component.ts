import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { ProviderService } from '../../../provider-management/new-public-provider/_entities/newApplicantModel';

@Component({
  selector: 'public-applicant-basic-info',
  templateUrl: './public-applicant-basic-info.component.html',
  styleUrls: ['./public-applicant-basic-info.component.scss']
})
export class PublicApplicantBasicInfoComponent implements OnInit {

  publicApplicantBasicInfoForm: FormGroup;
  applicantId: string;
  localDept: Array<any>;
  irsFormIndicator: Array<any>;
  @Input() isReadOnly: boolean;
  ageGroup = [];
  inquirySource = [];
  placementStructuresList = [];
  applicantInfo = [];




  constructor(
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute) {
    this.applicantId = route.parent.snapshot.params['id'];

  }

  ngOnInit() {
    this.initPublicApplicantBasicInfoForm();
    this.getInformation();
    this.getPlacementList();
    this.localDept = ['1', '2'];
    this.irsFormIndicator = ['Yes', 'No'];
    this.ageGroup = ['0-2', '3-5', '6-10', '11-14', '15-20', '21+'];
    this.inquirySource = ['Internet', 'Radio Station', 'Television', 'Flyer', 'Newspaper', 'Bus advertisement',
      'Bus stop', 'Public Resource Parent', 'Community Event', 'MVA', 'Billboard', 'In home mailer', 'Adoptuskids', 'Long time interest', 'Other'];
  }

  private initPublicApplicantBasicInfoForm() {
    this.publicApplicantBasicInfoForm = this.formBuilder.group({

      payment_info_medicaid_provider: null,
      payment_info_payee_first_nm: [''],
      payment_info_payee_last_nm: [''],
      payment_info_1099_indicator: [''],
      prgram: [''],
      provider_program_type: [''],
      age_group: [null],
      inquiry_source: [''],
      inquiry_source_details: [''],
      placement_structures: null,
    });


  }

  private getInformation() {
    this._commonHttpService.getById(
      this.applicantId,
      'publicproviderapplicant'
    ).subscribe(
      (response) => {
        console.log('get response', response);
        this.applicantInfo = response
        // this.publicApplicantBasicInfoForm.patchValue(response);
        this.publicApplicantBasicInfoForm.patchValue({
          inquiry_source: response.inquiry_source,
          inquiry_source_details: response.inquiry_source_details,
          prgram: response.prgram,
          provider_program_type: response.provider_program_type,
          // saved_placement_structures: response.saved_placement_structures,
          placement_structures: response.placement_structures,

          payment_info_medicaid_provider: response.payment_info_medicaid_provider,
          payment_info_payee_first_nm: response.payment_info_payee_first_nm,
          payment_info_payee_last_nm: response.payment_info_payee_last_nm,
          payment_info_1099_indicator: response.payment_info_1099_indicator,
          age_group: response.age_group,
        });

        this._dataStoreService.setData('publicApplicantBasicInfoForm', response);
        //this._alertService.success('Information saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }

    );
  }

  setApplicantId() {
    this.applicantId = this._dataStoreService.getData('applicantId');
    console.log('second time', this._dataStoreService.getData('applicantId'))
  }

  private saveInformation() {
    console.log('this.publicApplicantBasicInfoForm', this.publicApplicantBasicInfoForm.value);
    var payload = {}
    //payload['data'] = this.publicApplicantBasicInfoForm.value;
    payload = this.publicApplicantBasicInfoForm.value;
    payload['applicant_id'] = this.applicantId;

    this._commonHttpService.patch(
      this.applicantId,
      payload,
      'publicproviderapplicant'
    ).subscribe(
      (response) => {
        this._alertService.success('Information saved successfully!');
        this.getInformation();
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }

    );
  }
  getPlacementList() {
    this._commonHttpService.getArrayList({
      where: { structure_service_cd: 'P' },
      method: 'get',
      nolimit: true
    },
      'tb_services?filter'
    ).subscribe(placementStructuresList => {
      console.log('Placement', placementStructuresList);
      this.placementStructuresList = placementStructuresList;
    },
      (error) => {
        return false;
      }
    );
  }

}
