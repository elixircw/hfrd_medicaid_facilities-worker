import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantHomeInfoComponent } from './public-applicant-home-info.component';

describe('PublicApplicantHomeInfoComponent', () => {
  let component: PublicApplicantHomeInfoComponent;
  let fixture: ComponentFixture<PublicApplicantHomeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantHomeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantHomeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
