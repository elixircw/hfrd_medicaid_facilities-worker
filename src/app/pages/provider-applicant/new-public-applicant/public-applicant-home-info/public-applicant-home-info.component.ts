import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'public-applicant-home-info',
  templateUrl: './public-applicant-home-info.component.html',
  styleUrls: ['./public-applicant-home-info.component.scss']
})
export class PublicApplicantHomeInfoComponent implements OnInit {

  publicApplicantInfoForm: FormGroup;
  applicantId: string;
  isZipValid: boolean;
  addressTypes = [];
  addresses = [];
  suffixTypes = [];
  prefixTypes = [];
  addressForm: FormGroup;
  deleteAdrObj: any;
  @Input() isReadOnly: boolean;
  isPaymentAddress: boolean = false;
  isPaymentChecked: boolean = false;
  isCoApplicantAddress: boolean = false;
  countyList$: Observable<DropdownModel[]>;
  programsTypes$: Observable<DropdownModel[]>;
  paymentAddressId: number;
  dropDownValue: string;
  programs = [];
  providerReferralProgram: string;
  interestedIn = [];
  poolLocation = [];
  stateDropDownItems$: Observable<DropdownModel>;
  selectedState: string;
  selectedCounty: string;
  user: any;
  
  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService) {
    this.applicantId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    this.initPublicApplicantInfoForm();
    this.getInformation();
    this.initializeAddressForm();
    this.getApplicantAddresses();
    this.loadDropDown();
    this.suffixTypes = ['Jr', 'Sr'];
    this.prefixTypes = ['Mr', 'Mrs', 'Ms', 'Dr', 'Prof', 'Sister', 'Atty'];
    this.interestedIn = ['Foster Care', 'Adoption', 'Providing Foster Care'];
    this.poolLocation = ['In ground', 'Above ground'];
    this.selectedState = null;
  }

  private initPublicApplicantInfoForm() {
    this.publicApplicantInfoForm = this.formBuilder.group({
      // Home info   
      object_id: [this.applicantId],
      home_info_children_no: [''],
      home_info_bedroom_no: [''],
      is_home_water: null,
      is_home_swimming_pool: null,
      home_info_pool_location: [''],
      home_is_other_agency: null,
      home_info_agency_nm: [''],
      is_child_care_provider: null,
      home_info_child_care_details: [''],
      home_info_id: [''],

      home_phone: [''],
      interested_in: [''],
      explanatory_text: [''],
      is_previously_applied: null,
      previous_state: [''],
      previous_source: [''],
      ownerorrenter: [''],
      propertybuiltyear: ['']
    });
  }

  private getInformation() {

    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where:
        {
          object_id: this.applicantId,
        }
      },
      'publicproviderhomeinfo?filter')
      .subscribe(response => {
        if (response && Array.isArray(response) && response.length) {
          console.log('get home info response', response[0]);
          if (response[0].previous_state) {
            this.selectedState = response[0].previous_state;
          }
          this.publicApplicantInfoForm.patchValue(response[0]);
          this._dataStoreService.setData('publicProviderApplicantInfo', response);
        }
      },
        (error) => {
          this._alertService.error('Unable to retrieve information');
        });
  }
 
  private saveInformation() {
    this.publicApplicantInfoForm.value.prgram = this.providerReferralProgram;
    if (this.publicApplicantInfoForm.value.is_home_swimming_pool === false) {
      this.publicApplicantInfoForm.value.home_info_pool_location = '';
    }
    if (this.publicApplicantInfoForm.value.home_is_other_agency === false) {
      this.publicApplicantInfoForm.value.home_info_agency_nm = '';
    }
    if (this.publicApplicantInfoForm.value.is_child_care_provider === false) {
      this.publicApplicantInfoForm.value.home_info_child_care_details = '';
    }
    if (this.publicApplicantInfoForm.value.is_previously_applied === false) {
      this.publicApplicantInfoForm.value.previous_state = '';
      this.publicApplicantInfoForm.value.previous_source = '';
    }
    if (this.publicApplicantInfoForm.value.interested_in !== 'Providing Foster Care') {
      this.publicApplicantInfoForm.value.explanatory_text = '';
    }
    if (!this.selectedState) {
      this.publicApplicantInfoForm.value.previous_source = '';
    }
    var payload = {};
    //payload['data'] = this.publicApplicantInfoForm.value;
    payload = this.publicApplicantInfoForm.value;
    payload['applicant_id'] = this.applicantId;
    this._commonHttpService.updateWithoutid(
      payload,
      'publicproviderhomeinfo/'
    ).subscribe(
      (response) => {
        this._alertService.success('Information saved successfully!');
        this.getInformation();
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }

    );
  }


  private initializeAddressForm() {
    this.addressForm = this.formBuilder.group({
      applicant_id: [''],
      provider_applicant_profile_id: [''],
      object_id: [''],
      address_id: [''],
      adr_street_no: [''],
      adr_street_nm: [''],
      adr_city_nm: [''],
      adr_state_cd: [''],
      adr_zip5_no: [null],
      address_type: [''],
      adr_county_cd: [''],
      isMainAddress: false,
      address_mapping_id: [''],

    });
    this.addressTypes = ['Individual Applicant Address', 'Co-Applicant Address'];
  }


  private getApplicantAddresses() {
    this._commonHttpService.create(
      {
        'where': {
          applicant_id: this.applicantId
        }
      },
      'publicproviderapplicant/getpublicapplicantaddresses'
    ).subscribe(response => {
      // if( response.data[0].addresses != null) {
      this.addresses = response.data[0].addresses;
      //}
      console.log(this.addresses);
    },
      (error) => {
        this._alertService.error('Unable to get address information, please try again.');
        console.log('get address information Error', error);
        return false;
      }
    );
  }


  saveAddress(addressFormInfo) {

    addressFormInfo.applicant_id = this.applicantId;
    if (addressFormInfo.address_id && !this.isCoApplicantAddress) {
      this._commonHttpService.create(addressFormInfo, 'providerapplicantportal/updateapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Updated successfully!');
          this.getApplicantAddresses();
          this.addresses.unshift(addressFormInfo);

        },
        (error) => {
          this._alertService.error('Unable to Update address, please try again.');
          return false;
        }
      );
    }
    else {
      this._commonHttpService.create(addressFormInfo, 'providerapplicantportal/addapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Created successfully!');
          this.getApplicantAddresses();
          this.addresses.unshift(addressFormInfo);
        },
        (error) => {
          this._alertService.error('Unable to Create address, please try again.');
          console.log('Save address Error', error);
          return false;
        }
      );
    }
    (<any>$('#public-applicant-address')).modal('hide');
    this.isPaymentChecked = false;
    this.isPaymentAddress = false;
  }

  isPaymentSelected(selectedValue) {
    this.dropDownValue = selectedValue;
    this.isPaymentChecked = false;
    console.log('Selected Value', selectedValue);
    if (selectedValue == 'Payment') {
      this.isPaymentChecked = true;
      console.log('loop');
    }
  }

  isCheckedEvent(event) {
    if (event.checked) {
      this.isCoApplicantAddress = true;
      this.getAddressByType('Individual Applicant Address');
    }
    else {
      this.isCoApplicantAddress = false;
      this.initializeAddressForm();
      this.addressForm.patchValue({
        address_type: 'Co-Applicant Address'
      });
    }
  }

  getAddressByType(event) {
    let addressTypeVal;
    let addresGet;
    if (typeof event == 'string') {
      addresGet = 'Individual Applicant Address'
      addressTypeVal = 'Co-Applicant Address';
    }
    else {
      addresGet = event.value;
      addressTypeVal = event.value;
      if (event.value == 'Co-Applicant Address')
        this.isCoApplicantAddress = true;
      else
        this.isCoApplicantAddress = false;
    }
    this._commonHttpService.create(
      {
        'where': {
          applicant_id: this.applicantId,
          addressType: addresGet
        }
      },
      'publicproviderapplicant/getpublicapplicantByaddressesType'
    ).subscribe(response => {
      //if( response.data[0].addresses != null) {
      //this.addresses = response.data[0].addresses;
      //}
      this.initializeAddressForm();
      let addressFormInfo = response.data;
      // addressFormInfo.applicant_id = this.applicantId;
      if (addressFormInfo.length > 0) {
        this.addressForm.patchValue({
          object_id: this.applicantId,
          address_id: addressFormInfo[0].address_id,
          adr_street_no: addressFormInfo[0].adr_street_no,
          adr_street_nm: addressFormInfo[0].adr_street_nm,
          adr_city_nm: addressFormInfo[0].adr_city_nm,
          adr_state_cd: addressFormInfo[0].adr_state_cd,
          adr_zip5_no: addressFormInfo[0].adr_zip5_no,
        });
      }
      this.addressForm.patchValue({
        address_type: addressTypeVal
      })
    },
      (error) => {
        this._alertService.error('Unable to get address information, please try again.');
        console.log('get address information Error', error);
        return false;
      }
    );
  }

  EditAddress(editObj) {

    // var addressFormInfo = this.addressForm.value;

    //  addressFormInfo.applicant_id = this.applicantId;
    console.log(editObj)
    this.addressForm.patchValue({
      object_id: editObj.object_id,
      address_id: editObj.address_id,
      adr_street_no: editObj.adr_street_no,
      adr_street_nm: editObj.adr_street_nm,
      adr_city_nm: editObj.adr_city_nm,
      adr_state_cd: editObj.adr_state_cd,
      adr_zip5_no: editObj.adr_zip5_no,
      address_type: editObj.address_type,
      address_mapping_id: editObj.address_mapping_id
    });
    if (editObj.address_type == 'Payment')
      this.isPaymentAddress = true;
    (<any>$('#public-applicant-address')).modal('show');
  }

  DeleteAddress(Obj) {
    this.deleteAdrObj = Obj;
    (<any>$('#delet-address')).modal('show');
  }

  CancelDelete() {
    (<any>$('#delet-address')).modal('hide');
  }

  DeleteAddressConfirm() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          address_id: this.deleteAdrObj.address_id,

        }
      },
      'providerapplicantportal/deleteapplicantaddress')
      .subscribe(response => {
        if (response) {
          this._alertService.success('Address Deleted successfully');
          this.getInformation();
          this.getApplicantAddresses();
        }
      });
    (<any>$('#delet-address')).modal('hide');
  }

  zipValidation(event: any) {
    this.isZipValid = false;
    const pattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
    if (!pattern.test(event.target.value)) {
      this.isZipValid = false;
    }
    else {
      this.isZipValid = true;
    }
  }

  private loadDropDown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        'admin/county' + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        'States' + '?filter'
      ),
    ])

      .map((result) => {
        console.log(result);
        return {

          countyList: result[0].map(
            item =>
              new DropdownModel({
                text: item.countyname,
                value: item.countyid
              })
          ),
          stateList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),

        };
      })
      .share();

    this.countyList$ = source.pluck('countyList');
    this.stateDropDownItems$ = source.pluck('stateList');


  }
  // selectedProgramNames(event) {
  //   console.log('prgram', event);
  //   console.log('selected prgram', event.source.selected._element.nativeElement.innerText.trim());
  //   this.providerReferralProgram = event.source.selected._element.nativeElement.innerText.trim();

  //   let target = event.source.selected._element.nativeElement;
  //   this.programs = event.value;
  // }
  saveSource(event) {
    this.selectedCounty = event.source.selected._element.nativeElement.innerText.trim();
    console.log('this.selectedCounty', this.selectedCounty);
  }
  savePreviousState(event) {
    this.selectedState = event.source.selected._element.nativeElement.innerText.trim();
    console.log('this.selectedState', this.selectedState);
  }
}
