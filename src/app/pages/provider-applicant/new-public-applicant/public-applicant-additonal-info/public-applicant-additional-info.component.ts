import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'public-applicant-additional-info',
  templateUrl: './public-applicant-additional-info.component.html',
  styleUrls: ['./public-applicant-additional-info.component.scss']
})
export class PublicApplicantAdditionalInfoComponent implements OnInit {
  publicApplicantInfoForm: FormGroup;
  applicantId: string;
  isZipValid: boolean;
  addressTypes = [];
  addresses = [];
  suffixTypes = [];
  prefixTypes = [];
  addressForm: FormGroup;
  individualInformationForm: FormGroup;
 // homeInfoForm: FormGroup;
  coAppInfoForm: FormGroup;
  appInfoForm: FormGroup;
  deleteAdrObj: any;
  @Input() isReadOnly: boolean;
  isPaymentAddress: boolean = false;
  isPaymentChecked: boolean = false;
  isCoApplicantAddress: boolean = false;
  countyList$: Observable<DropdownModel[]>;
  programsTypes$: Observable<DropdownModel[]>;
  paymentAddressId: number;
  dropDownValue: string;
  programs = [];
  providerReferralProgram: string;
  applicantTypes: string[];

  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService) {
    this.applicantId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.initPublicApplicantInfoForm();
    this.getInformation();
    this.suffixTypes = ['Jr', 'Sr'];
    this.prefixTypes = ['Mr', 'Mrs', 'Ms', 'Dr', 'Prof', 'Sister', 'Atty'];
    this.applicantTypes = ['Fictive', 'Relative'];
  }

  private initPublicApplicantInfoForm() {
    this.publicApplicantInfoForm = this.formBuilder.group({
      organization_first_nm: [''],
      organization_tax_id: null,
      individual_applicant_first_nm: [''],
      individual_applicant_last_nm: [''],
      individual_applicant_ssn: null,
      individual_applicant_dob: null,
      // individual_applicant_background_check: [''],
      co_applicant_first_nm: [''],
      co_applicant_last_nm: [''],
      co_applicant_ssn: null,
      co_applicant_dob: null,
      // co_applicant_background_check: [''],
      individual_applicant_prefix: [''],
      individual_applicant_middle_nm: [''],
      individual_applicant_suffix: [''],
      co_applicant_prefix: [''],
      co_applicant_suffix: [''],
      co_applicant_middle_nm: [''],
      //Individual Applicant Extra fields
      individual_applicant_hm_phone: [''],
      individual_applicant_cell_nm: [''],
      individual_applicant_email: [''],
      individual_applicant_employer_nm: [''],
      individual_applicant_phone_nm: [''],
      individual_applicant_us_citizen: [''],
      //Co-applicant extra fields
      co_applicant_hm_phone: [''],
      co_applicant_cell_nm: [''],
      co_applicant_email: [''],
      co_applicant_employer_nm: [''],
      co_applicant_phone_nm: [''],
      co_applicant_us_citizen: [''],
    });
    this.individualInformationForm = this.formBuilder.group({
      drivers_license_number: [''],
      drivers_license_state: [''],
      hearing_impaired: [''],
      legally_blind: [''],

    });
    // this.homeInfoForm = this.formBuilder.group({
    //   home_status: [''],
    //   property_built_on: null,
    // });
    this.coAppInfoForm = this.formBuilder.group({
      drivers_license_number: [''],
      drivers_license_state: [''],
      hearing_impaired: [''],
      legally_blind: [''],
    });
    this.appInfoForm = this.formBuilder.group({
      applicant_relation_type: [''],
      co_applicant_relation_type: [''],
      declared_disabled: [''],
      is_child_care_provider: [''],
      license_no: [''],
      applying_child_care_provider: [''],
      license_denied: [''],
      license_denied_reson: [''],
      is_resident_disabled: [''],
    });
  }

  private getInformation() {
    this._commonHttpService.getById(
      this.applicantId,
      'publicproviderapplicant'
    ).subscribe(
      (response) => {
        console.log('get info response', response);
        this.publicApplicantInfoForm.patchValue(response);
        this.individualInformationForm.patchValue(response.individual_information);
        this.coAppInfoForm.patchValue(response.co_app_information);
        this.appInfoForm.patchValue(response.app_information);
        // this.homeInfoForm.patchValue({
        //   home_status: response.home_information.home_status,
        //   property_built_on: response.home_information.property_built_on
        // });
        this._dataStoreService.setData('publicProviderApplicantInfo', response);
        //this._alertService.success('Information saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }

    );
  }

  private saveInformation() {
    this.publicApplicantInfoForm.value.prgram = this.providerReferralProgram;

    var payload = {}
    //payload['data'] = this.publicApplicantInfoForm.value;
    payload = this.publicApplicantInfoForm.value;
    payload['applicant_id'] = this.applicantId;
    payload['individual_information'] = this.individualInformationForm.value;
   // payload['home_information'] = this.homeInfoForm.value;
    payload['co_app_information'] = this.coAppInfoForm.value;
    payload['app_information'] = this.appInfoForm.value;

    this._commonHttpService.patch(
      '',
      payload,
      'publicproviderapplicant'
    ).subscribe(
      (response) => {
        this._alertService.success('Information saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }

    );
  }

}
