import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantAdditionalInfoComponent } from './public-applicant-additional-info.component';

describe('PublicApplicantAdditionalInfoComponent', () => {
  let component: PublicApplicantAdditionalInfoComponent;
  let fixture: ComponentFixture<PublicApplicantAdditionalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantAdditionalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantAdditionalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
