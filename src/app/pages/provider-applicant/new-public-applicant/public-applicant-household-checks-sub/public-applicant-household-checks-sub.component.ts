import { Component, OnInit, Input } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { CommonHttpService, AlertService, DataStoreService } from "../../../../@core/services";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector : 'pub-app-house-hold-checks-sub',
    templateUrl: './public-applicant-household-checks-sub.component.html',
})
export class PubAppHouseHoldChecksSubComp implements OnInit{

    @Input()
    private outerForm : FormGroup;

    @Input()
    private clearanceType : string;

    @Input()
    private title : string;

    @Input()
    private subQuestionStr : string;

    @Input()
    private titleQuestion: string;

    @Input()
    private isAppInfo: boolean;

    

    objectID:any;
    @Input() uploadedFiles = [];
    constructor(private formBuilder: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _router: Router, private route: ActivatedRoute,
        private _dataStoreService: DataStoreService){

                        
        }

        ngOnInit(){
            console.log(" main Question "+this.clearanceType);
            this.objectID = this._dataStoreService.getData('OBJECT_ID');
           
        }



    }