import { Component,Input, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import {  AlertService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { ProviderService } from '../_entities/newApplicantModel';

@Component({
  selector: 'public-applicant-services',
  templateUrl: './public-applicant-services.component.html',
  styleUrls: ['./public-applicant-services.component.scss']
})
export class PublicApplicantServicesComponent implements OnInit {servicePaid = [];
  serviceClassification = [];
  serviceUnit = [];
  serviceStatus = [];
  activityDropdownItems$: DropdownModel[];
  activityDropdownItems1$: Observable<DropdownModel[]>;
  applicantServicesDataList: ProviderService[] = [];
  servicesList = [];
  servicesObj:any = {};
  availableTasks: any[] = [];
  tableData = [];
  currentData =[];
  FormMode:string;
  applicantId:any;
  
  @Input() isReadOnly: boolean;

    minDate = new Date();
    providerId: string;
    programId: string;
    applicantServiceForm: FormGroup;

  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute,private formBuilder: FormBuilder,private _alertService: AlertService)
   { 
     this.applicantId = route.parent.snapshot.params['id'];
    }

  ngOnInit() {this.initServiceForm();
    this.loadDropDown();   
    this.getProviderservices();
    this.servicePaid = ['DJS','SSA','LDSS','MSDE','Medicaid','DJS-SSA'];
    this.serviceClassification = ['Educational','Residential'];
    this.serviceUnit = ['Annually','Daily','Hourly'];
    this.serviceStatus = ['Active', 'Expired'];
  }
  private initServiceForm() {
    this.FormMode="Add";
    this.applicantServiceForm = this.formBuilder.group({
      applicant_service_id :0,
      applicant_id :this.applicantId,
      service_id :[''],
      start_dt :new Date(),
      end_dt:new Date(),
      paid_cd :[''],
      primary_sw :[''],
      location_sw :[''],
      create_ts :[''],
      create_user_id :[''],
      update_ts :[''],
      update_user_id :[''],
      delete_sw :[''],
      service_status :[''],
      service_category:[''],
      service_classification :[''],
      service_cost :null,
      service_description :[''],
      service_paid_by :[''],
      service_unit :[''],

    });
  }
  private loadDropDown() {
    this._commonHttpService.getArrayList(
      {
      },
      'providerservice/listservicecategories'
    ).subscribe(taskList => {
      console.log("@@@@@"+taskList);
      this.activityDropdownItems$ = taskList;
    });
  }
  

  private getProviderservices() {
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            applicant_id: this.applicantId,
          }
      },
      'publicproviderapplicant/getproviderapplicantservices')
      .subscribe((response: any) => 
        {
          let responseData = response.data;
          this.applicantServicesDataList = responseData.filter(x => x.structure_service_cd === 'S');
          // this.tableData = licenseservices;
          // this.tableData.unshift(this.currentData);
          // console.log("Final table data", this.tableData);

        });      
  }

  onChangeActivity(param: any, text:any) {

    this.availableTasks = [];

    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          service_category_cd: param
        }
      },
      'providerservice/getserviceslist'
    ).subscribe(res => {
      this.servicesObj = res;
      //console.log(this.servicesObj.data);      
      this.servicesObj.data.forEach(element => {
        var currentData = {};
        currentData['service_id'] = element.service_id;
        currentData['service_nm'] = element.service_nm;
        this.availableTasks.push(currentData);
        console.log("$$$$ " + this.availableTasks);
        
      });
    });

  }

  private deleteServices(obj){
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          provider_service_id: obj.applicant_service_id
        }
      },
      'providerservice/deleteservices'
    ).subscribe(res => {
      this.servicesObj = res;
      this.getProviderservices()
    });
   
  }

  addServices(){
  this.FormMode="Add";
  }

  createNewService(obj){
    console.log("@@@ "+obj);
    this._commonHttpService.create(obj, "publicproviderapplicant/addproviderapplicantservices").subscribe(
      (response) => {
        this._alertService.success("Services saved Successfully");
        this.getProviderservices();
        (<any>$('#contract-license')).modal('hide');
      },
      (error) => {
        this._alertService.error("Unable to save services");
      }
    );
 
  }  
  
  getServicesList() {

    this._commonHttpService.getArrayList({
      where: {structure_service_cd: 'S'},
      method: 'get',
      nolimit: true
    },
      'tb_services?filter'
    ).subscribe(servicesList => {
      console.log("Services", servicesList);
      this.servicesList = servicesList;
    },
      (error) => {
        return false;
      }
    );

  }

}



  
  
