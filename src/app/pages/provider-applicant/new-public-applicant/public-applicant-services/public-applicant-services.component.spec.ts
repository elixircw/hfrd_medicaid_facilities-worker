import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantServicesComponent } from './public-applicant-services.component';

describe('PublicApplicantServicesComponent', () => {
  let component: PublicApplicantServicesComponent;
  let fixture: ComponentFixture<PublicApplicantServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
