import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantDocumentCreatorComponent } from './public-applicant-document-creator.component';

describe('PublicApplicantDocumentCreatorComponent', () => {
  let component: PublicApplicantDocumentCreatorComponent;
  let fixture: ComponentFixture<PublicApplicantDocumentCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantDocumentCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantDocumentCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
