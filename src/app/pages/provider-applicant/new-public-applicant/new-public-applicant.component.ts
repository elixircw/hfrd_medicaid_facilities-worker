
import { Component, OnInit, AfterViewInit } from '@angular/core';

import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { ApplicantUrlConfig } from '../provider-applicant-url.config';
import { AlertService } from '../../../@core/services/alert.service';
import { Observable } from 'rxjs/Observable';
// import { ReferralUrlConfig } from '../../provider-referral/provider-referral-url.config';
// import { ProviderReferral } from '../../provider-management/new-provider/_entities/newApplicantModel';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { RoutingUser } from '../new-applicant/_entities/existingreferralModel';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AuthService, DataStoreService } from '../../../@core/services';
import { PublicApplicantTabs, APPLICANT_TAB_ORDER } from './public-applicant-tab-config';
declare var $: any;
@Component({
  selector: 'new-public-applicant',
  templateUrl: './new-public-applicant.component.html',
  styleUrls: ['./new-public-applicant.component.scss']
})
export class NewPublicApplicantComponent implements OnInit, AfterViewInit {

  generatedDocuments$ = new Subject<string[]>();
  applicantNumber: string;
  referralnumber: string;
  referralInfo: any = '';
  dayspassed: number = 0;
  applicationdateavailable: boolean = false;
  homestudydateavailable: boolean = false;
  applicationStatus: string;
  isReadOnly: boolean = false;
  inapplicationphase: boolean = false;
  providerApplicationData: any;

  //applicantInfo: Profile[] = [];

  markappreceived: Date;
  homeStudyCompletion: Date;
  isSupervisor: boolean;
  currentUser: AppUser;
  publicApplicantTabs = PublicApplicantTabs;

  constructor(private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private _authService: AuthService,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService
  ) {
    this.applicantNumber = route.snapshot.params['id'];
    this._dataStoreService.setData('APPLICANT_NUMBER', this.applicantNumber);
    this.providerApplicationData = route.snapshot.data.appData;
    this._dataStoreService.setData('publicProviderApplicantInfo', this.providerApplicationData);

  }

  ngOnInit() {
    this.currentUser = this._authService.getCurrentUser();
    this.getInformation();
  }

  closehomestudyalert() {
    (<any>$('#days-remaining')).modal('hide');
  }

  showhomestudyalert() {
    (<any>$('#days-remaining')).modal('show');
  }

  ngAfterViewInit() {
    const __this = this;
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }


  dateChanged(value) {
    if (value) {
      this.markappreceived = value;
    }
  }

  homeStudyDateChanged(value) {
    if (value) {
      this.homeStudyCompletion = value;
    }
  }

  getDiferenceInDays(): number {
    return Math.trunc(Math.floor(new Date().getTime() - (new Date(this.referralInfo.application_received_date)).getTime()) / (1000 * 60 * 60 * 24));
  }


  markApplicationReceived() {
    if (this.markappreceived) {
      console.log('this.markappreceived' + this.markappreceived);
      this.referralInfo.application_received_date = this.markappreceived;
      this._commonHttpService.update(this.applicantNumber,
        this.referralInfo,
        'publicproviderapplicant').subscribe(
          (response) => {
            this.getInformation();
            (<any>$('#entityRole')).modal('hide');
          },
          (error) => {
            console.log(error);
          }
        );
      (<any>$('#mark-application-received')).modal('hide');
    } else {
      this._alertService.warn("Please select application received date!");
    }
  }


  homeStudyCompletionSave() {
    if (this.homeStudyCompletion) {
      console.log('this.homeStudyCompletion' + this.homeStudyCompletion);
      this.referralInfo.home_study_completion_date = this.homeStudyCompletion;
      this._commonHttpService.update(this.applicantNumber,
        this.referralInfo,
        'publicproviderapplicant').subscribe(
          (response) => {
            this.getInformation();
            (<any>$('#entityRole')).modal('hide');
          },
          (error) => {
            console.log(error);
          }
        );
      (<any>$('#home-study-completion')).modal('hide');
    } else {
      this._alertService.warn("Please select home study completion date!");
    }
  }

  private getInformation() {

    this.referralInfo = this.providerApplicationData;
    if (this.referralInfo &&
      this.referralInfo.phase === 'Application') {
      console.log('this.referralInfo.phase', this.referralInfo.phase);
      this.inapplicationphase = true;
    }

    if (this.referralInfo && this.referralInfo.home_study_completion_date) {
      this.homestudydateavailable = true;
    }

    if (this.referralInfo && this.referralInfo.application_received_date && this.inapplicationphase) {
      this.applicationdateavailable = true;
      this.dayspassed = this.getDiferenceInDays();
      if (this.dayspassed > 90 && (this.referralInfo.home_study_completion_date === null
        || this.referralInfo.home_study_completion_date === undefined
        || this.referralInfo.home_study_completion_date === 'null')) {
        this.showhomestudyalert();
      }
    }
    console.log('this.inapplicationphase', this.inapplicationphase);
    console.log('this.referralInfo.phase', this.referralInfo.phase);
    this._dataStoreService.setData('inapplicationphase', this.inapplicationphase);
    this._dataStoreService.setData('providerProgramType', this.referralInfo.provider_program_type);
    this.refreshScrollingTabs();


  }
  refreshScrollingTabs() {
    const __this = this;
    let tabOrder = this.inapplicationphase ? APPLICANT_TAB_ORDER.APPLICANT_PHASE : APPLICANT_TAB_ORDER.PRE_APPLICANT_PHASE;
    if (this.currentUser && this.currentUser.role) {
      if (this.currentUser.role.name === 'LDSS_HOMESTUDY_WORKER') {
        const removeTab = ['applicant-survey', 'pet-info'];
        tabOrder = tabOrder.filter(tab => !removeTab.includes(tab));
      }
    }
    this.publicApplicantTabs = tabOrder.map(tabId => this.publicApplicantTabs.find(tab => tab.id === tabId));
    this.getCurrentTabDetails();
    const tabsLiContent = this.publicApplicantTabs.map(function (tab) {
      return '<li role="presentation" title="' + tab.title + '" data-trigger="hover" class="custom-li" routerLinkActive="active"></li>';
    });
    // this.publicApplicantTabs.forEach(tab => tab.active = false);
    const tabsPostProcessors = this.publicApplicantTabs.map(function (tab) {
      return function ($li, $a) {
        $a.attr('href', '');
        if (tab.active) {
          $a.attr('class', 'active');
        }
        $a.click(function () {
          __this.publicApplicantTabs.forEach(filteredTab => {
            filteredTab.active = false;
          });
          tab.active = true;
          __this._router.navigate([tab.route], { relativeTo: __this.route });
        });
      };
    });
    (<any>$('#public-applicant-tabs')).html('');
    (<any>$('#public-applicant-tabs')).scrollingTabs({
      tabs: __this.publicApplicantTabs,
      propPaneId: 'path', // optional - pass in default value for demo purposes
      propTitle: 'name',
      disableScrollArrowsOnFullyScrolled: true,
      tabsLiContent: tabsLiContent,
      tabsPostProcessors: tabsPostProcessors,
      scrollToActiveTab: true,
      tabClickHandler: function (e) {
        $(__this).addClass('active');
        $(__this).parent().addClass('active');
      }
    });
    

  }

  private getCurrentTabDetails() {
    this.publicApplicantTabs.forEach(tab => tab.active = false);
    const urlSegments = this._router.url.split('/');
    let tab: any;
    this.publicApplicantTabs.forEach(tabObj => {
      if (urlSegments.includes(tabObj.route)) {
        tab = tabObj;
        tab.active = true;
        return;
      }
    });
    return tab;
  }

}
