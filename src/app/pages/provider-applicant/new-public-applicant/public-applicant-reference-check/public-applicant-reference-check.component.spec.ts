import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantReferenceCheckComponent } from './public-applicant-reference-check.component';

describe('PublicApplicantReferenceCheckComponent', () => {
  let component: PublicApplicantReferenceCheckComponent;
  let fixture: ComponentFixture<PublicApplicantReferenceCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantReferenceCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantReferenceCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
