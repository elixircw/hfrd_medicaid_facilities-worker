import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
const LEAST_REFERECE_COUNT = 3;
const FACE_TO_FACE_COUNT = 2;
const NON_RELATIVE_COUNT = 2;

@Component({
  selector: 'public-applicant-reference-check',
  templateUrl: './public-applicant-reference-check.component.html',
  styleUrls: ['./public-applicant-reference-check.component.scss']
})
export class PublicApplicantReferenceCheckComponent implements OnInit {


  referenceCheckForm: FormGroup;
  applicantId: string;
  referenceInfo = [];
  currentReferenceId: string;
  isEditReference: boolean = false;
  relationshipList = [];
  contactType = [];
  availableHousehold = [];
  hasSatisfiedValidCount = false;
  hasFacetoFaceValidCount = false;
  hasNonRelativeValidCount = false;


  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.applicantId = route.snapshot.parent.params['id'];
  }

  ngOnInit() {
    this.formIntilizer();
    this.getInformation();
    this.getProviderHousehold();
    this.relationshipList = ['Father', 'Mother', 'Brother', 'Sister'];
    this.contactType = ['Written', 'Face-to-face', 'Telephone'];
  }

  formIntilizer() {
    this.referenceCheckForm = this.formBuilder.group(
      {
        object_id: [''],
        reference_check_date: [''],
        is_relative: [''],
        is_reference_recommends: [''],
        narrative: [''],
        reference_first_nm: [''],
        reference_middle_nm: [''],
        reference_last_nm: [''],
        relationship_to_application: [''],
        type_of_contact: [''],
        is_school_recommends: [''],
        household_member_id: [''],
      }
    );
  }
  addReference() {
    this.referenceCheckForm.reset();
    this.isEditReference = false;
  }

  checkForValidation() {
    this.hasFacetoFaceValidCount = this.getValidFaceToRefferences().length >= FACE_TO_FACE_COUNT;
    this.hasNonRelativeValidCount = this.getNonRelativeRefferences().length >= NON_RELATIVE_COUNT;
    this.hasSatisfiedValidCount = this.getValidRefferences().length >= LEAST_REFERECE_COUNT;
  }

  getValidRefferences() {
    return this.referenceInfo.filter(item => !item.is_school_recommends);
  }

  getValidFaceToRefferences() {
    return this.getValidRefferences().filter(item => item.type_of_contact === 'Face-to-face')
  }

  getNonRelativeRefferences() {
    return this.getValidRefferences().filter(item => !item.is_relative);
  }

  

  private getInformation() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: { object_id: this.applicantId }
      },
      'publicproviderreferencecheck?filter'
    ).subscribe(response => {
      this.referenceInfo = response;
      this.checkForValidation();

    });
  }
  editReferenceInfo(reference) {
    this.isEditReference = true;
    this.currentReferenceId = reference.reference_check_id;
    this.referenceCheckForm.patchValue(reference);
    (<any>$('#add-references')).modal('show');
  }

  patchReferenceDetails() {
    this.referenceCheckForm.value.object_id = this.applicantId;
    this._commonHttpService.patch(
      this.currentReferenceId,
      this.referenceCheckForm.value,
      'publicproviderreferencecheck'
    ).subscribe(
      (response) => {
        this._alertService.success("Information updated successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to update information");
      }

    );
    (<any>$('#add-references')).modal('hide');

  }

  saveReferenceDetails() {
    if (this.referenceCheckForm.invalid) {
      this._alertService.error('Please fill mandatory fields');
      return false;
    }
    this.referenceCheckForm.value.object_id = this.applicantId;
    this._commonHttpService.create(
      this.referenceCheckForm.value,
      'publicproviderreferencecheck/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-references')).modal('hide');
  }
  deleteReferenceDetails(reference) {
    this.currentReferenceId = reference.reference_check_id;
    (<any>$('#delete-reference')).modal('show');
  }
  cancelDelete() {
    this.currentReferenceId = null;
    (<any>$('#delete-reference')).modal('hide');
  }
  conformDeleteReferenceDetails() {
    this._commonHttpService.remove(
      this.currentReferenceId,
      this.referenceCheckForm.value,
      'publicproviderreferencecheck/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information Deleted successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to Delete information");
      }

    );
    (<any>$('#delete-reference')).modal('hide');
  }
  getProviderHousehold() {
    this.getHouseHolds().subscribe(
      (response) => {
        this.availableHousehold = response.data;
        console.log("Rahul-TableData:" + JSON.stringify(this.availableHousehold));
      },
      (error) => {
        this._alertService.error("Unable to retrieve Household member");
      }
    );
  }

  getHouseHolds() {

    const inputRequest = {
      intakenumber: this.applicantId
    };
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: inputRequest
        }),
        'People/getpersondetailcw?filter'
      );


  }

  clearNotes() {
    (<any>$('#add-references')).modal('hide');
    this.referenceCheckForm.reset();
  }

}
