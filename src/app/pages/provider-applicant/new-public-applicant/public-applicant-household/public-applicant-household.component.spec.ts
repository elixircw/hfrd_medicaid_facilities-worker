import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantHouseholdComponent } from './public-applicant-household.component';

describe('PublicApplicantHouseholdComponent', () => {
  let component: PublicApplicantHouseholdComponent;
  let fixture: ComponentFixture<PublicApplicantHouseholdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantHouseholdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantHouseholdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
