import { Component, Input, OnInit, OnChanges, ViewChild } from '@angular/core';
//import { ApplicantUrlConfig } from '../../provider-portal-temp-url.config'
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { CheckboxModel, DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { debug } from 'util';
import { tap } from 'rxjs/operators';
import { CommonDropdownsService } from '../../../../@core/services/common-dropdowns.service'
import { PubAppHouseHoldChecksComp } from '../public-applicant-household-checks/public-applicant-household-checks.component'
@Component({
  selector: 'public-applicant-household',
  templateUrl: './public-applicant-household.component.html',
  styleUrls: ['./public-applicant-household.component.scss']
})
export class PublicApplicantHouseholdComponent implements OnInit {

  applicantNumber: string;
  householdForm: FormGroup;
  tableData = Array<Object>();
  deleteHouseholdMemberId: any;
  householdMemberId: string;
  isEditHouseHold: boolean;
  isOutOfState: boolean;
  isStateCheckComplete: boolean;
  isNationalCheckComplete: boolean;
  isChildAbuseMaltreatmentCheckComplete: boolean;
  isChildAbuseMaltreatmentOosComplete: boolean;
  suffixTypes = [];
  prefixTypes = [];

  @Input() isSubmitted: boolean;
  @Input() isReadOnly: boolean;

  isPrepositionChecked: boolean;


  genderDropdownItems$: Observable<DropdownModel[]>;
  racetypeDropdownItems$: Observable<DropdownModel[]>;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  maritalDropdownItems$: Observable<DropdownModel[]>;
  relationShipDropdownItems$: Observable<DropdownModel[]>;

  religionDropDowm$: Observable<DropdownModel[]>;

  securityChecklistQuestions: string[];

  @ViewChild(PubAppHouseHoldChecksComp)
  houseHoldChecksComp: PubAppHouseHoldChecksComp;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute, private dropDownService: CommonDropdownsService) {
    this.applicantNumber = route.parent.snapshot.params['id'];
    this.securityChecklistQuestions = ["Is State criminal history background check completed?",
      "Is National criminal history background check completed?", "Is Child abuse and maltreatment database check completed?",
      "Has the MD Clearance been reviewed and determined to be acceptable?", "Has the FBI Clearance been reviewed and determined to be acceptable?",
      "Is the MVA Clearance acceptable?", "Is the FIA Clearance acceptable?", "Is there CPS History?"
    ]


  }

  ngOnInit() {
    this.initializeHouseholdForm();
    this.isPrepositionChecked = false;
    this.getProviderHousehold();
    this.isEditHouseHold = false;
    this.isOutOfState = false;
    this.isStateCheckComplete = false;
    this.isNationalCheckComplete = false;
    this.isChildAbuseMaltreatmentCheckComplete = false;
    this.isChildAbuseMaltreatmentOosComplete = false;
    this.suffixTypes = ["Jr", "Sr"];
    this.prefixTypes = ["Mr", "Mrs", "Ms", "Dr", "Prof", "Sister", "Atty"];
    this.loadDropDown();
    this.initializeSecurityQuestions();
  }

  initializeSecurityQuestions() {
    this.securityChecklistQuestions.forEach(item => {

      const bg_frm_grp = new FormGroup({
        bg_question_value: new FormControl(''),
        bg_clearance_date: new FormControl(''),
        bg_question: new FormControl(item)
      });

      (<FormArray>this.householdForm.get('household_member_bg_security_list')).push(bg_frm_grp);

    })
  }

  private initializeHouseholdForm() {
    this.isEditHouseHold = false;
    this.householdForm = this.formBuilder.group({
      object_id: this.applicantNumber,
      household_member_id: null,
      household_member_first_name: [''],
      household_member_middle_name: [''],
      household_member_last_name: [''],
      household_member_relation: [''],
      household_member_dob: null,
      household_member_ssn: null,
      household_member_clearance_status: [''],
      household_member_suffix: [''],
      household_member_prefix: [''],
      // suffix: null,
      // prefix:null,
      bg_chk_state: null,
      bg_chk_state_date: null,
      bg_chk_national: null,
      bg_chk_national_date: null,
      bg_chk_child_abuse_maltreatment: null,
      bg_chk_child_abuse_maltreatment_date: null,
      bg_chk_oos: null,
      bg_chk_child_abuse_maltreatment_oos: [''],
      bg_chk_child_abuse_maltreatment_oos_date: null,
      household_member_race: [''],
      household_member_ethnicity: [''],
      household_member_gender: [''],
      household_member_maritalstatus: [''],
      household_member_relation_coapp: [''],
      household_member_bg_security_list: new FormArray([]),

      alias: [''],
      former_name: [''],
      birthplace: [''],
      height: [''],
      weight: [''],
      hair: [''],
      eye_color: [''],
      religion: [''],
      tribal_affialiation: [''],
      languages: [''],

      education: [''],
      occupation: [''],
      employer: [''],
      annual_income: [''],
      additional_income_source: [''],

      is_school_attending: ['']
    });
  }

  resetForm() {
    this.householdForm.reset();
  }

  createHouseholdPerson() {

    console.log(this.householdForm.value);
    this.householdForm.get('object_id').setValue(this.applicantNumber);
    this._commonHttpService.create(
      this.householdForm.value,
      'publicproviderapplicanthousehold'
    ).subscribe(
      (response) => {
        this.householdMemberId = response.household_member_id;
        console.log('response.household_member_id', response.household_member_id);
        this._alertService.success("Applicant household added successfully!");
      },
      (error) => {
        this._alertService.error("Unable to create household information, please try again.");
      }
    );
  }

  getProviderHousehold() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { object_id: this.applicantNumber },
        order: 'household_member_id desc'
      },
      // 'publicproviderapplicanthousehold/getAll?filter'
      'publicproviderapplicanthousehold?filter'

    ).subscribe(
      (response) => {
        this.tableData = response;
        console.log("TableData:" + JSON.stringify(this.tableData));
      },
      (error) => {
        this._alertService.error("Unable to retrieve Household member");
      }
    );
  }

  patchProviderHousehold() {

    console.log('this.householdMemberId', this.householdMemberId);
    console.log('this.householdForm.value', this.householdForm.value);
    this.householdForm.value.household_member_id = this.householdMemberId;
    this._commonHttpService.patch(
      this.householdMemberId,
      this.householdForm.value,
      'publicproviderapplicanthousehold/updateHouseHold'
    ).subscribe(
      (response) => {
        this.getProviderHousehold();
        this.getpublicproviderhouseholdchecklist();
        this.resetForm();
        this._alertService.success("Household member updated successfully!");
      },
      (error) => {
        this._alertService.error("Unable to update Household member");
      }
    );
    (<any>$('#add-household-person')).modal('hide');
  }


  getpublicproviderhouseholdchecklist() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 10,
        where: { checklist_type: 'HHM' }
      }, 'publicproviderhouseholdchecklist/list?filter'
    ).subscribe(res => {
      if (res && res.length > 0) {
        this.getMemberApplicant(res);
      }
    });
  }


  getMemberApplicant(checklist) {
    let req = {
      household_member_id: this.householdMemberId,
      applicant_id: this.applicantNumber
    };

    this._commonHttpService.getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 10,
        where: req
      }, 'publicproviderhouseholdmember/getmemberapplicant?filter'
    ).subscribe(res => {
      if (res && res.length > 0) {

      } else {
        this.generateCheckList(checklist);
      }
    });
  }


  generateCheckList(checklist) {
    let req = {
      householdMemberId: this.householdMemberId,
      applicant_id: this.applicantNumber,
      checklist: checklist
    };

    this._commonHttpService.create(req, 'publicproviderhouseholdmember/addchecklist').subscribe(
      (response) => {
      },
      (error) => {
      }
    );
  }

  addSecurityChecks(person) {
    let houseHoldbgChecks;
    this.householdMemberId = person.household_member_id;
    this._commonHttpService.getSingle(
      {
        method: 'get',
        nolimit: true,
        where: { household_member_id: this.householdMemberId }
      },
      'pubprovapphouseholdbgchecks?filter'
    ).subscribe(
      (response) => {
        houseHoldbgChecks = response;
        console.log("Backgroud clearance data:" + JSON.stringify(houseHoldbgChecks));
        this.houseHoldChecksComp.household_member_id = this.householdMemberId;

        if (houseHoldbgChecks.length == 0) {
          this.houseHoldChecksComp.bgChecksForm.reset();
          this.houseHoldChecksComp.bgCriminalCheckForm.reset();
          delete this.houseHoldChecksComp.household_bg_id
          // this.houseHoldChecksComp.household_member_id = null;
        } else {
          this.houseHoldChecksComp.household_bg_id = houseHoldbgChecks[0].household_bg_id;
          this.houseHoldChecksComp.bgChecksForm.patchValue({
            fiaClearance: houseHoldbgChecks[0].submissiondata.fiaClearance,
            mvaClearance: houseHoldbgChecks[0].submissiondata.mvaClearance,
            judSearchClearance: houseHoldbgChecks[0].submissiondata.judSearchClearance,
            childProtectiveServ: houseHoldbgChecks[0].submissiondata.childProtectiveServ,
            mdSexOffRegClearance: houseHoldbgChecks[0].submissiondata.mdSexOffRegClearance,
            childSupportClearance: houseHoldbgChecks[0].submissiondata.childSupportClearance,
            natSexOffRegClearance: houseHoldbgChecks[0].submissiondata.natSexOffRegClearance,
            outOfStateClearences: houseHoldbgChecks[0].submissiondata.outOfStateClearences
          });
          this.houseHoldChecksComp.bgCriminalCheckForm.patchValue({
            mdClearance: houseHoldbgChecks[0].criminalhistorycheckdata.mdClearance,
            fbiClearance: houseHoldbgChecks[0].criminalhistorycheckdata.fbiClearance,
            outOfStateClearences: houseHoldbgChecks[0].criminalhistorycheckdata.outOfStateClearences,
            mdClearanceCheck: houseHoldbgChecks[0].criminalhistorycheckdata.mdClearanceCheck,
            fbiClearanceCheck: houseHoldbgChecks[0].criminalhistorycheckdata.fbiClearanceCheck
          });
        }

      },
      (error) => {
        this._alertService.error("Unable to retrieve Household member bgChecks");
        this.houseHoldChecksComp.bgChecksForm.reset();
        this.houseHoldChecksComp.bgCriminalCheckForm.reset();
        delete this.houseHoldChecksComp.household_bg_id;
      }
    );

    (<any>$('#add-household-person-security')).modal('show');

  }

  editHouseholdmember(person) {

    this.householdMemberId = person.household_member_id;
    (<any>$('#add-household-person')).modal('show');
    this.isEditHouseHold = true;
    this.householdForm.patchValue({
      object_id: this.applicantNumber,
      household_member_id: person.household_member_id,
      household_member_first_name: person.household_member_first_name,
      household_member_middle_name: person.household_member_middle_name,
      household_member_last_name: person.household_member_last_name,
      household_member_prefix: person.household_member_prefix,
      household_member_suffix: person.household_member_suffix,
      household_member_relation: person.household_member_relation,

      household_member_dob: person.household_member_dob,
      household_member_ssn: person.household_member_ssn,
      household_member_clearance_status: person.household_member_clearance_status,
      bg_chk_state: person.bg_chk_state,
      bg_chk_state_date: person.bg_chk_state_date,
      bg_chk_national: person.bg_chk_national,
      bg_chk_national_date: person.bg_chk_national_date,
      bg_chk_child_abuse_maltreatment: person.bg_chk_child_abuse_maltreatment,
      bg_chk_child_abuse_maltreatment_date: person.bg_chk_child_abuse_maltreatment_date,
      bg_chk_oos: person.bg_chk_oos,
      bg_chk_child_abuse_maltreatment_oos: person.bg_chk_child_abuse_maltreatment_oos,
      bg_chk_child_abuse_maltreatment_oos_date: person.bg_chk_child_abuse_maltreatment_oos_date,
      household_member_race: person.household_member_race,
      household_member_ethnicity: person.household_member_ethnicity,
      household_member_gender: person.household_member_gender,
      household_member_maritalstatus: person.household_member_maritalstatus,
      // household_member_bg_security_list : person.household_member_bg_security_list

      alias: person.alias,
      former_name: person.former_name,
      birthplace: person.birthplace,
      height: person.height,
      weight: person.weight,
      hair: person.hair,
      eye_color: person.eye_color,
      religion: person.religion,
      tribal_affialiation: person.tribal_affialiation,
      languages: person.languages,
      education: person.education,
      occupation: person.occupation,
      employer: person.employer,
      annual_income: person.annual_income,
      additional_income_source: person.additional_income_source,
      is_school_attending: person.is_school_attending
    });
    console.log('Form value', this.householdForm);
    if (person.household_member_bg_security_list && person.household_member_bg_security_list.length > 0) {
      while ((<FormArray>this.householdForm.get('household_member_bg_security_list')).length !== 0) {
        (<FormArray>this.householdForm.get('household_member_bg_security_list')).removeAt(0)
      }

      person.household_member_bg_security_list.forEach(item => {

        const bg_frm_grp = new FormGroup({
          bg_question_value: new FormControl(item.security_answer),
          bg_clearance_date: new FormControl(item.bg_clearance_date),
          bg_question: new FormControl(item.security_question),
          household_bg_id: new FormControl(item.household_bg_id)
        });

        (<FormArray>this.householdForm.get('household_member_bg_security_list')).push(bg_frm_grp);



      });
    }



    // if((<FormArray>this.householdForm.get('household_member_bg_security_list')).length==0){
    //   this.initializeSecurityQuestions();
    // }
  }

  deleteHouseholdmember(person) {
    this.deleteHouseholdMemberId = person.household_member_id;
    (<any>$('#delete-householdmember')).modal('show');

  }

  cancelDelete() {
    this.deleteHouseholdMemberId = null;
    (<any>$('#delete-householdmember')).modal('hide');
  }

  deleteHouseholdConfirm() {
    this._commonHttpService.remove(
      this.deleteHouseholdMemberId,
      {
        where: { household_member_id: this.deleteHouseholdMemberId }
      },
      'publicproviderapplicanthousehold' + '/' + this.deleteHouseholdMemberId
    ).subscribe(
      (response) => {
        this.getProviderHousehold();
        this._alertService.success("Household member deleted successfully!");
        (<any>$('#delete-householdmember')).modal('hide');
      },
      (error) => {
        this._alertService.error('Unable to delete Household member');
      }
    );
  }

  togglePreposition() {
    this.isPrepositionChecked = !this.isPrepositionChecked;
  }

  checkOutOfState(event) {
    console.log('Out of state', event.value);
    if (event.value = true) {
      this.isOutOfState = true;
    }
  }


  private loadDropDown() {
    this.dropDownService.getRelations().subscribe(item => {

      console.log("Relation item " + item)
    })

    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.GenderTypeUrl + '?filter'
      ),

      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true,
          order: 'typedescription'
        },
        NewUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
      ),


      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'typedescription'
        },
        NewUrlConfig.EndPoint.Intake.MaritalStatusUrl + '?filter'
      ),

      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'typedescription'
        },
        NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'typedescription'
        },
        NewUrlConfig.EndPoint.Intake.ReligionTypeUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          genders: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.gendertypekey
              })
          ),

          races: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.racetypekey
              })
          ),
          maritalstatus: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.maritalstatustypekey
              })
          ),

          relationShipResults: result[4].map(
            (res) => new DropdownModel({
              text: res.typedescription,
              value: res.relationshiptypekey
            })
          ),

          religionkey: result[5].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.religiontypekey
              })
          )

        };
      })
      .share();
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.genderDropdownItems$ = source.pluck('genders');
    this.maritalDropdownItems$ = source.pluck('maritalstatus');
    this.racetypeDropdownItems$ = source.pluck('races');
    //  this.relationShipDropdownItems$ = source.pluck('relationShipResults');
    this.relationShipDropdownItems$ = of([{ text: "Biological Brother", value: "BIOBR" }, { text: "Biological Brother", value: "BIOBR" }]);
    this.religionDropDowm$ = source.pluck('religionkey');


  }

}


