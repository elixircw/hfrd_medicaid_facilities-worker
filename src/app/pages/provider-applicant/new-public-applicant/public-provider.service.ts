import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRoute,
  ActivatedRouteSnapshot
} from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
@Injectable()
export class PublicProviderService implements Resolve<any> {
  moduleName: string;
  constructor(
    private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute
  ) {}
  resolve(route: ActivatedRouteSnapshot) {
    return this.getApplication(route.url[0].path);
  }
  private getApplication(id) {
     return this._commonHttpService.getById(id,'publicproviderapplicant');
  }
}