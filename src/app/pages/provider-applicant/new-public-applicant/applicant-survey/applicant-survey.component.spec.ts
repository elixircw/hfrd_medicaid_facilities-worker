import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantSurveyComponent } from './applicant-survey.component';

describe('ApplicantSurveyComponent', () => {
  let component: ApplicantSurveyComponent;
  let fixture: ComponentFixture<ApplicantSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
