import { Location } from '@angular/common';
import * as moment from 'moment';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { AuthService, SessionStorageService, CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { HttpService } from '../../../../@core/services/http.service';
import { environment } from '../../../../../environments/environment';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

declare var Formio: any;

export interface formInformationTable {
    templateName: string;
    templateDescription: string;
    action: string;
    // templateCategory:string;    
}
@Component({
  selector: 'applicant-survey',
  templateUrl: './applicant-survey.component.html',
  styleUrls: ['./applicant-survey.component.scss']
})
export class ApplicantSurveyComponent implements OnInit {
  dataSource: any;
  formInformation = [];
  selection = new SelectionModel<formInformationTable>(true, []);
  displayedColumns: string[] = ['templateName', 'templateDescription', 'action'];
  isShowForm: boolean = false;
  templateId: string;
  applicantId: string;

  constructor(private route: ActivatedRoute,
      private _authService: AuthService,
      private storage: SessionStorageService,
      private _commonService: CommonHttpService,
      private _commonHttpService: CommonHttpService,
      private _alertService: AlertService,
      private _http: HttpService,
      private _router: Router,
      private _dataStoreService: DataStoreService,
      private _changeDetect: ChangeDetectorRef,
      private location: Location) {
          this.applicantId = route.parent.snapshot.params['id'];

  }
  formioOptions: {
      formio: {
          ignoreLayout: true;
          emptyValue: '-';
      };
  };
  ngOnInit() {
      this.getFormConfigInformation();
  }
  ngAfterViewChecked(): void {
      this._changeDetect.detectChanges();
  }
  view(externalTempletId) {
      
      const _self = this;
      Formio.setToken(this.storage.getObj('fbToken'));
      Formio.baseUrl = environment.formBuilderHost;
      Formio.createForm(document.getElementById('homeStudyForm'), environment.formBuilderHost + '/form/' + externalTempletId, {
          hooks: {
              beforeSubmit: (submission, next) => {
                  console.log('beforeSubmit', submission);
                  if (!Formio.token) {
                      Formio.token = _self.storage.getObj('fbToken');
                  }
                  next();
              }
          }
      }).then(function (form) {
          form.components = form.components.map(item => {
              if (item.key === 'Complete' && item.type === 'button') {
                  item.action = 'submit';
              }
              return item;
          });

          _self._commonHttpService.getArrayList(
              {
                  method: 'get',
                  where: { 
                    object_id: _self.applicantId,
                    active_flag: '1',
                    template_id: _self.templateId
                   }
              },
              'providerformdetails?filter'
          ).subscribe(response => {
              form.submission = {
                  data: response[0].form_data
              };
          });

          form.on('submit', submission => {
              console.log('submission::' + JSON.stringify(submission));
              // this.currentFormInfo = submission.data;
              _self.saveCorrentFormInfo(submission.data);

          });
          form.on('change', formData => {

              // End - D-06446 , D-06602 Fix
          });
          form.on('render', formData => {
              /// (<any>$('#iframe-popup')).modal('show');
              setTimeout(function () {
                  $('#assessment-popup').scrollTop(0);
              }, 200);
          });

          form.on('error', error => {
              setTimeout(function () {
                  $('#assessment-popup').scrollTop(0);
              }, 200);

          });
      });
  }

  getFormConfigInformation() {
      this._commonHttpService
          .getArrayList(
              {
                  nolimit: true,
                  where: { 
                    activeFlag: 1,
                    template_category: 'APP'
                  },
                  method: 'get'
              },
              'providerformconfig?filter'
          )
          .subscribe((response) => {
            this.formInformation = response;
              console.log('this.formInformation', this.formInformation);
              this.dataSource = new MatTableDataSource<formInformationTable>(this.formInformation);
          });
}

  showForm(element) {
      this.isShowForm = true;
      this.templateId = element.template_id;
      this.view(element.external_template_id);

  }
  saveCorrentFormInfo(currentFormInfo) {
      var payload = {};
      payload['template_id'] = this.templateId;
      payload['form_data'] = currentFormInfo;
      payload['object_id']= this.applicantId;

      this._commonHttpService.create(
          payload,
          'providerformdetails'
      ).subscribe(
          (response) => {
              this._alertService.success("Information saved successfully!");
          },
          (error) => {
              this._alertService.error("Unable to save information");
          }
      );
  }
}

