import { HttpHeaders } from '@angular/common/http';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxfUploaderService } from 'ngxf-uploader';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { CheckboxModel, DropdownModel } from '../../../../@core/entities/common.entities';
import { ValidationService, DataStoreService, AuthService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ReferralDecision } from '../../../provider-referral/new-referral/_entities/newreferralModel';
import { AppUser } from '../../../../@core/entities/authDataModel';
@Component({
  selector: 'public-applicant-decision',
  templateUrl: './public-applicant-decision.component.html',
  styleUrls: ['./public-applicant-decision.component.scss']
})
export class PublicApplicantDecisionComponent implements OnInit {
  statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  dispositionFormGroup: FormGroup;
  referralDecision: ReferralDecision;
  @Input()
  referralDecision$ = new Subject<ReferralDecision>();
  @Input() isReadOnly: boolean;

  roleId: AppUser;


  constructor(private formBuilder: FormBuilder,
    private _authService: AuthService,
    private _alertService: AlertService) {
  }

  ngOnInit() {
    this.dispositionForm();
    this.roleId = this._authService.getCurrentUser();

    if (this.roleId.role.id == '25') {
      this.statusDropdownItems = [
        // Add conditional check here
        { "text": "Close", "value": "Closed" },
        { "text": "Review", "value": "Submitted" }
      ];
    } else {
      this.statusDropdownItems = [
        { "text": "Reject", "value": "Rejected" },
        { "text": "Approve", "value": "Approved" },
        // { "text": "Return to Worker", "value": "Incomplete" },
      ];
    }
  }

  dispositionForm() {
    this.dispositionFormGroup = this.formBuilder.group({
      status: [''],
      reason: ['']
    });

  }
  
  submitDecision() {
  this._alertService.success("Decision Submitted successfully!");
}

  notifySubscriber() {
    this.referralDecision$.next(this.dispositionFormGroup.value);
  }

}
