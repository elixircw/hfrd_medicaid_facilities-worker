import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantDecisionComponent } from './public-applicant-decision.component';

describe('PublicApplicantDecisionComponent', () => {
  let component: PublicApplicantDecisionComponent;
  let fixture: ComponentFixture<PublicApplicantDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
