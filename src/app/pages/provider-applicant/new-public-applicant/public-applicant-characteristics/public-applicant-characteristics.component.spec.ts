import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantCharacteristicsComponent } from './public-applicant-characteristics.component';

describe('PublicApplicantCharacteristicsComponent', () => {
  let component: PublicApplicantCharacteristicsComponent;
  let fixture: ComponentFixture<PublicApplicantCharacteristicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantCharacteristicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantCharacteristicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
