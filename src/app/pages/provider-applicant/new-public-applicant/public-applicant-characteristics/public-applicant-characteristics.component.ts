import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Input, Output, EventEmitter } from '@angular/core';
import { PublicApplicantAddCharacteristicsComponent } from '../public-applicant-add-characteristics/public-applicant-add-characteristics.component';

import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
// import { ApplicantUrlConfig } from '../../provider-management-url.config';
import { Subject } from 'rxjs';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
@Component({
  selector: 'public-applicant-characteristics',
  templateUrl: './public-applicant-characteristics.component.html',
  styleUrls: ['./public-applicant-characteristics.component.scss']
})
export class PublicApplicantCharacteristicsComponent implements OnInit {
  tableData = [];
  currentData =[];
  FormMode:string;
    id: string;
  @Input() isReadOnly: boolean;

  @ViewChild('addServices', { read: ViewContainerRef }) container: ViewContainerRef;
  @Output() setActivePlacements = new EventEmitter<boolean>();

    minDate = new Date();    
    // providerId: string;
    applicantId: string;
    picklistTypeId:string;
    contractInfoForm: FormGroup;
    applChildCharId:any;
  // constructor(private _cfr: ComponentFactoryResolver, private formBuilder: FormBuilder,
  //   private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private _alertService: AlertService) {
  //   this.id = route.snapshot.params['id'];
  // }

  monitorForm: FormGroup;
  isMonthDisabled: boolean = true;
  isYearDisabled: boolean = true;
  activityTaskStatusFormGroup: FormGroup;
  tasktypestatus = [];
  taskList = [];
  @Input()
  programTypeInputForMonitoring = new Subject<string>();
  programType: string;
  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute,private formBuilder: FormBuilder,
    private _alertService: AlertService,private _cfr: ComponentFactoryResolver) {
    this.applicantId = route.snapshot.params['id'];
    // this.providerId = '5000543';
    this.picklistTypeId= '43';
    this.id = route.snapshot.params['id'];
   } 

  ngOnInit() {
  this.getProviderContractChar();
    this.activityTaskStatusFormGroup = this.formBuilder.group({
      task: this.formBuilder.array([])
    });
    // this.getTaskList();
    this.programTypeInputForMonitoring.subscribe((data) => {
      this.programType = data;
    })
    this.tasktypestatus = ['Completed', 'Incomplete', 'N/A'];
  }

  createTaskForm() {
    return this.formBuilder.group({
      paid_non_paid_cd: [''],
      service_id: [''],
      service_nm: [''],
      structure_service_cd: ['']
    });
  }

  private getTaskList() {
    this.taskList.length = 0;
    this._commonHttpService.create(
      {
        method: 'post',
        applicant_id: this.id,
        category: 'Monitoring',
        from: 'Monitoring',
        monitoring_type: this.monitorForm.value.periodic,
        monitoring_year: this.monitorForm.value.yearly,
        monitoring_period: this.monitorForm.value.monthly
      },
     'providerapplicant/getchecklist'
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      this.setFormValues();
    });
  }

  private buildTaskForm(x): FormGroup {
    return this.formBuilder.group({
      // paid_non_paid_cd: x.paid_non_paid_cd,
      picklist_value_cd: x.picklist_value_cd,
      value_tx: x.value_tx,
      // structure_service_cd: x.structure_service_cd
    });
  }

  setFormValues() {
    this.activityTaskStatusFormGroup.setControl('task', this.formBuilder.array([]));
    const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    this.taskList.forEach((x) => {
      control.push(this.buildTaskForm(x));
    })
    console.log(this.activityTaskStatusFormGroup.value);
    console.log(this.activityTaskStatusFormGroup.controls);
  }

 
  openMonitoringCheckList() {
    // check and resolve the component
    let comp = this._cfr.resolveComponentFactory(PublicApplicantAddCharacteristicsComponent);
    // Create component inside container
    let expComponent = this.container.createComponent(comp);
    // see explanations
    expComponent.instance._ref = expComponent;
    expComponent.instance.category = 'Monitoring';
    expComponent.instance.type = this.programType;
    expComponent.instance.onSaveEventEmitter.subscribe(($event) => {
      console.log(' SIMAR Event Captured'+$event);
      // $event.forEach((x) => {
      //   this.taskList.push(x);
      // })
      this.setFormValues();
      console.log("SIMAR Calling services")
      this.getProviderContractChar();  
    });
  }


  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.monitorForm.controls['monthly'].setValue('');
    this.monitorForm.controls['yearly'].setValue('');
  }

  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
   
  }
  private getProviderContractChar() {
    this.tableData = [];
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            applicant_id: this.applicantId,
            picklist_type_id: this.picklistTypeId
          }
      },
      'publicproviderapplicant/getapplicantchildcharacteristics')
      .subscribe(licenseservices => 
        {
          this.tableData = licenseservices;
          // this.tableData.unshift(this.currentData);
          console.log("Final table data", this.tableData);
        });      
  }

  DeleteChar(applChildCharId){
    this.applChildCharId=applChildCharId;
    (<any>$('#delet-char')).modal('show');
  }
  deleteConfirm(){
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            applicant_id: this.applicantId,
            picklist_type_id: this.applChildCharId
          }
      },
      'publicproviderapplicant/deleteapplicantchildcharacteristics')
      .subscribe(licenseservices => 
        {
          if (licenseservices) {
            this._alertService.success("Characteristics Deleted successfully");
          this.getProviderContractChar();
          this.applChildCharId="";
          (<any>$('#delet-char')).modal('hide');
          }
        }); 
  }
  viewActivity() {
    // this.getTaskList();
  }

}
