import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantNarrativeComponent } from './public-applicant-narrative.component';

describe('PublicApplicantNarrativeComponent', () => {
  let component: PublicApplicantNarrativeComponent;
  let fixture: ComponentFixture<PublicApplicantNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
