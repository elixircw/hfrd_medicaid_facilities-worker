import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePlacementSpecificationComponent } from './home-placement-specification.component';

describe('HomePlacementSpecificationComponent', () => {
  let component: HomePlacementSpecificationComponent;
  let fixture: ComponentFixture<HomePlacementSpecificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePlacementSpecificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePlacementSpecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
