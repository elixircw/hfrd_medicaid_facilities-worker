import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';

@Component({
  selector: 'public-applicant-pet-info',
  templateUrl: './public-applicant-pet-info.component.html',
  styleUrls: ['./public-applicant-pet-info.component.scss']
})
export class PublicApplicantPetInfoComponent implements OnInit {

  petInfoForm: FormGroup;
  applicantId: string;
  petInfo= [];
  currentPetId: string;
  isEditPetInfo: boolean=false;
  

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.applicantId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.formIntilizer();
    this.getInformation();
   
  }

  formIntilizer() {
    this.petInfoForm = this.formBuilder.group(
      {
        object_id: [''],
        pet_type_tx: [''],
        pet_breed_tx: [''],
        pet_nm: [''],
        age_of_pet_tx: [''],
        rabies_certificate_expiry_dt: ['']
      }
    );
  }
addPetInfo(){
  this.isEditPetInfo=false;
}
  private getInformation() {
    this._commonHttpService.getArrayList(
			{
				method: 'get',
				where : { object_id : this.applicantId }
			},
			'publicproviderpetinfo?filter'
		).subscribe(response => {
			this.petInfo = response	

		});
  }
  editpetInfo(pets){
    this.isEditPetInfo=true;
    this.currentPetId=pets.provider_pet_id;
    this.petInfoForm.patchValue(pets);
    (<any>$('#add-pets')).modal('show');
  }

  patchPetDetails() {
    this.petInfoForm.value.object_id=this.applicantId;
    this._commonHttpService.patch(
      this.currentPetId,
      this.petInfoForm.value,
      'publicproviderpetinfo'
    ).subscribe(
      (response) => {
        this._alertService.success("Information updated successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to update information");
      }

    );
    (<any>$('#add-pets')).modal('hide');

  }

  savePetDetails() {
    this.petInfoForm.value.object_id=this.applicantId;
    this._commonHttpService.create(
      this.petInfoForm.value,
      'publicproviderpetinfo'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-pets')).modal('hide');
  }
  deletePetDetails(pets){
  this.currentPetId=pets.provider_pet_id;
  (<any>$('#delete-pets')).modal('show');
  }
  cancelDelete() {
    this.currentPetId = null;
    (<any>$('#delete-pets')).modal('hide');
  }
  conformDeletePetDetails() {
    this._commonHttpService.remove(
      this.currentPetId,
      this.petInfoForm.value,
      'publicproviderpetinfo'
    ).subscribe(
      (response) => {
        this._alertService.success("Information Deleted successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to Delete information");
      }

    );
    (<any>$('#delete-pets')).modal('hide');
  }


}
