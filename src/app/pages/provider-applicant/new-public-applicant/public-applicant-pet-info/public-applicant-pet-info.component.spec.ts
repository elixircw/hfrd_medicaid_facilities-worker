import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantPetInfoComponent } from './public-applicant-pet-info.component';

describe('PublicApplicantPetInfoComponent', () => {
  let component: PublicApplicantPetInfoComponent;
  let fixture: ComponentFixture<PublicApplicantPetInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantPetInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantPetInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
