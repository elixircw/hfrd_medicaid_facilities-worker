import { AppConstants } from '../../../@core/common/constants';

export const PublicApplicantTabs = [
    {
        id: 'program-info',
        title: 'Program info',
        name: 'Program info',
        role: [AppConstants.ROLES.PROVIDER, AppConstants.ROLES.PROVIDER_STAFF_ADMIN],
        route: 'program-info',
        active: false,
        securityKey: 'program-info-screen'
    },
    {
        id: 'applicants-info',
        title: 'Applicants info',
        name: 'Applicants info',
        role: [],
        route: 'applicants-info',
        active: false,
        securityKey: 'applicants-info-screen'
    },
    {
        id: 'home-info',
        title: 'Home info',
        name: 'Home info',
        role: [],
        route: 'home-info',
        active: false,
        securityKey: 'home-info-screen'
    },
    {
        id: 'household-members',
        title: 'Household Members',
        name: 'Household Members',
        role: [],
        route: 'household-members',
        active: false,
        securityKey: 'household-info-screen'
    },
    {
        id: 'background-checks',
        title: 'Background Checks',
        name: 'Background Checks',
        role: [],
        route: 'background-checks',
        active: false,
        securityKey: 'household-info-screen'
    },
    {
        id: 'services',
        title: 'Services',
        name: 'Services',
        role: [],
        route: 'services',
        active: false,
        securityKey: 'services-info-screen'
    },
    {
        id: 'checklist',
        title: 'Checklist',
        name: 'Checklist',
        role: [],
        route: 'checklist',
        active: false,
        securityKey: 'checklist-screen'
    },
    {
        id: 'contact-notes',
        title: 'Contact Notes',
        name: 'Contact Notes',
        role: [],
        route: 'contact-notes',
        active: false,
        securityKey: 'contact-notes-screen'
    },
    {
        id: 'applicant-survey',
        title: 'Applicant Survey',
        name: 'Applicant Survey',
        role: [],
        route: 'applicant-survey',
        active: false,
        securityKey: 'applicant-survey-screen'
    },
    {
        id: 'narrative',
        title: 'Narrative',
        name: 'Narrative',
        role: [],
        route: 'narrative',
        active: false,
        securityKey: 'narrative-screen'
    },
    {
        id: 'documents',
        title: 'Documents',
        name: 'Documents',
        role: [],
        route: 'documents',
        active: false,
        securityKey: 'documents-screen'
    },
    {
        id: 'home-study-visit',
        title: 'Home Study Visit',
        name: 'Home Study Visit',
        role: [],
        route: 'home-study-visit',
        active: false,
        securityKey: 'home-study-visit-screen'
    },
    {
        id: 'reference-checks',
        title: 'Reference Checks',
        name: 'Reference Checks',
        role: [],
        route: 'reference-checks',
        active: false,
        securityKey: 'reference-checks-screen'
    },
    {
        id: 'placmenet-specifications',
        title: 'Placement Specifications',
        name: 'Placement Specifications',
        role: [],
        route: 'placmenet-specifications',
        active: false,
        securityKey: 'placmenet-specifications-screen'
    },
    {
        id: 'pet-info',
        title: 'Pet Info',
        name: 'Pet Info',
        role: [],
        route: 'pet-info',
        active: false,
        securityKey: 'pet-info-screen'
    },
    {
        id: 'pre-service-training',
        title: 'Pre-Service Training',
        name: 'Pre-Service Training',
        role: [],
        route: 'pre-service-training',
        active: false,
        securityKey: 'pre-service-training-screen'
    },
    {
        id: 'review',
        title: 'Review',
        name: 'Review',
        role: [],
        route: 'review',
        active: false,
        securityKey: 'review-screen'
    }
];

export const APPLICANT_TAB_ORDER = {
    APPLICANT_PHASE: ['program-info', 'applicants-info', 'home-info', 'household-members', 'background-checks', 'services', 'checklist', 'contact-notes',
     'applicant-survey', 'narrative', 'documents', 'home-study-visit', 'reference-checks', 'placmenet-specifications', 'pet-info', 'pre-service-training', 'review'],
    PRE_APPLICANT_PHASE: ['program-info', 'applicants-info', 'home-info', 'household-members', 'background-checks', 'services',
     'checklist', 'contact-notes', 'applicant-survey', 'narrative', 'documents', 'pet-info', 'review'],
};

