import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { NewPublicApplicantRoutingModule } from './new-public-applicant-routing.module';
import { PublicApplicantContactComponent } from './public-applicant-contact/public-applicant-contact.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatStepperModule
} from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { NewApplicantComponent } from '../new-applicant/new-applicant.component';
import { NewPublicApplicantComponent } from './new-public-applicant.component';
import { PublicApplicantServicesComponent } from '../../provider-applicant/new-public-applicant/public-applicant-services/public-applicant-services.component';
import { PublicApplicantHouseholdComponent } from './public-applicant-household/public-applicant-household.component';
// import { PublicApplicantAddDocumentComponent } from './public-applicant-add-document/public-applicant-add-document.component';
// import { AttachmentDetailComponent } from './public-applicant-add-document/attachment-detail/attachment-detail.component';
// import { AttachmentUploadComponent } from './public-applicant-add-document/attachment-upload/attachment-upload.component';
// import { AudioRecordComponent } from './public-applicant-add-document/audio-record/audio-record.component';
// import { EditAttachmentComponent } from './public-applicant-add-document/edit-attachment/edit-attachment.component';
// import { ImageRecordComponent } from './public-applicant-add-document/image-record/image-record.component';
// import { VideoRecordComponent } from './public-applicant-add-document/video-record/video-record.component';
import { PublicApplicantCharacteristicsComponent } from './public-applicant-characteristics/public-applicant-characteristics.component';
import { PublicApplicantAddCharacteristicsComponent } from './public-applicant-add-characteristics/public-applicant-add-characteristics.component';
import { PublicApplicantBasicInfoComponent } from './public-applicant-basic-info/public-applicant-basic-info.component';
import { PublicApplicantNarrativeComponent } from './public-applicant-narrative/public-applicant-narrative.component';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { PublicApplicantDecisionComponent } from './public-applicant-decision/public-applicant-decision.component';
import { PublicApplicantDecisionNewComponent } from './public-applicant-decision-new/public-applicant-decision-new.component';
import { NgxMaskModule } from 'ngx-mask';
import { HomeApprovalInfoComponent } from './home-approval-info/home-approval-info.component';
import { PublicApplicantPreServiceTrainingComponent } from './public-applicant-pre-service-training/public-applicant-pre-service-training.component';
import { PublicApplicantReferenceCheckComponent } from './public-applicant-reference-check/public-applicant-reference-check.component';
import { PublicApplicantHomeStudyVisitComponent } from './public-applicant-home-study-visit/public-applicant-home-study-visit.component';
import { HomePlacementSpecificationComponent } from './home-placement-specification/home-placement-specification.component';
import { PublicApplicantDocumentCreatorComponent } from './public-applicant-document-creator/public-applicant-document-creator.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { AttachmentNarrativeComponent } from './add-document/attachment-narrative/attachment-narrative.component';
import { PublicApplicantPlacementStructuresComponent } from './public-applicant-placement-structures/public-applicant-placement-structures.component';
import { PublicApplicantServicesPlacementComponent } from './public-applicant-services-placement/public-applicant-services-placement.component';
import { PublicApplicantAdditionalInfoComponent } from './public-applicant-additonal-info/public-applicant-additional-info.component';
import { PublicApplicantHomeInfoComponent } from './public-applicant-home-info/public-applicant-home-info.component';
import { PublicApplicantCheckListComponent } from './public-applicant-check-list/public-applicant-check-list.component';
import { HouseHoldChecklistComponent } from './public-applicant-check-list/house-hold-checklist/house-hold-checklist.component';
import { HouseHoldMemberChecklistComponent } from './public-applicant-check-list/house-hold-member-checklist/house-hold-member-checklist.component';
import { CommonDropdownsService } from '../../../@core/services/common-dropdowns.service'
import { PubAppHouseHoldChecksComp } from './public-applicant-household-checks/public-applicant-household-checks.component';
import { PubAppHouseHoldChecksSubComp } from './public-applicant-household-checks-sub/public-applicant-household-checks-sub.component';
import { PublicProviderService } from './public-provider.service';
import { PublicApplicantPetInfoComponent } from './public-applicant-pet-info/public-applicant-pet-info.component';
import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';
import { ApplicantSurveyComponent } from './applicant-survey/applicant-survey.component';
import { IntakeConfigService } from '../../newintake/my-newintake/intake-config.service';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { ShareFeaturesModule } from '../../shared-pages/person-info/share-features/share-features.module';
import { AttachmentDetailComponent } from './add-document/attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './add-document/attachment-upload/attachment-upload.component';
import { AudioRecordComponent } from './add-document/audio-record/audio-record.component';
import { ImageRecordComponent } from './add-document/image-record/image-record.component';
import { EditAttachmentComponent } from './add-document/edit-attachment/edit-attachment.component';
import { VideoRecordComponent } from './add-document/video-record/video-record.component';



@NgModule({
  imports: [
    CommonModule, MatDatepickerModule, NewPublicApplicantRoutingModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    NgxfUploaderModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgSelectModule,
    NgxPaginationModule,
    MatButtonToggleModule,
    SharedDirectivesModule,
    MatSlideToggleModule,
    NgxMaskModule.forRoot(),
    MatStepperModule,
    FormMaterialModule,
    ShareFeaturesModule
  ],
  declarations: [NewPublicApplicantComponent,
    PublicApplicantContactComponent,
    PublicApplicantServicesComponent,
    PublicApplicantPlacementStructuresComponent,
    PublicApplicantHouseholdComponent,
  //  PublicApplicantAddDocumentComponent,
    AttachmentDetailComponent,
    AttachmentUploadComponent,
    AudioRecordComponent,
    EditAttachmentComponent,
    ImageRecordComponent,
    VideoRecordComponent,
    AddDocumentComponent,
    PublicApplicantCharacteristicsComponent,
    PublicApplicantAddCharacteristicsComponent,
    PublicApplicantBasicInfoComponent,
    PublicApplicantNarrativeComponent,
    PublicApplicantAdditionalInfoComponent,
    PublicApplicantDecisionComponent,
    PublicApplicantDecisionNewComponent,
    HomeApprovalInfoComponent,
    PublicApplicantPreServiceTrainingComponent,
    PublicApplicantReferenceCheckComponent,
    PublicApplicantHomeStudyVisitComponent,
    HomePlacementSpecificationComponent,
    PublicApplicantDocumentCreatorComponent,

    AttachmentNarrativeComponent,

    PublicApplicantPlacementStructuresComponent,

    PublicApplicantServicesPlacementComponent,

    PublicApplicantHomeInfoComponent,

    PublicApplicantCheckListComponent,

    HouseHoldChecklistComponent,

    HouseHoldMemberChecklistComponent,
    PubAppHouseHoldChecksComp,
    PubAppHouseHoldChecksSubComp,
    PublicApplicantPetInfoComponent,
    ApplicantSurveyComponent
    

  ],
  exports: [PubAppHouseHoldChecksComp, PubAppHouseHoldChecksSubComp],
  entryComponents: [PublicApplicantAddCharacteristicsComponent],

  providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService, CommonDropdownsService, PublicProviderService, IntakeConfigService]
})


export class NewPublicApplicantModule { }
