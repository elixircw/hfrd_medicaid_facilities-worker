import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
@Component({
  selector: 'home-approval-info',
  templateUrl: './home-approval-info.component.html',
  styleUrls: ['./home-approval-info.component.scss']
})
export class HomeApprovalInfoComponent implements OnInit {
  homeApprovalForm: FormGroup;
  applicantId: string;
  homeApprovalInfo= [];
  currentHomeApprovalId: string;
  isEditHomeApproval: boolean=false;
  genderTypes=[];

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.applicantId = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.formIntilizer();
    this.getInformation();
    this.genderTypes=["Male","Female", "Male/Female"];
  }

  formIntilizer() {
    this.homeApprovalForm = this.formBuilder.group(
      {
        object_id: [''],
        min_age_yr: null,
        max_age_yr: null,
        min_age_months: null,
        max_age_months: null,
        capacity: null,
        gender: null,
      }
    );
  }
addHomeApproval(){
  this.isEditHomeApproval=false;
}
  private getInformation() {
    this._commonHttpService.getArrayList(
			{
				method: 'get',
				where : { object_id : this.applicantId }
			},
			'publicproviderhomeapproval?filter'
		).subscribe(response => {
			this.homeApprovalInfo = response	

		});
  }
  edithomeApprovalInfo(homeApproval){
    this.isEditHomeApproval=true;
    this.currentHomeApprovalId=homeApproval.home_study_visit_id;
    this.homeApprovalForm.patchValue(homeApproval);
    (<any>$('#add-homeApproval')).modal('show');
  }

  patchHomeApprovalDetails() {
    this.homeApprovalForm.value.object_id=this.applicantId;
    this._commonHttpService.patch(
      this.currentHomeApprovalId,
      this.homeApprovalForm.value,
      'publicproviderhomeapproval'
    ).subscribe(
      (response) => {
        this._alertService.success("Information updated successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to update information");
      }

    );
    (<any>$('#add-homeApproval')).modal('hide');

  }

  saveHomeApprovalDetails() {
    this.homeApprovalForm.value.object_id=this.applicantId;
    this._commonHttpService.create(
      this.homeApprovalForm.value,
      'publicproviderhomeapproval'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-homeApproval')).modal('hide');
  }
  deleteHomeApprovalDetails(homeApproval){
  this.currentHomeApprovalId=homeApproval.home_study_visit_id;
  (<any>$('#delete-homeApproval')).modal('show');
  }
  cancelDelete() {
    this.currentHomeApprovalId = null;
    (<any>$('#delete-homeApproval')).modal('hide');
  }
  conformdeleteHomeApprovalDetails() {
    this._commonHttpService.remove(
      this.currentHomeApprovalId,
      this.homeApprovalForm.value,
      'publicproviderhomeapproval'
    ).subscribe(
      (response) => {
        this._alertService.success("Information Deleted successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to Delete information");
      }

    );
    (<any>$('#delete-homeApproval')).modal('hide');
  }


}
