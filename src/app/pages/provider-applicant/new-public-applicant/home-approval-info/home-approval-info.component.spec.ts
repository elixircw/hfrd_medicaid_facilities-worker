import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeApprovalInfoComponent } from './home-approval-info.component';

describe('HomeApprovalInfoComponent', () => {
  let component: HomeApprovalInfoComponent;
  let fixture: ComponentFixture<HomeApprovalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeApprovalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeApprovalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
