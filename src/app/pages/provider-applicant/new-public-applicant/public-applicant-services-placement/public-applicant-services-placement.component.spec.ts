import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantServicesPlacementComponent } from './public-applicant-services-placement.component';

describe('PublicApplicantServicesPlacementComponent', () => {
  let component: PublicApplicantServicesPlacementComponent;
  let fixture: ComponentFixture<PublicApplicantServicesPlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantServicesPlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantServicesPlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
