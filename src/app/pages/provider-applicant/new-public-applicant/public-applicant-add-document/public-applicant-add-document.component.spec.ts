import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantAddDocumentComponent } from './public-applicant-add-document.component';

describe('PublicApplicantAddDocumentComponent', () => {
  let component: PublicApplicantAddDocumentComponent;
  let fixture: ComponentFixture<PublicApplicantAddDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantAddDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantAddDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
