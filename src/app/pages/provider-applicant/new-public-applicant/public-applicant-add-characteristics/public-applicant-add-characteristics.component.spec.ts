import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantAddCharacteristicsComponent } from './public-applicant-add-characteristics.component';

describe('PublicApplicantAddCharacteristicsComponent', () => {
  let component: PublicApplicantAddCharacteristicsComponent;
  let fixture: ComponentFixture<PublicApplicantAddCharacteristicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantAddCharacteristicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantAddCharacteristicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
