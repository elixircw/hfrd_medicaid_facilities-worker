import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantContactComponent } from './public-applicant-contact.component';

describe('PublicApplicantContactComponent', () => {
  let component: PublicApplicantContactComponent;
  let fixture: ComponentFixture<PublicApplicantContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
