import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPublicApplicantComponent } from './new-public-applicant.component';

describe('NewPublicApplicantComponent', () => {
  let component: NewPublicApplicantComponent;
  let fixture: ComponentFixture<NewPublicApplicantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPublicApplicantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPublicApplicantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
