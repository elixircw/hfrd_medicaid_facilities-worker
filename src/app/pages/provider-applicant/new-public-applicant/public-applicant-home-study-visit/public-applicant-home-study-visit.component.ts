import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import * as moment from 'moment';
import { PaginationRequest } from '../../../../@core/entities/common.entities';

@Component({
  selector: 'public-applicant-home-study-visit',
  templateUrl: './public-applicant-home-study-visit.component.html',
  styleUrls: ['./public-applicant-home-study-visit.component.scss']
})
export class PublicApplicantHomeStudyVisitComponent implements OnInit {

  homeStudyVisitForm: FormGroup;
  applicantId: string;
  homeStudyVisitInfo = [];
  currentHomeVisitId: string;
  isEditHomeVisit: boolean = false;
  // availableHousehold = [];
  addresses = [];
  addressReadOnly: boolean;
  addedPersons: any[];
  isFormReset: boolean = false;

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.applicantId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.formIntilizer();
    this.getInformation();
    this.setupChangeSubscribers();
    this.getProviderHousehold();
  }

  formIntilizer() {
    this.homeStudyVisitForm = this.formBuilder.group(
      {
        object_id: [this.applicantId],
        interview_date: [null, [Validators.required]],
        interview_start_time: [null, [Validators.required]],
        interview_end_time: [null, [Validators.required]],
        duration: null,
        interview_location: [''],
        personid: [null, [Validators.required]],
        narrative: [null],
        islocationhome: [null]
      }
    );
  }
  clearNotes() {
    this.isFormReset = true;
    this.homeStudyVisitForm.reset();
  }
  addHomeVisit() {
    this.isFormReset = false;
    this.isEditHomeVisit = false;
  }  // private getInformation() {
  //   this._commonHttpService.getArrayList(
  // 		{
  // 			method: 'get',
  // 			where : { object_id : this.applicantId }
  // 		},
  // 		'publicproviderhomestudyvisit?filter'
  // 	).subscribe(response => {
  // 		this.homeStudyVisitInfo = response	

  // 	});
  // }
  private getInformation() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: { object_id: this.applicantId }
      },
      'publicproviderhomestudyhouseholdmapping/gethomestudyhousehold'
    ).subscribe(response => {
      this.homeStudyVisitInfo = response;
      console.log('this.homeStudyVisitInfo', this.homeStudyVisitInfo);
    });
  }

  edithomeStudyVisitInfo(homeVisit) {
    this.isEditHomeVisit = true;
    const data = Object.assign({}, homeVisit); ;
    this.currentHomeVisitId = data.home_study_visit_id;
    data.interview_start_time = moment.utc(data.interview_start_time).format('HH:mm');
    data.interview_end_time = moment.utc(data.interview_end_time).format('HH:mm');
    data.personid = (Array.isArray(data.personid)) ? data.personid.map(data => data.personid) : null;
    this.homeStudyVisitForm.patchValue(data);
    (<any>$('#add-homeVisit')).modal('show');
  }

  patchHomeVisitDetails() {
    if (this.homeStudyVisitForm.invalid) {
      this._alertService.error('Please fill required feilds');
      return false;
    }
    // his.homeStudyVisitForm.value.object_id = this.applicantId;
    const payload = this.homeStudyVisitFormPayload();
    payload.home_study_visit_id = this.currentHomeVisitId;

    this._commonHttpService.create(
      payload,
      'publicproviderhomestudyvisit/add'
    ).subscribe(
      (response) => {
        this._alertService.success('Information updated successfully!');
        this.getInformation();
      },
      (error) => {
        this._alertService.error('Unable to update information');
      }

    );
    (<any>$('#add-homeVisit')).modal('hide');

  }

  homeStudyVisitFormPayload() {
    const visitDetails = this.homeStudyVisitForm.getRawValue();
    let person = [];
    if (visitDetails.personid.length > 0) {
      visitDetails.personid.forEach(element => {
        person.push({'personid' : element});
      });
    }
    visitDetails.persons = person;
    const startTime = moment(this.homeStudyVisitForm.getRawValue().interview_date).format('MM/DD/YYYY') + ' ' + this.homeStudyVisitForm.getRawValue().interview_start_time;
    const endTime = moment(this.homeStudyVisitForm.getRawValue().interview_date).format('MM/DD/YYYY') + ' ' + this.homeStudyVisitForm.getRawValue().interview_end_time;
    visitDetails.interview_start_time = moment(startTime).format();
    visitDetails.interview_end_time = moment(endTime).format();
    visitDetails.object_id = this.applicantId
    return visitDetails;
  }


  saveHomeVisitDetails() {
    //this.homeStudyVisitForm.value.object_id=this.applicantId;
    if (this.homeStudyVisitForm.invalid) {
      this._alertService.error('Please fill required feilds');
      return false;
    }
    const payload = this.homeStudyVisitFormPayload();

    this._commonHttpService.create(
      payload,
      'publicproviderhomestudyvisit/add'
    ).subscribe(
      (response) => {
        this._alertService.success('Details saved successfully!');
       // this.saveSelectedPerson(response);
       // this.saveNote(payload);
       this.getInformation();
       this.clearNotes();
      },
      (error) => {
        this._alertService.error('Unable to save, please try again.');
      }
    );
    (<any>$('#add-homeVisit')).modal('hide');
  }
  saveSelectedPerson(response) {
    var payloadArray = [];
    for (var i = 0; i < this.homeStudyVisitForm.value.personid.length; i++) {
      let payload = {}
      payload['personid'] = this.homeStudyVisitForm.value.personid[i];
      payload['home_study_visit_id'] = response.home_study_visit_id;
      payloadArray.push(payload);
    }
    console.log(payloadArray);
    this._commonHttpService.create(
      payloadArray,
      'publicproviderhomestudyhouseholdmapping/'
    ).subscribe(
      (response) => {
        this._alertService.success('Information saved successfully!');
        this.getInformation();
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
    (<any>$('#add-homeVisit')).modal('hide');
    this.homeStudyVisitForm.reset();
  }


  deleteHomeVisitDetails(homeVisit) {
    this.currentHomeVisitId = homeVisit.home_study_visit_id;
    (<any>$('#delete-homeVisit')).modal('show');
  }
  cancelDelete() {
    this.currentHomeVisitId = null;
    (<any>$('#delete-homeVisit')).modal('hide');
  }
  conformdeleteHomeVisitDetails() {
    this._commonHttpService.remove(
      this.currentHomeVisitId,
      this.homeStudyVisitForm.value,
      'publicproviderhomestudyvisit/'
    ).subscribe(
      (response) => {
        this._alertService.success('Information Deleted successfully!');
        this.getInformation();
      },
      (error) => {
        this._alertService.error('Unable to Delete information');
      }

    );
    (<any>$('#delete-homeVisit')).modal('hide');
  }


  validationAlert(message: string) {
    this._alertService.error(message);
  }

  validateTimeFields(): boolean {
    if (!this.homeStudyVisitForm.controls['interview_date'].value && !this.isFormReset) {
      this.validationAlert('Please enter interview date');
      return false;
    } else if (!this.homeStudyVisitForm.controls['interview_start_time'].value && !this.isFormReset) {
      this.validationAlert('Please enter start time');
      return false;
    } else if (!this.homeStudyVisitForm.controls['interview_end_time'].value &&
      this.homeStudyVisitForm.controls['interview_end_time'].dirty && !this.isFormReset) {
      this.validationAlert('Please enter end time');
      return false;
    }
    return true;
  }

  setupChangeSubscribers() {
    this.homeStudyVisitForm.controls['interview_end_time'].valueChanges
      .subscribe(form => {
        if (this.validateTimeFields()) {
          this.computeDuration();
        }
      });
    this.homeStudyVisitForm.controls['interview_start_time'].valueChanges
      .subscribe(form => {
        if (this.validateTimeFields()) {
          this.computeDuration();
        }
      });
  }

  computeDuration() {
    const startTime = moment(moment(this.homeStudyVisitForm.getRawValue().interview_date).format('MM/DD/YYYY') + ' ' + this.homeStudyVisitForm.getRawValue().interview_start_time);
    const endTime = moment(moment(this.homeStudyVisitForm.getRawValue().interview_date).format('MM/DD/YYYY') + ' ' + this.homeStudyVisitForm.getRawValue().interview_end_time);

    if (startTime.isAfter(endTime)) {
      this.validationAlert('Start time must be before end time!');
    } else {
      const currentTrainingHours = moment.duration(endTime.diff(startTime)).asHours();
      if (currentTrainingHours) {
        this.homeStudyVisitForm.patchValue({
          'duration': currentTrainingHours
        });
      }
    }
  }
  getProviderHousehold() {
    const inputRequest = {
      intakenumber: this.applicantId
    };
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: inputRequest
        }),
        'People/getpersondetailcw?filter'
      ).subscribe(response => {
        this.addedPersons = response.data;
      });
    // this._commonHttpService.getArrayList(
    //   {
    //     method: 'get',
    //     nolimit: true,
    //     where: { object_id: this.applicantId },
    //     order: 'household_member_id desc'
    //   },
    //   'publicproviderapplicanthousehold?filter'
    // ).subscribe(
    //   (response) => {
    //     this.availableHousehold = response;
    //     console.log("Rahul-TableData:" + JSON.stringify(this.availableHousehold));
    //   },
    //   (error) => {
    //     this._alertService.error("Unable to retrieve Household member");
    //   }
    // );
  }

  saveNote(HomeStudyResponse) {
    const obj = {
      provider_applicant_id: HomeStudyResponse.object_id,
      communication_location: HomeStudyResponse.interview_location,
      type_of_contact: 'Home Stydy Visit',
      purpose_of_contact: 'Home Visit',
      narrative: HomeStudyResponse.narrative,
      communication_date: HomeStudyResponse.interview_date,
      appointment_time: HomeStudyResponse.interview_start_time,
    }
    console.log(JSON.stringify(obj));
    console.log(obj, 'body of contact');

    obj.provider_applicant_id = this.applicantId;
    // obj.communication_date = moment(obj.communication_date).format('YYYY/MM/DD') + ' ' + obj.appointment_time;

    this._commonHttpService.create(obj, 'providerapplicant/insertcommunicationinfo').subscribe(
      (response) => {

        this._alertService.success('contact saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save contact, please try again.');
        return false;
      }
    );
  }

  formatAddress() {

  }
  checkPatchAddress() {
    if (this.homeStudyVisitForm.value.islocationhome) {
      this.getApplicantAddresses();
      this.addressReadOnly = true;
    } else {
      this.homeStudyVisitForm.patchValue({
        interview_location: [''],
      })
      this.addressReadOnly = false;
    }
  }
  private getApplicantAddresses() {
    const applicant = this.addedPersons.find(person => {
      if (person.roles) {
        const applicantRole = person.roles.find(role => role.typedescription === 'Applicant');
        if (applicantRole) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }

    });
    if (applicant && applicant.address) {
      const address = applicant.address + ' ' + applicant.address2 + ' ' + applicant.city + ' ' + applicant.state;
      this.homeStudyVisitForm.patchValue({
        interview_location: address
      });
    }

  }

  getPersonName(name) {
    if (name) {
      return (Array.isArray(name)) ? name.map(data => data.personname).join() : '';
    }
    return '';
  }

  getInterviewTime(time) {
    if (time) {
      return moment.utc(time).format('HH:mm');
    } 
    return '';
  }
}
