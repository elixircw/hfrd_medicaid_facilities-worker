import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantHomeStudyVisitComponent } from './public-applicant-home-study-visit.component';

describe('PublicApplicantHomeStudyVisitComponent', () => {
  let component: PublicApplicantHomeStudyVisitComponent;
  let fixture: ComponentFixture<PublicApplicantHomeStudyVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantHomeStudyVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantHomeStudyVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
