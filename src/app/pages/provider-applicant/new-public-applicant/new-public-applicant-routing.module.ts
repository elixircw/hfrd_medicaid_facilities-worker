import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewPublicApplicantComponent } from './new-public-applicant.component';
import { CommonModule } from '@angular/common';
import { PublicProviderService } from './public-provider.service';
import { PublicApplicantBasicInfoComponent } from './public-applicant-basic-info/public-applicant-basic-info.component';
import { PublicApplicantAdditionalInfoComponent } from './public-applicant-additonal-info/public-applicant-additional-info.component';
import { PublicApplicantHomeInfoComponent } from './public-applicant-home-info/public-applicant-home-info.component';
import { PublicApplicantHouseholdComponent } from './public-applicant-household/public-applicant-household.component';
import { PublicApplicantCheckListComponent } from './public-applicant-check-list/public-applicant-check-list.component';
import { PublicApplicantContactComponent } from './public-applicant-contact/public-applicant-contact.component';
import { ApplicantSurveyComponent } from './applicant-survey/applicant-survey.component';
import { PublicApplicantNarrativeComponent } from './public-applicant-narrative/public-applicant-narrative.component';
// import { PublicApplicantAddDocumentComponent } from './public-applicant-add-document/public-applicant-add-document.component';
import { PublicApplicantPetInfoComponent } from './public-applicant-pet-info/public-applicant-pet-info.component';
import { PublicApplicantDecisionNewComponent } from './public-applicant-decision-new/public-applicant-decision-new.component';
import { AppConstants } from '../../../@core/common/constants';
import { HomeStudySurveyComponent } from '../../provider-management/public-provider-reconsideration/home-study-survey/home-study-survey.component';
import { PublicApplicantReferenceCheckComponent } from './public-applicant-reference-check/public-applicant-reference-check.component';
import { HomePlacementSpecificationComponent } from './home-placement-specification/home-placement-specification.component';
import { PublicApplicantPreServiceTrainingComponent } from './public-applicant-pre-service-training/public-applicant-pre-service-training.component';
import { PublicApplicantHomeStudyVisitComponent } from './public-applicant-home-study-visit/public-applicant-home-study-visit.component';
import { PublicApplicantServicesComponent } from './public-applicant-services/public-applicant-services.component';
import { AddDocumentComponent } from './add-document/add-document.component';




const routes: Routes = [
  {
    path: '',
    component: NewPublicApplicantComponent,
    // canActivate: [RoleGuard],
    children: [
      // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
      // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
      // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
      // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
    ]
  },
  {
    path: ':id',
    component: NewPublicApplicantComponent,
    resolve: {
      appData: PublicProviderService
    },
    children: [
      {
        path: '',
        redirectTo: 'program-info',
        pathMatch: 'full',
      },
      { path: 'program-info', component: PublicApplicantBasicInfoComponent },
      { path: 'applicants-info', component: PublicApplicantAdditionalInfoComponent },
      { path: 'home-info', component: PublicApplicantHomeInfoComponent },
      {
        path: 'household-members',
        loadChildren: '../../shared-pages/involved-persons/involved-persons.module#InvolvedPersonsModule',
        data: { source: AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_APPLICATION }
      },
      {
        path: 'background-checks',
        loadChildren: '../../shared-pages/person-background-check/person-background-check.module#PersonBackgroundCheckModule',
      },
      {
        path: 'services',
        component: PublicApplicantServicesComponent
      },
      {
        path: 'household-member',
        component: PublicApplicantHouseholdComponent
      },
      { path: 'checklist', component: PublicApplicantCheckListComponent },
      { path: 'contact-notes', component: PublicApplicantContactComponent },
      { path: 'applicant-survey', component: ApplicantSurveyComponent },
      { path: 'narrative', component: PublicApplicantNarrativeComponent },
      // { path: 'documents', component: PublicApplicantAddDocumentComponent },
      { path: 'documents', component: AddDocumentComponent },
      { path: 'home-study-visit', component: PublicApplicantHomeStudyVisitComponent },
      { path: 'reference-checks', component: PublicApplicantReferenceCheckComponent },
      { path: 'placmenet-specifications', component: HomePlacementSpecificationComponent },
      { path: 'pre-service-training', component: PublicApplicantPreServiceTrainingComponent },
      { path: 'pet-info', component: PublicApplicantPetInfoComponent },
      { path: 'review', component: PublicApplicantDecisionNewComponent }
    ]
    // canActivate: [RoleGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewPublicApplicantRoutingModule { }
