import { Component, Input, OnInit, OnChanges, Output } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatTableDataSource } from '@angular/material';
import { DataStoreService } from '../../../../@core/services';
import { FinanceAccountsPayableSearchEntry } from '../../../finance/_entities/finance-entity.module';
import { EventEmitter } from 'events';
export interface OrientationListTable {
	trainingNo: string;
	trainingDate: string;
	trainingDescription: string;
	trainingStartTime: string;
	trainingEndTime: string;
	sessionType: string;
}
@Component({
	selector: 'public-applicant-pre-service-training',
	templateUrl: './public-applicant-pre-service-training.component.html',
	styleUrls: ['./public-applicant-pre-service-training.component.scss']
})
export class PublicApplicantPreServiceTrainingComponent implements OnInit {

	selection = new SelectionModel<OrientationListTable>(true, []);
	dataSource: any;
	applicantId: string;
	duration: number = 0;
	trainingList = [];
	publicProviderApplicantInfo: any;
	attendee_first_nm: string;
	attendee_last_nm: string;
	PrideDuration: number = 0;
	assignedTrainingList = [];
	NonRepetingArray = [];
	trainingCompletedForm: FormGroup;
	trainingCompletedDate: string;
	isCompleted: string = 'false';
	displayedColumns: string[] = ['select', 'trainingNo', 'trainingDate', 'trainingDescription', 'trainingStartTime', 'trainingEndTime', 'sessionType'];
	@Output('isCompletedEventEmitter') isCompletedEventEmitter = new EventEmitter();
	constructor(private formBuilder: FormBuilder,
		private _commonHttpService: CommonHttpService,
		private _alertService: AlertService,
		private _router: Router, private route: ActivatedRoute,
		private _dataStoreService: DataStoreService
	) {
		this.applicantId = route.parent.snapshot.params['id'];
	}
	ngOnInit() {
		// this.getTrainingList();
		this.getassignedTrainingSessions();
		this.initTrainingCompletedForm();
		this.getCompletedTrainingInfo();
	}

	private initTrainingCompletedForm() {
		this.trainingCompletedForm = this.formBuilder.group({
			is_training_completed: null,
			training_completed_on: null,
			training_duration: ['']
		});


	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}
	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}
	getTrainingList() {
		this._commonHttpService.getArrayList(
			{
				method: 'get',
				where: { training_type: 'Pre-Service Training' }
			},
			'providerorientationtraining?filter'
		).subscribe(response => {
			var currentDate: Date = new Date();
			this.trainingList = response
			this.trainingList = this.trainingList.filter(
				x => (
					(new Date(x.training_date) >= currentDate)
					&&
					!this.assignedTrainingList.map(y => y.orientation_training_id).includes(x.orientation_training_id)
				)
			);
			this.dataSource = new MatTableDataSource<OrientationListTable>(this.trainingList);

		});
	}

	getApplicantInformation(): boolean {
		this.publicProviderApplicantInfo = this._dataStoreService.getData('publicProviderApplicantInfo');
		this.attendee_first_nm = this.publicProviderApplicantInfo['individual_applicant_first_nm'];
		this.attendee_last_nm = this.publicProviderApplicantInfo['individual_applicant_last_nm'];
		console.log('SIMAR', this.attendee_first_nm, ' ', this.attendee_last_nm);

		// if (!this.attendee_first_nm || this.attendee_first_nm === '' || !this.attendee_last_nm || this.attendee_last_nm === '') {
		// this._alertService.error('Applicant name is missing!')
		// 	return false;
		// } else {
		// 	return true;
		// }
		return true;
	}

	assignSelectedTrainingSessions() {
		const numSelected = this.selection.selected.length;
		var payload = {};
		payload = this.selection.selected;
		if (this.getApplicantInformation()) {
			for (var i = 0; i < numSelected; i++) {
				payload[i]['object_id'] = this.applicantId;
				payload[i]['phase'] = 'Application';
				payload[i]['attendee_first_nm'] = this.attendee_first_nm;
				payload[i]['attendee_last_nm'] = this.attendee_last_nm;
			}
			this._commonHttpService.create(
				payload,
				'providerorientationtrainingattendance/'
			).subscribe(
				(response) => {
					this._alertService.success('Orientations assigned successfully!');
					this.selection.clear();
					this.getassignedTrainingSessions();
					(<any>$('#assign-orientation')).modal('hide');
				},
				(error) => {
					this._alertService.error('Unable to assign Orientations');
				}
			);
		}
	}
	getassignedTrainingSessions() {
		this._commonHttpService.create(
			{
				where: {
					object_id: this.applicantId,
					training_type: 'Pre-Service Training'
				},
				method: 'post'
			},
			'providerorientationtrainingattendance/getassignedorientations'

		).subscribe(response => {
			this.assignedTrainingList = response;
			console.log('this.assignedTrainingList', this.assignedTrainingList);
			this.calculateDuration();
			this.getTrainingList();
		},
			(error) => {
				this._alertService.error('Unable to get assigned sessions, please try again.');
				console.log('get contact Error', error);
				return false;
			}
		);
	}
	calculateDuration() {
		for (let i = 0; i < this.assignedTrainingList.length; i++) {
			const curr = this.assignedTrainingList[i];
			if (this.assignedTrainingList[i].is_attended) {
				const x = parseFloat(this.assignedTrainingList[i].duration);
				this.duration = this.duration + x;
				console.log('this.duration1', this.assignedTrainingList[i].duration);
				console.log('this.duration', this.duration);
			}
			if (curr.session_type === 'PRIDE' && curr.is_attended) {
				if (!this.NonRepetingArray.includes(curr.session_no)) {
					this.NonRepetingArray.push(curr.session_no);
					const y = parseFloat(curr.duration);
					this.PrideDuration = this.PrideDuration + y;
					console.log('this.PrideDuration', this.NonRepetingArray);
				}
			}
		}

	}

	patchIntentToAttend(session) {
		console.log('session', session);
		var isIntentAttend: boolean;
		if (session.is_intent_to_attend === false) {
			isIntentAttend = true;
		} else if (session.is_intent_to_attend === true) {
			isIntentAttend = false;
		};
		var payload = {}
		payload['is_intent_to_attend'] = isIntentAttend;
		console.log('payload', payload);
		this._commonHttpService.patch(
			session.orientation_training_attendance_id,
			payload,
			'providerorientationtrainingattendance'
		).subscribe(
			(response) => {
				this._alertService.success('Orientations assigned successfully!');
				this.getassignedTrainingSessions();
			},
			(error) => {
				this._alertService.error('Unable to assign Orientations');
			});
	}

	saveCompletedTrainingData() {
		var payload = {};
		var finalPayload = {};
		payload = this.trainingCompletedForm.value;
		payload['applicant_id'] = this.applicantId;
		payload['training_duration'] = this.PrideDuration;
		payload['is_training_completed'] = false;
		finalPayload['training_info'] = payload;
		this._commonHttpService.patch(
			this.applicantId,
			finalPayload,
			'publicproviderapplicant'
		).subscribe(
			(response) => {
				this._alertService.success('Information saved successfully!');
				this.getCompletedTrainingInfo();
				(<any>$('#mark-complete')).modal('hide');

			},
			(error) => {
				this._alertService.error('Unable to save information');
			}

		);
	}

	private getCompletedTrainingInfo() {
		this._commonHttpService.getById(
			this.applicantId,
			'publicproviderapplicant'
		).subscribe(
			(response) => {
				if (response && response.training_info) {
					console.log('get response', response);
					this.trainingCompletedDate = response.training_info.training_completed_on;
					// this.publicApplicantBasicInfoForm.patchValue(response);
					this.trainingCompletedForm.patchValue({
						training_completed_on: response.training_info.training_completed_on,
					});
				}

			},
			(error) => {
				this._alertService.error('Unable to retrieve information');
			}

		);
	}

	resetForm() {
		this.trainingCompletedForm.reset();
	}
}
