import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicApplicantPreServiceTrainingComponent } from './public-applicant-pre-service-training.component';

describe('PublicApplicantPreServiceTrainingComponent', () => {
  let component: PublicApplicantPreServiceTrainingComponent;
  let fixture: ComponentFixture<PublicApplicantPreServiceTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicApplicantPreServiceTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApplicantPreServiceTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
