import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicantProgramInfo, ApplicantProfileInfo } from '../../../../pages/provider-portal-temp/current-application/_entities/portalApplicantModel';
import { AuthService, CommonHttpService, AlertService } from '../../../../@core/services';

import {applinfo } from '../_entities/newApplicantModel';
import { ProviderReferral } from '../../../provider-referral/new-referral/_entities/newreferralModel';

@Component({
  selector: 'applicant-information',
  templateUrl: './applicant-information.component.html',
  styleUrls: ['./applicant-information.component.scss']
})
export class ApplicantInformationComponent implements OnInit {
  applicantNumber: string;
  applicantProfileInfo: ApplicantProfileInfo = new ApplicantProfileInfo();

  @Input()
  profileOutputSubject$ = new Subject<applinfo>();
  @Input()
  referralinfooutputsubject$ = new Subject<ProviderReferral>();
  profileApplicantInfo: ProviderReferral;
  applicantInfo: applinfo;
  applicantProfileFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService, 
    private route: ActivatedRoute,
    private _commonHttpService: CommonHttpService) {
      this.applicantNumber = route.snapshot.params['id'];

      }

  ngOnInit() {
    //this.getApplicantProfileInformation();
    this.initializeApplicantProfileForm();


    this.applicantInfo = new applinfo;
    this.profileOutputSubject$.subscribe((data) => {
      if(data && data.mailing_address){
        data.mailing_address = data.mailing_address.replace(/,/g,'\n');
      }
      this.applicantInfo = data;
    });
    this.profileApplicantInfo = new ProviderReferral;
    this.referralinfooutputsubject$.subscribe((data) => {
      this.profileApplicantInfo = data;
    });
  }

  private initializeApplicantProfileForm() {
    this.applicantProfileFormGroup = this.formBuilder.group({
      tax_id_no: [null],
      provider_applicant_nm: [''],
      dba_name: [''],
      provider_applicant_first_nm: [''],
      provider_applicant_last_nm: [''],
      adr_url_tx: [''],
    });
    
  }

  getApplicantProfileInformation() {
    this._commonHttpService.create(
      {
        appid :this.applicantNumber      
      },
      'providerapplicantportal/getapplprofileinfo'
  ).subscribe(response => {      
    console.log("APPLICANT PROFILE INFORMATION")
    this.applicantProfileInfo = response.data;
    console.log(JSON.stringify(this.applicantProfileInfo));
  },
      (error) => {
        this._alertService.error('Unable to get profile information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
}

}
