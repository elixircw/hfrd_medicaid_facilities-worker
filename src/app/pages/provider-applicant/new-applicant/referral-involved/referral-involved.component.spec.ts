import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralInvolvedComponent } from './referral-involved.component';

describe('ReferralInvolvedComponent', () => {
  let component: ReferralInvolvedComponent;
  let fixture: ComponentFixture<ReferralInvolvedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralInvolvedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralInvolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
