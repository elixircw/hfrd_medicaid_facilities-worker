import { Component, OnInit, Input } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderReferral } from '../../../provider-referral/new-referral/_entities/newreferralModel';
import { ReferralUrlConfig } from '../../../provider-referral/provider-referral-url.config';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AlertService } from '../../../../@core/services';

@Component({
  selector: 'referral-involved',
  templateUrl: './referral-involved.component.html',
  styleUrls: ['./referral-involved.component.scss']
})
export class ReferralInvolvedComponent implements OnInit {

  referralInfo: ProviderReferral;
  referralnumber: string;

  constructor(private _commonHttpService: CommonHttpService,
              private _alertService: AlertService,
              private _router: Router,
              private route: ActivatedRoute)
              {
                this.referralnumber = route.snapshot.params['id'].replace('A','R');
                if (this.referralnumber.length == 14) {
                  this.referralnumber = this.referralnumber.slice(0, -1);
                }
              }

  ngOnInit() {
    this.referralInfo = new ProviderReferral;
    this.populateReferral();
  }

  private populateReferral() {
    this._commonHttpService.create(
      {
        method:'post',
        where: {
          referralnumber: this.referralnumber
            }
          },
            ReferralUrlConfig.EndPoint.Referral.getProviderReferralUrl
        ).subscribe(response => {
          this.referralInfo = response.data[0];
        },
        (error) => {
              this._alertService.error('Unable to fetch saved referral details.');
              console.log('get referral Error', error);
              return false;
                  }
              );           
          }
}