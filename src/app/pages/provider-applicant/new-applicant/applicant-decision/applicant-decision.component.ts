import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApplicationDecision } from '../_entities/newApplicantModel';
import { AuthService, CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { RoutingUser } from '../_entities/existingreferralModel';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { HttpHeaders } from '@angular/common/http';
import { NgxfUploaderService } from 'ngxf-uploader';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import { Subject } from 'rxjs/Subject';
import {applinfo } from '../_entities/newApplicantModel';
import { ApplicantUrlConfig } from '../../../provider-management/provider-management-url.config';

@Component({
  selector: 'applicant-decision',
  templateUrl: './applicant-decision.component.html',
  styleUrls: ['./applicant-decision.component.scss']
})
export class ApplicantDecisionComponent implements OnInit {

  decisionFormGroup: FormGroup;
  currentDecision: ApplicationDecision;
  roleId: AppUser;
  decisions = [];
  decisionsval=[];
  isSignDecisoin:boolean;
  applicantNumber: string;
  eventcode = 'PTA';

  // Assigning / Routing
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  selectedPerson: any;
  //isFinalDecisoin: boolean = false;
  isFinalDecisoin: boolean;

  isSupervisor: boolean;
  isGroup = false;

  //statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  signimage: any;
  isSignPresent: boolean;
  licenseNumber: String = '';
  hassubmitdecision = false;
  hastierapproved = false;
  signval = false;
  hidebtnval = false;
  
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _uploadService: NgxfUploaderService,
    private _dataStoreService: DataStoreService) {
    this.applicantNumber = route.snapshot.params['id'];
    //this.applicantNumber = 'A201800300025';
  }

  ngOnInit() {
    this.initializeDecisionForm();
    this.hassubmitdecision = this._authService.selectedRoleIs('Executive Director');
    this.hastierapproved = !this._authService.selectedRoleIs('Executive Director');   
    this.roleId = this._authService.getCurrentUser();
    this.getSignatureData();
    this.statusDropdownItems = [
      { 'text': 'Reject', 'value': 'Rejected' },
      { 'text': 'Approve', 'value': 'Approved' },
      { 'text': 'Return', 'value': 'Incomplete' },
    ];
    //this.roleId.user.userprofile.teamtypekey;

    if (this.roleId.role.name === 'Executive Director' || this.roleId.role.name === 'Provider_DJS_SecretaryDesignee') {
      this.isFinalDecisoin = true;
    } else {
      this.isFinalDecisoin = false;
    }
    if(this.roleId.role.name === 'Executive Director'){
      this.isSignDecisoin = true;
     }else {
      this.isSignDecisoin = false;
    }

    if (this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
    || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
    || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA' ) {
      this.eventcode = 'PTADJS';
    }
    this.getDecisions();
    this.getDecisionsval();
    this.getLicenseInformation();
    this._commonHttpService.signatureSource.subscribe((data) => {
        this.signimage = data;
    });
   }

  private getSignatureData() {
    this._commonHttpService .create(
    {
      method: 'post',
      where: {
        applicant_id: this.applicantNumber
      }
    },
    ApplicantUrlConfig.EndPoint.Applicant.getapplicantInfo
  )
  .subscribe(
      (response) => {
        if (response) {
          const data = response.data;
          this.signimage = data.uploadsignature;
          this.loadSignature();
        }
      },
        (error) => {
          this._alertService.error('Unable to fetch saved Applicant details.');
          return false;
        }
    );
}

  loadSignature() {
      this.decisionFormGroup.patchValue({
        signimage: (this.signimage) ? this.signimage : ''
      });
      this.checkIfSignPresent();
  }

  private checkIfSignPresent() {
    this.isSignPresent = Boolean(this.signimage);
    this._dataStoreService.setData('isSignPresent', this.isSignPresent);
    this._dataStoreService.setData('signImage', this.signimage);
  }

  initializeDecisionForm() {
    this.decisionFormGroup = this.formBuilder.group({
      status: [''],
      reason: [''],
      signimage: ['']
    });
  }


  getDecisions() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          eventcode: 'PTA',
          objectid: this.applicantNumber
        }
      },
      'providerapplicant/getapplicationdecisions'
    ).subscribe(decisionsarray => {

      this.decisions = decisionsarray.data;
    },
      (error) => {
        this._alertService.error('Unable to get decisions, please try again.');
        console.log('get decisions Error', error);
        return false;
      }
    );
  }

  getDecisionsval() {
    this._commonHttpService.create(
      {
        method:'post',
        where: 
        { eventcode: 'PTA',
          objectid :this.applicantNumber
       }        
      },
      'providerapplicant/gettierdecisions'
      //ApplicantUrlConfig.EndPoint.Applicant.getappointmentdetails
  ).subscribe(decisionsarray => {
    this.decisionsval = decisionsarray.data;
     },
      (error) => {
        this._alertService.error('Unable to get decisions, please try again.');
        console.log('get decisions Error', error);
        return false;
      }
    );
}
  submitDecision() {
    this._commonHttpService.create(
          {
            where: {
              eventcode: 'PTA',
              objectid: this.applicantNumber,
              fromroleid: this.roleId.role.name,
              remarks: this.decisionFormGroup.value.reason, //.get('reason'),
              status: this.decisionFormGroup.value.status, //.get('status')
              isfinalapproval: this.isFinalDecisoin,
              licenseNO: this.licenseNumber
            },
            method: 'post'
          },
          'providerapplicant/submitdecision'
        ).subscribe(result => {
          this._alertService.success('Decision Submitted successfully!');
          this.decisions.length = 0;
          this.getDecisions();
          this.getDecisionsval();
          this.hidebtnval = true;
         },
          (error) => {
            this._alertService.error('Unable to submit decision, please try again.');
            console.log('submit decision Error', error);
            return false;
          }
        );
    }

    // Assignment logic follows

  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
  }

  getRoutingUser() {
    //this.getServicereqid = modal.provider_referral_id;
    //  this.getServicereqid = 'R201800200374';
    // console.log(modal.provider_referral_id);
    // this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: this.eventcode },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  selectPerson(row) {
    this.selectedPerson = row;
    //console.log(this.selectedPerson);
  }

  getLicenseInformation() {
    this._commonHttpService.create(
      {
        method:'post',
        where: 
        {
          applicant_id :this.applicantNumber
      }        
      },
      'providerapplicant/getproviderapplicant'
  ).subscribe(response => {
    let licenseInfo = response.data;
    if (response.data) {
      this.licenseNumber = licenseInfo.license_no;
    }
  },
  (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
}

  assignUser() {
    // Doing the update of Status and assigning at the same time
    // In the same flow, first 'Submit' then 'Assign' will do them both
    // TODO: SIMAR - Put validations that status value has been selected before submitting
    console.log(this.isFinalDecisoin);

    if (this.selectedPerson) {
      this._commonHttpService
        .getPagedArrayList(
          new PaginationRequest({
            where: {
              eventcode: 'PTA',
              objectid: this.applicantNumber,
              fromroleid: this.roleId.role.name,
              tosecurityusersid: this.selectedPerson.userid,
              toroleid: this.selectedPerson.userrole,
              remarks: this.decisionFormGroup.value.reason, //.get('reason'),
              status: this.decisionFormGroup.value.status, //.get('status')
              isfinalapproval: this.isFinalDecisoin,
              licenseNO: this.licenseNumber
              // isreviewrequest: ,
            },
            method: 'post'
          }),
          'providerapplicant/submitdecision'
          //'Providerreferral/routereferralda'
        )
        .subscribe(result => {
          this._alertService.success('Application assigned successfully!');
          //  this.getAssignCase(1, false);
          this.closePopup();


          //Reset the decision table and get all new results
          this.decisions.length = 0;
          this.getDecisions();
          this.getDecisionsval();
          this.hidebtnval = true;  
      });
    } else {
      this._alertService.warn('Please select a person');
    }
  }

  closePopup() {
    (<any>$('#intake-caseassign')).modal('hide');
    (<any>$('#assign-preintake')).modal('hide');
    (<any>$('#reopen-intake')).modal('hide');
  }

  uploadAttachment() {  
    if (!this.signimage) {
      this._alertService.warn('Signature is empty.');
      return false;
    }
    const blob = this.dataURItoBlob(this.signimage);
    const finalImage = new File([blob], 'image.png');
    this._uploadService
      .upload({
        url:
          AppConfig.baseUrl +
          '/' +
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment
            .UploadAttachmentUrl +
          '?access_token=' +
          this.roleId.id +
          '&srno=userprofile',
        headers: new HttpHeaders()
          .set('access_token', this.roleId.id)
          .set('srno', 'userprofile')
          .set('ctype', 'file'),
        filesKey: ['file'],
        files: finalImage,
        process: true
      })
      .subscribe(
        response => {
          if (response && response.data && response.data.s3bucketpathname) {
            this._commonHttpService.create({
              usersignatureurl: response.data.s3bucketpathname
            }, 'admin/userprofile/updateusersignature').subscribe(res => {
              if (res.count) {
                this.signval = true;
                this._alertService.success('Signature added successfully.');
                this.patchSignatureBlob();
                }
            });
          }
        },
        err => {
          console.log(err);
        }
      );
  }
  statusfn(value){
   this.signval = true;
   this.hidebtnval = false;
  }
  patchSignatureBlob() {
    if (this.signimage) {
      const uploadData = {
        applicant_id: this.applicantNumber,
        uploadsignature: this.signimage
      };

      this._commonHttpService.patch(this.applicantNumber, uploadData,
        'providerapplicant').subscribe(result => {
            this.loadSignature();
      });
    }
  }

  dataURItoBlob(dataURI) {
    const binary = window.atob(dataURI.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
        type: 'image/png'
    });
  }

}
