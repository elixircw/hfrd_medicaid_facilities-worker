import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantDecisionComponent } from './applicant-decision.component';

describe('ApplicantDecisionComponent', () => {
  let component: ApplicantDecisionComponent;
  let fixture: ComponentFixture<ApplicantDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
