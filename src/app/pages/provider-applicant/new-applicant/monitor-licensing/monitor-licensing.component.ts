import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { AddChecklistComponent } from '../applicant-checklist/add-checklist/add-checklist.component';

import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { ApplicantUrlConfig } from '../../provider-applicant-url.config';
import { Subject } from 'rxjs';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
@Component({
  selector: 'monitor-licensing',
  templateUrl: './monitor-licensing.component.html',
  styleUrls: ['./monitor-licensing.component.scss']
})
export class MonitorLicensingComponent implements OnInit {
  id: string;

  @ViewChild('addCheckList', { read: ViewContainerRef }) container: ViewContainerRef;

  constructor(private _cfr: ComponentFactoryResolver, private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private _alertService: AlertService) {
    this.id = route.snapshot.params['id'];
  }

  monitorForm: FormGroup;
  isMonthDisabled: boolean = true;
  isYearDisabled: boolean = true;
  activityTaskStatusFormGroup: FormGroup;
  tasktypestatus = [];
  taskList = [];
  @Input()
  programTypeInputForMonitoring = new Subject<string>();
  programType: string;


  periodicDropDown = ['Monthly', 'Quarterly', 'Initial', 'Periodic', 'Mid-Year', 'Re-Licensure'];
  timeOfVisit = ['Night', 'Weekend', 'Regular'];
  visitSchedule = ['Announced', 'Unannounced'];
  monthDropDown = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  yearDropDown = [];

  ngOnInit() {
    for (var i = 2000; i <= new Date().getFullYear(); ++i) {
      this.yearDropDown.push(i);
    }
    this.monitorForm = this.formBuilder.group({
      periodic: [''],
      monthly: [''],
      yearly: null
    });
    this.activityTaskStatusFormGroup = this.formBuilder.group({
      task: this.formBuilder.array([])
    });
    // this.getTaskList();
    this.programTypeInputForMonitoring.subscribe((data) => {
      this.programType = data;
    })
    this.tasktypestatus = ['Completed', 'Incomplete', 'N/A'];
  }

  createTaskForm() {
    return this.formBuilder.group({
      checklist_id: [''],
      checklist_task: [''],
      commnts: [''],
      completeddate: [''],
      status: ['']
    });
  }

  private getTaskList() {
    this.taskList.length = 0;
    this._commonHttpService.create(
      {
        method: 'post',
        applicant_id: this.id,
        category: 'Monitoring',
        from: 'Monitoring',
        monitoring_type: this.monitorForm.value.periodic,
        monitoring_year: this.monitorForm.value.yearly,
        monitoring_period: this.monitorForm.value.monthly
      },
      ApplicantUrlConfig.EndPoint.Applicant.getapplicantchecklist
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      this.setFormValues();
    });
  }

  private buildTaskForm(x): FormGroup {
    return this.formBuilder.group({
      checklist_id: x.checklist_id,
      checklist_task: x.checklist_task,
      commnts: x.commnts,
      completeddate: x.completeddate,
      status: x.status
    });
  }

  setFormValues() {
    this.activityTaskStatusFormGroup.setControl('task', this.formBuilder.array([]));
    const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    this.taskList.forEach((x) => {
      control.push(this.buildTaskForm(x));
    })
    console.log(this.activityTaskStatusFormGroup.value);
    console.log(this.activityTaskStatusFormGroup.controls);
  }

  openMonitoringCheckList() {
    // check and resolve the component
    let comp = this._cfr.resolveComponentFactory(AddChecklistComponent);
    // Create component inside container
    let expComponent = this.container.createComponent(comp);
    // see explanations
    expComponent.instance._ref = expComponent;
    expComponent.instance.category = 'Monitoring';
    expComponent.instance.type = this.programType;
    expComponent.instance.onSaveEventEmitter.subscribe(() => {
      console.log('Event Captured');
      // this.getTaskList();
    });
  }

  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.monitorForm.controls['monthly'].setValue('');
    this.monitorForm.controls['yearly'].setValue('');
  }

  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
    let objVal = Object.values(this.activityTaskStatusFormGroup.value);
    // objVal.shift();
    // let merged = [].concat.apply([], objVal);
    this._commonHttpService.endpointUrl = 'providerapplicant/updatechecklist';
    let requestData = {
      'applicant_id': this.id,
      'task': objVal[0]
    };
    this._commonHttpService.create(requestData).subscribe(
      (response) => {
        if (response) {
          this._alertService.success('Task updated successfully');
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  onChange() {
    this.resetDropDowns();
    if (this.monitorForm.value.periodic == 'Monthly') {
      this.isMonthDisabled = false;
      this.isYearDisabled = false;
    } else {
      this.isYearDisabled = false;
    }
  }

  viewActivity() {
    this.getTaskList();
  }

}
