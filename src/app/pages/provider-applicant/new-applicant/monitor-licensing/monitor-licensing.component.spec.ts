import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorLicensingComponent } from './monitor-licensing.component';

describe('MonitorLicensingComponent', () => {
  let component: MonitorLicensingComponent;
  let fixture: ComponentFixture<MonitorLicensingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorLicensingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorLicensingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
