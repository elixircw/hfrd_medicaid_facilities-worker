import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewApplicantComponent } from './new-applicant.component';
import { NewApplicantRoutingModule } from './new-applicant-routing.module';
import { ApplicantAttachmentsComponent } from './applicant-attachments/applicant-attachments.component';
import { AttachmentUploadComponent } from './applicant-attachments/attachment-upload/attachment-upload.component';
import { EditAttachmentComponent } from './applicant-attachments/edit-attachment/edit-attachment.component';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule
} from '@angular/material';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { IntakeAppointmentComponent } from './intake-appointment/intake-appointment.component';
import { ApplicantStaffsComponent } from './applicant-staffs/applicant-staffs.component';
import { ApplicantChecklistComponent } from './applicant-checklist/applicant-checklist.component';
// import { ReferralDecisionComponent } from '../../provider-referral/new-referral/referral-decision/referral-decision.component';
import { ApplicantInformationComponent } from './applicant-information/applicant-information.component';
import { ReferralInvolvedComponent } from './referral-involved/referral-involved.component';
import { ApplicantNarrativeComponent } from './applicant-narrative/applicant-narrative.component';
import { ApplicantDocumentCreatorComponent } from './applicant-document-creator/applicant-document-creator.component';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { ApplicantDecisionComponent } from './applicant-decision/applicant-decision.component';
// import { ApplicantAssessmentsComponent } from './applicant-assessments/applicant-assessments.component';
import { ApplicantLicenseComponent } from './applicant-license/applicant-license.component';
import { SiteinspectionComponent } from './applicant-checklist/siteinspection/siteinspection.component';
import { WrittenpoliciesComponent } from './applicant-checklist/writtenpolicies/writtenpolicies.component';
import { AddChecklistComponent } from './applicant-checklist/add-checklist/add-checklist.component';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ApplicantContactComponent } from './applicant-contact/applicant-contact.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { SignatureFieldComponent } from './applicant-decision/signature-field/signature-field.component';
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  imports: [
    CommonModule, NewApplicantRoutingModule, MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    NgxfUploaderModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgxPaginationModule,
    SignaturePadModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [NewApplicantComponent,
    ApplicantAttachmentsComponent,
    AttachmentUploadComponent,
    EditAttachmentComponent,
    IntakeAppointmentComponent,
    ApplicantStaffsComponent,
    ApplicantChecklistComponent,
    ApplicantInformationComponent,
    ReferralInvolvedComponent,
    // ReferralDecisionComponent,
    ApplicantNarrativeComponent,
    ApplicantDocumentCreatorComponent,
    ApplicantDecisionComponent,
    // ApplicantAssessmentsComponent,
    ApplicantLicenseComponent,
    SiteinspectionComponent,
    WrittenpoliciesComponent,
    AddChecklistComponent,
    ApplicantContactComponent,
    ApplicantLicenseComponent,
    SignatureFieldComponent
  ],
  entryComponents: [AddChecklistComponent],
  providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService]
})
export class NewApplicantModule { }
