import { Component, OnInit, ViewChild } from '@angular/core';
import { AttachmentIntakes, GeneratedDocuments, applinfo, ProviderReferral } from './_entities/newApplicantModel';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { ApplicantUrlConfig } from '../provider-applicant-url.config';
import { AlertService } from '../../../@core/services/alert.service';
import { ReferralUrlConfig } from '../../provider-referral/provider-referral-url.config';
import { ApplicantAttachmentsComponent } from './applicant-attachments/applicant-attachments.component';
import { AuthService } from '../../../@core/services';
import { AppUser } from '../../../@core/entities/authDataModel';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { RoutingUser } from '../../cjams-dashboard/_entities/dashBoard-datamodel';

@Component({
  selector: 'new-applicant',
  templateUrl: './new-applicant.component.html',
  styleUrls: ['./new-applicant.component.scss']
})
export class NewApplicantComponent implements OnInit {

  addAttachementSubject$ = new Subject<AttachmentIntakes[]>();
  generatedDocuments$ = new Subject<string[]>();
  generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
  generateDocumentInput$ = new Subject<GeneratedDocuments[]>();
  profileOutputSubject$ = new Subject<applinfo>();
  referralinfooutputsubject$ = new Subject<ProviderReferral>();

  
  applicantInfo: applinfo = new applinfo();
  applicantNumber: string;  referralInfo: ProviderReferral = new ProviderReferral() ;
  referralnumber: string;



  //Check if the applicant has been tier approved, and now has a license
  isLicensed: boolean = false;
  licenseNumber: string;
  programTypeInput = new Subject<string>();
  programTypeInputForMonitoring = new Subject<string>();
  currentUser: AppUser;
  isLengthGrater:boolean;
  asssignedDetails:any;
  selectedPerson: any;
  isSupervisor: boolean;
  assignedInSection= [];
  eventcode:string;
  currentData: any;
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  isDJS: Boolean = false;

  @ViewChild(ApplicantAttachmentsComponent) documentGenerator;


  constructor(private _commonHttpService: CommonHttpService,private _alertService: AlertService,
    private _authService: AuthService,
    private _router: Router,
    private route: ActivatedRoute
  ) { 
    this.applicantNumber = route.snapshot.params['id'];
    this.referralnumber = route.snapshot.params['id'].replace('A','R');
                if (this.referralnumber.length == 14) {
                  this.referralnumber = this.referralnumber.slice(0, -1);
                }
  }

  ngOnInit() {
    this.currentUser = this._authService.getCurrentUser();
    this.isLengthGrater = false;
    this.assignDetails();
    this.getAssignedUsers();
    this.populateApplicant();
    this.populateReferral();
    if (this.currentUser.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
    || this.currentUser.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.currentUser.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
    || this.currentUser.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.currentUser.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA' ) {
      this.isDJS = true;
    }
  }

  private populateApplicant() {
        this._commonHttpService .create(
        {
          method:'post',
          where: {
            applicant_id:this.applicantNumber
          }
          
        },
        ApplicantUrlConfig.EndPoint.Applicant.getapplicantInfo
      )
      .subscribe(
          (response) => {
            
            this.applicantInfo = response.data;
            this.profileOutputSubject$.next(this.applicantInfo);

            this.programTypeInput.next(response.data['prgram']);
            this.programTypeInputForMonitoring.next(response.data['prgram']);

            //if the applicant has been approved, then they will have a license number
            this.licenseNumber = this.applicantInfo.license_no;
            if (this.licenseNumber) {
              this.isLicensed = true;
            }
          },
            (error) => {
              this._alertService.error('Unable to fetch saved Applicant details.');
              console.log('Get applicant details Error', error);
              return false;
            }
        );
    }


    private populateReferral() {
      this._commonHttpService.create(
        {
          method:'post',
          where: {
            referralnumber: this.referralnumber
              }
            },
              ReferralUrlConfig.EndPoint.Referral.getProviderReferralUrl
          ).subscribe(response => {
            //console.log("response", response);
            this.referralInfo = response.data[0];
            this.referralinfooutputsubject$.next(this.referralInfo);
            //console.log(this.referralInfo,"referral information");
          },
          (error) => {
                this._alertService.error('Unable to fetch saved referral details.');
                console.log('get referral Error', error);
                return false;
                    }
                );
              
            }  private assignDetails(){

    this.asssignedDetails={
      eventcode:'PAOW',
      fromsecurityusersid:'',
      tosecurityusersid:'',
      fromroleid:'',
      toroleid:'',
      objectid:this.applicantNumber,
    }
  }

  private getAssignedUsers() {
    this.assignedInSection = [];
    
    this._commonHttpService.create(
        {
          method:'post',
          where: {
                    eventcode : 'PAOW',
                    objectid:this.applicantNumber
          } 
        },
        'providerassignmentownership/getassignownership'
        )
      .subscribe(
            (response) => { 
              if(response.length>=2){
                this.isLengthGrater=true
              };

              for(var i=0;i<response.length;i++){ 
                this.currentData={"username": response[i].ownername};
                this.assignedInSection.push(this.currentData);
                }
              },
              (error) => {
                    this._alertService.error('Unable to fetch Ownership details.');
                    console.log('Get ownership details Error', error);
                    return false;
                    }        
                );
          }

    listUser(assigned: string) {
      this.selectedPerson = '';
        this.getUsersList = [];
        this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
          this.getUsersList = this.getUsersList.filter(res => {
            if (res.issupervisor === false) {
              this.isSupervisor = false;
              return res;
            }
          });
        }
      }


      getRoutingUser(modal) {
        this.getUsersList = [];
        this.originalUserList = [];
        this._commonHttpService
          .getPagedArrayList(
            new PaginationRequest({
              where: { appevent: 'PTA' },
              method: 'post'
            }),
            'providerassignmentownership/getroutingusers'
          )
          .subscribe(result => {
            // this.userNames();
            this.getUsersList = result.data;
            this.originalUserList = this.getUsersList;
            this.listUser('TOBEASSIGNED');
          });
      }
      
      selectPerson(row) {
        this.selectedPerson = row;
        // THIS IS THE PERSON LOGGED IN WHICH IS (FROM)
        this.asssignedDetails.fromsecurityusersid=this.currentUser.user.securityusersid;
        this.asssignedDetails.fromroleid=this.currentUser.role.name;

        // THIS IS THE PERSON WE SELECTED ON THE SCREEN WHICH IS (TO)
        this.asssignedDetails.tosecurityusersid=row.userid;
        this.asssignedDetails.toroleid=row.userrole;
      
    }

    assignNewUser() {
      if (this.selectedPerson){
        var payload = {}
        payload = this.asssignedDetails;                  
        this._commonHttpService.create(
            payload,
          'providerassignmentownership'
        ).subscribe(
          (response) => {
            this._alertService.success("Ownership assigned successfully!");
            this.getAssignedUsers();
            (<any>$('#intake-caseassignnew')).modal('hide');
          },
          (error) => {
            this._alertService.error("Unable to save Ownership");
          });
        }
        
      }
      
      
    generateAndDisplayLicense(event) {
      var currentDocument = event.generateLicenseDocument;
      this.documentGenerator.generateNewDocument(currentDocument);
      this.documentGenerator.downloadDocument(currentDocument);
    }

}
