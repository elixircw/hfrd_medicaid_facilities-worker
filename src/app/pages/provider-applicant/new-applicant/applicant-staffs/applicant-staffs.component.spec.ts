import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantStaffsComponent } from './applicant-staffs.component';

describe('ApplicantStaffsComponent', () => {
  let component: ApplicantStaffsComponent;
  let fixture: ComponentFixture<ApplicantStaffsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantStaffsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantStaffsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
