import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { ApplicantUrlConfig } from '../../provider-applicant-url.config'
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../../@core/services';

@Component({
  selector: 'applicant-staffs',
  templateUrl: './applicant-staffs.component.html',
  styleUrls: ['./applicant-staffs.component.scss']
})
export class ApplicantStaffsComponent implements OnInit {

  staffArray : any;
  applicantNumber: string;
  assignedStaff:any[];
  applicantId: string;

  constructor(private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _alertService: AlertService,

    ) { 
      this.applicantId = route.snapshot.params['id'];
    }

    ngOnInit() {
      this.getassignedstaff();
    }

    getassignedstaff(){
      this._commonHttpService.create(
        {
          where: { provider_applicant_id: this.applicantId },
          method: 'post'
        },
        'providerstaff/getassignedstaff'

        ).subscribe(response => {		
          this.assignedStaff = response;
        },
        (error) => {
          this._alertService.error('Unable to get assigned staffs, please try again.');
          return false;
      }
    );
  }
}
