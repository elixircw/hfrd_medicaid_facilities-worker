import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../../@core/services/auth.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { HttpService } from '../../../../@core/services/http.service';
import { AppConfig } from '../../../../app.config';
import { GenericService } from '../../../../@core/services';
import { Assessments } from '../_entities/newApplicantModel';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { ApplicantUrlConfig } from '../../provider-applicant-url.config';
import { Observable } from 'rxjs';

@Component({
  selector: 'applicant-assessments',
  templateUrl: './applicant-assessments.component.html',
  styleUrls: ['./applicant-assessments.component.scss']
})
export class ApplicantAssessmentsComponent implements OnInit {
  applicantNumber : string;
  roleId: AppUser;
  paginationInfo: PaginationInfo = new PaginationInfo();
  startAssessment$: Observable<Assessments[]>;
  totalRecords$: Observable<number>;
  constructor(
    private route: ActivatedRoute,
    private _authService: AuthService,
    private _http: HttpService,
    private _service: GenericService<Assessments>,
  ) { 
    this.applicantNumber = route.snapshot.params['id'];
    //console.log(route.snapshot.parent.parent.parent.params['id']);
  }

  ngOnInit() {
    this.roleId = this._authService.getCurrentUser();
    this.getPage(1);
  }

  getPage(page: number) {
    this._http.overrideUrl = false;
    this._http.baseUrl = AppConfig.baseUrl;
    const source = this._service
        .getPagedArrayList(
            new PaginationRequest({
                page: page,
                limit: this.paginationInfo.pageSize,
                where: {"intakeservicerequesttypeid":"","intakeservicerequestsubtypeid":"","agencycode":"DJS","intakenumber":"I201800647862","target":"Intake"},
                method: 'get'
            }),
            'admin/assessmenttemplate/getintakeassessment?filter'
        )
        .map((result) => {
            return { data: result.data, count: result.count };
        })
        .share();
    this.startAssessment$ = source.pluck('data');
    if (page === 1) {
        this.totalRecords$ = source.pluck('count');
    }
}

}
