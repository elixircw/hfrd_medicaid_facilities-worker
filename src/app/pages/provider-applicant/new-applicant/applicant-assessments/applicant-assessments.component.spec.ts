import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantAssessmentsComponent } from './applicant-assessments.component';

describe('ApplicantAssessmentsComponent', () => {
  let component: ApplicantAssessmentsComponent;
  let fixture: ComponentFixture<ApplicantAssessmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantAssessmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantAssessmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
