import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeAppointmentComponent } from './intake-appointment.component';

describe('IntakeAppointmentComponent', () => {
  let component: IntakeAppointmentComponent;
  let fixture: ComponentFixture<IntakeAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
