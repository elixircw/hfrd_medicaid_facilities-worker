import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IntakeAppointment,applinfo } from '../_entities/newApplicantModel';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ApplicantUrlConfig } from '../../provider-applicant-url.config';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';


//import moment = require('moment');
import * as moment from 'moment';


declare var $: any;
@Component({
  selector: 'intake-appointment',
  templateUrl: './intake-appointment.component.html',
  styleUrls: ['./intake-appointment.component.scss']
})
export class IntakeAppointmentComponent implements OnInit {

  appointments: IntakeAppointment[] = [];
  appointmentForm: FormGroup;
  maxDate = new Date();
  minDate = new Date();
  times: any[] = [];
  intakeWorkers = [];
  intaketest = [];
  titleText = "Create";
  applicantNumber: string;


  applicantData:any ;

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) { 
      this.applicantNumber = route.snapshot.params['id'];
    }

  ngOnInit() {
    this.initializeAppointmentForm();
    this.times = this.generateTimeList();
    this.intakeWorkers = ["Supervisor", "QA worker", "Executive director", "Deputy director", "License admin"];
    this.intaketest= ["Site visit","Review"];
    this.getAppointment();
this.getApplicantInfo();


  }


  private initializeAppointmentForm() {
    this.appointmentForm = this.formBuilder.group({
      provider_applicant_id: [''],
      appointment_title: [''],
      appointment_date: [''],
      appointment_time: [''],
      appointment_worker: [''],
      notes: [''],
      person_fullname: [''],
      person_role: ['']
    });
  }

  private generateTimeList(is24hrs = true) {
    const x = 15; // minutes interval
    const times = []; // time array
    let tt = 0; // start time
    const ap = [' AM', ' PM']; // AM-PM

    // loop to increment the time and push results in array
    for (let i = 0; tt < 24 * 60; i++) {
      const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
      const mm = (tt % 60); // getting minutes of the hour in 0-55 format
      if (is24hrs) {
        times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
      } else {
        times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
      }
      tt = tt + x;
    }
    return times;
  }

  createOrUpdateAppointment(obj) {
    obj.provider_applicant_id = this.applicantNumber;
    obj.appointment_status = 'Scheduled';
    obj.appointment_date = moment(obj.appointment_date).format('YYYY/MM/DD') + ' ' + obj.appointment_time;
    //console.log("createOrUpdateAppointment:-" + obj);
    this._commonHttpService.create(obj, ApplicantUrlConfig.EndPoint.Applicant.intakeappointment).subscribe(
      (response) => {
        this._alertService.success('Appointment Created successfully!');
        this.appointments.unshift(obj);
        // this.saveContact(obj);
      },
      (error) => {
        this._alertService.error('Unable to Create appointment, please try again.');
        console.log('Save appointment Error', error);
        return false;
      }
    );
    (<any>$('#intake-aptmnt')).modal('hide');
  }



  saveContact(obj) {
    let req : any = {};
    req.provider_applicant_id = this.applicantNumber;
    req.communication_date = new Date();
    //req.notes = obj.notes;
    req.person_contacted_name = obj.person_fullname;
    req.title_of_person_contacted = obj.person_role;
    //req.communication_location = "location";
    req.type_of_contact = "Email";
    req.purpose_of_contact = obj.appointment_title;
    req.narrative = obj.notes;
    //req.phone_of_contact = "000-000-0000";
    req.email_of_contact = this.applicantData.data.contact_email;
    console.log(JSON.stringify(req));

    this._commonHttpService.create(req, "providerapplicant/insertcommunicationinfo").subscribe(
        (response) => {
        },
        (error) => {
          return false;
        }
      );

}

  getAppointment() {

    this._commonHttpService.getArrayList(
      {
        method:'post',
        applicant_id:this.applicantNumber
      },
      ApplicantUrlConfig.EndPoint.Applicant.getappointmentdetails
  ).subscribe(apppointmentsarray => {
    
    this.appointments = apppointmentsarray;
    //console.log(JSON.stringify(this.appointments));
  },
      (error) => {
        this._alertService.error('Unable to get appointment, please try again.');
        console.log('get appointment Error', error);
        return false;
      }
    );
    (<any>$('#intake-aptmnt')).modal('hide');
  }

  resetForm() {
    this.appointmentForm.reset();
  }

  
  getApplicantInfo() {

    this._commonHttpService.getArrayList(
      {
        method:'post',
        where: {
          applicant_id:this.applicantNumber
        }
      },
      ApplicantUrlConfig.EndPoint.Applicant.getapplicantInfo
      ).subscribe(res => {
    
    this.applicantData = res;
    //console.log(JSON.stringify(this.appointments));
  },
      (error) => {
      
        console.log('get appointment Error', error);
        
      }
    );
  }

  

//   onEditAppointment(appointment) {
//     this.actionText = 'Update';
//     this.titleText = 'Edit';
//     this.isEditAppointment = true;
//     this.appointmentInAction = appointment;
//     appointment.appointmentTime = moment.utc(appointment.appointmentdate).format('HH:mm');
//     appointment.appointmentDate = moment(appointment.appointmentdate).format('YYYY-MM-DD');
//     this.appointmentForm.patchValue(appointment);

//     this.appointmentForm.patchValue({ actors: appointment.actors.map(actor => actor.actorid) });
//     this.setFormValidity();
//     this.processTitle();
// }

  onEditAppointment(obj) {
    obj.appointment_status = 'Rescheduled';

    this.appointmentForm.patchValue(obj);

    this._commonHttpService.create(obj, ApplicantUrlConfig.EndPoint.Applicant.intakeappointment).subscribe(
      (response) => {
        this._alertService.success('Appointment Rescheduled successfully!');
      },
      (error) => {
        this._alertService.error('Unable to Create appointment, please try again.');
        console.log('Save appointment Error', error);
        return false;
      }
    );
  }

  confirmDelete(obj) {
    //obj.provider_applicant_id = this.applicantNumber;
    obj.appointment_status = 'Cancelled';

    this._commonHttpService.create(obj, ApplicantUrlConfig.EndPoint.Applicant.intakeappointment).subscribe(
      (response) => {
        this._alertService.success('Appointment Cancelled successfully!');
      },
      (error) => {
        this._alertService.error('Unable to Create appointment, please try again.');
        console.log('Save appointment Error', error);
        return false;
      }
    );
  }

}
