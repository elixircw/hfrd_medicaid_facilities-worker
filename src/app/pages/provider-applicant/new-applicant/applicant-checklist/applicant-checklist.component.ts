import { DatePipe } from '@angular/common';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit, Input } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, GenericService, DataStoreService } from '../../../../@core/services';
import { ApplicantUrlConfig } from '../../provider-applicant-url.config';

declare var $: any;
@Component({
  selector: 'applicant-checklist',
  templateUrl: './applicant-checklist.component.html',
  styleUrls: ['./applicant-checklist.component.scss'],
  providers: [DatePipe]
})
export class ApplicantChecklistComponent implements OnInit, AfterViewChecked {
  id: string;
  taskList = [];
  tasktypestatus = [];
  activityTaskStatusFormGroup: FormGroup;
  config = {
    panels: [

    ]
  };
  @Input() 
  programTypeInput = new Subject<string>();
  
  programNameInputForSiteInspection = new Subject<string>();
  programNameInputForPolicy = new Subject<string>();
  
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private formBuilder: FormBuilder,
    private _formBuilder: FormBuilder,
    // private _service: GenericService<IntakeActivityTask>,
    private _commonHttpService: CommonHttpService,
    // private _planSummaryService: GenericService<InvestigationPlanSummary>,
    // private _taskService: GenericService<ActivityTaskModal>,
    // private _goalsService: GenericService<ActivityGoalModal>,
    private _authService: AuthService,
    private _alertService: AlertService,
    private datePipe: DatePipe,
    private _changeDetect: ChangeDetectorRef,
    private _dataStoreService: DataStoreService
  ) {
    this.id =  route.snapshot.params['id'];
  }
  ngOnInit() {
    this.initiateFormGroup();
    // this.editReferralSubject.subscribe((data) => {
    //   this.referralForm.patchValue(data);
    // });
    this.programTypeInput.subscribe((programTypeParam) => {
      this.programNameInputForSiteInspection.next(programTypeParam);
      this.programNameInputForPolicy.next(programTypeParam);
  });
    //console.log(this.programType);
  }
  ngAfterViewChecked() {
    this._changeDetect.detectChanges();
  }
  initiateFormGroup() {
    this.activityTaskStatusFormGroup = this._formBuilder.group({
      task: this._formBuilder.array([this.createTaskForm()])
    });
    this.getTaskList();
    this.tasktypestatus = ['OPEN', 'COMPLETED'];
  }
  createTaskForm() {
    return this._formBuilder.group({
      checklist_id: [''],
      checklist_task: [''],
      commnts: [''],
      completeddate: [''],
      status: ['']
    });
  }
  setFormValues() {
    // this.activityTaskStatusFormGroup.setControl('task', this._formBuilder.array([]));
    // const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    // this.taskList.forEach((x) => {
    //   control.push(this.buildTaskForm(x));
    // });

    let newObj={};
    this.taskList.forEach((x)=> {
        if(!newObj[x.category]){
          newObj[x.category] = [x];
        } else {
          newObj[x.category].push(x);
        }
    }
    );  

    Object.keys(newObj).forEach((key,index) => {
      this.activityTaskStatusFormGroup.setControl('task'+index, this._formBuilder.array([]));
      const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'+index];
      newObj[key].forEach((x) => {
        control.push(this.buildTaskForm(x));
      });
      this.config.panels.push({ name: key });
    });

    console.log(this.activityTaskStatusFormGroup.value);
    console.log(this.activityTaskStatusFormGroup.controls);
  }

  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
    let objVal = Object.values(this.activityTaskStatusFormGroup.value);
    objVal.shift();
    let merged = [].concat.apply([], objVal);
    this._commonHttpService.endpointUrl='providerapplicant/updatechecklist';
    let requestData = {
      'applicant_id': this.id,
      'task': merged
    };
    this._commonHttpService.create(requestData).subscribe(
      (response) => {
        if (response) {
          this._alertService.success('Task updated successfully');
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }


  //   saveTask() {
  //     console.log(this.activityTaskStatusFormGroup.value);
  //     this._service.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivitySaveUrl;
  //     let requestData = this.activityTaskStatusFormGroup.value.task.filter((item) => item.activitytaskstatustypekey).map((item) => {
  //         return {
  //             activitytaskid: item.activitytaskid,
  //             activitytaskstatustypekey: item.activitytaskstatustypekey,
  //             activitytaskdispositiontypekey: item.activitytaskdispositiontypekey,
  //             completeddate: item.completeddate
  //         };
  //     });
  //     requestData = {
  //         intakeserviceid: this.id,
  //         task: requestData
  //     };
  //     this._service.create(requestData).subscribe(
  //         (response) => {
  //             if (response) {
  //                 this._alertService.success('Task updated successfully');
  //                 this.getInvestigationPlanSummary(this.investigationId);
  //             }
  //         },
  //         (error) => {
  //             this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
  //         }
  //     );
  // }

  private buildTaskForm(x): FormGroup {
    return this._formBuilder.group({
      checklist_id: x.checklist_id,
      checklist_task: x.checklist_task,
      commnts: x.commnts,
      completeddate: x.completeddate,
      status: x.status
    });
  }

  private getTaskList() {
    this._commonHttpService.create(
      {
        method:'post',
        applicant_id:this.id
       },
      ApplicantUrlConfig.EndPoint.Applicant.getapplicantchecklist
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      this.setFormValues();
    });
  }

}
