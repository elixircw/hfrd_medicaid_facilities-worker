import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrittenpoliciesComponent } from './writtenpolicies.component';

describe('WrittenpoliciesComponent', () => {
  let component: WrittenpoliciesComponent;
  let fixture: ComponentFixture<WrittenpoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrittenpoliciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrittenpoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
