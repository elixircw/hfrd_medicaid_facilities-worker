import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantChecklistComponent } from './applicant-checklist.component';

describe('ApplicantChecklistComponent', () => {
  let component: ApplicantChecklistComponent;
  let fixture: ComponentFixture<ApplicantChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
