import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ChangeDetectorRef, Input } from '@angular/core';
import { AddChecklistComponent } from '../add-checklist/add-checklist.component';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonHttpService, AuthService, AlertService, DataStoreService } from '../../../../../@core/services';
import { DatePipe } from '@angular/common';
import { ApplicantUrlConfig } from '../../../provider-applicant-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { Subject } from 'rxjs';

@Component({
  selector: 'siteinspection',
  templateUrl: './siteinspection.component.html',
  styleUrls: ['./siteinspection.component.scss']
})
export class SiteinspectionComponent implements OnInit {
  id: string;
  taskList = [];
  tasktypestatus = [];
  tmpAccordionKey = [];
  activityCompleteStatus = 'Completed';
  activityList = [];
  activityTaskStatusFormGroup: FormGroup;
  config = {
    panels: [

    ]
  };
  task: any[] = [{
    'task': 'Personnel administration',
    'agency': 'OLM',
    'programname': 'CPA',
    'category': 'Site Inspection',
    'commnts': '',
    'subcategory': 'Personnel administration',
    'decription': 'PERSONNEL ADMINISTRATION'
  }, {
    'task': 'Physical Plant',
    'agency': 'OLM',
    'programname': 'CPA',
    'category': 'Site Inspection',
    'commnts': '',
    'subcategory': 'Physical Plant',
    'decription': 'PHYSICAL PLANT'
  }, {
    'task': 'Emergency Planning, General Safety and Transportation',
    'agency': 'OLM',
    'programname': 'CPA',
    'category': 'Site Inspection',
    'commnts': '',
    'subcategory': 'Emergency Planning, General Safety and Transportation',
    'decription': 'EMERGENCY PLANNING, GENERAL SAFETY AND TRANSPORTATION'
  }, {
    'task': 'Basic Life Needs',
    'agency': 'OLM',
    'programname': 'CPA',
    'category': 'Site Inspection',
    'commnts': '',
    'subcategory': 'Basic Life Needs',
    'decription': 'BASIC LIFE NEEDS'
  }, {
    'task': 'Employee Duties and Qualifications',
    'agency': 'OLM',
    'programname': 'CPA',
    'category': 'Site Inspection',
    'commnts': '',
    'subcategory': 'Employee Duties and Qualifications',
    'decription': 'EMPLOYEE DUTIES AND QUALIFICATIONS'
  }, {
    'task': 'General Program Requirements',
    'agency': 'OLM',
    'programname': 'CPA',
    'category': 'Site Inspection',
    'commnts': '',
    'subcategory': 'General Program Requirements',
    'decription': 'GENERAL PROGRAM REQUIREMENTS'
  }];

  @Input()
  programNameInputForSiteInspection = new Subject<string>();

  programType: string;

  @ViewChild('addCheckList', { read: ViewContainerRef }) container: ViewContainerRef;

  constructor(private _cfr: ComponentFactoryResolver,
    private route: ActivatedRoute,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private datePipe: DatePipe,
    private _changeDetect: ChangeDetectorRef,
    private _dataStoreService: DataStoreService
  ) {
    this.id = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.activityTaskStatusFormGroup = this._formBuilder.group({
      task: this._formBuilder.array([this.createTaskForm()])
    });
    this.programNameInputForSiteInspection.subscribe((data) => {
      this.programType = data;
      this.getTaskList();
    })
    //this.getTaskList();
    this.tasktypestatus = ['Incomplete', 'Completed', 'N/A'];
  }

  createTaskForm() {
    return this._formBuilder.group({
      checklist_id: [''],
      checklist_task: [''],
      commnts: [''],
      completeddate: [''],
      status: ['']
    });
  }

  private buildTaskForm(x): FormGroup {
    return this._formBuilder.group({
      checklist_id: x.checklist_id,
      checklist_task: x.checklist_task,
      commnts: x.commnts,
      completeddate: x.completeddate,
      status: x.status
    });
  }

  private getTaskList() {
    this.taskList.length = 0;
    this._commonHttpService.create(
      {
        method: 'post',
        applicant_id: this.id,
        category: 'Site Inspection'
      },
      ApplicantUrlConfig.EndPoint.Applicant.getapplicantchecklist
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      if(this.taskList.length === 0){
        this.getActivityList();
      }
      this.setFormValues();
    });
  }

  private getActivityList(){
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          programname: this.programType,
          agency: 'OLM',
          category: 'Site Inspection'
        }
      },
      ApplicantUrlConfig.EndPoint.Applicant.gettasklist
    ).subscribe(taskList => {
      this.activityList = taskList;
    });    
  }

  generateCheckList(){
    let requestObj = {
      "applicant_id": this.id,
      "task": this.task
    }
    this._commonHttpService.create(requestObj, 'providerapplicant/addchecklist').subscribe(
      (response) => {
        this._alertService.success('CheckList Added successfully!');
        this.getTaskList();
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        console.log('Save Checklist Error', error);
        return false;
      }
    );
  }

  setFormValues() {
    this.config.panels = [];
    this.tmpAccordionKey = [];
    let newObj = {};

    this.taskList.forEach((x) => {
      if (!newObj[x.subcategory]) {
        newObj[x.subcategory] = [x];
      } else {
        newObj[x.subcategory].push(x);
      }
    }
    );

    Object.keys(newObj).forEach((key, index) => {
      if (!this.tmpAccordionKey.includes(key)) {
        this.activityTaskStatusFormGroup.setControl('task' + index, this._formBuilder.array([]));
        this.config.panels.push({ name: key, description: this.activityCompleteStatus });
        this.tmpAccordionKey.push(key);
      }

      const control = <FormArray>this.activityTaskStatusFormGroup.controls['task' + index];

      newObj[key].forEach((x) => {
        control.push(this.buildTaskForm(x));
        if (x.status == 'INCOMPLETE' || x.status == 'Incomplete') {
          this.config.panels[index].description = 'Incomplete';
        }
      });
    });
  }

  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
    let objVal = Object.values(this.activityTaskStatusFormGroup.value);
    objVal.shift();
    let merged = [].concat.apply([], objVal);
    this._commonHttpService.endpointUrl = 'providerapplicant/updatechecklist';
    let requestData = {
      'applicant_id': this.id,
      'task': merged
    };
    this._commonHttpService.create(requestData).subscribe(
      (response) => {
        if (response) {
          this._alertService.success('Task updated successfully');
          this.taskList = [];
          this.getTaskList();
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  openSiteCheckList() {
    // check and resolve the component
    let comp = this._cfr.resolveComponentFactory(AddChecklistComponent);
    // Create component inside container
    let expComponent = this.container.createComponent(comp);
    // see explanations
    expComponent.instance._ref = expComponent;
    expComponent.instance.category = 'Site Inspection';
    expComponent.instance.onSaveEventEmitter.subscribe(() => {
      console.log('Event Captured');
      this.getTaskList();
    });
  }

}
