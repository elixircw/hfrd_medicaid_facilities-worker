import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteinspectionComponent } from './siteinspection.component';

describe('SiteinspectionComponent', () => {
  let component: SiteinspectionComponent;
  let fixture: ComponentFixture<SiteinspectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteinspectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteinspectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
