import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewApplicantComponent } from './new-applicant.component';



const routes: Routes = [
    {
        path: '',
        component: NewApplicantComponent,
        // canActivate: [RoleGuard],
        children: [
            // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
            // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
            // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
            // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
        ]
    },
    {
        path: ':id',
        component: NewApplicantComponent,
        children: [
            // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
            // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
            // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
            // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
        ]
        // canActivate: [RoleGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewApplicantRoutingModule {}
