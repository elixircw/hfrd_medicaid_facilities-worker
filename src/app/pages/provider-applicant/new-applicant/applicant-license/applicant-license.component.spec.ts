import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantLicenseComponent } from './applicant-license.component';

describe('ApplicantLicenseComponent', () => {
  let component: ApplicantLicenseComponent;
  let fixture: ComponentFixture<ApplicantLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
