import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { AuthService, CommonHttpService, AlertService } from '../../../../@core/services';
import { LicenseInfo, applinfo } from '../_entities/newApplicantModel';
import { AppUser } from '../../../../@core/entities/authDataModel';
//import { ApplicantDocumentCreatorComponent } from '../applicant-document-creator/applicant-document-creator.component';
import { GeneratedDocuments } from '../_entities/newintakeSaveModel';
import * as ALL_DOCUMENTS from '../applicant-attachments/_configurtions/documents.json'
import * as moment from 'moment';

@Component({
 // providers: [ApplicantDocumentCreatorComponent],
  selector: 'applicant-license',
  templateUrl: './applicant-license.component.html',
  styleUrls: ['./applicant-license.component.scss']
})
export class ApplicantLicenseComponent implements OnInit {

  applicantNumber: string;
  licenseInfo: applinfo = new applinfo();
  licenseFormGroup: FormGroup;
  token: AppUser;
  hasGenerate: Boolean = false;
  isDJS: Boolean = false;
  showsavebutton = true;
  @Input() generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
  generatedDocuments: GeneratedDocuments[] = [];
  
  //@Output() licenseDateUpdateEvent = new EventEmitter();
  
  allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;

  @Output() generateLicenseEvent = new EventEmitter();
  genderList: any[];


  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService, 
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute
    //private comp: ApplicantDocumentCreatorComponent
    ) { 
      this.applicantNumber = route.snapshot.params['id'];
    }

  ngOnInit() {
    this.initializeLicenseForm();
    this.getLicenseInformation();
    this.getGenderList();
    this.token = this._authService.getCurrentUser();
    if (this.token.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
    || this.token.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.token.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
    || this.token.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.token.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA' ) {
      this.isDJS = true;
    }
    
  }


  private initializeLicenseForm() {
    this.licenseFormGroup = this.formBuilder.group({
      license_no: [''],
      requested_license_effective_date: [''],
      requested_license_end_date: [''],
      
      program_name: [''],
      program_type: [''],
      prgram: [''],
      
      contact_first_nm: [''],
      contact_last_nm: [''],

      minimum_age_no: [''],
      maximum_age_no: [''],

      gender_cd: [''],
      formatted_address: [''],
      children_no: ['']
    });
  }


  // getLicenseInformation() {
  //     this._commonHttpService.create(
  //       {
  //         method:'post',
  //         where: 
  //         {
  //           objectid :this.applicantNumber
  //       }        
  //       },
  //       'providerapplicant/getproviderlicensing'
  //   ).subscribe(response => {
  //     console.log(response)
  //     this.licenseInfo = response.data[0];
  //     this.licenseFormGroup.patchValue(this.licenseInfo);
  //     console.log("SIMAR LICENSE INFORMATION")
  //     console.log(JSON.stringify(this.licenseInfo));
  //   },
  //       (error) => {
  //         this._alertService.error('Unable to get license information, please try again.');
  //         console.log('get license information Error', error);
  //         return false;
  //       }
  //     );
  // }


  getLicenseInformation() {
    this._commonHttpService.create(
      {
        method:'post',
        where: 
        {
          applicant_id :this.applicantNumber
      }        
      },
      'providerapplicant/getproviderapplicant'
  ).subscribe(response => { 
    this.licenseInfo = response.data;
    const mainAddress = (response.data&& response.data.addresses && response.data.addresses.length) ? response.data.addresses : [];
    if(mainAddress && mainAddress.length) {
      this.licenseInfo.formatted_address = mainAddress[0].formatted_address;
    }
    if (response.data) {
      if (this.licenseInfo.prgram === 'OOS' || this.licenseInfo.prgram === 'MDH') {
        this.licenseFormGroup.get('license_no')['enable']();
        this.hasGenerate = true;
      } else {
        this.licenseFormGroup.get('license_no')['disable']();
        this.hasGenerate = false;
      }
    }
    if (this.licenseInfo) {
    this.licenseFormGroup.patchValue(this.licenseInfo);
    if (this.licenseInfo.license_no) {
        this.showsavebutton = false;
    }
    }
    console.log("SIMAR LICENSE INFORMATION")
    console.log(JSON.stringify(this.licenseInfo));
  },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
}

  // saveLicenseInformation() {

  //   this._commonHttpService.create(
  //     {
  //       method:'post',
  //       where: 
  //       {
  //         license_issue_dt: this.licenseFormGroup.value.license_issue_dt,
  //         license_expiry_dt: this.licenseFormGroup.value.license_expiry_dt,
  //         license_no: this.licenseInfo.license_no
  //     }        
  //     },
  //     'providerapplicant/updateproviderlicensing'
  // ).subscribe(response => {
  //   console.log(response)
  //   console.log("SIMAR UPDATE LICENSE DATE INFORMATION")
    
  //   this.licenseDateUpdateEvent.emit(
  //     {
  //       license_issue_dt: this.licenseFormGroup.value.license_issue_dt,
  //       license_expiry_dt: this.licenseFormGroup.value.license_expiry_dt
  //     }
  //   );

  //   // this.comp.getLicenseInformation();

  // },
  //     (error) => {
  //       this._alertService.error('Unable to get license information, please try again.');
  //       console.log('get license information Error', error);
  //       return false;
  //     }
  //   );
  // }



generateLicense() {
  var licenseDocument = null; 

  this.allDocuments.forEach(document => {
    if (this.isDJS === false) {
      if (document.key === "olm-rcc-license-letter") {
        licenseDocument = document;
      }
    } else {
      if (document.key === "djs-rcc-license-certificate") {
        licenseDocument = document;
      }
    }
  });
  console.log("license document", licenseDocument);

  this.generateLicenseEvent.emit(
    {
      generateLicenseDocument: licenseDocument
    }
  );

  //this.generateNewDocument(licenseDocument);
}


  saveLicenseInformation() {
    var obj = this.licenseFormGroup.value;
    
    var payload = new applinfo();
    payload = this.licenseInfo;

    payload.requested_license_effective_date = moment(obj.requested_license_effective_date).format('YYYY-MM-DDThh:mm:ss');
    payload.requested_license_end_date = moment(obj.requested_license_end_date).format('YYYY-MM-DDThh:mm:ss');
    payload.program_name = obj.program_name;
    payload.minimum_age_no = obj.minimum_age_no;
    payload.maximum_age_no = obj.maximum_age_no;
    payload.gender_cd = obj.gender_cd;
    payload.children_no = obj.children_no;
    payload.formatted_address = obj.formatted_address;
    payload.contact_first_nm = obj.contact_first_nm;
    payload.license_no = obj.license_no;

    payload.applicant_id = this.applicantNumber;
    this._commonHttpService.update('',
      payload,
      'providerapplicant/updateapplicantlicenseinfo'
  ).subscribe(response => {
    console.log(response)
    this._alertService.success('License Information Saved Successfully!');

  //   this.licenseDateUpdateEvent.emit(
  //     {
  //       license_issue_dt: this.licenseFormGroup.value.license_issue_dt,
  //       license_expiry_dt: this.licenseFormGroup.value.license_expiry_dt
  //     }
  //   );

  },
      (error) => {
        this._alertService.error('Unable to update license information, please try again.');
        console.log('update program information Error', error);
        return false;
      }
    );
  }

  getGenderList() {
    this._commonHttpService.getArrayList(
      {
        where: { tablename: 'gender', teamtypekey: 'OLM' },
        method: 'get',
        nolimit: true
      },
      'referencetype/gettypes?filter'
    ).subscribe(result => {
      this.genderList = result;
    });
  }

}
