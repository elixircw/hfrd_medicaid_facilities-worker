import { Component, OnInit } from '@angular/core';

import { CommonHttpService } from '../../../@core/services/common-http.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';
import { AuthService } from '../../../@core/services';

@Component({
  selector: 'existing-applicants',
  templateUrl: './existing-applicants.component.html',
  styleUrls: ['./existing-applicants.component.scss']
})
export class ExistingApplicantsComponent implements OnInit {
  applicants: any;
  applicantType= [];
  applicationForm: FormGroup;

  // Current Selections
  currentApplicantType: String;
  currentSelectedStatus: String[];
  currenturl: string;
  whereClause: any;
  userInfo: any;
  agency: any;


  constructor(
    private _commonHttpService: CommonHttpService, 
    private formBuilder: FormBuilder,
    private _authService: AuthService) {
  }


  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    if (this.userInfo.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
    || this.userInfo.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.userInfo.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
    || this.userInfo.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.userInfo.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA' ) {
      this.agency = "DJS";
    }else{
      this.agency = "OLM";
    }
    this.initApplicationForm();
    this.applicantType = ['Public', 'Private', 'Vendor'];
    // Setup default starting values
    this.currentApplicantType = 'Public';
    this.currentSelectedStatus = ['Pending', 'Submitted'];
    this.currenturl = 'providerapplicant?filter';
    this.getApplicantType('Private');   
  }

  // ngAfterViewInit() {
  //   this.currentApplicantType == 'Private';
  // }

  private initApplicationForm(){
    this.applicationForm = this.formBuilder.group({
      applicant_type: [''],
    });
  }

  getProviderApplicant() {
    this.applicants = [];
    if (this.currentApplicantType === "Private") {
      this.currenturl = 'providerapplicant?filter';
    } else
    if (this.currentApplicantType === "Public") {
      this.currenturl = 'publicproviderapplicant?filter';
    }

    this.whereClause = {'application_status' : {inq: this.currentSelectedStatus},'agency':this.agency };

    if ( this.userInfo && (this.userInfo.role.name === 'Executive Director' || this.userInfo.role.name === 'Deputy Director' || this.userInfo.role.name === 'Provider_DJS_SecretaryDesignee' || this.userInfo.role.name === 'Provider_DJS_Programdirector')) {
      this.currenturl = 'providerreferral/getportaldecisionlist?filter';
      this.whereClause = {'application_status' : '{Pending,Submitted}' };
    }

    this._commonHttpService.getArrayList(
      {
          method: 'get',
          nolimit: true,
          where: this.whereClause,
          order : ['create_ts DESC']
      }, this.currenturl
      ).subscribe(applicants => {
        this.applicants = applicants;
      });
  }

  getApplicantType(obj) {
    this.currentApplicantType = obj;
    this.getProviderApplicant();
  }

  changeSelectedStatus(selectedStatus) {
    this.currentSelectedStatus = [selectedStatus];
    if (selectedStatus === 'Pending') {
      this.currentSelectedStatus.push('Submitted');
    }
    this.getProviderApplicant();
  }

}
