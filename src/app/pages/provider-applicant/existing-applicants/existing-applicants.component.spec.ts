import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingApplicantsComponent } from './existing-applicants.component';

describe('ExistingApplicantsComponent', () => {
  let component: ExistingApplicantsComponent;
  let fixture: ComponentFixture<ExistingApplicantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingApplicantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingApplicantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
