import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderApplicantComponent } from './provider-applicant.component';

describe('ProviderApplicantComponent', () => {
  let component: ProviderApplicantComponent;
  let fixture: ComponentFixture<ProviderApplicantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderApplicantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderApplicantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
