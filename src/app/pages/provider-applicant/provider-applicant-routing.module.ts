import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderApplicantComponent } from './provider-applicant.component'
import { ExistingApplicantsComponent } from './existing-applicants/existing-applicants.component';
import { RoleGuard } from '../../@core/guard/role.guard';


const routes: Routes = [
    {
        path: '',
        component: ProviderApplicantComponent,
        canActivate: [RoleGuard],
        children: [
            {
                path: 'new-applicant',
                loadChildren: './new-applicant/new-applicant.module#NewApplicantModule'
            },
            
        ]
    },
    {
        path: '',
        component: ProviderApplicantComponent,
        canActivate: [RoleGuard],
        children: [
            {
                path: 'new-public-applicant',
                loadChildren: './new-public-applicant/new-public-applicant.module#NewPublicApplicantModule'
            }
        ]
    },    
    {
        path: 'existing-applicant',
        component: ExistingApplicantsComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Saved Intakes'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProviderApplicantRoutingModule {}

