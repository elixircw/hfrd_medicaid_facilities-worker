import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { ProviderApplicantRoutingModule } from './provider-applicant-routing.module';
import { ProviderApplicantComponent } from './provider-applicant.component';
import { ExistingApplicantsComponent } from './existing-applicants/existing-applicants.component';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { PublicApplicantContactComponent } from './new-public-applicant/public-applicant-contact/public-applicant-contact.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule
} from '@angular/material';


//import { ExistingApplicantComponent } from './existing-applicant/existing-applicant.component';

@NgModule({
  imports: [
    CommonModule, ProviderApplicantRoutingModule, NgSelectModule,SharedPipesModule,NgxPaginationModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ProviderApplicantComponent, ExistingApplicantsComponent]
})
export class ProviderApplicantModule { }
