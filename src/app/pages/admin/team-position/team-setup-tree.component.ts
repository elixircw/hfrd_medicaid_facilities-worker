import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { TreeViewModel } from '../../../@core/entities/common.entities';
import { GenericService, DataStoreService } from '../../../@core/services';
import { TeamDetails, TeamPosition } from '../../admin/team-position/_entities/teamposition.data.model';
import { AdminUrlConfig } from '../admin-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'team-setup-tree',
  templateUrl: './team-setup-tree.component.html',
  styleUrls: ['./team-setup-tree.component.scss']
})
export class TeamSetupTreeComponent implements OnInit {

  @Input() teamDetailIdSubject$ = new Subject<Observable<TeamDetails>>();
  @Input() reloadTreeSubject = new Subject();
  @Input() selectedTreeSubject = new Subject();
  @Input() selectedTeamId = new Subject<string>();
  @Input() totalCount$ = new Subject<any>();
  @Input() pageChangedRequest$ = new Subject<any>();
  @Input() emailSearch$ = new Subject<string>();
  teamTreeview$: Observable<TreeViewModel[]>;
  paginationInfo: PaginationInfo = new PaginationInfo();
  private pageSubject$ = new Subject<number>();
  teamDetails: any;
  teamId: '';


  constructor(private _service: GenericService<TeamPosition>, private _dataStore: DataStoreService) {
    this._service.endpointUrl = AdminUrlConfig.EndPoint.TeamPosition.TeamTreeListUrl;
  }

  ngOnInit() {
    this.reloadTreeSubject.subscribe(value => this.getPage());
    this.pageChangedRequest$.subscribe((item) => {
      this.onSelected(null, this.teamId, item.page, item.itemsPerPage);
    });
    this.emailSearch$.subscribe((item) => {
      this.onSelected(item, this.teamId, 1, 10);
    });
    this.getPage();
    this.selectedTreeSubject.subscribe(value => this.onReload(this.teamId));
  }


  getPage() {
    this._service.endpointUrl = AdminUrlConfig.EndPoint.TeamPosition.TeamTreeListUrl;
    this.teamTreeview$ = this._service.getArrayList({}).map(response => {
      return response;
    });
  }

  onSelected(email?: string, event?: any, pageNo?: number, pageSize?: number, treeSelected?: boolean, isonselected?: boolean) {
    this._dataStore.setData('Email_Search_Reset', isonselected);
    this.teamId = event;
      this._service.endpointUrl = 'manage/team/details';
      const source = this._service.getArrayList({
        page: pageNo,
        limit: pageSize,
        email: email,
        method: 'get'
      }, AdminUrlConfig.EndPoint.TeamPosition.TeamDetailsUrl + '/' + this.teamId + '?filter').share();
       this.teamDetailIdSubject$.next(source.pluck('data') as Observable<TeamDetails>);
       this._dataStore.setData('Pagination_Reset', true);
       this._dataStore.setData('Email_Search_Reset', false);
      source.pluck('count').subscribe((item) => {
        if (item) {
          this.totalCount$.next(item);
        } else if (item === 0 && treeSelected) {
          this.totalCount$.next(item);
        }
      });
       this.selectedTeamId.next(this.teamId);
       this._service.endpointUrl = AdminUrlConfig.EndPoint.TeamPosition.TeamTreeListUrl;
  }

     onReload(id) {
      this._service.endpointUrl = 'manage/team/details';
      const source = this._service.getPagedArrayList(new PaginationRequest({
        page: 1,
        limit: 10,
        method: 'get'
      }), AdminUrlConfig.EndPoint.TeamPosition.TeamDetailsUrl + '/' + id + '?filter').share();
       this.teamDetailIdSubject$.next(source.pluck('data') as Observable<TeamDetails>);
      this._service.endpointUrl = AdminUrlConfig.EndPoint.TeamPosition.TeamTreeListUrl;
     }

  }

