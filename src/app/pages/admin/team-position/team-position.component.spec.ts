import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { TreeModule } from 'angular-tree-component';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';

import { CoreModule } from '../../../@core/core.module';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { TeamPositionComponent } from './team-position.component';
import { TeamSetupDetailComponent } from './team-setup-detail.component';
import { TeamSetupTreeComponent } from './team-setup-tree.component';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';

describe('TeamPositionComponent', () => {
    let component: TeamPositionComponent;
    let fixture: ComponentFixture<TeamPositionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                TreeModule,
                A2Edatetimepicker,
                SharedPipesModule
            ],
            declarations: [TeamPositionComponent, TeamSetupTreeComponent, TeamSetupDetailComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TeamPositionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
