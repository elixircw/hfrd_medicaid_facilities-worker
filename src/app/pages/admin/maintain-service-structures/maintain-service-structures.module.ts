import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintainServiceStructuresRoutingModule } from './maintain-service-structures-routing.module';
import { MaintainServiceStructuresComponent } from './maintain-service-structures.component';
import { MatCheckboxModule, MatDatepickerModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatListModule, MatNativeDateModule, MatSelectModule, MatRadioModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    MaintainServiceStructuresRoutingModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatSelectModule,
    A2Edatetimepicker,
    PaginationModule,
    MatRadioModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [MaintainServiceStructuresComponent]
})
export class MaintainServiceStructuresModule { }
