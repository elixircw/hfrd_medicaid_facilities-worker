import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleGuard } from '../../../@core/guard';
import { GeneralComponent } from './general.component';

const routes: Routes = [
    {
        path: '',
        component: GeneralComponent,
        canActivate: [RoleGuard],
        children: [
            { path: 'manage-dsds-action-type', loadChildren: './manage-dsds-action-type/manage-dsds-action-type.module#ManageDsdsActionTypeModule' },
            { path: 'da-status-disposition', loadChildren: './da-status-disposition/da-status-disposition.module#DaStatusDispositionModule' },
            { path: 'distribution-list', loadChildren: './distribution-list/distribution-list.module#DistributionListModule' },
            { path: 'person-configuration', loadChildren: './person-configuration/person-configuration.module#PersonConfigurationModule' },
            { path: 'entity-configuration', loadChildren: './entity-configuration/entity-configuration.module#EntityConfigurationModule' },
            { path: '', redirectTo: 'manage-dsds-action-type' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GeneralRoutingModule {}
