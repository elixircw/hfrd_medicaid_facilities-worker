export * from './address-type/address-type.component';
export * from './employee-type/employee-type.component';
export * from './ethnic-group-type/ethnic-group-type.component';
export * from './gender-type/gender-type.component';
export * from './identifier-type/identifier-type.component';
export * from './income-type/income-type.component';
export * from './language-type/language-type.component';
export * from './living-arrangement-type/living-arrangement-type.component';
export * from './marital-status-type/marital-status-type.component';
export * from './phone-type/phone-type.component';
export * from './person-role/person-role.component';
export * from './race-type/race-type.component';
