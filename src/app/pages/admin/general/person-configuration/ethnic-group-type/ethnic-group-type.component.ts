import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { DynamicObject, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES, REGEX } from '../../../../../@core/entities/constants';
import { AlertService, GenericService } from '../../../../../@core/services';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';
import { AdminUrlConfig } from '../../../admin-url.config';
import { EthnicGroupType, GeneralConfig, MasterCatelogConfig } from '../../_entities/general.data.models';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'ethnic-group-type',
    templateUrl: './ethnic-group-type.component.html',
    styleUrls: ['./ethnic-group-type.component.scss']
})
export class EthnicGroupTypeComponent implements OnInit {
    ethnicGroupTypes$: Observable<EthnicGroupType[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    ethnicGroupTypeConfig: MasterCatelogConfig;
    paginationInfo: PaginationInfo = new PaginationInfo();
    formGroup: FormGroup;
    dynamicObject: DynamicObject = {};
    ethnicGroupTypeData = new GeneralConfig();
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();
    constructor(
        private formBuilder: FormBuilder,
        private _service: GenericService<EthnicGroupType>,
        private _alertService: AlertService
    ) {
        this._service.endpointUrl =
            AdminUrlConfig.EndPoint.General.EthnicGroupTypeUrl;
        this.formGroup = this.formBuilder.group({
            name: ['', [Validators.required, Validators.maxLength(50), REGEX.NOT_EMPTY_VALIDATOR]],
            description: ['', [Validators.required, Validators.maxLength(50), REGEX.NOT_EMPTY_VALIDATOR]]
        });
        this.ethnicGroupTypeConfig = new MasterCatelogConfig({
            BlockTitle: 'Add Ethnic Group Type',
            LabelNameTitle: 'Ethnic Group Type',
            LabelDescriptionTitle: 'Type Description',
            TableHeaderName: 'Ethnic Group Type',
            TableHeaderDescription: 'Description',
            RouteUrl: ''
        });
    }

    ngOnInit() {
        this.paginationInfo.sortBy = 'ethnicgrouptypekey asc';
        this.getPage();
    }

    getPage() {
        const pageSource = this.pageStream$.map(pageNumber => {
            this.paginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObject, page: pageNumber };
        });

        const searchSource = this.searchTermStream$
            .debounceTime(1000)
            .map(searchTerm => {
                this.dynamicObject = searchTerm;
                return { search: searchTerm, page: 1 };
            });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .mergeMap((params: { search: DynamicObject; page: number }) => {
                return this._service
                    .getPagedArrayList(
                        {
                            limit: this.paginationInfo.pageSize,
                            order: this.paginationInfo.sortBy,
                            page: params.page,
                            count: this.paginationInfo.total,
                            method: 'get',
                            where: params.search
                        },
                        AdminUrlConfig.EndPoint.General.EthnicGroupTypeUrl +
                        '/list?filter'
                    )
                    .map(result => {
                        return {
                            data: result.data.map(model => new EthnicGroupType(model)),
                            count: result.count,
                            canDisplayPager: result.count > this.paginationInfo.pageSize
                        };
                    });
            }).share();

        this.ethnicGroupTypes$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    saveItem(modelData: EthnicGroupType) {
        modelData.typedescription = modelData.description;
        modelData.activeflag = 1;
        if (modelData.ethnicgrouptypekey) {
            this._service
                .update(modelData.ethnicgrouptypekey, modelData)
                .subscribe(
                    response => {
                        if (response) {
                            this._alertService.success(
                                'Enthnic group type saved successfully.'
                            );
                            this.pageStream$.next(this.paginationInfo.pageNumber);
                        }
                    },
                    error => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
        } else {
            modelData.ethnicgrouptypekey = modelData.name;
            modelData.effectivedate = new Date();
            this._service.create(modelData).subscribe(
                response => {
                    if (response.ethnicgrouptypekey) {
                        this._alertService.success(
                            'Enthnic group type saved successfully.'
                        );
                        this.paginationInfo.sortBy = 'insertedon desc';
                        this.pageStream$.next(1);
                        this.formGroup.patchValue({
                            nameSearch: this.ethnicGroupTypeData.name
                        });
                    }
                },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }

    deleteItem(modelData: EthnicGroupType) {
        this._service.remove(modelData.ethnicgrouptypekey).subscribe(
            response => {
                if (response) {
                    this._alertService.success(
                        'Enthnic group type deleted successfully.'
                    );
                    this.pageStream$.next(this.paginationInfo.pageNumber);
                }
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    onSort($event: ColumnSortedEvent) {
        if ($event.sortColumn === 'name') {
            this.paginationInfo.sortBy =
                'ethnicgrouptypekey ' + $event.sortDirection;
        } else if ($event.sortColumn === 'description') {
            this.paginationInfo.sortBy =
                'typedescription ' + $event.sortDirection;
        }
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    onSearch(event: any) {
        if (event.field === 'name') {
            this.dynamicObject['ethnicgrouptypekey'] = { like: '%25' + event.value + '%25' };
            if (!event.value) {
                delete this.dynamicObject['ethnicgrouptypekey'];
            }
        } else if (event.field === 'description') {
            this.dynamicObject['typedescription'] = { like: '%25' + event.value + '%25' };
            if (!event.value) {
                delete this.dynamicObject['typedescription'];
            }
        }
        this.searchTermStream$.next(this.dynamicObject);
    }
}
