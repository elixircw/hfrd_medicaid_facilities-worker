import { Component, OnInit } from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'person-configuration',
    templateUrl: './person-configuration.component.html',
    styleUrls: ['./person-configuration.component.scss']
})
export class PersonConfigurationComponent implements OnInit {
    selected: any;
    isInitialSelect = true;
    route: string;
    personConfigTabList: any;

    ngOnInit() {}
}
