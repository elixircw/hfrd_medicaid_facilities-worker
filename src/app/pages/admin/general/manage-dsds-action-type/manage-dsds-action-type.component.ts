import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { DynamicObject, PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService } from '../../../../@core/services';
import { GenericService } from '../../../../@core/services/generic.service';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';
import { AdminUrlConfig } from '../../admin-url.config';
import { ManageDAAction } from '../_entities/manage-da-action.models';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'manage-dsds-action-type',
    templateUrl: './manage-dsds-action-type.component.html',
    styleUrls: ['./manage-dsds-action-type.component.scss']
})
export class ManageDsdsActionTypeComponent implements OnInit {
    paginationInfo: PaginationInfo = new PaginationInfo();
    daAction: ManageDAAction;
    modalRef: BsModalRef;
    manageDAActions$: Observable<ManageDAAction[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    managedsdsactionTypeData = new ManageDAAction();
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();
    private dynamicObject: DynamicObject = {};
    constructor(private _service: GenericService<ManageDAAction>, private _alertService: AlertService) {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.General.IntakeServiceRequestTypeUrl;
    }

    ngOnInit() {
        this.paginationInfo.sortBy = 'intakeservreqtypekey asc';
        this.getPage();
    }
    getPage() {
        const pageSource = this.pageStream$.map((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObject, page: pageNumber };
        });

        const searchSource = this.searchTermStream$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObject = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .mergeMap((params: { search: DynamicObject; page: number }) => {
                return this._service
                    .getPagedArrayList(
                        {
                            limit: this.paginationInfo.pageSize,
                            order: this.paginationInfo.sortBy,
                            page: params.page,
                            count: this.paginationInfo.total,
                            nolimit: true,
                            method: 'get',
                            where: params.search
                        },
                        AdminUrlConfig.EndPoint.General.IntakeServiceRequestTypeUrl + '/list?filter'
                    )
                    .map((result) => {
                        return {
                            data: result.data,
                            count: result.count,
                            canDisplayPager: result.count > this.paginationInfo.pageSize
                        };
                    });
            })
            .share();

        this.manageDAActions$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    deleteItem() {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.General.IntakeServiceRequestTypeUrl + '/delete';
        this._service.remove(this.daAction.intakeservreqtypeid).subscribe(
            (response) => {
                this._alertService.success('Department action type deleted successfully.');
                this.pageStream$.next(this.paginationInfo.pageNumber);
                (<any>$('#delete-popup')).modal('hide');
            },
            (error) => {
                (<any>$('#delete-popup')).modal('hide');
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    confirmDelete(modelData: ManageDAAction) {
        this.daAction = modelData;
        (<any>$('#delete-popup')).modal('show');
    }

    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortColumn + ' ' + $event.sortDirection;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    onSearch(field: string, value: string) {
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }
}
