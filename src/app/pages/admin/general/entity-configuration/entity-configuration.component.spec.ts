import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaginationModule } from 'ngx-bootstrap';

import {
  ContactAddressTypeComponent,
  ContactPhoneTypeComponent,
  EntityAddressTypeComponent,
  EntityPhoneTypeComponent,
  EntityRoleTypeComponent,
  EntityTypeComponent,
} from '.';
import { CoreModule } from '../../../../@core/core.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { AdminSharedModule } from '../../_shared/admin-shared.module';
import { EntityConfigurationComponent } from './entity-configuration.component';

describe('EntityConfigurationComponent', () => {
    let component: EntityConfigurationComponent;
    let fixture: ComponentFixture<EntityConfigurationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, CoreModule.forRoot(), HttpClientModule, FormsModule, ReactiveFormsModule, ControlMessagesModule, PaginationModule, NgSelectModule, AdminSharedModule],
            declarations: [
                EntityConfigurationComponent,
                EntityAddressTypeComponent,
                EntityPhoneTypeComponent,
                EntityTypeComponent,
                EntityRoleTypeComponent,
                ContactAddressTypeComponent,
                ContactPhoneTypeComponent,
                EntityConfigurationComponent
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EntityConfigurationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
