export class AllegationConfigList {
    assessmenttemplateid: string;
    name: string;
    description: string;
    insertedby: string;
    updatedby: string;
    version: string;
    archivtitleheadertexteby: string;
    assessmenttextpositiontypekey: string;
    investigatable: boolean;
    assesssmenttemplateids: AllegationConfigList[];
}
