import { Injectable } from '@angular/core';
import { initializeObject } from '../../../../@core/common/initializer';
@Injectable()
export class MasterCatelogConfig {

    BlockTitle: string = null;
    LabelNameTitle: string = null;
    LabelDescriptionTitle: string = null;
    TableHeaderName: string = null;
    TableHeaderDescription: string = null;
    RouteUrl: string = null;
    constructor(initializer?: MasterCatelogConfig) {
        initializeObject(this, initializer);
    }
}

@Injectable()
export class CatelogConfig extends MasterCatelogConfig {
    LabelBeginDateTitle: Date = null;
    LabelEndDateTitle: Date = null;
}

export class GeneralConfig {
    id: number | string;
    sequencenumber: number | string;
    activeflag: number;
    datavalue: number;
    editable: number;
    effectivedate: Date;
    expirationdate: Date;
    name: string;
    description: string;
    constructor(initializer?: GeneralConfig) {
        initializeObject(this, initializer);
    }
}
export class EthnicGroupType extends GeneralConfig {
    ethnicgrouptypekey: string;
    typedescription: string;
    constructor(initializer?: EthnicGroupType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.ethnicgrouptypekey;
        super(initializer);
    }
}
export class LanguageType extends GeneralConfig {
    languagetypeid: string;
    languagetypename: string;
    constructor(initializer?: LanguageType) {
        initializer.id = initializer.languagetypeid;
        initializer.name = initializer.languagetypename;
        super(initializer);
    }
}
export class MaritalStatusType extends GeneralConfig {
    maritalstatustypekey: string;
    typedescription: string;
    constructor(initializer?: MaritalStatusType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.maritalstatustypekey;
        super(initializer);
    }
}
export class RaceType extends GeneralConfig {
    racetypekey: string;
    typedescription: string;
    constructor(initializer?: RaceType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.racetypekey;
        super(initializer);
    }
}
export class GenderType extends GeneralConfig {
    gendertypekey: string;
    typedescription: string;
    constructor(initializer?: GenderType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.gendertypekey;
        super(initializer);
    }
}
export class IncomeType extends GeneralConfig {
    incometypekey: string;
    typedescription: string;
    constructor(initializer?: IncomeType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.incometypekey;
        super(initializer);
    }
}
export class AddressType extends GeneralConfig {
    currentlocationflag: number;
    personaddresstypekey: string;
    typedescription: string;
    personid: string;
    danger: boolean;
    dangerreason: string;
    address: string;
    address2: string;
    zipcode: number;
    state: string;
    city: string;
    county: string;
    effectivedate: Date;
    expirationdate: Date;
    directions: string;
    countydescription: string;
    addressstartdate: Date;
    constructor(initializer?: AddressType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.personaddresstypekey;
        super(initializer);
    }
}
export class PhoneType extends GeneralConfig {
    personphonetypekey: string;
    phonenumber: number;
    personid: string;
    ismobile: boolean;
    typedescription: string;
    constructor(initializer?: PhoneType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.personphonetypekey;
        super(initializer);
    }
}
export class EmailType extends GeneralConfig {
    personemailtypekey: string;
    email: string;
    personid: string;
    typedescription: string;
    type: string;
    constructor(initializer?: EmailType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.personemailtypekey;
        super(initializer);
    }
}
export class PersonRole extends GeneralConfig {
    actortype: string;
    typedescription: string;
    tasktype: number;
    constructor(initializer?: PersonRole) {
        initializer.description = initializer.typedescription;
        initializer.id = initializer.actortype;
        initializer.name = initializer.actortype;
        initializer.datavalue = initializer.tasktype;
        super(initializer);
    }
}
export class EmployeeType extends GeneralConfig {
    employeetypeid: number;
    employeetypename: string;
    constructor(initializer?: EmployeeType) {
        initializer.id = initializer.employeetypeid;
        initializer.name = initializer.employeetypename;
        super(initializer);
    }
}
export class LivingArrangementType extends GeneralConfig {
    livingarrangementtypekey: string;
    description: string;
    constructor(initializer?: LivingArrangementType) {
        initializer.description = initializer.description;
        initializer.name = initializer.livingarrangementtypekey;
        super(initializer);
    }
}
export class IdentifierType extends GeneralConfig {
    personidentifiertypekey: string;
    typedescription: string;
    constructor(initializer?: IdentifierType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.personidentifiertypekey;
        super(initializer);
    }
}

export class EntityAddressType extends GeneralConfig {
    agencyaddresstypekey: string;
    typedescription: string;
    constructor(initializer?: EntityAddressType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.agencyaddresstypekey;
        super(initializer);
    }
}
export class EntityPhoneType extends GeneralConfig {
    agencyphonenumbertypekey: string;
    typedescription: string;
    constructor(initializer?: EntityPhoneType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.agencyphonenumbertypekey;
        super(initializer);
    }
}
export class EntityType extends GeneralConfig {
    agencytypekey: string;
    typedescription: string;
    oldId: string;
    agencycategorykey: string;
    constructor(initializer?: EntityType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.agencytypekey;
        super(initializer);
    }
}
export class EntityRoleType extends GeneralConfig {
    agencytypekey: string;
    typedescription: string;
    constructor(initializer?: EntityRoleType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.agencytypekey;
        super(initializer);
    }
}
export class ContactAddressType extends GeneralConfig {
    caregiveraddresstypekey: string;
    typedescription: string;
    constructor(initializer?: ContactAddressType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.caregiveraddresstypekey;
        super(initializer);
    }
}
export class ContactPhoneType extends GeneralConfig {
    agencyphonenumbertypekey: string;
    typedescription: string;
    constructor(initializer?: ContactPhoneType) {
        initializer.description = initializer.typedescription;
        initializer.name = initializer.agencyphonenumbertypekey;
        super(initializer);
    }
}
export class DispositionCodeType extends GeneralConfig {
    dispositioncode: string;
    dispositioncodeid: string;
    insertedby?: string;
    updatedby: string;
    intakeservreqtypeid: string;
    constructor(initializer?: DispositionCodeType) {
        // initializer.name = initializer.dispositioncode;
        super(initializer);
    }
}

export class DaStatusType {

    intakeserreqstatustypeid: string;
    intakeserreqstatustypekey: string;
    description: string;
    insertedby?: string;
    updatedby: string;
    activeflag: number;
    effectivedate: Date;
    expirationdate: Date;
    name: string;
    constructor(initializer?: DaStatusType) {
        initializeObject(this, initializer);
    }
}


export class Military extends GeneralConfig
{

}


export class Education extends GeneralConfig
{

}