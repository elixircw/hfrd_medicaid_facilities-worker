import { Injectable } from '@angular/core';

import { initializeObject } from '../../../../@core/common/initializer';

export class MasterRecordingTypesConfig {
    id: number;
    name: string;
    description: string;
    activeflag: number;
    updatedby: string;
    updatedon: Date;
    insertedby: string;
    insertedon: Date;
    effectivedate: Date = new Date();
    expirationdate: Date = new Date();
    timestamp: Date;
}


export class RecordingType {

    progressnotetypeid: string;
    progressnotetypekey: string;
    progressnotesubtypekey: string;
    parentid: string;
    progressnoteclassificationtypekey: string;
    progressnotetype: [string];
    progressnoteclassificationtype: string;
    description: string;
    insertedby?: string;
    updatedby: string;
    activeflag: 1;
    effectivedate: Date = new Date();
    expirationdate: Date;
    subeexpirationdate: Date;
    name: string;
}
