import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleGuard } from '../../../@core/guard';
import { DaConfigComponent } from './da-config.component';

const routes: Routes = [
  {
    path: '',
    component: DaConfigComponent,
    canActivate: [RoleGuard],
    children: [
      { path: '', loadChildren: './actions-tasks-goals/actions-tasks-goals.module#ActionsTasksGoalsModule' },
      { path: 'actions-tasks-goals', loadChildren: './actions-tasks-goals/actions-tasks-goals.module#ActionsTasksGoalsModule' },
      { path: 'allegations', loadChildren: './allegations/allegations.module#AllegationsModule' },
      { path: 'custom-forms', loadChildren: './custom-forms/custom-forms.module#CustomFormsModule' },
      { path: 'da-type-config', loadChildren: './da-type-config/da-type-config.module#DaTypeConfigModule' },
      { path: 'provider-contracting', loadChildren: './provider-contracting/provider-contracting.module#ProviderContractingModule' },
      { path: 'review-da', loadChildren: './review-da/review-da.module#ReviewDaModule'}
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DaConfigRoutingModule { }
