import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProviderContractingComponent } from './provider-contracting.component';


const routes: Routes = [
  {
    path: '',
    component: ProviderContractingComponent,
    children: [
      { path: 'doc-types', loadChildren: './doc-types/doc-types.module#DocTypesModule' },
      { path: 'doc-details', loadChildren: './doc-details/doc-details.module#DocDetailsModule' },
      { path: 'pa-tasks', loadChildren: './pa-tasks/pa-tasks.module#PaTasksModule' },
      { path: 'pa-activities', loadChildren: './pa-activities/pa-activities.module#PaActivitiesModule' },
      { path: 'pa-mapping', loadChildren: './pa-mapping/pa-mapping.module#PaMappingModule' },
      {path: '', redirectTo: 'doc-types', pathMatch: 'full'}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderContractingRoutingModule { }
