import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { DynamicObject, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES, REGEX } from '../../../../../@core/entities/constants';
import { AlertService, GenericService } from '../../../../../@core/services';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';
import { AdminUrlConfig } from '../../../admin-url.config';
import { MasterProviderContracting, PaActivitiesPC } from '../../_entities/daconfig.data.models';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'pa-activities',
    templateUrl: './pa-activities.component.html',
    styleUrls: ['./pa-activities.component.scss']
})
export class PaActivitiesComponent implements OnInit {
    addEditLabel: string;
    isEditMode = false;
    paActivitiesformGroup: FormGroup;
    paActivitiesPC$: Observable<PaActivitiesPC[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    providerContractingConfig: MasterProviderContracting;
    paActivitiesPC: PaActivitiesPC;
    paginationInfo: PaginationInfo = new PaginationInfo();
    currentDaStatusData = new PaActivitiesPC();
    private dynamicObject: DynamicObject = {};
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();
    constructor(private formBuilder: FormBuilder, private _service: GenericService<PaActivitiesPC>, private _alertService: AlertService) {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.DAConfig.AmactivityUrl;
    }

    ngOnInit() {
        this.paginationInfo.sortBy = 'name asc';
        this.paActivitiesformGroup = this.formBuilder.group({
            name: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            description: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]]
        });
        this.getPage();
    }
    getPage() {
        const pageSource = this.pageStream$.map((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObject, page: pageNumber };
        });

        const searchSource = this.searchTermStream$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObject = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .mergeMap((params: { search: DynamicObject; page: number }) => {
                return this._service
                    .getPagedArrayList(
                        {
                            limit: this.paginationInfo.pageSize,
                            order: this.paginationInfo.sortBy,
                            page: params.page,
                            count: this.paginationInfo.total,
                            method: 'get',
                            where: params.search
                        },
                        AdminUrlConfig.EndPoint.DAConfig.AmactivityUrl + '/list?filter'
                    )
                    .map((result) => {
                        return { data: result.data, count: result.count, canDisplayPager: result.count > this.paginationInfo.pageSize };
                    });
            })
            .share();
        this.paActivitiesPC$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    saveItem() {
        this.currentDaStatusData.description = this.paActivitiesformGroup.value.description;
        if (this.currentDaStatusData.amactivityid) {
            this._service.update(this.currentDaStatusData.amactivityid, this.currentDaStatusData).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Pa activities type saved successfully');
                        this.paginationInfo.sortBy = 'insertedon desc';
                        this.pageStream$.next(1);
                        this.paActivitiesformGroup.patchValue({
                            nameSearch: this.currentDaStatusData.name
                        });
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this.currentDaStatusData.activitytypekey = 'Investigation';
            this.currentDaStatusData.name = this.paActivitiesformGroup.value.name;
            this.currentDaStatusData.effectivedate = new Date();
            this.currentDaStatusData.activeflag = 1;
            this._service.create(this.currentDaStatusData).subscribe(
                (response) => {
                    if (response.amactivityid) {
                        this._alertService.success('Pa activities type saved successfully');
                        this.pageStream$.next(this.paginationInfo.pageNumber);
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    editItem(currentdata) {
        this.isEditMode = true;
        this.currentDaStatusData = Object.assign({}, currentdata);
        this.paActivitiesformGroup.setValue({
            name: this.currentDaStatusData.name,
            description: this.currentDaStatusData.description
        });
    }
    changeModelLabel(addEdit) {
        this.isEditMode = false;
        this.addEditLabel = addEdit;
        if (addEdit === 'Add Task') {
            this.paActivitiesformGroup.reset();
            this.currentDaStatusData = Object.assign({}, new PaActivitiesPC());
        }
    }
    deleteItem() {
        this._service.remove(this.paActivitiesPC.amactivityid).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Pa activities type deleted successfully');
                    this.pageStream$.next(this.paginationInfo.pageNumber);
                    (<any>$('#delete-popup')).modal('hide');
                }
            },
            (error) => {
                (<any>$('#delete-popup')).modal('hide');
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    declineDelete() {
        (<any>$('#delete-popup')).modal('hide');
    }

    confirmDelete(requestData: PaActivitiesPC) {
        this.paActivitiesPC = requestData;
        (<any>$('#delete-popup')).modal('show');
    }
    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortColumn + ' ' + $event.sortDirection;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    onSearch(field: string, value: string) {
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }
}
