import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'add-edit-doc-types',
  templateUrl: './add-edit-doc-types.component.html',
  styleUrls: ['./add-edit-doc-types.component.scss']
})
export class AddEditDocTypesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.showDocTypeEditor();
  }

  showDocTypeEditor() {
    (<any>$('#add-edit-doc-types')).modal('show');
  }

}
