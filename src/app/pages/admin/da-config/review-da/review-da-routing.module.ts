import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReviewDaComponent } from './review-da.component';

const routes: Routes = [
  {
    path: '',
    component: ReviewDaComponent,
    children: [
      { path: 'review-configuration', loadChildren: './review-configuration/review-configuration.module#ReviewConfigurationModule' },
      { path: 'edl-review-results', loadChildren: './edl-review-results/edl-review-results.module#EdlReviewResultsModule' },
      { path: 'management-review-results', loadChildren: './management-review-results/management-review-results.module#ManagementReviewResultsModule' },
      { path: 'supervisor-review-results', loadChildren: './supervisor-review-results/supervisor-review-results.module#SupervisorReviewResultsModule' },
      {path: '', redirectTo: 'review-configuration', pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewDaRoutingModule { }
