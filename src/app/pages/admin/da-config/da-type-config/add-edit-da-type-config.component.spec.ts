import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';

import { CoreModule } from '../../../../@core/core.module';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { AddEditDaTypeConfigComponent } from './add-edit-da-type-config.component';
import { DaTypeConfigComponent } from './da-type-config.component';
import { DefaultAccessListComponent } from './default-access-list/default-access-list.component';
import { DispositionComponent } from './disposition/disposition.component';
import { DocTypeComponent } from './doc-type/doc-type.component';
import { EntityConfigComponent } from './entity-config/entity-config.component';
import { ManageAlertsComponent } from './manage-alerts/manage-alerts.component';
import { PersonRolesComponent } from './person-roles/person-roles.component';
import { UserRolesComponent } from './user-roles/user-roles.component';

describe('AddEditDaTypeConfigComponent', () => {
    let component: AddEditDaTypeConfigComponent;
    let fixture: ComponentFixture<AddEditDaTypeConfigComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            // tslint:disable-next-line:max-line-length
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                A2Edatetimepicker,
                SharedPipesModule
            ],
            declarations: [
                AddEditDaTypeConfigComponent,
                DaTypeConfigComponent,
                AddEditDaTypeConfigComponent,
                PersonRolesComponent,
                DispositionComponent,
                DefaultAccessListComponent,
                EntityConfigComponent,
                UserRolesComponent,
                DocTypeComponent,
                ManageAlertsComponent
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddEditDaTypeConfigComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
