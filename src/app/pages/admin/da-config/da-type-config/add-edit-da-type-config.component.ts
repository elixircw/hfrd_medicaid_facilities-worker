import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService, GenericService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AdminUrlConfig } from '../../admin-url.config';
import * as categories from './_configurations/category.json';
import * as duedate from './_configurations/duedate.json';
import * as workload from './_configurations/workload.json';
import { DaTypeConfigMainData, DATypeScreenStatus, ServiceRequestDispositionConfig, ServiceRequestTypeConfig } from './_entities/da-type-config.models';
import { LENGTH } from '../../../../@core/entities/constants';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'add-edit-da-type-config',
    templateUrl: './add-edit-da-type-config.component.html',
    styleUrls: ['./add-edit-da-type-config.component.scss']
})
export class AddEditDaTypeConfigComponent implements OnInit {
    id = '0';
    hasMainContentFilled = false;
    isEditMode = false;
    addEditDaTypeConfigForm: FormGroup;
    rolesEntityForm: FormGroup;
    daTypeDropdownItems$: Observable<DropdownModel[]>;
    daSubTypeDropdownItems$: Observable<DropdownModel[]>;
    planTypeDropdownItems$: Observable<DropdownModel[]>;
    daTypeConfig = new DaTypeConfigMainData();
    daTypeScreenStatus: DATypeScreenStatus = new DATypeScreenStatus();
    dispositions$: Observable<ServiceRequestDispositionConfig[]>;
    personRolesTypes$: Observable<ServiceRequestTypeConfig[]>;
    entityCategories$: Observable<ServiceRequestTypeConfig[]>;
    roleEntityTypeChecked: DaTypeConfigMainData = new DaTypeConfigMainData();
    categoriesDropdown: DropdownModel[];
    workLoadDropdown: DropdownModel[];
    dueDateDropdown: DropdownModel[];
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _daTypeConfigService: GenericService<DaTypeConfigMainData>,
        private _commonHttpService: CommonHttpService
    ) {}
    ngOnInit() {
        this.categoriesDropdown = <any>categories;
        this.workLoadDropdown = <any>workload;
        this.dueDateDropdown = <any>duedate;
        this.formGroupInitialize();
        this.loadMainDropdowns();
        this.route.params.subscribe((item) => {
            this.id = item['id'];
            if (this.id !== '0') {
                this.isEditMode = true;
                this._daTypeConfigService.getById(this.id, AdminUrlConfig.EndPoint.DAConfig.ServiceRequestTypeConfigUrl).subscribe((result) => {
                    this.hasMainContentFilled = true;
                    this.addEditDaTypeConfigForm.patchValue(result);
                    this.addEditDaTypeConfigForm.patchValue({effectivedate: new Date(result.effectivedate), expirationdate: new Date(result.expirationdate)});
                    this.onChangeDaSubType(result.intakeservreqtypeid);
                    this.roleEntityTypeChecked = result;
                });
            }
            (<any>$('#add-DA-configuration')).modal('show');
        });
    }
    closeItem() {
        (<any>$('#add-DA-configuration')).modal('hide');
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        };
        const currentUrl = 'pages/admin/da-config/da-type-config';
        this.router.navigateByUrl(currentUrl).then(() => {
            this.router.navigated = false;
            this.router.navigate([currentUrl]);
        });
    }

    formGroupInitialize() {
        this.addEditDaTypeConfigForm = this.formBuilder.group(
            {
                intakeservreqtypeid: ['', Validators.required],
                servicerequestsubtypeid: ['', Validators.required],
                intakeservicerequestplantypekey: ['', Validators.required],
                internalfile: [false],
                duedateoffset: ['', Validators.required],
                limitedrouting: [false],
                focusroletype: [''],
                category: ['', Validators.required],
                effectivedate: ['', Validators.required],
                expirationdate: ['', [Validators.required , LENGTH.MIN_LENGTH_VALIDATOR]],
                workload: ['', Validators.required],
                intakeserreqstatustypeid: [''],
                displaydatype: [''],
                displaydasubtype: [''],
                focusentitytype: ['']
            },
            { validator: this.checkDateRange }
        );
    }
    checkDateRange(group: FormGroup) {
        if (group.controls.expirationdate.value) {
            if (group.controls.expirationdate.value < group.controls.effectivedate.value) {
                return { notValid: true };
            }
            return null;
        }
    }
    loadMainDropdowns() {
        const source = forkJoin([
            this._commonHttpService.getPagedArrayList(
                new PaginationRequest({
                    order: 'intakeservreqtypekey ASC',
                    nolimit: true,
                    where: { archiveon: null },
                    method: 'get'
                }),
                AdminUrlConfig.EndPoint.DAConfig.DaTypeUrl + '/list?filter'
            ),
            this._commonHttpService.getPagedArrayList(
                new PaginationRequest({
                    order: 'intakeservicerequestplantypekey ASC',
                    nolimit: true,
                    where: { activeflag: '1' },
                    method: 'get'
                }),
                AdminUrlConfig.EndPoint.DAConfig.PlanTypeUrl + '/list?filter'
            )
        ])
            .map((resultvalue) => {
                return {
                    daTypes: resultvalue[0].data.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeservreqtypeid
                            })
                    ),
                    planTypes: resultvalue[1].data.map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.intakeservicerequestplantypekey
                            })
                    )
                };
            })
            .share();
        this.daTypeDropdownItems$ = source.pluck('daTypes');
        this.planTypeDropdownItems$ = source.pluck('planTypes');
    }
    onChangeDaSubType(intakeservreqtypeid: string) {
        this.daSubTypeDropdownItems$ = this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    order: 'workloadweight ASC',
                    nolimit: true,
                    where: { intakeservreqtypeid: intakeservreqtypeid },
                    method: 'get'
                }),
                AdminUrlConfig.EndPoint.DAConfig.DaConfigSubtypeUrl + '/list?filter'
            )
            .map((result) => {
                return result.data.map(
                    (res) =>
                        new DropdownModel({
                            text: res.classkey,
                            value: res.servicerequestsubtypeid
                        })
                );
            });
    }
    changeRoleType(roleType: string) {
        if (roleType === 'Role') {
            this.addEditDaTypeConfigForm.get('focusentitytype').disable();
            this.addEditDaTypeConfigForm.get('focusroletype').enable();
            this.addEditDaTypeConfigForm.patchValue({ focusentitytype: '' });
            this.addEditDaTypeConfigForm.value.focusentitytype = null;
        } else if (roleType === 'Entity') {
            this.addEditDaTypeConfigForm.get('focusroletype').disable();
            this.addEditDaTypeConfigForm.get('focusentitytype').enable();
            this.addEditDaTypeConfigForm.patchValue({ focusroletype: '' });
            this.addEditDaTypeConfigForm.value.focusroletype = null;
        }
    }
    saveDaType(modal: DaTypeConfigMainData) {
        if (this.addEditDaTypeConfigForm.dirty && this.addEditDaTypeConfigForm.valid) {
            modal = Object.assign(new DaTypeConfigMainData(), modal);
            this._daTypeConfigService.endpointUrl = AdminUrlConfig.EndPoint.DAConfig.ServiceRequestTypeConfigUrl;
            ObjectUtils.removeEmptyProperties(modal);
            if (this.id === '0') {
                modal.insertedby = this._authService.getCurrentUser().userId;
                this._daTypeConfigService.create(modal).subscribe(
                    (response) => {
                        this.id = response.servicerequesttypeconfigid;
                        this.isEditMode = true;
                        this.hasMainContentFilled = true;
                        this._alertService.success('Service type saved successfully');
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            }
        } else {
            ControlUtils.validateAllFormFields(this.addEditDaTypeConfigForm);
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill mandatory fields');
        }
    }
    updateDaType(modal: DaTypeConfigMainData) {
        if (this.addEditDaTypeConfigForm.dirty && this.addEditDaTypeConfigForm.valid) {
            modal = Object.assign(new DaTypeConfigMainData(), modal);
            this._daTypeConfigService.endpointUrl = AdminUrlConfig.EndPoint.DAConfig.ServiceRequestTypeConfigUrl;
            ObjectUtils.removeEmptyProperties(modal);
            modal.updatedby = this._authService.getCurrentUser().userId;
            modal.insertedby = this._authService.getCurrentUser().userId;
            this._daTypeConfigService.patch(this.id, modal).subscribe(
                (response) => {
                    this.id = response.servicerequesttypeconfigid;
                    this.hasMainContentFilled = true;
                    this.closeItem();
                    this._alertService.success('Service type updated successfully');
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }
    dispositionListed(dispositions$: Observable<ServiceRequestDispositionConfig[]>) {
        this.dispositions$ = Observable.empty();
        this.dispositions$ = dispositions$;
    }
    focusCategoryRolesListed(entityCategories$: Observable<ServiceRequestTypeConfig[]>) {
        this.entityCategories$ = Observable.empty();
        this.entityCategories$ = entityCategories$;
    }
    focusRoleTypeListedItems(personRolesTypes$: Observable<ServiceRequestTypeConfig[]>) {
        this.personRolesTypes$ = Observable.empty();
        this.personRolesTypes$ = personRolesTypes$;
    }
}
