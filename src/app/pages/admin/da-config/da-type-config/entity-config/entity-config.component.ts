import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { GenericService, AlertService } from '../../../../../@core/services';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DATypeScreenStatus, ServiceRequestTypeConfig } from '../_entities/da-type-config.models';
import { AdminUrlConfig } from '../../../admin-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'entity-config',
    templateUrl: './entity-config.component.html',
    styleUrls: ['./entity-config.component.scss']
})
export class EntityConfigComponent implements OnInit {
    @Input() id: string;
    @Input() screenStatus: DATypeScreenStatus;
    @Output() focusentityCategoryRolesListed = new EventEmitter<Observable<ServiceRequestTypeConfig[]>>();
    entityConfigCategoryForm: FormGroup;
    entityConfigRolesForm: FormGroup;
    entityRoles$: Observable<ServiceRequestTypeConfig[]>;
    entityCategories$: Observable<ServiceRequestTypeConfig[]>;
    entityRoleDropdownList$: Observable<DropdownModel[]>;
    entityCategoryDropdownList$: Observable<DropdownModel[]>;
    constructor(
        private fromBuilder: FormBuilder,
        private _alertService: AlertService,
        private _serviceRequestTypeConfigService: GenericService<ServiceRequestTypeConfig>,
        private _entityConfigHttpService: CommonHttpService
    ) {
        this._serviceRequestTypeConfigService.endpointUrl = 'daconfig/servicerequesttypeconfigrole';
    }
    ngOnInit() {
        this.categoryFormInitilize();
        this.rolesFormInitilize();
        this.loadDropdownItems();
        if (this.id !== '0') {
            this.LoadCategoryEntityRole();
            this.loadEntityRoleType();
        }
    }
    categoryFormInitilize() {
        this.entityConfigCategoryForm = this.fromBuilder.group({
            entityroletypekey: ['', Validators.required]
        });
    }
    rolesFormInitilize() {
        this.entityConfigRolesForm = this.fromBuilder.group({
            entityroletypekey: ['', Validators.required]
        });
    }
    loadDropdownItems() {
        const source = forkJoin([
            this._entityConfigHttpService.getArrayList(new PaginationRequest({ nolimit: true, method: 'get' }), AdminUrlConfig.EndPoint.DAConfig.GetAgencyRoleTypeUrl + '?filter'),
            this._entityConfigHttpService.getArrayList(new PaginationRequest({ nolimit: true, method: 'get' }), AdminUrlConfig.EndPoint.DAConfig.GetAgencyCategoryUrl + '?filter')
        ])
            .map((resultsVal) => {
                return {
                    entityRoleDropdownList: resultsVal[0].map((res) => new DropdownModel({ text: res.typedescription, value: res.agencyroletypekey })),
                    entityCategoryDropdownList: resultsVal[1].map((res) => new DropdownModel({ text: res.description, value: res.agencycategorykey }))
                };
            })
            .share();
        this.entityRoleDropdownList$ = source.pluck('entityRoleDropdownList');
        this.entityCategoryDropdownList$ = source.pluck('entityCategoryDropdownList');
    }
    saveEntityCategory(modal: ServiceRequestTypeConfig) {
        let Category = [];
        this.entityCategories$.subscribe((categorySelect) => {
            if (categorySelect) {
                Category = categorySelect.filter((item) => item.name === modal.entityroletypekey);
            }
            if (!Category.length) {
                this._serviceRequestTypeConfigService.endpointUrl = 'daconfig/servicerequesttypeconfigrole';
                modal = Object.assign(new ServiceRequestTypeConfig(), modal);
                modal.servicerequesttypeconfigid = this.id;
                modal.entityroletype = 'EntityRole';
                modal.entityroletypekey = this.entityConfigCategoryForm.value.entityroletypekey;
                this._serviceRequestTypeConfigService.create(modal).subscribe(
                    (response) => {
                        this._alertService.success('Category added successfully');
                        this.LoadCategoryEntityRole();
                        this.categoryFormInitilize();
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                this._alertService.error('Category Already exists');
            }
        });
    }

    saveEntityRole(modal: ServiceRequestTypeConfig) {
        let Provider = [];
        this.entityRoles$.subscribe((providerSelect) => {
            if (providerSelect) {
                Provider = providerSelect.filter((item) => item.name === modal.entityroletypekey);
            }
            if (!Provider.length) {
                this._serviceRequestTypeConfigService.endpointUrl = 'daconfig/servicerequesttypeconfigrole';
                modal = Object.assign(new ServiceRequestTypeConfig(), modal);
                modal.servicerequesttypeconfigid = this.id;
                modal.entityroletype = 'EntityRoleType';
                modal.entityroletypekey = this.entityConfigRolesForm.value.entityroletypekey;
                this._serviceRequestTypeConfigService.create(modal).subscribe(
                    (response) => {
                        this._alertService.success('Provider added successfully');
                        this.loadEntityRoleType();
                        this.rolesFormInitilize();
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                this._alertService.error('Provider Already exists');
            }
        });
    }
    deleteCategoryRole(data) {
        this._serviceRequestTypeConfigService.endpointUrl = 'daconfig/servicerequesttypeconfigrole';
        this._serviceRequestTypeConfigService.patch(data.id, { activeflag: 0 }).subscribe(
            (response) => {
                if (response) {
                    this.LoadCategoryEntityRole();
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    deleteEntiryRoleType(data) {
        this._serviceRequestTypeConfigService.endpointUrl = 'daconfig/servicerequesttypeconfigrole';
        this._serviceRequestTypeConfigService.patch(data.id, { activeflag: 0 }).subscribe(
            (response) => {
                if (response) {
                    this.loadEntityRoleType();
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    LoadCategoryEntityRole() {
        this._entityConfigHttpService.endpointUrl = 'daconfig/servicerequesttypeconfigrole';
        this.entityCategories$ = this._entityConfigHttpService
            .getPagedArrayList(
                {
                    where: {
                        servicerequesttypeconfigid: this.id,
                        activeflag: 1,
                        entityroletype: 'EntityRole'
                    },
                    method: 'get'
                },
                'daconfig/servicerequesttypeconfigrole' + '/list?filter'
            )
            .map((result) => {
                this.screenStatus.hasEntityRole = !(result.data === null || result.data.length === 0);
                return result.data;
            });
        this.focusentityCategoryRolesListed.emit(this.entityCategories$);
    }
    loadEntityRoleType() {
        this._entityConfigHttpService.endpointUrl = 'daconfig/servicerequesttypeconfigrole';
        this.entityRoles$ = this._entityConfigHttpService
            .getAllPaged({
                where: {
                    servicerequesttypeconfigid: this.id,
                    activeflag: 1,
                    entityroletype: 'EntityRoleType'
                }
            })
            .map((result) => {
                this.screenStatus.hasEntityConfig = !(result.data === null || result.data.length === 0);
                return result.data;
            });
    }
}
