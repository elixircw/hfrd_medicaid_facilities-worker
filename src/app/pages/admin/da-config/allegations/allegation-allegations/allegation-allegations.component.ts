import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import {
    DropdownModel,
    DynamicObject,
    PaginationInfo,
    PaginationRequest,
} from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES, REGEX } from '../../../../../@core/entities/constants';
import { AlertService, CommonHttpService, GenericService } from '../../../../../@core/services';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';
import { AdminUrlConfig } from '../../../admin-url.config';
import { AllegationAllegations, Indicator } from '../../_entities/daconfig.data.models';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'allegation-allegations',
    templateUrl: './allegation-allegations.component.html',
    styleUrls: ['./allegation-allegations.component.scss']
})
export class AllegationAllegationsComponent implements OnInit {
    labelAddEdit: string;
    allegationForm: FormGroup;
    indicatorForm: FormGroup;
    totalRecords$: Observable<number>;
    allegationAllegations$: Observable<AllegationAllegations[]>;
    allegationAllegation$: Observable<AllegationAllegations>;
    allegationAllegationData: AllegationAllegations = new AllegationAllegations();
    dynamicObject: DynamicObject = {};
    paginationInfo: PaginationInfo = new PaginationInfo();
    daTypeDropdownItems$: Observable<DropdownModel[]>;
    canDisplayPager$: Observable<boolean>;
    daSubTypeDropdownItems$: Observable<DropdownModel[]>;
    indicators$: Observable<Indicator[]>;
    indicatorsData: Indicator[] = [];
    indicator: Indicator;
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();
    constructor(
        private formBuilder: FormBuilder,
        private _serviceAllegation: GenericService<AllegationAllegations>,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService
    ) {
        this._serviceAllegation.endpointUrl = AdminUrlConfig.EndPoint.DAConfig.AllegationUrl;
        this.allegationForm = this.formBuilder.group({
            name: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            self: [''],
            workload: [''],
            nameSearch: [''],
            intakeservicereqtypeid: ['', Validators.required],
            intakeservicereqsubtypeid: ['', Validators.required],
            indicatorname: ['']
        });
    }

    ngOnInit() {
        this.paginationInfo.sortBy = 'name asc';

        this.getPage();
    }

    addIndicator() {
        this.indicator = Object.assign(new Indicator(), {});
        if (this.allegationForm.value.indicatorname.trim().length >= 1) {
            this.indicator.indicatorname = this.allegationForm.value.indicatorname;
            this.indicatorsData.push(this.indicator);
            this.indicatorsData = this.indicatorsData.map((item, ix) => {
                item.index = ix;
                return item;
            });
            this.indicators$ = Observable.of(this.indicatorsData);
            this.allegationForm.patchValue({ indicatorname: '' });
        }
    }

    deleteIndicator(indicatorItem) {
        this.indicatorsData.splice(indicatorItem.index, 1);
        this.indicatorsData = this.indicatorsData.map((item, ix) => {
            item.index = ix;
            return item;
        });
        this.indicators$ = Observable.of(this.indicatorsData);
        this.allegationForm.markAsDirty();
    }

    onChangeDaType(intakeservreqtypeid: string) {
        this.daSubTypeDropdownItems$ = this.loadDASubTypes(intakeservreqtypeid);
    }

    getPage() {
        const pageSource = this.pageStream$.map((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObject, page: pageNumber };
        });

        const searchSource = this.searchTermStream$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObject = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .mergeMap((params: { search: DynamicObject; page: number }) => {
                return this._serviceAllegation
                    .getPagedArrayList(
                        {
                            limit: this.paginationInfo.pageSize,
                            order: this.paginationInfo.sortBy,
                            page: params.page,
                            count: this.paginationInfo.total,
                            method: 'get',
                            where: params.search
                        },
                        AdminUrlConfig.EndPoint.DAConfig.AllegationUrl + '/list?filter'
                    )
                    .map((result) => {
                        return { data: result.data, count: result.count, canDisplayPager: result.count > this.paginationInfo.pageSize };
                    });
            })
            .share();

        this.allegationAllegations$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    saveItem() {
        this.allegationAllegationData.name = this.allegationForm.value.name;
        this.allegationAllegationData.self = this.allegationForm.value.self;
        this.allegationAllegationData.workload = this.allegationForm.value.workload;
        this.allegationAllegationData.intakeservicereqtypeid = this.allegationForm.value.intakeservicereqtypeid;
        this.allegationAllegationData.intakeservicereqsubtypeid = this.allegationForm.value.intakeservicereqsubtypeid;
        this.allegationAllegationData.indicator = this.indicatorsData;
        if (this.allegationAllegationData.allegationid) {
            this._serviceAllegation.patch(this.allegationAllegationData.allegationid, this.allegationAllegationData, this._serviceAllegation.endpointUrl + '/updateAllegation').subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Allegation saved successfully');
                        this.pageStream$.next(this.paginationInfo.pageNumber);
                        (<any>$('#myModal-allegation-allegations-add')).modal('hide');
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._serviceAllegation.create(this.allegationAllegationData, this._serviceAllegation.endpointUrl + '/addAllegation').subscribe(
                (response) => {
                    if (response.allegationid) {
                        this._alertService.success('Allegation saved successfully');
                        this.paginationInfo.sortBy = 'insertedon desc';
                        this.pageStream$.next(1);
                        this.allegationForm.patchValue({
                            nameSearch: this.allegationAllegationData.name
                        });
                        (<any>$('#myModal-allegation-allegations-add')).modal('hide');
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    editItem(allegation: AllegationAllegations) {
        this.changeModelLabel('Edit');
        this.daTypeDropdownItems$ = this.loadDATypes();
        this.indicators$ = Observable.of([]);
        this.allegationForm.markAsPristine();
        this.loadAllegation(allegation);
    }

    changeModelLabel(addEdit) {
        this.labelAddEdit = addEdit;
        if (addEdit === 'Add') {
            this.clearItems();
            this.allegationAllegationData = Object.assign({}, new AllegationAllegations());
            this.daTypeDropdownItems$ = this.loadDATypes();
            this.indicatorsData = [];
            this.indicators$ = Observable.of([]);
        }
    }

    loadDATypes() {
        return this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    order: 'intakeservreqtypekey ASC',
                    where: { archiveon: null },
                    method: 'get',
                    nolimit: true
                }),
                AdminUrlConfig.EndPoint.DAConfig.DaTypeUrl + '/list?filter'
            )
            .map((result) =>
                result['data'].map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.intakeservreqtypeid
                        })
                )
            );
    }

    loadDASubTypes(intakeservreqtypeid: string) {
        return this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    order: 'workloadweight ASC',
                    where: { intakeservreqtypeid: intakeservreqtypeid },
                    method: 'get',
                    nolimit: true
                }),
                AdminUrlConfig.EndPoint.DAConfig.DaConfigSubtypeUrl + '/list?filter'
            )
            .map((result) =>
                result['data'].map(
                    (res) =>
                        new DropdownModel({
                            text: res.classkey,
                            value: res.servicerequestsubtypeid
                        })
                )
            );
    }

    loadAllegation(allegationItem) {
        // this.allegationAllegationData = this.allegationAllegationData;
        const __this = this;
        this._serviceAllegation.getSingle(new PaginationRequest({ method: 'get' }), this._serviceAllegation.endpointUrl + '/getAllegation/' + allegationItem.allegationid + '?filter').subscribe(
            (response) => {
                const result = response[0];
                __this.allegationAllegationData = Object.assign({}, result);

                __this.allegationForm.patchValue({
                    name: result.name,
                    self: result.self,
                    workload: result.workload,
                    intakeservicereqtypeid: result.intakeservicereqtypeid
                });
                __this.daSubTypeDropdownItems$ = this.loadDASubTypes(result.intakeservicereqtypeid);

                __this.allegationForm.patchValue({
                    name: result.name,
                    self: result.self,
                    workload: result.workload,
                    intakeservicereqtypeid: result.intakeservicereqtypeid
                    // intakeservicereqsubtypeid: result.intakeservicereqsubtypeid
                });
                __this.daSubTypeDropdownItems$ = this.loadDASubTypes(result.intakeservicereqtypeid);

                __this.daSubTypeDropdownItems$.subscribe(() => {
                    __this.allegationForm.patchValue({
                        intakeservicereqsubtypeid: result.intakeservicereqsubtypeid
                    });
                });

                __this.indicatorsData = result.indicator;
                let index = -1;
                __this.indicatorsData.forEach((x) => (x.index = ++index));
                __this.indicators$ = Observable.of(result.indicator);
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    deleteItem() {
        this._serviceAllegation.remove(this.allegationAllegationData.allegationid).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Allegation deleted successfully');
                    this.pageStream$.next(this.paginationInfo.pageNumber);
                    (<any>$('#delete-popup')).modal('hide');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    clearItems() {
        this.allegationForm.reset();
    }
    declineDelete() {
        (<any>$('#delete-popup')).modal('hide');
    }

    confirmDelete(allegation: AllegationAllegations) {
        this.allegationAllegationData = allegation;
        (<any>$('#delete-popup')).modal('show');
    }
    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortColumn + ' ' + $event.sortDirection;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    onSearch(field: string, value: string) {
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }
}
