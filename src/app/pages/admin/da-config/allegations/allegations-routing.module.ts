import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleGuard } from '../../../../@core/guard';
import { AllegationsComponent } from './allegations.component';

const routes: Routes = [
    {
        path: '',
        component: AllegationsComponent,
        canActivate: [RoleGuard],
        children: [
            { path: 'allegation-configuration', loadChildren: './allegation-configuration/allegation-configuration.module#AllegationConfigurationModule' },
            { path: 'allegation-activities', loadChildren: './allegation-activities/allegation-activities.module#AllegationActivitiesModule' },
            { path: 'allegation-goals', loadChildren: './allegation-goals/allegation-goals.module#AllegationGoalsModule' },
            { path: 'allegation-mappings', loadChildren: './allegation-mappings/allegation-mappings.module#AllegationMappingsModule' },
            { path: 'allegation-tasks', loadChildren: './allegation-tasks/allegation-tasks.module#AllegationTasksModule' },
            { path: 'allegation-allegations', loadChildren: './allegation-allegations/allegation-allegations.module#AllegationAllegationsModule' },
            { path: 'assessment-score-usage', loadChildren: './assessment-score-usage/assessment-score-usage.module#AssessmentScoreUsageModule' },
            { path: '', redirectTo: 'allegation-configuration', pathMatch: 'full' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AllegationsRoutingModule {}
