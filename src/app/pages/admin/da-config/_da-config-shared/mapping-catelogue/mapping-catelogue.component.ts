import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { MasterCatelogConfig } from '../../../general/_entities/general.data.models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mapping-catelogue',
  templateUrl: './mapping-catelogue.component.html',
  styleUrls: ['./mapping-catelogue.component.scss']
})
export class MappingCatelogueComponent implements OnInit {
  @Input() componentConfig: MasterCatelogConfig;
  @Input() catelogList: any[];
  @Output() catelogSaved = new EventEmitter<any>();
  catelogData: any;

  constructor() { }

  ngOnInit() {
  }

  clearCatelog() {

  }

  editCatelog(catelog) {
    this.catelogData = catelog;
  }

  saveCatelog(catelog) {
    this.catelogSaved.emit(catelog);
  }
}
