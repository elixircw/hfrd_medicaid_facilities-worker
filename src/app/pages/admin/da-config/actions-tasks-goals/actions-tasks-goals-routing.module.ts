import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActionsTasksGoalsComponent } from './actions-tasks-goals.component';


const routes: Routes = [
  {
    path: '',
    component: ActionsTasksGoalsComponent,
    children: [
      { path: 'dsds-action-mapping-categories', loadChildren: './dsds-action-mapping-categories/dsds-action-mapping-categories.module#DsdsActionMappingCategoriesModule' },
      { path: 'dsds-action-goals', loadChildren: './dsds-action-goals/dsds-action-goals.module#DsdsActionGoalsModule' },
      { path: 'dsds-action-tasks', loadChildren: './dsds-action-tasks/dsds-action-tasks.module#DsdsActionTasksModule' },
      { path: 'dsds-action-mappings', loadChildren: './dsds-action-mappings/dsds-action-mappings.module#DsdsActionMappingsModule' },
      {path: '', redirectTo: 'dsds-action-mapping-categories', pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActionsTasksGoalsRoutingModule { }
