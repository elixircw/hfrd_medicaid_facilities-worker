import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { DynamicObject, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES, REGEX } from '../../../../../@core/entities/constants';
import { AlertService, GenericService } from '../../../../../@core/services';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';
import { AdminUrlConfig } from '../../../admin-url.config';
import { DsdsActionMappingCateg } from '../../_entities/daconfig.data.models';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dsds-action-mapping-categories',
    templateUrl: './dsds-action-mapping-categories.component.html',
    styleUrls: ['./dsds-action-mapping-categories.component.scss']
})
export class DsdsActionMappingCategoriesComponent implements OnInit {
    addEditLabel: string;
    actionMappingCategFormGroup: FormGroup;
    dsdsActionMappingCateg$: Observable<DsdsActionMappingCateg[]>;
    canDisplayPager$: Observable<boolean>;
    actionMappingCategData: DsdsActionMappingCateg;
    totalRecords$: Observable<number>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    private dynamicObject: DynamicObject = {
        activitytypekey: 'Investigation',
        activeflag: 1
    };
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();

    constructor(private _actionMappingCategService: GenericService<DsdsActionMappingCateg>, private _alertService: AlertService, private formBuilder: FormBuilder) {
        this._actionMappingCategService.endpointUrl = AdminUrlConfig.EndPoint.DAConfig.AmactivityUrl;
        this.actionMappingCategFormGroup = this.formBuilder.group({
            name: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            description: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]]
        });
    }
    ngOnInit() {
        this.paginationInfo.sortBy = 'name asc';
        this.getPage();
    }

    getPage() {
        const pageSource = this.pageStream$.map((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObject, page: pageNumber };
        });

        const searchSource = this.searchTermStream$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObject = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .mergeMap((params: { search: DynamicObject; page: number }) => {
                return this._actionMappingCategService
                    .getPagedArrayList(
                        {
                            limit: this.paginationInfo.pageSize,
                            order: this.paginationInfo.sortBy,
                            page: params.page,
                            count: this.paginationInfo.total,
                            where: this.dynamicObject,
                            method: 'get'
                        },
                        AdminUrlConfig.EndPoint.DAConfig.AmactivityUrl + '/list?filter'
                    )
                    .map((result) => {
                        return { data: result.data, count: result.count, canDisplayPager: result.count > this.paginationInfo.pageSize };
                    });
            })
            .share();

        this.dsdsActionMappingCateg$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    changeModelLabel(addEdit) {
        this.addEditLabel = addEdit;
        if (addEdit === 'Add') {
            this.actionMappingCategFormGroup.reset();
            this.actionMappingCategData = Object.assign({});
        }
    }
    editActionMappingCateg(modelData) {
        this.changeModelLabel('Edit');
        this.actionMappingCategData = Object.assign({}, modelData);
        this.actionMappingCategFormGroup.setValue({
            name: this.actionMappingCategData.name,
            description: this.actionMappingCategData.description
        });
    }
    saveItem() {
        this.actionMappingCategData.name = this.actionMappingCategFormGroup.value.name;
        this.actionMappingCategData.description = this.actionMappingCategFormGroup.value.description;
        this.actionMappingCategData.activitytypekey = 'Investigation';
        this.actionMappingCategData.activeflag = 1;
        this.actionMappingCategData.effectivedate = new Date();
        if (this.actionMappingCategData.amactivityid) {
            this._actionMappingCategService.update(this.actionMappingCategData.amactivityid, this.actionMappingCategData).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Activity category saved successfully');
                        this.pageStream$.next(this.paginationInfo.pageNumber);
                        (<any>$('#myModal-action-mapping-categories-add-edit')).modal('hide');
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._actionMappingCategService.create(this.actionMappingCategData).subscribe(
                (response) => {
                    if (response.amactivityid) {
                        this._alertService.success('Activity category saved successfully');
                        this.paginationInfo.sortBy = 'insertedon desc';
                        this.pageStream$.next(1);
                        (<any>$('#myModal-action-mapping-categories-add-edit')).modal('hide');
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }

    confirmDelete(modelData: DsdsActionMappingCateg) {
        this.actionMappingCategData = Object.assign({}, modelData);
        (<any>$('#delete-popup')).modal('show');
    }

    deleteItem() {
        this._actionMappingCategService.remove(this.actionMappingCategData.amactivityid).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Activity category deleted successfully');
                    this.pageStream$.next(this.paginationInfo.pageNumber);
                    (<any>$('#delete-popup')).modal('hide');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                (<any>$('#delete-popup')).modal('hide');
            }
        );
    }

    clearItem() {
        this.actionMappingCategFormGroup.reset();
        this.actionMappingCategData = Object.assign({}, new DsdsActionMappingCateg());
    }
    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortColumn + ' ' + $event.sortDirection;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    onSearch(field: string, value: string) {
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }
}
