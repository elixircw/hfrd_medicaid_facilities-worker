import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchAnnualApplicationComponent } from './batch-annual-application.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '../../../@core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';

describe('BatchAnnualApplicationComponent', () => {
    let component: BatchAnnualApplicationComponent;
    let fixture: ComponentFixture<BatchAnnualApplicationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, CoreModule.forRoot(), HttpClientModule, FormsModule, ReactiveFormsModule, ControlMessagesModule, PaginationModule, NgSelectModule],
            declarations: [BatchAnnualApplicationComponent],
            providers: [PaginationConfig]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BatchAnnualApplicationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
