import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleGuard, SeoGuard } from '../../@core/guard';
import { AdminComponent } from './admin.component';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [RoleGuard, SeoGuard],
        children: [
            { path: 'home', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'general', loadChildren: './general/general.module#GeneralModule' },
            { path: 'da-config', loadChildren: './da-config/da-config.module#DaConfigModule' },
            { path: 'recording-type', loadChildren: './recording-type/recording-type.module#RecordingTypeModule' },
            { path: 'non-contracting-type', loadChildren: './non-contracting-type/non-contracting-type.module#NonContractingTypeModule' },
            { path: 'equipment-management', loadChildren: './equipment-management/equipment-management.module#EquipmentManagementModule' },
            { path: 'fiscal-category', loadChildren: './fiscal-category/fiscal-category.module#FiscalCategoryModule'},
            { path: 'maintain-service-structures', loadChildren: './maintain-service-structures/maintain-service-structures.module#MaintainServiceStructuresModule'},
            { path: 'batch-annual-application', loadChildren: './batch-annual-application/batch-annual-application.module#BatchAnnualApplicationModule' },
            { path: 'user-security-profile', loadChildren: './user-security-profile/user-security-profile.module#UserSecurityProfileModule' },
            { path: 'form-letter-template', loadChildren: './form-letter-template/form-letter-template.module#FormLetterTemplateModule' },
            { path: 'assessment-builder', loadChildren: './assessment-builder/assessment-builder.module#AssessmentBuilderModule' },
            { path: 'contracting', loadChildren: './contracting/contracting.module#ContractingModule' },
            { path: 'team-position', loadChildren: './team-position/team-position.module#TeamPositionModule' },
        ],
        data: {
            title: ['MDTHINK Admin configurations'],
            desc: 'Maryland department of human services',
            screen: { current: 'admin', modules: [], skip: false }
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
