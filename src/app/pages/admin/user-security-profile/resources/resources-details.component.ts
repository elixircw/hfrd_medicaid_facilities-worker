import { AfterViewChecked, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, CommonHttpService, DataStoreService, GenericService } from '../../../../@core/services';
import { AdminUrlConfig } from '../../admin-url.config';
import { TeamDetails } from '../../team-position/_entities/teamposition.data.model';

@Component({
    selector: 'resources-details',
    templateUrl: './resources-details.component.html',
    styleUrls: ['./resources-details.component.scss']
})
export class ResourcesDetailsComponent implements OnInit, AfterViewChecked {


    loadNumber: string;
    teamId: string;
    @Input() reloadTreeSubject = new Subject();
    @Input() selectedTreeSubject = new Subject();
    @Input() teamDetails$: Observable<any>;
    @Input() selectedResourceId = new Subject<string>();
    @Input() totalCount$ = new Subject<any>();
    @Input() pageChangedRequest$ = new Subject<any>();
    @Input() teamMemeberDetails: TeamDetails = new TeamDetails();

    resourcesModule$: Observable<any>;
    resourcesTypes$: Observable<any>;
    paginationInfo = new PaginationInfo();
    totalRecordCount: Observable<number>;
    tempTeamId: string;
    actionLabel: string;
    teamDetailLabel: string;
    resourseFormGroup: FormGroup;
    showModelBox = false;
    teamDetail: any[];

    teamTypes: any[];
    parentTeam$: Observable<any[]>;
    teamList$: Observable<any[]>;
    positionTeamlist: Array<any> = [];
    teammemberId: string;
    resourceList: any;
    teamDetailData: any;
    openingTimeHours: string;
    closingTimeHours: string;
    moduleField: boolean;
    constructor(
        private formBuilder: FormBuilder,
        private _service: GenericService<any>,
        private _commonService: CommonHttpService,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService,
        private cdRef: ChangeDetectorRef
    ) {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.TeamPosition.TeamDetailsUrl;
    }

    ngOnInit() {
        this.getResourcesTypes();
        this.getModulesList();
        this.showModelBox = false;
        this.selectedResourceId.subscribe((result) => {
            this.tempTeamId = result;
            this.loadNumber = '';
            this.showModelBox = false;
        });
        this.totalCount$.subscribe((item) => {
            this.totalRecordCount = item;
        });
        this._dataStoreService.currentStore.subscribe((item) => {
            if (item['Pagination_Reset']) {
                this.paginationInfo.pageNumber = 1;
            }
        });
        this.resourseFormGroup = this.formBuilder.group(
            {
                parentid: null,
                name: ['', [Validators.required]],
                description: [''],
                modulekey: [''],
                resourceid: ['', [Validators.required]],
                resourcetype: [null, [Validators.required]]
            }
            // { validator: this.checkDateRange }
        );
        // this.clearFormGroup();
        // this.resourseFormGroup.get('resourcetype').valueChanges.subscribe(res => {
        //     setTimeout(() => {
        //         if (res !== '2' || res !== 2) {
        //             this.moduleField = true;
        //         } else {
        //             this.moduleField = false;
        //         }
        //     }, 100);
        // });
        // this.resourseFormGroup.patchValue({
        //     parentid: this.tempTeamId
        // });
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }



    pageChanged(event) {
        this.pageChangedRequest$.next(event);
    }




    getResourcesTypes() {
        this.resourcesTypes$ = this._commonService
            .getArrayList(
                {
                    nolimit: true,
                    method: 'get',
                    where: {
                        referencetypeid: '344',
                        teamtypekey: 'CW'
                    }
                },
                AdminUrlConfig.EndPoint.resources.ResourcesTypeListUrl + '/gettypes?filter'
            )
            .map((result) => {
                return result;
            });
    }






    saveResourse(resourseData) {
        if (!this.resourseFormGroup.dirty && !this.resourseFormGroup.valid) {
            return false;
        }
        this._service.endpointUrl = AdminUrlConfig.EndPoint.resources.Add;
        this._service.create(this.resourseFormGroup.value).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Resourse setup details saved successfully');
                    (<any>$('#myModal-new-resource')).modal('hide');
                    const data1 = {
                        id : this.resourseFormGroup.get('parentid').value,
                        pageNo: 1
                    };
                    // this.pageChangedRequest$.next(data1);
                    this.selectedTreeSubject.next(data1);
                    this.paginationInfo.pageNumber = 1;
                    // this.selectedTreeSubject.next(this.resourseFormGroup.get('parentid').value); // right side data refresh
                    this.reloadTreeSubject.next(); // left side data refresh
                    // this.getListResource();
                    this.clearFormGroup();
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    deleteItem() {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.resources.ResourcesAdd;
        this._service.patch(this.teamDetailData.id, { 'activeflag': 0 }).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Resource deleted successfully');
                    this.reloadTreeSubject.next();
                    // this.getListResource();
                    // this.selectedTreeSubject.next();
                    const data1 = {
                        id : response.parentid,
                        pageNo: 1
                        // itemsPerPage: 10
                    };
                    // this.pageChangedRequest$.next(data1);
                    this.selectedTreeSubject.next(data1);
                    this.paginationInfo.pageNumber = 1;
                    (<any>$('#delete-popup')).modal('hide');
                }
            },
            (error) => {
                (<any>$('#delete-popup')).modal('hide');
                this._alertService.error('Cannot delete a Resource');
            }
        );
    }
    declineDelete() {
        (<any>$('#delete-popup')).modal('hide');
    }
    confirmDelete(requestdataDelete) {
        if (requestdataDelete.id) {
            this.resourseFormGroup.patchValue({
                parentid: requestdataDelete.parentid,
            });
            this.teamDetailData = requestdataDelete;
            this.teamDetailData.id = requestdataDelete.id;
            (<any>$('#delete-popup')).modal('show');
        }
    }

    editTeamItem(requestdata) {
        if (requestdata) {
            this.teamDetailData = requestdata;
            this.resourseFormGroup.patchValue({
                parentid: requestdata.parentid,
                name: requestdata.name,
                description: requestdata.description,
                modulekey: requestdata.modulekey,
                resourceid: requestdata.resourceid,
                resourcetype: String(requestdata.resourcetype)
            });
        }
        this.teamDetailLabel = 'Edit';
    }

    updateResourse() {
        // const updateDate = {...this.teamDetailData , ...this.resourseFormGroup.value };
        this._service.endpointUrl = AdminUrlConfig.EndPoint.resources.ResourcesAdd,
            this._service.patch(this.teamDetailData.id, this.resourseFormGroup.value).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Resource updated successfully');
                        (<any>$('#myModal-new-resource')).modal('hide');
                        // this.selectedTreeSubject.next();
                        // this.reloadTreeSubject.next();
                        const data = {
                            id : response.parentid,
                            pageNo: this.paginationInfo.pageNumber,
                        };
                        // this.pageChangedRequest$.next(data);
                        // selectedTreeSubject
                        this.selectedTreeSubject.next(data); // right side data refresh
                        this.reloadTreeSubject.next();
                        // this.getListResource();
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );

    }



    clearFormGroup() {
        // this.actionLabel = 'Add';
        this.teamDetailLabel = 'Add';
        this.resourseFormGroup.reset();
        // this.openingTimeHours = '';
        // this.closingTimeHours = '';
        this.resourseFormGroup.patchValue({
            // parentid: null,
            name: [''],
            description: [''],
            modulekey: [''],
            resourceid: [''],
            resourcetype: null
            // openingTimeHour: '',
            // closingTimeHour: '',
            // teamtypekey: '',
            // region: '',
            // parentteamid: null
        });
    }




    getModulesList() {
        const moduleslist = this._commonService.getArrayList(
            {
                nolimit: true,
                method: 'get',
                where: {
                    resourcetype: '2'
                }
            },
            AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl + '?filter'
        ).map(response => {
            return response;
        });
        this.resourcesModule$ = moduleslist.pluck('data');
    }

    getListResource() {
        let listData = Observable.of([]);
        let whereData: any;
        let parentidData = this.resourseFormGroup.get('parentid').value;
        if (parentidData) {
            whereData = {
                'activeflag': 1,
                'or': [{ parentid: parentidData },
                { id: parentidData }]
            };
        } else {
            whereData = {
                'or': [
                    {
                        'and': [
                            {
                                'parentid': null
                            },
                            {
                                'resourcetype': {
                                    'gt': 2
                                }
                            }
                        ]
                    },
                    {
                        'and': [
                            {
                                'parentid': null
                            },
                            {
                                'resourcetype': {
                                    'lt': 2
                                }
                            }
                        ]
                    }
                ]
            };
        }
        listData = this._commonService.getArrayList({
            // limit: pageSize,
            nolimit: 'true',
            method: 'get',
            where: whereData
        }, AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl + '?filter').map((item) => {
            return item;
        });

        this.teamDetails$ = listData;
    }
}

