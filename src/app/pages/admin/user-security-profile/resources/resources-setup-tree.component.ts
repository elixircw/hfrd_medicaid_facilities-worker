import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';
import { DataStoreService, GenericService } from '../../../../@core/services';
import { AdminUrlConfig } from '../../admin-url.config';
import { TeamPosition } from '../../team-position/_entities/teamposition.data.model';
import { PaginationInfo } from './../../../../@core/entities/common.entities';
import { CommonHttpService } from './../../../../@core/services/common-http.service';


@Component({
    selector: 'resources-setup-tree',
    templateUrl: './resources-setup-tree.component.html',
    styleUrls: ['./resources-setup-tree.component.scss']
})
export class ResourcesSetupTreeComponent implements OnInit {


    @Input() resourceDetailIdSubject$ = new Subject<Observable<any>>();
    @Input() reloadTreeSubject = new Subject();
    @Input() selectedTreeSubject = new Subject();
    @Input() selectedResourceId = new Subject<string>();
    @Input() totalCount$ = new Subject<any>();
    @Input() pageChangedRequest$ = new Subject<any>();
    teamTreeview$: Observable<any[]>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    private pageSubject$ = new Subject<number>();
    teamDetails: any;
    teamId: '';
    selectedIndex: string;
    parentid: string;


    constructor(private _service: GenericService<TeamPosition>, private _dataStore: DataStoreService, private _commonService: CommonHttpService) {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl;
    }

    ngOnInit() {
        this.reloadTreeSubject.subscribe(value => this.getPage());
        this.pageChangedRequest$.subscribe((item) => {
            if (item.id) {
                this.onSelected(item.id, item.page.page, item.page.itemsPerPage);
            } else {
                this.onSelected(this.teamId, item.page, item.itemsPerPage);
            }
            // this.onSelected(this.teamId, item.page, item.itemsPerPage);
        });
        this.getPage();
        this.onReload(null , 1);
        this.selectedTreeSubject.subscribe((value: any) => this.onReload(value.id, value.pageNo));
    }


    getPage() {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl;
        // this.teamTreeview$ = this._service.getArrayList(
        //     {
        //         nolimit: 'true',
        //         where: {
        //             resourcetype: '2'
        //         },
        //     }).map(response => {
        //         return response;
        //     });
        const source1 = this._commonService.getArrayList(
            {
                nolimit: true,
                method: 'get',
                where: {
                    resourcetype: '2'
                }
            },
            AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl + '?filter'
        ).map(response => {
            return response;
        });

        this.teamTreeview$ = source1.pluck('data');
        // this.teamTreeview$.next(source1.pluck('data') as Observable<any>);
        // .subscribe((result) => {
        //     this.teamDetails = result;
        // });
        // this.onReload(null);
    }

    onSelected(event?: any, pageNo?: number, pageSize?: number, treeSelected?: boolean, index?: number) {
        this.selectedIndex = event;
        this.teamId = event;
        // this._service.endpointUrl = 'manage/team/details';
        let whereData: any;
        if (this.teamId) {
            whereData = {
                parentid: this.teamId,
                id: this.teamId
            };
        } else {
            whereData = {
                'parentid': null
            };
        }
        const source = this._commonService.getArrayList({
            page: pageNo,
            // limit: pageSize,
            limit: 10,
            method: 'get',
            where: whereData,
        }, AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl + '?filter').map((item) => {
            return item;
        });
        this.resourceDetailIdSubject$.next(source.pluck('data') as Observable<any>);
        this._dataStore.setData('Pagination_Reset', true);
        // source.pluck('count')

        source.pluck('count').subscribe((item) => {
            // let item = item1.length;
            if (item) {
                this.totalCount$.next(item);
            } else if (item === 0 && treeSelected) {
                this.totalCount$.next(item);
            }
        });
        this.selectedResourceId.next(this.teamId);
        this._service.endpointUrl = AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl;
    }

    onReload(id , pageNo?) {
        // this._service.endpointUrl = 'manage/team/details';
        this.selectedIndex = id;
        // console.log(this.selectedIndex);
        this.teamId = id;
        let whereData1: any;
        if (this.teamId) {
            whereData1 = {
                parentid: this.teamId,
                id: this.teamId
            };
        } else {
            whereData1 = {
                'parentid': null
            };
        }

        // const source = this._service.getPagedArrayList(new PaginationRequest({
        //     page: 1,
        //     limit: 10,
        //     method: 'get',
        //     where: whereData1,
        // }), AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl + '?filter').share();
        // this.resourceDetailIdSubject$.next(source as Observable<any>);
        // this._service.endpointUrl = AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl;
        const source = this._commonService.getArrayList({
            page: pageNo ? pageNo : 1,
            limit: 10,
            // nolimit: 'true',
            method: 'get',
            where: whereData1,
        }, AdminUrlConfig.EndPoint.resources.ResourcesTreeListUrl + '?filter').map((item) => {
            return item;
        });
        this.resourceDetailIdSubject$.next(source.pluck('data') as Observable<any>);
        source.pluck('count').subscribe((item) => {
            // let item = item1.length;
            if (item) {
                this.totalCount$.next(item);
            } else if (item === 0) {
                this.totalCount$.next(item);
            }
        });
    }

}
