import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserSecurityProfileComponent } from './user-security-profile.component';

const routes: Routes = [
    {
        path: '',
        component: UserSecurityProfileComponent,
        children: [
            { path: 'permission-group', loadChildren: './permission-group/permission-group.module#PermissionGroupModule' },
            { path: 'resources', loadChildren: './resources/resources.module#ResourcesModule' },
            { path: 'manage-user-security', loadChildren: './manage-user-security/manage-user-security.module#ManageUserSecurityModule' },
            { path: 'user-role', loadChildren: './user-role/user-role.module#UserRoleModule' },
            { path: '', redirectTo: 'manage-user-security', pathMatch: 'full' }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserSecurityProfileRoutingModule {}
