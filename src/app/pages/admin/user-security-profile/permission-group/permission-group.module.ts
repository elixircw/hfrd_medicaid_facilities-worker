import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { PermissionGroupRoutingModule } from './permission-group-routing.module';
import { PermissionGroupComponent } from './permission-group.component';

// import { MenuModuleFormControlModule } from '../menu-module-form-control/menu-module-form-control.module';
@NgModule({
    imports: [CommonModule, ReactiveFormsModule, PermissionGroupRoutingModule, FormsModule, SharedPipesModule],
    declarations: [PermissionGroupComponent]
})
export class PermissionGroupModule {}
