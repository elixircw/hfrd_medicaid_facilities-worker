import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { GenericService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { AdminUrlConfig } from '../../admin-url.config';
import {
    MenuList,
    PermissionGroup,
    ResourceListTree,
    SaveResource,
} from '../_entites/user-security-profile.data.modal';
import { Observable } from 'rxjs';
import { ResourcePermission } from '../../../provider-applicant/new-public-applicant/_entities/newApplicantModel';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'permission-group',
    templateUrl: './permission-group.component.html',
    styleUrls: ['./permission-group.component.scss'],
    changeDetection: ChangeDetectionStrategy.Default
})
export class PermissionGroupComponent implements OnInit {
    permissionGroupForm: FormGroup;
    addPermission: boolean;
    permissionGroupId: string;
    editPermission: PermissionGroup;
    permissionGroup: PermissionGroup[];
    menuList$: Observable<MenuList[]>;
    moduleList$: Observable<MenuList[]>;
    screenList$: Observable<MenuList[]>;
    actionList$: Observable<MenuList[]>;
    isMenu = false;
    isModule = false;
    isScreen = false;
    isAction = false;
    menufilterText: FormControl;
    modulefilterText: FormControl;
    screenfilterText: FormControl;
    actionfilterText: FormControl;
    selectedControls: any[] = [];
    subMenuList$: Observable<any[]>;
    constructor(
        private formBuilder: FormBuilder,
        private _service: GenericService<PermissionGroup>,
        private _resourceListTree: GenericService<ResourcePermission>,
        private _saveResource: GenericService<SaveResource>,
        private _formBuilder: FormBuilder,
        private _alertService: AlertService
    ) { }

    ngOnInit() {
        this.menufilterText = new FormControl('');
        this.modulefilterText = new FormControl('');
        this.screenfilterText = new FormControl('');
        this.actionfilterText = new FormControl('');
        this.formInitilize();
        this.getPermissionGroup();
        this.menufilterText.valueChanges.debounceTime(400)
        .distinctUntilChanged()
        .subscribe(res => {
            this.getResourceList(res);
        }); 
        this.modulefilterText.valueChanges.debounceTime(400)
        .distinctUntilChanged()
        .subscribe(res => {
            this.getResourceList(res);
        }); 
        this.screenfilterText.valueChanges.debounceTime(400)
        .distinctUntilChanged()
        .subscribe(res => {
            this.getResourceList(res);
        }); 
        this.actionfilterText.valueChanges.debounceTime(400)
        .distinctUntilChanged()
        .subscribe(res => {
            this.getResourceList(res);
        }); 
    }
    formInitilize() {
        this.permissionGroupForm = this.formBuilder.group({
            groupName: ['', Validators.required]
        });
    }
    private getPermissionGroup() {
        this.addPermission = true;
        this._service.endpointUrl = AdminUrlConfig.EndPoint.UserProfile.PermissionGroupUrl + '?filter';
        this._service.getArrayList({ method: 'get', nolimit: true }).subscribe((result) => {
            this.permissionGroup = result;
            if (this.permissionGroup && this.permissionGroup.length) {
                this.permissionGroupId = this.permissionGroup[0].permissiongroupid;
                this.permissionGroup[0].isActive = true;
                this.addPermission = true;
                this.getPermissionDetail(this.permissionGroup[0], 0);
            } else {
                this.addPermission = false;
            }
        });
    }
    getResourceList(search) {
        const source = forkJoin(
            this._resourceListTree.getArrayList(
                {
                    method: 'get',
                    where: { permissiongroupid: this.editPermission.permissiongroupid, resourcetype: [1, 5] },
                    nolimit: true
                },
                AdminUrlConfig.EndPoint.UserProfile.ResourceFlatList + '?arg'
            ),
            this._resourceListTree.getArrayList(
                {
                    method: 'get',
                    where: { permissiongroupid: this.editPermission.permissiongroupid, resourcetype: [2] },
                    nolimit: true
                },
                AdminUrlConfig.EndPoint.UserProfile.ResourceFlatList + '?arg'
            ),
            this._resourceListTree.getArrayList(
                {
                    method: 'get',
                    where: { permissiongroupid: this.editPermission.permissiongroupid, resourcetype: [3] },
                    nolimit: true
                },
                AdminUrlConfig.EndPoint.UserProfile.ResourceFlatList + '?arg'
            ),
            this._resourceListTree.getArrayList(
                {
                    method: 'get',
                    where: { permissiongroupid: this.editPermission.permissiongroupid, resourcetype: [4] },
                    nolimit: true
                },
                AdminUrlConfig.EndPoint.UserProfile.ResourceFlatList + '?arg'
            )
        ).map(result => {
            result[0] = search ? result[0].filter(res => (res.name).toLowerCase().indexOf(search.toLowerCase()) !== -1) : result[0];
            result[1] = search ? result[1].filter(res => (res.name).toLowerCase().indexOf(search.toLowerCase()) !== -1) : result[1];
            result[2] = search ? result[2].filter(res => (res.name).toLowerCase().indexOf(search.toLowerCase()) !== -1) : result[2];
            result[3] = search ? result[3].filter(res => (res.name).toLowerCase().indexOf(search.toLowerCase()) !== -1) : result[3];
            return {
                menuList: result[0],
                moduleList: result[1],
                screenList: result[2],
                actionList: result[3],
                subMenuList: result[0].filter((item) => item.resourcetype === 5).length
            };
        }).share();
        this.menuList$ = source.pluck('menuList');
        this.moduleList$ = source.pluck('moduleList');
        this.screenList$ = source.pluck('screenList');
        this.actionList$ = source.pluck('actionList');
        this.subMenuList$ = source.pluck('subMenuList');
    }

    selectAllControls(event, type, prop) {
        if(type === 'menu') {
            this.menuList$.subscribe(res => {
                if (prop === 'allow') {
                    const selAll = [];
                    this.selectedControls = [];
                    res.map(item => {
                        if (item.resourcetype == '1') {
                            item.resourceid = item.id;
                            item.isallowed = event.target.checked;
                            selAll.push(item);
                        }
                    });
                    this.selectedControls.push(...selAll);
                    $('.menuAllowAll').prop('checked', event.target.checked);
                } else if (prop === 'enable') {
                    const selAll = [];
                    this.selectedControls = [];
                    res.map(item => {
                        if (item.resourcetype == '5') {
                            item.resourceid = item.id;
                            item.isallowed = event.target.checked;
                            selAll.push(item);
                        }
                    });
                    this.selectedControls.push(...selAll);
                    $('.subMenuAllowAll').prop('checked', event.target.checked);
                }
            });
        } else if(type === 'module') {
            this.moduleList$.subscribe(res => {
                if (prop === 'allow') {
                    res.map(item => {
                        item.resourceid = item.id;
                        item.isallowed = event.target.checked;
                    });
                    this.selectedControls = [];
                    const selAll = [].concat(...res);
                    this.selectedControls.push(...selAll);
                }
            });
            $(".moduleAllowAll").prop('checked', event.target.checked);
        } else if(type === 'screen') {
            this.screenList$.subscribe(res => {
                if (prop === 'allow') {
                    res.map(item => {
                        item.resourceid = item.id;
                        item.isallowed = event.target.checked;
                    });
                    this.selectedControls = [];
                    const selAll = [].concat(...res);
                    this.selectedControls.push(...selAll);
                    $(".screenAllowAll").prop('checked', event.target.checked);
                } else if (prop === 'enable') {
                    res.map(item => {
                        item.resourceid = item.id;
                        item.isenabled = event.target.checked;
                    })
                    this.selectedControls = [];
                    const selAll = [].concat(...res);
                    this.selectedControls.push(...selAll);
                    $(".screenEnableAll").prop('checked', event.target.checked);
                }
            })
        } else if(type === 'action') {
            this.actionList$.subscribe(res => {
                if (prop === 'allow') {
                    res.map(item => {
                        item.resourceid = item.id;
                        item.isallowed = event.target.checked;
                    })
                    this.selectedControls = [];
                    const selAll = [].concat(...res);
                    this.selectedControls.push(...selAll);
                    $(".actionAllowAll").prop('checked', event.target.checked);
                } else if (prop === 'visible') {
                    res.map(item => {
                        item.resourceid = item.id;
                        item.isvisible = event.target.checked;
                    })
                    this.selectedControls = [];
                    const selAll = [].concat(...res);
                    this.selectedControls.push(...selAll);
                    $(".actionVisibleAll").prop('checked', event.target.checked);
                } else if (prop === 'enable') {
                    res.map(item => {
                        item.resourceid = item.id;
                        item.isenabled = event.target.checked;
                    })
                    this.selectedControls = [];
                    const selAll = [].concat(...res);
                    this.selectedControls.push(...selAll);
                    $(".actionEnableAll").prop('checked', event.target.checked);
                }
            })
        }
    }

    addGroup() {
        this.permissionGroup.map((item) => {
            item.isActive = false;
        });
        const resource = {
            permissiongroupname: this.permissionGroupForm.value.groupName,
            description: ''
        };
        this.addUpdatePermissionGroup(resource, 'Add Group');
    }

    getPermissionDetail(modal: PermissionGroup, index) {
        this.permissionGroupId = modal.permissiongroupid;
        this.editPermission = modal;
        this.addPermission = true;
        this.isMenu = true;
        this.isModule = false;
        this.isScreen = false;
        this.isAction = false;
        $('#searchbtn').prop('checked', false);
        $('#searchbtnmodule').prop('checked', false);
        $('#searchbtnscreen').prop('checked', false);
        $('#searchbtnvisible').prop('checked', false);
        $('#searchbtnenabled').prop('checked', false);
        this.getResourceList('');
        if (index !== '') {
            this.permissionGroup.map((item) => {
                item.isActive = false;
            });
            this.permissionGroup[index].isActive = true;
        }
    }
    deletePermission(modal: PermissionGroup) {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.UserProfile.DeletePermissionGroupUrl;
        this._service.remove(modal.permissiongroupid).subscribe(
            (result) => {
                this._alertService.success('Deleted Group successfully');
                this.getPermissionGroup();
            },
            (err) => {
                console.log(err);
            }
        );
    }
    showAddPermission() {
        this.addPermission = true;
    }
    saveGroup() {
        const validation = this.conditionValidation();
        const resource = {
            permissiongroupname: this.editPermission.permissiongroupname,
            description: this.editPermission.description,
            resource: this.selectedControls,
            permissiongroupid: this.editPermission.permissiongroupid
        };
        if (validation) {
            this.addUpdatePermissionGroup(resource, 'Save Group');
        }
    }
    conditionValidation(): boolean {
        if (this.selectedControls.length === 0) {
            this._alertService.warn('Please select atlease one resource');
            return false;
        } else if (!this.editPermission.permissiongroupid) {
            this._alertService.warn('Please select permission group');
            return false;
        } else if (this.permissionGroupForm.value.groupName) {
            this._alertService.warn('Please enter group Name');
            return false;
        }
        return true;
    }
    addUpdatePermissionGroup(resource, type) {
        this._saveResource.create(resource, AdminUrlConfig.EndPoint.UserProfile.SaveGroupUrl).subscribe(
            (result) => {
                this._alertService.success(type + ' successfully');  
                this.permissionGroupForm.reset();
                // this.getPermissionGroup();
                this.selectedControls = [];
                this.getResourceList('');

            },
            (err) => {
                console.log(err);
            }
        );
    }
    onControlChange(resource, event, prop, resourceid?, type?) {
        const selObj = resource;
        selObj.resourceid = resource.id;
        if(this.selectedControls.length) {
            const checkObj = this.selectedControls.filter(itm => itm.resourceid === selObj.id);
                if(checkObj && checkObj.length) {
                    if (resource.pgresourceid) {
                        checkObj[0].pgresourceid = resource.pgresourceid;
                    }
                    if (prop === 'enable' && (resourceid === 5 || resourceid === 1)) {
                        checkObj[0].isallowed = (!event.target.checked && resourceid === 1) ? null : !event.target.checked;
                        checkObj[0].isvisible = !event.target.checked;
                        checkObj[0].isenabled = !event.target.checked;
                    } else if (prop === 'allow' && type === 'screen') {
                        checkObj[0].isallowed = event.target.checked;
                        checkObj[0].isvisible = event.target.checked;
                    } else if (prop === 'allow' && type === 'module') {
                        checkObj[0].isallowed = event.target.checked;
                        checkObj[0].isenabled = event.target.checked && !checkObj[0].isenabled ? true : false;
                    } else if (prop === 'allow') {
                        checkObj[0].isallowed = event.target.checked;
                    } else if (prop === 'visible') {
                        checkObj[0].isvisible = event.target.checked
                    } else if (prop === 'enable') {
                        checkObj[0].isenabled = event.target.checked
                    }
                } else {
                    if (prop === 'enable' && (resourceid === 5 || resourceid === 1)) {
                        selObj.isallowed = (!event.target.checked && resourceid === 1) ? null : !event.target.checked;
                        selObj.isvisible = !event.target.checked;
                        selObj.isenabled = !event.target.checked;
                    } else if (prop === 'allow' && type === 'screen') {
                        selObj.isallowed = event.target.checked;
                        selObj.isvisible = event.target.checked;
                    } else if (prop === 'allow' && type === 'module') {
                        selObj.isallowed = event.target.checked;
                        selObj.isenabled = event.target.checked && !selObj.isenabled ? true : false;
                    } else if (prop === 'allow') {
                        selObj.isallowed = event.target.checked;
                    } else if (prop === 'visible') {
                        selObj.isvisible = event.target.checked;
                    } else if (prop === 'enable') {
                        selObj.isenabled = event.target.checked;
                    }
                    this.selectedControls.push(selObj);
                }
        } else {
            if (prop === 'enable' && (resourceid === 5 || resourceid === 1)) {
                selObj.isallowed = (!event.target.checked && resourceid === 1) ? null : !event.target.checked;
                selObj.isvisible = !event.target.checked;
                selObj.isenabled = !event.target.checked;
            } else if (prop === 'allow' && type === 'screen') {
                selObj.isallowed = event.target.checked;
                selObj.isvisible = event.target.checked;
            } else if (prop === 'allow' && type === 'module') {
                selObj.isallowed = event.target.checked;
                selObj.isvisible = event.target.checked;
            } else if (prop === 'allow') {
                selObj.isallowed = event.target.checked;
            } else if(prop === 'visible') {
                selObj.isvisible = event.target.checked;
            } else if(prop === 'enable') {
                selObj.isenabled = event.target.checked;
            }
            this.selectedControls.push(selObj);
        }
    }
}
