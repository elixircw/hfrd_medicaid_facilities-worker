import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { AdminUrlConfig } from '../../admin-url.config';
import { Resource, ResourcePermission } from '../_entites/user-security-profile.data.modal';
import { Observable } from 'rxjs/Observable';
import { GenericService } from '../../../../@core/services';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'menu-module-form-control',
    templateUrl: './menu-module-form-control.component.html',
    styleUrls: ['./menu-module-form-control.component.scss']
})
export class MenuModuleFormControlComponent implements OnInit {
    roleId: number;
    selectedResources: Resource[] = [];
    actionPermission$: Observable<ResourcePermission[]>;
    controlPermission$: Observable<ResourcePermission[]>;
    modulePermission$: Observable<ResourcePermission[]>;
    menuPermission$: Observable<ResourcePermission[]>;
    selectedResourses$: Observable<Resource[]>;
    @Input() permissionGroupId: string;
    @Input() roleId$ = new Subject<number>();
    @Input() resourseListSubject$: Subject<Resource[]>;
    @Input() resourseSubject$: Subject<Resource>;
    constructor(private _resourceService: GenericService<ResourcePermission>) {}

    ngOnInit() {
        if (this.resourseListSubject$) {
            this.resourseListSubject$.subscribe((items) => {
                this.selectedResources = items;
            });
        }
    }

    resource(parent, type) {
        this.menuPermission$ = Observable.empty();
        if (type === 'menu') {
            if (this.permissionGroupId) {
                this.menuPermission$ = this._resourceService.getArrayList(
                    {
                        method: 'get',
                        where: {
                            resourcetype: [1],
                            permissiongroupid: this.permissionGroupId ? this.permissionGroupId : ''
                        }
                    },
                    AdminUrlConfig.EndPoint.UserProfile.ResourceListUrl + '?filter'
                );
            } else if (this.roleId) {
                this.menuPermission$ = this._resourceService.getArrayList(
                    {
                        method: 'get',
                        where: {
                            resourcetype: [1],
                            roleid: this.roleId
                        }
                    },
                    AdminUrlConfig.EndPoint.UserProfile.ResourceListUrl + '?filter'
                );
            }
        }
        if (type === 'module') {
            this.modulePermission$ = Observable.empty();
            this.controlPermission$ = Observable.empty();
            this.actionPermission$ = Observable.empty();
            this.modulePermission$ = this._resourceService.getArrayList(
                {
                    method: 'get',
                    where: {
                        resourcetype: [2],
                        parentid: parent.id,
                        permissiongroupid: this.permissionGroupId
                    }
                },
                AdminUrlConfig.EndPoint.UserProfile.ResourceListUrl + '?filter'
            );
        } else if (type === 'control') {
            this.controlPermission$ = Observable.empty();
            this.actionPermission$ = Observable.empty();
            this.controlPermission$ = this._resourceService.getArrayList(
                {
                    method: 'get',
                    where: {
                        resourcetype: [3],
                        parentid: parent.id,
                        permissiongroupid: this.permissionGroupId
                    }
                },
                AdminUrlConfig.EndPoint.UserProfile.ResourceListUrl + '?filter'
            );
        } else if (type === 'action') {
            this.actionPermission$ = Observable.empty();
            this.actionPermission$ = this._resourceService.getArrayList(
                {
                    method: 'get',
                    where: {
                        resourcetype: [4],
                        parentid: parent.id,
                        permissiongroupid: this.permissionGroupId
                    }
                },
                AdminUrlConfig.EndPoint.UserProfile.ResourceListUrl + '?filter'
            );
        }
    }

    selectedResource(selectedItem, event, type) {
        const index = this.selectedResources.indexOf(selectedItem);
        const isChecked = event.target.checked;
        switch (type) {
            case 'isenabled':
                selectedItem.isenabled = isChecked;
                break;
            case 'isvisible':
                selectedItem.isvisible = isChecked;
                break;
            case 'isallowed':
                selectedItem.isallowed = isChecked;
                break;
        }
        this.selectedResources[index] = selectedItem;
        this.resourseSubject$.next(selectedItem);
    }
}
