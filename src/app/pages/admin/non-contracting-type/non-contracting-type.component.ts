import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { DynamicObject, PaginationInfo } from '../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES, REGEX } from '../../../@core/entities/constants';
import { AlertService, GenericService } from '../../../@core/services';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { AdminUrlConfig } from '../admin-url.config';
import { NonContractingType } from './_entities/non-contract-type.data.models';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'non-contracting-type',
    templateUrl: './non-contracting-type.component.html',
    styleUrls: ['./non-contracting-type.component.scss']
})
export class NonContractingTypeComponent implements OnInit {
    addEditLabel: String;
    isEditMode = false;
    disableOnEdit = false;
    formGroup: FormGroup;
    nonContractionType$: Observable<NonContractingType[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    nonContracting: NonContractingType = new NonContractingType();
    paginationInfo: PaginationInfo = new PaginationInfo();
    providernonagreementtypekeyControler: AbstractControl;
    private dynamicObject: DynamicObject = {};
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();
    constructor(private formBuilder: FormBuilder, private _service: GenericService<NonContractingType>, private _alertService: AlertService) {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.NonContractingType.ProviderNonAgreementTypeUrl;
        this.formGroup = this.formBuilder.group({
            typedescription: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            providernonagreementtypekey: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]]
        });
    }
    ngOnInit() {
        this.paginationInfo.sortBy = 'providernonagreementtypekey asc';
        this.getPage();
        this.providernonagreementtypekeyControler = this.formGroup.get('providernonagreementtypekey');
    }
    getPage() {
        const pageSource = this.pageStream$.map((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObject, page: pageNumber };
        });

        const searchSource = this.searchTermStream$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObject = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .mergeMap((params: { search: DynamicObject; page: number }) => {
                return this._service
                    .getPagedArrayList(
                        {
                            limit: this.paginationInfo.pageSize,
                            order: this.paginationInfo.sortBy,
                            page: params.page,
                            count: this.paginationInfo.total,
                            method: 'get',
                            where: params.search
                        },
                        AdminUrlConfig.EndPoint.NonContractingType.ProviderNonAgreementTypeUrl + '/list?filter'
                    )
                    .map((result) => {
                        return { data: result.data, count: result.count, canDisplayPager: result.count > this.paginationInfo.pageSize };
                    });
            })
            .share();

        this.nonContractionType$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }
    saveItem() {
        this.nonContracting.typedescription = this.formGroup.value.typedescription;
        if (this.nonContracting.providernonagreementtypekey && this.isEditMode) {
            this.nonContracting.updatedby = 'Default';
            this._service.update(this.nonContracting.providernonagreementtypekey, this.nonContracting).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Non contracting type saved successfully');
                        this.pageStream$.next(this.paginationInfo.pageNumber);
                        (<any>$('#myModal-non-contracting-type-edit-add')).modal('hide');
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this.nonContracting.providernonagreementtypekey = this.formGroup.value.providernonagreementtypekey;
            this.nonContracting.activeflag = 1;

            this.nonContracting.effectivedate = new Date();
            this.nonContracting.expirationdate = new Date();
            this._service.create(this.nonContracting).subscribe(
                (response) => {
                    if (response.providernonagreementtypekey) {
                        this._alertService.success('Non contracting type saved successfully');
                        this.paginationInfo.sortBy = 'insertedon desc';
                        this.pageStream$.next(1);
                        this.formGroup.patchValue({
                            nameSearch: this.nonContracting.name
                        });
                        this.isEditMode = false;
                        (<any>$('#myModal-non-contracting-type-edit-add')).modal('hide');
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    updateActiveState(nonContract) {
        this._service.update(nonContract.providernonagreementtypekey, nonContract).subscribe(
            (response) => {
                if (response) {
                    this.pageStream$.next(this.paginationInfo.pageNumber);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    updateAddEditLabel(addEdit) {
        this.addEditLabel = addEdit;
        if (addEdit === 'Add') {
            this.cancelItem();
            this.nonContracting = Object.assign({}, new NonContractingType());
        }
    }
    editItem(nonContracting: NonContractingType) {
        this.providernonagreementtypekeyControler.disable();
        this.isEditMode = true;
        this.nonContracting = Object.assign({}, nonContracting);
        this.formGroup.setValue({
            providernonagreementtypekey: this.nonContracting.providernonagreementtypekey,
            typedescription: this.nonContracting.typedescription
        });
    }
    cancelItem() {
        this.disableOnEdit = false;
        this.formGroup.reset();
        this.providernonagreementtypekeyControler.enable();
        (<any>$('#myModal-non-contracting-type-edit-add')).modal('hide');
    }
    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortColumn + ' ' + $event.sortDirection;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    onSearch(field: string, value: string) {
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }
}
