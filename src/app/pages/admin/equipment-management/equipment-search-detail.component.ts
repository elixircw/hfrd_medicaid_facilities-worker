import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { EquipmentManagement, Equipment } from './_entities/equipment-management.models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'equipment-search-detail',
  templateUrl: './equipment-search-detail.component.html',
  styleUrls: ['./equipment-search-detail.component.scss']
})
export class EquipmentSearchDetailComponent implements OnInit {
  @Input() equipmentSearchDetails$: Observable<Equipment[]>;
  @Input() totalRecords$: Observable<number>;
  @Input() canDisplayPager$: Observable<boolean>;
  @Input() equipmentSearchPageSubject$ = new Subject<number>();
  @Input() equipmentSearchTriggeredSubject$: Subject<string>;
  currentPageNumber = 1;
  selectedDetailIdSubject$ = new Subject<string>();
  selectedEquipment= String;
  constructor() {
  }
  ngOnInit() {
  }

  pageChanged(pageInfo: any) {
    this.currentPageNumber = pageInfo.page;
    this.equipmentSearchPageSubject$.next(this.currentPageNumber);
  }

  equipmentSearchDetailItem(model) {
    this.selectedEquipment = model.equipmentid;
    this.selectedDetailIdSubject$.next(model.teammemberequipmentid);
  }
}
