import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { TreeModule } from 'angular-tree-component';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';

import { CoreModule } from '../../../@core/core.module';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { AddEditEquipmentSearchDetailComponent } from './add-edit-equipment-search-detail.component';
import { EquipmentManagementComponent } from './equipment-management.component';
import { EquipmentSearchDetailComponent } from './equipment-search-detail.component';
import { EquipmentSearchComponent } from './equipment-search.component';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';

describe('EquipmentSearchComponent', () => {
    let component: EquipmentSearchComponent;
    let fixture: ComponentFixture<EquipmentSearchComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                TreeModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                A2Edatetimepicker,
                SharedPipesModule
            ],
            declarations: [EquipmentSearchComponent, EquipmentManagementComponent, EquipmentSearchComponent, EquipmentSearchDetailComponent, AddEditEquipmentSearchDetailComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EquipmentSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
