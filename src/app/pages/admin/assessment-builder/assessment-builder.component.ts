import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { environment } from '../../../../environments/environment';
import { AppUser } from '../../../@core/entities/authDataModel';
import { DropdownModel, DynamicObject, PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, GenericService } from '../../../@core/services';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { AdminUrlConfig } from '../admin-url.config';
import { AssessmentBuilder } from './entities/assessment-builder.model';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'assessment-builder',
    templateUrl: './assessment-builder.component.html',
    styleUrls: ['./assessment-builder.component.scss']
})
export class AssessmentBuilderComponent implements OnInit {
    activeFlag = '1';
    targetList$: Observable<DropdownModel[]>;
    categoryList$: Observable<DropdownModel[]>;
    subcategoryList$: Observable<DropdownModel[]>;
    assessmentbuilderForm: FormGroup;
    paginationInfo: PaginationInfo = new PaginationInfo();
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    assessmentBuilder$: Observable<AssessmentBuilder[]>;
    assessmentBuilder: AssessmentBuilder = new AssessmentBuilder();
    private formBuilderUrl: string;
    private token: AppUser;
    safeUrl: SafeResourceUrl;
    dynamicObject: DynamicObject = {
        categoryid: null,
        subcategoryid: null,
        targetid: null,
        isactive: '1'
    };
    private searchTermStream$ = new Subject<DynamicObject>();
    pageStream$ = new Subject<number>();
    constructor(
        private _alertService: AlertService,
        private formBulider: FormBuilder,
        private _service: GenericService<AssessmentBuilder>,
        private _authService: AuthService,
        private _dropDownService: CommonHttpService,
        public sanitizer: DomSanitizer
    ) {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.AssessmentBuilder.AssessmentTemplatelistdataUrl;
    }
    ngOnInit() {
        this.formInitilize();
        this.token = this._authService.getCurrentUser();
        this.paginationInfo.sortBy = 'assessmenttemplatetargetid asc';
        this.getPage();
        this.loadCategory();
        this.loadTarget();
    }
    formInitilize() {
        this.assessmentbuilderForm = this.formBulider.group({
            assessmenttemplatecategory: [''],
            assessmenttemplateSubcategory: [''],
            target: [''],
            activeflag: ['']
        });
    }
    getPage() {
        const pageSource = this.pageStream$.map((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObject, page: pageNumber };
        });

        const searchSource = this.searchTermStream$.debounceTime(1000).map((searchTerm) => {
            this.paginationInfo.pageNumber = 1;
            this.dynamicObject = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .mergeMap((params: { search: DynamicObject; page: number }) => {
                return this._service.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.paginationInfo.pageSize,
                        page: params.page,
                        where: params.search,
                        count: this.paginationInfo.total,
                        method: 'get',
                        order: this.paginationInfo.sortBy
                    }),
                    AdminUrlConfig.EndPoint.AssessmentBuilder.AssessmentTemplatelistdataUrl + '?filter'
                )
                    .map((result) => {
                        return {
                            data: result.data,
                            count: result.count,
                            canDisplayPager: result.count > this.paginationInfo.pageSize
                        };
                    });
            })
            .share();
        this.assessmentBuilder$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }
    addTemplate() {
        this.formBuilderUrl = environment.formBuilderHost + `/#/create/form?t=` + this.token.id;
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.formBuilderUrl);
        console.log(this.formBuilderUrl);
    }
    viewTemplate(assessmentBuilder: AssessmentBuilder) {
        this.formBuilderUrl = environment.formBuilderHost + `/#/form/${assessmentBuilder.external_templateid}/?t=` + this.token.id;
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.formBuilderUrl);
        console.log(this.formBuilderUrl);
    }
    onChangeCategory(option) {
        this.assessmentBuilder.assessmenttemplateid = option.value;
        this.subcategoryList$ = this._dropDownService
            .getArrayList(
                {
                    include: 'servicerequestsubtype',
                    nolimit: true,
                    where: {
                        intakeservreqtypeid: this.assessmentBuilder.assessmenttemplateid,
                        activeflag: 1
                    },
                    method: 'get'
                },
                'admin/intakeservicerequesttype' + '?filter'
            )
            .map((data) => {
                return data[0].servicerequestsubtype.map((res) => new DropdownModel({ text: res.description, value: res.servicerequestsubtypeid }));
            });
    }

    onChangeSubCategory(option) {
        this.assessmentBuilder.external_templateid = option.value;
    }
    loadTarget() {
        this.targetList$ = this._dropDownService.getArrayList(
            {
                nolimit: true,
                where: {
                    activeflag: 1
                },
                method: 'get'
            },
            'admin/Assessmenttemplatetarget?filter'
        );
    }
    searchForms() {
        if (this.assessmentbuilderForm.value.activeflag) {
            this.activeFlag = '1';
        } else {
            this.activeFlag = null;
        }
        this.dynamicObject['isactive'] = this.activeFlag ? this.activeFlag : null;
        this.assessmentBuilder.target = this.assessmentbuilderForm.controls['target'].value ? this.assessmentbuilderForm.controls['target'].value: null;
        this.dynamicObject['categoryid'] = this.assessmentBuilder.assessmenttemplateid ? this.assessmentBuilder.assessmenttemplateid : null;
        this.dynamicObject['subcategoryid'] = this.assessmentBuilder.external_templateid ? this.assessmentBuilder.external_templateid : null;
        this.dynamicObject['targetid'] = this.assessmentBuilder.target ? this.assessmentBuilder.target : null;
        this.searchTermStream$.next(this.dynamicObject);
    }

    loadCategory() {
        this.categoryList$ = this._dropDownService.getArrayList(
            {
                nolimit: true,
                where: { activeflag: 1 },
                method: 'get'
            },
            'admin/intakeservicerequesttype' + '?filter'
        );
    }

    editTemplate(assessmentBuilder: AssessmentBuilder) {
        this.formBuilderUrl = environment.formBuilderHost + `/#/form/${assessmentBuilder.external_templateid}/edit?t=` + this.token.id;
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.formBuilderUrl);
        console.log(this.formBuilderUrl);
    }

    deleteTemplate() {
        this._service.endpointUrl = AdminUrlConfig.EndPoint.AssessmentBuilder.AssessmenttemplateUrl;
        this._service.remove(this.assessmentBuilder.assessmenttemplateid).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Assessment deleted successfully');
                    this.assessmentBuilder = new AssessmentBuilder();
                    this.pageStream$.next(this.paginationInfo.pageNumber);
                    (<any>$('#delete-popup')).modal('hide');
                }
            },
            (error) => {
                console.log(error);
                (<any>$('#delete-popup')).modal('hide');
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    declineDelete() {
        (<any>$('#delete-popup')).modal('hide');
    }
    confirmDelete(requestData: AssessmentBuilder) {
        this.assessmentBuilder = requestData;
        (<any>$('#delete-popup')).modal('show');
    }
    refresh() {
        this.assessmentbuilderForm.reset();
        this.assessmentbuilderForm.patchValue({ assessmenttemplatecategory: '', assessmenttemplateSubcategory: '', target: '' });
        this.dynamicObject = { categoryid: null, subcategoryid: null, targetid: null, isactive: '1' };
        this.searchTermStream$.next(this.dynamicObject);
    }

    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortColumn + ' ' + $event.sortDirection;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    onSearch(field: string, value: string) {
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }
}
