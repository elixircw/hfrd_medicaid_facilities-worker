import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiscalCategoryComponent } from './fiscal-category.component';

describe('FiscalCategoryComponent', () => {
  let component: FiscalCategoryComponent;
  let fixture: ComponentFixture<FiscalCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiscalCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
