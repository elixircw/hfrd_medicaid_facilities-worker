import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FiscalCategoryRoutingModule } from './fiscal-category-routing.module';
import { FiscalCategoryComponent } from './fiscal-category.component';
import { MatCheckboxModule, MatDatepickerModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatListModule, MatNativeDateModule, MatSelectModule, MatRadioModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    FiscalCategoryRoutingModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatSelectModule,
    A2Edatetimepicker,
    PaginationModule,
    MatRadioModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [FiscalCategoryComponent]
})
export class FiscalCategoryModule { }
