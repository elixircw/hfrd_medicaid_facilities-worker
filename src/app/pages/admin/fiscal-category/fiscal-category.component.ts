import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, AuthService, GenericService, SessionStorageService } from '../../../@core/services';
import { AdminUrlConfig } from '../admin-url.config';
import { PaginationRequest, PaginationInfo } from '../../../@core/entities/common.entities';
import { DropdownModel } from '../../../@core/entities/common.entities';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'fiscal-category',
  templateUrl: './fiscal-category.component.html',
  styleUrls: ['./fiscal-category.component.scss']
})
export class FiscalCategoryComponent implements OnInit {
  paginationInfo: PaginationInfo = new PaginationInfo();
  getFiscalCodeList: any[] = [];
  fiscalForm: FormGroup;
  editFiscalForm: FormGroup;
  totalcount: number;
  editIndex: number;
  fiscalList: any;
  paymentTypes: any;
  eligibilityCheck: any;
  eligibilityCheck$: Observable<DropdownModel[]>;
  paymentTypes$: Observable<DropdownModel[]>;
  selectedAccount: any;
  disabled: any;
  ancillary: string;
  fiscaldelete: any;

  constructor(private _commonService: CommonHttpService, private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private _alertService: AlertService, private _authService: AuthService) { }

  ngOnInit() {
    this.initAccountForm();
    this.editIndex = -1;
    this.initEditAccountForm();
    this.loadPayment();
    this.loadEligibilityCheck();
    this.getFiscalList();
  }

  initAccountForm() {
    this.fiscalForm = this.formBuilder.group({
      fiscal_category_cd: [''],
      start_dt: [null],
      end_dt: [null],
      fiscal_category_desc: [''],
      eligibility_cd: [''],
      ancillary_maintenance_sw: [''],
      payment_type_cd: '',
      totalcount: [null]
    });
  }

  initEditAccountForm() {
    this.editFiscalForm = this.formBuilder.group({
      fiscal_category_id: [''],
      fiscal_category_cd: [''],
      start_dt: [''],
      end_dt: [''],
      fiscal_category_desc: [''],
      eligibility_cd: [''],
      ancillary_maintenance_sw: [''],
      payment_type_cd: '',
      totalcount: [null]
    });
    this.editFiscalForm.get('fiscal_category_cd').disable();
  }

  getFiscalList() {
    const source = this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: this.paginationInfo.pageNumber,
          limit: this.paginationInfo.pageSize,
          where: {
            'fiscal_category_id': null,
            sortcolumn: 'fiscal_category_desc',
            sortorder: 'ase'
          },
            method: 'get'
        }),
        AdminUrlConfig.EndPoint.finance.fiscalCodeListUrl + '?filter'
        ).subscribe((res: any) => {
            this.getFiscalCodeList = res.data;
            this.totalcount = res.count;
      });
  }

  pageChanged(pageNumber: number) {
    this.paginationInfo.pageNumber = pageNumber;
    this.getFiscalList();
  }

  loadPayment() {
    this.paymentTypes$ = this._commonService.getArrayList({
      where: {picklist_type_id : '1'},
      nolimit: true,
      method: 'get'
    }, AdminUrlConfig.EndPoint.finance.AddFiscalPickList + '?filter'
    ) .map((result) => {
        // console.log('result...', result);
      return result.map(
          (res) =>
              new DropdownModel({
                  text: res.value_tx,
                  value: res.picklist_value_cd
              })
      );
    });
  }

  loadEligibilityCheck() {
    this.eligibilityCheck$ = this._commonService.getArrayList({
      where: {picklist_type_id : '446'},
      nolimit: true,
      method: 'get'
    }, AdminUrlConfig.EndPoint.finance.AddFiscalPickList + '?filter'
    ) .map((result) => {
        // console.log('result...', result);
      return result.map(
          (res) =>
              new DropdownModel({
                  text: res.value_tx,
                  value: res.description_tx
              })
      );
    });
  }

  addFiscal() {
    if (this.fiscalForm.valid) {
      const data = this.fiscalForm.getRawValue();
      if (this.fiscalForm.get('eligibility_cd').value === 'Yes') {
        this.eligibilityCheck = '3951';
      } else if (this.fiscalForm.get('eligibility_cd').value === 'No') {
        this.eligibilityCheck = '3952';
      } else if (this.fiscalForm.get('eligibility_cd').value === 'N/A') {
        this.eligibilityCheck = '3950';
      }
      console.log('Date Valid..', data.start_dt);
      this._commonService.create({
      fiscal_category_cd: data.fiscal_category_cd,
      start_dt: data.start_dt,
      end_dt: data.end_dt,
      fiscal_category_desc: data.fiscal_category_desc,
      eligibility_cd: this.eligibilityCheck,
      ancillary_maintenance_sw: data.ancillary_maintenance_sw,
      payment_type_cd: data.payment_type_cd
      },
      AdminUrlConfig.EndPoint.finance.AddFiscalCode).subscribe(result => {
        if (!result.insertstatus) {
          console.log('Result Test...', data.start_dt);
          if (result.value === 'false') {
            // console.log('Check the Value', result.value);
            this._alertService.warn('Fiscal Category Code Already exists');
          } else {
              this._alertService.success('Fiscal Category Code Added');
              (<any>$('#add-fiscal')).modal('hide');
              this.fiscalForm.reset();
              this.getFiscalList();
          }
        }
      });
  } else {
    this._alertService.warn('Please fill the mandatory details.');
  }
  }

  updateFiscal() {
    if (this.editFiscalForm.valid) {
      const data = this.editFiscalForm.getRawValue();
      if (this.editFiscalForm.get('eligibility_cd').value === 'Yes') {
        this.eligibilityCheck = '3951';
      } else if (this.editFiscalForm.get('eligibility_cd').value === 'No'){
        this.eligibilityCheck = '3952';
      } else if (this.editFiscalForm.get('eligibility_cd').value === 'N/A') {
        this.eligibilityCheck = '3950';
      }
      this._commonService.create({
        fiscal_category_id: data.fiscal_category_id,
        fiscal_category_cd: data.fiscal_category_cd,
        start_dt: data.start_dt,
        end_dt: data.end_dt,
        fiscal_category_desc: data.fiscal_category_desc,
        eligibility_cd: this.eligibilityCheck,
        ancillary_maintenance_sw: data.ancillary_maintenance_sw,
        payment_type_cd: data.payment_type_cd
      },
      AdminUrlConfig.EndPoint.finance.UpdateFiscalCode).subscribe(result => {
        if (!result.insertstatus) {
          this._alertService.success('Fiscal Category Code Updated Successfully');
          (<any>$('#update-fiscal')).modal('hide');
          this.getFiscalList();
        }
      });
  } else {
    this._alertService.warn('Please fill the mandatory details.');
  }
  }

  editAccount(fiscal) {
    // console.log('edit fiscal...', fiscal);
    this.selectedAccount = fiscal;
    this.editFiscalForm.patchValue({
      fiscal_category_id: fiscal.fiscal_category_id,
      fiscal_category_cd: fiscal.fiscal_category_cd,
      start_dt: fiscal.start_dt,
      end_dt: fiscal.end_dt,
      fiscal_category_desc: fiscal.fiscal_category_desc,
      eligibility_cd: fiscal.eligibilitytype,
      payment_type_cd: fiscal.payment_type_cd,
      ancillary_maintenance_sw: fiscal.ancillary_maintenance_sw
    });
  }

  confirmDelete(fiscal) {
    this.fiscaldelete = fiscal.fiscal_category_id;
    (<any>$('#delete-popup')).modal('show');
  }

  deleteAccount() {
    this._commonService.create({fiscal_category_id: this.fiscaldelete}, AdminUrlConfig.EndPoint.finance.DeleteFiscalCode).subscribe(
      (response) => {
        console.log('delete response', response);
        (<any>$('#delete-popup')).modal('hide');
        this._alertService.success('Fiscal Category Deleted Successfully');
        this.getFiscalList();
      });
  }

  // fiscalUnique(event) {
  //   const source = this._commonHttpService.getArrayList(
  //     {
  //       where: {
  //         'fiscal_category_cd': ''
  //       },
  //         method: 'get'
  //     }, AdminUrlConfig.EndPoint.finance.ValidateFiscalCode + '?filter')
  //     .subscribe((res: any) => {
  //     this._alertService.success('Validation successful');
  //   },
  //   (error) => {
  //     this.fiscalForm.reset();
  //     this._alertService.error('Fiscal Category Code should be Unique');
  //   });
  // }

  viewAccount(fiscal) {
    this.selectedAccount = fiscal;
      if(fiscal.ancillary_maintenance_sw === 'A') {
        this.ancillary = 'Ancillary';
      } else {
        this.ancillary = 'Maintenance';
      }
    // fiscal.ancillary_maintenance_sw === 'A' ? 'Ancillary' : 'Maintenance'
    this.closeModal();
  }
 
  closeModal() {
    (<any>$('#view-fiscal')).modal('hide');
  }

  clearItem() {
    this.fiscalForm.reset();
  }
}
