export class AdminUrlConfig {
    public static EndPoint = {
        General: {
            UserNotificationGroupUrl: 'admin/usernotificationgroup',
            IntakeServiceRequestTypeUrl: 'admin/intakeservicerequesttype',
            DispositionCodeUrl: 'admin/dispositioncode',
            IntakeSerReqstStatusTypeUrl: 'admin/intakeserreqstatustype',
            PersonAddressUrl: 'admin/personaddresstype',
            EmployeeTypetUrl: 'admin/employeetype',
            EthnicGroupTypeUrl: 'admin/ethnicgrouptype',
            GenderTypeUrl: 'admin/gendertype',
            PersonIdentifierTypeUrl: 'admin/personidentifiertype',
            IncomeTypeUrl: 'admin/incometype',
            LanguageTypeUrl: 'admin/languagetype',
            LivingArrangementTypeUrl: 'admin/livingarrangementtype',
            MartialStatusTypeUrl: 'admin/maritalstatustype',
            ActorTypeUrl: 'admin/actortype',
            PersonPhoneTypeUrl: 'admin/Personphonetype',
            RaceTypeUrl: 'admin/racetype',
            CaregiverAddressTypeUrl: 'admin/caregiveraddresstype',
            CaregiverPhoneTypeUrl: 'admin/caregiverphonetype',
            AgencyAddressTypetUrl: 'admin/agencyaddresstype',
            AgencyPhoneNumberTypetUrl: 'admin/agencyphonenumbertype',
            AgencyTypetUrl: 'admin/agencytype',
            GetGroupingListUrl: 'admin/teammember/getgroupinglist',
            GroupNameAddUpdateUrl: 'admin/usernotificationgroupdetail/addupdate',
            ProviderAgreementTypeUrl: 'admin/provideragreementtype',
            ProviderAgreementUrl: 'admin/Provideragreement',
            ProgressNoteUrl: 'admin/progressnote',
            ProgressNoteDetailUrl: 'admin/progressnotedetail/list',
            ServiceRequestSubtypeUrl: 'admin/servicerequestsubtype'
        },
        DAConfig: {
            DaTypeUrl: 'admin/intakeservicerequesttype',
            PlanTypeUrl: 'admin/intakeservicerequestplantype',
            ServiceRequestTypeConfigUrl: 'daconfig/servicerequesttypeconfig',
            DaTypeConfigComponentUrl: 'daconfig/servicerequesttypeconfig/gettypeconfiglist',
            DaConfigSubtypeUrl: 'admin/servicerequestsubtype',
            AmactivityUrl: 'admin/amactivity',
            AllegationUrl: 'admin/allegation',
            AmgoalUrl: 'admin/amgoal',
            AmtaskUrl: 'admin/Amtask',
            ReviewResultTemplateUrl: 'admin/reviewresulttemplate',
            ProviderAgreementDocumentTypesUrl: 'Provideragreementdocumenttypes',
            AllegationConfigListUrl: 'admin/assessmenttemplate',
            MappingListUrl: 'admin/amactivity/mappinglist?filter',
            ConfigurationSettingsUrl: 'admin/configurationsettings',
            AllegationConfigurationUrl: 'admin/configurationsettings/allegationconfiguration',
            ProviderAgreementDoctypeUrl: 'admin/provideragreementdocumenttype',
            ScoreUsageUrl: 'admin/assessmenttemplatescoremapping',
            AssessmentScoreUsageUrl: 'admin/assessmenttemplate',
            ProviderAgreementDocumentRuleConfigUrl: 'admin/Provideragreementdocumentruleconfig',
            GetDocDetailsUrl: 'getdocdetails/list',
            GetStateStatuteSearchUrl: 'admin/Provideragreementdocumentruleconfig/getstatestatutesearch',
            GetAssessmentScoreTypeUrl: 'admin/assessmentscoretype',
            GetAssessmentScoringUrl: 'admin/assessmentscoringmethod',
            GetDispositionCodeUrl: 'daconfig/servicerequesttypeconfigdispositioncode/',
            GetProviderAgreementDocTypeUrl: 'admin/provideragreementdocumenttype',
            GetAgencyRoleTypeUrl: 'admin/agencyroletype',
            GetAgencyCategoryUrl: 'admin/agencycategory',
            GetAlertEventTypeUrl: 'admin/alerteventtype/',
            GetTypeCongigAlertUrl: 'daconfig/servicerequesttypeconfigalert',
            GetItemStableUrl: 'admin/itemstable',
            StatusSearchUrl: 'admin/Provideragreementdocumentruleconfig/getstatestatutesearch',
            AllegationstatestatutsUrl: 'Allegationstatestatutes/addupdate',
            AllegationStatusEditUrl: 'admin/allegation/edit/'
        },
        NonContractingType: {
            ProviderNonAgreementTypeUrl: 'admin/providernonagreementtype'
        },
        AssessmentBuilder: {
            AssessmentTemplatelistdataUrl: 'admin/assessmenttemplate/listdata',
            AssessmentCategoryUrl: 'admin/Assessmenttemplatecategoryfilter/assessmentcategory',
            AssessmentSubCategotyUrl: 'admin/Assessmenttemplatecategoryfilter/assessmentsubcategory?filter',
            AssessmentTemplateCategoryFilter: 'admin/Assessmenttemplatecategoryfilter?filter',
            AssessmenttemplateUrl: 'admin/assessmenttemplate/deletetemplate'
        },
        RecordingType: {
            RecordingTypeUrl: 'admin/progressnotetype',
            DeleteSubTypeRecordingUrl: 'admin/progressnotetype/deletesubtype'
        },
        Contracting: {
            ActionTypeUrl: 'admin/intakeservicerequesttype/listactiontype',
            PaTypeListUrl: 'admin/intakeservicerequesttype/patypelist',
            SearchContractingTypeUrl: 'admin/agency/getprovidersearchlist'
        },
        TeamPosition: {
            TeamTypeListUrl: 'admin/teamtype/',
            ReligionListUrl: 'Counties/regionlist',
            PositionTitleUrl: 'admin/teammemberroletype/list',
            ZipCodeUrl: 'Regions',
            TeamTreeListUrl: 'manage/team/listtree',
            TeamDetailsUrl: 'manage/team/details',
            TeamListUrl: 'manage/team',
            ParentTeamListUrl: 'manage/team/parentteamlist',
            ZipCodeModelUrl: 'admin/teammember/countylist',
            CountyListUrl: 'admin/county',
            ZipcodeFinalUrl: 'admin/countyareateammember/addupdate',
            TeamMemberAddUrl: 'admin/teammember/',
            AssignmentListUrl: 'admin/teammember/getuserassignlist',
            AddReAssignmentURl: 'admin/teammemberassignment/add',
            DeleteReAssignmentURl: 'admin/teammemberassignment',
            GetUserWorkStatusTypeUrl: 'admin/userworkstatustype',
            GetRegionListUrl: 'admin/county/regionlist'
        },
        EquipmentMgmt: {
            EquipmenTypeUrl: 'admin/equipmenttype',
            EquipmentSearchUrl: 'admin/equipmenttype/getequipmentlist',
            TeamUrl: 'manage/team/listtree',
            TeamMemberEquipmentUrl: 'admin/teammemberequipment/list',
            EquipmentAddtUrl: 'admin/equipment/add',
            EquipmentUpdatetUrl: 'admin/equipment/update',
            TeamDetailUrl: 'manage/team/details',
            EquipmentDeleteUrl: 'admin/equipment',
            TeamMemberEquipmentDeleteUrl: 'admin/teammemberequipment',
            TeamMemberEquipmentAddUpdateUrl: 'admin/teammemberequipment/add',
            TeamMemberEquipmentAssignmentUrl: 'admin/teammemberequipment/assignlist'
        },
        UserProfile: {
            UsersListUrl: 'users/list',
            PermissionGroupUrl: 'Permissiongroups',
            ResourceListUrl: 'Resources/list',
            UserRoleUrl: 'role',
            DeletePermissionGroupUrl: 'Permissiongroups/deletepermission',
            ResourceListTreeUrl: 'Permissiongroups/listresource',
            SaveGroupUrl: 'Permissiongroups/addupdate',
            UpdateNarrativeTooltipUrl: 'Resources/updatetooltip',
            RoleResourceListUrl: 'Permissiongroups/listrole',
            UserRoleAgency: 'admin/teamtype/list?filter={}',
            AddRole: 'admin/teammemberroletype/roleadd',
            AgencyListUrl: 'admin/teamtype',
            TeamListUrl: 'manage/team/teamlist',
            PositionList: 'admin/teammemberroletype/teampositionlist/',
            DeleteRolesUrl: 'roleresources/Deleterole',
            UpdateRolesUrl: 'Roleresources/roleaddupdate',
            RoleNameUrl: 'roletype/',
            ResourceFlatList: 'Permissiongroups/flatresourcelist',
            PermissionGroupList: 'Permissiongroups/list'
        },
        finance: {
            AddFiscalCode: 'tb_fiscal_category_master/addupdate',
            AddFiscalPickList: 'tb_picklist_values/getpicklist',
            fiscalCodeListUrl: 'tb_fiscal_category_master/list',
            UpdateFiscalCode: 'tb_fiscal_category_master/addupdate',
            DeleteFiscalCode: 'tb_fiscal_category_master/deletecategory',
            ValidateFiscalCode: 'tb_fiscal_category_master/fiscalcategoryvalidation'
        },
        placement: {
            GetPlacementList: 'tb_services/list'
        },
        resources: {
        Add : 'Resources',
        ResourcesTypeListUrl: 'referencetype',
        ResourcesTreeListUrl: 'Resources/getresources',
        ResourcesAdd: 'Resources',
        }
    };
}
