import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { AlertService } from '../../../@core/services/alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NavigationUtils } from '../../../pages/_utils/navigation-utils.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService } from '../../../@core/services';
import { PeriodTablePopUpComponent } from '../period-table-pop-up/period-table-pop-up.component';
declare var $: any;
import * as moment from 'moment';
import { Titile4eUrlConfig } from '../_entities/title4e-dashboard-url-config';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { TitleIveadoptionService } from './title-iveadoption.service';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'adoption',
    templateUrl: './titleIVe-adoption.component.html',
    styleUrls: ['./titleIVe-adoption.component.scss']
})
export class AdoptionComponent implements OnInit {
    adoptionMissingFields: FormGroup;
    TitleIVeStatus: FormGroup;
    SpecialNeedsOfChild: FormGroup;
    ChildStatusInfo: FormGroup;
    PlacementAndMedicalInfo: FormGroup;
    @Output() AdoptionApplicable= new EventEmitter();
    @Output() AdoptionApplicability= new EventEmitter();
    AdoptionApplicableInn: any;
    RedeterminationStatus: any;
    @Output() AdoptionNonapplicability= new EventEmitter<string>();
    @Output() AdoptionIncomplete= new EventEmitter();
    adoptionAssistance= new EventEmitter();
    adoptionnonApplicable: any;
    RedeteradoptionAssistance: any;
    RedetExtAdoption: any;
    removalid: any;
    clientName: any;
    adoptionEligibilityData: any;
    adoptionData: any;
    childagency: any;
    casenumber: any;
    childSubmisson: FormGroup;
    submissondate: any;
    assignmentdate: any;
    adoptionapplicabiltystatus: boolean = false;
    showEligibility: boolean = false;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private navigateutil: NavigationUtils,
        public dialog: MatDialog,
        private fb: FormBuilder,
        private _dataStore: DataStoreService

    ) {
        this.route.data.subscribe(response => {
        
            if(response && response.adoptionData && response.adoptionData.adoptionEligibilityInfo && response.adoptionData.adoptionEligibilityInfo.length>0){
                this.adoptionEligibilityData =  response.adoptionData.adoptionEligibilityInfo[0];
                this.adoptionData =  response.adoptionData;
                this.clientName = response.adoptionData.adoptionEligibilityInfo[0].childname;
                this.childagency = response.adoptionData.adoptionEligibilityInfo[0].childagency;
                this.casenumber = response.adoptionData.adoptionEligibilityInfo[0].casenumber;
            }

            if(response && response.adoptionData && response.adoptionData.adoptionApplicabilityInfo && response.adoptionData.adoptionApplicabilityInfo.length>0 )  {  
                this.submissondate = response.adoptionData.adoptionApplicabilityInfo[0].submissiondate;
                var ivestatus = response.adoptionData.adoptionApplicabilityInfo[0].ivestatus;
                this.adoptionapplicabiltystatus = (ivestatus === undefined || ivestatus === null || ivestatus === 'PENDING' || ivestatus === 'COMPLETED') ? false : true;
            }

            if(this._dataStore.getData('adoption_dashboard') === 'adoption'){
                this.showEligibility = true;
            }

            if(response && response.adoptionData && response.adoptionData.assignmentData && response.adoptionData.assignmentData.length>0)  {  
                this.assignmentdate=response.adoptionData.assignmentData[0].insertedon;
            }
        });
     }
    client_id: any;
    ngOnInit() {
        this.client_id= this.router.routerState.snapshot.url.split('/')[4];
        this.removalid= this.router.routerState.snapshot.url.split('/')[5];
        this.childSubmisson = this.fb.group({
            submissondate: [],
            assignmentdate: [] 
        });
    }

    submitDetermination(){
        $('#adoptioneligibilitydetails').click();
      }

      applicabilitySubmission(){
        this.adoptionapplicabiltystatus = true;
        $('#eligibilityTab').click();
      }

    navigateCaseNumber(data: any){
        console.log("data...",data);
        this.navigateutil.routToServiceCase(data);
      }
}
