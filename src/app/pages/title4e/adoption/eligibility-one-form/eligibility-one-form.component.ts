import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PeriodTablePopUpComponent } from '../../period-table-pop-up/period-table-pop-up.component';
declare var $: any;
import * as moment from 'moment';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { TitleIveadoptionService } from '../title-iveadoption.service';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { DatePipe } from '@angular/common';

declare var Formio: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'eligibility-one-form',
    templateUrl: './eligibility-one-form.component.html',
    styleUrls: ['./eligibility-one-form.component.scss'],
    providers: [DatePipe]
})
export class EligibilityOneFormComponent implements OnInit {

    @Input() eligibilityData: any;
    generalRequirement: FormGroup;
    otherCriteria: FormGroup;
    adoptionExt: FormGroup;
    iveStatus: FormGroup;
    TbdFields: FormGroup;
    TitleIVeStatus: FormGroup;
    SpecialNeedsOfChild: FormGroup;
    ChildStatusInfo: FormGroup;
    PlacementAndMedicalInfo: FormGroup;
    AdoptionApplicable: any;
    AdoptionApplicability: any;
    AdoptionApplicableInn: any;
    RedeterminationStatus: any;
    AdoptionNonapplicability: any;
    AdoptionIncomplete: any;
    adoptionAssistance: any;
    adoptionnonApplicable: any;
    RedeteradoptionAssistance: any;
    RedetExtAdoption: any;
    eligibilityOneFormData: any;
    countyname: any;

    @Output() submitRedetermination = new EventEmitter();
    @Output() submitInitialdetermination = new EventEmitter();
    adoptionDetails: any;
    client_id: any;
    removalid: any;
    firstGenList = [];
    caseNumber: any;
    clientName: any;
    constructor(
        private router: Router,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        public dialog: MatDialog,
        private fb: FormBuilder,
        private adoptionService: TitleIveadoptionService,
        private storage: SessionStorageService,
        private datePipe: DatePipe)
     { }
   

    ChildStatusInfoSubmit() {
        this.ChildStatusInfo.reset();
    }
    PlacementAndMedicalInfoSubmit() {
        this.PlacementAndMedicalInfo.reset();
    }
    SpecialNeedsOfChildSubmit() {
        this.SpecialNeedsOfChild.reset();
    }
    TitleIVeStatusSubmit() {
        this.TitleIVeStatus.reset();
    }
    ngOnInit() {
        this.client_id = this.router.routerState.snapshot.url.split('/')[4];
        this.removalid= this.router.routerState.snapshot.url.split('/')[5]; 
        this.getAdoptionEligibilityInfo();
        //this.isTabSwitched();
    }

    isTabSwitched(){
      $('.adoption a').on('shown.bs.tab', (event) => {
        var x = $(event.target).text();
        if(x.includes("Eligibility")){
          this.getAdoptionEligibilityInfo();
        }
      });
    }

    submissionData(data) {
        interface LooseObject {
          [key: string]: any;
        }
    
       
        let siblingDetails = [];
        if(data.siblinggroup && data.siblinggroup.length > 0){
          data.siblinggroup.forEach((element, index) => {
            siblingDetails.push({
                  "SiblingAdoptionDecreeDate":  this.dateConversion(element.siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingAdoptionDecreeDate),
                  "SiblingApplicableChildAssessmentDate":  this.dateConversion(element.siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingApplicableChildAssessmentDate),
                  "SiblingAdoptionApplicable": element.siblinggroupsiblingadoptionstatus === '' ? null : element.siblinggroupsiblingadoptionstatus,
                  "SiblingAdoptiveProviderID": element.siblinggroupnameofthesiblingadoptionplacementProviderId,
                  "SiblingName": element.siblinggroupsiblingname,
                  "__metadata": {
                      "#type": "SiblingDetails",
                      "#id": "SiblingDetails_id_"+index
                  }
              });
            });
        }else{
              siblingDetails.push({
                "SiblingAdoptionDecreeDate":  null,
                "SiblingApplicableChildAssessmentDate":  null,
                "SiblingAdoptionApplicable": "NO",
                "SiblingAdoptiveProviderID": null,
                "SiblingName": null,
                "__metadata": {
                    "#type": "SiblingDetails",
                    "#id": "SiblingDetails_id_1"
                }
            });
        }

        const submissiondata: LooseObject = {
          'cjamsPid': Number(this.client_id),
          'removalid': this.removalid,
          'pagesnapshot': data,
          'initialDetermination': {
            'payload': {
                "name": "Adoption",
                "__metadataRoot": {},
                "Objects": [{
                    "DateAndTimeOfDocumentationForEffortsToPlaceWithoutSubsidy": data.effortsToPlaceChildWereMade === 'YES' ? data.dateandtimeofdocumentationforEffortstoplacewithoutasubsidy : null,
                    "AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_AdoptiveParents_2": this.dateTimeConversion(data.dateofsignatureofparent2OnAdoptionAgreement),
                    "AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_AdoptiveParents_1": this.dateTimeConversion(data.dateofsignatureofparent1OnAdoptionAgreement),
                    "AdoptionFinalizationDate": this.dateTimeConversion(data.dateofAdoptionFinalization),
                    "ChildApplicableAssessmentDate": this.dateConversion(data.childApplicableassessmentdateNotnullEmpty),
                    "AdoptionPetitionFiledDate":  this.dateConversion(data.adoptionPetitionFiledDate),
                    "DateAndTimeOfDocumentationForExceptionGrantedInChildsBestInterests": this.dateTimeConversion(data.dateandtimeofdocumentationforExceptiongrantedinchildsbestinterests),
                    "AdoptionAssistanceStartDate": this.dateConversion(data.startDateofAdoptionassistance),
                    "TPRGrantedtoBothParent": data.tprGrantedtoBothParent,
                    "AnotherReasonForChildNotReturningHome": (data.anotherreasonchildcouldnotreturntoparenthome === undefined || data.anotherreasonchildcouldnotreturntoparenthome === null) ? null : (data.anotherreasonchildcouldnotreturntoparenthome === '' ? 'NO' : 'YES'),
                    "AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_Agency": this.dateTimeConversion(data.dateofLdssAgencySignatureonAdoptionAgreement),
                    "IsReasonForExceptionRecorded": data.isReasonForExceptionRecorded,
                    "QualifiedAlien": data.isUsCitizen === 'YES' ? 'NO' : (data.isQualifiedAlien === '' ? null :  data.isQualifiedAlien) ,
                    "ReasonForNotGrantingTPRForParent": data.ifnoReasonfornotgrantingTpRforbothparent,
                    "USCitizen": data.isUsCitizen,
                    "applicantInformation": {
                        "ClientID": this.client_id,
                        "__metadata": {
                            "#type": "ApplicantInformation",
                            "#id": "ApplicantInformation_id_1"
                        }
                    },
                    "person": {
                        "ChildHasFosterParentEmotionalTies": null,
                        "IncomeAndAssetsMetAFDCStandards": data.wasIncomeAndAssetsMet,
                        "ChildAdoptionUnsuccessfulEffortsToPlace":  data.childAdoptionUnsuccessfulEffortsToPlace === '' ? null : data.childAdoptionUnsuccessfulEffortsToPlace,
                        "ChildRaceEthnicity": data.raceorethnicitycanonlybeincombinationwithoneofthefactorsabove15,
                        "Gender": data.gender, 
                        "parentDetails": [{
                            "MinorParentRemovalDate": this.dateConversion(data.childRemovalDateVpaRemovalDateofminorparentwhenRemovalTypeMinorParentVpa),
                            "MinorParentName": data.parentName,
                            "MinorParentCurrentPlacementType": data.childscurrentplacementtypeofminorparent === '' ? null : data.childscurrentplacementtypeofminorparent,
                            "MinorParentDateOfBirth": this.dateConversion(data.birthdateofparentAgeoftheparentatthetimeofapplicablechildassessment),
                            "MinorParentRemovalCourtOrderDate": this.dateConversion(data.removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt),
                            "MinorParentIVEEligibilityForFosterCareStatus": data.minorParentIveEligibilityForFosterCareStatus === '' ? null : data.minorParentIveEligibilityForFosterCareStatus,
                            "__metadata": {
                                "#type": "ParentDetails",
                                "#id": "ParentDetails_id_1"
                            },
                            "MinorParentIVEEligibilityForFosterCareStartDate": this.dateConversion(data.minorParentIveEligibilityForFosterCareStartDate),
                            "DateOfLatestPaymentOfMinorParentIVEEligibilityForFosterCare": this.dateConversion(data.dateOfLatestPaymentOfMinorParentIveEligibilityForFosterCare),
                            "MinorParentPhysicalAddress": data.physicaladdressofminorparent
                        }],
                        "ChildHasHighRiskOfDisability": data.recognizedhighriskofphysicalormentaldisabilityordisease ? 'YES' : 'NO',
                        "Name": data.nameofchild,
                        "Child_Removal_Date": this.dateConversion(data.childRemovalDate),
                        "ChildRemovedFromSpecifiedRelative": data.wasTheChildRemovedFromSpecifiedRelative,
                        "ChildDeprivedOfParentalSupport": data.wasTheChildDeprivedOfParentalSupport,
                        "ChildCanReturnToHome": data.childCanReturnToHome === "" ? null : data.childCanReturnToHome ,
                        "ChildExpectedAdoptiveProviderID": data.siblinggroupnameofthesiblingadoptionplacementProviderId,
                        "ChildHasEmotionalDisturbance": data.emotionaldisturbancemedicaldocumentationrequired ? 'YES' : 'NO',
                        "ChildCurrentIVEFosterCareEligibilityStatus": data.minorParentIveEligibilityForFosterCareStatus === ''? null : data.minorParentIveEligibilityForFosterCareStatus,
                        "__metadata": {
                            "#type": "Person",
                            "#id": "Person_id_1"
                        },
                        "DateOfBirth": this.dateConversion(data.dateofbirth),
                        "Removal_Court_Order_Date": this.dateConversion(data.removalCourtOrderDate),
                        "ChildPreviousAdoptiveParentTPRDate":  this.dateConversion(data.previousAdoptiveParentsTpRterminationofparentrightDateIfterminated),
                        "ChildPhysicalAddress": data.physicaladdressofchild,
                        "ChildMeetsSSIMedicalDisabledEligReqts": data.medicalconditionordisability,
                        "VoluntaryRelinquishment": data.voluntaryRelinquishment,
                        "ChildSSIEligibilityStatusOnOrBeforeDateOfAdoption": data.childsSsiEligibilityStatus,
                        "ChildPreviousAdoptiveParentDeathDate": this.dateConversion(data.previousAdoptiveParentsDeathDateIfdead),
                        "ChildHasPhysicalMentalEmotionalDisability":  data.physicalmentaloremotionaldisabilityordiseasemedicaldocumentationrequired ? 'YES' : 'NO',
                        "CountyOfJurisdiction_LDSS": data.countyofjurisdiction.trim(),
                        "siblingDetails": siblingDetails,
                        "StartDateOfReceivingSSI": this.dateConversion(data.startDateOfReceivingSsi),
                        "ChildPreviousAdoptionIVEStatus": data.childsIvEStatusofpreviousadoption === '' ? null : data.childsIvEStatusofpreviousadoption,
                        "ChildExpectedAdoptionDate": this.dateConversion(data.previousAdoptiveParentsTpRterminationofparentrightDateIfterminated),
                        //"ChildReceivingSSIAtRemoval": null,
                        "ChildPreviouslyAdopted": data.childsPreviouslyadopted
                    },
                    "DidEffortsToPlaceChildWereMade": data.effortsToPlaceChildWereMade,
                    "SingleParentAdoptionCheck": data.singleParentAdoptionCheck ? "YES" : "NO",
                    "__metadata": {
                        "#type": "Application",
                        "#id": "Application_id_1"
                    },
                    "DateOfTPROfParent_2": this.dateConversion(data.dateofTpRofParent2),
                    "DateOfTPROfParent_1": this.dateConversion(data.dateofTpRofParent1),
                    "status": {
                        "AdoptionAssistance": null,
                        "AdoptionApplicable": "YES",
                        "AdoptionNonApplicable": "NO",
                        "__metadata": {
                            "#type": "Status",
                            "#id": "Status_id_1"
                        }
                    }
                }]
            }
        }
      };
        return submissiondata;
      }
    


      
  dateConversion(date) {
    if (date === undefined || date === null || date === '') {
      return null;
    } else {
      return this.datePipe.transform(date, 'MM/dd/yyyy');
    }
  }

  dateTimeConversion(date){
    if(date === undefined || date === null || date === ''){
      return null;
    }else{
      return this.datePipe.transform(date,"MM/dd/yyyy HH:mm:ss");
    }
  }

    getPersonsList() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { objecttypekey: 'servicecase', objectid: this.adoptionDetails.servicecaseid }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
            .subscribe((result) => {
                console.log("-=-=-=-=-=-=-"+JSON.stringify(result));
                this.firstGenList = result.data;
            });
    }

    getAdoptionEligibilityInfo() {
        this._commonHttpService.getAll(
            'titleive/adoption/adoption-eligibility-worksheet/' + this.client_id+'/'+this.removalid
        ).subscribe((response: any) => {
            this.eligibilityOneFormData = response;
            this.adoptionDetails = response.adoptionEligibilityInfo[0];
            this.clientName = response.adoptionEligibilityInfo[0].childname;
            this.getPersonsList();

            if(this.eligibilityOneFormData && this.eligibilityOneFormData.demographicsInfo && this.eligibilityOneFormData.demographicsInfo.length > 0 
              && this.eligibilityOneFormData.demographicsInfo[0].childjurisdiction){
                this.loadCounty(this.eligibilityOneFormData.demographicsInfo[0].childjurisdiction)
            }else{
              this.startForm();
            }
        });
    }


    loadCounty(countyidinput) {
      this._commonHttpService.getArrayList(
        { where: { activeflag: '1', state: 'MD', countyid: countyidinput}, nolimit: true, method: 'get' },
        'admin/county' + '?filter'
      ).subscribe(res => {
        if(res && res.length > 0){
          this.countyname = res[0].countyname;
        }
        this.startForm();
      });
    }
  

    setData() {
        interface LooseObject {
          [key: string]: any;
        }
    
        const data: LooseObject = {};
          
          console.log('--123456789---'+JSON.stringify(this.eligibilityOneFormData));

          let gender = '';
          if (this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 && 
                this.eligibilityOneFormData.adoptionEligibilityInfo[0].gender === 'M') {
            gender = 'Male';
          } else if (this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 && 
            this.eligibilityOneFormData.adoptionEligibilityInfo[0].gender === 'F') {
            gender = 'Female';
          } else {
            gender = 'Other';
          }
              
         // console.log('this.eligibilityOneFormData' + JSON.stringify(this.eligibilityOneFormData)); 
            data.countyofjurisdiction = this.countyname; 

          if(this.eligibilityOneFormData && this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 ){ 
            data.nameofchild = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childname; 
            data.clientId = this.eligibilityOneFormData.adoptionEligibilityInfo[0].client_id; 
            data.dateofbirth = this.eligibilityOneFormData.adoptionEligibilityInfo[0].dateofbirth; 
            
            data.clientIDofAdoptiveParent  = this.eligibilityOneFormData.adoptionEligibilityInfo[0].parent1providerid; 
            data.nameofAdoptiveParent = this.eligibilityOneFormData.adoptionEligibilityInfo[0].parent1providername; 
            data.dateofFinalizationAdoptionDateofDecree  = this.eligibilityOneFormData.adoptionEligibilityInfo[0].finalizationdate; 

            data.dateandtimeofdocumentationforEffortstoplacewithoutasubsidy = this.eligibilityOneFormData.adoptionEligibilityInfo[0].effortdate;
            data.exceptiongrantedinchildsbestinterests = this.eligibilityOneFormData.adoptionEligibilityInfo[0].isexceptiongranted;
            data.dateandtimeofdocumentationforExceptiongrantedinchildsbestinterests = this.eligibilityOneFormData.adoptionEligibilityInfo[0].dateofexceptiongranted; 
            data.isReasonForExceptionRecorded = (this.eligibilityOneFormData.adoptionEligibilityInfo[0].isexceptiongranted === 'YES' && this.eligibilityOneFormData.adoptionEligibilityInfo[0].remarks !== null && this.eligibilityOneFormData.adoptionEligibilityInfo[0].remarks !== '') ? 'YES' : 'NO'; 
            
            data.gender = gender;
           // data.ChildRaceEthnicity = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childraceethnicity;
            data.ChildDisabilityEvaluationDocumentionDate = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdisabilityevaluationdocumentiondate;
            data.ChildDisabilityType = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdisabilitytype; 
            data.raceorethnicitycanonlybeincombinationwithoneofthefactorsabove15 = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childraceethnicity;  
            data.isUsCitizen = this.eligibilityOneFormData.adoptionEligibilityInfo[0].uscitizen;   
            data.isQualifiedAlien = this.eligibilityOneFormData.adoptionEligibilityInfo[0].qualifiedalien; 

            //Calculating age
            if(data.dateofbirth){
              let dt = new Date(data.dateofbirth);
              let timeDiff = Math.abs(Date.now() - dt.getTime());
              data.age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
            }

            //data.age = this.eligibilityOneFormData.adoptionEligibilityInfo[0].qualifiedalien;    
            data.removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt = this.eligibilityOneFormData.adoptionEligibilityInfo[0].removalcourtorderdate;
            data.physicaladdressofchild = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childphysicaladdress;
            data.wasTheChildDeprivedOfParentalSupport = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdeprivedofparentalsupport;
            data.wasTheChildRemovedFromSpecifiedRelative = this.eligibilityOneFormData.adoptionEligibilityInfo[0].wasthechildremovedfromspecifiedrelative;

            data.afdcEligibilityMet = this.eligibilityOneFormData.adoptionEligibilityInfo[0].isafdceligibilitymet;									
            data.wasIncomeAndAssetsMet = this.eligibilityOneFormData.adoptionEligibilityInfo[0].isincomeassetsmet;										

            data.effortsToPlaceChildWereMade = this.eligibilityOneFormData.adoptionEligibilityInfo[0].effortstoplacechildweremade;

            if(this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails !== null && 
              this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails.length > 0 ){
                var tprdetails = this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails;
                var isgranted = true;

                if(tprdetails && tprdetails.length === 2){
                  isgranted = true;
                }else{
                  isgranted = false;
                }

                if(isgranted){
                  data.tprGrantedtoBothParent = 'YES';
                  data.dateofTpRofParent1 = tprdetails[0].tprdecisiondate;
                  data.dateofTpRofParent2 = tprdetails[1].tprdecisiondate;
                }else{
                  data.tprGrantedtoBothParent = 'NO';
                  tprdetails.forEach(tprdetail => {
                    if(tprdetail.isgranted === 'NO'){
                      data.ifnoReasonfornotgrantingTpRforbothparent = tprdetail.reason;
                    }
                  });
                }
            }
          } 
          data.childApplicabilitystatusNotIncomplete = this.eligibilityOneFormData.adoptionApplicabilityauditInfo && this.eligibilityOneFormData.adoptionApplicabilityauditInfo.length > 0 ?  this.eligibilityOneFormData.adoptionApplicabilityauditInfo[0].finalresult : '';


          if(this.eligibilityOneFormData && this.eligibilityOneFormData.adoptionApplicabilityInfo && this.eligibilityOneFormData.adoptionApplicabilityInfo.length> 0 ){ 
            data.medicalconditionordisability = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childmeetallmedicaldisabilityrequirementsforssi;
            data.child617Yearsofage = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].child617yearsofage;
            data.physicalmentaloremotionaldisabilityordiseasemedicaldocumentationrequired = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].physicalmentalemotionaldisability;
            data.emotionaldisturbancemedicaldocumentationrequired = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].emotionaldisturbance;
            data.siblingInformationCheck = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].siblinginformationcheck;
            data.recognizedhighriskofphysicalormentaldisabilityordisease = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].recognizedhighriskofphysicaldisability;
            data.raceEthnicityofchild = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].raceethnicityofchild;

            data.childAdoptionUnsuccessfulEffortsToPlace  = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].unsuccessfulreasonableeffortsstatusrecords; 
            data.childApplicableassessmentdateNotnullEmpty = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].submissiondate;

            data.anotherreasonchildcouldnotreturntoparenthome =  this.eligibilityOneFormData.adoptionApplicabilityInfo[0].descriptionofreturnhome;
            data.childCanReturnToHome = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].canchildreturntohome;

            data.removalCourtOrderDate = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].removalcourtorderdate;
            data.childRemovalDate = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childremovaldate;
            data.childsPreviouslyadopted = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childspreviouslyadopted;
            
            data.voluntaryRelinquishment = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].voluntaryrelinquishment;


            if(this.eligibilityOneFormData.adoptionApplicabilityInfo[0].adoptionapplicabilityminorparentinfo !== null &&
              this.eligibilityOneFormData.adoptionApplicabilityInfo[0].adoptionapplicabilityminorparentinfo.length > 0 ){
               var minorparent = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].adoptionapplicabilityminorparentinfo;

               minorparent.forEach(mnrparnt => {
                  data.parentName = mnrparnt.minorparentname;
                  //data.birthdateofparentAgeoftheparentatthetimeofapplicablechildassessment = mnrparnt.minorparentname;
                  data.removalType = mnrparnt.removaltypeofminorparent;
                  data.removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt = mnrparnt.removalcourtorderdateofminorparent;
                  data.childRemovalDateVpaRemovalDateofminorparentwhenRemovalTypeMinorParentVpa = mnrparnt.removaldateofminorparent;
                  data.childscurrentplacementtypeofminorparent = mnrparnt.childscurrentplacementtype;
                  data.physicaladdressofminorparent = mnrparnt.physicaladdressofminorparent;
                  //data.physicaladdressofchild = mnrparnt.minorparentname;
                  //data.minorParentIveEligibilityForFosterCareStatus =mnrparnt.minorparentname;
                  //data.dateOfLatestPaymentOfMinorParentIveEligibilityForFosterCare = mnrparnt.minorparentname;
                  //data.minorParentIveEligibilityForFosterCareStartDate = mnrparnt.minorparentname;
               });
            }
          }

          if(this.eligibilityOneFormData && this.eligibilityOneFormData.demographicsInfo && this.eligibilityOneFormData.demographicsInfo.length> 0 ){ 
            data.adoptionPetitionFiledDate = this.eligibilityOneFormData.demographicsInfo[0].adoptionpetitiondate;
            data.dateofAdoptionFinalization = this.eligibilityOneFormData.demographicsInfo[0].adoptionfinalizationdate;
            data.dateofsignatureofparent1OnAdoptionAgreement = this.eligibilityOneFormData.demographicsInfo[0].adoptionparent1signdate;
            data.dateofsignatureofparent2OnAdoptionAgreement = this.eligibilityOneFormData.demographicsInfo[0].adoptionparent2signdate;
            data.dateofLdssAgencySignatureonAdoptionAgreement = this.eligibilityOneFormData.demographicsInfo[0].adoptionldssdate;
            data.singleParentAdoptionCheck = this.eligibilityOneFormData.demographicsInfo[0].singleparentadoptioncheck;
          }

          return data;
    
        } 

    startForm() {
       // console.log("worksheetData >>> ", this.eligibilityData);
       console.log('Inside form');
        const formDataToShow = this.setData();
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('titleIvEAdoptionEligibilityNewForm'), environment.formBuilderHost + `/form/5c9ce8218d24c6329b180b25`, {
            hooks: {
                beforeSubmit: (submission, next) => {
                    var submissiondata =  this.submissionData(submission.data);
                    console.log('-------submit---------'+JSON.stringify(submissiondata));
                    this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe(
                      (res) => {
                        this._alertService.success("Submitted successfully!");
                      },
                      (error) => {
                          this._alertService.error("Submission Failed");
                      });
                    // Only call next when we are ready.
                    // next();
                  }
            }
        }).then((form) => {
            form.submission = {
                data: formDataToShow
                //data: {
                    //persons_json: JSON.stringify(personsDropdown)
               // }
            };

            form.on('submit', submission => {
                console.log("form submit >>> ", submission);
            });

            form.on('change', formData => {
                // apply binded data to the formbuilder
                form.submission = {
                    data: formData.data
                };
            });
            form.on('render', formData => {
                console.log("form render >>> ", formData);
            });
            form.on('error', formData => {
                console.log("form error >>> ", formData);
            });
        

           

            
        }).catch((err) => {
            // Failed to load form builder
            console.log("Form Error:: ", err);
        });
    }
     
}
