import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibilityOneFormComponent } from './eligibility-one-form.component';

describe('EligibilityOneFormComponent', () => {
  let component: EligibilityOneFormComponent;
  let fixture: ComponentFixture<EligibilityOneFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EligibilityOneFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EligibilityOneFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
