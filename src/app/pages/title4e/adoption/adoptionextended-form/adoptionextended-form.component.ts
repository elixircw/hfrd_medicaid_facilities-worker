import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
//import { AlertService, CommonHttpService, SessionStorageService, DataStoreService } from '../../../../@core/services';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { TitleIveadoptionService } from '../title-iveadoption.service';
import { DatePipe } from '@angular/common';
declare var Formio: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'adoptionextended-form',
  templateUrl: './adoptionextended-form.component.html',
  styleUrls: ['./adoptionextended-form.component.scss'],
  providers: [DatePipe]
})
export class AdoptionExtendedFormComponent implements OnInit {

  assessmentFormData: any;
  ChildStatusInfo: FormGroup;
  PlacementAndMedicalInfo: FormGroup;
  SpecialNeedsOfChild: FormGroup;
  TitleIVeStatus: FormGroup;
  @Output() submitApplicability = new EventEmitter();
  adoptionDetails: any;
  client_id: any;
  childassessmentData: any;
  removalid: any;
  countyname: any;
  adoptionExtensionData: any;

  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private router: Router,
    private storage: SessionStorageService,
    private adoptionService: TitleIveadoptionService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.client_id = this.router.routerState.snapshot.url.split('/')[4];
    this.removalid= this.router.routerState.snapshot.url.split('/')[5];  
    this.searchAdoptionExtensionData();
    //this.isTabSwitched();
  }

  isTabSwitched(){
    $('.adoption a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("Extended")){
        this.searchAdoptionExtensionData();
      }
    });
  }
     

  setData() {        
    interface LooseObject {
    [key: string]: any;
    }
    const data: LooseObject = {};
          
    let gender = '';
    if (this.adoptionExtensionData.adoptionEligibilityInfo && this.adoptionExtensionData.adoptionEligibilityInfo.length> 0 && 
          this.adoptionExtensionData.adoptionEligibilityInfo[0].gender === 'M') {
      gender = 'Male';
    } else if (this.adoptionExtensionData.adoptionEligibilityInfo && this.adoptionExtensionData.adoptionEligibilityInfo.length> 0 && 
      this.adoptionExtensionData.adoptionEligibilityInfo[0].gender === 'F') {
      gender = 'Female';
    } else {
      gender = 'Other';
    }

    data.countyofjurisdiction = this.countyname; 
    if(this.adoptionExtensionData && this.adoptionExtensionData.adoptionEligibilityInfo && this.adoptionExtensionData.adoptionEligibilityInfo.length> 0 ){ 
      data.nameofchild = this.adoptionExtensionData.adoptionEligibilityInfo[0].childname; 
      data.clientId = this.adoptionExtensionData.adoptionEligibilityInfo[0].client_id; 
      data.dateofbirth = this.adoptionExtensionData.adoptionEligibilityInfo[0].dateofbirth;   
      data.gender = gender; 
    } 
    return data;

  }

  startForm() {
    const formDataToShow = this.setData();
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('titleIvEAdoptionExtendedForm'), environment.formBuilderHost + `/form/5c9d0fe98d24c6329b180b2b`, {
      hooks: {
        beforeSubmit: (submission, next) => {
          var submissiondata =  this.submissionData(submission.data);
          console.log('-------submit---------'+JSON.stringify(submissiondata));
          this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe(
            (res) => {
              this._alertService.success("Submitted successfully!");
            },
            (error) => {
                this._alertService.error("Submission Failed");
            });
          // Only call next when we are ready.
          // next();
        }
      }
    }).then((form) => {
      form.submission = {
          data: formDataToShow
      };

      form.on('submit', submission => {
        console.log("form submit >>> ", submission);
      });

      form.on('change', formData => {
        if (formData && formData.changed && formData.changed.value && this.childassessmentData) {
          if (formData.changed.value.clientInformation) {
          }

          if (formData.changed.value.eligibilityCriteria) {
          }


          if (formData.data.eligibilitycriterialist === 'YES') {
          }

          form.submission = {
            data: formData.data
          };
        }

      });
      form.on('render', formData => {
        console.log("form render >>> ", formData);
      });
      form.on('error', formData => {
        console.log("form error >>> ", formData);
      });
    }).catch((err) => {
      console.log("Form Error:: ", err);
    });
  }


  submissionData(data) {
    interface LooseObject {
      [key: string]: any;
    }
  

    const submissiondata: LooseObject = {
      'cjamsPid': Number(this.client_id),
      'removalid': this.removalid,
      'pagesnapshot': data,
      'reDetermination': {
        'payload': {
          "name": "EXT_Adoption",
            "__metadataRoot": {},
            "Objects": [{
                "ChildMeetContinueEligibilityCriteria": data.childMetContinueEligibilityCriteria,
                "AdoptionFinalizationDate": this.dateTimeConversion(data.dateofAdoptiondecree),
                "person": {
                    "DateOfBirth": this.dateConversion(data.dateofbirth),
                    "ChildDisabilityEvaluationDocumentionDate": this.dateConversion(data.evaluationdocumentationdate),
                    "StartDateOfSecondaryEducationOrEquivalentProgram": this.dateConversion(data.startdateofSecondaryeducationequivalentprogram),
                    "NameOfpostSecondaryOrVocationalEducation": data.nameofschoolprogram === "" ? null : data.nameofschoolprogram,
                    "CountyOfJurisdiction_LDSS": data.countyofjurisdiction,
                    "Gender": data.gender,
                    "NameOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment": data.nameofprogramactivity === "" ? null : data.nameofprogramactivity,
                    "Name": data.nameofchild,
                    "ChildDisabilityType": data.disabilityType=== "" ? null : data.disabilityType,
                    "StartDateOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment": this.dateConversion(data.participationStartdateofprogrampromotetoemployment),
                    "HoursPerMonthEmployed": 0,//data.hourspermonthemployed,
                    "StartDateOfEmployment": this.dateConversion(data.startdateofemployment),
                    "__metadata": {
                        "#type": "Person",
                        "#id": "Person_id_1"
                    },
                    "ChildDisabilityStartDate": this.dateConversion(data.disabilityStartdate),
                    "StartDateOfpostSecondaryOrVocationalEducation": this.dateConversion(data.enrollmentStartdateofpostSecondaryorvocationaleducationinstitution),
                    "NameOfSecondaryEducationOrEquivalentProgram": data.nameofinstitution=== "" ? null : data.nameofinstitution,
                    "NameOfEmployer": data.nameofemployer=== "" ? null : data.nameofemployer,
                },
                "__metadata": {
                    "#type": "Application",
                    "#id": "Application_id_1"
                },
                "IsDocumentedPhysicalAndMentalDisability": data.isDocumentedPhysicalAndMentalDisability=== "" ? null : data.isDocumentedPhysicalAndMentalDisability,
                "status": {
                    "AdoptionAssistance": "Eligible",
                    "EXTAdoption": null,
                    "__metadata": {
                        "#type": "Status",
                        "#id": "Status_id_1"
                    }
                }
            }]
        }
    }
  };
    return submissiondata;
  }

  dateConversion(date) {
    if (date === undefined || date === null || date === '') {
      return null;
    } else {
      return this.datePipe.transform(date, 'MM/dd/yyyy');
    }
  }

  dateTimeConversion(date){
    if(date === undefined || date === null || date === ''){
      return null;
    }else{
      return this.datePipe.transform(date,"MM/dd/yyyy HH:mm:ss");
    }
  }

  searchAdoptionExtensionData(){
   this._commonHttpService.getAll('titleive/adoption/adoption-eligibility-worksheet/' + this.client_id+'/'+this.removalid).subscribe(
      (response: any) => {
        this.adoptionExtensionData = response;
        if(this.adoptionExtensionData && this.adoptionExtensionData.demographicsInfo && this.adoptionExtensionData.demographicsInfo.length > 0 
          && this.adoptionExtensionData.demographicsInfo[0].childjurisdiction){
            this.loadCounty(this.adoptionExtensionData.demographicsInfo[0].childjurisdiction)
        }else{
          this.startForm();
        }
      },
      (error) => {
        console.log(error);
        return false;
      }
    );
  }


  loadCounty(countyidinput) {
    this._commonHttpService.getArrayList(
      { where: { activeflag: '1', state: 'MD', countyid: countyidinput}, nolimit: true, method: 'get' },
      'admin/county' + '?filter'
    ).subscribe(res => {
      if(res && res.length > 0){
        this.countyname = res[0].countyname;
      }
      this.startForm();
    });
  }



}



