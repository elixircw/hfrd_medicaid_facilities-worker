import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionExtendedFormComponent } from './adoptionextended-form.component';

describe('AdoptionExtendedFormComponent', () => {
  let component: AdoptionExtendedFormComponent;
  let fixture: ComponentFixture<AdoptionExtendedFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionExtendedFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionExtendedFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


