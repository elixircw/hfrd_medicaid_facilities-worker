import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildassessmentFormComponent } from './childassessment-form.component';

describe('ChildassessmentFormComponent', () => {
  let component: ChildassessmentFormComponent;
  let fixture: ComponentFixture<ChildassessmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildassessmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildassessmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


