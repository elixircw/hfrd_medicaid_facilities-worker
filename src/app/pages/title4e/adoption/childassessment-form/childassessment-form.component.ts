import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
//import { AlertService, CommonHttpService, SessionStorageService, DataStoreService } from '../../../../@core/services';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { AuthService } from '../../../../@core/services/auth.service';
import { DatePipe } from '@angular/common';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { TitleIveadoptionService } from '../title-iveadoption.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import * as _ from 'lodash';
declare var Formio: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'childassessment-form',
  templateUrl: './childassessment-form.component.html',
  styleUrls: ['./childassessment-form.component.scss'],
  providers: [DatePipe]
})
export class ChildassessmentFormComponent implements OnInit {

  @Input() assessmentFormData: any;
  @Input() clientIDInput: any;
  @Input() removalIDInput: any;
  userInfo: AppUser;
  ChildStatusInfo: FormGroup;
  PlacementAndMedicalInfo: FormGroup;
  SpecialNeedsOfChild: FormGroup;
  TitleIVeStatus: FormGroup;
  childassessmentData: any;
  casenumber: any;
  @Output() submitApplicability = new EventEmitter();
  adoptionDetails: any;
  client_id: any;
  removalid: any;
  personid: any;
  formSubmissionData: any;
  submissionRequest: boolean = false;
  agency = '';
  persondetails: any;
  relationship: any;
  constructor(
    private router: Router,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private _authService: AuthService,
    private storage: SessionStorageService,
    private adoptionService: TitleIveadoptionService,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.agency = this._authService.getAgencyName();
    this.userInfo = this._authService.getCurrentUser();
    if(this.agency === 'CW'){
      this.client_id= this.clientIDInput; 
      this.removalid= this.removalIDInput;     
    }else if(this.agency === 'IV-E'){
      this.client_id= this.router.routerState.snapshot.url.split('/')[4]; 
      this.removalid= this.router.routerState.snapshot.url.split('/')[5];     
    }  
    this.searchChildAssessmentId();
    //this.isTabSwitched();
  }

  isTabSwitched(){
    $('.intake-tabs a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("ASSESSMENT")){
        this.searchChildAssessmentId();
      }
    });
  }

  saveData(){
    this.saveToDB(this.formSubmissionData);
  }
  
  saveToDB(submission){
    
        //var submissionData = this.submissionData(submission);

    //  if(this.submissionRequest){
      if(submission.resubmissionCount === 0){
        submission.submissionDate = new Date();
      }else if(submission.resubmissionCount > 0 && this.userInfo.user.userprofile.title === 'CWCW'){
        submission.resubmissionCaseworkerName = this.userInfo.user.userprofile.firstname + ' ' +this.userInfo.user.userprofile.lastname;
        submission.resubmissionDate = new Date();
      }
      submission.resubmissionCount = Number(submission.resubmissionCount) + 1;
    //  }   
     


      const idInfo = {
        'clientId': Number(this.client_id),
        'removalid': this.removalid
      };

      var request = {
          ...idInfo,
          ...submission
      };

      request.childBirthDate = request.childBirthDate === '' ? null : request.childBirthDate;
      request.expectedAdoptionDate = request.expectedAdoptionDate === '' ? null : request.expectedAdoptionDate;
      request.startdateofreceivingSsi = request.startdateofreceivingSsi === '' ? null : request.startdateofreceivingSsi;
      request.adoptiveParentsTprDate = request.adoptiveParentsTprDate === '' ? null : request.adoptiveParentsTprDate;

      if(request.siblingStatus && request.siblingStatus.length > 0){
        request.siblingStatus.forEach(element => {
          element.siblingStatusColumnsdateofsiblingsadoptiondecree = element.siblingStatusColumnsdateofsiblingsadoptiondecree === '' ? null : element.siblingStatusColumnsdateofsiblingsadoptiondecree;
          element.siblingStatusColumnsdateofsiblingsapplicablechildassessment = element.siblingStatusColumnsdateofsiblingsapplicablechildassessment === '' ? null : element.siblingStatusColumnsdateofsiblingsapplicablechildassessment;
        });
      }

      if(request.minorParentInformation && request.minorParentInformation.length > 0){
        request.minorParentInformation.forEach(element => {
          element.minorParentInformationBirthdateofparent = element.minorParentInformationBirthdateofparent === '' ? null : element.minorParentInformationBirthdateofparent;
          element.minorParentInformationRemovalcourtorderdateofminorparent = element.minorParentInformationRemovalcourtorderdateofminorparent === '' ? null : element.minorParentInformationRemovalcourtorderdateofminorparent;
          element.minorParentInformationRemovalDateVpaRemovalDateofminorparent = element.minorParentInformationRemovalDateVpaRemovalDateofminorparent === '' ? null : element.minorParentInformationRemovalDateVpaRemovalDateofminorparent;
        });
      }

      this._commonHttpService.create(request, Titile4eUrlConfig.EndPoint.postAdoptionApplicabilityToDB).subscribe(
        (res) => {
          this._alertService.success("Submitted successfully!");
        },
        (error) => {
            this._alertService.error("Submission Failed");
        });
     
  }

  setData() {
    interface LooseObject {
      [key: string]: any;
    }
    const data: LooseObject = {};

      if(this.childassessmentData.demographicsInfo && this.childassessmentData.demographicsInfo.length> 0 ){
        data.childBirthDate = this.childassessmentData.demographicsInfo[0].dateofbirth;
      }

      if(this.childassessmentData.adoptionEligibilityInfo && this.childassessmentData.adoptionEligibilityInfo.length> 0 ){
        this.casenumber = this.childassessmentData.adoptionEligibilityInfo[0].servicecaseid;
        //B. Placement and Medical Information  === Logic
        if(this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'JD'){
          data.aCourtOrder = 'YES';
          data.BAvoluntaryplacementagreement = 'NO';
          data.voluntaryRelinquishment = 'NO';
          data.dateoffirstcourtorderwithCtw = this.childassessmentData.adoptionEligibilityInfo[0].removalcourtorderdate;
          data.removalCourtorderdate = this.childassessmentData.adoptionEligibilityInfo[0].childremovaldate;
        }else if(this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'TLV' || 
                  this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'CDVP' || 
                  this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'EHA' ){
                    data.aCourtOrder = 'NO';
                    data.BAvoluntaryplacementagreement = 'YES';
                    data.voluntaryRelinquishment = 'NO';
        }else{
          data.aCourtOrder = 'NO';
          data.BAvoluntaryplacementagreement = 'NO';
          data.voluntaryRelinquishment = 'YES';
        }
        //B. Placement and Medical Information  === Logic
      }
      

      
        //Minor parent information
        var persondetails = this.persondetails;
        if(persondetails && persondetails.data && persondetails.data.length > 0){
          data.minorParentInformation = [];
          persondetails.data.forEach((person, personIndex) => {
            if(this.client_id === person.cjamspid){
              this.personid = person.personid;
            };
            if (person.roles) {
              person.roles.forEach((role, roleIndex) => {
                var date1 = new Date(person.dob);
                var date2 = new Date(data.childBirthDate);
                var diff = Math.abs(date1.getTime() - date2.getTime());
                var diffDays = Math.ceil(diff / (1000 * 3600 * 24)); 
                var years = Math.floor(diffDays/365); 
                if(person.gender === 'Female' && role.intakeservicerequestpersontypekey === 'PARENT' && years < 100){
                  data.minorParentInformation.push({
                    minorParentInformationMinorParentName : person.fullname
                  });
                  data.isthechildresidinginafosterfamilyhome = 'YES';
                }
              });
            }
            
          });
        }


        //Sibling information
        if(this.relationship && this.relationship.length > 0){
          data.siblingStatus = [];
          data.siblingInformation = [];
          let name = '';
          this.relationship.forEach((relationsp, index) => {
            if(relationsp.relationshiptypekey === 'BIOBR' || relationsp.relationshiptypekey === 'BGSISTR'){
              if(name !== (relationsp.firstname + ' ' + relationsp.lastname)){
                data.siblingStatus.push({
                  siblingStatusColumnsSiblingsRelationshipwithchild : relationsp.relation,
                  siblingStatusColumnsNameofsiblingchild: relationsp.firstname + ' ' + relationsp.lastname
                });
                //data.Isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage = 'YES';
                data.siblingInformationCheck = true;
                data.siblingInformation.push({
                  siblingInformationSiblingname :  relationsp.firstname + ' ' + relationsp.lastname
                });
              }
              name = relationsp.firstname + ' ' + relationsp.lastname;
            }
          });
        }

      if(this.childassessmentData.adoptionApplicabilityInfo && this.childassessmentData.adoptionApplicabilityInfo.length> 0 ){
        data.childBirthDate = this.childassessmentData.adoptionApplicabilityInfo[0].childbirthdate;
        data.fosterparentemotionalbonding = this.childassessmentData.adoptionApplicabilityInfo[0].fosterparentemotionalbonding;
        data.unsuccessfulreasonableeffortsstatusrecords = this.childassessmentData.adoptionApplicabilityInfo[0].unsuccessfulreasonableeffortsstatusrecords;
        data.raceEthnicityofchild = this.childassessmentData.adoptionApplicabilityInfo[0].raceethnicityofchild;
        data.descriptionofreturnhome = this.childassessmentData.adoptionApplicabilityInfo[0].descriptionofreturnhome;
        data.recognizedhighriskofphysicaldisability = this.childassessmentData.adoptionApplicabilityInfo[0].recognizedhighriskofphysicaldisability;
        data.childRemovalDate = this.childassessmentData.adoptionApplicabilityInfo[0].childremovaldate;
        data.canchildreturntohome = this.childassessmentData.adoptionApplicabilityInfo[0].canchildreturntohome ;
        data.emotionalDisturbance = this.childassessmentData.adoptionApplicabilityInfo[0].emotionaldisturbance;
        data.childscurrentIvEfostercareeligibilitystatus = this.childassessmentData.adoptionApplicabilityInfo[0].childscurrentivefostercareeligibilitystatus;
        data.removalCourtorderdate = this.childassessmentData.adoptionApplicabilityInfo[0].removalcourtorderdate;
        data.previousAdoptiveParentsTpr = this.childassessmentData.adoptionApplicabilityInfo[0].previousadoptiveparentstpr;
        data.childMeetAllMedicalDisabilityRequirementsforSsi = this.childassessmentData.adoptionApplicabilityInfo[0].childmeetallmedicaldisabilityrequirementsforssi;
        data.hasthechildbeenincare60Monthsormore = this.childassessmentData.adoptionApplicabilityInfo[0].hasthechildbeenincare60monthsormore;
        data.childmeetsSsImedicaldisabledeligliblerequirements = this.childassessmentData.adoptionApplicabilityInfo[0].childmeetsssimedicaldisabledeligliblerequirements;
        data.physicaladdressofchild = this.childassessmentData.adoptionApplicabilityInfo[0].physicaladdressofchild;
        data.voluntaryRelinquishment = this.childassessmentData.adoptionApplicabilityInfo[0].voluntaryrelinquishment;
        data.adoptiveParentsTprDate = this.childassessmentData.adoptionApplicabilityInfo[0].adoptiveparentstprdate;
        data.physicalMentalEmotionalDisability = this.childassessmentData.adoptionApplicabilityInfo[0].physicalmentalemotionaldisability;
        data.childsSsIeligibilitystatus = this.childassessmentData.adoptionApplicabilityInfo[0].childsssieligibilitystatus;
        data.childsIvEstatusofpreviousadoption = this.childassessmentData.adoptionApplicabilityInfo[0].childsivestatusofpreviousadoption;
        data.startdateofreceivingSsi = this.childassessmentData.adoptionApplicabilityInfo[0].startdateofreceivingssi;
        data.expectedAdoptionDate = this.childassessmentData.adoptionApplicabilityInfo[0].expectedadoptiondate;
        data.childreceivingSsIatremoval = this.childassessmentData.adoptionApplicabilityInfo[0].childreceivingssiatremoval;
        data.childspreviouslyadopted = this.childassessmentData.adoptionApplicabilityInfo[0].childspreviouslyadopted;

        data.caseworkerName =  this.childassessmentData.adoptionApplicabilityInfo[0].caseworkername;
        data.submissionDate =  this.childassessmentData.adoptionApplicabilityInfo[0].submissiondate;
        data.caseworkerSignature =  this.childassessmentData.adoptionApplicabilityInfo[0].caseworkersignature;
        data.resubmissionCaseworkerName =  this.childassessmentData.adoptionApplicabilityInfo[0].resubmissioncaseworkername;
        data.resubmissionDate =  this.childassessmentData.adoptionApplicabilityInfo[0].resubmissiondate;
        data.resubmissionCaseworkerSignature =  this.childassessmentData.adoptionApplicabilityInfo[0].resubmissioncaseworkersignature;
        data.resubmissionCount =  this.childassessmentData.adoptionApplicabilityInfo[0].resubmissioncount;

        data.childAgeTable1618 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable1618;
        data.childAgeTable418 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable418;
        data.childAgeTable1418 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable1418;
        data.childAgeTable218 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable218;
        data.childAgeTable1218 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable1218;
        data.childAgeTable018 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable018;
        data.childAgeTable1018 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable1018;
        data.childAgeTable218dublicate = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable218dublicate;
        data.childAgeTable818 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable818;
        data.childAgeTable018dublicate = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable018dublicate;
        data.childAgeTable618 = this.childassessmentData.adoptionApplicabilityInfo[0].childagetable618;
        data.childAgeTableChildagenoneoftheagesapply = this.childassessmentData.adoptionApplicabilityInfo[0].childagetablechildagenoneoftheagesapply;
        data.Isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage = this.childassessmentData.adoptionApplicabilityInfo[0].isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage;
        data.aCourtOrder = this.childassessmentData.adoptionApplicabilityInfo[0].acourtorder;
        data.dateoffirstcourtorderwithCtw = this.childassessmentData.adoptionApplicabilityInfo[0].dateoffirstcourtorderwithctw;
        data.BAvoluntaryplacementagreement = this.childassessmentData.adoptionApplicabilityInfo[0].bavoluntaryplacementagreement;
        //dateoffirstcourtorderwithbestinterestorCtWfindingifconvertedtoCina = 
        data.dateofRelinquishment = this.childassessmentData.adoptionApplicabilityInfo[0].dateofrelinquishment;
        data.isthechildresidinginafosterfamilyhome = this.childassessmentData.adoptionApplicabilityInfo[0].isthechildresidinginafosterfamilyhome;
        data.physicaladdressofchild = this.childassessmentData.adoptionApplicabilityInfo[0].physicaladdressofchild;
        data.child617Yearsofage = this.childassessmentData.adoptionApplicabilityInfo[0].child617yearsofage;
        data.physicalMentalEmotionalDisability = this.childassessmentData.adoptionApplicabilityInfo[0].physicalmentalemotionaldisability;
        data.emotionalDisturbance = this.childassessmentData.adoptionApplicabilityInfo[0].emotionaldisturbance;
        data.siblingInformationCheck = this.childassessmentData.adoptionApplicabilityInfo[0].siblinginformationcheck;
        data.recognizedhighriskofphysicaldisability = this.childassessmentData.adoptionApplicabilityInfo[0].recognizedhighriskofphysicaldisability;
        data.raceEthnicityofchild = this.childassessmentData.adoptionApplicabilityInfo[0].raceethnicityofchild;
        data.unsuccessfulreasonableeffortsstatusrecords = this.childassessmentData.adoptionApplicabilityInfo[0].unsuccessfulreasonableeffortsstatusrecords;
        data.unsuccessfulreasonableeffortsstatusrecordsdescription = this.childassessmentData.adoptionApplicabilityInfo[0].unsuccessfulreasonableeffortsstatusrecordsdescription;
        data.fosterparentemotionalbonding = this.childassessmentData.adoptionApplicabilityInfo[0].fosterparentemotionalbonding;
        data.fosterparentemotionalbondingdescription = this.childassessmentData.adoptionApplicabilityInfo[0].fosterparentemotionalbondingdescription;
      } 


        if(data.resubmissionCount === null || data.resubmissionCount === undefined || data.resubmissionCount === ''){
          data.resubmissionCount = 0;
          if((data.caseworkerName === undefined || data.caseworkerName === null || data.caseworkerName === '') && 
            this.userInfo.user.userprofile.title === 'CWCW'){
            data.caseworkerName = this.userInfo.user.userprofile.firstname + ' ' +this.userInfo.user.userprofile.lastname;
          }
        }else if(data.resubmissionCount > 0){
            if((data.resubmissionCaseworkerName === undefined || data.resubmissionCaseworkerName === null || data.resubmissionCaseworkerName === '') && 
                  this.userInfo.user.userprofile.title === 'CWCW'){
              data.resubmissionCaseworkerName = this.userInfo.user.userprofile.firstname + ' ' +this.userInfo.user.userprofile.lastname;
            }
        }

      data.agency = this.agency;
      return data;
    } 

  submissionData(data) {
    interface LooseObject {
      [key: string]: any;
    }

    let gender = '';
    if (data.gender === 'M') {
      gender = 'Male';
    } else if (data.gender === 'F') {
      gender = 'Female';
    } else {
      gender = 'Other';
    }

 

    let minorParentDetails = [];
    if(data.minorParentInformation){
      data.minorParentInformation.forEach((element, index) => {
        minorParentDetails.push({
              "MinorParentRemovalDate":  this.dateConversion(element.minorParentInformationRemovalDateVpaRemovalDateofminorparent),
              "MinorParentName": element.minorParentInformationMinorParentName === "" ? null : element.minorParentInformationMinorParentName,
              "MinorParentCurrentPlacementType": element.minorParentInformationMinorParentscurrentplacementType === "" ? null : element.minorParentInformationMinorParentscurrentplacementType,
              "MinorParentDateOfBirth": this.dateConversion(element.minorParentInformationBirthdateofparent),
              "MinorParentRemovalCourtOrderDate": this.dateConversion(element.minorParentInformationRemovalcourtorderdateofminorparent),
              "__metadata": {
                  "#type": "ParentDetails",
                  "#id": "ParentDetails_id_"+index
              },
              "MinorParentPhysicalAddress": element.minorParentInformationPhysicaladdressofminorparent
          });
        });
    };
    
    
    let siblingDetails = [];
    if(data.siblingStatus){
      data.siblingStatus.forEach((element, index) => {
        siblingDetails.push({
              "SiblingAdoptionDecreeDate":  this.dateConversion(element.siblingStatusColumnsdateofsiblingsadoptiondecree),
              "SiblingApplicableChildAssessmentDate":  this.dateConversion(element.siblingStatusColumnsdateofsiblingsapplicablechildassessment),
              "SiblingAdoptionApplicable": element.siblingStatusColumnsChildssiblingsapplicabilitystatus === "" ? null : element.siblingStatusColumnsChildssiblingsapplicabilitystatus,
              "SiblingAdoptiveProviderID": element.expectedchildadoptiveplacementProviderId === "" ? null : element.expectedchildadoptiveplacementProviderId,
              "SiblingName": element.siblingStatusColumnsNameofsiblingchild,
              "__metadata": {
                  "#type": "SiblingDetails",
                  "#id": "SiblingDetails_id_"+index
              }
          });
        });
    };

    const submissiondata: LooseObject = {
      'cjamsPid': Number(this.client_id),
      'removalid': this.removalid,
      'pagesnapshot': data,
      'applicability': {
        'payload': {
        "name": "AdoptionApplicability",
        "__metadataRoot": {},
        "Objects": [{
            "person": {
                "ChildHasFosterParentEmotionalTies": data.fosterparentemotionalbonding === "" ? null :  data.fosterparentemotionalbonding,
                "ChildAdoptionUnsuccessfulEffortsToPlace": data.unsuccessfulreasonableeffortsstatusrecords === "" ? null :  data.unsuccessfulreasonableeffortsstatusrecords,
                "ChildRaceEthnicity": data.raceEthnicityofchild ? 'YES' : 'NO',
                "ChildNotReturnHomeExplanation": data.descriptionofreturnhome === "" ? null :  data.descriptionofreturnhome,
                "parentDetails": minorParentDetails,
                "ChildHasHighRiskOfDisability": data.recognizedhighriskofphysicaldisability  ? 'YES' : 'NO',
                "Child_Removal_Date": this.dateConversion(data.childRemovalDate),
                "ChildCanReturnToHome": data.canchildreturntohome === "" ? null :  data.canchildreturntohome,
                "ChildExpectedAdoptiveProviderID": "",
                "ChildHasEmotionalDisturbance": data.emotionalDisturbance ? 'YES' :  'NO',
                "ChildCurrentIVEFosterCareEligibilityStatus": data.childscurrentIvEfostercareeligibilitystatus === "" ? null :  (data.childscurrentIvEfostercareeligibilitystatus === 'YES' ? 'Eligible' : 'InEligible'),
                "__metadata": {
                    "#type": "Person",
                    "#id": "Person_id_1"
                },
                "DateOfBirth": this.dateConversion(data.childBirthDate),
                "Removal_Court_Order_Date": this.dateConversion(data.removalCourtorderdate),
                "ChildPreviousAdoptiveParentTPRDate": this.dateConversion(data.previousAdoptiveParentsTpr),
                "ChildMeetsSSIMedicalDisabilityReqts": data.childMeetAllMedicalDisabilityRequirementsforSsi === "" ? null :  data.childMeetAllMedicalDisabilityRequirementsforSsi,
                "ChildHasBeenInCare60Months": data.hasthechildbeenincare60Monthsormore === "" ? null :  data.hasthechildbeenincare60Monthsormore,
                "ChildMeetsSSIMedicalDisabledEligReqts": data.childmeetsSsImedicaldisabledeligliblerequirements === "" ? null :  data.childmeetsSsImedicaldisabledeligliblerequirements,
                "ChildPhysicalAddress": data.physicaladdressofchild === "" ? null :  data.physicaladdressofchild,
                "VoluntaryRelinquishment": data.voluntaryRelinquishment === "" ? null :  data.voluntaryRelinquishment,
                "ChildPreviousAdoptiveParentDeathDate": this.dateConversion(data.adoptiveParentsTprDate),
                "ChildHasPhysicalMentalEmotionalDisability": data.physicalMentalEmotionalDisability ? 'YES' :  'NO',
                "ChildIsSSIEligible": data.childsSsIeligibilitystatus === "" ? null :  data.childsSsIeligibilitystatus,
                "siblingDetails": siblingDetails,
                "ChildPreviousAdoptionIVEStatus": data.childsIvEstatusofpreviousadoption ?   (data.childsIvEstatusofpreviousadoption === '' ? 'InEligible' : data.childsIvEstatusofpreviousadoption ) : null,
                "StartDateOfReceivingSSI": this.dateConversion(data.startdateofreceivingSsi),
                "ChildExpectedAdoptionDate": this.dateConversion(data.expectedAdoptionDate),
                "ChildReceivingSSIAtRemoval": data.childreceivingSsIatremoval === "" ? null :  data.childreceivingSsIatremoval,
                "ChildPreviouslyAdopted": data.childspreviouslyadopted === "" ? null :  data.childspreviouslyadopted
            },
            "__metadata": {
                "#type": "Application",
                "#id": "Application_id_1"
            },
            "status": {
                "AdoptionApplicable": null,
                "Applicable_DoesTheChildMeetAnyOfTheChildStatusCriteriaOfSection_I_A1_2or3":  null,
                "HasChildBeenAssessedToNOTBeAnApplicableChild": null,
                "Applicable_DoesTheChildMeetTheSpecialNeedsCriteriaInSection_I_C1_2_AorB_AND_3_AorB":  null,
                "NonApplicable_DoesTheChildMeetPlacementOrMedicalCriteriaOfSection_I_B1_2or3":  null,
                "AdoptionNonApplicable": null,
                "Applicable_DoesTheChildMeetPlacementOrMedicalCriteriaOfSection_I_B1_2or3":  null,
                "__metadata": {
                    "#type": "Status",
                    "#id": "Status_id_1"
                },
                "AdoptionDataIncomplete": null,
                "ApplicableAndNonApplicable": null
            }
        }]
    }
    }
  };

    return submissiondata;
  }


  getPersonsList() {
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          nolimit: true,
          method: 'get',
          where: { servicecaseid: this.casenumber  }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      )
      .subscribe((result) => {
        this.persondetails = result;
        var persondetails = this.persondetails;
        if(persondetails && persondetails.data && persondetails.data.length > 0){
          persondetails.data.forEach((person, personIndex) => {
            if(this.client_id === person.cjamspid){
              this.personid = person.personid;
            };
          });
        }
        this.getInvolvedPersonWithPersonID(1,20,this.personid);
      });
  }


  getInvolvedPersonWithPersonID(page: number, limit: number,personid: string) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: page,
          limit: limit,
          method: 'get',
          where: {
            objectid: this.casenumber,
            objecttypekey: 'servicecase',
            personid: personid
          }
        }),
        'People/getallpersonrelationbyprovidedpersonid?filter'
      ).subscribe((result) => {
        if (result && result && Array.isArray(result)) {
          for (var i = 0; i < this.persondetails.data.length; i++) {
            var missing = false;
            if (result.length > 0) {
              for (var j = 0; j < result.length; j++) {
                if (this.persondetails.data[i].personid === result[j].person2id) {
                  this.persondetails.data[i].relation = result[j].relationshiptypekey;
                  missing = false;
                  break;
                } else {
                  missing = true;
                }
              }
              if (missing) {
                this.persondetails.data[i].relation = 'unknown'
              }

            } else {
              this.persondetails.data[i].relation = 'unknown';
            }
          }
        }
        this.relationship = result;
        this.startForm();
      });
  }


  dateConversion(date) {
    if (date === undefined || date === null || date === '') {
      return null;
    } else {
      return this.datePipe.transform(date, 'MM/dd/yyyy');
    }
  }

  dateTimeConversion(date){
    if(date === undefined || date === null || date === ''){
      return null;
    }else{
      return this.datePipe.transform(date,"MM/dd/yyyy HH:mm:ss");
    }
  }

  startForm() {
    const formDataToShow = this.setData();
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('titleIvEChildAssessmentForm'), environment.formBuilderHost + `/form/5c9b91c58d24c6329b180ae8`, {
      hooks: {
        beforeSubmit: (submission, next) => {
          this.submissionRequest = true;
          this.saveData();
          this.submissionRequest = false;
          var submissiondata =  this.submissionData(submission.data);
          this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe(
            (res) => {
              this._alertService.success("Submitted successfully!");
            },
            (error) => {
                this._alertService.error("Submission Failed");
            });
        }
      }
    }).then((form) => {
      form.submission = {
        data: formDataToShow
      };

      form.on('submit', submission => {
        
      });

      form.on('SaveToDB', submission => {
        this.saveToDB(submission);
      });

      form.on('change', formData => {
        if (formData.changed.component.key === 'aCourtOrder' && formData.changed.value === 'YES') {
          formData.data.BAvoluntaryplacementagreement = 'NO';
          formData.data.voluntaryRelinquishment = 'NO';
        }

        if (formData.changed.component.key === 'BAvoluntaryplacementagreement' && formData.changed.value === 'YES') {
          formData.data.aCourtOrder = 'NO';
          formData.data.voluntaryRelinquishment = 'NO';
        }

        if (formData.changed.component.key === 'voluntaryRelinquishment' && formData.changed.value === 'YES') {
          formData.data.BAvoluntaryplacementagreement = 'NO';
          formData.data.aCourtOrder = 'NO';
        }
        
        form.submission = {
          data: formData.data
        };
        this.formSubmissionData = formData.data;
      });
      form.on('render', formData => {
        console.log("form render >>> ", formData);
      });
      form.on('error', formData => {
        console.log("form error >>> ", formData);
      });
    }).catch((err) => {
      console.log("Form Error:: ", err);
    });
  }

  searchChildAssessmentId() {
    this._commonHttpService.getAll('titleive/adoption/adoption-eligibility-worksheet/' + this.client_id+'/'+this.removalid).subscribe(
      (response: any) => {
        this.childassessmentData = response;
        if(this.childassessmentData.adoptionEligibilityInfo && this.childassessmentData.adoptionEligibilityInfo.length> 0 ){
          this.casenumber = this.childassessmentData.adoptionEligibilityInfo[0].servicecaseid;
        }
        this.getPersonsList();
      },
      (error) => {
        console.log(error);
        return false;
      }
    );
  }



  

}
