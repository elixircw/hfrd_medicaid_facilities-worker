import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
//import { AlertService, CommonHttpService, SessionStorageService, DataStoreService } from '../../../../@core/services';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { TitleIveadoptionService } from '../title-iveadoption.service';
import { DatePipe } from '@angular/common';
declare var Formio: any;


@Component({
  selector: 'adoption-extended',
  templateUrl: './adoption-extended.component.html',
  styleUrls: ['./adoption-extended.component.scss'],
  providers: [DatePipe]
})
export class AdoptionExtendedComponent implements OnInit {

  @Input() adoptionData: any;
  @Output() submitForReview: EventEmitter<any> = new EventEmitter();
  assessmentFormData: any;
  ChildStatusInfo: FormGroup;
  PlacementAndMedicalInfo: FormGroup;
  SpecialNeedsOfChild: FormGroup;
  TitleIVeStatus: FormGroup;
  @Output() submitApplicability = new EventEmitter();
  adoptionDetails: any;
  client_id: any;
  childassessmentData: any;
  removalid: any;
  countyname: any;
  adoptionExtensionData: any;
  adoptionextensionForm:  FormGroup;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private router: Router,
    private storage: SessionStorageService,
    private adoptionService: TitleIveadoptionService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.createFormGroup();
    this.client_id = this.router.routerState.snapshot.url.split('/')[4];
    this.removalid= this.router.routerState.snapshot.url.split('/')[5];  
    this.searchAdoptionExtensionData();
    //this.isTabSwitched();
  }

  isTabSwitched(){
    $('.adoption a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("Extended")){
        this.searchAdoptionExtensionData();
      }
    });
  }
    
  
  
  createFormGroup(){
    this.adoptionextensionForm = this.fb.group({
      countyofjurisdiction: [''],
      nameofchild: [''],
      clientId: [''],
      dateofbirth: [''],
      gender: [''],
      nameofguardian: [''],
      clientIDofguardian: [''],
      dateofAdoptiondecree: [''],
      childMetContinueEligibilityCriteria: [''],
      isDocumentedPhysicalAndMentalDisability: [''],
      startdateofSecondaryeducationequivalentprogram: [''],
      nameofschoolprogram: [''],
      enrollmentStartdateofpostSecondaryorvocationaleducationinstitution: [''],
      nameofinstitution: [''],
      participationStartdateofprogrampromotetoemployment: [''],
      nameofprogramactivity: [''],
      startdateofemployment: [''],
      hourspermonthemployed: [''],
      nameofemployer: [''],
      disabilityType: [''],
      disabilityStartdate: [''],
      evaluationdocumentationdate: [''],
      clientIDofAdoptiveParent: [''],
      nameofAdoptiveParent: ['']
    });
  }

  setData() {        
          
    let gender = '';
    if (this.adoptionExtensionData.adoptionEligibilityInfo && this.adoptionExtensionData.adoptionEligibilityInfo.length> 0 && 
          this.adoptionExtensionData.adoptionEligibilityInfo[0].gender === 'M') {
      gender = 'Male';
    } else if (this.adoptionExtensionData.adoptionEligibilityInfo && this.adoptionExtensionData.adoptionEligibilityInfo.length> 0 && 
      this.adoptionExtensionData.adoptionEligibilityInfo[0].gender === 'F') {
      gender = 'Female';
    } else {
      gender = 'Other';
    }

    this.adoptionextensionForm.patchValue({
      countyofjurisdiction : this.countyname
    });
    if(this.adoptionExtensionData && this.adoptionExtensionData.adoptionEligibilityInfo && this.adoptionExtensionData.adoptionEligibilityInfo.length> 0 ){ 
      this.adoptionextensionForm.patchValue({
        countyofjurisdiction : this.adoptionExtensionData.adoptionEligibilityInfo[0].childjurisdiction, 
        nameofchild : this.adoptionExtensionData.adoptionEligibilityInfo[0].childname,
        clientId : this.adoptionExtensionData.adoptionEligibilityInfo[0].client_id,
        dateofbirth : this.adoptionExtensionData.adoptionEligibilityInfo[0].dateofbirth,   
        gender : gender,
        clientIDofAdoptiveParent  : this.adoptionExtensionData.adoptionEligibilityInfo[0].parent1providerid, 
        nameofAdoptiveParent : this.adoptionExtensionData.adoptionEligibilityInfo[0].parent1providername, 
      });
    } 

          if(this.adoptionExtensionData && this.adoptionExtensionData.demographicsInfo && this.adoptionExtensionData.demographicsInfo.length> 0 ){ 
            this.adoptionextensionForm.patchValue({
              dateofAdoptiondecree : this.adoptionExtensionData.demographicsInfo[0].adoptionfinalizationdate
            });
          }

    if(this.adoptionExtensionData && this.adoptionExtensionData.adoptionExtensionInfo && this.adoptionExtensionData.adoptionExtensionInfo.length> 0 ){ 
      this.adoptionextensionForm.patchValue({
        childMetContinueEligibilityCriteria:this.adoptionExtensionData.adoptionExtensionInfo[0].childmeetcontinueeligibilitycriteria === null ? 'NO' : this.adoptionExtensionData.adoptionExtensionInfo[0].childmeetcontinueeligibilitycriteria,
        nameofschoolprogram:this.adoptionExtensionData.adoptionExtensionInfo[0].nameofsecondaryeducationorequivalentprogram,
        startdateofSecondaryeducationequivalentprogram:this.adoptionExtensionData.adoptionExtensionInfo[0].startdateofsecondaryeducationorequivalentprogram,
        nameofinstitution:this.adoptionExtensionData.adoptionExtensionInfo[0].nameofpostsecondaryorvocationaleducation,
        enrollmentStartdateofpostSecondaryorvocationaleducationinstitution:this.adoptionExtensionData.adoptionExtensionInfo[0].startdateofpostsecondaryorvocationaleducation,
        nameofprogramactivity:this.adoptionExtensionData.adoptionExtensionInfo[0].nameofpromotebarrierstoemploymentprogram,
        participationStartdateofprogrampromotetoemployment:this.adoptionExtensionData.adoptionExtensionInfo[0].startdateofpromotebarrierstoemploymentprogram,
        nameofemployer:this.adoptionExtensionData.adoptionExtensionInfo[0].nameofemployer,
        startdateofemployment:this.adoptionExtensionData.adoptionExtensionInfo[0].startdateofemployment,
        hourspermonthemployed:this.adoptionExtensionData.adoptionExtensionInfo[0].hourspermonthemployed,
        disabilityType:this.adoptionExtensionData.adoptionExtensionInfo[0].childdisabilitytype,
        disabilityStartdate:this.adoptionExtensionData.adoptionExtensionInfo[0].childdisabilitystartdate,
        evaluationdocumentationdate:this.adoptionExtensionData.adoptionExtensionInfo[0].childdisabilityevaluationdocumentiondate
      });
    } 
  
  }

  startForm() {
    const formDataToShow = this.setData();
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('titleIvEAdoptionExtendedForm'), environment.formBuilderHost + `/form/5c9d0fe98d24c6329b180b2b`, {
      hooks: {
        beforeSubmit: (submission, next) => {
          var submissiondata =  this.submissionData();
          console.log('-------submit---------'+JSON.stringify(submissiondata));
          this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe(
            (res) => {
              this._alertService.success("Submitted successfully!");
            },
            (error) => {
                this._alertService.error("Submission Failed");
            });
          // Only call next when we are ready.
          // next();
        }
      }
    }).then((form) => {
      form.submission = {
          data: formDataToShow
      };

      form.on('submit', submission => {
        console.log("form submit >>> ", submission);
      });

      form.on('change', formData => {
        if (formData && formData.changed && formData.changed.value && this.childassessmentData) {
          if (formData.changed.value.clientInformation) {
          }

          if (formData.changed.value.eligibilityCriteria) {
          }


          if (formData.data.eligibilitycriterialist === 'YES') {
          }

          form.submission = {
            data: formData.data
          };
        }

      });
      form.on('render', formData => {
        console.log("form render >>> ", formData);
      });
      form.on('error', formData => {
        console.log("form error >>> ", formData);
      });
    }).catch((err) => {
      console.log("Form Error:: ", err);
    });
  }


  submissionData() {
    interface LooseObject {
      [key: string]: any;
    }

    const submissiondata: LooseObject = {
      'cjamsPid': Number(this.client_id),
      'removalid': this.removalid,
      'pagesnapshot': this.adoptionextensionForm.getRawValue(),
      'reDetermination': {
        'payload': {
          "name": "EXT_Adoption",
            "__metadataRoot": {},
            "Objects": [{
                "ChildMeetContinueEligibilityCriteria": this.adoptionextensionForm.value.childMetContinueEligibilityCriteria,
                "AdoptionFinalizationDate": this.dateTimeConversion(this.adoptionextensionForm.value.dateofAdoptiondecree),
                "person": {
                    "DateOfBirth": this.dateConversion(this.adoptionextensionForm.value.dateofbirth),
                    "ChildDisabilityEvaluationDocumentionDate": this.dateConversion(this.adoptionextensionForm.value.evaluationdocumentationdate),
                    "StartDateOfSecondaryEducationOrEquivalentProgram": this.dateConversion(this.adoptionextensionForm.value.startdateofSecondaryeducationequivalentprogram),
                    "NameOfpostSecondaryOrVocationalEducation": this.adoptionextensionForm.value.nameofschoolprogram === "" ? null : this.adoptionextensionForm.value.nameofschoolprogram,
                    "CountyOfJurisdiction_LDSS": this.adoptionextensionForm.value.countyofjurisdiction,
                    "Gender": this.adoptionextensionForm.value.gender,
                    "NameOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment": this.adoptionextensionForm.value.nameofprogramactivity === "" ? null : this.adoptionextensionForm.value.nameofprogramactivity,
                    "Name": this.adoptionextensionForm.value.nameofchild,
                    "ChildDisabilityType": this.adoptionextensionForm.value.disabilityType=== "" ? null : this.adoptionextensionForm.value.disabilityType,
                    "StartDateOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment": this.dateConversion(this.adoptionextensionForm.value.participationStartdateofprogrampromotetoemployment),
                    "HoursPerMonthEmployed": 0,//this.adoptionextensionForm.value.hourspermonthemployed,
                    "StartDateOfEmployment": this.dateConversion(this.adoptionextensionForm.value.startdateofemployment),
                    "__metadata": {
                        "#type": "Person",
                        "#id": "Person_id_1"
                    },
                    "ChildDisabilityStartDate": this.dateConversion(this.adoptionextensionForm.value.disabilityStartdate),
                    "StartDateOfpostSecondaryOrVocationalEducation": this.dateConversion(this.adoptionextensionForm.value.enrollmentStartdateofpostSecondaryorvocationaleducationinstitution),
                    "NameOfSecondaryEducationOrEquivalentProgram": this.adoptionextensionForm.value.nameofinstitution=== "" ? null : this.adoptionextensionForm.value.nameofinstitution,
                    "NameOfEmployer": this.adoptionextensionForm.value.nameofemployer=== "" ? null : this.adoptionextensionForm.value.nameofemployer,
                },
                "__metadata": {
                    "#type": "Application",
                    "#id": "Application_id_1"
                },
                "IsDocumentedPhysicalAndMentalDisability": this.adoptionextensionForm.value.isDocumentedPhysicalAndMentalDisability=== "" ? null : this.adoptionextensionForm.value.isDocumentedPhysicalAndMentalDisability,
                "status": {
                    "AdoptionAssistance": "Eligible Reimbursable",
                    "EXTAdoption": null,
                    "__metadata": {
                        "#type": "Status",
                        "#id": "Status_id_1"
                    }
                }
            }]
        }
    }
  };

  this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe(
    (res) => {
      this.submitForReview.emit();
      this._alertService.success("Submitted successfully!");
    },
    (error) => {
        this._alertService.error("Submission Failed");
    });
   
  }


  dateConversion(date) {
    if (date === undefined || date === null || date === '') {
      return null;
    } else {
      return this.datePipe.transform(date, 'MM/dd/yyyy');
    }
  }

  dateTimeConversion(date){
    if(date === undefined || date === null || date === ''){
      return null;
    }else{
      return this.datePipe.transform(date,"MM/dd/yyyy HH:mm:ss");
    }
  }

  searchAdoptionExtensionData(){
   if(this.adoptionData){
        this.adoptionExtensionData = this.adoptionData;
        // if(this.adoptionExtensionData && this.adoptionExtensionData.demographicsInfo && this.adoptionExtensionData.demographicsInfo.length > 0 
        //   && this.adoptionExtensionData.demographicsInfo[0].childjurisdiction){
        //     this.loadCounty(this.adoptionExtensionData.demographicsInfo[0].childjurisdiction)
        // }
        // else{
        //   this.startForm();
        // }
        this.setData();
      };
  }


  loadCounty(countyidinput) {
    this._commonHttpService.getArrayList(
      { where: { activeflag: '1', state: 'MD', countyid: countyidinput}, nolimit: true, method: 'get' },
      'admin/county' + '?filter'
    ).subscribe(res => {
      if(res && res.length > 0){
        this.countyname = res[0].countyname;
      }
      this.setData();
      //this.startForm();
    });
  }



}
