import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionEligibilityWorksheetComponent } from './adoption-eligibility-worksheet.component';

describe('AdoptionEligibilityWorksheetComponent', () => {
  let component: AdoptionEligibilityWorksheetComponent;
  let fixture: ComponentFixture<AdoptionEligibilityWorksheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionEligibilityWorksheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionEligibilityWorksheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
