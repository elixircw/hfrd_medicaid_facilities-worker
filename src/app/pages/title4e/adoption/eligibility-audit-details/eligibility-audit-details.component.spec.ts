import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibilityAuditDetailsComponent } from './eligibility-audit-details.component';

describe('EligibilityAuditDetailsComponent', () => {
  let component: EligibilityAuditDetailsComponent;
  let fixture: ComponentFixture<EligibilityAuditDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EligibilityAuditDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EligibilityAuditDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
