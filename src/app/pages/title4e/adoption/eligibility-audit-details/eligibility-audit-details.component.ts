import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Title4eService } from '../../services/title4e.service';
import { AuthService, DataStoreService, SessionStorageService} from '../../../../@core/services';
import { AppConstants } from '../../../../@core/common/constants';
import { environment } from '../../../../../environments/environment';
import { CASE_STORE_CONSTANTS } from '../../../case-worker/_entities/caseworker.data.constants';
declare var Formio: any;
@Component({
  selector: 'eligibility-audit-details',
  templateUrl: './eligibility-audit-details.component.html',
  styleUrls: ['./eligibility-audit-details.component.scss']
})
export class EligibilityAuditDetailsComponent implements OnInit {
  id: string;
  adoptionAuditList: any = [];
  client_id: String;
  removalid: any;
  approveRejectDisabled = true;
  approvalid: any;
  isSupervisor = false;
  isSpecialist = false;
  adoptionmessages: any;
  adoptionwarnings: any;
  eligibilityDetailsChilds = true;
  getUsersList: any[];
  userInfo: any;
  warningdate: any;
  selectedPerson: any;
  selectedItem: any;
  selectedItemSnapshot: any;
  showSnapshot: boolean = false;
  constructor(private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _authService: AuthService,
    private storage: SessionStorageService,
    private titleIVeService: Title4eService, private _datastore: DataStoreService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    this.isSpecialist = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SPECIALIST);
    this.userInfo = this._authService.getCurrentUser();

    if (this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SUPERVISOR) {
      this.isSupervisor = true;
    }

    if (this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SPECIALIST) {
      this.isSpecialist = true;
    }

    this.client_id = this.activatedRoute.snapshot.paramMap.get('clientid');
    this.removalid = this.activatedRoute.snapshot.paramMap.get('removalId');
    //this.eligibilityDetails();
    this.isTabSwitched();
  }

  isTabSwitched(){
    $('.intake-tabs a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("DETAILS")){
        this.adoptionAuditList = null;
        this.eligibilityDetails();
      }
    });
  }

  toggleChildren() {
    this.eligibilityDetailsChilds = !this.eligibilityDetailsChilds;
  }

  eligibilityDetails() {
    this.showSnapshot = false;
    this._commonHttpService
    .getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 10,
        where: { clientid: this.client_id }
      },
      'tb_ive_adoption_audit/list?filter'
    ).subscribe(res => {
        if (res && res.length > 0) {
            this.adoptionAuditList = res;
            if (this.adoptionAuditList && this.adoptionAuditList.length > 0) {
              if (this.adoptionAuditList[0].approvalstatus === 'PENDING') {
                this.approveRejectDisabled = false;
                this.approvalid = this.adoptionAuditList[0].approvalid;
              }
            }
        }
      });
  }

  showSupervisorList() {
    this.titleIVeService.getUsersList().subscribe(result => {
        this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    });
    (<any>$('#assign')).modal('show');
  }

  selectPerson(item) {
    this.selectedPerson = item;
  }

  sendForApproval() {
    if (this.selectedPerson) {
      this._commonHttpService.create(
        {
          'where': {
            'clientid': this.client_id,
            'removalid': this.removalid,
            'status': '{68}',
            'eventType': 'Adoption'
          },
            'page': 1,
            'limit' : 10
        },
        'titleive/ive/approval-status'
      ).subscribe(response => {
        if (response && response.data && response.data.length > 0) {
          this.routingUpdate(response);
        }
      },
        (error) => {
          this._alertService.error('Unable to process');
          return false;
        }
      );
    } else {
      this._alertService.error('Select a Supervisor');
    }
  }


  ApproveReject(status) {
    this._commonHttpService.create(
      {
        'where': {
          'approval_id': this.approvalid,
          'approval_status': status,
          'placement_type': 'Adoption'
        },
          'page': 1,
          'limit' : 10
      },
      'titleive/ive/ivespv-approval'
    ).subscribe(response => {
      if (response && response.data && response.data.length > 0) {
        this.approveRejectDisabled = true;
        this.approvalid = null;
        this.eligibilityDetails();
      }
    },
      (error) => {
        this._alertService.error('Unable to process');
        return false;
      }
    );

  }


  

  updateSnapshot({checked}){

    const createForm = (form: any) => {
      form.submission = {
        data: this.selectedItemSnapshot
      };

      form.on('submit', submission => {
      });

      form.on('change', formData => {
        form.submission = {
          data: formData.data
        };
      });

      form.on('render', formData => {
        console.log('form render >>> ', formData);
      });

      form.on('error', formData => {
        console.log('form error >>> ', formData);
      });
    };
    console.log('checked '+checked);
    if(checked){
      if(this.selectedItem && this.selectedItem.category === 'I'){
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('titleIvEAdoptionEligibilityNewFormView'), environment.formBuilderHost + `/form/5c9ce8218d24c6329b180b25`, {}).then(createForm).catch((err) => {
          console.log('Form Error:: ', err);
        });
      }

      if(this.selectedItem && this.selectedItem.category === 'R'){
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('titleIvEAdoptionExtendedFormView'), environment.formBuilderHost + `/form/5c9d0fe98d24c6329b180b2b`, {}).then(createForm).catch((err) => {
          console.log('Form Error:: ', err);
        });
      }
    } else {
      document.getElementById('titleIvEAdoptionEligibilityNewFormView').innerHTML = "";
      document.getElementById('titleIvEAdoptionExtendedFormView').innerHTML = "";
    }
  }


  adoptionerrors(item, logtype) {
    this.adoptionmessages = null;
    this.adoptionwarnings = null;
    this.selectedItem = item;
    this.showSnapshot = false;
    this._commonHttpService.getById(item.adoptionauditid, 'titleive/adoption/audit-messages').subscribe(
        (response: any) => {
          this.adoptionmessages = [];
          this.adoptionwarnings = [];
          this.warningdate = item.insertedon;
            if (response && response.data && response.data.length > 0) {
                this.adoptionmessages = response.data;
                for (const adoptionwarning of response.data) {
                    this.selectedItemSnapshot = adoptionwarning.pagesnapshot;
                    if (adoptionwarning.severity === 'Warning') {
                        this.adoptionwarnings.push(adoptionwarning);
                    }
                }
            }

            if (logtype === 'W') {
              this.adoptionmessages = null;
            }

            if (logtype === 'L') {
                this.adoptionwarnings = null;
            }

        },
        (error) => {
          console.log(error);
          return false;
        }
      );
}

  routingUpdate(sendForApprovalResponse) {
    this._commonHttpService.create(
      {
        'where': {
          'assignedtoid': this.selectedPerson.userid,
          'eventcode': 'ABLR',
          // "servicecaseid": sendForApprovalResponse.data[0].caseid,
          'status': 'SplReview',
          'notifymsg': 'Request for Review',
          'routeddescription': 'route',
          'comments': 'Request for Review',
          'adoptionbreakthelinkid': sendForApprovalResponse.data[0].sp_ive_status_approval,
          'signText': 'Signed',
          servicecaseid: this.id
        }
      },
      'titleive/ive/routingUpdate'
    ).subscribe(response => {
      this.eligibilityDetails();
      (<any>$('#assign')).modal('hide');
      this._alertService.success('Sent For Approval');
    },
      (error) => {
        this._alertService.error('Unable To Process');
        return false;
      }
    );


  }


}
