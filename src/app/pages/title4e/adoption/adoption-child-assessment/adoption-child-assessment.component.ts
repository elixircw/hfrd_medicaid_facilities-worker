import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import * as _ from 'lodash';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { DatePipe } from '@angular/common';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../@core/services/auth.service';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { AppConstants } from '../../../../@core/common/constants';
import { DataStoreService } from '../../../../@core/services';

@Component({
  selector: 'adoption-child-assessment',
  templateUrl: './adoption-child-assessment.component.html',
  styleUrls: ['./adoption-child-assessment.component.scss'],
  providers: [DatePipe]
})
export class AdoptionChildAssessmentComponent implements OnInit {
  @Output() applicabilityDecision: EventEmitter<any> = new EventEmitter();
  adoptionChildAssessmentForm: FormGroup;
  @Input() clientIDInput: any;
  @Input() removalIDInput: any;
  @Input() adoptionData: any;
  adoptionLogData: any;
  adoptionmessages: any;
  adoptionwarnings: any;
  warningdate: any;
  client_id: any;
  removalid: any;
  personid: any;
  ivestatus: any;
  agency = '';
  userInfo: AppUser;
  childassessmentData: any;
  casenumber: any;
  persondetails: any;
  relationship: any;
  isCaseWorker = false;
  isSupervisor = false;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private commonHttpService: CommonHttpService,
    private alertService: AlertService,
    private _authService: AuthService,
    private datePipe: DatePipe,
    private _dataStore: DataStoreService
  ) { }

  ngOnInit() {
    this.createFormGroup();
    this.agency = this._authService.getAgencyName();
    this.userInfo = this._authService.getCurrentUser();
    this.client_id= this._dataStore.getData('adoption_clientid');
    this.removalid= this._dataStore.getData('adoption_removalid');
    this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    console.log('isCaseWorker - '+this.isCaseWorker);
    console.log('isSupervisor - '+this.isSupervisor);
    if(this.adoptionData === undefined || this.adoptionData === null){
      this.getEligibInfo();  
    }else{
      this.getAdoptionEligibilityInfo();
    }  
  }


  getEligibInfo() {
    this.commonHttpService.getAll(
        'titleive/adoption/adoption-eligibility-worksheet/' + this.client_id+'/'+this.removalid
        ).subscribe((response: any) => {
          if (response && response.adoptionApplicabilityInfo && response.adoptionApplicabilityInfo.length)
            this._dataStore.setData('hasAdoptionApplicabilityInfo', true);
          else
            this._dataStore.setData('hasAdoptionApplicabilityInfo', false);
          this.adoptionData = response;
          this.getAdoptionEligibilityInfo();
        });
    }
  
  createFormGroup(){
    this.adoptionChildAssessmentForm = this.formBuilder.group({
      childbirthdate: [null],
      ivestatus: [null],
      ivecomment: [''],
      expectedadoptiondate: [null],
      hasthechildbeenincare60monthsormore: [null],
      isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage: [null],
      acourtorder: [null],
      removalcourtorderdate: [null],
      dateoffirstcourtorderwithctw: [null],
      bavoluntaryplacementagreement: [null],
      childremovaldate: [null],
      dateoffirstcourtorderwithbestinterestorctwfindingifconvertedtocina: [null],
      voluntaryrelinquishment: [null],
      dateofrelinquishment: [null],
      childmeetsssimedicaldisabledeligliblerequirements: [null],
      childreceivingssiatremoval: [null],
      startdateofreceivingssi: [null],
      isthechildresidinginafosterfamilyhome: [null],
      physicaladdressofchild: [null],
      canchildreturntohome: [null],
      descriptionofreturnhome: [null],
      childmeetallmedicaldisabilityrequirementsforssi: [null],
      child617yearsofage: [false],
      physicalmentalemotionaldisability: [false],
      emotionaldisturbance: [false],
      siblinginformationcheck: [false],
      recognizedhighriskofphysicaldisability: [false],
      raceethnicityofchild: [false],
      raceorethnicitycanonlybeincombinationwithoneofthefactorsabove15: [null],
      unsuccessfulreasonableeffortsstatusrecords: [null],
      unsuccessfulreasonableeffortsstatusrecordsdescription: [null],
      fosterparentemotionalbonding: [null],
      fosterparentemotionalbondingdescription: [null],
      childspreviouslyadopted: [null],
      childsivestatusofpreviousadoption: [null],
      previousadoptiveparentstpr: [null],
      adoptiveparentstprdate: [null],
      childscurrentivefostercareeligibilitystatus: [null],
      childsssieligibilitystatus: [null],
      caseworkername: [null],
      submissiondate: [null],
      resubmissioncaseworkername: [null],
      resubmissiondate: [null],
      childAgeTable1618: [false],
      childAgeTable418: [false],
      childAgeTable1418: [false],
      childAgeTable218: [false],
      childAgeTable1218: [false],
      childAgeTable018: [false],
      childAgeTable1018: [false],
      childAgeTable218dublicate: [false],
      childAgeTable818: [false],
      childAgeTable018dublicate: [false],
      childAgeTable618: [false],
      resubmissioncount: [null],
      childAgeTableChildagenoneoftheagesapply: [false],
      adoptionapplicabilitysiblinginfo: this.formBuilder.array([]),
      membersiblinginfo: this.formBuilder.array([]),
      adoptionapplicabilityminorparentinfo: this.formBuilder.array([]),
      caseworkersignature: [null],
      resubmissioncaseworkersignature: [null]
    });
  }


  
  saveData(){
    this.saveToDB(this.adoptionChildAssessmentForm.value);
  }
  
  saveToDB(submission){
    
        //var submissionData = this.submissionData(submission);

    //  if(this.submissionRequest){
      if(submission.resubmissioncount === 0){
        submission.submissiondate = new Date();
      }else if(submission.resubmissioncount > 0 && this.isCaseWorker){
        submission.resubmissioncaseworkername = this.userInfo.user.userprofile.firstname + ' ' +this.userInfo.user.userprofile.lastname;
        submission.resubmissiondate = new Date();
      }
      submission.resubmissioncount = Number(submission.resubmissioncount) + 1;
    //  }   
     

      const finalsubmission = {
        childBirthDate : submission.childbirthdate,
        ivestatus: this.ivestatus === null ? 'PENDING' : this.ivestatus,
        fosterparentemotionalbonding : submission.fosterparentemotionalbonding,
        unsuccessfulreasonableeffortsstatusrecords : submission.unsuccessfulreasonableeffortsstatusrecords,
        raceEthnicityofchild : submission.raceethnicityofchild,
        descriptionofreturnhome : submission.descriptionofreturnhome,
        recognizedhighriskofphysicaldisability : submission.recognizedhighriskofphysicaldisability,
        childRemovalDate : submission.childremovaldate,
        canchildreturntohome : submission.canchildreturntohome ,
        emotionalDisturbance : submission.emotionaldisturbance,
        childscurrentIvEfostercareeligibilitystatus : submission.childscurrentivefostercareeligibilitystatus,
        removalCourtorderdate : submission.removalcourtorderdate,
        previousAdoptiveParentsTpr : submission.previousadoptiveparentstpr,
        childMeetAllMedicalDisabilityRequirementsforSsi : submission.childmeetallmedicaldisabilityrequirementsforssi,
        hasthechildbeenincare60Monthsormore : submission.hasthechildbeenincare60monthsormore,
        childmeetsSsImedicaldisabledeligliblerequirements : submission.childmeetsssimedicaldisabledeligliblerequirements,
        physicaladdressofchild : submission.physicaladdressofchild,
        voluntaryRelinquishment : submission.voluntaryrelinquishment,
        adoptiveParentsTprDate : submission.adoptiveparentstprdate,
        physicalMentalEmotionalDisability : submission.physicalmentalemotionaldisability,
        childsSsIeligibilitystatus : submission.childsssieligibilitystatus,
        childsIvEstatusofpreviousadoption : submission.childsivestatusofpreviousadoption,
        startdateofreceivingSsi : submission.startdateofreceivingssi,
        expectedAdoptionDate : submission.expectedadoptiondate,
        childreceivingSsIatremoval : submission.childreceivingssiatremoval,
        childspreviouslyadopted : submission.childspreviouslyadopted,
        ivecomment : submission.ivecomment,

        caseworkerName :  submission.caseworkername,
        submissionDate :  submission.submissiondate,
        caseworkerSignature :  submission.caseworkersignature,
        resubmissionCaseworkerName :  submission.resubmissioncaseworkername,
        resubmissionDate :  submission.resubmissiondate,
        resubmissionCaseworkerSignature :  submission.resubmissioncaseworkersignature,
        resubmissionCount :  submission.resubmissioncount,

        childAgeTable1618 : submission.childagetable1618,
        childAgeTable418 : submission.childagetable418,
        childAgeTable1418 : submission.childagetable1418,
        childAgeTable218 : submission.childagetable218,
        childAgeTable1218 : submission.childagetable1218,
        childAgeTable018 : submission.childagetable018,
        childAgeTable1018 : submission.childagetable1018,
        childAgeTable218dublicate : submission.childagetable218dublicate,
        childAgeTable818 : submission.childagetable818,
        childAgeTable018dublicate : submission.childagetable018dublicate,
        childAgeTable618 : submission.childagetable618,
        childAgeTableChildagenoneoftheagesapply : submission.childagetablechildagenoneoftheagesapply,
        Isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage : submission.isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage,
        aCourtOrder : submission.acourtorder,
        dateoffirstcourtorderwithCtw : submission.dateoffirstcourtorderwithctw,
        BAvoluntaryplacementagreement : submission.bavoluntaryplacementagreement,
        //dateoffirstcourtorderwithbestinterestorCtWfindingifconvertedtoCina : 
        dateofRelinquishment : submission.dateofrelinquishment,
        isthechildresidinginafosterfamilyhome : submission.isthechildresidinginafosterfamilyhome,
        child617Yearsofage : submission.child617yearsofage,
        siblingInformationCheck : submission.siblinginformationcheck,
        unsuccessfulreasonableeffortsstatusrecordsdescription : submission.unsuccessfulreasonableeffortsstatusrecordsdescription,
        fosterparentemotionalbondingdescription : submission.fosterparentemotionalbondingdescription
      };

      const idInfo = {
        'clientId': Number(this.client_id),
        'removalid': this.removalid
      };

      var request = {
          ...idInfo,
          ...finalsubmission
      };

      request.childBirthDate = request.childBirthDate === '' ? null : request.childBirthDate;
      request.expectedAdoptionDate = request.expectedAdoptionDate === '' ? null : request.expectedAdoptionDate;
      request.startdateofreceivingSsi = request.startdateofreceivingSsi === '' ? null : request.startdateofreceivingSsi;
      request.adoptiveParentsTprDate = request.adoptiveParentsTprDate === '' ? null : request.adoptiveParentsTprDate;

      // if(request.adoptionapplicabilitysiblinginfo && request.adoptionapplicabilitysiblinginfo.length > 0){
      //   request.adoptionapplicabilitysiblinginfo.forEach(element => {
      //     element.dateofsiblingsadoptiondecree = element.dateofsiblingsadoptiondecree === '' ? null : element.dateofsiblingsadoptiondecree;
      //     element.dateofsiblingsapplicablechildassessment = element.dateofsiblingsapplicablechildassessment === '' ? null : element.dateofsiblingsapplicablechildassessment;
      //   });
      // }

      // if(request.adoptionapplicabilityminorparentinfo && request.adoptionapplicabilityminorparentinfo.length > 0){
      //   request.adoptionapplicabilityminorparentinfo.forEach(element => {
      //     element.birthdateofparent = element.birthdateofparent === '' ? null : element.birthdateofparent;
      //     element.removalcourtorderdateofminorparent = element.removalcourtorderdateofminorparent === '' ? null : element.removalcourtorderdateofminorparent;
      //     element.removaldateofminorparent = element.removaldateofminorparent === '' ? null : element.removaldateofminorparent;
      //   });
      // }

      this.commonHttpService.create(request, Titile4eUrlConfig.EndPoint.postAdoptionApplicabilityToDB).subscribe(
        (res) => {
          (<any>$('#scwComment')).modal('hide');
          this.alertService.success("Submitted successfully!");
          this._dataStore.setData('hasAdoptionApplicabilityInfo', true);
        },
        (error) => {
            this.alertService.error("Submission Failed");
            this._dataStore.setData('hasAdoptionApplicabilityInfo', false);
        });
     
  }


  getAdoptionEligibilityInfo() {
    if(this.adoptionData){
      this.getLogMessages(this.adoptionData);
      this.childassessmentData = this.adoptionData;
      if(this.childassessmentData.adoptionEligibilityInfo && this.childassessmentData.adoptionEligibilityInfo.length> 0 ){
        this.casenumber = this.childassessmentData.adoptionEligibilityInfo[0].servicecaseid;
      }

      if(this.childassessmentData.demographicsInfo && this.childassessmentData.demographicsInfo.length> 0 ){
        this.adoptionChildAssessmentForm.patchValue({childbirthdate : this.childassessmentData.demographicsInfo[0].dateofbirth});
      }

      if(this.childassessmentData.adoptionEligibilityInfo && this.childassessmentData.adoptionEligibilityInfo.length> 0 ){
        this.casenumber = this.childassessmentData.adoptionEligibilityInfo[0].servicecaseid;
        //B. Placement and Medical Information  === Logic
        if(this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'JD'){
          this.adoptionChildAssessmentForm.patchValue(
            {
              acourtorder : 'YES',
              bavoluntaryplacementagreement : 'NO',
              voluntaryrelinquishment : 'NO',
              dateoffirstcourtorderwithctw : this.childassessmentData.adoptionEligibilityInfo[0].removalcourtorderdate,
              removalcourtorderdate : this.childassessmentData.adoptionEligibilityInfo[0].childremovaldate
            });
        }else if(this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'TLV' || 
                  this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'CDVP' || 
                  this.childassessmentData.adoptionEligibilityInfo[0].removaltypekey === 'EHA' ){
                    this.adoptionChildAssessmentForm.patchValue({
                       acourtorder : 'NO',
                       bavoluntaryplacementagreement : 'YES',
                       voluntaryrelinquishment : 'NO'
                      });
        }else{
          this.adoptionChildAssessmentForm.patchValue({
            acourtorder : 'NO',
            bavoluntaryplacementagreement : 'NO',
            voluntaryrelinquishment : 'YES'
           });
        }
        //B. Placement and Medical Information  === Logic
      }
      

        // if(this.userInfo.user.userprofile.title === 'CWCW'){
        //   this.getPersonsList();
        // }
          
         
        if(this.childassessmentData.adoptionApplicabilityInfo && this.childassessmentData.adoptionApplicabilityInfo.length)  {  
          this.adoptionChildAssessmentForm.patchValue(this.childassessmentData.adoptionApplicabilityInfo[0]);
          this.ivestatus = this.childassessmentData.adoptionApplicabilityInfo[0].ivestatus;
        }
        
        if(this.childassessmentData.adoptionEligibilityInfo && this.childassessmentData.adoptionEligibilityInfo.length> 0 ){
          this.adoptionChildAssessmentForm.patchValue({physicaladdressofchild : this.childassessmentData.adoptionEligibilityInfo[0].childphysicaladdress});
        }  
       
        if(this.childassessmentData.minorParentSiblingInfo && this.childassessmentData.minorParentSiblingInfo.length)  {  
            const adoptionapplicabilitysiblinginfocontrol = <FormArray>this.adoptionChildAssessmentForm.controls.adoptionapplicabilitysiblinginfo;
            const adoptionapplicabilityminorparentinfocontrol = <FormArray>this.adoptionChildAssessmentForm.controls.adoptionapplicabilityminorparentinfo;
            const membersiblinginfocontrol = <FormArray>this.adoptionChildAssessmentForm.controls.membersiblinginfo;
            if (this.childassessmentData.minorParentSiblingInfo[0] && _.isArray(this.childassessmentData.minorParentSiblingInfo[0].siblingsinfo)) {
                this.adoptionChildAssessmentForm.patchValue({isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage : 'YES'});
                    this.childassessmentData.minorParentSiblingInfo[0].siblingsinfo.forEach(element => {
                      adoptionapplicabilitysiblinginfocontrol.push(this.formBuilder.group({
                        nameofsiblingchild: [element.name],
                        nameofsiblingchildsadoptiveplacement: [element.provider_nm],
                        dateofsiblingsadoptiondecree: [element.decreedate],
                        dateofsiblingsapplicablechildassessment : [element.childapplicableassessmentdate],
                        childssiblingsapplicabilitystatus : [element.childapplicabilitystatus],
                        expectedchildadoptiveplacement : [element.altproviderid],
                        siblingsrelationshipwithchild : element.relationshiptypekey === 'BIOBR'?  'Biological Brother' : 'Biological Sister' ,
                      })
                    );

                    membersiblinginfocontrol.push(this.formBuilder.group({
                      nameofsiblingchild: [element.name],
                      siblingadoptionstatus : [element.childapplicabilitystatus],
                      providerid : [element.altproviderid]
                    })
                  );
                });
            }
            else{
              this.adoptionChildAssessmentForm.patchValue({isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage : 'NO'});
            }

            if (this.childassessmentData.minorParentSiblingInfo[0] && _.isArray(this.childassessmentData.minorParentSiblingInfo[0].minorparent)) {
              this.childassessmentData.minorParentSiblingInfo[0].minorparent.forEach(element => {
                adoptionapplicabilityminorparentinfocontrol.push(this.formBuilder.group({
                  minorparentname: [element.name],
                  birthdateofparent: [element.dob],
                  removaltypeofminorparent: [element.relationshiptypekey],
                  removalcourtorderdateofminorparent : [element.removaldate],
                  removaldateofminorparent : [element.vpastartdate],
                  minorparentscurrentplacementtype : [element.placementtypekey],
                  childscurrentplacementtype : [element.childscurrentplacementtype],
                  physicaladdressofminorparent: [element.address],
                })
              );
              });
            }
        }


        if(this.adoptionChildAssessmentForm.value.resubmissioncount === null || this.adoptionChildAssessmentForm.value.resubmissioncount === undefined || this.adoptionChildAssessmentForm.value.resubmissioncount === ''){
          this.adoptionChildAssessmentForm.patchValue({resubmissioncount : 0});
          if((this.adoptionChildAssessmentForm.value.caseworkername === undefined || this.adoptionChildAssessmentForm.value.caseworkername === null || this.adoptionChildAssessmentForm.value.caseworkername === '') && 
              this.isCaseWorker){
              this.adoptionChildAssessmentForm.patchValue({caseworkername : this.userInfo.user.userprofile.firstname + ' ' +this.userInfo.user.userprofile.lastname});
              this.adoptionChildAssessmentForm.patchValue({submissiondate : new Date() });
          }
        }else if(this.adoptionChildAssessmentForm.value.resubmissioncount > 0){
            if((this.adoptionChildAssessmentForm.value.resubmissioncaseworkername === undefined || this.adoptionChildAssessmentForm.value.resubmissioncaseworkername === null || this.adoptionChildAssessmentForm.value.resubmissioncaseworkername === '') && 
                this.isCaseWorker){
                    this.adoptionChildAssessmentForm.patchValue({resubmissioncaseworkername : this.userInfo.user.userprofile.firstname + ' ' +this.userInfo.user.userprofile.lastname});
                    this.adoptionChildAssessmentForm.patchValue({resubmissiondate : new Date() });
            }
        }

        

        this.adoptionChildAssessmentForm.patchValue({agency : this.agency});
  

        // else {
        //   const control = <FormArray>this.adoptionChildAssessmentForm.controls.adoptionapplicabilitysiblinginfo;
        //   control.push(this.createSiblingService());
        //   const control1 = <FormArray>this.adoptionChildAssessmentForm.controls.adoptionapplicabilityminorparentinfo;
        //   control.push(this.createMinorService());

        // }       
    }
              
       
}




getPersonsList() {
  this.commonHttpService
    .getPagedArrayList(
      new PaginationRequest({
        page: 1,
        nolimit: true,
        method: 'get',
        where: { servicecaseid: this.casenumber  }
      }),
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
    )
    .subscribe((result) => {
      this.persondetails = result;
      var persondetails = this.persondetails;
      if(persondetails && persondetails.data && persondetails.data.length > 0){
        persondetails.data.forEach((person, personIndex) => {
          if(this.client_id === person.cjamspid){
            this.personid = person.personid;
          };
        });
      }

      this.getInvolvedPersonWithPersonID(1,20,this.personid);
    });
}



getInvolvedPersonWithPersonID(page: number, limit: number,personid: string) {
  return this.commonHttpService
    .getPagedArrayList(
      new PaginationRequest({
        page: page,
        limit: limit,
        method: 'get',
        where: {
          objectid: this.casenumber,
          objecttypekey: 'servicecase',
          personid: personid
        }
      }),
      'People/getallpersonrelationbyprovidedpersonid?filter'
    ).subscribe((result) => {
      if (result && result && Array.isArray(result)) {
        for (var i = 0; i < this.persondetails.data.length; i++) {
          var missing = false;
          if (result.length > 0) {
            for (var j = 0; j < result.length; j++) {
              if (this.persondetails.data[i].personid === result[j].person2id) {
                this.persondetails.data[i].relation = result[j].relationshiptypekey;
                missing = false;
                break;
              } else {
                missing = true;
              }
            }
            if (missing) {
              this.persondetails.data[i].relation = 'unknown'
            }

          } else {
            this.persondetails.data[i].relation = 'unknown';
          }
        }
      }
      this.relationship = result;

      var persondetails = this.persondetails;
      if(persondetails && persondetails.data && persondetails.data.length > 0){
        this.adoptionChildAssessmentForm.patchValue({adoptionapplicabilityminorparentinfo : []});

        persondetails.data.forEach((person, personIndex) => {
          if(this.client_id === person.cjamspid){
            this.personid = person.personid;
          };
          if (person.roles) {
            person.roles.forEach((role, roleIndex) => {
              var date1 = new Date(person.dob);
              var date2 = new Date(this.adoptionChildAssessmentForm.value.childBirthDate);
              var diff = Math.abs(date1.getTime() - date2.getTime());
              var diffDays = Math.ceil(diff / (1000 * 3600 * 24)); 
              var years = Math.floor(diffDays/365); 
              if(person.gender === 'Female' && role.intakeservicerequestpersontypekey === 'PARENT' && years < 100){
                this.adoptionChildAssessmentForm.value.adoptionapplicabilityminorparentinfo.push({
                  minorparentname : person.fullname
                });
                this.adoptionChildAssessmentForm.patchValue({isthechildresidinginafosterfamilyhome : 'YES'});
              }
            });
          }
        
        });
      }


      //Sibling information
      if(this.relationship && this.relationship.length > 0){
        this.adoptionChildAssessmentForm.value.adoptionapplicabilitysiblinginfo = [];
        this.adoptionChildAssessmentForm.value.siblingInformation = [];
        let name = '';
        this.relationship.forEach((relationsp, index) => {
          if(relationsp.relationshiptypekey === 'BIOBR' || relationsp.relationshiptypekey === 'BGSISTR'){
            if(name !== (relationsp.firstname + ' ' + relationsp.lastname)){
              this.adoptionChildAssessmentForm.value.adoptionapplicabilitysiblinginfo.push({
                siblingsrelationshipwithchild : relationsp.relation,
                nameofsiblingchild: relationsp.firstname + ' ' + relationsp.lastname
              });
              //data.Isthechildasiblingtoachildwhoqualifiesasanapplicablechildbyage = 'YES';
              this.adoptionChildAssessmentForm.value.siblingInformationCheck = true;
              this.adoptionChildAssessmentForm.value.siblingInformation.push({
                siblingInformationSiblingname :  relationsp.firstname + ' ' + relationsp.lastname
              });
            }
            name = relationsp.firstname + ' ' + relationsp.lastname;
          }
        });
      }
      //this.startForm();
    });
}



  createSiblingService(): FormGroup {
    return this.formBuilder.group({
      nameofsiblingchild: [''],
      nameofsiblingchildsadoptiveplacement: [''],
      dateofsiblingsadoptiondecree: [''],
      dateofsiblingsapplicablechildassessment : [''],
      childssiblingsapplicabilitystatus : [''],
      expectedchildadoptiveplacement : [''],
      siblingsrelationshipwithchild : [''],
    });
  }
  createMinorService(): FormGroup {
    return this.formBuilder.group({
      minorparentname: [''],
      birthdateofparent: [''],
      removaltypeofminorparent: [''],
      removalcourtorderdateofminorparent : [''],
      removaldateofminorparent : [''],
      minorparentscurrentplacementtype : [''],
      childscurrentplacementtype : [''],
      physicaladdressofminorparent: [''],
    });
  }

  submitData() {
    this.ivestatus = 'DETERMINE';
    this.saveData();
    var submissiondata =  this.submissionData();
    this.commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe((response: any) => {
      this.applicabilityDecision.emit();
      this.alertService.success("Submitted successfully!");
      this.commonHttpService.getAll(
        'titleive/adoption/adoption-eligibility-worksheet/' +  this.client_id+'/'+ this.removalid
        ).subscribe(response => {
            this.getLogMessages(response);
        });
      },
      (error) => {
        console.log(error, 'in error');
        this.alertService.error("Submission Failed");
      }
      );
  }

  checklogs(){
    (<any>$('#checklogs')).modal('show');
  }

  checkwarnings(){
    (<any>$('#checkwarnings')).modal('show');
  }

  

  getLogMessages(response){
    this.adoptionLogData = response;
    console.log('log - '+JSON.stringify(this.adoptionLogData));
    if(this.adoptionLogData && this.adoptionLogData.adoptionApplicabilityauditInfo && this.adoptionLogData.adoptionApplicabilityauditInfo.length > 0){
      var auditInfo = this.adoptionLogData.adoptionApplicabilityauditInfo[0];
      this.adoptionmessages = [];
      this.adoptionwarnings = [];
      this.warningdate = auditInfo.insertedon;
      if(auditInfo.outputjson && auditInfo.outputjson.Messages && auditInfo.outputjson.Messages.Message && auditInfo.outputjson.Messages.Message.length>0){
        this.adoptionmessages = auditInfo.outputjson.Messages.Message;
        for (const adoptionwarning of auditInfo.outputjson.Messages.Message) {
            if (adoptionwarning.severity === 'Warning') {
                this.adoptionwarnings.push(adoptionwarning);
            }
        }
      }
    }
  }

  completeForm(){
    this.ivestatus = 'COMPLETED';
    this.saveData();
  }


  returnToWorker(){
    if(this.adoptionChildAssessmentForm.getRawValue().ivecomment && this.adoptionChildAssessmentForm.getRawValue().ivecomment != ''){
      this.ivestatus = 'RETURNED';
      this.saveData();
    }
    else
    this.alertService.error("Please add a comment to proceed");
  }

  submissionData() {
   var data = this.adoptionChildAssessmentForm.getRawValue();
    interface LooseObject {
      [key: string]: any;
    }

    let gender = '';
    if (data.gender === 'M') {
      gender = 'Male';
    } else if (data.gender === 'F') {
      gender = 'Female';
    } else {
      gender = 'Other';
    }

 

    let minorParentDetails = [];
    if(data.adoptionapplicabilityminorparentinfo){
      data.adoptionapplicabilityminorparentinfo.forEach((element, index) => {
        minorParentDetails.push({
              "MinorParentRemovalDate":  this.dateConversion(element.removaldateofminorparent),
              "MinorParentName": element.minorparentname === "" ? null : element.minorparentname,
              "MinorParentCurrentPlacementType": element.minorparentscurrentplacementtype === "" ? null : element.minorparentscurrentplacementtype,
              "MinorParentDateOfBirth": this.dateConversion(element.birthdateofparent),
              "MinorParentRemovalCourtOrderDate": this.dateConversion(element.removalcourtorderdateofminorparent),
              "__metadata": {
                  "#type": "ParentDetails",
                  "#id": "ParentDetails_id_"+index
              },
              "MinorParentPhysicalAddress": element.physicaladdressofminorparent
          });
        });
    };
    
    
    let siblingDetails = [];
    if(data.adoptionapplicabilitysiblinginfo){
      data.adoptionapplicabilitysiblinginfo.forEach((element, index) => {
        siblingDetails.push({
              "SiblingAdoptionDecreeDate":  this.dateConversion(element.dateofsiblingsadoptiondecree),
              "SiblingApplicableChildAssessmentDate":  this.dateConversion(element.siblingStatusColumnsdateofsiblingsapplicablechildassessment),
              "SiblingAdoptionApplicable": element.childssiblingsapplicabilitystatus === "" ? null : element.childssiblingsapplicabilitystatus,
              "SiblingAdoptiveProviderID": element.expectedchildadoptiveplacement === "" ? null : element.expectedchildadoptiveplacement,
              "SiblingName": element.nameofsiblingchild,
              "__metadata": {
                  "#type": "SiblingDetails",
                  "#id": "SiblingDetails_id_"+index
              }
          });
        });
    };

    const submissiondata: LooseObject = {
      'cjamsPid': Number(this.client_id),
      'removalid': this.removalid,
      'pagesnapshot': data,
      'applicability': {
        'payload': {
        "name": "AdoptionApplicability",
        "__metadataRoot": {},
        "Objects": [{
            "person": {
                "ChildHasFosterParentEmotionalTies": data.fosterparentemotionalbonding === "" ? null :  data.fosterparentemotionalbonding,
                "ChildAdoptionUnsuccessfulEffortsToPlace": data.unsuccessfulreasonableeffortsstatusrecords === "" ? null :  data.unsuccessfulreasonableeffortsstatusrecords,
                "ChildRaceEthnicity": data.raceethnicityofchild ? 'YES' : 'NO',
                "ChildNotReturnHomeExplanation": data.descriptionofreturnhome === "" ? null :  data.descriptionofreturnhome,
                "parentDetails": minorParentDetails,
                "ChildHasHighRiskOfDisability": data.recognizedhighriskofphysicaldisability  ? 'YES' : 'NO',
                "Child_Removal_Date": this.dateConversion(data.childremovaldate),
                "ChildCanReturnToHome": data.canchildreturntohome === "" ? null :  data.canchildreturntohome,
                "ChildExpectedAdoptiveProviderID": "",
                "ChildHasEmotionalDisturbance": data.emotionaldisturbance ? 'YES' :  'NO',
                "ChildCurrentIVEFosterCareEligibilityStatus": data.childscurrentivefostercareeligibilitystatus === "" ? null :  (data.childscurrentivefostercareeligibilitystatus === 'YES' ? 'Eligible Reimbursable' : 'Ineligible'),
                "__metadata": {
                    "#type": "Person",
                    "#id": "Person_id_1"
                },
                "DateOfBirth": this.dateConversion(data.childbirthdate),
                "Removal_Court_Order_Date": this.dateConversion(data.removalcourtorderdate),
                "ChildPreviousAdoptiveParentTPRDate": this.dateConversion(data.previousadoptiveparentstpr),
                "ChildMeetsSSIMedicalDisabilityReqts": data.childmeetallmedicaldisabilityrequirementsforssi === "" ? null :  data.childmeetallmedicaldisabilityrequirementsforssi,
                "ChildHasBeenInCare60Months": data.hasthechildbeenincare60monthsormore === "" ? null :  data.hasthechildbeenincare60monthsormore,
                "ChildMeetsSSIMedicalDisabledEligReqts": data.childmeetsssimedicaldisabledeligliblerequirements === "" ? null :  data.childmeetsssimedicaldisabledeligliblerequirements,
                "ChildPhysicalAddress": data.physicaladdressofchild === "" ? null :  data.physicaladdressofchild,
                "VoluntaryRelinquishment": data.voluntaryrelinquishment === "" ? null :  data.voluntaryrelinquishment,
                "ChildPreviousAdoptiveParentDeathDate": this.dateConversion(data.adoptiveparentstprdate),
                "ChildHasPhysicalMentalEmotionalDisability": data.physicalmentalemotionaldisability ? 'YES' :  'NO',
                "ChildIsSSIEligible": data.childsssieligibilitystatus === "" ? null :  data.childsssieligibilitystatus,
                "siblingDetails": siblingDetails,
                "ChildPreviousAdoptionIVEStatus": data.childsivestatusofpreviousadoption ?   (data.childsivestatusofpreviousadoption === '' ? 'Ineligible' : data.childsivestatusofpreviousadoption ) : null,
                "StartDateOfReceivingSSI": this.dateConversion(data.startdateofreceivingssi),
                "ChildExpectedAdoptionDate": this.dateConversion(data.expectedadoptiondate),
                "ChildReceivingSSIAtRemoval": data.childreceivingssiatremoval === "" ? 'NO' :  data.childreceivingssiatremoval,
                "ChildPreviouslyAdopted": data.childspreviouslyadopted === "" ? null :  data.childspreviouslyadopted
            },
            "__metadata": {
                "#type": "Application",
                "#id": "Application_id_1"
            },
            "status": {
                "AdoptionApplicable": null,
                "Applicable_DoesTheChildMeetAnyOfTheChildStatusCriteriaOfSection_I_A1_2or3":  null,
                "HasChildBeenAssessedToNOTBeAnApplicableChild": null,
                "Applicable_DoesTheChildMeetTheSpecialNeedsCriteriaInSection_I_C1_2_AorB_AND_3_AorB":  null,
                "NonApplicable_DoesTheChildMeetPlacementOrMedicalCriteriaOfSection_I_B1_2or3":  null,
                "AdoptionNonApplicable": null,
                "Applicable_DoesTheChildMeetPlacementOrMedicalCriteriaOfSection_I_B1_2or3":  null,
                "__metadata": {
                    "#type": "Status",
                    "#id": "Status_id_1"
                },
                "AdoptionDataIncomplete": null,
                "ApplicableAndNonApplicable": null
            }
        }]
    }
    }
  };

    return submissiondata;
  }

  dateConversion(date) {
    if (date === undefined || date === null || date === '') {
      return null;
    } else {
      return this.datePipe.transform(date, 'MM/dd/yyyy');
    }
  }

  openSCWPopup(){
    (<any>$('#scwComment')).modal('show');
  }
  
  closeSCWPopup(){
    (<any>$('#scwComment')).modal('hide');
  }
}
