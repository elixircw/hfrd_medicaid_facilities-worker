import { Location } from '@angular/common';
import * as moment from 'moment';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AlertService, AuthService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { HttpService } from '../../../../@core/services/http.service';
// import { InvolvedPerson } from '../../../provider-portal-temp/current-private-provider/_entities/newintakeModel';
// import { YouthInvolvedPersons } from '../../../case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
// import { RoutingInfo } from '../../../case-worker/dsds-action/kinship/_entities/kinship.data.models';
import { Placement } from '../../../case-worker/dsds-action/service-plan/_entities/service-plan.model';
import { ChildRoles } from '../../../case-worker/dsds-action/child-removal/_entities/childremoval.model';
// import { GetintakAssessment } from '../../../provider-management/new-public-provider/_entities/newintakeModel';
// import { DSDSActionSummary } from '../../../case-worker/_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { AssessmentPreFill } from '../../../case-worker/dsds-action/assessment/assessment-prefill';
import { DSDSActionSummary, GetintakAssessment, RoutingInfo } from '../../../case-worker/_entities/caseworker.data.model';
// import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
// import { ChildRoles } from '../../child-removal/_entities/childremoval.model';
import { InvolvedPerson, YouthInvolvedPersons } from '../../../case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { NgxfUploaderService } from 'ngxf-uploader';
import { HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../../../../app.config';
import { TitleIveadoptionService } from '../title-iveadoption.service';
import { SubscriptionLog } from 'rxjs/testing/SubscriptionLog';
import { Subscription } from 'rxjs/Subscription';
import { constants } from 'http2';
import { Title4eService } from '../../services/title4e.service';
import { AppConstants } from '../../../../@core/common/constants';
import { CASE_STORE_CONSTANTS } from '../../../case-worker/_entities/caseworker.data.constants';
// import { Placement } from '../service-plan/_entities/service-plan.model';
// import { AssessmentPreFill } from '../../assessment-prefill';

declare var Formio: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'adoption-eligibility-details',
  templateUrl: './adoption-eligibility-details.component.html',
  styleUrls: ['./adoption-eligibility-details.component.scss']
})
export class AdoptionEligibilityDetailsComponent implements OnInit, OnDestroy {
  id: string;
  roleId: AppUser;
  AdoptionApplicable: any;
  AdoptionIncomplete: any;
  AdoptionNonapplicability: any;
  AdoptionApplicability: any;
  @Input() adoptionData: any;
  signimage: any;
  isSignPresent: boolean;
  eligibility: any;
  adoptionSubscription: Subscription;
  fileToUpload: File = null;
  client_id: any;
  removalid: any;
  sendforcaseworker = true;
  isSupervisor = false;
  isSpecialist = false;
  userInfo: any;
  adoptionApplicabilityDecisionForm: FormGroup;
  getUsersList: any[];
  selectedPerson: any;
  constructor(
    private router: Router,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _uploadService: NgxfUploaderService,
    private _dataStoreService: DataStoreService,
    private titleIVeService: Title4eService,
    private adoptionService: TitleIveadoptionService
  ) { }
  ngOnInit() {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.roleId = this._authService.getCurrentUser();
    this.client_id = this.router.routerState.snapshot.url.split('/')[4];
    this.removalid = this.router.routerState.snapshot.url.split('/')[5];
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    this.isSpecialist = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SPECIALIST);
    this.userInfo = this._authService.getCurrentUser();
    if (this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SUPERVISOR) {
      this.isSupervisor = true;
    }
    if (this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SPECIALIST) {
      this.isSpecialist = true;
    }
    this.listenForEligibility();
    this.initializeForm();
    this.getAdoptionApplicabilityDecision();
    this.isTabSwitched();
  }

  isTabSwitched(){
    $('.intake-tabs a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("DECISION")){
        this.listenForEligibility();
        this.client_id= this.router.routerState.snapshot.url.split('/')[4];
        this.removalid= this.router.routerState.snapshot.url.split('/')[5];
        this._commonHttpService.getAll(
            'titleive/adoption/adoption-eligibility-worksheet/' +  this.client_id+'/'+ this.removalid
            ).subscribe(response => {
                if(response){
                  this.adoptionData = response;
                  this.initializeForm();
                  this.getAdoptionApplicabilityDecision();
                }
            });
      }
    });
  }

  initializeForm() {
    this.adoptionApplicabilityDecisionForm = this._formBuilder.group({
          appchildmeetchildstatuscriteriaofsectionia12or3 : null,
          appplacementormedicalcriteriaofsectionib12or3 : null,
          appthespecialneedscriteriainsecic12aorband3aorb : null,
          haschildbeenassessedtonotbeanappchild : null,
          nonappplacementormedicalcriteriaofsecib12or3 : null,
          nonapplsplneedscriteriainsecic12aorband3aorb : null,
          nonappltitleivestandardsofsecid1prioradptionaorbor2ivefcorssiao : null,
          applicableandnonapplicable : null,
          adoptiondataincomplete: null,
          adoptionapplicable: null,
          adoptionnonapplicable: null,
          neitheranappnornonappchildfortitleivepurposes : null,
          incompletespecalistname: null,
          incompletedate: null,
          incompletespecalistsignature: null,
          decisionsubmissionspecalistname: null,
          decisionsubmissiondate: null,
          decisionsubmissionspecalistsignature: null,
          decisionresubmissionspecalistname: null,
          decisionresubmissiondate: null,
          decisionresubmissionspecalistsignature: null
    });
  }


  getAdoptionApplicabilityDecision() {
    if(this.adoptionData){
      var response = this.adoptionData;
          if (response && response.adoptionApplicabilityauditInfo && response.adoptionApplicabilityauditInfo.length > 0) {
            this.adoptionApplicabilityDecisionForm.patchValue({
              incompletespecalistname: response.adoptionApplicabilityauditInfo[0].incompletespecalistname,
              incompletedate: response.adoptionApplicabilityauditInfo[0].incompletedate,
              incompletespecalistsignature: response.adoptionApplicabilityauditInfo[0].incompletespecalistsignature,
              decisionsubmissionspecalistname: response.adoptionApplicabilityauditInfo[0].decisionsubmissionspecalistname,
              decisionsubmissiondate: response.adoptionApplicabilityauditInfo[0].decisionsubmissiondate,
              decisionsubmissionspecalistsignature: response.adoptionApplicabilityauditInfo[0].decisionsubmissionspecalistsignature,
              decisionresubmissionspecalistname: response.adoptionApplicabilityauditInfo[0].decisionresubmissionspecalistname,
              decisionresubmissiondate: response.adoptionApplicabilityauditInfo[0].decisionresubmissiondate,
              decisionresubmissionspecalistsignature: response.adoptionApplicabilityauditInfo[0].decisionresubmissionspecalistsignature,
              appchildmeetchildstatuscriteriaofsectionia12or3 : response.adoptionApplicabilityauditInfo[0].appchildmeetchildstatuscriteriaofsectionia12or3,
              appplacementormedicalcriteriaofsectionib12or3 : response.adoptionApplicabilityauditInfo[0].appplacementormedicalcriteriaofsectionib12or3,
              appthespecialneedscriteriainsecic12aorband3aorb : response.adoptionApplicabilityauditInfo[0].appthespecialneedscriteriainsecic12aorband3aorb,
              haschildbeenassessedtonotbeanappchild : response.adoptionApplicabilityauditInfo[0].haschildbeenassessedtonotbeanappchild,
              nonappplacementormedicalcriteriaofsecib12or3 : response.adoptionApplicabilityauditInfo[0].nonappplacementormedicalcriteriaofsecib12or3,
              nonapplsplneedscriteriainsecic12aorband3aorb : response.adoptionApplicabilityauditInfo[0].nonapplsplneedscriteriainsecic12aorband3aorb,
              nonappltitleivestandardsofsecid1prioradptionaorbor2ivefcorssiao : response.adoptionApplicabilityauditInfo[0].nonappltitleivestandardsofsecid1prioradptionaorbor2ivefcorssiao,
              applicableandnonapplicable : response.adoptionApplicabilityauditInfo[0].applicableandnonapplicable === 'YES' ? true : false,
              adoptiondataincomplete: response.adoptionApplicabilityauditInfo[0].adoptiondataincomplete === 'YES' ? true : false,
              adoptionapplicable: response.adoptionApplicabilityauditInfo[0].adoptionapplicable === 'YES' && response.adoptionApplicabilityauditInfo[0].applicableandnonapplicable !== 'YES'  ?
               true : false,
              adoptionnonapplicable: response.adoptionApplicabilityauditInfo[0].adoptionnonapplicable === 'YES' && response.adoptionApplicabilityauditInfo[0].applicableandnonapplicable !== 'YES'  ?
               true : false,
              neitheranappnornonappchildfortitleivepurposes : response.adoptionApplicabilityauditInfo[0].neitheranappnornonappchildfortitleivepurposes === 'YES' ? true : false,
            });
            this.sendforcaseworker = response.adoptionApplicabilityauditInfo[0].adoptiondataincomplete === 'YES' ? true : false;
          }
      };
  }

  showSupervisorList() {
    this.titleIVeService.getUsersList().subscribe(result => {
        this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    });
    (<any>$('#assign')).modal('show');
  }

  sendToCaseWorker(){
    this._commonHttpService.create({ ivestatus: 'PENDING',
      clientId:this.client_id,
    removalId:this.removalid},
      'titleive/updatestatus'
    ).subscribe(res => {
      this._alertService.success('Sent To Case Worker');
    });
  }

  selectPerson(item) {
    this.selectedPerson = item;
  }

  sendToSupervisor() {
    // if (this.selectedPerson) {
    // this._commonHttpService.create(
    //   {
    //     'where': {
    //       'clientid': this.client_id,
    //       'removalid': this.removalid,
    //       'status': '{68}',
    //       'eventType': 'Adoption'
    //     },
    //       'page': 1,
    //       'limit' : 10
    //   },
    //   'titleive/ive/approval-status'
    // ).subscribe(response => {
    //   if (response && response.data && response.data.length > 0) {
    //     this.routingUpdate(response);
    //   }
    // },
    //   (error) => {
    //     this._alertService.error('Unable to process');
    //     return false;
    //   }
    // );
    // } else {
    //   this._alertService.error('Select a Supervisor');
    // }
    this._alertService.success('Sent To Supervisor');
    (<any>$('#assign')).modal('hide');
  }


  ApproveReject(status) {
    // this._commonHttpService.create(
    //   {
    //     'where': {
    //       //'approval_id': this.approvalid, //To do
    //       'approval_status': status,
    //       'placement_type': 'Adoption'
    //     },
    //       'page': 1,
    //       'limit' : 10
    //   },
    //   'titleive/ive/ivespv-approval'
    // ).subscribe(response => {
    //   if (response && response.data && response.data.length > 0) {

    //   }
    // },
    //   (error) => {
    //     this._alertService.error('Unable to process');
    //     return false;
    //   }
    // );


        this._commonHttpService.create({ ivestatus: status,
        clientId:this.client_id,
      removalId:this.removalid},
        'titleive/updatestatus'
      ).subscribe(res => {
        this._alertService.success('Sent To Case Worker');
      });

  }

  routingUpdate(sendForApprovalResponse) {
    this._commonHttpService.create(
      {
        'where': {
          'assignedtoid': this.selectedPerson.userid,
          'eventcode': 'ADAP',
          // "servicecaseid": sendForApprovalResponse.data[0].caseid,
          'status': 'SplReview',
          'notifymsg': 'Request for Review',
          'routeddescription': 'route',
          'comments': 'Request for Review',
          'adoptionbreakthelinkid': sendForApprovalResponse.data[0].sp_ive_status_approval,
          'signText': 'Signed',
          servicecaseid: this.id
        }
      },
      'titleive/ive/routingUpdate'
    ).subscribe(response => {
      (<any>$('#assign')).modal('hide');
      this._alertService.success('Sent For Approval');
    },
      (error) => {
        this._alertService.error('Unable To Process');
        return false;
      }
    );
  }


  listenForEligibility() {
    this.adoptionSubscription = this.adoptionService.adoptionSubject$.subscribe(data => {
      if (data === 'ELIGIBILITY_UPDATED') {
        this.eligibility = this.adoptionService.eligibility;
      }
    });
  }
  AdoptionApplicablee(e) {
    this.AdoptionApplicable = e;
  }
  AdoptionIncompletee(e) {
    this.AdoptionIncomplete = e;
  }
  AdoptionNonapplicabilitye(e) {
    this.AdoptionNonapplicability = e;
  }
  AdoptionApplicabilitye(e) {
    this.AdoptionApplicability = e;
  }
  handleFileInput(files: FileList) {
    const file = files.item(0);
    const reader = new FileReader();
    const self = this;
    reader.addEventListener('load', function () {
      self.signimage = reader.result;
      self._commonHttpService.updateSignature(self.signimage);
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }
  uploadAttachment() {
    //  console.log(this.signimage);
    const blob = this.dataURItoBlob(this.signimage);
    const finalImage = new File([blob], 'image.png');
    this._uploadService
      .upload({
        url:
          AppConfig.baseUrl +
          '/' +
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment
            .UploadAttachmentUrl +
          '?access_token=' +
          this.roleId.id +
          '&srno=userprofile',
        headers: new HttpHeaders()
          .set('access_token', this.roleId.id)
          .set('srno', 'userprofile')
          .set('ctype', 'file'),
        filesKey: ['file'],
        files: finalImage,
        process: true
      })
      .subscribe(
        response => {
          if (response && response.data && response.data.s3bucketpathname) {
            this._commonHttpService.create({
              usersignatureurl: response.data.s3bucketpathname
            }, 'admin/userprofile/updateusersignature').subscribe(res => {
              if (res.count) {
                this._alertService.success('Signature added successfully.');
                this.loadSignature();
              }
            });
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  loadSignature() {
    this._commonHttpService.getSingle({ method: 'get', securityusersid: this.roleId.user.securityusersid }, 'admin/userprofile/listusersignature?filter')
      .subscribe(res => {
        this.signimage = res.usersignatureurl;
        this.checkIfSignPresent();
      });
  }
  private checkIfSignPresent() {
    this.isSignPresent = Boolean(this.signimage);
    this._dataStoreService.setData('isSignPresent', this.isSignPresent);
  }

  dataURItoBlob(dataURI) {
    const binary = atob(dataURI.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/png'
    });
  }
  ngOnDestroy(): void {
    this.adoptionSubscription.unsubscribe();
  }
}

