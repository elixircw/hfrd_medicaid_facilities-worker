import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionSignatureFeildComponent } from './adoption-signature-feild.component';

describe('AdoptionSignatureFeildComponent', () => {
  let component: AdoptionSignatureFeildComponent;
  let fixture: ComponentFixture<AdoptionSignatureFeildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionSignatureFeildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionSignatureFeildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
