import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PeriodTablePopUpComponent } from '../../period-table-pop-up/period-table-pop-up.component';
declare var $: any;
import * as moment from 'moment';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { TitleIveadoptionService } from '../title-iveadoption.service';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { DatePipe } from '@angular/common';

declare var Formio: any;


@Component({
  selector: 'adoption-eligibility-newform',
  templateUrl: './adoption-eligibility-newform.component.html',
  styleUrls: ['./adoption-eligibility-newform.component.scss'],
  providers: [DatePipe]
})
export class AdoptionEligibilityNewformComponent implements OnInit {
  @Output() submitForReview: EventEmitter<any> = new EventEmitter();
  adoptioneligibilityForm: any;
  @Input() eligibilityData: any;
  @Input() adoptionData: any;
  generalRequirement: FormGroup;
  otherCriteria: FormGroup;
  adoptionExt: FormGroup;
  iveStatus: FormGroup;
  TbdFields: FormGroup;
  TitleIVeStatus: FormGroup;
  SpecialNeedsOfChild: FormGroup;
  ChildStatusInfo: FormGroup;
  PlacementAndMedicalInfo: FormGroup;
  AdoptionApplicable: any;
  AdoptionApplicability: any;
  AdoptionApplicableInn: any;
  RedeterminationStatus: any;
  AdoptionNonapplicability: any;
  AdoptionIncomplete: any;
  adoptionAssistance: any;
  adoptionnonApplicable: any;
  RedeteradoptionAssistance: any;
  RedetExtAdoption: any;
  eligibilityOneFormData: any;
  countyname: any;

  @Output() submitRedetermination = new EventEmitter();
  @Output() submitInitialdetermination = new EventEmitter();
  adoptionDetails: any;
  client_id: any;
  removalid: any;
  firstGenList = [];
  caseNumber: any;
  clientName: any;
  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private _commonHttpService: CommonHttpService,
      private _alertService: AlertService,
      public dialog: MatDialog,
      private fb: FormBuilder,
      private adoptionService: TitleIveadoptionService,
      private storage: SessionStorageService,
      private datePipe: DatePipe)
   { }
 
   ChildStatusInfoSubmit() {
    this.ChildStatusInfo.reset();
    }
    PlacementAndMedicalInfoSubmit() {
        this.PlacementAndMedicalInfo.reset();
    }
    SpecialNeedsOfChildSubmit() {
        this.SpecialNeedsOfChild.reset();
    }
    TitleIVeStatusSubmit() {
        this.TitleIVeStatus.reset();
    }
    ngOnInit() {
        this.createFormGroup();
        this.client_id = this.router.routerState.snapshot.url.split('/')[4];
        this.removalid= this.router.routerState.snapshot.url.split('/')[5]; 
        this.getAdoptionEligibilityInfo();
        //this.isTabSwitched();
    }
 

  createFormGroup(){
    this.adoptioneligibilityForm = this.formBuilder.group({
        countyofjurisdiction: [''],
        nameofchild: [''],
        clientId: [''],
        dateofbirth: [''],
        gender: [''],
        nameofAdoptiveParent: [''],
        clientIDofAdoptiveParent: [''],
        dateofAdoptionFinalization: [''],
        singleParentAdoptionCheck: [''],
        dateofsignatureofparent1OnAdoptionAgreement: [''],
        dateofsignatureofparent2OnAdoptionAgreement: [''],
        dateofLdssAgencySignatureonAdoptionAgreement: [''],
        adoptionPetitionFiledDate: [''],
        medicalconditionordisability: [''],
        child617Yearsofage: [''],
        physicalmentaloremotionaldisabilityordiseasemedicaldocumentationrequired: [''],
        emotionaldisturbancemedicaldocumentationrequired: [''],
        siblingInformationCheck: [''],
        siblinggroup: this.formBuilder.array([]),
        recognizedhighriskofphysicalormentaldisabilityordisease: [''],
        raceEthnicityofchild: [''],
        raceorethnicitycanonlybeincombinationwithoneofthefactorsabove15: [''],
        effortsToPlaceChildWereMade: [''],
        dateandtimeofdocumentationforEffortstoplacewithoutasubsidy: [''],
        exceptiongrantedinchildsbestinterests: [''],
        dateandtimeofdocumentationforExceptiongrantedinchildsbestinterests: [''],
        isReasonForExceptionRecorded: [''],
        childAdoptionUnsuccessfulEffortsToPlace: [''],
        tprGrantedtoBothParent: [''],
        dateofTpRofParent1: [''],
        dateofTpRofParent2: [''],
        ifnoReasonfornotgrantingTpRforbothparent: [''],
        childCanReturnToHome: [''],
        anotherreasonchildcouldnotreturntoparenthome: [''],
        isUsCitizen: [''],
        isQualifiedAlien: [''],
        age: [''],
        childsPreviouslyadopted: [''],
        childsIvEStatusofpreviousadoption: [''],
        previousAdoptiveParentsTpRterminationofparentrightDateIfterminated: [''],
        previousAdoptiveParentsDeathDateIfdead: [''],
        childsSsiEligibilityStatus: [''],
        startDateOfReceivingSsi: [''],
        parentName: [''],
        birthdateofparentAgeoftheparentatthetimeofapplicablechildassessment: [''],
        removalType: [''],
        removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt: [''],
        childRemovalDateVpaRemovalDateofminorparentwhenRemovalTypeMinorParentVpa: [''],
        childscurrentplacementtypeofminorparent: [''],
        physicaladdressofminorparent: [''],
        physicaladdressofchild: [''],
        minorParentIveEligibilityForFosterCareStatus: [''],
        dateOfLatestPaymentOfMinorParentIveEligibilityForFosterCare: [''],
        minorParentIveEligibilityForFosterCareStartDate: [''],
        afdcEligibilityMet: [''],
        wasTheChildRemovedFromSpecifiedRelative: [''],
        wasTheChildDeprivedOfParentalSupport: [''],
        wasIncomeAndAssetsMet: [''],
        childRemovalDate: [''],
        removalCourtOrderDate: [''],
        voluntaryRelinquishment: [''],
        childApplicableassessmentdateNotnullEmpty: [''],
        childApplicabilitystatusNotIncomplete: [''],
        dateofFinalizationAdoptionDateofDecree: [''],
        startDateofAdoptionassistance: ['']
       
    });
  }

  addSibling(){
    const siblinggroupArry = <FormArray>this.adoptioneligibilityForm.controls.siblinggroup;
    siblinggroupArry.push(this.formBuilder.group({
      siblinggroupsiblingname: '',
      siblinggroupsiblingadoptionstatus: '',
      siblinggroupnameofthesiblingadoptionplacementProviderId: '',
      siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingAdoptionDecreeDate: '',
      siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingApplicableChildAssessmentDate: ''
    })
    );
  }

  isTabSwitched(){
    $('.adoption a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("Eligibility")){
        this.getAdoptionEligibilityInfo();
      }
    });
  }

  submissionData() {
      interface LooseObject {
        [key: string]: any;
      }
  
     
      let siblingDetails = [];
      if(this.adoptioneligibilityForm.value.siblinggroup && this.adoptioneligibilityForm.value.siblinggroup.length > 0){
        this.adoptioneligibilityForm.value.siblinggroup.forEach((element, index) => {
          siblingDetails.push({
                "SiblingAdoptionDecreeDate":  this.dateConversion(element.siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingAdoptionDecreeDate),
                "SiblingApplicableChildAssessmentDate":  this.dateConversion(element.siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingApplicableChildAssessmentDate),
                "SiblingAdoptionApplicable": element.siblinggroupsiblingadoptionstatus === '' ? null : element.siblinggroupsiblingadoptionstatus,
                "SiblingAdoptiveProviderID": element.siblinggroupnameofthesiblingadoptionplacementProviderId,
                "SiblingName": element.siblinggroupsiblingname,
                "__metadata": {
                    "#type": "SiblingDetails",
                    "#id": "SiblingDetails_id_"+index
                }
            });
          });
      }else{
            siblingDetails.push({
              "SiblingAdoptionDecreeDate":  null,
              "SiblingApplicableChildAssessmentDate":  null,
              "SiblingAdoptionApplicable": "NO",
              "SiblingAdoptiveProviderID": null,
              "SiblingName": null,
              "__metadata": {
                  "#type": "SiblingDetails",
                  "#id": "SiblingDetails_id_1"
              }
          });
      }

      const submissiondata: LooseObject = {
        'cjamsPid': Number(this.client_id),
        'removalid': this.removalid,
        'pagesnapshot': this.adoptioneligibilityForm.getRawValue(),
        'initialDetermination': {
          'payload': {
              "name": "Adoption",
              "__metadataRoot": {},
              "Objects": [{
                  "DateAndTimeOfDocumentationForEffortsToPlaceWithoutSubsidy": this.adoptioneligibilityForm.value.effortsToPlaceChildWereMade === 'YES' ? this.adoptioneligibilityForm.value.dateandtimeofdocumentationforEffortstoplacewithoutasubsidy : null,
                  "AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_AdoptiveParents_2": this.dateTimeConversion(this.adoptioneligibilityForm.value.dateofsignatureofparent2OnAdoptionAgreement),
                  "AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_AdoptiveParents_1": this.dateTimeConversion(this.adoptioneligibilityForm.value.dateofsignatureofparent1OnAdoptionAgreement),
                  "AdoptionFinalizationDate": this.dateTimeConversion(this.adoptioneligibilityForm.value.dateofAdoptionFinalization),
                  "ChildApplicableAssessmentDate": this.dateConversion(this.adoptioneligibilityForm.value.childApplicableassessmentdateNotnullEmpty),
                  "AdoptionPetitionFiledDate":  this.dateConversion(this.adoptioneligibilityForm.value.adoptionPetitionFiledDate),
                  "DateAndTimeOfDocumentationForExceptionGrantedInChildsBestInterests": this.dateTimeConversion(this.adoptioneligibilityForm.value.dateandtimeofdocumentationforExceptiongrantedinchildsbestinterests),
                  "AdoptionAssistanceStartDate": this.dateConversion(this.adoptioneligibilityForm.value.startDateofAdoptionassistance),
                  "TPRGrantedtoBothParent": this.adoptioneligibilityForm.value.tprGrantedtoBothParent,
                  "AnotherReasonForChildNotReturningHome": (this.adoptioneligibilityForm.value.anotherreasonchildcouldnotreturntoparenthome === undefined || this.adoptioneligibilityForm.value.anotherreasonchildcouldnotreturntoparenthome === null) ? null : (this.adoptioneligibilityForm.value.anotherreasonchildcouldnotreturntoparenthome === '' ? 'NO' : 'YES'),
                  "AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_Agency": this.dateTimeConversion(this.adoptioneligibilityForm.value.dateofLdssAgencySignatureonAdoptionAgreement),
                  "IsReasonForExceptionRecorded": this.adoptioneligibilityForm.value.isReasonForExceptionRecorded,
                  "QualifiedAlien": this.adoptioneligibilityForm.value.isUsCitizen === 'YES' ? 'NO' : (this.adoptioneligibilityForm.value.isQualifiedAlien === '' ? null :  this.adoptioneligibilityForm.value.isQualifiedAlien) ,
                  "ReasonForNotGrantingTPRForParent": this.adoptioneligibilityForm.value.ifnoReasonfornotgrantingTpRforbothparent,
                  "USCitizen": this.adoptioneligibilityForm.value.isUsCitizen,
                  "applicantInformation": {
                      "ClientID": this.client_id,
                      "__metadata": {
                          "#type": "ApplicantInformation",
                          "#id": "ApplicantInformation_id_1"
                      }
                  },
                  "person": {
                      "ChildHasFosterParentEmotionalTies": null,
                      "IncomeAndAssetsMetAFDCStandards": this.adoptioneligibilityForm.value.wasIncomeAndAssetsMet,
                      "ChildAdoptionUnsuccessfulEffortsToPlace":  this.adoptioneligibilityForm.value.childAdoptionUnsuccessfulEffortsToPlace === '' ? null : this.adoptioneligibilityForm.value.childAdoptionUnsuccessfulEffortsToPlace,
                      "ChildRaceEthnicity": this.adoptioneligibilityForm.value.raceorethnicitycanonlybeincombinationwithoneofthefactorsabove15,
                      "Gender": this.adoptioneligibilityForm.value.gender, 
                      "parentDetails": [{
                          "MinorParentRemovalDate": this.dateConversion(this.adoptioneligibilityForm.value.childRemovalDateVpaRemovalDateofminorparentwhenRemovalTypeMinorParentVpa),
                          "MinorParentName": this.adoptioneligibilityForm.value.parentName,
                          "MinorParentCurrentPlacementType": this.adoptioneligibilityForm.value.childscurrentplacementtypeofminorparent === '' ? null : this.adoptioneligibilityForm.value.childscurrentplacementtypeofminorparent,
                          "MinorParentDateOfBirth": this.dateConversion(this.adoptioneligibilityForm.value.birthdateofparentAgeoftheparentatthetimeofapplicablechildassessment),
                          "MinorParentRemovalCourtOrderDate": this.dateConversion(this.adoptioneligibilityForm.value.removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt),
                          "MinorParentIVEEligibilityForFosterCareStatus": this.adoptioneligibilityForm.value.minorParentIveEligibilityForFosterCareStatus === '' ? null : this.adoptioneligibilityForm.value.minorParentIveEligibilityForFosterCareStatus,
                          "__metadata": {
                              "#type": "ParentDetails",
                              "#id": "ParentDetails_id_1"
                          },
                          "MinorParentIVEEligibilityForFosterCareStartDate": this.dateConversion(this.adoptioneligibilityForm.value.minorParentIveEligibilityForFosterCareStartDate),
                          "DateOfLatestPaymentOfMinorParentIVEEligibilityForFosterCare": this.dateConversion(this.adoptioneligibilityForm.value.dateOfLatestPaymentOfMinorParentIveEligibilityForFosterCare),
                          "MinorParentPhysicalAddress": this.adoptioneligibilityForm.value.physicaladdressofminorparent
                      }],
                      "ChildHasHighRiskOfDisability": this.adoptioneligibilityForm.value.recognizedhighriskofphysicalormentaldisabilityordisease ? 'YES' : 'NO',
                      "Name": this.adoptioneligibilityForm.value.nameofchild,
                      "Child_Removal_Date": this.dateConversion(this.adoptioneligibilityForm.value.childRemovalDate),
                      "ChildRemovedFromSpecifiedRelative": this.adoptioneligibilityForm.value.wasTheChildRemovedFromSpecifiedRelative,
                      "ChildDeprivedOfParentalSupport": this.adoptioneligibilityForm.value.wasTheChildDeprivedOfParentalSupport,
                      "ChildCanReturnToHome": this.adoptioneligibilityForm.value.childCanReturnToHome === "" ? null : this.adoptioneligibilityForm.value.childCanReturnToHome ,
                      "ChildExpectedAdoptiveProviderID": this.adoptioneligibilityForm.value.siblinggroupnameofthesiblingadoptionplacementProviderId,
                      "ChildHasEmotionalDisturbance": this.adoptioneligibilityForm.value.emotionaldisturbancemedicaldocumentationrequired ? 'YES' : 'NO',
                      "ChildCurrentIVEFosterCareEligibilityStatus": this.adoptioneligibilityForm.value.minorParentIveEligibilityForFosterCareStatus === ''? null : this.adoptioneligibilityForm.value.minorParentIveEligibilityForFosterCareStatus,
                      "__metadata": {
                          "#type": "Person",
                          "#id": "Person_id_1"
                      },
                      "DateOfBirth": this.dateConversion(this.adoptioneligibilityForm.value.dateofbirth),
                      "Removal_Court_Order_Date": this.dateConversion(this.adoptioneligibilityForm.value.removalCourtOrderDate),
                      "ChildPreviousAdoptiveParentTPRDate":  this.dateConversion(this.adoptioneligibilityForm.value.previousAdoptiveParentsTpRterminationofparentrightDateIfterminated),
                      "ChildPhysicalAddress": this.adoptioneligibilityForm.value.physicaladdressofchild,
                      "ChildMeetsSSIMedicalDisabledEligReqts": this.adoptioneligibilityForm.value.medicalconditionordisability,
                      "VoluntaryRelinquishment": this.adoptioneligibilityForm.value.voluntaryRelinquishment,
                      "ChildSSIEligibilityStatusOnOrBeforeDateOfAdoption": this.adoptioneligibilityForm.value.childsSsiEligibilityStatus,
                      "ChildPreviousAdoptiveParentDeathDate": this.dateConversion(this.adoptioneligibilityForm.value.previousAdoptiveParentsDeathDateIfdead),
                      "ChildHasPhysicalMentalEmotionalDisability":  this.adoptioneligibilityForm.value.physicalmentaloremotionaldisabilityordiseasemedicaldocumentationrequired ? 'YES' : 'NO',
                      "CountyOfJurisdiction_LDSS": this.adoptioneligibilityForm.value.countyofjurisdiction ? this.adoptioneligibilityForm.value.countyofjurisdiction.trim() : null,
                      "siblingDetails": siblingDetails,
                      "StartDateOfReceivingSSI": this.dateConversion(this.adoptioneligibilityForm.value.startDateOfReceivingSsi),
                      "ChildPreviousAdoptionIVEStatus": this.adoptioneligibilityForm.value.childsIvEStatusofpreviousadoption === '' ? null : this.adoptioneligibilityForm.value.childsIvEStatusofpreviousadoption,
                      "ChildExpectedAdoptionDate": this.dateConversion(this.adoptioneligibilityForm.value.previousAdoptiveParentsTpRterminationofparentrightDateIfterminated),
                      //"ChildReceivingSSIAtRemoval": null,
                      "ChildPreviouslyAdopted": this.adoptioneligibilityForm.value.childsPreviouslyadopted
                  },
                  "DidEffortsToPlaceChildWereMade": this.adoptioneligibilityForm.value.effortsToPlaceChildWereMade,
                  "SingleParentAdoptionCheck": this.adoptioneligibilityForm.value.singleParentAdoptionCheck ? "YES" : "NO",
                  "__metadata": {
                      "#type": "Application",
                      "#id": "Application_id_1"
                  },
                  "DateOfTPROfParent_2": this.dateConversion(this.adoptioneligibilityForm.value.dateofTpRofParent2),
                  "DateOfTPROfParent_1": this.dateConversion(this.adoptioneligibilityForm.value.dateofTpRofParent1),
                  "status": {
                      "AdoptionAssistance": null,
                      "AdoptionApplicable": "YES",
                      "AdoptionNonApplicable": "NO",
                      "__metadata": {
                          "#type": "Status",
                          "#id": "Status_id_1"
                      }
                  }
              }]
          }
      }
    }

    this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe(
      (res) => {
        this.submitForReview.emit();
        this._alertService.success("Submitted successfully!");
      },
      (error) => {
          this._alertService.error("Submission Failed");
      });
    }
  


    
dateConversion(date) {
  if (date === undefined || date === null || date === '') {
    return null;
  } else {
    return this.datePipe.transform(date, 'MM/dd/yyyy');
  }
}

dateTimeConversion(date){
  if(date === undefined || date === null || date === ''){
    return null;
  }else{
    return this.datePipe.transform(date,"MM/dd/yyyy HH:mm:ss");
  }
}

  getPersonsList() {
      this._commonHttpService
          .getPagedArrayList(
              new PaginationRequest({
                  page: 1,
                  limit: 20,
                  method: 'get',
                  where: { objecttypekey: 'servicecase', objectid: this.adoptionDetails.servicecaseid }
              }),
              CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
          )
          .subscribe((result) => {
              console.log("-=-=-=-=-=-=-"+JSON.stringify(result));
              this.firstGenList = result.data;
          });
  }

  getAdoptionEligibilityInfo() {
      if(this.adoptionData){
          this.eligibilityOneFormData = this.adoptionData;
          this.adoptionDetails = this.eligibilityOneFormData.adoptionEligibilityInfo[0];
          this.clientName = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childname;
          //this.getPersonsList();

          // if(this.eligibilityOneFormData && this.eligibilityOneFormData.demographicsInfo && this.eligibilityOneFormData.demographicsInfo.length > 0 
          //   && this.eligibilityOneFormData.demographicsInfo[0].childjurisdiction){
          //     this.loadCounty(this.eligibilityOneFormData.demographicsInfo[0].childjurisdiction)
          // }
          // else{
          //   this.startForm();
          // }
          this.mapData();
      };
  }


  loadCounty(countyidinput) {
    this._commonHttpService.getArrayList(
      { where: { activeflag: '1', state: 'MD', countyid: countyidinput}, nolimit: true, method: 'get' },
      'admin/county' + '?filter'
    ).subscribe(res => {
      if(res && res.length > 0){
        this.countyname = res[0].countyname;
      }
      //this.startForm();
      //this.mapData();
    });
  }


  setData() {
      interface LooseObject {
        [key: string]: any;
      }
  
      const data: LooseObject = {};
        
        console.log('--123456789---'+JSON.stringify(this.eligibilityOneFormData));

        let gender = '';
        if (this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 && 
              this.eligibilityOneFormData.adoptionEligibilityInfo[0].gender === 'M') {
          gender = 'Male';
        } else if (this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 && 
          this.eligibilityOneFormData.adoptionEligibilityInfo[0].gender === 'F') {
          gender = 'Female';
        } else {
          gender = 'Other';
        }
            
       // console.log('this.eligibilityOneFormData' + JSON.stringify(this.eligibilityOneFormData)); 
          data.countyofjurisdiction = this.countyname; 

        if(this.eligibilityOneFormData && this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 ){ 
          data.nameofchild = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childname; 
          data.clientId = this.eligibilityOneFormData.adoptionEligibilityInfo[0].client_id; 
          data.dateofbirth = this.eligibilityOneFormData.adoptionEligibilityInfo[0].dateofbirth; 
          
          data.clientIDofAdoptiveParent  = this.eligibilityOneFormData.adoptionEligibilityInfo[0].parent1providerid; 
          data.nameofAdoptiveParent = this.eligibilityOneFormData.adoptionEligibilityInfo[0].parent1providername; 
          data.dateofFinalizationAdoptionDateofDecree  = this.eligibilityOneFormData.adoptionEligibilityInfo[0].finalizationdate; 

          data.dateandtimeofdocumentationforEffortstoplacewithoutasubsidy = this.eligibilityOneFormData.adoptionEligibilityInfo[0].effortdate;
          data.exceptiongrantedinchildsbestinterests = this.eligibilityOneFormData.adoptionEligibilityInfo[0].isexceptiongranted;
          data.dateandtimeofdocumentationforExceptiongrantedinchildsbestinterests = this.eligibilityOneFormData.adoptionEligibilityInfo[0].dateofexceptiongranted; 
          data.isReasonForExceptionRecorded = (this.eligibilityOneFormData.adoptionEligibilityInfo[0].isexceptiongranted === 'YES' && this.eligibilityOneFormData.adoptionEligibilityInfo[0].remarks !== null && this.eligibilityOneFormData.adoptionEligibilityInfo[0].remarks !== '') ? 'YES' : 'NO'; 
          
          data.gender = gender;
         // data.ChildRaceEthnicity = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childraceethnicity;
          data.ChildDisabilityEvaluationDocumentionDate = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdisabilityevaluationdocumentiondate;
          data.ChildDisabilityType = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdisabilitytype; 
          data.raceorethnicitycanonlybeincombinationwithoneofthefactorsabove15 = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childraceethnicity;  
          data.isUsCitizen = this.eligibilityOneFormData.adoptionEligibilityInfo[0].uscitizen;   
          data.isQualifiedAlien = this.eligibilityOneFormData.adoptionEligibilityInfo[0].qualifiedalien; 

          //Calculating age
          if(data.dateofbirth){
            let dt = new Date(data.dateofbirth);
            let timeDiff = Math.abs(Date.now() - dt.getTime());
            data.age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
          }

          //data.age = this.eligibilityOneFormData.adoptionEligibilityInfo[0].qualifiedalien;    
          data.removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt = this.eligibilityOneFormData.adoptionEligibilityInfo[0].removalcourtorderdate;
          data.physicaladdressofchild = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childphysicaladdress;
          data.wasTheChildDeprivedOfParentalSupport = this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdeprivedofparentalsupport;
          data.wasTheChildRemovedFromSpecifiedRelative = this.eligibilityOneFormData.adoptionEligibilityInfo[0].wasthechildremovedfromspecifiedrelative;

          data.afdcEligibilityMet = this.eligibilityOneFormData.adoptionEligibilityInfo[0].isafdceligibilitymet;									
          data.wasIncomeAndAssetsMet = this.eligibilityOneFormData.adoptionEligibilityInfo[0].isincomeassetsmet;										

          data.effortsToPlaceChildWereMade = this.eligibilityOneFormData.adoptionEligibilityInfo[0].effortstoplacechildweremade;

          if(this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails !== null && 
            this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails.length > 0 ){
              var tprdetails = this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails;
              var isgranted = true;

              if(tprdetails && tprdetails.length === 2){
                isgranted = true;
              }else{
                isgranted = false;
              }

              if(isgranted){
                data.tprGrantedtoBothParent = 'YES';
                data.dateofTpRofParent1 = tprdetails[0].tprdecisiondate;
                data.dateofTpRofParent2 = tprdetails[1].tprdecisiondate;
              }else{
                data.tprGrantedtoBothParent = 'NO';
                tprdetails.forEach(tprdetail => {
                  if(tprdetail.isgranted === 'NO'){
                    data.ifnoReasonfornotgrantingTpRforbothparent = tprdetail.reason;
                  }
                });
              }
          }
        } 
        data.childApplicabilitystatusNotIncomplete = this.eligibilityOneFormData.adoptionApplicabilityauditInfo && this.eligibilityOneFormData.adoptionApplicabilityauditInfo.length > 0 ?  this.eligibilityOneFormData.adoptionApplicabilityauditInfo[0].finalresult : '';


        if(this.eligibilityOneFormData && this.eligibilityOneFormData.adoptionApplicabilityInfo && this.eligibilityOneFormData.adoptionApplicabilityInfo.length> 0 ){ 
          data.medicalconditionordisability = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childmeetallmedicaldisabilityrequirementsforssi;
          data.child617Yearsofage = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].child617yearsofage;
          data.physicalmentaloremotionaldisabilityordiseasemedicaldocumentationrequired = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].physicalmentalemotionaldisability;
          data.emotionaldisturbancemedicaldocumentationrequired = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].emotionaldisturbance;
          data.siblingInformationCheck = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].siblinginformationcheck === null ? false : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].siblinginformationcheck;
          data.recognizedhighriskofphysicalormentaldisabilityordisease = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].recognizedhighriskofphysicaldisability;
          data.raceEthnicityofchild = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].raceethnicityofchild;

          data.childAdoptionUnsuccessfulEffortsToPlace  = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].unsuccessfulreasonableeffortsstatusrecords; 
          data.childApplicableassessmentdateNotnullEmpty = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].submissiondate;

          data.anotherreasonchildcouldnotreturntoparenthome =  this.eligibilityOneFormData.adoptionApplicabilityInfo[0].descriptionofreturnhome;
          data.childCanReturnToHome = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].canchildreturntohome;

          data.removalCourtOrderDate = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].removalcourtorderdate;
          data.childRemovalDate = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childremovaldate;
          data.childsPreviouslyadopted = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childspreviouslyadopted;
          
          data.voluntaryRelinquishment = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].voluntaryrelinquishment;


          if(this.eligibilityOneFormData.adoptionApplicabilityInfo[0].adoptionapplicabilityminorparentinfo !== null &&
            this.eligibilityOneFormData.adoptionApplicabilityInfo[0].adoptionapplicabilityminorparentinfo.length > 0 ){
             var minorparent = this.eligibilityOneFormData.adoptionApplicabilityInfo[0].adoptionapplicabilityminorparentinfo;

             minorparent.forEach(mnrparnt => {
                data.parentName = mnrparnt.minorparentname;
                //data.birthdateofparentAgeoftheparentatthetimeofapplicablechildassessment = mnrparnt.minorparentname;
                data.removalType = mnrparnt.removaltypeofminorparent;
                data.removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt = mnrparnt.removalcourtorderdateofminorparent;
                data.childRemovalDateVpaRemovalDateofminorparentwhenRemovalTypeMinorParentVpa = mnrparnt.removaldateofminorparent;
                data.childscurrentplacementtypeofminorparent = mnrparnt.childscurrentplacementtype;
                data.physicaladdressofminorparent = mnrparnt.physicaladdressofminorparent;
                //data.physicaladdressofchild = mnrparnt.minorparentname;
                //data.minorParentIveEligibilityForFosterCareStatus =mnrparnt.minorparentname;
                //data.dateOfLatestPaymentOfMinorParentIveEligibilityForFosterCare = mnrparnt.minorparentname;
                //data.minorParentIveEligibilityForFosterCareStartDate = mnrparnt.minorparentname;
             });
          }
        }

        if(this.eligibilityOneFormData && this.eligibilityOneFormData.demographicsInfo && this.eligibilityOneFormData.demographicsInfo.length> 0 ){ 
          data.adoptionPetitionFiledDate = this.eligibilityOneFormData.demographicsInfo[0].adoptionpetitiondate;
          data.dateofAdoptionFinalization = this.eligibilityOneFormData.demographicsInfo[0].adoptionfinalizationdate;
          data.dateofsignatureofparent1OnAdoptionAgreement = this.eligibilityOneFormData.demographicsInfo[0].adoptionparent1signdate;
          data.dateofsignatureofparent2OnAdoptionAgreement = this.eligibilityOneFormData.demographicsInfo[0].adoptionparent2signdate;
          data.dateofLdssAgencySignatureonAdoptionAgreement = this.eligibilityOneFormData.demographicsInfo[0].adoptionldssdate;
          data.singleParentAdoptionCheck = this.eligibilityOneFormData.demographicsInfo[0].singleparentadoptioncheck;
        }

        return data;
  
      } 



  

      mapData() {
          let gender = '';
          if (this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 && 
                this.eligibilityOneFormData.adoptionEligibilityInfo[0].gender === 'M') {
            gender = 'Male';
          } else if (this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 && 
            this.eligibilityOneFormData.adoptionEligibilityInfo[0].gender === 'F') {
            gender = 'Female';
          } else {
            gender = 'Other';
          }
              
  
          if(this.eligibilityOneFormData && this.eligibilityOneFormData.adoptionEligibilityInfo && this.eligibilityOneFormData.adoptionEligibilityInfo.length> 0 ){ 
            this.adoptioneligibilityForm.patchValue({
              countyofjurisdiction :  this.eligibilityOneFormData.adoptionEligibilityInfo[0].childjurisdiction, 
              nameofchild : this.eligibilityOneFormData.adoptionEligibilityInfo[0].childname, 
              clientId : this.eligibilityOneFormData.adoptionEligibilityInfo[0].client_id, 
              dateofbirth : this.eligibilityOneFormData.adoptionEligibilityInfo[0].dateofbirth, 
              clientIDofAdoptiveParent  : this.eligibilityOneFormData.adoptionEligibilityInfo[0].parent1providerid, 
              nameofAdoptiveParent : this.eligibilityOneFormData.adoptionEligibilityInfo[0].parent1providername, 
              dateofFinalizationAdoptionDateofDecree  : this.eligibilityOneFormData.adoptionEligibilityInfo[0].finalizationdate, 
              dateandtimeofdocumentationforEffortstoplacewithoutasubsidy : this.eligibilityOneFormData.adoptionEligibilityInfo[0].effortdate,
              exceptiongrantedinchildsbestinterests : this.eligibilityOneFormData.adoptionEligibilityInfo[0].isexceptiongranted,
              dateandtimeofdocumentationforExceptiongrantedinchildsbestinterests : this.eligibilityOneFormData.adoptionEligibilityInfo[0].dateofexceptiongranted, 
              isReasonForExceptionRecorded : (this.eligibilityOneFormData.adoptionEligibilityInfo[0].isexceptiongranted === 'YES' && this.eligibilityOneFormData.adoptionEligibilityInfo[0].remarks !== null && this.eligibilityOneFormData.adoptionEligibilityInfo[0].remarks !== '') ? 'YES' : 'NO', 
              gender : gender,
            // ChildRaceEthnicity : this.eligibilityOneFormData.adoptionEligibilityInfo[0].childraceethnicity,
              ChildDisabilityEvaluationDocumentionDate : this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdisabilityevaluationdocumentiondate,
              ChildDisabilityType : this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdisabilitytype, 
              raceorethnicitycanonlybeincombinationwithoneofthefactorsabove15 : this.eligibilityOneFormData.adoptionEligibilityInfo[0].childraceethnicity,  
              isUsCitizen : this.eligibilityOneFormData.adoptionEligibilityInfo[0].uscitizen,   
              isQualifiedAlien : this.eligibilityOneFormData.adoptionEligibilityInfo[0].qualifiedalien,
              removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt : this.eligibilityOneFormData.adoptionEligibilityInfo[0].removalcourtorderdate,
              physicaladdressofchild : this.eligibilityOneFormData.adoptionEligibilityInfo[0].childphysicaladdress,
              wasTheChildDeprivedOfParentalSupport : this.eligibilityOneFormData.adoptionEligibilityInfo[0].childdeprivedofparentalsupport,
              wasTheChildRemovedFromSpecifiedRelative : this.eligibilityOneFormData.adoptionEligibilityInfo[0].wasthechildremovedfromspecifiedrelative,
              afdcEligibilityMet : this.eligibilityOneFormData.adoptionEligibilityInfo[0].isafdceligibilitymet,									
              wasIncomeAndAssetsMet : this.eligibilityOneFormData.adoptionEligibilityInfo[0].isincomeassetsmet,										
              effortsToPlaceChildWereMade : this.eligibilityOneFormData.adoptionEligibilityInfo[0].effortstoplacechildweremade
            });
  
            //Calculating age
            if(this.adoptioneligibilityForm.value.dateofbirth){
              let dt = new Date(this.adoptioneligibilityForm.value.dateofbirth);
              let timeDiff = Math.abs(Date.now() - dt.getTime());
              let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
              this.adoptioneligibilityForm.patchValue({
                age: age
              });
            }  

            if(this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails !== null && 
              this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails.length > 0 ){
                var tprdetails = this.eligibilityOneFormData.adoptionEligibilityInfo[0].tprdetails;
                var isgranted = true;
  
                if(tprdetails && tprdetails.length === 2){
                  isgranted = true;
                }else{
                  isgranted = false;
                }
  
                if(isgranted){
                 this.adoptioneligibilityForm.patchValue({
                    tprGrantedtoBothParent : 'YES', 
                    dateofTpRofParent1 : tprdetails[0].tprdecisiondate, 
                    dateofTpRofParent2 : tprdetails[1].tprdecisiondate
                  });
                }else{
                  this.adoptioneligibilityForm.patchValue({
                    tprGrantedtoBothParent : 'NO'
                  });
                  tprdetails.forEach(tprdetail => {
                    if(tprdetail.isgranted === 'NO'){
                      this.adoptioneligibilityForm.patchValue({
                        ifnoReasonfornotgrantingTpRforbothparent : tprdetail.reason
                      });
                    }
                  });
                }
            }
          } 

          this.adoptioneligibilityForm.patchValue({
            childApplicabilitystatusNotIncomplete : this.eligibilityOneFormData.adoptionApplicabilityauditInfo && this.eligibilityOneFormData.adoptionApplicabilityauditInfo.length > 0 ?  this.eligibilityOneFormData.adoptionApplicabilityauditInfo[0].finalresult : ''
          });
         
  
          if(this.eligibilityOneFormData && this.eligibilityOneFormData.adoptionApplicabilityInfo && this.eligibilityOneFormData.adoptionApplicabilityInfo.length> 0 ){ 
            
            this.adoptioneligibilityForm.patchValue({
              medicalconditionordisability : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childmeetallmedicaldisabilityrequirementsforssi,
              child617Yearsofage : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].child617yearsofage,
              physicalmentaloremotionaldisabilityordiseasemedicaldocumentationrequired : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].physicalmentalemotionaldisability,
              emotionaldisturbancemedicaldocumentationrequired : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].emotionaldisturbance,
              siblingInformationCheck : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].siblinginformationcheck,
              recognizedhighriskofphysicalormentaldisabilityordisease : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].recognizedhighriskofphysicaldisability,
              raceEthnicityofchild : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].raceethnicityofchild,
              childAdoptionUnsuccessfulEffortsToPlace  : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].unsuccessfulreasonableeffortsstatusrecords, 
              childApplicableassessmentdateNotnullEmpty : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].submissiondate,
              anotherreasonchildcouldnotreturntoparenthome :  this.eligibilityOneFormData.adoptionApplicabilityInfo[0].descriptionofreturnhome,
              childCanReturnToHome : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].canchildreturntohome,
              removalCourtOrderDate : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].removalcourtorderdate,
              childRemovalDate : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childremovaldate,
              childsPreviouslyadopted : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childspreviouslyadopted,
              voluntaryRelinquishment : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].voluntaryrelinquishment,
              childsSsiEligibilityStatus : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].childreceivingssiatremoval,
              startDateOfReceivingSsi : this.eligibilityOneFormData.adoptionApplicabilityInfo[0].startdateofreceivingssi
            });
            
          }
  


            if(this.eligibilityOneFormData.minorParentSiblingInfo && this.eligibilityOneFormData.minorParentSiblingInfo.length)  {  
                const adoptionapplicabilitysiblinginfocontrol = <FormArray>this.adoptioneligibilityForm.controls.siblinggroup;
                if (this.eligibilityOneFormData.minorParentSiblingInfo[0] && _.isArray(this.eligibilityOneFormData.minorParentSiblingInfo[0].siblingsinfo)) {
                  this.eligibilityOneFormData.minorParentSiblingInfo[0].siblingsinfo.forEach(element => {
                    adoptionapplicabilitysiblinginfocontrol.push(this.formBuilder.group({
                      siblinggroupsiblingname: [element.name],
                      siblinggroupsiblingadoptionstatus: [element.childapplicabilitystatus],
                      siblinggroupnameofthesiblingadoptionplacementProviderId: [element.altproviderid],
                      siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingAdoptionDecreeDate : [element.decreedate],
                      siblinggrouplistsiblingsincareeligibleforadoptioninsameadoptiveplacementColumnsSiblingApplicableChildAssessmentDate : [element.childapplicableassessmentdate],
                    })
                  );
                  });
                }



                if(this.eligibilityOneFormData.minorParentSiblingInfo[0] && _.isArray(this.eligibilityOneFormData.minorParentSiblingInfo[0].minorparent)){
                   var minorparent = this.eligibilityOneFormData.minorParentSiblingInfo[0].minorparent;
                   minorparent.forEach(mnrparnt => {
                    this.adoptioneligibilityForm.patchValue({
                      parentName : mnrparnt.name,
                      birthdateofparentAgeoftheparentatthetimeofapplicablechildassessment : mnrparnt.dob,
                      removalType : mnrparnt.relationshiptypekey,
                      removalCourtorderdateofMinorparentwhenRemovalTypeMinorParentCourt : mnrparnt.removaldate,
                      childRemovalDateVpaRemovalDateofminorparentwhenRemovalTypeMinorParentVpa : mnrparnt.vpastartdate,
                      childscurrentplacementtypeofminorparent : mnrparnt.childscurrentplacementtype,
                      physicaladdressofminorparent : mnrparnt.address
                      //physicaladdressofchild : mnrparnt.minorparentname,
                      //minorParentIveEligibilityForFosterCareStatus :mnrparnt.minorparentname,
                      //dateOfLatestPaymentOfMinorParentIveEligibilityForFosterCare : mnrparnt.minorparentname,
                      //minorParentIveEligibilityForFosterCareStartDate : mnrparnt.minorparentname,
                    });
                   });
                }

              }



          if(this.eligibilityOneFormData && this.eligibilityOneFormData.demographicsInfo && this.eligibilityOneFormData.demographicsInfo.length> 0 ){ 
            this.adoptioneligibilityForm.patchValue({
              adoptionPetitionFiledDate : this.eligibilityOneFormData.demographicsInfo[0].adoptionpetitiondate,
              dateofAdoptionFinalization : this.eligibilityOneFormData.demographicsInfo[0].adoptionfinalizationdate,
              dateofsignatureofparent1OnAdoptionAgreement : this.eligibilityOneFormData.demographicsInfo[0].adoptionparent1signdate,
              dateofsignatureofparent2OnAdoptionAgreement : this.eligibilityOneFormData.demographicsInfo[0].adoptionparent2signdate,
              dateofLdssAgencySignatureonAdoptionAgreement : this.eligibilityOneFormData.demographicsInfo[0].adoptionldssdate,
              singleParentAdoptionCheck : this.eligibilityOneFormData.demographicsInfo[0].singleparentadoptioncheck
            });
          }

    } 



  startForm() {
     // console.log("worksheetData >>> ", this.eligibilityData);
     console.log('Inside form');
      const formDataToShow = this.setData();
      Formio.setToken(this.storage.getObj('fbToken'));
      Formio.baseUrl = environment.formBuilderHost;
      Formio.createForm(document.getElementById('titleIvEAdoptionEligibilityNewForm'), environment.formBuilderHost + `/form/5c9ce8218d24c6329b180b25`, {
          hooks: {
              beforeSubmit: (submission, next) => {
                  var submissiondata =  this.submissionData();
                  console.log('-------submit---------'+JSON.stringify(submissiondata));
                  this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postAdoptionApplicability).subscribe(
                    (res) => {
                      this._alertService.success("Submitted successfully!");
                    },
                    (error) => {
                        this._alertService.error("Submission Failed");
                    });
                  // Only call next when we are ready.
                  // next();
                }
          }
      }).then((form) => {
          form.submission = {
              data: formDataToShow
              //data: {
                  //persons_json: JSON.stringify(personsDropdown)
             // }
          };

          form.on('submit', submission => {
              console.log("form submit >>> ", submission);
          });

          form.on('change', formData => {
              // apply binded data to the formbuilder
              form.submission = {
                  data: formData.data
              };
          });
          form.on('render', formData => {
              console.log("form render >>> ", formData);
          });
          form.on('error', formData => {
              console.log("form error >>> ", formData);
          });
      

         

          
      }).catch((err) => {
          // Failed to load form builder
          console.log("Form Error:: ", err);
      });
  }
   
}
