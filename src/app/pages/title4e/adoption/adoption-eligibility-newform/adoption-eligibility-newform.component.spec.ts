import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionEligibilityNewformComponent } from './adoption-eligibility-newform.component';

describe('AdoptionEligibilityNewformComponent', () => {
  let component: AdoptionEligibilityNewformComponent;
  let fixture: ComponentFixture<AdoptionEligibilityNewformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionEligibilityNewformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionEligibilityNewformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
