import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionDispositionComponent } from './adoption-disposition.component';

describe('AdoptionDispositionComponent', () => {
  let component: AdoptionDispositionComponent;
  let fixture: ComponentFixture<AdoptionDispositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionDispositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionDispositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
