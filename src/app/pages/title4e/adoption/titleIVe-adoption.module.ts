import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { QuillModule } from 'ngx-quill';
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule, 
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    MatDialogModule,
    MatStepperModule,
    MatPaginatorModule,
} from '@angular/material';
import { AdoptionRoutingModule } from './titleIVe-adoption-routing.module';
import { AdoptionComponent } from './titleIVe-adoption.component';
import { AdoptionResolverService } from './titleIVe-adoption-resolver.service';
import { AdoptionChildAssessmentComponent } from './adoption-child-assessment/adoption-child-assessment.component';
import { AdoptionDispositionComponent } from './adoption-disposition/adoption-disposition.component';
import { AdoptionAttachmentsComponent } from './adoption-attachments/adoption-attachments.component';
import { ApplicabilityFormComponent } from './adoption-attachments/applicability-form/applicability-form.component';
import { AdpAttachmentUploadComponent } from './adoption-attachments/adp-attachment-upload/adp-attachment-upload.component';
import { AdoptionEligibilityDetailsComponent } from './adoption-eligibility-details/adoption-eligibility-details.component';
import { AdoptionSignatureFeildComponent } from './adoption-eligibility-details/adoption-signature-feild/adoption-signature-feild.component';
import { AdoptionEligibilityNewformComponent } from './adoption-eligibility-newform/adoption-eligibility-newform.component';
import { AdoptionEligibilityWorksheetComponent } from './adoption-eligibility-worksheet/adoption-eligibility-worksheet.component';
import { AdoptionExtendedComponent } from './adoption-extended/adoption-extended.component';
import { AdoptionNarrativeComponent } from './adoption-narrative/adoption-narrative.component';
import { AdoptionReportsComponent } from './adoption-reports/adoption-reports.component';
import { AdoptionExtendedFormComponent } from './adoptionextended-form/adoptionextended-form.component';
import { ChildassessmentFormComponent } from './childassessment-form/childassessment-form.component';
import { EligibilityAuditDetailsComponent } from './eligibility-audit-details/eligibility-audit-details.component';
import { EligibilityOneFormComponent } from './eligibility-one-form/eligibility-one-form.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxPrintModule } from 'ngx-print';
import { MatTooltipModule, MatChipsModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { FormMaterialModule } from '../../../@core/form-material.module';
@NgModule({
    imports: [NgxfUploaderModule,  
        MatDatepickerModule,
        MatTooltipModule, 
        MatChipsModule,
        MatButtonModule,
        MatIconModule,
        MatNativeDateModule,
        NgSelectModule,
        A2Edatetimepicker,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatRadioModule,
        MatTabsModule,
        MatCheckboxModule,
        MatListModule,
        MatCardModule,
        MatButtonToggleModule,
        FormMaterialModule,
        MatTableModule,
        MatExpansionModule,
        MatStepperModule,
        MatPaginatorModule,
          MatDialogModule,  MatFormFieldModule, CommonModule,QuillModule,  FormsModule, ReactiveFormsModule, PaginationModule, 
          MatAutocompleteModule, AdoptionRoutingModule, SignaturePadModule, NgxPrintModule],
    declarations: [AdoptionComponent, AdoptionExtendedFormComponent, ChildassessmentFormComponent, EligibilityAuditDetailsComponent, EligibilityOneFormComponent, AdoptionReportsComponent, AdoptionNarrativeComponent, AdoptionExtendedComponent, AdoptionEligibilityWorksheetComponent, AdoptionEligibilityNewformComponent, AdoptionSignatureFeildComponent,AdoptionEligibilityDetailsComponent, AdoptionChildAssessmentComponent,AdoptionDispositionComponent,AdoptionAttachmentsComponent,ApplicabilityFormComponent, AdpAttachmentUploadComponent],
    providers: [AdoptionResolverService],
    exports: [AdoptionChildAssessmentComponent]
})
export class AdoptionModule {}
