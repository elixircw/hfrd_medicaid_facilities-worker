import { Injectable } from '@angular/core';
import { CommonHttpService, AuthService } from '../../../@core/services';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DataStoreService } from '../../../@core/services';

@Injectable()
export class AdoptionResolverService {

  constructor(private _commonHttpService: CommonHttpService, private router: Router,
    private _dataStore: DataStoreService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.getAdoptionEligibilityInfo();
  }

  getAdoptionEligibilityInfo() {
    var client_id= this._dataStore.getData('adoption_clientid');
    var removalid= this._dataStore.getData('adoption_removalid');
    console.log('client_id'+client_id);
    console.log('removalid'+removalid);
    return this._commonHttpService.getAll(
        'titleive/adoption/adoption-eligibility-worksheet/' + client_id+'/'+removalid
        );
}

}
