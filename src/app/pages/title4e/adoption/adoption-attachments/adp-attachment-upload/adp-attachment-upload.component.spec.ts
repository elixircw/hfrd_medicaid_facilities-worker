import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdpAttachmentUploadComponent } from './adp-attachment-upload.component';

describe('AdpAttachmentUploadComponent', () => {
  let component: AdpAttachmentUploadComponent;
  let fixture: ComponentFixture<AdpAttachmentUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdpAttachmentUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdpAttachmentUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
