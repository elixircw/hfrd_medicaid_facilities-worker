// import { Component, OnInit } from '@angular/core';

/// <reference types="dwt" />
import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { HttpHeaders, HttpSentEvent } from '@angular/common/http';
import { AppConfig } from '../../../../../app.config';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { CommonHttpService, AuthService, AlertService, GenericService, DataStoreService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { config } from '../../../../../../environments/config';
import { HttpService } from '../../../../../../app/@core/services/http.service';
import { FileUtils } from '../../../../../@core/common/file-utils';
import { AttachmentUpload } from '../../../../provider-portal-temp/current-application/_entities/newApplicantModel';
import { AttachmentDetailComponent } from '../../../../newintake/my-newintake/intake-attachments/attachment-detail/attachment-detail.component';
import { AttachmentSubCategory } from '../../../../newintake/my-newintake/_entities/newintakeModel';
import { IntakeAssessmentRequestIds } from '../../../../provider-applicant/new-applicant/_entities/newintakeModel';
import { Attachment } from '../../../../newintake/my-newintake/intake-attachments/_entities/attachmnt.model';
import { IntakeStoreConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { NewUrlConfig } from '../../../../provider-referral/provider-referral-url.config';
// import { IntakeStoreConstants } from '../../my-newintake.constants';
declare var $: any;
declare var Dynamsoft: any;
declare var EnumDWT_ImageType: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'adp-attachment-upload',
  templateUrl: './adp-attachment-upload.component.html',
  styleUrls: ['./adp-attachment-upload.component.scss']
})
// export class AdpAttachmentUploadComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }






export class AdpAttachmentUploadComponent implements OnInit, OnDestroy {
    curDate: Date;
    fileToSave = [];
    // intakeNumber: any;
    uploadedFile = [];

    tabActive = false;
    daNumber: string;
    id: string;
    attachmentResponse: AttachmentUpload;
    attachmentClassificationTypeDropDown$: Observable<DropdownModel[]>;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    private token: AppUser;
    @Input() intakeNumber: string;
    @Output() attachment = new EventEmitter();
    @ViewChild(AttachmentDetailComponent) attachmentDetail: AttachmentDetailComponent;
    /*Dynamo Soft */
    dwObject: any;
    fileName: string;
    isFileScanned: boolean;
    docNames: any;
    myData: any;
    fileExistsMsg: any;
    isUploading = false;
    disableScanBtn = false;

    showDialog: boolean;
    img_width: string;
    img_height: string;
    selectedInterpolation: number;
    _iLeft: any;
    _iTop: any;
    _iRight: any;
    _iBottom: any;
    DW_PreviewMode: any;
    isAttachDetail = false;
    dwtIntakeNo: any;
    subCategoryClassificationType$: Observable<AttachmentSubCategory[]>;
    subCategoryList$: Observable<AttachmentSubCategory[]>;
    createdCases = [];
    private assessmentRequestDetail: IntakeAssessmentRequestIds;
    store: any;
    showAssesment: number;
    paginationInfo: PaginationInfo = new PaginationInfo();
    assessmentTemplateID: any;
    assessmentTemplateName: any;
    subCategoryList = [];
    subType = [];


    constructor(
        private router: Router,
        private _dataStoreService: DataStoreService,
        private _service: GenericService<Attachment>,
        private _dropDownService: CommonHttpService,
        private route: ActivatedRoute,
        private _uploadService: NgxfUploaderService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _http: HttpService
    ) {
        this.docNames = [];
        this.id = route.snapshot.parent.params['id'];
        // this.intakeNumber = route.snapshot.params['intakeNumber'];
        this.token = this._authService.getCurrentUser();
        this.store = this._dataStoreService.getCurrentStore();
    }

    ngOnInit() {
        this.loadDropdown();
        this.curDate = new Date();
        Dynamsoft.WebTwainEnv.Load();
        Dynamsoft.WebTwainEnv.Trial = false;
        Dynamsoft.WebTwainEnv.ProductKey = config.DynamsoftProductKey;
        Dynamsoft.WebTwainEnv.ResourcesPath = 'assets/images/dwt/scanner';
        // (<any>$('#upload-attachment')).modal('show');
        const purposeSelected = this._dataStoreService.getData(IntakeStoreConstants.purposeSelected);
        console.log('purposeSelected', purposeSelected);
        this.createdCases = this._dataStoreService.getData(IntakeStoreConstants.createdCases);
    }

    ngOnDestroy() {
        Dynamsoft.WebTwainEnv.Unload();
    }

    uploadFile(file: File | FileError): void {
        console.log('file', file);
        if (!(file instanceof Array)) {
            this._alertService.error('Please enter a valid file');
            // this.alertError(file);
            return;
        }
        file.map((item, index) => {
            const fileExt = item.name
                .toLowerCase()
                .split('.')
                .pop();
            if (
                fileExt === 'mp3' ||
                fileExt === 'ogg' ||
                fileExt === 'wav' ||
                fileExt === 'acc' ||
                fileExt === 'flac' ||
                fileExt === 'aiff' ||
                fileExt === 'mp4' ||
                fileExt === 'mov' ||
                fileExt === 'avi' ||
                fileExt === '3gp' ||
                fileExt === 'wmv' ||
                fileExt === 'mpeg-4' ||
                fileExt === 'pdf' ||
                fileExt === 'txt' ||
                fileExt === 'docx' ||
                fileExt === 'doc' ||
                fileExt === 'xls' ||
                fileExt === 'xlsx' ||
                fileExt === 'jpeg' ||
                fileExt === 'jpg' ||
                fileExt === 'png' ||
                fileExt === 'gif' ||
                fileExt === 'ppt' ||
                fileExt === 'pptx' ||
                fileExt === 'gif'
            ) {
                // const uploadedFile = [];
                // uploadedFile.push(item);
                // this.uploadedFile = this.uploadedFile.concat(uploadedFile);
                console.log('item', item);
                this.uploadedFile.push(item);
                this.uploadAttachment(index);
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error(fileExt + " format can't be uploaded");
                return;
            }
            // for (let i = 0; i < this.uploadedFile.length; i++) {
            //     this.uploadAttachment(i);
            // }
        });
        // this.uploadedFile.push(file);
        // this.uploadedFile = this.uploadedFile.concat(uploadedFile);
        // this.uploadedFile.map((item) => {
        //     this.saveAttachment(item);
        // });
    }

    humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    uploadAttachment(index) {
        console.log('index', index);
        console.log('uploaded file len', this.uploadedFile.length);

        const workEnv = config.workEnvironment;
        let uploadUrl = '';
        if (workEnv === 'state') {
            uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' +
                NewUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=' + this.intakeNumber;
            console.log('state', uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + NewUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=' + this.intakeNumber;
            console.log('local', uploadUrl);
        }

        this._uploadService

            .upload({
                url: uploadUrl,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile[index],
                process: true
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        // this.progress.percentage = response.percent;
                        this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        this.attachmentResponse = response.data;
                        this.fileToSave.push(response.data);
                        this.fileToSave[this.fileToSave.length - 1].documentattachment = {
                            assessmenttemplateid: '',
                            attachmenttypekey: '',
                            attachmentclassificationtypekey: '',
                            attachmentdate: new Date(),
                            sourceauthor: '',
                            attachmentsubject: '',
                            sourceposition: '',
                            attachmentpurpose: '',
                            sourcephonenumber: '',
                            acquisitionmethod: '',
                            sourceaddress: '',
                            locationoforiginal: '',
                            insertedby: this.token.user.userprofile.displayname,
                            note: '',
                            updatedby: this.token.user.userprofile.displayname,
                            activeflag: 1
                        };
                        this.fileToSave[this.fileToSave.length - 1].description = '';
                        this.fileToSave[this.fileToSave.length - 1].documentdate = new Date();
                        this.fileToSave[this.fileToSave.length - 1].title = '';
                        this.fileToSave[this.fileToSave.length - 1].intakenumber = this.intakeNumber;
                        this.fileToSave[this.fileToSave.length - 1].objecttypekey = 'ServiceRequest';
                        this.fileToSave[this.fileToSave.length - 1].rootobjecttypekey = 'ServiceRequest';
                        this.fileToSave[this.fileToSave.length - 1].activeflag = 1;
                        this.fileToSave[this.fileToSave.length - 1].intakenumber = this.intakeNumber;
                        this.fileToSave[this.fileToSave.length - 1].insertedby = this.token.user.userprofile.displayname;
                        this.fileToSave[this.fileToSave.length - 1].updatedby = this.token.user.userprofile.displayname;

                        // this._alertService.success('File Uploaded Succesfully!');
                    }
                },
                (err) => {
                    console.log(err);
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    this.uploadedFile.splice(index, 1);
                },
                () => {
                    console.log('complete', this.fileToSave[this.fileToSave.length - 1]);
                }
            );
    }
    formTab() {
        this.attachmentDetail.patchAttachmentDetail(this.attachmentResponse);
        this.attachmentDetail.loadDropdown();
        this.tabActive = true;
        $('#step1').removeClass('active');
        $('#complete').addClass('active');
    }
    modalDismiss() {
        (<any>$('#upload-attachment')).modal('hide');
    }
    deleteUpload(index) {
        // const curIndex = this.uploadedFile.length - index - 1;
        this.uploadedFile.splice(index, 1);
        this.fileToSave.splice(index, 1);
    }
    clearAllUpload() {
        this.uploadedFile = [];
        this.fileToSave = [];
    }
    titleUpdate(event, index) {
        // const curIndex = this.fileToSave.length - index;
        // this.fileToSave[index].title = event.target.value;
        this.uploadedFile[index].title = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidTitle = false;
        } else {
            this.uploadedFile[index].invalidTitle = true;
        }
    }
    descUpdate(event, index) {
        // const curIndex = this.fileToSave.length - index;
        this.uploadedFile[index].description = event.target.value;
    }
    docDateUpdate(event, index) {
        this.uploadedFile[index].docDate = event.target.value;
    }
    typeUpdate(event, index) {
        // const curIndex = this.fileToSave.length - index;
        this.uploadedFile[index].attachmenttypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentType = false;
        } else {
            this.uploadedFile[index].invalidAttachmentType = true;
        }
    }
    categoryUpdate(event, index) {
        // const curIndex = this.fileToSave.length - index;
        this.uploadedFile[index].attachmentclassificationtypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentClassify = false;
        } else {
            this.uploadedFile[index].invalidAttachmentClassify = true;
        }
        if (this._authService.getCurrentUser().user.userprofile.teamtypekey === 'AS') {
            if (this.createdCases.length > 0) {
                for (let i = 0; i < this.createdCases.length; i++) {
                    // if (this.createdCases[i].subSeriviceTypeValue === 'Project Home' || this.createdCases[i].subSeriviceTypeValue === 'Adult Foster Care') {
                    if (event.target.value === 'Assessment Document') {
                        this.getSubCategory();
                    } else {
                        this.subCategoryList$ = null;
                    }
                    // }
                }
            }
        }
    }


    getSubCategory() {
        console.log('store...', this.store);
        if (this.store[IntakeStoreConstants.purposeSelected]) {
            const purpose = this.store[IntakeStoreConstants.purposeSelected];
            this.subType = this.store[IntakeStoreConstants.createdCases];
            // if (this.store['purposesubtype']) {
            // const subType =  this.store['purposesubtype'];
            if (this.subType && this.subType.length > 0) {
                const purposeSubType = this.subType[0].subServiceTypeID;
                this.subCategoryClassificationType$ = this._dropDownService
                    .getArrayList(
                        {
                            where: {
                                intakeservicerequesttypeid: purpose.value,
                                intakeservicerequestsubtypeid: purposeSubType,
                                agencycode: 'AS',
                                target: 'Intake'
                            },
                            method: 'get'
                        },
                        'admin/assessmenttemplate/listassessmenttemplate?filter'
                    )
                    .map(result => {
                        return result;
                    });
                this.subCategoryClassificationType$.subscribe(result => {
                    console.log('result', result);
                    if (result && result.length > 0) {
                        this.subCategoryList = [];
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].isrequired === true) {
                                this.subCategoryList.push(result[i]);
                            }
                        }
                    }
                    this._dataStoreService.setData('categorySubType', this.subCategoryList);
                    // this._dataStoreService.setData(IntakeStoreConstants.attachements, this.subCategoryList);
                    this.subCategoryList$ = Observable.of(this.subCategoryList);
                });
            }
        }
    }

    onSubcategory(categoryid) {
        this.assessmentTemplateID = categoryid.target.value;
    }



    saveAttachmentDetails() {
        console.log('Called afterscanning ');
        if (this.uploadedFile.length !== this.fileToSave.length) {
            this._alertService.error('Please wait till files get uploaded');
        } else {
            this.uploadedFile.map((item, index) => {
                this.fileToSave[index].title = item.title;
                this.fileToSave[index].description = item.description;
                this.fileToSave[index].documentattachment.attachmenttypekey = item.attachmenttypekey;
                this.fileToSave[index].documentattachment.attachmentclassificationtypekey = item.attachmentclassificationtypekey;
                this.fileToSave[index].documentattachment.assessmenttemplateid = this.assessmentTemplateID;
                // this.fileToSave[index].documentattachment.assessmenttemplatename = this.assessmentTemplateName;
            });
            const AttachValidate = this.fileToSave.filter((wer) => !wer.documentattachment.attachmentclassificationtypekey || !wer.documentattachment.attachmenttypekey || !wer.title);
            // this.fileToSave.map((wer) => {
            if (AttachValidate.length === 0) {
                this._service.endpointUrl = NewUrlConfig.EndPoint.Intake.SaveAttachmentUrl;
                console.log('fileToSave', this.fileToSave[0]);
                this._service.createArrayList(this.fileToSave).subscribe(
                    (response) => {
                        response.map((item, index) => {
                            if (item.documentpropertiesid) {
                                response.splice(index, 1);
                                this.fileToSave.splice(index, 1);
                                this.uploadedFile.splice(index, 1);
                            }
                            const docProp = response.filter((docId) => docId.Documentattachment);
                            if (docProp.length === 0) {
                                this._alertService.success('Attachment(s) added successfully!');
                                this.attachment.emit('all');
                                (<any>$('#upload-attachment')).modal('hide');
                                (<any>$('#upload-scanner-attachment')).modal('hide');
                                this.fileToSave = [];
                                this.uploadedFile = [];
                                this.dynamoScanReset();
                            }
                            if (item.Documentattachment) {
                                const attPos = index + 1;
                                this._alertService.error(item.Documentattachment + ' for Attachment ' + attPos);
                            } else if (!item.documentpropertiesid) {
                                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                            }
                        });
                        // if (response[0] && response[0].documentpropertiesid) {
                        //     this._alertService.success('Attachment added successfully!');
                        //     // this.modalDismiss.emit();
                        //     // let currentUrl = '/pages/newintake/my-newintake';
                        //     // if (this.id) {
                        //     //     currentUrl = '/pages/newintake/my-newintake/' + this.id;
                        //     // }
                        //     // this.router.navigateByUrl(currentUrl).then(() => {
                        //     //     this.router.navigated = true;
                        //     //     this.router.navigate([currentUrl]);
                        //     // });
                        //     this.attachment.emit('all');
                        //     (<any>$('#upload-attachment')).modal('hide');
                        //     this.fileToSave = [];
                        //     this.uploadedFile = [];
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error('Please fill all mandatory fields');
                this.uploadedFile.map((item) => {
                    if (!item.title) {
                        item.invalidTitle = true;
                    } else {
                        item.invalidTitle = false;
                    }
                    if (!item.attachmentclassificationtypekey) {
                        item.invalidAttachmentClassify = true;
                    } else {
                        item.invalidAttachmentClassify = false;
                    }
                    if (!item.attachmenttypekey) {
                        item.invalidAttachmentType = true;
                    } else {
                        item.invalidAttachmentType = false;
                    }
                });
            }
        }
        // });
        // this.router.routeReuseStrategy.shouldReuseRoute = function() {
        //     return false;
        // };
    }
    private loadDropdown() {
        const source = forkJoin(
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentTypeUrl + '?filter={"nolimit": true}'
            ),
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
            )
        )
            .map((result) => {
                return {
                    attachmentType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmenttypekey
                            })
                    ),
                    attachmentClassificationType: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmentclassificationtypekey
                            })
                    )
                };
            })
            .share();
        this.attachmentTypeDropdown$ = source.pluck('attachmentType');
        this.attachmentClassificationTypeDropDown$ = source.pluck('attachmentClassificationType');
        this.attachmentClassificationTypeDropDown$.subscribe((response) => {
            console.log('response...', response);
        });
    }


    /*Dynamo Soft Implementation */
    // acquireImage1(): void {
    //     this.dwObject = Dynamsoft.WebTwainEnv.GetWebTwain('dwtcontrolContainer');
    //     const bSelected = this.dwObject.SelectSource();
    //     if (bSelected) {
    //       const onAcquireImageSuccess = () => { this.dwObject.CloseSource(); };
    //       const onAcquireImageFailure = onAcquireImageSuccess;
    //       this.dwObject.OpenSource();
    //       this.dwObject.AcquireImage({}, onAcquireImageSuccess, onAcquireImageFailure);
    //     }
    //   }
    //   onSubmit(scanFileForm: NgForm) {
    //        if (scanFileForm.valid) {
    //       console.log(scanFileForm.controls.fileName.value);
    //       this.fileName = scanFileForm.controls.fileName.value;
    //       console.log("fileName::"   + this.fileName);
    //       this.upLoad();
    //       // ...our form is valid, we can submit the data
    //     }
    //   }
    // acquireImage(): void {
    //     this.dwObject = Dynamsoft.WebTwainEnv.GetWebTwain('dwtcontrolContainer');
    //     this.dwObject.RegisterEvent('OnTopImageInTheViewChanged', this.Dynamsoft_OnTopImageInTheViewChanged);
    //     this.dwObject.RegisterEvent('OnMouseClick', this.Dynamsoft_OnMouseClick);
    //     this.dwObject.RegisterEvent('OnPostTransfer', this.Dynamsoft_OnPostTransfer);
    //     this.dwObject.RegisterEvent('OnPostLoad', this.Dynamsoft_OnPostLoadfunction);
    //     this.dwObject.RegisterEvent('OnPostAllTransfers', this.Dynamsoft_OnPostAllTransfers);
    //     this.dwObject.RegisterEvent('OnImageAreaSelected', this.Dynamsoft_OnImageAreaSelected);
    //     this.dwObject.RegisterEvent('OnImageAreaDeSelected', this.Dynamsoft_OnImageAreaDeselected);
    //     this.dwObject.RegisterEvent('OnGetFilePath', this.Dynamsoft_OnGetFilePath);
    //     const bSelected = this.dwObject.SelectSource();
    //     if (bSelected) {
    //         const onAcquireImageSuccess = () => {
    //             this.isFileScanned = true;
    //             console.log('this.isFileScanned', this.isFileScanned);
    //             this.disableScanBtn = true;
    //         };
    //         const onAcquireImageFailure = onAcquireImageSuccess;
    //         this.dwObject.OpenSource();
    //         this.dwObject.AcquireImage({}, onAcquireImageSuccess, onAcquireImageFailure);
    //     }
    // }

    uploadScannedFile(file: File | FileError): void {
        console.log('file', file);
        if (!(file instanceof Array)) {
            // this.alertError(file);
            return;
        }
        file.map((item, index) => {
            const fileExt = item.name
                .toLowerCase()
                .split('.')
                .pop();
            if (
                fileExt === 'mp3' ||
                fileExt === 'ogg' ||
                fileExt === 'wav' ||
                fileExt === 'acc' ||
                fileExt === 'flac' ||
                fileExt === 'aiff' ||
                fileExt === 'mp4' ||
                fileExt === 'mov' ||
                fileExt === 'avi' ||
                fileExt === '3gp' ||
                fileExt === 'wmv' ||
                fileExt === 'mpeg-4' ||
                fileExt === 'pdf' ||
                fileExt === 'txt' ||
                fileExt === 'docx' ||
                fileExt === 'doc' ||
                fileExt === 'xls' ||
                fileExt === 'xlsx' ||
                fileExt === 'jpeg' ||
                fileExt === 'jpg' ||
                fileExt === 'png' ||
                fileExt === 'gif' ||
                fileExt === 'ppt' ||
                fileExt === 'pptx' ||
                fileExt === 'gif'
            ) {
                console.log('item', item);
                this.uploadedFile.push(item);
                this.uploadAttachment(index);
            } else {
                this._alertService.error(fileExt + ' format can\'t be uploaded');
                return;
            }
        });
    }

    // scanDocuments() {
    //     this.isAttachDetail = true;
    //     let scanDocs = [];
    //     let len = this.dwObject.HowManyImagesInBuffer;
    //     if (len === 0) {
    //         scanDocs.push(0);
    //     } else {
    //         for (let i = 0; i < len; i++) {
    //             console.log('i', i);
    //             scanDocs.push(i);
    //         }
    //     }
    //     this.dwObject.ConvertToBlob(scanDocs, EnumDWT_ImageType.IT_PDF, (res) => {
    //         console.log('****' + res.size);
    //         this.updatePageInfo();
    //         const fileName = FileUtils.getFileName('pdf');
    //         const fileObject = new File([res], fileName, {
    //             type: 'application/pdf'
    //         });
    //         this.uploadScannedFile(fileObject);
    //         this.uploadedFile.push(fileObject);
    //         this.uploadAttachment(0);
    //     }, (num, err) => {
    //         console.log('error is ' + err);
    //     });
    // }


    private createJsonBlob<T>(content: T) {
        return new Blob([JSON.stringify(content)], { type: 'application/json' });
    }
    public blobToFile(theBlob: Blob, fileName: string): File {
        const b: any = theBlob;
        b.lastModifiedDate = new Date();
        b.name = fileName;
        return <File>b;
    }
    upLoad() {
        this.dwObject.ConvertToBlob([0], EnumDWT_ImageType.IT_PDF, (res) => {
            const file = this.blobToFile(res, this.fileName);
        }, (num, err) => {
            console.log('error is ' + err);
        });
    }

    checkIfImagesInBuffer() {
        if (this.dwObject !== undefined) {
            if (this.dwObject.HowManyImagesInBuffer === 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    updatePageInfo() {
        if (document.getElementById('DW_TotalImage')) {
            (<HTMLInputElement>document.getElementById('DW_TotalImage')).value = this.dwObject.HowManyImagesInBuffer + '';
        }
        const currImgIndex: number = this.dwObject.CurrentImageIndexInBuffer + 1;
        if (document.getElementById('DW_CurrentImage')) {
            (<HTMLInputElement>document.getElementById('DW_CurrentImage')).value = currImgIndex + '';
        }
    }
    // ************************** Edit Image ******************************
    btnShowImageEditor_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.ShowImageEditor();
    }

    btnRotateLeft_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.RotateLeft(this.dwObject.CurrentImageIndexInBuffer);
    }

    btnRotateRight_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.RotateRight(this.dwObject.CurrentImageIndexInBuffer);
    }

    btnRotate180_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.Rotate(this.dwObject.CurrentImageIndexInBuffer, 180, true);
    }

    btnMirror_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.Mirror(this.dwObject.CurrentImageIndexInBuffer);
    }

    btnFlip_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.Flip(this.dwObject.CurrentImageIndexInBuffer);
    }

    btnRemoveCurrentImage_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.RemoveAllSelectedImages();
        if (this.dwObject.HowManyImagesInBuffer === 0) {
            if (document.getElementById('DW_TotalImage')) {
                (<HTMLInputElement>document.getElementById('DW_TotalImage')).value = this.dwObject.HowManyImagesInBuffer + '';
                this.isFileScanned = false;
            }
            if (document.getElementById('DW_CurrentImage')) {
                (<HTMLInputElement>document.getElementById('DW_CurrentImage')).value = 0 + '';
            }
            return;
        } else {
            this.updatePageInfo();
        }
    }

    dynamoScanReset() {
        console.log('check');
        // Dynamsoft.WebTwainEnv.DeleteDWTObject("dwtcontrolContainer");
        // console.log("recreate");
        // Dynamsoft.WebTwainEnv.CreateDWTObject("dwtcontrolContainer");
        this.btnRemoveCurrentImage_onclick();
        this.isAttachDetail = false;
        this.uploadedFile = [];

    }

    btnRemoveAllImages_onclick() {
        this.isAttachDetail = false;
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.RemoveAllImages();
        this.isFileScanned = false;
        if (document.getElementById('DW_TotalImage')) {
            (<HTMLInputElement>document.getElementById('DW_TotalImage')).value = 0 + '';
        }
        if (document.getElementById('DW_CurrentImage')) {
            (<HTMLInputElement>document.getElementById('DW_CurrentImage')).value = 0 + '';
        }
    }
    /*----------------Change Image Size--------------------*/
    btnChangeImageSize_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        if (this.dwObject !== undefined) {
            this.showDialog = true;
            this.img_width = this.dwObject.GetImageWidth(this.dwObject.CurrentImageIndexInBuffer) + '';
            this.img_height = this.dwObject.GetImageHeight(this.dwObject.CurrentImageIndexInBuffer) + '';
        } else {
            alert('Please scan a document');
            this.showDialog = false;
            return;
        }
    }

    btnChangeImageSizeOK_onclick(changeImageSizeForm: NgForm) {
       // this.dwObject.ChangeImageSize(this.dwObject.CurrentImageIndexInBuffer, parseInt(this.img_width), parseInt(this.img_height), this.selectedInterpolation);
        this.showDialog = false;
    }

    selectInterpolcationChangeHandler(event: any) {
        this.selectedInterpolation = event.target.value;
    }

    /*----------------Crop Image--------------------*/
    btnCrop_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        console.log('crop called');
        if (this._iLeft !== 0 || this._iTop !== 0 || this._iRight !== 0 || this._iBottom !== 0) {
            console.log('crop2');
            this.dwObject.Crop(
                this.dwObject.CurrentImageIndexInBuffer,
                this._iLeft, this._iTop, this._iRight, this._iBottom
            );
            this._iLeft = 0;
            this._iTop = 0;
            this._iRight = 0;
            this._iBottom = 0;
            return;
        } else {
            alert('Please select the area you\'d like to crop');
        }
    }

    /* Navigator Dynamo Soft*/
    setlPreviewMode() {
        let varNum = (<HTMLSelectElement>document.getElementById('DW_PreviewMode')).selectedIndex;
        varNum = varNum + 1;
        const btnCrop1 = (<HTMLImageElement>document.getElementById('btnCrop'));
        if (btnCrop1) {
            let tmpstr = btnCrop1.src;
            if (varNum > 1) {
                tmpstr = tmpstr.replace('Crop.', 'Crop_gray.');
                btnCrop1.src = tmpstr;
                btnCrop1.onclick = function () { };
            } else {
                tmpstr = tmpstr.replace('Crop_gray.', 'Crop.');
                btnCrop1.src = tmpstr;
                btnCrop1.onclick = function () {
                    this.btnCrop_onclick();
                }.bind(this);
            }
        }
        this.dwObject.SetViewMode(varNum, varNum);
        if (Dynamsoft.Lib.env.bMac || Dynamsoft.Lib.env.bLinux) {
            return;
        } else if (this.DW_PreviewMode.selectedIndex !== 0) {
            this.dwObject.MouseShape = true;
        } else {
            this.dwObject.MouseShape = false;
        }
    }

    // private Dynamsoft_OnMouseClick = function Dynamsoft_OnMouseClick(index) {
    //     // console.log("Dynamsoft_OnMouseClick called" );
    //     this.updatePageInfo();
    // }.bind(this);

    // private Dynamsoft_OnPostTransfer = function Dynamsoft_OnPostTransfer() {
    //     // console.log("Dynamsoft_OnPostTransfer called" );
    //     this.updatePageInfo();
    // }.bind(this);

    // private Dynamsoft_OnPostLoadfunction = function Dynamsoft_OnPostLoadfunction(path, name, type) {
    //     // console.log("Dynamsoft_OnPostLoadfunction called" );
    //     this.updatePageInfo();
    // }.bind(this);

    // private Dynamsoft_OnPostAllTransfers = function Dynamsoft_OnPostAllTransfers() {
    //     // console.log("Dynamsoft_OnPostAllTransfers called" );
    //     if (this.dwObject !== undefined) {
    //         this.dwObject.CloseSource();
    //     }
    //     this.updatePageInfo();
    //     // this.checkErrorString();
    // }.bind(this);

    // private Dynamsoft_OnTopImageInTheViewChanged = function Dynamsoft_OnTopImageInTheViewChanged(index) {
    //     // console.log("Dynamsoft_OnTopImageInTheViewChanged called" );
    //     this._iLeft = 0;
    //     this._iTop = 0;
    //     this._iRight = 0;
    //     this._iBottom = 0;
    //     this.dwObject.CurrentImageIndexInBuffer = index;
    //     this.updatePageInfo();
    // }.bind(this);

    // private Dynamsoft_OnImageAreaSelected = function Dynamsoft_OnImageAreaSelected(index, left, top, right, bottom) {
    //     this._iLeft = left;
    //     this._iTop = top;
    //     this._iRight = right;
    //     this._iBottom = bottom;
    // }.bind(this);

    // private Dynamsoft_OnImageAreaDeselected = function Dynamsoft_OnImageAreaDeselected(index) {
    //     this._iLeft = 0;
    //     this._iTop = 0;
    //     this._iRight = 0;
    //     this._iBottom = 0;
    // }.bind(this);

    // private Dynamsoft_OnGetFilePath = function Dynamsoft_OnGetFilePath(bSave, count, index, path, name) {

    // };


    btnFirstImage_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.CurrentImageIndexInBuffer = 0;
        this.updatePageInfo();
    }

    btnLastImage_onclick() {
        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        const k: number = this.dwObject.HowManyImagesInBuffer - 1;
        this.dwObject.CurrentImageIndexInBuffer = k;
        this.updatePageInfo();
    }
    btnPreImage_onclick() {
        this.dwObject.CurrentImageIndexInBuffer = this.dwObject.CurrentImageIndexInBuffer - 1;
        this.updatePageInfo();
    }

    btnNextImage_onclick() {
        let i: number;
        let j: number;
        if (this.dwObject !== undefined) {
            i = this.dwObject.HowManyImagesInBuffer;
            j = this.dwObject.CurrentImageIndexInBuffer;
        }

        if (!this.checkIfImagesInBuffer()) {
            return;
        }
        this.dwObject.CurrentImageIndexInBuffer = j + 1;
        this.updatePageInfo();
    }

    btnPreImage_wheel() {
        if (this.dwObject.HowManyImagesInBuffer !== 0) {
            this.btnPreImage_onclick();
        }
    }
    btnNextImage_wheel() {
        if (this.dwObject.HowManyImagesInBuffer !== 0) {
            this.btnNextImage_onclick();
        }
    }

}
