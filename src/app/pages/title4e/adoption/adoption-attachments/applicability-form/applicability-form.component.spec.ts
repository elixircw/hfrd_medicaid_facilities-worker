import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicabilityFormComponent } from './applicability-form.component';

describe('ApplicabilityFormComponent', () => {
  let component: ApplicabilityFormComponent;
  let fixture: ComponentFixture<ApplicabilityFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicabilityFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicabilityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
