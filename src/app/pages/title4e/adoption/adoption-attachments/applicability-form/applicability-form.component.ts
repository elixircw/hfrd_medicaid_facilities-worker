import { Component, OnInit,Input } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services';
import { Router } from '@angular/router';
import { TitleIveadoptionService } from '../../title-iveadoption.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'applicability-form',
  templateUrl: './applicability-form.component.html',
  styleUrls: ['./applicability-form.component.scss']
})
export class ApplicabilityFormComponent implements OnInit {
  @Input() adoptionData: any;
  adoptionDetails: any;
  client_id: any;
  childname: any;
  dob: any;
  eligibility: any;
  childremovaldate: any;
  removalid: any;
  adoptionSubscription: Subscription;
  signature = '';
  constructor(
    private router: Router,
    private _commonHttpService: CommonHttpService,
    private adoptionService: TitleIveadoptionService
  ) {
   }

  ngOnInit() {
    this.client_id = this.router.routerState.snapshot.url.split('/')[4];
    this.removalid= this.router.routerState.snapshot.url.split('/')[5];
    this.adoptionSubscription = this.adoptionService.adoptionSubject$.subscribe(data => {
      if (data === 'ELIGIBILITY_UPDATED') {
        this.eligibility = this.adoptionService.eligibility;
      }
    });
    this.getAdoptionEligibilityInfo();
    this._commonHttpService.signature.subscribe(data => {
      this.signature = data;
    });
  }
  printDiv(divName) {
    const printContents = document.getElementById(divName).innerHTML;
    const originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}

getAdoptionEligibilityInfo() {
  if(this.adoptionData){
    this.adoptionDetails = this.adoptionData.adoptionEligibilityInfo[0];
    this.childname = this.adoptionDetails.childname;
    this.dob = this.adoptionDetails.dateofbirth;
    this.childremovaldate = this.adoptionDetails.childremovaldate;
  };
}

}
