import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionAttachmentsComponent } from './adoption-attachments.component';

describe('AdoptionAttachmentsComponent', () => {
  let component: AdoptionAttachmentsComponent;
  let fixture: ComponentFixture<AdoptionAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
