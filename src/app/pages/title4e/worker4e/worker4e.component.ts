import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { HighChartOptions } from '../_entities/home-dash-entities';
import { CommonHttpService } from '../../../@core/services';
import { Titile4eUrlConfig } from '../_entities/title4e-dashboard-url-config';
import { Title4eService } from '../../title4e/services/title4e.service';
import { PaginationInfo } from '../../../@core/entities/common.entities';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataStoreService } from '../../../@core/services';
declare var $: any;
@Component({

    // tslint:disable-next-line:component-selector
    selector: 'worker4e',
    templateUrl: './worker4e.component.html',
    styleUrls: ['./worker4e.component.scss']
})
export class Worker4eComponent implements OnInit {
    fosterCare: FormGroup;
    gap: FormGroup;
    adoption: FormGroup;
    acaform: FormGroup;
    caseCloseureChart: HighChartOptions;
    taskClosurechart: HighChartOptions;
    closedCasesToday: number;
    closedCasesWeek: number;
    averageclosurerate: number;
    closedCompliance: number;
    openCompliance: number;
    irRatio: number;
    arRatio: number;
    closedCases: number;
    openCases: number;
    totalCases: number;
    irCase: number;
    arCase: number;
    taskclosed: number;
    taskCompliance: number;
    totaltask: number;
    opentask: number;
    showCaseChart: boolean;
    showTaskChart: boolean;
    clientId = '';
    approvalData: any = [];
    selectedStatus = 'OPEN';
    add = {
        test2: '',
        test3: '',
        test4: ''
    };
    moreData: any;
    display = false;
    clientdata: any;
    bookFilteredList: {
        'CLIENTID': string; 'FULLNAME': string; 'REMOVALDATE': string; 'JURISDICTION': string; 'AGENCY': string; 'ELIGIBILITYSTATUS': string; 'TYPE': string; 'ASSIGNDATE': string;
        'STATUS': string; 'SPECIALISTNAME': string;
    }[];
    intakesData= [];
    intakesMasterData= [];
    fcCount: number;
    gapCount: number;
    adopCount: number;
    acaCount: number;
    gapintakesData: any;
    gapintakesMasterData: any;
    adoptionintakesData: any;
    adoptionintakesMasterData: any;
    acaintakesData: any;
    acaintakesMasterData: any;
    paginationInfo: PaginationInfo = new PaginationInfo();
    gapPaginationInfo: PaginationInfo = new PaginationInfo();
    adopPaginationInfo: PaginationInfo = new PaginationInfo();
    acaPaginationInfo: PaginationInfo = new PaginationInfo();
    constructor(
         private router: Router,
         private _commonService: CommonHttpService,
         private title4eService: Title4eService,
         private _formBuilder: FormBuilder,
         private _dataStore: DataStoreService
    ) { }
    user = {};
    ngOnInit() {
        this.fosterCare = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
         this.gap = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
         this.adoption = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
         this.acaform = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
         this.user = JSON.parse(localStorage.getItem('userProfile'));
        console.log('userData:', this.user);
        this.searchFosterCare();
        this.loadGap();
        this.loadAdoption();
        this.loadACA();
        this.aprovalDetails();
    }

    aprovalDetails() {
        const approvalDashboardParam = {
            'status': '{71,74,68}'
        };
        this.title4eService.getDashboardApprovalData(approvalDashboardParam).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.approvalData = response.data;
                console.log('this.approvalData =------'+JSON.stringify(this.approvalData));
            }
        });
    }


    searchFosterCare() {
        const fosterCareDashboardParam = {
            'status': '{70}',
            'eventType' : 'Fostercare',
            'fname' : this.fosterCare.value.fname ? this.fosterCare.value.fname.trim() : null,
            'lname' : this.fosterCare.value.lname ? this.fosterCare.value.lname.trim() : null,
            'clientId': this.fosterCare.value.clientId ? this.fosterCare.value.clientId : null
        };
        const pagination = {
            page: this.paginationInfo.pageNumber,
            limit: this.paginationInfo.pageSize,
        }
        this.title4eService.getDashboardData(fosterCareDashboardParam, pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.intakesData = response.data;
                this.intakesMasterData = response.data;
                if (response.data && response.data.length) {
                  this.fcCount = response.data[0].countdata;
                }
            }
        });
    }

    pageChanged(pageNumber: any) {
        this.paginationInfo.pageNumber = pageNumber.page;
        this.searchFosterCare();
    }
    gapPageChanged(pageNumber: any) {
        this.gapPaginationInfo.pageNumber =  pageNumber.page;
        this.loadGap();
    }
    adopPageChanged(pageNumber: any) {
        this.adopPaginationInfo.pageNumber =  pageNumber.page;
        this.loadAdoption();
    }
    acaPageChanged(pageNumber: any) {
        this.acaPaginationInfo.pageNumber =  pageNumber.page;
        this.loadACA();
    }
    loadGap() {
        const gapDashboardParam = {
            'status': '{73}',
            'eventType' : 'Gap',
            'fname' : this.gap.value.fname ? this.gap.value.fname.trim() : null,
            'lname' : this.gap.value.lname ? this.gap.value.lname.trim() : null,
            'clientId': this.gap.value.clientId ? this.gap.value.clientId : null
        };
        const pagination = {
            page: this.gapPaginationInfo.pageNumber,
            limit: this.gapPaginationInfo.pageSize,
        }
        this.title4eService.getDashboardData(gapDashboardParam, pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.gapintakesData = response.data;
                this.gapintakesMasterData = response.data;
                if (response.data && response.data.length) {
                    this.gapCount = response.data[0].countdata;
                }
            }
        });
    }
    loadAdoption() {
        const adoptionDashboardParam = {
            'status': '{67}',
            'eventType' : 'Adoption',
            'fname' : this.adoption.value.fname ? this.adoption.value.fname.trim() : null,
            'lname' : this.adoption.value.lname ? this.adoption.value.lname.trim() : null,
            'clientId': this.adoption.value.clientId ? this.adoption.value.clientId : null
        };
        const pagination = {
            page: this.adopPaginationInfo.pageNumber,
            limit: this.adopPaginationInfo.pageSize,
        }
        this.title4eService.getDashboardData(adoptionDashboardParam, pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.adoptionintakesData = response.data;
                this.adoptionintakesMasterData = response.data;
                if (response.data && response.data.length) {
                    this.adopCount = response.data[0].countdata;
                }
            }
        });
    }

    loadACA() {
        const adoptionDashboardParam = {
            'status': '{67}',
            'eventType' : 'ACA',
            'fname' : this.acaform.value.fname ? this.acaform.value.fname.trim() : null,
            'lname' : this.acaform.value.lname ? this.acaform.value.lname.trim() : null,
            'clientId': this.acaform.value.clientId ? this.acaform.value.clientId : null
        };
        const pagination = {
            page: this.acaPaginationInfo.pageNumber,
            limit: this.acaPaginationInfo.pageSize,
        }
        this.title4eService.getDashboardData(adoptionDashboardParam, pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.acaintakesData = response.data;
                this.acaintakesMasterData = response.data;
                if (response.data && response.data.length) {
                    this.acaCount = response.data[0].countdata;
                }
            }
        });
    }

    searchGap(id) {
        // this.title4eService.getClientId(id);
         if (id) {
             this._dataStore.setData('guardianship_clientid',id);
             this.router.navigate(['/pages/title4e/guardianship/' + id]);
         } else {
            //  console.log('error');
         }
     }

     searchAdoption(item) {
        // this.title4eService.getClientId(id);
         if (item) {
            this._dataStore.setData('adoption_clientid', item.clientid);
            this._dataStore.setData('adoption_removalid', item.removalid);
            this._dataStore.setData('adoption_dashboard', 'adoption');
            this.router.navigate(['/pages/title4e/adoption/' + item.clientid + '/' + item.removalid]);
         } else {
            //  console.log('error');
         }
     }


     searchACA(item) {
        // this.title4eService.getClientId(id);
         if (item) {
            this._dataStore.setData('adoption_clientid', item.clientid);
            this._dataStore.setData('adoption_removalid', item.removalid);
            this._dataStore.setData('adoption_dashboard', 'aca');
            this.router.navigate(['/pages/title4e/adoption/' + item.clientid + '/' + item.removalid]);
         } else {
            //  console.log('error');
         }
     }


     searchFoster(item) {
        if (item.client_id) {
            this.router.navigate(['/pages/title4e/foster-car/' + item.client_id + '/' + item.placement_id + '/' + item.removal_id]);
        }
    }

    resetadop() {
        this.adoption.reset();
        this.loadAdoption();
    }
    resetaca() {
        this.acaform.reset();
        this.loadACA();
    }

    resetgap() {
        this.gap.reset();
        this.loadGap();
    }
    reset() {
        this.fosterCare.reset();
        this.searchFosterCare();
    }

}

