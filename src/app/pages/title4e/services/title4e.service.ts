import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../@core/services';
import { PaginationRequest, PaginationInfo } from '../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';

@Injectable()
export class Title4eService {

  citizenData: any;
  SectionAData: any;
  SectionBData: any;
  SectionCData: any;
  clientIdData: any;
  removalIdData: any;
  paginationInfo: PaginationInfo = new PaginationInfo();

  constructor(private _commonHttpService: CommonHttpService) { }

  gapCitizenData(data) {
    this.citizenData = data;
    console.log(this.citizenData);
  }
  gepSectionA(data) {
    this.SectionAData = data;
  }
  gepSectionB(data) {
    this.SectionBData = data;
  }
  gepSectionC(data) {
    this.SectionCData = data;
  }
  getClientId(data) {
    this.clientIdData = data;
  }
  getRemovalId(id) {
    this.removalIdData = id;
  }

  getInvolvedPersonList(caseUUID) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: { objecttypekey: 'servicecase', objectid: caseUUID }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
      );
  }

  getDashboardData(requestParam: any, pagination = {
    page: this.paginationInfo.pageNumber,
    limit: this.paginationInfo.pageSize,
  }) {
    return this._commonHttpService
    .getPagedArrayList(
      new PaginationRequest({
        page: pagination.page,
        limit: pagination.limit,
        method: 'post',
        where: requestParam
      }),
      'titleive/ive/routing?filter'
    );
  }


  getDashboardApprovalData(requestParam: any) {
    return this._commonHttpService
    .getPagedArrayList(
      new PaginationRequest({
        page: 1,
        limit: 20,
        method: 'post',
        where: requestParam
      }),
      'titleive/ive/routingsv?filter'
    );
  }

  getUsersList() {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'IVEADOP' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      );
  }

  routingUpdate(data) {
    // http://localhost:3000/api/titleive/ive/iveAdoptionRouting
    return this._commonHttpService.create(data, 'titleive/ive/routingUpdate');
  }




}
