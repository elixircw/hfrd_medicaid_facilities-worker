import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestoreWidgetsComponent } from './restore-widgets.component';

describe('RestoreWidgetsComponent', () => {
  let component: RestoreWidgetsComponent;
  let fixture: ComponentFixture<RestoreWidgetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestoreWidgetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestoreWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
