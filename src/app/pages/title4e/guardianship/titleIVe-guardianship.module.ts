import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { QuillModule } from 'ngx-quill';
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule, 
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    MatDialogModule,
    MatStepperModule,
    MatPaginatorModule,
} from '@angular/material';
import { SignaturePadModule } from 'angular2-signaturepad';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxPrintModule } from 'ngx-print';
import { MatTooltipModule, MatChipsModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { GuardianshipComponent } from './titleIVe-guardianship.component';
import { GuardianshipResolverService } from './titleIVe-guardianship-resolver.service';
import { TitleIveGapeligibilityComponent } from './title-ive-gapeligibility/title-ive-gapeligibility.component';
import { TitleIveGapextensionComponent } from './title-ive-gapextension/title-ive-gapextension.component';
import { TitleIveGapEligibilityDetailsComponent } from './title-ive-gap-eligibility-details/title-ive-gap-eligibility-details.component';
import { AppropriateComponent } from './titleIVe-appropriateness/titleIVe-appropriate.component';
import { CitizenComponent } from './titleIVe-citizen/titleIVe-citizen.component';
import { GeneralreqComponent } from './titleIVe-generalReq/titleIVe-generalreq.component';
import { ReimbursementComponent } from './titleIVe-reimbursement/titleIVe-reimbursement.component';
import { TittleIveExtendableComponent } from './tittle-ive-extendable/tittle-ive-extendable.component';
import { GuardianshipRoutingModule } from './titleIVe-guardianship-routing.module'
@NgModule({
    imports: [NgxfUploaderModule,  
        MatDatepickerModule,
        MatTooltipModule, 
        MatChipsModule,
        MatButtonModule,
        MatIconModule,
        MatNativeDateModule,
        NgSelectModule,
        A2Edatetimepicker,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatRadioModule,
        MatTabsModule,
        MatCheckboxModule,
        MatListModule,
        MatCardModule,
        MatButtonToggleModule,
        FormMaterialModule,
        MatTableModule,
        MatExpansionModule,
        MatStepperModule,
        MatPaginatorModule,GuardianshipRoutingModule,
          MatDialogModule,  MatFormFieldModule, CommonModule,QuillModule,  FormsModule, ReactiveFormsModule, PaginationModule, 
          MatAutocompleteModule,  SignaturePadModule, NgxPrintModule],
    declarations: [  CitizenComponent, GeneralreqComponent, TittleIveExtendableComponent,
        AppropriateComponent, ReimbursementComponent, GuardianshipComponent,TitleIveGapeligibilityComponent, TitleIveGapextensionComponent,  TitleIveGapEligibilityDetailsComponent],
    providers: [GuardianshipResolverService],
    exports: []
})
export class GuardianshipModule {}
