import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';
import { FileUtils } from '../../../../@core/common/file-utils';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { DatePipe } from '@angular/common';
declare var Formio: any;

@Component({
  selector: 'title-ive-gapextension',
  templateUrl: './title-ive-gapextension.component.html',
  styleUrls: ['./title-ive-gapextension.component.scss'],
  providers: [DatePipe]
})
export class TitleIveGapextensionComponent implements OnInit {
  @Output() submitForReview: EventEmitter<any> = new EventEmitter();
  @Input() gapData: any; 
  gapExtensionData: any;
  client_id: any;
  cjamspid: any;
  countyname: any;
  userInfo: AppUser;
  gapExtensionForm: FormGroup;
  constructor( public dialog: MatDialog,
    private fb: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private storage: SessionStorageService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe) { }

  ngOnInit() {
    this.createFormGroup();
    this.client_id = this.activatedRoute.snapshot.paramMap.get('id');
    //this.cjamspid = this.userInfo.user.userprofile.cjamspid;
    console.log('------'+this.cjamspid);
    this.searchGapClientId();
    //this.isTabSwitched();
  }


  
  createFormGroup(){
    this.gapExtensionForm = this.fb.group({
          nameOfChild: [''],
          clientId: [''],
          countyOfJurisdiction: [''],
          dateofBirth: [''],
          gender: [''],
          nameOfGuardian: [''],
          clientIdOfGuardian: [''],
          guardiansubsidyid:  [''],
          dateofguardianshipdecree: [''],
          guardianshipApplicationDate: [''],
          eligibilitycriterialist: [''],
          nameofschoolprogram: [''],
          startdateofSecondaryeducationequivalentprogram: [''],
          nameofinstitution: [''],
          entrollmentStartdateofpostSecondaryorvocationaleducationinstitution: [''],
          nameofprogramactivity: [''],
          participattionStartdateofprogrampromotetoemployment: [''],
          nameofemployer: [''],
          hourspermonthemployed: [''],
          startdateofemployment: [''],
          disabilityType: [''],
          disabilityStartdate: [''],
          evaluationdocumentationdate: ['']
    });
  }



  isTabSwitched(){
    $('.gap a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("Extended")){
        this.searchGapClientId();
      }
    });
  }

  startForm() {
    var formData = this.setData();
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('titleIvEGapExtensionForm'), environment.formBuilderHost + `/form/5c9a6ed88d24c6329b180ad5`, {
      hooks: {
        beforeSubmit: (submission, next) => {

          var submissiondata =  this.submissionData();
          console.log('---------'+JSON.stringify(submissiondata));

          this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postGapEligibilityNExtension).subscribe(
            (res) => {
              this._alertService.success("Submitted successfully!");
            },
            (error) => {
                this._alertService.error("Submission Failed");
            });

          console.log("form before submit >>>", submission);
          // Only call next when we are ready.
          // next();
        }
      }
    }).then((form) => {


      form.submission = {
        data: formData
      };
      
      form.on('submit', submission => {
        console.log("form submit >>> ", submission);
      });

      form.on('change', formData => {

        if (formData && formData.changed && formData.changed.value &&  this.gapExtensionData) {

          // if(formData.data.eligibilitycriterialist === 'YES'){
          //   formData.data.startdateofSecondaryeducationequivalentprogram =  this.gapExtensionData.gapExtensionInfo[0].startdateofsecondaryeducationorequivalentprogram;
          //   formData.data.nameofschoolprogram = this.gapExtensionData.gapExtensionInfo[0].nameofsecondaryeducationorequivalentprogram;
          //   formData.data.entrollmentStartdateofpostSecondaryorvocationaleducationinstitution = this.gapExtensionData.gapExtensionInfo[0].startdateofpostsecondaryorvocationaleducation;
          //   formData.data.nameofinstitution = this.gapExtensionData.gapExtensionInfo[0].nameofpostsecondaryorvocationaleducation;
          //   formData.data.participattionStartdateofprogrampromotetoemployment = this.gapExtensionData.gapExtensionInfo[0].startdateofpromotebarrierstoemploymentprogram;
          //   formData.data.nameofprogramactivity = this.gapExtensionData.gapExtensionInfo[0].nameofpromotebarrierstoemploymentprogram;
          //   formData.data.startdateofemployment = this.gapExtensionData.gapExtensionInfo[0].startdateofemployment;
          //   formData.data.hourspermonthemployed = this.gapExtensionData.gapExtensionInfo[0].hourspermonthemployed;
          //   formData.data.nameofemployer = this.gapExtensionData.gapExtensionInfo[0].nameofemployer;
          //   formData.data.disabilityType = this.gapExtensionData.gapExtensionInfo[0].childdisabilitytype;
          //   formData.data.disabilityStartdate = this.gapExtensionData.gapExtensionInfo[0].childdisabilitystartdate;
          //   formData.data.evaluationdocumentationdate = this.gapExtensionData.gapExtensionInfo[0].childdisabilityevaluationdocumentiondate;
          // }

          form.submission = {
            data: formData.data
          }; 
        }



        // if (formData && formData.changed && formData.changed.value &&  this.gapExtensionData) {
        //   if (formData.changed.value.clientInformation) {
        //     formData.data.nameOfChild = this.gapExtensionData.demographicsInfo[0].childname;
        //     formData.data.clientId = this.gapExtensionData.demographicsInfo[0].client_id;
        //     formData.data.countyOfJurisdiction = this.gapExtensionData.demographicsInfo[0].countyofjurisdiction;
        //     formData.data.dateofBirth = this.gapExtensionData.demographicsInfo[0].dateofbirth;
        //     formData.data.gender = this.gapExtensionData.demographicsInfo[0].gender;
        //     formData.data.nameOfGuardian = this.gapExtensionData.demographicsInfo[0].childguardianname;
        //     formData.data.clientIdOfGuardian = this.gapExtensionData.demographicsInfo[0].childguardianid;
        //     formData.data.dateofguardianshipdecree= this.gapExtensionData.gapEligibilityInfo[0].dateofcourtorderguardianshipfinalization;
        //     formData.data.guardianshipApplicationDate= this.gapExtensionData.generalInfo[0].guardianshipapplicationdate;
        //   }

        //   if (formData.changed.value.eligibilityCriteria) {
        //     formData.data.eligibilitycriterialist =  this.gapExtensionData.gapEligibilityInfo[0].childmeetcontinueeligibilitycriteria;
        //   }


        //   if(formData.data.eligibilitycriterialist === 'YES'){
        //     formData.data.startdateofSecondaryeducationequivalentprogram =  this.gapExtensionData.gapExtensionInfo[0].startdateofsecondaryeducationorequivalentprogram;
        //     formData.data.nameofschoolprogram = this.gapExtensionData.gapExtensionInfo[0].nameofsecondaryeducationorequivalentprogram;
        //     formData.data.entrollmentStartdateofpostSecondaryorvocationaleducationinstitution = this.gapExtensionData.gapExtensionInfo[0].startdateofpostsecondaryorvocationaleducation;
        //     formData.data.nameofinstitution = this.gapExtensionData.gapExtensionInfo[0].nameofpostsecondaryorvocationaleducation;
        //     formData.data.participattionStartdateofprogrampromotetoemployment = this.gapExtensionData.gapExtensionInfo[0].startdateofpromotebarrierstoemploymentprogram;
        //     formData.data.nameofprogramactivity = this.gapExtensionData.gapExtensionInfo[0].nameofpromotebarrierstoemploymentprogram;
        //     formData.data.startdateofemployment = this.gapExtensionData.gapExtensionInfo[0].startdateofemployment;
        //     formData.data.hourspermonthemployed = this.gapExtensionData.gapExtensionInfo[0].hourspermonthemployed;
        //     formData.data.nameofemployer = this.gapExtensionData.gapExtensionInfo[0].nameofemployer;
        //     formData.data.disabilityType = this.gapExtensionData.gapExtensionInfo[0].childdisabilitytype;
        //     formData.data.disabilityStartdate = this.gapExtensionData.gapExtensionInfo[0].childdisabilitystartdate;
        //     formData.data.evaluationdocumentationdate = this.gapExtensionData.gapExtensionInfo[0].childdisabilityevaluationdocumentiondate;
        //   }

        //   form.submission = {
        //     data: formData.data
        //   }; 
        // }
        
      });
      form.on('render', formData => {
        console.log("form render >>> ", formData);
      });
      form.on('error', formData => {
        console.log("form error >>> ", formData);
      });
    }).catch((err) => {
      console.log("Form Error:: ", err);
    });
  }


 

  setData(){
    interface LooseObject {
      [key: string]: any
    };

    var data: LooseObject = {};

          if(this.gapExtensionData.demographicsInfo && this.gapExtensionData.demographicsInfo.length > 0){
              data.nameOfChild = this.gapExtensionData.demographicsInfo[0].childname;
              data.clientId = this.gapExtensionData.demographicsInfo[0].client_id;
              //data.countyOfJurisdiction = this.gapExtensionData.demographicsInfo[0].countyofjurisdiction;
              data.dateofBirth = this.gapExtensionData.demographicsInfo[0].dateofbirth;
              data.gender = this.gapExtensionData.demographicsInfo[0].gender;
              data.nameOfGuardian = this.gapExtensionData.demographicsInfo[0].childguardianname;
              data.clientIdOfGuardian = this.gapExtensionData.demographicsInfo[0].childguardianid;
              data.dateofguardianshipdecree= this.gapExtensionData.demographicsInfo[0].dateofcourtorderguardianshipfinalization;
            }
            data.countyOfJurisdiction = this.countyname;

            data.guardianshipApplicationDate= (this.gapExtensionData.generalInfo && this.gapExtensionData.generalInfo.length > 0) ? this.gapExtensionData.generalInfo[0].guardianshipapplicationdate : null;
            if(this.gapExtensionData.otherDeterminationInfo && this.gapExtensionData.otherDeterminationInfo.length > 0){
              data.eligibilityStatus = this.gapExtensionData.otherDeterminationInfo[0].gapeligibilitystatus ? this.gapExtensionData.otherDeterminationInfo[0].gapeligibilitystatus : "";
            }
            data.eligibilitycriterialist = (this.gapExtensionData.gapExtensionInfo && this.gapExtensionData.gapExtensionInfo.length > 0) ? this.gapExtensionData.gapExtensionInfo[0].childmeetcontinueeligibilitycriteria : "";


            if(data.eligibilitycriterialist === 'YES' && 
            this.gapExtensionData.gapExtensionInfo && 
            this.gapExtensionData.gapExtensionInfo.length > 0){
              data.startdateofSecondaryeducationequivalentprogram =  this.gapExtensionData.gapExtensionInfo[0].startdateofsecondaryeducationorequivalentprogram;
              data.nameofschoolprogram = this.gapExtensionData.gapExtensionInfo[0].nameofsecondaryeducationorequivalentprogram;
              data.entrollmentStartdateofpostSecondaryorvocationaleducationinstitution = this.gapExtensionData.gapExtensionInfo[0].startdateofpostsecondaryorvocationaleducation;
              data.nameofinstitution = this.gapExtensionData.gapExtensionInfo[0].nameofpostsecondaryorvocationaleducation;
              data.participattionStartdateofprogrampromotetoemployment = this.gapExtensionData.gapExtensionInfo[0].startdateofpromotebarrierstoemploymentprogram;
              data.nameofprogramactivity = this.gapExtensionData.gapExtensionInfo[0].nameofpromotebarrierstoemploymentprogram;
              data.startdateofemployment = this.gapExtensionData.gapExtensionInfo[0].startdateofemployment;
              data.hourspermonthemployed = this.gapExtensionData.gapExtensionInfo[0].hourspermonthemployed;
              data.nameofemployer = this.gapExtensionData.gapExtensionInfo[0].nameofemployer;
              data.disabilityType = this.gapExtensionData.gapExtensionInfo[0].childdisabilitytype;
              data.disabilityStartdate = this.gapExtensionData.gapExtensionInfo[0].childdisabilitystartdate;
              data.evaluationdocumentationdate = this.gapExtensionData.gapExtensionInfo[0].childdisabilityevaluationdocumentiondate;
            }
        return data;
  }



  mapData(){
          if(this.gapExtensionData.demographicsInfo && this.gapExtensionData.demographicsInfo.length > 0){

            this.gapExtensionForm.patchValue({
                nameOfChild : this.gapExtensionData.demographicsInfo[0].childname,
                clientId : this.gapExtensionData.demographicsInfo[0].client_id,
                dateofBirth : this.gapExtensionData.demographicsInfo[0].dateofbirth,
                gender : this.gapExtensionData.demographicsInfo[0].gender,
                nameOfGuardian : this.gapExtensionData.demographicsInfo[0].childguardianname,
                clientIdOfGuardian : this.gapExtensionData.demographicsInfo[0].childguardianid,
                dateofguardianshipdecree: this.gapExtensionData.demographicsInfo[0].dateofcourtorderguardianshipfinalization,
                guardiansubsidyid: this.gapExtensionData.demographicsInfo[0].guardiansubsidyid
              });
            }

         
            this.gapExtensionForm.patchValue({
              countyOfJurisdiction : this.gapExtensionData.summaryInfo && this.gapExtensionData.summaryInfo.length > 0 ? this.gapExtensionData.summaryInfo[0].childjurisdiction : null,
              guardianshipApplicationDate : (this.gapExtensionData.generalInfo && this.gapExtensionData.generalInfo.length > 0) ? this.gapExtensionData.generalInfo[0].guardianshipapplicationdate : null
            });
            
            if(this.gapExtensionData.otherDeterminationInfo && this.gapExtensionData.otherDeterminationInfo.length > 0){
              this.gapExtensionForm.patchValue({
                eligibilityStatus : this.gapExtensionData.otherDeterminationInfo[0].gapeligibilitystatus ? this.gapExtensionData.otherDeterminationInfo[0].gapeligibilitystatus : ""
              });
            }
            
            this.gapExtensionForm.patchValue({
              eligibilitycriterialist : (this.gapExtensionData.gapExtensionInfo && this.gapExtensionData.gapExtensionInfo.length > 0) ? this.gapExtensionData.gapExtensionInfo[0].childmeetcontinueeligibilitycriteria : ""
            });

            if( this.gapExtensionData.gapExtensionInfo && 
            this.gapExtensionData.gapExtensionInfo.length > 0 && 
            this.gapExtensionData.gapExtensionInfo[0].childmeetcontinueeligibilitycriteria === 'YES'){

              this.gapExtensionForm.patchValue({
                  startdateofSecondaryeducationequivalentprogram :  this.gapExtensionData.gapExtensionInfo[0].startdateofsecondaryeducationorequivalentprogram,
                  nameofschoolprogram : this.gapExtensionData.gapExtensionInfo[0].nameofsecondaryeducationorequivalentprogram,
                  entrollmentStartdateofpostSecondaryorvocationaleducationinstitution : this.gapExtensionData.gapExtensionInfo[0].startdateofpostsecondaryorvocationaleducation,
                  nameofinstitution : this.gapExtensionData.gapExtensionInfo[0].nameofpostsecondaryorvocationaleducation,
                  participattionStartdateofprogrampromotetoemployment : this.gapExtensionData.gapExtensionInfo[0].startdateofpromotebarrierstoemploymentprogram,
                  nameofprogramactivity : this.gapExtensionData.gapExtensionInfo[0].nameofpromotebarrierstoemploymentprogram,
                  startdateofemployment : this.gapExtensionData.gapExtensionInfo[0].startdateofemployment,
                  hourspermonthemployed : this.gapExtensionData.gapExtensionInfo[0].hourspermonthemployed,
                  nameofemployer : this.gapExtensionData.gapExtensionInfo[0].nameofemployer,
                  disabilityType : this.gapExtensionData.gapExtensionInfo[0].childdisabilitytype,
                  disabilityStartdate : this.gapExtensionData.gapExtensionInfo[0].childdisabilitystartdate,
                  evaluationdocumentationdate : this.gapExtensionData.gapExtensionInfo[0].childdisabilityevaluationdocumentiondate
              });
            }
  }

  submissionData(){

    interface LooseObject {
      [key: string]: any
    };

    var gender = '';
    if(this.gapExtensionForm.value.gender === 'M'){
      gender = 'Male';
    }else if(this.gapExtensionForm.value.gender === 'F'){
      gender = 'Female';
    }else{
      gender = 'Other';
    }


    var submissiondata: LooseObject = {
      "cjamsPid": Number(this.gapExtensionForm.value.clientId),
      "guardiansubsidyid":Number(this.gapExtensionForm.value.guardiansubsidyid),
      "removalid": this.gapExtensionData.reimbursementInfo[0].removalid ? this.gapExtensionData.reimbursementInfo[0].removalid : '99999',
      "pagesnapshot": this.gapExtensionForm.getRawValue(),
      "reDetermination": {
        "payload": {
          "name": "EXT_GAP",
          "__metadataRoot": {},
          "Objects": [
            {
              "ChildMeetContinueEligibilityCriteria": this.gapExtensionForm.value.eligibilitycriterialist,
              "person": {
                "DateOfBirth": this.dateConversion(this.gapExtensionForm.value.dateofBirth),
                "ChildDisabilityEvaluationDocumentionDate": this.dateConversion(this.gapExtensionForm.value.evaluationdocumentationdate),
                "StartDateOfSecondaryEducationOrEquivalentProgram": this.dateConversion(this.gapExtensionForm.value.startdateofSecondaryeducationequivalentprogram),
                "NameOfpostSecondaryOrVocationalEducation": this.gapExtensionForm.value.nameofinstitution === "" ? null : this.gapExtensionForm.value.nameofinstitution,
                "CountyOfJurisdiction_LDSS": this.gapExtensionForm.value.countyOfJurisdiction,
                "Gender": gender,
                "NameOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment": this.gapExtensionForm.value.nameofprogramactivity === "" ? null : this.gapExtensionForm.value.nameofprogramactivity,
                "Name": this.gapExtensionForm.value.nameOfChild,
                "ChildDisabilityType": this.gapExtensionForm.value.disabilityType === '' ? null : this.gapExtensionForm.value.disabilityType,
                "StartDateOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment":  this.dateConversion(this.gapExtensionForm.value.participattionStartdateofprogrampromotetoemployment),
                "HoursPerMonthEmployed": (this.gapExtensionForm.value.hourspermonthemployed === '' || this.gapExtensionForm.value.hourspermonthemployed === null) ? 0 : this.gapExtensionForm.value.hourspermonthemployed,
                "StartDateOfEmployment": this.dateConversion(this.gapExtensionForm.value.startdateofemployment),
                "__metadata": {
                  "#type": "Person",
                  "#id": "Person_id_1"
                },
                "ChildDisabilityStartDate": this.dateConversion(this.gapExtensionForm.value.disabilityStartdate),
                "StartDateOfpostSecondaryOrVocationalEducation": this.dateConversion(this.gapExtensionForm.value.entrollmentStartdateofpostSecondaryorvocationaleducationinstitution),
                "NameOfSecondaryEducationOrEquivalentProgram": this.gapExtensionForm.value.nameofschoolprogram === "" ? null : this.gapExtensionForm.value.nameofschoolprogram,
                "NameOfEmployer": this.gapExtensionForm.value.nameofemployer === "" ? null : this.gapExtensionForm.value.nameofemployer,
              },
              "GuardianshipFinalizationDate_FinalizationDateOfCourtOrder": this.dateTimeConversion(this.gapExtensionForm.value.dateofguardianshipdecree),
              "__metadata": {
                "#type": "Application",
                "#id": "Application_id_1"
              },
              "status": {
                "GAPEligibilityStatus": "Eligible Reimbursable",
                "__metadata": {
                  "#type": "Status",
                  "#id": "Status_id_1"
                },
                "EXTGAPEligibilityStatus": null
              }
            }
          ]
        }
      }
    } ;
 
    this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postGapEligibilityNExtension).subscribe(
      (res) => {
        this.submitForReview.emit();
        this._alertService.success("Submitted successfully!");
      },
      (error) => {
          this._alertService.error("Submission Failed");
      });


  }


  searchGapClientId() {
        if(this.gapData) {
            this.gapExtensionData = this.gapData;
            this.mapData();
        };
  }


  loadCounty(countyidinput) {
    this._commonHttpService.getArrayList(
      { where: { activeflag: '1', state: 'MD', countyid: countyidinput}, nolimit: true, method: 'get' },
      'admin/county' + '?filter'
    ).subscribe(res => {
      if(res && res.length > 0){
        this.countyname = res[0].countyname;
      }
      //this.startForm();
    });
  }


    dateConversion(date){
      if(date === undefined || date === null || date === ''){
        return null;
      }else{
        return this.datePipe.transform(date,"MM/dd/yyyy");
      }
    }


    dateTimeConversion(date){
      if(date === undefined || date === null || date === ''){
        return null;
      }else{
        return this.datePipe.transform(date,"MM/dd/yyyy HH:mm:ss");
      }
    }


}
