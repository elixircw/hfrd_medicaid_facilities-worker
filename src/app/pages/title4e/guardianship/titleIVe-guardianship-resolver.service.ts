import { Injectable } from '@angular/core';
import { CommonHttpService, AuthService } from '../../../@core/services';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DataStoreService } from '../../../@core/services';

@Injectable()
export class GuardianshipResolverService {

  constructor(private _commonHttpService: CommonHttpService, private router: Router,
    private _dataStore: DataStoreService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.getGapInfo();
  }

  getGapInfo() {
    var client_id= this._dataStore.getData('guardianship_clientid');
    return this._commonHttpService.getAll(
      'titleive/gap/gap-eligibility-worksheet/' + client_id
        );
}



}
