import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { Title4eService } from '../../services/title4e.service';
import { AuthService } from '../../../../@core/services';
import { AppConstants } from '../../../../@core/common/constants';
declare var Formio: any;

@Component({
  selector: 'title-ive-gap-eligibility-details',
  templateUrl: './title-ive-gap-eligibility-details.component.html',
  styleUrls: ['./title-ive-gap-eligibility-details.component.scss']
})
export class TitleIveGapEligibilityDetailsComponent implements OnInit {

  isSupervisor = false;
  isSpecialist = false;
  getUsersList: any[];
  selectedPerson: any;
  client_id: String;
  removal_id: String;  
  userInfo: any;
  gapAuditList: any = [];
  gapSendForApprovalDisable:boolean = true;
  approveRejectDisabled: boolean = true;
  approvalid : any;
  gapmessages : any;
  gapwarnings : any;
  warningdate: any;
  selectedItem: any;
  showSnapshot: boolean = false;
  selectedItemSnapshot: any;
  eligibilityDetailsChilds: boolean = true;
  constructor( private router: Router,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    public dialog: MatDialog,
    private title4eService: Title4eService,
    private storage: SessionStorageService,
    private fb: FormBuilder,
    private _authService: AuthService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    this.isSpecialist = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SPECIALIST);
    this.userInfo = this._authService.getCurrentUser();

    if(this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SUPERVISOR){
      this.isSupervisor = true;
    }

    if(this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SPECIALIST){
      this.isSpecialist = true;
    }

    this.client_id = this.activatedRoute.snapshot.paramMap.get('id');
    this.isTabSwitched();
  }

  isTabSwitched(){
    $('.intake-tabs a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("DETAILS")){
        this.gapAuditList = null;
        this.eligibilityDetails();
      }
    });
  }

  
  showSupervisorList() {
    this.title4eService.getUsersList().subscribe(result => {
        this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    });
    (<any>$('#assign')).modal('show');
  }

  selectPerson(item) {
    this.selectedPerson = item;
  }


sendForApproval(){
    if(this.selectedPerson){
    this._commonHttpService.create(
      {
        "where":{
          "clientid": this.client_id,
          "removalid": this.removal_id,
          "status":"{73}",
          "eventType":"Gap"
        },
          "page": 1,
          "limit" : 10
      },
      'titleive/ive/approval-status'
    ).subscribe(response => {
      if(response && response.data && response.data.length > 0){
          console.log('response --- '+JSON.stringify(response));
          this.routingUpdate(response);
      }
    },
      (error) => {
        this._alertService.error('Unable to process');
        return false;
      }
    );
    }else{
        this._alertService.error('Select a Supervisor');
    }

  }

  toggleChildren(){
    this.eligibilityDetailsChilds = !this.eligibilityDetailsChilds;
}


  routingUpdate(sendForApprovalResponse){
    this._commonHttpService.create(
      {
        "where": {
          "assignedtoid": this.selectedPerson.userid,
          "eventcode": "GAAR",
          //"servicecaseid": sendForApprovalResponse.data[0].caseid,
          "status": "SplReview",
          "notifymsg": "Request for Review",
          "routeddescription": "route",
          "comments": "Request for Review",
          "gapagreementid": sendForApprovalResponse.data[0].sp_ive_status_approval,    
          "signText": "Signed"
        }
      },
      'titleive/ive/routingUpdate'
    ).subscribe(response => {
      this.eligibilityDetails();
      (<any>$('#assign')).modal('hide');
      this._alertService.success('Sent For Approval');
    },
      (error) => {
        this._alertService.error('Unable To Process');
        return false;
      }
    );
  }


  gaperrors(item,logtype){
    this.gapmessages = null;
    this.gapwarnings = null;
    this.selectedItem = item;
    this.showSnapshot = false;
    this._commonHttpService.getById(item.gapauditid,'titleive/gap/audit-messages').subscribe(
        (response: any) => {
            this.gapmessages = [];
            this.gapwarnings = [];
            this.warningdate = item.insertedon;
            if(response && response.data && response.data.length > 0){
                this.gapmessages = response.data;
                for(let gapwarning of response.data){
                    this.selectedItemSnapshot = gapwarning.pagesnapshot;
                    if(gapwarning.severity === 'Warning'){
                        this.gapwarnings.push(gapwarning);
                    }
                }
            }
           
            if(logtype === 'W'){
                this.gapmessages = null;
            }

            if(logtype === 'L'){
                this.gapwarnings = null;
            }

        },
        (error) => {
          console.log(error);
          return false;
        }
      );
}

  
  eligibilityDetails(){
    this.showSnapshot = false;
    this._commonHttpService
    .getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 10,
        where: { clientid: this.client_id }
      },
      'tb_ive_gapaudit/list?filter'
    ).subscribe(res => {
        if (res && res.length>0) {
            this.gapAuditList = res;
            if(this.gapAuditList && this.gapAuditList.length > 0){
                this.gapSendForApprovalDisable = (this.gapAuditList[0].approvalstatus === null || this.gapAuditList[0].approvalstatus === '') ? false : true;

                if(this.gapAuditList[0].approvalstatus === 'PENDING'){
                    this.approveRejectDisabled = false;
                    this.approvalid = this.gapAuditList[0].approvalid; 
                }
            }
        }
      });
}




updateSnapshot({checked}){

  const createForm = (form: any) => {
    form.submission = {
      data: this.selectedItemSnapshot
    };

    form.on('submit', submission => {
    });

    form.on('change', formData => {
      form.submission = {
        data: formData.data
      };
    });

    form.on('render', formData => {
      console.log('form render >>> ', formData);
    });

    form.on('error', formData => {
      console.log('form error >>> ', formData);
    });
  };

  if(checked){
    if(this.selectedItem && this.selectedItem.category === 'I'){
      Formio.setToken(this.storage.getObj('fbToken'));
      Formio.baseUrl = environment.formBuilderHost;
      Formio.createForm(document.getElementById('titleIvEGapEligibilityFormReview'), environment.formBuilderHost + `/form/5c9a653b8d24c6329b180ad3`, {}).then(createForm).catch((err) => {
        console.log('Form Error:: ', err);
      });
    }

    if(this.selectedItem && this.selectedItem.category === 'R'){
      Formio.setToken(this.storage.getObj('fbToken'));
      Formio.baseUrl = environment.formBuilderHost;
      Formio.createForm(document.getElementById('titleIvEGapExtensionFormReview'), environment.formBuilderHost + `/form/5c9a6ed88d24c6329b180ad5`, {}).then(createForm).catch((err) => {
        console.log('Form Error:: ', err);
      });
    }
  } else {
    document.getElementById('titleIvEGapEligibilityFormReview').innerHTML = "";
    document.getElementById('titleIvEGapExtensionFormReview').innerHTML = "";
  }
}


ApproveReject(status){
  this._commonHttpService.create(
    {
      "where":{
        "approval_id": this.approvalid,
        "approval_status": status,
        "placement_type":"Gap"
      },
        "page": 1,
        "limit" : 10
    },
    'titleive/ive/ivespv-approval'
  ).subscribe(response => {
    if(response && response.data && response.data.length > 0){
      this.approveRejectDisabled= true;
      this.approvalid = null;
      this.eligibilityDetails();
    }
  },
    (error) => {
      this._alertService.error('Unable to process');
      return false;
    }
  );

}

}
