import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PeriodTablePopUpComponent } from '../../period-table-pop-up/period-table-pop-up.component';
import { Title4eService } from '../../services/title4e.service';
declare var $: any;
import * as moment from 'moment';
import { DropdownModel } from '../../../../@core/entities/common.entities';
@Component({

    selector: 'citizen',
    templateUrl: './titleIVe-citizen.component.html',
    styleUrls: ['./titleIVe-citizen.component.scss']
})
export class CitizenComponent implements OnInit {

    @Input() CitizenInfoForm: FormGroup;
    countyDropDownData: any;
    constructor(
        private router: Router,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        public dialog: MatDialog,
        private title4eService: Title4eService,
        private fb: FormBuilder
    ) { }

    CitizenInfoReset() {
        this.CitizenInfoForm.reset();
    }
    ngOnInit() {
        this.getCountyDropdown().subscribe(res => {
            console.log(JSON.stringify(res));
            this.countyDropDownData = res;
        });
    }
    getCountyDropdown() {
        return this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                'admin/county?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }
}
