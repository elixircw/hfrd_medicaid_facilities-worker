import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TittleIveExtendableComponent } from './tittle-ive-extendable.component';

describe('TittleIveExtendableComponent', () => {
  let component: TittleIveExtendableComponent;
  let fixture: ComponentFixture<TittleIveExtendableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TittleIveExtendableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TittleIveExtendableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
