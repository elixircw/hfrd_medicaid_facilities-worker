import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'extendable',
  templateUrl: './tittle-ive-extendable.component.html',
  styleUrls: ['./tittle-ive-extendable.component.scss']
})
export class TittleIveExtendableComponent implements OnInit {
  @Input() extendableForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
