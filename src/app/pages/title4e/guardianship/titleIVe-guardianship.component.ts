import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { AlertService } from '../../../@core/services/alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Title4eService } from '../services/title4e.service';
import { NavigationUtils } from '../../../pages/_utils/navigation-utils.service';
import { PeriodTablePopUpComponent } from '../period-table-pop-up/period-table-pop-up.component';
declare var $: any;
import * as moment from 'moment';
import { Titile4eUrlConfig } from '../_entities/title4e-dashboard-url-config';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';

import { AuthService } from '../../../@core/services';
import { AppConstants } from '../../../@core/common/constants';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'guardianship',
    templateUrl: './titleIVe-guardianship.component.html',
    styleUrls: ['./titleIVe-guardianship.component.scss']
})
export class GuardianshipComponent implements OnInit {

    client_id: String;
    clientId: any;
    removalId: any;
    userInfo: any;
    gapData: any;
    placementId: any;
    summaryinfo: any;
    child_name: any;
    agency: any;
    constructor(
        private _commonHttpService: CommonHttpService,
        public dialog: MatDialog,
        private navigateutil: NavigationUtils,
        private route: ActivatedRoute,
        private _authService: AuthService,
        private activatedRoute: ActivatedRoute
    ) { 
      this.route.data.subscribe(response => {
        this.gapData = response.gapData;
        this.summaryinfo = this.gapData.summaryInfo && this.gapData.summaryInfo.length > 0 ? this.gapData.summaryInfo[0] : null ;
        this.child_name = this.gapData.demographicsInfo && this.gapData.demographicsInfo.length > 0 ? this.gapData.demographicsInfo[0].childname : null;
        this.agency = this.gapData.generalInfo && this.gapData.generalInfo.length > 0 ? this.gapData.generalInfo[0].childagency : null;
        this.clientId = this.gapData.reimbursementInfo && this.gapData.reimbursementInfo.length > 0 ? this.gapData.reimbursementInfo[0].client_id : null;
        this.placementId = this.gapData.reimbursementInfo && this.gapData.reimbursementInfo.length > 0 ? this.gapData.reimbursementInfo[0].placementid : null;
        this.removalId = this.gapData.reimbursementInfo && this.gapData.reimbursementInfo.length > 0 ? this.gapData.reimbursementInfo[0].removalid : null;
      });

    }

    ngOnInit() {
        this.userInfo = this._authService.getCurrentUser();
        this.client_id = this.activatedRoute.snapshot.paramMap.get('id');
       // this.searchGapClientId();
    }
    navigateCaseNumber(data: any){
      this.navigateutil.routToServiceCase(data);
    }
    navigateToFostercare(){
      this.navigateutil.loadFostcareIVE(this.clientId, this.removalId, this.placementId );
    }

    submitDetermination(){
      $('#gapEligibilityDetailsBtnTrigger').click();
    }


    searchGapClientId() {
        this._commonHttpService.getAll('titleive/gap/gap-eligibility-worksheet/' + this.client_id).subscribe(
          (response: any) => {
            this.gapData = response;
            this.summaryinfo = response.summaryInfo && response.summaryInfo.length > 0 ? response.summaryInfo[0] : null ;
            this.child_name = this.gapData.demographicsInfo && this.gapData.demographicsInfo.length > 0 ? this.gapData.demographicsInfo[0].childname : null;
            this.agency = this.gapData.generalInfo && this.gapData.generalInfo.length > 0 ? this.gapData.generalInfo[0].childagency : null;
          },
          (error) => {
            console.log(error);
            return false;
          }
        );
    }

}


