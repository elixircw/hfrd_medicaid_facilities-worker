import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Title4eService } from '../../services/title4e.service';
import { PeriodTablePopUpComponent } from '../../period-table-pop-up/period-table-pop-up.component';
declare var $: any;
import * as moment from 'moment';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'appropriate',
    templateUrl: './titleIVe-appropriate.component.html',
    styleUrls: ['./titleIVe-appropriate.component.scss']
})
export class AppropriateComponent implements OnInit {

    @Input() appropriateForm: FormGroup;
    constructor(
        private router: Router,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        public dialog: MatDialog,
        private title4eService: Title4eService,
        private fb: FormBuilder
    ) {  }

    
    ngOnInit() {

    }
    get otherHousehold() { return this.appropriateForm.get('otherHousehold') as FormArray; }

}
