import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PeriodTablePopUpComponent } from '../../period-table-pop-up/period-table-pop-up.component';
import { Title4eService } from '../../services/title4e.service';
declare var $: any;
import * as moment from 'moment';
import { constants } from 'fs';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'reimbursement',
    templateUrl: './titleIVe-reimbursement.component.html',
    styleUrls: ['./titleIVe-reimbursement.component.scss']
})
export class ReimbursementComponent implements OnInit {

    @Input() reimbursementForm: FormGroup;
    personListdata: any[];

    constructor(
        private router: Router,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        public dialog: MatDialog,
        private title4eService: Title4eService,
        private fb: FormBuilder,
    ) {

    }

    ngOnInit() {
        console.log(this.reimbursementForm.value);
        this.getPersonsList();
    }

    getPersonsList() {
        // console.log("myid",this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID))
        // const casnumber = '6404b59c-3be4-40ec-91c5-018653fcfd03';
        const casnumber = this.reimbursementForm.get('servicecaseid').value;
         this._commonHttpService
          .getPagedArrayList(
            new PaginationRequest({
              page: 1,
              limit: 20,
              method: 'get',
              where: {  objecttypekey: 'servicecase' , objectid: casnumber}
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
            .subscribe((result) => {
                console.log(result.data);
                this.personListdata = result.data;
                this.personListdata = this.personListdata.map(v => {
                    return {
                        name: v.firstname + ' ' + v.lastname
                    };
                });
                console.log(this.personListdata);
          });
          //  })
            // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          // );
      }
    get otherSibling() { return this.reimbursementForm.get('otherSibling') as FormArray; }

}
