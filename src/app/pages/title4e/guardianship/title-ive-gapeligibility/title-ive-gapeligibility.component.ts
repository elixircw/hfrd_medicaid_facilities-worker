import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';
import { FileUtils } from '../../../../@core/common/file-utils';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { environment } from '../../../../../environments/environment';
import { DatePipe } from '@angular/common';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
declare var Formio: any;

@Component({
  selector: 'title-ive-gapeligibility',
  templateUrl: './title-ive-gapeligibility.component.html',
  styleUrls: ['./title-ive-gapeligibility.component.scss'],
  providers: [DatePipe]
})
export class TitleIveGapeligibilityComponent implements OnInit {
  @Output() submitForReview: EventEmitter<any> = new EventEmitter();
  @Input() gapData: any;
  client_id = '';
  gapEligibilityData: any;
  formData: any;
  countyname: any;
  safetyAndAppropriateness = false;
  gapEligibilityForm: FormGroup;
  constructor(public dialog: MatDialog,
    private fb: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private storage: SessionStorageService,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.createFormGroup();
    this.client_id = this.activatedRoute.snapshot.paramMap.get('id');
    this.searchGapClientId();
    //this.isTabSwitched();
  }

  createFormGroup(){
    this.gapEligibilityForm = this._formBuilder.group({
        nameofchild: [''],
        clientId: [''],
        countyofjurisdiction: [''],
        dateofbirth: [''],
        gender: [''],
        nameofGuardian: [''],
        clientIDofguardian: [''],
        dateoffullapprovalasfosterparent: [''],
        dateandtimeofguardianshipdecreecourtorder: [''],
        guardiansubsidyid:  [''],
        guardianshipApplicationDate: [''],
        latestfostercareplacementstartdatewithprospectiveguardian: [''],
        guardiandetailsName: [''],
        guardiandetailsClientId: [''],
        guardiandetailsAgreementSignedDate: [''],
        guardiandetailsIsRelative: [''],
        guardiandetailsRelationshipID: [''],
        guardiandetailsRelationship: [''],
        guardiandetailsNoofHhMembers: [''],
        secondGuardianExists: [''],
        secondGuardianSecondGuardianName: [''],
        secondGuardianClientId: [''],
        secondGuardianAgreementSignedDate: [''],
        secondGuardianIsRelative: [''],
        secondGuardianRelationshipID: [''],
        secondGuardianRelationship: [''],
        secondGuardianLivingwithGuardian: [''],
        secondGuardianNoofHhMembers: [''],
        successorGuardianExists: [''],
        successorGuardianName: [''],
        successorGuardianId: [''],
        dateofSuccessionAddendum: [''],
        fosterHomeApprover: [''],
        appropriatePermanencyForChildOption1: [''],
        appropriatePermanencyForChildOption2: [''],
        guardianCommitmentForPermanentChildCare: [''],
        ageAppropriateConsultationTakenPlaceWiththeChild: [''],
        guardiandetailsChildAttachment: [''],
        secondGuardianChildAttachment: [''],
        YesIndicatetypeofremoval: [''],
        voluntaryVpa: [''],
        courtOrderwithcontrarytothewelfarebestinterestsdetermination: [''],
        noChildisNoTeligibleforTitleIvEGuardianshipAssistanceContinuetoSection: [''],
        DateofRemovalVpa: [''],
        DateofRemovalCourtOrder: [''],
        thechildisasiblingofachildwhoisTitleIvEreimbursableforguardianshipassistancepayments: [''],
        lastSixMonthFiscalCodeandPaymentStatus: [''],
        thechildisnotasiblingofachildwhoisTitleIvEreimbursableforguardianshipassistancepayments: [''],
        fosterHomeGrid2 : this.fb.array([]),
        fosterHomeGrid : this.fb.array([]),
        secondGuardianGrid2: this.fb.array([]),
        secondGuardianGrid : this.fb.array([]),
        siblingInformationGrid : this.fb.array([])
    });
  }

  isTabSwitched(){
    $('.gap a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("Eligibility")){
        this.searchGapClientId();
      }
    });
  }



  startForm() {
    const formData = this.setData();
    this.mapData();
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('titleIvEGapEligibilityForm'), environment.formBuilderHost + `/form/5c9a653b8d24c6329b180ad3`, {
      hooks: {
        beforeSubmit: (submission, next) => {
          var submissiondata =  this.submissionData();
          console.log('-----'+JSON.stringify(submissiondata));
          
          this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postGapEligibilityNExtension).subscribe(
            (res) => {
              this._alertService.success("Submitted successfully!");
            },
            (error) => {
                this._alertService.error("Submission Failed");
            });
          // Only call next when we are ready.
          // next();
        }
      }
    }).then((form) => {
      console.log('Persons >>> ');
      form.submission = {
        data: formData
      };

      form.on('submit', submission => {
        console.log('form submit >>> ', submission);
      });

      form.on('change', formData => {
        if (formData.data.fosterHomeApprover === 'CPA') {
          //this.setHousehold(formData.data);
        }
        if (formData.data.fosterHomeApprover === 'LDSS') {
          //formData.data.fosterHomeGrid2 = this.setGuardianData();
        }
        form.submission = {
          data: formData.data
        };


        // }

      });
      form.on('render', formData => {
        console.log('form render >>> ', formData);
      });
      form.on('error', formData => {
        console.log('form error >>> ', formData);
      });
    }).catch((err) => {
      console.log('Form Error:: ', err);
    });
  }


  setData() {
    interface LooseObject {
      [key: string]: any;
    }

    const data: LooseObject = {};

    if(this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0){
      data.nameofchild = this.gapEligibilityData.demographicsInfo[0].childname;
      data.clientId = this.gapEligibilityData.demographicsInfo[0].client_id;
      //data.countyofjurisdiction = this.gapEligibilityData.demographicsInfo[0].countyofjurisdiction;
      data.dateofbirth = this.gapEligibilityData.demographicsInfo[0].dateofbirth;
      data.gender = this.gapEligibilityData.demographicsInfo[0].gender;
      data.nameofGuardian = this.gapEligibilityData.demographicsInfo[0].childguardianname;
      data.clientIDofguardian = this.gapEligibilityData.demographicsInfo[0].childguardianid;
      data.dateandtimeofguardianshipdecreecourtorder = this.gapEligibilityData.demographicsInfo[0].dateofcourtorderguardianshipfinalization;
    }

    data.countyofjurisdiction = this.countyname;

    if(this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0){
    data.guardianshipApplicationDate = this.gapEligibilityData.generalInfo[0].guardianshipapplicationdate;
    data.dateoffullapprovalasfosterparent = this.gapEligibilityData.generalInfo[0].dateoffullapprovalfp;
    //data.childremovaldateVpa = this.gapEligibilityData.generalInfo[0].childremovaldate ? this.gapEligibilityData.generalInfo[0].childremovaldate : ((this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0) ? this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate : null) ;
    data.latestfostercareplacementstartdatewithprospectiveguardian = this.gapEligibilityData.generalInfo[0].latestfcplacementstartdatewithperpectiveguardian;
    }

    data.guardiandetailsName= (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childguardianname : null;
    data.guardiandetailsClientId= (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childguardianid : null;
    data.guardiandetailsAgreementSignedDate= (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].guardianshipagreementsigneddate : null;
    data.guardiandetailsIsRelative= (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].primaryguardianrelative : null;
    data.guardiandetailsRelationship= (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].primaryguardianrelationship : null;
    data.guardiandetailsNoofHhMembers= (this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0) ? this.gapEligibilityData.safetyInfo[0].primaryguardianhhmembers : null;
    

    
    data.secondGuardianExists = this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0 && this.gapEligibilityData.safetyInfo[0].secondguardianexists === 'YES' ? true : false;
    if (data.secondGuardianExists) {
      data.secondGuardianSecondGuardianName= (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childsecguardianname : null,
      data.secondGuardianClientId= (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childsecguardianid : null,
      data.secondGuardianAgreementSignedDate= (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].secondaryguardianshipagreementsigneddate : null,
      data.secondGuardianIsRelative= (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].secondguardianrelative : null,
      data.secondGuardianRelationship= (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].secondguardianrelationship : null,
      data.secondGuardianLivingwithGuardian= (this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0) ? this.gapEligibilityData.safetyInfo[0].secguardlivingwithpriguard : null,
      data.secondGuardianNoofHhMembers= 0 //this.gapEligibilityData.gapEligibilityInfo[0].secnoofadditionalhhmembers
    }

    
    if(this.gapEligibilityData.otherDeterminationInfo && this.gapEligibilityData.otherDeterminationInfo.lenght > 0){
      data.successorGuardianExists = this.gapEligibilityData.otherDeterminationInfo[0].successorguardianexists === 'YES' ? true : false;
      if (data.successorGuardianExists) {
        data.successorGuardianName = this.gapEligibilityData.otherDeterminationInfo[0].successorguardianname;
        data.successorGuardianId = this.gapEligibilityData.otherDeterminationInfo[0].successorguardianid;
        data.dateofSuccessionAddendum = this.gapEligibilityData.otherDeterminationInfo[0].dateofsuccessionaddendum;
      }
    }
    
    data.fosterHomeApprover = (this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0) ? this.gapEligibilityData.safetyInfo[0].fosterhomeapprover : null;

    if (data.fosterHomeApprover === 'CPA') {
      //this.setHousehold(data);
    }

    if (this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length && this.gapEligibilityData.safetyInfo.length > 0) {
      data.secondGuardianGrid = [{
        // data.secondGuardianGrid[0].secondGuardianGridHhPersonName= element.
        // data.secondGuardianGrid[0].secondGuardianGridDob = element.
        secondGuardianGridBackgroundCheck: this.gapEligibilityData.safetyInfo[0].secnationalandstatecriminalhistorybackgroundcheck === 'YES' ? true : false,
        secondGuardianGridDateofCriminalHistory: this.gapEligibilityData.safetyInfo[0].secdateofnationalandstatecriminalhistorybackgroundcheck,
        //secondGuardianGridDateofstatecriminalhistorybackground
        secondGuardianGridChildabusecheck: this.gapEligibilityData.safetyInfo[0].secchildabuseandmaltreatmentdatabasecheck === 'YES' ? true : false,
        secondGuardianGridDateofChildAbuse: this.gapEligibilityData.safetyInfo[0].secdateofchildabuseandmaltreatmentdbcheck,
        secondGuardianGridOutofStateinPast5Yrs: this.gapEligibilityData.safetyInfo[0].secoutofstatewithinpast5years === 'YES' ? true : false,
        secondGuardianGridChildWelfareAgenciesinPreviousStateContacted: this.gapEligibilityData.safetyInfo[0].secchildabuseandmaltreatmentinformationinthepreviousstates === 'YES' ? true : false,
        secondGuardianGridDateOfChildAbuseOutOfState: this.gapEligibilityData.safetyInfo[0].secdateofchildabuseandmaltreatmentdbcheckoos
      }];

    }


    data.appropriatePermanencyForChildOption1 = (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isreunificationremoved ? 'YES' : 'NO') : null;
    data.appropriatePermanencyForChildOption2 = (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isadoptionremoved ? 'YES' : 'NO')  : null;
    data.guardianCommitmentForPermanentChildCare = (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].iscgprovidesafe ? 'YES' : 'NO')  : null;
    data.ageAppropriateConsultationTakenPlaceWiththeChild = (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isconsultationchildage ? 'YES' : 'NO')  : null;
    data.guardiandetailsChildAttachment = (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isguardianattach ? 'YES' : 'NO')  : null;
    data.secondGuardianChildAttachment = (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].ischildsecondguardianattach ? 'YES' : 'NO')  : null;




    //data.childremovaldateVpaCourtorderremovaldateCourt =  (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0 && this.gapEligibilityData.generalInfo[0].childremovaldate) ? this.gapEligibilityData.generalInfo[0].childremovaldate : ((this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0) ? this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate : null);

        if(this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0
          && this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate && this.gapEligibilityData.reimbursementInfo[0].childremovedbyvpa === 'YES'){
            data.YesIndicatetypeofremoval = true;
            data.voluntaryVpa = true;
            data.DateofRemovalVpa = this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate;
        }

        if(this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0
          && this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate && this.gapEligibilityData.reimbursementInfo[0].childremovedbyvpa === 'NO'){
            data.YesIndicatetypeofremoval = true;
            data.courtOrderwithcontrarytothewelfarebestinterestsdetermination = true;
            data.DateofRemovalCourtOrder = this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate;
        }

        if(!data.YesIndicatetypeofremoval){
          data.noChildisNoTeligibleforTitleIvEGuardianshipAssistanceContinuetoSection = true;
        }
  
    data.lastSixMonthFiscalCodeandPaymentStatus =  (this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0) ? this.gapEligibilityData.reimbursementInfo[0].last6monthfiscalpaymentstatus : null;
    data.siblingInformationGrid = [{}];

    if (this.gapEligibilityData.siblingExceptionInfo && 
      this.gapEligibilityData.siblingExceptionInfo.length && 
      this.gapEligibilityData.siblingExceptionInfo.length > 0 &&
      this.gapEligibilityData.siblingExceptionInfo[0].siblinginfo ) {
      data.thechildisasiblingofachildwhoisTitleIvEreimbursableforguardianshipassistancepayments = true;  
      this.gapEligibilityData.siblingExceptionInfo[0].siblinginfo.forEach((element, index) => {
        data.siblingInformationGrid.push({
          siblingInformationGridSiblingName: element.siblingclientname,
          siblingInformationGridSiblingGuardianId: element.siblingguardianid,
          siblingInformationGridSiblingGapEligiblityStatus: element.siblinggapeligibility
        });
      });
    }
    else{
      data.thechildisnotasiblingofachildwhoisTitleIvEreimbursableforguardianshipassistancepayments = true;
      data.siblingInformationGrid.push({
        siblingInformationGridSiblingName: null,
        siblingInformationGridSiblingGuardianId: null,
        siblingInformationGridSiblingGapEligiblityStatus: null
      });
    }

    if (data.siblingInformationGrid.length > 0) {
      data.siblingInformationGrid.splice(0, 1);
    }

    return data;
  }





  mapData() {
    interface LooseObject {
      [key: string]: any;
    }
    //const data: LooseObject = {};
    if(this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0){
        this.gapEligibilityForm.patchValue({
          nameofchild: this.gapEligibilityData.demographicsInfo[0].childname,
          clientId: this.gapEligibilityData.demographicsInfo[0].client_id,
          dateofbirth: this.gapEligibilityData.demographicsInfo[0].dateofbirth,
          gender: this.gapEligibilityData.demographicsInfo[0].gender,
          nameofGuardian: this.gapEligibilityData.demographicsInfo[0].childguardianname,
          clientIDofguardian: this.gapEligibilityData.demographicsInfo[0].childguardianid,
          dateandtimeofguardianshipdecreecourtorder: this.gapEligibilityData.demographicsInfo[0].dateofcourtorderguardianshipfinalization,
          guardiansubsidyid: this.gapEligibilityData.demographicsInfo[0].guardiansubsidyid
      });
    }


    this.gapEligibilityForm.patchValue({
      countyofjurisdiction: this.gapEligibilityData.summaryInfo && this.gapEligibilityData.summaryInfo.length > 0 ? this.gapEligibilityData.summaryInfo[0].childjurisdiction : null
    });

    if(this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0){
      this.gapEligibilityForm.patchValue({
        guardianshipApplicationDate: this.gapEligibilityData.generalInfo[0].guardianshipapplicationdate,
        dateoffullapprovalasfosterparent: this.gapEligibilityData.generalInfo[0].dateoffullapprovalfp,
        latestfostercareplacementstartdatewithprospectiveguardian: this.gapEligibilityData.generalInfo[0].latestfcplacementstartdatewithperpectiveguardian
      });
    }

    const name =  (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childsecguardianname.trim() : null;
    this.gapEligibilityForm.patchValue({
      guardiandetailsName: (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childguardianname : null,
      guardiandetailsClientId: (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childguardianid : null,
      guardiandetailsAgreementSignedDate: (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].guardianshipagreementsigneddate : null,
      guardiandetailsIsRelative: (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0 && (this.gapEligibilityData.generalInfo[0].primaryguardianrelationshipid=== 1001 || this.gapEligibilityData.generalInfo[0].primaryguardianrelationshipid=== 1014)) ?  'YES' : 'NO',
      guardiandetailsRelationshipID: (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].primaryguardianrelationshipid : null,
      guardiandetailsRelationship: (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].primaryguardianrelationship : null,
      guardiandetailsNoofHhMembers: 0,
      secondGuardianExists: this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0 && this.gapEligibilityData.safetyInfo[0].secondguardianexists === 'YES' && name !== '' && name !== null ? true : false
    });

    this.setPrimaryGuardianDataAndHousehold();
    
    if ( this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0 && this.gapEligibilityData.safetyInfo[0].secondguardianexists === 'YES' ) {
      this.gapEligibilityForm.patchValue({
        secondGuardianSecondGuardianName: (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childsecguardianname : null,
        secondGuardianClientId: (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].childsecguardianid : null,
        secondGuardianAgreementSignedDate: (this.gapEligibilityData.demographicsInfo && this.gapEligibilityData.demographicsInfo.length > 0) ? this.gapEligibilityData.demographicsInfo[0].secondaryguardianshipagreementsigneddate : null,
        secondGuardianIsRelative: (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0 && (this.gapEligibilityData.generalInfo[0].secondguardianrelationshipid === 1001 || this.gapEligibilityData.generalInfo[0].secondguardianrelationshipid === 1014)) ? 'YES' : 'NO',
        secondGuardianRelationshipID:  (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].secondguardianrelationshipid : null,
        secondGuardianRelationship: (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? this.gapEligibilityData.generalInfo[0].secondguardianrelationship : null,
        secondGuardianLivingwithGuardian: (this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0) ? this.gapEligibilityData.safetyInfo[0].secguardlivingwithpriguard : null,
        secondGuardianNoofHhMembers: 0 //this.gapEligibilityData.gapEligibilityInfo[0].secnoofadditionalhhmembers
      });
    }

    this.setSecondaryGuardianDataAndHousehold();
    
    if(this.gapEligibilityData.otherDeterminationInfo && this.gapEligibilityData.otherDeterminationInfo.lenght > 0){
      this.gapEligibilityForm.patchValue({
        successorGuardianExists: this.gapEligibilityData.otherDeterminationInfo[0].successorguardianexists === 'YES' ? true : false
      });
      if (this.gapEligibilityData.otherDeterminationInfo[0].successorguardianexists === 'YES') {
        this.gapEligibilityForm.patchValue({
          successorGuardianName : this.gapEligibilityData.otherDeterminationInfo[0].successorguardianname,
          successorGuardianId : this.gapEligibilityData.otherDeterminationInfo[0].successorguardianid,
          dateofSuccessionAddendum : this.gapEligibilityData.otherDeterminationInfo[0].dateofsuccessionaddendum
        });
      }
    }
    
    this.gapEligibilityForm.patchValue({
      fosterHomeApprover: (this.gapEligibilityData.safetyInfo && this.gapEligibilityData.safetyInfo.length > 0) ? this.gapEligibilityData.safetyInfo[0].fosterhomeapprover : null
    });

  
    this.gapEligibilityForm.patchValue({
      appropriatePermanencyForChildOption1 : (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isreunificationremoved ? 'YES' : 'NO') : null,
      appropriatePermanencyForChildOption2 : (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isadoptionremoved ? 'YES' : 'NO')  : null,
      guardianCommitmentForPermanentChildCare : (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].iscgprovidesafe ? 'YES' : 'NO')  : null,
      ageAppropriateConsultationTakenPlaceWiththeChild : (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isconsultationchildage ? 'YES' : 'NO')  : null,
      guardiandetailsChildAttachment : (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].isguardianattach ? 'YES' : 'NO')  : null,
      secondGuardianChildAttachment : (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0) ? (this.gapEligibilityData.generalInfo[0].ischildsecondguardianattach ? 'YES' : 'NO')  : null
    });

    
    //data.childremovaldateVpaCourtorderremovaldateCourt =  (this.gapEligibilityData.generalInfo && this.gapEligibilityData.generalInfo.length > 0 && this.gapEligibilityData.generalInfo[0].childremovaldate) ? this.gapEligibilityData.generalInfo[0].childremovaldate : ((this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0) ? this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate : null);

        if(this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0
          && this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate && this.gapEligibilityData.reimbursementInfo[0].childremovedbyvpa === 'YES'){
            this.gapEligibilityForm.patchValue({
              YesIndicatetypeofremoval : true,
              voluntaryVpa : true,
              DateofRemovalVpa : this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate
            });
        }

        if(this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0
          && this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate && this.gapEligibilityData.reimbursementInfo[0].childremovedbyvpa === 'NO'){
            this.gapEligibilityForm.patchValue({
              YesIndicatetypeofremoval : true,
              courtOrderwithcontrarytothewelfarebestinterestsdetermination : true,
              DateofRemovalCourtOrder : this.gapEligibilityData.reimbursementInfo[0].removalcourtorderdate  
            });
        }

        if(!this.gapEligibilityForm.value.YesIndicatetypeofremoval){
          this.gapEligibilityForm.patchValue({
            noChildisNoTeligibleforTitleIvEGuardianshipAssistanceContinuetoSection  : true
          });
        }
  
        this.gapEligibilityForm.patchValue({
          lastSixMonthFiscalCodeandPaymentStatus :  (this.gapEligibilityData.reimbursementInfo && this.gapEligibilityData.reimbursementInfo.length > 0) ? this.gapEligibilityData.reimbursementInfo[0].last6monthfiscalpaymentstatus : null
        });

  
   const siblingArry = <FormArray>this.gapEligibilityForm.controls.siblingInformationGrid;

   if (this.gapEligibilityData.minorParentSiblingInfo && 
    this.gapEligibilityData.minorParentSiblingInfo.length > 0 && 
    this.gapEligibilityData.minorParentSiblingInfo[0].siblingsinfo && 
    this.gapEligibilityData.minorParentSiblingInfo[0].siblingsinfo.length > 0) {
        this.gapEligibilityForm.patchValue({
          thechildisasiblingofachildwhoisTitleIvEreimbursableforguardianshipassistancepayments  : true
        }); 
      this.gapEligibilityData.minorParentSiblingInfo[0].siblingsinfo.forEach((element, index) => {
        siblingArry.push(this._formBuilder.group({
            siblingInformationGridSiblingName: element.name,
            siblingInformationGridSiblingGuardianId: element.altproviderid,
            siblingInformationGridSiblingGapEligiblityStatus: element.childapplicabilitystatus
          }))
      });
    }else{
      this.gapEligibilityForm.patchValue({
        thechildisnotasiblingofachildwhoisTitleIvEreimbursableforguardianshipassistancepayments  : true
      }); 
    }

    if(!this.gapEligibilityForm.value.secondGuardianExists){
      this.gapEligibilityForm.patchValue({
        secondGuardianSecondGuardianName  : '',
        secondGuardianClientId  : 0,
        secondGuardianAgreementSignedDate  : '',
        secondGuardianIsRelative  : '',
        secondGuardianRelationship  : '',
        secondGuardianLivingwithGuardian  : '',
        secondGuardianNoofHhMembers  : 0
      }); 
    }

  }



  
  setSecondaryGuardianDataAndHousehold() {
    if (this.gapEligibilityData.demographicsInfo 
      && this.gapEligibilityData.demographicsInfo.length 
      && this.gapEligibilityData.demographicsInfo.length > 0
      && this.gapEligibilityData.demographicsInfo[0].secondaryguardianinfo && this.gapEligibilityData.demographicsInfo[0].secondaryguardianinfo.length > 0) {
        this.gapEligibilityData.demographicsInfo[0].secondaryguardianinfo.forEach((element, index) => {
        if (element.persontype !== 'Applicant' ) {
          const secondGuardianGridArry = <FormArray>this.gapEligibilityForm.controls.secondGuardianGrid;
          secondGuardianGridArry.push(this._formBuilder.group({
              secondGuardianGridHhPersonName: element.name,
              secondGuardianGridDob: element.dob,
              secondGuardianGridBackgroundCheck: element.federalbackgrounddate === 'YES' ? true : false,
              secondGuardianGridDateofCriminalHistory: element.federalbackgrounddate,
              secondGuardianGridDateofstatecriminalhistorybackground: element.statebackgrounddate,
              secondGuardianGridChildabusecheck: element.childabusemaltdatacheck === 'YES' ? true : false,
              secondGuardianGridDateofChildAbuse: element.childabusemaltdatacheck,
              secondGuardianGridOutofStateinPast5Yrs: element.secoutofstatewithinpast5years === 'YES' ? true : false,
              secondGuardianGridChildWelfareAgenciesinPreviousStateContacted: element.secchildabuseandmaltreatmentinformationinthepreviousstates === 'YES' ? true : false,
              secondGuardianGridDateOfChildAbuseOutOfState: element.secdateofchildabuseandmaltreatmentdbcheckoos
          })
          );
          
          this.gapEligibilityForm.patchValue({
            secondGuardianNoofHhMembers: this.gapEligibilityForm.value.secondGuardianGrid.length
          });
        }

        if (element.persontype === 'Applicant' ) {
            const control = <FormArray>this.gapEligibilityForm.controls.fosterHomeGrid2;
            control.push(this._formBuilder.group({
              secondGuardianGridHhPersonName2: element.name,
              secondGuardianGridDob2:  element.dob,
              secondGuardianGridDateofCriminalHistory2: element.federalbackgrounddate,
              secondGuardianGridDateofstatecriminalhistorybackground2: element.statebackgrounddate,
              secondGuardianGridDateofChildAbuse2:  element.childabusemaltdatacheck,
              secondGuardianGridOutofStateinPast5Yrs2: '',
              secondGuardianGridDateOfChildAbuseOutOfState2: ''
            })
          );
        }
      });
    }
  }



  setPrimaryGuardianDataAndHousehold() {
    if (this.gapEligibilityData.demographicsInfo 
      && this.gapEligibilityData.demographicsInfo.length 
      && this.gapEligibilityData.demographicsInfo.length > 0
      && this.gapEligibilityData.demographicsInfo[0].primaryguardianinfo && this.gapEligibilityData.demographicsInfo[0].primaryguardianinfo.length > 0) {
        this.gapEligibilityData.demographicsInfo[0].primaryguardianinfo.forEach((element, index) => {
        if (element.persontype !== 'Applicant' ) {
          const fosterHomeGridArry = <FormArray>this.gapEligibilityForm.controls.fosterHomeGrid;
          fosterHomeGridArry.push(this._formBuilder.group({
              fosterHomeGridHhPersonName: element.name,
              fosterHomeGridDob: element.dob,
              fosterHomeGridBackgroundCheck: element.federalbackgrounddate === 'YES' ? true : false,
              fosterHomeGridDateofCriminalHistory: element.federalbackgrounddate,
              fosterHomeGridDateofstatecriminalhistorybackground: element.statebackgrounddate,
              fosterHomeGridChildabusecheck: element.childabusemaltdatacheck === 'YES' ? true : false,
              fosterHomeGridDateofChildAbuse: element.childabusemaltdatacheck,
              fosterHomeGridOutofStateinPast5Yrs: element.outofstatewithinpast5years === 'YES' ? true : false,
              fosterHomeGridChildWelfareAgenciesinPreviousStateContacted: element.childabuseandmaltreatmentinformationinthepreviousstates === 'YES' ? true : false,
              fosterHomeGridDateofChildAbuseOutofState: element.dateofchildabuseandmaltreatmentdbcheckoos
          })
          );

          this.gapEligibilityForm.patchValue({
            guardiandetailsNoofHhMembers: this.gapEligibilityForm.value.fosterHomeGrid.length
          });
        }

        if (element.persontype === 'Applicant' ) {
            const control = <FormArray>this.gapEligibilityForm.controls.fosterHomeGrid2;
            control.push(this._formBuilder.group({
              fosterHomeGridHhPersonName2: element.name,
              fosterHomeGridDob2:  element.dob,
              fosterHomeGridDateofCriminalHistory2: element.federalbackgrounddate,
              fosterHomeGridDateofstatecriminalhistorybackground2: element.statebackgrounddate,
              fosterHomeGridDateofChildAbuse2:  element.childabusemaltdatacheck,
              fosterHomeGridOutofStateinPast5Yrs2: '',
              fosterHomeGridDateofChildAbuseOutofState2: ''
            })
          );
        }
      });
    }
  }



  submissionData() {

    if(this.gapEligibilityForm.value.voluntaryVpa){
      this.gapEligibilityForm.patchValue({
        courtOrderwithcontrarytothewelfarebestinterestsdetermination: false,
        DateofRemovalCourtOrder: ''
      });
    }

    if(this.gapEligibilityForm.value.courtOrderwithcontrarytothewelfarebestinterestsdetermination){
      this.gapEligibilityForm.patchValue({
        voluntaryVpa: false,
        DateofRemovalVpa: ''
      });
    }

    interface LooseObject {
      [key: string]: any;
    }
    console.log('this.gapEligibilityForm.value.secondGuardianGrid2 ==== '+JSON.stringify(this.gapEligibilityForm.value));
    let gender = '';
    if (this.gapEligibilityForm.value.gender === 'M') {
      gender = 'Male';
    } else if (this.gapEligibilityForm.value.gender === 'F') {
      gender = 'Female';
    } else {
      gender = 'Other';
    }
    const submissiondata: LooseObject = {
      'cjamsPid': Number(this.gapEligibilityForm.value.clientId),
      'guardiansubsidyid':Number(this.gapEligibilityForm.value.guardiansubsidyid),
      'removalid': this.gapEligibilityData.reimbursementInfo[0].removalid ? this.gapEligibilityData.reimbursementInfo[0].removalid : '99999',
      'pagesnapshot': this.gapEligibilityForm.getRawValue(),
      'initialDetermination': {
        'payload': {
          'name': 'GAP',
          '__metadataRoot': {},
          'Objects': [
            {
              'reason': {
                '__metadata': {
                  '#type': 'Reason',
                  '#id': 'Reason_id_1'
                }
              },
              'secondGuardian': {
                'Child_Second_Guardian_Attachment': this.gapEligibilityForm.value.secondGuardianChildAttachment ? this.gapEligibilityForm.value.secondGuardianChildAttachment : null,
                'SecondGuardianNumberOfAdditionalHouseholdMembers': this.gapEligibilityForm.value.secondGuardianNoofHhMembers !== '' ? this.gapEligibilityForm.value.secondGuardianNoofHhMembers : null,
                //'SG_National_and_state_criminal_history_background_check': this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 &&  this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofCriminalHistory2  &&  this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofstatecriminalhistorybackground2 ? 'YES': 'NO',
                'SG_Child_abuse_and_maltreatment_data_base_check': this.gapEligibilityForm.value.secondGuardianExists ? (this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 && this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofChildAbuse2 === '' ? null : this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofChildAbuse2) : null , 
                'IsRelative': this.gapEligibilityForm.value.secondGuardianIsRelative !=='' ? (this.gapEligibilityForm.value.secondGuardianIsRelative ? 'YES' : 'NO') : null,
                'SecondGuardianRelationshipID': this.gapEligibilityForm.value.secondGuardianRelationshipID === '' ? null : this.gapEligibilityForm.value.secondGuardianRelationshipID,
                'SG_DateOf_State_criminal_history_background_check': this.dateConversion(this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 ? this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofstatecriminalhistorybackground2 : null),
                'SecondaryGuardianshipAgreementSignedDate': this.dateConversion(this.gapEligibilityForm.value.secondGuardianAgreementSignedDate !=='' ? this.gapEligibilityForm.value.secondGuardianAgreementSignedDate : null),
                //'SG_DateOf_National_and_state_criminal_history_background_check': this.dateConversion(this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 ? this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofCriminalHistory2 : null),
                'LivingWithPrimaryGuardian': this.gapEligibilityForm.value.secondGuardianLivingwithGuardian !== '' ? this.gapEligibilityForm.value.secondGuardianLivingwithGuardian: null,
                'SG_Date_Of_FBI_Criminal_History_Background_Check': this.dateConversion(this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 ? this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofCriminalHistory2 : null),
                'SG_Date_Of_Child_abuse_and_maltreatment_data_base_check': this.dateConversion(this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 ? this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofChildAbuse2 : null),
                'SG_Applicable_child_welfare_agencies_in_the_previous_states_contacted_to_obtain_child_abuse_and_maltreatment_information': this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? (this.dateConversion(this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 ? this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateOfChildAbuseOutOfState2 : null) ? 'YES' : 'NOT_APPLICABLE') : null ,
                'secondGuardianHouseholdMembers': this.secondaryHHMemberSubmissionData(),
                'SG_State_criminal_history_background_check': this.gapEligibilityForm.value.secondGuardianExists ? (this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 && this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofstatecriminalhistorybackground2 === '' ? null : this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofstatecriminalhistorybackground2) : null,
                '__metadata': {
                  '#type': 'SecondGuardian',
                  '#id': 'SecondGuardian_id_1'
                },
                'SG_FBI_Criminal_History_Background_Check': this.gapEligibilityForm.value.secondGuardianExists ? (this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 &&  this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofCriminalHistory2  === '' ? null : this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateofCriminalHistory2) : null,
                'ChildSecondGuardianID': '9874',
                'SG_Out_of_state_within_past_5_years_of_the_application_for_GAP': this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? (this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 && this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridOutofStateinPast5Yrs2  ? 'YES' : 'NO') : null  ,
                'SG_Date_Of_Child_abuse_and_maltreatment_data_base_check_OutOfState': this.dateConversion(this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' && this.gapEligibilityForm.value.secondGuardianGrid2 && this.gapEligibilityForm.value.secondGuardianGrid2.length > 0 ? this.gapEligibilityForm.value.secondGuardianGrid2[0].secondGuardianGridDateOfChildAbuseOutOfState2 : null)
              },
              'GuardianshipApplicationDate': this.dateConversion(this.gapEligibilityForm.value.guardianshipApplicationDate),
              'GuardianshipAgreementSignedDate': this.dateTimeConversion(this.gapEligibilityForm.value.guardiandetailsAgreementSignedDate ? this.gapEligibilityForm.value.guardiandetailsAgreementSignedDate : null),
              'applicantInformation': {
                'RelationshipID': this.gapEligibilityForm.value.guardiandetailsRelationshipID === '' ? 0 : this.gapEligibilityForm.value.guardiandetailsRelationshipID,
                'Guardian_Commitment_For_Permanent_Child_Care': this.gapEligibilityForm.value.guardianCommitmentForPermanentChildCare?this.gapEligibilityForm.value.guardianCommitmentForPermanentChildCare:null,
                'ClientID': this.gapEligibilityForm.value.clientId,
                'IsRelative': this.gapEligibilityForm.value.guardiandetailsIsRelative === '' ? null : this.gapEligibilityForm.value.guardiandetailsIsRelative,
                '__metadata': {
                  '#type': 'ApplicantInformation',
                  '#id': 'ApplicantInformation_id_1'
                }
              },
              'person': {
                'DateOfBirth': this.dateConversion(this.gapEligibilityForm.value.dateofbirth),
                'Removal_Court_Order_Date': this.dateConversion(this.gapEligibilityForm.value.DateofRemovalCourtOrder),
                'Appropriate_Permanency_For_Child_Option_1_Not_Being_Returned_Home': this.gapEligibilityForm.value.appropriatePermanencyForChildOption1 ? this.gapEligibilityForm.value.appropriatePermanencyForChildOption1 : null,
                'CountyOfJurisdiction_LDSS': this.gapEligibilityForm.value.countyofjurisdiction ? this.gapEligibilityForm.value.countyofjurisdiction : null,
                'Child_Guardian_Attachment': this.gapEligibilityForm.value.guardiandetailsChildAttachment ? this.gapEligibilityForm.value.guardiandetailsChildAttachment: null,
                'ChildGuardianID': this.gapEligibilityForm.value.clientIDofguardian ? this.gapEligibilityForm.value.clientIDofguardian : null,
                'Gender': gender? gender: null,
                'SiblingExists' : this.gapEligibilityForm.value.thechildisasiblingofachildwhoisTitleIvEreimbursableforguardianshipassistancepayments ? 'YES' : 'NO' ,
                'siblingDetails': this.sibilingSubmissionData(),
                'Name': this.gapEligibilityForm.value.nameofchild?this.gapEligibilityForm.value.nameofchild:null,
                'Child_Removal_Date': this.dateConversion(this.gapEligibilityForm.value.DateofRemovalVpa), //this.dateConversion(this.gapEligibilityForm.value.childremovaldateVpa):null,
                'licensedFosterHome': [
                  {
                    'LastSixMonthFiscalCodeandPaymentStatus': this.gapEligibilityForm.value.lastSixMonthFiscalCodeandPaymentStatus,
                    //'DateOf_National_and_state_criminal_history_background_check': this.dateConversion(this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 ? this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofCriminalHistory2 : null),
                    'DateOfFullApprovalFP': this.dateConversion(this.gapEligibilityForm.value.dateoffullapprovalasfosterparent),
                    'Date_Of_Child_abuse_and_maltreatment_data_base_check_OutOfState': this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? (this.dateConversion(this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 ? this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofChildAbuseOutofState2 : null)) : null,
                    'LatestFosterCarePlacementStartDateWithPerpectiveGuardian': this.dateConversion(this.gapEligibilityForm.value.latestfostercareplacementstartdatewithprospectiveguardian),
                    'Date_Of_Child_abuse_and_maltreatment_data_base_check': this.dateConversion(this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 ? this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofChildAbuse2 : null),
                    'FBI_criminal_history_background_check': this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 && this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofCriminalHistory2 ? 'YES' : 'NO',
                    'State_criminal_history_background_check': this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 && this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofstatecriminalhistorybackground2 ? 'YES' : 'NO',
                    //'National_and_state_criminal_history_background_check': this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 && this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofCriminalHistory2 && this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofstatecriminalhistorybackground2 ? 'YES' : 'NO',
                    'Out_of_state_within_past_5_years_of_the_application_for_GAP': (this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 && this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridOutofStateinPast5Yrs2 && this.gapEligibilityForm.value.fosterHomeApprover === 'CPA') ? 'YES' : 'NO' ,
                    'Applicable_child_welfare_agencies_in_the_previous_states_contacted_to_obtain_child_abuse_and_maltreatment_information': this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? (this.dateConversion(this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 ? this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofChildAbuseOutofState2 : null) ? 'YES' : 'NOT_APPLICABLE') : null,
                    '__metadata': {
                      '#type': 'LicensedFosterHome',
                      '#id': 'LicensedFosterHome_id_1'
                    },
                    'FosterHome_Approver': this.gapEligibilityForm.value.fosterHomeApprover,
                    'DateOf_FBI_criminal_history_background_check': this.dateConversion(this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 ? this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofCriminalHistory2 : null),
                    'Child_abuse_and_maltreatment_data_base_check': this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 && this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofChildAbuse2 ? 'YES' : 'NO',
                    'DateOf_State_criminal_history_background_check': this.dateConversion(this.gapEligibilityForm.value.fosterHomeGrid2 && this.gapEligibilityForm.value.fosterHomeGrid2.length > 0 ? this.gapEligibilityForm.value.fosterHomeGrid2[0].fosterHomeGridDateofstatecriminalhistorybackground2 : null)
                  }
                ],
                '__metadata': {
                  '#type': 'Person',
                  '#id': 'Person_id_1'
                },
                'Age_Appropriate_Consultation_Taken_Place_With_the_Child': this.gapEligibilityForm.value.ageAppropriateConsultationTakenPlaceWiththeChild,
                'SecondGuardianExists': this.gapEligibilityForm.value.secondGuardianExists ? 'YES' : 'NO',
                'relationship': [
                  {
                    '__metadata': {
                      '#type': 'Relationship',
                      '#id': 'Relationship_id_1'
                    }
                  }
                ],
                'Appropriate_Permanency_For_Child_Option_2_Not_Being_Adopted': this.gapEligibilityForm.value.appropriatePermanencyForChildOption2,
                'SuccessorGuardianExists': this.gapEligibilityForm.value.successorGuardianExists ? 'YES' : 'NO'
              },
              'householdMember': this.primaryHHMemberSubmissionData(),
              'GuardianshipFinalizationDate_FinalizationDateOfCourtOrder': this.dateTimeConversion(this.gapEligibilityForm.value.dateandtimeofguardianshipdecreecourtorder),
              'successorGuardian': {
                'NameOfSuccessorGuardian': this.gapEligibilityForm.value.successorGuardianName,
                'ChildSuccessorGuardianID': this.gapEligibilityForm.value.successorGuardianId,
                'DateOfSuccessionAddendum': this.dateConversion(this.gapEligibilityForm.value.dateofSuccessionAddendum),
                '__metadata': {
                  '#type': 'SuccessorGuardian',
                  '#id': 'SuccessorGuardian_id_1'
                }
              },
              '__metadata': {
                '#type': 'Application',
                '#id': 'Application_id_1'
              },
              'NumberOfAdditionalHouseholdMembers': this.gapEligibilityForm.value.guardiandetailsNoofHhMembers,
              'status': {
                '__metadata': {
                  '#type': 'Status',
                  '#id': 'Status_id_1'
                }
              }
            }
          ]
        }
      }
    };


    this._commonHttpService.create(submissiondata, Titile4eUrlConfig.EndPoint.postGapEligibilityNExtension).subscribe(
      (res) => {
        this.submitForReview.emit();
        this._alertService.success("Submitted successfully!");
      },
      (error) => {
          this._alertService.error("Submission Failed");
      });
  }

  
  searchGapClientId() {
    if(this.gapData) {
        this.gapEligibilityData = this.gapData;
        this.mapData();
      };
  }


  loadCounty(countyidinput) {
    this._commonHttpService.getArrayList(
      { where: { activeflag: '1', state: 'MD', countyid: countyidinput}, nolimit: true, method: 'get' },
      'admin/county' + '?filter'
    ).subscribe(res => {
      if(res && res.length > 0){
        this.countyname = res[0].countyname;
      }
      //this.mapData();
      //this.startForm();
    });
  }



  dateConversion(date) {
    if (date === undefined || date === null || date === '') {
      return null;
    } else {
      return this.datePipe.transform(date, 'MM/dd/yyyy');
    }
  }

  dateTimeConversion(date){
    if(date === undefined || date === null || date === ''){
      return null;
    }else{
      return this.datePipe.transform(date,"MM/dd/yyyy HH:mm:ss");
    }
  }


  secondaryHHMemberSubmissionData(){
    let secondaryHHMemberList = []; 

    if(this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.secondGuardian && this.gapEligibilityForm.value.secondGuardian.length > 0
       && this.gapEligibilityForm.value.secondGuardianExists && this.gapEligibilityForm.value.secondGuardianLivingwithGuardian === 'NO' && this.gapEligibilityForm.value.secondGuardianNoofHhMembers > 0){
        if(this.gapEligibilityForm.value.secondGuardianGrid){
          this.gapEligibilityForm.value.secondGuardianGrid.forEach((element, index) => {
            secondaryHHMemberList.push({
              'SG_DateOfBirthOfHouseholdMember': this.dateConversion(element.secondGuardianGridDob),
              'SG_FBI_Criminal_History_Background_Check_HH': element.secondGuardianGridDateofCriminalHistory ? 'YES' : 'NO',
              'SG_Date_Of_FBI_Criminal_History_Background_Check_HH': this.dateConversion(element.secondGuardianGridDateofCriminalHistory),
              'SG_DateOf_State_criminal_history_background_check_HH': this.dateConversion(element.secondGuardianGridDateofstatecriminalhistorybackground),
              'SG_State_criminal_history_background_check_HH': element.secondGuardianGridDateofstatecriminalhistorybackground ? 'YES' : 'NO',
              //'SG_National_and_state_criminal_history_background_check_HH': element.secondGuardianGridDateofCriminalHistory && element.secondGuardianGridDateofstatecriminalhistorybackground ? 'YES' : 'NO',
              'SG_Out_of_state_within_past_5_years_of_the_application_for_GAP_HH': this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? (element.secondGuardianGridOutofStateinPast5Yrs ? 'YES' : 'NO') : null,
              '__metadata': {
                '#type': 'SecondGuardianHouseholdMembers',
                '#id': 'SecondGuardianHouseholdMembers_id_'+(index+1)
              },
              'SG_Applicable_child_welfare_agencies_in_the_previous_states_contacted_to_obtain_child_abuse_and_maltreatment_information_HH': this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ?  (this.dateConversion(element.secondGuardianGridDateOfChildAbuseOutOfState) ? 'YES' : 'NOT_APPLICABLE') : null,
              'SG_Date_Of_Child_abuse_and_maltreatment_data_base_check_OutOfState_HH': this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ?  this.dateConversion(element.secondGuardianGridDateOfChildAbuseOutOfState) : null,
              //'SG_DateOf_National_and_state_criminal_history_background_check_HH': this.dateConversion(element.secondGuardianGridDateofCriminalHistory),
              'SG_Child_abuse_and_maltreatment_data_base_check_HH': element.secondGuardianGridDateofChildAbuse ? 'YES' : 'NO',
              'SG_Date_Of_Child_abuse_and_maltreatment_data_base_check_HH':  this.dateConversion(element.secondGuardianGridDateofChildAbuse)
            });
            });
        }
    }
    if(secondaryHHMemberList === []){
      secondaryHHMemberList.push({

      });
      return null;
    }
      return secondaryHHMemberList;
  }

  primaryHHMemberSubmissionData(){
    let primaryHHMemberList = []; 
    if(this.gapEligibilityForm.value.fosterHomeGrid && this.gapEligibilityForm.value.guardiandetailsNoofHhMembers > 0){
      this.gapEligibilityForm.value.fosterHomeGrid.forEach((element, index) => {
        primaryHHMemberList.push({
            'Applicable_child_welfare_agencies_in_the_previous_states_contacted_to_obtain_child_abuse_and_maltreatment_information_HH': this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? (this.dateConversion(element.fosterHomeGridDateofChildAbuseOutofState) ? 'YES' : 'NOT_APPLICABLE') : null,
            //'National_and_state_criminal_history_background_check_HH': element.fosterHomeGridDateofCriminalHistory && element.fosterHomeGridDateofstatecriminalhistorybackground ? 'YES' : 'NO',
            'Date_Of_Child_abuse_and_Maltreatment_database_Check_HH':this.dateConversion(element.fosterHomeGridDateofChildAbuse),
            'Date_Of_FBI_Criminal_History_Background_Check_HH':this.dateConversion(element.fosterHomeGridDateofCriminalHistory),
            'Date_Of_State_Criminal_History_Background_Check_HH':this.dateConversion(element.fosterHomeGridDateofstatecriminalhistorybackground),
            'Out_of_state_within_past_5_years_of_the_application_for_GAP_HH': this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? (element.fosterHomeGridOutofStateinPast5Yrs ? 'YES' : 'NO') : null,
            'Date_Of_Child_abuse_and_Maltreatment_database_Check_OutOfState_HH':this.gapEligibilityForm.value.fosterHomeApprover === 'CPA' ? this.dateConversion(element.fosterHomeGridDateofChildAbuseOutofState) : null,
            'FBI_Criminal_History_Background_Check_HH': element.fosterHomeGridDateofCriminalHistory ? 'YES' : 'NO',
            'DateOfBirthOfHouseholdMember': this.dateConversion(element.fosterHomeGridDob),
            'Child_abuse_and_maltreatment_data_base_check_HH': element.fosterHomeGridDateofChildAbuse ? 'YES' : 'NO',
            'State_criminal_history_background_check_HH': element.fosterHomeGridDateofstatecriminalhistorybackground ? 'YES' : 'NO',
            '__metadata': {
              '#type': 'HouseholdMember',
              '#id': 'HouseholdMember_id_'+(index+1)
            }
          });
        });
    }

    if(primaryHHMemberList === []){
      return null;
    }
      return primaryHHMemberList;
  }



  sibilingSubmissionData(){
      let siblingList = []; 
      if(this.gapEligibilityForm.value.siblingInformationGrid){
        this.gapEligibilityForm.value.siblingInformationGrid.forEach((element, index) => {
          siblingList.push({
          'SiblingGAPEligiblityStatus': element.siblingInformationGridSiblingGapEligiblityStatus === '' ? null : element.siblingInformationGridSiblingGapEligiblityStatus,
          'SiblingName': element.siblingInformationGridSiblingName === '' ? null : element.siblingInformationGridSiblingName,
          'SiblingGuardianID': element.siblingInformationGridSiblingGuardianId === '' ? null : element.siblingInformationGridSiblingGuardianId,
          '__metadata': {
            '#type': 'SiblingDetails',
              '#id': 'SiblingDetails_id_'+(index+1)
            }
          });
        });
      }else{
        siblingList.push({
          'SiblingGAPEligiblityStatus': null,
          'SiblingName': null,
          'SiblingGuardianID': null,
          '__metadata': {
            '#type': 'SiblingDetails',
              '#id': 'SiblingDetails_id_1'
            }
          });
      }
      console.log('siblingList - '+siblingList);
    return siblingList;
  }
}