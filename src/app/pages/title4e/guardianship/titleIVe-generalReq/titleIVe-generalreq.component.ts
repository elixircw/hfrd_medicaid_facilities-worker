import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PeriodTablePopUpComponent } from '../../period-table-pop-up/period-table-pop-up.component';
declare var $: any;
import * as moment from 'moment';
import { Title4eService } from '../../services/title4e.service';
@Component({
    selector: 'general-req',
    templateUrl: './titleIVe-generalreq.component.html',
    styleUrls: ['./titleIVe-generalreq.component.scss']
})
export class GeneralreqComponent implements OnInit {

    @Input() generalRequestForm: FormGroup;

    constructor(
        private router: Router,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        public dialog: MatDialog,
        private title4eService: Title4eService,
        private fb: FormBuilder
    ) { }
    SectionAReset() {
        this.generalRequestForm.reset();
    }
    ngOnInit() { }
}
