import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Supervisor4eComponent } from './supervisor4e/supervisor4e.component';
import { Worker4eComponent } from './worker4e/worker4e.component';
import { PeriodTablePopUpComponent } from './period-table-pop-up/period-table-pop-up.component';
import { TitleIveFosterCarComponent } from './title-ive-foster-car/title-ive-foster-car.component';
import { GuardianshipComponent } from './guardianship/titleIVe-guardianship.component';
import { GuardianshipResolverService } from './guardianship/titleIVe-guardianship-resolver.service';
const routes: Routes = [
    {
        path: '',
        component: Worker4eComponent,
        // canActivate: [RoleGuard],
        children: [
            {
                path: 'worker4e',
                loadChildren: './title4e.module#Title4eModule'
            }
        ]
    },
    {
        path: 'supervisor4e',
        component: Supervisor4eComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Saved Intakes'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    },
    {
        path: 'worker4e',
        component: Worker4eComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Saved Intakes'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    },
    {
        path: 'foster-car/:clientId/:placementId/:removalId',
        component: TitleIveFosterCarComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Title IV-E Foster Care'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    },
    {
        path: 'guardianship/:id',
        loadChildren: './guardianship/titleIVe-guardianship.module#GuardianshipModule',
    },
    {
        path: 'adoption/:clientid/:removalId',
        loadChildren: './adoption/titleIVe-adoption.module#AdoptionModule'
    },
    {
        path: 'period-table-pop-up',
        component: PeriodTablePopUpComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Saved Intakes'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Title4eRoutingModule {}
