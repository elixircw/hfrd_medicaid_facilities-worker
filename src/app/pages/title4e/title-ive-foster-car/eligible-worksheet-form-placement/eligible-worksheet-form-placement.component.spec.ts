import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibleWorksheetFormPlacementComponent } from './eligible-worksheet-form-placement.component';

describe('EligibleWorksheetFormPlacementComponent', () => {
  let component: EligibleWorksheetFormPlacementComponent;
  let fixture: ComponentFixture<EligibleWorksheetFormPlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EligibleWorksheetFormPlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EligibleWorksheetFormPlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
