import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { CommonHttpService } from '../../../../../@core/services';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';


@Component({
  selector: 'deemed-income-worksheet',
  templateUrl: './deemed-income-worksheet.component.html',
  styleUrls: ['./deemed-income-worksheet.component.scss']
})
export class DeemedIncomeWorksheetComponent implements OnInit {
  @Input() summaryinfo: any;
  @Input() history: any;
  @Input() deemedclientid: any;
  involvedpersons: any;
  stepparent: any;
  corticoninput: any;
  constructor(
    private _commonHttpService: CommonHttpService
  ) { }

  ngOnInit() {
    console.log(this.deemedclientid, 'deemedclientiddeemedclientiddeemedclientid');
    console.log(this.summaryinfo, 'inputdatainputdatainputdatainputdata');
    console.log(this.history, 'historyhistoryhistoryhistory');
    this.getPersonsList();
    this.corticoninput = this.history.inputjson.Objects[0];
    // this.deemedincome = this.history.inputjson.Objects[0]

  }

  getPersonsList() {
    const casenumber = this.summaryinfo[0].servicecaseid;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          nolimit: true,
          method: 'get',
          where: { servicecaseid: casenumber }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      )
      .subscribe((result) => {
        console.log(result, 'resultresultresultresult');
        this.involvedpersons = result;
        this.stepparent = _.head(this.involvedpersons.data.filter((person)  => person.cjamspid === this.deemedclientid));


      });
  }



}
