import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibleWorksheetFormAfdcComponent } from './eligible-worksheet-form-afdc.component';

describe('EligibleWorksheetFormAfdcComponent', () => {
  let component: EligibleWorksheetFormAfdcComponent;
  let fixture: ComponentFixture<EligibleWorksheetFormAfdcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EligibleWorksheetFormAfdcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EligibleWorksheetFormAfdcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
