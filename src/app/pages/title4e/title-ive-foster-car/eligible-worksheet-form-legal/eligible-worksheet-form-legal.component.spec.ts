import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibleWorksheetFormLegalComponent } from './eligible-worksheet-form-legal.component';

describe('EligibleWorksheetFormLegalComponent', () => {
  let component: EligibleWorksheetFormLegalComponent;
  let fixture: ComponentFixture<EligibleWorksheetFormLegalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EligibleWorksheetFormLegalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EligibleWorksheetFormLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
