import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class Title4eFosterCareService { 

  fosterCareData: any;
  placementId: any;
  public fileuploadcompleted = new BehaviorSubject(null);

  constructor() {}
    uploadCompleted() {
      this.fileuploadcompleted.next(true);
   }

}
