import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, AuthService, SessionStorageService, DataStoreService } from '../../../../@core/services';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConstants } from '../../../../@core/common/constants';
import { MatDialog } from '@angular/material';
import { uniq as _uniq, uniqBy as _uniqBy, orderBy as _orderBy } from 'lodash';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { Title4eService } from '../../services/title4e.service';
import { environment } from '../../../../../environments/environment';

declare const Formio: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'eligible-details',
  templateUrl: './eligible-details.component.html',
  styleUrls: ['./eligible-details.component.scss']
})
export class EligibleDetailsComponent implements OnInit {

  // @Input() eligibilityPerioodData: any[];
  isSupervisor = false;
  isSpecialist = false;
  eligibilityPeriodAccordion: any[];
  eligibilityPeriodData: any[] = [];
  latestInitialDet: any;
  eventTableData: any[] = [];
  detStatusDetails: any[] = [];
  isShowEventTable = false;
  messages: any;
  fieldMessages: any;
  fieldName: string;
  clientId: number;
  removalId: number;
  fostercareEvents: any;
  periodInfo: any;
  userInfo: any;
  submitForApprovalRemark: string;
  approveRejectDisabled: boolean = true;
  approvalid : any;
  sendforapprovalready = false;
  getUsersList: any[];
  selectedPerson: any;
  showSnapshot: boolean = false;
  worksheetType: string;
  eligibilitysummaryinfo: any;
  deemedclientid: any;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private titleIVeService: Title4eService,
    private route: ActivatedRoute,
    private storage: SessionStorageService,
  ) { }

  ngOnInit() {
    // Get client id from route params
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    this.isSpecialist = this._authService.selectedRoleIs(AppConstants.ROLES.TITLE_IVE_SPECIALIST);
    this.userInfo = this._authService.getCurrentUser();

    if(this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SUPERVISOR){
      this.isSupervisor = true;
    }

    if(this.userInfo.user.userprofile.teammemberassignment.teammember.teammemberroletype.roletypekey === AppConstants.ROLES.TITLE_IVE_SPECIALIST){
      this.isSpecialist = true;
    }

    this.clientId = this.route.snapshot.params['clientId'];
    // Get removal id from route params
    this.removalId = this.route.snapshot.params['removalId'];
    this.getEligibilityHistory();
    this.isTabSwitched();
  }

  updateSnapshot({checked}){
    const rawJson = _.get(this.periodInfo, 'inputjson.Objects.0');
    let determination: string;
    const regex = /^[R]\d+$/gm;
    if (this.periodInfo.period === "I") {
      determination = "ID";
    } else if (regex.exec(this.periodInfo.period) !== null) {
      determination = "RD";
    } else if (this.periodInfo.period === "18BDAY") {
      determination = "RD";
    } else {
      determination = null;
    }

    const createForm = (form: any) => {
      form.submission = {
        data: {
          persons_json: JSON.stringify({}),
          specifiedrelatives_json: JSON.stringify({}),
          determination: determination,
        }
      };
      form.on('change', formData => {
        if (_.has(formData, "changed.component.key") && formData.changed.component.key === "components") {
          switch (formData.changed.value) {
            case "legal":
              formData.data.issafehavenbaby = _.get(rawJson, "person.IsSafeHavenBaby");
              formData.data.typeofremoval = _.get(rawJson, "person.TypeOfRemoval");
              formData.data.typeofvpa = _.get(rawJson, "person.TypeOfVPA");
              formData.data.childphysicalremovaldate = _.get(rawJson, "person.ChildPhysicalRemovalDate");
              formData.data.clientnameofpersonfromwhomchildwasphysicallyremoved = _.get(rawJson, "person.ClientIDOfPersonFromWhomChildWasPhysicallyRemoved"); // TODO
              formData.data.clientidofpersonfromwhomchildwasphysicallyremoved = _.get(rawJson, "person.ClientIDOfPersonFromWhomChildWasPhysicallyRemoved");
              formData.data.childphysicaladdressafterremoval = _.get(rawJson, "person.ChildPhysicalAddressAfterRemoval");
              formData.data.relationshipofpersonfromwhomchildwasphysicallyremoved = _.get(rawJson, "person.RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved"); // TODO
              formData.data.relationshipidofpersonfromwhomchildwasphysicallyremoved = _.get(rawJson, "person.RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved");
              // formData.data.intakeservreqcourtorderid = _.get(rawJson, "intakeservreqcourtorderid");

              // courtinfotable
              formData.data.dateofcourthearing = _.get(rawJson, "person.DateOfCourtHearing");
              formData.data.magistrateorjudgename = _.get(rawJson, "JudgeName");
              formData.data.issignedbyjudge = _.get(rawJson, "person.IsSignedByJudge");
              formData.data.ctwdecision = _.get(rawJson, "person.CTWDecision");
              formData.data.dateoffindingctwdecision = _.get(rawJson, "person.DateOfFindingCTWDecision");
              formData.data.dateofnexthearing = _.get(rawJson, "person.DateOfNextHearing");
              formData.data.isiveagencyresponsibleforplacementandcare = _.get(rawJson, "person.IsIVEAgencyResponsibleForPlacementAndCare");
              formData.data.nameofsubjectctwfinding = _.get(rawJson, "person.NameOfSubjectCTWFinding");
              formData.data.clientidofsubjectctwfinding = _.get(rawJson, "person.ClientIDOfSubjectCTWFinding");
              formData.data.relationshipofsubjectctwfinding = _.get(rawJson, "person.RelationshipOfSubjectCTWFinding");
              formData.data.NameOfSubjectOfCTWFindingGroup = _.get(rawJson, "person.NameOfSubjectCTWFinding");
              formData.data.courtorderdelayremoval = _.get(rawJson, "person.CourtOrderDelayRemoval");
              formData.data.courtorderdelaytimedays = _.get(rawJson, "person.CourtOrderDelayTimeFrame");
              formData.data.dateagencylostlegalresponsibility = _.get(rawJson, "DateAgencyLostLegalResponsibility");
              formData.data.typeofcourthearing = _.get(rawJson, "TypeOfCourtHearing");
              formData.data.reasonableeffortsmade = _.get(rawJson, "person.ReasonableEffortsMade");
              formData.data.dateofreasonableeffortscourthearing = _.get(rawJson, "person.DateOfReasonableEffortsCourtHearing");
              formData.data.reasonableeffortsnotnecessaryduetoemergentcircumstances = _.get(rawJson, "person.ReasonableEffortsNotNecessaryDueToEmergentCircumstances");

              formData.data.fostercarepermanencyplan = _.get(rawJson, "FosterCarePermanencyPlan");

              formData.data.dateofjudicialfindingofrefpp = _.get(rawJson, "DateOfJudicialFindingOfREFPP");
              formData.data.dateofcurrentjudicialfindingofrefpp = _.get(rawJson, "DateOfCurrentJudicialFindingOfREFPP");
              formData.data.dateofsubsequentjudicialfindingofrefpp = _.get(rawJson, "DateOfSubsequentJudicialFindingOfREFPP");
              formData.data.dateofpreviousjudicialfindingofrefpp = _.get(rawJson, "DateOfPreviousJudicialFindingOfREFPP");

              formData.data.dateofsubsequentfindingofbestinterest = _.get(rawJson, "DateOfSubsequentFindingOfBestInterest");
              formData.data.dateofcurrentjudicialfindingofbestinterest = _.get(rawJson, "DateOfCurrentJudicialFindingOfBestInterest");
              formData.data.dateofpreviousbestinterestfinding = _.get(rawJson, "person.DateOfPreviousBestInterestFinding");

              formData.data.clientnamewhosignedvpa = _.get(rawJson, "person.ClientIDWhoSignedVPA"); // TODO
              formData.data.dateof1stparentsignatureonvpa = _.get(rawJson, "person.DateOf1stParentSignatureOnVPA");
              formData.data.dateof2ndparentsignatureonvpa = _.get(rawJson, "person.DateOf2ndParentSignatureOnVPA");
              formData.data.dateofguardiansignatureonvpa = _.get(rawJson, "person.DateOfGuardianSignatureOnVPA");
              formData.data.dateofldsssignatureonvpa = _.get(rawJson, "person.DateOfLDSSSignatureOnVPA");

              formData.data.mandatorynoteonmissing2ndparentsignatureonvpa = _.get(rawJson, "person.MandatoryNoteOnMissing2ndParentSignatureOnVPA");
              formData.data.dateofyouthsignatureonvpa = _.get(rawJson, "person.DateOfYouthSignatureOnVPA");
              formData.data.previousfostercareepisodeexist = _.get(rawJson, "PreviousFosterCareEpisodeExist");
              formData.data.exitcaredatefrompreviousfostercareepisode = _.get(rawJson, "ExitCareDateFromPreviousFosterCareEpisode");
              formData.data.reasonforexit = _.get(rawJson, "ReasonForExit");
              break;
            case "afdcRelatedness":
              // Income Summary
              formData.data.dateofbirth = _.get(rawJson, "person.DateOfBirth");
              formData.data.ageofthechild = _.has(rawJson, "person.DateOfBirth") ? moment().diff(new Date(rawJson.person.DateOfBirth), 'years') : null;
              formData.data.uscitizen = _.get(rawJson, "USCitizen");
              formData.data.qualifiedalien = _.get(rawJson, "QualifiedAlien");
              formData.data.qualifiedalienstaus = _.get(rawJson, "QualifiedAlienStaus");
              formData.data.alienregistrationnumber = _.get(rawJson, "AlienRegistrationNumber");

              // Household info
              // formData.data.householdincome_grid = householdData; // TODO

              // Income Info
              formData.data.standardunitno = _.get(rawJson, "StandardOfNeedForAU");
              formData.data.notinstandardunitno = _.get(rawJson, "StandardOfNeedForNotInAU");
              formData.data.assistanceunitno = _.get(rawJson, "NoOfMembersInAU");
              formData.data.notinassistanceunitno = _.get(rawJson, "NoOfMembersNotInAU");
              formData.data.childcarecost = _.get(rawJson, "TotalChildCareCost");
              formData.data.grossincome185pcunitno = _.get(rawJson, "GrossIncome185PctForAU");

              // assets
              formData.data.assetsallowance = _.get(rawJson, "AssetAllowance");
              formData.data.assetsmarketvalue = _.get(rawJson, "AssetsMarketValue");
              // formData.data.iveincomesumaryid = _.get(rawJson, "iveincomesumaryid");

              // Deprivation
              // formData.data.deprivation_grid = deprivationData; // TODO

              // Specified relatives
              // formData.data.specifiedrelatives_grid = specifiedRelativesData; // TODO
              break;
            case "otherCriteria":
              // 18 - 21
              formData.data.childdisabilityevaluationdocumentiondate = _.get(rawJson, "person.ChildDisabilityEvaluationDocumentionDate");
              formData.data.childdisabilitystartdate = _.get(rawJson, "person.ChildDisabilityStartDate");
              formData.data.childdisabilitytype = _.get(rawJson, "person.ChildDisabilityType");
              formData.data.dateofvalidsilaagreement = _.get(rawJson, "DateOfValidSILAAgreement");
              formData.data.hourspermonthemployed = _.get(rawJson, "person.HoursPerMonthEmployed");
              formData.data.istherevalidsilaagreement = _.get(rawJson, "IsThereValidSILAAgreement");
              formData.data.nameofemployer = _.get(rawJson, "NameOfEmployer");
              formData.data.nameofpostsecondaryorvocationaleducation = _.get(rawJson, "person.NameOfpostSecondaryOrVocationalEducation");
              formData.data.nameofpromotetoemploymentprogram = _.get(rawJson, "person.NameOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment");
              formData.data.nameofsecondaryeducationorequivalentprogram = _.get(rawJson, "person.NameOfSecondaryEducationOrEquivalentProgram");
              formData.data.startdateofemployment = _.get(rawJson, "person.StartDateOfEmployment");
              formData.data.startdateofpostsecondaryorvocationaleducation = _.get(rawJson, "person.StartDateOfpostSecondaryOrVocationalEducation");
              formData.data.startdateofpromotetoemploymentprogram = _.get(rawJson, "person.StartDateOfProgramOrActivityThatPromotesOrRemovesBarriersToEmployment");
              formData.data.startdateofsecondaryeducationorequivalentprogram = _.get(rawJson, "person.StartDateOfSecondaryEducationOrEquivalentProgram");

              // SSI & SSA
              formData.data.childreceivingssiorssa = _.get(rawJson, "person.ChildReceivingSSIOrSSADuringReviewPeriod");
              formData.data.agencyrepresentativeflag = _.get(rawJson, "IsTheAgencyTheRepresentativePayee");
              formData.data.reasonforagencynotrepresentativepayee = _.get(rawJson, "ReasonForWhyTheAgencyISNOTTheRepresentativePayee");
              formData.data.whoisrepresentativepayee = _.get(rawJson, "WhoIsTheRepresentativePayee");
              formData.data.suspendssipaymentflag = _.get(rawJson, "HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE");
              formData.data.reasonfornotsuspendingssipayment = _.get(rawJson, "ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE");
              formData.data.ischildageabove18 = _.has(rawJson, "dateofbirth") ? (moment().diff(new Date(rawJson.person.dateofbirth), 'years') >= 18 ? 'YES' : 'NO') : null;
              formData.data.dateofmedicaldetermination = _.get(rawJson, "DateOfMedicalDetermination");
              formData.data.doesagencyhasmedicaldocstostateincapabilityofchild = _.get(rawJson, "DoesAgencyHasMedicalDocsToStateIncapabilityOfChild");
              formData.data.hasagencyapplytobecomerepresentativepayee = _.get(rawJson, "HasAgencyApplyToBecomeRepresentativePayee");
              formData.data.dateofapplicationtobecomerepresentativepayee = _.get(rawJson, "DateOfApplicationToBecomeRepresentativePayee");
              formData.data.dateofrequesttosuspendthessipaymentandclaimive = _.get(rawJson, "DateOfRequestToSuspendTheSSIPaymentAndClaimIVE");
              formData.data.typeofbenefit = _.get(rawJson, "person.TypeOfBenefit");
              formData.data.amountofbenefit = _.get(rawJson, "person.AmountOfBenefit");

              break;
            case "placement":
              formData.data.dateofchildplacement = _.get(rawJson, "person.DateOfChildPlacement");
              formData.data.isdjsordsschild = _.get(rawJson, "person.IsDJSOrDSSChild");
              formData.data.dateoflivingarrangement = _.get(rawJson, "person.DateOfLivingArrangement");
              formData.data.childbeeninfostercare12monthormore = _.get(rawJson, "person.ChildBeenInFosterCare12MonthOrMore");
              // formData.data.isreasonableeffortsfindingtimely = _.get(rawJson, "IsReasonableEffortsFindingTimely"); //TODO
              // formData.data.reasonableeffortspermanancyplan = _.get(periodData, "IsThereAnyReasonableEffortsFindingDuringReviewPeriod"); // TODO
              // formData.data.placement_grid = placementData; // TODO
              break;
            default:
              break;
          }

          form.submission = {
            data: formData.data
          };
        }
      });
      // form.on('render', formData => {
      // });
      // form.on('error', formData => {
      // });
    }

    if(checked){
      Formio.setToken(this.storage.getObj('fbToken'));
      Formio.baseUrl = environment.formBuilderHost;
      Formio.createForm(document.getElementById('fosterCareFormReview'), environment.formBuilderHost + `/form/5c8fef928d24c6329b180a4e`).then(createForm).catch((err) => {
        // Failed to load form builder
        this._alertService.error('Failed to load data, please try again.');
      });
    } else {
      document.getElementById('fosterCareFormReview').innerHTML = "";
    }
  }

  isTabSwitched(){
    $('.intake-tabs a').on('shown.bs.tab', (event) => {
      var x = $(event.target).text();
      if(x.includes("DETAILS")){
        this.getEligibilityHistory();
      }
    });
  }

  onPeriodStartDate(period) {
    console.log(period);
    this.isShowEventTable = period[0].v_sqnm_sw.split('').includes('E');
    if (!this.isShowEventTable) {
      this.detStatusDetails = period;
    }
    console.log(period[0].v_sqnm_sw.split('').includes('E'));
    period = _orderBy(period, 'v_start_dt');
    this.eventTableData = _orderBy(period, 'v_eventreason');
    // this.eventTableData = _uniqBy(period, 'v_eventreason');
    // console.log(this.eventTableData);
  }

  // get initialDetStatusDetails() {
  //   const temp = [];
  //   this.eligibilityPerioodData.forEach(a => {
  //     a.forEach(b => {
  //       if (b.v_sqnm_sw === 'I') {
  //         temp.push(b);
  //       }
  //     });
  //   });
  //   return temp;
  // }

  toggleChildren(period: any): void {
    const filterChildrenData: any = {
      period,
      isParent: false
    }

    const filterParentData: any = {
      period,
      isParent: true
    }

    _.filter(this.eligibilityPeriodData, filterChildrenData).forEach((period : any)=>{
      period.isVisible = !period.isVisible;
    });

    _.filter(this.eligibilityPeriodData, filterParentData).forEach((period : any)=>{
      period.isCollapsed = !period.isCollapsed;
    });
  }


  getEligibilityHistory() {
    this.periodInfo = {};
    if (this.clientId && this.removalId) {
      this._commonHttpService.getAll(`${Titile4eUrlConfig.EndPoint.eligibilityHistory}/${this.clientId}/${this.removalId}`).subscribe(
        (responsearray: any) => {
          this.eligibilitysummaryinfo = responsearray.summaryInfo;
          let response = responsearray.history;
          let previousDet = "";
          this.eligibilityPeriodData = [];
          this.latestInitialDet = _.get(response, "I.0");
          for (const key in response) {
            if( key && _.isArray(response[key])) {
            response[key].forEach(element => {
              element.isParent = true;
              element.isVisible = true;
              element.isCollapsed = true;
              if (element.period === previousDet) {
                element.isParent = false;
                element.isVisible = false;
                element.isCollapsed = false;
              }
              this.eligibilityPeriodData.push(element);
              previousDet = element.period;
            });
          }
          }

          this.sendforapprovalready = true;
          _.filter(this.eligibilityPeriodData, { isParent: true as any }).forEach(({ approvalstatus, approvalid }) => {
            if (approvalid && approvalstatus === 'PENDING') {
              this.approvalid = approvalid;
              this.approveRejectDisabled = false;
            }
            if (status === 'Eligible Reimbursable' || (approvalstatus === null || approvalstatus === '')) {
              this.sendforapprovalready = this.sendforapprovalready && true;
            } else {
              this.sendforapprovalready = this.sendforapprovalready && false;
            }
          });
        },
        (error) => {
          this._alertService.error('Unable to get Audit Messages, please try again.');
          return false;
        }
      );
    }
  }

  ApproveReject(status){
    this._commonHttpService.create(
      {
        "where":{
          "approval_id": this.approvalid,
          "approval_status": status,
          "placement_type":"Fostercare"
        },
          "page": 1,
          "limit" : 10
      },
      'titleive/ive/ivespv-approval'
    ).subscribe(response => {
      if(response && response.data && response.data.length > 0){
        this.approveRejectDisabled= true;
        this.approvalid = null;
        this.sendforapprovalready = false;
        this.getEligibilityHistory();
      }
    },
      (error) => {
        this._alertService.error('Unable to process');
        return false;
      }
    );

  }

  showSupervisorList() {
    if (this._dataStoreService.getData('addNarrative')) {
    this.titleIVeService.getUsersList().subscribe(result => {
        this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    });
    (<any>$('#assign')).modal('show');
   }
   else {
    this._alertService.error('Please add Narrative to proceed.');
   }
  }

  selectPerson(item) {
    this.selectedPerson = item;
  }

  sendForApproval(){
    if(this.selectedPerson){
    this._commonHttpService.create(
      {
        "where":{
          "clientid": this.clientId,
          "removalid": this.removalId,
          "status":"{70}",
          "eventType":"Fostercare"
        },
          "page": 1,
          "limit" : 10
      },
      'titleive/ive/approval-status'
    ).subscribe(response => {
      if(response && response.data && response.data.length > 0){
        this.routingUpdate(response);
      }
    },
      (error) => {
        this._alertService.error('Unable to process');
        return false;
      }
    );
  }else{
    this._alertService.error('Select a Supervisor');
  }

  }


  routingUpdate(sendForApprovalResponse){
    this._commonHttpService.create(
      {
        'where': {
          'assignedtoid': this.selectedPerson.userid,
          'eventcode': 'PLTR',
          // "servicecaseid": sendForApprovalResponse.data[0].caseid,
          'status': 'SplReview',
          'notifymsg': 'Request for Review',
          'routeddescription': 'route',
          'comments': 'Request for Review - ' + this.submitForApprovalRemark,
          'placementid': sendForApprovalResponse.data[0].sp_ive_status_approval,
          'signText': 'Signed'
        }
      },
      'titleive/ive/routingUpdate'
    ).subscribe(response => {
      (<any>$('#assign')).modal('hide');
      this._alertService.success('Sent For Approval');
    },
      (error) => {
        this._alertService.error('Unable To Process');
        return false;
      }
    );
  }

  formatStatus(str: string): string{
    return _.replace(str, /_/g, " ");
  }

  getEligibilityDetails(period: any){
    this.fieldMessages = [];
    this.periodInfo = period;
    this.showSnapshot = false;
    if(period.period !== "I" && this.latestInitialDet){
      this.periodInfo['removaltype'] = this.periodInfo.removaltype ? this.periodInfo.removaltype : this.latestInitialDet.removaltype;
      this.periodInfo['removalhome'] = this.periodInfo.removalhome ? this.periodInfo.removalhome : this.latestInitialDet.removalhome;
      this.periodInfo['income'] = this.periodInfo.income ? this.periodInfo.income : this.latestInitialDet.income;
      this.periodInfo['assets'] = this.periodInfo.assets ? this.periodInfo.assets : this.latestInitialDet.assets;
      this.periodInfo['deprivation'] = this.periodInfo.deprivation ? this.periodInfo.deprivation : this.latestInitialDet.deprivation;
    }
  }


  getMessages(item, fieldName): void {
    this.fieldMessages = item.period_messages.filter((a) => {
      if(fieldName === 'Demographic') {
        if( ( a.message.search(/ReceiptOfOtherBenefits|Age|Citizenship|SILAAgreement|Youth_18_21_Eligibility_Criteria|Initialize|Demographic/) !==-1 ) && a.message.indexOf("]") != -1 ){
          a.dispMessage = a.message.substr(a.message.indexOf("]")+1).trim();
          return true;
        }
      } else if(fieldName === 'CourtStatus_') {
        if( ( a.message.search(/Legal_Court_Ordered_Or_VPA_Removals|Judicial_Determination_of_REFPP_Removal_after_27_March_2000___First_Re_Determination|Judicial_Determination_of_REFPP_Removal_after_27_March_2000_Subsequent_Re_Determination/) !==-1 ||
              a.message.search(/Judicial_Determination_of_REFPP_Removal_before_27_March_2000_First_Re_Determination|Judicial_Determination_of_REFPP_Removal_before_27_March_2000_Subsequent_Re_Determination|Voluntary_Placement_Removals_First_Re_Determination|Voluntary_Placement_Removals_Subsequent_Re_Determination|CourtStatus/) !==-1 ) 
          && a.message.indexOf("]") != -1 ){
          a.dispMessage = a.message.substr(a.message.indexOf("]")+1).trim();
          return true;
        }
      } else if(fieldName === 'RemovalHome_') {
        if( ( a.message.search(/Child_In_FosterCare_for_12_Months_or_More|Child_Not_In_FosterCare_for_12_Months_or_More_REFPP_Not_Due|RemovalHome/) != -1 ) && a.message.indexOf("]") != -1 ){
          a.dispMessage = a.message.substr(a.message.indexOf("]")+1).trim();
          return true;
        }
      } else {
        if(a.message.indexOf(fieldName) != -1 && a.message.indexOf("]") != -1 ){
          a.dispMessage = a.message.substr(a.message.indexOf("]")+1).trim();
          return true;
        }
      }
      return false;
    });
  }

  displayWorksheet(worksheet:string, clientId, transactionid, removalid){
    this.worksheetType = worksheet;
    this.deemedclientid = clientId;
  
      const inputRequest = {
        'transactionid': transactionid,
        'removalid': removalid,
        'type': worksheet,
        'documenttemplatekey': [
            'inhome'
        ],
        'isheaderrequired': false
    };
    const payload = {
      method: 'post',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest,
      documntkey: [
        'oohome'
    ],
    };
    this._commonHttpService.create(payload, 'titleive/fc/worksheet/getreportpdf').subscribe(
      response => {
        console.log(response, 'pdf response');
        setTimeout(() => window.open(response.data.documentpath), 2000);
     });
  
    }
    // (<any>$('#deemed-income-worksheet')).modal('show');

  // get detStatusDetails() {
  //   const temp = [];
  //   this.eligibilityPerioodData.forEach(a => {
  //   this.messagesagesb => {
  //         temp.push(b);
  //     });
  //   });
  //   return temp;
  // }

  getStatusDetails(status) {
    this.detStatusDetails = [status];

  }

  // get initialDetStatusDetails() {
  //   const temp = [];
  //   this.eligibilityPerioodData.forEach(a => {
  //     a.forEach(b => {
  //       if (b.v_sqnm_sw === 'I') {
  //         temp.push(b);
  //       }
  //     });
  //   });
  //   return temp;
  // }

  // get reDetStatusDetails() {
  //   const temp = [];
  //   this.eligibilityPerioodData.forEach(a => {
  //     a.forEach(b => {
  //       if (b.v_sqnm_sw !== 'I' && b.v_sqnm_sw !== 'R1E1' && b.v_sqnm_sw !== 'R2E1') {
  //         temp.push(b);
  //       }
  //     });
  //   });
  //   return temp;
  // }

  // get reDetEventStatusDetails() {
  //   const temp = [];
  //   this.eligibilityPerioodData.forEach(a => {
  //     a.forEach(b => {
  //       if (b.v_sqnm_sw === 'R1E1' || b.v_sqnm_sw === 'R2E1') {
  //         temp.push(b);
  //       }
  //     });
  //   });
  //   return temp;
  // }


  
  onCourtStatus(item) {
    console.log("myitem", item);
  }

}
