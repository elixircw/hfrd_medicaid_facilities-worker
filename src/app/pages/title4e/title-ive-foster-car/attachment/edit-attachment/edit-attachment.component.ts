import { Attachment } from '../../../../newintake/my-newintake/intake-attachments/_entities/attachmnt.model';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { Titile4eUrlConfig } from '../../../../title4e/_entities/title4e-dashboard-url-config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AlertService, GenericService, CommonHttpService, AuthService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { Observable } from 'rxjs/Rx';
declare var $: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'edit-attachment',
  templateUrl: './edit-attachment.component.html',
  styleUrls: ['./edit-attachment.component.scss']
})
export class EditAttachmentComponent implements OnInit {
    attachmentClassificationTypeDropDown$: Observable<DropdownModel[]>;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    attachmentClassificationtypelookup = [];
    attachmentClassificationtype = [];
    fileUpdate: FormGroup;
    token: AppUser;

    attachmentDetail: Attachment;
    @Output() attachment = new EventEmitter();
    isServiceCase = false;
    isCW: boolean;
    constructor(
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _dropDownService: CommonHttpService,
        private _service: GenericService<Attachment>,
        private _alertService: AlertService
    ) {
        this.token = this._authService.getCurrentUser();
    }

    ngOnInit() {
        this.isCW = this._authService.isCW();
        this.loadDropdown();
        this.fileUpdate = this.formBuilder.group({
            title: [''],
            description: ['', Validators.maxLength(150)],
            attachmentTypeKey: [''],
            attachmentClassificationTypeKey: ['']
        });
    }
    editForm(attachmentDetail) {
        this.fileUpdate.markAsPristine();
        this.fileUpdate.patchValue({
            title: attachmentDetail.title ? attachmentDetail.title : '',
            description: attachmentDetail.description ? attachmentDetail.description : '',
            attachmentTypeKey: '',
            attachmentClassificationTypeKey: ''
        });
        if (attachmentDetail.documentattachment) {
            this.fileUpdate.patchValue({
                attachmentTypeKey: attachmentDetail.documentattachment.attachmenttypekey ? attachmentDetail.documentattachment.attachmenttypekey : '',
                attachmentClassificationTypeKey: attachmentDetail.documentattachment.attachmentclassificationtypekey ? attachmentDetail.documentattachment.attachmentclassificationtypekey : ''
            });
        } else {
            attachmentDetail.documentattachment = Object.assign({});
        }
        this.attachmentDetail = attachmentDetail;
    }
    saveAttachmentDetails() {
        if (this.fileUpdate.value.title !== '' && this.fileUpdate.value.attachmentTypeKey !== '' && this.fileUpdate.value.attachmentClassificationTypeKey !== '') {
            this.attachmentDetail.title = this.fileUpdate.value.title;
            this.attachmentDetail.description = this.fileUpdate.value.description;
            this.attachmentDetail.documentattachment.attachmenttypekey = this.fileUpdate.value.attachmentTypeKey;
            this.attachmentDetail.documentattachment.attachmentclassificationtypekey = this.fileUpdate.value.attachmentClassificationTypeKey;
            this.attachmentDetail.documentattachment.updatedby = this.token.user.userprofile.displayname;
            this.attachmentDetail.documentattachment.attachmentdate = new Date();
            this.attachmentDetail.documentdate = new Date();
            this._service.endpointUrl = Titile4eUrlConfig.EndPoint.Attachment.SaveAttachmentUrl;
            this._service.createArrayList([this.attachmentDetail]).subscribe(
                (response) => {
                    this._alertService.success('Attachment updated successfully!');
                    (<any>$('#edit-attachment')).modal('hide');
                    this.attachment.emit('all');
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._alertService.error('Please fill all mandatory fields');
        }
    }
    private loadDropdown() {
        this._dropDownService
        .getSingle(
            {},
            Titile4eUrlConfig.EndPoint.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
        )
        .subscribe(data => {
            const dp_att_arr = [];
            if (data && data.length > 0) {
                this.attachmentClassificationtypelookup = data;
                for (let i = 0; i < this.attachmentClassificationtypelookup.length; i++) {
                    if (this.attachmentClassificationtypelookup[i].typedescription && dp_att_arr.indexOf(this.attachmentClassificationtypelookup[i].typedescription) < 0 ) {
                        if (this.isCW) {
                            if (this.attachmentClassificationtypelookup[i].typedescription.startsWith('CW-')) {
                                this.attachmentClassificationtype.push({typedescription: this.attachmentClassificationtypelookup[i].typedescription});
                                dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                            }
                        } else {
                            this.attachmentClassificationtype.push({typedescription:this.attachmentClassificationtypelookup[i].typedescription});
                            dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                        }                    }
                }

            }
        });
        const source = forkJoin(
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                Titile4eUrlConfig.EndPoint.Attachment.AttachmentTypeUrl + '?filter={"nolimit": true}'
            ),
            // this._dropDownService.getArrayList(
            //     {
            //         nolimit: true
            //     },
            //     CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
            // )
        )
            .map((result) => {
                return {
                    attachmentType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmenttypekey
                            })
                    ),
                    // attachmentClassificationType: result[1].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.typedescription,
                    //             value: res.attachmentclassificationtypekey
                    //         })
                    // )
                };
            })
            .share();
        this.attachmentTypeDropdown$ = source.pluck('attachmentType');
        // this.attachmentClassificationTypeDropDown$ = source.pluck('attachmentClassificationType');
    }
}
