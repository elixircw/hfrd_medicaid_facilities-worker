/// <reference types="dwt" />
import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { HttpHeaders, HttpSentEvent } from '@angular/common/http';
import { AppConfig } from '../../../../../app.config';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { CommonHttpService, AuthService, AlertService, GenericService, DataStoreService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { config } from '../../../../../../environments/config';
import { HttpService } from '../../../../../../app/@core/services/http.service';
import { FileUtils } from '../../../../../@core/common/file-utils';
import { AttachmentUpload } from '../../../../case-worker/_entities/caseworker.data.model';
import { AttachmentDetailComponent } from '../../../../newintake/my-newintake/intake-attachments/attachment-detail/attachment-detail.component';
import { AttachmentSubCategory } from '../../../../newintake/my-newintake/_entities/newintakeModel';
import { IntakeAssessmentRequestIds } from '../../../../provider-applicant/new-applicant/_entities/newintakeModel';
import { Attachment } from '../../../../newintake/my-newintake/intake-attachments/_entities/attachmnt.model';
import { IntakeStoreConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { Titile4eUrlConfig } from '../../../../title4e/_entities/title4e-dashboard-url-config';
import { Title4eFosterCareService } from '../../title4e-foster-care.service';

declare var $: any;
declare var Dynamsoft: any;
declare var EnumDWT_ImageType: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attachment-upload',
    templateUrl: './attachment-upload.component.html',
    styleUrls: ['./attachment-upload.component.scss']
})
export class AttachmentUploadComponent implements OnInit, OnDestroy {
    curDate: Date;
    fileToSave = [];
    // intakeNumber: any;
    uploadedFile = [];

    tabActive = false;
    daNumber: string;
    id: string;
    attachmentResponse: AttachmentUpload;
    userfilteredAttachmentGrid: Attachment[] = [];
    attachmentClassificationTypeDropDown$: Observable<DropdownModel[]>;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    private token: AppUser;
    @Input() intakeNumber: string;
    @Output() attachment = new EventEmitter();
    @ViewChild(AttachmentDetailComponent) attachmentDetail: AttachmentDetailComponent;
    /*Dynamo Soft */
    dwObject: any;
    fileName: string;
    isFileScanned: boolean;
    docNames: any;
    myData: any;
    fileExistsMsg: any;
    isUploading = false;
    disableScanBtn = false;
    isCW = true;
    attachmentClassificationtypelookup = [];
    attachmentClassificationtype = [];
    isAttachType = '';
    isCate = '';
    issubCate= '';
    showDialog: boolean;
    clientId: any;
    attachmenttype: string;
    personid= '';
    img_width: string;
    img_height: string;
    selectedInterpolation: number;
    _iLeft: any;
    _iTop: any;
    _iRight: any;
    _iBottom: any;
    DW_PreviewMode: any;
    isAttachDetail = false;
    dwtIntakeNo: any;
    subCategoryClassificationType$: Observable<AttachmentSubCategory[]>;
    subCategoryList$: Observable<AttachmentSubCategory[]>;
    createdCases = [];
    private assessmentRequestDetail: IntakeAssessmentRequestIds;
    store: any;
    showAssesment: number;
    paginationInfo: PaginationInfo = new PaginationInfo();
    assessmentTemplateID: any;
    assessmentTemplateName: any;
    subCategoryList = [];
    subType = [];


    constructor(
        private router: Router,
        private _dataStoreService: DataStoreService,
        private _service: GenericService<Attachment>,
        private _dropDownService: CommonHttpService,
        private route: ActivatedRoute,
        private _uploadService: NgxfUploaderService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _http: HttpService,
        private _titlefoasterservice: Title4eFosterCareService
    ) {
        this.docNames = [];
        this.id = route.snapshot.parent.params['id'];
        // this.intakeNumber = route.snapshot.params['intakeNumber'];
        this.token = this._authService.getCurrentUser();
        this.store = this._dataStoreService.getCurrentStore();
    }

    ngOnInit() {
        this.loadDropdown();
        this.curDate = new Date(); 
        this.clientId = this.route.snapshot.params['clientId'];
        console.log("rote..",this.route);
        this.attachmenttype = this.route.snapshot.params['attachmenttype'] || 'person';
        this._dropDownService
        .getSingle(
            {
                code:'person_id',
                value: this.clientId,
                ownerSystem: 'CJAMS',
                method: 'post'
            },
            'Documentproperties/ecmsclientdetails'
        )
        .subscribe((result) => {
          if(result && result.personId) {
            this.personid = result.personId;
          }
        });
        // (<any>$('#upload-attachment')).modal('show');
        const purposeSelected = this._dataStoreService.getData(IntakeStoreConstants.purposeSelected);
        console.log('purposeSelected', purposeSelected);
        this.createdCases = this._dataStoreService.getData(IntakeStoreConstants.createdCases);
    }

    ngOnDestroy() {
        // Dynamsoft.WebTwainEnv.Unload();
    }

    uploadFile(file: File | FileError): void {
        console.log('file', file);
        if (!(file instanceof Array)) {
            this._alertService.error('Please enter a valid file');
            // this.alertError(file);
            return;
        }
        file.map((item, index) => {
            const fileExt = item.name
                .toLowerCase()
                .split('.')
                .pop();
            if (
                fileExt === 'mp3' ||
                fileExt === 'ogg' ||
                fileExt === 'wav' ||
                fileExt === 'acc' ||
                fileExt === 'flac' ||
                fileExt === 'aiff' ||
                fileExt === 'mp4' ||
                fileExt === 'mov' ||
                fileExt === 'avi' ||
                fileExt === '3gp' ||
                fileExt === 'wmv' ||
                fileExt === 'mpeg-4' ||
                fileExt === 'pdf' ||
                fileExt === 'txt' ||
                fileExt === 'docx' ||
                fileExt === 'doc' ||
                fileExt === 'xls' ||
                fileExt === 'xlsx' ||
                fileExt === 'jpeg' ||
                fileExt === 'jpg' ||
                fileExt === 'png' ||
                fileExt === 'gif' ||
                fileExt === 'ppt' ||
                fileExt === 'pptx' ||
                fileExt === 'gif'
            ) {
                // const uploadedFile = [];
                // uploadedFile.push(item);
                // this.uploadedFile = this.uploadedFile.concat(uploadedFile);
                console.log('item', item);
                this.uploadedFile.push(item);
                index = this.uploadedFile.length - 1;
                this.uploadAttachment(index);
                const audio_ext = ['mp3', 'ogg' , 'wav', 'acc', 'flac', 'aiff'];
                const video_ext = ['mp4', 'avi' , 'mov', '3gp', 'wmv', 'mpeg-4'];
                if ( audio_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[index].attachmenttypekey = 'Audio';
                } else if ( video_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[index].attachmenttypekey = 'Video';
                } else {
                    this.uploadedFile[index].attachmenttypekey = 'Document';
                }
                this.isAttachType = this.uploadedFile[index].attachmenttypekey;
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error(fileExt + " format can't be uploaded");
                return;
            }
            // for (let i = 0; i < this.uploadedFile.length; i++) {
            //     this.uploadAttachment(i);
            // }
        });
        // this.uploadedFile.push(file);
        // this.uploadedFile = this.uploadedFile.concat(uploadedFile);
        // this.uploadedFile.map((item) => {
        //     this.saveAttachment(item);
        // });
    }

    humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    uploadAttachment(index) {

        this.isAttachType = '';
        this.isCate = '';
        const _self = this;
        const isDataFilled =  setInterval(function() {
                if (_self.isAttachType !== '' && _self.isCate !== ''  &&  _self.issubCate !== ''  ) {
                    console.log('filed');
                    clearInterval(isDataFilled);
                    _self.processResponseData(index);
                } else {
                    console.log('data not filled');
                }
          }, 1000);
    }
    processResponseData(index) {
        console.log('index', index);
        console.log('uploaded file len', this.uploadedFile.length);

        const workEnv = config.workEnvironment;
        let uploadUrl = '';
        const dynam  = this.isAttachType + '|' + this.isCate + '|' + this.issubCate ;

        if (workEnv === 'state') {
            uploadUrl = AppConfig.baseUrl  + '/attachment/v1' + '/' +
               // NewUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=' + this.intakeNumber;
               Titile4eUrlConfig.EndPoint.Attachment.UploadAttachmentUrl +
                '?access_token=' + this.token.id + '&' + 'srno=' +
                 this.clientId + '&' + 'docsInfo=' + dynam + '&attachmenttype=' + this.attachmenttype + '&personid=' + this.personid;
            console.log('state', uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + Titile4eUrlConfig.EndPoint.Attachment.UploadAttachmentUrl +
            '?access_token=' + this.token.id + '&' + 'srno=' + this.clientId
             + 'docsInfo=' + dynam + '&attachmenttype=' + this.attachmenttype + '&personid=' + this.personid;
            console.log('local', uploadUrl);
        }

        this._uploadService

            .upload({
                url: uploadUrl,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile[index],
                process: true
            })
            .subscribe(
                (response) => { 
                console.log("response...",response);
                    if (response.status) {
                        // this.progress.percentage = response.percent;
                        this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        this.attachmentResponse = response.data;
                        this.fileToSave.push(response.data);
                        this.fileToSave[this.fileToSave.length - 1].documentattachment = {
                            assessmenttemplateid: '',
                            attachmenttypekey: '',
                            attachmentclassificationtypekey: '',
                            attachmentclassificationsubtypekey: '',
                            attachmentdate: new Date(),
                            sourceauthor: '',
                            attachmentsubject: '',
                            sourceposition: '',
                            attachmentpurpose: '',
                            sourcephonenumber: '',
                            acquisitionmethod: '',
                            sourceaddress: '',
                            locationoforiginal: '',
                            insertedby: this.token.user.userprofile.displayname,
                            note: '',
                            updatedby: this.token.user.userprofile.displayname,
                            activeflag: 1
                        };
                        this.fileToSave[this.fileToSave.length - 1].description = '';
                        this.fileToSave[this.fileToSave.length - 1].personid = this.personid;
                        this.fileToSave[this.fileToSave.length - 1].servicecaseid = null;
                        this.fileToSave[this.fileToSave.length - 1].attachmenttype = 'person',
                        this.fileToSave[this.fileToSave.length - 1].srno = '';
                        this.fileToSave[this.fileToSave.length - 1].documentdate = new Date();
                        this.fileToSave[this.fileToSave.length - 1].title = '';
                        this.fileToSave[this.fileToSave.length - 1].objecttypekey = 'Person';
                        this.fileToSave[this.fileToSave.length - 1].rootobjecttypekey = 'Person';
                        this.fileToSave[this.fileToSave.length - 1].activeflag = 1;
                        this.fileToSave[this.fileToSave.length - 1].clientId = this.clientId;
                        this.fileToSave[this.fileToSave.length - 1].insertedby = this.token.user.userprofile.displayname;
                        this.fileToSave[this.fileToSave.length - 1].updatedby = this.token.user.userprofile.displayname;

                        // this._alertService.success('File Uploaded Succesfully!');
                    }
                },
                (err) => {
                    console.log(err);
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    this.uploadedFile.splice(index, 1);
                },
                () => {
                    console.log('complete', this.fileToSave[this.fileToSave.length - 1]);
                }
            );
    }
    formTab() {
        this.attachmentDetail.patchAttachmentDetail(this.attachmentResponse);
        this.attachmentDetail.loadDropdown();
        this.tabActive = true;
        $('#step1').removeClass('active');
        $('#complete').addClass('active');
    }
    modalDismiss() {
        (<any>$('#upload-attachment')).modal('hide');
    }
    deleteUpload(index) {
        // const curIndex = this.uploadedFile.length - index - 1;
        this.uploadedFile.splice(index, 1);
        this.fileToSave.splice(index, 1);
    }
    clearAllUpload() {
        this.uploadedFile = [];
        this.fileToSave = [];
    }
    titleUpdate(event, index) {
        // const curIndex = this.fileToSave.length - index;
        // this.fileToSave[index].title = event.target.value;
        this.uploadedFile[index].title = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidTitle = false;
        } else {
            this.uploadedFile[index].invalidTitle = true;
        }
    }
    descUpdate(event, index) {
        // const curIndex = this.fileToSave.length - index;
        this.uploadedFile[index].description = event.target.value;
    }
    docDateUpdate(event, index) {
        this.uploadedFile[index].docDate = event.target.value;
    }
    typeUpdate(event, index) {
        // const curIndex = this.fileToSave.length - index;
        this.uploadedFile[index].attachmenttypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentType = false;
        } else {
            this.uploadedFile[index].invalidAttachmentType = true;
        }
    }
    // categoryUpdate(event, index) {
    //     // const curIndex = this.fileToSave.length - index;
    //     this.uploadedFile[index].attachmentclassificationtypekey = event.target.value;
    //     if (event.target.value) {
    //         this.uploadedFile[index].invalidAttachmentClassify = false;
    //     } else {
    //         this.uploadedFile[index].invalidAttachmentClassify = true;
    //     }
    //     if (this._authService.getCurrentUser().user.userprofile.teamtypekey === 'AS') {
    //         if (this.createdCases.length > 0) {
    //             for (let i = 0; i < this.createdCases.length; i++) {
    //                 // if (this.createdCases[i].subSeriviceTypeValue === 'Project Home' || this.createdCases[i].subSeriviceTypeValue === 'Adult Foster Care') {
    //                 if (event.target.value === 'Assessment Document') {
    //                     this.getSubCategory();
    //                 } else {
    //                     this.subCategoryList$ = null;
    //                 }
    //                 // }
    //             }
    //         }
    //     }
    // }
    categoryUpdate(event, index) {
        if (event.target.value !== '')  {
            this.isCate  = event.target.value;
        }
        this.issubCate  = '';
        this.uploadedFile[index].attachmentclassificationsubtypekey = '';
        this.uploadedFile[index].attachmentClassificationsubtype = [];
        this.uploadedFile[index].attachmentclassificationtypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentClassify = false;
            for (let i = 0; i < this.attachmentClassificationtypelookup.length; i++) {
                if (this.attachmentClassificationtypelookup[i].typedescription === this.uploadedFile[index].attachmentclassificationtypekey) {
                    this.uploadedFile[index].attachmentClassificationsubtype.push({subcategory: this.attachmentClassificationtypelookup[i].subcategory});
                }
            }
        } else {
            this.uploadedFile[index].invalidAttachmentClassify = true;
        }
    }
    subcategoryUpdate(event, index) {
        if (event.target.value !== '')  {
            this.issubCate  = event.target.value;
        }
        this.uploadedFile[index].attachmentclassificationsubtypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentsubClassify = false;
        } else {
            this.uploadedFile[index].invalidAttachmentsubClassify = true;
        }
    }

    getSubCategory() {
        console.log('store...', this.store);
        if (this.store[IntakeStoreConstants.purposeSelected]) {
            const purpose = this.store[IntakeStoreConstants.purposeSelected];
            this.subType = this.store[IntakeStoreConstants.createdCases];
            // if (this.store['purposesubtype']) {
            // const subType =  this.store['purposesubtype'];
            if (this.subType && this.subType.length > 0) {
                const purposeSubType = this.subType[0].subServiceTypeID;
                this.subCategoryClassificationType$ = this._dropDownService
                    .getArrayList(
                        {
                            where: {
                                intakeservicerequesttypeid: purpose.value,
                                intakeservicerequestsubtypeid: purposeSubType,
                                agencycode: 'AS',
                                target: 'Intake'
                            },
                            method: 'get'
                        },
                        'admin/assessmenttemplate/listassessmenttemplate?filter'
                    )
                    .map(result => {
                        return result;
                    });
                this.subCategoryClassificationType$.subscribe(result => {
                    console.log('result', result);
                    if (result && result.length > 0) {
                        this.subCategoryList = [];
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].isrequired === true) {
                                this.subCategoryList.push(result[i]);
                            }
                        }
                    }
                    this._dataStoreService.setData('categorySubType', this.subCategoryList);
                    // this._dataStoreService.setData(IntakeStoreConstants.attachements, this.subCategoryList);
                    this.subCategoryList$ = Observable.of(this.subCategoryList);
                });
            }
        }
    }

    onSubcategory(categoryid) {
        this.assessmentTemplateID = categoryid.target.value;
    }



    saveAttachmentDetails() {
        console.log('Called afterscanning ');
        if (this.uploadedFile.length !== this.fileToSave.length) {
            this._alertService.error('Please wait till files get uploaded');
        } else {
            this.uploadedFile.map((item, index) => {
                this.fileToSave[index].title = item.title;
                this.fileToSave[index].description = item.description;
                this.fileToSave[index].documentattachment.attachmenttypekey = item.attachmenttypekey;
                this.fileToSave[index].documentattachment.attachmentclassificationtypekey = item.attachmentclassificationtypekey;
                this.fileToSave[index].documentattachment.attachmentclassificationsubtypekey = item.attachmentclassificationsubtypekey;

                this.fileToSave[index].documentattachment.assessmenttemplateid = this.assessmentTemplateID;
                // this.fileToSave[index].documentattachment.assessmenttemplatename = this.assessmentTemplateName;
            });
            const AttachValidate = this.fileToSave.filter((wer) => !wer.documentattachment.attachmentclassificationtypekey || !wer.documentattachment.attachmentclassificationsubtypekey || !wer.documentattachment.attachmenttypekey || !wer.title);
            // this.fileToSave.map((wer) => {
            if (AttachValidate.length === 0) {
                this._service.endpointUrl = Titile4eUrlConfig.EndPoint.Attachment.SaveAttachmentUrl;
                console.log('fileToSave', this.fileToSave[0]);
                this._service.createArrayList(this.fileToSave).subscribe(
                    (response) => {
                        response.map((item, index) => {
                            if (item.documentpropertiesid) {
                                response.splice(index, 1);
                                this.fileToSave.splice(index, 1);
                                this.uploadedFile.splice(index, 1);
                            }
                            const docProp = response.filter((docId) => docId.Documentattachment);
                            if (docProp.length === 0) {
                                this._alertService.success('Attachment(s) added successfully!');
                            (<any>$('#upload-attachment')).modal('hide');
                            (<any>$('#upload-scanner-attachment')).modal('hide');
                            this._titlefoasterservice.uploadCompleted();
                            this.fileToSave = [];
                                this.uploadedFile = [];
                                // this.dynamoScanReset();
                            }
                            // this.personattachment('4545');
                            if (item.Documentattachment) {
                                const attPos = index + 1;
                                this._alertService.error(item.Documentattachment + ' for Attachment ' + attPos);
                            } else if (!item.documentpropertiesid) {
                                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                            }
                        });
                        // if (response[0] && response[0].documentpropertiesid) {
                        //     this._alertService.success('Attachment added successfully!');
                        //     // this.modalDismiss.emit();
                        //     // let currentUrl = '/pages/newintake/my-newintake';
                        //     // if (this.id) {
                        //     //     currentUrl = '/pages/newintake/my-newintake/' + this.id;
                        //     // }
                        //     // this.router.navigateByUrl(currentUrl).then(() => {
                        //     //     this.router.navigated = true;
                        //     //     this.router.navigate([currentUrl]);
                        //     // });
                        //     this.attachment.emit('all');
                        //     (<any>$('#upload-attachment')).modal('hide');
                        //     this.fileToSave = [];
                        //     this.uploadedFile = [];
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error('Please fill all mandatory fields');
                this.uploadedFile.map((item) => {
                    if (!item.title) {
                        item.invalidTitle = true;
                    } else {
                        item.invalidTitle = false;
                    }
                    if (!item.attachmentclassificationtypekey) {
                        item.invalidAttachmentClassify = true;
                    } else {
                        item.invalidAttachmentClassify = false;
                    }
                    if (!item.attachmentclassificationsubtypekey) {
                        item.invalidAttachmentsubClassify = true;
                    } else {
                        item.invalidAttachmentsubClassify = false;
                    }
                    if (!item.attachmenttypekey) {
                        item.invalidAttachmentType = true;
                    } else {
                        item.invalidAttachmentType = false;
                    }
                });
            }
        }
        // });
        // this.router.routeReuseStrategy.shouldReuseRoute = function() {
        //     return false;
        // };
    }
    private loadDropdown() {
        this._dropDownService
        .getSingle(
            {},
            Titile4eUrlConfig.EndPoint.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
        )
        .subscribe(data => {
            const dp_att_arr = [];
            if (data && data.length > 0) {
                this.attachmentClassificationtypelookup = data;
                for (let i = 0; i < this.attachmentClassificationtypelookup.length; i++) {
                    if (this.attachmentClassificationtypelookup[i].typedescription && dp_att_arr.indexOf(this.attachmentClassificationtypelookup[i].typedescription) < 0 ) {
                        if (this.isCW) {
                            if (this.attachmentClassificationtypelookup[i].typedescription.startsWith('CW-')) {
                                this.attachmentClassificationtype.push({typedescription: this.attachmentClassificationtypelookup[i].typedescription});
                                dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                            }
                        } else {
                            this.attachmentClassificationtype.push({typedescription:this.attachmentClassificationtypelookup[i].typedescription});
                            dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                        }
                    }
                }

            }
        });
        const source = forkJoin(
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                Titile4eUrlConfig.EndPoint.Attachment.AttachmentTypeUrl + '?filter={"nolimit": true}'
            ),
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                Titile4eUrlConfig.EndPoint.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
            )
        )
            .map((result) => {
                return {
                    attachmentType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmenttypekey
                            })
                    )
                    // ,
                    // attachmentClassificationType: result[1].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.typedescription,
                    //             value: res.attachmentclassificationtypekey
                    //         })
                    // )
                };
            })
            .share();
        this.attachmentTypeDropdown$ = source.pluck('attachmentType');
        // this.attachmentClassificationTypeDropDown$ = source.pluck('attachmentClassificationType');
        // this.attachmentClassificationTypeDropDown$.subscribe((response) => {
        //     console.log('response...', response);
        // });
    }

}
