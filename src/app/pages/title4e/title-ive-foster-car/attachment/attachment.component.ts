import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Attachment } from '../../../newintake/my-newintake/intake-attachments/_entities/attachmnt.model';
import { CommonHttpService, AuthService, AlertService, GenericService, DataStoreService } from '../../../../@core/services';
import { Titile4eUrlConfig } from '../../../title4e/_entities/title4e-dashboard-url-config';
// import { EditAttachmentComponent } from '../../../case-worker/dsds-action/attachment/edit-attachment/edit-attachment.component';
import { EditAttachmentComponent } from '../../../title4e/title-ive-foster-car/attachment/edit-attachment/edit-attachment.component';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { config } from '../../../../../environments/config';
import { AppConfig } from '../../../../app.config';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { Title4eFosterCareService } from '../title4e-foster-care.service';

@Component({
  selector: 'attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit {

  personid: any = '';
  downldSrcURL: any;
  documentPropertiesId: any;
  documentId: any;
  baseUrl: string;
  clientId:any;
  @ViewChild(EditAttachmentComponent) editAttach: EditAttachmentComponent;
  userfilteredAttachmentGrid: Attachment[] = [];
  constructor(
    private route: ActivatedRoute,
    private _dropDownService: CommonHttpService,
    private _service: GenericService<Attachment>,
    private _alertService: AlertService,
    private _titlefoasterservice: Title4eFosterCareService
  ) {
    this.baseUrl = AppConfig.baseUrl;
   }

  ngOnInit() {
    console.log('this.route.snapshot.params', this.route.snapshot.params);
    this.clientId =  this.route.snapshot.params['clientId'];
    this._dropDownService
    .getSingle(
        {
            code:'person_id',
            value: this.clientId,
            ownerSystem: 'CJAMS',
            method: 'post'
        },
        'Documentproperties/ecmsclientdetails'
    )
    .subscribe((result) => {
      if(result && result.personId) {
        this.personid = result.personId;
        this.personattachment(this.personid);

      }

    });
    this._titlefoasterservice.fileuploadcompleted
    .subscribe((value) => {
      if(value === true) {
        this.personattachment(this.personid);
      }
    });
  }

  humanizeBytes(bytes: number): string {
    if (bytes === 0) {
        return '0 Byte';
    }
    const k = 1024;
    const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
    const i: number = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
}
  private personattachment(personid) {
    console.log('perdonal...', personid);
    this.userfilteredAttachmentGrid = [];
    this._dropDownService
        .getArrayList(
            new PaginationRequest({
                nolimit: true,
                method: 'get'
            }),
            Titile4eUrlConfig.EndPoint.Attachment.PersonAttachmentGridUrl + '/' + this.personid + '/' + this.personid + '?data'
            // this.personid + '?data'
        )
        .subscribe((result) => {
            result.map(item => {
                 item.numberofbytes = this.humanizeBytes(item.numberofbytes);
            });
            this.userfilteredAttachmentGrid = result;
        });
}
editAttachment(modal) {
  this.editAttach.editForm(modal);
  (<any>$('#edit-attachment')).modal('show');
}

confirmDelete(modal) {
  this.documentPropertiesId = modal.documentpropertiesid;
  this.documentId = modal.filename;
  console.log("modal...",modal);
  (<any>$('#delete-attachment')).modal('show');
}
deleteAttachment() {
  const workEnv = config.workEnvironment;
  if (workEnv === 'state') {
      this._service.endpointUrl =
      Titile4eUrlConfig.EndPoint.Attachment.DeleteAttachmentUrl;
      const id = this.documentPropertiesId + '&' + this.documentId;
      this._service.remove(id).subscribe(
          result => {
              (<any>$('#delete-attachment')).modal('hide');
              this._alertService.success('Attachment Deleted successfully!');
              this.personattachment(this.personid);
          },
          err => {
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          });
  } else {
      this._service.endpointUrl =
      Titile4eUrlConfig.EndPoint.Attachment.DeleteAttachmentUrl;
      this._service.remove(this.documentPropertiesId).subscribe(
          result => {
              (<any>$('#delete-attachment')).modal('hide');

              // this.loadAttachmentList();
              // this.getContactRecordings();
              this._alertService.success('Attachment Deleted successfully!');
              this.personattachment(this.personid);
          },
          err => {
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          }
      );
  }

}
checkFileType(file: string, accept: string): boolean {
  if (accept && file) {
      const acceptedFilesArray = accept.split(',');
      return acceptedFilesArray.some(type => {
          const validType = type.trim();
          if (validType.charAt(0) === '.') {
              return file.toLowerCase().endsWith(validType.toLowerCase());
          }
          return false;
      });
  }
  return true;
}

downloadFile(s3bucketpathname) {
  const workEnv = config.workEnvironment;
  if (workEnv === 'state') {
      this.downldSrcURL = this.baseUrl + '/attachment/v1' + s3bucketpathname;
  } else {
      // 4200
      this.downldSrcURL = s3bucketpathname;
  }
  console.log('this.downloadSrc', this.downldSrcURL);
  window.open(this.downldSrcURL, '_blank');
}

 
}
