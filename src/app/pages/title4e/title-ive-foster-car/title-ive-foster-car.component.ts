import { Component, OnInit, Input } from '@angular/core';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AlertService, CommonHttpService, SessionStorageService, DataStoreService } from '../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { TitleiveURLs } from '../config';
import { Titile4eUrlConfig } from '../_entities/title4e-dashboard-url-config';
import { Title4eService } from '../../title4e/services/title4e.service';
import { NavigationUtils } from '../../../pages/_utils/navigation-utils.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { toString } from 'lodash';
import { PeriodTablePopUpComponent } from '../period-table-pop-up/period-table-pop-up.component';

import { DatePipe } from '@angular/common';
import { AuthService } from '../../../@core/services/auth.service';
import value from '*.json';
import { NullAstVisitor } from '@angular/compiler';
import { Title4eFosterCareService } from './title4e-foster-care.service';
import { AppConstants } from '../../../@core/common/constants';
import {Location} from '@angular/common';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'title-ive-foster-car',
  templateUrl: './title-ive-foster-car.component.html',
  styleUrls: ['./title-ive-foster-car.component.scss'],
  providers: [DatePipe]
})
export class TitleIveFosterCarComponent implements OnInit {
  // client_id = '1008560';
  SupervisorDetails: any;
  ageCalculate: any;
  WorkersList: any;
  supervisorList: any[];
  caseNumber: any;
  workerList: any[] = [];
  eventTableData: any[] = [];
  tempRows1 = Array();
  typeOfPlacement = ['Foster Care', 'Therapeutic foster care', 'Group Home', 'RCC', 'Formal Kinship Care', 'Provisional Kinship Care', 'Pre-Adoptive Home', 'Adoptive Home'];
  mainTanble = [];
  selectedPeriodItems = [];
  STATUS = ['Pending', 'Returned', 'Completed', 'Submitted', 'In progress', 'Approved'];
  TYPE = ['Foster-Care', 'Adoption', 'GAP', 'Foster Care 18-21', 'Adoption 18-21', 'GAP 18-21'];
  ELIGIBILITYSTATUS = ['Initial Determination', 'Corrected initial determination', 'Redetermination', 'Corrected redetermination', '180 day VPA redetermination'];
  JURISDICTION = ['ELK RDG', 'LINTHICUM', 'ELCOTT CITY'];
  approvesObj = [];
  approveData = [{
    status: '',
    type: '',
    elegibilityStatus: '',
    jurisdiction: ''
  }];
  determinationTransactionId: string;
  FilterData = [];
  selectedSuperviser: any;
  worksheetForm: FormGroup;
  worksheetData: any;
  generalForm: FormGroup;
  ageDetailsForm: FormGroup;
  otherCriteriaRdForm: FormGroup;
  criteriaForm: FormGroup;
  deprivationForm: FormGroup;
  assetForm: FormGroup;
  incomeForm: FormGroup;
  removalHomeForm: FormGroup;
  // removalTypeForm: FormGroup;
  courtOrderForm: FormGroup;
  legalForm: FormGroup;
  demographicsForm: FormGroup;
  placementForm: FormGroup;
  missingFields: FormGroup;
  ssiForm: FormGroup;
  clientId: any;
  summaryInfo: any;
  userInfo: any;
  getUsersList: any[];
  removalId: any;
  placementId: any;
  constructor(
    private router: Router,
    private _location: Location,
    private fb: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    public dialog: MatDialog,
    private navigateutil: NavigationUtils,
    private title4eService: Title4eService,
    private datePipe: DatePipe,
    private storage: SessionStorageService,
    private _service: Title4eFosterCareService,
    private route: ActivatedRoute,
    private _authService: AuthService,

  ) { }
  ngOnInit() {
    this._dataStoreService.setData('addNarrative', false);
    this.userInfo = this._authService.getCurrentUser();
    this.clientId = this.route.snapshot.params['clientId'];
    this.removalId = this.route.snapshot.params['removalId'];
    this.placementId = this.route.snapshot.params['placementId'];
    this._service.placementId = this.placementId;
    console.log('placement id', this.placementId);
    this.createForm();
    // this.getSupervisorDetails();
    this.loadUsersList();
    this.getSummaryInfo();
    // this.clientId = this.title4eService.clientIdData;
    this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').valueChanges.subscribe(val => {
      if (val === 'None') {
        this.legalForm.controls.removalTypeForm.get('IsSafeHavenBaby').setValue('YES');
      }
      console.log(val);
    });
  }

  navigateCaseNumber(data: any){
    console.log("data...",data);
    this.navigateutil.routToServiceCase(data);
  }

  goBack() {
    this._location.back();
}

  getSummaryInfo() {
    const data = {
      clientId: this.clientId,
      removalId: this.removalId
    }
    this._commonHttpService.create(data, Titile4eUrlConfig.EndPoint.getEligibilityWorksheet).subscribe(
      (response: any) => {
        this.summaryInfo = _.assign({}, _.get(response, "summaryInfo.0"), _.get(response, "generalInfo.0"));
        this.summaryInfo.specialistName = _.get(this.userInfo, "user.userprofile.fullname");
      },
      (error) => {
        console.log(error);
        return false;
      }
    );
  }

  getEligibilityWorksheetData() {
    const clientId = this.title4eService.clientIdData;
    this._commonHttpService.getAll(Titile4eUrlConfig.EndPoint.getEligibilityWorksheet + this.clientId).subscribe(
      (response: any) => {
        this.worksheetData = response;
        this._service.fosterCareData = response;
        if (response.summaryInfo)
          this.setSummaryInfoData(response.summaryInfo[0]);
        if (response.generalInfo)
          this.setGeneralInfoData(response.generalInfo[0]);
        this.setcourtInfoData(response.courtInfo[0]);
        this.setRemovalInfoData(response.removalInfo[0]);
        this.setIncomeInfoData(response.incomeInfo[0]);
        this.setRedetCriteriaInfo(response.redetCriteriaInfo[0]);
        this.setEighteenCriteriaInfo(response.eighteenCriteriaInfo[0]);
        if (response.incomeInfo && response.incomeInfo.length > 0)
          this.setincomeInfoTable(response.incomeInfo[0].incometype);
        this.setotherCriteriaIdTable(response.initialCriteriaInfo[0].specifiedrelatives);
        this.setssissaCriteriaInfo(response.ssissaCriteriaInfo[0]);

      },
      (error) => {
        console.log(error);
        return false;
      }
    );
  }
  get incomeSummaryForm() {
    console.log('this.incomeSummaryForm', this.incomeForm);
    return this.incomeForm.get('incomeSummaryForm');
  }

  get incomeSUmmaryTable() {
    // console.log('this.incomeSummaryForm', JSON.stringify(this.incomeSummaryForm));
    return this.incomeSummaryForm.get('incomeSUmmaryTable') as FormArray;
  }
  get otherCriteriaIdForm() {
    return this.criteriaForm.get('otherCriteriaIdForm');
  }
  get otherCriteriaIdTable() {
    return this.otherCriteriaIdForm.get('otherCriteriaIdTable') as FormArray;
  }
  setincomeInfoTable(data) {
    if (data) {
      data.forEach(val => {
        this.incomeSUmmaryTable.push(
          this.fb.group({
            ClientID: val.client_id,
            involvedclientid: val.involvedclientid,
            involvedclientname: val.involvedclientname,
            InAU: val.assistanceunit,
            EarnedIncomeAmount: val.earnedincomeno,
            UnearnedIncomeType: [],
            UnearnedIncomeAmount: val.unearnedincome,
            IsIncomeDeemed: val.deemedincome,
            SupportExpenseAmount: val.supportexpenseamount,
            ivePersonIncomeiId: val.ivepersonincomeid
          })
        );
      });
    }

    console.log(this.incomeForm.get('incomeSummaryForm').get('incomeSUmmaryTable').value);
  }
  setotherCriteriaIdTable(data) {
    if (data) {
      data.forEach(val => {
        this.otherCriteriaIdTable.push(
          this.fb.group({
            SpecifiedRelativeClientID: val.specifiedrelativeclientid ? val.specifiedrelativeclientid.toString() : null,
            SpecifiedRelativeName: val.specifiedrelativename,
            SpecifiedRelativeDateChildLastLivedWith: val.specifiedrelativedatechildlastlivedwith ? moment(val.specifiedrelativedatechildlastlivedwith).format('MM/DD/YYYY') : null,
            SpecifiedRelativePhysicalAddress: val.specifiedrelativephysicaladdress,
            SpecifiedRelativeRelationshipID: val.specifiedrelativerelationshipid,
            specifiedrelativerelationship: val.specifiedrelativerelationship,
          })
        );
      });
    }
  }

  transform(value: string): string {
    const today = moment();
    const birthdate = moment(value);
    const years = today.diff(birthdate, 'years');
    this.ageCalculate = years + ' yr ';
    this.ageCalculate += today.subtract(years, 'years').diff(birthdate, 'months') + ' mon';
    return this.ageCalculate;
  }

  setSummaryInfoData(data) {

    console.log("removalage", (this.transform(data.dateofbirth)));
    this.worksheetForm.get('clientId').setValue(data.cjams_pid);
    this.worksheetForm.get('fullName').setValue(data.childname);
    this.worksheetForm.get('dob').setValue(data.dateofbirth);
    this.worksheetForm.get('JURISDICTION').setValue(data.childjurisdiction);
    this.worksheetForm.get('dateOfRemoval').setValue(this.convertToDate(data.removaldate));
    this.worksheetForm.get('casenumber').setValue(data.casenumber);
    this.worksheetForm.get('ageOfRemoval').setValue(data.removalage);
    this.worksheetForm.get('Agency').setValue(data.childagency);


    this.storage.setObj('casenumber', data.servicecaseid);

  }

  setGeneralInfoData(data) {
    this.generalForm.controls.placementForm.get('DateOfChildPlacement').setValue(this.convertToDate(data.dateofchildplacement));
    this.generalForm.controls.placementForm.get('DateOfLivingArrangement').setValue(this.convertToDate(data.dateoflivingarrangement));
    this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').setValue(data.islivingarrangementsameasplacement);
    this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').setValue(data.isplacementeligible);
    this.generalForm.controls.demographicsForm.get('IsDJSOrDSSChild').setValue(data.isdjsordsschild);
    this.incomeForm.controls.incomeSummaryForm.get('ClientID').setValue(data.cjamspid);
    this.generalForm.controls.demographicsForm.get('DateOfBirth').setValue(this.convertToDate(data.dateofbirth));
    this.generalForm.controls.demographicsForm.get('USCitizen').setValue(data.uscitizen);
    this.generalForm.controls.demographicsForm.get('QualifiedAlien').setValue(data.qualifiedalien);
    this.generalForm.controls.demographicsForm.get('QualifiedAlienStaus').setValue(data.qualifiedalienstaus);
    this.generalForm.controls.demographicsForm.get('AlienRegistrationNumber').setValue(data.alienregistrationnumber);
  }

  convertToDate(date) {
    return date ? new Date(date) : null;
  }

  setcourtInfoData(data) {
    this.legalForm.controls.removalTypeForm.get('RemovalId').setValue(data.removal_id);
    this.legalForm.controls.courtOrderForm.get('TypeOfCourtHearing').setValue(data.typeofcourthearing);
    this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').setValue(this.convertToDate(data.dateofcourthearing));
    this.legalForm.controls.courtOrderForm.get('CTWDecision').setValue(data.ctwdecision);
    this.legalForm.controls.courtOrderForm.get('DateOfFindingCTWDecision').setValue(this.convertToDate(data.dateoffindingctwdecision));
    this.legalForm.controls.courtOrderForm.get('NameOfSubjectCTWFinding').setValue(data.nameofsubjectctwfinding);
    this.legalForm.controls.courtOrderForm.get('ClientIDOfSubjectCTWFinding').setValue(data.clientidofsubjectctwfinding);
    this.legalForm.controls.courtOrderForm.get('RelationshipOfSubjectCTWFinding').setValue(data.relationshipofsubjectctwfinding);
    this.legalForm.controls.courtOrderForm.get('CourtOrderDelayRemoval').setValue(data.courtorderdelayremoval);
    this.legalForm.controls.courtOrderForm.get('CourtOrderDelayTimeFrame').setValue(data.courtorderdelaytimeframe);
    this.legalForm.controls.courtOrderForm.get('ReasonableEffortsMade').setValue(data.reasonableeffortsmade);
    this.legalForm.controls.courtOrderForm.get('ReasonableEffortsNotNecessaryDueToEmergentCircumstances').setValue(data.reasonableeffortsnotnecessaryduetoemergentcircumstances);
    this.legalForm.controls.courtOrderForm.get('REFPPNotDue').setValue(data.refppnotdue);
    this.legalForm.controls.courtOrderForm.get('DateOfJudicialFindingOfREFPP').setValue(this.convertToDate(data.dateofjudicialfindingofreffpp));
    this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfREFPP').setValue(this.convertToDate(data.dateofcurrentjudicialfindingofrefpp));
    this.legalForm.controls.courtOrderForm.get('DateOfPreviousJudicialFindingOfREFPP').setValue(this.convertToDate(data.dateofpreviousjudicialfindingofrefpp));
    this.legalForm.controls.courtOrderForm.get('DateOfSubsequentJudicialFindingOfREFPP').setValue(this.convertToDate(data.dateofsubsequentjudicialfindingofrefpp));
    this.legalForm.controls.courtOrderForm.get('DateOfBestInterestFinding').setValue(this.convertToDate(data.dateofbestinterestfinding));
    this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfBestInterest').setValue(this.convertToDate(data.dateofcurrentjudicialfindingofbestinterest));
    this.legalForm.controls.courtOrderForm.get('DateOfPreviousBestInterestFinding').setValue(this.convertToDate(data.dateofpreviousbestinterestfinding));
    this.legalForm.controls.courtOrderForm.get('DateOfSubsequentFindingOfBestInterest').setValue(this.convertToDate(data.dateofsubsequentfindingofbestinterest));
    this.legalForm.controls.courtOrderForm.get('FosterCarePermanencyPlan').setValue(data.fostercarepermanencyplan);
    this.legalForm.controls.courtOrderForm.get('IsIVEAgencyResponsibleForPlacementAndCare').setValue(data.iveagencyresponsibleforplacementandcare);
    this.legalForm.controls.courtOrderForm.get('MagistrateOrJudgeName').setValue(data.magistrateorjudgename);
    this.legalForm.controls.courtOrderForm.get('DateOfNextHearing').setValue(this.convertToDate(data.dateofnexthearing));
    this.legalForm.controls.courtOrderForm.get('SignedByJudge').setValue(data.issignedbyjudge);
    this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').setValue(this.convertToDate(data.dateagencylostlegalresponsibility));
    this.title4eService.getRemovalId(data.removal_id);
    this.legalForm.controls.courtOrderForm.get('CourtOrderDelayTimeFrame').setValue(data.courtorderdelaytimedays);
    this.legalForm.controls.courtOrderForm.get('IsIVEAgencyResponsibleForPlacementAndCare').setValue(data.isiveagencyresponsibleforplacementandcare);

  }

  setRemovalInfoData(data) {
    this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').setValue(data.typeofremoval);
    this.legalForm.controls.removalTypeForm.get('CourtOrdered').setValue(data.courtordered);
    this.legalForm.controls.removalTypeForm.get('TypeOfVPA').setValue(data.typeofvpa);
    this.legalForm.controls.removalTypeForm.get('IsSafeHavenBaby').setValue(data.issafehavenbaby);

    this.legalForm.controls.removalHomeForm.get('ChildPhysicalRemovalDate').setValue(data.childphysicalremovaldate);
    this.legalForm.controls.removalHomeForm.get('ChildPhysicalAddressAfterRemoval').setValue(data.childphysicaladdressafterremoval);
    this.legalForm.controls.removalHomeForm.get('ClientIDOfPersonFromWhomChildWasPhysicallyRemoved').setValue(data.clientnameofpersonfromwhomchildwasphysicallyremoved);
    //this.legalForm.controls.removalHomeForm.get('RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved').setValue(data.relationshipofpersonfromwhomchildwasphysicallyremoved);
    this.legalForm.controls.removalHomeForm.get('RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved').setValue(data.relationshipofpersonfromwhomchildwasphysicallyremoved ? data.relationshipofpersonfromwhomchildwasphysicallyremoved : null);
    this.legalForm.controls.removalHomeForm.get('DateOf1stParentSignatureOnVPA').setValue(this.convertToDate(data.dateof1stparentsignatureonvpa));
    this.legalForm.controls.removalHomeForm.get('DateOf2ndParentSignatureOnVPA').setValue(this.convertToDate(data.dateof2ndparentsignatureonvpa));
    this.legalForm.controls.removalHomeForm.get('MandatoryNoteOnMissing2ndParentSignatureOnVPA').setValue(data.mandatorynoteonmissing2ndparentsignatureonvpa);
    this.legalForm.controls.removalHomeForm.get('DateOfYouthSignatureOnVPA').setValue(this.convertToDate(data.dateofyouthsignatureonvpa));
    this.legalForm.controls.removalHomeForm.get('PreviousFosterCareEpisodeExist').setValue(data.previousfostercareepisodeexist);
    this.legalForm.controls.removalHomeForm.get('ReasonForExit').setValue(data.reasonforexit);
    // this.legalForm.controls.removalHomeForm.get('ReasonForExit').setValidators(disable)= data.previousfostercareepisodeexist=='Yes'?true:false;
    this.legalForm.controls.removalHomeForm.get('ExitCareDateFromPreviousFosterCareEpisode').setValue(this.convertToDate(data.exitcaredatefrompreviousfostercareepisode));
    this.legalForm.controls.removalHomeForm.get('DateOfGuardianSignatureOnVPA').setValue(this.convertToDate(data.dateofguardiansignatureonvpa));
    this.legalForm.controls.removalHomeForm.get('ClientIDWhoSignedVPA').setValue(data.clientnamewhosignedvpa);
    this.legalForm.controls.removalHomeForm.get('DateOfLDSSSignatureOnVPA').setValue(this.convertToDate(data.dateofldsssignatureonvpa));
    this.legalForm.controls.courtOrderForm.get('DateOfReasonableEffortsCourtHearing').setValue(this.convertToDate(data.dateofreasonableeffortscourthearing));
    this.criteriaForm.controls.otherCriteriaRdForm.get('IsReasonableEffortsFindingTimely').setValue(data.isreasonableeffortsfindingtimely);
    //this.legalForm.controls.removalHomeForm.get('reasonForexistSelected').setValue(data.reasonforexit);
  }

  setIncomeInfoData(data) {
    console.log('Set income info data', data);
    this.incomeForm.controls.incomeSummaryForm.get('EarnedIncomeAmount').setValue(data.earnedincome);
    this.incomeForm.controls.incomeSummaryForm.get('UnearnedIncomeAmount').setValue(data.unearnedincomeamount);
    this.incomeForm.controls.incomeSummaryForm.get('UnearnedIncomeType').setValue(data.unearnedincometype);
    this.incomeForm.controls.incomeSummaryForm.get('IsIncomeDeemed').setValue(data.isincomedeemed);
    this.incomeForm.controls.incomeSummaryForm.get('NoOfMembersInAU').setValue(data.noofmembersinau);
    this.incomeForm.controls.incomeSummaryForm.get('NoOfMembersNotInAU').setValue(data.noofmembersnotinau);
    this.incomeForm.controls.incomeSummaryForm.get('GrossIncome185PctForAU').setValue(data.grossincome185pct);
    this.incomeForm.controls.deprivationForm.get('ChildDeprivedOfParentalSupport').setValue(data.childdeprivedofparentalsupport);
    this.incomeForm.controls.deprivationForm.get('ReasonForAbsence').setValue(data.reasonforabsence);
    this.incomeForm.controls.incomeSummaryForm.get('InAU').setValue(data.inau);

    this.incomeForm.controls.incomeSummaryForm.get('TypeOfBenefit').setValue(data.typeofbenefit);
    this.incomeForm.controls.assetForm.get('AssetAllowance').setValue(data.assetallowance);
    this.incomeForm.controls.assetForm.get('AssetsMarketValue').setValue(data.assetsmarketvalue);
    this.incomeForm.controls.deprivationForm.get('DeprivationFactor').setValue(data.deprivationfactor);
    this.incomeForm.controls.deprivationForm.get('DateOfParentDeath').setValue(this.convertToDate(data.dateofparentdeath));
    this.incomeForm.controls.deprivationForm.get('IncarcerationDate').setValue(this.convertToDate(data.incarcerationdate));
    this.incomeForm.controls.deprivationForm.get('Unemployment').setValue(data.unemploymentorunderemployment);
    this.incomeForm.controls.incomeSummaryForm.get('AmountOfBenefit').setValue(data.benefitamount);
    this.incomeForm.controls.incomeSummaryForm.get('TotalChildCareCost').setValue(data.childcarecost);
    this.incomeForm.controls.incomeSummaryForm.get('StandardOfNeedForNotInAU').setValue(data.notinstandardunitno);
    this.incomeForm.controls.incomeSummaryForm.get('StandardOfNeedForAU').setValue(data.standardunitno);

    // this.setIncomeAmoutData();
  }

  setIncomeAmoutData() {
    // this.tempRows1.push(
    //   {
    //     unearnedIncome: [{
    //       UnearnedIncomeType: this.incomeForm.controls.incomeSummaryForm.get('UnearnedIncomeType').value,
    //       UnearnedIncomeAmount:  this.incomeForm.controls.incomeSummaryForm.get('UnearnedIncomeAmount').value
    //     }],
    //     IsIncomeDeemed: this.incomeForm.controls.incomeSummaryForm.get('IsIncomeDeemed').value,
    //     supportExpense: [{
    //       SupportExpenseAmount: this.incomeForm.controls.incomeSummaryForm.get('SupportExpenseAmount').value,
    //     }],
    //     ClientID: this.clientId,
    //     InAU: this.incomeForm.controls.incomeSummaryForm.get('InAU').value,
    //     earnedIncome: [{
    //       EarnedIncomeAmount: this.incomeForm.controls.incomeSummaryForm.get('EarnedIncomeAmount').value,
    //     }]
    //   });
    // this.incomeSummaryData.emit(this.tempRows1);
    // ,{
    //   ClientID: this.incomeForm.controls.incomeSummaryForm.get('ClientID').value,
    //   InAU: this.incomeForm.controls.incomeSummaryForm.get('InAU').value,
    //   EarnedIncomeAmount: this.incomeForm.controls.incomeSummaryForm.get('EarnedIncomeAmount').value,
    //   UnearnedIncomeType: this.incomeForm.controls.incomeSummaryForm.get('UnearnedIncomeType').value,
    //   UnearnedIncomeAmount: this.incomeForm.controls.incomeSummaryForm.get('UnearnedIncomeAmount').value,
    //   IsIncomeDeemed: this.incomeForm.controls.incomeSummaryForm.get('IsIncomeDeemed').value,
    //   SupportExpenseAmount: this.incomeForm.controls.incomeSummaryForm.get('SupportExpenseAmount').value
    // }
  }

  setRedetCriteriaInfo(data) {
    this.criteriaForm.controls.otherCriteriaRdForm.get('ChildBeenInFosterCare').setValue(data.childbeeninfostercare12monthormore);
  }


  setssissaCriteriaInfo(data) {
    this.criteriaForm.controls.ssiForm.get('ChildReceivingSSIOrSSADuringReviewPeriod').setValue(data.childreceivingssiorssa);
    this.criteriaForm.controls.ssiForm.get('IsTheAgencyTheRepresentativePayee').setValue(data.agencyrepresentativeflag);
    this.criteriaForm.controls.ssiForm.get('WHOISTHEREPERSINTIVEPAYEE').setValue(data.whoisrepresentativepayee);
    this.criteriaForm.controls.ssiForm.get('HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE').setValue(data.suspendssipaymentflag);
    this.criteriaForm.controls.ssiForm.get('ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE').setValue(data.reasonfornotsuspendingssipayment);
    this.criteriaForm.controls.ssiForm.get('ReasonForWhyTheAgencyISNOTTheRepresentativePayee').setValue(data.reasonforagencynotrepresentativepayee);
  }

  setEighteenCriteriaInfo(data) {
    this.criteriaForm.controls.ageDetailsForm.get('NameOfSecondaryEducationOrEquivalentProgram').setValue(data.nameofsecondaryeducationorequivalentprogram);
    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').setValue(data.startdateofsecondaryeducationorequivalentprogram);
    this.criteriaForm.controls.ageDetailsForm.get('NameOfpostSecondaryOrVocationalEducation').setValue(data.nameofpostsecondaryorvocationaleducation);
    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').setValue(data.startdateofpostsecondaryorvocationaleducation);
    this.criteriaForm.controls.ageDetailsForm.get('NameOfPromoteToEmploymentProgram').setValue(data.nameofpromotetoemploymentprogram);
    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').setValue(data.startdateofpromotetoemploymentprogram);
    this.criteriaForm.controls.ageDetailsForm.get('NameOfEmployer').setValue(data.nameofemployer);
    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').setValue(data.startdateofemployment);
    this.criteriaForm.controls.ageDetailsForm.get('HoursPerMonthEmployed').setValue(data.hourspermonthemployed);
    this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityType').setValue(data.childdisabilitytype);
    this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').setValue(data.childdisabilitystartdate);
    this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityEvaluationDocumentionDate').setValue(data.childdisabilityevaluationdocumentiondate);
    this.criteriaForm.controls.ageDetailsForm.get('IsThereValidSILAAgreement').setValue(data.istherevalidsilaagreement);
    this.criteriaForm.controls.ageDetailsForm.get('DateOfValidSILAAgreement').setValue(data.dateofvalidsilaagreement);
  }

  getTableData(tabdata) {
    console.log(tabdata);
    // this.tempRows1=tabdata;
    tabdata.controls.forEach(element => {
      this.tempRows1.push(
        {
          unearnedIncome: [{
            UnearnedIncomeType: element.value.UnearnedIncomeType,
            UnearnedIncomeAmount: element.value.UnearnedIncomeAmount
          }],
          IsIncomeDeemed: element.value.IsIncomeDeemed,
          supportExpense: [{
            SupportExpenseAmount: element.value.SupportExpenseAmount,
          }],
          ClientID: element.value.ClientId,
          InAU: element.value.InAU,
          earnedIncome: [{
            EarnedIncomeAmount: element.value.EarnedIncomeAmount,
          }]
        });
    });

  }



  setEligibilityWorksheetData(data) {

    //   this.worksheetForm.get('clientId').setValue(data.clientid);
    //   this.worksheetForm.get('fullName').setValue(data.firstname + ' ' + (data.middlename ? (data.middlename + ' ') : '') + data.lastname );
    //   this.worksheetForm.get('dob').setValue(data.dateofbirth);
    //   this.worksheetForm.get('JURISDICTION').setValue(data.county);
    //   this.worksheetForm.get('dateOfRemoval').setValue(data.v_removal_dt);
    //   this.generalForm.controls.placementForm.get('DateOfChildPlacement').setValue(data.dateofchildplacement);
    //  this.generalForm.controls.placementForm.get('DateOfLivingArrangement').setValue(data.dateoflivingarrangement);
    //   this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').setValue(data.islivingarrangementsameasplacement);

    //   this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').setValue(data.isplacementeligible);
    //   this.generalForm.controls.demographicsForm.get('IsDJSOrDSSChild').setValue(data.isdjsordsschild);
    //   this.incomeForm.controls.incomeSummaryForm.get('ClientID').setValue(data.client_id);
    //   this.generalForm.controls.demographicsForm.get('DateOfBirth').setValue(data.dateofbirth);
    //   this.generalForm.controls.demographicsForm.get('USCitizen').setValue(data.uscitizen);
    //   this.generalForm.controls.demographicsForm.get('QualifiedAlien').setValue(data.qualifiedalien);
    //   this.generalForm.controls.demographicsForm.get('QualifiedAlienStaus').setValue(data.qualifiedalienstaus);
    //   this.generalForm.controls.demographicsForm.get('AlienRegistrationNumber').setValue(data.alienregistrationnumber);
    //   this.legalForm.controls.courtOrderForm.get('RemovalId').setValue(data.removalid);
    //   this.legalForm.controls.courtOrderForm.get('TypeOfCourtHearing').setValue(data.typeofcourthearing);
    //   this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').setValue(data.dateofcourthearing);
    //   this.legalForm.controls.courtOrderForm.get('CTWDecision').setValue(data.ctwdecision);
    //   this.legalForm.controls.courtOrderForm.get('DateOfFindingCTWDecision').setValue(data.dateoffindingctwdecision);
    //   this.legalForm.controls.courtOrderForm.get('NameOfSubjectCTWFinding').setValue(data.nameofsubjectctwfinding);
    //   this.legalForm.controls.courtOrderForm.get('ClientIDOfSubjectCTWFinding').setValue(data.clientidofsubjectctwfinding);
    //   this.legalForm.controls.courtOrderForm.get('RelationshipOfSubjectCTWFinding').setValue(data.relationshipofsubjectctwfinding);
    //   this.legalForm.controls.courtOrderForm.get('CourtOrderDelayRemoval').setValue(data.courtorderdelayremoval);
    //   this.legalForm.controls.courtOrderForm.get('CourtOrderDelayTimeFrame').setValue(data.courtorderdelaytimeframe);
    //   this.legalForm.controls.courtOrderForm.get('ReasonableEffortsMade').setValue(data.reasonableeffortsmade);
    //   this.legalForm.controls.courtOrderForm.get('ReasonableEffortsNotNecessaryDueToEmergentCircumstances').setValue(data.reasonableeffortsnotnecessaryduetoemergentcircumstances);
    //   this.legalForm.controls.courtOrderForm.get('REFPPNotDue').setValue(data.refppnotdue);
    //   this.legalForm.controls.courtOrderForm.get('DateOfReasonableEffortsCourtHearing').setValue(data.dateofreasonableeffortscourthearing);
    //   this.legalForm.controls.courtOrderForm.get('DateOfJudicialFindingOfREFPP').setValue(data.dateofjudicialfindingofreffpp);
    //   this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfREFPP').setValue(data.dateofcurrentjudicialfindingofrefpp);
    //   this.legalForm.controls.courtOrderForm.get('DateOfPreviousJudicialFindingOfREFPP').setValue(data.dateofpreviousjudicialfindingofrefpp);
    //   this.legalForm.controls.courtOrderForm.get('DateOfSubsequentJudicialFindingOfREFPP').setValue(data.dateofsubsequentjudicialfindingofrefpp);
    //   this.legalForm.controls.courtOrderForm.get('DateOfBestInterestFinding').setValue(data.dateofbestinterestfinding);
    //   this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfBestInterest').setValue(data.dateofcurrentjudicialfindingofbestinterest);
    //   this.legalForm.controls.courtOrderForm.get('DateOfPreviousBestInterestFinding').setValue(data.dateofpreviousbestinterestfinding);
    //   this.legalForm.controls.courtOrderForm.get('DateOfSubsequentFindingOfBestInterest').setValue(data.dateofsubsequentfindingofbestinterest);
    //   this.legalForm.controls.courtOrderForm.get('FosterCarePermanencyPlan').setValue(data.fostercarepermanencyplan);
    //   this.legalForm.controls.courtOrderForm.get('IsIVEAgencyResponsibleForPlacementAndCare').setValue(data.iveagencyresponsibleforplacementandcare);
    //   this.legalForm.controls.courtOrderForm.get('MagistrateOrJudgeName').setValue(data.magistrateorjudgename);
    //   this.legalForm.controls.courtOrderForm.get('DateOfNextHearing').setValue(data.dateofnexthearing);
    //   this.legalForm.controls.courtOrderForm.get('SignedByJudge').setValue(data.issignedbyjudge);
    //   this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').setValue(data.dateagencylostlegalresponsibility);
    //   this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').setValue(data.typeofremoval);
    //   this.legalForm.controls.removalTypeForm.get('CourtOrdered').setValue(data.courtordered);
    //   this.legalForm.controls.removalTypeForm.get('TypeOfVPA').setValue(data.typeofvpa);
    //   this.legalForm.controls.removalTypeForm.get('IsSafeHavenBaby').setValue(data.issafehavenbaby);

    //    this.legalForm.controls.removalHomeForm.get('ChildPhysicalRemovalDate').setValue(data.childphysicalremovaldate);

    //   this.legalForm.controls.removalHomeForm.get('ChildPhysicalAddressAfterRemoval').setValue(data.childphysicaladdressafterremoval);

    //    this.legalForm.controls.removalHomeForm.get('ClientIDOfPersonFromWhomChildWasPhysicallyRemoved').setValue(data.clientidofpersonfromwhomchildwasphysicallyremoved);
    //    this.legalForm.controls.removalHomeForm.get('RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved').setValue(data.relationshipidofpersonfromwhomchildwasphysicallyremoved);
    //    this.legalForm.controls.removalHomeForm.get('DateOf1stParentSignatureOnVPA').setValue(data.dateof1stparentsignatureonvpa);
    //    this.legalForm.controls.removalHomeForm.get('DateOf2ndParentSignatureOnVPA').setValue(data.dateof2ndparentsignatureonvpa);
    //    this.legalForm.controls.removalHomeForm.get('MandatoryNoteOnMissing2ndParentSignatureOnVPA').setValue(data.mandatorynoteonmissing2ndparentsignatureonvpa);
    //    this.legalForm.controls.removalHomeForm.get('DateOfYouthSignatureOnVPA').setValue(data.dateofyouthsignatureonvpa);
    //    this.legalForm.controls.removalHomeForm.get('PreviousFosterCareEpisodeExist').setValue(data.previousfostercareepisodeexist);

    //    this.legalForm.controls.removalHomeForm.get('ReasonForExit').setValue(data.reasonforexit);

    //    this.legalForm.controls.removalHomeForm.get('ExitCareDateFromPreviousFosterCareEpisode').setValue(data.exitcaredatefrompreviousfostercareepisode);
    //     this.legalForm.controls.removalHomeForm.get('DateOfGuardianSignatureOnVPA').setValue(data.dateofguardiansignatureonvpa);
    //    this.legalForm.controls.removalHomeForm.get('ClientIDWhoSignedVPA').setValue(data.clientidwhosignedvpa);
    //    this.legalForm.controls.removalHomeForm.get('DateOfLDSSSignatureOnVPA').setValue(data.dateofldsssignatureonvpa);

    //    this.incomeForm.controls.incomeSummaryForm.get('AmountOfBenefit').setValue(data.amountofbenefit);
    //    this.incomeForm.controls.incomeSummaryForm.get('SupportExpenseAmount').setValue(data.supportexpenseamount);
    //    this.incomeForm.controls.incomeSummaryForm.get('TotalChildCareCost').setValue(data.totalchildcarecost);
    //    this.incomeForm.controls.incomeSummaryForm.get('TypeOfBenefit').setValue(data.typeofbenefit);
    //    this.incomeForm.controls.incomeSummaryForm.get('StandardOfNeedForAU').setValue(data.standardofneedforau);
    //    this.incomeForm.controls.incomeSummaryForm.get('StandardOfNeedForNotInAU').setValue(data.standardofneedfornotinau);

    //    this.incomeForm.controls.assetForm.get('AssetAllowance').setValue(data.assetallowance);
    //    this.incomeForm.controls.assetForm.get('AssetsMarketValue').setValue(data.assetsmarketvalue);

    //    this.incomeForm.controls.deprivationForm.get('DeprivationFactor').setValue(data.deprivationfactor);
    //    this.incomeForm.controls.deprivationForm.get('DateOfParentDeath').setValue(data.dateofparentdeath);
    //    this.incomeForm.controls.deprivationForm.get('IncarcerationDate').setValue(data.incarcerationdate);
    //    this.incomeForm.controls.deprivationForm.get('Unemployment').setValue(data.unemploymentorunderemployment);

    //    this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativeClientID').setValue(data.specifiedrelativeclientid);
    //    this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativeDateChildLastLivedWith').setValue(data.specifiedrelativedatechildlastlivedwith);
    //    this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativeName').setValue(data.Ssecifiedrelativename);
    //    this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativePhysicalAddress').setValue(data.specifiedrelativephysicaladdress);
    //    this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativeRelationshipID').setValue(data.specifiedrelativerelationshipid);
    //    this.criteriaForm.controls.otherCriteriaIdForm.get('FosterCareEligibilityStatus').setValue(data.v_fostercareeligibilitystatus);

    //    this.criteriaForm.controls.otherCriteriaRdForm.get('ChildBeenInFosterCare').setValue(data.childbeeninfostercare12monthormore);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('HaveThereBeenAnyLapsesInPlacementAndCareResponsibilityToIVEAgency')
    //    .setValue(data.havetherebeenanylapsesinplacementandcareresponsibilitytoiveagency);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('TypeOfLapses').setValue(data.typeoflapses);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationStage').setValue(data.fostercareredeterminationstage);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodEndDate').setValue(data.fostercarereviewperiodenddate);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodStartDate').setValue(data.fostercarereviewperiodstartdate);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('EventReason').setValue(data.eventreason);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('EventStage').setValue(data.eventstage);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').setValue(data.eventstartdate);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').setValue(data.EventEndDate);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('EventStatus').setValue(data.eventstatus);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationCompletionDate').setValue(data.fostercareredeterminationcompletiondate);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationEligibilityStatus').setValue(data.fostercareredeterminationeligibilitystatus);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationEligibilityStatusWithEventChange').setValue(data.fostercareredeterminationeligibilitystatuswitheventchange);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('IsThereAnyReasonableEffortsFindingDuringReviewPeriod').setValue(data.isthereanyreasonableeffortsfindingduringreviewperiod);
    //    this.criteriaForm.controls.otherCriteriaRdForm.get('IsReasonableEffortsFindingTimely').setValue(data.isreasonableeffortsfindingtimely);

    //    this.criteriaForm.controls.ageDetailsForm.get('NameOfSecondaryEducationOrEquivalentProgram').setValue(data.nameofsecondaryeducationorequivalentprogram);
    //    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').setValue(data.startdateofsecondaryeducationorequivalentprogram);
    //    this.criteriaForm.controls.ageDetailsForm.get('NameOfpostSecondaryOrVocationalEducation').setValue(data.nameofpostsecondaryorvocationaleducation);
    //    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').setValue(data.startdateofpostsecondaryorvocationaleducation);
    //    this.criteriaForm.controls.ageDetailsForm.get('NameOfPromoteToEmploymentProgram').setValue(data.nameofpromotetoemploymentprogram);
    //    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').setValue(data.startdateofpromotetoemploymentprogram);
    //    this.criteriaForm.controls.ageDetailsForm.get('NameOfEmployer').setValue(data.nameofemployer);
    //    this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').setValue(data.startdateofemployment);
    //    this.criteriaForm.controls.ageDetailsForm.get('HoursPerMonthEmployed').setValue(data.hourspermonthemployed);
    //    this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityType').setValue(data.childdisabilitytype);
    //    this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').setValue(data.childdisabilitystartdate);
    //    this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityEvaluationDocumentionDate').setValue(data.childdisabilityevaluationdocumentiondate);
    //    this.criteriaForm.controls.ageDetailsForm.get('IsThereValidSILAAgreement').setValue(data.istherevalidsilaagreement);
    //    this.criteriaForm.controls.ageDetailsForm.get('DateOfValidSILAAgreement').setValue(data.dateofvalidsilaagreement);

    //    this.criteriaForm.controls.ssiForm.get('ChildReceivingSSIOrSSADuringReviewPeriod').setValue(data.childreceivingssiorssaduringreviewperiod);
    //    this.criteriaForm.controls.ssiForm.get('IsTheAgencyTheRepresentativePayee').setValue(data.istheagencytherepresentativepayee);
    //    this.criteriaForm.controls.ssiForm.get('WHOISTHEREPERSINTIVEPAYEE').setValue(data.whoistherepersintivepayee);
    //    this.criteriaForm.controls.ssiForm.get('HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE').setValue(data.hastheagencyoptedtosuspendthessipaymentandclaimive);
    //    this.criteriaForm.controls.ssiForm.get('ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE').setValue(data.reasonfornotoptedtosuspendthessipaymentandclaimive);
    //    this.criteriaForm.controls.ssiForm.get('ReasonForWhyTheAgencyISNOTTheRepresentativePayee').setValue(data.reasonforwhytheagencyisnottherepresentativepayee);
    //    this.title4eService.getRemovalId(this.legalForm.controls.courtOrderForm.get('RemovalId').value);

    //   // this.legalStatusForm.get('DateOf2ndParentSignatureOnVPA').setValue(data.dateof2ndparentsignatureonvpa);
    //   // this.legalStatusForm.get('ClientIDWhoSignedVPA').setValue(data.v_primarycaregiverid);
    //   // this.legalStatusForm.get('DateOf1stParentSignatureOnVPA').setValue(data.dateof1stparentsignatureonvpa);
    //   // // this.legalStatusForm.get('DateOfGuardianSignatureOnVPA').setValue(data.dateofldsssignatureonvpa);
    //   // this.missingFields.get('DateOfCourtHearing').setValue(data.dateofcourthearing);
    //   // this.legalStatusForm.get('ChildPhysicalRemovalDate').setValue(data.childphysicalremovaldate);
    //   // this.missingFields.get('removalID').setValue(data.removalid);
    //   // this.legalStatusForm.get('PetitionFileDate').setValue(data.v_petitionfileddate);
    //   // this.missingFields.get('placementID').setValue(data.v_placement_id);
    //   // this.missingFields.get('providerID').setValue(data.v_provider_id);
    //   // this.afdcForm.controls.DeprivationForCurrentFosterCare.get('ExitCareDateFromPreviousFosterCareEpisode').setValue(data.v_exit_dt);
    //   // this.legalStatusForm.get('DateOfChildPlacement').setValue(data.dateofchildplacement);
    //   // this.afdcForm.controls.CitizenShip.get('QualifiedAlienStatus').setValue(data.qualifiedalienstaus);
    //   // this.afdcForm.controls.CitizenShip.get('USCitizen').setValue(data.uscitizen);
    //   // this.afdcForm.controls.CitizenShip.get('AlienRegistrationNumber').setValue(data.alienregistrationnumber);
    //   // this.legalStatusForm.get('TypeOfVPA').setValue(data.typeofvpa);
    //   // this.legalStatusForm.get('DateOfChildSignatureOnVPA').setValue(data.dateofchildsignatureonvpa);
    //   // this.legalStatusForm.get('DateOfGuardianSignatureOnVPA').setValue(data.dateofguardiansignatureonvpa);
    //   // this.legalStatusForm.get('MandatoryNoteOnMissing2ndParentSignatureOnVPA').setValue(data.mandatorynoteonmissing2ndparentsignatureonvpa);
    //   // this.legalStatusForm.get('DateOfNextHearing').setValue(data.dateofnexthearing);
    //   // this.missingFields.get('TypeOfCourtHearing').setValue(data.typeofcourthearing);
    //   // this.legalStatusForm.get('IsSignedByJudge').setValue(data.issignedbyjudge);
    //   // this.legalStatusForm.get('CTWDecision').setValue(data.ctwdecision);
    //   // this.legalStatusForm.get('ReasonableEffortsNotNecessaryDueToEmergentCircumstances').setValue(data.reasonableeffortsnotnecessaryduetoemergentcircumstances);
    //   // this.legalStatusForm.get('DateOfFindingCTWDecision').setValue(data.dateoffindingctwdecision);
    //   // this.afdcForm.controls.AgeDetails.get('ChildDisabilityEvaluationDocumentionDate').setValue(data.childdisabilityevaluationdocumentiondate);
    //   // this.afdcForm.controls.AgeDetails.get('ChildDisabilityStartDate').setValue(data.childdisabilitystartdate);
    //   // this.afdcForm.controls.DeprivationForCurrentFosterCare.get('Parent1DeprivationFactor').setValue(data.deprivationfactor);
    //   // this.missingFields.get('DateOfLivingArrangement').setValue(data.dateoflivingarrangement);
    //   // this.legalStatusForm.get('ChildPhysicalAddressAfterRemoval').setValue(data.childphysicaladdressafterremoval);
    //   // this.missingFields.get('assetsmarketvalue').setValue(data.assetsmarketvalue);
    //   // this.afdcForm.controls.AgeDetails.get('NameOfEmployer').setValue(data.nameofemployer);
    //   // this.afdcForm.controls.AgeDetails.get('StartDateOfEmployment').setValue(data.startdateofemployment);
    //   // this.missingFields.get('NameOfPromoteToEmploymentProgram').setValue(data.nameofpromotetoemploymentprogram);
    //   // this.missingFields.get('StartDateOfPromoteToEmploymentProgram').setValue(data.startdateofpromotetoemploymentprogram);
    //   // this.afdcForm.controls.AgeDetails.get('HoursPerMonthEmployed').setValue(data.hourspermonthemployed);
  }
  createForm() {
    this.worksheetForm = this.fb.group({
      status: [],
      casenumber: [],
      fullName: [],
      clientId: [],
      dob: [],
      JURISDICTION: [],
      dateOfRemoval: [],
      ageOfRemoval: [],
      Agency: []
    });
    this.generalForm = this.fb.group({
      placementForm: this.fb.group({
        DateOfChildPlacement: [],
        DateOfLivingArrangement: []
      }),
      demographicsForm: this.fb.group({
        IsDJSOrDSSChild: [],
        DateOfBirth: [],
        USCitizen: [],
        QualifiedAlien: [],
        QualifiedAlienStaus: [],
        AlienRegistrationNumber: []
      })
    });

    this.legalForm = this.fb.group({
      courtOrderForm: this.fb.group({
        TypeOfCourtHearing: [],
        DateOfCourtHearing: [],
        CTWDecision: [],
        DateOfFindingCTWDecision: [],
        NameOfSubjectCTWFinding: [],
        ClientIDOfSubjectCTWFinding: [],
        RelationshipOfSubjectCTWFinding: [],
        CourtOrderDelayRemoval: [],
        CourtOrderDelayTimeFrame: [],
        ReasonableEffortsMade: [],
        ReasonableEffortsNotNecessaryDueToEmergentCircumstances: [],
        REFPPNotDue: [],
        DateOfReasonableEffortsCourtHearing: [],
        DateOfJudicialFindingOfREFPP: [],
        DateOfCurrentJudicialFindingOfREFPP: [],
        DateOfPreviousJudicialFindingOfREFPP: [],
        DateOfSubsequentJudicialFindingOfREFPP: [],
        DateOfBestInterestFinding: [],
        DateOfCurrentJudicialFindingOfBestInterest: [],
        DateOfPreviousBestInterestFinding: [],
        DateOfSubsequentFindingOfBestInterest: [],
        FosterCarePermanencyPlan: [],
        IsIVEAgencyResponsibleForPlacementAndCare: [],
        MagistrateOrJudgeName: [],
        DateOfNextHearing: [],
        SignedByJudge: [],
        DateAgencyLostLegalResponsibility: []
      }),
      removalTypeForm: this.fb.group({
        RemovalId: [],
        TypeOfRemoval: [],
        CourtOrdered: [],
        TypeOfVPA: [],
        IsSafeHavenBaby: []
      }),
      removalHomeForm: this.fb.group({
        ChildPhysicalRemovalDate: [],
        ChildPhysicalAddressAfterRemoval: [],
        ClientIDOfPersonFromWhomChildWasPhysicallyRemoved: [],
        RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved: [],
        DateOf1stParentSignatureOnVPA: [],
        DateOf2ndParentSignatureOnVPA: [],
        MandatoryNoteOnMissing2ndParentSignatureOnVPA: [],
        DateOfYouthSignatureOnVPA: [],
        PreviousFosterCareEpisodeExist: [],
        ReasonForExit: [{ value: '', disabled: false }],
        ExitCareDateFromPreviousFosterCareEpisode: [],
        DateOfGuardianSignatureOnVPA: [],
        ClientIDWhoSignedVPA: [],
        DateOfLDSSSignatureOnVPA: []
      })
    });
    this.incomeForm = this.fb.group({
      incomeSummaryForm: this.fb.group({
        ClientID: [],
        EarnedIncomeAmount: [],
        UnearnedIncomeAmount: [],
        UnearnedIncomeType: [],
        AmountOfBenefit: [],
        SupportExpenseAmount: [],
        TotalChildCareCost: [],
        TypeOfBenefit: [],
        IsIncomeDeemed: [],
        NoOfMembersInAU: [],
        NoOfMembersNotInAU: [],
        StandardOfNeedForAU: [],
        StandardOfNeedForNotInAU: [],
        GrossIncome185PctForAU: [],
        InAU: [],
        incomesummaryInfo: [],
        incomeSUmmaryTable: this.fb.array([])
      }),
      assetForm: this.fb.group({
        AssetAllowance: [],
        AssetsMarketValue: []
      }),
      deprivationForm: this.fb.group({
        ChildDeprivedOfParentalSupport: [],
        ReasonForAbsence: [],
        DeprivationFactor: [],
        DateOfParentDeath: [],
        IncarcerationDate: [],
        Unemployment: []
      })
    });
    this.criteriaForm = this.fb.group({
      otherCriteriaIdForm: this.fb.group({
        SpecifiedRelativeClientID: [],
        SpecifiedRelativeDateChildLastLivedWith: [],
        SpecifiedRelativeName: [],
        SpecifiedRelativePhysicalAddress: [],
        SpecifiedRelativeRelationshipID: [],
        FosterCareEligibilityStatus: [],
        otherCriteriaIdTable: this.fb.array([])
      }),
      otherCriteriaRdForm: this.fb.group({
        IsLivingArrangementSameAsPlacement: [],
        IsPlacementEligible: [],
        ChildBeenInFosterCare: [],
        HaveThereBeenAnyLapsesInPlacementAndCareResponsibilityToIVEAgency: [],
        TypeOfLapses: [],
        FosterCareRedeterminationStage: [],
        FosterCareReviewPeriodEndDate: [],
        FosterCareReviewPeriodStartDate: [],
        EventReason: [],
        EventStage: [],
        EventStartDate: [],
        EventEndDate: [],
        EventStatus: [],
        FosterCareRedeterminationCompletionDate: [],
        FosterCareRedeterminationEligibilityStatus: [],
        FosterCareRedeterminationEligibilityStatusWithEventChange: [],
        IsThereAnyReasonableEffortsFindingDuringReviewPeriod: [],
        IsReasonableEffortsFindingTimely: []
      }),
      ageDetailsForm: this.fb.group({
        NameOfSecondaryEducationOrEquivalentProgram: [],
        StartDateOfSecondaryEducationOrEquivalentProgram: [],
        NameOfpostSecondaryOrVocationalEducation: [],
        StartDateOfpostSecondaryOrVocationalEducation: [],
        NameOfPromoteToEmploymentProgram: [],
        StartDateOfPromoteToEmploymentProgram: [],
        NameOfEmployer: [],
        StartDateOfEmployment: [],
        HoursPerMonthEmployed: [],
        ChildDisabilityType: [],
        ChildDisabilityStartDate: [],
        ChildDisabilityEvaluationDocumentionDate: [],
        IsThereValidSILAAgreement: [],
        DateOfValidSILAAgreement: []
      }),
      ssiForm: this.fb.group({
        ChildReceivingSSIOrSSADuringReviewPeriod: [],
        IsTheAgencyTheRepresentativePayee: [],
        WHOISTHEREPERSINTIVEPAYEE: [],
        HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE: [],
        ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE: [],
        ReasonForWhyTheAgencyISNOTTheRepresentativePayee: []
      })
    });
    this.missingFields = this.fb.group({
      FosterCareReviewPeriodEndDate: [],
      ChildFosterCareEntryDate: [],
      ChildBeenInFosterCare12MonthOrMore: [],
      Step: [],
    });
  }
  // modal implementation start
  getSupervisorDetails() {
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PTA' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe((result) => {
        this.supervisorList = result.data.filter(s => s.issupervisor);
      });
  }



  showSupervisorList() {
    this.title4eService.getUsersList().subscribe(result => {
        this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    });
    (<any>$('#myModal5')).modal('show');
  }


  submitClentCase() {

    if(this.selectedSuperviser){
      const transaction_id = this.determinationTransactionId;
      this._commonHttpService
        .create({
          // where: { transactionId: transaction_id },
          method: 'post'
        },
          'titleive/fc/determination-transaction-id/' + this.determinationTransactionId
        )
        .subscribe((result) => {
          this._alertService.success(result.message);
        });
        const message = 'Placement for ' + this._service.fosterCareData.summaryInfo[0].casenumber + 'sent for Review';
      const data = {
        'where': {
          'assignedtoid': this.selectedSuperviser.userid,
          'eventcode': 'PLTR',
          'servicecaseid': this._service.fosterCareData.summaryInfo[0].servicecaseid,
          'placementid': this.placementId,
          'status': 'SplReview',
          'notifymsg': message,
          'routeddescription': message,
          'comments': message,
        }
      };
  
      this.title4eService.routingUpdate(data).subscribe(response => {
        console.log(response);
        this._alertService.success('Case Assigned Successfully ');
        (<any>$('#caseassign')).modal('hide');
        (<any>$('#assign')).modal('hide');
        // go back to dashboard
      });
    }else{
      this._alertService.error('Select a Supervisor');
    }
    
  }
  selectPerson(person) {
    this.selectedSuperviser = person;
  }

  // assignUser() {
  //     if (this.selectedSuperviser) {
  //         this._commonHttpService
  //             .getPagedArrayList(
  //                 new PaginationRequest({
  //                     where: {
  //                         // appeventcode: 'APPL',
  //                         // serreqid: 'R201800200374',
  //                         assigneduserid: this.selectedSuperviser.userid,
  //                         isgroup: false
  //                     },
  //                     method: 'post'
  //                 }),
  //                 // 'Providerreferral/routereferralda'
  //             )
  //             .subscribe(result => {
  //                 this._alertService.success('Case assigned successfully!');
  //                 $('#worker-caseassign').modal('hide');
  //                 this.closePopup();
  //             });
  //     } else {
  //         this._alertService.warn('Please select a person');
  //     }
  // }

  closePopup() {
    (<any>$('#worker-caseassign')).modal('hide');
  }
  // modal implementation end

  approveSubmit() {
    this.approvesObj = this.approveData;
  }
  onPeriodStartDate(item) {
    this.eventTableData = item.filter(s => s.v_start_dt === item[0].v_start_dt);
  }

  addSpecifiedRelative(): FormGroup {
    return this.fb.group({
      SpecifiedRelativeDateChildLastLivedWith: [],
      SpecifiedRelativePhysicalAddress: [],
      SpecifiedRelativeName: [],
      SpecifiedRelativeClientID: [],
      SpecifiedRelativeRelationshipID: []
    });
  }
  onSearch(field: string, value: string) {
    this.router.navigate(['/pages/title4e/foster-care']);
  }

  // ReviewOnSubmit(): void {
  //   const dialogRef = this.dialog.open(PeriodTablePopUpComponent, {
  //     width: '1100px', height: '700px', data: {
  //       clientId: this.incomeForm.controls.incomeSummaryForm.get('ClientID').value
  //     }
  //   });

  //   dialogRef.componentInstance.selectedPeriodData.subscribe(selectedItems => {
  //     if (selectedItems) {
  //       console.log(selectedItems);
  //       this.selectedPeriodItems = selectedItems;
  //       this.submitDetermination(this.selectedPeriodItems);
  //     }
  //   });

  //   dialogRef.componentInstance.onClose.subscribe(isConfirmed => {
  //     if (isConfirmed) {
  //       if (this.selectedPeriodItems.length > 0) {
  //         this.submitDetermination(this.selectedPeriodItems);
  //       }
  //     }
  //   });
  // }



  submitRedet() {
    const payload = {
      'name': 'Annual_21BDAY_FC_Redet',

      '__metadataRoot': {},
      'Objects': [{
        'reason': {
          'Step': null,
          '__metadata': {
            '#type': 'Reason',
            '#id': 'Reason_id_1'
          },
          'ReasonCode': null
        },
        'DateOfCourtHearing': this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').value ?
          moment(this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').value).format('MM/DD/YYYY') : null,
        'FosterCareReviewPeriodEndDate': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodEndDate').value,
        'suspendssipaymentflag': this.criteriaForm.controls.ssiForm.get('HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE').value,
        'TypeOfCourtHearing': this.legalForm.controls.courtOrderForm.get('TypeOfCourtHearing').value,
        'REFPPNotDue': this.legalForm.controls.courtOrderForm.get('REFPPNotDue').value,
        'FosterCareReviewPeriodStartDate': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodStartDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodStartDate').value).format('MM/DD/YYYY') : null,
        'DateOfSubsequentFindingOfBestInterest': this.legalForm.controls.courtOrderForm.get('DateOfSubsequentFindingOfBestInterest').value ?
          moment(this.legalForm.controls.courtOrderForm.get('DateOfSubsequentFindingOfBestInterest').value).format('MM/DD/YYYY') : null,
        'IsPlacementEligible': this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
        'IsThereValidSILAAgreement': this.criteriaForm.controls.ageDetailsForm.get('IsThereValidSILAAgreement').value,
        'applicantInformation': {
          'ClientID': this.incomeForm.controls.incomeSummaryForm.get('ClientID').value,
          '__metadata': {
            '#type': 'ApplicantInformation',
            '#id': 'ApplicantInformation_id_1'
          }
        },
        'FosterCareRedeterminationStage': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationStage').value,
        'DateOfCurrentJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfREFPP').value ?
          moment(this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfREFPP').value).format('MM/DD/YYYY') : null,
        'FosterCarePermanencyPlan': this.legalForm.controls.courtOrderForm.get('FosterCarePermanencyPlan').value,
        'FosterCareRedeterminationCompletionDate': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationCompletionDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationCompletionDate').value).format('MM/DD/YYYY') : null,
        '__metadata': {
          '#type': 'Application',
          '#id': 'Application_id_1'
        },
        'DateOfCurrentJudicialFindingOfBestInterest': this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfBestInterest').value ?
          moment(this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfBestInterest').value).format('MM/DD/YYYY') : null,
        'IsTheAgencyTheRepresentativePayee': this.criteriaForm.controls.ssiForm.get('IsTheAgencyTheRepresentativePayee').value,
        'DateOfJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfJudicialFindingOfREFPP').value ?
          moment(this.legalForm.controls.courtOrderForm.get('DateOfJudicialFindingOfREFPP').value).format('MM/DD/YYYY') : null,
        'fosterCareEvents': [],
        'ReasonForWhyTheAgencyISNOTTheRepresentativePayee': this.criteriaForm.controls.ssiForm.get('ReasonForWhyTheAgencyISNOTTheRepresentativePayee').value,
        'WhoIsTheRepresentativePayee': this.criteriaForm.controls.ssiForm.get('WHOISTHEREPERSINTIVEPAYEE').value,
        'DateOfValidSILAAgreement': this.criteriaForm.controls.ageDetailsForm.get('DateOfValidSILAAgreement').value,
        'ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE': this.criteriaForm.controls.ssiForm.get('ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE').value,
        'DateOfSubsequentJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfSubsequentJudicialFindingOfREFPP').value,
        'DateOfPreviousJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfPreviousJudicialFindingOfREFPP').value,
        'person': {
          'DateOfBirth': this.generalForm.controls.demographicsForm.get('DateOfBirth').value ?
            moment(this.generalForm.controls.demographicsForm.get('DateOfBirth').value).format('MM/DD/YYYY') : null,
          'ChildDisabilityEvaluationDocumentionDate': this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityEvaluationDocumentionDate').value,
          'DateOfPreviousBestInterestFinding': this.legalForm.controls.courtOrderForm.get('DateOfPreviousBestInterestFinding').value ?
            moment(this.legalForm.controls.courtOrderForm.get('DateOfPreviousBestInterestFinding').value).format('MM/DD/YYYY') : null,
          'StartDateOfSecondaryEducationOrEquivalentProgram': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value ?
            moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value).format('MM/DD/YYYY') : null,
          'ChildFosterCareEntryDate': this.missingFields.get('ChildFosterCareEntryDate').value ? moment(this.missingFields.get('ChildFosterCareEntryDate').value).format('MM/DD/YYYY') : null,
          'TypeOfBenefit': this.incomeForm.controls.incomeSummaryForm.get('TypeOfBenefit').value,
          //'TypeOfRemoval': this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').value,
          'TypeOfRemoval': this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').value ? this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').value : null,
          'NameOfpostSecondaryOrVocationalEducation': this.criteriaForm.controls.ageDetailsForm.get('NameOfpostSecondaryOrVocationalEducation').value,
          'ChildPhysicalRemovalDate': moment(this.legalForm.controls.removalHomeForm.get('ChildPhysicalRemovalDate').value).format('MM/DD/YYYY'),
          'ChildReceivingSSIOrSSADuringReviewPeriod': this.criteriaForm.controls.ssiForm.get('ChildReceivingSSIOrSSADuringReviewPeriod').value,
          'IsSignedByJudge': 'YES',
          'NameOfPromoteToEmploymentProgram': this.criteriaForm.controls.ageDetailsForm.get('NameOfPromoteToEmploymentProgram').value,
          'ChildDisabilityType': this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityType').value,
          'AmountOfBenefit': this.incomeForm.controls.incomeSummaryForm.get('AmountOfBenefit').value,
          'ChildBeenInFosterCare12MonthOrMore': this.missingFields.get('ChildBeenInFosterCare12MonthOrMore').value,
          'DateOfBestInterestFinding': this.legalForm.controls.courtOrderForm.get('DateOfBestInterestFinding').value ?
            moment(this.legalForm.controls.courtOrderForm.get('DateOfBestInterestFinding').value).format('MM/DD/YYYY') : null,
          'StartDateOfPromoteToEmploymentProgram': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').value ?
            moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').value).format('MM/DD/YYYY') : null,
          'HoursPerMonthEmployed': this.criteriaForm.controls.ageDetailsForm.get('HoursPerMonthEmployed').value,
          'StartDateOfEmployment': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').value ?
            moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').value).format('MM/DD/YYYY') : null,
          '__metadata': {
            '#type': 'Person',
            '#id': 'Person_id_1'
          },
          'ChildDisabilityStartDate': this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value ?
            moment(this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value).format('MM/DD/YYYY') : null,
          'StartDateOfpostSecondaryOrVocationalEducation': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value ?
            moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value).format('MM/DD/YYYY') : null,
          'NameOfSecondaryEducationOrEquivalentProgram': this.criteriaForm.controls.ageDetailsForm.get('NameOfSecondaryEducationOrEquivalentProgram').value,
          'NameOfEmployer': this.criteriaForm.controls.ageDetailsForm.get('NameOfEmployer').value
        },
        'IsIVEAgencyResponsibleForPlacementAndCare': this.legalForm.controls.courtOrderForm.get('IsIVEAgencyResponsibleForPlacementAndCare').value,
        'JudgeName': 'Judy',
        'DateAgencyLostLegalResponsibility': this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').value ?
          moment(this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').value).format('MM/DD/YYYY') : null,
        'status': {
          'FosterCareRedeterminationEligibilityStatus': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationEligibilityStatus').value,
          '__metadata': {
            '#type': 'Status',
            '#id': 'Status_id_1'
          }
        }
      }]
    };
    this._commonHttpService.create(payload, Titile4eUrlConfig.EndPoint.postFCRedet).subscribe(
      (res) => {
        console.log(res);
      });
  }

  submitDetermination({clientId, removalId, selectedPeriods}) {
    /*
    console.log(this.incomeForm);
    console.log(this.criteriaForm);
    let selectedIE = null;
    const selectedRE = [];
    let selected18 = null;
    selectedPeriods.forEach((item) => {
      if (item.sqnm_sw === 'I') {
        selectedIE = item;
      }
      if (item.sqnm_sw === '18BDAY') {
        selected18 = item;
      } else if (item.sqnm_sw !== '18BDAY' && item.sqnm_sw !== 'I') {
        selectedRE.push(item);
        this.submitRedet();
      }
    });
    const obj = {
      cjamspid: this.worksheetForm.get('clientId').value,
      removalid: this.legalForm.controls.removalTypeForm.get('RemovalId').value,
      initialDetermination: null,
      reDetermination: null,
      eighteenthBirthday: null
    };
    //console.log('UnearnedIncomeType', this.incomeForm.controls.incomeSummaryForm.get('UnearnedIncomeAmount').value);
    if (selectedIE) {
      obj.initialDetermination = {
        selectedPeriod: selectedIE,
        'payload': {
          'name': 'FosterCare',

          '__metadataRoot': {},
          Objects: [{
            DeprivationFactor: this.incomeForm.controls.deprivationForm.get('DeprivationFactor').value,
            GrossIncome185PctForAU: this.incomeForm.controls.incomeSummaryForm.get('GrossIncome185PctForAU').value,
            ReasonForExit: this.legalForm.controls.removalHomeForm.get('ReasonForExit').value,
            QualifiedAlienStaus: this.generalForm.controls.demographicsForm.get('QualifiedAlienStaus').value,
            PreviousFosterCareEpisodeExist: this.legalForm.controls.removalHomeForm.get('PreviousFosterCareEpisodeExist').value,
            ReasonForAbsence: this.incomeForm.controls.deprivationForm.get('ReasonForAbsence').value,
            AlienRegistrationNumber: this.generalForm.controls.demographicsForm.get('AlienRegistrationNumber').value,
            NoOfMembersInAU: this.incomeForm.controls.incomeSummaryForm.get('NoOfMembersInAU').value,
            QualifiedAlien: this.generalForm.controls.demographicsForm.get('QualifiedAlien').value,
            DateOfParentDeath: this.incomeForm.controls.deprivationForm.get('DateOfParentDeath').value
              ? moment(this.incomeForm.controls.deprivationForm.get('DateOfParentDeath').value).format('MM/DD/YYYY') : null,
            USCitizen: this.generalForm.controls.demographicsForm.get('USCitizen').value,
            'person': {
              DateOfLivingArrangement: this.generalForm.controls.placementForm.get('DateOfLivingArrangement').value ?
                moment(this.generalForm.controls.placementForm.get('DateOfLivingArrangement').value).format('MM/DD/YYYY') : null,
              ChildDisabilityEvaluationDocumentionDate: this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityEvaluationDocumentionDate').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityEvaluationDocumentionDate').value).format('MM/DD/YYYY') : null,
              StartDateOfSecondaryEducationOrEquivalentProgram: this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value).format('MM/DD/YYYY') : null,
              IsDJSOrDSSChild: this.generalForm.controls.demographicsForm.get('IsDJSOrDSSChild').value == "DSS" ? "DSSChild" : null,
              DateOfCourtHearing: this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').value
                ? moment(this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').value).format('MM/DD/YYYY') : null,
              DateOf2ndParentSignatureOnVPA: this.legalForm.controls.removalHomeForm.get('DateOf2ndParentSignatureOnVPA').value ?
                moment(this.legalForm.controls.removalHomeForm.get('DateOf2ndParentSignatureOnVPA').value).format('MM/DD/YYYY') : null,
              //RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved: this.legalForm.controls.removalHomeForm.get('relationshipofpersonfromwhomchildwasphysicallyremoved').value,
              RelationshipIDOfPersonFromWhomChildWasPhysicallyRemoved: this.legalForm.controls.removalHomeForm.get('relationshipofpersonfromwhomchildwasphysicallyremoved') ? this.legalForm.controls.removalHomeForm.get('relationshipofpersonfromwhomchildwasphysicallyremoved').value : null,
              TypeOfRemoval: this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').value,
              ChildPhysicalRemovalDate: this.legalForm.controls.removalHomeForm.get('ChildPhysicalRemovalDate').value ?
                moment(this.legalForm.controls.removalHomeForm.get('ChildPhysicalRemovalDate').value).format('MM/DD/YYYY') : null,
              CTWDecision: this.legalForm.controls.courtOrderForm.get('CTWDecision').value,

              // DateOfChildSignatureOnVPA: null,
              DateOfLDSSSignatureOnVPA: this.legalForm.controls.removalHomeForm.get('DateOfLDSSSignatureOnVPA').value
                ? moment(this.legalForm.controls.removalHomeForm.get('DateOfLDSSSignatureOnVPA').value).format('MM/DD/YYYY') : null,
              IsSafeHavenBaby: this.legalForm.controls.removalTypeForm.get('IsSafeHavenBaby').value,
              // DateOfFindingCTWDecision: moment(this.legalForm.controls.courtOrderForm.get('DateOfFindingCTWDecision').value).format('MM/DD/YYYY'),
              DateOfFindingCTWDecision: this.legalForm.controls.courtOrderForm.get('DateOfFindingCTWDecision').value ?
                moment(this.legalForm.controls.courtOrderForm.get('DateOfFindingCTWDecision').value).format('MM/DD/YYYY') : null,
              NameOfPromoteToEmploymentProgram: this.criteriaForm.controls.ageDetailsForm.get('NameOfPromoteToEmploymentProgram').value,
              DateOfNextHearing: this.legalForm.controls.courtOrderForm.get('DateOfNextHearing').value ?
                moment(this.legalForm.controls.courtOrderForm.get('DateOfNextHearing').value).format('MM/DD/YYYY') : null,
              //ClientIDWhoSignedVPA: this.legalForm.controls.removalHomeForm.get('clientnamewhosignedvpa').value ? this.legalForm.controls.removalHomeForm.get('clientnamewhosignedvpa').value.toString() : null,
              //ClientIDWhoSignedVPA: toString(this.legalForm.controls.removalHomeForm.get('ClientIDWhoSignedVPA').value),
              ClientIDWhoSignedVPA: this.legalForm.controls.removalHomeForm.get('clientnamewhosignedvpa') ? this.legalForm.controls.removalHomeForm.get('clientnamewhosignedvpa').value.toString() : null,
              ChildDisabilityType: this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityType').value,
              ChildPhysicalAddressAfterRemoval: this.legalForm.controls.removalHomeForm.get('ChildPhysicalAddressAfterRemoval').value,
              NameOfSubjectCTWFinding: this.legalForm.controls.courtOrderForm.get('NameOfSubjectCTWFinding').value,
              RemovalId: this.legalForm.controls.removalTypeForm.get('RemovalId').value,
              DateOf1stParentSignatureOnVPA: this.legalForm.controls.removalHomeForm.get('DateOf1stParentSignatureOnVPA').value ?
                moment(this.legalForm.controls.removalHomeForm.get('DateOf1stParentSignatureOnVPA').value).format('MM/DD/YYYY') : null,
              IsPlacementEligible: this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
              ClientIDOfPersonFromWhomChildWasPhysicallyRemoved: this.legalForm.controls.removalHomeForm.get('clientnameofpersonfromwhomchildwasphysicallyremoved') ? this.legalForm.controls.removalHomeForm.get('clientnameofpersonfromwhomchildwasphysicallyremoved').value.toString() : null,
              // ClientIDOfPersonFromWhomChildWasPhysicallyRemoved: this.legalForm.controls.removalHomeForm.get('clientnameofpersonfromwhomchildwasphysicallyremoved').value ? this.legalForm.controls.removalHomeForm.get('clientnameofpersonfromwhomchildwasphysicallyremoved').value.toString() : null,
              // ClientIDOfPersonFromWhomChildWasPhysicallyRemoved: toString(this.legalForm.controls.removalHomeForm.get('ClientIDOfPersonFromWhomChildWasPhysicallyRemoved').value),
              ChildDeprivedOfParentalSupport: this.incomeForm.controls.deprivationForm.get('ChildDeprivedOfParentalSupport').value,
              RelationshipOfSubjectCTWFinding: this.legalForm.controls.courtOrderForm.get('RelationshipOfSubjectCTWFinding').value,
              'specifiedRelative': [{
                // SpecifiedRelativeDateChildLastLivedWith: this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativeDateChildLastLivedWith').value,
                // SpecifiedRelativePhysicalAddress: this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativePhysicalAddress').value,
                // SpecifiedRelativeName:  this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativeName').value,
                // SpecifiedRelativeClientID: this.criteriaForm.controls.otherCriteriaIdForm.get('SpecifiedRelativeClientID').value,

                // SpecifiedRelativeDateChildLastLivedWith: this.otherCriteriaIdTable.at(0).value.SpecifiedRelativeDateChildLastLivedWith ?
                // moment(this.otherCriteriaIdTable.at(0).value.SpecifiedRelativeDateChildLastLivedWith).format('MM/DD/YYYY') : null,

                SpecifiedRelativePhysicalAddress: this.otherCriteriaIdTable.at(0) ? this.otherCriteriaIdTable.at(0).value.SpecifiedRelativePhysicalAddress : null,
                SpecifiedRelativeName: this.otherCriteriaIdTable.at(0) ? this.otherCriteriaIdTable.at(0).value.SpecifiedRelativeName : null,
                SpecifiedRelativeClientID: this.otherCriteriaIdTable.at(0) ? this.otherCriteriaIdTable.at(0).value.SpecifiedRelativeClientID : null,

                SpecifiedRelativeDateChildLastLivedWith: this.otherCriteriaIdTable.at(0) ?
                  moment(this.otherCriteriaIdTable.at(0).value.SpecifiedRelativeDateChildLastLivedWith).format('MM/DD/YYYY') : null,

                '__metadata': {
                  '#type': 'SpecifiedRelative',
                  '#id': 'SpecifiedRelative_id_1'
                },
                SpecifiedRelativeRelationshipID: this.otherCriteriaIdTable.at(0) ? this.otherCriteriaIdTable.at(0).value.SpecifiedRelativeRelationshipID : null,
              }],
              DateOfReasonableEffortsCourtHearing: this.legalForm.controls.courtOrderForm.get('DateOfReasonableEffortsCourtHearing').value ?
                moment(this.legalForm.controls.courtOrderForm.get('DateOfReasonableEffortsCourtHearing').value).format('MM/DD/YYYY') : null,
              StartDateOfEmployment: this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').value).format('MM/DD/YYYY') : null,
              '__metadata': {
                '#type': 'Person',
                '#id': 'Person_id_1'
              },
              ChildDisabilityStartDate: this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value).format('MM/DD/YYYY') : null,
              NameOfSecondaryEducationOrEquivalentProgram: this.criteriaForm.controls.ageDetailsForm.get('NameOfSecondaryEducationOrEquivalentProgram').value,
              DateOfBirth: moment(this.generalForm.controls.demographicsForm.get('DateOfBirth').value).format('MM/DD/YYYY'),
              ClientIDOfSubjectCTWFinding: this.legalForm.controls.courtOrderForm.get('ClientIDOfSubjectCTWFinding').value ? this.legalForm.controls.courtOrderForm.get('ClientIDOfSubjectCTWFinding').value.toString() : null,
              // ClientIDOfSubjectCTWFinding: toString(this.legalForm.controls.courtOrderForm.get('ClientIDOfSubjectCTWFinding').value),
              NameOfpostSecondaryOrVocationalEducation: this.criteriaForm.controls.ageDetailsForm.get('NameOfpostSecondaryOrVocationalEducation').value,
              IsLivingArrangementSameAsPlacement: this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').value,
              TypeOfVPA: this.legalForm.controls.removalTypeForm.get('TypeOfVPA').value,
              IsSignedByJudge: this.legalForm.controls.courtOrderForm.get('SignedByJudge').value,
              CourtOrderDelayRemoval: this.legalForm.controls.courtOrderForm.get('CourtOrderDelayRemoval').value,
              CourtOrderDelayTimeFrame: this.legalForm.controls.courtOrderForm.get('CourtOrderDelayTimeFrame').value,
              ReasonableEffortsNotNecessaryDueToEmergentCircumstances: this.legalForm.controls.courtOrderForm.get('ReasonableEffortsNotNecessaryDueToEmergentCircumstances').value,
              ReasonableEffortsMade: this.legalForm.controls.courtOrderForm.get('ReasonableEffortsMade').value,
              DateOfChildPlacement: this.generalForm.controls.placementForm.get('DateOfChildPlacement').value ?
                moment(this.generalForm.controls.placementForm.get('DateOfChildPlacement').value).format('MM/DD/YYYY') : null,
              IsIVEAgencyResponsibleForPlacementAndCare: this.legalForm.controls.courtOrderForm.get('IsIVEAgencyResponsibleForPlacementAndCare').value,
              MagistrateOrJudgeName: this.legalForm.controls.courtOrderForm.get('MagistrateOrJudgeName').value,
              MandatoryNoteOnMissing2ndParentSignatureOnVPA: this.legalForm.controls.removalHomeForm.get('MandatoryNoteOnMissing2ndParentSignatureOnVPA').value,
              StartDateOfPromoteToEmploymentProgram: this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').value).format('MM/DD/YYYY') : null,
              HoursPerMonthEmployed: this.criteriaForm.controls.ageDetailsForm.get('HoursPerMonthEmployed').value,
              StartDateOfpostSecondaryOrVocationalEducation: this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value).format('MM/DD/YYYY') : null,
              DateOfGuardianSignatureOnVPA: this.legalForm.controls.removalHomeForm.get('DateOfGuardianSignatureOnVPA').value ?
                moment(this.legalForm.controls.removalHomeForm.get('DateOfGuardianSignatureOnVPA').value).format('MM/DD/YYYY') : null,
              NameOfEmployer: this.criteriaForm.controls.ageDetailsForm.get('NameOfEmployer').value
            },
            AssetAllowance: this.incomeForm.controls.assetForm.get('AssetAllowance').value,
            'householdMember': this.getHouseholdData(this.incomeSUmmaryTable.value),
            StandardOfNeedForNotInAU: this.incomeForm.controls.incomeSummaryForm.get('StandardOfNeedForNotInAU').value,
            AssetsMarketValue: this.incomeForm.controls.assetForm.get('AssetsMarketValue').value,
            TotalChildCareCost: this.incomeForm.controls.incomeSummaryForm.get('TotalChildCareCost').value,
            IncarcerationDate: this.incomeForm.controls.deprivationForm.get('IncarcerationDate').value ?
              moment(this.incomeForm.controls.deprivationForm.get('IncarcerationDate').value).format('MM/DD/YYYY') : null,
            NoOfMembersNotInAU: this.incomeForm.controls.incomeSummaryForm.get('NoOfMembersNotInAU').value,
            '__metadata': {
              '#type': 'Application',
              '#id': 'Application_id_1'
            },
            StandardOfNeedForAU: this.incomeForm.controls.incomeSummaryForm.get('StandardOfNeedForAU').value,
            ExitCareDateFromPreviousFosterCareEpisode: this.legalForm.controls.removalHomeForm.get('ExitCareDateFromPreviousFosterCareEpisode').value ?
              moment(this.legalForm.controls.removalHomeForm.get('ExitCareDateFromPreviousFosterCareEpisode').value).format('MM/DD/YYYY') : null,
            'status': {
              FosterCareEligibilityStatus: this.criteriaForm.controls.otherCriteriaIdForm.get('FosterCareEligibilityStatus').value,
              '__metadata': {
                '#type': 'Status',
                '#id': 'Status_id_1'
              }
            }
          }]
        }
      };
    }
    if (selectedRE && selectedRE.length > 0) {
      obj.reDetermination = {
        selectedPeriod: selectedRE,
        payload: {
          'name': 'Annual_21BDAY_FC_Redet',

          '__metadataRoot': {},
          'Objects': [{
            'reason': {
              'Step': null,
              '__metadata': {
                '#type': 'Reason',
                '#id': 'Reason_id_1'
              },
              'ReasonCode': null
            },
            'DateOfCourtHearing': this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').value ?
              moment(this.legalForm.controls.courtOrderForm.get('DateOfCourtHearing').value).format('MM/DD/YYYY') : null,
            'FosterCareReviewPeriodEndDate': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodEndDate').value,
            'HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE': this.criteriaForm.controls.ssiForm.get('HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE').value,
            'TypeOfCourtHearing': this.legalForm.controls.courtOrderForm.get('TypeOfCourtHearing').value,
            'REFPPNotDue': this.legalForm.controls.courtOrderForm.get('REFPPNotDue').value,
            'FosterCareReviewPeriodStartDate': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodStartDate').value ?
              moment(this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareReviewPeriodStartDate').value).format('MM/DD/YYYY') : null,
            'DateOfSubsequentFindingOfBestInterest': this.legalForm.controls.courtOrderForm.get('DateOfSubsequentFindingOfBestInterest').value ?
              moment(this.legalForm.controls.courtOrderForm.get('DateOfSubsequentFindingOfBestInterest').value).format('MM/DD/YYYY') : null,
            'IsPlacementEligible': this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
            'IsThereValidSILAAgreement': this.criteriaForm.controls.ageDetailsForm.get('IsThereValidSILAAgreement').value,
            'applicantInformation': {
              'ClientID': this.incomeForm.controls.incomeSummaryForm.get('ClientID').value,
              '__metadata': {
                '#type': 'ApplicantInformation',
                '#id': 'ApplicantInformation_id_1'
              }
            },
            'FosterCareRedeterminationStage': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationStage').value,
            'DateOfCurrentJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfREFPP').value ?
              moment(this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfREFPP').value).format('MM/DD/YYYY') : null,
            'FosterCarePermanencyPlan': this.legalForm.controls.courtOrderForm.get('FosterCarePermanencyPlan').value,
            'FosterCareRedeterminationCompletionDate': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationCompletionDate').value ?
              moment(this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationCompletionDate').value).format('MM/DD/YYYY') : null,
            '__metadata': {
              '#type': 'Application',
              '#id': 'Application_id_1'
            },
            'DateOfCurrentJudicialFindingOfBestInterest': this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfBestInterest').value ?
              moment(this.legalForm.controls.courtOrderForm.get('DateOfCurrentJudicialFindingOfBestInterest').value).format('MM/DD/YYYY') : null,
            'IsTheAgencyTheRepresentativePayee': this.criteriaForm.controls.ssiForm.get('IsTheAgencyTheRepresentativePayee').value,
            'DateOfJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfJudicialFindingOfREFPP').value ?
              moment(this.legalForm.controls.courtOrderForm.get('DateOfJudicialFindingOfREFPP').value).format('MM/DD/YYYY') : null,
            'fosterCareEvents': [],
            'ReasonForWhyTheAgencyISNOTTheRepresentativePayee': this.criteriaForm.controls.ssiForm.get('ReasonForWhyTheAgencyISNOTTheRepresentativePayee').value,
            'WhoIsTheRepresentativePayee': this.criteriaForm.controls.ssiForm.get('WHOISTHEREPERSINTIVEPAYEE').value,
            'DateOfValidSILAAgreement': this.criteriaForm.controls.ageDetailsForm.get('DateOfValidSILAAgreement').value,
            'ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE': this.criteriaForm.controls.ssiForm.get('ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE').value,
            'DateOfSubsequentJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfSubsequentJudicialFindingOfREFPP').value,
            'DateOfPreviousJudicialFindingOfREFPP': this.legalForm.controls.courtOrderForm.get('DateOfPreviousJudicialFindingOfREFPP').value,
            'person': {
              'DateOfBirth': this.generalForm.controls.demographicsForm.get('DateOfBirth').value ?
                moment(this.generalForm.controls.demographicsForm.get('DateOfBirth').value).format('MM/DD/YYYY') : null,
              'ChildDisabilityEvaluationDocumentionDate': this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityEvaluationDocumentionDate').value,
              'DateOfPreviousBestInterestFinding': this.legalForm.controls.courtOrderForm.get('DateOfPreviousBestInterestFinding').value ?
                moment(this.legalForm.controls.courtOrderForm.get('DateOfPreviousBestInterestFinding').value).format('MM/DD/YYYY') : null,
              'StartDateOfSecondaryEducationOrEquivalentProgram': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value).format('MM/DD/YYYY') : null,
              'ChildFosterCareEntryDate': this.missingFields.get('ChildFosterCareEntryDate').value ? moment(this.missingFields.get('ChildFosterCareEntryDate').value).format('MM/DD/YYYY') : null,
              'TypeOfBenefit': this.incomeForm.controls.incomeSummaryForm.get('TypeOfBenefit').value,
              'TypeOfRemoval': this.legalForm.controls.removalTypeForm.get('TypeOfRemoval').value,
              'NameOfpostSecondaryOrVocationalEducation': this.criteriaForm.controls.ageDetailsForm.get('NameOfpostSecondaryOrVocationalEducation').value,
              'ChildPhysicalRemovalDate': moment(this.legalForm.controls.removalHomeForm.get('ChildPhysicalRemovalDate').value).format('MM/DD/YYYY'),
              'ChildReceivingSSIOrSSADuringReviewPeriod': this.criteriaForm.controls.ssiForm.get('ChildReceivingSSIOrSSADuringReviewPeriod').value,
              'IsSignedByJudge': 'YES',
              'NameOfPromoteToEmploymentProgram': this.criteriaForm.controls.ageDetailsForm.get('NameOfPromoteToEmploymentProgram').value,
              'ChildDisabilityType': this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityType').value,
              'AmountOfBenefit': this.incomeForm.controls.incomeSummaryForm.get('AmountOfBenefit').value,
              'ChildBeenInFosterCare12MonthOrMore': this.missingFields.get('ChildBeenInFosterCare12MonthOrMore').value,
              'DateOfBestInterestFinding': this.legalForm.controls.courtOrderForm.get('DateOfBestInterestFinding').value ?
                moment(this.legalForm.controls.courtOrderForm.get('DateOfBestInterestFinding').value).format('MM/DD/YYYY') : null,
              'StartDateOfPromoteToEmploymentProgram': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfPromoteToEmploymentProgram').value).format('MM/DD/YYYY') : null,
              'HoursPerMonthEmployed': this.criteriaForm.controls.ageDetailsForm.get('HoursPerMonthEmployed').value,
              'StartDateOfEmployment': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfEmployment').value).format('MM/DD/YYYY') : null,
              '__metadata': {
                '#type': 'Person',
                '#id': 'Person_id_1'
              },
              'ChildDisabilityStartDate': this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value).format('MM/DD/YYYY') : null,
              'StartDateOfpostSecondaryOrVocationalEducation': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value ?
                moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value).format('MM/DD/YYYY') : null,
              'NameOfSecondaryEducationOrEquivalentProgram': this.criteriaForm.controls.ageDetailsForm.get('NameOfSecondaryEducationOrEquivalentProgram').value,
              'NameOfEmployer': this.criteriaForm.controls.ageDetailsForm.get('NameOfEmployer').value
            },
            'IsIVEAgencyResponsibleForPlacementAndCare': this.legalForm.controls.courtOrderForm.get('IsIVEAgencyResponsibleForPlacementAndCare').value,
            'JudgeName': this.legalForm.controls.courtOrderForm.get('MagistrateOrJudgeName').value,
            'DateAgencyLostLegalResponsibility': this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').value ?
              moment(this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').value).format('MM/DD/YYYY') : null,
            'status': {
              'FosterCareRedeterminationEligibilityStatus': this.criteriaForm.controls.otherCriteriaRdForm.get('FosterCareRedeterminationEligibilityStatus').value,
              '__metadata': {
                '#type': 'Status',
                '#id': 'Status_id_1'
              }
            }
          }]
        }
      };

      if (selected18) {
        obj.eighteenthBirthday = {
          selectedPeriod: selected18,
          'payload': {
            'name': '18BDAY_FC_Redet',
            '__metadataRoot': {},
            Objects: [
              {
                'ReasonForWhyTheAgencyISNOTTheRepresentativePayee': null,
                'DateOfValidSILAAgreement': '03/01/2008',
                'HasTheAgencyOptedToSuspendTheSSIPaymentAndClaimIVE': 'YES',
                'TypeOfCourtHearing': null,
                'ReasonForNOTOptedToSuspendTheSSIPAymentAndClaimIVE': null,
                'IsPlacementEligible': this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
                'IsThereValidSILAAgreement': 'YES',
                'applicantInformation': {
                  'ClientID': this.incomeForm.controls.incomeSummaryForm.get('ClientID').value,
                  '__metadata': {
                    '#type': 'ApplicantInformation',
                    '#id': 'ApplicantInformation_id_1'
                  }
                },
                person: {
                  'DateOfBirth': '01/01/1991',
                  'ChildDisabilityEvaluationDocumentionDate': null,
                  'StartDateOfSecondaryEducationOrEquivalentProgram': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value ?
                    moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfSecondaryEducationOrEquivalentProgram').value).format('MM/DD/YYYY') : null,
                  'TypeOfBenefit': 'SSA',
                  'TypeOfRemoval': 'Court_Order',
                  'NameOfpostSecondaryOrVocationalEducation': null,
                  'ChildReceivingSSIOrSSADuringReviewPeriod': 'YES',
                  'NameOfPromoteToEmploymentProgram': null,
                  'ChildDisabilityType': null,
                  'AmountOfBenefit': 1000,
                  'StartDateOfPromoteToEmploymentProgram': null,
                  'HoursPerMonthEmployed': null,
                  'StartDateOfEmployment': null,
                  '__metadata': {
                    '#type': 'Person',
                    '#id': 'Person_id_1'
                  },
                  'ChildDisabilityStartDate': this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value ?
                    moment(this.criteriaForm.controls.ageDetailsForm.get('ChildDisabilityStartDate').value).format('MM/DD/YYYY') : null,
                  'StartDateOfpostSecondaryOrVocationalEducation': this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value ?
                    moment(this.criteriaForm.controls.ageDetailsForm.get('StartDateOfpostSecondaryOrVocationalEducation').value).format('MM/DD/YYYY') : null,
                  'NameOfSecondaryEducationOrEquivalentProgram': 'Johns',
                  'NameOfEmployer': null
                },
                'FosterCareRedeterminationStage': '18BDAY',
                'IsIVEAgencyResponsibleForPlacementAndCare': 'YES',
                'FosterCareRedeterminationCompletionDate': null,
                '__metadata': {
                  '#type': 'Application',
                  '#id': 'Application_id_1'
                },
                'IsTheAgencyTheRepresentativePayee': 'YES',
                'DateAgencyLostLegalResponsibility': this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').value ?
                  moment(this.legalForm.controls.courtOrderForm.get('DateAgencyLostLegalResponsibility').value).format('MM/DD/YYYY') : null,
                'status': {
                  'FosterCareRedeterminationEligibilityStatus': null,
                  '__metadata': {
                    '#type': 'Status',
                    '#id': 'Status_id_1'
                  }
                }
              }
            ]
          }
        };
      }
    }
*/
    const obj = {
      clientId,
      removalId,
      selectedPeriods
    }

    this._commonHttpService.create(obj, Titile4eUrlConfig.EndPoint.auditPeriods).subscribe(
      (response) => {
        try {
          // this.mainTanble = response.map(v => v.data.map(d => d));
          // this.determinationTransactionId = response[0].data[0].v_transactionid;
          let status = true;
          response.selectedPeriods.forEach(determinationResult => {
            if(determinationResult.result.status !== "success"){
              status = false;
              // console.log(determinationResult.result.status);
              this._alertService.warn(determinationResult.result.msg);
            }
          });

          if(status){
            this._alertService.success('Eligibility Worksheet summary sent successfully!');
          }

          $('#eligibilityDetailsBtnTrigger').click();
        } catch (error) {
          this._alertService.error('Unable to send Eligibility Worksheet summary, please try again.');
          return false;
        }
      },
      (error) => {
        this._alertService.error('Unable to send Eligibility Worksheet summary, please try again.');
        return false;
      }
    );
    // console.log(JSON.stringify('Object!!', obj));
  }

  submitForReview(worker, i) {
    this.workerList.splice(i, 1);
  }

  getHouseholdData(data) {
    const temp = data.map(v => {
      return {
        'unearnedIncome': [{
          '__metadata': {
            '#type': 'UnearnedIncome',
            '#id': 'UnearnedIncome_id_1'
          },
          'UnearnedIncomeAmount': v.UnearnedIncomeAmount ? v.UnearnedIncomeAmount.UnearnedIncomeAmount : null,
          'UnearnedIncomeType': v.UnearnedIncomeAmount.UnearnedIncomeType ? v.UnearnedIncomeAmount.UnearnedIncomeType : null,
          // 'UnearnedIncomeAmount':  v.UnearnedIncomeAmount.unearnedIncomeAmount
        }],
        'IsIncomeDeemed': v.IsIncomeDeemed,
        'supportExpense': [{
          'SupportExpenseAmount': v.SupportExpenseAmount,
          '__metadata': {
            '#type': 'SupportExpense',
            '#id': 'SupportExpense_id_1'
          }
        }],
        'ClientID': v.ClientID,
        'InAU': v.InAU,
        '__metadata': {
          '#type': 'HouseholdMember',
          '#id': 'HouseholdMember_id_1'
        },
        'earnedIncome': [{
          'EarnedIncomeAmount': v.EarnedIncomeAmount,
          '__metadata': {
            '#type': 'EarnedIncome',
            '#id': 'EarnedIncome_id_1'
          }
        }]
      };
    });
    return temp;
  }

  getFCEvents() {
    const fcEvents = [
      {
        'EventReason': 'PC',
        'IsLivingArrangementSameAsPlacement': this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').value,
        'HaveThereBeenAnyLapsesInPlacementAndCareResponsibilityToIVEAgency': null,
        'EventStatus': null,
        'Step': null,
        'TypeOfLapses': this.criteriaForm.controls.otherCriteriaRdForm.get('TypeOfLapses').value,
        'EventStage': 'E1',
        IsPlacementEligible: this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
        'EventEndDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value).format('MM/DD/YYYY') : null,
        'IsReasonableEffortsFindingTimely': null,
        'EventStartDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value).format('MM/DD/YYYY') : null,
        'IsThereAnyReasonableEffortsFindingDuringReviewPeriod': null,
        '__metadata': {
          '#type': 'FosterCareEvents',
          '#id': 'FosterCareEvents_id_1'
        },
        'FosterCareRedeterminationEligibilityStatusWithEventChange': null
      },
      {
        'EventReason': 'PC',
        'IsLivingArrangementSameAsPlacement': this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').value,
        'HaveThereBeenAnyLapsesInPlacementAndCareResponsibilityToIVEAgency': null,
        'EventStatus': null,
        'Step': null,
        'TypeOfLapses': this.criteriaForm.controls.otherCriteriaRdForm.get('TypeOfLapses').value,
        'EventStage': 'E2',
        IsPlacementEligible: this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
        'EventEndDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value).format('MM/DD/YYYY') : null,
        'IsReasonableEffortsFindingTimely': null,
        'EventStartDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value).format('MM/DD/YYYY') : null,
        'IsThereAnyReasonableEffortsFindingDuringReviewPeriod': null,
        '__metadata': {
          '#type': 'FosterCareEvents',
          '#id': 'FosterCareEvents_id_2'
        },
        'FosterCareRedeterminationEligibilityStatusWithEventChange': null
      },
      {
        'EventReason': 'PC',
        'IsLivingArrangementSameAsPlacement': this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').value,
        'HaveThereBeenAnyLapsesInPlacementAndCareResponsibilityToIVEAgency': null,
        'EventStatus': null,
        'Step': null,
        'TypeOfLapses': this.criteriaForm.controls.otherCriteriaRdForm.get('TypeOfLapses').value,
        'EventStage': 'E3',
        IsPlacementEligible: this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
        'EventEndDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value).format('MM/DD/YYYY') : null,
        'IsReasonableEffortsFindingTimely': null,
        'EventStartDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value).format('MM/DD/YYYY') : null,
        'IsThereAnyReasonableEffortsFindingDuringReviewPeriod': null,
        '__metadata': {
          '#type': 'FosterCareEvents',
          '#id': 'FosterCareEvents_id_3'
        },
        'FosterCareRedeterminationEligibilityStatusWithEventChange': null
      },
      {
        'EventReason': 'PC',
        'IsLivingArrangementSameAsPlacement': this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').value,
        'HaveThereBeenAnyLapsesInPlacementAndCareResponsibilityToIVEAgency': null,
        'EventStatus': null,
        'Step': null,
        'TypeOfLapses': this.criteriaForm.controls.otherCriteriaRdForm.get('TypeOfLapses').value,
        'EventStage': 'E4',
        IsPlacementEligible: this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
        'EventEndDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value).format('MM/DD/YYYY') : null,
        'IsReasonableEffortsFindingTimely': null,
        'EventStartDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value).format('MM/DD/YYYY') : null,
        'IsThereAnyReasonableEffortsFindingDuringReviewPeriod': null,
        '__metadata': {
          '#type': 'FosterCareEvents',
          '#id': 'FosterCareEvents_id_4'
        },
        'FosterCareRedeterminationEligibilityStatusWithEventChange': null
      },
      {
        'EventReason': 'PC',
        'IsLivingArrangementSameAsPlacement': this.criteriaForm.controls.otherCriteriaRdForm.get('IsLivingArrangementSameAsPlacement').value,
        'HaveThereBeenAnyLapsesInPlacementAndCareResponsibilityToIVEAgency': null,
        'EventStatus': null,
        'Step': null,
        'TypeOfLapses': this.criteriaForm.controls.otherCriteriaRdForm.get('TypeOfLapses').value,
        'EventStage': 'E5',
        'IsPlacementEligible': this.criteriaForm.controls.otherCriteriaRdForm.get('IsPlacementEligible').value,
        'EventEndDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventEndDate').value).format('MM/DD/YYYY') : null,
        'IsReasonableEffortsFindingTimely': null,
        'EventStartDate': this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value ?
          moment(this.criteriaForm.controls.otherCriteriaRdForm.get('EventStartDate').value).format('MM/DD/YYYY') : null,
        'IsThereAnyReasonableEffortsFindingDuringReviewPeriod': null,
        '__metadata': {
          '#type': 'FosterCareEvents',
          '#id': 'FosterCareEvents_id_5'
        },
        'FosterCareRedeterminationEligibilityStatusWithEventChange': null
      }
    ];

    if (this.selectedPeriodItems.filter(v => v.sqnm_sw === 'R1E1' || v.sqnm_sw === 'R2E1').length > 0) {
      return fcEvents;
    }
    return [];
  }


  // getfcEvents() {
  //     let fcEvents = this.formArray.filter(v => v.sqnm_sw ==='R1E1')
  //     .map(v => {
  //         let temp = {
  //             'EventReason': v.EventReason,
  //             'IsLivingArrangementSameAsPlacement': v.IsLivingArrangementSameAsPlacement,
  //         }
  //         return temp;
  //     })
  //     return fcEvents;
  // }

  loadUsersList() {
    this.title4eService.getUsersList().subscribe(result => {
      this.supervisorList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SUPERVISOR);
    });
  }
}