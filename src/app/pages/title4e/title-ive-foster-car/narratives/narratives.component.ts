import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import { MatDialog, MatHorizontalStepper, MatTableDataSource, MatPaginatorModule} from "@angular/material";
import * as moment from "moment";
import * as _ from "lodash";
import { forkJoin } from "rxjs/observable/forkJoin";
import { RecordingNotes, requestObject } from "./iveNarrativeModel";
import { FileUtils } from "../../../../@core/common/file-utils";
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService} from "../../../../@core/services";
import { PeriodTablePopUpComponent } from "../../period-table-pop-up/period-table-pop-up.component";
import { Titile4eUrlConfig } from "../../_entities/title4e-dashboard-url-config";
import { RelationshipService } from "../../../case-worker/dsds-action/relationship/relationship.service";
import { PaginationRequest } from "../../../../@core/entities/common.entities";
import { Title4eService } from "../../services/title4e.service";
import { CaseWorkerUrlConfig } from "../../../case-worker/case-worker-url.config";
import { CASE_STORE_CONSTANTS } from "../../../case-worker/_entities/caseworker.data.constants";
import { environment } from "../../../../../environments/environment";
import { NavigationUtils } from "../../../_utils/navigation-utils.service";
import { AppConstants } from "../../../../@core/common/constants";
// import { RelationshipService } from '../../../person-details/person-relationship/relationship.service';
// import { RelationshipService } from './relationship.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: "narratives",
    templateUrl: "./narratives.component.html",
    styleUrls: ["./narratives.component.scss"]
})
export class NarrativesComponent implements OnInit {
    progressNote: RecordingNotes = new RecordingNotes();
    requestObject: requestObject = new requestObject();
    clientId : string;
    constructor(
        private route: ActivatedRoute,
        public dialog: MatDialog,
        private fb: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService,
        private storage: SessionStorageService,
        private title4eService: Title4eService,
        private _formBuilder: FormBuilder,
        private _navigationUtils: NavigationUtils
    ) {}

    ngOnInit() {
      this.clientId = this.route.snapshot.params['clientId'];
      this.getNarrative(this.clientId);
    }

    getNarrative(clientId) {
      this._commonHttpService
      .create(
        {entitytypeid: clientId},
          Titile4eUrlConfig.EndPoint.getNarrative
      )
      .subscribe(
          (response: any) => {
              if (response && response.length > 0 && response[0] !== '') {
                this.progressNote = response[0];
                this._dataStoreService.setData('addNarrative', true);
              }
              else{
                this._dataStoreService.setData('addNarrative', false);
              }
          },
          error => {
              console.log(error);
              return false;
          }
      );
    }
    addUpdateNarrative() {
      // hard coded progressnotetypeid to 'note' type and entitytype to 'IVE'
        this.progressNote.progressnotetypeid = 'a1f78e9f-ea8d-4f0b-8df5-7d4c0eda21cc';
        this.progressNote.entitytypeid = this.clientId;
        this.progressNote.entitytype = 'IVE';
        this._commonHttpService
            .create(
                this.progressNote,
                Titile4eUrlConfig.EndPoint.addUpdateNarrative
            )
            .subscribe(
                (response: any) => {
                    if (response) {
                      this._alertService.success('Narrative saved successfully.');
                      this.getNarrative(this.clientId);
                    }
                },
                error => {
                    console.log(error);
                    this._alertService.error('Unable to save narrative.');
                    this.getNarrative(this.clientId);
                    return false;
                }
            );
    }
}
