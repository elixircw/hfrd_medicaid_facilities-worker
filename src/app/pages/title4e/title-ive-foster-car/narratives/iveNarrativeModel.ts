export class RecordingNotes {
    progressnoteid: string;
    progressnotetypeid: string;
    description: string;
    entitytypeid: string;
    entitytype: string;
}

export class requestObject {
    entitytypeid: string;
}