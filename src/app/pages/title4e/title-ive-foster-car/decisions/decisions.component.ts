import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'decisions',
  templateUrl: './decisions.component.html',
  styleUrls: ['./decisions.component.scss']
})
export class DecisionsComponent implements OnInit {
  @Input() worksheetForm: FormGroup;
  @Input() eligibilityPerioodData: any;

  constructor() { }

  ngOnInit() {
  }

}
