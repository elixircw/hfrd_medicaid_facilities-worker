import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatHorizontalStepper, MatTableDataSource, MatPaginatorModule  } from '@angular/material';
import * as moment from 'moment';
import * as _ from 'lodash';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FileUtils } from '../../../../@core/common/file-utils';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { PeriodTablePopUpComponent } from '../../period-table-pop-up/period-table-pop-up.component';
import { Titile4eUrlConfig } from '../../_entities/title4e-dashboard-url-config';
import { RelationshipService } from '../../../case-worker/dsds-action/relationship/relationship.service';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { Title4eService } from '../../services/title4e.service';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../../case-worker/_entities/caseworker.data.constants';
import { environment } from '../../../../../environments/environment';
import { NavigationUtils } from '../../../_utils/navigation-utils.service'; 
import { AppConstants } from '../../../../@core/common/constants';
// import { RelationshipService } from '../../../person-details/person-relationship/relationship.service';
// import { RelationshipService } from './relationship.service';

declare var Formio: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'eligible-worksheet-form',
  templateUrl: './eligible-worksheet-form.component.html',
  styleUrls: ['./eligible-worksheet-form.component.scss']
})
export class EligibleWorksheetFormComponent implements OnInit {
 
  @ViewChild('stepper') stepper: MatHorizontalStepper;
  isStart = false;
  dataSource;
  displayedColumns: string[] = 
    ['placementlivingtype', 'type', 'samelivingarrplacement', 'placementreimbursible', 'lapses', 
    'lapsetype', 'startdt', 'provider', 'provideraddress', 'enddt'];

  placementList = [];
  livingArrangement = [];
  pageSize=1;
  tempList=[];

  // public ngxCurrencyOptions = {
  //   prefix: '$ ',
  //   thousands: ',',
  //   decimal: ',',
  //   precision: 0,
  //   allowNegative: false,
  //   nullable: true
  // };
  // isPreviousEpisodeExist: boolean;
  // incomeSummaryTableForm: FormGroup;
  client_id = '1008560';
  selectedPeriodItems: any;
  // isReadOnly = true;
  periodTable: any[];
  clientId: number;
  removalId: number;
  placementId: number;
  periodType: string;
  personsList: any = {};
  selectedDetCount: number = 0;
  subsequent: boolean = true;
  fromIVtab: boolean = true;
  selectedComponent: String;
  componentsList: string[] = ['legal', 'afdcRelatedness', 'placement', 'otherCriteria'];
  selectPeriodItem: any;
  msg = [];
  errorMessage =  [];
  childCost: string;
  specifiedRelativeRelationShips = {
    1001: 'Brother/Sister',
    1002: 'Nephew/Nieces',
    1003: 'Grand Nephew/Nieces',
    1004: 'Great-Grand Nephew/Nieces',
    1005: 'Grand Parents',
    1006: 'Uncles Aunts',
    1007: 'First Cousins',
    1008: 'First Cousins once removed',
    1009: 'Great-Grand Parents',
    1010: 'Great-Uncles Aunts',
    1011: 'Great-Great Grand Parents',
    1012: 'Great-Grand Uncles Aunts',
    1013: 'God Child',
    1014: 'Other',
    1015: 'Non-Relative',
  };

  @Input() summaryInfo: any;
  @Input() worksheetData: any;
  @Input() worksheetForm: FormGroup;
  @Input() legalStatusForm: FormGroup;
  @Input() afdcForm: FormGroup;
  @Output() submitForReview: EventEmitter<any> = new EventEmitter();
  @Output() incomeSummaryData: EventEmitter<any> = new EventEmitter();
  @Input() generalForm: FormGroup;
  @Input() legalForm: FormGroup;
  @Input() incomeForm: FormGroup;
  @Input() criteriaForm: FormGroup;
  @Input() tempRows1: any;

  firstGenList = [];
  personsDropdown = [];
  personSubjectCtw = [];
  deprivationData = [];
  householdData = [];
  controlarry = [];
  controlDevarry = [];
  controlRel = [];
  selectedHouseHold: any;
  scheduleHLookUp: any;
  scheduleHLookUpForNotInAU: any;

  casnumber: any;
  legalGroup: FormGroup;
  afdc: FormGroup;
  placement: FormGroup;
  other: FormGroup;
  isMessageShow = false;
  // ssiForm: FormGroup;
  // seletedForm: String;
  // seelctedtempRow: any;
  // tempRows = [];
  // // tempRows1 = [];
  // tempRows2 = [];
  // incomeSummaryDetails: any;
  // tableData = Array();
  // ReasonForExistArray = Array();
  // reasonForexistSelected: any;
  // personList: any;
  // isTrue = 'YES';
  // isRepresntativeTrue = 'NO';
  // isPaymentTrue = 'YES';
  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private storage: SessionStorageService,
    private title4eService: Title4eService,
    private _formBuilder: FormBuilder,
    private _navigationUtils: NavigationUtils
  ) { }

  ngOnInit() {
    this.dataSource = [];
    // this.dataSource = new MatTableDataSource(this.dataSource);
    // Get client id from route params
    this.clientId = this.route.snapshot.params['clientId'];
    // Get removal id from route params
    this.removalId = this.route.snapshot.params['removalId'];
    // Get Placement id from route params
     this.placementId = this.route.snapshot.params['placementId'];
    // Get persons list
    this.getPersonsList();
    // Get eligibility data
    this.getEligibilityData();
    this.buildFormGroup();
    
    this.placement.disable();
  }

  buildFormGroup() {
    this.legalGroup = this._formBuilder.group({
      issafehavenbaby : [{ value: '', disabled: true }],
      typeofremoval : [{ value: '', disabled: true }],
      typeofvpa : [{ value: '', disabled: true }],
      childphysicalremovaldate : [{ value: '', disabled: true }],
      clientnameofpersonfromwhomchildwasphysicallyremoved : [{ value: '', disabled: true }],
      clientidofpersonfromwhomchildwasphysicallyremoved : [{ value: '', disabled: true }],
      childphysicaladdressafterremoval : [{ value: '', disabled: true }],
      relationshipofpersonfromwhomchildwasphysicallyremoved : [{ value: '', disabled: true }],
      relationshipidofpersonfromwhomchildwasphysicallyremoved : [{ value: '', disabled: true }],
      intakeservreqcourtorderid : [{ value: '', disabled: true }],
       courtinfotable: [{ value: '', disabled: true }],
      dateofcourthearing : [{ value: '', disabled: true }],
      magistrateorjudgename : [{ value: '', disabled: true }],
      issignedbyjudge : [{ value: '', disabled: true }],
      ctwdecision : [{ value: '', disabled: true }],
      dateoffindingctwdecision : [{ value: '', disabled: true }],
      dateofnexthearing : [{ value: '', disabled: true }],
      isiveagencyresponsibleforplacementandcare : [{ value: '', disabled: true }],
      nameofsubjectctwfinding : [{ value: '', disabled: true }],
      clientidofsubjectctwfinding : [{ value: '', disabled: true }],
      relationshipofsubjectctwfinding : [{ value: '', disabled: true }],
      NameOfSubjectOfCTWFindingGroup : [''],
      courtorderdelayremoval : [{ value: '', disabled: true }],
      courtorderdelaytimedays : [{ value: '', disabled: true }],
      dateagencylostlegalresponsibility : [{ value: '', disabled: true }],
      typeofcourthearing : [{ value: '', disabled: true }],
      reasonableeffortsmade : [{ value: '', disabled: true }],
      dateofreasonableeffortscourthearing : [{ value: '', disabled: true }],
      reasonableeffortsnotnecessaryduetoemergentcircumstances : [{ value: '', disabled: true }],
      fostercarepermanencyplan : [{ value: '', disabled: true }],
      dateofjudicialfindingofrefpp : [{ value: '', disabled: true }],
      dateofcurrentjudicialfindingofrefpp : [{ value: '', disabled: true }],
      dateofsubsequentjudicialfindingofrefpp : [{ value: '', disabled: true }],
      dateofpreviousjudicialfindingofrefpp : [{ value: '', disabled: true }],
      dateofsubsequentfindingofbestinterest : [{ value: '', disabled: true }],
      dateofcurrentjudicialfindingofbestinterest : [{ value: '', disabled: true }],
      dateofpreviousbestinterestfinding : [{ value: '', disabled: true }],
      clientnamewhosignedvpa : [{ value: '', disabled: true }],
      dateof1stparentsignatureonvpa : [{ value: '', disabled: true }],
      dateof2ndparentsignatureonvpa : [{ value: '', disabled: true }],
      dateofguardiansignatureonvpa : [{ value: '', disabled: true }],
      dateofldsssignatureonvpa : [{ value: '', disabled: true }],
      mandatorynoteonmissing2ndparentsignatureonvpa : [{ value: '', disabled: true }],
      dateofyouthsignatureonvpa : [{ value: '', disabled: true }],
      previousfostercareepisodeexist : [{ value: '', disabled: true }],
      exitcaredatefrompreviousfostercareepisode : [{ value: '', disabled: true }],
      reasonforexit : [{ value: '', disabled: true }]

    });
    this.afdc = this._formBuilder.group({
       dateofbirth : [{ value: '', disabled: true }],
       ageofthechild : [{ value: '', disabled: true }],
       uscitizen : [{ value: '', disabled: true }],
       qualifiedalien : [{ value: '', disabled: true }],
       qualifiedalienstaus : [{ value: '', disabled: true }],
       alienregistrationnumber : [{ value: '', disabled: true }],
       household: this._formBuilder.array([]),
       delist: this._formBuilder.array([]),
       specifiedRelativesList: this._formBuilder.array([]),
       costChild : ['']
    });
    this.placement = this._formBuilder.group({
      childbeeninfostercare12monthormore: [''],
      isreasonableeffortsfindingtimely: [''],
      // IsThereAnyReasonableEffortsFindingDuringReviewPeriod: [''],
      dateofchildplacement: [''],
      dateoflivingarrangement: ['']
    });
    this.other = this._formBuilder.group({
      nameofsecondaryeducationorequivalentprogram: [{ value: '', disabled: true }],
      startdateofsecondaryeducationorequivalentprogram: [{ value: '', disabled: true }],
      nameofpostsecondaryorvocationaleducation: [{ value: '', disabled: true }],
      startdateofpostsecondaryorvocationaleducation: [{ value: '', disabled: true }],
      nameofpromotetoemploymentprogram: [{ value: '', disabled: true }],
      startdateofpromotetoemploymentprogram: [{ value: '', disabled: true }],
      nameofemployer: [{ value: '', disabled: true }],
      startdateofemployment: [{ value: '', disabled: true }],
      hourspermonthemployed: [{ value: '', disabled: true }],
      childdisabilitytype: [{ value: '', disabled: true }],
      childdisabilitystartdate: [{ value: '', disabled: true }],
      childdisabilityevaluationdocumentiondate: [{ value: '', disabled: true }],
      childreceivingssiorssa: [null],
      typeofbenefit: [''],
      amountofbenefit: [null],
      ischildageabove18: [null],
      agencyrepresentativeflag: [null],
      whoisrepresentativepayee: [''],
      reasonforagencynotrepresentativepayee: [''],
      dateofmedicaldetermination: [''],
      hasagencyapplytobecomerepresentativepayee: [null],
      dateofapplicationtobecomerepresentativepayee: [''],
      suspendssipaymentflag: [null],
      reasonfornotsuspendingssipayment: [''],
      doesagencyhasmedicaldocstostateincapabilityofchild : [null],
      dateofrequesttosuspendthessipaymentandclaimive: ['']
        });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngAfterViewInit() {
    if (this.stepper) {
     this.stepper._getIndicatorType = () => 'number';
    }
  }

  getPersonVal(clientId) {
    if (_.has(this, `personsList.${clientId}`)) {
      return {
        value: this.personsList[clientId].value,
        label: this.personsList[clientId].label,
      }
    } else {
      return { value: '', label: '' };
    }
  }

  getPersonLabel(clientId: number){
    return _.get(this.personsList, `${clientId}.label`);
  }

  getPersonNameD(clientId: number){
    return _.get(this.personsList, `${clientId}.name`);
  }
  get4eRelationId(clientId: number): number {
    return _.get(this.personsList, `${clientId}.fourerelid`);
  }

  getRelationVal(id) {
    if (_.has(this, `specifiedRelativeRelationShips.${id}`)) {
      return {
        value: id,
        label: this.specifiedRelativeRelationShips[id],
      }
    } else {
      return { value: '', label: '' };
    }
  }

  startForm(periodData: any) {
        this.isStart = true;
        if (this.stepper) {
          this.stepper._getIndicatorType = () => 'number';
        }
        const worksheetData = this.worksheetData;
        if ( worksheetData && worksheetData.data ) {
          this.legalGroup.patchValue(worksheetData.data);
          this.legalGroup.patchValue(worksheetData.courtInfo);
          this.afdc.patchValue(worksheetData.data);
          this.afdc.patchValue({costChild : worksheetData.data.childcarecost});
          this.afdc.patchValue({ageofthechild : moment().diff(worksheetData.data.dateofbirth, 'years')});
          this.populatePlacementInfo(worksheetData);
          this.populateOtherCriteria(worksheetData);
          this.legalGroup.patchValue({NameOfSubjectOfCTWFindingGroup : worksheetData.data.clientidofsubjectctwfinding});
        }
        this.personsDropdown = [];
        this.personSubjectCtw = [];
        _.forIn(this.personsList, (person) => {
          this.personsDropdown.push({
            value: person.value,
            label: person.label
          });
        });
       _.forIn(this.personsList, (person) => {
              if (!person.relationship.some(relation => relation === 'Child' )) {
                  this.personSubjectCtw.push({
                    value: person.value,
                    label: person.label
                  });
              }
          });
        const deprivationInfo = _.get(worksheetData, 'deprivationInfo');
        const controlArry = <FormArray>this.afdc.controls.delist;
        while (this.controlDevarry.length > 0) {
          this.controlDevarry.pop();
          controlArry.removeAt(this.controlDevarry.length - 1);
        }
        // controlDevarry
        if (deprivationInfo && _.isArray(deprivationInfo)) {
                deprivationInfo.forEach(element => {
                  controlArry.push(this._formBuilder.group({
                    deprivationParentId: [this.getPersonVal(element.parentid).value],
                    childdeprivedofparentalsupport: [element.childdeprivedofparentalsupport],
                    reasonforabsence: [element.reasonforabsence],
                    deprivationfactor: [ element.deprivationtype],
                    dateofparentdeath: [element.dateofparentdeath],
                    incarcerationdate: [element.dateofincarceration],
                    unemploymentorunderemployment: [element.isunemployment],
                    ivepersondeprivationid: [element.ivepersondeprivationid],
                    activeFlag: [1]
                  })
                );
                this.controlDevarry.push('1');
                });
              }
        const householdInfo = _.get(worksheetData, 'householdInfo');
        const placementInfo = _.get(worksheetData, 'placementInfo');
        // House hold info
        const control = <FormArray>this.afdc.controls.household;
        while (this.controlarry.length > 0) {
          this.controlarry.pop();
          control.removeAt(this.controlarry.length - 1);
        }
        if (householdInfo && _.isArray(householdInfo)  && _.isArray(householdInfo[0].getfinanceincomebycase)) {
          householdInfo[0].getfinanceincomebycase.forEach(hh => {
            control.push(this._formBuilder.group({
                name: [this.getPersonName(hh)],
                id: [hh.personid],
                ivepersonincomeid : [hh.iveincomesumary  ? hh.iveincomesumary.ivepersonincomeid : ''],
                involvedclientid : [hh.cjamspid],
                assistanceunit: [hh.iveincomesumary  ? hh.iveincomesumary.assistanceunit : ''],
                deemedincome : [hh.iveincomesumary  ? hh.iveincomesumary.deemedincome === 'YES'  : false],
                disregardearnedincome : [hh.iveincomesumary  ? hh.iveincomesumary.disregardearnedincome === 'YES' : false],
                income : [hh.getfinanceincome ? hh.getfinanceincome : []],
                caseid : hh.servicecaseid,
                casenumber : hh.servicecasenumber,
                assets : [hh.getfinanceassets ? hh.getfinanceassets : []],
              })
            );
            this.controlarry.push('1');
           });
           this.showScheduleH();
          }
          const specifiedRelativeInfo = _.get(worksheetData, 'specifiedRelativeInfo');
          // Specified Relatives
           const controlArryRel = <FormArray>this.afdc.controls.specifiedRelativesList;
           while (this.controlRel.length > 0) {
             this.controlRel.pop();
             controlArryRel.removeAt(this.controlRel.length - 1);
           }
          const specifiedRelativesData = [];
          if (specifiedRelativeInfo && _.isArray(specifiedRelativeInfo)) {
            specifiedRelativeInfo.forEach(element => {
              controlArryRel.push(this._formBuilder.group({
                sepecifiedrelativeuniqid: element.specifiedrelativeid,
                specifiedrelativename: this.getPersonVal(element.specifiedrelativeclientid).value,
                specifiedrelativerelationshipid: this.get4eRelationId(element.specifiedrelativeclientid), //;this.getRelationVal(element.specifiedrelativerelationshipid),
                specifiedrelativephysicaladdress: element.specifiedrelativephysicaladdress,
                specifiedrelativedatechildlastlivedwith: element.specifiedrelativedatechildlastlivedwith,
                activeFlag: [1]
              })
            );
            this.controlRel.push('1');
            });
          }
          console.log(this.afdc);
  }

  addDeprivation() {
    const controlArry = <FormArray>this.afdc.controls.delist;
    controlArry.push(this._formBuilder.group({
      deprivationParentId: [''],
      childdeprivedofparentalsupport: [''],
      reasonforabsence: [''],
      deprivationfactor: [''],
      dateofparentdeath: [''],
      incarcerationdate: [''],
      unemploymentorunderemployment: [''],
      ivepersondeprivationid: [''],
      activeFlag: [1]
    })
    );
    this.controlDevarry.push('1');
  }

  deletedeprivation(index, item) {
    const data = {
      ivepersondeprivationid: item,
    }
    this._commonHttpService.create(data, Titile4eUrlConfig.EndPoint.deprivationDelete)
    .subscribe(
      response => {
          if (response) {
            this.msg.push( 'Deprivation deleted successfully');
          }
      },
      error => {
        this.errorMessage.push('Deprivation not delted successfully');
      }
    );
    const controlArry = <FormArray>this.afdc.controls.delist;
    controlArry.removeAt(index);
  }

  addRelatives() {
    const controlArryRel = <FormArray>this.afdc.controls.specifiedRelativesList;
    controlArryRel.push(this._formBuilder.group({
      sepecifiedrelativeuniqid: '',
      specifiedrelativename:  '',
      specifiedrelativerelationshipid:  '',
      specifiedrelativephysicaladdress: '',
      specifiedrelativedatechildlastlivedwith:  '',
      activeFlag: [1]
    })
    );
    this.controlRel.push('1');
  }
  deleteRelatives(index) {
    const controlArryRel = <FormArray>this.afdc.controls.specifiedRelativesList;
    controlArryRel.removeAt(index);
  }

  showScheduleH() {
    const control = <FormArray>this.afdc.controls.household;
    console.log(control['value']);
    const arr = [];
    _.forIn(control['value'], (val) => {
       if (val.assistanceunit === 'YES'){
         arr.push(1);
       }
    });
    const notInAU = [];
    _.forIn(control['value'], (val) => {
       if (val.assistanceunit === 'NO'){
        notInAU.push(1);
       }
    });
    this.getScheduleH(arr.length, true);
    this.getScheduleH(notInAU.length, false);
   }

   getScheduleH(length,isAU){
    this._commonHttpService
    .getPagedArrayList(
      new PaginationRequest({
        page: 1,
        nolimit: true,
        method: 'get',
        where: { householdsize: length }
      }),
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.getScheduleH + '?filter'
    )
    .subscribe((result) => {
       if (result && _.isArray(result)) {
        if (isAU) {
          this.scheduleHLookUp = result[0] ;
        } else {
          this.scheduleHLookUpForNotInAU = result[0] ;
        }
       }
    });
   }

  getPersonName(person) {
    const roles = person.roles ? person.roles.map(x => x.typedescription).join(',') : '' ;
    return person.firstname + ' ' + person.lastname + '(' + roles + ')';
  }

  showAssetOrIncome(household,isIncome) {
    this.selectedHouseHold = null;
    const house = household;
    this.selectedHouseHold = house['value'];
    console.log(this.selectedHouseHold);
    if(isIncome){
      (<any>$('#iframe-income')).modal('show');
    } else {
      (<any>$('#iframe-asset')).modal('show');
    }
  }

  showPersonFin(household) {
    const house = household;
    this.selectedHouseHold = house['value'];
    if (this.selectedHouseHold && this.selectedHouseHold.id && this.selectedHouseHold.caseid && this.selectedHouseHold.casenumber) {
      this._navigationUtils.openEditFinance(this.selectedHouseHold.id,this.clientId,this.placementId,this.removalId,this.fromIVtab, AppConstants.CASE_TYPE.SERVICE_CASE, this.selectedHouseHold.caseid, this.getData(this.selectedHouseHold.casenumber));
    }
  }


  getData(casenumber) {
    const data = {
      purposeId: null,
      caseNumber: casenumber
    };
    return data;
  }

  saveData() {
    this.isMessageShow  = true;
    this.msg = [];
    this.errorMessage = [];
    this.updateVPA(this.legalGroup.value).subscribe(
      response => {
          if (response) {
            this.msg.push('VPA saved successfully') ;
          }
      },
      error => {
          this.errorMessage.push('VPA not saved successfully');
      }
    );
    this.updateCourtOrder(this.legalGroup.value).subscribe(
      response => {
          if (response) {
            this.msg.push( 'Court Order saved successfully') ;
          }
      },
      error => {
        this.errorMessage.push( 'Court Order not saved successfully');
      }
    );
    this.updateDeprivation(this.afdc.value.delist).subscribe(
      response => {
          if (response) {
            this.msg.push( 'Deprivation saved successfully');
          }
      },
      error => {
        this.errorMessage.push('Deprivation not saved successfully');
      }
    );
    this.updateSpecifiedRelatives(this.afdc.value.specifiedRelativesList).subscribe(
      response => {
          if (response) {
            this.msg.push(
                  'Specified Relatives saved successfully' );
            }
     },
      error => {
        this.errorMessage.push('Specified Relatives not saved successfully');
      }
    );
    this.updateIncomeAndAssetsSummary().subscribe(
      response => {
          if (response) {
            this.msg.push(
                  'Income And Assets Summar saved successfully')
              ;
          }
      },
      error => {
        this.errorMessage.push('Income And Assets Summar not saved successfully');
      }
    );
    this.updateHouseHoldIncome(this.afdc.value.household).subscribe(
      response => {
          if (response) {
            this.msg.push(
                  'House Hold Income saved successfully');
          }
      },
      error => {
        this.errorMessage.push('House Hold Income not saved successfully');
      }
    );
    this.updateSSISSAData(this.other.value).subscribe(
      response => {
          if (response) {
            this.msg.push(
                  'SSI saved successfully');
          }
      },
      error => {
        this.errorMessage.push('SSI not saved successfully');
      }
    );
    window.scrollTo(0, 100);
    setTimeout(() => {
      this.isMessageShow = false;
      this.getEligibilityWorksheetData(this.selectPeriodItem);
    }, 5000);
  }

  getEligibilityWorksheetData(item: any) {
    const data = {
      clientId: this.clientId,
      removalId: this.removalId,
      detPeriodType: item.sqnm_sw,
    }
    this._commonHttpService.create(data, Titile4eUrlConfig.EndPoint.getEligibilityWorksheet).subscribe(
      (response: any) => {
        if (response) {
          this.worksheetData = response;
          this.startForm(item);
        }
      },
      (error) => {
        console.log(error);
        return false;
      }
    );
  }

  toggleChildren(key_id: number): void {
    const filterChildrenData: any = {
      key_id,
      isParent: false
    }

    const filterParentData: any = {
      key_id,
      isParent: true
    }

    _.filter(this.periodTable, filterChildrenData).forEach((period : any)=>{
      period.isVisible = !period.isVisible;
    });

    _.filter(this.periodTable, filterParentData).forEach((period : any)=>{
      period.isCollapsed = !period.isCollapsed;
    });
  }

  getEligibilityData(): void {
    this._commonHttpService.getAll(Titile4eUrlConfig.EndPoint.getPeriods + '/' + this.clientId + '/' + this.removalId).subscribe((response: any) => {
      response.data.forEach(v => {
        v.isSelected = false;
      });
      const data = response.data;
      let previousKey = 0;
      for (let index = data.length-1; index >= 0; index--) {
          data[index].isParent = true;
          data[index].isVisible = true;
          data[index].isCollapsed = true;
          if(previousKey == data[index].key_id){
            data[index].isVisible = false;
              data[index].isParent = false;
              data[index].isCollapsed = false;
          }
          previousKey = data[index].key_id;
      }
      this.periodTable = data;
    },
      (error) => {
        // console.log(error);
      }
    );
  }

  getPersonsList() {
    const casenumber = this.summaryInfo.servicecaseid;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          nolimit: true,
          method: 'get',
          where: { servicecaseid: casenumber }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      )
      .subscribe((result) => {

        this.firstGenList = result.data;
        this.firstGenList.forEach(element => {
          const roles = _.flatMap(element.roles, 'typedescription');
          const relationships = _.join(roles, ', ');
          this.personsList[element.cjamspid] = {
            name: `${element.firstname.trim()} ${element.lastname.trim()}`,
            relationship: roles,
            fourerelid: element.fourerelid,
            value: element.cjamspid,
            label: `${element.firstname.trim()} ${element.lastname.trim()}(${element.cjamspid}) - ${relationships}`,
            personid: element.personid
          };
        });

      });
    //  })
    // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
    // );
  }

  onSelectItem(event: any, item: any): void {
    item.isSelected = event.target.checked;
    this.selectedDetCount = _.filter(this.periodTable, {isSelected: true as any}).length;
  }

  submitEligibility() {
    this.selectedPeriodItems = {
      clientId: this.clientId,
      removalId: this.removalId,
      selectedPeriods: _.filter(this.periodTable, { 'isSelected': true as any }),
    }
    console.log('selectedPeriods >>> ', this.selectedPeriodItems);
    this.submitForReview.emit(this.selectedPeriodItems);
    // $('#eligibilityDetailsBtnTrigger').click();

  }

  private getAge(dateValue) {
    if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
        const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
        return moment().diff(rCDob, 'years');
    } else {
        return '';
    }
}

  onPeriodSelectItem(item: any): void {
    const regex = /^[R]\d+$/gm;
    this.selectPeriodItem = item;
    if (item.sqnm_sw === 'I') {
      // initial determination
      this.periodType = 'ID';
    } else if (regex.exec(item.sqnm_sw) !== null) {
      this.periodType = 'RD';
    } else if (item.sqnm_sw === '18BDAY') {
      this.periodType = 'RD';
    } else {
      this.periodType = null;
    }

    if(item.sqnm_sw === 'I' || item.sqnm_sw === 'R1' ){
      this.subsequent = false;
    } else {
      this.subsequent = true;
    }

    this.getEligibilityWorksheetData(item);
  }

  updateCourtOrder(submission: any) {
    // relationshipofsubjectctwfinding was always hardcoded by previous developper in formbuilder 
    const payload = {
      clientId: this.clientId,
      removalid: this.removalId,
      clientidofsubjectctwfinding: _.get(submission, 'NameOfSubjectOfCTWFindingGroup') ?  _.get(submission, 'NameOfSubjectOfCTWFindingGroup') : null,
      nameofsubjectctwfinding:  _.get(submission, 'NameOfSubjectOfCTWFindingGroup') ? this.getPersonNameD(_.get(submission, 'NameOfSubjectOfCTWFindingGroup')) : null,
      relationshipofsubjectctwfinding: 1001,
    };
    return this._commonHttpService.update('', payload, Titile4eUrlConfig.EndPoint.courtOrderUpdate);
  }

  updateVPA(submission: any) {
    const payload = {
      clientId: this.clientId,
      removalReasonTypeKey: _.get(submission, 'reasonforexit') ? _.get(submission, 'reasonforexit') : null,
      returnDate: _.get(submission, 'exitcaredatefrompreviousfostercareepisode') ? moment(_.get(submission, 'exitcaredatefrompreviousfostercareepisode')).format('MM-DD-YYYY') : null,
    };
    return this._commonHttpService.update('', payload, Titile4eUrlConfig.EndPoint.updateRemoval);
  }


  updateDeprivation(submission: any) {
    const payload = submission.map((data: any) => {
      // console.log('updateDeprivation >>>> ', data);
      if (data.deprivationParentId) {
      const deprivationdata = {
        clientId: this.clientId,
        removalid: this.removalId,
        parentid: data.deprivationParentId,
        relationship: _.get(this.personsList, `${data.deprivationParentId}.relationship`),
        childdeprivedofparentalsupport: data.childdeprivedofparentalsupport ? data.childdeprivedofparentalsupport : null,
        isunemployment: data.unemploymentorunderemployment ? data.unemploymentorunderemployment : null,
        deprivationtype: data.deprivationfactor ? data.deprivationfactor : null,
        reasonforabsence: data.reasonforabsence ? data.reasonforabsence : null,
        dateofparentdeath: data.dateofparentdeath ? data.dateofparentdeath : null,
        dateofincarceration: data.incarcerationdate ? data.incarcerationdate : null,
      };
      if (_.has(data, 'ivepersondeprivationid') && !_.isEmpty(data.ivepersondeprivationid)) {
        deprivationdata['ivepersondeprivationid'] = data.ivepersondeprivationid;
      } 
      return deprivationdata;
      }
    });

    return this._commonHttpService.create(payload, Titile4eUrlConfig.EndPoint.deprivationUpdate);
  }

  updateHouseHoldIncome(submission: any) {

    const incomeType = submission.map((household: any) => {
      // console.log('household >>> ', household);
      if (household.involvedclientid) {
      const householddata = {
        clientId: this.clientId,
        removalid: this.removalId,
        involvedClientId: household.involvedclientid,
        involvedClientName: this.personsList[household.involvedclientid] ? this.personsList[household.involvedclientid].name : '',
        assistanceUnit: household.assistanceunit ? household.assistanceunit : 'NO',
        deemedIncome: household.deemedincome ? 'YES' : 'NO',
        earnedIncomeNo: household.earnedincomeno,
        disregardEarnedIncome: household.disregardearnedincome ? 'YES' : 'NO',
        unearnedIncome: household.income,
      };
      if (_.has(household, 'ivepersonincomeid') && !_.isEmpty(household.ivepersonincomeid)) {
        householddata['ivepersonincomeid'] = household.ivepersonincomeid;
      }
      return householddata;
     }
    });
    return this._commonHttpService.create({ incomeType }, Titile4eUrlConfig.EndPoint.saveIncomeSummary);
  }

  updateIncomeAndAssetsSummary() {
    let payload = {};
    if (this.scheduleHLookUp || this.scheduleHLookUpForNotInAU) {
     payload = {
      clientId: this.clientId,
      removalid: this.removalId,
      // ivepersondeprivationid: FileUtils.newGuid(),
      assistanceunitno:  this.scheduleHLookUp ? (this.scheduleHLookUp.familysize ? this.scheduleHLookUp.familysize : 0 ) : 0,
      notinassistanceunitno: this.scheduleHLookUpForNotInAU ? (this.scheduleHLookUpForNotInAU.familysize ? this.scheduleHLookUpForNotInAU.familysize : 0) : 0 ,
      standardunitno:  this.scheduleHLookUp ? (this.scheduleHLookUp.standardofneed ? this.scheduleHLookUp.standardofneed : 0) : 0 ,
      notinstandardunitno: this.scheduleHLookUpForNotInAU ? (this.scheduleHLookUpForNotInAU.standardofneed ? this.scheduleHLookUpForNotInAU.standardofneed : 0) : 0 ,
      grossincome185pcunitno: this.scheduleHLookUp ? (this.scheduleHLookUp.grossincomeoneeightyfive ? this.scheduleHLookUp.grossincomeoneeightyfive : 0) : 0,
      assetsallowance: 0, // Needs to be calcualted
      assetsmarketvalue: 0,
      childcarecost:  this.afdc.value.costChild ? this.afdc.value.costChild : 0
    };
   }
    return this._commonHttpService.create(payload, Titile4eUrlConfig.EndPoint.incomeUpdate);
  }

  updateSpecifiedRelatives(submission: any) {

    const payload = submission.map((data: any) => {
      return {
        clientId: this.clientId,
        removalid: this.removalId,
        specifiedrelativeid: data.sepecifiedrelativeuniqid || FileUtils.newGuid(),
        specifiedrelativeclientid: data.specifiedrelativename,
        specifiedrelativedatechildlastlivedwith: data.specifiedrelativedatechildlastlivedwith ? data.specifiedrelativedatechildlastlivedwith : null ,
        specifiedrelativename: _.get(this.personsList, `${data.specifiedrelativename}.name`),
        specifiedrelativephysicaladdress: data.specifiedrelativephysicaladdress,
        specifiedrelativerelationshipid: this.get4eRelationId(data.specifiedrelativename),
      }
    });

    return this._commonHttpService.create(payload, Titile4eUrlConfig.EndPoint.saveSpecificRelative);
  }

  updateSSISSAData(data: any) {
    const payload = {
      clientId: this.clientId,
      removalid: this.removalId,
      ischildageabove18: data.ischildageabove18,
      childReceivingSsiOrSsa: data.childreceivingssiorssa,
      agencyRepresentativeFlag: data.agencyrepresentativeflag,
      noteForAgencyNotAsPayee: data.reasonforagencynotrepresentativepayee,
      representativePayee: data.whoisrepresentativepayee,
      suspendSsiPaymentFlag: data.suspendssipaymentflag,
      noteForNotSuspendingSsi: data.reasonfornotsuspendingssipayment,
      dateofmedicaldetermination: data.dateofmedicaldetermination ? data.dateofmedicaldetermination : null ,
      doesagencyhasmedicaldocstostateincapabilityofchild: data.doesagencyhasmedicaldocstostateincapabilityofchild,
      hasagencyapplytobecomerepresentativepayee: data.hasagencyapplytobecomerepresentativepayee,
      dateofapplicationtobecomerepresentativepayee: data.dateofapplicationtobecomerepresentativepayee ? data.dateofapplicationtobecomerepresentativepayee : null,
      dateofrequesttosuspendthessipaymentandclaimive: data.dateofrequesttosuspendthessipaymentandclaimive ? data.dateofrequesttosuspendthessipaymentandclaimive : null,
      typeofbenefit: data.typeofbenefit,
      amountofbenefit: data.amountofbenefit,
    }

    return this._commonHttpService.create(payload, Titile4eUrlConfig.EndPoint.ssiSsaUpdate);
  }

  populatePlacementInfo(worksheetData) {
    this.placementList = [];
    this.livingArrangement = [];
      // placementData
    const placementData = [];
    if (worksheetData.data && worksheetData.placementInfo && _.isArray(worksheetData.placementInfo)) {
      const placementTypes = {
        PLTR: 'Placement',
        LA: 'Living'
      };
      this.placement.patchValue(worksheetData.data);
      if ( worksheetData.placementInfo &&  worksheetData.placementInfo.length) {
        if (worksheetData.placementInfo[0]) {
          if(worksheetData.placementInfo[0].placement && worksheetData.placementInfo[0].placement.length) {
            worksheetData.placementInfo[0].placement.forEach(element => {
              if(element.placementid) {
                this.placementList.push({
                  livingarrangementtype: _.get(placementTypes, element.livingarrangementtype),
                  placement_type: element.placement_type ? element.placement_type : null,
                  isplacementreimbursible: element.isplacementreimbursible ? 'YES' : 'NO',
                  islivingarrangementsameasplacement: element.livingarrangementtype === 'PLTR' ? 'YES' : (element.livingarrangementtype === 'LA' ? 'NO' : null),
                  start_dt: element.start_dt ? element.start_dt : null,
                  end_dt: element.end_dt ? element.end_dt : null,
                  provider_nm: element.provider_nm ? element.provider_nm : null,
                  provider_address: element.provider_address ? element.provider_address : null
                });
              }
            });
          }

          if( worksheetData.placementInfo[0].livingarrangement && worksheetData.placementInfo[0].livingarrangement.length) {
            worksheetData.placementInfo[0].livingarrangement.forEach(element => {
              if(element.livingarrangementtype) {
                this.livingArrangement.push({
                  livingarrangementtype: _.get(placementTypes, element.livingarrangementtype),
                  livingorplacement_type: element.livingorplacement_type ? element.livingorplacement_type : null,
                  isplacementreimbursible: element.isplacementreimbursible ? 'YES' : 'NO',
                  islivingarrangementsameasplacement: element.livingarrangementtype === 'PLTR' ? 'YES' : (element.livingarrangementtype === 'LA' ? 'NO' : null),
                  start_dt: element.start_dt ? element.start_dt : null,
                  end_dt: element.end_dt ? element.end_dt : null,
                  provider_nm: element.provider_nm ? element.provider_nm : null,
                  provider_address: element.provider_address ? element.provider_address : null
                });
              }
            })
          } 
        }
      }
    }
  }

  populateOtherCriteria(worksheetData) {
    if (worksheetData.data) {
      this.other.patchValue(worksheetData.data);
    }
  }

  onPageChange(e) {
    this.tempList = this.placementList.slice(e.pageIndex * e.pageSize,(e.pageIndex + 1) *e.pageSize);
  } 
}
 