import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibleWorksheetFormOthercriteriaComponent } from './eligible-worksheet-form-othercriteria.component';

describe('EligibleWorksheetFormOthercriteriaComponent', () => {
  let component: EligibleWorksheetFormOthercriteriaComponent;
  let fixture: ComponentFixture<EligibleWorksheetFormOthercriteriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EligibleWorksheetFormOthercriteriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EligibleWorksheetFormOthercriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
