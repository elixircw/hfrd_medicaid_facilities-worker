export class Titile4eUrlConfig {
    public static EndPoint = {
        slaCompliance: {
            slaComplianceURL: `/manage/team/`
        },
        teamStatistics: {
            teamStatisticsURL: `Usernotifications/getTeamCaseCountByUser`
        },
        myDsdsActions: {
            myDsdsActionsURL: `servicerequestsearches/getDetails`,
            DSDSActionDetailsUrl: 'servicerequestsearches/usersservicerequest'
        },
        myTasks: {
            myTasksURL: `Usernotifications/getActivityTasksByUser`
        },
        restoreWidget: {
            widgetURL: `Usernotifications/getCaseCountByUser`,
            taskCloseureURL: 'Usernotifications/getTasksCountByUser',
            caseworkerDashboardURL: 'Usernotifications/getcaseworkerdashboard'
        },
        Attachment: { 
            UploadAttachmentUrl: 'attachments/uploadsFile',
            AttachmentUploadUrl: 'attachments/uploadsFile',
            AttachmentTypeUrl: 'Intake/attachmenttype',
            AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype',
            DeleteAttachmentUrl: 'Documentproperties/delete',
            AttachmentGridUrl: 'Documentproperties/getcaseworkerattachments',
            PersonAttachmentGridUrl: 'Documentproperties/getpersonattachments',
            PersonAttachmentUpdate: 'Documentproperties/updatepersonattachments',
            SaveAttachmentUrl: 'Documentproperties/addcaseworkerattachment',
        },
        adoptionDetermination: '/titleive/adoption-id',
        adoptionRedetermination: '/titleive/adoption-rd',
        adoptionApplicability: '/titleive/adoption-applicability',
        getEligibilityWorksheet: 'titleive/fc/eligibility-worksheet/',
        updateEligibilityWorksheet: 'titleive/fc/eligibility-worksheet',
        getGapEligibilityWorksheet: 'titleive/gap/gap-eligibility-worksheet/',

        postGapEligibilityNExtension: 'titleive/gap/audit',

        postAdoptionApplicability: 'titleive/adoption/audit',
        postAdoptionApplicabilityToDB: 'titleive/adoption/adoption-applicability',
        
        getPeriods: 'titleive/fc/get-periods',
        auditPeriods: 'titleive/fc/audit-periods',
        auditMessages: 'titleive/fc/audit-messages',
        eligibilityHistory: 'titleive/fc/eligibility-history',
        postFCRedet: 'titleive/foster-care-epc',
        caseSubmit: 'titleive/fc/determination-transaction-id',
        updateRemoval: 'titleive/fc/worksheet/updateRemoval',
        assetUpdate: 'titleive/fc/worksheet/asset',
        deprivationUpdate: 'titleive/fc/worksheet/deprivation',
        deprivationDelete: 'titleive/fc/worksheet/deprivation/delete',
        incomeUpdate: 'titleive/fc/worksheet/income',
        courtOrderUpdate: 'titleive/fc/worksheet/updateCourt',
        removalTypeUpdate: 'titleive/fc/worksheet/removaltype',
        ssiSsaUpdate: 'titleive/fc/worksheet/ssissa',
        saveSpecificRelative:'titleive/fc/worksheet/specified-relative',
        saveIncomeSummary:'titleive/fc/worksheet/income-type/create',
        updateIncomeSummary:'titleive/fc/worksheet/income-type/update',
        PersonList: 'People/getpersondetail',
        addUpdateNarrative: 'admin/progressnote/addrecordingsive',
        getNarrative: 'admin/progressnote/getrecordingsive'
    };
}
