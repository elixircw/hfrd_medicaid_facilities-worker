import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { AlertService } from '../../../@core/services/alert.service';
import { Subject } from 'rxjs/Subject';
import { Titile4eUrlConfig } from '../_entities/title4e-dashboard-url-config';
import { Title4eService } from '../services/title4e.service';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'period-table-pop-up',
  templateUrl: './period-table-pop-up.component.html',
  styleUrls: ['./period-table-pop-up.component.scss']
})
export class PeriodTablePopUpComponent implements OnInit {
  selectedPeriodData = new Subject<any>();
  onClose = new Subject<boolean>();
  ClientId: any;
  periodTable: any[];
  selectedPeriodsRE: any[];
  selectedPeriodsIN: {};
  constructor(public dialogRef: MatDialogRef<PeriodTablePopUpComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private commonHttpService: CommonHttpService,
    private alertService: AlertService,
    private title4eService: Title4eService
  ) { }

  ngOnInit() {
    this.ClientId = this.router.routerState.snapshot.url.split('/')[4];

    this.getData();
  }
  onSubmit() {
    this.selectedPeriodData.next(this.periodTable.filter(v => v.isSelected));
    this.onClose.next(true);
    this.dialogRef.close();
  }
  onTimes() {
    this.onClose.next(false);
    this.dialogRef.close();
  }
  onSelectItem(event, item) {
    item.isSelected = event.target.checked;
  }
  getData() {
    const clientId = this.title4eService.clientIdData;
     const removalId = this.title4eService.removalIdData;
    // const removalId = 403;
    this.commonHttpService.getAll(Titile4eUrlConfig.EndPoint.getPeriods + '/' + this.ClientId + '/' + removalId).subscribe(
      (response: any) => {
        console.log('response');
        response.data.forEach(v => {
          v.isSelected = false;
        });
        this.periodTable = response.data;
      },
      (error) => {
        console.log(error);
        return false;
      }
    );
  }
}

