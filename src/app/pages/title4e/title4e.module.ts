import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntakeFilterPipe } from './pipes/intakeFilter';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxPrintModule } from 'ngx-print';
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule, 
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckbox,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    MatDialogModule,
    MatStepperModule,
    MatPaginatorModule,
} from '@angular/material';
import { MatTooltipModule, MatChipsModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { SortTableModule } from '../../shared/modules/sortable-table/sortable-table.module';
import { SharedDirectivesModule } from '../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { Supervisor4eComponent } from './supervisor4e/supervisor4e.component';
import { Worker4eComponent } from './worker4e/worker4e.component';
import { Title4eRoutingModule } from './title4e-routing.module';
import { Title4eComponent } from './title4e.component';
import { PeriodTablePopUpComponent } from './period-table-pop-up/period-table-pop-up.component';
import { TitleIveFosterCarComponent } from './title-ive-foster-car/title-ive-foster-car.component';
import { EligibleWorksheetFormComponent } from './title-ive-foster-car/eligible-worksheet-form/eligible-worksheet-form.component';
import { AttachmentComponent } from './title-ive-foster-car/attachment/attachment.component';
import { DecisionsComponent } from './title-ive-foster-car/decisions/decisions.component';
import { EligibleDetailsComponent } from './title-ive-foster-car/eligible-details/eligible-details.component';
import { NarrativesComponent } from './title-ive-foster-car/narratives/narratives.component';
import { ReportComponent } from './title-ive-foster-car/report/report.component';
import { DeemedIncomeWorksheetComponent } from './title-ive-foster-car/worksheets/deemed-income-worksheet/deemed-income-worksheet.component';
import { IncomeAssetWorksheetComponent } from './title-ive-foster-car/worksheets/income-asset-worksheet/income-asset-worksheet.component';

import { Title4eService } from './services/title4e.service';
import { SummaryComponent } from './title-ive-foster-car/summary/summary.component';
export function highchartfactory() {
    return require('highcharts');
}
import { NgxCurrencyModule } from 'ngx-currency';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ngx-currency/src/currency-mask.config';
import { AttachmentUploadComponent } from './title-ive-foster-car/attachment/attachment-upload/attachment-upload.component';
import { AudioRecordComponent } from './title-ive-foster-car/attachment/audio-record/audio-record.component';
import { VideoRecordComponent } from './title-ive-foster-car/attachment/video-record/video-record.component';
import { ImageRecordComponent } from './title-ive-foster-car/attachment/image-record/image-record.component';
import { EditAttachmentComponent } from './title-ive-foster-car/attachment/edit-attachment/edit-attachment.component';
import { SignaturePadModule } from 'angular2-signaturepad';

import {NgxPaginationModule} from 'ngx-pagination';
import { DynamicModule } from 'ng-dynamic-component';
import { FormMaterialModule } from '../../@core/form-material.module';
// import { DashboardRestorewidgetsComponent } from '../home-dashboard/dashboard-restorewidgets/dashboard-restorewidgets.component';
// import { GridsterModule } from 'angular-gridster2';
// import { DashboardTeamstatisticsComponent } from '../home-dashboard/dashboard-teamstatistics/dashboard-teamstatistics.component';
// import { DashboardMytasksComponent } from '../home-dashboard/dashboard-mytasks/dashboard-mytasks.component';
// import { DashboardMytodoComponent } from '../home-dashboard/dashboard-mytodo/dashboard-mytodo.component';
// import { DashboardMyirComponent } from '../home-dashboard/dashboard-myir/dashboard-myir.component';
// import { PersonInformationComponent } from '../home-dashboard/person-information/person-information.component';
import { TitleIveadoptionService } from './adoption/title-iveadoption.service';
import { Title4eFosterCareService } from './title-ive-foster-car/title4e-foster-care.service';
import { EligibleWorksheetFormLegalComponent } from './title-ive-foster-car/eligible-worksheet-form-legal/eligible-worksheet-form-legal.component';
import { EligibleWorksheetFormAfdcComponent } from './title-ive-foster-car/eligible-worksheet-form-afdc/eligible-worksheet-form-afdc.component';
import { EligibleWorksheetFormPlacementComponent } from './title-ive-foster-car/eligible-worksheet-form-placement/eligible-worksheet-form-placement.component';
import { EligibleWorksheetFormOthercriteriaComponent } from './title-ive-foster-car/eligible-worksheet-form-othercriteria/eligible-worksheet-form-othercriteria.component';
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: 'right',
    allowNegative: true,
    allowZero: true,
    decimal: ',',
    precision: 2,
    prefix: 'R$ ',
    suffix: '',
    thousands: '.',
    nullable: true
};
@NgModule({
    imports: [MatTooltipModule, MatFormFieldModule, CommonModule, ChartModule,    NgxfUploaderModule.forRoot(),
        FormMaterialModule,
        NgxPaginationModule,
        NgxCurrencyModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatRadioModule,
        MatTabsModule,
        MatTooltipModule,
        MatCheckboxModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatStepperModule,
        MatPaginatorModule,
        MatChipsModule,
        MatIconModule,
        MatDialogModule,
        QuillModule,
        Title4eRoutingModule, FormsModule, ReactiveFormsModule, PaginationModule, SharedDirectivesModule,
        SharedPipesModule, MatAutocompleteModule, MatChipsModule, SortTableModule, NgSelectModule, A2Edatetimepicker, SignaturePadModule, NgxPrintModule],

        entryComponents: [
            // DashboardRestorewidgetsComponent,
            // DashboardTeamstatisticsComponent,
            // DashboardMytodoComponent,
            // DashboardMytasksComponent,
            // DashboardMyirComponent
        ],
    declarations: [Supervisor4eComponent,
        // DashboardRestorewidgetsComponent,
        // DashboardTeamstatisticsComponent,
        // DashboardMytodoComponent,
        // DashboardMytasksComponent,
        // DashboardMyirComponent,
        // PersonInformationComponent,
       
        Title4eComponent, Worker4eComponent, PeriodTablePopUpComponent, IntakeFilterPipe,
        TitleIveFosterCarComponent, EligibleWorksheetFormComponent,
        EligibleDetailsComponent, AttachmentComponent, DecisionsComponent, NarrativesComponent, ReportComponent, SummaryComponent,
         AttachmentUploadComponent, VideoRecordComponent, AudioRecordComponent, ImageRecordComponent, EditAttachmentComponent, EligibleWorksheetFormLegalComponent, EligibleWorksheetFormAfdcComponent, EligibleWorksheetFormPlacementComponent, EligibleWorksheetFormOthercriteriaComponent,
          DeemedIncomeWorksheetComponent, IncomeAssetWorksheetComponent],
              

    providers: [
        { provide: HighchartsStatic, useFactory: highchartfactory }, Title4eService  , 
        TitleIveadoptionService, // add as factory to your providers
        Title4eFosterCareService
    ],
    exports: []
})
export class Title4eModule { }
