import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Title4eComponent } from './title4e.component';

describe('Title4eComponent', () => {
  let component: Title4eComponent;
  let fixture: ComponentFixture<Title4eComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Title4eComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Title4eComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
