import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { Title4eService } from '../services/title4e.service';
import { AppConstants } from '../../../@core/common/constants';
import { AlertService } from '../../../@core/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PaginationInfo } from '../../../@core/entities/common.entities';
import { DataStoreService } from '../../../@core/services';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'supervisor4e',
    templateUrl: './supervisor4e.component.html',
    styleUrls: ['./supervisor4e.component.scss']
})
export class Supervisor4eComponent implements OnInit {
    fosterCare: FormGroup;
    gap: FormGroup;
    adoption: FormGroup;
    acaform: FormGroup;
    supervisorDetails: any;
    fcCount: number;
    gapCount: number;
    adopCount: number;
    acaCount: number;
    fosterCareData: any = [];
    fosterCareMasterData: any = [];
    guardianShipData: any = [];
    guardianShipMasterData: any = [];
    adoptionData: any = [];
    acaData: any = [];
    adoptionMasterData: any = [];
    acaMasterData: any = [];
    specialistList: any[];
    fosterCareApprovalData: any = [];
    guardianShipApprovalData: any = [];
    adoptionApprovalData: any = [];
    approvalData: any = [];
    workersList: any[] = [];
    clientId = '';
    selectedStatus = 'OPEN';
    user = {};
    add = {
        test2: '',
        test3: '',
        test4: ''
    };
    routeData: any;
    getUsersList: any[];
    selectedPerson: any;
    paginationInfo: PaginationInfo = new PaginationInfo();
    gapPaginationInfo: PaginationInfo = new PaginationInfo();
    adopPaginationInfo: PaginationInfo = new PaginationInfo();
    acaPaginationInfo: PaginationInfo = new PaginationInfo();
    constructor(
        private router: Router,
        private _commonHttpService: CommonHttpService,
        private titleIVeService: Title4eService,
        private _alertService: AlertService,
        private _formBuilder: FormBuilder,
        private _dataStore: DataStoreService
    ) { }

    ngOnInit() {
        this.fosterCare = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
         this.gap = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
         this.adoption = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
         this.acaform = this._formBuilder.group({
            clientId: [''],
            fname: [''],
            lname: ['']
         });
        this.getSupervisorDetails();
        this.getGuardianShipDetails();
        this.getAdoptionDetails();
        this.getACADetails();
        this.getWorkersList();
        this.aprovalDetails();
        this.user = JSON.parse(localStorage.getItem('userProfile'));
    }
    Restore() {
        this.add.test2 = '';
        this.add.test3 = '';
        this.add.test4 = '';
    }
    onspecialistSelect(specialist, i) {
        this.specialistList.splice(i, 1);
    }
    getWorkersList() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INVR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.workersList = result.data.filter(s => s.rolecode !== 'SP');
            });
    }

    selectPerson(item) {
        this.selectedPerson = item;
    }
    accept(item) {
        item.STATUS = 'ACCEPTED';
    }
    changeSelectedStatus(status) {
        this.selectedStatus = status;
    }
    getSupervisorDetails() {
        this.supervisorDetails = {
            firstName: '',
            lastName: '',
            title: '',
            Jurisdiction: '',
            phoneNumber: ''
        };
        this.specialistList = [
            {
                id: '',
                name: ''
            },
            {
                id: '',
                name: ''
            }
        ];
        const fosterCareDashboardParam = {
            'status': '{16,71}',
            'eventType' : 'Fostercare',
            'fname' : this.fosterCare.value.fname ? this.fosterCare.value.fname.trim() : null,
            'lname' : this.fosterCare.value.lname ? this.fosterCare.value.lname.trim() : null,
            'clientId': this.fosterCare.value.clientId ? this.fosterCare.value.clientId : null
        };
        const pagination = {
            page: this.paginationInfo.pageNumber,
            limit: this.paginationInfo.pageSize
        }
        this.titleIVeService.getDashboardData(fosterCareDashboardParam, pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.fosterCareData = response.data;
                this.fosterCareMasterData = response.data;
                if (response.data && response.data.length) {
                    this.fcCount = response.data[0].countdata;
                  }
            }
        });

    }


    getGuardianShipDetails() {
        const guardianShipDashboardParam = {
            'status': '{16,73}',
            'eventType' : 'Gap',
            'fname' : this.gap.value.fname ? this.gap.value.fname.trim() : null,
            'lname' : this.gap.value.lname ? this.gap.value.lname.trim() : null,
            'clientId': this.gap.value.clientId ? this.gap.value.clientId : null
        };
        const pagination = {
            page: this.gapPaginationInfo.pageNumber,
            limit: this.gapPaginationInfo.pageSize,
        }
        this.titleIVeService.getDashboardData(guardianShipDashboardParam, pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.guardianShipData = this.guardianShipMasterData = response.data;
            }
            if (response.data && response.data.length) {
                this.gapCount = response.data[0].countdata;
            }
        });
    }

    getAdoptionDetails() {
        const adoptionDashboardParam = {
            'status': '{16,68,69}',
            'eventType' : 'Adoption',
            'fname' : this.adoption.value.fname ? this.adoption.value.fname.trim() : null,
            'lname' : this.adoption.value.lname ? this.adoption.value.lname.trim() : null,
            'clientId': this.adoption.value.clientId ? this.adoption.value.clientId : null
        };
        const pagination = {
            page: this.adopPaginationInfo.pageNumber,
            limit: this.adopPaginationInfo.pageSize,
        }
        this.titleIVeService.getDashboardData(adoptionDashboardParam,pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.adoptionData = this.adoptionMasterData = response.data;
            }
            if (response.data && response.data.length) {
                this.adopCount = response.data[0].countdata;
            }
        });
    }

    getACADetails(){
        const adoptionDashboardParam = {
            'status': '{16,68,69}',
            'eventType' : 'ACA',
            'fname' : this.acaform.value.fname ? this.acaform.value.fname.trim() : null,
            'lname' : this.acaform.value.lname ? this.acaform.value.lname.trim() : null,
            'clientId': this.acaform.value.clientId ? this.acaform.value.clientId : null
        };
        const pagination = {
            page: this.acaPaginationInfo.pageNumber,
            limit: this.acaPaginationInfo.pageSize,
        }
        this.titleIVeService.getDashboardData(adoptionDashboardParam,pagination).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.acaData = this.acaMasterData = response.data;
            }
            if (response.data && response.data.length) {
                this.acaCount = response.data[0].countdata;
            }
        });
    }


    aprovalDetails() {
        const approvalDashboardParam = {
            'status': '{71,74,68}'
        };
        this.titleIVeService.getDashboardApprovalData(approvalDashboardParam).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.approvalData = response.data;
            }
        });

    }


    getFosterCareApprovalDetails() {
        const fosterCareDashboardParam = {
            'status': '{71}',
            'eventType' : 'Fostercare'
        };
        this.titleIVeService.getDashboardApprovalData(fosterCareDashboardParam).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.fosterCareApprovalData = response.data;
            }
        });

    }


    getGuardianShipApprovalDetails() {
        const guardianShipDashboardParam = {
            'status': '{74}',
            'eventType' : 'Gap'
        };
        this.titleIVeService.getDashboardApprovalData(guardianShipDashboardParam).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.guardianShipApprovalData = response.data;
            }
        });
    }
    
    getAdoptionApprovalDetails() {
        const adoptionDashboardParam = {
            'status': '{68}',
            'eventType' : 'Adoption'
        };
        this.titleIVeService.getDashboardApprovalData(adoptionDashboardParam).subscribe(response => {
            if (response && response.data && Array.isArray(response.data)) {
                this.adoptionApprovalData = response.data;
            }
        });
    }

    searchClientIds(type: string, searchQuery: string){
        const clientid = searchQuery.trim();
        switch (type) {
            case 'FC':
                this.fosterCareData = this.filterClientId(clientid, this.fosterCareMasterData);
                break;
            case 'GAP':
                this.guardianShipData = this.filterClientId(clientid, this.guardianShipMasterData);
                break;
            case 'ADOP':
                this.adoptionData = this.filterClientId(clientid, this.adoptionMasterData);
                break;
            case 'ACA':
                this.adoptionData = this.filterClientId(clientid, this.acaMasterData);
                break;
        }
    }

    filterClientId(clientid, masterData){
        return _.isEmpty(clientid) ? masterData : _.filter(masterData, { clientid });
    }

    onSearch(field: string, value: string) {
        this.router.navigate(['/pages/title4e/supervisor4e']);
    }
    searchFoster(item) {
        this.titleIVeService.getClientId(item.client_id);
        if (item.client_id) {
            this.router.navigate(['/pages/title4e/foster-car/' + item.client_id + '/' + item.placement_id + '/' + item.removal_id]);
        }
    }

    searchFosterDashboard(item) {
        this.titleIVeService.getClientId(item.clientid);
        if (item.clientid) {
            this.router.navigate(['/pages/title4e/foster-car/' + item.clientid + '/' + item.placementid + '/' + item.removalid]);
        }
    }

    searchGap(id) {
        // this.title4eService.getClientId(id);
         if (id) {
             this._dataStore.setData('guardianship_clientid',id);
             this.router.navigate(['/pages/title4e/guardianship/' + id]);
         } else {
            //  console.log('error');
         }
     }

     searchAdoption(item) {
        // this.title4eService.getClientId(id);
         if (item) {
            this.router.navigate(['/pages/title4e/adoption/' + item.clientid +'/'+item.removalid]);
            this._dataStore.setData('adoption_clientid', item.clientid);
            this._dataStore.setData('adoption_removalid', item.removalid);
            this._dataStore.setData('adoption_dashboard', 'adoption');
         } else {
            //  console.log('error');
         }
     }


     searchACA(item) {
        // this.title4eService.getClientId(id);
         if (item) {
            this.router.navigate(['/pages/title4e/adoption/' + item.clientid +'/'+item.removalid]);
            this._dataStore.setData('adoption_clientid', item.clientid);
            this._dataStore.setData('adoption_removalid', item.removalid);
            this._dataStore.setData('adoption_dashboard', 'aca');
         } else {
            //  console.log('error');
         }
     }

     searchAdoptionApproval(item) {
        // this.title4eService.getClientId(id);
         if (item) {
            this._dataStore.setData('adoption_clientid', item.client_id);
            this._dataStore.setData('adoption_removalid', item.removal_id);
            this.router.navigate(['/pages/title4e/adoption/' + item.client_id +'/'+item.removal_id]);
         } else {
            //  console.log('error');
         }
     }

     
    assignFosterCare(data) {
        this.titleIVeService.getUsersList().subscribe(result => {
            this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SPECIALIST);
        });
        this.routeData = data;
        (<any>$('#caseassign')).modal('show');
    }


    assignGuardianShip(data) {
        this.titleIVeService.getUsersList().subscribe(result => {
            this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SPECIALIST);
        });
        this.routeData = data;
        (<any>$('#caseassignguardianship')).modal('show');
    }


    assignAdoption(data) {
        this.titleIVeService.getUsersList().subscribe(result => {
            this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SPECIALIST);
        });
        this.routeData = data;
        (<any>$('#caseassignadoption')).modal('show');
    }

    assignACA(data) {
        this.titleIVeService.getUsersList().subscribe(result => {
            this.getUsersList = result.data.filter(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SPECIALIST);
        });
        this.routeData = data;
        console.log('-----'+JSON.stringify(this.routeData));
        (<any>$('#caseassignaca')).modal('show');
    }

    assignUser() {
        const data = {
            'where': {
                'assignedtoid': this.selectedPerson.userid,
                'eventcode': 'PLTR',
                'servicecaseid': this.routeData.caseid,
                'placementid': this.routeData.placementid,
                'status': 'SplApproval',
                'notifymsg': 'Placement for ' + this.routeData.casenumber + 'sent for approval',
                'routeddescription': 'Placement sent for approval',
                'comments': 'Placement sent for approval',
            }
        };
        this.titleIVeService.routingUpdate(data).subscribe(response => {
            this._alertService.success('Case Assigned Successfully ');
            (<any>$('#caseassign')).modal('hide');
            // go back to dashboard
        });
    }


    assignUserGuardianShip() {
        const data = {
            'where': {
                'assignedtoid': this.selectedPerson.userid,
                'eventcode': 'GAAR',
                'servicecaseid': this.routeData.caseid,
                'gapagreementid': this.routeData.gapagreementid,
                'status': 'SplApproval',
                'notifymsg': 'Placement for ' + this.routeData.casenumber + 'sent for approval',
                'routeddescription': 'Placement sent for approval',
                'comments': 'Placement sent for approval',
            }
        };
        this.titleIVeService.routingUpdate(data).subscribe(response => {
            this.getGuardianShipDetails();
            this._alertService.success('Case Assigned Successfully ');
            (<any>$('#caseassignguardianship')).modal('hide');
            // go back to dashboard
        });
    }


    assignUserAdoption() {
        const data = {
            'where': {
                'assignedtoid': this.selectedPerson.userid,
                'eventcode': 'ABLR',
                'servicecaseid': this.routeData.caseid,
                'adoptionbreakthelinkid': this.routeData.adoptionbreakthelinkid,
                'status': 'SplApproval',
                'notifymsg': 'Placement for ' + this.routeData.casenumber + 'sent for approval',
                'routeddescription': 'Placement sent for approval',
                'comments': 'Placement sent for approval',
            }
        };
        this.titleIVeService.routingUpdate(data).subscribe(response => {
            this.getAdoptionDetails();
            this._alertService.success('Case Assigned Successfully ');
            (<any>$('#caseassignadoption')).modal('hide');
            // go back to dashboard
        });
    }


    assignUserACA() {
        const data = {
            'where': {
                'assignedtoid': this.selectedPerson.userid,
                'eventcode': 'ADAP',
                'servicecaseid': this.routeData.caseid,
                'adoptionbreakthelinkid': this.routeData.adoptionbreakthelinkid,
                'status': 'SplApproval',
                'notifymsg': 'Placement for ' + this.routeData.casenumber + 'sent for approval',
                'routeddescription': 'Placement sent for approval',
                'comments': 'Placement sent for approval',
            }
        };
        this.titleIVeService.routingUpdate(data).subscribe(response => {
            this.getACADetails();
            this._alertService.success('Case Assigned Successfully ');
            (<any>$('#caseassignaca')).modal('hide');
            // go back to dashboard
        });
    }

    resetaca() {
        this.acaform.reset();
        this.getACADetails();
    }
    resetadop() {
        this.adoption.reset();
        this.getAdoptionDetails();
    }
    resetgap() {
        this.gap.reset();
        this.getGuardianShipDetails();
    }
    reset() {
        this.fosterCare.reset();
        this.getSupervisorDetails();
    }

    pageChanged(pageNumber: any) {
        this.paginationInfo.pageNumber = pageNumber.page;
        this.getSupervisorDetails();
    }
    gapPageChanged(pageNumber: any) {
        this.gapPaginationInfo.pageNumber =  pageNumber.page;
        this.getGuardianShipDetails();
    }
    adopPageChanged(pageNumber: any) {
        this.adopPaginationInfo.pageNumber =  pageNumber.page;
        this.getAdoptionDetails();
    }
    acaPageChanged(pageNumber: any) {
        this.acaPaginationInfo.pageNumber =  pageNumber.page;
        this.getACADetails();
    }
}
