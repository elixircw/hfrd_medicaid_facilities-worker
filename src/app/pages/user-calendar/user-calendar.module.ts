import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserCalendarRoutingModule } from './user-calendar-routing.module';
import { UserCalendarComponent } from './user-calendar.component';
import { CalendarModule } from 'angular-calendar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ControlMessagesModule } from '../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    UserCalendarRoutingModule,
    CalendarModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    ControlMessagesModule,
    NgSelectModule,
    SharedPipesModule
  ],
  declarations: [UserCalendarComponent]
})
export class UserCalendarModule { }
