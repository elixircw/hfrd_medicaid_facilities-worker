import { Component, ChangeDetectionStrategy, ViewChild, TemplateRef, OnInit } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { GenericService } from '../../@core/services';
import { PaginationRequest } from '../../@core/entities/common.entities';
import { CalendarEvents } from './_entities/usercalendar-entity.module';
import { Router } from '@angular/router';
const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    // tslint:disable-next-line:component-selector
    selector: 'user-calendar',
    templateUrl: './user-calendar.component.html',
    styleUrls: ['./user-calendar.component.scss']
})
export class UserCalendarComponent {
    @ViewChild('modalContent') modalContent: TemplateRef<any>;

    activeDayIsOpen = true;
    view = 'month';

    viewDate: Date = new Date();

    modalData: {
        action: string;
        event: CalendarEvent;
    };
    calendarEvent: CalendarEvents[];
    actions: CalendarEventAction[] = [
        {
            label: '',
            onClick: ({ event }: { event: CalendarEvents }): void => {
                this.handleEvent('Edited', event);
            }
        }
    ];

    refresh: Subject<any> = new Subject();

    events: CalendarEvent[] = [];

    constructor(private _service: GenericService<CalendarEvents>, private router: Router) {}

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnInit() {
        this.getNotificationData();
    }

    getNotificationData() {
        const url = 'Intakeservicerequests/getcalendarevents';
        this._service
            .getArrayList(
                new PaginationRequest({
                    method: 'get',
                    nolimit: true
                }),
                url + '?filter'
            )
            .subscribe((Response) => {
                this.calendarEvent = Response;
                this.events = this.calendarEvent.map((item) => {
                    return {
                        start: addDays(item.targetdate, 0),
                        title: item.comments + ' (' + item.servicerequestnumber + ')',
                        color: colors.blue,
                        servicerequestnumber: item.servicerequestnumber,
                        intakeservicereqid: item.intakeservicereqid
                    } as CalendarEvents;
                });
                this.refresh.next();
            });
    }
    dayClicked({ date, events }: { date: Date; events: CalendarEvents[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }

    eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.handleEvent('Dropped or resized', event as CalendarEvents);
        this.refresh.next();
    }

    handleEvent(action: string, event: CalendarEvents): void {
        this.modalData = { event, action };
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        };
        const currentUrl = '/pages/case-worker/' + event.intakeservicereqid + '/' + event.servicerequestnumber + '/dsds-action/report-summary';
        this.router.navigateByUrl(currentUrl).then(() => {
            this.router.navigated = true;
            this.router.navigate([currentUrl]);
        });
    }
}
