import { CalendarEvent, EventColor, EventAction } from 'calendar-utils';

export class CalendarEvents implements CalendarEvent {
    targetdate: Date;
    comments: string;
    servicerequestnumber?: string;
    intakeservicereqid?: string;
    id?: string | number;
    start: Date;
    end?: Date;
    title: string;
    color: EventColor;
    actions?: EventAction[];
    allDay?: boolean;
    cssClass?: string;
    resizable?: {
        beforeStart?: boolean;
        afterEnd?: boolean;
    };
    draggable?: boolean;
    meta?: any;
}
