import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderReferralComponent } from './provider-referral.component';
//import { NewSaveintakeComponent } from './new-saveintake/new-saveintake.component';
import { ExistingReferralComponent } from './existing-referral/existing-referral.component';
import { RoleGuard } from '../../@core/guard/role.guard';

const routes: Routes = [
    {
        path: '',
        component: ProviderReferralComponent,
        // canActivate: [RoleGuard],
        children: [
            {
                path: 'new-referral',
                loadChildren: './new-referral/new-referral.module#NewReferralModule'
            },
            {
                path: 'new-public-referral',
                loadChildren: './new-public-referral/new-public-referral.module#NewPublicReferralModule'
            },
            {
                path: 'new-private-referral',
                loadChildren: './new-private-referral/new-private-referral.module#NewPrivateReferralModule'
            }
        ]
    },
    {
        path: 'existing-referral',
        component: ExistingReferralComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Saved Intakes'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProviderReferralRoutingModule {}
