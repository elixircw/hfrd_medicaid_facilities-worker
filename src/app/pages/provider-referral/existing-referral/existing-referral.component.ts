import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { Subject } from 'rxjs/Rx';

import { DynamicObject, PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { AuthService } from '../../../@core/services/auth.service';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { GenericService } from '../../../@core/services/generic.service';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';

import { MyReferralDetails } from '../new-referral/_entities/newreferralModel';
import { ReferralSearchCase,   RoutingUser,
} from '../new-referral/_entities/existingreferralModel';
import { BroadCostMessage } from '../new-referral/_entities/newintakeSaveModel';
import { NewUrlConfig, ReferralUrlConfig } from '../provider-referral-url.config';
import { AlertService } from '../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataStoreService } from '../../../@core/services';

declare var $: any;
@Component({
    selector: 'existing-referral',
    templateUrl: './existing-referral.component.html',
    styleUrls: ['./existing-referral.component.scss']
})

export class ExistingReferralComponent implements OnInit {
    showHistory: boolean;
    searchHistory = [];
    searchReferralForm: FormGroup;
    referralType= [];
    referralForm: FormGroup;
    currentReferralType: String;
    paginationInfo: PaginationInfo = new PaginationInfo();
    referrals: MyReferralDetails[];
    getUsersList: RoutingUser[];
    originalUserList: RoutingUser[];
    isSupervisor: boolean;
    totalRecords: number;
    dynamicObject: DynamicObject = {};
    showBroadCostMessage: BroadCostMessage;
    roleName: string;
    isPreIntake = false;
    previousPage: number;
    searchCriteria: ReferralSearchCase;
    selectedPerson: any;
    getServicereqid: any;
    publicReferrals: any;
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();
    isDjs = false;
    isGroup = false;
    status: string;
    isPrivate: Boolean = false;
    teamtypekey = 'OLM';

    constructor(private formBuilder: FormBuilder, private _authService: AuthService, private _alertService: AlertService,
        private _service: GenericService<MyReferralDetails>, private _commonHttpService: CommonHttpService,
        private _dataStoreService: DataStoreService,
        private _router: Router, private route: ActivatedRoute) {}

    ngOnInit() {
        this.formInitilize();
        // this.getProviderReferral('Pending');
        const role = this._authService.getCurrentUser();
        const resources = role.resources;
        this.currentReferralType = 'Public';
        if (resources.length > 0 ) {
          let tempres = resources.filter(res => {
            if (res.name === 'Private-Provider') {
              this.isPrivate = true;
              this.currentReferralType = 'Private';
              return;
            }
          });
        }
        this.referralType = ['Public', 'Private', 'Vendor'];
        this.paginationInfo.sortBy = 'desc';
        this.paginationInfo.sortColumn = 'datereceived';
        this.isDjs = role.user.userprofile.teamtypekey === 'DJS';
        // if (role.user.userprofile.teamtypekey === 'DJS') {
        //   this.teamtypekey = 'DJS';
        // }
        if (role.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
          || role.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || role.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
          || role.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || role.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA' ) {
            this.teamtypekey = 'DJS';
        }
        if (role.role.name === 'superuser' || role.role.name === 'cru' || role.role.name === 'ASCW') {
          this.getBroadCostMessage();
        }
        this.roleName = role.role.name;
        if (this.roleName === 'SCRNW') {
          this.isPreIntake = true;
        }
        this.showHistory = false;
        this.status = 'Pending';
        if (this.isPrivate === true) {
          this.getReferralType('Private', 1 , '');
        } else {
          this.getReferralType('Public', 1 , '');
        }
        // this.getPage(1, 'Pending');
        this.initReferralForm();
      }

    formInitilize() {
        this.searchReferralForm = this.formBuilder.group({
            referralnumber: ['']
        });
    }

    getProviderReferral( status: String) {
      if (this.isPrivate === true) {
        this.getPrivateProviderReferral(status);
      } else {
        this.getPublicProviderReferral(status);
      }
    }
    getPublicProviderReferral(status: String) {
      this.publicReferrals = [];
      this._commonHttpService.getArrayList(
          {
              method: 'get',
              order: ['create_ts DESC'],
              nolimit: true,
              where: {"referral_status" : status}
          }, 'publicproviderreferral?filter'
          ).subscribe(publicReferrals => {
            this.publicReferrals = publicReferrals;
          });
    }

    getPrivateProviderReferral(status: String) {
      this.referrals = [];
      console.log("teamtypekey====>",this.teamtypekey);      
      this._commonHttpService.getArrayList(
          {
              method: 'get',
              order: ['create_ts DESC'],
              nolimit: true,
              where: {"referral_decision" : status,"agency": this.teamtypekey}
          }, 'providerreferral?filter'
          ).subscribe(referrals => {
            this.referrals = referrals;
          });
    }

    private initReferralForm() {
      if ( this.isPrivate === true) {
        this.referralForm = this.formBuilder.group({
          applicant_type: ['Private'],
        });
      } else {
        this.referralForm = this.formBuilder.group({
          applicant_type: ['Public'],
        });
      }
      this.referralForm.disable();
      }

      getReferralType(referralType: string, pageNumber: number, status: string) {
        this.status = status;
        if (referralType !== '') {
          this.currentReferralType = referralType;
        }
        //   alert(this.currentReferralType);
        if (status === '') {
          status = 'Pending';
        }
        this.getProviderReferral(status);
        // if (this.currentReferralType == "Public") {
        // } else
        // if (this.currentReferralType == "Private") {
        //   this.getPage(pageNumber,status);
        // }
        // console.log("Selected referral Type:"+this.currentReferralType)
      }

    getPage(selectPageNumber: number, status: string) {
        this.status = status;
        const pageSource = this.pageStream$.map((pageNumber) => {
            if (this.paginationInfo.pageNumber !== 1) {
                this.previousPage = pageNumber;
            } else {
                this.previousPage = this.paginationInfo.pageNumber;
            }
            return { search: this.dynamicObject, page: this.previousPage };
        });
        
        const searchSource = this.searchTermStream$.debounceTime(1000).map((searchTerm) => {
            this.previousPage = 1;
            this.dynamicObject = searchTerm;
            if (searchTerm && searchTerm.referralnumber) {
                this.searchHistory.push({ referralnumber: searchTerm.referralnumber.like.replace('%25', '').replace('%25', '') });
            }
            this.showHistory = false;
            return { search: searchTerm, page: this.previousPage };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                let referralnumber = '';
                
                if (this.searchReferralForm.value.referralnumber) {
                    referralnumber = this.searchReferralForm.value.referralnumber;
                }
                
                this.searchCriteria = {
                    status: this.status,
                    referralnumber: referralnumber,
                    sortcolumn: this.paginationInfo.sortColumn,
                    sortorder: this.paginationInfo.sortBy
                };

                return this._service.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.paginationInfo.pageSize,
                        page: this.previousPage,
                        method: 'post',
                        where: this.searchCriteria
                    }),
                    NewUrlConfig.EndPoint.Intake.saveIntakeUrl
                );
            })
            .subscribe((result) => {
                this.referrals = result.data;
                this.totalRecords = 10;
                
console.log(result.data);
console.log(result.count);

            });
    }

    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortDirection;
        this.paginationInfo.sortColumn = $event.sortColumn;
        this.getPage(this.paginationInfo.pageNumber, this.status);
    }

    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.previousPage = this.paginationInfo.pageNumber;
        this.getPage(this.previousPage, this.status);
    }

    onSearch(field: string, value: string) {
        this.showHistory = true;
        this.searchReferralForm.patchValue({ referralnumber: value });
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }

    htmlToPlaintext(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    }
    
    getBroadCostMessage() {
        this._commonHttpService.getSingle({}, 'announcement/getuserannouncement').subscribe((result) => {
            if (result !== null) {
                (<any>$('#broadcoastmessage')).modal('show');
                this.showBroadCostMessage = result;
            }
        });
    }

    showHideHistory(value) {
        this.showHistory = value;
    }

    acceptAnnouncement() {
        this._commonHttpService.endpointUrl = 'announcement/acceptannouncement';
        (<any>$('#broadcoastmessage')).modal('hide');
        this._commonHttpService.patch(this.showBroadCostMessage.userannouncementid, { id: this.showBroadCostMessage.userannouncementid }).subscribe((data) => {});
    }

    listUser(assigned: string) {
        this.selectedPerson = '';
        this.getUsersList = [];
        this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
          this.getUsersList = this.getUsersList.filter(res => {
            if (res.issupervisor === false) {
              this.isSupervisor = false;
              return res;
            }
          });
        }
    }

    getRoutingUser(modal) {
        this.getServicereqid = modal.provider_referral_id;
     //  this.getServicereqid = "R201800200374";
        console.log(modal.provider_referral_id);
       // this.zipCode = modal.incidentlocation;
        this.getUsersList = [];
        this.originalUserList = [];
       // this.isGroup = modal.isgroup;
        this._commonHttpService
          .getPagedArrayList(
            new PaginationRequest({
              where: { appevent: 'PTA' },
              method: 'post'
            }),
            'Intakedastagings/getroutingusers'
          )
         .subscribe(result => {
            this.getUsersList = result.data;
            this.originalUserList = this.getUsersList;
           this.listUser('TOBEASSIGNED');
         });
    
    }

    selectPerson(row) {
        this.selectedPerson = row;
      }

      assignUser() {
        if (this.selectedPerson) {
          this._commonHttpService
            .getPagedArrayList(
              new PaginationRequest({
                where: {
                  appeventcode: 'APPL',
                  serreqid: this.getServicereqid,
                  assigneduserid: this.selectedPerson.userid,
                  isgroup: this.isGroup
                },
                method: 'post'
              }),
              'providerreferral/routereferralda'
            )
            .subscribe(result => {
              this._alertService.success('Case assigned successfully!');
            //  this.getAssignCase(1, false);
              this.closePopup();
            });
        } else {
          this._alertService.warn('Please select a person');
        }
      }

      closePopup() {
        (<any>$('#intake-caseassign')).modal('hide');
        (<any>$('#assign-preintake')).modal('hide');
        (<any>$('#reopen-intake')).modal('hide');
      }

      newPublicInquiry() {
        this._commonHttpService.getArrayList(
          {}, ReferralUrlConfig.EndPoint.Referral.getNextReferralNumberUrl
          ).subscribe((result) => {
            const refferalID =  result['nextNumber'];
          this._dataStoreService.setData('referralId', refferalID);
          console.log("first time", this._dataStoreService.getData('referralId'));
          // Make a record in the db with the new referral number
          this._commonHttpService.create( 
            {
              referral_id:  result['nextNumber'],
              referral_status: 'Pending'
            },
            'publicproviderreferral/'
          ).subscribe(
            (response) => {
              this._router.navigate(['/pages/provider-referral/new-public-referral', refferalID]);
            },
            (error) => {
              this._alertService.error('Unable to create new referral, Please try again');
            });
      });
    }
}
