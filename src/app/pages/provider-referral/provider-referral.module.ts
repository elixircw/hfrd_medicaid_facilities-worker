import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { SortTableModule } from '../../shared/modules/sortable-table/sortable-table.module';
import { SharedDirectivesModule } from '../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { ExistingReferralComponent } from './existing-referral/existing-referral.component';
import { ProviderReferralRoutingModule } from './provider-referral-routing.module';
import { ProviderReferralComponent } from './provider-referral.component';
import { MatAutocompleteModule,  MatSelectModule,
    MatFormFieldModule, MatChipsModule , MatTooltipModule, MatPaginatorModule, MatInputModule} from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
    imports: [MatTooltipModule, MatFormFieldModule, MatInputModule ,CommonModule, ProviderReferralRoutingModule, FormsModule, ReactiveFormsModule, PaginationModule, SharedDirectivesModule,
         SharedPipesModule, MatAutocompleteModule,  MatSelectModule,
         MatChipsModule, SortTableModule, MatPaginatorModule,NgxPaginationModule],
    declarations: [ProviderReferralComponent, ExistingReferralComponent]
})
export class ProviderReferralModule {}
