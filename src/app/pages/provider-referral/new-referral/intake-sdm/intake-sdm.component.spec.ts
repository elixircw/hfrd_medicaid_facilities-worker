import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeSdmComponent } from './intake-sdm.component';

describe('IntakeSdmComponent', () => {
  let component: IntakeSdmComponent;
  let fixture: ComponentFixture<IntakeSdmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeSdmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeSdmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
