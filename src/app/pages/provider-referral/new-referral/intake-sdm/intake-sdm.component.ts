import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { FormArray, FormBuilder, FormControl, FormGroup } from '../../../../../../node_modules/@angular/forms';
import { Subject } from '../../../../../../node_modules/rxjs/Subject';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { MaltreatorsName, ProviderName, Sdm, VictimName } from '../_entities/newintakeModel';
import { InvolvedPerson } from './../_entities/newintakeModel';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-sdm',
    templateUrl: './intake-sdm.component.html',
    styleUrls: ['./intake-sdm.component.scss']
})
export class IntakeSdmComponent implements OnInit {
    sdmFormGroup: FormGroup;
    isDisplayProvider = false;
    isChildInderOneYear: string;
    isScreenOutIN = 'No';
    isImmediate: string;
    sdm = new Sdm();
    populateSdm: Sdm;
    allegedVictim: VictimName[] = [];
    allegedMaltreator: MaltreatorsName[] = [];
    provider: ProviderName[] = [];
    disablecheckboxFields = [];
    maxDate = new Date();
    @Input()
    sdmInputSubject$ = new Subject<Sdm>();
    @Input()
    sdmOutputSubject$ = new Subject<Sdm>();
    @Input()
    addedPersons$ = new Subject<InvolvedPerson[]>();
    sdmCountyValuesDropdownItems$: Observable<DropdownModel[]>;
    roleId: AppUser;
    dayToOverride: Number;
    sdmSettings: {
        issexualabuse: boolean;
        isoutofhome: boolean;
        isdeathorserious: boolean;
        issignordiagonises: boolean;
        isPopulate: boolean;
        isDisableOverride: boolean;
        isSupervisor: boolean;
    };
    constructor(
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService
    ) {
        this.sdmSettings = {
            issexualabuse: false,
            isoutofhome: false,
            isdeathorserious: false,
            issignordiagonises: false,
            isPopulate: false,
            isDisableOverride: false,
            isSupervisor: false
        };
    }

    ngOnInit() {
        this.buildFormGroup();
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.role.name === 'apcs') {
            this.disablecheckboxFields = [
                'physicalAbuse',
                'sexualAbuse',
                'generalNeglect',
                'negfp_cargiverintervene',
                'negab_abandoned',
                'unattendedChild',
                'riskofHarm',
                'negmn_unreasonabledelay',
                'menab_psycologicalability',
                'menng_psycologicalability'
            ];
            // this.disablecheckboxFields.map((controlName) => {
            //     this.sdmFormGroup.controls[controlName].disable();
            // });
        }

        this.addedPersons$.subscribe(result => {
            result.map(item => {
                item.personRole.map(roleval => {
                    if (roleval.rolekey === 'AV') {
                        this.sdmFormGroup.setControl(
                            'allegedvictim',
                            this.formBuilder.array([])
                        );
                        this.allegedVictim =
                            this.allegedVictim.length !== 0
                                ? this.allegedVictim
                                : [{ victimname: item.fullName }];
                        const avData = this.allegedVictim.filter(
                            name => name.victimname === item.fullName
                        );
                        if (avData.length === 0) {
                            const allegedV = {
                                victimname: item.fullName
                            };
                            this.allegedVictim.push(allegedV);
                        }
                    }
                    if (roleval.rolekey === 'AM') {
                        this.sdmFormGroup.setControl(
                            'allegedmaltreator',
                            this.formBuilder.array([])
                        );
                        this.allegedMaltreator =
                            this.allegedMaltreator.length !== 0
                                ? this.allegedMaltreator
                                : [{ maltreatorsname: item.fullName }];
                        const amData = this.allegedMaltreator.filter(
                            name => name.maltreatorsname === item.fullName
                        );
                        if (amData.length === 0) {
                            const allegedM = {
                                maltreatorsname: item.fullName
                            };
                            this.allegedMaltreator.push(allegedM);
                        }
                    }
                });
            });
            this.setFormValues();
        });

        this.sdmFormGroup.valueChanges.subscribe(() => {
            this.sdm = Object.assign({}, this.sdmFormGroup.value);
            if (
                this.validateGroup(this.sdmFormGroup.controls[
                    'physicalAbuse'
                ] as FormGroup) &&
                this.validateGroup(this.sdmFormGroup.controls[
                    'sexualAbuse'
                ] as FormGroup) &&
                this.validateGroup(this.sdmFormGroup.controls[
                    'generalNeglect'
                ] as FormGroup) &&
                this.validateGroup(this.sdmFormGroup.controls[
                    'unattendedChild'
                ] as FormGroup) &&
                this.validateGroup(this.sdmFormGroup.controls[
                    'riskofHarm'
                ] as FormGroup) &&
                !this.sdm.negfp_cargiverintervene &&
                !this.sdm.negab_abandoned &&
                !this.sdm.negmn_unreasonabledelay &&
                !this.sdm.menab_psycologicalability &&
                !this.sdm.menng_psycologicalability
            ) {
                this.sdmFormGroup.patchValue(
                    { screeningRecommend: 'ScreenOUT' },
                    { emitEvent: false }
                );
            } else {
                this.sdmFormGroup.patchValue(
                    { screeningRecommend: 'Scrnin' },
                    { emitEvent: false }
                );
            }
            if (!this.sdmSettings.isPopulate) {
                this.cpsResponseValidation(this.sdmFormGroup.value);
            }
            if (
                this.validateGroup(this.sdmFormGroup.controls[
                    'disqualifyingCriteria'
                ] as FormGroup) &&
                this.validateGroup(this.sdmFormGroup.controls[
                    'disqualifyingFactors'
                ] as FormGroup)
            ) {
                this.sdmFormGroup.patchValue(
                    { cpsResponseType: 'CPS-AR' },
                    { emitEvent: false }
                );
            } else {
                this.sdmFormGroup.patchValue(
                    { cpsResponseType: 'CPS-IR' },
                    { emitEvent: false }
                );
            }

            this.sdmInputSubject$.next(this.sdmFormGroup.value);
        });
        this.sdmOutputSubject$.subscribe(sdm => {
            this.sdmSettings.isPopulate = true;
            this.populateSdm = Object.assign({}, sdm);
            this.sdmFormGroup.setControl(
                'allegedvictim',
                this.formBuilder.array([])
            );
            this.sdmFormGroup.setControl(
                'allegedmaltreator',
                this.formBuilder.array([])
            );
            this.sdmFormGroup.setControl(
                'provider',
                this.formBuilder.array([])
            );
            this.populateSdm.referraldob = new Date(
                this.populateSdm.referraldob
            );
            this.populateSdm.reportdate = new Date(this.populateSdm.reportdate);
            this.sdmFormGroup.patchValue(this.populateSdm);
            this.allegedVictim = this.populateSdm.allegedvictim;
            this.allegedMaltreator = this.populateSdm.allegedmaltreator;
            this.provider = this.populateSdm.provider;

            if (this.populateSdm.maltreatment === 'yes') {
                this.isDisplayProvider = true;
            }
            this.cpsResponseValidation(sdm);
            this.setFormValues();
            this.roleId = this._authService.getCurrentUser();
            if (
                this.roleId.role.name === 'apcs' &&
                this.populateSdm.datesubmitted
            ) {
                this.sdmSettings.isSupervisor = true;
                this.validateOverrideDays(this.populateSdm.datesubmitted);
            }
            this.sdmSettings.isPopulate = false;
        });
        this.getCountyDropdown();
    }

    buildFormGroup() {
        this.sdmFormGroup = this.formBuilder.group({
            referralname: [''],
            referraldob: [new Date()],
            referralid: [''],
            countyid: [null],
            ismaltreatment: [false],
            maltreatment: [''],
            physicalAbuse: this.formBuilder.group({
                malpa_suspeciousdeath: [false],
                malpa_nonaccident: [false],
                malpa_injuryinconsistent: [false],
                malpa_insjury: [false],
                malpa_childtoxic: [false],
                malpa_caregiver: [false]
            }),
            sexualAbuse: this.formBuilder.group({
                malsa_sexualmolestation: [false],
                malsa_sexualact: [false],
                malsa_sexualexploitation: [false],
                malsa_physicalindicators: [false]
            }),
            generalNeglect: this.formBuilder.group({
                neggn_suspiciousdeath: [false],
                neggn_signsordiagnosis: [false],
                neggn_inadequatefood: [false],
                neggn_exposuretounsafe: [false],
                neggn_inadequateclothing: [false],
                neggn_inadequatesupervision: [false],
                neggn_childdischarged: [false]
            }),
            negfp_cargiverintervene: [false],
            negab_abandoned: [false],
            unattendedChild: this.formBuilder.group({
                neguc_leftunsupervised: [false],
                neguc_leftaloneinappropriatecare: [false],
                neguc_leftalonewithoutsupport: [false]
            }),
            riskofHarm: this.formBuilder.group({
                negrh_priordeath: [false],
                negrh_sexualperpetrator: [false],
                negrh_basicneedsunmet: [false]
            }),
            negmn_unreasonabledelay: [false],
            menab_psycologicalability: [false],
            menng_psycologicalability: [false],
            screeningRecommend: [''],
            scnRecommendOveride: [''],
            screenOut: this.formBuilder.group({
                isscrnoutrecovr_insufficient: [false],
                isscrnoutrecovr_information: [false],
                isscrnoutrecovr_historicalinformation: [false],
                isscrnoutrecovr_otherspecify: [false],
                scrnout_description: ['']
            }),
            screenIn: this.formBuilder.group({
                isscrninrecovr_courtorder: [false],
                isscrninrecovr_otherspecify: [false],
                scrnin_description: ['']
            }),
            immediate: [''],
            immediateList: this.formBuilder.group({
                isimmed_childfaatility: [false],
                isimmed_seriousinjury: [false],
                isimmed_childleftalone: [false],
                isimmed_allegation: [false],
                isimmed_otherspecify: [false],
                immediateList6: ['']
            }),
            noImmediateList: this.formBuilder.group({
                isnoimmed_physicalabuse: [false],
                isnoimmed_sexualabuse: [false],
                isnoimmed_neglectresponse: [false],
                isnoimmed_mentalinjury: [false]
            }),
            childunderoneyear: [''],
            childUnderOneYear: [''],
            officerfirstname: [''],
            officermiddlename: [''],
            officerlastname: [''],
            badgenumber: [''],
            recordnumber: [''],
            reportdate: [''],
            worker: [''],
            workerdate: [null],
            supervisor: [''],
            supervisordate: [null],
            disqualifyingCriteria: this.formBuilder.group({
                issexualabuse: [false],
                isoutofhome: [false],
                isdeathorserious: [false],
                isrisk: [false],
                isreportmeets: [false],
                issignordiagonises: [false],
                ismaltreatment3yrs: [false],
                ismaltreatment12yrs: [false],
                ismaltreatment24yrs: [false],
                isactiveinvestigation: [false]
            }),
            disqualifyingFactors: this.formBuilder.group({
                isreportedhistory: [false],
                ismultiple: [false],
                isdomesticvoilence: [false],
                iscriminalhistory: [false],
                isthread: [false],
                islawenforcement: [false],
                iscourtiinvestigation: [false]
            }),
            cpsResponseType: [null],
            isar: [true],
            isir: [false],
            // iscps: [null],
            isfinalscreenin: [null]
        });
        this.sdmFormGroup.addControl(
            'allegedvictim',
            this.formBuilder.array([this.createFormGroup('allegedvictim')])
        );
        this.sdmFormGroup.addControl(
            'allegedmaltreator',
            this.formBuilder.array([this.createFormGroup('allegedmaltreator')])
        );
        this.sdmFormGroup.addControl(
            'provider',
            this.formBuilder.array([this.createFormGroup('provider')])
        );
    }

    getCountyDropdown() {
        this.sdmCountyValuesDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.SdmCountyListUrl + '?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }

    setFormValues() {
        const control = <FormArray>this.sdmFormGroup.controls.allegedvictim;
        // if (this.allegedVictim.length === 1) {
        //     this.sdmFormGroup.setControl('allegedvictim', this.formBuilder.array([]));
        // }
        this.allegedVictim.forEach(x => {
            control.push(this.buildAllegedVictimForm(x));
        });
        const allegedMaltreatorControl = <FormArray>(
            this.sdmFormGroup.controls.allegedmaltreator
        );
        // if (this.allegedMaltreator.length === 1) {
        //     this.sdmFormGroup.setControl('allegedmaltreator', this.formBuilder.array([]));
        // }
        this.allegedMaltreator.forEach(x => {
            allegedMaltreatorControl.push(this.buildAllegedMaltreatorForm(x));
        });
        const providerControl = <FormArray>this.sdmFormGroup.controls.provider;
        this.provider.forEach(x => {
            providerControl.push(this.buildProviderForm(x));
        });
    }
    private buildAllegedVictimForm(x): FormGroup {
        return this.formBuilder.group({
            victimname: x.victimname
        });
    }
    private buildAllegedMaltreatorForm(x): FormGroup {
        return this.formBuilder.group({
            maltreatorsname: x.maltreatorsname
        });
    }
    private buildProviderForm(x): FormGroup {
        return this.formBuilder.group({
            providername: x.providername
        });
    }
    createFormGroup(formGroupName) {
        if (formGroupName === 'allegedvictim') {
            return this.formBuilder.group({
                victimname: ['']
            });
        } else if (formGroupName === 'allegedmaltreator') {
            return this.formBuilder.group({
                maltreatorsname: ['']
            });
        } else if (formGroupName === 'provider') {
            return this.formBuilder.group({
                providername: ['']
            });
        }
    }

    addNewFormGroup(formGroupName) {
        const control = <FormArray>this.sdmFormGroup.controls[formGroupName];
        control.push(this.createFormGroup(formGroupName));
    }

    deleteFormGroup(index: number, formGroupName) {
        const control = <FormArray>this.sdmFormGroup.controls[formGroupName];
        control.removeAt(index);
    }

    onChangeMaltreatment(item) {
        // console.log(item);
        if (item === 'yes') {
            this.isDisplayProvider = true;
        } else {
            this.isDisplayProvider = false;
            const control = <FormArray>this.sdmFormGroup.controls['provider'];
            control.controls = [];
            control.push(this.createFormGroup('provider'));
        }
    }
    changeScreenOutIN(item) {
        this.isScreenOutIN = item;
        if (item === 'Scrnin') {
            this.sdmFormGroup.patchValue(
                { cpsResponseType: 'CPS-IR' },
                { emitEvent: false }
            );
        } else if (item === 'ScreenOUT') {
            this.sdmFormGroup.patchValue(
                { cpsResponseType: 'CPS-AR' },
                { emitEvent: false }
            );
        } else {
        }
    }
    changeOverScreenOutIN(item) {
        this.isScreenOutIN = item;
        if (item === 'Ovrscrnin') {
            this.sdmFormGroup.patchValue(
                { cpsResponseType: 'CPS-IR' },
                { emitEvent: false }
            );
        } else if (item === 'OvrScrnout') {
            this.sdmFormGroup.patchValue(
                { cpsResponseType: 'CPS-AR' },
                { emitEvent: false }
            );
        } else {
        }
    }
    changeImmediate(item) {
        this.isImmediate = item;
    }
    changeChildInderOneYear(item) {
        this.isChildInderOneYear = item;
    }

    // changeFinalScreenDecision(item) {
    //     if (item === 'true') {
    //         this.sdmFormGroup.patchValue({ iscps: 'true' }, { emitEvent: false });
    //     } else if (item === 'false') {
    //         this.sdmFormGroup.patchValue({ iscps: 'false' }, { emitEvent: false });
    //     } else {
    //     }
    //     this.sdmInputSubject$.next(this.sdmFormGroup.value);

    // }

    private validateGroup(formGroup: FormGroup) {
        for (const key in formGroup.controls) {
            if (formGroup.controls.hasOwnProperty(key)) {
                const control: FormControl = <FormControl>(
                    formGroup.controls[key]
                );
                if (control.value) {
                    return null;
                }
            }
        }
        return {
            validateGroup: {
                valid: false
            }
        };
    }

    private cpsResponseValidation(sdm) {
        this.populateSdm = Object.assign({}, sdm);
        if (ObjectUtils.checkTrueProperty(this.populateSdm.sexualAbuse) >= 1) {
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                { issexualabuse: true },
                { emitEvent: false }
            );
            this.sdmSettings.issexualabuse = true;
        } else {
            if (this.sdmSettings.issexualabuse) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                    { issexualabuse: false },
                    { emitEvent: false }
                );
            }
            this.sdmSettings.issexualabuse = false;
        }
        if (
            ObjectUtils.checkTrueProperty(this.populateSdm.generalNeglect) >= 1
        ) {
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                { isoutofhome: true },
                { emitEvent: false }
            );
            this.sdmSettings.isoutofhome = true;
        } else {
            if (this.sdmSettings.isoutofhome) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                    { isoutofhome: false },
                    { emitEvent: false }
                );
            }
            this.sdmSettings.isoutofhome = false;
        }
        if (
            this.populateSdm.menab_psycologicalability === true ||
            this.populateSdm.menng_psycologicalability === true
        ) {
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                { isdeathorserious: true },
                { emitEvent: false }
            );
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                { isreportmeets: true },
                { emitEvent: false }
            );
            this.sdmSettings.isdeathorserious = true;
        } else {
            if (this.sdmSettings.isdeathorserious) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                    { isdeathorserious: false },
                    { emitEvent: false }
                );
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                    { isreportmeets: false },
                    { emitEvent: false }
                );
            }
            this.sdmSettings.isdeathorserious = false;
        }
        if (this.populateSdm.negfp_cargiverintervene === true) {
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                { issignordiagonises: true },
                { emitEvent: false }
            );
            this.sdmSettings.issignordiagonises = true;
        } else {
            if (this.sdmSettings.issignordiagonises) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue(
                    { issignordiagonises: false },
                    { emitEvent: false }
                );
            }
            this.sdmSettings.issignordiagonises = false;
        }
    }

    private validateOverrideDays(date) {
        const start_date = moment(date, 'YYYY-MM-DD');
        const end_date = moment(new Date(), 'YYYY-MM-DD');
        const duration = moment.duration(end_date.diff(start_date)).asDays();
        if (duration) {
            this.dayToOverride = 60 - Math.ceil(duration);
            if (this.dayToOverride > 0) {
                this.sdmSettings.isDisableOverride = false;
            } else {
                this.sdmSettings.isDisableOverride = true;
            }
        }
    }
}
