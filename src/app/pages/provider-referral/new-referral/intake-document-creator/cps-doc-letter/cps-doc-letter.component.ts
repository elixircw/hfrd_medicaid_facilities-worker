import { sdmData } from './../../_data/sdm';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { InvolvedPerson, CpsDocInput, ReviewStatus, SDMDescription, Sdm, Narrative } from '../../_entities/newintakeModel';
import { General, EvaluationFields } from '../../_entities/newintakeSaveModel';
import { Subject } from 'rxjs/Subject';
import * as jsPDF from 'jspdf';
import { AuthService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cps-doc-letter',
    templateUrl: './cps-doc-letter.component.html',
    styleUrls: ['./cps-doc-letter.component.scss']
})
export class CpsDocLetterComponent implements OnInit, OnChanges {
    @Input() persons: InvolvedPerson[];
    @Input() general: General;
    @Input() evalFields: EvaluationFields;
    @Input() cpsdocData: CpsDocInput;
    @Input() cpsdocGenAction: Subject<string>;
    @Input() reviewStatus: string;
    @Input() sdmReport$ = new Subject<Sdm>();
    @Input() narrativeOutputSubject$ = new Subject<Narrative>();
    casehead: InvolvedPerson;
    householdMem: InvolvedPerson[] = [];
    nonHouseholdMem: InvolvedPerson[] = [];
    imgname: any;
    currentDate = new Date();
    sdmJsonData: SDMDescription;
    sdmFormData: Sdm;
    narrativeFormData: Narrative;
    roleId: AppUser;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    PAGE_HEIGHT = 920;
    constructor(private _authService: AuthService) { }

    ngOnChanges() {
        // console.log('ONCHANGES persons', this.persons);
        // console.log('general:', this.general);
        this.getCaseHead();
        this.getMembersOfHousehold();

    }

    ngOnInit() {
        this.sdmJsonData = sdmData;
        this.imgname = require('assets/images/draft.png');
        this.roleId = this._authService.getCurrentUser();
        this.cpsdocGenAction.subscribe((data) => {

            if (data === 'generate') {
                if ($('#vir-csp').children().length === 0) {
                    this.createpreview();
                }
            } else if (data === 'download') {
                this.downloadCasePdf('vir-csp');
            }
        });

        this.sdmReport$.subscribe((data) => {
            this.sdmFormData = data;
            // Object.keys(data).forEach(key => {
            //     console.log(this.sdmData[key]);
            // });
        });

        this.narrativeOutputSubject$.subscribe((data) => {
            this.narrativeFormData = data;
            // console.log('narrative-data', this.narrativeFormData);
        });
    }

    sdmKeys(obj) {
        if (obj) {
            const sdmObject = [];
            Object.keys(obj).forEach(key => {
                if (obj[key] === true) {
                    sdmObject.push(key);
                }
            });

            return sdmObject;
        }
    }
    generatePageDiv() {
        const pageDiv = document.createElement('div');
        pageDiv.className = 'pdf-page sizeA4 page-paddings';
        return pageDiv;
    }

    genrateContainerDiv() {
        const container = document.createElement('div');
        container.className = 'pdf-container';
        return container;
    }

    getSections() {
        const list = [
            'record-of-contact',
            'rocommended-response-time',
            'referral-casehead',
            'add-n-contact',
            'referral-narrative',
            'other-mem-of-household',
            'ref-parti-in-household',
            'oth-mem-not-in-household',
            'out-home-maltrtmt',
            'order-of-shelter',
            'law-enf',
            'allg-pert-chld',
            'recomm-n-over',
            'work-approvals',
            'related-cases',
            'source-referral',

        ];
        const containers = [];
        list.forEach(element => {
            if (element === 'ref-parti-in-household') {
                const householdMeminfos = document.getElementsByClassName('householdMeminfo');
                const householdMemaddinfos = document.getElementsByClassName('householdMemaddinfo');
                const householdMemconinfos = document.getElementsByClassName('householdMemconinfo');
                const householdMeminfoheader = document.getElementsByClassName('householdMeminfoheader');

                for (let i = 0; i < this.householdMem.length; i++) {
                    containers.push(householdMeminfos[i]);
                    containers.push(householdMeminfoheader[i]);
                    containers.push(householdMemaddinfos[i]);
                    containers.push(householdMemconinfos[i]);
                }
            } else if (element === 'ref-parti-in-household') {
                const householdMeminfos = document.getElementsByClassName('outhouseholdMeminfo');
                const householdMemaddinfos = document.getElementsByClassName('outhouseholdMemaddinfo');
                const householdMemconinfos = document.getElementsByClassName('outhouseholdMemconinfo');
                const householdMeminfoheader = document.getElementsByClassName('outhouseholdMeminfoheader');

                for (let i = 0; i < this.householdMem.length; i++) {
                    containers.push(householdMeminfos[i]);
                    containers.push(householdMeminfoheader[i]);
                    containers.push(householdMemaddinfos[i]);
                    containers.push(householdMemconinfos[i]);
                }
            } else {
                containers.push(document.getElementById(element));
            }

        });
        return containers;
    }

    createpreview() {
        const virelement = document.getElementById('vir-csp');
        const header = document.getElementById('cps-intake-header');
        const footer = document.getElementById('cps-intake-footer');
        footer.className = 'mt-10 cps-footer';
        let divele = this.generatePageDiv();
        let wraper = this.genrateContainerDiv();
        divele.appendChild(header);
        divele.appendChild(wraper);
        let pageHeight = this.PAGE_HEIGHT;
        this.getSections().forEach(element => {
            const offsetheight = element.offsetHeight;
            if ((pageHeight - offsetheight) > 0) {
                wraper.appendChild(element);
                pageHeight = pageHeight - offsetheight;
            } else {
                pageHeight = this.PAGE_HEIGHT;
                divele.appendChild(footer.cloneNode(true));
                virelement.appendChild(divele);
                divele = this.generatePageDiv();
                wraper = this.genrateContainerDiv();
                divele.appendChild(header.cloneNode(true));
                wraper.appendChild(element);
                pageHeight = pageHeight - offsetheight;
                divele.appendChild(wraper);
            }
        });

        divele.appendChild(footer);
        virelement.appendChild(divele);
        const pages = virelement.getElementsByClassName('page-index');
        for (let i = 0; i < pages.length; i++) {
            pages.item(i).innerHTML = (i + 1) + ' of ' + (pages.length);

        }
        document.getElementById('CPS-Intake-Letter').hidden = true;
    }

    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {

            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
            });
        }
        const pageName = 'pageName';
        this.pdfFiles.push({ fileName: pageName, images: pageImages });
        pageImages = [];
        this.convertImageToPdf();

    }
    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            if (this.reviewStatus !== 'Review' && this.reviewStatus !== 'Accepted') {
                this.addWaterMark(doc);
            }
            doc.save(pdfFile.fileName);
        });
        (<any>$('#intake-complaint-pdf1')).modal('hide');
        this.pdfFiles = [];
        // this.downloadInProgress = false;
    }

    cpsPdfCreator() {
        const element = document.getElementById('CPS-Intake-Letter');
        const header = document.getElementById('cps-intake-header');
        const footer = document.getElementById('cps-intake-footer');

        const pdf = new jsPDF('p', 'pt', 'a4');
        pdf.internal.scaleFactor = 3.75;

        const w = element.clientWidth;
        const h = element.clientHeight;
        const newCanvas = document.createElement('canvas');
        newCanvas.width = w * 2;
        newCanvas.height = h * 2;
        newCanvas.style.width = w + 'px';
        newCanvas.style.height = h + 'px';
        const context = newCanvas.getContext('2d');
        context.scale(2, 2);

        const doc = new jsPDF();

        doc.setFontSize(14);
        const list = [
            'cps-intake-header',
            'cps-intake-footer',
            'record-of-contact',
            'rocommended-response-time',
            'referral-casehead',
            'add-n-contact',
            'referral-narrative',
            'other-mem-of-household',
            'end'
        ];
        // this.downloadCasePdf('file.pdf', 'true', list);
    }



    addWaterMark(doc: jsPDF) {
        const totalPages = doc.internal.getNumberOfPages();

        for (let i = 1; i <= totalPages; i++) {
            doc.setPage(i);
            const canvas = document.createElement('canvas');

            doc.addImage(this.imgname, 'PNG', 0, 0);
        }

        return doc;
    }

    getCaseHead() {
        if (this.persons) {
            this.persons.forEach((element) => {
                if (element.personRole) {
                    const casehead = element.personRole.filter(item => item.rolekey === 'CASEHD');
                    if (casehead.length) {
                        element.displayMultipleRole = element.personRole.map(role => role.description);
                        this.casehead = element;
                    }
                }
            });
        }
        // let father;
        // let mother;
        // if (this.persons) {
        //     this.persons.forEach((element) => {
        //         if (element.RelationshiptoRA === '@') {
        //             mother = element;
        //         } else if (element.RelationshiptoRA === '1') {
        //             father = element;
        //         }
        //     });
        // }
        // if (father) {
        //     this.casehead = father;
        // } else if (mother) {
        //     this.casehead = mother;
        // }
    }

    getMembersOfHousehold() {
        if (this.persons) {
            this.persons.filter(item => item.Role !== 'CASEHD').forEach((element) => {
                if (element.personRole) {
                    const casehead = element.personRole.filter(item => item.rolekey === 'CASEHD');
                    if (!casehead.length) {
                        element.displayMultipleRole = element.personRole.map(role => role.description);
                        if (element.RelationshiptoRA === 'A3') {
                            this.nonHouseholdMem.push(element);
                        } else {
                            this.householdMem.push(element);
                        }
                    }
                }
            });

        }
    }
}
