import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticePreintakeLetterComponent } from './notice-preintake-letter.component';

describe('NoticePreintakeLetterComponent', () => {
  let component: NoticePreintakeLetterComponent;
  let fixture: ComponentFixture<NoticePreintakeLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticePreintakeLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticePreintakeLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
