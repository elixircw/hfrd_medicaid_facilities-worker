import { Component, OnInit, Input } from '@angular/core';

import { InvolvedPerson, DispostionOutput, PreIntakeDisposition } from '../../_entities/newintakeModel';
import { General, EvaluationFields, IntakeAppointment, CourtDetails } from '../../_entities/newintakeSaveModel';
import { Subject } from 'rxjs/Subject';
import * as jsPDF from 'jspdf';
import { AuthService } from '../../../../../@core/services';
import { School } from '../../../../case-worker/_entities/caseworker.data.model';
const APPOINTMENT_COMPLETED = 'Completed';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'notice-preintake-letter',
    templateUrl: './notice-preintake-letter.component.html',
    styleUrls: ['./notice-preintake-letter.component.scss']
})
export class NoticePreintakeLetterComponent implements OnInit {
    // tslint:disable-next-line:no-input-rename

    @Input() persons: InvolvedPerson[];
    @Input() general: General;
    @Input() dispositionOutPut$ = new Subject<DispostionOutput[]>();
    @Input() evalFieldsOutputSubject$ = new Subject<EvaluationFields>();
    @Input() generatedDocuments$ = new Subject<string[]>();
    @Input() preIntakeDisposition: PreIntakeDisposition;
    @Input() offenceCategoriesInputSubject$ = new Subject<any[]>();
    @Input() evalFields: EvaluationFields;
    @Input() preIntakedispositionPost$ = new Subject<PreIntakeDisposition>();
    @Input() appointments: IntakeAppointment[] = [];
    @Input() courtDetails: CourtDetails;
    appointment: IntakeAppointment;
    documentsToDownload: string[] = [];
    supComments = '';
    reason = '';
    comments = '';
    addedPersons: InvolvedPerson[];
    youth: InvolvedPerson;
    fatherObj: InvolvedPerson;
    motherObj: InvolvedPerson;
    guardianObj: InvolvedPerson;
    downloadInProgress: boolean;
    complaintID: string;
    complaintReceiveDate: string;
    selectedAllegedOffenseIDs: any[];
    allegedOffenseDate: string;
    allAllegedOffense: string;
    offenses: any[];
    loggedInUser: string;
    preintakeAppointmentDate: string;
    finalNotificationDate: Date;
    youthName: string;
    youthDob;
    youthPhoneNumber: string;
    youthId: string;
    victimName: string;
    victimAddress: string;
    victimPhoneNumber: string;
    currentDateString;
    mother: { name: string, phoneNumber: string, address: string };
    father: { name: string, phoneNumber: string, address: string };
    guardian: { name: string, phoneNumber: string, address: string };
    appointmentHeld = false;
    appointmentNotes = '';
    offenseString: string;
    parentOrGaurdianAddress: string;
    parentOrGaurdianName: string;
    youthLastSchool: School;

    petitionID: string;
    youthAge: number;

    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    constructor(private _authService: AuthService) { }

    ngOnInit() {
        const currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + 10);
        this.finalNotificationDate = currentDate;
        this.dispositionOutPut$.subscribe(data => {
            if (data && data.length > 0) {
                this.supComments = data[0].supComments;
                this.reason = data[0].reason;
            }
        });
        this.generatedDocuments$.subscribe(data => {
            if (data) {
                this.documentsToDownload = data;
                this.resetInputs();
                this.processInputs();
                const elmnt = document.getElementById('doc-gen');
                elmnt.scrollIntoView();

            }
        }
        );

        this.evalFieldsOutputSubject$.subscribe(data => {
            if (data) {
                this.complaintID = data.complaintid;
                this.complaintReceiveDate = data.complaintreceiveddate;
                // this.selectedAllegedOffenseIDs = data.allegedoffense;
                this.allegedOffenseDate = data.allegedoffensedate;
                // if (this.selectedAllegedOffenseIDs && this.offenses) {
                //     this.generateOffenseString();
                // }
            }
        });

        this.offenceCategoriesInputSubject$.subscribe(offenses => {
            this.offenses = offenses;
            if (this.selectedAllegedOffenseIDs && this.offenses) {
                this.generateOffenseString();
            }
        });

        this.loggedInUser = this._authService.getCurrentUser().user.userprofile.displayname;

        this.preIntakedispositionPost$.subscribe(data => {
            if (data) {
                this.comments = data.comment;
            }
        });
    }

    resetInputs() {
        this.mother = { name: '', address: '', phoneNumber: '' };
        this.father = { name: '', address: '', phoneNumber: '' };
        this.guardian = { name: '', address: '', phoneNumber: '' };
    }

    processInputs() {
        const mother = this.getPersonByRelation('mother');
        const father = this.getPersonByRelation('father');
        const guardian = this.getPersonByRelation('guardian');
        this.currentDateString = this.currentDate();
        const person = this.getPerson('Youth');
        if (person) {
            this.youthName = person.fullName;
            this.youthDob = person.Dob;
            this.youthPhoneNumber = person.primaryPhoneNumber;
            if (person) {
                this.youthId = person.Pid.substr(person.Pid.length - 8).toUpperCase();
            }
        }


        if (mother) {
            this.mother.name = mother.fullName;
            this.mother.address = this.getPersonAddress(mother);
            this.mother.phoneNumber = mother.primaryPhoneNumber;
        }
        if (father) {
            this.father.name = father.fullName;
            this.father.address = this.getPersonAddress(father);
            this.father.phoneNumber = father.primaryPhoneNumber;
        }
        if (guardian) {
            this.guardian.name = guardian.fullName;
            this.guardian.address = this.getPersonAddress(guardian);
            this.guardian.phoneNumber = guardian.primaryPhoneNumber;
        }
        if (this.appointments && this.appointments.length > 0) {
            this.preintakeAppointmentDate = this.appointments[0].appointmentDate;
            this.appointmentHeld = (this.appointments[0].status === APPOINTMENT_COMPLETED);
            this.appointmentNotes = this.appointments[0].notes;

            this.appointment = this.appointments[0];
        }
        const victim = this.getPerson('Victim');
        if (victim) {
            this.victimName = victim.fullName;
            this.victimAddress = this.getPersonAddress(victim);
            this.victimPhoneNumber = victim.primaryPhoneNumber;

        }

        this.offenseString = this.generateOffenseString();
        this.parentOrGaurdianAddress = this.getParentOrGaurdianAddress();
        this.parentOrGaurdianName = this.getParentOrGaurdianName();

        this.youth = this.getPerson('Youth');
        this.youthAge = this.getYouthAge();
        console.log('youth', this.youth);

        this.fatherObj = this.getPersonByRelation('father');
        this.motherObj = this.getPersonByRelation('mother');
        this.guardianObj = this.getPersonByRelation('guardian');

        if (this.youth) {
            const youthSchools = this.youth.school;

            if (youthSchools && youthSchools.length > 0) {
                this.youthLastSchool = youthSchools[youthSchools.length - 1];
            }
        }

    }

    private getYouthAge() {
        if (!this.youth || (this.youth && !this.youth.Dob)) {
            return null;
        }
        let youthDob;
        let offenceDate;
        youthDob = new Date(this.youth.Dob);

        offenceDate = new Date();

        const timeDiff = offenceDate - youthDob;
        const youthAge = new Date(timeDiff); // miliseconds from epoch
        return Math.abs(youthAge.getUTCFullYear() - 1970);

    }

    getPersonAddress(person: any) {
        return `${person.personAddressInput[0] ? `${person.personAddressInput[0].address1}, ${person.personAddressInput[0].Address2}<br>
                    ${person.personAddressInput[0].city}, ${person.personAddressInput[0].county}<br>
                    ${person.personAddressInput[0].zipcode}` : `<br><br><br>`}`;
    }


    generateOffenseString(): string {
        if (this.evalFields && this.evalFields.allegedoffense && this.offenses) {
            const selectedIDs = this.evalFields.allegedoffense.map(offense => offense.allegationid);

            return this.offenses.filter(offense => {
                return selectedIDs.indexOf(offense.allegationid) !== -1;
            }).map(offense => offense.name).toString().replace(',', ', ');
        }

        return '';
    }

    getVictimName() {
        const person = this.getPerson('Victim');
        if (person) {
            return person.fullName;
        }
        return '';
    }

    getPerson(Role: string): InvolvedPerson {
        if (this.persons) {
            return this.persons.find((person) => person.Role === Role);
        }
        return null;
    }

    getPersonByRelation(Relationship: string): InvolvedPerson {
        if (this.persons) {
            return this.persons.find((person) => person.RelationshiptoRA === Relationship);
        }
        return null;
    }

    getParentOrGaurdianName() {
        const father = this.getPersonByRelation('father');
        const mother = this.getPersonByRelation('mother');
        const guardian = this.getPersonByRelation('guardian');

        if (mother) {
            return mother.fullName;
        } else if (father) {
            return father.fullName;
        } else if (guardian) {
            return guardian.fullName;
        }
    }

    getParentOrGaurdianAddress() {
        const father = this.getPersonByRelation('father');
        const mother = this.getPersonByRelation('mother');
        const guardian = this.getPersonByRelation('guardian');

        if (mother) {
            return `${this.getParentOrGaurdianName()}<br>
                    ${mother.personAddressInput[0] ? `${mother.personAddressInput[0].address1}, ${mother.personAddressInput[0].Address2}<br>
                    ${mother.personAddressInput[0].city}, ${mother.personAddressInput[0].county}<br>
                    ${mother.personAddressInput[0].zipcode}` : `<br><br><br>`}`;
        } else if (father) {
            return `${this.getParentOrGaurdianName()}<br>
                    ${father.personAddressInput[0] ? `${father.personAddressInput[0].address1}, ${father.personAddressInput[0].Address2}<br>
                    ${father.personAddressInput[0].city}, ${father.personAddressInput[0].county}<br>
                    ${father.personAddressInput[0].zipcode}` : `<br><br><br>`}`;
        } else if (guardian) {
            return `${this.getParentOrGaurdianName()}<br>
                    ${guardian.personAddressInput[0] ? `${guardian.personAddressInput[0].address1}, ${guardian.personAddressInput[0].Address2}<br>
                    ${guardian.personAddressInput[0].city}, ${guardian.personAddressInput[0].county}<br>
                    ${guardian.personAddressInput[0].zipcode}` : `<br><br><br>`}`;
        }
    }

    getYouthName() {
        const person = this.getPerson('Youth');
        if (person) {
            return person.fullName;
        }
        return '';
    }
    getYouth(key) {
        const person = this.getPerson('Youth');
        let result = '';
        if (person) {
            result = person[key] ? person[key] : '-';
        }
        return result;
    }

    getVictimAddress() {
        const person = this.getPerson('Alleged Victim');
        if (person) {
            return person.fullAddress;
        }
        return '';
    }

    getMaltreatorName() {
        const person = this.getPerson('Alleged Maltreator');
        if (person) {
            return person.fullName;
        }
        return '';
    }

    getMaltreatorAddress() {
        const person = this.getPerson('Alleged Maltreator');
        if (person) {
            return person.fullAddress;
        }
        return '';
    }

    getPersonID(role) {
        const person = this.getPerson(role);
        if (person && person.Pid) {
            return person.Pid.substr(person.Pid.length - 8).toUpperCase();
        }
        return '';
    }

    appointmentDate() {
        const RecivedDate = new Date(this.general.RecivedDate);
        return RecivedDate.setDate(RecivedDate.getDate() + 7);
    }

    currentDate() {
        return new Date();
    }
    collectivePdfCreator() {
        this.downloadInProgress = true;
        const pdfList = this.documentsToDownload;
        pdfList.forEach((element) => {
            this.downloadCasePdf(element);
        });
    }
    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            // console.log(pages.item(i).getAttribute('data-page-name'));
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
            });
        }
        this.convertImageToPdf();
    }

    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#docu-View')).modal('hide');
        this.pdfFiles = [];
        this.downloadInProgress = false;
    }

    isNotSelected(key) {
        let toHide = true;
        if (this.documentsToDownload) {
            toHide = this.documentsToDownload.indexOf(key) === -1;
        }
        return toHide;
    }

}
