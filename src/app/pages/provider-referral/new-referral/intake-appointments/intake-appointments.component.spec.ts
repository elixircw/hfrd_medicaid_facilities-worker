import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeAppointmentsComponent } from './intake-appointments.component';

describe('IntakeAppointmentsComponent', () => {
  let component: IntakeAppointmentsComponent;
  let fixture: ComponentFixture<IntakeAppointmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeAppointmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeAppointmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
