import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs/Rx';
import { AuthService, CommonHttpService, AlertService } from '../../../../@core/services';
import { InvolvedPerson } from '../_entities/newintakeModel';
import { IntakeAppointment, MembersInMeeting } from '../_entities/newintakeSaveModel';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { RoutingUser } from '../../../cjams-dashboard/_entities/dashBoard-datamodel';
import { NewUrlConfig } from '../../provider-referral-url.config';
import * as moment from 'moment';
import { RouteDA } from '../_entities/newintakeModel';
import { AppConstants } from '../../../../@core/common/constants';
declare var $: any;
const APPOINTMENT_SCHEDULED = 'Scheduled';
const APPOINTMENT_COMPLETED = 'Completed';
const APPOINTMENT_RESCHEDULED = 'Rescheduled';
const DEFAULT_APPOINTMENT_TITLE = 'Intake interview';
const SCREENING_WORKER = 'SCRNW';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-appointments',
    templateUrl: './intake-appointments.component.html',
    styleUrls: ['./intake-appointments.component.scss']
})
export class IntakeAppointmentsComponent implements OnInit {
    // tslint:disable-next-line:no-input-rename
    @Input('appointmentInputSubject') appointmentInputSubject$ = new Subject<IntakeAppointment[]>();
    // tslint:disable-next-line:no-input-rename
    @Input('appointmentOutputSubject') appointmentOutputSubject$ = new Subject<IntakeAppointment[]>();
    // tslint:disable-next-line:no-input-rename
    @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
    // tslint:disable-next-line:no-input-rename
    @Input('preIntakeSupDicision') preIntakeSupDicision$ = new Subject<string>();

    addedPersons: InvolvedPerson[] = [];
    appointmentForm: FormGroup;
    currentUser: AppUser;
    intakeWorkers$: Observable<RoutingUser[]>;
    intakeWorkerList: RoutingUser[] = [];
    times: any[] = [];
    appointments: IntakeAppointment[] = [];
    isEditAppointment = false;
    isViewAppointment = false;
    isNotesRequired = false;
    actionText = 'Create';
    titleText = 'Create';
    appointmentInAction: IntakeAppointment;
    maxDate = new Date();
    minDate = new Date();
    relations: any = [];
    roles: any = [];
    completionNotes = '';
    isYouthSelectedforAppmnt: boolean;
    isParentSelectedforAppmnt: number;
    appointmentHistoryObj: IntakeAppointment[];
    notes = 'Notes...';
    readOnly = false;


    constructor(private formBuilder: FormBuilder, private _authService: AuthService,
        private _commonHttpService: CommonHttpService, private _alertService: AlertService) {
    }

    ngOnInit() {
        this.loadIntakeWorkers();
        // console.log(this._authService.getCurrentUser());
        this.currentUser = this._authService.getCurrentUser();
        this.initializeAppointmentForm();
        this.setFormValidity();
        this.isYouthSelectedforAppmnt = false;
        this.isParentSelectedforAppmnt = 0;
        this.times = this.generateTimeList();
        this.maxDate.setDate(new Date().getDate() + 10);
        this.appointmentOutputSubject$.subscribe((data) => {
            if (data) {
                this.appointments = data;
            } else {
                this.appointments = [];
            }

        });
        this.addedPersonsChanges$.subscribe((data) => {
            this.addedPersons = data ? data : [];
            // Need to validate if persons changed related with appointment
            this.addedPersons.forEach((person, index) => {
                if (!person.Pid) {
                    person.Pid = AppConstants.PERSON.TEMP_ID + index;
                }
            });
            console.log(this.addedPersons);

        });
        this.preIntakeSupDicision$.subscribe(data => {
            if (data !== 'Approved' && data !== 'Rejected') {
                this.appointments.forEach(item => {
                    item.intakeWorkerId = data;
                });
            }
        });
        this.loadMapList();
    }

    broadCastAppointmentUpdated() {
        this.appointmentInputSubject$.next(this.appointments);
    }

    private loadMapList() {
        Observable.forkJoin([this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true,
                order: 'description'
            },
            NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true
            },
            NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter'
        )]).map(([relations, roles]) => {

            this.relations = relations;
            this.roles = roles;
            // console.log(this.relations, this.roles);

        }).subscribe();
    }

    private initializeAppointmentForm() {
        this.appointmentForm = this.formBuilder.group({
            id: [''],
            title: [''],
            appointmentDate: [''],
            appointmentTime: [''],
            intakeWorkerId: [''],
            actors: [[]],
            notes: ['']

        });
    }

    private setFormValidity() {

        if (this.isEditAppointment && !this.isScreeningWorker()) {
            this.isNotesRequired = true;
        } else {
            this.isNotesRequired = false;
        }
    }

    private setDefaultValues() {
        if (this.currentUser.role.name === SCREENING_WORKER) {
            this.appointmentForm.patchValue({ title: DEFAULT_APPOINTMENT_TITLE });
            this.appointmentForm.get('title').disable();
        }

    }

    private loadIntakeWorkers() {

        this.intakeWorkers$ = this._commonHttpService.getPagedArrayList(
            {

                method: 'get',
                nolimit: true,
                page: 1,
                limit: 50,
                order: 'username'
            },
            NewUrlConfig.EndPoint.Intake.intakeWorkerList + '?filter'
        ).map(response => {
            this.intakeWorkerList = response.data;
            return response.data;
        });


    }

    isDateTimeChanged(appointment) {
        if (appointment.appointmentDate === this.appointmentInAction.appointmentDate && appointment.appointmentTime === this.appointmentInAction.appointmentDate) {
            return false;
        }

        return true;
    }

    updateRescheduledAppointmentWithHistory(rescheduledAppointment) {
        const appointmentIndex = this.appointments.findIndex(appointment => rescheduledAppointment.id === appointment.id);
        const histories = [...this.appointmentInAction.history];
        this.appointmentInAction.history = [];
        histories.push(this.appointmentInAction);
        rescheduledAppointment.history = [...histories];
        this.appointments[appointmentIndex] = rescheduledAppointment;
        // console.log('updated', this.appointments);
    }

    updateScheduledAppointment(scheduledAppointment) {
        const appointmentIndex = this.appointments.findIndex(appointment => scheduledAppointment.id === appointment.id);
        this.appointments[appointmentIndex] = scheduledAppointment;
        // console.log('updated', this.appointments);
    }

    involvedPersonChanged(person) {
        // RelationshiptoRA
        // Role
        console.log(person);
        const AppointmetPersons = this.appointmentForm.getRawValue().actors;
        if (person.Role === 'Youth') {
            const youthindex = AppointmetPersons.findIndex(actor => actor === person.Pid);
            if (youthindex !== -1) {
                this.isYouthSelectedforAppmnt = true;
            } else {
                this.isYouthSelectedforAppmnt = false;
            }
        } else if (person.RelationshiptoRA === 'father' || person.RelationshiptoRA === 'mother' || person.RelationshiptoRA === 'guardian') {
            const patentindex = AppointmetPersons.findIndex(actor => actor === person.Pid);
            if (patentindex !== -1) {
                this.isParentSelectedforAppmnt += 1;
            } else {
                this.isParentSelectedforAppmnt -= 1;
            }
        }
    }
    createOrUpdateAppointment() {
        if (this.appointmentForm.valid) {
            const appointmentForm = this.appointmentForm.getRawValue();
            const selectedActors = appointmentForm.actors;
            selectedActors.forEach(selectedactor => {
                const youthindex = this.addedPersons.find(actor => actor.Pid === selectedactor);
                this.involvedPersonChanged(youthindex);
            });
            if (this.isYouthSelectedforAppmnt && this.isParentSelectedforAppmnt <= 0) {
                this._alertService.error('Please Choose Parent / Guardian for Youth');
            } else {
                if (this.isEditAppointment) {
                    const editAppointmentObject = this.appointments.find(appointment => appointment.id === this.appointmentInAction.id);

                    if (this.isDateTimeChanged(editAppointmentObject) && !this.isScreeningWorker()) {
                        const rescheduledAppointment = this.createAppointmentObject(APPOINTMENT_RESCHEDULED);
                        rescheduledAppointment.id = this.appointmentInAction.id;
                        this.updateRescheduledAppointmentWithHistory(rescheduledAppointment);
                    } else {
                        const scheduledAppointment = this.createAppointmentObject(APPOINTMENT_SCHEDULED);
                        scheduledAppointment.id = this.appointmentInAction.id;
                        this.updateScheduledAppointment(scheduledAppointment);
                    }
                } else {
                    const newAppointment = this.createAppointmentObject(APPOINTMENT_SCHEDULED);
                    newAppointment.id = this.generateAppointmentID();
                    newAppointment.history = [];
                    this.appointments.push(newAppointment);
                }
                this.updateIntakeWorker(appointmentForm.intakeWorkerId);
                this.resetForm();

                this.broadCastAppointmentUpdated();
                (<any>$('#intake-appointment')).modal('hide');
            }
        } else {
            this._alertService.error('Please fill the required fields');
        }
    }

    createAppointmentObject(status) {

        const actors = this.createActors();
        const appointmentObject = new IntakeAppointment();
        const appointmentForm = this.appointmentForm.getRawValue();
        appointmentObject.title = appointmentForm.title;
        appointmentObject.status = status;
        appointmentObject.notes = appointmentForm.notes;
        appointmentObject.isChanged = false;
        appointmentObject.intakeWorkerId = appointmentForm.intakeWorkerId;
        appointmentObject.scheduledBy = this.currentUser.user.userprofile.fullname;

        const appointmentDate: any = new Date(appointmentForm.appointmentDate);
        const timeSplit = appointmentForm.appointmentTime.split(':');
        const appointmentTimeHour = timeSplit[0];
        const appointmentTimeMin = timeSplit[1];

        appointmentDate.setHours(appointmentTimeHour);
        appointmentDate.setMinutes(appointmentTimeMin);

        appointmentObject.appointmentDate = appointmentDate;
        appointmentObject.appointmentTime = appointmentForm.appointmentTime;
        appointmentObject.actors = actors;
        return appointmentObject;
    }

    createActors() {
        return this.appointmentForm.value.actors.map(actor => {
            const person = this.getPerson(actor);
            return { 'actorid': actor, 'isoptional': false, firstName: person.Firstname, lastName: person.Lastname};
        });
    }

    isCreateAppointmentVisible() {

        if (!this.isPersonsAdded()) {
            return false;
        }
        if (!this.isScreeningWorker()) {
            return true;
        }
        if (this.isScreeningWorker() && this.appointments.length === 0) {
            return true;
        }

        // otherwise
        return false;

    }

    isAppointmentListVisible() {
        if (this.appointments) {
            return (this.appointments.length > 0);
        } else {
            return false;
        }


    }

    isPersonsAdded() {
        if (this.addedPersons) {
            return this.addedPersons.length > 0;
        }
        return false;
    }

    isScreeningWorker() {
        return (this.currentUser.role.name === SCREENING_WORKER);
    }

    getIntakeWorkerName(intakeWorkerID: string) {
        const intakeWorker = this.intakeWorkerList.find(iw => iw.userid === intakeWorkerID);
        if (intakeWorker) {
            return intakeWorker.username;
        }
        return null;
    }

    getPersonName(personId: string) {
        const person = this.addedPersons.find(p => p.Pid === personId);
        if (person) {
            return person.fullName;
        }
    }

    getPerson(personId: string) {
        const person = this.addedPersons.find(p => p.Pid === personId);
        if (person) {
            return person;
        }
        return null;
    }

    onNewAppointment() {
        this.isEditAppointment = false;
        this.resetForm();
        this.setDefaultValues();
        this.resetActions();
        this.setFormValidity();
        this.chooseIwIfSelecte();
    }

    onEditAppointment(appointment) {
        this.actionText = 'Update';
        this.titleText = 'Edit';
        this.isEditAppointment = true;
        this.appointmentInAction = appointment;
        this.appointmentForm.patchValue(appointment);
        this.appointmentForm.patchValue({ 'actors': appointment.actors.map(actor => actor.actorid) });
        this.setFormValidity();
    }

    resetForm() {
        this.appointmentForm.enable();
        this.appointmentForm.reset();
        this.isEditAppointment = false;
        this.isViewAppointment = false;
    }

    resetActions() {
        this.actionText = 'Create';
        this.titleText = 'Create';
    }

    chooseIwIfSelecte() {
        if (this.appointments && this.appointments.length > 0) {
            this.appointmentForm.patchValue({ 'intakeWorkerId': this.appointments[0].intakeWorkerId });
            this.appointmentForm.get('intakeWorkerId').disable();
        } else {
            this.appointmentForm.get('intakeWorkerId').enable();
        }
    }

    generateAppointmentID() {
        return new Date().getTime();
    }

    confirmDelete(appointment) {
        this.appointmentInAction = appointment;
    }

    deleteAppointmentConfirm() {
        this.appointments = this.appointments.filter(appointment => appointment.id !== this.appointmentInAction.id);
        this.broadCastAppointmentUpdated();
    }

    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = (tt % 60); // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }

    getPersonRole(person) {
        // typedescription,actortype
        const personRole = this.roles.find(role => role.actortype === person.Role);
        if (personRole) {
            return personRole.typedescription;
        }

    }

    getPersonRelation(person) {
        // relationshiptypekey -- description
        const personRelation = this.relations.find(relation => relation.relationshiptypekey === person.RelationshiptoRA);
        if (personRelation) {
            return personRelation.description;
        }
        // console.log(this.relations);
    }



    isCompleteVisible(appointment) {
        return (!this.isScreeningWorker() && appointment.status !== APPOINTMENT_COMPLETED);
    }
    isEditVisible(appointment) {
        return appointment.status !== APPOINTMENT_COMPLETED;
    }

    isHistoryVisible(appointment) {
        if (appointment.history) {
            return (appointment.history.length > 0);
        }
        return false;

    }

    isDeleteVisible(appointment) {
        return appointment.status !== APPOINTMENT_COMPLETED;
    }

    onCompleteAppointment(appointment) {
        this.completionNotes = '';
        this.appointmentInAction = appointment;
    }

    openAppointmentComments(appointment) {
        this.completionNotes = appointment.notes;
        this.readOnly = true;
    }

    appointmentCompleteConfirm() {
        if (this.appointmentInAction) {
            const appointmentIndex = this.appointments.findIndex(appointment => this.appointmentInAction.id === appointment.id);
            if (this.completionNotes !== '') {
                this.appointments[appointmentIndex].notes = this.completionNotes;
                this.appointments[appointmentIndex].status = APPOINTMENT_COMPLETED;
                this.broadCastAppointmentUpdated();
                (<any>$('#complete-appointment-popup')).modal('hide');
            } else {
                this._alertService.error('Completion notes is required');
            }
        }


    }

    updateIntakeWorker(intakeWorkerId) {
        this.appointments.forEach(appointment => {
            appointment.intakeWorkerId = intakeWorkerId;
        });
    }

    showHistory(appointment: IntakeAppointment) {
        // console.log(appointment);
        this.appointmentHistoryObj = appointment.history;
    }

    viewAppointment(appointment) {
        this.appointmentInAction = appointment;
        (<any>$('#appointment-history-pu')).modal('hide');
        this.titleText = 'View';
        this.isViewAppointment = true;
        this.appointmentForm.patchValue(appointment);
        this.appointmentForm.patchValue({ 'actors': appointment.actors.map(actor => actor.actorid) });
        this.appointmentForm.disable();
        this.setFormValidity();
        (<any>$('#intake-appointment')).modal('show');

    }

    openHistory() {
        this.resetForm();
        (<any>$('#intake-appointment')).modal('hide');
        (<any>$('#appointment-history-pu')).modal('show');

    }


}
