import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ProviderReferral, ProviderSearchResponses } from '../_entities/newreferralModel';

@Component({
  selector: 'referral-search',
  templateUrl: './referral-search.component.html',
  styleUrls: ['./referral-search.component.scss']
})
export class ReferralSearchComponent implements OnInit {
  providerSearchForm: FormGroup;
  providerSearchResponses$: Observable<ProviderSearchResponses[]>;
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.providerSearchForm = this._formBuilder.group({
      lastname: [''],
      firstname: [''],
      maidenname: [''],
      gender: [''],
      dob: [''],
      dateofdeath: [''],
      ssn: [''],
      mediasrc: [''],
      mediasrctxt: [''],
      occupation: [''],
      dl: [''],
      stateid: [''],
      address1: [''],
      address2: [''],
      zip: [''],
      city: [''],
      county: [''],
      selectedPerson: [''],
      cjisnumber: [''],
      complaintnumber: [''],
      fein: [''],
      age: [''],
      email: [''],
      phone: [''],
      petitionid: [''],
      alias: [''],
      oldId: ['']
    });
  }

  searchProviders() {
    //this.providerSearchResponses$ = Observable.from({});
    this.providerSearchResponses$ = Observable.from([]);
    this.tabNavigation('searchresult');
  }

  tabNavigation(id) {
    (<any>$('#' + id)).click();
  }

  addNewReferral(){
    (<any>$('#intake-findprovider')).modal('hide');
    (<any>$('#add-referral')).modal('show');
  }

}
