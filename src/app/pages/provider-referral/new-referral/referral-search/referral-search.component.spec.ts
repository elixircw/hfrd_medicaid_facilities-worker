import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralSearchComponent } from './referral-search.component';

describe('ReferralSearchComponent', () => {
  let component: ReferralSearchComponent;
  let fixture: ComponentFixture<ReferralSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
