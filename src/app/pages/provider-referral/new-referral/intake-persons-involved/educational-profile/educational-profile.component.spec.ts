import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationalProfileComponent } from './educational-profile.component';

describe('EducationalProfileComponent', () => {
  let component: EducationalProfileComponent;
  let fixture: ComponentFixture<EducationalProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationalProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationalProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
