import { AfterViewChecked, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { ValidationService } from '../../../../@core/services/validation.service';
import { NewUrlConfig } from '../../provider-referral-url.config';
// tslint:disable-next-line:max-line-length
import { AddressDetails, InvolvedPerson, InvolvedPersonSearch, InvolvedPersonSearchResponse, PersonDsdsAction, PersonRelativeDetails, PersonRole, PriorAuditLog, Work, Health } from '../_entities/newintakeModel';

// import { Address } from '../../../case-worker/dsds-action/involved-person/_entities/involvedperson.data.model';
import { AuthService } from '../../../../@core/services/auth.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { NewReferralConstants } from '../new-referral.constants';
import { DataStoreService } from '../../../../@core/services';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-person-search',
    templateUrl: './intake-person-search.component.html',
    styleUrls: ['./intake-person-search.component.scss']
})
export class IntakePersonSearchComponent implements OnInit, AfterViewChecked {
    selectedPerson: InvolvedPersonSearchResponse;
    @Input()
    intakeNumber: string;
    @Output()
    intakePersonAdd = new EventEmitter();
    @Output()
    newPersonAdd = new EventEmitter();
    roleDropdownItems$: Observable<DropdownModel[]>;
    stateDropdownItems$: Observable<DropdownModel[]>;
    relationShipToRADropdownItems$: Observable<DropdownModel[]>;
    involvedPersonSearchForm: FormGroup;
    personSearchAddForm: FormGroup;
    involvedPersondata = false;
    profileTabActive = false;
    serachResultTabActive = false;
    personRoleTabActive = false;
    showPersonDetail = -1;
    maxDate = new Date();
    involvedPersonSearchResponses$: Observable<InvolvedPersonSearchResponse[]>;
    personDSDSActions$: Observable<PersonDsdsAction[]>;
    personRelations$: Observable<PersonRelativeDetails[]>;
    personAddresses$: Observable<AddressDetails[]>;
    genderDropdownItems$: Observable<DropdownModel[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    profileDetails: InvolvedPersonSearchResponse;
    roleName: string;
    personRole: PersonRole[] = [];
    isDjs = false;
    isDjsYouth: boolean;
    private involvedPersonSearch: InvolvedPersonSearch;
    private priorAuditLogRequest = new PriorAuditLog();
    roleDetails: AppUser;
    @Input()
    healthFormReset$ = new Subject<boolean>();
    @Input()
    workFormReset$ = new Subject<boolean>();
    constructor(
        private _formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private _involvedPersonSeachService: GenericService<InvolvedPersonSearchResponse>,
        private _detect: ChangeDetectorRef,
        private _dataStoreService: DataStoreService
    ) { }

    ngOnInit() {
        this.initiateFormGroup();
        //this.loadDropDown();
        this.roleDetails = this._authService.getCurrentUser();
        const teamTypeKey = this.roleDetails.user.userprofile.teamtypekey;
        if (teamTypeKey === 'DJS') {
            this.isDjs = true;
            this.isDjsYouth = false;
        } else {
            this.isDjsYouth = true;
        }
        // this.getPage(1);
    }
    ngAfterViewChecked() {
        this._detect.detectChanges();
    }

    initiateFormGroup() {
        this.personSearchAddForm = this._formBuilder.group({
            Pid: [''],
            Lastname: [''],
            Firstname: [''],
            middlename: [''],
            Gender: [''],
            Dob: [''],
            ssn: [''],
            Role: [''],
            rolekeyDesc: [''],
            suffix: [''],
            aliasname: [''],
            address1: [''],
            Address2: [''],
            zipcode: [''],
            state: [''],
            city: [''],
            county: [''],
            RelationshiptoRA: [''],
            Dangerousself: ['', [Validators.required]],
            DangerousselfReason: [''],
            Dangerousworker: ['', [Validators.required]],
            DangerousWorkerReason: [''],
            mentallyimpaired: ['', [Validators.required]],
            mentallyimpairedReason: [''],
            mentalillsign: ['', [Validators.required]],
            mentalillsignReason: [''],
            iscollateralcontact: [false],
            ishousehold: ['', [!this._authService.isDJS() ? Validators.required : Validators.pattern('')]]
        });
        this.personSearchAddForm.addControl('personRole', this._formBuilder.array([this.createFormGroup(false)]));
        this.involvedPersonSearchForm = this._formBuilder.group({
            lastname: [''],
            firstname: [''],
            maidenname: [''],
            gender: [''],
            dob: [''],
            dateofdeath: [''],
            ssn: [''],
            mediasrc: [''],
            mediasrctxt: [''],
            occupation: [''],
            dl: [''],
            stateid: [''],
            address1: [''],
            address2: [''],
            zip: [''],
            city: [''],
            county: [''],
            selectedPerson: [''],
            cjisnumber: [''],
            complaintnumber: [''],
            fein: [''],
            age: [''],
            email: ['', [ValidationService.mailFormat]],
            phone: [''],
            petitionid: [''],
            alias: [''],
            oldId: ['']
        });
    }
    searchPersonDetailsRow(id: number, model: PersonDsdsAction) {
        this.searchPersonDetails(id);
        this.getPersonDSDSAction(model);
    }
    searchPersonDetails(id: number) {
        if (this.showPersonDetail !== id) {
            this.showPersonDetail = id;
        } else {
            this.showPersonDetail = -1;
        }
    }
    private loadDropDown() {
        const actortypeUrl = this.isDjs ? NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter' : NewUrlConfig.EndPoint.Intake.UserActorTypeUrl + '?filter';
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.GenderTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                actortypeUrl
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true,
                    order: 'description'
                },
                NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    ethinicities: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.ethnicgrouptypekey
                            })
                    ),
                    genders: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gendertypekey
                            })
                    ),
                    roles: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.actortype
                            })
                    ),
                    relationShipToRAs: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.relationshiptypekey
                            })
                    ),
                    states: result[4].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    )
                };
            })
            .share();
        this.genderDropdownItems$ = source.pluck('genders');
        this.roleDropdownItems$ = source.pluck('roles');
        this.relationShipToRADropdownItems$ = source.pluck('relationShipToRAs');
        this.stateDropdownItems$ = source.pluck('states');
    }
    searchInvolvedPersons(model: InvolvedPersonSearch) {
        this.showPersonDetail = -1;
        this.selectedPerson = Object.assign({}, new InvolvedPersonSearchResponse());
        // console.log('Date value' + $('#dobirth').val().length);
        if ($('#dobirth').val().length > 4) {
            if (model.dob) {
                const event = new Date(model.dob);
                model.dob = event.getMonth() + 1 + '/' + event.getDate() + '/' + event.getFullYear();
            }
        } else {
            model.dob = $('#dobirth').val();
        }
        this.involvedPersondata = true;
        this.involvedPersonSearch = Object.assign(new InvolvedPersonSearch(), model);
        this.involvedPersonSearch.intakeNumber = this.intakeNumber;
        if (this.involvedPersonSearchForm.value.address1) {
            this.involvedPersonSearch.address = this.involvedPersonSearchForm.value.address1 + '' + this.involvedPersonSearchForm.value.address2;
        }
        this.involvedPersonSearch.stateid = this.involvedPersonSearchForm.value.dl;
        this.getPage(1);
        this.tabNavigation('searchresult');
        this.profileTabActive = true;
        this.personRoleTabActive = false;
        const roleDetails = this._authService.getCurrentUser();
        this.roleName = roleDetails.role.name;
    }
    private getPage(pageNumber: number) {
        ObjectUtils.removeEmptyProperties(this.involvedPersonSearch);
        const source = this._involvedPersonSeachService
            .getPagedArrayList(
                {
                    limit: this.paginationInfo.pageSize,
                    order: this.paginationInfo.sortBy,
                    page: pageNumber,
                    count: this.paginationInfo.total,
                    where: this.involvedPersonSearch,
                    method: 'post'
                },
                NewUrlConfig.EndPoint.Intake.GlobalPersonSearchUrl
            )
            .map((result) => {
                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.involvedPersonSearchResponses$ = source.pluck('data');
        if (pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    clearPersonSearch() {
        this.involvedPersonSearchForm.reset();
        this.involvedPersonSearchForm.patchValue({
            occupation: '',
            gender: '',
            mediasrctxt: '',
            stateid: ''
        });
        this.personSearchAddForm.reset();
        this.personSearchAddForm.patchValue({
            Role: '',
            RelationshiptoRA: ''
        });
    }

    getPersonDSDSAction(model: PersonDsdsAction) {
        const url = NewUrlConfig.EndPoint.Intake.IntakeServiceRequestsUrl + `?personid=` + model.personid + '&filter';
        const source = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    method: 'get',
                    where: { intakerequestid: null }
                }),
                url
            )
            .share();
        this.personDSDSActions$ = source.pluck('data');
        this.personDSDSActions$
            .map((data) => {
                data.map((address) => {
                    address.daDetails.map((addressdata) => {
                        if (addressdata.dasubtype === 'Peace Order') {
                            address.highLight = true;
                            return address;
                        }
                    });
                });
                return data;
            })
            .subscribe((finalValue) => console.log(finalValue));
    }

    getPersonRelations(personid) {
        const url = NewUrlConfig.EndPoint.Intake.PersonRelativeUrl + '/list?filter';
        this.personRelations$ = this._commonHttpService.getArrayList(
            new PaginationRequest({
                method: 'get',
                where: { personid: personid }
            }),
            url
        );
    }
    // getPersonAddress() {
    //     const url = NewUrlConfig.EndPoint.Intake.PersonAddressesUrl + '?filter';
    //     this.personAddresses$ = this._commonHttpService.getArrayList(
    //         new PaginationRequest({
    //             method: 'get',
    //             where: { personid: this.selectedPerson.personid },
    //             include: { relation: 'Personaddresstype', scope: { fields: ['typedescription'] } }
    //         }),
    //         url
    //     );
    // }
    selectPerson(row) {
        this.selectedPerson = row;
        this.serachResultTabActive = true;
    }
    addSearchPerson() {
        if (this.selectedPerson.lastname) {
            this.personSearchAddForm.patchValue({
                Lastname: this.selectedPerson.lastname,
                Firstname: this.selectedPerson.firstname,
                middlename: this.selectedPerson.middlename,
                Gender: this.selectedPerson.gendertypekey,
                Dob: this.selectedPerson.dob,
                ssn: this.selectedPerson.ssn,
                Pid: this.selectedPerson.personid,
                suffix: this.selectedPerson.suffix,
                aliasname: this.selectedPerson.alias
            });
            if (this.selectedPerson.primaryaddress) {
                this.personSearchAddForm.patchValue({
                    address1: this.selectedPerson.primaryaddress.split('~')[0] ? this.selectedPerson.primaryaddress.split('~')[0] : '',
                    address2: this.selectedPerson.primaryaddress.split('~')[1] ? this.selectedPerson.primaryaddress.split('~')[1] : '',
                    zipcode: this.selectedPerson.primaryaddress.split('~')[3] ? this.selectedPerson.primaryaddress.split('~')[3] : '',
                    state: this.selectedPerson.primaryaddress.split('~')[5] ? this.selectedPerson.primaryaddress.split('~')[5] : '',
                    city: this.selectedPerson.primaryaddress.split('~')[2] ? this.selectedPerson.primaryaddress.split('~')[2] : '',
                    county: this.selectedPerson.primaryaddress.split('~')[4] ? this.selectedPerson.primaryaddress.split('~')[4] : ''
                });
            }
            // console.log(this.selectedPerson);
            this.tabNavigation('reporterrole');
        } else {
            this._alertService.warn('Please select a person');
        }
    }
    addPerson(involvedPerson: InvolvedPerson) {
        // this.personSearchAddForm.valid &&
        if (this.personSearchAddForm.valid && this.personSearchAddForm.dirty) {
            const url = NewUrlConfig.EndPoint.Intake.PersonAddressesUrl + '?filter';
            this._commonHttpService
                .getArrayList(
                    new PaginationRequest({
                        method: 'get',
                        where: { personid: this.selectedPerson.personid },
                        include: { relation: 'Personaddresstype', scope: { fields: ['typedescription'] } }
                    }),
                    url
                )
                .subscribe((result) => {
                    if (result && result.length !== 0) {
                        result.map((item) => {
                            item.addresstype = item.personaddresstypekey;
                            item.addresstypeLabel = item.Personaddresstype ? item.Personaddresstype.typedescription : '';
                            item.address1 = item.address ? item.address : '';
                            item.Address2 = item.address2 ? item.address2 : '';
                            item.zipcode = item.zipcode ? item.zipcode : '';
                            item.state = item.state ? item.state : '';
                            item.city = item.city ? item.city : '';
                            item.county = item.country ? item.country : '';
                            item.addressid = item.personaddressid;
                        });
                        involvedPerson.personAddressInput = result;
                    } else {
                        involvedPerson.personAddressInput = [];
                    }
                    involvedPerson.personRole.map((role) => {
                        if (role.isprimary === 'true') {
                            involvedPerson.Role = role.rolekey;
                            involvedPerson.rolekeyDesc = role.description;
                        }
                        if (role.relationshiptorakey) {
                            involvedPerson.RelationshiptoRA = role.relationshiptorakey;
                        }
                    });
                    involvedPerson.Pid = this.selectedPerson.personid;
                    involvedPerson.userPhoto = this.selectedPerson.userphoto;
                    if (this.validatePrimaryRole()) {
                        this.intakePersonAdd.emit(involvedPerson);
                        this.clearSearchDetails();
                    } else {
                        this._alertService.error('Please Select One Primary Role Type');
                    }
                });
        } else {
            ControlUtils.validateAllFormFields(this.personSearchAddForm);
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill mandatory fields');
        }
    }
    addNewPerson() {
        this.newPersonAdd.emit();
        this.clearSearchDetails();
        (<any>$('#intake-findperson')).modal('hide');
        this._dataStoreService.setData(NewReferralConstants.Intake.PersonsInvolved.Health.Health, new Health());
        this._dataStoreService.setData(NewReferralConstants.Intake.PersonsInvolved.Work.Work, new Work());
        this.healthFormReset$.next(true);
        this.workFormReset$.next(true);
    }
    clearSearchDetails() {
        this.personSearchAddForm.setControl('personRole', this._formBuilder.array([]));
        this.addNewRole();
        this.involvedPersonSearchResponses$ = Observable.empty();
        this.tabNavigation('profileinfo');
        (<any>$('#intake-findperson')).modal('hide');
        this.clearPersonSearch();
        this.personRoleTabActive = false;
        this.serachResultTabActive = false;
        this.profileTabActive = false;
    }

    viewProfileDetails(vieeDetails: InvolvedPersonSearchResponse) {
        (<any>$('#info-details')).modal('show');
        this.profileDetails = vieeDetails;
    }
    closeInformation() {
        (<any>$('#info-details')).modal('hide');
    }
    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.getPage(this.paginationInfo.pageNumber);
    }
    disabledInput(state: boolean, inputfield: string, manditory: string) {
        if (state === false && manditory === 'isManditory') {
            this.personSearchAddForm.get(inputfield).enable();
            if (inputfield === 'DangerousselfReason') {
                this.personSearchAddForm.get('Dangerousself').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.personSearchAddForm.get('DangerousselfReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('DangerousselfReason').updateValueAndValidity();
                    } else {
                        this.personSearchAddForm.get('DangerousselfReason').clearValidators();
                        this.personSearchAddForm.get('DangerousselfReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'DangerousWorkerReason') {
                this.personSearchAddForm.get('Dangerousworker').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.personSearchAddForm.get('DangerousWorkerReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('DangerousWorkerReason').updateValueAndValidity();
                    } else {
                        this.personSearchAddForm.get('DangerousWorkerReason').clearValidators();
                        this.personSearchAddForm.get('DangerousWorkerReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'mentallyimpairedReason') {
                this.personSearchAddForm.get('mentallyimpaired').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.personSearchAddForm.get('mentallyimpairedReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('mentallyimpairedReason').updateValueAndValidity();
                    } else {
                        this.personSearchAddForm.get('mentallyimpairedReason').clearValidators();
                        this.personSearchAddForm.get('mentallyimpairedReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'mentalillsignReason') {
                this.personSearchAddForm.get('mentalillsign').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.personSearchAddForm.get('mentalillsignReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('mentalillsignReason').updateValueAndValidity();
                    } else {
                        this.personSearchAddForm.get('mentalillsignReason').clearValidators();
                        this.personSearchAddForm.get('mentalillsignReason').updateValueAndValidity();
                    }
                });
            }
        }
        if (state) {
            this.personSearchAddForm.get(inputfield).disable();
        } else if (!state && manditory === 'notManditory') {
            this.personSearchAddForm.get(inputfield).enable();
        }
    }
    tabNavigation(id) {
        (<any>$('#' + id)).click();
        if (id === 'searchresult') {
            this.personRoleTabActive = true;
        }
        if (id === 'profileinfo') {
            this.paginationInfo.pageNumber = 1;
        }
    }

    relationShipToRO(event: any, i: number) {
        if (this.isDjs) {
            this.personsRoleSelected(event, i);
        } else {
            const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
            const _personRole = personRoleArray.controls[i];
            _personRole.patchValue({ description: event.source.triggerValue });

            if (event.value !== 'RA' && event.value !== 'RC' && event.value !== 'CLI') {
                const raCount = personRoleArray.controls.filter((res) => res.value.hidden === false);
                if (raCount.length === 0) {
                    _personRole.patchValue({ hidden: false });
                    _personRole.get('relationshiptorakey').setValidators([Validators.required]);
                    _personRole.get('relationshiptorakey').updateValueAndValidity();
                } else {
                    _personRole.patchValue({ hidden: true });
                    _personRole.get('relationshiptorakey').clearValidators();
                    _personRole.get('relationshiptorakey').updateValueAndValidity();
                }
            } else {
                _personRole.patchValue({ hidden: true });
                _personRole.get('relationshiptorakey').clearValidators();
                _personRole.get('relationshiptorakey').updateValueAndValidity();
            }
        }
    }
    personsRoleSelected(event: any, i: number) {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls[i];
        _personRole.patchValue({ isprimary: 'true' });
        if (event.value !== 'RA' && event.value !== 'RC' && event.value !== 'CLI' && event.value !== 'Youth') {
            _personRole.patchValue({ hidden: false });
            _personRole.get('relationshiptorakey').setValidators([Validators.required]);
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        } else {
            _personRole.patchValue({ hidden: true });
            _personRole.get('relationshiptorakey').clearValidators();
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        }

      
        if (event.value !== 'Youth') {
            this.isDjsYouth = false;
        } else {
            this.isDjsYouth = true;
        }

        //D-07265 Start
        if (event.value !== 'Youth' || event.value === 'Youth') {
                this.personSearchAddForm.get('Dangerousself').clearValidators();
                this.personSearchAddForm.get('Dangerousself').updateValueAndValidity();
                this.personSearchAddForm.get('Dangerousworker').clearValidators();
                this.personSearchAddForm.get('Dangerousworker').updateValueAndValidity();
                this.personSearchAddForm.get('mentallyimpaired').clearValidators();
                this.personSearchAddForm.get('mentallyimpaired').updateValueAndValidity();
                this.personSearchAddForm.get('mentalillsign').clearValidators();
                this.personSearchAddForm.get('mentalillsign').updateValueAndValidity();
            } 
        //D-07265 End

    }
    priorAuditLog(item: PriorAuditLog) {
        window.open('#/pages/case-worker/' + item.intakeserviceid + '/' + item.danumber + '/dsds-action/report-summary', '_blank');
        const url = NewUrlConfig.EndPoint.Intake.PriorAuditLogUrl;
        this.priorAuditLogRequest = Object.assign({}, item);
        this.priorAuditLogRequest.priordanumber = item.danumber;
        this.priorAuditLogRequest.danumber = this.intakeNumber;
        const obj = this.priorAuditLogRequest;
        this._commonHttpService
            .getArrayList(
                {
                    method: 'post',
                    obj
                },
                url
            )
            .subscribe();
    }
    addNewRole() {
        const control = <FormArray>this.personSearchAddForm.controls['personRole'];
        control.push(this.createFormGroup(true));
    }
    deleteRole(index: number) {
        const control = <FormArray>this.personSearchAddForm.controls['personRole'];
        control.removeAt(index);
    }
    createFormGroup(isHideRA) {
        return this._formBuilder.group({
            rolekey: ['', Validators.required],
            description: [''],
            isprimary: ['', Validators.required],
            relationshiptorakey: [null],
            hidden: [isHideRA]
        });
    }
    setFormValues() {
        this.personSearchAddForm.setControl('personRole', this._formBuilder.array([]));
        const control = <FormArray>this.personSearchAddForm.controls.personRole;
        this.personRole.forEach((x) => {
            control.push(this.buildPersonRoleForm(x));
        });
    }
    private buildPersonRoleForm(x): FormGroup {
        return this._formBuilder.group({
            rolekey: x.rolekey,
            isprimary: x.isprimary,
            relationshiptorakey: x.relationshiptorakey,
            hidden: x.hidden
        });
    }
    checkPrimaryRole(i) {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach((x) => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        if (primaryCount > 1) {
            this._alertService.error('Primary role for this person is already selected');
            _personRole[i].patchValue({ isprimary: 'false' });
        }
    }
    validatePrimaryRole() {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach((x) => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        // if (primaryCount === 1) {
        //     return true;
        // } else {
        //     return false;
        // }
        //quick fix on primary rle check, this will be fixed when person profile redeisnged
        return true;
    }
}
