import { HttpHeaders } from '@angular/common/http';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxfUploaderService } from 'ngxf-uploader';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { CheckboxModel, DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { ValidationService, DataStoreService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { AppConfig } from '../../../../app.config';
import { Provider, InvolvedPerson } from '../_entities/newintakeModel';
import { NewUrlConfig } from '../../provider-referral-url.config';

declare var $: any;
@Component({
    selector: 'intake-persons-involved',
    templateUrl: './intake-persons-involved.component.html',
    styleUrls: ['./intake-persons-involved.component.scss']
})

export class IntakePersonsInvolvedComponent implements OnInit, AfterViewInit, AfterViewChecked {
    @Input('intakeCommunication')
    referrels: any;
    @Input()
    intakeNumberNarrative: string;
    @Input()
    reviewstatus: string;
    @Input()
    addedPersons: InvolvedPerson[] = [];
    @Input()
    personsList$ = new Subject<InvolvedPerson[]>();
    @Input('changePersonEvent')
    changePersonEvent$ = new Subject<InvolvedPerson[]>();
    @ViewChild('Dangerousself')
    Dangerousself: any;
    
    addedProviders = [];
    maxDate = new Date();
    
    providerFormGroup: FormGroup;
    private provider: Provider = new Provider();
    addEditLabel='';

    // referrels = [];
    programs = [];
    programTypes = [];

    constructor(
        private _formBuilder: FormBuilder,
        // private _personManageService: GenericService<PersonInvolved>,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _uploadService: NgxfUploaderService,
        private _detect: ChangeDetectorRef,
        private _dataStoreService: DataStoreService
    ) { }

    ngOnInit() {

        this.initiateFormGroup();
        this.loadDropDown();

            this.programs = [
                    {value: '1', viewValue: 'RCC'},
                    {value: '2', viewValue: 'CAP'}
                ]


    }
    ngAfterViewInit() {
    }
    ngAfterViewChecked() {
     
    }


    editProvider(modal, no, text) {
        modal.index = no;
        console.log(modal.index+"--editProvider--"+no);
        (<any>$('#intake-addperson')).modal('show');
        (<any>$('#profile-click')).click();

        this.providerFormGroup.patchValue({
        index:modal.index,
            providerName:modal.providerName,
            Lastname: modal.Lastname,
            Firstname: modal.Firstname,
            communicationType: modal.communicationType, 
            emailId:modal.emailId,
            phoneNumber:modal.phoneNumber,   
            TaxID:modal.TaxID,
            program:modal.program,
            programType:modal.programType,
            parentTaxId:modal.parentTaxId,
            parentEntityName:modal.parentEntityName,
            entityName:modal.entityName,
            taxId:modal.taxId,
            programName:modal.programName,   
            isSON:modal.isSON,
            isRFP:modal.isRFP,
            isFEIN:modal.isFEIN,
            country:modal.country,
            zipCode:modal.zipCode,
            streetNo:modal.streetNo,
        streetName:modal.streetName,
    cityName:modal.CityName,
    stateName:modal.StateName,
    countryCode:modal.CountryCode
            });
    }

    initiateFormGroup() {
       
    this.providerFormGroup = this._formBuilder.group({
            index:null,
            providerName:[''],
            Lastname: [''],
            Firstname: [''],
            communicationType: [''],
            emailId: [''],
            phoneNumber:[''],   
            TaxID:[''], 
            program:[''],    
            programType:[''],                
            parentTaxId:[''],    
            taxId:[''],    
            programName:[''], 
            isSON:[''], 
            isRFP:[''], 
            isFEIN:[''],
            entityName:[''],
            parentEntityName:[''],
            country:[''],
            zipCode:[''],
    streetNo:[''],
    streetName:[''],
    cityName:[''],
    stateName:[''],
    countryCode:['']
            });
    }



    private loadDropDown() {
        // const actortypeUrl = this.isDjs ? NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter' : NewUrlConfig.EndPoint.Intake.UserActorTypeUrl + '?filter';
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.GenderTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.LivingArrangementTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.LanguageTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true,
                    order: 'description'
                },
                NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter'
                
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.MaritalStatusUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.ReligionTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    activeflag: 1,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    activeflag: 1
                },
                NewUrlConfig.EndPoint.Intake.AddressTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    filter: {}
                },
                NewUrlConfig.EndPoint.Intake.PhoneTypeUrl + '?filter'
            ),
            // this._commonHttpService.getArrayList(
            //     {
            //         method: 'get',
            //         nolimit: true,
            //         filter: {}
            //     },
            //     NewUrlConfig.EndPoint.Intake.TaxTypeUrl + '?filter'
            // ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    filter: {}
                },
                NewUrlConfig.EndPoint.Intake.EmailTypeUrl + '?filter'
            
                )
        
        ])
            .map((result) => {
                return {
                    ethinicities: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.ethnicgrouptypekey
                            })
                    ),
                    genders: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gendertypekey
                            })
                    ),
                    livingArrangements: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.livingarrangementtypekey
                            })
                    ),
                    primaryLanguages: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.languagetypename,
                                value: res.languagetypeid
                            })
                    ),
                    races: result[4].map(
                        (res) =>
                            new CheckboxModel({
                                text: res.typedescription,
                                value: res.racetypekey,
                                isSelected: false
                            })
                    ),
                    relationShipToRAs: result[5].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.relationshiptypekey
                            })
                    ),
                    roles: result[6].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.actortype
                            })
                    ),
                    states: result[7].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    maritalstatus: result[8].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.maritalstatustypekey
                            })
                    ),
                    religionkey: result[9].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.religiontypekey
                            })
                    ),
                    racetype: result[10].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.racetypekey
                            })
                    ),
                    addresstype: result[11].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personaddresstypekey
                            })
                    ),
                    phonetype: result[12].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personphonetypekey
                            })
                    ),
                    Taxtype: result[13].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personTaxtypekey
                            })
                    ),
                    emailtype: result[14].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personemailtypekey
                            })
                    )

                };
            })
            .share();

    }
   
    addProvider(provider: Provider) {
         if (this.providerFormGroup.valid) {
            console.log(provider.index+"----"+JSON.stringify(provider));

            if(provider.index != null){
                console.log("ifff");
                 this.addedProviders[provider.index]=provider;
            } else {
                console.log("else");

                this.addedProviders.push(provider);
            }

            this.clearProvider();

            (<any>$('#intake-addperson')).modal('hide');
         } else {
            ControlUtils.validateAllFormFields(this.providerFormGroup);
            ControlUtils.setFocusOnInvalidFields();
         }
    }

        toggleAddPopup(text) {
        this.addEditLabel = text;
        // this.isImageHide = true;
     
        (<any>$('#intake-addperson')).modal('show');
        (<any>$('#profile-click')).click();
    }

    clearProvider() {
        this.provider = new Provider();
        this.providerFormGroup.reset();
    }

}
