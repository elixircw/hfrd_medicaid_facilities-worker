import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Health, Physician } from '../../_entities/newintakeModel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService } from '../../../../../@core/services/alert.service';
import { InvolvedPerson } from '../../../../case-worker/_entities/caseworker.data.model';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../../@core/services/auth.service';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'person-health',
    templateUrl: './person-health.component.html',
    styleUrls: ['./person-health.component.scss']
})
export class PersonHealthComponent implements OnInit {
    @Input()
    addHealthSubject$ = new Subject<Health>();
    @Input()
    addHealthOutputSubject$ = new Subject<Health>();
    @Input()
    healthFormReset$ = new Subject<boolean>();
    addHealth: Health;
    physicianForm: FormGroup;
    physician: Physician[] = [];
    token: AppUser;
    healthSections: any[];
    djshealthSections = [
        { id: 'physician-information', name: 'Physician Information', isActive: true },
        { id: 'health-insurance-information', name: 'Health Insurance Information', isActive: false },
        { id: 'medication-including-psychotropic', name: 'Medication Including Psychotropic', isActive: false },
        { id: 'health-examination', name: 'Health Examination/Evaluations', isActive: false },
        { id: 'medical-conditions', name: 'Medical Conditions', isActive: false },
        { id: 'behavioral-health-info', name: 'Behavioral Health Info', isActive: false },
        { id: 'history-of-abuse', name: 'History of Abuse', isActive: false },
        { id: 'substance-abuse', name: 'Substance Abuse', isActive: false },
        // { id: '', name: 'Substance Use' },
        // { id: '', name: 'Substance Abuse Assessments' },
        // { id: '', name: 'Dental' },
        // { id: '', name: 'Vision' },
        // { id: '', name: 'Immunizations' },
        // { id: '', name: 'Behavioral/Mental Health Situation' },
        { id: 'log', name: 'Log', isActive: false }
    ];
    cwhealthSections = [
        { id: 'provider-dental-information-cw', name: 'Provider Information', isActive: true },
        { id: 'insurance-information-cw', name: 'Insurance Information', isActive: false },
        { id: 'medication-including-psychotropic-cw', name: 'Medication Including Psychotropic', isActive: false },
        { id: 'medical-conditions-cw', name: 'Medical Conditions', isActive: false },
        { id: 'behavioral-health-info-cw', name: 'Behavioral Health Info', isActive: false },
        { id: 'substance-abuse', name: 'Substance Abuse', isActive: false },
    ];


    selectedHealthSection: any;
    physicianEditInd = -1;

    @Input()
    addedPersons: InvolvedPerson[] = [];

    constructor(private formbulider: FormBuilder,
        private _alertSevice: AlertService,
        private _authService: AuthService) {
    }

    ngOnInit() {
        this.token = this._authService.getCurrentUser();
        const teamTypeKey = this.token.user.userprofile.teamtypekey;
        if (teamTypeKey === 'DJS') {
            this.healthSections = this.djshealthSections;
        } else if (teamTypeKey === 'CW') {
            this.healthSections = this.cwhealthSections;
        }
        this.selectedHealthSection = this.healthSections[0];
        // this.physicianForm = this.formbulider.group({
        //     'ismedicaidmedicare': true,
        //     'providertype': 1,
        //     'policyholdername': '',
        //   'address1': '',
        //   'address2': '',
        //   'city': '',
        //   'state': '',
        //   'county': '',
        //   'zip': '',
        //   'providerphone': '',
        //   'patientpolicyholderrelation': '',
        //   'policyname': '',
        //   'groupnumber': '',
        //   'startdate': '',
        //   'enddate': ''
        // });
        // this.addHealth = {
        //     physician: []
        // };
        this.healthFormReset$.subscribe((res) => {
            if (res === true) {
                // this.resetForm();
                this.showHealthSection(this.healthSections[0]);
            }
        });
        // this.addHealthOutputSubject$.subscribe((health) => {
        //     this.physician = health.physician ? health.physician : [];
        //     this.viewPhysician(this.physician);
        // });
        // this.addHealthSubject$.next(this.addHealth);

        // this.physicianForm.valueChanges.subscribe(result => {
        //     const physician = this.physicianForm.getRawValue();
        //     this.addPhysician(physician);
        // });

        // console.log('Selected Tab', this.selectedHealthSection);
    }

    addPhysician(model: Physician) {

        this.physician.push(model);
        this.addHealth.physician = this.physician;
        this.addHealthSubject$.next(this.addHealth);
    }
    viewPhysician(model) {
        this.physicianForm.patchValue(model);
    }

    showHealthSection(healthSection) {
        this.healthSections.forEach(section => section.isActive = false);
        healthSection.isActive = true;
        this.selectedHealthSection = healthSection;
    }

    // private resetForm() {
    //      this.physicianForm.reset();
    // }
}

