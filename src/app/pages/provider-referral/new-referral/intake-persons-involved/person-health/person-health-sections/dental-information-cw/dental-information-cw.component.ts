import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService, ValidationService } from '../../../../../../../@core/services';
import { Health, PersonDentalInfo } from '../../../../_entities/newintakeModel';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../../../../provider-referral-url.config';
import { NewReferralConstants } from '../../../../new-referral.constants';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dental-information-cw',
  templateUrl: './dental-information-cw.component.html',
  styleUrls: ['./dental-information-cw.component.scss']
})
export class DentalInformationCwComponent implements OnInit {
  @Input()
  healthFormReset$ = new Subject<boolean>();
  minDate = new Date();
  maxDate = new Date();
  dentalForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  persondentalinfo: PersonDentalInfo[] = [];
  health: Health = {};
  specialty$: Observable<DropdownModel[]>;
  specialty: any = [];
  constants = NewReferralConstants.Intake.PersonsInvolved.Health;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {

  }

  ngOnInit() {
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.dentalForm = this.formbulider.group({
      'isdentalinfo': [false, Validators.required],
      'dentistname': ['', Validators.required],
      'dentalspecialtytypekey': ['', Validators.required],
      'phone': ['', Validators.required],
      'email': ['', ValidationService.mailFormat],
      'address1': '',
      'address2': '',
      'city': '',
      'state': '',
      'county': ['', Validators.required],
      'zip': '',
      'startdate': ['', Validators.required],
      'enddate': '',
    });
    this.healthFormReset$.subscribe((res) => {
      if (res === true) {
        this.initializeProvider();
      }
    });

    this.initializeProvider();
    //this.loadDropDowns();
  }

  initializeProvider() {
    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.persondentalinfo) {
      this.persondentalinfo = this.health.persondentalinfo;
    }

    this.resetForm();
  }


  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'description'
        },
        NewUrlConfig.EndPoint.Intake.dentalspecialtytype + '?filter'
      ),
    ])
      .map((result) => {
        result[3].forEach(type => {
          this.specialty[type.dentalspecialtytypekey] = type.description;
        });
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),
          counties: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.countyname,
                value: res.countyname
              })
          ),
          specialty: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.dentalspecialtytypekey
              })
          ),
        };
      })
      .share();
    this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
    this.specialty$ = source.pluck('specialty');
  }

  private add() {
    this.persondentalinfo.push(this.dentalForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.persondentalinfo = this.persondentalinfo;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.dentalForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.dentalForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.persondentalinfo[this.modalInt] = this.dentalForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.dentalForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.dentalForm.enable();
  }

  private delete(index) {
    this.persondentalinfo.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  startDateChanged() {
    this.dentalForm.patchValue({enddate : ''});
    const empForm = this.dentalForm.getRawValue();
    this.maxDate = new Date(empForm.startdate);
  }
  endDateChanged() {
    this.dentalForm.patchValue({startdate : ''});
    const empForm = this.dentalForm.getRawValue();
    this.minDate = new Date(empForm.enddate);
  }
  private patchForm(modal: PersonDentalInfo) {
    this.dentalForm.patchValue(modal);
  }

}
