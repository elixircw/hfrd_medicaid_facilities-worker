import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../../../../../../../@core/services';
import { Health } from '../../../../_entities/newintakeModel';
import { NewReferralConstants } from '../../../../new-referral.constants';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'substance-abuse',
  templateUrl: './substance-abuse.component.html',
  styleUrls: ['./substance-abuse.component.scss']
})
export class SubstanceAbuseComponent implements OnInit {

  substanceabuseForm: FormGroup;
  health: Health = {};
  constants = NewReferralConstants.Intake.PersonsInvolved.Health;
  alchoholSelectionEnabled: boolean;
  drugSelectionEnabled: boolean;
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this.substanceabuseForm = this.formbulider.group({
      isusetobacco: '',
      isusedrugoralcohol: '',
      isusedrug: '',
      drugfrequencydetails: '',
      drugageatfirstuse: '',
      isusealcohol: '',
      alcoholfrequencydetails: '',
      alcoholageatfirstuse: '',
      drugoralcoholproblems: ''
    });

    this.health = this._dataStoreService.getData(this.constants.Health);
    this.alchoholSelectionEnabled = false;
    this.drugSelectionEnabled = false;
    if (this.health && this.health.substanceAbuse) {
      this.substanceabuseForm.patchValue(this.health.substanceAbuse);
    }
    this.health.substanceAbuse = this.substanceabuseForm.getRawValue();
    this.drugSelection(this.health.substanceAbuse.isusedrug);
    this.alchoholSelection(this.health.substanceAbuse.isusealcohol);
    this.substanceabuseForm.valueChanges.subscribe(data => {
      this.health = this._dataStoreService.getData(this.constants.Health);
      this.health.substanceAbuse = this.substanceabuseForm.getRawValue();
      this._dataStoreService.setData(this.constants.Health, this.health);
    });
  }

  drugSelection(control) {
    if (control === '1') {
      this.drugSelectionEnabled = true;
      this.substanceabuseForm.get('drugfrequencydetails').enable();
      this.substanceabuseForm.get('drugfrequencydetails').setValidators([Validators.required]);
      this.substanceabuseForm.get('drugfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('drugageatfirstuse').enable();
      this.substanceabuseForm.get('drugageatfirstuse').setValidators([Validators.required]);
      this.substanceabuseForm.get('drugageatfirstuse').updateValueAndValidity();

    } else {
      this.drugSelectionEnabled = false;
      this.substanceabuseForm.get('drugfrequencydetails').disable();
      this.substanceabuseForm.get('drugfrequencydetails').clearValidators();
      this.substanceabuseForm.get('drugfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('drugageatfirstuse').disable();
      this.substanceabuseForm.get('drugageatfirstuse').clearValidators();
      this.substanceabuseForm.get('drugageatfirstuse').updateValueAndValidity();
    }
  }

  alchoholSelection(control) {
    if (control === '1') {
      this.alchoholSelectionEnabled = true;
      this.substanceabuseForm.get('alcoholfrequencydetails').enable();
      this.substanceabuseForm.get('alcoholfrequencydetails').setValidators([Validators.required]);
      this.substanceabuseForm.get('alcoholfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('alcoholageatfirstuse').enable();
      this.substanceabuseForm.get('alcoholageatfirstuse').setValidators([Validators.required]);
      this.substanceabuseForm.get('alcoholageatfirstuse').updateValueAndValidity();
    } else {
      this.alchoholSelectionEnabled = false;
      this.substanceabuseForm.get('alcoholfrequencydetails').disable();
      this.substanceabuseForm.get('alcoholfrequencydetails').clearValidators();
      this.substanceabuseForm.get('alcoholfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('alcoholageatfirstuse').disable();
      this.substanceabuseForm.get('alcoholageatfirstuse').clearValidators();
      this.substanceabuseForm.get('alcoholageatfirstuse').updateValueAndValidity();
    }
  }

  resetForm() {
    this.substanceabuseForm.reset();
    this.drugSelection('2');
    this.alchoholSelection('2');
  }
  save() {
    this._alertSevice.success('Saved Successfully');
  }
}
