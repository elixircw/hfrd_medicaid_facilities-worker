import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../../../../@core/services';
import { Health, Medication } from '../../../../_entities/newintakeModel';
import { NewReferralConstants } from '../../../../new-referral.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../../../../provider-referral-url.config';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'medication-including-psychotropic',
  templateUrl: './medication-including-psychotropic.component.html',
  styleUrls: ['./medication-including-psychotropic.component.scss']
})
export class MedicationIncludingPsychotropicComponent implements OnInit {
  medicationpsychotropicForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  medicationpsychotropic:  Medication[] = [];
  health: Health = {};
  constants = NewReferralConstants.Intake.PersonsInvolved.Health;
  prescriptionReasonType$: Observable<DropdownModel[]>;
  informationSourceType$: Observable<DropdownModel[]>;
  maxDate = new Date();
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    // this.loadDropDowns();
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.medicationpsychotropicForm = this.formbulider.group({
      medicationname: ['', Validators.required],
      dosage: ['', Validators.required],
      frequency: ['', Validators.required],
      lastdosetakendate: ['', Validators.required],
      informationsourcetypekey: '',
      medicationeffectivedate: ['', Validators.required],
      medicationexpirationdate: '',
      prescribingdoctor: '',
      prescriptionreasontypekey: '',
      monitoring: '',
      medicationcomments: ''
    });

    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.medication) {
      this.medicationpsychotropic = this.health.medication;
    }

    // console.log('medicationpsychotropic', this.health);

    // this.medicationpsychotropicForm.valueChanges.subscribe(physician => {
    //   this._dataStoreService.setData('medication_including_psychopatric', this.medicationpsychotropicForm.getRawValue());
    // });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.prescriptionreasontype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.informationsourcetype + '?filter'
      )
    ])
      .map((result) => {
        return {
          prescriptionreasontype: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.prescriptionreasontypekey
              })
          ),
          informationsourcetype: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.informationsourcetypekey
              })
          )
        };
      })
      .share();
    this.prescriptionReasonType$ = source.pluck('prescriptionreasontype');
    this.informationSourceType$ = source.pluck('informationsourcetype');
  }

  private add() {
    this.medicationpsychotropic.push(this.medicationpsychotropicForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.medication = this.medicationpsychotropic;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.medicationpsychotropicForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.medicationpsychotropicForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.medicationpsychotropic[this.modalInt] = this.medicationpsychotropicForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.medicationpsychotropicForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.medicationpsychotropicForm.enable();
  }

  private delete(index) {
    this.medicationpsychotropic.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: Medication) {
    this.medicationpsychotropicForm.patchValue(modal);
  }

}
