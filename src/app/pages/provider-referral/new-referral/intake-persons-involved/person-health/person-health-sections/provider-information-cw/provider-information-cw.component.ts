import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService, ValidationService } from '../../../../../../../@core/services';
import { Health, Physician } from '../../../../_entities/newintakeModel';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../../../../provider-referral-url.config';
import { NewReferralConstants } from '../../../../new-referral.constants';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-information-cw',
  templateUrl: './provider-information-cw.component.html',
  styleUrls: ['./provider-information-cw.component.scss']
})
export class ProviderInformationCwComponent implements OnInit {
  @Input()
  healthFormReset$ = new Subject<boolean>();
  minDate = new Date();
  maxDate = new Date();
  providerForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  specialty$: Observable<DropdownModel[]>;
  specialty: any = [];
  providercw: Physician[] = [];
  health: Health = {};
  constants = NewReferralConstants.Intake.PersonsInvolved.Health;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {

  }

  ngOnInit() {
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.providerForm = this.formbulider.group({
      'isprimaryphycisian': [false, Validators.required],
      'name': ['', Validators.required],
      'physicianspecialtytypekey': ['', Validators.required],
      'facility': ['', Validators.required],
      'phone': ['', Validators.required],
      'email': ['', ValidationService.mailFormat],
      'address1': '',
      'address2': '',
      'city': '',
      'state': '',
      'county': ['', Validators.required],
      'zip': '',
      'startdate': ['', Validators.required],
      'enddate': '',
    });

    this.healthFormReset$.subscribe((res) => {
      if (res === true) {
        this.initializeProvider();
      }
    });

    this.initializeProvider();
    //this.loadDropDowns();
  }

  initializeProvider() {
    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.physician) {
      this.providercw = this.health.physician;
    }

    this.resetForm();
  }


  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
           order: 'description'
        },
        NewUrlConfig.EndPoint.Intake.physicianspecialtytype + '?filter'
      ),
    ])
      .map((result) => {
        result[3].forEach(type => {
          this.specialty[type.physicianspecialtytypekey] = type.description;
        });
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),
          counties: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.countyname,
                value: res.countyname
              })
          ),
          specialty: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.physicianspecialtytypekey
              })
          ),
        };
      })
      .share();
    this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
    this.specialty$ = source.pluck('specialty');
  }

  private add() {
    this.providercw.push(this.providerForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.physician = this.providercw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.providerForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.providerForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.providercw[this.modalInt] = this.providerForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.providerForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.providerForm.enable();
  }

  private delete(index) {
    this.providercw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  startDateChanged() {
    this.providerForm.patchValue({enddate : ''});
    const empForm = this.providerForm.getRawValue();
    this.maxDate = new Date(empForm.startdate);
  }
  endDateChanged() {
    this.providerForm.patchValue({startdate : ''});
    const empForm = this.providerForm.getRawValue();
    this.minDate = new Date(empForm.enddate);
  }
  private patchForm(modal: Physician) {
    this.providerForm.patchValue(modal);

    if(modal.startdate){
      this.providerForm.patchValue({startdate: new Date(modal.startdate)});
    }
    if(modal.enddate){
      this.providerForm.patchValue({enddate: new Date(modal.enddate)});
    }
  }

}
