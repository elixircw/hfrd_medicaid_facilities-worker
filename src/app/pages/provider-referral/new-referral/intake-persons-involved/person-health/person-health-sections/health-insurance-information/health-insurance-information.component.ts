import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../../../../@core/services';
import { NewReferralConstants } from '../../../../new-referral.constants';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../../../../provider-referral-url.config';
import { InvolvedPerson, Health, HealthInsuranceInformation } from '../../../../_entities/newintakeModel';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'health-insurance-information',
  templateUrl: './health-insurance-information.component.html',
  styleUrls: ['./health-insurance-information.component.scss']
})
export class HealthInsuranceInformationComponent implements OnInit {
  healthinsuranceForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  minDate = new Date();
  maxDate = new Date();
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  healthinsurance: HealthInsuranceInformation[] = [];
  health: Health;
  constants = NewReferralConstants.Intake.PersonsInvolved.Health;

  medAssistance: boolean;
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) {
  }

  ngOnInit() {
    //this.loadDropDowns();
    this.medAssistance = false;
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.healthinsuranceForm = this.formbulider.group({
      ismedicaidmedicare: ['', Validators.required],
      medicalinsuranceprovider: ['', Validators.required],
      providertype: ['', Validators.required],
      policyholdername: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      county: '',
      zip: '',
      providerphone: '',
      patientpolicyholderrelation: '',
      policyname: ['', Validators.required],
      groupnumber: '',
      startdate: '',
      enddate: ''

    });

    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.healthInsurance) {
      this.healthinsurance = this.health.healthInsurance;
    }

    // console.log('health', this.health);

    // if (this.health.healthInsurance) {
    //   this.healthinsuranceForm.patchValue(this.health.healthInsurance);
    // }

    // this.healthinsuranceForm.valueChanges.subscribe(physician => {
    //   this._dataStoreService.setData('health_insurance', this.healthinsuranceForm.getRawValue());
    // });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
      ),
    ])
      .map((result) => {
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),
          counties: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.countyname,
                value: res.countyname
              })
          ),
        };
      })
      .share();
    this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
  }

  selectmedicalassistance(control) {
    this.medAssistance = control;
  }
  private add() {
    this.healthinsurance.push(this.healthinsuranceForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.healthInsurance = this.healthinsurance;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this.resetForm();
    this._alertSevice.success('Added Successfully');
  }

  private resetForm() {
    this.healthinsuranceForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.healthinsuranceForm.enable();
    this.medAssistance = false;
  }

  private update() {
    if (this.modalInt !== -1) {
      this.healthinsurance[this.modalInt] = this.healthinsuranceForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.medAssistance = modal.medicalassistance;
    this.reportMode = 'edit';
    this.editMode = false;
    this.patchForm(modal);
    this.healthinsuranceForm.disable();
  }

  private edit(modal, i) {
    this.medAssistance = modal.medicalassistance;
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.healthinsuranceForm.enable();
  }

  private delete(index) {
    this.healthinsurance.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }


  private startDateChanged() {
    this.healthinsuranceForm.patchValue({ enddate: '' });
    const empForm = this.healthinsuranceForm.getRawValue();
    this.maxDate = new Date(empForm.enddate);
  }
  private endDateChanged() {
    this.healthinsuranceForm.patchValue({ startdate: '' });
    const empForm = this.healthinsuranceForm.getRawValue();
    this.minDate = new Date(empForm.startdate);
  }

  private patchForm(modal: HealthInsuranceInformation) {
    this.healthinsuranceForm.patchValue(modal);
  }
}
