import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../../../../../../../@core/services';
import { Health, HistoryofAbuse } from '../../../../_entities/newintakeModel';
import { NewReferralConstants } from '../../../../new-referral.constants';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'history-of-abuse',
  templateUrl: './history-of-abuse.component.html',
  styleUrls: ['./history-of-abuse.component.scss']
})
export class HistoryOfAbuseComponent implements OnInit {

  historyofabuseForm: FormGroup;
  health: Health = {};
  physicalabuseSelectionEnabled: boolean;
  emotionalabuseSelectionEnabled: boolean;
  sexualabuseSelectionEnabled: boolean;
  neglectSelectionEnabled: boolean;
  constants = NewReferralConstants.Intake.PersonsInvolved.Health;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this.historyofabuseForm = this.formbulider.group({
      isneglect: '',
      neglectnotes: '',
      isphysicalabuse: '',
      physicalnotes: '',
      isemotionalabuse: '',
      emotionalnotes: '',
      issexualabuse: '',
      sexualnotes: ''
    });
    this.physicalabuseSelectionEnabled = false;
    this.emotionalabuseSelectionEnabled = false;
    this.sexualabuseSelectionEnabled = false;
    this.neglectSelectionEnabled = false;
    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.history) {
      this.historyofabuseForm.patchValue(this.health.history);
    }
    this.health.history = this.historyofabuseForm.getRawValue();
    this.emotionalabuseSelection(this.health.history.isemotionalabuse);
    this.physicalabuseSelection(this.health.history.isphysicalabuse);
    this.sexualabuseSelection(this.health.history.issexualabuse);
    this.neglectSelection(this.health.history.isneglect);
    this.historyofabuseForm.valueChanges.subscribe(data => {
      this.health = this._dataStoreService.getData(this.constants.Health);
      this.health.history = this.historyofabuseForm.getRawValue();
      this._dataStoreService.setData(this.constants.Health, this.health);
    });
  }

  resetForm() {
    this.historyofabuseForm.reset();
    this.emotionalabuseSelection(false);
    this.physicalabuseSelection(false);
    this.sexualabuseSelection(false);
    this.neglectSelection(false);
  }


  neglectSelection(control) {
    if (control) {
      this.neglectSelectionEnabled = true;
      this.historyofabuseForm.get('neglectnotes').enable();
      this.historyofabuseForm.get('neglectnotes').setValidators([Validators.required]);
      this.historyofabuseForm.get('neglectnotes').updateValueAndValidity();

    } else {
      this.neglectSelectionEnabled = false;
      this.historyofabuseForm.get('neglectnotes').disable();
      this.historyofabuseForm.get('neglectnotes').clearValidators();
      this.historyofabuseForm.get('neglectnotes').updateValueAndValidity();

    }
  }


  sexualabuseSelection(control) {
    if (control) {
      this.sexualabuseSelectionEnabled = true;
      this.historyofabuseForm.get('sexualnotes').enable();
      this.historyofabuseForm.get('sexualnotes').setValidators([Validators.required]);
      this.historyofabuseForm.get('sexualnotes').updateValueAndValidity();

    } else {
      this.sexualabuseSelectionEnabled = false;
      this.historyofabuseForm.get('sexualnotes').disable();
      this.historyofabuseForm.get('sexualnotes').clearValidators();
      this.historyofabuseForm.get('sexualnotes').updateValueAndValidity();

    }
  }


  emotionalabuseSelection(control) {
    if (control) {
      this.emotionalabuseSelectionEnabled = true;
      this.historyofabuseForm.get('emotionalnotes').enable();
      this.historyofabuseForm.get('emotionalnotes').setValidators([Validators.required]);
      this.historyofabuseForm.get('emotionalnotes').updateValueAndValidity();

    } else {
      this.emotionalabuseSelectionEnabled = false;
      this.historyofabuseForm.get('emotionalnotes').disable();
      this.historyofabuseForm.get('emotionalnotes').clearValidators();
      this.historyofabuseForm.get('emotionalnotes').updateValueAndValidity();

    }
  }


  physicalabuseSelection(control) {
    if (control) {
      this.physicalabuseSelectionEnabled = true;
      this.historyofabuseForm.get('physicalnotes').enable();
      this.historyofabuseForm.get('physicalnotes').setValidators([Validators.required]);
      this.historyofabuseForm.get('physicalnotes').updateValueAndValidity();

    } else {
      this.physicalabuseSelectionEnabled = false;
      this.historyofabuseForm.get('physicalnotes').disable();
      this.historyofabuseForm.get('physicalnotes').clearValidators();
      this.historyofabuseForm.get('physicalnotes').updateValueAndValidity();

    }
  }

  save() {
    this._alertSevice.success('Saved Successfully');
  }
}
