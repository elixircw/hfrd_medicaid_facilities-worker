import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeSocialHistoryInrComponent } from './intake-social-history-inr.component';

describe('IntakeSocialHistoryInrComponent', () => {
  let component: IntakeSocialHistoryInrComponent;
  let fixture: ComponentFixture<IntakeSocialHistoryInrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeSocialHistoryInrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeSocialHistoryInrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
