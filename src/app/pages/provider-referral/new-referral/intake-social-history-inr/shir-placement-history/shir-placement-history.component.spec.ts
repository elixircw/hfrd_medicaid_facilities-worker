import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirPlacementHistoryComponent } from './shir-placement-history.component';

describe('ShirPlacementHistoryComponent', () => {
  let component: ShirPlacementHistoryComponent;
  let fixture: ComponentFixture<ShirPlacementHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirPlacementHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirPlacementHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
