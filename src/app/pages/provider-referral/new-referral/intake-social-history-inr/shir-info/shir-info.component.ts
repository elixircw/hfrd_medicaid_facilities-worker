import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { InvolvedPerson } from '../../_entities/newintakeModel';
import { DataStoreService } from '../../../../../@core/services';
import { NewReferralConstants } from '../../new-referral.constants';
import { CourtDetails, PetitionDetails } from '../../_entities/newintakeSaveModel';

@Component({
  selector: 'shir-info',
  templateUrl: './shir-info.component.html',
  styleUrls: ['./shir-info.component.scss']
})
export class ShirInfoComponent implements OnInit {
  basicInfoForm: FormGroup;
  persons: InvolvedPerson[];
  youth: InvolvedPerson;
  courtDetails: CourtDetails;
  petitionDetails: PetitionDetails;
  constructor(private formBuilder: FormBuilder, private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this._dataStoreService.currentStore.subscribe(data => {
      this.extractData();
    });
  }

  extractData(): void {
    this.persons = this._dataStoreService.getData(NewReferralConstants.Intake.addedPersons);
    this.youth = this.getPerson('Youth');

    this.courtDetails = this._dataStoreService.getData(NewReferralConstants.Intake.courtDetails);
    this.petitionDetails = this._dataStoreService.getData(NewReferralConstants.Intake.petitionDetails);
    console.log(this.courtDetails, this.petitionDetails);

    this.initializeForm();
  }

  initializeForm() {
    this.basicInfoForm = this.formBuilder.group({
      circuitCourtTo: '',
      circuitCourtFrom: '',
      date: new Date(),
      youthDOB: this.youth ? this.youth.Dob : '',
      re: '',
      pertionID: this.petitionDetails ? this.petitionDetails.petitionid : '',
      courtDate: this.courtDetails ? this.courtDetails.hearingdatetime : '',
      typeOfReport: '',
      youthName: this.youth ? this.youth.fullName : '',
      address: this.youth ? this.youth.fullAddress : '',
      telephone: (this.youth && this.youth.phoneNumber && this.youth.phoneNumber.length > 0) ? this.youth.phoneNumber[0].contactnumber : '',
      dob: this.youth ? this.youth.Dob : '',
      driverLicense: this.youth ? this.youth.stateid : '',
      cjamsID: '',
      currentOffense: '',
      petitionNumber: this.petitionDetails ? this.petitionDetails.petitionid : '',
      adjudicatedOffense: '',
      dispositionDate: this.courtDetails ? this.courtDetails.adjudicationdatetime : ''
    });
  }

  getPerson(Role: string): InvolvedPerson {
    if (this.persons) {
      return this.persons.find((person) => person.Role === Role);
    }
    return null;
  }

}
