import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirPriorOffenceRecordComponent } from './shir-prior-offence-record.component';

describe('ShirPriorOffenceRecordComponent', () => {
  let component: ShirPriorOffenceRecordComponent;
  let fixture: ComponentFixture<ShirPriorOffenceRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirPriorOffenceRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirPriorOffenceRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
