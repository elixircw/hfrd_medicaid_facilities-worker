import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirHouseholdFamilyComponent } from './shir-household-family.component';

describe('ShirHouseholdFamilyComponent', () => {
  let component: ShirHouseholdFamilyComponent;
  let fixture: ComponentFixture<ShirHouseholdFamilyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirHouseholdFamilyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirHouseholdFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
