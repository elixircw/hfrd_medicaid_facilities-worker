import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirSummaryRecommendatiomComponent } from './shir-summary-recommendatiom.component';

describe('ShirSummaryRecommendatiomComponent', () => {
  let component: ShirSummaryRecommendatiomComponent;
  let fixture: ComponentFixture<ShirSummaryRecommendatiomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirSummaryRecommendatiomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirSummaryRecommendatiomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
