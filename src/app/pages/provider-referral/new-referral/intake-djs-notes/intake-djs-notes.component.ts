import { Component, OnInit, Input } from '@angular/core';
// tslint:disable-next-line:import-blacklist
import { Observable, Subject } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { ContactType, PurposeType, TypeofLocation, PersonsInvolvedType, InvolvedPerson, Notes } from '../_entities/newintakeModel';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService } from '../../../../@core/services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../../@core/services/auth.service';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-djs-notes',
    templateUrl: './intake-djs-notes.component.html',
    styleUrls: ['./intake-djs-notes.component.scss']
})
export class IntakeDjsNotesComponent implements OnInit {
    contactType$: Observable<ContactType[]>;
    purposeType$: Observable<PurposeType[]>;
    typeofLocation$: Observable<TypeofLocation[]>;
    personsInvolvedType$: Observable<PersonsInvolvedType[]>;
    notesForm: FormGroup;
    savednotes: Notes[] = [];
    purposeType: PurposeType[] = [];
    contactType: ContactType[] = [];
    typeofLocation: TypeofLocation[] = [];
    personsInvolvedType: PersonsInvolvedType[] = [];
    // tslint:disable-next-line:no-input-rename
    @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
    @Input() communicationInputSubject$ = new Subject<Notes[]>();
    @Input() communicationOutputSubject$ = new Subject<Notes[]>();
    addedPersons: InvolvedPerson[];
    youthList: InvolvedPerson[];
    intakeId: string;
    notesId: string;
    editMode: boolean;
    userInfo: any;

    constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _authService: AuthService,
        private _alertService: AlertService, private route: ActivatedRoute, ) { }


    ngOnInit() {
        //this.loadDropdowns();
        this.userInfo = this._authService.getCurrentUser();
        this.notesId = null;
        this.editMode = false;
        this.notesForm = this.formBuilder.group({
            progressnoteid: '',
            progressnotetypeid: '',
            progressnotesubtypeid: '',
            contactdate: '',
            date: '',
            location: '',
            contactname: '',
            contactroletypekey: [''],
            contactphone: '',
            contactemail: '',
            attemptindicator: false,
            description: '',
            starttime: '',
            staff: [''],
            endtime: '',
            entitytypeid: 'a426ee09-ed7f-4b37-aead-60925f13425b',
            entitytype: 'intakeservicerequest',
            savemode: 1,
            stafftype: 1,
            notes: '',
            instantresults: 1,
            contactstatus: true,
            drugscreen: true,
            progressnotepurposetypekey: '',
            progressnoteroletype: ['']
        });
        // this.getNotesList();
        // this.loadLocationTypeDropdown('8d25a26b-91a1-4423-947e-e451e8eff6b7');


        this.addedPersonsChanges$.subscribe((data) => {
            this.addedPersons = data;
            this.youthList = this.addedPersons.filter((p) => p.Role === 'Youth');
        });

        // this.notesForm.valueChanges.subscribe((val) => {
        //     if (this.savednotes) {
        //     this.communicationInputSubject$.next(this.savednotes);
        //     }
        //   });

        this.notesForm.patchValue({ 'staff': this.userInfo ? this.userInfo.user.userprofile.displayname : '' });
        this.notesForm.get('staff').disable();
        this.communicationOutputSubject$.subscribe((data) => {
            this.savednotes = data && data.length > 0 ? data : [];
        });
    }


    private loadDropdowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.contactType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.purposeType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.personsInvolvedType)
        ])
            .map(([contactType, purposeType, personsInvolvedType]) => {
                return {
                    contactType,
                    purposeType,
                    personsInvolvedType
                };
            })
            .share();

        this.contactType$ = source.pluck('contactType');
        this.purposeType$ = source.pluck('purposeType');
        this.personsInvolvedType$ = source.pluck('personsInvolvedType');
        this.contactType$.subscribe((data) => { this.contactType = data; });
        this.purposeType$.subscribe((data) => { this.purposeType = data; });
        this.personsInvolvedType$.subscribe((data) => { this.personsInvolvedType = data; });

    }
    private loadLocationTypeDropdown(progressnotetypeid: string) {
        this.notesForm.patchValue({
            progressnotetypeid: progressnotetypeid,
        });
        const locationType = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    'nolimit': true,
                    'order': 'description'
                }),
                NewUrlConfig.EndPoint.Intake.typeofLocation + '?prognotetypeid=' + progressnotetypeid
            )
            .map((typeofLocation) => {
                return {
                    typeofLocation
                };
            })
            .share();

        this.typeofLocation$ = locationType.pluck('typeofLocation');
        this.typeofLocation$.subscribe((data) => { this.typeofLocation = data; });
    }

    private getNotesList() {
        this.route.params.subscribe((item) => {
            this.intakeId = item['id'];
        });
        const NotesList = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    count: -1,
                    page: 1,
                    limit: 10,
                    where: {},
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.Intake.notesList + '/' + this.intakeId + '?data'
            )
            .map((notes) => {
                return {
                    notes
                };
            })
            .share();

        // this.savedNotes$ = NotesList.pluck('notes');

    }



    private saveNote() {
        if (this.notesForm.valid) {
            this.notesForm.patchValue({ progressnoteid: this.savednotes.length + 1 });
            const data = this.notesForm.getRawValue();
            this.savednotes.push(data);
            this.broadCastCommunicationUpdated();
            (<any>$('#add-djs-notes')).modal('hide'); this.resetForm();
        } else {
            this._alertService.error('Please fill the required fields');
        }
    }

    resetForm() {
        this.notesForm.reset();
        this.notesId = null;
        this.notesForm.enable();
        this.notesForm.patchValue({ 'staff': this.userInfo ? this.userInfo.user.userprofile.displayname : '' });
    }
    private updateNotes() {
        if (this.notesForm.valid) {
            this.notesForm.patchValue({ progressnoteid: this.notesId });
            const formdata = this.notesForm.getRawValue();
            const index = this.savednotes.findIndex((p) => p.progressnoteid === this.notesId);
            if (index !== -1) {
                this.savednotes[index] = formdata;
            }
            // this.notesForm.reset();
            this.broadCastCommunicationUpdated(); (<any>$('#add-djs-notes')).modal('hide'); this.resetForm();
        } else {
            this._alertService.error('Please fill the required fields');
        }

    }


    private getNotes(note: Notes, editFlag) {
        this.resetForm();
        this.loadLocationTypeDropdown(note.progressnotetypeid);
        this.notesId = note.progressnoteid;
        this.editMode = editFlag;
        if (editFlag === 0) {
            this.notesForm.disable();
        } else {
            this.notesForm.enable();
        }
        // const data = this.savednotes.filter((p) => p.progressnoteid === id);

        this.patchForm(note);
    }



    private broadCastCommunicationUpdated() {
        this.communicationInputSubject$.next(this.savednotes);
    }

    private getSavedNote(id) {
        const NotesList = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    count: -1,
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.Intake.notesEdit + '/' + id
            )
            .map((notes) => {
                return {
                    notes
                };
            })
            .share();

        return NotesList.pluck('notes');
    }

    private getValue(value, name) {

        switch (name) {
            case 'contype':

                const cindex = this.contactType.findIndex((p) => p.progressnotetypeid === value);
                if (cindex !== -1) {
                    return this.contactType[cindex].description;
                } else {
                    return '';
                }
            case 'stafftype': if (value === '1') {
                return 'Field';
            } else if (value === '2') {
                return 'Community Detention';
            } else {
                return '';
            }
            case 'constatus': if (value === '1') {
                return 'Yes';
            } else if (value === '2') {
                return 'No';
            } else {
                return '';
            }
            case 'location':
                if (this.typeofLocation$ && this.typeofLocation$ !== null) {

                    const i = this.typeofLocation.findIndex((p) => p.progressnotetypeid === value);
                    if (i !== -1) {
                        return this.typeofLocation[i].description;
                    } else {
                        return '';
                    }

                } else {
                    return '';
                }
            case 'drugscrn': if (value === '1') {
                return 'Yes';
            } else if (value === '2') {
                return 'No';
            } else {
                return '';
            }
            case 'instresult': if (value === '1') {
                return 'Positive';
            } else if (value === '2') {
                return 'Negative';
            } else {
                return '';
            }

            case 'purpose':
                const index = this.purposeType.findIndex((p) => p.progressnotepurposetypeid === value);
                if (index !== -1) {
                    return this.purposeType[index].description;
                } else {
                    return '';
                }

            default: return '';
        }
    }

    private setLocation(desc) {
        this.notesForm.patchValue({ 'location': desc });
    }

    private patchForm(data) {
        if (data) {
            this.notesForm.patchValue(data);
        }
    }
}
