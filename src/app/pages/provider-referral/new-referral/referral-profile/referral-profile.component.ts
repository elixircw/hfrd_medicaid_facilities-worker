import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';

import { FormGroup, FormBuilder } from '@angular/forms';
import { Profile } from '../_entities/newreferralModel';

@Component({
  selector: 'referral-profile',
  templateUrl: './referral-profile.component.html',
  styleUrls: ['./referral-profile.component.scss']
})
export class ReferralProfileComponent implements OnInit {

  @Input()
  profileOutputSubject$ = new Subject<Profile>();

  constructor(private formBuilder: FormBuilder) { }
  addressInfo:Profile;

  ngOnInit() {
   this.addressInfo = new Profile;
    this.profileOutputSubject$.subscribe((data) => {
      console.log(data.firstName,"data firstname");
        this.addressInfo.fullname = data.fullname;
        this.addressInfo.firstName = data.firstName;
        this.addressInfo.phoneNumber = data.phoneNumber;
        this.addressInfo.lastName = data.lastName;
        this.addressInfo.streetName = data.streetName;
        this.addressInfo.cityName = data.cityName;
        this.addressInfo.zipCode = data.zipCode;
        this.addressInfo.state = data.state;
        this.addressInfo.streetNo = data.streetNo;
        this.addressInfo.countyCd = data.countyCd;
        this.addressInfo.country = data.country;
        this.addressInfo.taxid = data.taxid;
        this.addressInfo.faxNumber = data.faxNumber;
        this.addressInfo.workNumber = data.workNumber;
        this.addressInfo.email = data.email;
        this.addressInfo.program = data.program;
        this.addressInfo.programType = data.programType;
        this.addressInfo.gender = data.gender;
        this.addressInfo.minAge = data.minAge;
        this.addressInfo.maxAge = data.maxAge;
        this.addressInfo.bedCapacity = data.bedCapacity;
        console.log(this.addressInfo,"addressinfo");
        console.log(data.phoneNumber);
      });
  }
}
