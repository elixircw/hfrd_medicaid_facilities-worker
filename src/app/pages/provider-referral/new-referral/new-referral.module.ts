import 'trumbowyg/dist/trumbowyg.min.js';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';
import { PopoverModule } from 'ngx-popover';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { TrumbowygNgxModule } from 'trumbowyg-ngx';
// tslint:disable-next-line:max-line-length
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckbox,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule
} from '@angular/material';
import { MatTooltipModule, MatChipsModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SortTableModule } from '../../../shared/modules/sortable-table/sortable-table.module';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
// import { IntakeAssessmentComponent } from './intake-assessment/intake-assessment.component';
import { AttachmentDetailComponent } from './intake-attachments/attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './intake-attachments/attachment-upload/attachment-upload.component';
import { AudioRecordComponent } from './intake-attachments/audio-record/audio-record.component';
import { EditAttachmentComponent } from './intake-attachments/edit-attachment/edit-attachment.component';
import { ImageRecordComponent } from './intake-attachments/image-record/image-record.component';
import { IntakeAttachmentsComponent } from './intake-attachments/intake-attachments.component';
import { VideoRecordComponent } from './intake-attachments/video-record/video-record.component';
// import { IntakeCommunicationsComponent } from './intake-communications/intake-communications.component';
import { IntakeComplaintTypeComponent } from './intake-complaint-type/intake-complaint-type.component';
// import { IntakeCrossRefferenceComponent } from './intake-cross-refference/intake-cross-refference.component';
import { IntakeDispositionComponent } from './intake-disposition/intake-disposition.component';
// import { IntakeEntitiesComponent } from './intake-entities/intake-entities.component';
import { IntakeEvaluationFieldsComponent } from './intake-evaluation-fields/intake-evaluation-fields.component';
import { IntakePersonSearchComponent } from './intake-persons-involved/intake-person-search.component';
// import { IntakePersonsInvolvedComponent } from './intake-persons-involved/intake-persons-involved.component';
// import { IntakeServiceSubtypeComponent } from './intake-service-subtype/intake-service-subtype.component';
import { MyNewintakeRoutingModule } from './new-referral-routing.module';
import { NewReferralComponent } from './new-referral.component';
import { NewintakeNarrativeComponent } from './newintake-narrative/newintake-narrative.component';
import { PdfPeaceOrderAppealLetterComponent } from './intake-complaint-type/pdf-peace-order-appeal-letter/pdf-peace-order-appeal-letter.component';
import { IntakeSdmComponent } from './intake-sdm/intake-sdm.component';
// moved to shared module: import { CpsDocLetterComponent } from './intake-document-creator/cps-doc-letter/cps-doc-letter.component';
// import { PersonHealthComponent } from './intake-persons-involved/person-health/person-health.component';
// import { PersonHealthSectionLogComponent } from './intake-persons-involved/person-health/person-health-section-log/person-health-section-log.component';
// import { EducationalProfileComponent } from './intake-persons-involved/educational-profile/educational-profile.component';
// import { IntakeServiceTypeComponent } from './intake-service-type/intake-service-type.component';
// import { IntakeAppointmentsComponent } from './intake-appointments/intake-appointments.component';
import { CitizenshipDetailsComponent } from './intake-persons-involved/citizenship-details/citizenship-details.component';
// import { IntakeDecisionComponent } from './intake-decision/intake-decision.component';
// import { IntakeSaoCourtDetailsComponent } from './intake-sao-court-details/intake-sao-court-details.component';
// import { IntakeSaoPetitionDetailsComponent } from './intake-sao-petition-details/intake-sao-petition-details.component';
// import { NoticePreintakeLetterComponent } from './intake-document-creator/notice-preintake-letter/notice-preintake-letter.component';
import { QuillModule } from 'ngx-quill';
// import { PhysicianInformationComponent } from './intake-persons-involved/person-health/person-health-sections/physician-information/physician-information.component';
// import { HealthInsuranceInformationComponent } from './intake-persons-involved/person-health/person-health-sections/health-insurance-information/health-insurance-information.component';
// import { MedicationIncludingPsychotropicComponent } from './intake-persons-involved/person-health/person-health-sections/medication-including-psychotropic/medication-including-psychotropic.component';
// import { HealthExaminationComponent } from './intake-persons-involved/person-health/person-health-sections/health-examination/health-examination.component';
// import { MedicalConditionsComponent } from './intake-persons-involved/person-health/person-health-sections/medical-conditions/medical-conditions.component';
// import { BehavioralHealthInfoComponent } from './intake-persons-involved/person-health/person-health-sections/behavioral-health-info/behavioral-health-info.component';
// import { HistoryOfAbuseComponent } from './intake-persons-involved/person-health/person-health-sections/history-of-abuse/history-of-abuse.component';
// import { SubstanceAbuseComponent } from './intake-persons-involved/person-health/person-health-sections/substance-abuse/substance-abuse.component';
//import { WorkProfileComponent } from './intake-persons-involved/work-profile/work-profile.component';
//import { ProviderInformationCwComponent } from './intake-persons-involved/person-health/person-health-sections/provider-information-cw/provider-information-cw.component';
//import { DentalInformationCwComponent } from './intake-persons-involved/person-health/person-health-sections/dental-information-cw/dental-information-cw.component';
// import { IntakeDjsNotesComponent } from './intake-djs-notes/intake-djs-notes.component';
// import { IntakeSocialHistoryInrComponent } from './intake-social-history-inr/intake-social-history-inr.component';
// import { ShirInfoComponent } from './intake-social-history-inr/shir-info/shir-info.component';
// import { ShirPriorOffenceRecordComponent } from './intake-social-history-inr/shir-prior-offence-record/shir-prior-offence-record.component';
// import { ShirOffenceCircumstancesComponent } from './intake-social-history-inr/shir-offence-circumstances/shir-offence-circumstances.component';
// import { ShirPpiRelationshipComponent } from './intake-social-history-inr/shir-ppi-relationship/shir-ppi-relationship.component';
// import { ShirBioProfileComponent } from './intake-social-history-inr/shir-bio-profile/shir-bio-profile.component';
// import { ShirMedicalProfileComponent } from './intake-social-history-inr/shir-medical-profile/shir-medical-profile.component';
// import { ShirEducationEmploymentComponent } from './intake-social-history-inr/shir-education-employment/shir-education-employment.component';
// import { ShirHomeCommunityEnviComponent } from './intake-social-history-inr/shir-home-community-envi/shir-home-community-envi.component';
// import { ShirHouseholdFamilyComponent } from './intake-social-history-inr/shir-household-family/shir-household-family.component';
// import { ShirPlacementHistoryComponent } from './intake-social-history-inr/shir-placement-history/shir-placement-history.component';
// import { ShirSummaryRecommendatiomComponent } from './intake-social-history-inr/shir-summary-recommendatiom/shir-summary-recommendatiom.component';
// import {InsuranceInformationCwComponent} from "./intake-persons-involved/person-health/person-health-sections/insurance-information-cw/insurance-information-cw.component";
// import {MedicationIncludingPsychotropicCwComponent} from "./intake-persons-involved/person-health/person-health-sections/medication-including-psychotropic-cw/medication-including-psychotropic-cw.component";
// import {BehavioralHealthInfoCwComponent} from "./intake-persons-involved/person-health/person-health-sections/behavioral-health-info-cw/behavioral-health-info-cw.component";
// import {MedicalConditionsCwComponent} from "./intake-persons-involved/person-health/person-health-sections/medical-conditions-cw/medical-conditions-cw.component";
import { AddReferralComponent } from './add-referral/add-referral.component';
import { IntakeReferralComponent } from './intake-referral/intake-referral.component';
import { ReferralSearchComponent } from './referral-search/referral-search.component';
import { ReferralDecisionComponent } from './referral-decision/referral-decision.component';
import { ReferralProfileComponent } from './referral-profile/referral-profile.component';
import { SharedComponentsModule } from '../../../shared/shared-components/shared-components.module';

@NgModule({
    imports: [
        CommonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatRadioModule,
        MatTabsModule,
        MatTooltipModule,
        MatCheckboxModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatChipsModule,
        MatIconModule,
        MyNewintakeRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        TimepickerModule,
        ControlMessagesModule,
        SharedDirectivesModule,
        SharedPipesModule,
        NgSelectModule,
        ImageCropperModule,
        SortTableModule,
        MatAutocompleteModule,
        NgxMaskModule.forRoot(),
        TrumbowygNgxModule.withConfig({
            svgPath: '../../../../assets/images/icons.svg'
        }),
        A2Edatetimepicker,
        NgxMaskModule.forRoot(),
        PopoverModule,
        NgxfUploaderModule.forRoot(),
        QuillModule,
        SharedComponentsModule
    ],
    declarations: [
        NewReferralComponent,
        NewintakeNarrativeComponent,
        // IntakePersonsInvolvedComponent,
        // IntakeEntitiesComponent,
        // IntakeServiceSubtypeComponent,
        // IntakeAssessmentComponent,
        // IntakeCrossRefferenceComponent,
        // IntakeCommunicationsComponent,
        IntakeAttachmentsComponent,
        IntakePersonSearchComponent,
        AudioRecordComponent,
        // CitizenshipDetailsComponent,
        VideoRecordComponent,
        ImageRecordComponent,
        AttachmentUploadComponent,
        AttachmentDetailComponent,
        EditAttachmentComponent,
        IntakeDispositionComponent,
        IntakeEvaluationFieldsComponent,
        IntakeComplaintTypeComponent,
        PdfPeaceOrderAppealLetterComponent,
        IntakeSdmComponent,
        // PersonHealthComponent,
        // PersonHealthSectionLogComponent,
        // EducationalProfileComponent,
        // IntakeServiceTypeComponent,
        // IntakeAppointmentsComponent,
        // IntakeDecisionComponent,
        // IntakeSaoCourtDetailsComponent,
        // IntakeSaoPetitionDetailsComponent,
        // NoticePreintakeLetterComponent,
        // IntakeDjsNotesComponent,
        // CitizenshipDetailsComponent,
        // NoticePreintakeLetterComponent,
        // PhysicianInformationComponent,
        // HealthInsuranceInformationComponent,
        // MedicationIncludingPsychotropicComponent,
        // HealthExaminationComponent,
        // MedicalConditionsComponent,
        // BehavioralHealthInfoComponent,
        // HistoryOfAbuseComponent,
        // SubstanceAbuseComponent,
        //ProviderInformationCwComponent,
        //DentalInformationCwComponent,
        // InsuranceInformationCwComponent,
        // MedicationIncludingPsychotropicCwComponent,
        // BehavioralHealthInfoCwComponent,
        // MedicalConditionsCwComponent,
        //WorkProfileComponent,
        // IntakeSocialHistoryInrComponent,
        // ShirInfoComponent,
        // ShirPriorOffenceRecordComponent,
        // ShirOffenceCircumstancesComponent,
        // ShirPpiRelationshipComponent,
        // ShirBioProfileComponent,
        // ShirMedicalProfileComponent,
        // ShirEducationEmploymentComponent,
        // ShirHomeCommunityEnviComponent,
        // ShirHouseholdFamilyComponent,
        // ShirPlacementHistoryComponent,
        // ShirSummaryRecommendatiomComponent,
        AddReferralComponent,
        IntakeReferralComponent,
        ReferralSearchComponent,
        ReferralDecisionComponent,
        ReferralProfileComponent
    ],
    exports: [NewReferralComponent],
    providers: [SpeechRecognitionService, SpeechRecognizerService, NgxfUploaderService]
})
export class NewReferralModule {}
