import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Observable, Subject } from 'rxjs/Rx';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PetitionDetails, EvaluationFields } from '../_entities/newintakeSaveModel';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../../@core/services';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'intake-sao-petition-details',
  templateUrl: './intake-sao-petition-details.component.html',
  styleUrls: ['./intake-sao-petition-details.component.scss']
})
export class IntakeSaoPetitionDetailsComponent implements OnInit {

  hearingType$: Observable<any[]>;
  petitionDetailsForm: FormGroup;
  times: string[] = [];
  intakeNumber: string;
  PetitionDetails: PetitionDetails;
  saoTabIndex: number;
  evalFields: EvaluationFields;

  constructor(private _commonHttpService: CommonHttpService, private _formBuilder: FormBuilder,  private route: ActivatedRoute, private _alertService: AlertService) { }
  @Input() petitionDetailsInputSubject$ = new Subject<PetitionDetails>();
  @Input() petitionDetailsOutputSubject$ = new Subject<PetitionDetails>();
  @Input() evalFieldsInputSubject$ = new Subject<EvaluationFields>();
  @Input() saoTabIndex$ = new Subject<number>();

  ngOnInit() {
    //this.loadDroddowns();
    this.initiateFormGroup();
    this.route.params.subscribe((item) => {
      this.intakeNumber = item['id']; });

    this.petitionDetailsForm.valueChanges.subscribe((val) => {
      this.petitionDetailsInputSubject$.next(this.petitionDetailsForm.getRawValue());
    });
    if (this.saoTabIndex$) {
    this.saoTabIndex$.subscribe((data) => {
      this.saoTabIndex = data;
      if (this.saoTabIndex > 2) {
        this.petitionDetailsForm.disable();
      }
    }); }
    this.petitionDetailsOutputSubject$.subscribe((data) => {
      this.patchForm(data);

    });
    this.times = this.generateTimeList(true);
    this.evalFieldsInputSubject$.subscribe((evalFileds) => {
      this.evalFields = evalFileds;
       this.petitionDetailsForm.patchValue({complaintid: this.evalFields ? this.evalFields.complaintid : ''});
    });
  }
  initiateFormGroup() {
    this.petitionDetailsForm = this._formBuilder.group({

      intakenumber: '',
      petitionid: '',
      associatedattorneys: '',
      complaintid: this.evalFields ? this.evalFields.complaintid : '',
      transferpetitionid: '',
      petitionfiled: 0,
      hearingdatetime: '',
      hearingtypekey: '',
      hearingnotes: '',
      hearingDate: '',
      hearingTime: ''

    });
  }

  private generateTimeList(is24hrs = true) {
    const x = 15; // minutes interval
    const times = []; // time array
    let tt = 0; // start time
    const ap = [' AM', ' PM']; // AM-PM

    // loop to increment the time and push results in array
    for (let i = 0; tt < 24 * 60; i++) {
      const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
      const mm = (tt % 60); // getting minutes of the hour in 0-55 format
      if (is24hrs) {
        times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
      } else {
        times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
      }
      tt = tt + x;
    }
    return times;
  }
  private loadDroddowns() {

    const source = forkJoin([
      this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.hearingType),
    ])
      .map(([hearingType]) => {
        return {
          hearingType: hearingType.map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.hearingtypekey
              })
          )
        };
      })
      .share();
    this.hearingType$ = source.pluck('hearingType');

  }
  patchForm(data: PetitionDetails) {
    if (data) {
      this.petitionDetailsForm.patchValue(data);
    }
  }
  private submitPetitionDetails() {
    this.PetitionDetails = this.petitionDetailsForm.getRawValue();
    this.PetitionDetails.intakenumber = this.intakeNumber;
    this._commonHttpService.create(this.PetitionDetails, NewUrlConfig.EndPoint.Intake.addpetition).subscribe(
      (response) => {
        // console.log(response);
        this._alertService.success('Petition details saved successfully!');
      });
  }
}
