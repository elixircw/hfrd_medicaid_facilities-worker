import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralDecisionComponent } from './referral-decision.component';

describe('ReferralDecisionComponent', () => {
  let component: ReferralDecisionComponent;
  let fixture: ComponentFixture<ReferralDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
