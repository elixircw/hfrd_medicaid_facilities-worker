import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { AppUser } from '../../../../@core/entities/authDataModel';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { AuthService, CommonHttpService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { ValidationService } from '../../../../@core/services/validation.service';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { ContactTypeAdd, IntakeContactRole, IntakeContactRoleType, ProgressNoteRoleType, IntakeFileAttachement } from '../_entities/newintakeModel';
import { Attachment } from '../intake-attachments/_entities/attachmnt.model';

declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-communications',
    templateUrl: './intake-communications.component.html',
    styleUrls: ['./intake-communications.component.scss']
})
export class IntakeCommunicationsComponent implements OnInit {
    @Input()
    contactListAdded$ = new Subject<ContactTypeAdd[]>();
    @Input()
    contactListOutput$ = new Subject<ContactTypeAdd[]>();
    noteForm: FormGroup;
    updateAppend: FormGroup;
    @Input()
    agencyType$ = new Subject<string>();
    @Input()
    addAttachementSubject$ = new Subject<Attachment[]>();
    duration: string;
    multipleRoles: string;
    contactType: string;
    selectedAgency: string;
    contactSubtypeLabel: string;
    editViewNoteLabel: string;
    roleName: AppUser;
    recordingDetail: ContactTypeAdd = new ContactTypeAdd();
    progressNoteRoleType: ProgressNoteRoleType[] = [];
    contactListAdded: ContactTypeAdd[] = [];
    conctactType$: Observable<IntakeContactRoleType[]>;
    contactRoles$: Observable<IntakeContactRole[]>;
    contactSubTypeList$: Observable<IntakeContactRoleType[]>;
    fileAttachment: IntakeFileAttachement;
    attachmentTitle: string;
    constructor(private _alertService: AlertService, private _commonHttpService: CommonHttpService, private formBuilder: FormBuilder, private _authService: AuthService) {}

    ngOnInit() {
        this.conctatTypeDropDown();
        this.formIntilizer();
        this.roleName = this._authService.getCurrentUser();
        this.contactListOutput$.subscribe((res) => (this.contactListAdded = res));
    }
    formIntilizer() {
        this.noteForm = this.formBuilder.group(
            {
                RecordingType: ['', Validators.required],
                progressnotesubtypeid: [null],
                contactdate: ['', Validators.required],
                contactname: ['', Validators.required],
                contactroletypekey: ['', Validators.required],
                documentpropertiesid: [''],
                contactphone: [''],
                contactemail: ['', ValidationService.mailFormat],
                attemptindicator: [false],
                description: ['', Validators.required],
                starttime: [''],
                endtime: ['']
            },
            { validator: this.checkTimeValidation }
        );
        this.updateAppend = this.formBuilder.group({
            appendtitle: ['', Validators.required]
        });
        this.agencyType$.subscribe((res) => (this.selectedAgency = res));
    }

    contactSubtype(event) {
        if (event) {
            const contact = event.label;
            this.contactType = contact.trim();
            this.contactSubTypeList$ = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.ContactSubTypeUrl + '?prognotetypeid=' + event.value).map((result) => {
                return result;
            });
        }
    }
    timeDuration() {
        const start_date = moment(this.noteForm.value.starttime, 'HH:mm a');
        const end_date = moment(this.noteForm.value.endtime, ' HH:mm a');
        const duration = moment.duration(end_date.diff(start_date));
        if (duration['_data'] && this.noteForm.value.starttime && this.noteForm.value.endtime) {
            this.duration = duration['_data'].hours + ' Hr :' + duration['_data'].minutes + ' Min';
        }
    }
    checkTimeValidation(group: FormGroup) {
        if (!group.controls.endtime.value || group.controls.endtime.value !== '') {
            if (group.controls.endtime.value < group.controls.starttime.value) {
                return { notValid: true };
            }
            return null;
        }
    }
    selectRoleType(event) {
        if (event) {
            this.multipleRoles = '';
            const progressNoteRoleType = event.map((res) => {
                if (this.multipleRoles === '') {
                    this.multipleRoles = this.multipleRoles + '' + res.contactroletypekey;
                } else {
                    this.multipleRoles = this.multipleRoles + ', ' + res.contactroletypekey;
                }
                return { contactroletypekey: res.contactroletypekey };
            });
            this.progressNoteRoleType = progressNoteRoleType;
        }
    }
    subtypeName(event) {
        const subType = event.label;
        this.contactSubtypeLabel = subType.trim();
    }
    addAttachement(event) {
        this.attachmentTitle = event.label;
    }
    saveNote(recording: ContactTypeAdd) {
        if (this.noteForm.valid) {
            if (recording.contactdate) {
                const event = new Date(recording.contactdate);
                recording.contactdate = event.getMonth() + 1 + '/' + event.getDate() + '/' + event.getFullYear();
            }
            recording.entitytypeid = null;
            recording.entitytype = 'intakeservicerequest';
            recording.progressnotetypekey = 'Manual';
            recording.savemode = 1;
            recording.description = this.noteForm.value.description;
            recording.contactroletypekey = this.progressNoteRoleType;
            recording.recordingtype = this.contactType;
            recording.recordingsubtype = this.contactSubtypeLabel;
            recording.recordingdate = new Date();
            recording.duriation = this.duration;
            recording.team = this.selectedAgency ? this.selectedAgency : 'All';
            recording.title = this.attachmentTitle ? this.attachmentTitle : '';
            // recording.s3bucketpathname = this.fileAttachment.s3bucketpathname ? this.fileAttachment.s3bucketpathname : '';
            if (this.roleName.role.name === 'apcs') {
                recording.author = 'Supervisor';
            } else if (this.roleName.role.name === 'SCRNW') {
                recording.author = 'Screening Worker';
            } else if (this.roleName.role.name === 'Intake Worker') {
                recording.author = 'Intake Worker';
            }
            recording.multipleRoles = this.multipleRoles;
            // this.noteDetails = Object.assign({
            //     description: recording.description,
            //     contactdate: recording.contactdate,
            //     author: recording.author
            // });
            // this.multipleNotesDetail.push(this.noteDetails);
            if (recording) {
                this.contactListAdded.push(recording);
                this.contactListAdded$.next(this.contactListAdded);
                (<any>$('#add-notes')).modal('hide');
                this.noteForm.reset();
                this.duration = '';
                this._alertService.success('Notes added successfully!');
            } else {
                this._alertService.warn('Please check input data');
            }
        }
    }
    editNoteList(model) {
        this.recordingDetail = model;
    }
    clearNotes() {
        this.noteForm.reset();
        this.updateAppend.reset();
        this.duration = '';
    }
    // updateNote(model) {
    //     this.noteDetails = Object.assign(
    //         {},
    //         {
    //             description: model.appendtitle,
    //             contactdate: new Date(),
    //             author: this.roleName.role.name
    //         }
    //     );
    //     this.multipleNotesDetail.push(this.noteDetails);
    //     (<any>$('#myModal-recordings-edit')).modal('hide');
    //     this.updateAppend.reset();
    // }
    private conctatTypeDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                }),
                NewUrlConfig.EndPoint.Intake.ListProgressTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                }),
                NewUrlConfig.EndPoint.Intake.ContactRoleTypeUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    recordingType: result[0],
                    contactRoles: result[1]
                };
            })
            .share();
        this.conctactType$ = source.pluck('recordingType');
        this.contactRoles$ = source.pluck('contactRoles');
    }
}
