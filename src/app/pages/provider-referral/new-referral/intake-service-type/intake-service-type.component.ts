import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import {
    DropdownModel,
    PaginationRequest
} from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { Subject } from 'rxjs/Rx';
import {
  ComplaintTypeCase,
  ChoosenAllegation
} from '../_entities/newintakeSaveModel';
import { AuthService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import {
  IntakePurpose,
  SubType,
  AllegationItem,
  Allegation
} from '../_entities/newintakeModel';
import { REGEX } from '../../../../@core/entities/constants';
import { AlertService } from '../../../../@core/services/alert.service';
import * as reason from './_configurations/reason.json';
declare var $: any;
@Component({
  selector: 'intake-service-type',
  templateUrl: './intake-service-type.component.html',
  styleUrls: ['./intake-service-type.component.scss']
})
export class IntakeServiceTypeComponent implements OnInit {

  serviceTypes$: Observable<DropdownModel[]>;
  subServiceTypes$: Observable<DropdownModel[]>;
  servicetype: DropdownModel;
  createdCases: ComplaintTypeCase[] = [];
  caseCreationFormGroup: FormGroup;
  caseEditFormGroup: FormGroup;
  selectedPurpose: IntakePurpose;
  purposeList: IntakePurpose[];
  subServiceTypes: SubType[];
  editCase: ComplaintTypeCase;
  deleteCaseID: string;
  roleId: AppUser;
  token: AppUser;
  reasonDropdown: DropdownModel[];
  allegationDropDownItems: DropdownModel[];
  allegationItems: AllegationItem[];
  @Input() purposeInputSubject$ = new Subject<IntakePurpose>();
  @Input() createdCaseInputSubject$ = new Subject<ComplaintTypeCase[]>();
  @Input() createdCaseOuptputSubject$ = new Subject<ComplaintTypeCase[]>();
  @Input() reviewstatus: string;
  filteredAllegationItems: AllegationItem[] = [];
  choosenAllegation: ChoosenAllegation;


  // selectedDaTypeKeyforEdit: string;
  selectedcaseIndex: number;
  groupSetupForm: FormGroup;
  groupDAReasonForm: FormGroup;

  groupSelectedDAIds: string[];
  ungroupSelectedGroupIds: string[];

  constructor(
      private _commonHttpService: CommonHttpService,
      private formBuilder: FormBuilder,
      private _alertService: AlertService,
      private _authService: AuthService
  ) {}

  ngOnInit() {
    this.buildFormGroup();
    this.roleId = this._authService.getCurrentUser();
    this.token = this._authService.getCurrentUser();
    this.reasonDropdown = <any>reason;
    this.purposeInputSubject$.subscribe(purpose => {
        this.selectedPurpose = purpose;
        if (purpose.teamtype) {
            this.listServicetypes(purpose.teamtype.teamtypekey);
        }

        if (purpose.intakeservreqtypeid) {
            this.listServiceSubtype(purpose.intakeservreqtypeid);
        }
        this.caseCreationFormGroup.patchValue({
            serviceType: this.selectedPurpose.intakeservreqtypeid
        });
    });
    this.createdCaseOuptputSubject$.subscribe(createdCases => {
        if (createdCases) {
            this.createdCases = createdCases;
            this.createdCaseInputSubject$.next(this.createdCases);
        } else {
            this.createdCases = [];
        }
    });
  }

  buildFormGroup() {
    this.caseCreationFormGroup = this.formBuilder.group({
        serviceType: ['', Validators.required],
        subServiceType: ['', Validators.required],
        allegationId: ['']
    });
    this.caseEditFormGroup = this.formBuilder.group({
        serviceType: ['', Validators.required],
        subServiceType: ['', Validators.required]
    });
    this.groupSetupForm = this.formBuilder.group({
        DAItems: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
        selectedDAItemsGroupNo: ['']
    });
    this.groupDAReasonForm = this.formBuilder.group({
        reasonType: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
        reasonComments: ['']
    });
}

listServicetypes(teamtypekey) {
    const checkInput = {
        nolimit: true,
        where: { teamtypekey: teamtypekey },
        method: 'get',
        order: 'description'
    };

    this.serviceTypes$ = this._commonHttpService
        .getArrayList(
            new PaginationRequest(checkInput),
            NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter'
        )
        .map(result => {
            this.purposeList = result;
            return result.map(
                res =>
                    new DropdownModel({
                        text: res.description,
                        value: res.intakeservreqtypeid
                    })
            );
        });
}

getSelectedPurpose(purposeID): IntakePurpose {
    return this.purposeList.find(
        puroposeItem => puroposeItem.intakeservreqtypeid === purposeID
    );
}

getSelectedSubtype(subTypeID): SubType {
    return this.subServiceTypes.find(
        subServiceType =>
            subServiceType.servicerequestsubtypeid === subTypeID
    );
}

listServiceSubtype(intakeservreqtypeid) {
    const checkInput = {
        include: 'servicerequestsubtype',
        nolimit: true,
        where: { intakeservreqtypeid: intakeservreqtypeid },
        method: 'get'
    };
    this.subServiceTypes$ = this._commonHttpService
        .getArrayList(
            new PaginationRequest(checkInput),
            NewUrlConfig.EndPoint.Intake.DATypeUrl + '/?filter'
        )
        .map(result => {
            this.subServiceTypes = result[0].servicerequestsubtype;
            return result[0].servicerequestsubtype.map(
                res =>
                    new DropdownModel({
                        text: res.description,
                        value: res.servicerequestsubtypeid
                    })
            );
        });
}

loadSubServiceTypes(serviceType: DropdownModel) {
    this.listServiceSubtype(serviceType.value);
}
isSubTypeExists(servicerequestsubtypeid: string) {
    if (this.createdCases) {
        return this.createdCases.find(
            createdCase =>
                createdCase.subServiceTypeID === servicerequestsubtypeid
        );
    }
    return false;
}

addCase() {
    if (this.caseCreationFormGroup.valid) {
        if (
            this.isSubTypeExists(
                this.caseCreationFormGroup.value.subServiceType
            )
        ) {
            this._alertService.error(
                'Already Case created for this sub type '
            );
            return false;
        }
        this._commonHttpService
            .getArrayList({}, NewUrlConfig.EndPoint.Intake.NextnumbersUrl)
            .subscribe(result => {
                const complaintTypeCase: ComplaintTypeCase = new ComplaintTypeCase();
                complaintTypeCase.caseID = result['nextNumber'];
                const selectedPurpose = this.getSelectedPurpose(
                    this.caseCreationFormGroup.value.serviceType
                );
                const selectedSubtype = this.getSelectedSubtype(
                    this.caseCreationFormGroup.value.subServiceType
                );
                complaintTypeCase.serviceTypeID = this.caseCreationFormGroup.value.serviceType;
                complaintTypeCase.serviceTypeValue =
                    selectedPurpose.description;
                complaintTypeCase.subServiceTypeID =
                    selectedSubtype.servicerequestsubtypeid;
                complaintTypeCase.subSeriviceTypeValue =
                    selectedSubtype.description;
                this.createdCases.push(complaintTypeCase);
                this.updateCreatedCases();
            });
    }
}

deleteCase(intakeCase: ComplaintTypeCase) {
    this.deleteCaseID = intakeCase.caseID;
}

onEditCase(intakeCase: ComplaintTypeCase) {
    this.caseEditFormGroup.patchValue({
        serviceType: intakeCase.serviceTypeID,
        subServiceType: intakeCase.subServiceTypeID
    });
    this.editCase = intakeCase;
}

updateCreatedCases() {
    this.createdCaseInputSubject$.next(this.createdCases);
}
isUpdateDisabled() {
    if (this.editCase) {
        return (
            this.caseEditFormGroup.value.subServiceType ===
            this.editCase.subServiceTypeID
        );
    }
    return false;
}
updateSubTypes() {
    if (this.caseEditFormGroup.valid) {
        if (
            this.isSubTypeExists(
                this.caseEditFormGroup.value.subServiceType
            )
        ) {
            this._alertService.error(
                'Already Case created for this sub type '
            );
            return false;
        }
        this.createdCases.find(complaintTypeCase => {
            if (complaintTypeCase.caseID === this.editCase.caseID) {
                const selectedPurpose = this.getSelectedPurpose(
                    this.caseEditFormGroup.value.serviceType
                );
                const selectedSubtype = this.getSelectedSubtype(
                    this.caseEditFormGroup.value.subServiceType
                );
                complaintTypeCase.serviceTypeID = this.caseEditFormGroup.value.serviceType;
                complaintTypeCase.serviceTypeValue =
                    selectedPurpose.description;
                complaintTypeCase.subServiceTypeID =
                    selectedSubtype.servicerequestsubtypeid;
                complaintTypeCase.subSeriviceTypeValue =
                    selectedSubtype.description;
                $('#editClose').click();
                return true;
            }
        });
    }
}

deleteCaseConfirm() {
    this.createdCases = this.createdCases.filter(
        createdCase => createdCase.caseID !== this.deleteCaseID
    );
    this.updateCreatedCases();
    (<any>$('#delete-case-popup')).modal('hide');
}

isPeaceOrder(complaintCase: ComplaintTypeCase): boolean {
  return complaintCase.subSeriviceTypeValue === 'Peace Order';
}

loadAllegation(caseIndex: number, daTypeKey: string) {
  this.selectedcaseIndex = caseIndex;
  this.choosenAllegation = new ChoosenAllegation();

  this.allegationItems = [];
  this.allegationDropDownItems = [];
  this.filteredAllegationItems = [];
  this.caseCreationFormGroup.patchValue({
      allegationId: ''
  });
  const url =
      NewUrlConfig.EndPoint.Intake.AllegationsIndicaorUrl + '?filter';
  this._commonHttpService
      .getArrayList(
          {
              where: { intakeservreqtypeid: daTypeKey },
              method: 'get'
          },
          url
      )
      .subscribe(result => {
          this.allegationItems = result.map(
              item => new AllegationItem(item)
          );
          this.allegationDropDownItems = this.allegationItems.map(
              item =>
                  new DropdownModel({
                      text: item.name,
                      value: item.allegationid
                  })
          );
      });
}

allegationOnChange(option: any) {
  this.loadSelectedAllegation(option.value);
  this.choosenAllegation.allegationID = option.value;
  this.choosenAllegation.allegationValue = option.label;
}

loadSelectedAllegation(selectedAllegation: string) {
  this.filteredAllegationItems = this.allegationItems.filter(item => {
      return item.allegationid === selectedAllegation;
  });
}

indicatorsChecked(model: string, control) {
  if (control.target.checked) {
      this.choosenAllegation.indicators.push(model);
  } else {
      const index = this.choosenAllegation.indicators.indexOf(model);
      this.choosenAllegation.indicators.splice(index, 1);
  }
}

saveAllegations() {
  let ispresent = false;
  this.createdCases[this.selectedcaseIndex].choosenAllegation.forEach(
      element => {
          if (
              this.choosenAllegation.allegationID === element.allegationID
          ) {
              element.indicators = this.choosenAllegation.indicators;
              ispresent = true;
          }
      }
  );
  if (!ispresent) {
      this.createdCases[this.selectedcaseIndex].choosenAllegation.push(
          this.choosenAllegation
      );
  }
}

deleteAllegation(caseIndex: number, aligationIndex: number) {
//   console.log(caseIndex, aligationIndex);
  this.createdCases[caseIndex].choosenAllegation.splice(
      aligationIndex,
      1
  );
}

openGroupSetup() {
  if (
      this.groupSetupForm.value.DAItems &&
      this.groupSetupForm.value.DAItems.length >= 2 &&
      this.createdCases.filter(item => !item.GroupNumber).length >= 2
  ) {
      (<any>$('#serviceGroupReason')).modal('show');
  } else {
      this._alertService.error('Please select at least 2 Case');
  }
}

cancelGroupSetup() {
  this.groupDAReasonForm.reset();
  this.groupSetupForm.reset();
  (<any>$('#serviceGroupReason')).modal('hide');
}

saveGroupSetup() {
  if (
      this.groupSetupForm.value.DAItems &&
      (this.groupDAReasonForm.valid && this.groupDAReasonForm.dirty)
  ) {
      this.groupSelectedDAIds = this.groupSetupForm.value.DAItems;
      this._commonHttpService
          .getArrayList(
              {},
              'Nextnumbers/getNextNumber?apptype=GroupAuthorizationNumber'
          )
          .subscribe(result => {
              this.createdCases = this.createdCases.map(item => {
                  this.groupSelectedDAIds.map(da => {
                      if (item.caseID === da) {
                          item.GroupNumber = result['nextNumber'];
                          item.GroupReasonType = this.groupDAReasonForm.value.reasonType;
                          item.GroupComment = this.groupDAReasonForm.value.reasonComments;
                      }
                  });
                  return item;
              });
              this.createdCaseInputSubject$.next(this.createdCases);
              this.cancelGroupSetup();
              return { nextNumber: result };
          });
  } else {
      this._alertService.error('Please select Grouping reason type');
  }
}

unGroupDAItems() {
  if (this.groupSetupForm.value.selectedDAItemsGroupNo) {
      this.ungroupSelectedGroupIds = this.groupSetupForm.value.selectedDAItemsGroupNo;
      this.createdCases = this.createdCases.map(item => {
          this.ungroupSelectedGroupIds.map(da => {
              if (item.GroupNumber === da) {
                  item.GroupNumber = '';
                  item.GroupReasonType = '';
                  item.GroupComment = '';
              }
          });
          return item;
      });
      this.createdCaseInputSubject$.next(this.createdCases);
  }
}

}
