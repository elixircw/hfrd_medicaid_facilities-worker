import { Component, OnInit, Input } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Observable, Subject } from 'rxjs/Rx';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../@core/services/auth.service';
import { CourtDetails, PetitionDetails } from '../_entities/newintakeSaveModel';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../../@core/services';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-sao-court-details',
    templateUrl: './intake-sao-court-details.component.html',
    styleUrls: ['./intake-sao-court-details.component.scss']
})
export class IntakeSaoCourtDetailsComponent implements OnInit {

    offenceCategories$: Observable<any[]>;
    courtActionsType$: Observable<any[]>;
    conditionTypes$: Observable<any[]>;
    courtOrder$: Observable<any[]>;
    findingsType$: Observable<any[]>;
    adijuctedDecisions$: Observable<any[]>;
    @Input() offenceCategoriesInputSubject$ = new Subject<any>();
    courtDetailsForm: FormGroup;
    roleDetails: any;
    CourtDetails: CourtDetails;
    userInfo: any;
    intakeNumber: string;
    saoTabIndex: number;
    times: string[] = [];

    constructor(private _commonHttpService: CommonHttpService, private _formBuilder: FormBuilder, private _authService: AuthService, private route: ActivatedRoute,
        private _alertService: AlertService) { }

    // @Input() courtDetailsInputSubject$ = new Subject<CourtDetails>();
    // @Input() courtDetailsOutputSubject$ = new Subject<CourtDetails>();

    @Input() petitionDetailsInputSubject$ = new Subject<PetitionDetails>();
    @Input() saoTabIndex$ = new Subject<number>();
    petitionDetails: PetitionDetails;
    ngOnInit() {

        //this.loadDroddowns();
        this.userInfo = this._authService.getCurrentUser();
        this.initiateFormGroup();
        this.route.params.subscribe((item) => {
            this.intakeNumber = item['id'];
        });
        // this.courtDetailsForm.valueChanges.subscribe((val) => {
        //     this.courtDetailsInputSubject$.next(this.courtDetailsForm.getRawValue());
        // });
        // this.courtDetailsOutputSubject$.subscribe((data) => {
        //     this.patchForm(data);
        // });
        this.petitionDetailsInputSubject$.subscribe((petitionDetails) => {
            this.petitionDetails = petitionDetails;
            this.courtDetailsForm.patchValue({
                hearingdatetime: this.petitionDetails ? this.petitionDetails.hearingdatetime : '',
                hearingTime: this.petitionDetails ? this.petitionDetails.hearingTime : ''
            });
        });
        if (this.saoTabIndex$) {
            this.saoTabIndex$.subscribe((data) => {
                this.saoTabIndex = data;
                if (this.saoTabIndex > 5) {
                    this.courtDetailsForm.disable();
                }
            });
        }
        this.offenceCategoriesInputSubject$.subscribe(offenceCategories => {
            if (offenceCategories) {
                this.offenceCategories$ = Observable.of(offenceCategories);
            }

        });
        this.times = this.generateTimeList(true);
        // this.getPage(1);
    }

    initiateFormGroup() {
        this.courtDetailsForm = this._formBuilder.group({
            legalcounselname: '',
            magistratename: '',
            intakenumber: '',
            workername: this.userInfo ? this.userInfo.user.userprofile.fullname : '',
            hearingdatetime: this.petitionDetails ? this.petitionDetails.hearingdatetime : '',
            courtactiontypekey: '',
            courtordertypekey: '',
            courtorderdatetime: '',
            conditiontypekey: '',
            conditiontypedescription: '',
            conditiontypecompletiondatetime: '',
            terminationdatetime: '',
            adjudicationdatetime: '',
            adjudicationdecision: '',
            allegationid: '',
            adjudicationDate: '',
            adjudicationTime: '',
            hearingDate: '',
            hearingTime: this.petitionDetails ? this.petitionDetails.hearingTime : '',
            terminationDate: '',
            terminationTime: '',
            orderDate: '',
            orderTime: '',
            completionDate: '',
            completionTime: ''



        });
    }
    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = (tt % 60); // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }
    private loadDroddowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.courtActionType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.conditionType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.courtOrderType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.findingType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.adjuctedDecisions)
        ])
            .map(([courtActionsType, conditionTypes, courtOrder, findingsType, adijuctedDecisions]) => {
                return {
                    courtActionsType: courtActionsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtactiontypekey
                            })
                    ),
                    conditionTypes: conditionTypes.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.conditiontypekey
                            })
                    ),
                    courtOrder: courtOrder.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtordertypekey
                            })
                    ),
                    findingsType: findingsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.findingtypeid
                            })
                    ),
                    adijuctedDecisions: adijuctedDecisions.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.adjudicateddecisiontypekey
                            })
                    )
                };
            })
            .share();
        this.courtActionsType$ = source.pluck('courtActionsType');
        this.conditionTypes$ = source.pluck('conditionTypes');
        this.courtOrder$ = source.pluck('courtOrder');
        this.findingsType$ = source.pluck('findingsType');
        this.adijuctedDecisions$ = source.pluck('adijuctedDecisions');

    }
    patchForm(data: CourtDetails) {
        if (data) {
            this.courtDetailsForm.patchValue(data);
        }
    }
    private submitCourtDetails() {
        this.CourtDetails = this.courtDetailsForm.getRawValue();
        // this.CourtDetails.conditiontypecompletiondatetime = this.mergeDateTime(this.CourtDetails.conditiontypecompletiondatetime, this.CourtDetails.completionTime);
        // this.CourtDetails.terminationdatetime = this.mergeDateTime(this.CourtDetails.terminationdatetime, this.CourtDetails.terminationTime);
        // this.CourtDetails.adjudicationdatetime = this.mergeDateTime(this.CourtDetails.adjudicationdatetime, this.CourtDetails.adjudicationTime);
        // this.CourtDetails.hearingdatetime = this.mergeDateTime(this.CourtDetails.hearingdatetime, this.CourtDetails.hearingTime);
        // this.CourtDetails.courtorderdatetime = this.mergeDateTime(this.CourtDetails.courtorderdatetime, this.CourtDetails.orderTime);
        this.CourtDetails.intakenumber = this.intakeNumber;
        this._commonHttpService.create(this.CourtDetails, NewUrlConfig.EndPoint.Intake.addcourtdetails).subscribe(
            (response) => {
                // console.log(response);
                this._alertService.success('Court details saved successfully!');
            });
    }
    private mergeDateTime(dateobj, timeobj) {
        const Dateobj: any = new Date(dateobj);
        const timeSplit = timeobj.split(':');
        const TimeHour = timeSplit[0];
        const TimeMin = timeSplit[1];

        Dateobj.setHours(TimeHour);
        Dateobj.setMinutes(TimeMin);

        return Dateobj;
    }
}
