import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeSaoCourtDetailsComponent } from './intake-sao-court-details.component';

describe('IntakeSaoCourtDetailsComponent', () => {
  let component: IntakeSaoCourtDetailsComponent;
  let fixture: ComponentFixture<IntakeSaoCourtDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeSaoCourtDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeSaoCourtDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
