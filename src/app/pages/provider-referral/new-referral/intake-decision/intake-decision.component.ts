import value from '*.json';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { DispositionCode, DispostionOutput, InvolvedPerson, PreIntakeDisposition, AssessmentScores } from '../_entities/newintakeModel';
import { ComplaintTypeCase, DelayForm, DelayResponse, General, IntakeAppointment, MembersInMeeting, EvaluationFields } from '../_entities/newintakeSaveModel';
import { NewUrlConfig } from './../../provider-referral-url.config';
import * as reason from './_configurations/reason.json';
import { DispositionConfig } from './_configurations/reason';
import { RoutingUser } from '../../../cjams-dashboard/_entities/dashBoard-datamodel';
import { Router } from '@angular/router';
declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-decision',
    templateUrl: './intake-decision.component.html',
    styleUrls: ['./intake-decision.component.scss']
})
export class IntakeDecisionComponent implements OnInit, AfterViewInit, AfterViewChecked {
    @Input() dispositionInput$ = new Subject<string>();
    @Input() dispositionOutPut$ = new Subject<DispostionOutput[]>();
    @Input() dispositionRetrive$ = new Subject<DispostionOutput[]>();
    @Input() timeReceived$ = new Subject<string>();
    @Input() purposeToDispOutput$ = new Subject<string>();
    @Input() isDateDelayed$ = new Subject<DelayResponse>();
    @Input() delayformValue$ = new Subject<DelayForm>();
    @Input() general$ = new Subject<General>();
    general: General;
    @Input() agencyStatus$ = new Subject<string>();
    @Input() createdCaseInputSubject$ = new Subject<ComplaintTypeCase[]>();
    @Input() offenceCategoriesInputSubject$ = new Subject<any>();
    // tslint:disable-next-line:no-input-rename
    @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
    // tslint:disable-next-line:no-input-rename
    @Input('appointmentOutputSubject') appointmentOutputSubject$ = new Subject<IntakeAppointment[]>();
    // tslint:disable-next-line:no-input-rename
    @Input('appointmentInputSubject') appointmentInputSubject$ = new Subject<IntakeAppointment[]>();
    // tslint:disable-next-line:no-input-rename
    @Input('isPreIntake') isPreIntake$ = new Subject<boolean>();
    // tslint:disable-next-line:no-input-rename
    @Input('evalFields') evalFields$ = new Subject<EvaluationFields>();
    @Input() intakeNumber = '';
    // tslint:disable-next-line:no-input-rename
    @Input('decision') decision$ = new Subject<string>();
    // tslint:disable-next-line:no-input-rename
    @Input('preIntakedisposition') preIntakedisposition$ = new Subject<PreIntakeDisposition>();
    // tslint:disable-next-line:no-input-rename
    @Input('preIntakedispositionReciv') preIntakedispositionReciv$ = new Subject<PreIntakeDisposition>();
    @Input('scoresSubject') scoresSubject$ = new Subject<AssessmentScores>();

    assmentScores: AssessmentScores;
    evalFields: EvaluationFields;
    isPreIntake = false;
    createdCase: DispostionOutput[];
    offenceCategories$: Observable<DropdownModel[]>;
    offenceCategories: any[] = [];
    statusDropdownItems$: Observable<DropdownModel[]>;
    dispositionList: DispositionCode[];
    dispositionDropDown: DropdownModel[];
    supDispositionDropDown: DropdownModel[];
    disposition: string;
    dispositionDropdownItems$: Observable<DropdownModel[]>;
    dispositionFormGroup: FormGroup;
    preIntakeDispositionFormGroup: FormGroup;
    dispositionDelayForm: FormGroup;
    date = new Date();
    serviceTypeId: string;
    showReason = false;
    supervisorUser: boolean;
    timeReceived: string;
    showStatus = true;
    daTypeSubType: string;
    generalRecievedDate: DelayResponse;
    role: AppUser;
    agencyStatus: string;
    intakePurpose: any;
    showBehalfOfRequester = false;
    actionDropdown: DropdownModel[];
    initRecomentdationDropdown: DropdownModel[];
    supMultipleDispositionDropdown: DropdownModel[];
    intakeWorker: MembersInMeeting;
    intakeWorkerId: string;
    intakeWorkerList: any[] = [];
    mother: InvolvedPerson;
    father: InvolvedPerson;
    youth: InvolvedPerson;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    intakersList$: Observable<RoutingUser[]>;
    intakerinMind: RoutingUser;
    showScreen1 = false;
    showScreen2 = false;
    showScreen3 = false;
    showScreen4 = false;
    commonDispositionStatusData: DispostionOutput;
    jurisdiction = { id: '', name: '' };
    toShowReason = false;
    countyid: string;
    intakeWorkerName: string;
    system = {
        ageInValid: false, youthValid: false, parentsValid: false,
        jurisdictionValid: false, offenceValid: false, status: '', reason: ''
    };

    constructor(private _commonHttpService: CommonHttpService, private formBuilder: FormBuilder, private _authService: AuthService, private _changeDetect: ChangeDetectorRef,
        private _alertService: AlertService,
        private _router: Router) { }

    ngOnInit() {
        // console.log(this.intakeNumber);
        this.loadIntaker(1);
        this.loadOffenceList();
        this.actionDropdown = DispositionConfig.config.action;
        this.initRecomentdationDropdown = DispositionConfig.config.initialRecomendation;
        const role = this._authService.getCurrentUser();
        this.role = role;
        this.setScreenToShow();
        this.agencyStatus$.subscribe((res) => {
            this.agencyStatus = res;
        });
        this.dispositionForm();
        this.dispositionInput$.subscribe((res) => {
            this.serviceTypeId = res;
        });


        this.isPreIntake$.subscribe(data => {
            this.isPreIntake = data;
            this.setScreenToShow();
        });

        this.scoresSubject$.subscribe(data => {
            this.assmentScores = data;
        });

        this.preIntakedispositionReciv$.subscribe(data => {
            if (data) {
                this.system.status = data.systemRecmdatnStatus;
                this.system.reason = data.systemRecmdatn;
                if (data.status === 'Approved') {
                    this.toShowReason = false;
                } else if (data.status === 'Rejected') {
                    this.toShowReason = true;
                }
                this.preIntakeDispositionFormGroup.patchValue({
                    status: data.status,
                    reason: data.reason,
                    comment: data.comment
                });
            }
        });

        this.createdCaseInputSubject$.subscribe((response) => {
            this.dispositionFormGroup.patchValue({
                intakeserreqstatustypekey: ''
            });
            // to get the label for datype
            const checkInput = {
                nolimit: true,
                where: { teamtypekey: 'all' },
                method: 'get',
                order: 'description'
            };
            this._commonHttpService.getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter').subscribe((result) => {
                this.intakePurpose = result;
            });
            // end of - to get the label for datype
            if (response) {
                this.createdCase = [];
                response.map((item) => {
                    const createdCase = {
                        ServiceRequestNumber: item.caseID,
                        DaTypeKey: item.serviceTypeID,
                        subSeriviceTypeValue: item.subSeriviceTypeValue,
                        DasubtypeKey: item.subServiceTypeID,
                        DAStatus: '',
                        DADisposition: '',
                        Summary: '',
                        dispositioncode: item.dispositioncode ? item.dispositioncode : '',
                        intakeserreqstatustypekey: item.intakeserreqstatustypekey ? item.intakeserreqstatustypekey : '',
                        comments: '',
                        ReasonforDelay: '',
                        supStatus: item.supStatus ? item.supStatus : '',
                        supDisposition: item.supDisposition ? item.supDisposition : '',
                        supComments: '',
                        intakeMultipleDispositionDropdown: [],
                        supMultipleDispositionDropdown: [],
                        GroupNumber: item.GroupNumber ? item.GroupNumber : null,
                        GroupReasonType: item.GroupReasonType ? item.GroupReasonType : null,
                        GroupComment: item.GroupComment ? item.GroupComment : null
                    };
                    this.createdCase.push(createdCase);
                    // this.createdCase.dispositioncode: '',
                    // this.createdCase.intakeserreqstatustypekey: '',
                    if (this.createdCase.length > 0) {
                        if (this.createdCase[0].intakeserreqstatustypekey) {
                            this.onChangeTaskStatus(this.createdCase[0].intakeserreqstatustypekey);
                        }
                        if (this.createdCase[0].supStatus) {
                            this.supOnChangeTaskStatus(this.createdCase[0].supStatus);
                            // this.changeDisp();
                        }
                        this.dispositionFormGroup.patchValue({
                            intakeserreqstatustypekey: this.createdCase[0].intakeserreqstatustypekey ? this.createdCase[0].intakeserreqstatustypekey : '',
                            dispositioncode: this.createdCase[0].dispositioncode ? this.createdCase[0].dispositioncode : '',
                            supStatus: this.createdCase[0].supStatus ? this.createdCase[0].supStatus : '',
                            supDisposition: this.createdCase[0].supDisposition ? this.createdCase[0].supDisposition : ''
                        });
                        this.changeDisp();
                    }
                });
            }
        });

        if (role.role.name === 'apcs') {
            this.supervisorUser = true;
            this.dispositionFormGroup.get('intake');
        } else {
            this.supervisorUser = false;
        }
        this.dispositionOutPut$.subscribe((data) => {
            const roleId = this._authService.getCurrentUser();
            if (roleId.role.name === 'apcs') {
                this.supervisorUser = true;
                this.showStatus = false;
            } else {
                this.supervisorUser = false;
            }
            if (data && data.length > 0) {
                this.commonDispositionStatusData = data[0];
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            nolimit: true,
                            where: {
                                intakeservreqtypeid: data[0].DAStatus,
                                servicerequestsubtypeid: this.daTypeSubType
                            },
                            method: 'get'
                        }),
                        'Intakeservicerequestdispositioncodes/getstatuslist?filter'
                    )
                    .subscribe((result) => {
                        this.dispositionList = result;
                        if (data && data.length > 0) {
                            if (data[0].intakeserreqstatustypekey) {
                                // this.onChangeTaskStatus(data[0].intakeserreqstatustypekey);
                                this.createdCase = data;
                                this.onChangeTaskStatus(data[0].intakeserreqstatustypekey);
                                this.dispositionFormGroup.patchValue({
                                    intakeserreqstatustypekey: data[0].intakeserreqstatustypekey,
                                    dispositioncode: data[0].dispositioncode,
                                    comments: data[0].comments,
                                    intakeAction: data[0].intakeAction
                                });
                            }
                            if (data[0].supStatus) {
                                // this.supOnChangeTaskStatus(data[0].supStatus);
                                this.dispositionFormGroup.patchValue({
                                    reason: data[0].reason,
                                    supStatus: data[0].supStatus,
                                    supDisposition: data[0].supDisposition,
                                    supComments: data[0].supComments
                                });
                            }
                        }
                    });
            }
        });
        this.timeReceived$.subscribe((time) => {
            this.timeReceived = time;
            this.enableReasonfordelay();
        });
        this.purposeToDispOutput$.subscribe((resp) => {
            this.daTypeSubType = resp;
            this.statusDropdownItems$ = Observable.empty();
            if (resp === '619c4dcf-ef22-4fc4-9269-d7678e8a8f6a') {
                this.showBehalfOfRequester = true;
                const model = new DropdownModel();
                model.text = 'Closed';
                model.value = 'Closed';
                this.statusDropdownItems$ = Observable.of([model]);
                this.dispositionFormGroup.patchValue({
                    intakeserreqstatustypekey: 'Closed'
                });
                this.onChangeTaskStatus('Closed');
                // this.dispositionFormGroup.get('intakeserreqstatustypekey').disable();
            } else {
                this.showBehalfOfRequester = false;
                this.dispositionFormGroup.get('intakeserreqstatustypekey').enable();
                // this.loadDropdown();
            }
        });

        this.appointmentOutputSubject$.subscribe((data) => {
            if (data) {
                if (data && data.length > 0) {
                    this.intakeWorkerId = data[0].intakeWorkerId;
                    if (this.intakeWorkerList.length > 0) {
                        if (this.getIntakeWorkerName()) {
                            this.intakeWorkerName = this.getIntakeWorkerName().username;
                        }
                    }
                }

            }
        });

        this.appointmentInputSubject$.subscribe((data) => {
            if (data) {
                if (data && data.length > 0) {
                    this.intakeWorkerId = data[0].intakeWorkerId;
                    if (this.intakeWorkerList.length > 0) {
                        if (this.getIntakeWorkerName()) {
                            this.intakeWorkerName = this.getIntakeWorkerName().username;
                        }
                    }
                }

            }
        });

        this.addedPersonsChanges$.subscribe((addedPersons) => {
            this.mother = null;
            this.father = null;
            this.youth = null;
            if (addedPersons) {
                addedPersons.forEach(person => {
                    this.getPersonRole(person);
                });
            }
            this.estimateSytemRecomendation();
        });

        this.evalFields$.subscribe((data) => {
            this.evalFields = data;
            if (this.jurisdiction && this.jurisdiction.id !== data.countyid) {
                if (this.countyid !== data.countyid) {
                    this.countyid = data.countyid;
                    this.getjurisdiction(data.countyid);
                }
            }
            this.estimateSytemRecomendation();
        });
    }

    ngAfterViewInit() {
        this.isDateDelayed$.subscribe((item) => {
            this.generalRecievedDate = item;
            (<any>$('#reason-for-delay')).modal('show');
            // console.log('calculated time', item);
        });
        this.general$.subscribe((item) => {
            this.general = item;
        });
    }
    ngAfterViewChecked() {
        this._changeDetect.detectChanges();
    }



    private getAllegedOffence(offence) {

        if (offence) {
            const Offence = this.offenceCategories.find((offenceCategory) => offenceCategory.allegationid === offence.allegationid);
            if (Offence) {
                return Offence.name;
            }
        }

        return null;
    }

    private isAgeBarExceeds() {

        if (this.evalFields && this.evalFields.allegedoffense && this.offenceCategories.length > 0) {

            if (!this.evalFields.yearsofage) {
                // No age found
                return true;
            }
            let allegationIds = [];
            if (this.evalFields.allegedoffense) {
                allegationIds = this.evalFields.allegedoffense.map(offence => offence.allegationid);
            }
            const selectedAllegations = this.offenceCategories.filter(allegation => {
                const found = allegationIds.indexOf(allegation.allegationid);
                if (found === - 1) {
                    return false;
                } else {
                    return true;
                }

            });

            if (selectedAllegations) {
                const ageBars = selectedAllegations.map(allegation => {
                    if (allegation && allegation.agecutoff) {
                        return allegation.agecutoff;
                    }

                }).filter(age => {
                    if (age) {
                        return true;
                    } else {
                        return false;
                    }
                });
                if (ageBars) {
                    const minAge = Math.min(...ageBars);
                    if (this.evalFields.yearsofage && Number(this.evalFields.yearsofage) > minAge) {
                        return true;  // if Age exceeds
                    }
                }


            }


        }

        return false;
    }

    private estimateSytemRecomendation() {
        this.system = {
            ageInValid: false, youthValid: false, parentsValid: false,
            jurisdictionValid: false, offenceValid: false, status: 'Accept', reason: 'Meeting criteria'
        };
        this.system.ageInValid = this.isAgeBarExceeds();
        if (this.youth) {
            this.system.youthValid = true;
        }
        if (this.mother || this.father) {
            this.system.parentsValid = true;
        }
        if (this.jurisdiction.name !== '') {
            this.system.jurisdictionValid = true;
        }
        if (this.evalFields && this.evalFields.allegedoffense && this.evalFields.allegedoffense.length > 0) {
            this.system.offenceValid = true;
        }
        if (this.system.ageInValid) {
            this.system.status = 'Reject';
            this.system.reason = 'Not meeting age criteria';
        }
        if ((!this.system.youthValid) || (!this.system.jurisdictionValid) || (!this.system.offenceValid) || (!this.system.parentsValid)) {
            this.system.status = 'Reject';
            this.system.reason = 'Insufficient information';
        }
        // console.log(this.system);
    }

    private loadOffenceList() {
        this.offenceCategoriesInputSubject$.subscribe((res) => {
            this.offenceCategories = res;
        });

    }

    setScreenToShow() {
        this.showScreen1 = false;
        this.showScreen2 = false;
        this.showScreen3 = false;
        this.showScreen4 = false;
        if (this.role.role.name === 'apcs' && this.isPreIntake) {
            this.showScreen3 = true;
        } else if (this.role.role.name === 'apcs' && !this.isPreIntake) {
            this.showScreen1 = true;
        } else if (this.role.role.name !== 'apcs') {
            this.showScreen2 = true;
        }
        // this.showScreen1 = true;
    }

    dispositionForm() {
        this.dispositionFormGroup = this.formBuilder.group({
            intakeserreqstatustypekey: ['', Validators.required],
            dispositioncode: ['', Validators.required],
            statusdate: [this.date],
            duedate: [this.date],
            completiondate: [this.date],
            dateseen: null,
            financial: [''],
            seenwithin: [this.date],
            edl: [''],
            jointinvestigation: [''],
            investigationsummary: [''],
            visitinfo: [''],
            supStatus: [''],
            supDisposition: [''],
            supComments: [''],
            comments: [''],
            reason: [''],
            isDelayed: [false],
            intakeAction: ['']
        });
        this.preIntakeDispositionFormGroup = this.formBuilder.group({
            systemRecmdatnStatus: [''],
            systemRecmdatn: [''],
            status: [''],
            reason: [''],
            comment: [''],
        });
        this.dispositionDelayForm = this.formBuilder.group({
            fiveDaysDelay: [''],
            twentyFivedaysDelay: ['']
        });
    }

    loadDropdown() {
        this.statusDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        intakeservreqtypeid: this.daTypeSubType,
                        servicerequestsubtypeid: this.daTypeSubType
                    },
                    method: 'get'
                }),
                'Intakeservicerequestdispositioncodes/getstatuslist?filter'
            )
            .map((result) => {
                this.dispositionList = result;
                if (this.showScreen3) {
                    return result.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeserreqstatustypekey
                            })
                    ).filter(data => (data.text === 'Accepted' || data.text === 'Rejected'));
                } else {
                    return result.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeserreqstatustypekey
                            })
                    );
                }
            });
    }

    onChangeTaskStatus(statusId: any) {
        if (this.createdCase && this.createdCase.length > 0) {
            this.createdCase.map((item) => {
                item.DAStatus = statusId;
                item.intakeMultipleDispositionDropdown = [];
                // this.dispositionDropDown = [];
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            nolimit: true,
                            where: {
                                intakeservreqtypeid: item.DaTypeKey,
                                servicerequestsubtypeid: item.DasubtypeKey ? item.DasubtypeKey : item.DaTypeKey,
                                statuskey: statusId
                            },
                            method: 'get'
                        }),
                        'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                    )
                    .subscribe((result) => {
                        item.intakeMultipleDispositionDropdown = result.map(
                            (res) =>
                                new DropdownModel({
                                    text: res.description,
                                    value: res.dispositioncode
                                })
                        );
                    });
            });
        }
    }

    supPreIntakeStatusSelected(id, event: number) {
        if (event === 1) {
            this.decision$.next(id);
            if (id === 'Approved') {
                this.toShowReason = false;
            } else if (id === 'Rejected') {
                this.toShowReason = true;
            }
            this._commonHttpService
                .getArrayList(
                    new PaginationRequest({
                        nolimit: true,
                        where: {
                            intakeservreqtypeid: this.serviceTypeId,
                            servicerequestsubtypeid: this.serviceTypeId,
                            statuskey: id
                        },
                        method: 'get'
                    }),
                    'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                )
                .subscribe((result) => {
                    this.supMultipleDispositionDropdown = result.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.dispositioncode
                            })
                    );
                });
        } else if (event === 2) {
        } else if (event === 3) { }
        this.preIntakeDispositionFormGroup.patchValue({
            systemRecmdatn: this.system.reason,
            systemRecmdatnStatus: this.system.status
        });

        this.preIntakedisposition$.next(this.preIntakeDispositionFormGroup.value);
    }

    supOnChangeTaskStatus(id) {
        this.changeDisp();
        this.createdCase.map((item) => {
            item.DAStatus = id;
            item.supMultipleDispositionDropdown = [];
            this._commonHttpService
                .getArrayList(
                    new PaginationRequest({
                        nolimit: true,
                        where: {
                            intakeservreqtypeid: item.DaTypeKey,
                            servicerequestsubtypeid: item.DasubtypeKey,
                            statuskey: id
                        },
                        method: 'get'
                    }),
                    'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                )
                .subscribe((result) => {
                    item.supMultipleDispositionDropdown = result.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.dispositioncode
                            })
                    );
                });
        });
    }
    onChangeDispoType(daSubtype, index) {
        this.createdCase[index].DADisposition = daSubtype;
        this.createdCase[index].dispositioncode = daSubtype;
        this.changeDisp();
    }
    onChangeSupDispoType(daSubtype, index) {
        this.createdCase[index].DADisposition = daSubtype;
        this.createdCase[index].supDisposition = daSubtype;
        this.changeDisp();
    }

    changeDisp() {
        if (this.createdCase && this.createdCase.length > 0) {
            this.createdCase.map((item) => {
                item.intakeserreqstatustypekey = this.dispositionFormGroup.value.intakeserreqstatustypekey ? this.dispositionFormGroup.value.intakeserreqstatustypekey : '';
                item.comments = this.dispositionFormGroup.value.comments ? this.dispositionFormGroup.value.comments : '';
                item.intakeAction = this.dispositionFormGroup.value.intakeAction ? this.dispositionFormGroup.value.intakeAction : '';
                item.supStatus = this.dispositionFormGroup.value.supStatus ? this.dispositionFormGroup.value.supStatus : '';
                item.supComments = this.dispositionFormGroup.value.supComments ? this.dispositionFormGroup.value.supComments : '';
                item.reason = this.dispositionFormGroup.value.reason ? this.dispositionFormGroup.value.reason : '';
            });
            this.dispositionRetrive$.next(this.createdCase);
        }
    }
    enableReasonfordelay() {
        if (this.timeReceived === 'Overdue') {
            this.dispositionFormGroup.patchValue({ isDelayed: true });
            this.showReason = true;
            this.dispositionFormGroup.get('reason').setValidators([Validators.required]);
            this.dispositionFormGroup.get('reason').updateValueAndValidity();
        } else {
            this.dispositionFormGroup.get('reason').clearValidators();
            this.dispositionFormGroup.get('reason').updateValueAndValidity();
            this.dispositionFormGroup.patchValue({ isDelayed: false });
        }
    }
    submitDelayForm() {
        if (this.dispositionDelayForm.dirty) {
            const delayReason = {
                fiveDays: this.dispositionDelayForm.get('fiveDaysDelay').value,
                twentyFiveDays: this.dispositionDelayForm.get('twentyFivedaysDelay').value
            };
            this.delayformValue$.next(delayReason);
        }
        (<any>$('#reason-for-delay')).modal('hide');
    }

    getPersonRole(person: InvolvedPerson) {
        if (person.RelationshiptoRA === 'mother') {
            this.mother = person;
        } else if (person.RelationshiptoRA === 'father') {
            this.father = person;
        } else if (person.Role === 'Youth') {
            this.youth = person;
        }

    }

    private loadIntaker(pageNumber: number) {
        const source = this._commonHttpService
            .getPagedArrayList(
                {
                    limit: this.paginationInfo.pageSize,
                    // order: this.paginationInfo.sortBy,
                    page: pageNumber,
                    // count: this.paginationInfo.total,
                    // where: this.involvedPersonSearch,
                    method: 'get'
                },
                'Intakedastagings/getIntakeUsers?filter'
            )
            .map((result) => {
                this.intakeWorkerList = result.data;
                if (this.intakeWorkerId) {
                    if (this.getIntakeWorkerName()) {
                        this.intakeWorkerName = this.getIntakeWorkerName().username;
                    }
                }

                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.intakersList$ = source.pluck('data');
        if (pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }
    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.loadIntaker(this.paginationInfo.pageNumber);
    }

    intakerChoosen(intaker: RoutingUser) {
        this.intakerinMind = intaker;
    }

    selectIntaker() {
        /* if (!this.intakeWorker) {
             this.intakeWorker = new MembersInMeeting();
         }
         this.intakeWorker.fullname = this.intakerinMind.username;
         this.intakeWorker.isSelected = true;
         this.intakeWorker.pid = this.intakerinMind.userid;
         this.intakeWorker.role = this.intakerinMind.userrole;*/
        this.intakeWorkerId = this.intakerinMind.userid;
        this.decision$.next(this.intakeWorkerId);
        // if (this.appmntList.length === 0) {
        //   this.appmntList.push(new IntakeAppointment());
        // }
        // this.appmntList[0].caseWorker = this.caseWorker;
        // this.appointmentInputSubject$.next(this.appmntList);
    }

    assignIntaker(modal) {
        if (this.intakeWorkerId) {
            this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        where: {
                            appeventcode: modal,
                            intakenumber: this.intakeNumber,
                            assigneduserid: this.intakeWorkerId
                        },
                        method: 'post'
                    }),
                    'Intakedastagings/assignIntake'
                )
                .subscribe(result => {
                    this._alertService.success('Intake assigned successfully!');
                    this.closePopup();
                    const dashboardURL = '/pages/cjams-dashboard';
                    this._router.navigate([dashboardURL]);
                });
        } else {
            this._alertService.warn('Please select a intake worker');
        }
    }

    closePopup() {
        (<any>$('#list-intank')).modal('hide');
        // (<any>$('#assign-preintake')).modal('hide');
        // (<any>$('#reopen-intake')).modal('hide');
    }

    getjurisdiction(id: string) {
        if (id && id !== '') {
            this._commonHttpService.getArrayList({
                where: { state: 'MD', countyid: id },
                order: 'countyname asc', nolimit: true, method: 'get'
            }, NewUrlConfig.EndPoint.Intake.MDCountryListUrl + '?filter').subscribe((data) => {
                if (data && data.length > 0) {
                    this.jurisdiction = { id: data[0].countyid, name: data[0].countyname };
                    // console.log('jurisdiction found');
                    this.estimateSytemRecomendation();
                }
            });
        }
        return 'adf';
    }

    getIntakeWorkerName() {
        const intakeWorker = this.intakeWorkerList.find(iw => iw.userid === this.intakeWorkerId);
        if (intakeWorker) {
            return intakeWorker;
        }
        return null;
    }
}
