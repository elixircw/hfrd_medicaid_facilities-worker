import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { environment } from '../../../../../environments/environment';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService, AuthService, CommonHttpService, GenericService } from '../../../../@core/services';
import { HttpService } from '../../../../@core/services/http.service';
import { SessionStorageService } from '../../../../@core/services/storage.service';
import { AppConfig } from '../../../../app.config';
import {
    AllegationItem,
    Assessments,
    AssessmentSummary,
    GetAssessmentScore,
    GetintakAssessment,
    IntakeAssessmentRequestIds,
    IntakeDATypeDetail,
    InvolvedPerson,
    AssessmentScores,
} from '../_entities/newintakeModel';
import { IntakeService, EvaluationFields } from '../_entities/newintakeSaveModel';
import { AssessmentMode } from './AssessmentMode';
import { AssessmentPreFill } from './assessment-prefill';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
declare var $: any;
declare var Formio: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-assessment',
    templateUrl: './intake-assessment.component.html',
    styleUrls: ['./intake-assessment.component.scss']
})
export class IntakeAssessmentComponent implements OnInit {
    id: string;
    daNumber: string;
    assessmmentName: string;

    @Input() addedIntakeDATypeDetails: IntakeDATypeDetail[] = [];
    @Input() addedIntakeDATypeSubject$ = new Subject<IntakeDATypeDetail[]>();
    @Input() intakeNumber: string;
    @Input() intakeNumberNarrative: string;
    @Input() reviewstatus: string;
    @Input() agencyCodeSubject$ = new Subject<IntakeDATypeDetail>();
    @Input() purposeCheckboxOutput$ = new Subject<IntakeService>();
    @Input() purposeCheckboxView$ = new Subject<IntakeService>();
    @Input() serviceCheckboxOutput$ = new Subject<string>();
    @Input() serviceCheckboxInput$ = new Subject<Assessments>();
    @Input() purposeCheckboxInput$ = new Subject<GetAssessmentScore[]>();
    @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
    @Input() evalFieldsOutputSubject$ = new Subject<EvaluationFields>();
    @Input('scoresSubject') scoresSubject$ = new Subject<AssessmentScores>();
    private intakeDATypeDetail: IntakeDATypeDetail;
    role: AppUser;
    private assessmentRequestDetail: IntakeAssessmentRequestIds;
    startAssessment$: Observable<Assessments[]>;
    assessmentSummary$: Observable<AssessmentSummary[]>;
    totalRecords$: Observable<number>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    private pageSubject$ = new Subject<number>();
    daTypeDropDownItems$: Observable<DropdownModel[]>;
    daSubTypeDropDownItems$: Observable<DropdownModel[]>;
    filteredAllegationItems: AllegationItem[] = [];
    formBuilderUrl: string;
    safeUrl: SafeResourceUrl;
    submissionId: string;
    assessmentTemplateId: string;
    showAssesment = -1;
    getAsseesmentHistory: GetintakAssessment[] = [];
    private token: AppUser;
    formioOptions: {
        formio: {
            ignoreLayout: true;
            emptyValue: '-';
        };
    };
    templateComponentData: any;
    templateSubmissionData: any;
    getScoreOnClose: boolean;
    selectedPurpose: string;
    currentTemplateId: string;
    isReadOnlyForm = false;
    private assessmentMode: AssessmentMode;
    refreshForm: any;
    safeCKeys: string[];
    selectedSafeCDangerInfluence: any[] = [];
    intakeFormData: any;
    addedPersons: InvolvedPerson[] = [];
    evalFields: EvaluationFields;
    assmntScores: AssessmentScores = new AssessmentScores();
    constructor(
        private route: ActivatedRoute,
        private _authService: AuthService,
        private _service: GenericService<Assessments>,
        private _commonService: CommonHttpService,
        private storage: SessionStorageService,
        private _alertService: AlertService,
        public sanitizer: DomSanitizer,
        private _http: HttpService,
        private _cd: ChangeDetectorRef
    ) {
        this.id = route.snapshot.params['id'];
        this.daNumber = route.snapshot.params['daNumber'];
        this.safeCKeys = [
            'caregiverdescribes',
            'caregiverfailstoprotect',
            'caregivermadeaplausible',
            'caregiverrefuses',
            'caregiversemotionalinstability',
            'caregiversexplanation',
            'caregiversjustification',
            'caregiverssuspected',
            'childscurrentimminent',
            'childsexualabuse',
            'childswhereabouts',
            'currentactofmaltreatment',
            'domesticviolence',
            'extremelyanxious',
            'multiplereports',
            'servicestothecaregiver',
            'specialneeds',
            'unabletoprotect',
            'servicestothecaregiver2'
        ];
    }
    ngOnInit() {
        this.refreshForm = new EventEmitter();
        this.token = this._authService.getCurrentUser();
        this.pageSubject$.subscribe((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            this.getPage(this.paginationInfo.pageNumber);
        });
        this.getPage(1);
        // this.populateIntake();
        // this.loadDropdownItems();
        this.role = this._authService.getCurrentUser();
        this.agencyCodeSubject$.subscribe(agency => {
            this._cd.detectChanges();
            if (!this.id) {
                this.id = this.intakeNumberNarrative;
            }
            this.getIntakeAssessmentDetails(agency);
        });
        this.addedPersonsChanges$.subscribe(data => {
            this.addedPersons = data;
        });
        this.evalFieldsOutputSubject$.subscribe(data => {
            this.evalFields = data;
        });
        // this.serviceCheckboxOutput$.subscribe(result => {
        //     this.selectedPurpose = result;
        //     if (!this.id) {
        //         this.id = this.intakeNumberNarrative;
        //     }
        //     this._service
        //         .getPagedArrayList(
        //             new PaginationRequest({
        //                 page: this.paginationInfo.pageNumber,
        //                 limit: this.paginationInfo.pageSize,
        //                 where: this.assessmentRequestDetail,
        //                 method: 'get'
        //             }),
        //             'admin/assessmenttemplate/getintakeassessment?filter'
        //         )
        //         .subscribe(resp => {
        //             if (resp.data) {
        //                 const isFormExist = resp.data.filter(item => item.assessmenttemplateid === this.assessmentTemplateId);
        //                 if (isFormExist && isFormExist.length > 0) {
        //                     this.submissionId = isFormExist[0].intakassessment.submissionid;
        //                     this.serviceCheckboxInput$.next(isFormExist[0]);
        //                 }
        //             }
        //         });
        // });
        // this.purposeCheckboxOutput$.subscribe(result => {
        //     this.getScoreOnClose = true;
        //     const externalId = result.external_templateid;
        //     this.assessmentTemplateId = result.assessmenttemplateid;
        //     this.assessmmentName = result.description;
        //     this._service
        //         .getPagedArrayList(
        //             new PaginationRequest({
        //                 page: this.paginationInfo.pageNumber,
        //                 limit: this.paginationInfo.pageSize,
        //                 where: this.assessmentRequestDetail,
        //                 method: 'get'
        //             }),
        //             'admin/assessmenttemplate/getintakeassessment?filter'
        //         )
        //         .subscribe(resp => {
        //             const isFormExist = resp.data.filter(item => item.assessmenttemplateid === this.assessmentTemplateId);
        //             if (isFormExist && isFormExist.length > 0 && isFormExist[0].intakassessment) {
        //                 this.submissionId = isFormExist[0].intakassessment.submissionid;
        //             }
        //             (<any>$('#iframe-popup')).modal('show');
        //             if (this.submissionId) {
        //                 // this.formBuilderUrl = environment.formBuilderHost + `/#/views/completeform?sid=${this.submissionId}&fid=${externalId}&da=` + this.draftId + `&ro=true&t=` + this.token.id;
        //                 this.formBuilderUrl = environment.formBuilderHost + `/#/views/completeform?sid=${this.submissionId}&fid=${externalId}&da=` + this.id + `&ro=false&t=` + this.token.id;
        //             } else {
        //                 this.formBuilderUrl = environment.formBuilderHost + `/#/views/submitform?da=` + this.id + `&fid=${externalId}&t=` + this.token.id + '&g=true';
        //             }
        //             this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.formBuilderUrl);
        //         });
        // });
        // this.purposeCheckboxView$.subscribe(result => {
        //     this.getScoreOnClose = true;
        //     const externalId = result.external_templateid;
        //     this.assessmentTemplateId = result.assessmenttemplateid;
        //     this.assessmmentName = result.description;
        //     this._service
        //         .getPagedArrayList(
        //             new PaginationRequest({
        //                 page: this.paginationInfo.pageNumber,
        //                 limit: this.paginationInfo.pageSize,
        //                 where: this.assessmentRequestDetail,
        //                 method: 'get'
        //             }),
        //             'admin/assessmenttemplate/getintakeassessment?filter'
        //         )
        //         .subscribe(resp => {
        //             const isFormExist = resp.data.filter(item => item.assessmenttemplateid === this.assessmentTemplateId);
        //             if (isFormExist && isFormExist.length > 0 && isFormExist[0].intakassessment) {
        //                 this.submissionId = isFormExist[0].intakassessment.submissionid;
        //             }
        //             (<any>$('#iframe-popup')).modal('show');
        //             if (this.submissionId) {
        //                 // this.formBuilderUrl = environment.formBuilderHost + `/#/views/completeform?sid=${this.submissionId}&fid=${externalId}&da=` + this.draftId + `&ro=true&t=` + this.token.id;
        //                 this.formBuilderUrl = environment.formBuilderHost + `/#/views/completeform?sid=${this.submissionId}&fid=${externalId}&da=` + this.id + `&ro=false&t=` + this.token.id;
        //             } else {
        //                 this.formBuilderUrl = environment.formBuilderHost + `/#/views/submitform?da=` + this.id + `&fid=${externalId}&t=` + this.token.id + '&g=true';
        //             }
        //             this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.formBuilderUrl);
        //         });
        // });
    }

    private loadDropdownItems() {
        this.daTypeDropDownItems$ = this._commonService
            .getArrayList(
                {
                    nolimit: true,
                    where: { activeflag: 1 },
                    method: 'get'
                },
                NewUrlConfig.EndPoint.Intake.DATypeUrl + '?filter'
            )
            .map(result => {
                return result.map(res => new DropdownModel({ text: res.description, value: res.intakeservreqtypeid }));
            });
    }

    getPage(page: number) {
        if (this.assessmentRequestDetail) {
            this._http.overrideUrl = false;
            this._http.baseUrl = AppConfig.baseUrl;
            this.showAssesment = -1;
            const source = this._service
                .getPagedArrayList(
                    new PaginationRequest({
                        page: this.paginationInfo.pageNumber,
                        limit: this.paginationInfo.pageSize,
                        where: this.assessmentRequestDetail,
                        method: 'get'
                    }),
                    'admin/assessmenttemplate/getintakeassessment?filter'
                )
                .map(result => {
                    this.getDRAIScore(result);
                    this.getMCAPScore(result);
                    return { data: result.data, count: result.count };
                })
                .share();
            this.startAssessment$ = source.pluck('data');
            if (page === 1) {
                this.totalRecords$ = source.pluck('count');
            }
        }
    }

    startAssessment(assessment) {
        this.assessmmentName = assessment.titleheadertext;
        this.currentTemplateId = assessment.external_templateid;
        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`).then(function (form) {
            form.components = form.components.map((item) => {
                if (item.key === 'Complete' && item.type === 'button') {
                    item.action = 'submit';
                }
                return item;
            });
            form.submission = {
                data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
            };
            form.on('submit', (submission) => {
                if (_self.assessmmentName === 'SAFE-C') {
                    submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
                }
                _self._http
                    .post('admin/assessment/Add', {
                        externaltemplateid: _self.currentTemplateId,
                        assessmentstatustypekey1: 'Submitted',
                        objectid: _self.id,
                        submissionid: submission._id,
                        submissiondata: submission.data ? submission.data : null,
                        form: submission.form ? submission.form : null,
                        score: submission.data.score ? submission.data.score : 0
                    })
                    .subscribe((response) => {
                        _self._alertService.success(_self.assessmmentName + ' saved successfully.');
                        _self.getPage(1);
                        (<any>$('#iframe-popup')).modal('hide');
                    });
            });
            form.on('change', (formData) => {
                _self.safeCProcess(formData);
            });
            form.on('render', (formData) => {
                (<any>$('#iframe-popup')).modal('show');
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });

            form.on('error', (error) => {
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
                _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
            });
        });
    }

    updateAssessment(assessment: GetintakAssessment) {
        this.assessmmentName = assessment.titleheadertext;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        // this.assessmmentName = assessment.description;
        // this.currentTemplateId = assessment.external_templateid;
        const _self = this;
        // Formio.setToken(this.storage.getObj('fbToken'));
        // Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
            readOnly: false
        }).then(function (form) {
            form.components = form.components.map(item => {
                if (item.key === 'Complete' && item.type === 'button') {
                    item.action = 'submit';
                }
                return item;
            });
            form.submission = {
                data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
            };
            (<any>$('#iframe-popup')).modal('show');
            form.on('render', formData => {
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });
            form.on('submit', submission => {
                if (_self.assessmmentName === 'SAFE-C') {
                    submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
                }
                let status = '';
                let comments = '';
                if (_self.token.role.name === 'apcs') {
                    status = submission.data.assessmentstatus;
                    comments = submission.data.supervisorcomments2;
                } else if (_self.token.role.name === 'field') {
                    status = submission.data.assessmentreviewed;
                    comments = submission.data.caseworkercomments;
                }
                _self._http
                    .post('admin/assessment/Add', {
                        externaltemplateid: _self.currentTemplateId,
                        objectid: _self.id,
                        submissionid: submission._id,
                        submissiondata: submission.data ? submission.data : null,
                        form: submission.form ? submission.form : null,
                        score: submission.data.score ? submission.data.score : 0,
                        assessmentstatustypekey1: status,
                        comments: comments
                    })
                    .subscribe(response => {
                        _self._alertService.success(_self.assessmmentName + ' saved successfully.');
                        _self.getPage(1);
                        _self.showAssessment(_self.showAssesment, _self.getAsseesmentHistory);
                        (<any>$('#iframe-popup')).modal('hide');
                    });
            });
            form.on('change', formData => {
                _self.safeCProcess(formData);
            });
            // form.on('render', (formData) => {
            //     (<any>$('#iframe-popup')).modal('show');
            //     setTimeout(function () {
            //         $('#assessment-popup').scrollTop(0);
            //     }, 200);
            // });

            form.on('error', error => {
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
                _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
            });
        });
    }

    editAssessment(assessment: GetintakAssessment) {
        this.getScoreOnClose = false;
        this.assessmmentName = assessment.description;
        this.formBuilderUrl = environment.formBuilderHost + `/#/views/completeform?sid=${assessment.submissionid}&fid=${assessment.external_templateid}&da=`
            + this.id + `&ro=false&t=` + this.token.id;
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.formBuilderUrl);
    }

    submittedAssessment(assessment: GetintakAssessment) {
        this.assessmmentName = assessment.titleheadertext;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
            readOnly: true
        }).then(function (submission) {
            (<any>$('#iframe-popup')).modal('show');
            submission.on('render', (formData) => {
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });
        });
    }

    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageSubject$.next(this.paginationInfo.pageNumber);
    }

    assessmentPrintView(assessment: GetintakAssessment) {
        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
            readOnly: true
        }).then(function (submission) {
            const options = {
                ignoreLayout: true
            };
            _self.viewHtml(submission._form, submission._submission, options);
        });
    }

    private safeCProcess($event) {
        if (this.assessmmentName === 'SAFE-C') {
            if ($event.changed) {
                const dangerInfluenceKey = $event.changed.component.key;
                if (dangerInfluenceKey && this.safeCKeys.indexOf(dangerInfluenceKey) > -1) {
                    const dangerInflunceItem = this.selectedSafeCDangerInfluence.find((item) => item.value === dangerInfluenceKey);
                    if (dangerInflunceItem) {
                        if ($event.changed.value === 'no' || $event.data[$event.changed.component.key] === 'no') {
                            const itemIndex = this.selectedSafeCDangerInfluence.indexOf(dangerInflunceItem);
                            this.selectedSafeCDangerInfluence.splice(itemIndex, 1);
                        }
                        // console.log(this.selectedSafeCDangerInfluence);
                    } else {
                        if ($event.changed.value === 'yes' || $event.data[$event.changed.component.key] === 'yes') {
                            this.selectedSafeCDangerInfluence.push({
                                text: $event.changed.component.label,
                                value: dangerInfluenceKey
                            });
                            // console.log(this.selectedSafeCDangerInfluence);
                        }
                    }
                }
            }
        } else {
            this.selectedSafeCDangerInfluence = [];
        }
    }

    onCustomEvent($event) {
        // console.log($event);
    }

    viewHtml(componentData, submissionData, formioOptions) {
        delete submissionData._id;
        delete submissionData.owner;
        delete submissionData.modified;
        const exporter = new FormioExport(componentData, submissionData, formioOptions);
        const appDiv = document.getElementById('divPrintView');
        exporter.toHtml().then((html) => {
            html.style.margin = 'auto';
            const iframe = this.createIframe(appDiv);
            const doc = iframe.contentDocument || iframe.contentWindow.document;
            doc.body.appendChild(html);
            window.frames['ifAssessmentView'].focus();
            window.frames['ifAssessmentView'].print();
        });
    }

    private createIframe(el) {
        _.forEach(el.getElementsByTagName('iframe'), (_iframe) => {
            el.removeChild(_iframe);
        });
        const iframe = document.createElement('iframe');
        iframe.setAttribute('id', 'ifAssessmentView');
        iframe.setAttribute('name', 'ifAssessmentView');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('webkitallowfullscreen', '');
        iframe.setAttribute('mozallowfullscreen', '');
        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('style', 'width: -webkit-fill-available;height: -webkit-fill-available;');
        el.appendChild(iframe);
        return iframe;
    }

    showAssessment(id: number, row) {
        this.getAsseesmentHistory = row;
        if (this.showAssesment !== id) {
            this.showAssesment = id;
        } else {
            this.showAssesment = -1;
        }
    }

    private getFormPrePopulation(formName: string, submissionData: any) {
        const prefillUtil = new AssessmentPreFill(this.addedPersons, this.evalFields, this._authService);
        switch (formName.toUpperCase()) {
            case 'MARYLAND FAMILY INITIAL  RISK ASSESSMENT':
                // submissionData = prefillUtil.fillMFRA(submissionData, this.daNumber);
                break;

            case 'MARYLAND FAMILY RISK REASSESSMENT':
                // submissionData = prefillUtil.fillMFRR(submissionData, this.daNumber);

                break;

            case 'SAFE-C':
                // submissionData = prefillUtil.fillSafeC(submissionData, this.daNumber);
                break;

            case 'CANS-F':
                // submissionData = prefillUtil.fillCansF(submissionData, this.daNumber);
                break;
            case 'HOME HEALTH REPORT':
                // submissionData = prefillUtil.fillHomeHealthReport(submissionData, this.daNumber);
                break;
            case 'SAFE-C OHP':
                // submissionData = prefillUtil.fillSafeCOHP(submissionData, this.daNumber);
                break;
            case 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT':
                // submissionData = prefillUtil.fillTransportationPlan(submissionData, this.daNumber);
                break;
            case 'BEST INTEREST DETERMINATION FORM':
                // submissionData = prefillUtil.fillBestInterestDetermination(submissionData, this.daNumber, this.token.user.userprofile.displayname);
                break;
            case 'INTAKE DETENTION RISK ASSESSMENT INSTRUMENT':
                submissionData = prefillUtil.fillIntakeDetentnRiskAssmntInstrument(submissionData);
                break;
            case 'DOMESTIC VIOLENCE LETHALITY ASSESSMENT':
                submissionData = prefillUtil.fillDomsticViolncLethalityAssmnt(submissionData);
                break;
            case 'DMST SCREENING TOOL':
                submissionData = prefillUtil.fillDMSTScreeningTool(submissionData);
                break;
            case 'MCASP RISK ASSESSMENT':
                submissionData = prefillUtil.fillMCASPriskAssment(submissionData);
        }
        return submissionData;
    }

    private populateIntake() {
        this._commonService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        servicerequestid: this.id
                    }
                },
                'Intakedastagings/getintakesnapshot?filter'
            )
            .subscribe((response) => {
                // console.log(JSON.stringify(response));
                this.intakeFormData = response[0];
            });
    }

    private getSubmittedAssessmentForm(external_templateid: string, submissionid: string) {
        const templateUrl = environment.formBuilderHost + `/form/${external_templateid}`;
        const submissionUrl = environment.formBuilderHost + `/form/${external_templateid}/submission/${submissionid}`;
        const fbToken = this.storage.getObj('fbToken');
        this._http.setHeader('x-jwt-token', `${fbToken}`);
        this._http.overrideUrl = true;
        return this._http.get(templateUrl).flatMap((data) => {
            this.templateComponentData = data;
            return this._http.get(submissionUrl);
        });
    }

    private getAssessmentForm(external_templateid: string) {
        const templateUrl = environment.formBuilderHost + `/form/${external_templateid}`;
        const fbToken = this.storage.getObj('fbToken');
        this._http.setHeader('x-jwt-token', `${fbToken}`);
        this._http.overrideUrl = true;
        return this._http.get(templateUrl);
    }

    onFormSubmit($event) {
        const submissionUrl = environment.formBuilderHost + `/form/${this.currentTemplateId}/submission?live=1`;
        const fbToken = this.storage.getObj('fbToken');
        this._http.setHeader('x-jwt-token', `${fbToken}`);
        this._http.overrideUrl = true;
        let submittedForm = {};
        if (this.assessmmentName === 'SAFE-C') {
            $event.data['safeCDangerInfluence'] = this.selectedSafeCDangerInfluence;
        }
        // console.log($event);
        return this._http
            .post(submissionUrl, $event)
            .flatMap(
                (data) => {
                    console.log(data);
                    submittedForm = {
                        externaltemplateid: this.currentTemplateId,
                        assessmentstatustypekey1: 'Submitted',
                        objectid: this.id,
                        submissionid: data._id,
                        submissiondata: $event.data ? $event.data : null,
                        form: data.form ? data.form : null,
                        score: $event.data.score ? $event.data.score : 0
                    };
                    console.log(submittedForm);
                    this._http.overrideUrl = false;
                    this._http.baseUrl = AppConfig.baseUrl;
                    return this._http.post(NewUrlConfig.EndPoint.Intake.SubmitAssessment, submittedForm);
                },
                (error) => {
                    console.log(error);
                }
            )
            .subscribe((response) => {
                console.log(response);
                this._alertService.success(this.assessmmentName + ' saved successfully.');
                (<any>$('#iframe-popup')).modal('hide');
                this.closeAssessment();
            });
    }
    onFormRendered($event) {
        console.log($event);
        $('#iframe-popup').removeClass(' intake radio-inline');
    }
    onFormInvalid($event) {
        console.log($event);
    }
    onFormChange($event) {
        if (this.assessmmentName === 'SAFE-C') {
            if ($event.changed) {
                const dangerInfluenceKey = $event.changed.component.key;
                if (dangerInfluenceKey && this.safeCKeys.indexOf(dangerInfluenceKey) > -1) {
                    const dangerInflunceItem = this.selectedSafeCDangerInfluence.find((item) => item.value === dangerInfluenceKey);
                    if (dangerInflunceItem) {
                        if ($event.changed.value === 'no' || $event.data[$event.changed.component.key] === 'no') {
                            const itemIndex = this.selectedSafeCDangerInfluence.indexOf(dangerInflunceItem);
                            this.selectedSafeCDangerInfluence.splice(itemIndex, 1);
                        }
                        console.log(this.selectedSafeCDangerInfluence);
                    } else {
                        if ($event.changed.value === 'yes' || $event.data[$event.changed.component.key] === 'yes') {
                            this.selectedSafeCDangerInfluence.push({
                                text: $event.changed.component.label,
                                value: dangerInfluenceKey
                            });
                            console.log(this.selectedSafeCDangerInfluence);
                        }
                    }
                }
            }
        } else {
            this.selectedSafeCDangerInfluence = [];
        }
    }


    closeAssessment() {
        // this.getPage(1);
        this.getPage(1);
    }



    navigationToSdm() {
        (<any>$('#sdm-tab')).click();
    }

    printView() {
        window.frames['ifAssessmentView'].focus();
        window.frames['ifAssessmentView'].print();
    }

    viewPdf() {
        const exporter = new FormioExport(this.templateComponentData, this.templateSubmissionData, this.formioOptions);
        const appDiv = document.getElementById('divPrintView');
        const formioPdfConfig = {
            download: false,
            filename: this.assessmmentName + '.pdf',
            html2canvas: {
                logging: true,
                onclone: doc => {
                    // You can modify the html before converting it to canvas (add additional page breaks, etc)
                    console.log('html cloned!', doc);
                },
                onrendered: canvas => {
                    // You can access the canvas before converting it to PDF
                    console.log('html rendered!', canvas);
                }
            }
        };

        exporter.toPdf(formioPdfConfig).then(pdf => {
            console.log('pdf ready', pdf);
            const iframe = this.createIframe(appDiv);
            iframe.src = pdf.output('datauristring');
        });
    }

    private getIntakeAssessmentDetails(model: IntakeDATypeDetail) {
        this.assessmentRequestDetail = Object.assign({});
        this.token = this._authService.getCurrentUser();
        const assessmentRequest = new IntakeAssessmentRequestIds();
        assessmentRequest.intakeservicerequesttypeid = '';
        assessmentRequest.intakeservicerequestsubtypeid = '';
        assessmentRequest.agencycode = model.agencycode;
        assessmentRequest.intakenumber = this.id;
        assessmentRequest.target = 'Intake';
        this.assessmentRequestDetail = assessmentRequest;
        this.getPage(1);
    }



    getSavedIntakeAssessmentDetails(daDetails: IntakeDATypeDetail[], intakeNo: string) {
        this.token = this._authService.getCurrentUser();
        this.intakeNumber = intakeNo;
        daDetails.map(item => {
            const savedAssessmentRequest = new IntakeAssessmentRequestIds();
            savedAssessmentRequest.intakeservicerequesttypeid = '';
            savedAssessmentRequest.intakeservicerequestsubtypeid = '';
            savedAssessmentRequest.agencycode = item.agencycode;
            savedAssessmentRequest.intakenumber = this.id;
            savedAssessmentRequest.target = 'Intake';
            this.assessmentRequestDetail = savedAssessmentRequest;
        });
        this.getPage(1);
    }
    onDASubTypeChange(option: any) {
        if (option.value) {
            this.intakeDATypeDetail.DasubtypeKey = option.value;
            this.intakeDATypeDetail.DasubtypeText = option.label;
        }
    }

    onDATypeChange(option: any) {
        this.intakeDATypeDetail = new IntakeDATypeDetail();
        this.intakeDATypeDetail.DaTypeKey = option.value;
        this.intakeDATypeDetail.DaTypeText = option.label;
        const url = NewUrlConfig.EndPoint.Intake.DATypeUrl + '?filter';
        this.daSubTypeDropDownItems$ = this._commonService
            .getArrayList(
                {
                    include: 'servicerequestsubtype',
                    where: { intakeservreqtypeid: this.intakeDATypeDetail.DaTypeKey },
                    method: 'get',
                    nolimit: true
                },
                url
            )
            .map(data => {
                return data[0].servicerequestsubtype.map(res => new DropdownModel({ text: res.description, value: res.servicerequestsubtypeid }));
            });
    }

    getDRAIScore(result) {
        if (result && result.data) {
            result.data.forEach(element => {
                if (element.titleheadertext === 'Intake Detention Risk Assessment Instrument') {
                    if (element.intakassessment && element.intakassessment.length > 0) {
                        const latestDrai = element.intakassessment[0].submissiondata;
                        const scores = {
                            score: latestDrai.score,
                            value: latestDrai.value,
                            AD: latestDrai.AD,
                            SD: latestDrai.SD,
                            SD2: latestDrai.SD2
                        };
                        this.assmntScores.DRAI = scores;
                        this.scoresSubject$.next(this.assmntScores);
                    }
                }
            });
        }
    }
    getMCAPScore(result) {
        if (result && result.data) {
            result.data.forEach(element => {
                if (element.titleheadertext === 'MCASP Risk Assessment') {
                    if (element.intakassessment && element.intakassessment.length > 0) {
                        const latestMcasp = element.intakassessment[0].submissiondata;
                        const scores = {
                            dhs: latestMcasp.dhs1,
                            shs: latestMcasp.shs2,
                            risklevel: latestMcasp.risklevel
                        };
                        this.assmntScores.MCASP = scores;
                        this.scoresSubject$.next(this.assmntScores);
                    }
                }
            });
        }
    }

    isAssmentCompelete(submissionData: any) {
        if (submissionData) {
            if (submissionData.Complete === true) {
                return 1;
            }
            if (submissionData.submit === true) {
                return 2;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}

