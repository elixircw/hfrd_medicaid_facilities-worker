import * as moment from 'moment';
import { InvolvedPerson } from '../_entities/newintakeModel';
import { AuthService } from '../../../../@core/services';
import { EvaluationFields } from '../_entities/newintakeSaveModel';

export class AssessmentPreFill {
    constructor(private intakeData: InvolvedPerson[], private evalFields: EvaluationFields, private _authService: AuthService) { }
    get caseHeadDetail() {
        return this.intakeData.find((item: InvolvedPerson) => item.Role === 'CASEHD');
    }
    get careGiverDetails() {
        return this.intakeData.find((item: InvolvedPerson) => item.Role === 'CARTKR');
    }
    get reportedChildDetails() {
        return this.intakeData.find((item: InvolvedPerson) => item.Role === 'RC');
    }
    get childDetails() {
        return this.intakeData.find((item: InvolvedPerson) => item.Role === 'CHILD');
    }
    // get supervisorDetail() {
    //     return this.intakeData.routinginfo.find((item) => item.fromrole === 'Supervisor');
    // }
    // get caseWorkerDetail() {
    //     return this.intakeData.routinginfo.find((item) => item.torole === 'Case Worker');
    // }
    get youthDetail() {
        return this.intakeData.find((item: InvolvedPerson) => item.Role === 'Youth');
    }
    // public fillSafeC(submissionData, daNumber) {
    //     submissionData['dateassessmentinitiated'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     if (this.caseHeadDetail) {
    //         submissionData['caseheadsname'] = this.caseHeadDetail.fullName;
    //         submissionData['relationship'] = this.caseHeadDetail.RelationshiptoRA;
    //         if (moment(this.caseHeadDetail.DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //             const rCDob = moment(this.caseHeadDetail.DobFormatted, 'MM/DD/YYYY').toDate();
    //             const year = moment().diff(rCDob, 'years');
    //             submissionData['age'] = year;
    //         }
    //     }
    //     if (this.careGiverDetails.length) {
    //         submissionData['nameofcaregiver'] = this.careGiverDetails[0].fullName;
    //     }
    //     submissionData['cpscaseid'] = daNumber;
    //     if (this.reportedChildDetails) {
    //         submissionData['childsname'] = this.reportedChildDetails.fullName;
    //         if (moment(this.reportedChildDetails.DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //             const rCDob = moment(this.reportedChildDetails.DobFormatted, 'MM/DD/YYYY').toDate();
    //             const year = moment().diff(rCDob, 'years');
    //             submissionData['age'] = year;
    //         }
    //     }
    //     if (this.childDetails.length) {
    //         submissionData['addchildren'] = [];
    //         this.childDetails.forEach((child) => {
    //             if (moment(child.DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //                 const childDob = moment(child.DobFormatted, 'MM/DD/YYYY').toDate();
    //                 const year = moment().diff(childDob, 'years');
    //                 submissionData['addchildren'].push({ seconename: child.fullName, seconeage: year });
    //             } else {
    //                 submissionData['addchildren'].push({ seconename: child.fullName, seconeage: '' });
    //             }
    //         });
    //     }
    //     if (this.supervisorDetail) {
    //         submissionData['supervisorname'] = this.supervisorDetail.fromusername;
    //         submissionData['supervisortitle'] = this.supervisorDetail.fromrole;
    //         submissionData['safetyassessmentapprovaldate'] = this.supervisorDetail.routedon ? this.supervisorDetail.routedon : '';
    //     }
    //     if (this.caseWorkerDetail) {
    //         submissionData['workersname'] = this.caseWorkerDetail.tousername;
    //         submissionData['workertitle'] = this.caseWorkerDetail.torole;
    //         submissionData['safetyassessmentcompletiondate'] = this.caseWorkerDetail.routedon ? this.caseWorkerDetail.routedon : '';
    //     }
    //     return submissionData;
    // }
    // public fillCansF(submissionData, daNumber) {
    //     submissionData['caseheaddatetime'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     if (this.caseHeadDetail) {
    //         submissionData['caseheadname'] = this.caseHeadDetail.fullName;
    //         submissionData['caregiverrelationship'] = this.caseHeadDetail.RelationshiptoRA;
    //     }
    //     if (this.careGiverDetails.length) {
    //         submissionData['familygrid'] = [];
    //         this.careGiverDetails.forEach((child) => {
    //             if (!this.reportedChildDetails || !this.reportedChildDetails.school) {
    //                 this.reportedChildDetails.school = [];
    //                 this.reportedChildDetails.school.push({ educationname: '' });
    //             }
    //             if (moment(child.DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //                 const childDob = moment(child.DobFormatted, 'MM/DD/YYYY').toDate();
    //                 const year = moment().diff(childDob, 'years');
    //                 submissionData['familygrid'].push({
    //                     caregiveryouthname: child.fullName,
    //                     caregiveryouthdob: child.DobFormatted,
    //                     caregiveryouthage: year,
    //                     caregiveryouthrelationship: child.RelationshiptoRA,
    //                     caregiveryouthschool: this.reportedChildDetails.school[0].educationname
    //                 });
    //             } else {
    //                 submissionData['familygrid'].push({
    //                     caregiveryouthname: child.fullName,
    //                     caregiveryouthdob: '',
    //                     caregiveryouthage: '',
    //                     caregiveryouthrelationship: child.RelationshiptoRA,
    //                     caregiveryouthschool: this.reportedChildDetails.school[0].educationname
    //                 });
    //             }
    //         });
    //     }
    //     submissionData['cpscaseid'] = daNumber;
    //     if (this.supervisorDetail) {
    //         submissionData['supervisorname'] = this.supervisorDetail.fromusername;
    //         submissionData['supervisortitle'] = this.supervisorDetail.fromrole;
    //     }
    //     if (this.caseWorkerDetail) {
    //         submissionData['workersname'] = this.caseWorkerDetail.fromusername;
    //         submissionData['workertitle'] = this.caseWorkerDetail.fromrole;
    //         submissionData['workernameandId'] = this.caseWorkerDetail.tousername;
    //     }
    //     submissionData['workerdate'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     submissionData['supervisordate'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     return submissionData;
    // }
    // public fillMFRR(submissionData, daNumber) {
    //     submissionData['assessmentInitiated'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     if (this.caseHeadDetail) {
    //         submissionData['caseheadname'] = this.caseHeadDetail.fullName;
    //     }
    //     if (this.childDetails.length) {
    //         submissionData['familygrid'] = [];
    //         this.childDetails.forEach((child) => {
    //             if (moment(child.DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //                 const childDob = moment(child.DobFormatted, 'MM/DD/YYYY').toDate();
    //                 const year = moment().diff(childDob, 'years');
    //                 submissionData['familygrid'].push({ childrenname: child.fullName, childrendob: child.DobFormatted, childrenage: year, childrenrelationship: child.RelationshiptoRA });
    //             } else {
    //                 submissionData['familygrid'].push({ childrenname: child.fullName, childrendob: '', childrenage: '', childrenrelationship: child.RelationshiptoRA });
    //             }
    //         });
    //     }
    //     submissionData['servicecaseID'] = daNumber;
    //     return submissionData;
    // }
    // public fillMFRA(submissionData, daNumber) {
    //     submissionData['assessmentInitiated'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     if (this.caseHeadDetail) {
    //         submissionData['caseheadname'] = this.caseHeadDetail.fullName;
    //     }
    //     if (this.childDetails.length) {
    //         submissionData['familygrid'] = [];
    //         this.childDetails.forEach((child) => {
    //             if (moment(child.DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //                 const childDob = moment(child.DobFormatted, 'MM/DD/YYYY').toDate();
    //                 const year = moment().diff(childDob, 'years');
    //                 submissionData['familygrid'].push({ childrenname: child.fullName, childrendob: child.DobFormatted, childrenrelationship: child.RelationshiptoRA, primarycaregiver: '' });
    //             } else {
    //                 submissionData['familygrid'].push({ childrenname: child.fullName, childrendob: '', childrenrelationship: child.RelationshiptoRA, primarycaregiver: '' });
    //             }
    //         });
    //     }
    //     submissionData['servicecaseID'] = daNumber;
    //     return submissionData;
    // }

    // public fillSafeCOHP(submissionData, daNumber) {
    //     submissionData['dateassessmentinitiated'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     submissionData['approveddate'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //     submissionData['caseid'] = daNumber;
    //     if (this.caseHeadDetail) {
    //         submissionData['casehead'] = this.caseHeadDetail.fullName;
    //     }
    //     if (this.reportedChildDetails) {
    //         submissionData['clientname'] = this.reportedChildDetails.fullName;
    //         // submissionData['clientid'] = this.childDetails[0].Pid;
    //         if (moment(this.reportedChildDetails.DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //             const childDob = moment(this.reportedChildDetails.DobFormatted, 'MM/DD/YYYY').toDate();
    //             const year = moment().diff(childDob, 'years');
    //             submissionData['dob'] = year;
    //         }
    //     }
    //     if (this.supervisorDetail) {
    //         submissionData['supervisor'] = this.supervisorDetail.fromusername;
    //     }
    //     return submissionData;
    // }

    // public fillHomeHealthReport(submissionData, daNumber) {
    //     submissionData['casenumber'] = daNumber;
    //     if (this.caseHeadDetail) {
    //         submissionData['nameofcaretaker'] = this.caseHeadDetail.fullName;
    //     }
    //     if (this.careGiverDetails.length) {
    //         submissionData['nameofcaretaker'] = this.careGiverDetails[0].fullName;
    //         submissionData['caregiverdob'] = this.careGiverDetails[0].DobFormatted;
    //         if (this.careGiverDetails[0].address && this.careGiverDetails[0].address.length) {
    //             submissionData['address'] = this.careGiverDetails[0].address[0].address;
    //             submissionData['city'] = this.careGiverDetails[0].address[0].city;
    //             submissionData['state'] = this.careGiverDetails[0].address[0].state;
    //             submissionData['zip'] = this.careGiverDetails[0].address[0].zipcode;
    //         }
    //         if (moment(this.careGiverDetails[0].DobFormatted, 'MM/DD/YYYY', true).isValid()) {
    //             const dob = moment(this.careGiverDetails[0].DobFormatted, 'MM/DD/YYYY').toDate();
    //             const year = moment().diff(dob, 'years');
    //             submissionData['age'] = year;
    //         }
    //     }
    //     if (this.caseWorkerDetail) {
    //         submissionData['workername1'] = this.caseWorkerDetail.tousername;
    //     }
    //     if (this.childDetails.length) {
    //         submissionData['childerenname'] = [];
    //         submissionData['childrendob1'] = [];
    //         this.childDetails.forEach((child) => {
    //             submissionData['childerenname'].push(child.fullName);
    //             submissionData['childrendob1'].push(child.DobFormatted);
    //         });
    //     }
    //     return submissionData;
    // }

    // public fillTransportationPlan(submissionData, daNumber) {
    //     if (this.reportedChildDetails) {
    //         submissionData['studentname'] = this.reportedChildDetails.fullName;
    //         // submissionData['Studentschoolidno'] = this.reportedChildDetails.Pid;
    //         submissionData['studentdob'] = moment(this.reportedChildDetails.DobFormatted, 'mm/DD/YYYY').toDate();
    //         if (this.reportedChildDetails.school) {
    //             submissionData['currentgrade'] = this.reportedChildDetails.school[0].currentgradetypekey;
    //             submissionData['localdepartmentofsocialservices'] = this.reportedChildDetails.school[0].educationname;
    //         }
    //     }
    //     return submissionData;
    // }

    // public fillBestInterestDetermination(submissionData, daNumber, currentUser) {
    //     if (this.reportedChildDetails) {
    //         submissionData['date'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
    //         submissionData['studentsname'] = this.reportedChildDetails.fullName;
    //         // submissionData['assignedstudent'] = this.reportedChildDetails.Pid;
    //         submissionData['dob'] = moment(this.reportedChildDetails.DobFormatted, 'MM/DD/YYYY').toDate();
    //         if (this.reportedChildDetails.school) {
    //             submissionData['grade'] = this.reportedChildDetails.school[0].currentgradetypekey;
    //             submissionData['previousschool'] = this.reportedChildDetails.school[0].educationname;
    //             if (this.reportedChildDetails.school.length > 1) {
    //                 submissionData['currentschool'] = this.reportedChildDetails.school[this.reportedChildDetails.school.length - 1].educationname;
    //             }
    //         }
    //         submissionData['saseworkername'] = currentUser;
    //     }
    //     return submissionData;
    // }

    public fillIntakeDetentnRiskAssmntInstrument(submissionData) {
        submissionData['panel86083281892257Columns2YouthName'] = this.youthDetail.fullName;
        return submissionData;
    }
    public fillDomsticViolncLethalityAssmnt(submissionData) {
        submissionData['panel8874437846051964ColumnsPractitioner'] = this._authService.getCurrentUser().user.userprofile.fullname;
        return submissionData;
    }

    public fillDMSTScreeningTool(submissionData) {
        submissionData['panel7282047397273181TableYouthName'] = this.youthDetail.fullName;
        submissionData['panel7282047397273181Table2Dob'] = this.youthDetail.Dob;
        submissionData['panel7282047397273181Table2Ethnicity'] = this.youthDetail.Ethicity;
        submissionData['panel7282047397273181Table2Race'] = this.youthDetail.Race;
        submissionData['panel7282047397273181Table3Gender'] = this.youthDetail.Gender;
        submissionData['panel7282047397273181Table3Interviewername'] = this._authService.getCurrentUser().user.userprofile.fullname;
        submissionData['panel7282047397273181Table3JurisdictionofResidence'] = this.youthDetail.County;
        submissionData['panel7282047397273181Table2Interviewdate'] = new Date();
        submissionData['panel7282047397273181Table2Age'] = this.getYouthAge(this.youthDetail);
        submissionData['panel7282047397273181TableYouthCjamsid'] = this.youthDetail.Pid;
        return submissionData;
    }

    public fillMCASPriskAssment(submissionData) {
        return submissionData;
    }

    private getYouthAge(person: InvolvedPerson) {
        let personDob;
        let offenceDate;
        personDob = new Date(person.Dob);
        if (!this.evalFields.unknownrange && (this.evalFields.allegedoffensedate || this.evalFields.begindate)) {
            offenceDate = this.evalFields.dateRange ? this.evalFields.begindate : this.evalFields.allegedoffensedate;
            offenceDate = new Date(offenceDate);
        } else {
            offenceDate = new Date();
        }
        const timeDiff = offenceDate - personDob;
        const youthAge = new Date(timeDiff); // miliseconds from epoch
        return Math.abs(youthAge.getUTCFullYear() - 1970);
    }
}
