import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProviderReferral } from '../_entities/newreferralModel';
import { AlertService } from '../../../../@core/services/alert.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { ReferralUrlConfig } from '../../provider-referral-url.config';
import { ProviderReferralModule } from '../../provider-referral.module';
import { ValidationService, AuthService } from '../../../../@core/services';
import { CommonDropdownsService, CommonHttpService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  selector: 'add-referral',
  templateUrl: './add-referral.component.html',
  styleUrls: ['./add-referral.component.scss']
})
export class AddReferralComponent implements OnInit {
  addReferralForm: FormGroup;
  referral: ProviderReferral;
  programType: string;
  stateList$: Observable<any[]>;
  accreditationList$ : Observable<any[]>;
  selectedProgram: string;

  @Input()
  referralId;
  @Output()
  addReferralEventEmitter = new EventEmitter();
  @Input()
  editReferralSubject = new Subject<ProviderReferral>();

  providerReferralType = ['Private'];

  programsTypes$: Observable<DropdownModel[]>;
  countyList$: Observable<DropdownModel[]>;
  programs = [];
  programTypes = [];
  selectedProgramType: string;
  isPublic = false;
  isCPA: Boolean = false;
  suggestedAddress$: Observable<any[]>;
  isDisable = false;
  roleId: AppUser;
  roleType = ['PVRDJSSD', 'PVRDJSRS', 'PVRDJSPD', 'PVRDJSR', 'PVRDJSQA'];
  programTypeList: DropdownModel[] = [];
  agency: any;
  programnamesList: string[] = [];
  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _commonDropdownService: CommonDropdownsService,
    private _authService: AuthService
  ) {
  }

  ngOnInit() {
    this.roleId = this._authService.getCurrentUser();
    if (this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
      || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
      || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA') {
      this.agency = 'DJS';
    } else {
      this.agency = 'OLM';
    }

    // We initialize selection with Public providers, then change the dropdown values whenever the provider type selcetion changes
    this.loadDropDown('Private');

    this.addReferralForm = this._formBuilder.group({
      provider_referral_id: [''],
      provider_referral_nm: [''],
      provider_referral_last_nm: [''],
      provider_referral_first_nm: [''],

      provider_referral_program: [''],
      provider_program_type: [''],
      provider_program_name: [''],

      adr_email_tx: ['', [ValidationService.mailFormat]],
      adr_home_phone_tx: [''],
      adr_cell_phone_tx: [''],
      adr_fax_tx: [''],
      adr_line2: [''],
      adr_street_nm: [null],
      adr_city_nm: [''],
      adr_state_cd: [''],
      adr_county_cd: [''],
      adr_country_tx: [''],
      adr_zip5_no: [null],
      is_son: [''],
      is_taxid: [''],
      state_funding_eligible: [''],
      is_rfp: [''],
      parent_entity: [''],
      parent_entity_taxid: [null],
      corporation_entity: [''],
      corporation_entity_taxid: [null],
      provider_referral_med: [''],
      referral_status: [''],
      referral_decision: [''],
      comment: [''],
      state: [''],

      narrative: [''],
      provider_catergory: ['Private'],
      profit:[null],
      accreditation: [null]
    });
    this.subscribeToEditData();
  }

  subscribeToEditData() {
    this.editReferralSubject.subscribe((data) => {
      if (data) {
        this.addReferralForm.patchValue(data);
        this.addReferralForm.controls['provider_referral_program'].patchValue(this.programnamesList[data.provider_referral_program]);
        const provider_program_type = data.provider_program_type as any;
        this.selectedProgramNames(this.programnamesList[data.provider_referral_program], true, data.provider_referral_program);
        if (provider_program_type && provider_program_type.length) {
          const seletectProgram = provider_program_type.map((item) => {
            return item.program_type;
          });
          // this.programType = this.programnamesList[data.provider_referral_program];
          this.addReferralForm.controls['provider_program_type'].patchValue(seletectProgram);
        }
      } else {
        this.addReferralForm.reset();
        this.addReferralForm.patchValue({
          provider_catergory: 'Private'
        });
      }
    });
  }

  private loadDropDown(selectedProviderType: string) {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'post',
          nolimit: true,
          providertype: selectedProviderType
        },
        ReferralUrlConfig.EndPoint.Referral.listprogramnamesUrl
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        ReferralUrlConfig.EndPoint.Referral.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList('get', 'providerreferral/listcountycodes'),
    ])
      .map((result) => {
        return {
          programsTypes: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.programtype,
                value: res.programnames
              })
          ),
          stateList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),
          countyListitems: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.value_tx,
                value: res.picklist_value_cd
              })
          ),

        };
      })
      .share();

    this.programsTypes$ = source.pluck('programsTypes');

    this.programsTypes$.subscribe((data) => {
      this.programTypeList = data;
      if (data && data.length) {
        data.forEach((item) => {
          this.programnamesList[item.text] = item.value;
        });
      }
      if (this.roleType.indexOf(this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey) !== -1) {
        this.programTypeList = this.programTypeList.filter(type => type.text !== 'CPA');
      }

      if (this.agency !== 'DJS') {
        this.programTypeList = this.programTypeList.filter(type => type.text !== 'YSB');
    }
  });

    // this.stateDropDownItems$ = source.pluck('stateList');
    // this.countyList$ = source.pluck('countyListitems');
    this.stateList$ = this._commonDropdownService.getPickListByName('state');
    this.accreditationList$ = this._commonDropdownService.getPickListByName('accreditation');
this.countyList$ = this._commonDropdownService.getPickList('328');
  }

selectedProgramNames(event: any, isEditMode: boolean, isCPA ?: string) {
  if (!isEditMode) {
    const target = event.source.selected._element.nativeElement;
    this.programType = target.innerText.trim();
    this.programs = event.value;
    if (this.programType === 'CPA') {
      this.isCPA = true;
    } else {
      this.isCPA = false;
    }
  } else {
    this.programs = event;
    this.isCPA = isCPA === 'CPA';
  }
}

addProvider() {
  this.referral = this.addReferralForm.value;
  this.referral.provider_referral_program = this.programType;
  this.referral.provider_referral_id = this.referralId;
  this.referral.adr_email_tx = this.referral.adr_email_tx.toLowerCase();
  console.log(this.referral.provider_referral_id);
  this.saveReferralDraft();
  // (<any>$('#add-referral')).modal('hide');
  (<any>$('#intake-findprovider')).modal('hide');
}

onOptionChange(event: any) {
  this.isPublic = event.value === 'Public' ? false : true;

  // Set the dropdown values based on the provider type selected
  this.loadDropDown(event.value);
}



saveReferralDraft() {
  if (this.referral.referral_status && this.referral.referral_status !== '') {
    this._commonHttpService.update('', this.referral, 'providerreferral/updateproviderreferral').subscribe(
      (response) => {
        this._alertService.success('Referral Updated successfully!');
        this.loadReferralDetails();
      },
      (error) => {
        this._alertService.error('Unable to save referral, please try again.');
        console.log('Save Referral Error', error);
        return false;
      }
    );
  } else {
    this.referral.referral_status = 'Draft';
    this.referral.referral_decision = 'Pending';
    this.referral.agency = this.agency;
    this._commonHttpService.create(this.referral, 'providerreferral/addproviderreferral').subscribe(
      (response) => {
        if (response) {
          /* if (response.statuscode === '10001') {
            this._alertService.success('Referral saved successfully!');
            (<any>$('#add-referral')).modal('hide');
            this.loadReferralDetails();
          } else if (response.statuscode === '10002') {
            this._alertService.warn('This is an existing Tax ID, please verify the Tax ID with the Applican!');
            return false;
          } else {
            this._alertService.error('Unable to save referral, please try again.');
          return false;
          } */
          if (response[0].addproviderreftable === 'Tax id already exists') {
            this._alertService.warn('This is an existing Tax ID, please verify the Tax ID with the Applican!');
            return false;
          } else {
            this._alertService.success('Referral saved successfully!');
            (<any>$('#add-referral')).modal('hide');
            this.loadReferralDetails();
          }
        }
      },
      (error) => {
        this._alertService.error('Unable to save referral, please try again.');
        console.log('Save Referral Error', error);
        return false;
      }
    );
  }

}

loadReferralDetails() {
  this._commonHttpService
    .create(
      {
        page: 1,
        limit: 10,
        where: {
          referralnumber: this.referralId
        }
      },
      'providerreferral/getproviderreferral'
    )
    .subscribe(
      (response) => {
        if (response.data[0]) {
          const intakeModel = response.data[0];
          this.addReferralEventEmitter.emit(response.data);
        }
      },
      (error) => {
        this._alertService.error('Unable to fetch saved intake details.');
      }
    );
}


getSuggestedAddress() {
  this.suggestAddress();
}
suggestAddress() {
  this._commonHttpService
    .getArrayListWithNullCheck(
      {
        method: 'post',
        where: {
          prefix: this.addReferralForm.value.adr_street_nm,
          cityFilter: '',
          stateFilter: '',
          geolocate: '',
          geolocate_precision: '',
          prefer_ratio: 0.66,
          suggestions: 25,
          prefer: 'MD'
        }
      },
      'People/suggestaddress'
    ).subscribe(
      (result: any) => {
        if (result.length > 0) {
          this.suggestedAddress$ = result;
        }
      }
    );
}
selectedAddress(model) {
  this.addReferralForm.patchValue({
    adr_street_nm: model.streetLine ? model.streetLine : '',
    adr_city_nm: model.city ? model.city : '',
    state: model.state ? model.state : ''
  });
  const addressInput = {
    street: model.streetLine ? model.streetLine : '',
    street2: '',
    city: model.city ? model.city : '',
    state: model.state ? model.state : '',
    zipcode: '',
    match: 'invalid'
  };
  this._commonHttpService
    .getSingle(
      {
        method: 'post',
        where: addressInput
      },
      'People/validateaddress'
    )
    .subscribe(
      (result) => {
        if (result[0].analysis) {
          this.addReferralForm.patchValue({
            adr_zip5_no: result[0].components.zipcode ? result[0].components.zipcode : ''
          });
          this.loadCounty(this.addReferralForm.value.state);
          if (result[0].metadata.countyName) {
            this._commonHttpService.getArrayList(
              {
                nolimit: true,
                where: { referencetypeid: 306, mdmcode: this.addReferralForm.value.state, description: result[0].metadata.countyName }, method: 'get'
              },
              'referencevalues?filter'
            ).subscribe(
              (resultresp) => {
                if (resultresp[0]) {
                  this.addReferralForm.patchValue({
                    adr_county_cd: resultresp[0].ref_key
                  });
                }
              }
            );
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
}
loadCounty(state) {
  console.log(state);
  this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
    this.countyList$ = Observable.of(countyList);
  });
}

getStatus() {
  const isSon = this.addReferralForm.getRawValue().is_nson;
  const isProposal = this.addReferralForm.getRawValue().is_rfp;

  if (isSon === 'NO' && isProposal === 'NO') {
    (<any>$('#add-referral')).modal('hide');
    (<any>$('#criteria-popup')).modal('show');
    this.isDisable = true;
    this.addReferralForm.get('provider_referral_nm').disable();
    this.addReferralForm.get('provider_referral_first_nm').disable();
    this.addReferralForm.get('provider_referral_last_nm').disable();
    this.addReferralForm.get('adr_email_tx').disable();
    this.addReferralForm.get('adr_home_phone_tx').disable();
    this.addReferralForm.get('adr_cell_phone_tx').disable();
    this.addReferralForm.get('adr_fax_tx').disable();
    this.addReferralForm.get('adr_street_nm').disable();
    this.addReferralForm.get('adr_line2').disable();
    this.addReferralForm.get('state').disable();
    this.addReferralForm.get('adr_city_nm').disable();
    this.addReferralForm.get('adr_county_cd').disable();
    this.addReferralForm.get('adr_zip5_no').disable();
  } else {
    this.isDisable = false;
    this.addReferralForm.get('provider_referral_nm').enable();
    this.addReferralForm.get('provider_referral_first_nm').enable();
    this.addReferralForm.get('provider_referral_last_nm').enable();
    this.addReferralForm.get('adr_email_tx').enable();
    this.addReferralForm.get('adr_home_phone_tx').enable();
    this.addReferralForm.get('adr_cell_phone_tx').enable();
    this.addReferralForm.get('adr_fax_tx').enable();
    this.addReferralForm.get('adr_street_nm').enable();
    this.addReferralForm.get('adr_line2').enable();
    this.addReferralForm.get('state').enable();
    this.addReferralForm.get('adr_city_nm').enable();
    this.addReferralForm.get('adr_county_cd').enable();
    this.addReferralForm.get('adr_zip5_no').enable();
  }
}

resetForm() {
  this.addReferralForm.reset();
}
}

