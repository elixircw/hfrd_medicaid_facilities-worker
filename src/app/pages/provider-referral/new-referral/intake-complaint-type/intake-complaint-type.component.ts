import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormControl
} from '@angular/forms';
import {
    DropdownModel,
    PaginationRequest
} from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { Subject } from 'rxjs/Rx';
import {
    ComplaintTypeCase,
    ChoosenAllegation,
    EvaluationFields
} from '../_entities/newintakeSaveModel';
import {
    IntakePurpose,
    SubType,
    AllegationItem
} from '../_entities/newintakeModel';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { REGEX } from '../../../../@core/entities/constants';
import * as reason from './_configurations/reason.json';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent } from '@angular/material';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-complaint-type',
    templateUrl: './intake-complaint-type.component.html',
    styleUrls: ['./intake-complaint-type.component.scss']
})
export class IntakeComplaintTypeComponent implements OnInit {
    serviceTypes$: Observable<DropdownModel[]>;
    subServiceTypes$: Observable<DropdownModel[]>;
    allegations$: Observable<any[]>;
    selectedAllegation$: Observable<any[]>;
    filteredAllegations$: Observable<any[]>;
    allegations: any = [];
    selectedAllegations: any = [];
    servicetype: DropdownModel;
    createdCases: ComplaintTypeCase[] = [];
    caseCreationFormGroup: FormGroup;
    caseEditFormGroup: FormGroup;
    selectedPurpose: IntakePurpose;
    purposeList: IntakePurpose[];
    subServiceTypes: SubType[];
    editCase: ComplaintTypeCase;
    deleteCaseID: string;
    pdfFiles: { fileName: string; images: string[] }[] = [];
    images: string[] = [];
    allegationDropDownItems: DropdownModel[];
    allegationItems: AllegationItem[];
    @Input() purposeInputSubject$ = new Subject<IntakePurpose>();
    @Input() createdCaseInputSubject$ = new Subject<ComplaintTypeCase[]>();
    @Input() createdCaseOuptputSubject$ = new Subject<ComplaintTypeCase[]>();
    @Input() reviewstatus: string;
    @Input() offenceCategoriesInputSubject$ = new Subject<any>();
    @Input() evalFieldsInputSubject$ = new Subject<EvaluationFields>();
    @Input() evalFieldsOutputSubject$ = new Subject<EvaluationFields>();
    filteredAllegationItems: AllegationItem[] = [];
    choosenAllegation: ChoosenAllegation;
    roleId: AppUser;
    // selectedDaTypeKeyforEdit: string;
    selectedcaseIndex: number;
    groupSetupForm: FormGroup;
    groupDAReasonForm: FormGroup;
    reasonDropdown: DropdownModel[];
    groupSelectedDAIds: string[];
    ungroupSelectedGroupIds: string[];
    token: AppUser;
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = false;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    @ViewChild('allegationInput') allegationInput: ElementRef;
    allegationCtrl = new FormControl();
    allegation: string[];
    indicators: any[];
    offenceCategories: any[] = [];
    selectedAllegationList$: Observable<any[]>;
    isComplaintTypeDisabled = false;
    evaluationFields: EvaluationFields;
    constructor(
        private _commonHttpService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _authService: AuthService
    ) {


    }

    ngOnInit() {
        this.token = this._authService.getCurrentUser();
        this.buildFormGroup();
        this.reasonDropdown = <any>reason;
        this.roleId = this._authService.getCurrentUser();
        this.purposeInputSubject$.subscribe(purpose => {
            this.selectedPurpose = purpose;
            this.isComplaintTypeDisabled = false;
            this.caseCreationFormGroup.get('serviceType').enable();
            if (purpose.teamtype) {
                this.listServicetypes(purpose.teamtype.teamtypekey);
            }
            this.caseCreationFormGroup.patchValue({
                serviceType: this.selectedPurpose.intakeservreqtypeid
            });
            this.isComplaintTypeDisabled = true;
            this.caseCreationFormGroup.get('serviceType').disable();
        });
        this.createdCaseOuptputSubject$.subscribe(createdCases => {
            if (createdCases) {
                this.createdCases = createdCases;
                this.createdCaseInputSubject$.next(this.createdCases);
            } else {
                this.createdCases = [];
            }
        });

        this.listenAllegationList();
        this.listenSelectedAllegationList();
    }

    buildFormGroup() {
        this.caseCreationFormGroup = this.formBuilder.group({
            serviceType: ['', Validators.required],
            selectedAllegations: [[]],
            subServiceType: ['', Validators.required],
            allegationId: [{ value: '', disabled: true }]
        });
        this.caseEditFormGroup = this.formBuilder.group({
            serviceType: ['', Validators.required],
            subServiceType: ['', Validators.required]
        });
        this.groupSetupForm = this.formBuilder.group({
            DAItems: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            selectedDAItemsGroupNo: ['']
        });
        this.groupDAReasonForm = this.formBuilder.group({
            reasonType: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            reasonComments: ['']
        });
    }

    listServicetypes(teamtypekey) {
        const checkInput = {
            nolimit: true,
            where: { teamtypekey: teamtypekey },
            method: 'get',
            order: 'description'
        };

        this.serviceTypes$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter'
            )
            .map(result => {
                this.purposeList = result;
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.intakeservreqtypeid
                        })
                );
            });
    }

    getSelectedPurpose(purposeID): IntakePurpose {
        return this.purposeList.find(
            puroposeItem => puroposeItem.intakeservreqtypeid === purposeID
        );
    }

    getSelectedSubtype(subTypeID): SubType {
        return this.subServiceTypes.find(
            subServiceType =>
                subServiceType.servicerequestsubtypeid === subTypeID
        );
    }

    listServiceSubtype(allegationIDs: string[]) {
        const checkInput = {
            where: { 'type': 'AL', 'allegationids': allegationIDs },
            method: 'get',
            order: 'description'
        };
        this.subServiceTypes$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsSubTypes + '/?filter'
            )
            .map(result => {
                this.subServiceTypes = result;
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.servicerequestsubtypeid
                        })
                );
            });
    }

    listAllegations(intakeservreqtypeid) {
        const checkInput = {
            where: { intakeservreqtypeid: intakeservreqtypeid },
            method: 'get',
            order: 'name'
        };
        this.filteredAllegations$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsUrl + '/?filter'
            )
            .map(result => {
                this.allegations = result;
                return result;
            });
    }

    loadAllegations(serviceType: DropdownModel) {
        this.listAllegations(serviceType.value);
    }
    isSubTypeExists(servicerequestsubtypeid: string) {
        if (this.createdCases) {
            return this.createdCases.find(
                createdCase =>
                    createdCase.subServiceTypeID === servicerequestsubtypeid
            );
        }
        return false;
    }

    loadAllegationSubtypes() {
        if (this.caseCreationFormGroup.value.selectedAllegations.length > 0) {
            this.listServiceSubtype(this.caseCreationFormGroup.value.selectedAllegations);
        }
    }



    addCase() {
        if (this.caseCreationFormGroup.valid) {
            /* if (
                 this.isSubTypeExists(
                     this.caseCreationFormGroup.value.subServiceType
                 )
             ) {
                 this._alertService.error(
                     'Already Case created for this sub type '
                 );
                 return false;
             }*/
            const caseForm = this.caseCreationFormGroup.getRawValue();
            this._commonHttpService
                .getArrayList({}, NewUrlConfig.EndPoint.Intake.NextnumbersUrl)
                .subscribe(result => {
                    const complaintTypeCase: ComplaintTypeCase = new ComplaintTypeCase();
                    complaintTypeCase.caseID = result['nextNumber'];
                    const selectedPurpose = this.getSelectedPurpose(
                        caseForm.serviceType
                    );
                    const selectedSubtype = this.getSelectedSubtype(
                        caseForm.subServiceType
                    );
                    complaintTypeCase.serviceTypeID = caseForm.serviceType;
                    complaintTypeCase.serviceTypeValue =
                        selectedPurpose.description;
                    complaintTypeCase.subServiceTypeID =
                        selectedSubtype.servicerequestsubtypeid;
                    complaintTypeCase.subSeriviceTypeValue =
                        selectedSubtype.description;
                    complaintTypeCase.choosenAllegation = this.caseCreationFormGroup.value.selectedAllegations.map(allegationId => {
                        const allegation = this.findAllegationById(allegationId);
                        return {
                            allegationID: allegation.allegationid,
                            allegationValue: allegation.name,
                            indicators: []
                        };
                    });

                    this.createdCases.push(complaintTypeCase);
                    this.updateCreatedCases();
                    this.resetCaseCreationForm();

                });
        }
    }

    deleteCase(intakeCase: ComplaintTypeCase) {
        this.deleteCaseID = intakeCase.caseID;
    }

    onEditCase(intakeCase: ComplaintTypeCase) {
        this.caseEditFormGroup.patchValue({
            serviceType: intakeCase.serviceTypeID,
            subServiceType: intakeCase.subServiceTypeID
        });
        this.editCase = intakeCase;
    }

    updateCreatedCases() {
        this.createdCaseInputSubject$.next(this.createdCases);
    }
    isUpdateDisabled() {
        if (this.editCase) {
            return (
                this.caseEditFormGroup.value.subServiceType ===
                this.editCase.subServiceTypeID
            );
        }
        return false;
    }
    updateSubTypes() {
        if (this.caseEditFormGroup.valid) {
            if (
                this.isSubTypeExists(
                    this.caseEditFormGroup.value.subServiceType
                )
            ) {
                this._alertService.error(
                    'Already Case created for this sub type '
                );
                return false;
            }

            this.createdCases.find(complaintTypeCase => {
                if (complaintTypeCase.caseID === this.editCase.caseID) {
                    const selectedPurpose = this.getSelectedPurpose(
                        this.caseEditFormGroup.getRawValue().serviceType
                    );
                    const selectedSubtype = this.getSelectedSubtype(
                        this.caseEditFormGroup.value.subServiceType
                    );
                    complaintTypeCase.serviceTypeID = this.caseEditFormGroup.getRawValue().serviceType;
                    complaintTypeCase.serviceTypeValue =
                        selectedPurpose.description;
                    complaintTypeCase.subServiceTypeID =
                        selectedSubtype.servicerequestsubtypeid;
                    complaintTypeCase.subSeriviceTypeValue =
                        selectedSubtype.description;
                    $('#editClose').click();
                    return true;
                }
            });
        }
    }

    deleteCaseConfirm() {
        this.createdCases = this.createdCases.filter(
            createdCase => createdCase.caseID !== this.deleteCaseID
        );
        this.updateCreatedCases();
        (<any>$('#delete-case-popup')).modal('hide');
    }

    isPeaceOrder(complaintCase: ComplaintTypeCase): boolean {
        return complaintCase.subSeriviceTypeValue === 'Peace Order';
    }

    loadAllegation(caseIndex: number, daTypeKey: string) {
        this.selectedcaseIndex = caseIndex;
        this.choosenAllegation = new ChoosenAllegation();

        this.allegationItems = [];
        this.allegationDropDownItems = [];
        this.filteredAllegationItems = [];
        this.caseCreationFormGroup.patchValue({
            allegationId: ''
        });
        const url =
            NewUrlConfig.EndPoint.Intake.allegationsUrl + '/?filter';
        this._commonHttpService
            .getArrayList(
                {
                    where: { intakeservreqtypeid: daTypeKey },
                    method: 'get',
                    order: 'name',
                    nolimit: true
                },
                url
            )
            .subscribe(result => {
                this.allegationItems = result.map(
                    item => new AllegationItem(item)
                );
                this.allegationDropDownItems = this.allegationItems.map(
                    item =>
                        new DropdownModel({
                            text: item.name,
                            value: item.allegationid
                        })
                );
            });
    }

    allegationOnChange(option: any) {
        this.loadSelectedAllegation(option.value);
        this.choosenAllegation.allegationID = option.value;
        this.choosenAllegation.allegationValue = option.text;
    }

    loadSelectedAllegation(selectedAllegation: string) {
        this.filteredAllegationItems = this.allegationItems.filter(item => {
            return item.allegationid === selectedAllegation;
        });
        // console.log(this.filteredAllegationItems[0]);
    }

    indicatorsChecked(model: any, control) {
        if (!this.choosenAllegation) {
            this.choosenAllegation = new ChoosenAllegation();
        }
        const Index = this.choosenAllegation.indicators.findIndex(indicator => indicator.indicatorid === model.indicatorid);
        if (Index === -1) {
            this.choosenAllegation.indicators.push(model);
        } else {
            this.choosenAllegation.indicators.splice(Index, 1);
        }

    }

    saveAllegations() {
        let ispresent = false;
        this.createdCases[this.selectedcaseIndex].choosenAllegation.forEach(
            element => {
                if (
                    this.choosenAllegation.allegationID === element.allegationID
                ) {
                    element.indicators = this.choosenAllegation.indicators;
                    ispresent = true;
                }
            }
        );
        if (!ispresent) {
            this.createdCases[this.selectedcaseIndex].choosenAllegation.push(
                this.choosenAllegation
            );
        }
    }


    deleteAllegation(caseIndex: number, aligationIndex: number) {
        // console.log(caseIndex, aligationIndex);
        this.createdCases[caseIndex].choosenAllegation.splice(
            aligationIndex,
            1
        );
    }

    openGroupSetup() {
        if (
            this.groupSetupForm.value.DAItems &&
            this.groupSetupForm.value.DAItems.length >= 2 &&
            this.createdCases.filter(item => !item.GroupNumber).length >= 2
        ) {
            (<any>$('#groupReason')).modal('show');
        } else {
            this._alertService.error('Please select at least 2 Case');
        }
    }

    cancelGroupSetup() {
        this.groupDAReasonForm.reset();
        this.groupSetupForm.reset();
        (<any>$('#groupReason')).modal('hide');
    }

    saveGroupSetup() {
        if (
            this.groupSetupForm.value.DAItems &&
            (this.groupDAReasonForm.valid && this.groupDAReasonForm.dirty)
        ) {
            this.groupSelectedDAIds = this.groupSetupForm.value.DAItems;
            this._commonHttpService
                .getArrayList(
                    {},
                    'Nextnumbers/getNextNumber?apptype=GroupAuthorizationNumber'
                )
                .subscribe(result => {
                    this.createdCases = this.createdCases.map(item => {
                        this.groupSelectedDAIds.map(da => {
                            if (item.caseID === da) {
                                item.GroupNumber = result['nextNumber'];
                                item.GroupReasonType = this.groupDAReasonForm.value.reasonType;
                                item.GroupComment = this.groupDAReasonForm.value.reasonComments;
                            }
                        });
                        return item;
                    });
                    this.createdCaseInputSubject$.next(this.createdCases);
                    this.cancelGroupSetup();
                    return { nextNumber: result };
                });
        } else {
            this._alertService.error('Please select Grouping reason type');
        }
    }

    unGroupDAItems() {
        if (this.groupSetupForm.value.selectedDAItemsGroupNo) {
            this.ungroupSelectedGroupIds = this.groupSetupForm.value.selectedDAItemsGroupNo;
            this.createdCases = this.createdCases.map(item => {
                this.ungroupSelectedGroupIds.map(da => {
                    if (item.GroupNumber === da) {
                        item.GroupNumber = '';
                        item.GroupReasonType = '';
                        item.GroupComment = '';
                    }
                });
                return item;
            });
            this.createdCaseInputSubject$.next(this.createdCases);
        }
    }


    removeAllegation(allegationToRemove: any): void {
        this.selectedAllegations = this.selectedAllegations.filter(allegation => allegation.allegationid !== allegationToRemove.allegationid);
        this.loadAllegationSubtypes();
    }

    allegationSelected(event: MatAutocompleteSelectedEvent): void {

        if (!this.isAllegationExist(event.option.value)) {
            this.selectedAllegations.push(event.option.value);
            this.loadAllegationSubtypes();
        }
        this.allegationInput.nativeElement.value = '';
        this.allegationCtrl.setValue(null);
    }

    isAllegationExist(searchAllegation: any) {
        return this.selectedAllegations.find(allegation => allegation.allegationid === searchAllegation.allegationid) ? true : false;
    }

    isIndicatorExist(searchIndicator: any) {
        const Index = this.indicators.findIndex(indicator => indicator.indicatorid === searchIndicator.indicatorid);
        return Index !== -1 ? true : false;
    }

    editAllegation(intakeservreqtypeid: string, allegation: any) {
        this.allegation = allegation;
        this.choosenAllegation = new ChoosenAllegation();
        this.choosenAllegation.allegationID = allegation.allegationID;
        this.choosenAllegation.allegationValue = allegation.allegationValue;
        this.choosenAllegation.indicators = allegation.indicators;
        // console.log(allegation);
        this.indicators = allegation.indicators;
        const checkInput = {
            where: { intakeservreqtypeid: intakeservreqtypeid },
            method: 'get',
            order: 'name'
        };
        this.selectedAllegation$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsUrl + '/?filter'
            )
            .map(result => {
                this.allegations = result;
                return result;
            });
        this.selectedAllegation$ = this.selectedAllegation$.map(allegations => {
            return allegations.filter(algn => algn.allegationid === allegation.allegationID);
        });

    }

    resetSelectedAllegations() {
        this.selectedAllegations = [];
    }

    resetCaseCreationForm() {
        this.resetSelectedAllegations();
        this.caseCreationFormGroup.patchValue({ subServiceType: null });
    }

    private _filter(value: any): any[] {

        if (value && value !== '') {
            const filterValue = value.toLowerCase();
            return this.allegations.filter(allegation => allegation.name.toLowerCase().indexOf(filterValue) !== -1);
        } else {
            return this.allegations;
        }
    }



    private listenAllegationList() {
        this.offenceCategoriesInputSubject$.subscribe(offenceCategories => {
            if (offenceCategories) {
                this.offenceCategories = offenceCategories;
            }
            this.loadSelectedAllegationList();
        });

    }


    private listenSelectedAllegationList() {
        this.evalFieldsInputSubject$.subscribe(evalFields => {
            this.evaluationFields = evalFields;
            this.loadSelectedAllegationList();
        });

        this.evalFieldsOutputSubject$.subscribe(evalFields => {
            this.evaluationFields = evalFields;
            this.loadSelectedAllegationList();
        });
    }

    private loadSelectedAllegationList() {
        if (this.evaluationFields && this.evaluationFields.allegedoffense && this.offenceCategories.length > 0) {

            let allegationIds = [];
            if (this.evaluationFields.allegedoffense) {
                allegationIds = this.evaluationFields.allegedoffense.map(offence => offence.allegationid);
            }
            const selectedAllegations = this.offenceCategories.filter(allegation => {
                const found = allegationIds.indexOf(allegation.allegationid);
                if (found === - 1) {
                    return false;
                } else {
                    return true;
                }

            });

            this.selectedAllegationList$ = Observable.of(selectedAllegations);

        }

    }

    private findAllegationById(allegationID) {
        if (this.evaluationFields.allegedoffense) {
            return this.offenceCategories.find(allegation => allegation.allegationid === allegationID);
        }

    }

    onAllegationWindow(windowOpened) {
        if (!windowOpened) {
            this.loadAllegationSubtypes();
        }
    }
}

