import value from '*.json';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { DispositionCode, DispostionOutput } from '../_entities/newintakeModel';
import { ComplaintTypeCase, DelayForm, DelayResponse, General } from '../_entities/newintakeSaveModel';
import { NewUrlConfig } from './../../provider-referral-url.config';
import { DispositionConfig } from './_configurations/reason';
import * as reason from './_configurations/reason.json';

declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-disposition',
    templateUrl: './intake-disposition.component.html',
    styleUrls: ['./intake-disposition.component.scss']
})
export class IntakeDispositionComponent implements OnInit, AfterViewInit, AfterViewChecked {
    @Input()
    dispositionInput$ = new Subject<string>();
    @Input()
    dispositionOutPut$ = new Subject<DispostionOutput[]>();
    @Input()
    dispositionRetrive$ = new Subject<DispostionOutput[]>();
    @Input()
    timeReceived$ = new Subject<string>();
    @Input()
    purposeToDispOutput$ = new Subject<string>();
    @Input()
    isDateDelayed$ = new Subject<DelayResponse>();
    @Input()
    delayformValue$ = new Subject<DelayForm>();
    @Input()
    general$ = new Subject<General>();
    general: General;
    @Input()
    agencyStatus$ = new Subject<string>();
    @Input()
    inputDispositionFmSdm$ = new Subject<string>();
    @Input()
    createdCaseInputSubject$ = new Subject<ComplaintTypeCase[]>();
    createdCase: DispostionOutput[];
    statusDropdownItems$: Observable<DropdownModel[]>;
    dispositionList: DispositionCode[];
    dispositionDropDown: DropdownModel[];
    supDispositionDropDown: DropdownModel[];
    disposition: string;
    dispositionDropdownItems$: Observable<DropdownModel[]>;
    dispositionFormGroup: FormGroup;
    dispositionDelayForm: FormGroup;
    date = new Date();
    serviceTypeId: string;
    showReason = false;
    supervisorUser: boolean;
    timeReceived: string;
    showStatus = true;
    daTypeSubType: string;
    generalRecievedDate: DelayResponse;
    role: AppUser;
    agencyStatus: string;
    intakePurpose: any;
    showBehalfOfRequester = false;
    actionDropdown: DropdownModel[];
    initRecomentdationDropdown: DropdownModel[];
    constructor(private _commonHttpService: CommonHttpService, private formBuilder: FormBuilder, private _authService: AuthService, private _changeDetect: ChangeDetectorRef) {}

    ngOnInit() {
        this.role = this._authService.getCurrentUser();
        this.actionDropdown = DispositionConfig.config.action;
        this.initRecomentdationDropdown = DispositionConfig.config.initialRecomendation;
        this.agencyStatus$.subscribe((res) => {
            this.agencyStatus = res;
        });
        this.dispositionForm();
        this.dispositionInput$.subscribe((res) => {
            this.serviceTypeId = res;
        });
        this.createdCaseInputSubject$.subscribe((response) => {
            this.dispositionFormGroup.patchValue({
                intakeserreqstatustypekey: ''
            });
            // to get the label for datype
            const checkInput = {
                nolimit: true,
                where: { teamtypekey: 'all' },
                method: 'get'
            };
            this._commonHttpService.getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter').subscribe((result) => {
                this.intakePurpose = result;
            });
            // end of - to get the label for datype
            if (response) {
                this.createdCase = [];
                response.map((item) => {
                    const createdCase = {
                        ServiceRequestNumber: item.caseID,
                        DaTypeKey: item.serviceTypeID,
                        subSeriviceTypeValue: item.subSeriviceTypeValue,
                        DasubtypeKey: item.subServiceTypeID,
                        DAStatus: '',
                        DADisposition: '',
                        Summary: '',
                        dispositioncode: item.dispositioncode ? item.dispositioncode : '',
                        intakeserreqstatustypekey: item.intakeserreqstatustypekey ? item.intakeserreqstatustypekey : '',
                        comments: '',
                        ReasonforDelay: '',
                        supStatus: item.supStatus ? item.supStatus : '',
                        supDisposition: item.supDisposition ? item.supDisposition : '',
                        supComments: '',
                        intakeMultipleDispositionDropdown: [],
                        supMultipleDispositionDropdown: [],
                        GroupNumber: item.GroupNumber ? item.GroupNumber : null,
                        GroupReasonType: item.GroupReasonType ? item.GroupReasonType : null,
                        GroupComment: item.GroupComment ? item.GroupComment : null
                    };
                    this.createdCase.push(createdCase);
                    // this.createdCase.dispositioncode: '',
                    // this.createdCase.intakeserreqstatustypekey: '',
                    if (this.createdCase.length > 0) {
                        if (this.createdCase[0].intakeserreqstatustypekey) {
                            this.onChangeTaskStatus(this.createdCase[0].intakeserreqstatustypekey);
                        }
                        if (this.createdCase[0].supStatus) {
                            this.supOnChangeTaskStatus(this.createdCase[0].supStatus);
                            // this.changeDisp();
                        }
                        this.dispositionFormGroup.patchValue({
                            intakeserreqstatustypekey: this.createdCase[0].intakeserreqstatustypekey ? this.createdCase[0].intakeserreqstatustypekey : '',
                            dispositioncode: this.createdCase[0].dispositioncode ? this.createdCase[0].dispositioncode : '',
                            supStatus: this.createdCase[0].supStatus ? this.createdCase[0].supStatus : '',
                            supDisposition: this.createdCase[0].supDisposition ? this.createdCase[0].supDisposition : ''
                        });
                        this.changeDisp();
                    }
                });
            }
        });

        if (this.role.role.name === 'apcs') {
            this.supervisorUser = true;
            this.dispositionFormGroup.get('intake');
        } else {
            this.supervisorUser = false;
        }
        this.dispositionOutPut$.subscribe((data) => {
            const roleId = this._authService.getCurrentUser();
            if (roleId.role.name === 'apcs') {
                this.supervisorUser = true;
                this.showStatus = false;
            } else {
                this.supervisorUser = false;
            }
            if (data && data.length > 0) {
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            nolimit: true,
                            where: {
                                intakeservreqtypeid: data[0].DAStatus,
                                servicerequestsubtypeid: this.daTypeSubType
                            },
                            method: 'get'
                        }),
                        'Intakeservicerequestdispositioncodes/getstatuslist?filter'
                    )
                    .subscribe((result) => {
                        this.dispositionList = result;
                        if (data && data.length > 0) {
                            if (data[0].intakeserreqstatustypekey) {
                                // this.onChangeTaskStatus(data[0].intakeserreqstatustypekey);
                                this.createdCase = data;
                                // this.onChangeTaskStatus(data[0].intakeserreqstatustypekey);
                                this.dispositionFormGroup.patchValue({
                                    intakeserreqstatustypekey: data[0].intakeserreqstatustypekey,
                                    dispositioncode: data[0].dispositioncode,
                                    comments: data[0].comments,
                                    intakeAction: data[0].intakeAction
                                });
                                this.onChangeTaskStatus(this.dispositionFormGroup.value.intakeserreqstatustypekey);
                            }
                            if (data[0].supStatus) {
                                // this.supOnChangeTaskStatus(data[0].supStatus);
                                if (this.role.role.name === 'apcs') {
                                    this.dispositionFormGroup.patchValue({
                                        reason: data[0].reason,
                                        supStatus: data[0].supStatus,
                                        supDisposition: data[0].supDisposition,
                                        supComments: data[0].supComments
                                    });
                                } else {
                                    this.dispositionFormGroup.patchValue({
                                        reason: data[0].reason,
                                        supStatus: data[0].supStatus !== 'Reopen' ? data[0].supStatus : '',
                                        supDisposition: data[0].supDisposition,
                                        supComments: data[0].supComments
                                    });
                                }
                                this.supOnChangeTaskStatus(this.dispositionFormGroup.value.supStatus);
                            }
                        }
                    });
            }
        });
        this.timeReceived$.subscribe((time) => {
            this.timeReceived = time;
            this.enableReasonfordelay();
        });
        this.purposeToDispOutput$.subscribe((resp) => {
            this.daTypeSubType = resp;
            this.statusDropdownItems$ = Observable.empty();
            if (resp === '619c4dcf-ef22-4fc4-9269-d7678e8a8f6a') {
                this.showBehalfOfRequester = true;
                const model = new DropdownModel();
                model.text = 'Closed';
                model.value = 'Closed';
                this.statusDropdownItems$ = Observable.of([model]);
                this.dispositionFormGroup.patchValue({
                    intakeserreqstatustypekey: 'Closed'
                });
                this.onChangeTaskStatus('Closed');
                // this.dispositionFormGroup.get('intakeserreqstatustypekey').disable();
            } else {
                this.showBehalfOfRequester = false;
                this.dispositionFormGroup.get('intakeserreqstatustypekey').enable();
                //this.loadDropdown();
            }
        });
    }

    ngAfterViewInit() {
        this.isDateDelayed$.subscribe((item) => {
            this.generalRecievedDate = item;
            (<any>$('#reason-for-delay')).modal('show');
            // console.log('calculated time', item);
        });
        this.general$.subscribe((item) => {
            this.general = item;
        });
    }
    ngAfterViewChecked() {
        this._changeDetect.detectChanges();
    }
    dispositionForm() {
        this.dispositionFormGroup = this.formBuilder.group({
            intakeserreqstatustypekey: ['', Validators.required],
            dispositioncode: ['', Validators.required],
            statusdate: [this.date],
            duedate: [this.date],
            completiondate: [this.date],
            dateseen: null,
            financial: [''],
            seenwithin: [this.date],
            edl: [''],
            jointinvestigation: [''],
            investigationsummary: [''],
            visitinfo: [''],
            supStatus: [''],
            supDisposition: [''],
            supComments: [''],
            comments: [''],
            reason: [''],
            isDelayed: [false],
            intakeAction: ['']
        });
        this.dispositionDelayForm = this.formBuilder.group({
            fiveDaysDelay: [''],
            twentyFivedaysDelay: ['']
        });
    }

    loadDropdown() {
        this.statusDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        intakeservreqtypeid: this.daTypeSubType,
                        servicerequestsubtypeid: this.daTypeSubType
                    },
                    method: 'get'
                }),
                'Intakeservicerequestdispositioncodes/getstatuslist?filter'
            )
            .map((result) => {
                this.dispositionList = result;
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.intakeserreqstatustypekey
                        })
                );
            });
    }

    onChangeTaskStatus(statusId: any) {
        if (this.createdCase && this.createdCase.length > 0) {
            this.createdCase.map((item) => {
                item.DAStatus = statusId;
                item.intakeMultipleDispositionDropdown = [];
                if (item.issubtypekey) {
                    item.DasubtypeKey = null;
                }
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            nolimit: true,
                            where: {
                                intakeservreqtypeid: item.DaTypeKey,
                                servicerequestsubtypeid: item.DasubtypeKey ? item.DasubtypeKey : item.DaTypeKey,
                                statuskey: statusId
                            },
                            method: 'get'
                        }),
                        'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                    )
                    .subscribe((result) => {
                        item.intakeMultipleDispositionDropdown = result.map(
                            (res) =>
                                new DropdownModel({
                                    text: res.description,
                                    value: res.dispositioncode
                                })
                        );
                    });
            });
        }
    }
    supOnChangeTaskStatus(id) {
        this.changeDisp();
        this.createdCase.map((item) => {
            item.DAStatus = id;
            item.supMultipleDispositionDropdown = [];
            this._commonHttpService
                .getArrayList(
                    new PaginationRequest({
                        nolimit: true,
                        where: {
                            intakeservreqtypeid: item.DaTypeKey,
                            servicerequestsubtypeid: item.DasubtypeKey,
                            statuskey: id
                        },
                        method: 'get'
                    }),
                    'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                )
                .subscribe((result) => {
                    item.supMultipleDispositionDropdown = result.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.dispositioncode
                            })
                    );
                });
        });
    }
    onChangeDispoType(daSubtype, index) {
        this.createdCase[index].DADisposition = daSubtype;
        this.createdCase[index].dispositioncode = daSubtype;
        this.changeDisp();
    }
    onChangeSupDispoType(daSubtype, index) {
        this.createdCase[index].DADisposition = daSubtype;
        this.createdCase[index].supDisposition = daSubtype;
        this.changeDisp();
    }

    changeDisp() {
        if (this.createdCase && this.createdCase.length > 0) {
            this.createdCase.map((item) => {
                item.intakeserreqstatustypekey = this.dispositionFormGroup.value.intakeserreqstatustypekey ? this.dispositionFormGroup.value.intakeserreqstatustypekey : '';
                item.comments = this.dispositionFormGroup.value.comments ? this.dispositionFormGroup.value.comments : '';
                item.intakeAction = this.dispositionFormGroup.value.intakeAction ? this.dispositionFormGroup.value.intakeAction : '';
                item.supStatus = this.dispositionFormGroup.value.supStatus ? this.dispositionFormGroup.value.supStatus : '';
                item.supComments = this.dispositionFormGroup.value.supComments ? this.dispositionFormGroup.value.supComments : '';
                item.reason = this.dispositionFormGroup.value.reason ? this.dispositionFormGroup.value.reason : '';
            });
            this.dispositionRetrive$.next(this.createdCase);
        }
    }
    enableReasonfordelay() {
        if (this.timeReceived === 'Overdue') {
            this.dispositionFormGroup.patchValue({ isDelayed: true });
            this.showReason = true;
            this.dispositionFormGroup.get('reason').setValidators([Validators.required]);
            this.dispositionFormGroup.get('reason').updateValueAndValidity();
        } else {
            this.dispositionFormGroup.get('reason').clearValidators();
            this.dispositionFormGroup.get('reason').updateValueAndValidity();
            this.dispositionFormGroup.patchValue({ isDelayed: false });
        }
    }
    submitDelayForm() {
        if (this.dispositionDelayForm.dirty) {
            const delayReason = {
                fiveDays: this.dispositionDelayForm.get('fiveDaysDelay').value,
                twentyFiveDays: this.dispositionDelayForm.get('twentyFivedaysDelay').value
            };
            this.delayformValue$.next(delayReason);
        }
        (<any>$('#reason-for-delay')).modal('hide');
    }
}
