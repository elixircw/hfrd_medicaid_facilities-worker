import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Observable, Subject } from 'rxjs/Rx';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { EvaluationFields } from '../_entities/newintakeSaveModel';
import { AuthService, AlertService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material';
import { InvolvedPerson, EvaluationSourceType, EvaluationSourceObject, EvaluationSourceAgency, IntakePurpose, CrossReferenceSearchResponse, CrossReferenceResponse } from '../_entities/newintakeModel';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-evaluation-fields',
    templateUrl: './intake-evaluation-fields.component.html',
    styleUrls: ['./intake-evaluation-fields.component.scss']
})
export class IntakeEvaluationFieldsComponent implements OnInit {
    @ViewChild('trigger') trigger: MatAutocompleteTrigger;
    @Input() evalFieldsInputSubject$ = new Subject<EvaluationFields>();
    @Input() evalFieldsOutputSubject$ = new Subject<EvaluationFields>();
    @Input() complaintCrossReferenceSubject$ = new Subject<CrossReferenceResponse>();
    @Input() purposeInputSubject$ = new Subject<IntakePurpose>();
    // tslint:disable-next-line:no-input-rename
    @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
    addedPersons: InvolvedPerson[] = [];
    // tslint:disable-next-line:no-input-rename
    @Input() reviewStatus$ = new Subject<string>();
    intakeEvalForm: FormGroup;
    incidentDateOption: number;
    mdCountys$: Observable<DropdownModel[]>;
    offenceCategories$: Observable<any[]>;
    @Input() offenceCategoriesInputSubject$ = new Subject<any>();
    evaluationSourceTypeList$: Observable<EvaluationSourceType[]>;
    evaluationAgencyTypeList$: Observable<EvaluationSourceAgency[]>;
    evaluationSourceSearchList$: Observable<EvaluationSourceObject[]>;
    sourceData: EvaluationSourceObject;
    oldDate = {
        beginDate: '',
        endDate: ''
    };
    maxDate = new Date();
    roleName: AppUser;
    constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _authService: AuthService, private _alertService: AlertService) { }

    ngOnInit() {
        this.incidentDateOption = 0;
        this.intakeEvalForm = this.formBuilder.group({
            isdetention: false,
            petitionid: [''],
            adultpetitionid: [''],
            evaluationsourceagencyid: [''],
            evaluationsourceagencykey: [''],
            evaluationsourcetypeid: [''],
            evaluationsourcetypekey: [''],
            complaintid: [''],
            wvrnumber: [''],
            offenselocation: [''],
            complaintreceiveddate: [''],
            arrestdate: [''],
            zipcode: [null],
            countyid: [null],
            allegedoffense: [[]],
            allegedoffenseknown: [0],
            allegedoffensedate: [''],
            begindate: [''],
            enddate: [''],
            unknownrange: false,
            dateRange: false,
            // isdrai: false,
            // ismcasp: false,
            yearsofage: [{ value: '', disabled: true }],
            sourcesearch: '',
            sourceTitle: '',
            sourceBadgeNo: '',
            sourcelastname: '',
            sourcefirstname: '',
            sourceStreetno: '',
            sourceStreet1: '',
            sourceStreet2: '',
            sourceKey: '',
            sourceCount: '',
            evaluationsourceid: null, // temp id need to change once api deployed with id
        });
        this.evalFieldsOutputSubject$.subscribe((data) => {
            this.patchForm(data);

        });

        // this.loadDropdowns();
        this.offenceCategoriesInputSubject$.subscribe(offenceCategories => {
            if (offenceCategories) {
                this.offenceCategories$ = Observable.of(offenceCategories);
            }

        });
        this.intakeEvalForm.valueChanges.subscribe((val) => {
            this.evalFieldsInputSubject$.next(this.intakeEvalForm.getRawValue());
        });
        this.roleName = this._authService.getCurrentUser();
        /*  this.reviewStatus$.subscribe((res) => {
              if (this.roleName.role.name === 'apcs' && (res === 'Review' || res === 'Reopen')) {
                  this.intakeEvalForm.disable();
              }
          });*/

        this.addedPersonsChanges$.subscribe((data) => {
            this.addedPersons = data;
            this.setYouthAge();
        });

    }

    private loadDefault() {
        const intakeEvalForm = this.intakeEvalForm.getRawValue();
        if (intakeEvalForm.sourceTitle) {
            const source = {
                title: intakeEvalForm.sourceTitle,
                badgeno: intakeEvalForm.sourceBadgeNo,
                lastname: intakeEvalForm.sourcelastname,
                firstname: intakeEvalForm.sourcefirstname,
                streetno: intakeEvalForm.sourceStreetno,
                street1: intakeEvalForm.sourceStreet1,
                street2: intakeEvalForm.sourceStreet2,
                totalcount: intakeEvalForm.totalcount,
                evaluationsourcekey: intakeEvalForm.evaluationsourcekey
            };
            this.sourceData = source;
        }
        if (intakeEvalForm.evaluationsourcetypekey) {
            this.loadSourceAgencyTypeDropdown(intakeEvalForm.evaluationsourcetypekey);
        }
    }
    private loadDropdowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({ where: { state: 'MD' }, order: 'countyname asc', nolimit: true, method: 'get' }, NewUrlConfig.EndPoint.Intake.MDCountryListUrl + '?filter'),
            // this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.OffenceCategoryListUrl + '?filter={"nolimit":true,"order":"description"}'),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.EvaluationSourceTypeList + '?filter={"nolimit":true,"order":"description"}')
            // this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.IntakeAgencies)
        ])
            .map(([sourseListResult, evaluationSourceTypeList]) => {
                return {
                    sourceList: sourseListResult.map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyid
                            })
                    ),
                    evaluationSourceTypeList
                };
            })
            .share();
        this.mdCountys$ = source.pluck('sourceList');
        this.evaluationSourceTypeList$ = source.pluck('evaluationSourceTypeList');

    }

    private setSourceAgencyTypeDropdown(evaluationsourceagencykey: string) {
        this.intakeEvalForm.patchValue({
            evaluationsourceagencykey: evaluationsourceagencykey,
        });
    }

    private loadSourceAgencyTypeDropdown(evaluationsourcetypekey: string) {
        this.intakeEvalForm.patchValue({
            evaluationsourcetypekey: evaluationsourcetypekey,
        });
        const sourceAgencyType = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    'where': { 'evaluationsourcetypekey': evaluationsourcetypekey },
                    'nolimit': true,
                    'order': 'description'
                }),
                NewUrlConfig.EndPoint.Intake.EvaluationAgencyTypeList + '?filter='
            )
            .map((evaluationAgencyTypeList) => {
                return {
                    evaluationAgencyTypeList
                };
            })
            .share();

        this.evaluationAgencyTypeList$ = sourceAgencyType.pluck('evaluationAgencyTypeList');
    }

    loadSourceDropDown($event) {
        const intakeEvalForm = this.intakeEvalForm.getRawValue();
        if (intakeEvalForm.sourcesearch) {
            setTimeout(_ => this.trigger.openPanel());
            const source = this._commonHttpService
                .getArrayList({
                    where: {
                        searchkey: intakeEvalForm.sourcesearch,
                        evaluationsourceagencykey: intakeEvalForm.evaluationsourceagencykey
                    },
                    nolimit: true, method: 'get'
                },
                    NewUrlConfig.EndPoint.Intake.EvaluationSourceSearchList + '?filter')
                .map((evaluationSourceSearchList) => {
                    return {
                        evaluationSourceSearchList
                    };
                })
                .share();
            this.evaluationSourceSearchList$ = source.pluck('evaluationSourceSearchList');
        }
    }

    private sourceSelected(source: EvaluationSourceObject) {
        const intakeEvalForm = this.intakeEvalForm.getRawValue();
        this.sourceData = source;
        this.intakeEvalForm.patchValue({
            sourceTitle: source.title,
            sourceBadgeNo: source.badgeno,
            sourcelastname: source.lastname,
            sourcefirstname: source.firstname,
            sourceStreetno: source.streetno,
            sourceStreet1: source.street1,
            sourceStreet2: source.street2,
            sourceKey: source.evaluationsourcekey, // source.evaluationsourcekey,
            sourceCount: source.totalcount,
            evaluationsourceid: source.evaluationsourceid, // temp id need to change once api deployed with id
            sourcesearch: ''
        });
    }

    onIncidentDateOptionChange(event: boolean, option: string) {
        if (option === 'range' && event) {
            this.incidentDateOption = 2;
            this.intakeEvalForm.patchValue({
                unknownrange: false,
                allegedoffenseknown: 2,
                begindate: this.oldDate.beginDate,
                enddate: this.oldDate.endDate
            });
        } else if (option === 'range' && !event) {
            if (this.intakeEvalForm.value.unknownrange) {
                this.incidentDateOption = 1;
                this.intakeEvalForm.patchValue({
                    allegedoffenseknown: 1,
                    begindate: '',
                    enddate: ''
                });
            } else {
                this.incidentDateOption = 0;
                this.intakeEvalForm.patchValue({
                    allegedoffenseknown: 0,
                    allegedoffensedate: this.oldDate.beginDate
                });
            }
        } else if (option === 'unknown' && event) {
            this.incidentDateOption = 1;
            this.intakeEvalForm.patchValue({
                dateRange: false,
                allegedoffenseknown: 1,
                begindate: '',
                enddate: ''
            });
        } else if (option === 'unknown' && !event) {
            this.incidentDateOption = 0;
            this.intakeEvalForm.patchValue({
                allegedoffenseknown: 0,
                allegedoffensedate: this.oldDate.beginDate
            });
        }
        this.setYouthAge();
    }

    patchForm(data: EvaluationFields) {
        if (data) {
            this.intakeEvalForm.patchValue(data);
            this.loadDefault();
            let offenceList = [];
            if (data.allegedoffense) {
                offenceList = data.allegedoffense.map(offence => offence.allegationid);
            }
            this.intakeEvalForm.patchValue({
                complaintrcddate: data.complaintreceiveddate ? new Date(data.complaintreceiveddate) : '',
                arrestdate: data.arrestdate ? new Date(data.arrestdate) : '',
                allegedoffensedate: data.allegedoffensedate ? new Date(data.allegedoffensedate) : '',
                beginDate: data.begindate ? new Date(data.begindate) : '',
                endDate: data.enddate ? new Date(data.enddate) : '',
                allegedoffense: offenceList
            });
            this.incidentDateOption = data.allegedoffenseknown;
            this.oldDate.beginDate = data.begindate;
            this.oldDate.endDate = data.enddate;
            if (data.allegedoffenseknown === 0) {
                this.intakeEvalForm.patchValue({
                    unknownrange: false,
                    dateRange: false
                });
            } else if (data.allegedoffenseknown === 1) {
                this.intakeEvalForm.patchValue({
                    dateRange: false,
                    unknownrange: true
                });
            } else if (data.allegedoffenseknown === 2) {
                this.intakeEvalForm.patchValue({
                    dateRange: true,
                    unknownrange: false
                });
            }
        }
    }


    private getYouthAge() {
        const intakeEvalForm = this.intakeEvalForm.getRawValue();
        if (this.addedPersons && this.addedPersons.length > 0) {
            const youth = this.addedPersons.find((p) => p.Role === 'Youth');
            if (!youth || (youth && !youth.Dob)) {
                return null;
            }
            let youthDob;
            let offenceDate;
            youthDob = new Date(youth.Dob);
            if (!intakeEvalForm.unknownrange && (intakeEvalForm.allegedoffensedate || intakeEvalForm.beginDate)) {
                offenceDate = intakeEvalForm.dateRange ? intakeEvalForm.begindate : intakeEvalForm.allegedoffensedate;
                offenceDate = new Date(offenceDate);
            } else {
                offenceDate = new Date();
            }
            const timeDiff = offenceDate - youthDob;
            const youthAge = new Date(timeDiff); // miliseconds from epoch
            return Math.abs(youthAge.getUTCFullYear() - 1970);
        } else {
            return null;
        }
    }

    private setYouthAge() {
        const youthAge = this.getYouthAge();
        this.intakeEvalForm.patchValue({
            yearsofage: youthAge,
        });
    }

    isComplaintIdExists() {
        const intakeEvalForm = this.intakeEvalForm.getRawValue();
        if (intakeEvalForm.countyid && intakeEvalForm.countyid !== 'all') {
            this._commonHttpService.getSingle(
                {
                    'method': 'get',
                    'where': {
                        'countyid': intakeEvalForm.countyid,
                        'complaintid': intakeEvalForm.complaintid
                    }
                }, NewUrlConfig.EndPoint.Intake.validateComplaintId + '?filter')
                .subscribe((result) => {
                    if (result.isexists) {
                        if (result.crossrefs.length > 0 || result.intakecrossrefs.length > 0) {
                            const crossReference = result.crossrefs[0];
                            const intakeCrossReference = result.intakecrossrefs[0];
                            if ((crossReference && crossReference.srstatus === 'Rejected') || (intakeCrossReference && intakeCrossReference.status === 'rejected')) {
                                this.complaintCrossReferenceSubject$.next(result);
                            } else {
                                (<any>$('#active-intake-popup')).modal('show');
                                this.clearComplaintID();
                                this.complaintCrossReferenceSubject$.next(new CrossReferenceResponse());
                            }

                        } else {
                                (<any>$('#active-intake-popup')).modal('show');
                                this.clearComplaintID();
                                this.complaintCrossReferenceSubject$.next(new CrossReferenceResponse());
                        }

                    } else {
                        this.complaintCrossReferenceSubject$.next(new CrossReferenceResponse());
                    }
                });
        } else {
            this._alertService.error('Choose County to validate complaint id');
        }

    }

    clearComplaintID() {
        this.intakeEvalForm.patchValue({ complaintid: '' });
    }
}
