import { AfterContentInit, AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Rx';

import { ControlUtils } from '../../../@core/common/control-utils';
import { ObjectUtils } from '../../../@core/common/initializer';
import { AppUser } from '../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../@core/entities/common.entities';
import { DataStoreService, GenericService } from '../../../@core/services';
import { AlertService } from '../../../@core/services/alert.service';
import { AuthService } from '../../../@core/services/auth.service';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { RoutingUser } from '../../cjams-dashboard/_entities/dashBoard-datamodel';
import { NewUrlConfig, ReferralUrlConfig } from '../provider-referral-url.config';

import {
    AssessmentScores,
    ContactTypeAdd,
    CpsDocInput,
    CrossReference,
    CrossReferenceSearchResponse,
    DispostionOutput,
    EntitesSave,
    FinalIntake,
    GeneralNarative,
    GetAssessmentScore,
    IntakeDATypeDetail,
    IntakePurpose,
    IntakeTemporarySaveModel,
    InvolvedEntitySearchResponse,
    InvolvedPerson,
    Narrative,
    NarrativeIntake,
    Notes,
    PreIntakeDisposition,
    ReviewStatus,
    Sdm,
    SubType,
    Assessment
} from './_entities/newintakeModel';


import {
    Agency,
    AttachmentIntakes,
    ComplaintTypeCase,
    //CourtDetails,
    DATypeDetail,
    DelayForm,
    DelayResponse,
    EvaluationFields,
    General,
    GeneratedDocuments,
    IntakeAppointment,
    IntakeScreen,
    IntakeService,
    Person,
    //PetitionDetails
} from './_entities/newintakeSaveModel';


import { IntakeAssessmentComponent } from './intake-assessment/intake-assessment.component';
import { IntakeAttachmentsComponent } from './intake-attachments/intake-attachments.component';
import { NewReferralConstants } from './new-referral.constants';
import { ProviderReferral, ReferralDecision, ReferralNarrative, Profile } from './_entities/newreferralModel';

declare var require: any;
declare var html2pdf: any;

declare var $: any;

const PENDING_FOR_SAO_DOCS = 1;
const WAITING_FOR_SAO_UPDATES = 2;
const WAITNG_FOR_COURT_UPDATE = 3;
const WAITNG_FOR_COURT_HEARING = 4;
const WAITNG_FOR_CASE_WORKER = 5;
const SAO_CLOSED = 6;
const SUSTAINED = 'S';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'my-newintake',
    templateUrl: './new-referral.component.html',
    styleUrls: ['./new-referral.component.scss']
})


export class NewReferralComponent implements OnInit, AfterViewInit, AfterContentInit {

    // Our Stuff
    draftReferralintake: any;
    dropdownselectval: any;
    providerReferralIntake = new ProviderReferral();

    referralDecision$ = new Subject<ReferralDecision>();
    referralDecisionModel: ReferralDecision;
    referralInformation$: Observable<ProviderReferral[]>;

    referralNarrative$ = new Subject<string>();
    referralNarrativeModel: ReferralNarrative;

    currentAgency: String;
    intakeAgencies: any;
    //legacy
    //This is the main form group at the base level that captures all te info for provider referral
    departmentActionIntakeFormGroup: FormGroup;
    generalResourceFormGroup: FormGroup;


    //intakeSourceList$: Observable<DropdownModel[]>;
    intakeCommunication$: Observable<DropdownModel[]>;
    intakeCommunication: any;
    intakeAgencies$: Observable<DropdownModel[]>;
    intakePurpose$: Observable<DropdownModel[]>;

    intakeServices: IntakeService[];

    intakeId: string;
    narrativeInputSubject$ = new Subject<Narrative>();
    sdmInputSubject$ = new Subject<Sdm>();
    sdmReport$ = new Subject<Sdm>();
    sdmOutputSubject$ = new Subject<Sdm>();
    sdmDispositionCall: string;
    finalNarrativeText$ = new Subject<string>();
    narrativeOutputSubject$ = new Subject<Narrative>();
    profileOutputSubject$ = new Subject<Profile>();
    addAttachementSubject$ = new Subject<AttachmentIntakes[]>();
    agencyStatus$ = new Subject<string>();
    agencyType$ = new Subject<string>();
    purposeStatus$ = new Subject<string>();
    narrativeDetails: Narrative = new Narrative();
    profileDetails: Profile = new Profile();

    addPerson: string;
    draftId: string;
    btnDraft: boolean;

    intakeNumber: string;
    intakeNumberNarrative: string;

    general: General = new General();
    reviewstatus: ReviewStatus = new ReviewStatus();

    narrative: NarrativeIntake = new NarrativeIntake();
    addAttachement: AttachmentIntakes[] = [];

    intakeScreen: IntakeScreen = new IntakeScreen();
    addedCrossReference: CrossReferenceSearchResponse[] = [];

    addedPersons: InvolvedPerson[] = [];
    @Input()
    addedPersons$ = new Subject<InvolvedPerson[]>();
    @Input()
    personsList$ = new Subject<InvolvedPerson[]>();

    // addedPersonsInput$ = new Subject<InvolvedPerson[]>();
    // addedPersonsOutput$ = new Subject<InvolvedPerson[]>();

    addedEntities: InvolvedEntitySearchResponse[] = [];

    addedIntakeDATypeDetails: IntakeDATypeDetail[] = [];
    addedIntakeDATypeSubject$ = new Subject<IntakeDATypeDetail[]>();
    evalFieldsInputSubject$ = new Subject<EvaluationFields>();
    evalFieldsOutputSubject$ = new Subject<EvaluationFields>();

    // communicationInputSubject$ = new Subject<Notes[]>();
    // communicationOutputSubject$ = new Subject<Notes[]>();

    complaintCrossReferenceSubject$ = new Subject<CrossReferenceSearchResponse>();

    appointmentInputSubject$ = new Subject<IntakeAppointment[]>();
    appointmentOutputSubject$ = new Subject<IntakeAppointment[]>();

    dispositionInput$ = new Subject<string>();
    dispositionOutPut$ = new Subject<DispostionOutput[]>();
    dispositionRetrive$ = new Subject<DispostionOutput[]>();

    preIntakedisposition$ = new Subject<PreIntakeDisposition>();
    preIntakedispositionPost$ = new Subject<PreIntakeDisposition>();

    agencyCodeSubject$ = new Subject<IntakeDATypeDetail>();

    purposeCheckboxOutput$ = new Subject<IntakeService>();

    serviceCheckboxOutput$ = new Subject<string>();
    serviceCheckboxInput$ = new Subject<Assessment>();


    // purposeCheckboxInput$ = new Subject<GetAssessmentScore[]>();

    //preIntakeSupDicision$ = new Subject<string>();
    //preIntakeSupDicision = '';

    saoTabIndex$ = new Subject<number>();
    offenceCategoriesInputSubject$ = new Subject<any[]>();
    // petitionDetailsInputSubject$ = new Subject<PetitionDetails>();
    // petitionDetailsOutputSubject$ = new Subject<PetitionDetails>();

    // courtDetailsInputSubject$ = new Subject<CourtDetails>();
    // courtDetailsOutputSubject$ = new Subject<CourtDetails>();

    generatedDocuments$ = new Subject<string[]>();
    assessmentInput: IntakeDATypeDetail;
    timeReceived$ = new Subject<string>();
    general$ = new Subject<General>();
    evalFields: EvaluationFields;
    communicationFields: Notes[] = [];
    //petitionDetails: PetitionDetails;
    isPetionDetailsSubmited = false;

    //courtDetails: CourtDetails;

    intakeAppointment: IntakeAppointment[];
    recordings: ContactTypeAdd[] = [];
    Person: Person[] = [];
    addNarrative: Narrative;
    addSdm: Sdm;
    selectteamtypekey: string;
    itnakeServiceGrid: boolean;
    intakeInfoNreffGrid: boolean;
    intakeservice = [];
    intakeInfoReffTypes = [];
    //otherAgencyControlName: AbstractControl;
    djsSelected = false;
    isDjs = false;
    role: string;
    isCWSelected = false;
    checkValidation: boolean;
    saveIntakeBtn: boolean;
    intakeType: string;
    disposition: DispostionOutput[];
    purposeList: IntakePurpose[] = [];
    createdCases: ComplaintTypeCase[];
    createdCaseInputSubject$ = new Subject<ComplaintTypeCase[]>();
    purposeInputSubject$ = new Subject<IntakePurpose>();
    createdCaseOuptputSubject$ = new Subject<ComplaintTypeCase[]>();
    selectedPurpose: DropdownModel;
    selectedAgency: DropdownModel;
    roleValue = false;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    resourcePoplabel: string;
    generalResource: GeneralNarative[];
    roleId: AppUser;
    submitResourceObject: GeneralNarative;
    downloadInProgress = false;
    reviewStatus: string;
    cpsdocData: CpsDocInput = new CpsDocInput();
    cpsIntakeGenAction$: Subject<string> = new Subject<string>();
    reviewStatus$ = new Subject<string>();
    showViewAssessment: boolean;
    safeHavenAssessmentScore: number;
    kinshipAssessmentScore: number;
    viewKinship: boolean;
    viewSafeHaven: boolean;
    purposeCheckboxView$ = new Subject<IntakeService>();
    isKinshipSafehaven: string;
    serviceCheckboxId: any;
    purposeToDispOutput$ = new Subject<string>();
    isDateDelayed$ = new Subject<DelayResponse>();
    delayFormData: DelayForm;
    checkConditionForDelay = false;
    entityDetails: EntitesSave[];
    entitiesSubject$ = new Subject<EntitesSave[]>();
    purposeSubject$ = new Subject<string>();
    zipCode: string;
    supervisorsList: RoutingUser[];
    intakersList: RoutingUser[];
    selectedSupervisor: string;
    selectedIntaker: string;
    isManualRouting: string;
    finalIntake = new FinalIntake();
    accessStatus: boolean;
    selectedPerson: any;
    getUsersList: RoutingUser[];
    originalUserList: RoutingUser[];
    subServiceTypes: SubType[];
    timeLeft: string;
    @Input()
    delayformValue$ = new Subject<DelayForm>();
    @Input()
    entityDetailsSaveSubject$ = new Subject<EntitesSave[]>();
    @Input()
    zipCodeSubject$ = new Subject<string>();
    @ViewChild(IntakeAttachmentsComponent)
    intakeAttachment: IntakeAttachmentsComponent;
    @ViewChild(IntakeAssessmentComponent)
    daAllegaDispo: IntakeAssessmentComponent;
    viewAscrs: boolean;
    ascrsScore: number;
    isCW = false;
    isPreIntake$ = new Subject<boolean>();
    isPreIntake = false;
    isCLW = false;
    clwStatus: any;
    intakerWorkerId = '';
    isPurposeWithAgency = true;
    IWtoAssign: string;
    preIntakeDisposition: PreIntakeDisposition;
    generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
    generateDocumentInput$ = new Subject<GeneratedDocuments[]>();
    genratedDocumentList: GeneratedDocuments[];
    intakedetailList: any;
    scoresSubject$: Subject<AssessmentScores> = new Subject<AssessmentScores>();
    // for HTML BINDING
    PENDING_FOR_SAO_DOCS = PENDING_FOR_SAO_DOCS;
    WAITING_FOR_SAO_UPDATES = WAITING_FOR_SAO_UPDATES;
    WAITNG_FOR_COURT_UPDATE = WAITNG_FOR_COURT_UPDATE;
    WAITNG_FOR_COURT_HEARING = WAITNG_FOR_COURT_HEARING;
    SAO_CLOSED = SAO_CLOSED;
    WAITNG_FOR_CASE_WORKER = WAITNG_FOR_CASE_WORKER;

    constructor(
        private _router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private speechRecognizer: SpeechRecognizerService,
        private _genericServiceNarative: GenericService<GeneralNarative>,
        private _dataStoreService: DataStoreService
    ) {
        this.draftId = route.snapshot.params['id'];
        this._dataStoreService.setData('REFERRAL_NUMBER', this.draftId);
        if (this.draftId !== null && this.draftId !== '' && this.draftId !== undefined) {
            this._dataStoreService.setData('isReferral', 'true');
        } else {
            this._dataStoreService.setData('isReferral', 'false');
        }
    }

    subscribeToDecision() {
        this.referralDecision$.subscribe((decision) => {

            if (decision.status !== undefined) {
                this.dropdownselectval = true;
            }
            else {
                this.dropdownselectval = false;
            }
            this.referralDecisionModel = decision;
            this.providerReferralIntake.referral_decision = decision.status;
            this.providerReferralIntake.comment = decision.reason;
        });
    }

    subscribeToNarrative() {
        this.referralNarrative$.subscribe((qwe) => {
            this.providerReferralIntake.narrative = qwe;

        });
    }

    setReferralStatus() {
        const decision = this.providerReferralIntake.referral_decision;
        if (decision == 'Approved') {
            this.providerReferralIntake.referral_status = 'Approved';
            this.providerReferralIntake.referral_decision = 'Approved';
        } else if (decision == 'Rejected') {
            this.providerReferralIntake.referral_status = 'Rejected';
            this.providerReferralIntake.referral_decision = 'Rejected';
        } else if (decision == 'Incomplete') {
            this.providerReferralIntake.referral_status = 'Incomplete';
            this.providerReferralIntake.referral_decision = 'Pending';
        } else if (decision == 'Closed') {
            this.providerReferralIntake.referral_status = 'Closed';
            this.providerReferralIntake.referral_decision = 'Rejected';
        } else if (decision == 'Submitted') {
            this.providerReferralIntake.referral_status = 'Submitted';
            this.providerReferralIntake.referral_decision = 'Pending';
        }
    }



    submitReferral() {
        console.log(this.selectedPerson);
        this.setReferralStatus();
        let toUserid = '';
        let toRoleid = '';
        if (this.selectedPerson) {
            toUserid = this.selectedPerson.userid;
            toRoleid = this.selectedPerson.userrole;
        }
        const approvedReferral = {
            "provider_referral_id": this.intakeNumber,
            "referral_status": this.providerReferralIntake.referral_status,
            "referral_decision": this.providerReferralIntake.referral_decision,
            "comment": this.providerReferralIntake.comment,
            "fromroleid": this.roleId.role.name,
            "tosecurityusersid": toUserid,
            "toroleid": toRoleid
        }

        this._commonHttpService.update('', approvedReferral, 'providerreferral/approveproviderreferral').subscribe(
            (response) => {
                this._alertService.success('Referral Submitted!');
               // this._dataStoreService.setData('isReferral', 'false');
                // (<any>$('#provider-search')).click();
                this.closePopup();
            },
            (error) => {
                this._alertService.error('Unable to approve, please try again.');
                console.log('Referral approval error', error);
                return false;
            }
        );
    }

    closePopup() {
        (<any>$('#intake-caseassign')).modal('hide');
    }
    ngOnInit() {

        this.buildFormGroup();
        this.subscribeToDecision();
        this.subscribeToNarrative();

        this.loadDroddowns();
        //this.loadDropDownItems();
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.user.userprofile.teamtypekey === 'DJS' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
            || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
            || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA') {
            this.role = "DJS";
        }
        else {
            this.role = "OLM";
        }

        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.user.userprofile.teamtypekey === 'DJS') {
            this.isDjs = true;
        }


        // We might need to persist the agency info, so that when a person comes back to an existing referral
        // they know which agency person captured it. Could get this information a part of the get request
        // based on the security if of the person who captured the referral.
        this.currentAgency = this.roleId.user.userprofile.teamtypekey;

        if (this.roleId && this.roleId.role && this.roleId.role.name === 'Court Liaison Worker') {
            this.isCLW = true;
        }

        console.log("SIMAR ngOnInit");
        console.log(this.draftId);

        if (!this.draftId) {
            this.loadDefaults();
        }

        this.narrativeInputSubject$.subscribe((narrative) => {
            this.addNarrative = narrative;
            if (this.addNarrative.offenselocation !== '') {
                this.zipCode = narrative.offenselocation;
            }
        });

        this.finalNarrativeText$.subscribe((text) => {
            this.addNarrative.Narrative = text;
        });

        this.addAttachementSubject$.subscribe((attach) => {
            this.addAttachement = attach;
        });

        this.addedIntakeDATypeSubject$.subscribe((intakeDAType) => {
            this.addedIntakeDATypeDetails = intakeDAType;
        });

        this.dispositionRetrive$.subscribe((disposot) => {
            if (disposot && disposot[0].dispositioncode) {
                this.disposition = disposot;
            }
        });

        this.preIntakedisposition$.subscribe((data) => {
            this.preIntakeDisposition = data;
        });

        this.entityDetailsSaveSubject$.subscribe((entity) => {
            this.entityDetails = entity;
        });

        // this.addedPersonsInput$.subscribe((persons) => {
        //     this.addedPersonsOutput$.next(persons);
        // });

        this.zipCodeSubject$.subscribe((data) => {
            // this.general.incidentlocation = data;
            if (data !== '') {
                this.zipCode = data;
            }
        });

        //this.otherAgencyControlName = this.departmentActionIntakeFormGroup.get('otheragency');
        //this.otherAgencyControlName.disable();

        // this.communicationInputSubject$.subscribe((commFileds) => {
        //     this.communicationFields = commFileds;
        // });

        this.evalFieldsInputSubject$.subscribe((evalFileds) => {
            this.evalFields = evalFileds;
            if (this.evalFields && this.evalFields.offenselocation) {
                this.zipCode = '' + this.evalFields.offenselocation;
            }
            this.evalFields = evalFileds;
            const allegations = this.evalFields.allegedoffense;
            if (allegations) {
                this.evalFields.allegedoffense = allegations.map((allegation) => {
                    return { allegationid: allegation };
                });
            }
            this.evalFields.yearsofage = this.evalFields.yearsofage ? this.evalFields.yearsofage : null;
            this.evalFields.arrestdate = this.evalFields.arrestdate ? this.evalFields.arrestdate : null;
            this.evalFields.complaintreceiveddate = this.evalFields.complaintreceiveddate ? this.evalFields.complaintreceiveddate : null;
            this.evalFields.begindate = this.evalFields.begindate ? this.evalFields.begindate : null;
            this.evalFields.enddate = this.evalFields.enddate ? this.evalFields.enddate : null;
            this.evalFields.zipcode = this.evalFields.zipcode ? this.evalFields.zipcode : null;
        });

        this.appointmentInputSubject$.subscribe((data) => {
            this.intakeAppointment = data;
            if (data && data.length > 0) {
                this.IWtoAssign = data[0].intakeWorkerId;
            }
        });

        this.createdCaseInputSubject$.subscribe((createdCases) => {
            this.createdCases = createdCases;
        });



        this.serviceCheckboxInput$.subscribe((result) => {
            // if (result) {
            //     // this.showViewAssessment = true;
            // }
        });

        this.delayformValue$.subscribe((resp) => {
            this.delayFormData = resp;
            if (this.delayFormData) {
                this.checkConditionForDelay = false;
            } else {
                this.checkConditionForDelay = true;
            }
        });

        this.personsList$.subscribe((item) => {
            this.addedPersons$.next(item);
        });

        this.sdmInputSubject$.subscribe((sdm) => {
            sdm = Object.assign(sdm, sdm.physicalAbuse);
            sdm = Object.assign(sdm, sdm.sexualAbuse);
            sdm = Object.assign(sdm, sdm.generalNeglect);
            sdm = Object.assign(sdm, sdm.unattendedChild);
            sdm = Object.assign(sdm, sdm.riskofHarm);
            sdm = Object.assign(sdm, sdm.screenOut);
            sdm = Object.assign(sdm, sdm.screenIn);
            sdm = Object.assign(sdm, sdm.immediateList);
            sdm = Object.assign(sdm, sdm.noImmediateList);
            sdm = Object.assign(sdm, sdm.disqualifyingCriteria);
            sdm = Object.assign(sdm, sdm.disqualifyingFactors);

            if (sdm.cpsResponseType) {
                if (sdm.cpsResponseType === 'CPS-IR') {
                    sdm.isir = true;
                    sdm.isar = false;
                } else {
                    this.validateIRAR(sdm);
                }
            } else {
                this.validateIRAR(sdm);
            }

            if (sdm.maltreatment === 'yes') {
                sdm.ismaltreatment = true;
            } else {
                sdm.ismaltreatment = false;
            }
            // sdm.ismaltreatment = sdm.maltreatment === 'no' ? false : true;

            //     "recovr_scrrenin": true,
            // "reccps_screenout": false,

            if (sdm.screeningRecommend === 'ScreenOUT') {
                sdm.recsc_screenout = true;
                sdm.recsc_scrrenin = false;
            } else {
                sdm.recsc_screenout = false;
                sdm.recsc_scrrenin = true;
            }

            this.addSdm = sdm;
            if (this.roleId.role.name !== 'apcs') {
                if (sdm.screeningRecommend && this.createdCases) {
                    this.createdCases[0].dispositioncode = sdm.screeningRecommend;
                    this.createdCases[0].intakeserreqstatustypekey = 'Review';
                    if (sdm.screeningRecommend !== this.sdmDispositionCall) {
                        this.sdmDispositionCall = sdm.screeningRecommend;
                        this.createdCaseInputSubject$.next(this.createdCases);
                    }
                }
            } else {
                if (sdm.scnRecommendOveride && this.createdCases) {
                    this.createdCases[0].intakeserreqstatustypekey = 'Review';
                    this.createdCases[0].dispositioncode = sdm.screeningRecommend;
                    this.createdCases[0].supDisposition = sdm.scnRecommendOveride;
                    this.createdCases[0].supStatus = 'Approved';
                    if (sdm.scnRecommendOveride !== this.sdmDispositionCall) {
                        this.sdmDispositionCall = sdm.scnRecommendOveride;
                        this.createdCaseInputSubject$.next(this.createdCases);
                    }
                }
            }
        });

        // this.contactList$.subscribe((res) => (this.recordings = res));

        this.generateDocumentOutput$.subscribe((generatedDocuments) => {
            this.genratedDocumentList = generatedDocuments;
        });
    }



    ngAfterViewInit() {
        console.log("SIMAR ngAfterViewInit");
        console.log(this.draftId);
        if (this.draftId) {
            this.populateIntake();
        }
    }

    ngAfterContentInit() {
        $('.btnNext').click(function () {
            $('.click-triggers > .active')
                .next('li')
                .find('a')
                .trigger('click');
            // console.log('next');
        });

        $('.btnPrevious').click(function () {
            $('.click-triggers > .active')
                .prev('li')
                .find('a')
                .trigger('click');
            // console.log('prev');
        });

        $('#intake-cps-doc').on('shown.bs.modal', () => {
            // do something...
            this.genCpsIntakeDoc('generate');
        });
    }

    buildFormGroup() {
        this.departmentActionIntakeFormGroup = this.formBuilder.group(
            {
                Source: [''],
                InputSource: ['', Validators.required],
                RecivedDate: [new Date(), [Validators.required, Validators.minLength(1)]],
                CreatedDate: [new Date()],
                Author: [''],
                IntakeNumber: [''],
                Agency: ['', Validators.required],
                Purpose: ['', Validators.required],
                IntakeService: [''],
                //otheragency: ['', Validators.maxLength(50)],
                //isOtherAgency: false
            },
            { validator: this.dateFormat }
        );
        this.generalResourceFormGroup = this.formBuilder.group({
            helpDescription: ['']
        });
    }

    dateFormat(group: FormGroup) {
        if (group.controls.RecivedDate.value !== '' && group.controls.RecivedDate.value !== null) {
            if (group.controls.RecivedDate.value > new Date()) {
                return { futureDate: true };
            }
            return null;
        }
    }

    private loadDropDownItems() {

    }

    private loadDroddowns() {
        const source = forkJoin([
            //this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.CommunicationUrl),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.IntakeServiceRequestInputTypeUrl),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.IntakeAgencies)
        ])
            .map((result) => {
                return {
                    // sourceList: result[0].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.description,
                    //             value: res.intakeservreqinputsourceid
                    //         })
                    // ),
                    communicationList: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeservreqinputtypeid
                            })
                    ),
                    agenciesList: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.teamtypekey,
                                value: res.teamtypekey,
                                additionalProperty: res.ismanualrouting
                            })
                    )
                };
            })
            .share();

        //this.intakeSourceList$ = source.pluck('sourceList');
        this.intakeCommunication$ = source.pluck('communicationList');
        this.intakeCommunication$.subscribe((data) => {
            this.intakeCommunication = data;
        });

        this.intakeAgencies$ = source.pluck('agenciesList');
        this.intakeAgencies$.subscribe((data) => {
            this.intakeAgencies = data;
            this.loadAgancyAsPerUser(data);

        });
    }



    // USE THIS TO MAP THE AGENCY AS PER OLM/DJS WORKER
    loadAgancyAsPerUser(data) {
        // console.log('user role:', this.roleId.user.userprofile.teamtypekey);

        data.map((element) => {
            if (element.value === this.roleId.user.userprofile.teamtypekey) {
                //this.departmentActionIntakeFormGroup.get('Agency').disable();
                this.departmentActionIntakeFormGroup.patchValue({
                    // Agency: element.value

                });
                this.listPurpose({ text: '', value: element.value + '~' + element.additionalProperty });
            }
        });
    }


    private loadDefaults() {

        this._commonHttpService.getArrayList({}, ReferralUrlConfig.EndPoint.Referral.getNextReferralNumberUrl).subscribe((result) => {

            console.log("&&&&&&&&&&&&" + JSON.stringify(result));
            this.departmentActionIntakeFormGroup.patchValue({
                IntakeNumber: result['nextNumber']
            });
            this.intakeNumberNarrative = result['nextNumber'];

            this.intakeNumber = result['nextNumber'];

            if (!this.draftId) {
                this.intakeAttachment.getIntakeNumber(result['nextNumber']);
            }

            this._dataStoreService.setData(NewReferralConstants.Intake.intakenumber, result['nextNumber']);
        });

        this._authService.currentUser.subscribe((userInfo) => {
            //console.log("@@@@@@@@@@@@"+JSON.stringify(userInfo));
            if (userInfo && userInfo.user) {
                this.departmentActionIntakeFormGroup.patchValue({
                    Author: userInfo.user.userprofile.displayname ? userInfo.user.userprofile.displayname : '',
                    //Agency: userInfo.user.agencyid ?  userInfo.user.agencyid : ''

                });
            }
        });


        const resourceURL = 'Helptexts/list?filter';
        this._commonHttpService
            .getArrayList(
                {
                    where: { formkey: 'intake.narrative.general' },
                    method: 'get'
                },
                resourceURL
            )
            .subscribe((response) => {
                this.generalResource = response;
            });
    }



    listPurpose(agency: DropdownModel) {
        if (agency.value && agency.value !== 'all') {
            this.selectedAgency = Object.assign({}, new DropdownModel());
            const items = agency.value.split('~');
            this.selectedAgency.value = items[0];
            this.isManualRouting = items[1];
        } else {
            this.selectedAgency = agency;
        }

        if (this.selectedAgency.value === 'CW') {
            this.isCW = true;
        } else {
            this.isCW = false;
        }

        this.agencyStatus$.next(this.selectedAgency.value);
        this.agencyType$.next(this.selectedAgency.value);
        const teamtypekey = this.selectedAgency.value;
        this.departmentActionIntakeFormGroup.patchValue({
            Purpose: ''
        });


        this.itnakeServiceGrid = false;
        this.intakeInfoNreffGrid = false;


        const checkInput = {
            nolimit: true,
            where: { teamtypekey: teamtypekey },
            method: 'get',
            order: 'description'
        };

        this.intakePurpose$ = this._commonHttpService.getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter').map((result) => {
            this.purposeList = result;
            if (this.selectteamtypekey) {
                this.departmentActionIntakeFormGroup.controls['Purpose'].setValue(this.selectteamtypekey);
                const items = this.selectteamtypekey.split('~');
                const serDescription = items[0];
                this.checkForDJSSelected(serDescription);

                if (items.length === 1) {
                    this.isPurposeWithAgency = false;
                }
            }
            if (agency.text === 'myIntake' && this.selectedAgency.value === 'DJS') {
                this.djsSelected = true;
            }
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.description,
                        value: res.intakeservreqtypeid,
                        additionalProperty: res.teamtype.teamtypekey
                    })
            );
        });
    }



    selectService(event: any, selectedItem: any) {
        this.serviceCheckboxId = '';
        if (event.target.checked) {
            this.intakeservice.push(selectedItem);
            const role = this._authService.getCurrentUser();
            if (role.role.name !== 'apcs') {
                // d302182e-d33e-4086-a7c7-d9c8aad1d5b9 = Kinship Navigator
                // "dbf82713-b43c-4ef7-8772-284c6788e5bc" = Save Haven Babies
                // if (
                //     selectedItem.intakeservid === 'd302182e-d33e-4086-a7c7-d9c8aad1d5b9' ||
                //     selectedItem.intakeservid === 'dbf82713-b43c-4ef7-8772-284c6788e5bc' ||
                //     selectedItem.intakeservid === '8ea95fc8-78a7-4ec3-9ed5-d2b5646ccfef'
                // ) {
                if (selectedItem.assessmenttemplateid) {
                    this.serviceCheckboxId = selectedItem.intakeservid;
                    (<any>$('#assessment-tab')).click();
                    this.purposeCheckboxOutput$.next(selectedItem);
                }
            }
        } else {
            this.intakeservice = this.intakeservice.map((item) => {
                if (item.intakeservid !== selectedItem.intakeservid) {
                    return item;
                } else {
                    return {};
                }
            });
        }
    }

    viewAssessment(modal) {
        (<any>$('#assessment-tab')).click();
        this.purposeCheckboxView$.next(modal);
    }

    isSelectedItems(modal) {
        const index = this.intakeservice.filter((item) => item.intakeservid === modal.intakeservid);
        if (index.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    // savenAssignPreIntake() {
    //     // console.log(this.preIntakeSupDicision);
    //     if (this.preIntakeSupDicision === 'Approved') {
    //         // this.mainIntake(this.departmentActionIntakeFormGroup.value, 'SITR', true);
    //         const Istatus = 10; // Approved
    //         this.draftIntake(this.departmentActionIntakeFormGroup.value, 'DRAFT');
    //         setTimeout(() => this.assignIntaker('SITR', this.IWtoAssign, Istatus), 3000);
    //     } else if (this.preIntakeSupDicision === 'Rejected') {
    //         const Istatus = 3; // Rejected
    //         this.draftIntake(this.departmentActionIntakeFormGroup.value, 'DRAFT');
    //         setTimeout(() => this.assignIntaker('SITR', this.intakerWorkerId, Istatus), 3000);
    //     } else {
    //         this._alertService.warn('Please select a status.');
    //     }
    // }

    submitIntake(modal: General, appevent: string) {
        this.checkValidation = this.conditionalValidation();
        if (this.checkValidation) {
            // Added rejected status (to visible in intake worker dashboard)
            if (this.disposition[0].supStatus === 'Approved' || this.disposition[0].supStatus === 'Closed' || this.disposition[0].supStatus === 'Rejected') {
                this.approveIntake(modal, appevent);
            } else {
                this.mainIntake(modal, appevent, true);
            }
        }
    }

    submitReviewIntake() {
        if (this.selectedSupervisor) {
            this.finalIntake.review.assignsecurityuserid = this.selectedSupervisor;
            this.finalIntake.review.ismanualrouting = true;
            if (this.disposition[0].intakeserreqstatustypekey === 'Closed') {
                this.approveIntake(this.departmentActionIntakeFormGroup.value, 'INTR');
            } else {
                this.createIntake(this.finalIntake);
            }
            (<any>$('#list-supervisor')).modal('hide');
        } else {
            this._alertService.error('Please select Supervisor');
        }
    }

    submitIntakewithIntakers(Intaker: string) {
        if (Intaker === '1') {
            if (this.selectedIntaker) {
                this.finalIntake.review.assignIntakeuserid = this.selectedIntaker;
                this.finalIntake.review.ismanualrouting = true;
                this.createIntake(this.finalIntake);
                (<any>$('#list-intaker')).modal('hide');
            } else {
                this._alertService.error('Please select Intertaker');
            }
        } else {
            this.createIntake(this.finalIntake);
            (<any>$('#list-intaker')).modal('hide');
        }
    }

    submitPreIntake(modal: General, appevent: string) {
        this.mainIntake(modal, appevent, false);
    }



    draftIntake(modal: General, appevent: string) {
        this.mainIntake(modal, appevent, false);
    }




    mainIntake(modal: General, appevent: string, isSubmitReview: boolean) {
        if (this.roleId.role.name === 'apcs') {
            modal = this.departmentActionIntakeFormGroup.getRawValue();
        }

        this.intakeType = appevent;

        if (this.departmentActionIntakeFormGroup.valid) {
            if (appevent === 'DRAFT' || appevent === 'CLWDRAFT') {
                this.checkValidation = true;
            } else {
                this.checkValidation = this.conditionalValidation();
            }
            if (this.checkValidation) {
                this.general = Object.assign(new General(), modal);
                this.general.offenselocation = this.zipCode;
                this.general.intakeservice = this.intakeservice;
                const recDate = new Date(modal.RecivedDate);
                this.general.RecivedDate = recDate.toLocaleString();
                ObjectUtils.removeEmptyProperties(this.general);

                if (this.departmentActionIntakeFormGroup.getRawValue().Agency.split('~')[0] === 'CW') {
                    if (this.addSdm) {
                        if (this.disposition[0]) {
                            this.disposition[0].issubtypekey = true;
                        }
                        this.general.Iscps = this.addSdm.iscps ? this.addSdm.iscps : null;
                        if (this.subServiceTypes) {
                            this.subServiceTypes.map((item) => {
                                if (this.addSdm.cpsResponseType === item.classkey) {
                                    this.disposition[0].DasubtypeKey = item.servicerequestsubtypeid;
                                }
                            });
                        }
                    } else {
                        this.general.Iscps = null;
                    }
                }
                const intake = this.mapIntakeScreenInfo(this.general);
                if (this.evalFields) {
                    switch (this.evalFields.allegedoffenseknown) {
                        case 0: {
                            this.evalFields.enddate = null;
                            break;
                        }
                        case 1: {
                            this.evalFields.enddate = null;
                            this.evalFields.begindate = null;
                            break;
                        }
                        case 2: {
                            break;
                        }
                    }
                }

                const role = this._authService.getCurrentUser();

                const reviewStatus = {
                    appevent: appevent,
                    status: appevent === 'INTR' ? 'supreview' : '',
                    commenttext: appevent === 'INTR' ? '' : '',
                    ispreintake: this.isPreIntake
                };

                if ((appevent === 'SITR' || appevent === 'DRAFT') && role.role.name === 'SCRNW') {
                    reviewStatus.appevent = appevent;
                    reviewStatus.status = 'supreview';
                    reviewStatus.ispreintake = true;
                }

                if (this.disposition && this.disposition.length > 0) {
                    if (role.role.name === 'apcs') {
                        reviewStatus.commenttext = this.disposition[0].supComments ? this.disposition[0].supComments : '';
                        reviewStatus.status = this.disposition[0].supStatus;
                    } else {
                        reviewStatus.commenttext = this.disposition[0].comments ? this.disposition[0].comments : '';
                    }
                }

                if (intake) {
                    if (this.genratedDocumentList) {
                        this.genratedDocumentList.forEach((document) => {
                            document.isSelected = false;
                        });
                    }
                    // const clwStatus = (appevent === 'CLWDRAFT') ? this.clwStatus : null;

                    const intakeSaveModel = new IntakeTemporarySaveModel({
                        crossReference: this.addedCrossReference,
                        persons: this.addedPersons,
                        entities: this.addedEntities,
                        agency: this.entityDetails,
                        intakeDATypeDetails: this.addedIntakeDATypeDetails,
                        recordings: this.recordings,
                        General: this.general,
                        narrative: this.narrative,
                        clwStatus: this.clwStatus,
                        disposition: this.disposition,
                        attachement: this.addAttachement,
                        evaluationFields: this.isDjs ? this.evalFields : null,
                        appointments: this.intakeAppointment,
                        reviewstatus: reviewStatus,
                        createdCases: this.createdCases,
                        sdm: this.addSdm,
                        //petitionDetails: this.petitionDetails,
                        //courtDetails: this.courtDetails,
                        communicationFields: this.communicationFields,
                        preIntakeDispo: this.preIntakeDisposition,
                        generatedDocuments: this.genratedDocumentList
                    });

                    if (this.disposition && this.disposition.length > 0) {
                        if (!this.isDjs) {
                            // added for retaining initial recommendation during update draft.
                            this.disposition.map((item) => {
                                item.intakeMultipleDispositionDropdown = [];
                                item.supMultipleDispositionDropdown = [];
                            });
                        }
                        intakeSaveModel.DAType = {
                            DATypeDetail: this.disposition
                        };
                    }
                    intakeSaveModel.preIntakeDispo = this.preIntakeDisposition;
                    // intakeSaveModel.clwStatus = this.clwStatus ? this.clwStatus : null;
                    intakeSaveModel.narrative = Object.assign(intake.NarrativeIntake);
                    intakeSaveModel.General.safeHavenAssessmentScore = this.safeHavenAssessmentScore;
                    intakeSaveModel.General.kinshipAssessmentScore = this.kinshipAssessmentScore;
                    if (this.delayFormData && this.delayFormData.fiveDays) {
                        intakeSaveModel.General.receiveddelay = this.delayFormData.fiveDays;
                    }
                    if (this.delayFormData && this.delayFormData.fiveDays && this.delayFormData.twentyFiveDays) {
                        intakeSaveModel.General.receiveddelay = this.delayFormData.fiveDays;
                        intakeSaveModel.General.submissiondelay = this.delayFormData.twentyFiveDays;
                    }
                    intakeSaveModel.General.ascrsScore = this.ascrsScore;
                    // console.log(JSON.stringify(intakeSaveModel));
                    const finalIntake = {
                        intake: intakeSaveModel,
                        review: reviewStatus
                    };

                    if (this.isManualRouting === 'true' && isSubmitReview && role.role.name === 'Intake Worker') {
                        this.finalIntake = Object.assign({}, finalIntake);
                        this.loadSupervisor();
                        (<any>$('#list-supervisor')).modal('show');
                    } else {
                        // if (this.roleId.role.name === 'SCRNW' && this.intakeType === 'SITR') {
                        //     this.finalIntake = Object.assign({}, finalIntake);
                        //     this.loadIntaker();
                        //     this.selectedIntaker = '';
                        //     (<any>$('#list-intaker')).modal('show');
                        // } else {
                        // if (this.clwStatus === WAITNG_FOR_COURT_HEARING || this.clwStatus === SAO_CLOSED) {
                        //     this.CompleteSAO(finalIntake);
                        // } else {
                        //     this.createIntake(finalIntake);
                        // }
                        // }
                    }
                }
            }
        } else {
            ControlUtils.validateAllFormFields(this.departmentActionIntakeFormGroup);
            this._alertService.warn('Please input the mandatory fields.');
        }
    }


    setReferralInfo(referral) {
        console.log("SIMAR RECEIVED")
        console.log(referral);
        this.draftReferralintake = referral;
        this.providerReferralIntake = referral[0];

        console.log("VALUES FOR DRAFT SAVE FIRST TRY")
        console.log(this.providerReferralIntake);
        this.setProfileDetails(this.providerReferralIntake);
    }

    setProfileDetails(profileDetails) {
        this.profileDetails.fullname = profileDetails.provider_referral_nm;
        this.profileDetails.firstName = profileDetails.provider_referral_first_nm;
        this.profileDetails.phoneNumber = profileDetails.adr_cell_phone_tx;
        this.profileDetails.cityName = profileDetails.adr_city_nm;
        this.profileDetails.state = profileDetails.adr_state_cd;
        this.profileDetails.streetName = profileDetails.adr_street_nm;
        this.profileDetails.zipCode = profileDetails.adr_zip5_no;
        this.profileDetails.lastName = profileDetails.provider_referral_last_nm;
        this.profileDetails.country = profileDetails.adr_country_tx;
        this.profileDetails.countyCd = profileDetails.adr_county_cd;
        this.profileDetails.streetNo = profileDetails.adr_street_no;
        this.profileDetails.taxid = profileDetails.corporation_entity_taxid;
        this.profileDetails.faxNumber = profileDetails.adr_street_nm;
        this.profileDetails.workNumber = profileDetails.adr_cell_phone_tx;
        this.profileDetails.email = profileDetails.adr_email_tx;

        this.profileDetails.program = profileDetails.provider_referral_program;
        this.profileDetails.programType = profileDetails.provider_program_type;
        this.profileDetails.programName = profileDetails.provider_program_name;

        this.profileDetails.gender = "Male";
        this.profileDetails.minAge = 2;
        this.profileDetails.maxAge = 8;
        this.profileDetails.bedCapacity = 10;
        this.profileOutputSubject$.next(this.profileDetails);
    }

    updateReferralDraft() {

        const selectedCommunication = this._dataStoreService.getData('SelectedCommunication');
        if (!selectedCommunication) {
            this._alertService.error('Please select the communication');
            return false;
        }
        // Pass the referral id in the payload to update

        console.log("VALUES FOR DRAFT SAVE RETURN TRY")
        console.log(this.providerReferralIntake);

        // SIMAR: TODO Capture the high level details
        //console.log(this.departmentActionIntakeFormGroup.getRawValue().InputSource);
        this.providerReferralIntake.provider_referral_med = this.departmentActionIntakeFormGroup.getRawValue().InputSource;

        const draftReferralIntake = {
            "provider_referral_id": this.intakeNumber, // Comes from the new-referral component

            "provider_referral_nm": this.providerReferralIntake.provider_referral_nm,
            "provider_referral_last_nm": this.providerReferralIntake.provider_referral_last_nm,
            "provider_referral_first_nm": this.providerReferralIntake.provider_referral_first_nm,

            "provider_referral_program": this.providerReferralIntake.provider_referral_program,
            "provider_program_type": this.providerReferralIntake.provider_program_type,
            "provider_program_name": this.providerReferralIntake.provider_program_name,

            "adr_email_tx": this.providerReferralIntake.adr_email_tx,
            "adr_cell_phone_tx": this.providerReferralIntake.adr_cell_phone_tx,
            "adr_street_nm": this.providerReferralIntake.adr_street_nm,
            "adr_street_no": this.providerReferralIntake.adr_street_no,
            "adr_city_nm": this.providerReferralIntake.adr_city_nm,
            "adr_state_cd": this.providerReferralIntake.adr_state_cd,
            "adr_county_cd": this.providerReferralIntake.adr_county_cd,
            "adr_country_tx": this.providerReferralIntake.adr_country_tx,
            "adr_zip5_no": this.providerReferralIntake.adr_zip5_no,
            "is_son": this.providerReferralIntake.is_son,
            "is_taxid": this.providerReferralIntake.is_taxid,
            "is_rfp": this.providerReferralIntake.is_rfp,
            "parent_entity": this.providerReferralIntake.parent_entity,
            "parent_entity_taxid": this.providerReferralIntake.parent_entity_taxid,
            "corporation_entity": this.providerReferralIntake.corporation_entity,
            "corporation_entity_taxid": this.providerReferralIntake.corporation_entity_taxid,

            // NARRATIVE
            "narrative": this.providerReferralIntake.narrative,


            // SIMAR TODO Check this logic
            "provider_referral_med": this.providerReferralIntake.provider_referral_med,
            "referral_status": "Draft",
            "referral_decision": "Pending",
            //"agency": this.providerReferralIntake.Agency,
            //"agency": this.departmentActionIntakeFormGroup.value.Agency
            "agency": this.role
        }


        this._commonHttpService.update('', draftReferralIntake, 'providerreferral/updateproviderreferral').subscribe(
            (response) => {
                this._alertService.success('Referral saved successfully!');
            },
            (error) => {
                this._alertService.error('Unable to save referral, please try again.');
                console.log('Save Referral Error', error);
                return false;
            }
        );
    }


    approveReferral() {
        const approvedReferral = {
            "provider_referral_id": this.intakeNumber
        }
        this._commonHttpService.update('', approvedReferral, 'providerreferral/approveproviderreferral').subscribe(
            (response) => {
                this._alertService.success('Referral Approved!');
            },
            (error) => {
                this._alertService.error('Unable to approve, please try again.');
                console.log('Referral approval error', error);
                return false;
            }
        );
    }


    createIntake(finalIntake) {
        this._commonHttpService.create(finalIntake, NewUrlConfig.EndPoint.Intake.SendtoSupervisorreviewUrl).subscribe(
            (response) => {
                if (response.data.isreceiveddelay || response.data.issubmitdelay) {
                    // this.checkConditionForDelay = true;
                    (<any>$('#disposition-tab')).click();
                    this.isDateDelayed$.next(response.data);
                } else {
                    if (finalIntake.review.appevent === 'CLWDRAFT') {
                        this.onReload();
                        this.btnDraft = true;
                        this._alertService.success('Sao Details saved successfully!');
                    } else if (finalIntake.review.appevent !== 'DRAFT') {
                        this.onReload();
                        this.btnDraft = false;
                    } else {
                        if (!this.isDjs) {
                            this.onReload();
                        }
                        this.btnDraft = true;
                        this._alertService.success('Intake saved successfully!');
                    }
                }
            },
            (error) => {
                this._alertService.error('Unable to save intake, please try again.');
                console.log('Save Intake Error', error);
                return false;
            }
        );
    }


    approveIntake(modal: General, appevent: string) {
        modal = this.departmentActionIntakeFormGroup.getRawValue();
        this.intakeType = appevent;
        if (this.departmentActionIntakeFormGroup.valid) {
            if (appevent !== 'DRAFT') {
                this.checkValidation = this.conditionalValidation();
            } else {
                this.checkValidation = this.conditionalValidation();
            }
            if (this.checkValidation) {
                this.general = Object.assign(new General(), modal);
                this.general.offenselocation = this.zipCode;
                this.general.intakeservice = this.intakeservice;
                const recDate = new Date(modal.RecivedDate);
                this.general.RecivedDate = recDate.toLocaleString();
                this.general.Purpose = this.general.Purpose ? this.general.Purpose.split('~')[0] : '';
                ObjectUtils.removeEmptyProperties(this.general);
                const intake = this.mapIntakeScreenInfo(this.general);
                const reviewStatus = {
                    appevent: appevent,
                    status: this.disposition[0].supStatus,
                    commenttext: ''
                };
                if (this.disposition && this.disposition.length > 0) {
                    const role = this._authService.getCurrentUser();
                    if (role.role.name === 'apcs') {
                        reviewStatus.commenttext = this.disposition[0].supComments ? this.disposition[0].supComments : '';
                    } else {
                        reviewStatus.commenttext = this.disposition[0].comments ? this.disposition[0].comments : '';
                    }
                }
                this.addedPersons.map((item) => {
                    item.Pid = item.Pid ? item.Pid : '';
                    if (item.emailID) {
                        item.contactsmail = item.emailID;
                    } else {
                        item.contactsmail = [];
                    }
                    if (item.phoneNumber) {
                        item.contacts = item.phoneNumber;
                    } else {
                        item.contacts = [];
                    }
                    if (item.personAddressInput) {
                        item.address = item.personAddressInput;
                    } else {
                        item.address = [];
                    }
                });
                if (!this.isDjs && this.disposition && this.disposition.length > 0) {
                    this.disposition.map((item) => {
                        item.intakeMultipleDispositionDropdown = [];
                        item.supMultipleDispositionDropdown = [];
                    });
                }
                if (this.departmentActionIntakeFormGroup.getRawValue().Agency.split('~')[0] === 'CW') {
                    if (this.disposition[0].dispositioncode === this.disposition[0].supDisposition) {
                        this.general.isDisposition = false;
                    } else {
                        this.general.isDisposition = true;
                    }
                    if (this.addSdm) {
                        this.disposition[0].issubtypekey = true;
                        this.general.Iscps = this.addSdm.iscps ? this.addSdm.iscps : null;
                        if (this.subServiceTypes) {
                            this.subServiceTypes.map((item) => {
                                if (this.addSdm.cpsResponseType === item.classkey) {
                                    this.disposition[0].DasubtypeKey = item.servicerequestsubtypeid;
                                }
                            });
                        }
                    } else {
                        this.general.Iscps = null;
                    }
                }
                if (intake) {
                    const intakeSaveModel = {
                        CrossReferences: this.addedCrossReference ? this.addedCrossReference.map((item) => new CrossReference(item)) : [],
                        persondetails: { Person: this.addedPersons },
                        entities: this.addedEntities,
                        agency: this.entityDetails,
                        createdCases: this.createdCases,
                        disposition: this.disposition,
                        intakeDATypeDetails: this.addedIntakeDATypeDetails,
                        recordings: this.recordings,
                        General: this.general,
                        attachement: this.addAttachement,
                        evaluationFields: this.evalFields,
                        appointments: this.intakeAppointment,
                        DAType: { DATypeDetail: this.disposition },
                        reviewstatus: reviewStatus,
                        Allegations: [],
                        sdm: this.addSdm,
                        clwStatus: null,
                        generatedDocuments: this.genratedDocumentList,
                        communicationFields: this.communicationFields
                    };

                    intakeSaveModel.General.Firstname = this.addNarrative.Firstname ? this.addNarrative.Firstname : '';
                    intakeSaveModel.General.Lastname = this.addNarrative.Lastname ? this.addNarrative.Lastname : '';
                    // intakeSaveModel.General.AgencyCode = intakeSaveModel.General.Agency ? intakeSaveModel.General.Agency : '';
                    intakeSaveModel.General.AgencyCode = intakeSaveModel.General.Agency ? intakeSaveModel.General.Agency.split('~')[0] : '';
                    intakeSaveModel.General.Source = intakeSaveModel.General.InputSource ? intakeSaveModel.General.InputSource : '';
                    /* if (reviewStatus.status === 'Approved') {
                         intakeSaveModel.clwStatus = PENDING_FOR_SAO_DOCS; // 1 Document need to generate
                     }*/
                    const finalIntake = {
                        intake: intakeSaveModel,
                        review: reviewStatus
                    };
                    if (this.selectteamtypekey) {
                        const selectedPurpose = this.getSelectedPurpose(this.selectteamtypekey);
                        if (selectedPurpose) {
                            if (selectedPurpose.description === 'Information and Referral') {
                                finalIntake.review['isfromintake'] = true;
                                finalIntake.review['isclosecase'] = true;
                            }
                        }
                    }

                    this._commonHttpService.create(finalIntake, NewUrlConfig.EndPoint.Intake.SupervisorApprovalUrl).subscribe(
                        (response) => {
                            this.onReload();
                            this.btnDraft = false;
                        },
                        (error) => {
                            this._alertService.error('Unable to approve intake, please try again.');
                            console.log('Approve Intake Error', error);
                            return false;
                        }
                    );
                }
            }
        } else {
            ControlUtils.validateAllFormFields(this.departmentActionIntakeFormGroup);
            this._alertService.warn('Please input the mandatory fields.');
        }
    }
    onReload() {
        const role = this._authService.getCurrentUser();
        let url = '';
        if (role.role.name === 'apcs') {
            url = '/pages/cjams-dashboard';
        } else if (this.isCLW) {
            url = '/pages/sao-dashboard';
        } else {
            url = '/pages/newintake/new-saveintake';
        }
        this._alertService.success('Intake saved successfully!');
        Observable.timer(500).subscribe(() => {
            this._router.routeReuseStrategy.shouldReuseRoute = function () {
                return false;
            };
            this._router.navigate([url]);
        });
    }
    conditionalValidation(): boolean {
        if (this.addedPersons.length > 0) {
            this.addedPersons.map((item) => {
                if (this.roleId.user.userprofile.teamtypekey !== 'DJS') {
                    if (item.Role === 'RA' || item.Role === 'RC' || item.Role === 'CLI') {
                        this.roleValue = true;
                    }
                } else {
                    if (item.Role === 'Youth') {
                        this.roleValue = true;
                    }
                }
            });
        } else {
            this._alertService.error('Please add a person to submit intake');
            return false;
        }
        if (this.roleValue === false) {
            if (this.roleId.user.userprofile.teamtypekey === 'CW') {
                this._alertService.error('Please add Reported Child as primary role.');
            } else if (this.roleId.user.userprofile.teamtypekey === 'DJS') {
                this._alertService.error('Please add user of role Youth.');
            }
            return false;
        }

        const roleId = this._authService.getCurrentUser();
        if (roleId.role.name !== 'apcs') {
            if (roleId.role.name !== 'SCRNW') {
                if (this.disposition && this.disposition.length > 0) {
                    if (!this.disposition[0].dispositioncode || !this.disposition[0].intakeserreqstatustypekey) {
                        this._alertService.error('Please fill status and disposition');
                        return false;
                    }
                } else {
                    this._alertService.error('Please fill status and disposition');
                    return false;
                }
            }
        } else if (roleId.role.name === 'apcs') {
            if (this.disposition && this.disposition.length > 0) {
                if (!this.disposition[0].reason && this.timeLeft === 'Overdue') {
                    this._alertService.error('Please input the reason for delay');
                    return false;
                }
                if (!this.disposition[0].supDisposition || !this.disposition[0].supStatus) {
                    this._alertService.error('Please fill status and disposition');
                    return false;
                }
            } else {
                this._alertService.error('Please fill status and disposition');
                return false;
            }
        }
        // if (this.departmentActionIntakeFormGroup.value.isOtherAgency) {
        //     if (this.departmentActionIntakeFormGroup.value.otheragency === '') {
        //         this._alertService.error('Please fill request from other agency');
        //         return false;
        //     }
        // }

        if (this.roleId.user.userprofile.teamtypekey !== 'DJS') {
            if (this.addNarrative.Narrative === '') {
                this._alertService.error('Please fill narrative');
                return false;
            }

            if (!this.addNarrative.IsAnonymousReporter && !this.addNarrative.IsUnknownReporter && !this.intakeInfoNreffGrid) {
                if (this.addNarrative.Firstname === '' || this.addNarrative.Lastname === '') {
                    this._alertService.error('Please fill first name and last name');
                    return false;
                }
            } else if (!this.addNarrative.RefuseToShareZip && this.intakeInfoNreffGrid) {
                if (this.addNarrative.ZipCode === '') {
                    this._alertService.error('Please fill zip code in narrative');
                    return false;
                }
            }
        }

        if (this.isDjs) {
            if (!this.evalFields.countyid) {
                this._alertService.error('Please fill County of Occurrence in Evaluation Fields');
                return false;
            } else if (this.evalFields.evaluationsourcetypeid === '') {
                this._alertService.error('Please fill Source Type in Evaluation Fields');
                return false;
            } else if (this.evalFields.evaluationsourceagencyid === '') {
                this._alertService.error('Please fill Source Agency in Evaluation Fields');
                return false;
            }
        }
        return true;
    }

    getSelectedPurpose(purposeID) {
        if (this.purposeList) {
            return this.purposeList.find((puroposeItem) => puroposeItem.intakeservreqtypeid === purposeID);
        }
        return null;
    }
    checkForDJSSelected(puropose) {
        this.djsSelected = false;
        // console.log(this.purposeList);
        const selectedPurpose = this.getSelectedPurpose(puropose);
        if (selectedPurpose) {
            this.purposeInputSubject$.next(selectedPurpose);
            this.listAllegations(selectedPurpose.intakeservreqtypeid);
            if (selectedPurpose.teamtype.teamtypekey === 'DJS') {
                this.djsSelected = true;
            } else {
                this.djsSelected = false;
            }
        }
    }
    private mapIntakeScreenInfo(model: General): IntakeScreen {
        model.Narrative = this.addNarrative.Narrative;
        model.IsAnonymousReporter = this.addNarrative.IsAnonymousReporter;
        model.IsUnknownReporter = this.addNarrative.IsUnknownReporter;
        model.RefuseToShareZip = this.addNarrative.RefuseToShareZip;
        if (this.addedPersons) {
            this.intakeScreen.Person = this.addedPersons.map((item) => {
                ObjectUtils.removeEmptyProperties(item);
                return new Person(item);
            });
        }
        this.intakeScreen.NarrativeIntake = [].concat.apply(
            Object.assign({
                Firstname: this.addNarrative.Firstname,
                Lastname: this.addNarrative.Lastname,
                PhoneNumber: this.addNarrative.PhoneNumber,
                ZipCode: this.addNarrative.ZipCode,
                Role: this.addNarrative.Role
            })
        );
        this.intakeScreen.EvaluationField = this.evalFields;
        this.intakeScreen.Appointments = this.intakeAppointment;
        if (this.addedIntakeDATypeDetails) {
            this.intakeScreen.DAType.DATypeDetail = this.addedIntakeDATypeDetails.map((item) => new DATypeDetail(item));
        }
        if (this.addedCrossReference) {
            this.intakeScreen.CrossReferences = this.addedCrossReference.map((item) => new CrossReference(item));
        }
        this.intakeScreen.Recording.Recordings = this.recordings;
        this.intakeScreen.General = model;
        this.intakeScreen.Allegations = [].concat.apply(
            [],
            this.addedIntakeDATypeDetails.map((item) => {
                return item.Allegations;
            })
        );
        if (this.addedEntities) {
            this.intakeScreen.Agency = this.addedEntities.map((item) => new Agency(item));
        }
        this.intakeScreen.AttachmentIntake = this.addAttachement;

        return this.intakeScreen;
    }

    private populateIntake() {
        const roleDls = this._authService.getCurrentUser();
        this.route.params.subscribe((item) => {
            this.intakeId = item['id'];
            if (this.intakeId) {
                this._commonHttpService
                    .create(
                        {
                            page: 1,
                            limit: 10,
                            where: {
                                referralnumber: this.intakeId
                            }
                        },
                        ReferralUrlConfig.EndPoint.Referral.getProviderReferralUrl
                    )
                    .subscribe(
                        (response) => {
                            console.log("SIMAR populateIntake");
                            this.referralInformation$ = response.data;

                            console.log(this.referralInformation$);
                            this.providerReferralIntake = this.referralInformation$[0];
                            console.log(this.providerReferralIntake);

                            this.intakeNumber = response.data[0].provider_referral_id;
                            this.departmentActionIntakeFormGroup.value.IntakeNumber = this.intakeNumber;
                            console.log(this.intakeNumber);
                            this.patchReferralForm(response);

                            this.narrativeDetails.draftId = '1';
                            this.narrativeDetails.Narrative = response.data[0].narrative;
                            this.narrativeOutputSubject$.next(this.narrativeDetails);
                            console.log("Manasa");
                            console.log(response.data[0]);
                            this.profileDetails.fullname = response.data[0].provider_referral_nm;
                            this.profileDetails.firstName = response.data[0].provider_referral_first_nm;
                            this.profileDetails.phoneNumber = response.data[0].adr_cell_phone_tx;
                            this.profileDetails.cityName = response.data[0].adr_city_nm;
                            this.profileDetails.state = response.data[0].adr_state_cd;
                            this.profileDetails.streetName = response.data[0].adr_street_nm;
                            this.profileDetails.zipCode = response.data[0].adr_zip5_no;
                            this.profileDetails.lastName = response.data[0].provider_referral_last_nm;
                            this.profileDetails.country = response.data[0].adr_country_tx;
                            this.profileDetails.countyCd = response.data[0].adr_county_cd;
                            this.profileDetails.streetNo = response.data[0].adr_street_no;
                            this.profileDetails.taxid = response.data[0].corporation_entity_taxid;
                            this.profileDetails.faxNumber = response.data[0].adr_street_nm;
                            this.profileDetails.workNumber = response.data[0].adr_cell_phone_tx;
                            this.profileDetails.email = response.data[0].adr_email_tx;

                            this.profileDetails.program = response.data[0].provider_referral_program;
                            this.profileDetails.programType = response.data[0].provider_program_type;
                            this.profileDetails.programName = response.data[0].provider_program_name;

                            this.profileDetails.gender = "Male";
                            this.profileDetails.minAge = 2;
                            this.profileDetails.maxAge = 8;
                            this.profileDetails.bedCapacity = 10;
                            console.log(this.profileDetails);
                            this.profileOutputSubject$.next(this.profileDetails);
                            console.log(this.profileOutputSubject$);
                            // this.btnDraft = true;
                            // this.timeLeft = response.data[0].timeleft;
                            // this.clwStatus = response.data[0].clwstatus;
                            // this.saoTabIndex$.next(this.clwStatus);
                            // this.timeReceived$.next(this.timeLeft);
                            // this.isPreIntake$.next(response.data[0].ispreintake);
                            // this.isPreIntake = response.data[0].ispreintake;
                            // this._dataStoreService.setData(NewReferralConstants.Intake.ispreintake, response.data[0].ispreintake);
                            // this._dataStoreService.setData(NewReferralConstants.Intake.timeleft, this.timeLeft);
                            // if (response.data[0].jsondata) {
                            //     const intakeModel = response.data[0].jsondata;
                            //     this.intakerWorkerId = intakeModel.securityuserid;
                            //     this.entityDetails = intakeModel.agency;
                            //     this.addedCrossReference = intakeModel.crossReference;
                            //     this.addedPersons = intakeModel.persons ? intakeModel.persons : intakeModel.persondetails ? intakeModel.persondetails.Person : [];

                            //     // this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Health.PhysicianFromIntake, this.addedPersons);

                            //     this.addedEntities = intakeModel.entities;
                            //     this.addedIntakeDATypeDetails = intakeModel.intakeDATypeDetails;
                            //     this.recordings = intakeModel.recordings;
                            //     if (intakeModel.General.Purpose.indexOf('~') === -1) {
                            //         intakeModel.General.Purpose = intakeModel.General.Purpose + '~' + intakeModel.General.AgencyCode;
                            //     }
                            //     this.general = intakeModel.General;
                            //     this.intakeNumber = intakeModel.General.IntakeNumber;
                            //     this.general$.next(this.general);
                            //     this.preIntakeDisposition = intakeModel.preIntakeDispo;
                            //     this.preIntakedispositionPost$.next(this.preIntakeDisposition);
                            //     this.reviewstatus = intakeModel.reviewstatus;
                            //     this.evalFields = intakeModel.evaluationFields;
                            //     this.createdCases = intakeModel.createdCases;
                            //     this.communicationFields = intakeModel.communicationFields;
                            //     this.addedPersonsOutput$.next(this.addedPersons);
                            //     this.createdCaseInputSubject$.next(this.createdCases);
                            //     this.evalFieldsOutputSubject$.next(intakeModel.evaluationFields);
                            //     this.communicationOutputSubject$.next(intakeModel.communicationFields);
                            //     if (intakeModel.DAType) {
                            //         if (intakeModel.DAType.DATypeDetail && intakeModel.DAType.DATypeDetail.length) {
                            //             this.disposition = intakeModel.DAType.DATypeDetail;
                            //             this.reviewStatus = intakeModel.DAType.DATypeDetail[0].DAStatus;
                            //         }
                            //     } else {
                            //         this.reviewStatus = intakeModel.reviewstatus.status;
                            //         this.disposition = intakeModel.disposition;
                            //     }
                            //     this.reviewStatus$.next(this.reviewStatus);
                            //     if ((this.roleId.role.name === 'apcs' || this.roleId.role.name === 'Intake Worker') && (this.reviewStatus === 'Closed' || this.reviewStatus === 'Approved')) {
                            //         this.narrativeDetails.Firstname = this.general.Firstname;
                            //         this.narrativeDetails.Lastname = this.general.Lastname;
                            //         this.narrativeDetails.ZipCode = this.general.offenselocation;
                            //         // this.narrativeDetails.PhoneNumber = intakeModel.narrative ? intakeModel.narrative[0].PhoneNumber : '';
                            //     } else {
                            //         this.narrativeDetails.Firstname = intakeModel.narrative ? intakeModel.narrative[0].Firstname : '';
                            //         this.narrativeDetails.Lastname = intakeModel.narrative ? intakeModel.narrative[0].Lastname : '';
                            //         this.narrativeDetails.ZipCode = intakeModel.narrative ? intakeModel.narrative[0].ZipCode : '';
                            //         this.narrativeDetails.PhoneNumber = intakeModel.narrative ? intakeModel.narrative[0].PhoneNumber : '';
                            //     }
                            //     this.narrativeDetails.Narrative = this.general.Narrative;
                            //     this.narrativeDetails.draftId = this.draftId;
                            //     this.narrativeDetails.IsAnonymousReporter = this.general.IsAnonymousReporter === true ? true : false;
                            //     this.narrativeDetails.IsUnknownReporter = this.general.IsUnknownReporter === true ? true : false;
                            //     this.narrativeDetails.RefuseToShareZip = this.general.RefuseToShareZip === true ? true : false;
                            //     this.dispositionOutPut$.next(this.disposition);
                            //     this.narrativeOutputSubject$.next(this.narrativeDetails);
                            //     this.createdCaseOuptputSubject$.next(this.createdCases);
                            //     this.intakeAppointment = intakeModel.appointments;
                            //     this.appointmentOutputSubject$.next(this.intakeAppointment);
                            //     // this.courtDetailsOutputSubject$.next(intakeModel.courtDetails);
                            //     //this.petitionDetailsOutputSubject$.next(intakeModel.petitionDetails);
                            //     this.contactListOutput$.next(this.recordings);
                            //     this.genratedDocumentList = intakeModel.generatedDocuments;
                            //     this.generateDocumentInput$.next(this.genratedDocumentList);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.preIntakeDisposition, intakeModel.preIntakeDispo);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.disposition, intakeModel.disposition);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.createdCases, intakeModel.createdCases);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.intakenumber, intakeModel.General.IntakeNumber);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.addedPersons, this.addedPersons);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.evalFields, intakeModel.evaluationFields);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.intakeappointment, intakeModel.appointments);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.communications, intakeModel.communicationFields);
                            //     this._dataStoreService.setData(NewReferralConstants.Intake.generatedDocuments, intakeModel.generatedDocuments);
                            //     // this._dataStoreService.setData(NewReferralConstants.Intake.courtDetails, intakeModel.courtDetails);
                            //     //this._dataStoreService.setData(NewReferralConstants.Intake.petitionDetails, intakeModel.petitionDetails);
                            //     if (this.intakeAppointment && this.intakeAppointment.length > 0) {
                            //         this.IWtoAssign = this.intakeAppointment[0].intakeWorkerId;
                            //     }

                            //     // if (intakeModel.preIntakeDispo) {
                            //     //     this.preIntakeSupDicision = intakeModel.preIntakeDispo.status;
                            //     // }

                            //     if (intakeModel.sdm) {
                            //         intakeModel.sdm.datesubmitted = response.data[0].datesubmitted;
                            //         this.sdmReport$.next(intakeModel.sdm);
                            //         this.sdmOutputSubject$.next(intakeModel.sdm);
                            //     }

                            //     // if (this.general) {
                            //     //     this.disposition.dispositioncode = this.general.dispositioncode !== '' ? this.general.dispositioncode : '';
                            //     //     this.disposition.intakeserreqstatustypekey = this.general.intakeserreqstatustypekey !== '' ? this.general.intakeserreqstatustypekey : '';
                            //     //     this.dispositionRetrive$.next(this.disposition);
                            //     // }
                            //     if (this.entityDetails) {
                            //         this.entitiesSubject$.next(this.entityDetails);
                            //     }
                            //     if (this.addedIntakeDATypeDetails && this.addedIntakeDATypeDetails.length) {
                            //         this.daAllegaDispo.getSavedIntakeAssessmentDetails(this.addedIntakeDATypeDetails, intakeModel.General.IntakeNumber);
                            //     }
                            //     if (this.general.Agency) {
                            //         // this.listPurpose(this.general.Agency);
                            //         // this.listService(this.general.Purpose);
                            //         // this.listPurpose({ text: 'myIntake', value: this.general.Agency });
                            //         this.listService({ text: '', value: this.general.Purpose });
                            //         this.selectteamtypekey = this.general.Purpose;
                            //         if (this.general.Purpose === 'd207bdd4-f281-4ec8-949c-8fd9657227f9') {
                            //             this.serviceCheckboxOutput$.next('fromDashboard');
                            //         }
                            //         this.intakeservice = this.general.intakeservice;
                            //     }
                            //     if (this.reviewstatus.appevent === 'INTR') {
                            //         this.saveIntakeBtn = true;
                            //     }
                            //     if (this.reviewstatus.appevent === 'SITR' && roleDls.role.name === 'SCRNW') {
                            //         this.saveIntakeBtn = true;
                            //     }
                            //     if (this.general.safeHavenAssessmentScore && this.general.safeHavenAssessmentScore >= 1) {
                            //         this.viewSafeHaven = true;
                            //     }
                            //     if (this.general.kinshipAssessmentScore && this.general.kinshipAssessmentScore >= 1) {
                            //         this.viewKinship = true;
                            //     }
                            //     if (this.general.ascrsScore && this.general.ascrsScore >= 1) {
                            //         this.viewAscrs = true;
                            //     }
                            //     this.intakeservice = this.general.intakeservice;

                            //     if (this.reviewstatus.appevent === 'INTR') {
                            //         this.saveIntakeBtn = true;
                            //     }
                            //     if (this.reviewstatus.appevent === 'SITR' && roleDls.role.name === 'SCRNW') {
                            //         this.saveIntakeBtn = true;
                            //     }
                            //     if (this.general.safeHavenAssessmentScore && this.general.safeHavenAssessmentScore >= 1) {
                            //         this.viewSafeHaven = true;
                            //     }
                            //     if (this.general.kinshipAssessmentScore && this.general.kinshipAssessmentScore >= 1) {
                            //         this.viewKinship = true;
                            //     }
                            //     if (this.general.ascrsScore && this.general.ascrsScore >= 1) {
                            //         this.viewAscrs = true;
                            //     }
                            //     this.safeHavenAssessmentScore = this.general.safeHavenAssessmentScore;
                            //     this.kinshipAssessmentScore = this.general.kinshipAssessmentScore;
                            //     this.ascrsScore = this.general.ascrsScore;
                            //     this.patchFormGroup(this.general);
                            //     this.prepareCpsDocDetails(this.general.InputSource);
                            //  ControlUtils.markFormGroupTouched(this.departmentActionIntakeFormGroup);
                            // } else {
                            //     this.loadDefaults();
                            // }
                        },
                        (error) => {
                            this._alertService.error('Unable to fetch saved intake details.');
                            this.loadDefaults();
                        }
                    );
            } else {
                this.loadDefaults();
            }
        });
    }

    private patchReferralForm(response) {
        // alert(response.data[0].agency);
        //const recDate = new Date(this.general.RecivedDate);
        //this.general.RecivedDate = recDate.toUTCString();

        this.departmentActionIntakeFormGroup.patchValue({
            IntakeNumber: this.intakeNumber,
            Author: response.data[0].author,
            Agency: response.data[0].agency,
            InputSource: response.data[0].provider_referral_med
        });
        // if (this.departmentActionIntakeFormGroup.value.isOtherAgency) {
        //     this.otherAgencyControlName.enable();
        // }
    }

    private patchFormGroup(general: General = new General()) {
        const recDate = new Date(this.general.RecivedDate);
        this.general.RecivedDate = recDate.toLocaleString();

        this.departmentActionIntakeFormGroup.patchValue({
            Source: general.Source ? general.Source : '',
            InputSource: general.InputSource ? general.InputSource : '',
            RecivedDate: new Date(this.general.RecivedDate),
            CreatedDate: general.CreatedDate ? general.CreatedDate : '',
            Author: general.Author ? general.Author : '',
            IntakeNumber: general.IntakeNumber ? general.IntakeNumber : '',
            Agency: general.Agency ? general.Agency : '',
            Purpose: general.Purpose ? general.Purpose : '',
            // otheragency: general.otheragency ? general.otheragency : '',
            // isOtherAgency: general.isOtherAgency ? general.isOtherAgency : false
        });
        if (this.draftId) {
            this.intakeAttachment.getIntakeNumber(general.IntakeNumber);
        }
        // if (this.departmentActionIntakeFormGroup.value.isOtherAgency) {
        //     this.otherAgencyControlName.enable();
        // }
    }

    genCpsIntakeDoc(action: string) {
        this.cpsIntakeGenAction$.next(action);
    }

    collectivePdfCreator() {
        this.downloadInProgress = true;
        const pdfList = ['Appeal-Letter', 'Formal-Action-Letter', 'Formal-Action-Letter-Complaint', 'Process-Letter'];
        pdfList.forEach((element) => {
            this.downloadCasePdf(element);
        });
    }
    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            // console.log(pages.item(i).getAttribute('data-page-name'));
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
            });
        }
        this.convertImageToPdf();
    }
    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#intake-complaint-pdf1')).modal('hide');
        this.pdfFiles = [];
        this.downloadInProgress = false;
    }

    showResourcePopup(labelName) {
        this.resourcePoplabel = '';
        if (labelName === 'CONTACT NUMBER') {
            this.resourcePoplabel = labelName;
            this.generalResourceFormGroup.patchValue({
                helpDescription: this.generalResource[0].helptext
            });
            // (<any>$('#save-edit-resource-popup')).modal('show');
        } else if (labelName === 'CREATED DATE') {
            this.resourcePoplabel = labelName;
            this.generalResourceFormGroup.patchValue({
                helpDescription: this.generalResource[1].helptext
            });
        } else if (labelName === 'COMMUNICATION') {
            this.resourcePoplabel = labelName;
            this.generalResourceFormGroup.patchValue({
                helpDescription: this.generalResource[2].helptext
            });
        } else if (labelName === 'REQUEST FROM OTHER AGENCY') {
            this.resourcePoplabel = labelName;
            // this.generalResourceFormGroup.patchValue({
            //     helpDescription: this.generalResource[3].helptext
            // });
        } else if (labelName === 'AGENCY') {
            this.resourcePoplabel = labelName;
            this.generalResourceFormGroup.patchValue({
                helpDescription: this.generalResource[3].helptext
            });
        } else if (labelName === 'PURPOSE') {
            this.resourcePoplabel = labelName;
            this.generalResourceFormGroup.patchValue({
                helpDescription: this.generalResource[4].helptext
            });
        } else if (labelName === 'RECEIVED DATE') {
            this.resourcePoplabel = labelName;
            this.generalResourceFormGroup.patchValue({
                helpDescription: this.generalResource[5].helptext
            });
        }
        (<any>$('#save-edit-resource-popup')).modal('show');
    }

    submitResource(resourcePoplabel) {
        this.submitResourceObject = Object.assign({}, new GeneralNarative());
        if (this.resourcePoplabel === 'CONTACT NUMBER') {
            this.submitResourceObject.controlindex = 0;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get('helpDescription').value;
            // (<any>$('#save-edit-resource-popup')).modal('show');
        } else if (this.resourcePoplabel === 'CREATED DATE') {
            this.submitResourceObject.controlindex = 1;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get('helpDescription').value;
        } else if (this.resourcePoplabel === 'COMMUNICATION') {
            this.submitResourceObject.controlindex = 2;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get('helpDescription').value;
        } else if (this.resourcePoplabel === 'REQUEST FROM OTHER AGENCY') {
            // this.submitResourceObject.controlindex = 3;
            // this.submitResourceObject.helptext = this.generalResourceFormGroup.get('helpDescription').value;
        } else if (this.resourcePoplabel === 'AGENCY') {
            this.submitResourceObject.controlindex = 3;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get('helpDescription').value;
        } else if (this.resourcePoplabel === 'PURPOSE') {
            this.submitResourceObject.controlindex = 4;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get('helpDescription').value;
        } else if (this.resourcePoplabel === 'RECEIVED DATE') {
            this.submitResourceObject.controlindex = 5;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get('helpDescription').value;
        }
        const jsonData = Object.assign({}, this.submitResourceObject);
        const submitResourceURL = 'helptexts/addupdate';
        this._genericServiceNarative.create(jsonData, submitResourceURL).subscribe((response) => {
            // console.log(response);
            (<any>$('#save-edit-resource-popup')).modal('hide');
            this.loadDefaults();
        });
    }

    prepareCpsDocDetails(input: string) {
        console.log('communica');
        this.cpsdocData.intakePurpose = this.itnakeServiceGrid;
        if (this.intakeCommunication) {
            this.intakeCommunication.map((data) => {
                if (data.value === input) {
                    this.cpsdocData.InputSource = data.text;
                }
            });
        }
    }

    onChangeSupervisor(supervisor: RoutingUser) {
        console.log(supervisor);
        this.selectedSupervisor = supervisor.userid;
    }

    private listServiceSubtype(intakeservreqtypeid) {
        const checkInput = {
            include: 'servicerequestsubtype',
            nolimit: true,
            where: { intakeservreqtypeid: intakeservreqtypeid },
            method: 'get'
        };
        this._commonHttpService.getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.DATypeUrl + '/?filter').subscribe((result) => {
            this.subServiceTypes = result[0].servicerequestsubtype;
        });
    }

    private loadSupervisor() {
        this.selectedSupervisor = '';
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INTR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.supervisorsList = result.data;
                this.supervisorsList = this.supervisorsList.filter((res) => res.agencykey === this.selectedAgency.value);
            });
    }

    onChangeIntaker(intaker: RoutingUser) {
        this.selectedIntaker = intaker['userid'];
    }

    private loadIntaker() {
        this._commonHttpService.getAll('Intakedastagings/getIntakeUsers?filter={}').subscribe((result) => {
            this.intakersList = result;
        });
    }

    accessRights() {
        if (this.roleId.role.name === 'apcs') {
            if (this.isPreIntake) {
                this.accessStatus = this.reviewStatus !== 'supreview';
            } else {
                const acceptableStatusList = ['Review', 'Approved'];
                const notFound = acceptableStatusList.indexOf(this.reviewStatus) == -1;
                this.accessStatus = notFound;
            }

        } else if (this.roleId.role.name === 'Intake Worker') {
            this.accessStatus = (this.reviewStatus === 'Review' && this.reviewstatus.appevent === 'INTR') || this.reviewStatus === 'Approved' || this.reviewStatus === 'Closed';
        } else if (this.roleId.role.name === 'SCRNW') {
            this.accessStatus = this.reviewStatus === 'Approved' || this.reviewStatus === 'Closed';
        }
    }

    validateIRAR(sdm) {
        if (sdm.scnRecommendOveride !== '') {
            if (sdm.scnRecommendOveride === 'Ovrscrnin') {
                sdm.isir = true;
                sdm.isar = false;
            }
            if (sdm.scnRecommendOveride === 'OvrScrnout') {
                sdm.isir = false;
                sdm.isar = true;
            }
        } else {
            if (sdm.screeningRecommend === 'Scrnin') {
                sdm.isir = true;
                sdm.isar = false;
            }
            if (sdm.screeningRecommend === 'ScreenOUT') {
                sdm.isir = false;
                sdm.isar = true;
            }
        }
    }

    goToHome() {
        let url = '';

        if (this.isCLW) {
            url = '/pages/sao-dashboard';
        } else {
            url = '/pages/newintake/new-saveintake';
        }

        this._router.navigate([url]);
    }

    getSelectedYouth() {
        if (this.addedPersons) {
            const personYouth = this.addedPersons.find((person) => person.Role === 'Youth');
            if (personYouth) {
                return personYouth.fullName;
            }
        }

        return null;
    }

    listAllegations(purposeID) {
        const checkInput = {
            where: { intakeservreqtypeid: purposeID },
            method: 'get',
            nolimit: true,
            order: 'name'
        };
        this._commonHttpService
            .getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.allegationsUrl + '/?filter')
            .map((result) => {
                this.offenceCategoriesInputSubject$.next(result);
                // console.log('all', result);
                return result;
            })
            .subscribe();
    }
    getRoutingUser(modal) {


        const generalForm = this.departmentActionIntakeFormGroup.getRawValue();

        if (!generalForm.InputSource) {
            this._alertService.error('please enter communication');

            (<any>$('#intake-caseassign')).modal('hide');
            return false;
        }
       
        if (!this.dropdownselectval) {
            this._alertService.error('please provide required fields');
            (<any>$('#intake-caseassign')).modal('hide');
        } else {
            // this.updateReferralDraft();
            this.getUsersList = [];
            this.originalUserList = [];
            let roletype = 'PTA';
            if (this.role === 'DJS') {
                roletype = 'PTADJS';
            }
            this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        where: { appevent: roletype },
                        method: 'post'
                    }),
                    'providerassignmentownership/getroutingusers'
                )
                .subscribe(result => {
                    if (result) {
                        this.originalUserList = result.data;
                        if (roletype === 'PTADJS') {
                            this.getUsersList = this.originalUserList.filter(res => {
                                if (res.issupervisor === false) {
                                    return res;
                                }
                            });
                        } else {
                            this.getUsersList = this.originalUserList.filter(res => {
                                if (res.rolecode === 'PM' || res.rolecode === 'QA' || res.userrole === 'Supervisor') {
                                    return res;
                                }
                            });
                        }

                        this.originalUserList = this.getUsersList;
                    }
                });
            (<any>$('#intake-caseassign')).modal('show');
        }
    }

    selectPerson(row) {
        this.selectedPerson = row;
        // THIS IS THE PERSON LOGGED IN WHICH IS (FROM)
    }
    assignIntaker(modal, assignId, status) {
        if (assignId) {
            this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        where: {
                            appeventcode: modal,
                            intakenumber: this.intakeNumber,
                            assigneduserid: assignId,
                            status: status
                        },
                        method: 'post'
                    }),
                    'Intakedastagings/assignIntake'
                )
                .subscribe((result) => {
                    this._alertService.success('Intake assigned successfully!');
                    // this.closePopup();
                    const dashboardURL = '/pages/cjams-dashboard';
                    setTimeout(() => {
                        this._router.navigate([dashboardURL]);
                    }, 2000);
                });
        } else {
            this._alertService.warn('Please select a intake worker');
        }
    }
    submitDocuments() {
        if (!this.genratedDocumentList) {
            this.showDocumentGenerateErrorMessage();
            return false;
        }
        const notGeneratedRequiredDocs = this.genratedDocumentList.filter((document) => document.isRequired && !document.isGenerated);
        if (notGeneratedRequiredDocs.length !== 0) {
            this.showDocumentGenerateErrorMessage();
            return false;
        }
        this.clwStatus = WAITING_FOR_SAO_UPDATES;
        (<any>$('#document-complete-modal')).modal('hide');
        this.draftIntake(this.departmentActionIntakeFormGroup.value, 'CLWDRAFT');
    }

    private showDocumentGenerateErrorMessage() {
        this._alertService.error('Please generate required Documents');
        (<any>$('#document-complete-modal')).modal('hide');
    }

    backToDashboard() {
        this._router.navigate(['/cjams-dashboard/assign-case']);
    }

    onChangeCommunication(value) {
        this._dataStoreService.setData('SelectedCommunication', value);
    }



}
