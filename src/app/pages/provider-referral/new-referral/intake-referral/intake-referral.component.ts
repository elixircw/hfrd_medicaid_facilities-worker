

import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';

import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProviderReferral } from '../_entities/newreferralModel';
import { Observable, Subject } from 'rxjs';
import { identifierModuleUrl } from '@angular/compiler';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService, DataStoreService } from '../../../../@core/services';
import { Router } from '@angular/router';
import { AddReferralComponent } from '../add-referral/add-referral.component';

@Component({
  selector: 'intake-referral',
  templateUrl: './intake-referral.component.html',
  styleUrls: ['./intake-referral.component.scss']
})
export class IntakeReferralComponent implements OnInit {
  providerSearchForm: FormGroup;
  searchFieldsForm: FormGroup;
  referralForm: Observable<ProviderReferral>;
  referral: ProviderReferral;
  addedProviders = [];
  searchProviderId: Number;
  searchProviderName: string = '';
  searchTaxId: string = '';
  searchStatus: string = '';
  searchJurisdiction: string = '';
  eventSelect: Array<any> = [];
  providers: any;
  accordianData: Array<any> = [];
  accordianData$: Observable<ProviderReferral>;
  newObj: any = {};
  hasReferralNo: Boolean = false;
  paginationInfo: PaginationInfo = new PaginationInfo();
  providerid : any;
  providername : any;
  providertaxid : any;
  status : any;
  juridiction : any;
  categoryCodeRef: any = {
    '1782': 'CPA Home',
    '1783': 'Local Department Home',
    // '1784' : 'LDSS Kinship Home',
    '1785': 'ICPC Home Study',
    '3049': 'Private Organization',
    '3274': 'RCC Facility',
    '3302': 'CPA Office',
    '3304': 'Vendor',
    '3305': 'Community Provider'
  };
  countyList$: Observable<DropdownModel[]>;
  statusList = [{text: 'All', value : null}, {text: 'Pending', value : 'Pending'}, {text: 'Approved', value : 'Approved'}];

  @Input() referralInformation$: Observable<ProviderReferral[]>;
  @Input() referralId;
  viewReferral: ProviderReferral;
  
  @Output() emitReferralInformation = new EventEmitter();
  @Output() passReferalObj = new EventEmitter();

  editReferralSubject = new Subject<ProviderReferral>();
  providerSelected: Boolean = false;
  referralAdded: Boolean = false;
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  previousPage: number;
  private pageSubject$ = new Subject<number>();
  @ViewChild(AddReferralComponent) addRefferal: AddReferralComponent;
  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _router: Router) {
  }

  ngOnInit() {
    //this.viewReferral.provider_referral_id=this.referralId;
    if (this._dataStoreService.getData('isReferral') === 'true') {
      this.referralId = this._dataStoreService.getData('REFERRAL_NUMBER');
      this.referralAdded = true;
    }
    console.log('TESTTTT :: ', this.referralId);
    this.providerSearchForm = this._formBuilder.group({
      lastname: [''],
      firstname: [''],
      maidenname: [''],
      gender: [''],
      dob: [''],
      dateofdeath: [''],
      ssn: [''],
      mediasrc: [''],
      mediasrctxt: [''],
      occupation: [''],
      dl: [''],
      stateid: [''],
      address1: [''],
      address2: [''],
      zip: [''],
      city: [''],
      county: [''],
      selectedPerson: [''],
      cjisnumber: [''],
      complaintnumber: [''],
      fein: [''],
      age: [''],
      email: [''],
      phone: [''],
      petitionid: [''],
      alias: [''],
      oldId: ['']
    });
    this.searchFieldsForm = this._formBuilder.group({ 
      providerid : [''],
      providername : [''],
      providertaxid : [''],
      status : [''],
      juridiction : ['']
    });
    this.searchProviderId = null;
    this.searchProviderName = null;
    this.searchTaxId = null;
    this.searchStatus = null;
    this.searchJurisdiction = null;
    this.loadCountyDropdown();
    this.pageSubject$.subscribe((pageNumber) => {
      this.paginationInfo.pageNumber = pageNumber;
      this.getPrivateProviderReferral(this.paginationInfo.pageNumber);
  });
    this.getPrivateProviderReferral(1);
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.previousPage = this.paginationInfo.pageNumber;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    // this.getPrivateProviderReferral(this.paginationInfo.pageNumber);
    this.pageSubject$.next(this.paginationInfo.pageNumber);
  }

  setReferralInfo(referralInfo) {
    console.log(JSON.stringify(referralInfo));
    referralInfo.isSON = (referralInfo.isSON) ? 'Yes' : 'No';
    referralInfo.isRFP = (referralInfo.isRFP) ? 'Yes' : 'No';
    referralInfo.isFEIN = (referralInfo.isFEIN) ? 'Yes' : 'No';
    this.referralInformation$ = referralInfo;
    this.canDisplayPager$ = null;
    this.totalRecords$ = null;
    this.accordianData$ = Observable.of(referralInfo);
    this.referralAdded = true;
    //this.addedProviders.push(referralInfo);
    this.emitReferralInformation.emit(referralInfo);
  }

  selectPerson(referral) {
    this.providerSelected = true;
    this.referral = referral;
    this.referral.provider_referral_id = this.referralId;
  }
  selectedReferral() {
    if ( this.providerSelected === true) {
      this.viewReferral = this.referral;
      this.referral.referral_status = '';
      this.editReferralSubject.next(this.referral);
      (<any>$('#add-referral')).modal('show');
    } else {
      this._alertService.warn('Please select a Provider');
    }
  }
  editReferral(referral) {
    this.viewReferral = referral;
    this.editReferralSubject.next(referral);
    (<any>$('#add-referral')).modal('show');
  }
  addNewReferral() {
    const selectedCommunication = this._dataStoreService.getData('SelectedCommunication');
    if (selectedCommunication) {
      this.editReferralSubject.next(null);
      (<any>$('#add-referral')).modal('show');
    } else {
      this._alertService.error('Please, select the communication');
    }
  }
  onSearch(searchType: string, searchValue: any) {
    console.log(searchValue);
    // Preferably use switch
    if (searchType === 'providerid') {
      this.searchProviderId = Number(searchValue);
    }
    if (searchType === 'providername') {
      this.searchProviderName = searchValue;
    }
    if (searchType === 'providertaxid') {
      this.searchTaxId = searchValue;
    }
    if (searchType === 'providerstatus') {
      this.searchStatus = searchValue;
    }
    if (searchType === 'jurisdiction') {
      this.searchJurisdiction = searchValue;
    }
    this.paginationInfo.pageNumber = 1;
    this.getPrivateProviderReferral(1);
  }
  getPrivateProviderReferral(page) {
    let id = '';
    if (this.referralId !== '' && this.referralId !== null && this.referralId !== undefined && this.referralAdded === true) {
      id = this.referralId;
    } else {
      id = null;
    }
    const source = this._commonHttpService.getArrayList(
      new PaginationRequest({
            method: 'get',
            page: page,
            limit: this.paginationInfo.pageSize,
            count: this.paginationInfo.total,
            where: {
              referral_id: id !== '' && id !== 'null' ? id : null,
              referral_decision : this.searchStatus !== '' && this.searchStatus !== 'null' ? this.searchStatus : null,
              tax_id: this.searchTaxId  !== '' && this.searchTaxId  !== 'null' ?  this.searchTaxId : null,
              provider_referral_nm :  this.searchProviderName,
              provider_id :this.searchProviderId,
              jurisdiction: this.searchJurisdiction!== '' && this.searchJurisdiction !== 'null' ? this.searchJurisdiction : null
            }
      }),
      'providerreferral/getreferrallist?filter'
        ).map((result) => {
          return {
            data: result.length && result[0].getreferrallist && result[0].getreferrallist.length ? result[0].getreferrallist : null,
            count: result.length && result[0].getreferrallist&& result[0].getreferrallist.length ? result[0].getreferrallist[0].totalcount : null,
            canDisplayPager: result.length && result[0].getreferrallist && result[0].getreferrallist.length ? result[0].getreferrallist[0].totalcount > this.paginationInfo.pageSize : false
          };
        })
        .share();
        this.accordianData$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
          this.totalRecords$ = source.pluck('count');
          this.canDisplayPager$ = source.pluck('canDisplayPager');
      }
  }
  loadCountyDropdown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        'admin/county' + '?filter'
      )
    ]).map(res => {
      return {
        countyList: res[0].map(
          item =>
            new DropdownModel({
              text: item.countyname,
              value: item.countyid
            })
        )
      };
    });
    this.countyList$ = source.pluck('countyList');
  }

  enableReferral() {
    (<any>$('#add-referral')).modal('show');
    (<any>$('#criteria-popup')).modal('hide');
    this.addRefferal.resetForm();
  }

  redirectToDashboard() {
    (<any>$('#criteria-popup')).modal('hide');
    this._router.navigate(['/pages/provider-applicant/existing-applicant']);
  }

  resetSearch() {
    this.searchFieldsForm.reset();
    this.searchStatus = null;
    this.searchTaxId = null;
    this.searchProviderName = null;
    this.searchProviderId  = null;
    this.searchJurisdiction = null;

    this.getPrivateProviderReferral(1);
  }

}
