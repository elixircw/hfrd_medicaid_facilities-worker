import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateReferralDecisionComponent } from './private-referral-decision.component';

describe('PrivateReferralDecisionComponent', () => {
  let component: PrivateReferralDecisionComponent;
  let fixture: ComponentFixture<PrivateReferralDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateReferralDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateReferralDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
