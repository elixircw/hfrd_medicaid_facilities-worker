import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApplicationDecision } from '../_entities/newApplicantModel';
import { AuthService, CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { RoutingUser } from '../_entities/existingreferralModel';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { HttpHeaders } from '@angular/common/http';
import { NgxfUploaderService } from 'ngxf-uploader';
@Component({
  selector: 'private-referral-decision',
  templateUrl: './private-referral-decision.component.html',
  styleUrls: ['./private-referral-decision.component.scss']
})
export class PrivateReferralDecisionComponent implements OnInit {

 
  decisionFormGroup: FormGroup;
  currentDecision: ApplicationDecision;
  roleId: AppUser;
  decisions = [];
  @Input() referralId;
  // applicantNumber: string;
  eventcode = 'PTA';

  // Assigning / Routing
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  selectedPerson: any;
  //isFinalDecisoin: boolean = false;
  isFinalDecisoin: boolean;

  isSupervisor: boolean;
  isGroup = false;

  //statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  signimage: any;
  isSignPresent: boolean;
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _uploadService: NgxfUploaderService,
    private _dataStoreService: DataStoreService) {
    // this.applicantNumber = route.snapshot.params['id'];
    //this.applicantNumber = 'A201800300025';
  }

  ngOnInit() {
    this.initializeDecisionForm();

    this.roleId = this._authService.getCurrentUser();
    this.loadSignature();
    console.log(this.roleId);
    this.statusDropdownItems = [
      { "text": "Reject", "value": "Rejected" },
      { "text": "Approve", "value": "Approved" },
      { "text": "Return", "value": "Incomplete" },
    ];
    //this.roleId.user.userprofile.teamtypekey;

    if (this.roleId.role.name == 'Executive Director') {
      this.isFinalDecisoin = true;
    } else {
      this.isFinalDecisoin = false;
    }

    this.getDecisions();
  }

  loadSignature() {
    this._commonHttpService.getSingle({method: 'get', securityusersid: this.roleId.user.securityusersid}, 'admin/userprofile/listusersignature?filter')
      .subscribe(res => {
        this.signimage = res.usersignatureurl;
        this.checkIfSignPresent();
      });
  }

  private checkIfSignPresent() {
    this.isSignPresent = Boolean(this.signimage);
    this._dataStoreService.setData('isSignPresent', this.isSignPresent);
  }

  initializeDecisionForm() {
    this.decisionFormGroup = this.formBuilder.group({
      status: [''],
      reason: ['']
    });
  }


  getDecisions() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          eventcode: 'PTA',
          objectid: this.referralId
        }
      },
      'providerapplicant/getapplicationdecisions'
    ).subscribe(decisionsarray => {

      this.decisions = decisionsarray.data;
    },
      (error) => {
        this._alertService.error('Unable to get decisions, please try again.');
        console.log('get decisions Error', error);
        return false;
      }
    );
  }

  submitDecision() {
    console.log("Submitting decision");
    this._commonHttpService.create(
          {
            where: {
              eventcode: 'PTA',
              objectid: this.referralId,
              fromroleid: this.roleId.role.name,
              remarks: this.decisionFormGroup.value.reason, //.get('reason'),
              status: this.decisionFormGroup.value.status, //.get('status')
              isfinalapproval: this.isFinalDecisoin
            },
            method: 'post'
          },
          'providerapplicant/submitdecision'
        ).subscribe(result => {
          this._alertService.success('Decision Submitted successfully!');
          this.decisions.length = 0;
          this.getDecisions();
        },
          (error) => {
            this._alertService.error('Unable to submit decision, please try again.');
            console.log('submit decision Error', error);
            return false;
          }
        );
    }

    // Assignment logic follows

  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
  }

  getRoutingUser() {
    //this.getServicereqid = modal.provider_referral_id;
    //  this.getServicereqid = "R201800200374";
    // console.log(modal.provider_referral_id);
    // this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PTA' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  selectPerson(row) {
    this.selectedPerson = row;
    //console.log(this.selectedPerson);
  }

  assignUser() {
    // Doing the update of Status and assigning at the same time
    // In the same flow, first 'Submit' then 'Assign' will do them both
    // TODO: SIMAR - Put validations that status value has been selected before submitting
    console.log(this.isFinalDecisoin);

    if (this.selectedPerson) {
      this._commonHttpService
        .getPagedArrayList(
          new PaginationRequest({
            where: {
              eventcode: 'PTA',
              objectid: this.referralId,
              fromroleid: this.roleId.role.name,
              tosecurityusersid: this.selectedPerson.userid,
              toroleid: this.selectedPerson.userrole,
              remarks: this.decisionFormGroup.value.reason, //.get('reason'),
              status: this.decisionFormGroup.value.status, //.get('status')
              isfinalapproval: this.isFinalDecisoin
              // isreviewrequest: ,
            },
            method: 'post'
          }),
          'providerapplicant/submitdecision'
          //'Providerreferral/routereferralda'
        )
        .subscribe(result => {
          console.log("Inside subscribe", result);
          this._alertService.success('Application assigned successfully!');
          //  this.getAssignCase(1, false);
          this.closePopup();


          //Reset the decision table and get all new results
          this.decisions.length = 0;
          this.getDecisions();
        });
    } else {
      this._alertService.warn('Please select a person');
    }
  }

  closePopup() {
    (<any>$('#intake-caseassign')).modal('hide');
    (<any>$('#assign-preintake')).modal('hide');
    (<any>$('#reopen-intake')).modal('hide');
  }

  uploadAttachment() {
    console.log(this.signimage);
    const blob = this.dataURItoBlob(this.signimage);
    const finalImage = new File([blob], 'image.png');
    this._uploadService
      .upload({
        url:
          AppConfig.baseUrl +
          '/' +
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment
            .UploadAttachmentUrl +
          '?access_token=' +
          this.roleId.id +
          '&srno=userprofile',
        headers: new HttpHeaders()
          .set('access_token', this.roleId.id)
          .set('srno', 'userprofile')
          .set('ctype', 'file'),
        filesKey: ['file'],
        files: finalImage,
        process: true
      })
      .subscribe(
        response => {
          if (response && response.data && response.data.s3bucketpathname) {
            this._commonHttpService.create({
              usersignatureurl: response.data.s3bucketpathname
            }, 'admin/userprofile/updateusersignature').subscribe(res => {
              if (res.count) {
                this._alertService.success('Signature added successfully.');
                this.loadSignature();
              }
            });
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  dataURItoBlob(dataURI) {
    const binary = atob(dataURI.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
        type: 'image/png'
    });
}

}
