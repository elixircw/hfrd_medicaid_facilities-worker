import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPrivateReferralComponent } from './new-private-referral.component';

describe('NewPrivateReferralComponent', () => {
  let component: NewPrivateReferralComponent;
  let fixture: ComponentFixture<NewPrivateReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPrivateReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPrivateReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
