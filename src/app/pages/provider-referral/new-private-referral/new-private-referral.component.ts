import { AfterContentInit, AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Rx';

import { ControlUtils } from '../../../@core/common/control-utils';
import { ObjectUtils } from '../../../@core/common/initializer';
import { AppUser } from '../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../@core/entities/common.entities';
import { DataStoreService, GenericService } from '../../../@core/services';
import { AlertService } from '../../../@core/services/alert.service';
import { AuthService } from '../../../@core/services/auth.service';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { RoutingUser } from '../../cjams-dashboard/_entities/dashBoard-datamodel';
import { NewUrlConfig, ReferralUrlConfig } from '../provider-referral-url.config';
import { ProviderReferral, ReferralDecision, Profile } from './_entities/newreferralModel';
import { NewReferralConstants } from './new-referral.constants';

@Component({
  selector: 'new-private-referral',
  templateUrl: './new-private-referral.component.html',
  styleUrls: ['./new-private-referral.component.scss']
})

export class NewPrivateReferralComponent implements OnInit {

  // Our Stuff
  draftReferralintake: any;
  providerReferralIntake = new ProviderReferral();

  referralDecision$ = new Subject<ReferralDecision>();
  referralDecisionModel: ReferralDecision;
  referralInformation$: Observable<ProviderReferral[]>;

  referralNarrative$ = new Subject<string>();

  currentAgency: String;
  intakeAgencies:any;
  //legacy
  //This is the main form group at the base level that captures all te info for provider referral
  departmentActionIntakeFormGroup: FormGroup;
  generalResourceFormGroup: FormGroup;


  //intakeSourceList$: Observable<DropdownModel[]>;
  intakeCommunication$: Observable<DropdownModel[]>;
  intakeCommunication: any;
  intakeAgencies$: Observable<DropdownModel[]>;

  intakeId: string;
  
  draftId: string;
  btnDraft: boolean;

  intakeNumber: string;
  intakeNumberNarrative: string;
  roleId: AppUser;
  role: string;
  isDjs = false;

  constructor(
      private _router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private _authService: AuthService,
      private _alertService: AlertService,
      private _commonHttpService: CommonHttpService,
      private speechRecognizer: SpeechRecognizerService,
      private _dataStoreService: DataStoreService
  ) {
      this.draftId = route.snapshot.params['id'];
  }

//   subscribeToDecision() {
//       this.referralDecision$.subscribe((decision) => {
//           this.referralDecisionModel = decision;
//           this.providerReferralIntake.referral_decision = decision.status;
//           this.providerReferralIntake.comment = decision.reason;
//       });
//   }

//   subscribeToNarrative() {
//       this.referralNarrative$.subscribe((qwe) => {
//           this.providerReferralIntake.narrative = qwe;

//       });
//   }

//   setReferralStatus() {
//       const decision = this.providerReferralIntake.referral_decision;
//       if (decision == 'Approved') {
//           this.providerReferralIntake.referral_status = 'Approved';
//           this.providerReferralIntake.referral_decision = 'Approved';
//       } else if (decision == 'Rejected') {
//           this.providerReferralIntake.referral_status = 'Rejected';
//           this.providerReferralIntake.referral_decision = 'Rejected';
//       } else if (decision == 'Incomplete') {
//           this.providerReferralIntake.referral_status = 'Incomplete';
//           this.providerReferralIntake.referral_decision = 'Pending';
//       } else if (decision == 'Closed') {
//           this.providerReferralIntake.referral_status = 'Closed';
//           this.providerReferralIntake.referral_decision = 'Rejected';
//       } else if (decision == 'Submitted') {
//           this.providerReferralIntake.referral_status = 'Submitted';
//           this.providerReferralIntake.referral_decision = 'Pending';
//       }
//   }

//   submitReferral() {
//       this.setReferralStatus();
//       const approvedReferral = {
//           "provider_referral_id": this.intakeNumber,
//           "referral_status": this.providerReferralIntake.referral_status,
//           "referral_decision": this.providerReferralIntake.referral_decision,
//           "comment": this.providerReferralIntake.comment
//       }

//       this._commonHttpService.update('', approvedReferral, 'providerreferral/approveproviderreferral').subscribe(
//           (response) => {
//               this._alertService.success('Referral Submitted!');
//           },
//           (error) => {
//               this._alertService.error('Unable to approve, please try again.');
//               console.log('Referral approval error', error);
//               return false;
//           }
//       );
//   }


  ngOnInit() {

      this.buildFormGroup();
    //   this.subscribeToDecision();
    //   this.subscribeToNarrative();
      this.loadDroddowns();
      this.roleId = this._authService.getCurrentUser();

      if (this.roleId.user.userprofile.teamtypekey === 'DJS') {
          this.role = "DJS";
      }
      else{
          this.role = "OLM";
      }

      this.roleId = this._authService.getCurrentUser();
      if (this.roleId.user.userprofile.teamtypekey === 'DJS') {
          this.isDjs = true;
      }

      // We might need to persist the agency info, so that when a person comes back to an existing referral
      // they know which agency person captured it. Could get this information a part of the get request
      // based on the security if of the person who captured the referral.
      this.currentAgency = this.roleId.user.userprofile.teamtypekey;

      if (!this.draftId) {
          this.loadDefaults();
      } else {
          this.intakeNumber = this.draftId;
      }

 
      //this.otherAgencyControlName = this.departmentActionIntakeFormGroup.get('otheragency');
      //this.otherAgencyControlName.disable();

      // this.communicationInputSubject$.subscribe((commFileds) => {
      //     this.communicationFields = commFileds;
      // });

      // this.contactList$.subscribe((res) => (this.recordings = res));

  }


  ngAfterViewInit() {
      if (this.draftId) {
          this.populateIntake();
      }
  }


  buildFormGroup() {
      this.departmentActionIntakeFormGroup = this.formBuilder.group(
          {
              Source: [''],
              InputSource: ['', Validators.required],
              RecivedDate: [new Date(), [Validators.required, Validators.minLength(1)]],
              CreatedDate: [new Date()],
              Author: [''],
              IntakeNumber: [''],
              Agency: ['', Validators.required],
              Purpose: ['', Validators.required],
              IntakeService: [''],
              //otheragency: ['', Validators.maxLength(50)],
              //isOtherAgency: false
          },
          { validator: this.dateFormat }
      );
      this.generalResourceFormGroup = this.formBuilder.group({
          helpDescription: ['']
      });
  }

  dateFormat(group: FormGroup) {
      if (group.controls.RecivedDate.value !== '' && group.controls.RecivedDate.value !== null) {
          if (group.controls.RecivedDate.value > new Date()) {
              return { futureDate: true };
          }
          return null;
      }
  }

  private loadDroddowns() {
      const source = forkJoin([
          //this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.CommunicationUrl),
          this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.IntakeServiceRequestInputTypeUrl),
          this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.IntakeAgencies)
      ])
          .map((result) => {
              return {
                  communicationList: result[0].map(
                      (res) =>
                          new DropdownModel({
                              text: res.description,
                              value: res.intakeservreqinputtypeid
                          })
                  ),
                  agenciesList: result[1].map(
                      (res) =>
                          new DropdownModel({
                              text: res.teamtypekey,
                              value: res.teamtypekey,
                              additionalProperty: res.ismanualrouting
                          })
                  )
              };
          })
          .share();

      //this.intakeSourceList$ = source.pluck('sourceList');
      this.intakeCommunication$ = source.pluck('communicationList');
      this.intakeCommunication$.subscribe((data) => {
          this.intakeCommunication = data;
      });

      this.intakeAgencies$ = source.pluck('agenciesList');
      this.intakeAgencies$.subscribe((data) => {
          this.intakeAgencies = data;
          this.loadAgancyAsPerUser(data);

      });
  }



  // USE THIS TO MAP THE AGENCY AS PER OLM/DJS WORKER
  loadAgancyAsPerUser(data) {
      // console.log('user role:', this.roleId.user.userprofile.teamtypekey);

      data.map((element) => {
          if (element.value === this.roleId.user.userprofile.teamtypekey) {
              //this.departmentActionIntakeFormGroup.get('Agency').disable();
              this.departmentActionIntakeFormGroup.patchValue({
                  // Agency: element.value

              });
              //this.listPurpose({ text: '', value: element.value + '~' + element.additionalProperty });
          }
      });
  }


  private loadDefaults() {

      this._commonHttpService.getArrayList({}, ReferralUrlConfig.EndPoint.Referral.getNextReferralNumberUrl).subscribe((result) => {
          this.departmentActionIntakeFormGroup.patchValue({
              IntakeNumber: result['nextNumber']
          });
          this.intakeNumberNarrative = result['nextNumber'];
          this.intakeNumber = result['nextNumber'];

          this._dataStoreService.setData(NewReferralConstants.Intake.intakenumber, result['nextNumber']);
      });

      this._authService.currentUser.subscribe((userInfo) => {
          if (userInfo && userInfo.user) {
              this.departmentActionIntakeFormGroup.patchValue({
                  Author: userInfo.user.userprofile.displayname ? userInfo.user.userprofile.displayname : '',
              //Agency: userInfo.user.agencyid ?  userInfo.user.agencyid : ''
              });
          }
      });
  }


  setReferralInfo(referral) {
      console.log(referral);
      this.draftReferralintake = referral;
      this.providerReferralIntake = referral[0];

      console.log(this.providerReferralIntake);

  }


  approveReferral() {
      const approvedReferral = {
          "provider_referral_id": this.intakeNumber
      }
      this._commonHttpService.update('', approvedReferral, 'providerreferral/approveproviderreferral').subscribe(
          (response) => {
              this._alertService.success('Referral Approved!');
          },
          (error) => {
              this._alertService.error('Unable to approve, please try again.');
              console.log('Referral approval error', error);
              return false;
          }
      );
  }


  private populateIntake() {
      const roleDls = this._authService.getCurrentUser();
      this.route.params.subscribe((item) => {
          this.intakeId = item['id'];
          if (this.intakeId) {
              this._commonHttpService
                  .create(
                      {
                          page: 1,
                          limit: 10,
                          where: {
                              referralnumber: this.intakeId
                          }
                      },
                      ReferralUrlConfig.EndPoint.Referral.getProviderReferralUrl
                  )
                  .subscribe(
                      (response) => {
                          console.log("SIMAR populateIntake");
                          this.referralInformation$ = response.data;

                          console.log(this.referralInformation$);
                          this.providerReferralIntake = this.referralInformation$[0];
                          console.log(this.providerReferralIntake);

                          this.intakeNumber = response.data[0].provider_referral_id;
                          this.departmentActionIntakeFormGroup.value.IntakeNumber = this.intakeNumber;
                          console.log(this.intakeNumber);
                          this.patchReferralForm(response);

                      },
                      (error) => {
                          this._alertService.error('Unable to fetch saved intake details.');
                          this.loadDefaults();
                      }
                  );
          } else {
              this.loadDefaults();
          }
      });
  }

  private patchReferralForm(response) {
      // alert(response.data[0].agency);
      //const recDate = new Date(this.general.RecivedDate);
      //this.general.RecivedDate = recDate.toUTCString();

      this.departmentActionIntakeFormGroup.patchValue({
          IntakeNumber: this.intakeNumber,
          Author: response.data[0].author,
          Agency: response.data[0].agency,
          InputSource: response.data[0].provider_referral_med
      });
      // if (this.departmentActionIntakeFormGroup.value.isOtherAgency) {
      //     this.otherAgencyControlName.enable();
      // }
  }
}