import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewPrivateReferralComponent } from './new-private-referral.component';

const routes: Routes = [
  {
      path: '',
      component: NewPrivateReferralComponent,
      // canActivate: [RoleGuard],
      children: [
          // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
          // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
          // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
          // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
      ]
  },
  {
      path: ':id',
      component: NewPrivateReferralComponent,
      children: [
          // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
          // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
          // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
          // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
      ]
      // canActivate: [RoleGuard]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewPrivateReferralRoutingModule { }
