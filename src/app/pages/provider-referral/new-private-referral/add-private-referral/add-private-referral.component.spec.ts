import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPrivateReferralComponent } from './add-private-referral.component';

describe('AddPrivateReferralComponent', () => {
  let component: AddPrivateReferralComponent;
  let fixture: ComponentFixture<AddPrivateReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPrivateReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPrivateReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
