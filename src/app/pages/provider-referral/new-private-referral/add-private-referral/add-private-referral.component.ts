import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';
import { ProviderReferral } from '../_entities/newreferralModel';

import { ValidationService } from '../../../../@core/services/validation.service';
 import { ContactTypeAdd, IntakeContactRole, IntakeContactRoleType, ProgressNoteRoleType, IntakeFileAttachement } from '../_entities/newintakeModel';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { DropdownModel } from '../../../../@core/entities/common.entities';
@Component({
  selector: 'add-private-referral',
  templateUrl: './add-private-referral.component.html',
  styleUrls: ['./add-private-referral.component.scss']
})
export class AddPrivateReferralComponent implements OnInit {
  referralForm: FormGroup;
  programsTypes$: Observable<DropdownModel[]>;
  countyList$:Observable<DropdownModel[]>;
  stateDropDownItems$: Observable<DropdownModel>;
  referral: ProviderReferral;
  programs = [];
  programTypes = [];
  programType: string;
  @Input()
  referralId;
  isPublic = false;
  // @Output()
  // addReferralEventEmitter = new EventEmitter();
  referralDetails=[];



  constructor(private formBuilder: FormBuilder,private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
     }
  ngOnInit() {
    this.formIntilizer();
    this.loadDropDown("Private");
    this.loadReferralDetails();

  }
  formIntilizer() {
    this.referralForm = this.formBuilder.group({
      provider_referral_id: [''],
      provider_referral_nm: [''],
      provider_referral_last_nm: [''],
      provider_referral_first_nm: [''],
      
      provider_referral_program: [''],
      provider_program_type: [''],
      provider_program_name: [''],
      
      adr_email_tx: [''],
      adr_home_phone_tx: [''],
      adr_cell_phone_tx: [''],
      adr_fax_tx: [''],
      adr_street_nm: [''],
      adr_street_no: [null],
      adr_city_nm: [''],
      adr_state_cd: [''],
      adr_county_cd: [''],
      adr_country_tx: [''],
      adr_zip5_no: [null],
      is_son: [''],
      is_taxid: [''],
      is_rfp: [''],
      parent_entity: [''],
      parent_entity_taxid: [null],
      corporation_entity: [''],
      corporation_entity_taxid: [null],
      provider_referral_med: [''],
      referral_status: [''],
      referral_decision: [''],
      comment: [''],
      state: ['', Validators.required],

      narrative: [''],
      provider_catergory:['']
    });
      }
      
      private loadDropDown(selectedProviderType:string) {
        const source = forkJoin([
          this._commonHttpService.getArrayList(
            {
              method: 'post',
              nolimit: true,
              providertype:selectedProviderType
            },
            'providerreferral/listprogramnames'
            ),
           this._commonHttpService.getArrayList(
             {
                 method: 'get',
                 nolimit: true
             },
             'States' + '?filter'
         ),
         this._commonHttpService.getArrayList('get','providerreferral/listcountycodes'),
        ])
          .map((result) => {
            return {
              programsTypes: result[0].map(
                (res) =>
                  new DropdownModel({
                    text: res.programtype,
                    value: res.programnames
                  })
              ),
              stateList: result[1].map(
                (res) =>
                    new DropdownModel({
                        text: res.statename,
                        value: res.stateabbr
                    })
            ),
            countyListitems: result[2].map(
              (res) =>
                  new DropdownModel({
                      text: res.value_tx,
                      value: res.picklist_value_cd
                  })
          ),
    
            };
          })
          .share();
    
        this.programsTypes$ = source.pluck('programsTypes');
        this.stateDropDownItems$ = source.pluck('stateList');
        this.countyList$ = source.pluck('countyListitems');
    
      }
    
      selectedProgramNames(event: any) {
        let target = event.source.selected._element.nativeElement;
        this.programType = target.innerText.trim();
        this.programs = event.value;
      }
    
      addProvider() {
        this.referral = this.referralForm.value;
        this.referral.provider_referral_program = this.programType;
        this.referral.provider_referral_id = this.referralId;
        console.log(this.referral.referral_status);
        this.saveReferralDraft();
        (<any>$('#add-referral')).modal('hide');
        (<any>$('#intake-findprovider')).modal('hide');
      }
      
      onOptionChange(event: any){
        this.isPublic= event.value == 'Public' ? false :true;
        
        // Set the dropdown values based on the provider type selected
        this.loadDropDown(event.value);
      }
      saveReferralDraft() {
        if(this.referral.referral_status && this.referral.referral_status!==''){
            this._commonHttpService.update('', this.referral, 'providerreferral/updateproviderreferral').subscribe(
              (response) => {
                  this._alertService.success('Referral Updated successfully!');
                  this.loadReferralDetails();
              },
              (error) => {
                  this._alertService.error('Unable to save referral, please try again.');
                  console.log('Save Referral Error', error);
                  return false;
              }
          );
        } else {
          this.referral.referral_status = "Draft";
          this.referral.referral_decision = "Pending";
          this._commonHttpService.create(this.referral, 'providerreferral/addproviderreferral').subscribe(
            (response) => {
              this._alertService.success('Referral saved successfully!');
              this.loadReferralDetails();
            },
            (error) => {
              this._alertService.error('Unable to save referral, please try again.');
              console.log('Save Referral Error', error);
              return false;
            }
          );
        }
        (<any>$('#add-notes')).modal('hide')
        
      }
    
      loadReferralDetails() {
        this._commonHttpService
          .create(
            {
              page: 1,
              limit: 10,
              where: {
                referralnumber: this.referralId
              }
            },
            'providerreferral/getproviderreferral'
          )
          .subscribe(
            (response) => {
              if (response.data[0]) {
                //const intakeModel = response.data[0];
                this.referralDetails=response.data;
                console.log(this.referralDetails,'this.referralDetails');
                //this.addReferralEventEmitter.emit(response.data);
              }
            },
            (error) => {
              this._alertService.error('Unable to fetch saved intake details.');
            }
          );
      }
      // setReferralInfo(referralInfo) {
      //   console.log(JSON.stringify(referralInfo));
      //   referralInfo.isSON = (referralInfo.isSON) ? 'Yes' : 'No';
      //   referralInfo.isRFP = (referralInfo.isRFP) ? 'Yes' : 'No';
      //   referralInfo.isFEIN = (referralInfo.isFEIN) ? 'Yes' : 'No';
      //   this.referralInformation$ = referralInfo;
      //   //this.addedProviders.push(referralInfo);
      //   // this.emitReferralInformation.emit(referralInfo);
      // }
    }
    
    
