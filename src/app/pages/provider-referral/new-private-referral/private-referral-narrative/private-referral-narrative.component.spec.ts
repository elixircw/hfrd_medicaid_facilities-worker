import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateReferralNarrativeComponent } from './private-referral-narrative.component';

describe('PrivateReferralNarrativeComponent', () => {
  let component: PrivateReferralNarrativeComponent;
  let fixture: ComponentFixture<PrivateReferralNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateReferralNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateReferralNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
