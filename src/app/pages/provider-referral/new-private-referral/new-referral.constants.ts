export class NewReferralConstants {
    public static Intake = {
        PersonsInvolved: {
            Health: {
                Health: 'health',
                Physician: 'health_physician',
                PhysicianFromIntake: 'intake_health_physician',
                Insurance: 'health_insurance'
            },
            Work: {
                Work: 'work',
                Employer: 'employer',
                CareerGoals: 'career_goals'
            }
        },
        timeleft: 'timeleft',
        ispreintake: 'ispreintake',
        agency: 'agency',
        intakenumber: 'intakenumber',
        selectedPurpose: 'selectedPurpose',
        preIntakeDisposition: 'preIntakeDisposition',
        createdCases: 'createdCases',
        disposition: 'disposition',
        addedPersons: 'addedPersons',
        evalFields: 'evalFields',
        intakeappointment: 'intakeappointment',
        preIntakeSupDicision: 'preIntakeSupDicision',
        communications: 'communications',
        generatedDocuments: 'generatedDocument',
        courtDetails: 'courtDetails',
        petitionDetails: 'petitionDetails'
    };
}
