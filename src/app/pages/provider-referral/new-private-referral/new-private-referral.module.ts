import 'trumbowyg/dist/trumbowyg.min.js';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';
import { PopoverModule } from 'ngx-popover';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { TrumbowygNgxModule } from 'trumbowyg-ngx';
// tslint:disable-next-line:max-line-length
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckbox,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule
} from '@angular/material';
import { MatTooltipModule, MatChipsModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SortTableModule } from '../../../shared/modules/sortable-table/sortable-table.module';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { QuillModule } from 'ngx-quill';

import { NewPrivateReferralRoutingModule } from './new-private-referral-routing.module';
import { NewPrivateReferralComponent } from './new-private-referral.component';
import { PrivateReferralNarrativeComponent } from './private-referral-narrative/private-referral-narrative.component';
import { PrivateReferralDecisionComponent } from './private-referral-decision/private-referral-decision.component';
import { AddPrivateReferralComponent } from './add-private-referral/add-private-referral.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SignaturePadModule } from 'angular2-signaturepad';
import { SignatureFieldComponent } from './private-referral-decision/signature-field/signature-field.component';

@NgModule({
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatChipsModule,
    MatIconModule,    
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TimepickerModule,
    ControlMessagesModule,
    SharedDirectivesModule,
    SharedPipesModule,
    NgSelectModule,
    ImageCropperModule,
    SortTableModule,
    MatAutocompleteModule,
    NgxPaginationModule,
    SignaturePadModule,
    NgxMaskModule.forRoot(),
    TrumbowygNgxModule.withConfig({
        svgPath: '../../../../assets/images/icons.svg'
    }),
    A2Edatetimepicker,
    NgxMaskModule.forRoot(),
    PopoverModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    NewPrivateReferralRoutingModule
  ],
  declarations: [
     NewPrivateReferralComponent,
     PrivateReferralNarrativeComponent, 
     PrivateReferralDecisionComponent, 
     AddPrivateReferralComponent,
     SignatureFieldComponent],
  exports: [NewPrivateReferralComponent],
  providers: [SpeechRecognitionService, SpeechRecognizerService, NgxfUploaderService]

})
export class NewPrivateReferralModule { }
