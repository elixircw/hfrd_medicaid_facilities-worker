import { AppConstants } from '../../../@core/common/constants';

export const ReferralTabs = [
    {
        id: 'inq-basic-info',
        title: 'Inquiry Basic Information',
        name: 'Inquiry Basic Information',
        role: [AppConstants.ROLES.PROVIDER, AppConstants.ROLES.PROVIDER_STAFF_ADMIN],
        route: 'inq-basic-info',
        active: false,
        securityKey: 'inq-basic-info-screen'
    },
    {
        id: 'inq-add-info',
        title: 'Home Information',
        name: 'Home Information',
        role: [],
        route: 'inq-add-info',
        active: false,
        securityKey: 'inq-add-info-screen'
    },
    {
        id: 'household-members',
        title: 'Household Members',
        name: 'Household Members',
        role: [],
        route: 'household-members',
        active: false,
        securityKey: 'household-info-screen'
    },
    {
        id: 'narrative',
        title: 'Narrative',
        name: 'Narrative',
        role: [],
        route: 'narrative',
        active: false,
        securityKey: 'narrative-screen'
    },
    {
        id: 'contact-notes',
        title: 'Contact Notes',
        name: 'Contact Notes',
        role: [],
        route: 'contact-notes',
        active: false,
        securityKey: 'contact-notes-screen'
    },
    {
        id: 'attachment',
        title: 'Documents',
        name: 'Documents',
        role: [],
        route: 'attachment',
        active: false,
        securityKey: 'attachment-screen'
    },
    {
        id: 'meeting-info',
        title: 'Information Meeting',
        name: 'Information Meeting',
        role: [],
        route: 'meeting-info',
        active: false,
        securityKey: 'meeting-info-screen'
    },
    {
        id: 'review',
        title: 'Review',
        name: 'Review',
        role: [],
        route: 'review',
        active: false,
        securityKey: 'review-screen'
    }
];



