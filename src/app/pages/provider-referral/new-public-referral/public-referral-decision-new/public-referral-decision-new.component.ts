import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApplicationDecision } from '../../_entities/newApplicantModel';
import { AuthService, CommonHttpService, AlertService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { RoutingUser } from '../../_entities/existingreferralModel';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'public-referral-decision-new',
  templateUrl: './public-referral-decision-new.component.html',
  styleUrls: ['./public-referral-decision-new.component.scss']
})

export class PublicReferralDecisionNewComponent implements OnInit {

  decisionFormGroup: FormGroup;
  currentDecision: ApplicationDecision;
  roleId: AppUser;
  decisions = [];

  
  applicantNumber: string;
  eventcode = 'PTA';

  // Assigning / Routing
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  selectedPerson: any;
  //isFinalDecisoin: boolean = false;
  isFinalDecisoin: boolean;
  
  isSupervisor: boolean;
  isGroup = false;

  //statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService, 
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute) { 
      this.applicantNumber = route.snapshot.params['id'];
      //this.applicantNumber = 'A201800300025';
    }

  ngOnInit() {
    this.initializeDecisionForm();

    this.roleId = this._authService.getCurrentUser();
    //console.log(this.roleId);
    this.statusDropdownItems = [
      { "text": "Reject", "value": "Rejected" },
      { "text": "Approve", "value": "Approved" },
      { "text": "Return", "value": "Incomplete" },
    ];
    //this.roleId.user.userprofile.teamtypekey;

    if (this.roleId.role.name == 'Executive Director') {
      this.isFinalDecisoin = true;
    } else {
      this.isFinalDecisoin = false;
    }

    this.getDecisions();
  }

  initializeDecisionForm() {
    this.decisionFormGroup = this.formBuilder.group({
      status: [''],
      reason: ['']
    });
  }
  

  getDecisions() {
      this._commonHttpService.create(
        {
          method:'post',
          where: 
          { eventcode: 'PTA',
            objectid :this.applicantNumber
         }        
        },
        'providerapplicant/gettierdecisions'
        //ApplicantUrlConfig.EndPoint.Applicant.getappointmentdetails
    ).subscribe(decisionsarray => {
      
      this.decisions = decisionsarray.data;
      console.log("SIMAR DECISIONS")
      console.log(JSON.stringify(this.decisions));
    },
        (error) => {
          this._alertService.error('Unable to get decisions, please try again.');
          console.log('get decisions Error', error);
          return false;
        }
      );
  }


  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
}

  getRoutingUser() {
    //this.getServicereqid = modal.provider_referral_id;
  //  this.getServicereqid = "R201800200374";
    // console.log(modal.provider_referral_id);
    // this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PTA' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  selectPerson(row) {
    this.selectedPerson = row;
    //console.log(this.selectedPerson);
  }


  assignUser() {
    // Doing the update of Status and assigning at the same time
    // In the same flow, first 'Submit' then 'Assign' will do them both
    // TODO: SIMAR - Put validations that status value has been selected before submitting
    console.log("In assignUser");
    console.log(this.isFinalDecisoin);
    
    if (this.selectedPerson) {
      this._commonHttpService
        .getPagedArrayList(
          new PaginationRequest({
            where: {
              eventcode: 'PTA',
              objectid: this.applicantNumber,
              fromroleid: this.roleId.role.name,
              tosecurityusersid: this.selectedPerson.userid,
              toroleid: this.selectedPerson.userrole,
              remarks: this.decisionFormGroup.value.reason, //.get('reason'),
              status: this.decisionFormGroup.value.status, //.get('status')
              isfinalapproval: this.isFinalDecisoin
              // isreviewrequest: ,
            },
            method: 'post'
          }),
          'providerapplicant/submitdecision'
          //'Providerreferral/routereferralda'
        )
        .subscribe(result => {
          console.log("Inside subscribe", result);
          this._alertService.success('Application assigned successfully!');
        //  this.getAssignCase(1, false);
          this.closePopup();


          //Reset the decision table and get all new results
          this.decisions.length = 0;
          this.getDecisions();
        });
    } else {
      this._alertService.warn('Please select a person');
    }
  }

  closePopup() {
    (<any>$('#intake-caseassign')).modal('hide');
    (<any>$('#assign-preintake')).modal('hide');
    (<any>$('#reopen-intake')).modal('hide');
  }

}

