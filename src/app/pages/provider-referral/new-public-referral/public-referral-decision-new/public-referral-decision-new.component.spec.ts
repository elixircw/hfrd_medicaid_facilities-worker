import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralDecisionNewComponent } from './public-referral-decision-new.component';

describe('PublicReferralDecisionNewComponent', () => {
  let component: PublicReferralDecisionNewComponent;
  let fixture: ComponentFixture<PublicReferralDecisionNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralDecisionNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralDecisionNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
