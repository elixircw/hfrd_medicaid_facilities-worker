import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralDecisionComponent } from './public-referral-decision.component';

describe('PublicReferralDecisionComponent', () => {
  let component: PublicReferralDecisionComponent;
  let fixture: ComponentFixture<PublicReferralDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
