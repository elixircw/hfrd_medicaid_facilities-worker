import { HttpHeaders } from '@angular/common/http';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxfUploaderService } from 'ngxf-uploader';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { CheckboxModel, DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { ValidationService, DataStoreService, AuthService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';

//import { AddressDetails, InvolvedPerson, Provider } from '../_entities/newintakeModel';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { ReferralDecision } from '../../new-referral/_entities/newreferralModel';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'public-referral-decision',
  templateUrl: './public-referral-decision.component.html',
  styleUrls: ['./public-referral-decision.component.scss']
})
export class PublicReferralDecisionComponent implements OnInit {
  selectedPerson: any;
  eventcode: string;
  getUsersList: any[];
  statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  dispositionFormGroup: FormGroup;
  referralDecision: ReferralDecision;
  @Input()
  referralDecision$ = new Subject<ReferralDecision>();
  // @Input() isReadOnly: boolean;

  referralId: string;
  user: AppUser;

  constructor(private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _router: Router) {
    this.referralId = route.parent.snapshot.params['id'];
  }



  ngOnInit() {
    this.dispositionForm();
    this.user = this._authService.getCurrentUser();
    //@DP- Hardcoded coding should be removed 
    if (this.user.role.name === 'LDSS_RECRUITER_TRAINER') {
      // Dropdowns specifically for QA worker
      this.statusDropdownItems = [
        // { 'text': 'Accept', 'value': 'Submitted' }
        // Worker can directly accept inquiry without submitting for approval
        { 'text': 'Accept', 'value': 'accepted' }
      ];
    } else { //Dropdowns for Supervisor
      this.statusDropdownItems = [
        { 'text': 'Accept', 'value': 'accepted' },
        { 'text': 'Screen Out', 'value': 'rejected' },

      ];
    }
  }

  dispositionForm() {
    this.dispositionFormGroup = this.formBuilder.group({
      status: [''],
      reason: ['']
    });
  }

  getDecionData() {
    const decision = this.dispositionFormGroup.get('status').value;

    const payload = {}
    payload['referral_status'] = decision;
    payload['referral_decision'] = decision;
    payload['referral_id'] = this.referralId;

    return payload;

  }

  acceptInquiry() {

    const decision = this.dispositionFormGroup.get('status').value;
    const payload = this.getDecionData();

    this._commonHttpService.patch(
      '',
      payload,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        if (decision === 'accepted') {
          (<any>$('#inquiry-route')).modal('show');
          this.getRoutingUser('PRASS');
        } else {
          this._alertService.success('Inquiry Accepted successfully!');
        }
      },
      (error) => {
        this._alertService.error('Unable to accept referral');
      }

    );


  }

  /* approvReferral() {
    const data = this.getDecionData();
    this._commonHttpService.create(
      data,
      'publicproviderreferral/approvepublicproviderreferral')
      .subscribe(
        (response) => {
          (<any>$('#inquiry-route')).modal('hide');
        },
        (error) => {
          (<any>$('#inquiry-route')).modal('hide');
          this._alertService.error('')
        }
      );
  } */


  /**
   * If approval and routing needed
   */
  setReferralStatus() {
    const decision = this.dispositionFormGroup.get('status').value; //this.providerReferralIntake.referral_decision;
    // if (decision == 'Approved') {
    //     this.providerReferralIntake.referral_status = 'Approved';
    //     this.providerReferralIntake.referral_decision = 'Approved';
    // } else if (decision == 'Rejected') {
    //     this.providerReferralIntake.referral_status = 'Rejected';
    //     this.providerReferralIntake.referral_decision = 'Rejected';
    // } else if (decision == 'Incomplete') {
    //     this.providerReferralIntake.referral_status = 'Incomplete';
    //     this.providerReferralIntake.referral_decision = 'Pending';
    // } else if (decision == 'Closed') {
    //     this.providerReferralIntake.referral_status = 'Closed';
    //     this.providerReferralIntake.referral_decision = 'Rejected';
    // } else if (decision == 'Submitted') {
    //     this.providerReferralIntake.referral_status = 'Submitted';
    //     this.providerReferralIntake.referral_decision = 'Pending';
    // }
  }
  getRoutingUser(appevent) {
    if (this.user.role.name !== 'LDSS_SUPERVISOR') {
      this.eventcode = appevent;
      this.getUsersList = [];
      this._commonHttpService
        .getPagedArrayList(
          new PaginationRequest({
            where: { appevent: appevent },
            method: 'post'
          }),
          'Intakedastagings/getroutingusers'
        )
        .subscribe(result => {
          this.getUsersList = result.data;
          this.getUsersList = this.getUsersList.filter(
            users => users.userid !== this._authService.getCurrentUser().user.securityusersid
          );
        });
    } else {
      this.submitReferral();
    }
  }
  submitReferral() {
    if (this.user.role.name === 'LDSS_SUPERVISOR') {
      const decision = this.dispositionFormGroup.get('status').value;

      var payload = {}

      // Do the setting up as before
      payload['referral_status'] = decision;
      payload['referral_decision'] = decision;
      payload['referral_id'] = this.referralId;
      this._commonHttpService.patch(
        '',
        payload,
        'publicproviderreferral'
      ).subscribe(
        (response) => {
          this._alertService.success('Referral Submitted successfully!');
        },
        (error) => {
          this._alertService.error('Unable to submit referral');
        }

      );

      if (decision === 'accepted') {
        this.approvReferral();
      }
    }
    this.assignNewUser();

  }

  approvReferral() {
    const decision = this.dispositionFormGroup.get('status').value;
    const payload = this.getDecionData();
    payload['tosecurityusersid'] = this.selectedPerson.userid;
    payload['comments'] = this.dispositionFormGroup.get('reason').value;
    this._commonHttpService.create(
      payload,
      'publicproviderreferral/approvepublicproviderreferral')
      .subscribe(
        (response) => {
          this._alertService.success('Inquiry Accepted successfully!');
          (<any>$('#inquiry-route')).modal('hide');
          (<any>$('#inquiry-success')).modal('show');
        },
        (error) => {
          this._alertService.error('Unable to submit referral');
        }
      );

  }
  selectPerson(row) {
    this.selectedPerson = row;
  }

  assignNewUser() {
    console.log(this.user.role.name);
    var payload = {
      eventcode: 'PIRTS',
      tosecurityusersid: this.user.role.name === 'LDSS_SUPERVISOR' ? this._authService.getCurrentUser().user.securityusersid : this.selectedPerson.userid,
      objectid: this.user.role.name === 'LDSS_SUPERVISOR' ? this.referralId.replace('R', 'A') : this.referralId,
      typeofobj: 'referral',
      approvalstatustypekey: this.user.role.name === 'LDSS_SUPERVISOR' ? this.dispositionFormGroup.get('status').value : ''
    }
    this._commonHttpService.create(
      payload,
      'publicproviderapplicant/publicproviderapplicantrouting'
    ).subscribe(
      (response) => {
        this._alertService.success('Ownership assigned successfully!');
        (<any>$('#intake-caseassignnew')).modal('hide');
      },
      (error) => {
        this._alertService.error('Unable to save ownership');
      });
  }

  redirectInquiry() {
    this._router.navigate(['/pages/provider-referral/existing-referral']);
  }
  

}