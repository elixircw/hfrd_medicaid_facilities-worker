import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonHttpService, AuthService, AlertService } from '../../../@core/services';
import { ReferralUrlConfig } from '../provider-referral-url.config';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataStoreService, GenericService } from '../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../@core/entities/common.entities';
import { ReferralTabs } from './new-public-referral.config';
declare var $:any;


@Component({
  selector: 'new-public-referral',
  templateUrl: './new-public-referral.component.html',
  styleUrls: ['./new-public-referral.component.scss']
})
export class NewPublicReferralComponent implements OnInit {
  currentDate = new Date();
  referralId: string;
  publicReferralForm: FormGroup;
  referralStatus: string;
  isReadOnly: boolean = false;
  communication_mediums = [];
  maxDate = new Date();
  countyList$: Observable<DropdownModel[]>;
  tabs = ReferralTabs;



  constructor(private route: ActivatedRoute,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private _alertService: AlertService,
    private router: Router,
  ) {
    this.referralId = route.snapshot.params['id'];
    this._dataStoreService.setData('REFERRAL_NUMBER', this.referralId);
  }

  ngOnInit() {
    this.initReferralForm();
    this.getReadOnlyStatus();
    this.loadDropdown();
    if (!this.referralId) {
      this.loadDefaultNewReferral();
    } else {
      this.getInformation();
    }

    this.communication_mediums = [
      { id: 1, type: 'Phone' },
      { id: 2, type: 'Mail' },
      { id: 3, type: 'E-mail' },
      { id: 4, type: 'Fax' },
      { id: 5, type: 'Face-to-Face' },

    ];
    this.refreshScrollingTabs();


  }
  private getReadOnlyStatus() {
    this._commonHttpService.getById(
      this.referralId,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        console.log("Rahul Referral response" + response);
        this.referralStatus = response.referral_status;

        console.log(this.referralStatus, this.isReadOnly);

        if (this.referralStatus == 'Approved' || 'Rejected') {
          this.isReadOnly = true;
          console.log("Rahul referral readOnly:" + this.isReadOnly);

        }
        if (this.referralStatus == 'Pending') {
          this.isReadOnly = false;
          console.log("Rahul: referral Pending readOnly:" + this.isReadOnly);

        }
      },
      (error) => {
        //this._alertService.error('Unable to get applicant program information, please try again.');
        console.log('get applicant program information Error', error);
        return false;
      }

    );
  }
  private initReferralForm() {
    this.publicReferralForm = this.formBuilder.group(
      {
        referralId: [''],
        author: [''],
        communication_medium: [''],
        date_of_contact: null,
        jurisdiction: ['']
      }
    );

  }

  private getInformation() {
    this._commonHttpService.getById(
      this.referralId,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        console.log(response);
        this.publicReferralForm.patchValue(response);
        //this._alertService.success("Information saved successfully!");
      },
      (error) => {
        //this._alertService.error("Unable to retrieve information");
      }

    );
  }

  private loadDefaultNewReferral() {
    this._commonHttpService.getArrayList(
      {}, ReferralUrlConfig.EndPoint.Referral.getNextReferralNumberUrl
    ).subscribe((result) => {

      this.publicReferralForm.patchValue({
        referralId: result['nextNumber']
      });

      this.referralId = result['nextNumber'];
      this._dataStoreService.setData('referralId', result['nextNumber']);
      console.log("first time", this._dataStoreService.getData('referralId'))


      // Make a record in the db with the new referral number
      this._commonHttpService.create(
        {
          referral_id: this.referralId,
          referral_status: 'Pending'
        },
        'publicproviderreferral/'
      ).subscribe(
        (response) => {
          this.router.navigate(['/pages/provider-referral/new-public-referral', this.referralId]);
        },
        (error) => {
          this._alertService.error("Unable to create new referral, Please try again");
        }

      );


    });

    this._authService.currentUser.subscribe((userInfo) => {
      if (userInfo && userInfo.user) {
        this.publicReferralForm.patchValue({
          author: userInfo.user.userprofile.displayname ? userInfo.user.userprofile.displayname : '',
          //Agency: userInfo.user.agencyid ?  userInfo.user.agencyid : ''
        });
      }
    });
  }


  private saveJurisdictionInfo(event) {
    var payload = {}
    payload['referral_id'] = this.referralId;
    payload['jurisdiction'] = this.publicReferralForm.controls.jurisdiction.value;    //event.source.selected._element.nativeElement.innerText.trim();

    this._commonHttpService.patch(
      this.referralId,
      payload,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        //this._alertService.success("Information saved successfully!");
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }

    );
  }

  private saveGeneralInfo(fieldType, fieldValue) {
    console.log("Saving now");
    console.log(fieldValue);

    console.log(fieldType, fieldValue);
    var payload = {}

    payload['referral_id'] = this.referralId;
    payload[fieldType] = fieldValue;

    this._commonHttpService.patch(
      this.referralId,
      payload,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        //this._alertService.success("Information saved successfully!");
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }

    );
  }
  loadDropdown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        'admin/county' + '?filter'
      )
    ]).map(res => {
      return {
        countyList: res[0].map(
          item =>
            new DropdownModel({
              text: item.countyname,
              value: item.countyid
            })
        )
      };
    });
    this.countyList$ = source.pluck('countyList');
  }

  refreshScrollingTabs() {
    const __this = this;
    this.getCurrentTabDetails();
    const tabsLiContent = this.tabs.map(function (tab) {
      return '<li role="presentation" title="' + tab.title + '" data-trigger="hover" class="custom-li" routerLinkActive="active"></li>';
    });
    // this.publicApplicantTabs.forEach(tab => tab.active = false);
    const tabsPostProcessors = this.tabs.map(function (tab) {
      return function ($li, $a) {
        $a.attr('href', '');
        if (tab.active) {
          $a.attr('class', 'active');
        }
        $a.click(function () {
          __this.tabs.forEach(filteredTab => {
            filteredTab.active = false;
          });
          tab.active = true;
          __this.router.navigate([tab.route], { relativeTo: __this.route });
        });
      };
    });
    (<any>$('#public-applicant-tabs')).html('');
    (<any>$('#public-applicant-tabs')).scrollingTabs({
      tabs: __this.tabs,
      propPaneId: 'path', // optional - pass in default value for demo purposes
      propTitle: 'name',
      disableScrollArrowsOnFullyScrolled: true,
      tabsLiContent: tabsLiContent,
      tabsPostProcessors: tabsPostProcessors,
      scrollToActiveTab: true,
      tabClickHandler: function (e) {
        $(__this).addClass('active');
        $(__this).parent().addClass('active');
      }
    });


  }

  private getCurrentTabDetails() {
    this.tabs.forEach(tab => tab.active = false);
    const urlSegments = this.router.url.split('/');
    let tab: any;
    this.tabs.forEach(tabObj => {
      if (urlSegments.includes(tabObj.route)) {
        tab = tabObj;
        tab.active = true;
        return;
      }
    });
    return tab;
  }


}
