import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralInfoComponent } from './public-referral-info.component';

describe('PublicReferralInfoComponent', () => {
  let component: PublicReferralInfoComponent;
  let fixture: ComponentFixture<PublicReferralInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
