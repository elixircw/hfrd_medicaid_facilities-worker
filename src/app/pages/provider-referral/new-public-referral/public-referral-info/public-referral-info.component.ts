import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, ValidationService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { DataStoreService, GenericService } from '../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';

@Component({
  selector: 'public-referral-info',
  templateUrl: './public-referral-info.component.html',
  styleUrls: ['./public-referral-info.component.scss']
})
export class PublicReferralInfoComponent implements OnInit {

  publicReferralInfoForm: FormGroup;
  referralId: string;
  addressTypes = [];
  addresses = [];
  addressForm: FormGroup;
  deleteAdrObj: any;
  isZipValid: boolean;
  @Input() isReadOnly: boolean;
  isCoApplicantAddress: boolean = false;
  //isPaymentCheckd: boolean = false;
  programsTypes$: Observable<DropdownModel[]>;
  programs = [];
  providerReferralProgram: string;
  poolLocation = [];
  prefixTypes=[];
  suffixTypes=[];


  constructor(private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService) {
    this.referralId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.initPublicReferralInfoForm();
    this.loadDropDown("Public");
    this.getInformation();
    this.initializeAddressForm();
    this.getApplicantAddresses();
    this.poolLocation = ["In ground", "Above ground"];
    this.prefixTypes=["Mr", "Mrs", "Ms", "Dr", "Prof", "Sister", "Atty"];
    this.suffixTypes=["Jr","Sr"];
  }

  private initPublicReferralInfoForm() {
    this.publicReferralInfoForm = this.formBuilder.group({
      organization_first_nm: [''],
      organization_tax_id: null,
      individual_applicant_first_nm: [''],
      individual_applicant_last_nm: [''],
      individual_applicant_ssn: null,
      individual_applicant_dob: null,
      co_applicant_first_nm: [''],
      co_applicant_last_nm: [''],
      co_applicant_ssn: null,
      co_applicant_dob: null,
      individual_applicant_prefix: [''],
      individual_applicant_middle_nm: [''],
      individual_applicant_suffix: [''],
      co_applicant_prefix: [''],
      co_applicant_suffix: [''],
      co_applicant_middle_nm: [''],
      // Home info    
      home_info_children_no: [''],
      home_info_bedroom_no: [''],
      is_home_water: null,
      is_home_swimming_pool: null,
      home_info_pool_location: [''],
      home_is_other_agency: null,
      home_info_agency_nm: [''],
      is_child_care_provider: null,
      home_info_child_care_details: [''],
      //Individual Applicant Extra fields
      individual_applicant_hm_phone: [''],
      individual_applicant_cell_nm: [''],
      individual_applicant_email: [''],
      individual_applicant_employer_nm: [''],
      individual_applicant_phone_nm: [''],
      individual_applicant_us_citizen: null,
      //Co-applicant extra fields
      co_applicant_hm_phone: [''],
      co_applicant_cell_nm: [''],
      co_applicant_email: [''],
      co_applicant_employer_nm: [''],
      co_applicant_phone_nm: [''],
      co_applicant_us_citizen: null,
    });
  }

  getInformation() {
    this._commonHttpService.getById(
      this.referralId,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        this.publicReferralInfoForm.patchValue(response);
        this._dataStoreService.setData('publicProviderInfo', response);
      },
      (error) => {
        this._alertService.error("Unable to retrieve provider inquiry information");
      }

    );
  }

  setReferralId() {
    this.referralId = this._dataStoreService.getData('referralId');
    console.log("second time", this._dataStoreService.getData('referralId'))
  }

  private saveInformation() {
    //this.setReferralId();

    this.publicReferralInfoForm.value.prgram = this.providerReferralProgram;
    var payload = {}
    payload = this.publicReferralInfoForm.value;
    payload['referral_id'] = this.referralId;
    this._commonHttpService.patch(
      this.referralId,
      payload,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }

    );
  }


  private initializeAddressForm() {
    this.addressForm = this.formBuilder.group({
      applicant_id: [''],
      provider_applicant_profile_id: [''],
      object_id: [''],
      address_id: [''],
      adr_street_no: [''],
      adr_street_nm: [''],
      adr_city_nm: [''],
      adr_state_cd: [''],
      adr_zip5_no: [null],
      address_type: ['']
    });
    //  this.addressTypes = ["Individual Applicant Address","Co-Applicant Address","Location Address","Payment Address"];
    this.addressTypes = ["Individual Applicant Address", "Co-Applicant Address"];

  }

  zipValidation(event: any) {
    this.isZipValid = false;
    const pattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
    if (!pattern.test(event.target.value)) {
      this.isZipValid = false;
    }
    else {
      this.isZipValid = true;
    }
  }

  private getApplicantAddresses() {
    this._commonHttpService.create(
      {
        "where": {
          applicant_id: this.referralId
        }
      },
      'publicproviderapplicant/getpublicapplicantaddresses'
    ).subscribe(response => {
      //if( response.data[0].addresses != null) {
      this.addresses = response.data[0].addresses;
      //}
      console.log(this.addresses);
    },
      (error) => {
        this._alertService.error('Unable to get address information, please try again.');
        console.log('get address information Error', error);
        return false;
      }
    );
  }


  saveAddress(addressFormInfo) {
    addressFormInfo.applicant_id = this.referralId;
    if (addressFormInfo.address_id && !this.isCoApplicantAddress) {
      this._commonHttpService.create(addressFormInfo, 'providerapplicantportal/updateapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Updated successfully!');
          this.getApplicantAddresses();
          this.addresses.unshift(addressFormInfo);

        },
        (error) => {
          this._alertService.error('Unable to Update address, please try again.');
          return false;
        }
      );
    }
    else {
      this._commonHttpService.create(addressFormInfo, 'providerapplicantportal/addapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Created successfully!');
          this.getApplicantAddresses();
          this.addresses.unshift(addressFormInfo);
        },
        (error) => {
          this._alertService.error('Unable to Create address, please try again.');
          console.log('Save address Error', error);
          return false;
        }
      );
    }
    (<any>$('#public-applicant-address')).modal('hide');
  }

  isCheckedEvent(event) {
    if (event.checked) {
      this.isCoApplicantAddress = true;
      this.getAddressByType("Individual Applicant Address");
    }
    else {
      this.isCoApplicantAddress = false;
      this.initializeAddressForm();
      this.addressForm.patchValue({
        address_type: "Co-Applicant Address"
      });
    }
  }

  getAddressByType(event) {
    let addressTypeVal;
    let addresGet;
    if (typeof event == "string") {
      addresGet = "Individual Applicant Address"
      addressTypeVal = "Co-Applicant Address";
    }
    else {
      addresGet = event.value;
      addressTypeVal = event.value;
      if (event.value == "Co-Applicant Address")
        this.isCoApplicantAddress = true;
      else
        this.isCoApplicantAddress = false;
    }
    this._commonHttpService.create(
      {
        "where": {
          applicant_id: this.referralId,
          addressType: addresGet
        }
      },
      'publicproviderapplicant/getpublicapplicantByaddressesType'
    ).subscribe(response => {
      //if( response.data[0].addresses != null) {
      //this.addresses = response.data[0].addresses;
      //}
      this.initializeAddressForm();
      let addressFormInfo = response.data;
      // addressFormInfo.applicant_id = this.applicantId;
      if (addressFormInfo.length > 0) {
        this.addressForm.patchValue({
          object_id: this.referralId,
          address_id: addressFormInfo[0].address_id,
          adr_street_no: addressFormInfo[0].adr_street_no,
          adr_street_nm: addressFormInfo[0].adr_street_nm,
          adr_city_nm: addressFormInfo[0].adr_city_nm,
          adr_state_cd: addressFormInfo[0].adr_state_cd,
          adr_zip5_no: addressFormInfo[0].adr_zip5_no,
        });
      }
      this.addressForm.patchValue({
        address_type: addressTypeVal
      })
    },
      (error) => {
        this._alertService.error('Unable to get address information, please try again.');
        console.log('get address information Error', error);
        return false;
      }
    );
  }

  EditAddress(editObj) {

    var addressFormInfo = this.addressForm.value;

    addressFormInfo.applicant_id = this.referralId;
    console.log(editObj)
    this.addressForm.patchValue({
      object_id: editObj.object_id,
      address_id: editObj.address_id,
      adr_street_no: editObj.adr_street_no,
      adr_street_nm: editObj.adr_street_nm,
      adr_city_nm: editObj.adr_city_nm,
      adr_state_cd: editObj.adr_state_cd,
      adr_zip5_no: editObj.adr_zip5_no,
      address_type: editObj.address_type
    });
    (<any>$('#public-applicant-address')).modal('show');
  }

  DeleteAddress(Obj) {
    this.deleteAdrObj = Obj;
    (<any>$('#delet-address')).modal('show');
  }

  CancelDelete() {
    (<any>$('#delet-address')).modal('hide');
  }

  DeleteAddressConfirm() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          address_id: this.deleteAdrObj.address_id,
          // address_type:this.deleteAdrObj.address_type
        }
      },
      'providerapplicantportal/deleteapplicantaddress')
      .subscribe(response => {
        if (response) {
          this._alertService.success("Address Deleted successfully");
          this.getApplicantAddresses();
          this.CancelDelete()
        }
      });
  }

  private loadDropDown(selectedProviderType: string) {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'post',
          nolimit: true,
          providertype: selectedProviderType
        },
        'providerreferral/listprogramnames'
      ),
    ])
      .map((result) => {
        return {
          programsTypes: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.programtype,
                value: res.programnames
              })
          ),
        };
      })
      .share();

    this.programsTypes$ = source.pluck('programsTypes');

  }
  selectedProgramNames(event) {
    //console.log('prgram',event);
    console.log('selected prgram', event.source.selected._element.nativeElement.innerText.trim());
    this.providerReferralProgram = event.source.selected._element.nativeElement.innerText.trim();

    let target = event.source.selected._element.nativeElement;
    //this.programType = target.innerText.trim();
    this.programs = event.value;
  }

}

