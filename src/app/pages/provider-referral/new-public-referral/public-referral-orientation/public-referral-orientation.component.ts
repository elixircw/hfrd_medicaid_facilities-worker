import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatTableDataSource } from '@angular/material';
import { DataStoreService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
export interface orientationListTable {
	trainingNo: string;
	orientationDate: string;
	orientationNarrative: string;
	OrientationStartTime: string;
	orientationEndTime: string;
	sessionType: string;
}
@Component({
	selector: 'public-referral-orientation',
	templateUrl: './public-referral-orientation.component.html',
	styleUrls: ['./public-referral-orientation.component.scss']
})
export class PublicReferralOrientationComponent implements OnInit {
	selection = new SelectionModel<orientationListTable>(true, []);
	dataSource: any;
	referralId: string;
	orientationList = [];
	assignedTrainingList = [];
	orientationArray = [];
	duration: number = 0;
	publicProviderInfo: any;
	attendee_first_nm: string;
	attendee_last_nm: string;
	houseHolds = [];

	displayedColumns: string[] = ['select', 'trainingNo', 'orientationDate', 'orientationNarrative', 'OrientationStartTime', 'orientationEndTime', 'sessionType'];

	constructor(private formBuilder: FormBuilder,
		private _commonHttpService: CommonHttpService,
		private _alertService: AlertService,
		private _router: Router, private route: ActivatedRoute,
		private _dataStoreService: DataStoreService
	) {
		this.referralId = route.parent.snapshot.params['id'];
	}

	ngOnInit() {
		this.getassignedOrientations();
		this.getHouseHolds().subscribe(response => {
			if (response && response.data) {
				this.houseHolds = response.data;
			}
		});
	}

	getHouseHolds() {

		const inputRequest = {
			intakenumber: this.referralId
		};
		return this._commonHttpService
			.getPagedArrayList(
				new PaginationRequest({
					page: 1,
					limit: 20,
					method: 'get',
					where: inputRequest
				}),
				'People/getpersondetailcw?filter'
			);


	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}

	getOrientationList() {
		this._commonHttpService.getArrayList(
			{
				method: 'get',
				where: { training_type: 'Information Meeting', object_id: this.referralId }
			},
			'providerorientationtraining?filter'
		).subscribe(response => {
			this.orientationList = response
			this.orientationList = this.orientationList.filter(
				x => (!this.assignedTrainingList.map(y => y.orientation_training_id).includes(x.orientation_training_id))
			);
			this.dataSource = new MatTableDataSource<orientationListTable>(this.orientationList);
		});
	}

	getApplicantInformation(): boolean {
		if (this.houseHolds.length === 0) {
			this._alertService.error('Applicant  is missing!');
			return false;
		}
		const applicant = this.houseHolds.find(person => {
			const applicantRole = person.roles.find(role => role.typedescription === 'Applicant');
			if (applicantRole) {
				return true;
			} else {
				return false;
			}
		});

		if(!applicant) {
			this._alertService.error('Applicant  is missing!');
			return false;
		}
		this.attendee_first_nm = applicant.firstname;
		this.attendee_last_nm = applicant.lastname;

		if (!this.attendee_first_nm || this.attendee_first_nm === '' || !this.attendee_last_nm || this.attendee_last_nm === '') {
			this._alertService.error('Applicant name is missing!')
			return false;
		} else {
			return true;
		}
	}

	assignOrientationList() {
		const numSelected = this.selection.selected.length;
		var payload = {};
		payload = this.selection.selected;
		if (this.getApplicantInformation()) {
			for (var i = 0; i < numSelected; i++) {
				payload[i]['object_id'] = this.referralId;
				payload[i]['phase'] = 'Inquiry';
				payload[i]['attendee_first_nm'] = this.attendee_first_nm;
				payload[i]['attendee_last_nm'] = this.attendee_last_nm;
			}
			this._commonHttpService.create(
				payload,
				'providerorientationtrainingattendance/'
			).subscribe(
				(response) => {
					this._alertService.success("Information Meeting assigned successfully!");
					this.getassignedOrientations();
					(<any>$('#assign-orientation')).modal('hide');
				},
				(error) => {
					this._alertService.error("Unable to assign Orientations");
				}
			);
		}
	}

	getassignedOrientations() {
		this._commonHttpService.create(
			{
				where: {
					object_id: this.referralId,
					training_type: 'Information Meeting'
				},
				method: 'post'
			},
			'providerorientationtrainingattendance/getassignedorientations'
		).subscribe(response => {
			this.assignedTrainingList = response;
			this.calculateDuration();
			this.getOrientationList();
		},
			(error) => {
				this._alertService.error('Unable to get assigned staffs, please try again.');
				return false;
			}
		);
	}

	calculateDuration() {
		for (var i = 0; i < this.assignedTrainingList.length; i++) {
			if (this.assignedTrainingList[i].is_attended) {
				var x = parseFloat(this.assignedTrainingList[i].duration);
				this.duration = this.duration + x;
			}
		}
	}

	patchIntentToAttend(session) {
		var isIntentAttend: boolean;
		if (session.is_intent_to_attend === false) {
			isIntentAttend = true;
		} else if (session.is_intent_to_attend === true) {
			isIntentAttend = false;
		};
		var payload = {}
		payload['is_intent_to_attend'] = isIntentAttend;
		this._commonHttpService.patch(
			session.orientation_training_attendance_id,
			payload,
			'providerorientationtrainingattendance'
		).subscribe(
			(response) => {
				this._alertService.success("Orientations assigned successfully!");
				this.getassignedOrientations();
			},
			(error) => {
				this._alertService.error("Unable to assign Orientations");
			});
	}
}
