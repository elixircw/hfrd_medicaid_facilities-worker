import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralOrientationComponent } from './public-referral-orientation.component';

describe('PublicReferralOrientationComponent', () => {
  let component: PublicReferralOrientationComponent;
  let fixture: ComponentFixture<PublicReferralOrientationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralOrientationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralOrientationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
