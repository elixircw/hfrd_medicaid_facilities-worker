import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralHouseholdmembersComponent } from './public-referral-householdmembers.component';

describe('PublicReferralHouseholdmembersComponent', () => {
  let component: PublicReferralHouseholdmembersComponent;
  let fixture: ComponentFixture<PublicReferralHouseholdmembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralHouseholdmembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralHouseholdmembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
