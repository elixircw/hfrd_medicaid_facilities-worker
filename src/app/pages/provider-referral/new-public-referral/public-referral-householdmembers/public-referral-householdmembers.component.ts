import { Component, Input, OnInit, OnChanges } from '@angular/core';
//import { ApplicantUrlConfig } from '../../provider-portal-temp-url.config'
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';



@Component({
  selector: 'public-referral-householdmembers',
  templateUrl: './public-referral-householdmembers.component.html',
  styleUrls: ['./public-referral-householdmembers.component.scss']
})
export class PublicReferralHouseholdmembersComponent implements OnInit {
 
  applicantNumber: string;
  householdForm: FormGroup;
  tableData = Array<Object>();
  deleteHouseholdMemberId: any;
  householdMemberId: string;
  isEditHouseHold: boolean;
  isOutOfState: boolean;
  isStateCheckComplete: boolean;
  isNationalCheckComplete: boolean;
  isChildAbuseMaltreatmentCheckComplete: boolean;
  isChildAbuseMaltreatmentOosComplete: boolean;
  suffixTypes=[];
  prefixTypes=[];

  @Input() isSubmitted: boolean;
  @Input() isReadOnly: boolean;

  isPrepositionChecked: boolean;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute) {
    this.applicantNumber = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.initializeHouseholdForm();
    this.isPrepositionChecked = false;
    this.getProviderHousehold();
    this.isEditHouseHold = false;
    this.isOutOfState = false;
    this.isStateCheckComplete = false;
    this.isNationalCheckComplete = false;
    this.isChildAbuseMaltreatmentCheckComplete = false;
    this.isChildAbuseMaltreatmentOosComplete = false;
    this.suffixTypes=["Jr","Sr"];
    this.prefixTypes=["Mr", "Mrs", "Ms", "Dr", "Prof", "Sister", "Atty"];
  }

  private initializeHouseholdForm() {
    this.isEditHouseHold=false;
    this.householdForm = this.formBuilder.group({
      object_id: this.applicantNumber,
      household_member_id: null,
      household_member_first_name: [''],
      household_member_middle_name: [''],
      household_member_last_name: [''],
      household_member_relation: [''],
      household_member_dob: null,
      household_member_ssn: [''],
      household_member_clearance_status: [''],
      household_member_suffix: [''],
      household_member_prefix: [''],
      bg_chk_state: null,
      bg_chk_state_date: null,
      bg_chk_national: null,
      bg_chk_national_date: null,
      bg_chk_child_abuse_maltreatment: null,
      bg_chk_child_abuse_maltreatment_date: null,
      bg_chk_oos: null,
      bg_chk_child_abuse_maltreatment_oos: [''],
      bg_chk_child_abuse_maltreatment_oos_date: null
    });
  }

  resetForm() {
    this.householdForm.reset();
  }

  createHouseholdPerson() {
    console.log(this.householdForm.value);
    this._commonHttpService.create(
      this.householdForm.value,
      'publicproviderapplicanthousehold'
    ).subscribe(
      (response) => {
        this.householdMemberId = response.household_member_id;
        this._alertService.success("Applicant household added successfully!");
      },
      (error) => {
        this._alertService.error("Unable to create household information, please try again.");
      }
    );
  }

  getProviderHousehold() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { object_id: this.applicantNumber },
        order: 'household_member_id desc'
      },
      'publicproviderapplicanthousehold?filter'
    ).subscribe(
      (response) => {
        this.tableData = response;
        console.log("TableData:" + JSON.stringify(this.tableData));
      },
      (error) => {
        this._alertService.error("Unable to retrieve Household member");
      }
    );
  }

  patchProviderHousehold() {
    console.log('this.householdMemberId',this.householdMemberId);
    console.log('this.householdForm.value',this.householdForm.value);
    this.householdForm.value.household_member_id=this.householdMemberId;
    this._commonHttpService.patch(
      this.householdMemberId,
      this.householdForm.value,
      'publicproviderapplicanthousehold'
    ).subscribe(
      (response) => {
        this.getProviderHousehold();
        this.resetForm();
        this._alertService.success("Household member updated successfully!");
      },
      (error) => {
        this._alertService.error("Unable to update Household member");
      }
    );
    (<any>$('#add-household')).modal('hide');
  }

  editHouseholdmember(person) {
    this.householdMemberId = person.household_member_id;
    (<any>$('#add-household')).modal('show');
    this.isEditHouseHold = true;
    this.householdForm.patchValue({
      object_id: this.applicantNumber,
      household_member_id: person.household_member_id,
      household_member_first_name: person.household_member_first_name,
      household_member_middle_name: person.household_member_middle_name,
      household_member_last_name: person.household_member_last_name,
      household_member_relation: person.household_member_relation,
      household_member_dob: person.household_member_dob,
      household_member_ssn: person.household_member_ssn,
      household_member_clearance_status: person.household_member_clearance_status,
      bg_chk_state: person.bg_chk_state,
      bg_chk_state_date: person.bg_chk_state_date,
      bg_chk_national: person.bg_chk_national,
      bg_chk_national_date: person.bg_chk_national_date,
      bg_chk_child_abuse_maltreatment: person.bg_chk_child_abuse_maltreatment,
      bg_chk_child_abuse_maltreatment_date: person.bg_chk_child_abuse_maltreatment_date,
      bg_chk_oos: person.bg_chk_oos,
      bg_chk_child_abuse_maltreatment_oos: person.bg_chk_child_abuse_maltreatment_oos,
      bg_chk_child_abuse_maltreatment_oos_date: person.bg_chk_child_abuse_maltreatment_oos_date
    });
  }

  deleteHouseholdmember(person) {
    this.deleteHouseholdMemberId = person.household_member_id;
    (<any>$('#delete-householdmember')).modal('show');
  }

  cancelDelete() {
    this.deleteHouseholdMemberId = null;
    (<any>$('#delete-householdmember')).modal('hide');
  }

  deleteHouseholdConfirm() {
    this._commonHttpService.remove(
      this.deleteHouseholdMemberId,
      {
        where : {household_member_id: this.deleteHouseholdMemberId}
      },
      'publicproviderapplicanthousehold'+'/'+this.deleteHouseholdMemberId
      ).subscribe(
        (response) => {
          this.getProviderHousehold();
          this._alertService.success("Household member deleted successfully!");
          (<any>$('#delete-householdmember')).modal('hide');
        },
        (error) => {
          this._alertService.error('Unable to delete Household member');
        }
      );
  }

  togglePreposition() {
    this.isPrepositionChecked = !this.isPrepositionChecked;
  }
  
  checkOutOfState(event) {
    console.log('Out of state', event.value);
    if (event.value = true) {
      this.isOutOfState = true;
    }
  }
}


