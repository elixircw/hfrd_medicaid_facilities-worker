import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { GenericService, SessionStorageService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
// import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { NewUrlConfig } from '../../provider-referral-url.config';
import { Attachment } from './_entities/attachment.data.models';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import { GeneratedDocuments } from './_entities/attachment.data.models';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../@core/services/auth.service';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';
const SCREENING_WORKER = 'SCRNW';
const SUPERVISOR = 'apcs';
const CLW = 'Court Liaison Worker';
const CASE_WORKER = 'Case Worker';
declare var $: any;
import * as ALL_DOCUMENTS from './_configurtions/documents.json';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
// import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { Router } from '@angular/router';
// import { DsdsService } from '../_services/dsds.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attachment',
    templateUrl: './attachment.component.html',
    styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit, AfterViewInit {
    petitions: any;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    daNumber: string;
    id: string;
    baseUrl: string;
    filteredAttachmentGrid: Attachment[] = [];
    userfilteredAttachmentGrid: Attachment[] = [];
    @ViewChild(EditAttachmentComponent) editAttach: EditAttachmentComponent;
    documentPropertiesId: any;
    documentId: any;
    generatedDocuments: GeneratedDocuments[] = [];
    allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;
    token: AppUser;
    config = { isGenerateUploadTabNeeded: false };
    downldSrcURL: any;
    isServiceCase = false;
    selectedDocument: any;
    isGenerateByVictim: boolean;
    isGenerateByComplaint: boolean;
    isGenerateByPetition: boolean;
    complaints = [];
    victims = [];
    generateDocForm: FormGroup;
    intakeNumber: string;
    involvedPerson = [];
    involvedUnkPerson = [];
    selectedPerson = '';
    selectedPersonName = '';
    tab_type= 'case';
    contactrecording: any[] = [];
    reasonForContact: any;
    constructor(
        private _dropDownService: CommonHttpService,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _service: GenericService<Attachment>,
        private _authService: AuthService,
        private _dataStoreService: DataStoreService,
        private storage: SessionStorageService,
        private _commonService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _router: Router,
        // private _dsdsService: DsdsService
    ) {
        this.id = route.snapshot.parent.parent.params['id'];
        console.log(route.snapshot.parent);
        this.daNumber = route.snapshot.parent.parent.parent.parent.params['daNumber'];
        this.baseUrl = AppConfig.baseUrl;
        if (route.snapshot.params['type']) {
            this.tab_type = route.snapshot.params['type'];
        }
    }

    ngOnInit() {
        // this.attachment();
        this.intakeNumber = this._dataStoreService.getData('da_intakenumber');
        // this.isServiceCase = this._dsdsService.isServiceCase();
        this.token = this._authService.getCurrentUser();
        // const caseType = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_TYPE);

        // this.getInvolvedPerson();
        this.loadAttachments();
        this.getReasonForContact();
    }
    loadAttachments() {
        /* if (this.isServiceCase) {
            this.serviceCaseAttachmentList();
        } else {
            this.attachment();
        } */
        this.loadAttachmentList();
        this.getContactRecordings();
        this.prepareConfig();
        if (this.config.isGenerateUploadTabNeeded) {
            this.loadGeneratedDocumentList(this.token, true);
            this._dataStoreService.currentStore.subscribe((store) => {
                if (store['caseSummary']) {
                    const actionSummary = store['caseSummary'];
                    const jsonData = actionSummary['intake_jsondata'];
                    this.generatedDocuments = jsonData['generatedDocuments'] ? jsonData['generatedDocuments'] : [];
                }
                this.loadGeneratedDocumentList(this.token, false);
            });
        }
        this.initGenerateDocForm();
    }
    addtoCase(event, fileDetails) {
        fileDetails.isSelected = event.checked;
        this.linkPersonDocuments(this.selectedPerson, event.checked, fileDetails.documentpropertiesid);
    }
    onPersonChecked(event, person) {
        this.selectedPerson = person.personid;
        this.selectedPersonName = person.firstname + ' ' + person.lastname;
        this.personattachment(this.selectedPerson);
    }
    uploadFileforPerson(uploadtype) {
        if (this.selectedPerson === '') {
            this._alertService.error('Please select a person');
        } else {
            const page_type = 'person';
            // for(var i= 0;i<this.involvedPerson.length;i++) {
            //     if(this.involvedPerson[i].personid == this.selectedPerson) {
            //         if(this.involvedPerson[i].isSelected == true){
            //             page_type='Client';
            //         }
            //         break;
            //     }
            // }
            this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment/' + uploadtype + '/' + page_type + '/' + this.selectedPerson]);
        }
    }
    // getInvolvedPerson() {
    //     let inputRequest: Object;
    //     if (this.isServiceCase) {
    //         inputRequest = {
    //             objectid: this.id,
    //             objecttypekey: 'servicecase'
    //         };
    //     } else {
    //         inputRequest = {
    //             intakeserviceid: this.id
    //         };
    //     }
    //     this._dropDownService
    //         .getSingle(
    //             new PaginationRequest({
    //                 page: 1,
    //                 limit: 20,
    //                 method: 'get',
    //                 where: inputRequest
    //             }),
    //             NewUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
    //         )
    //         .subscribe(data => {
    //             const dp_att_arr = [];
    //             if (data && data.data.length > 0) {
    //                 this.involvedPerson = data.data;
    //                 console.log('involved');
    //                 console.log(this.involvedPerson);
    //             }
    //         });
    // }
    ngAfterViewInit() {
        const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
        if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('i')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
            (<any>$('th a')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
        }
    }
    checkFileType(file: string, accept: string): boolean {
        if (accept && file) {
            const acceptedFilesArray = accept.split(',');
            return acceptedFilesArray.some(type => {
                const validType = type.trim();
                if (validType.charAt(0) === '.') {
                    return file.toLowerCase().endsWith(validType.toLowerCase());
                }
                return false;
            });
        }
        return true;
    }
    editAttachment(modal) {
        this.editAttach.editForm(modal);
        (<any>$('#edit-attachment')).modal('show');
    }
    confirmDelete(modal) {
        this.documentPropertiesId = modal.documentpropertiesid;
        this.documentId = modal.filename;
        (<any>$('#delete-attachment')).modal('show');
    }
    deleteAttachment() {
        const workEnv = config.workEnvironment;
        if (workEnv === 'state') {
            this._service.endpointUrl =
                NewUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
            const id = this.documentPropertiesId + '&' + this.documentId;
            this._service.remove(id).subscribe(
                result => {
                    (<any>$('#delete-attachment')).modal('hide');
                    this.loadAttachmentList();
                    this.getContactRecordings();
                    this._alertService.success('Attachment Deleted successfully!');
                },
                err => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        } else {
            this._service.endpointUrl =
                NewUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
            this._service.remove(this.documentPropertiesId).subscribe(
                result => {
                    (<any>$('#delete-attachment')).modal('hide');
                    this.loadAttachmentList();
                    this.getContactRecordings();
                    this._alertService.success('Attachment Deleted successfully!');
                },
                err => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }

    }

    downloadFile(s3bucketpathname) {
        const workEnv = config.workEnvironment;
        if (workEnv === 'state') {
            this.downldSrcURL = this.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            // 4200
            this.downldSrcURL = s3bucketpathname;
        }
        console.log('this.downloadSrc', this.downldSrcURL);
        window.open(this.downldSrcURL, '_blank');
    }
    linkPersonDocuments(personid, status, documentpropertiesid) {
        this._service
            .getSingle(
                {},
                NewUrlConfig.EndPoint.DSDSAction.Attachment.PersonAttachmentUpdate + '/' + this.id + '/' + personid + '/' + status + '/' + documentpropertiesid + '?data'
            )
            .subscribe((result: any) => {
                if (result.error === 0) {
                    this.loadAttachments();
                    this._alertService.success(result.message);
                } else {
                    this._alertService.success('error occured');
                }
            });
    }
    private personattachment(personId) {
        this.userfilteredAttachmentGrid = [];
        this._dropDownService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.DSDSAction.Attachment.PersonAttachmentGridUrl + '/' + this.id + '/' + personId + '?data'
            )
            .subscribe((result) => {
                result.map(item => {
                    item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                });
                this.userfilteredAttachmentGrid = result;
            });
    }
    private attachment() {
        this._dropDownService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/' + this.id + '?data'
            )
            .subscribe((result) => {
                result.map(item => {
                    item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                });
                this.filteredAttachmentGrid = result;
            });
    }

    private loadAttachmentList() {
        // const inputreq = {
        //     servicerequestid: this.isServiceCase ? null : this.id,
        //     servicecaseid: this.isServiceCase ? this.id : null,
        //     objecttypekey: this.isServiceCase ? 'Servicecase' : 'ServiceRequest',
        //     page: 1,
        //     limit: 10
        // };

        this._dropDownService
            .getPagedArrayList(
                new PaginationRequest({
                    nolimit: true,
                    order: 'originalfilename',
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.Intake.AttachmentGridUrl + '/' + this.id + '?data')
                .subscribe((result: any) => {
                    if (result) {
                        result.map(item => {
                            item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                        });
                        this.filteredAttachmentGrid = result;
                    }
                });
    }

    getContactRecordings() {
        const source = this._dropDownService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 100,
                    where: {},
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.DSDSAction.Recording.GetAllDaRecordingUrl + '/' + this.id + '?data'
            )
            .subscribe((result) => {
                this.contactrecording = result.data;
                this.setcontacttype();
            });
    }
    getReasonForContact() {
        this._dropDownService.getSingle({}, 'Progressnotereasontypes?filter={"nolimit":true}').map((itm) => {
            return itm;
        }).subscribe(data => {
            this.reasonForContact = data;
            this.setcontacttype();
        });
    }

    setcontacttype() {
        if (Array.isArray(this.reasonForContact) && Array.isArray(this.contactrecording)) {
            this.contactrecording.forEach(item => {
                const pr = item.progressnotereasontypekey.split(',');
                let description = '';
                pr.forEach((element, index) => {
                    const reason = this.reasonForContact.find(re => re.progressnotereasontypekey === element);
                    description = ((index === (pr.length - 1))) ? description + reason.typedescription : description + reason.typedescription + ', ';
                });
                item.progressnotereasontypedescription = description;
            });
        }
    }

    private humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        if (!bytes) {
            return '';
         }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    private loadGeneratedDocumentList(user: AppUser, loadAPI: boolean) {
        const isDJS = this._authService.isDJS();
        this.documentsListFromJSON(user);
        setTimeout(() => {
            if (isDJS && loadAPI) {
                this._service.getAllPaged({
                    where: {
                        roletypekey: this._authService.getCurrentUser().user.userprofile.teammemberassignment.teammember.roletypekey,
                        intakeserviceid: this.id
                    },
                    limit: 10,
                    nolimit: true,
                    page: 1
                },
                    NewUrlConfig.EndPoint.Intake.GenerateDocumentList).subscribe((res: any) => {
                        const generatedDocuments = res;
                        generatedDocuments.forEach((document: any) => {
                            document.isGenerated = document.generatedDateTime ? true : false;
                            document.fromAPI = true;
                        });
                        console.log(generatedDocuments);
                        this.generatedDocuments = [...generatedDocuments, ...this.generatedDocuments];
                    });
            }
        }, 1000);
    }

    private documentsListFromJSON(user: AppUser) {
        this.generatedDocuments = [];
        this.allDocuments.forEach(document => {
            if (document.access.indexOf(user.role.name) !== -1) {
                // const isExist = this.generatedDocuments.find(gDocument => document.id === gDocument.id);
                // if (!isExist) {
                this.generatedDocuments.push(document);
                // }
            }
        });
    }

    downloadDocument(document) {
        if (document.documentpath) {
            window.open(document.documentpath, '_blank');
        } else {
            this._dataStoreService.setData('documentsToDownload', [document.key]);
        }
    }

    downloadSelectedDocuments() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            this._dataStoreService.setData('documentsToDownload', selectedDocuments.map(document => document.key));

        }

    }

    getSelectedDocuments() {
        return this.generatedDocuments.filter(document => document.isSelected);
    }

    isFileSelected() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            return selectedDocuments.length > 0;
        }

        return false;
    }

    generateNewDocument(document: any) {
        if (document.fromAPI) {
            this.generateDocumentFromAPI(document);
        } else {
            document.isInProgress = true;
            setTimeout(() => {
                document.isGenerated = true;
                document.generatedBy = this.token.user.userprofile.displayname;
                document.generatedDateTime = new Date();
                document.isInProgress = false;
            }, 1000);
        }
    }

    initGenerateDocForm() {
        this.generateDocForm = this.formBuilder.group({
            complaint: [null],
            victim: [null],
            petition: [null]
        });
    }

    generateDocumentFromAPI(document: any) {
        this.selectedDocument = document;
        document.isInProgress = true;
        if (document.inputfields === 'intakeserviceid') {
            this.generateDocumnet(document);
        } else {
            this.initGenerateDocForm();
            this.isGenerateByVictim = false;
            const inputs = document.inputfields.split(',');
            if (inputs.includes('complaintid')) {
                this.generateDocForm.get('complaint').setValidators(Validators.required);
                this._commonService.getArrayList({
                    where: {
                        intakeserviceid: this.id
                    },
                    limit: 10,
                    nolimit: true,
                    page: 1,
                    method: 'get'
                },
                    NewUrlConfig.EndPoint.DSDSAction.Attachment.CompaintFromCase)
                    .subscribe((res: any) => {
                        this.complaints = res;
                    });
                this.isGenerateByComplaint = true;
            } else {
                this.isGenerateByComplaint = false;
                this.generateDocForm.get('complaint').clearValidators();
            }
            this.generateDocForm.get('complaint').updateValueAndValidity();

            if (inputs.includes('victimid')) {
                this.isGenerateByVictim = true;
                this.generateDocForm.get('victim').setValidators(Validators.required);
            } else {
                this.isGenerateByVictim = false;
                this.generateDocForm.get('victim').clearValidators();
            }
            this.generateDocForm.get('victim').updateValueAndValidity();

            if (inputs.includes('petitionid')) {
                this.isGenerateByPetition = true;
                this.generateDocForm.get('petition').setValidators(Validators.required);
                this._commonService.getArrayList({
                    page: 1,
                    limit: 10,
                    nolimit: true,
                    sortcolumn: 'intakenumber',
                    sortorder: 'asc',
                    where: {
                        intakenumber: this.intakeNumber
                    },
                    method: 'get'
                }, NewUrlConfig.EndPoint.Intake.getAddedPetitions).subscribe(
                    (response) => {
                        this.petitions = response;
                    });
            } else {
                this.isGenerateByPetition = false;
                this.generateDocForm.get('petition').clearValidators();
            }
            (<any>$('#generate-details')).modal('show');
            $('#generate-details').on('hidden.bs.modal', function () {
                document.isInProgress = false;
            });
        }
    }

    // loadVictims() {
    //     const complaintid = this.generateDocForm.get('complaint').value;
    //     this._commonService
    //         .getArrayList({
    //             where: {
    //                 intakeservicerequestevaluationid: complaintid
    //             },
    //             method: 'post'
    //         }, NewUrlConfig.EndPoint.Intake.getComplaintDetails).subscribe((response: any) => {
    //             const victims = [];
    //             response.data.forEach(complaint => {
    //                 complaint.victim.forEach(victim => {
    //                     victims.push(victim);
    //                 });
    //             });
    //             this.victims = Array.from(new Set(victims));
    //         });
    // }

    private generateDocumnet(document: any) {
        const documentKey = [];
        documentKey.push(document.documenttemplatekey);
        const generateDocDetails = this.generateDocForm.getRawValue();
        const complaintid = generateDocDetails.complaint ? [].concat(generateDocDetails.complaint) : [];
        const victimid = generateDocDetails.victim ? [].concat(generateDocDetails.victim) : [];
        const petitionid = generateDocDetails.petition ? [].concat(generateDocDetails.petition) : [];
        this._commonService.getPagedArrayList(new PaginationRequest({
            where: {
                intakenumber: this.intakeNumber,
                intakeserviceid: this.id,
                documenttemplatekey: documentKey,
                reportername: this.token.user.userprofile.displayname,
                reporteddate: new Date(),
                screenername: this.token.user.userprofile.displayname,
                isdraft: false,
                downloadtype: document.downloadtype,
                intakeservicerequestevaluationid: complaintid,
                victim: victimid,
                isheaderrequired: document.isheaderrequired,
                petition: petitionid
            },
            method: 'post'
        }), 'evaluationdocument/generateintakedocument')
            .subscribe(res => {
                if (res.data && res.data.length) {
                    //   this._store.setData(IntakeStoreConstants.generatedDocuments, this.generatedDocuments);
                    (<any>$('#generate-details')).modal('hide');
                    this.loadGeneratedDocumentList(this.token, true);
                }
            }, (err) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                document.isInProgress = false;
            });
    }

    generateSelectedDocuments() {
        this.generatedDocuments.forEach(document => {
            if (document.isSelected) {
                this.generateNewDocument(document);
            }

        });
    }

    prepareConfig() {
        const isDjs = this._authService.isDJS();
        if (isDjs) {
            this.config.isGenerateUploadTabNeeded = true;
        } else {
            this.config.isGenerateUploadTabNeeded = false;
        }
    }

    toggleTable(id) {
        (<any>$('#' + id)).collapse('toggle');
    }
}
