import { Component, Input, OnInit, NgModule} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { ValidationService } from '../../../../@core/services/validation.service';
import { ContactTypeAdd, IntakeContactRole, IntakeContactRoleType, ProgressNoteRoleType, IntakeFileAttachement } from '../../_entities/newintakeModel';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { DropdownModel } from '../../../../@core/entities/common.entities';

@Component({
  selector: 'public-referral-contact',
  templateUrl: './public-referral-contact.component.html',
  styleUrls: ['./public-referral-contact.component.scss']
})
export class PublicReferralContactComponent implements OnInit {
  noteForm: FormGroup;
  duration: string;
  contacts:any[]=[];
  timesArray:any[] = [];
  @Input() isReadOnly: boolean;

  minDate = new Date();
   @Input()
   contactListAdded$ = new Subject<ContactTypeAdd[]>();
   @Input()
   contactListOutput$ = new Subject<ContactTypeAdd[]>();
  updateAppend: FormGroup;
  
   phoneTypeDropdownItems$: Observable<DropdownModel[]>;

   @Input()
  contactListAdded: ContactTypeAdd[] = [];
  recordingDetail: ContactTypeAdd = new ContactTypeAdd();
  provider_applicant_id:string;
  contactTypes = [];
  contactPurpose = [];
  contactRole = [];




  constructor(private formBuilder: FormBuilder,private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
        this.provider_applicant_id = route.parent.snapshot.params['id'];
     }

  ngOnInit() {
      this.formIntilizer();
      this.loadDropDown();
      this.getContacts();
      this.timesArray = this.generateTimeList();
      this.contactTypes = [
        {id: 1, type: 'RCC facility'},
        {id: 2, type: 'Provider corporate office'},
        {id: 3, type: 'DHS Central'},
        {id: 3, type: 'LDSS office'},
        {id: 3, type: 'Other'}];

        this.contactPurpose = [
          {id: 1, type: 'Entrance Conference'},
          {id: 2, type: 'Exit Conference'},
          {id: 3, type: 'Foster Parent Visit'},
          {id: 4, type: 'Governance Board review'},
          {id: 5, type: 'Financial Review'},
          {id: 6, type: 'Basic life needs'},
          {id: 7, type: 'Restraints'},
          {id: 8, type: 'Complaints'},
          {id: 9, type: 'Litigation'},
          {id: 10, type: 'Overview of program '},
          {id: 11, type: 'Administrative'},
          {id: 12, type: 'Medical'},
          {id: 13, type: 'Health Inspections'},
          {id: 14, type: 'Fire Inspections'},
          {id: 15, type: 'Program operations'}];
         
          this.contactRole = [
            {id: 1, type: 'Principal'},
            {id: 2, type: 'Seargent'},
            {id: 3, type: 'Doctor'},
            {id: 4, type: 'Nurse'},
            {id: 5, type: 'Other'}];


  }

  formIntilizer() {
    this.noteForm = this.formBuilder.group(
        { provider_applicant_id:[''],
        communication_location: [''],
        type_of_contact: [''],
        purpose_of_contact: [''],
        narrative: [''],
        communication_date: [''],
        person_contacted_name: [''],
        title_of_person_contacted: [''],
        phone_of_contact: [''],
        appointment_time: [''],

        email_of_contact: ['']
           
        }
    );
      }



      private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM
    
        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
          const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
          const mm = (tt % 60); // getting minutes of the hour in 0-55 format
          if (is24hrs) {
            times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
          } else {
            times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
          }
          tt = tt + x;
        }
        return times;
      }

      private loadDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    filter: {}
                },
                //'Contactroletypes' + '?filter'

                'admin/personphonetype' + '?filter'
            )
        ])        
            .map((result) => {
                return {
                  recordingType: result[0],
                   
                     phonetype: result[0].map(
                        (res) =>
                            new DropdownModel({
                                 text: res.typedescription,
                                 value: res.typedescription
                             })
                     )
                };
            })
            .share();

         this.phoneTypeDropdownItems$ = source.pluck('phonetype');
        //this.phoneTypeDropdownItems$ = source.pluck('recordingType');

        
    }

      
      clearNotes() {
        this.noteForm.reset();
      }
    //   checkTimeValidation(group: FormGroup) {
    //     if (!group.controls.endtime.value || group.controls.endtime.value !== '') {
    //         if (group.controls.endtime.value < group.controls.starttime.value) {
    //             return { notValid: true };
    //         }
    //         return null;
    //     }
    // }

    contactSubtype(event) {
      if (event) {
          const contact = event.label;
        //   this.contactType = contact.trim();
        //   this.contactSubTypeList$ = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.ContactSubTypeUrl + '?prognotetypeid=' + event.value).map((result) => {
        //       return result;
        //   });
      }
  }

  
//   timeDuration() {
//       const start_date = moment(this.noteForm.value.starttime, 'HH:mm a');
//       const end_date = moment(this.noteForm.value.endtime, ' HH:mm a');
//       const duration = moment.duration(end_date.diff(start_date));
//       if (duration['_data'] && this.noteForm.value.starttime && this.noteForm.value.endtime) {
//           this.duration = duration['_data'].hours + ' Hr :' + duration['_data'].minutes + ' Min';
//       }
//   }

  saveNote(obj) {
    console.log(JSON.stringify(obj));
    (<any>$('#add-notes')).modal('hide');
    console.log(obj,"body of contact");

    obj.provider_applicant_id = this.provider_applicant_id;
    obj.communication_date = moment(obj.communication_date).format('YYYY/MM/DD') + ' ' + obj.appointment_time;

    this._commonHttpService.create(obj, "providerapplicant/insertcommunicationinfo").subscribe(
        (response) => {
             this.contacts.push(obj);
          this.getContacts();    
            this._alertService.success('contact saved successfully!');
        },
        (error) => {
          this._alertService.error('Unable to save contact, please try again.');
          return false;
        }
      );
      this.clearNotes();

 
}

getContacts() {

    this._commonHttpService.getArrayList(
      {
        
        method:'post',
        applicant_id:this.provider_applicant_id
      },
      'providerapplicant/getcommunicationinfo'
      
  ).subscribe(contactsarray => {
    
    this.contacts = contactsarray;
    //console.log(JSON.stringify(this.appointments));
  },
      (error) => {
        this._alertService.error('Unable to get contacts, please try again.');
        console.log('get contact Error', error);
        return false;
      }
    );
    (<any>$('#intake-contact')).modal('hide');
  }

}
