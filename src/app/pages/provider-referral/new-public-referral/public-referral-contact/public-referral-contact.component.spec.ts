import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralContactComponent } from './public-referral-contact.component';

describe('PublicReferralContactComponent', () => {
  let component: PublicReferralContactComponent;
  let fixture: ComponentFixture<PublicReferralContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
