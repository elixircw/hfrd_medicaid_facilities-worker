import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewPublicReferralComponent } from './new-public-referral.component';
import { PublicReferralBasicInfoComponent } from './public-referral-basic-info/public-referral-basic-info.component';
import { PublicReferralInfoComponent } from './public-referral-info/public-referral-info.component';
import { PublicReferralNarrativeComponent } from './public-referral-narrative/public-referral-narrative.component';
import { AppConstants } from '../../../@core/common/constants';
import { PublicReferralContactComponent } from './public-referral-contact/public-referral-contact.component';
import { PublicReferralOrientationComponent } from './public-referral-orientation/public-referral-orientation.component';
import { PublicReferralDecisionNewComponent } from './public-referral-decision-new/public-referral-decision-new.component';
import { PublicReferralAddDocumentComponent } from './public-referral-add-document/public-referral-add-document.component';
import { PublicReferralDecisionComponent } from './public-referral-decision/public-referral-decision.component';

const routes: Routes = [
    {
        path: '',
        component: NewPublicReferralComponent,
        // canActivate: [RoleGuard],
        children: [
        ]
    },
    {
        path: ':id',
        component: NewPublicReferralComponent,
        children: [
            {
                path: '',
                redirectTo: 'inq-basic-info',
                pathMatch: 'full',
              },
              { path: 'inq-basic-info', component: PublicReferralBasicInfoComponent },
              { path: 'inq-add-info', component: PublicReferralInfoComponent },
              { path: 'narrative', component: PublicReferralNarrativeComponent },
              {
                path: 'household-members',
                loadChildren: '../../shared-pages/involved-persons/involved-persons.module#InvolvedPersonsModule',
                data: { source: AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_REFERRAL }
              },
              { path: 'contact-notes', component: PublicReferralContactComponent },
              // { path: 'documents', component: PublicReferralAddDocumentComponent },
              { path: 'attachment', loadChildren: './attachment/provider-attachment.module#ProviderAttachmentModule'},
              { path: 'attachment/:type', loadChildren: './attachment/provider-attachment.module#ProviderAttachmentModule'},
              { path: 'meeting-info', component: PublicReferralOrientationComponent },
              { path: 'review', component: PublicReferralDecisionComponent },

        ]
        // canActivate: [RoleGuard]
    }
    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewPublicReferralRoutingModule {}
