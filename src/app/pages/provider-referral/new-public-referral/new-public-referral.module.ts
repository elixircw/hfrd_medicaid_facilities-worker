import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatStepperModule

} from '@angular/material';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { NewPublicReferralRoutingModule } from './new-public-referral-routing.module'
import { NewPublicReferralComponent } from './new-public-referral.component';
import { PublicReferralInfoComponent } from './public-referral-info/public-referral-info.component';
import { PublicReferralNarrativeComponent } from './public-referral-narrative/public-referral-narrative.component';
import { PublicReferralContactComponent } from './public-referral-contact/public-referral-contact.component';
import { PublicReferralHouseholdmembersComponent } from './public-referral-householdmembers/public-referral-householdmembers.component'
import { PublicApplicantAddCharacteristicsComponent } from '../../provider-applicant/new-public-applicant/public-applicant-add-characteristics/public-applicant-add-characteristics.component';
// import { PublicReferralAddDocumentComponent } from './public-referral-add-document/public-referral-add-document.component';
// import { AttachmentDetailComponent } from './public-referral-add-document/attachment-detail/attachment-detail.component';
// import { AttachmentUploadComponent } from './public-referral-add-document/attachment-upload/attachment-upload.component';
// import { AudioRecordComponent } from './public-referral-add-document/audio-record/audio-record.component';
// import { EditAttachmentComponent } from './public-referral-add-document/edit-attachment/edit-attachment.component';
// import { ImageRecordComponent } from './public-referral-add-document/image-record/image-record.component';
// import { VideoRecordComponent } from './public-referral-add-document/video-record/video-record.component';
import { PublicReferralDecisionNewComponent } from './public-referral-decision-new/public-referral-decision-new.component';
import { PublicReferralDecisionComponent } from './public-referral-decision/public-referral-decision.component';
import { PublicReferralBasicInfoComponent } from './public-referral-basic-info/public-referral-basic-info.component';
import { PublicReferralOrientationComponent } from './public-referral-orientation/public-referral-orientation.component';
import { IntakeConfigService } from '../../newintake/my-newintake/intake-config.service';

@NgModule({
  imports: [
    CommonModule, MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgSelectModule,
    NewPublicReferralRoutingModule,
    NgxPaginationModule,    
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatStepperModule

  ],
  declarations: [NewPublicReferralComponent, 
    PublicReferralInfoComponent, 
    PublicReferralNarrativeComponent, 
    PublicReferralContactComponent, 
    PublicReferralHouseholdmembersComponent, 
    // PublicReferralAddDocumentComponent, 
    // AttachmentDetailComponent, 
    // AttachmentUploadComponent, 
    // AudioRecordComponent, 
    // EditAttachmentComponent, 
    // ImageRecordComponent, 
    // VideoRecordComponent,
    PublicReferralDecisionNewComponent,
    PublicReferralDecisionComponent,
    PublicReferralBasicInfoComponent,
    PublicReferralOrientationComponent
  ],
   entryComponents: [],

  // providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService]

 

  providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService, IntakeConfigService]
})

export class NewPublicReferralModule { }
