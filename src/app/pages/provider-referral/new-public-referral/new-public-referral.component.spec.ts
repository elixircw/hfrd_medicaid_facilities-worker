import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPublicReferralComponent } from './new-public-referral.component';

describe('NewPublicReferralComponent', () => {
  let component: NewPublicReferralComponent;
  let fixture: ComponentFixture<NewPublicReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPublicReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPublicReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
