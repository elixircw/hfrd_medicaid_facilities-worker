import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralAddDocumentComponent } from './public-referral-add-document.component';

describe('PublicReferralAddDocumentComponent', () => {
  let component: PublicReferralAddDocumentComponent;
  let fixture: ComponentFixture<PublicReferralAddDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralAddDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralAddDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
