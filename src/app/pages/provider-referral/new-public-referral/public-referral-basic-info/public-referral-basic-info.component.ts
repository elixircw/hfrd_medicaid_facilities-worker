import { Component,Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, ValidationService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { DataStoreService, GenericService } from '../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';
@Component({
  selector: 'public-referral-basic-info',
  templateUrl: './public-referral-basic-info.component.html',
  styleUrls: ['./public-referral-basic-info.component.scss']
})
export class PublicReferralBasicInfoComponent implements OnInit {

  
  publicReferralInfoForm: FormGroup;
  referralId: string;
  addressTypes = [];
  addresses = [];
  addressForm: FormGroup;
  // savedProgramDetailsForm: FormGroup;
  deleteAdrObj:any;
  @Input() isReadOnly: boolean;
  isPaymentAddress:boolean=false;
  isPaymentCheckd:boolean=false;
  programsTypes$: Observable<DropdownModel[]>;
  programs = [];
  providerReferralProgram: string;
  poolLocation=[];
  ageGroup=[];
  inquirySource=[];


  constructor(private _commonHttpService: CommonHttpService, private _alertService: AlertService, private formBuilder: FormBuilder, private route: ActivatedRoute,   private _dataStoreService: DataStoreService) {
    this.referralId = route.parent.snapshot.params['id'];
   }

  ngOnInit() {
    this.initPublicReferralInfoForm();
    this.loadDropDown("Public");
    this.getInformation(); 
    this.poolLocation=["In ground","Above ground"];
    this.ageGroup=["0-2","3-5","6-10","11-14","15-20","21+"];
    this.inquirySource=["Internet","Radio Station","Television","Flyer","Newspaper","Bus advertisement",
    "Bus stop","Public Resource Parent","Community Event","MVA","Billboard","In home mailer","Adoptuskids","Long time interest","Other"];
    
  }

  private initPublicReferralInfoForm(){
    this.publicReferralInfoForm= this.formBuilder.group({
      prgram:[null],
      provider_program_type:[null],

      age_group:[null],

      inquiry_source:[null],
      inquiry_source_details:[null]
    });
  }

   getInformation() {
    this._commonHttpService.getById(
      this.referralId, 
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        console.log('RESPONSE ----------------  ', JSON.stringify(response));
        // this.publicReferralInfoForm.patchValue(response);  
        
        this.publicReferralInfoForm.patchValue({
          prgram: response.prgram,
          provider_program_type: response.provider_program_type,
          inquiry_source: response.inquiry_source,
          inquiry_source_details: response.inquiry_source_details, 
          age_group: response.age_group,
        });
        this.selectedProgramNames();  
      },
      (error) => {
        //this._alertService.error("Unable to retrieve information");
      }

    );
  }

  setReferralId() {
    this.referralId = this._dataStoreService.getData('referralId');
    console.log("second time", this._dataStoreService.getData('referralId'))
  }

  private saveInformation() {
    //this.setReferralId();
    
    this.publicReferralInfoForm.value.prgram= this.providerReferralProgram;
    var payload = {}
    payload = this.publicReferralInfoForm.value;
    payload['referral_id'] = this.referralId;
    this._commonHttpService.patch(
      this.referralId, 
      payload,
      'publicproviderreferral'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this. getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }

    );
  }


private loadDropDown(selectedProviderType:string) {
  const source = forkJoin([
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        providertype:selectedProviderType
      },
      'providerreferral/listprogramnames'
      ),   
  ])
    .map((result) => {
      return {
        programsTypes: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.programtype,
              value: res.programnames
            })
        ),
      };
    })
    .share();

  this.programsTypes$ = source.pluck('programsTypes'); 
  this.selectedProgramNames(); 

  }
 
  selectedProgramNames() {
    //console.log('prgram',event);
   // console.log('selected prgram',event.source.selected._element.nativeElement.innerText.trim());
    //console.log('selected prgram',event.value);
    this.providerReferralProgram= this.publicReferralInfoForm.controls.prgram.value; //event.source.selected._element.nativeElement.innerText.trim();

    //let target = event.source.selected._element.nativeElement;
    //this.programType = target.innerText.trim();

    this.programsTypes$.forEach(programsType =>{
      programsType.forEach(element => {
        if(element.text === this.providerReferralProgram){
          this.programs = element.value;
        }
      })
    });


   // this.programs = event.value;
  }

}

