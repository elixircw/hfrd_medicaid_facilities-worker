import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralBasicInfoComponent } from './public-referral-basic-info.component';

describe('PublicReferralBasicInfoComponent', () => {
  let component: PublicReferralBasicInfoComponent;
  let fixture: ComponentFixture<PublicReferralBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
