import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReferralNarrativeComponent } from './public-referral-narrative.component';

describe('PublicReferralNarrativeComponent', () => {
  let component: PublicReferralNarrativeComponent;
  let fixture: ComponentFixture<PublicReferralNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReferralNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReferralNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
