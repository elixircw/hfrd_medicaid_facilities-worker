import { Component, OnInit, Input, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ProviderReferral } from '../../../provider-applicant/new-applicant/_entities/newApplicantModel';
import { Subject } from 'rxjs/Subject';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import { AlertService, AuthService } from '../../../../@core/services';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ApplicantUrlConfig } from '../../../provider-applicant/provider-applicant-url.config';
import { AppUser } from '../../../../@core/entities/authDataModel';
@Component({
  selector: 'public-referral-narrative',
  templateUrl: './public-referral-narrative.component.html',
  styleUrls: ['./public-referral-narrative.component.scss']
})
export class PublicReferralNarrativeComponent implements OnInit {

  referralInfo: ProviderReferral;
  narratives: any[] = [];

  @Input()
  referralinfooutputsubject$ = new Subject<ProviderReferral>();
  intakeNarrativeForm: FormGroup;
  recognizing = false;
  speechRecogninitionOn: boolean;
  speechData: string;
  notification: string;
  currentLanguage: string;
  applicantNumber: string;
  provider_applicant_id: string;
  private token: AppUser;

  constructor(private formBuilder: FormBuilder,
    private _speechRecognitionService: SpeechRecognitionService,
    private speechRecognizer: SpeechRecognizerService,
    private _alertService: AlertService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _authService: AuthService) {
    this.applicantNumber = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.currentLanguage = 'en-US';
    this.speechRecognizer.initialize(this.currentLanguage);
    this.narrativeForm();
    this.getNarrative();
    this.referralInfo = new ProviderReferral;
    this.referralinfooutputsubject$.subscribe((data) => {
      this.referralInfo.narrative = data.narrative;
    });
    this.token = this._authService.getCurrentUser();
  }

  private narrativeForm() {
    this.intakeNarrativeForm = this.formBuilder.group({
      narrative: [''],
      provider_applicant_id: this.applicantNumber
    });
  }

  activateSpeechToText(): void {
    this.recognizing = true;
    this.speechRecogninitionOn = !this.speechRecogninitionOn;
    if (this.speechRecogninitionOn) {
      this._speechRecognitionService.record().subscribe(
        // listener
        (value) => {
          this.speechData = value;
          this.intakeNarrativeForm.patchValue({ narrative: this.speechData });
        },
        // errror
        (err) => {
          console.log(err);
          this.recognizing = false;
          if (err.error === 'no-speech') {
            this.notification = `No speech has been detected. Please try again.`;
            this._alertService.warn(this.notification);
            this.activateSpeechToText();
          } else if (err.error === 'not-allowed') {
            this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
            this._alertService.warn(this.notification);
          } else if (err.error === 'not-microphone') {
            this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
            this._alertService.warn(this.notification);
          }
        },
        // completion
        () => {
          this.speechRecogninitionOn = true;
          console.log('--complete--');
          this.activateSpeechToText();
        }
      );
    } else {
      this.recognizing = false;
      this.deActivateSpeechRecognition();
    }
  }
  deActivateSpeechRecognition() {
    this.speechRecogninitionOn = false;
    this._speechRecognitionService.destroySpeechObject();
  }
  ngOnDestroy() {
    this._speechRecognitionService.destroySpeechObject();
  }

  private stripHtml(html){
    var temporalDivElement = document.createElement("div");
    temporalDivElement.innerHTML = html;
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
}

  saveNarrative(obj) {
    obj.provider_applicant_id = this.applicantNumber;
    obj.narrativefrom = this.token.user.username;
    obj.narrative = this.stripHtml(obj.narrative);
    this._commonHttpService.create({ "where": obj }, "providerapplicantportal/insertapplnarrative").subscribe(
      (response) => {
        this._alertService.success('Data saved successfully!');
        this.getNarrative();
      },
      (error) => {
        this._alertService.error('Unable to save Narrative, please try again.');
        return false;
      }
    );

  }

  getNarrative() {
    this._commonHttpService.create(
      {
        where: { applicant_id: this.applicantNumber },
        method: 'post'
        //applicant_id:this.applicantNumber
      },
      ApplicantUrlConfig.EndPoint.Applicant.applicantNarrative

    ).subscribe(narrativesarray => {

      this.narratives = narrativesarray.data;
    },
      (error) => {
        this._alertService.error('Unable to get contacts, please try again.');
        console.log('get contact Error', error);
        return false;
      }
    );
  }

}
