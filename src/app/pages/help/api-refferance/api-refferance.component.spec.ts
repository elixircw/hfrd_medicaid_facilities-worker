import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiRefferanceComponent } from './api-refferance.component';

describe('ApiRefferanceComponent', () => {
  let component: ApiRefferanceComponent;
  let fixture: ComponentFixture<ApiRefferanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiRefferanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiRefferanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
