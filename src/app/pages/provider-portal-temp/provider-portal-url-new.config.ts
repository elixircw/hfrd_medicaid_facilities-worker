export class ReferralUrlConfigNew {
    public static EndPoint = {
        Referral: {
            IntakeAttachmenttype:'Intake/attachmenttype',
            attachmentclassificationtype:'Intake/attachmentclassificationtype',
            newintake:'/pages/newintake/my-newintake',
            uploadattachments:'attachments/uploadsFile',
            gettierdecisions:'providerapplicant/gettierdecisions',
            submitdecision:'providerapplicant/submitdecision',
            getroutingusers:'Intakedastagings/getroutingusers',
            getproviderlicensing:'providerapplicant/getproviderlicensing',
            insertapplnarrative:'providerapplicantportal/insertapplnarrative',
            getnarrative:'providerapplicantportal/getnarrative',
            getapplprofileinfo:'providerapplicantportal/getapplprofileinfo',
            updateapplicantprofile:'providerapplicantportal/updateapplicantprofile',
            updateapplicantaddress:'providerapplicantportal/updateapplicantaddress',
            addapplicantaddress:'providerapplicantportal/addapplicantaddress',
            addapplicantemail:'providerapplicantportal/addappemail',
            updateappphone:'providerapplicantportal/updateappphone',
            addappphone:'providerapplicantportal/addappphone',
            deleteapplicantaddress:'providerapplicantportal/deleteapplicantaddress',
            getapplicantstaff:'providerapplicant/getapplicantstaff',
            addapplicantstaff:'providerapplicantportal/addapplicantstaff',
            getproviderapplicant:'providerapplicant/getproviderapplicant',
            providerstaff:'providerstaff',
            getNextNumber:'Nextnumbers/getNextNumber',
            addincidentreport:'providerincident/addincidentreport',
            providerincident:'providerincident',
            getincidentreport:'providerincident/getincidentreport',
            getproviderlicenseinformation:'providercontract/getproviderlicenseinformation',
            providerlicensesanctions:'providerlicensesanctions',
            providerapplicant:'providerapplicant' 

        }
    
    }
}