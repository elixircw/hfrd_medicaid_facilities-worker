import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProviderPortalTempRoutingModule } from './provider-portal-temp-routing.module';
import { ProviderPortalTempComponent } from './provider-portal-temp.component';
import { ProviderApplicationsComponent } from './provider-applications/provider-applications.component';
import { NgxPaginationModule } from 'ngx-pagination';


//import { ExistingApplicantComponent } from './existing-applicant/existing-applicant.component';

@NgModule({
  imports: [
    CommonModule, ProviderPortalTempRoutingModule,
    NgxPaginationModule
  ],
  declarations: [ProviderPortalTempComponent, ProviderApplicationsComponent]
})
export class ProviderPortalTempModule { }
