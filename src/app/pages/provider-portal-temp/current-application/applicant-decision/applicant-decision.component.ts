import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplicationDecision } from '../_entities/newApplicantModel';
import { AuthService, CommonHttpService, AlertService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { RoutingUser } from '../_entities/existingreferralModel';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'applicant-decision',
  templateUrl: './applicant-decision.component.html',
  styleUrls: ['./applicant-decision.component.scss']
})
export class ApplicantDecisionComponent implements OnInit {

  decisionFormGroup: FormGroup;
  applicantProgramFormGroup: FormGroup;
  currentDecision: ApplicationDecision;
  roleId: AppUser;
  decisions = [];
  disbtn = false;
  @Input() isSubmitted: boolean;

  
  applicantNumber: string;
  eventcode = 'PTA';

  // Assigning / Routing
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  selectedPerson: any;
  //isFinalDecisoin: boolean = false;
  isFinalDecisoin: boolean;
  
  isSupervisor: boolean;
  isGroup = false;

  //statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService, 
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute) { 
      this.applicantNumber = route.snapshot.params['id'];
      //this.applicantNumber = 'A201800300025';
    }

  ngOnInit() {
    this.initializeDecisionForm();
    this.initApplicantForm();
    this.getApplicantProgramInformation();

    this.roleId = this._authService.getCurrentUser();
    //console.log(this.roleId);
    this.statusDropdownItems = [
      { "text": "Submit For Review", "value": "Submitted" },
      { "text": "Appeal", "value": "Appealed" }
    ];
    //this.roleId.user.userprofile.teamtypekey;

    if (this.roleId.role.name == 'Executive Director') {
      this.isFinalDecisoin = true;
    } else {
      this.isFinalDecisoin = false;
    }

    this.getDecisions();
  }

  initializeDecisionForm() {
    this.decisionFormGroup = this.formBuilder.group({
      status: [''],
      reason: ['']
    });
  }

  initApplicantForm() {
    this.applicantProgramFormGroup = this.formBuilder.group({
      prgram: [{ value: '', disabled: true }, Validators.required],
      program_type: ['', Validators.required],
      program_name: ['', Validators.required],
      program_tax_id_no: [null, Validators.required],
      contact_first_nm: [''],
      contact_last_nm: [''],
      gender_cd: ['', Validators.required],
      minimum_age_no: ['', Validators.required],
      maximum_age_no: ['', Validators.required],
      children_no: ['', Validators.required],
      iq_range_from: [''],
      iq_range_to: [''],
      profit: [null],
      accreditation: [null],
      addresses: [null, Validators.required]
    });
  }

  getApplicantProgramInformation() {
    return this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          applicant_id: this.applicantNumber
        }
      },
      'providerapplicant/getproviderapplicant'
    ).subscribe(response => {
     
     this.applicantProgramFormGroup.patchValue(response.data);
    //  return response;
    },
      (error) => {
        this._alertService.error('Unable to get applicant program information, please try again.');
        console.log('get applicant program information Error', error);
        return false;
      }
    );
  }
  
  

  getDecisions() {
      this._commonHttpService.create(
        {
          method:'post',
          where: 
          { eventcode: 'PTA',
            objectid :this.applicantNumber
         }        
        },
        'providerapplicant/gettierdecisions'
        //ApplicantUrlConfig.EndPoint.Applicant.getappointmentdetails
    ).subscribe(decisionsarray => {
      
      this.decisions = decisionsarray.data;
      console.log("SIMAR DECISIONS")
      console.log(JSON.stringify(this.decisions));
    },
        (error) => {
          this._alertService.error('Unable to get decisions, please try again.');
          console.log('get decisions Error', error);
          return false;
        }
      );
  
      this._commonHttpService.create(
        {
          method:'post',
          where: 
          { eventcode: 'APPEAL',
            objectid :this.applicantNumber
         }        
        },
        'providerapplicant/gettierdecisions'
        //ApplicantUrlConfig.EndPoint.Applicant.getappointmentdetails
    ).subscribe(decisionsarray => {
      
      this.decisions = this.decisions.concat(decisionsarray.data);
      console.log("SIMAR APPEALS")
      console.log(JSON.stringify(this.decisions));
    },
        (error) => {
          this._alertService.error('Unable to get decisions, please try again.');
          console.log('get decisions Error', error);
          return false;
        }
      );
  
  
  
  
    }
    valuepatchfn(){
      this.disbtn = true;
    }

  submitAction() {

     this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          applicant_id: this.applicantNumber
        }
      },
      'providerapplicant/getproviderapplicant'
    ).subscribe(response => {
      this.applicantProgramFormGroup.patchValue(response.data);
     if(!this.applicantProgramFormGroup.valid) {
      this._alertService.error('Please add all required fields in Program Details');
      return;
     }
     if(this.decisionFormGroup.value.status == "Appealed") {
      this.submitAppeal();
    } else {
      this.submitForReview();
    }
    this.disbtn = false;
    // console.log("Is Valid", this.applicantProgramFormGroup.valid);
    //  return response;
    },
      (error) => {
        this._alertService.error('Unable to get applicant program information, please try again.');
     
        return false;
      }
    );


  }

  submitAppeal() {
        // Does not add to routing table, only to tier decision

        this._commonHttpService
        .getPagedArrayList(
          new PaginationRequest({
            where: {
              eventcode: 'APPEAL',
              objectid: this.applicantNumber,
              fromroleid: "Applicant/Provider",
              tosecurityusersid: '5516ce3c-90ae-4c5c-a58e-2171f7e55ebf',
              toroleid: 'Quality Assurance',
              remarks: this.decisionFormGroup.value.reason, //.get('reason'),
              status: this.decisionFormGroup.value.status, //.get('status')
              //isfinalapproval: this.isFinalDecisoin
              // isreviewrequest: ,
            },
            method: 'post'
          }),
          'providerapplicant/submitdecision'
          //'Providerreferral/routereferralda'
        )
        .subscribe(result => {
          console.log("Inside subscribe", result);
          this._alertService.success('Application Submitted successfully!');
        //  this.getAssignCase(1, false);
    
          //Reset the decision table and get all new results
          this.decisions.length = 0;
          this.getDecisions();
        });
  }

  submitForReview() {

    // Does not add to routing table, only to tier decision

    this._commonHttpService
    .getPagedArrayList(
      new PaginationRequest({
        where: {
          eventcode: 'PTA',
          objectid: this.applicantNumber,
          fromroleid: "Applicant/Provider",
          tosecurityusersid: '972b45ea-a1d6-4330-be7b-3472a011fb7c',
          toroleid: 'Licensing Administrator',
          remarks: this.decisionFormGroup.value.reason, //.get('reason'),
          status: this.decisionFormGroup.value.status, //.get('status')
          //isfinalapproval: this.isFinalDecisoin
          // isreviewrequest: ,
        },
        method: 'post'
      }),
      'providerapplicant/submitdecision'
      //'Providerreferral/routereferralda'
    )
    .subscribe(result => {
      console.log("Inside subscribe", result);
      this._alertService.success('Application Submitted successfully!');
    //  this.getAssignCase(1, false);

      //Reset the decision table and get all new results
      this.decisions.length = 0;
      this.getDecisions();
    });
  }


  //===================== Tier Decision Stuff ===================================//
  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
}

  getRoutingUser() {
    //this.getServicereqid = modal.provider_referral_id;
  //  this.getServicereqid = "R201800200374";
    // console.log(modal.provider_referral_id);
    // this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PTA' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  selectPerson(row) {
    this.selectedPerson = row;
    //console.log(this.selectedPerson);
  }


  assignUser() {
    // Doing the update of Status and assigning at the same time
    // In the same flow, first 'Submit' then 'Assign' will do them both
    // TODO: SIMAR - Put validations that status value has been selected before submitting
    console.log("In assignUser");
    console.log(this.isFinalDecisoin);
    
    if (this.selectedPerson) {
      this._commonHttpService
        .getPagedArrayList(
          new PaginationRequest({
            where: {
              eventcode: 'PTA',
              objectid: this.applicantNumber,
              fromroleid: this.roleId.role.name,
              tosecurityusersid: this.selectedPerson.userid,
              toroleid: this.selectedPerson.userrole,
              remarks: this.decisionFormGroup.value.reason, //.get('reason'),
              status: this.decisionFormGroup.value.status, //.get('status')
              isfinalapproval: this.isFinalDecisoin
              // isreviewrequest: ,
            },
            method: 'post'
          }),
          'providerapplicant/submitdecision'
          //'Providerreferral/routereferralda'
        )
        .subscribe(result => {
          console.log("Inside subscribe", result);
          this._alertService.success('Application assigned successfully!');
        //  this.getAssignCase(1, false);
          this.closePopup();


          //Reset the decision table and get all new results
          this.decisions.length = 0;
          this.getDecisions();
        });
    } else {
      this._alertService.warn('Please select a person');
    }
  }

  closePopup() {
    (<any>$('#intake-caseassign')).modal('hide');
    (<any>$('#assign-preintake')).modal('hide');
    (<any>$('#reopen-intake')).modal('hide');
  }

}
