import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentApplicationComponent } from './current-application.component';



const routes: Routes = [
    {
        path: ':id',
        component: CurrentApplicationComponent,
        children: [
            // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
            // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
            // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
            // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
        ]
        // canActivate: [RoleGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CurrentApplicationRoutingModule {}
