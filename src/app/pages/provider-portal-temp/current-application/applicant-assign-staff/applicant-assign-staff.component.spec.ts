import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantAssignStaffComponent } from './applicant-assign-staff.component';

describe('ApplicantAssignStaffComponent', () => {
  let component: ApplicantAssignStaffComponent;
  let fixture: ComponentFixture<ApplicantAssignStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantAssignStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantAssignStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
