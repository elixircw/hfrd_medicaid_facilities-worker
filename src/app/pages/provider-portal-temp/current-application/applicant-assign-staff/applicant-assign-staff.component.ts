import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import { AlertService } from '../../../../@core/services/alert.service';
import {MatTableDataSource} from '@angular/material';
export interface staffInfoTable {
  firstName: string;
  lastName: string;
  affiliationType:string;
  jobTitle:string;
  employeeType:string;
 // provider_staff_id:string;

  }
@Component({
  selector: 'applicant-assign-staff',
  templateUrl: './applicant-assign-staff.component.html',
  styleUrls: ['./applicant-assign-staff.component.scss']
})export class ApplicantAssignStaffComponent implements OnInit {
	staffInformation = [];
	selection = new SelectionModel < staffInfoTable > (true, []);
	dataSource: any;
	staffArray :any[];
	applicantId :string;
	assignedStaff:any[];
	displayedColumns: string[] = ['select', 'firstName', 'lastName', 'affiliationType', 'jobTitle', 'employeeType'];

  @Input() isSubmitted: boolean;
  @Input() providerId: string;
	constructor(private formBuilder: FormBuilder,
		private _commonHttpService: CommonHttpService,
		private _alertService: AlertService,
		private _router: Router, private route: ActivatedRoute) {
		this.applicantId = route.snapshot.params['id'];
	}

	ngOnInit() {
		this.getStaffInformation();
		this.getassignedstaff();
	}
	getStaffInformation() {
		console.log('Provider Id in staff', this.providerId);
		this._commonHttpService.getArrayList({
				method: 'get',
				nolimit: true,
				where: {
					 object_id : this.providerId,
				},
			},
			'providerstaff?filter'
		).subscribe(
			(response) => {
				this.staffInformation = response;
				this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
			},
			(error) => {
				this._alertService.error("Unable to retrieve information");
			}
		);
	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}
	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}
	assignStaff() {
		this.staffArray = this.selection.selected;		
		var selectedstaffs = [];		
	//	selectedstaffs.push({provider_applicant_id:this.applicantId});
		for(var i=0; i<this.staffArray.length;i++){
			selectedstaffs.push({provider_staff_id:this.staffArray[i].provider_staff_id});
		}
		var payload = {}
		payload['applicant_id'] =this.applicantId ;
		payload['staff'] = selectedstaffs;
		console.log('payload staff',payload);
		this._commonHttpService.create(
			payload,
			'providerstaff/assignstaff'
		).subscribe(
			(response) => {
			this._alertService.success("Staff assigned successfully!");
			this.getassignedstaff();
			( < any > $('#assign-staff')).modal('hide');
		},
			(error) => {
			this._alertService.error("Unable to assign staffs");
			});
		
		this._alertService.success("Staffs assigned successfully!");    

	}
	getassignedstaff(){
		this._commonHttpService.create(
			{
			  where: { provider_applicant_id: this.applicantId },
			  method: 'post'
			  //applicant_id:this.applicantNumber
			},
			'providerstaff/getassignedstaff'
		
		  ).subscribe(response => {		
			this.assignedStaff = response;
			console.log(JSON.stringify('assigned staff',this.assignedStaff));
		  },
			(error) => {
			  this._alertService.error('Unable to get assigned staffs, please try again.');
			  console.log('get contact Error', error);
			  return false;
			}
		  );
	}

}