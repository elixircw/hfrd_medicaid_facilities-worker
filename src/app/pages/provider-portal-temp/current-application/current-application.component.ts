import { Component, OnInit, ViewChild } from '@angular/core';
import { AttachmentIntakes, GeneratedDocuments, applinfo, ProviderReferral } from './_entities/newApplicantModel';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { ApplicantUrlConfig } from '../provider-portal-temp-url.config';
import { AlertService } from '../../../@core/services/alert.service';
import { ReferralUrlConfig } from '../../provider-referral/provider-referral-url.config';
import { ApplicantDocumentCreatorComponent } from './applicant-document-creator/applicant-document-creator.component';

@Component({
  selector: 'current-application',
  templateUrl: './current-application.component.html',
  styleUrls: ['./current-application.component.scss']
})
export class CurrentApplicationComponent implements OnInit {

  addAttachementSubject$ = new Subject<AttachmentIntakes[]>();
  generatedDocuments$ = new Subject<string[]>();
  generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
  generateDocumentInput$ = new Subject<GeneratedDocuments[]>();
  profileOutputSubject$ = new Subject<applinfo>();
  referralinfooutputsubject$ = new Subject<ProviderReferral>();
  
  applicantInfo: applinfo = new applinfo();
  applicantNumber: string;
  referralInfo: ProviderReferral = new ProviderReferral() ;
  //applicantInfo: Profile[] = [];

  //Check if the applicant has been tier approved, and now has a license
  isLicensed: boolean = false;
  licenseNumber: string;

  //Check if the application is submitted, then make it read-only
  isSubmitted: boolean = false;

  applicationStatus: string;
  providerId: string;

  @ViewChild(ApplicantDocumentCreatorComponent) documentGenerator;

  constructor(private _commonHttpService: CommonHttpService,private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute
  ) { 
    this.applicantNumber = route.snapshot.params['id'];
  }

    ngOnInit() {
      this.getSubmissionStatus();      
    }

      getSubmissionStatus() {
        this._commonHttpService.create(
          {
            method:'post',
            where: 
            {
              applicant_id:this.applicantNumber
          }        
          },
          'providerapplicant/getproviderapplicant'
      ).subscribe(response => {
        if (response.data.provider_id) {
          this.providerId = response.data.provider_id;
        }
        if (response.data.application_status) {
          this.applicationStatus = response.data.application_status;
        }
        
        if (this.applicationStatus == 'Submitted') {
          this.isSubmitted = true;
          console.log("Rahul readOnly:"+ this.isSubmitted);

        }

      },
          (error) => {
            this._alertService.error('Unable to get applicant program information, please try again.');
            console.log('get applicant program information Error', error);
            return false;
          }
        );
    }
      
      updateLicenseDateValues(licenseDateUpdateEvent) {
        console.log("In the parent");
        this.documentGenerator.updateLicenseDateValues(licenseDateUpdateEvent);
      }
}
