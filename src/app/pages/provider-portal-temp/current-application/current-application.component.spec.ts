import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentApplicationComponent } from './current-application.component';

describe('NewApplicantComponent', () => {
  let component: CurrentApplicationComponent;
  let fixture: ComponentFixture<CurrentApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
