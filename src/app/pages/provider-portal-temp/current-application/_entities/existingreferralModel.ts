import {  } from './newApplicantModel';

export class ReferralSearchCase {
    status: string;
    referralnumber: string;
    sortcolumn: string;
    sortorder: string;
}

export class RoutingUser {
    userid: string;
    teamname: string;
    workloads: string;
    username: string;
    agencykey: string;
    userrole: string;
    loadnumber: string;
    email: string;
    available: string;
    issupervisor: boolean;
    zipcode: number;
    worklocationcode: string;
    homelocationcode: string;
    totalcases: number;
    rolecode: string;
}