import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, CommonHttpService, AlertService, CommonDropdownsService } from '../../../../@core/services';
import { ApplicantProgramInfo } from '../_entities/portalApplicantModel';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
//import { ApplicantDocumentCreatorComponent } from '../applicant-document-creator/applicant-document-creator.component';

@Component({
  // providers: [ApplicantDocumentCreatorComponent],
  selector: 'applicant-program-details',
  templateUrl: './applicant-program-details.component.html',
  styleUrls: ['./applicant-program-details.component.scss']
})
export class ApplicantProgramDetailsComponent implements OnInit {

  applicantNumber: string;
  @Input() isSubmitted: boolean;


  applicantProgramInfo: ApplicantProgramInfo = new ApplicantProgramInfo();
  applicantProgramFormGroup: FormGroup;

  addresses = []
  addressTypes = [];
  addressForm: FormGroup;
  deleteAdrObj: any;
  isZipValid: boolean;
  isPaymentChecked: boolean = false;
  dropDownValue: string;
  countyList$: Observable<DropdownModel[]>;
  stateList$: Observable<any[]>;
  isPaymentAddress: boolean = false;
  paymentAddressId: number;
  stateDropDownItems$: Observable<DropdownModel>;
  suggestedAddress$: Observable<any[]>;
  genderList: any[];
  accreditationList$: Observable<any[]>;
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _commonDropdownService: CommonDropdownsService
  ) {
    this.applicantNumber = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.initializeApplicantProgramForm();
    this.initializeAddressForm();
    this.getApplicantProgramInformation();
    this.getApplicantAddresses();
    this.loadDropDown();
    this.getGenderList();
  }


  private initializeApplicantProgramForm() {
    //let addresses = new FormArray([]);

    this.applicantProgramFormGroup = this.formBuilder.group({
      prgram: [{ value: '', disabled: true }, Validators.required],
      program_type: ['', Validators.required],
      program_name: ['', Validators.required],
      program_tax_id_no: [null, Validators.required],
      contact_first_nm: [''],
      contact_last_nm: [''],
      gender_cd: ['', Validators.required],
      minimum_age_no: ['', Validators.required],
      maximum_age_no: ['', Validators.required],
      children_no: ['', Validators.required],
      iq_range_from: [''],
      iq_range_to: [''],
      profit: [null],
      accreditation: [null]
      //addresses: addresses
    });
  }



  private initializeAddressForm() {
    this.addressForm = this.formBuilder.group({
      applicant_id: [''],
      provider_applicant_profile_id: [''],
      object_id: [''],
      address_id: [''],
      adr_street_nm: [''],
      adr_line2: [''],
      adr_city_nm: [''],
      adr_state_cd: [''],
      adr_zip5_no: [null],
      address_type: [''],
      adr_county_cd: [''],
      isMainAddress: false,
      address_mapping_id: ['']

    });
    this.addressTypes = ["Main", "Site", "Payment"];
    // if(this.addressForm.controls.address_type.value!="Payment"){
    //   this.isPaymentAddress=false;
    // }
    // else{
    //   this.isPaymentAddress=true;
    // }
  }


  getApplicantProgramInformation() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          applicant_id: this.applicantNumber
        }
      },
      'providerapplicant/getproviderapplicant'
    ).subscribe(response => {
      console.log("SIMAR APPLICANT PROGRAM INFORMATION")
      this.applicantProgramInfo = response.data;
      console.log(JSON.stringify(this.applicantProgramInfo));

      this.applicantProgramFormGroup.patchValue(this.applicantProgramInfo);
      if (response.data.addresses != null) {
        this.addresses = response.data.addresses;
      }

    },
      (error) => {
        this._alertService.error('Unable to get applicant program information, please try again.');
        console.log('get applicant program information Error', error);
        return false;
      }
    );
  }

  saveApplicantProgramInformation() {
    // var payload = new ApplicantProgramInfo();
    const payload = this.applicantProgramFormGroup.getRawValue();
    if (this.applicantProgramFormGroup.invalid) {
      this._alertService.error('Please fill required fields');
      return false;
    }
    payload.applicant_id = this.applicantNumber;
    this._commonHttpService.update('',
      payload,
      'providerapplicantportal/updateapplprograminfo'
    ).subscribe(response => {
      console.log(response)
      this._alertService.success('Program Information Saved Successfully!');
    },
      (error) => {
        this._alertService.error('Unable to update program information, please try again.');
        console.log('update program information Error', error);
        return false;
      }
    );
  }


  saveAddress(obj) {
    obj.applicant_id = this.applicantNumber;
    //console.log("createOrUpdateAppointment:-" + obj);
    if (obj.address_id && !this.isPaymentChecked) {
      this._commonHttpService.create(obj, 'providerapplicantportal/updateapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Updated successfully!');
          this.getApplicantAddresses();
          this.addresses.unshift(obj);
        },
        (error) => {
          this._alertService.error('Unable to Update address, please try again.');
          return false;
        }
      );
    }
    else {
      this._commonHttpService.create(obj, 'providerapplicantportal/addapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Created successfully!');

          // this.addresses.unshift(obj);
          this.getApplicantAddresses();
        },
        (error) => {
          this._alertService.error('Unable to Create address, please try again.');
          console.log('Save address Error', error);
          return false;
        });
    }
    (<any>$('#intake-address')).modal('hide');
    this.isPaymentChecked = false;
    this.isPaymentAddress = false;
  }


  resetForm() {
    this.addressForm.reset();
  }


  private getApplicantAddresses() {
    this._commonHttpService.create(
      {
        "where": {
          applicant_id: this.applicantNumber
        }
      },
      'publicproviderapplicant/getpublicapplicantaddresses'
    ).subscribe(response => {
      // if( response.data[0].addresses != null) {
      this.addresses = response.data[0].addresses;
      //}
      console.log(this.addresses);
    },
      (error) => {
        this._alertService.error('Unable to get address information, please try again.');
        console.log('get address information Error', error);
        return false;
      }
    );
  }


  EditAddress(editObj) {
    console.log(editObj)
    this.addressForm.patchValue({
      object_id: editObj.object_id,
      address_id: editObj.address_id,
      adr_street_nm: editObj.adr_street_nm,
      adr_line2: editObj.adr_line2,
      adr_city_nm: editObj.adr_city_nm,
      adr_state_cd: editObj.adr_state_cd,
      adr_county_cd: editObj.adr_county_cd,
      adr_zip5_no: editObj.adr_zip5_no,
      address_type: editObj.address_type,
      address_mapping_id: editObj.address_mapping_id
    });
    if (editObj.address_type == "Payment")
      this.isPaymentAddress = true;
    (<any>$('#intake-address')).modal('show');
  }

  DeleteAddress(Obj) {
    this.deleteAdrObj = Obj;
    (<any>$('#delet-address')).modal('show');
  }

  CancelDelete() {
    (<any>$('#delet-address')).modal('hide');
  }

  DeleteAddressConfirm() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          address_id: this.deleteAdrObj.address_id,
          address_mapping_id: this.deleteAdrObj.address_mapping_id,
          address_type: this.deleteAdrObj.address_type
        }
      },
      'providerapplicantportal/deleteapplicantaddress')
      .subscribe(response => {
        if (response) {
          this._alertService.success("Address Deleted successfully");
          this.getApplicantProgramInformation();
          this.getApplicantAddresses();
        }
      });
    (<any>$('#delet-address')).modal('hide');
  }

  zipValidation(event: any) {
    this.isZipValid = false;
    const pattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
    if (!pattern.test(event.target.value)) {
      this.isZipValid = false;
    }
    else {
      this.isZipValid = true;
    }
  }
  isPaymentSelected(selectedValue) {
    this.dropDownValue = selectedValue;
    this.isPaymentChecked = false;
    console.log("Selected Value", selectedValue);
    if (selectedValue == "Payment") {
      this.isPaymentChecked = true;
      console.log("loop");
    }
  }

  isCheckedEvent(event) {
    if (event.checked) {
      this.isPaymentChecked = true;
      this.paymentAddressId = this.addressForm.controls.address_id.value;
      this.getAddressByType("Main");

    }
    else {
      this.isPaymentChecked = false;
      this.initializeAddressForm();
      this.addressForm.patchValue({
        address_type: "Payment"
      });
    }
  }
  getAddressByType(event) {
    let addressTypeVal;
    let addresGet;
    if (typeof event == "string") {
      addresGet = "Main"
      addressTypeVal = "Payment";
    }
    else {
      addresGet = event.value;
      addressTypeVal = event.value;
      if (event.value == "Payment")
        this.isPaymentAddress = true;
      else
        this.isPaymentAddress = false;
    }
    this._commonHttpService.create(
      {
        "where": {
          applicant_id: this.applicantNumber,
          addressType: addresGet
        }
      },
      'publicproviderapplicant/getpublicapplicantByaddressesType'
    ).subscribe(response => {
      //if( response.data[0].addresses != null) {
      //this.addresses = response.data[0].addresses;
      //}

      this.initializeAddressForm();
      let addressFormInfo = response.data;
      // addressFormInfo.applicant_id = this.applicantId;
      if (addressFormInfo.length > 0) {
        this.addressForm.patchValue({
          object_id: this.applicantNumber,
          address_id: addressFormInfo[0].address_id,
          adr_street_nm: addressFormInfo[0].adr_street_nm,
          adr_line2: addressFormInfo[0].adr_line2,
          adr_city_nm: addressFormInfo[0].adr_city_nm,
          adr_state_cd: addressFormInfo[0].adr_state_cd,
          adr_county_cd: addressFormInfo[0].adr_county_cd,
          adr_zip5_no: addressFormInfo[0].adr_zip5_no,
          isMainAddress: this.isPaymentChecked
        });
      }
      this.addressForm.patchValue({
        address_type: addressTypeVal
      })
      //this.isPaymentChecked=false;
      //this.isPaymentAddress=false;
    },
      (error) => {
        this._alertService.error('Unable to get address information, please try again.');
        console.log('get address information Error', error);
        return false;
      }
    );
  }


  private loadDropDown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList('get', 'providerreferral/listcountycodes'),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        'States?filter'
      ),
    ])
      .map((result) => {
        console.log(result);
        return {

          countyListItems: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.value_tx,
                value: res.picklist_value_cd
              })
          ),
          stateList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();
    // this.countyList$ = source.pluck('countyListItems');
    // this.stateDropDownItems$ = source.pluck('stateList');
    this.stateList$ = this._commonDropdownService.getPickListByName('state');
    this.countyList$ = this._commonDropdownService.getPickList('328');
    this.accreditationList$ = this._commonDropdownService.getPickListByName('accreditation');
  }

  checkNumber(el) {
    if (el.target.value !== '') {
      el.target.value = (el.target.value.replace(/[^0-9]/g, '') > 100) ? '' : el.target.value;
      return el.target.value;
    }
    return '';
  }

  getSuggestedAddress() {
    this.suggestAddress();
  }
  suggestAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.addressForm.value.adr_street_nm,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        'People/suggestaddress'
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
            this.suggestedAddress$ = result;
          }
        }
      );
  }
  selectedAddress(model) {
    this.addressForm.patchValue({
      adr_street_nm: model.streetLine ? model.streetLine : '',
      adr_city_nm: model.city ? model.city : '',
      adr_state_cd: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        'People/validateaddress'
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.addressForm.patchValue({
              adr_zip5_no: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.addressForm.value.adr_state_cd, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  if (resultresp && resultresp.length) {
                    this.addressForm.patchValue({
                      adr_county_cd: resultresp[0].ref_key
                    });
                  }
                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  loadCounty(state) {
    console.log(state);
    // this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
    //  this.countyList$ = Observable.of(countyList);
    // });
  }

  getGenderList() {
    this._commonHttpService.getArrayList(
      {
        where: { tablename: 'gender', teamtypekey: 'OLM' },
        method: 'get',
        nolimit: true
      },
      'referencetype/gettypes?filter'
    ).subscribe(result => {
      this.genderList = result;
    });
  }
}
