import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantProgramDetailsComponent } from './applicant-program-details.component';

describe('ApplicantLicenseComponent', () => {
  let component: ApplicantProgramDetailsComponent;
  let fixture: ComponentFixture<ApplicantProgramDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantProgramDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantProgramDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
