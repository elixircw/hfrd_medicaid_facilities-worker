import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantNarrativeComponent } from './applicant-narrative.component';

describe('ApplicantNarrativeComponent', () => {
  let component: ApplicantNarrativeComponent;
  let fixture: ComponentFixture<ApplicantNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
