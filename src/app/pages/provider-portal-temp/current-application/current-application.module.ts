import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrentApplicationComponent } from './current-application.component';
import { CurrentApplicationRoutingModule } from './current-application-routing.module';
import { ApplicantAttachmentsComponent } from './applicant-attachments/applicant-attachments.component';
import { AttachmentUploadComponent } from './applicant-attachments/attachment-upload/attachment-upload.component';
import { EditAttachmentComponent } from './applicant-attachments/edit-attachment/edit-attachment.component';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule
} from '@angular/material';

import { ReferralDecisionComponent } from '../../provider-referral/new-referral/referral-decision/referral-decision.component';
import { ApplicantNarrativeComponent } from './applicant-narrative/applicant-narrative.component';
import { ApplicantDocumentCreatorComponent } from './applicant-document-creator/applicant-document-creator.component';

import { ApplicantDecisionComponent } from './applicant-decision/applicant-decision.component';
import { ApplicantProgramDetailsComponent } from './applicant-program-details/applicant-program-details.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { ApplicantAssignStaffComponent } from './applicant-assign-staff/applicant-assign-staff.component';
import { NgxMaskModule } from 'ngx-mask';
import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';


@NgModule({
  imports: [
    CommonModule, CurrentApplicationRoutingModule, 
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    NgxfUploaderModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    NgxPaginationModule,
    ControlMessagesModule,
    NgxMaskModule.forRoot(),
    SharedDirectivesModule
  ],
  declarations: [CurrentApplicationComponent, 
    ApplicantAttachmentsComponent, 
    AttachmentUploadComponent, 
    EditAttachmentComponent,
    ApplicantProgramDetailsComponent,
     ReferralDecisionComponent,
     ApplicantNarrativeComponent,
     ApplicantDocumentCreatorComponent,
     ApplicantDecisionComponent,
     ApplicantAssignStaffComponent],
  providers: [NgxfUploaderService,SpeechRecognizerService,SpeechRecognitionService]
})
export class CurrentApplicationModule { }
