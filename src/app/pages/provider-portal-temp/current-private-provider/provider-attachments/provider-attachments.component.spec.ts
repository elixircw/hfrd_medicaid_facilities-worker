import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAttachmentsComponent } from './provider-attachments.component';

describe('ProviderAttachmentsComponent', () => {
  let component: ProviderAttachmentsComponent;
  let fixture: ComponentFixture<ProviderAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
