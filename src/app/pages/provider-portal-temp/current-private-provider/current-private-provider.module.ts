import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatStepperModule
} from '@angular/material';
import { CurrentPrivateProviderRoutingModule } from './current-private-provider-routing.module';
import { CurrentPrivateProviderComponent } from './current-private-provider.component';
import { PrivateProviderStaffComponent } from './private-provider-staff/private-provider-staff.component';
import { ProviderIncidentReportingComponent } from './provider-incident-reporting/provider-incident-reporting.component';
import { ProviderSanctionsComponent } from '../../provider-management/new-provider/provider-sanctions/provider-sanctions.component';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { SignaturePadModule } from 'angular2-signaturepad';
import { ProviderCorporateInformationComponent } from './provider-corporate-information/provider-corporate-information.component';
import { ProviderUirComponent } from './provider-uir/provider-uir.component';
import { ProviderChangeRequestComponent } from './provider-change-request/provider-change-request.component';
// import { PrivateProviderDocumentsComponent } from './private-provider-documents/private-provider-documents.component';
// import { ProviderPortalTempUtilsModule } from '../provider-portal-temp-utils/provider-portal-temp-utils.module';
// import { AttachmentModule } from '../provider-portal-temp-utils/attachment/attachment.module';
import { ProviderAttachmentsComponent } from './provider-attachments/provider-attachments.component';
import { AttachmentUploadComponent } from './provider-attachments/attachment-upload/attachment-upload.component';
import { EditAttachmentComponent } from './provider-attachments/edit-attachment/edit-attachment.component';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { NgxMaskModule } from 'ngx-mask';
import { RequestSignatureFieldComponent } from './provider-change-request/signature-field/signature-field.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { SanctionSignatureFieldComponent } from './provider-sanctions/signature-field/signature-field.component';
import { CapComponent } from './cap/cap.component';
import { PaginationModule } from 'ngx-bootstrap';
import { Attachment } from './provider-attachments/_entities/attachmnt.model';
import { AttachmentComponent } from '../provider-portal-temp-utils/attachment/attachment.component';
import { AttachmentService } from '../provider-portal-temp-utils/attachment/attachment.service';
import { SortTableModule } from '../../../shared/modules/sortable-table/sortable-table.module';
import { PrivateProviderCpaHomeComponent } from './private-provider-cpa-home/private-provider-cpa-home.component';
import { ProviderClearanceMedicalComponent } from './provider-clearance-medical/provider-clearance-medical.component';
import { PersonCardComponent } from './provider-clearance-medical/person-card/person-card.component';
import { ClearanceMedicalFormComponent } from './provider-clearance-medical/clearance-medical-form/clearance-medical-form.component';
import { ProviderClearanceMedicalService } from './provider-clearance-medical/provider-clearance-medical.service';
import { ProviderMonitorLicensingComponent } from '../../provider-management/new-provider/provider-monitor-licensing/provider-monitor-licensing.component';

@NgModule({
  imports: [
    CommonModule, CurrentPrivateProviderRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    MatStepperModule,
    NgxfUploaderModule.forRoot(),
    SignaturePadModule,
    ControlMessagesModule,
    NgxMaskModule.forRoot(),
    FormMaterialModule,
    PaginationModule,
    SortTableModule
    // ProviderPortalTempUtilsModule,
    // AttachmentModule
  ],
  declarations: [
    CurrentPrivateProviderComponent,
    PrivateProviderStaffComponent,
    ProviderIncidentReportingComponent,
    ProviderSanctionsComponent,
    SanctionSignatureFieldComponent,
    ProviderCorporateInformationComponent,
    ProviderUirComponent,
    ProviderChangeRequestComponent,
    // PrivateProviderDocumentsComponent,
    ProviderAttachmentsComponent,
    AttachmentUploadComponent,
    EditAttachmentComponent,
    RequestSignatureFieldComponent,
    CapComponent,
    AttachmentComponent,
    PrivateProviderCpaHomeComponent,
    ProviderClearanceMedicalComponent,
    PersonCardComponent,
    ClearanceMedicalFormComponent,
    ProviderMonitorLicensingComponent
  ],
  providers: [NgxfUploaderService, AttachmentService, ProviderClearanceMedicalService]
})
export class CurrentPrivateProviderModule { }
