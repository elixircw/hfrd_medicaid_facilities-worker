import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, AuthService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'provider-incident-reporting',
  templateUrl: './provider-incident-reporting.component.html',
  styleUrls: ['./provider-incident-reporting.component.scss']
})
export class ProviderIncidentReportingComponent implements OnInit {

  providerId: string;
  incidents=[];
  incidentForm: FormGroup;
  youthForm: FormGroup;
  notificationEmailForm: FormGroup;
  programName:any[];
  class3Types:any[];
  
  // Arrays for the payload
  emails: any[];
  phones: any[];
  youths: any[];

  showAddEmail: boolean;
  notification_emails:any[];
  default_notification_emails:any[];

  
  showYouthinfo: boolean;
  showContactInfo: boolean;

  selectedSiteId: string;
  selectedLicenseNo: string;
  licenseInformation:any;
  selectedSiteLicenseInformation:any;

  isClassIII: boolean;
  
  constructor(private formBuilder: FormBuilder, 
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _authService: AuthService) { 
      this.providerId = route.snapshot.params['id'];
    }

  ngOnInit() {
    this.initIncidentForm();
    this.initYouthForm();
    this.getIncidents();
    this.getProviderContractLicenseTypes();
    this.initNotificationForm();
    this.class3Types=["Allegation Against Staff Referred (non-sexual)","Alleged Abuse (In custody/supervision) ","Assault: Youth on STAFF - Level 3","Assault: Youth on Youth - Level 3",
    "AWOL","Death of a child","Death of a staff on duty or foster parent","Emergency Petition","Fight- Level 3","Fire in the physical plant or that poses  a safety risk to the child",
    "Group Disturbance –Law Enforcement Required","Group Disturbance – No Law Enforcement Required","Hostage Taking","Loss of a Class “A” Tool","Neglect during the current placement episode",
    "Physical plant problem where children are moved to another location","Contraband - Level 3","Sexual Abuse Allegation (STAFF on Youth Indecent Exposure)","Sexual Abuse Allegation (STAFF on Youth Sexually Abusive Contact)",
    "Sexual Abuse Allegation (STAFF on Youth Sexually Abusive Penetration)","Sexual Abuse Allegation (STAFF on Youth Voyeurism)","Sexual Abuse Allegation (Youth on STAFF Sexual Misconduct)",
    "Sexual Abuse Allegation (Youth on Youth Indecent Exposure)","Sexual Abuse Allegation (Youth on Youth Sexual Contact)","Sexual Abuse Allegation (Youth on Youth Sexual Penetration)",
    "STAFF Injury or Illness: Level 3","Suicide attempt","Vehicle Accident","Youth Injury or Illness - Level 3"];
    
    this.showYouthinfo= false;
    this.showContactInfo= false;
    this.showAddEmail= false;
    this.isClassIII= false;

    this.emails = [];
    this.phones = [];
    this.youths = [];
    this.default_notification_emails=["OLM.Incidents@Maryland.gov ","Licensing Coordinator","Program Managers",
    "Case Manager","Deputy Director","Executive Director"];
    this.notification_emails=[];
  }

  initIncidentForm() {
    this.incidentForm = this.formBuilder.group({
      incident_no: [''],
      incident_date: null,
      uir_program_nm:[''],
      uir_phone_no: null,
      uir_email_id: [''],
      uir_is_class3:[''],
      uir_class3:[''],
      uir_adr_street_no:[''],
      uir_adr_street_nm:[''],
      uir_adr_city_nm:[''],
      uir_adr_state_cd:[''],
      uir_adr_zip_no:[''],
      incident_time:[''],    
      program_director_first_nm :[''],
      program_director_last_nm :[''],
      program_director_phone :[''],
      law_enforcement_notify_date :null,
      law_enforcement_notify_time:[''],
      law_enforcement_police_report_no :[''],
      law_enforcement_first_nm :[''],
      law_enforcement_last_nm:[''],
      law_enforcement_phone :[''],
      uir_addn_phone:[''],
      uir_addn_email:[''],

      incident_category :[''],
      youth_narrative:[''],

      program_nm: [''],
      site_id: null,
      license_no: ['']
    });
  }

  initYouthForm() {
    this.youthForm = this.formBuilder.group({
      uir_youth_first_nm:[''],
      uir_youth_last_nm:[''],
      uir_youth_id_no:[''],
      uir_youth_dob:null,
      uir_youth_admitting_charge:[''],
      uir_youth_adr_street_no:[''],
      uir_youth_adr_street_nm:[''],
      uir_youth_adr_city_nm:[''],
      uir_youth_adr_state_cd:[''],
      uir_youth_adr_zip_no:[''],
      uir_youth_adr_county:['']
    });
  }
  initNotificationForm() {
    this.notificationEmailForm = this.formBuilder.group({
      notificationEmail:[''],
    });
  }

  openAddUIR() {
    this._commonHttpService.getArrayList(
      {},
      'Nextnumbers/getNextNumber?apptype=providerIncident'
      ).subscribe(result => {
        console.log('Incident Number', result);
        this.incidentForm.patchValue({
          incident_no: result['nextNumber']
        });
    });
  }

  createIncident() {
		var payload = {}
    
    if (this.incidentForm.value.uir_class3[0]=="AWOL"){
      console.log("in AWOL");
      this.default_notification_emails=[];
      this.default_notification_emails=["Case Manager"];
    }
    console.log(this.incidentForm.value.uir_class3);

    console.log('Building payload');
    payload = this.incidentForm.getRawValue();
    payload['provider_id'] = this.providerId;
		payload['site_id'] = this.selectedSiteId;
    //payload['license_no'] = this.selectedLicenseNo;
    payload['emails'] = this.emails;
    payload['phones'] = this.phones;
    payload['youths'] = this.youths;

    if (this.incidentForm.value.uir_is_class3=='yes') {
      this.isClassIII = true;
      payload['incident_category']='Class III';
    }

		this._commonHttpService.create({
		 	where: payload
		  },
		  'providerincident/addincidentreport')
		  .subscribe(
			  (response) => {
			  this._alertService.success('UIR added successfully!');
        //this.getSanctions();
        this.getIncidents();
			  (<any>$('#add-uir')).modal('hide');
		    },
		    (error) => {
			  this._alertService.error("Unable to save UIR Information");
		    }
		  );
  }

  getIncidents() {
    this._commonHttpService.getArrayList({
			method: 'get',
			where: {
				'provider_id': this.providerId
			}
		}, 'providerincident?filter')
		.subscribe( response => {
      console.log(response);
      this.incidents = response;
		});
  }

  getIncidentInformation(incident_no) {
      this._commonHttpService.getArrayList({
        method: 'get',
        where: {
          'incident_no': incident_no
        }
      }, 'providerincident/getincidentreport?filter')
      .subscribe( response => {
        console.log(response);
      });
  }


  patchIncident() {
    (<any>$('#add-uir')).modal('hide');
  }

  editIncident(incident) {
    this.getIncidentInformation(incident.incident_no);

  }

  deleteIncident(incident) {

  }
  
  resetForm() {

  }
  
  addYouthInfo(){
    this.showYouthinfo=true;
  }

  saveYouthInfo(){
    console.log("Saving in array youth")
    var currentYouthInfo = this.youthForm.getRawValue();
    this.youths.push(currentYouthInfo);
    this.youthForm.reset();
    this.showYouthinfo=false;
  }

  addContactInfo(){
    this.showContactInfo=true;
  }

  saveContactInfo(){
    if(this.incidentForm.value.uir_addn_email != '') {
      this.emails.push(this.incidentForm.value.uir_addn_email);
    }
    if(this.incidentForm.value.uir_addn_phone != '') {
      this.phones.push(this.incidentForm.value.uir_addn_phone);
    }
    this.incidentForm.patchValue({
      uir_addn_email: [''],
      uir_addn_phone: ['']
    })
    this.showContactInfo=false;    
  }

  private getProviderContractLicenseTypes() {
    this.licenseInformation={};
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
           provider_id: this.providerId
          }
      },
      'providercontract/getproviderlicenseinformation'
    ).subscribe(response => {
      this.licenseInformation = response;
      this.programName = this.licenseInformation.data.map(a => a.site_id);
      console.log(this.programName);
    });
  }


  changeSiteId(event) {
    this.selectedSiteId = event.value;
    //this.selectedLicenseNo;
    this.getSiteLicenseInformation();
  }

  getSiteLicenseInformation() {
    this.selectedSiteLicenseInformation = {}
    this._commonHttpService.create(
      {
        method:'post',
        where: 
        {
          objectid :this.selectedSiteId
        }        
      },
      'providerlicense/getproviderlicensing'
    ).subscribe(response => {
      this.selectedSiteLicenseInformation = response;
      this.selectedSiteLicenseInformation = this.selectedSiteLicenseInformation.data[0];
      this.selectedLicenseNo = this.selectedSiteLicenseInformation.license_no;
      this.patchSiteLicenseInformation();
      
    },
        (error) => {
          this._alertService.error('Unable to get license information, please try again.');
          console.log('get license information Error', error);
          return false;
        }
      );
    }

    patchSiteLicenseInformation() {
      this.incidentForm.patchValue({
        uir_phone_no: null,
        uir_email_id: [''],
        uir_adr_street_no: this.selectedSiteLicenseInformation.adr_street_no,
        uir_adr_street_nm: this.selectedSiteLicenseInformation.adr_street_nm,
        uir_adr_city_nm: this.selectedSiteLicenseInformation.adr_city_nm,
        uir_adr_state_cd: this.selectedSiteLicenseInformation.adr_state_cd,
        uir_adr_zip_no: this.selectedSiteLicenseInformation.adr_zip5_no,
        program_nm: this.selectedSiteLicenseInformation.provider_nm,
        license_no: this.selectedSiteLicenseInformation.license_no
      });
    }

    addNotificationEmail(){
      this.showAddEmail=true;
    }

    saveNotificationEmail(){
      this.notification_emails.push(this.notificationEmailForm.value.notificationEmail);
      this.notificationEmailForm.reset();
      this.showAddEmail=false;
    }

    sendNotificationEmail() {
      var payload = {}
      payload['email_content'] = "A Class III Incident has occuered at Provider " + this.providerId + ", This is a 1 hour UIR notification. Please take appropriate actions." 
      
      // Add the default emails, and the additional emails
      payload['emails'] = this.notification_emails.join();
      
      this._commonHttpService.create({
        where: payload
       },
       'providerincident/sendemailnotification')
       .subscribe(
         (response) => {
           this._alertService.success('UIR notification sent successfully!');
          (<any>$('#send-notifications')).modal('hide');
         },
         (error) => {
          this._alertService.error("Unable to send UIR notification");
         }
       );
      
    }
}