import { Component, ViewChild, forwardRef, AfterViewInit, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { DataStoreService, CommonHttpService } from '../../../../../@core/services';

@Component({
  selector: 'request-signature-field',
  templateUrl: './signature-field.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RequestSignatureFieldComponent),
      multi: true,
    },
  ],
  styleUrls: ['./signature-field.component.scss']
})
export class RequestSignatureFieldComponent implements ControlValueAccessor, AfterViewInit {
  @ViewChild(SignaturePad) public signaturePad: SignaturePad;
  // @ViewChild('signaturepad') signaturepadContainer: ElementRef;
  isSignPresent: boolean;
  public options: Object = { };

  public _signature: any = null;

  public propagateChange: Function = null;

  constructor(
    private _dataStoreService: DataStoreService,
    private _commonHttpService : CommonHttpService
  ) { }

  get signature(): any {
    return this._signature;
  }

  set signature(value: any) {
    this._signature = value;
    // console.log('set signature to ' + this._signature);
    // console.log('signature data :');
    // console.log(this.signaturePad.toData());
    this.propagateChange(this.signature);
  }

  public writeValue(value: any): void {
    if (!value) {
      return;
    }
    this._signature = value;
    this.signaturePad.fromDataURL(this.signature);
  }

  public registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(): void {
    // no-op
  }

  public ngAfterViewInit(): void {
    this.signaturePad.clear();
    // this.signaturePad.set('canvasWidth', (this.signaturepadContainer.nativeElement as HTMLElement).offsetWidth);
    // this.signaturePad.set('canvasHeight', (this.signaturepadContainer.nativeElement as HTMLElement).offsetHeight);
  }
  
  public drawBegin(): void {
    console.log('Begin Drawing');
  }

  public drawComplete(): void {
   this.signature = this.signaturePad.toDataURL('image/png');
   console.log(this.signaturePad.toDataURL());
    this._commonHttpService.updateSignature(this.signaturePad.toDataURL());
  }

  public clear(): void {
    this.signaturePad.clear();
    this.signature = '';
    this._commonHttpService.updateSignature(this.signature);
  }

}
