import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestSignatureFieldComponent } from './signature-field.component';

describe('SignatureFieldComponent', () => {
  let component: RequestSignatureFieldComponent;
  let fixture: ComponentFixture<RequestSignatureFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestSignatureFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestSignatureFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
