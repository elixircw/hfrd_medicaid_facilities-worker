import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderChangeRequestComponent } from './provider-change-request.component';

describe('ProviderChangeRequestComponent', () => {
  let component: ProviderChangeRequestComponent;
  let fixture: ComponentFixture<ProviderChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
