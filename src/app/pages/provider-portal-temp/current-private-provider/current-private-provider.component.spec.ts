import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentPrivateProviderComponent } from './current-private-provider.component';

describe('CurrentPrivateProviderComponent', () => {
  let component: CurrentPrivateProviderComponent;
  let fixture: ComponentFixture<CurrentPrivateProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentPrivateProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentPrivateProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
