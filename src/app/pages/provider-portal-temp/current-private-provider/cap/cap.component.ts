import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApplicantUrlConfig } from '../../provider-portal-temp-url.config';
import { AttachmentService } from '../../provider-portal-temp-utils/attachment/attachment.service';
import { AttachmentConfig } from '../../provider-portal-temp-utils/attachment/_entities/attachment.data.models';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { config } from '../../../../../environments/config';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { HttpHeaders } from '@angular/common/http';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';

@Component({
  selector: 'cap',
  templateUrl: './cap.component.html',
  styleUrls: ['./cap.component.scss']
})
export class CapComponent implements OnInit {
  deficiencyList: any[];
  id: any;
  showCapForm: boolean = false;
  selectedDeficiency: any;
  capForm: FormGroup;
  pageInfo: PaginationInfo = new PaginationInfo();
  totalCount: any;
  assignedWorker: any[];
  providerImage = false;
  disableCapForm: boolean;
  search_cap_number: string;
  uploadedfile: any[] = [];
  token: AppUser;
  capNaumber: string;
  deleteAttachmentIndex: number;

  constructor(
    private _commonHttpService: CommonHttpService,
    private attachService: AttachmentService,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _uploadService: NgxfUploaderService
  ) {
    this.id = route.snapshot.params['id'];
    const attachmentConfig: AttachmentConfig = new AttachmentConfig();
    // attachmentConfig.uniqueNumber = this.id;
    this.attachService.setAttachmentConfig(attachmentConfig);
  }

  ngOnInit() {
    this.resetCapForm();
    this.initiateCapForm();
    this.token = this._authService.getCurrentUser();
    this.pageInfo.pageNumber = 1;
    const capNumber = this._dataStoreService.getData('CapNumber');
    if (capNumber) {
      this.search_cap_number = capNumber;
      this.loadDeficiencyList();
    } else {
      this.loadDeficiencyList();
    }
    this.getAssignedWorker(this.id);
  }

  initiateCapForm() {
    this.capForm = this.formBuilder.group({
      prv_signimage: null,
      prv_signdate: null,
      planofcorrection: null,
      commenttext: null,
      findings: null,
      targetdate: null,
      isdisbute: null,
      disbutecomarcitation: null,
      uploadedfile: null
    });
    this.capForm.get('commenttext').disable();
    this.capForm.get('findings').disable();
  }

  loadDeficiencyList() {
  const data = { provider_id: this.id, provider_complaintid : null, cap_number: this.search_cap_number ? this.search_cap_number : null,
    sortcolumn: this.pageInfo.sortColumn,
    sortorder: this.pageInfo.sortBy};
   this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: this.pageInfo.pageSize,
        page: this.pageInfo.pageNumber,
        method: 'get',
        where: data
      }), ApplicantUrlConfig.EndPoint.Cap.ListCap
      ).subscribe(response => {
      console.log(response);
      if (response.data && response.data.length) {
        this.deficiencyList = response.data;
        this.totalCount = response.data[0].totalcount ? response.data[0].totalcount : 0;
        this._dataStoreService.setData('CapNumber', null);
      }
    });
  }

  pageChanged(page) {
    this.pageInfo.pageNumber = page;
    this.loadDeficiencyList();
  }

  openCapForm(deficiency) {
    this.disableCapForm = (deficiency && deficiency.cap_status && deficiency.cap_status !== '106' && deficiency.cap_status !== '107' && deficiency.cap_status !== '108') ? true : false;
    if (!this.disableCapForm) {
      this.capForm.enable();
      this.capForm.get('commenttext').disable();
      this.capForm.get('findings').disable();
    } else {
      this.disableCapForm = true;
      this.capForm.disable();
    }
    if (deficiency && deficiency.prv_signimage) {
      this.providerImage = true;
    } else {
      this.providerImage = false;
    }
    this.uploadedfile = deficiency.uploadedfile ? deficiency.uploadedfile : [];
    this.capForm.patchValue(deficiency);
    this.capForm.patchValue({
      commenttext: deficiency.comments
    });
    this.showCapForm = true;
    this.selectedDeficiency = deficiency;
  }

  resetCapForm() {
    this.showCapForm = false;
    this.selectedDeficiency = null;
    this.uploadedfile = [];
  }

  saveCap(action) {
    const capdata = this.capForm.getRawValue();
    capdata.assignsecurityuserid = [];
    capdata.status = 101;
    capdata.statusmsg = 'cap_submitted';
    capdata.appevent = 'PRCAP';
    capdata.providerid = this.id;
    capdata.uploadedfile = this.uploadedfile;
    capdata.notification = 'Corrective Action Plan has been Submitted to the Assigned Worker ( Cap Number :' +  this.selectedDeficiency.cap_number+ ' )' ;
    capdata.prv_signdate = new Date();
    if (this.assignedWorker && this.assignedWorker.length) {
      capdata.assignsecurityuserid = this.assignedWorker;
    } else {
      capdata.assignsecurityuserid.push((this.selectedDeficiency && this.selectedDeficiency.notificationsecurityuserid) ? this.selectedDeficiency.notificationsecurityuserid : null);
    }
    // capdata.assignsecurityuserid.push((this.assignedWorker && this.assignedWorker.length) ? this.assignedWorker :  this.selectedDeficiency.notificationsecurityuserid);
    capdata.objectid = this.selectedDeficiency.cap_number;
    capdata.routingdescription = 'Complaint Submitted';
    const decision = {
      capdata
    };
    if (action === 'submit') {
    this._commonHttpService.create(decision, ApplicantUrlConfig.EndPoint.Cap.SubmitCap).subscribe(
      (response) => {
        console.log(response);
        this._alertService.success('Cap Submitted');
        this.resetCapForm();
        this.loadDeficiencyList();
      },
      (error) => {
        console.log(error);
        this._alertService.error('Unable to save information');
      }
    );
  }  else if (action === 'draft') {
    const draftdata = this.capForm.getRawValue();
    draftdata.isdraft = 1;
    draftdata.comments = draftdata.commenttext;
    draftdata.tb_provider_complaint_deficiencyid = this.selectedDeficiency.tb_provider_complaint_deficiencyid;
    this._commonHttpService.create(
      draftdata,
      'tb_provider_complaint_deficiency/addupdate'
    ).subscribe(
      (response) => {
        console.log(response);
        this._alertService.success('Corrective Action Plan has been updated');
        this.loadDeficiencyList();
      },
      (error) => {
        console.log(error);
        this._alertService.error('Unable to save information');
      }
    );
  } else {
    this.resetCapForm();
  }
 }

 getAssignedWorker(provider) {
  this.assignedWorker = [];
  this._commonHttpService.getPagedArrayList(
    new PaginationRequest ({
      where: {
        objectid: provider
      },
      nolimit: true,
      method: 'post'
  }), 'tb_provider_complaint_deficiency/getassignedusers'
    ).subscribe(response => {
      if (response.data && response.data.length) {
        response.data.forEach(item => {
          if (item && item.fromsecurityusersid) {
            this.assignedWorker.push(item.fromsecurityusersid);
          }
        });
      }
    });
}

searchCap() {
  this.loadDeficiencyList();
}

uploadFile(file: File | FileError): void {
  if (!(file instanceof Array)) {
      return;
  }
  file.map((item, index) => {
      const fileExt = item.name
          .toLowerCase()
          .split('.')
          .pop();
      if (
          fileExt === 'mp3' ||
          fileExt === 'ogg' ||
          fileExt === 'wav' ||
          fileExt === 'acc' ||
          fileExt === 'flac' ||
          fileExt === 'aiff' ||
          fileExt === 'mp4' ||
          fileExt === 'mov' ||
          fileExt === 'avi' ||
          fileExt === '3gp' ||
          fileExt === 'wmv' ||
          fileExt === 'mpeg-4' ||
          fileExt === 'pdf' ||
          fileExt === 'txt' ||
          fileExt === 'docx' ||
          fileExt === 'doc' ||
          fileExt === 'xls' ||
          fileExt === 'xlsx' ||
          fileExt === 'jpeg' ||
          fileExt === 'jpg' ||
          fileExt === 'png' ||
          fileExt === 'ppt' ||
          fileExt === 'pptx' ||
          fileExt === 'gif'
      ) {
          this.uploadedfile.push(item);
          const uindex = this.uploadedfile.length - 1;
          if (!this.uploadedfile[uindex].hasOwnProperty('percentage')) {
              this.uploadedfile[uindex].percentage = 1;
          }

          this.uploadAttachment(uindex);
          const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
          const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
          if (audio_ext.indexOf(fileExt) >= 0) {
              this.uploadedfile[uindex].attachmenttypekey = 'Audio';
          } else if (video_ext.indexOf(fileExt) >= 0) {
              this.uploadedfile[uindex].attachmenttypekey = 'Video';
          } else {
              this.uploadedfile[uindex].attachmenttypekey = 'Document';
          }
      } else {
          // tslint:disable-next-line:quotemark
          this._alertService.error(fileExt + " format can't be uploaded");
          return;
      }
  });
}
uploadAttachment(index) {
  console.log('check');
  const workEnv = config.workEnvironment;
  let uploadUrl = '';
  if (workEnv === 'state') {
      uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
          + '?access_token=' + this.token.id + '&' + 'srno=' + this.capNaumber + '&' + 'docsInfo='; // Need to discuss about the docsInfo
      console.log('state', uploadUrl);
  } else {
      uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
          '&' + 'srno=' + this.capNaumber;
      console.log('local', uploadUrl);
  }

  this._uploadService
      .upload({
          url: uploadUrl,
          headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
          filesKey: ['file'],
          files: this.uploadedfile[index],
          process: true,
      })
      .subscribe(
          (response) => {
              if (response.status) {
                  this.uploadedfile[index].percentage = response.percent;
              }
              if (response.status === 1 && response.data) {
                  const doucumentInfo = response.data;
                  doucumentInfo.documentdate = doucumentInfo.date;
                  doucumentInfo.title = doucumentInfo.originalfilename;
                  doucumentInfo.objecttypekey = 'Court';
                  doucumentInfo.rootobjecttypekey = 'Court';
                  doucumentInfo.activeflag = 1;
                  doucumentInfo.servicerequestid = null;
                  this.uploadedfile[index] = { ...this.uploadedfile[index], ...doucumentInfo };
                  console.log(index, this.uploadedfile[index]);
                  this._alertService.success('File Upload successful.');
              }
          }, (err) => {
              console.log(err);
              this._alertService.error('Upload failed due to Server error, please try again later.');
              this.uploadedfile.splice(index, 1);
          }
      );
}
deleteAttachment() {
  this.uploadedfile.splice(this.deleteAttachmentIndex, 1);
  (<any>$('#delete-attachment-popup')).modal('hide');
}

downloadFile(s3bucketpathname) {
  const workEnv = config.workEnvironment;
  let downldSrcURL;
  if (workEnv === 'state') {
      downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
  } else {
      // 4200
      downldSrcURL = s3bucketpathname;
  }
  console.log('this.downloadSrc', downldSrcURL);
  window.open(downldSrcURL, '_blank');
}

confirmDeleteAttachment(index: number) {
  (<any>$('#delete-attachment-popup')).modal('show');
  this.deleteAttachmentIndex = index;
}

onCapSorted($event: ColumnSortedEvent) {
  this.pageInfo.sortBy = $event.sortDirection;
  this.pageInfo.sortColumn = $event.sortColumn;
  this.loadDeficiencyList();
}

}
