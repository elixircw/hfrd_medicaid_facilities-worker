import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthService, CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { NgxfUploaderService } from 'ngxf-uploader';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { HttpHeaders } from '@angular/common/http';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  selector: 'provider-sanctions',
  templateUrl: './provider-sanctions.component.html',
  styleUrls: ['./provider-sanctions.component.scss']
})
export class ProviderSanctionsComponent implements OnInit {
  sanctions:any[];
	providerId: string;

	signimage: any;
	isSignPresent: boolean;
	roleId: AppUser;

  constructor(private formbuilder: FormBuilder,
		private _alertService: AlertService,
		private _commonHttpService: CommonHttpService,
		private route: ActivatedRoute,
		private _uploadService: NgxfUploaderService,
		private _dataStoreService: DataStoreService,
		private _authService: AuthService
	) {
		this.providerId = route.snapshot.params['id'];
	}


  ngOnInit() {
		this.getSanctions();
		this.roleId = this._authService.getCurrentUser();
    this.loadSignature();
    console.log(this.roleId);
	}
	
  getSanctions() {
		this._commonHttpService.getArrayList({
			method: 'get',
			where: {
				'provider_id': this.providerId
			}
		}, 'providerlicensesanctions?filter')
		.subscribe( response => {
			this.sanctions = response;
			console.log(this.sanctions);
		});
	}

  loadSignature() {
    this._commonHttpService.getSingle({method: 'get', securityusersid: this.roleId.user.securityusersid}, 'admin/userprofile/listusersignature?filter')
      .subscribe(res => {
        this.signimage = res.usersignatureurl;
        this.checkIfSignPresent();
      });
  }

  private checkIfSignPresent() {
    this.isSignPresent = Boolean(this.signimage);
    this._dataStoreService.setData('isSignPresent', this.isSignPresent);
  }
	
  uploadAttachment() {
    console.log(this.signimage);
    const blob = this.dataURItoBlob(this.signimage);
    const finalImage = new File([blob], 'image.png');
    this._uploadService
      .upload({
        url:
          AppConfig.baseUrl +
          '/' +
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment
            .UploadAttachmentUrl +
          '?access_token=' +
          this.roleId.id +
          '&srno=userprofile',
        headers: new HttpHeaders()
          .set('access_token', this.roleId.id)
          .set('srno', 'userprofile')
          .set('ctype', 'file'),
        filesKey: ['file'],
        files: finalImage,
        process: true
      })
      .subscribe(
        response => {
          if (response && response.data && response.data.s3bucketpathname) {
            this._commonHttpService.create({
              usersignatureurl: response.data.s3bucketpathname
            }, 'admin/userprofile/updateusersignature').subscribe(res => {
              if (res.count) {
                this._alertService.success('Signature added successfully.');
                this.loadSignature();
              }
            });
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  dataURItoBlob(dataURI) {
    const binary = atob(dataURI.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
        type: 'image/png'
    });
}
}
