import { Component, ViewChild, forwardRef, AfterViewInit, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { DataStoreService } from '../../../../../@core/services';

/*
  Generated class for the SignatureField component.
  See https://angular.io/docs/ts/latest/api/core/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sanction-signature-field',
  templateUrl: 'signature-field.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SanctionSignatureFieldComponent),
      multi: true,
    },
  ],
})
export class SanctionSignatureFieldComponent implements ControlValueAccessor, AfterViewInit {
  @ViewChild(SignaturePad) public signaturePad: SignaturePad;
  // @ViewChild('signaturepad') signaturepadContainer: ElementRef;
  isSignPresent: boolean;
  public options: Object = { };

  public _signature: any = null;

  public propagateChange: Function = null;

  constructor(private _dataStoreService: DataStoreService) {
    if (this._dataStoreService.getData('isSignPresent')) {
      this.isSignPresent = this._dataStoreService.getData('isSignPresent');
      if (this.signaturePad && this.isSignPresent) {
        this.signaturePad.off();
      }
    }
   /*  this._dataStoreService.currentStore.subscribe(store => {
      this.isSignPresent = store['isSignPresent'];
      if (this.signaturePad && this.isSignPresent) {
        this.signaturePad.off();
      }
    }); */
  }

  get signature(): any {
    return this._signature;
  }

  set signature(value: any) {
    this._signature = value;
    // console.log('set signature to ' + this._signature);
    // console.log('signature data :');
    // console.log(this.signaturePad.toData());
    this.propagateChange(this.signature);
  }

  public writeValue(value: any): void {
    if (!value) {
      return;
    }
    this._signature = value;
    this.signaturePad.fromDataURL(this.signature);
  }

  public registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(): void {
    // no-op
  }

  public ngAfterViewInit(): void {
    this.signaturePad.clear();
    // this.signaturePad.set('canvasWidth', (this.signaturepadContainer.nativeElement as HTMLElement).offsetWidth);
    // this.signaturePad.set('canvasHeight', (this.signaturepadContainer.nativeElement as HTMLElement).offsetHeight);
  }

  public drawBegin(): void {
    console.log('Begin Drawing');
  }

  public drawComplete(): void {
    this.signature = this.signaturePad.toDataURL('image/png', 0.5);
    console.log(this.signaturePad.toDataURL());
  }

  public clear(): void {
    this.signaturePad.clear();
    this.signature = '';
  }
}
