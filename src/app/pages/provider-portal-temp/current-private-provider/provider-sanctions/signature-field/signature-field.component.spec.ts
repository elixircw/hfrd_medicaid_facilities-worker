import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SanctionSignatureFieldComponent } from './signature-field.component';

describe('SanctionSignatureFieldComponent', () => {
  let component: SanctionSignatureFieldComponent;
  let fixture: ComponentFixture<SanctionSignatureFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SanctionSignatureFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SanctionSignatureFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
