import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderUirComponent } from './provider-uir.component';

describe('ProviderUirComponent', () => {
  let component: ProviderUirComponent;
  let fixture: ComponentFixture<ProviderUirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderUirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderUirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
