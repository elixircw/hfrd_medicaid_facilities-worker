import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, CommonHttpService, AlertService, ValidationService, CommonDropdownsService } from '../../../../@core/services';
import { ApplicantProgramInfo, ApplicantProfileInfo } from '../_entities/portalApplicantModel';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs';
import { ProviderPortalService } from '../../../../provider-portal/provider-portal.service';
declare var $: any;
@Component({
  selector: 'provider-corporate-information',
  templateUrl: './provider-corporate-information.component.html',
  styleUrls: ['./provider-corporate-information.component.scss']
})
export class ProviderCorporateInformationComponent implements OnInit {


  providerId: string;
  //providerApplicantProfileId: string;
  @Input() isSubmitted: boolean;
  isPhoneValid: boolean;
  applicantProfileInfo: ApplicantProfileInfo = new ApplicantProfileInfo();
  stateDropDownItems$: Observable<DropdownModel>;
  applicantProfileFormGroup: FormGroup;
  deleteAddressForm: any;
  addresses = [];
  emails = [];
  phones = [];

  addressTypes = [];
  phoneTypes = [];

  addressForm: FormGroup;
  emailForm: FormGroup;
  phoneForm: FormGroup;
  suggestedAddress$: Observable<any[]>;
  countyList$: Observable<DropdownModel[]>;
  stateList$: Observable<any[]>;
  accreditationList$: Observable<any[]>;
  isYSB = false;
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _commonDropdownService: CommonDropdownsService,
    private _service: ProviderPortalService
  ) {
    this.providerId = route.snapshot.params['id'];
  }


  ngOnInit() {
    this.initializeApplicantProfileForm();
    this.initializeAddressForm();

    this.getApplicantProfileInformation();
    this.loadDropDown();
    const applications = this._service.getApplications();
    if (Array.isArray(applications) && applications.length) {
      const program = applications[0].prgram;
      if (program === 'YSB') {
        this.isYSB = true;
      } else {
        this.isYSB = false;
      }

    }
  }

  private initializeApplicantProfileForm() {
    this.applicantProfileFormGroup = this.formBuilder.group({
      tax_id_no: [null],
      provider_applicant_nm: [''],
      dba_name: [''],
      provider_applicant_first_nm: [''],
      provider_applicant_last_nm: [''],
      adr_url_tx: [''],
      accreditation: [''],
      profit: ['']
    });

  }

  private initializeAddressForm() {
    this.addressForm = this.formBuilder.group({
      applicant_id: [''],
      provider_applicant_profile_id: [''],
      object_id: [''],
      address_id: [''],
      adr_street_nm: [''],
      adr_line2: [''],
      adr_city_nm: [''],
      adr_state_cd: [''],
      adr_county_cd: [''],
      adr_zip5_no: [null],
      address_type: ['']
    });

    this.emailForm = this.formBuilder.group({
      object_id: [''],
      email: ['', ValidationService.mailFormat],

      email_unique_id: [''],
      email_type: ['']
    });

    this.phoneForm = this.formBuilder.group({
      object_id: [''],
      phone_no: ['', ValidationService.numeric],
      phone_id: [''],
      phone_type: ['']
    });

    this.addressTypes = ["Main", "Site", "Payment"];
    this.phoneTypes = ["Office", "Cell", "Fax"];
  }

  getApplicantProfileInformation() {
    this._commonHttpService.create(
      {
        // Make this and api to take provider id instead of appid
        appid: this.providerId
      },
      'providerapplicantportal/getapplprofileinfo'
    ).subscribe(response => {
      console.log("SIMAR APPLICANT PROFILE INFORMATION")
      this.applicantProfileInfo = response.data;
      console.log(JSON.stringify(this.applicantProfileInfo));
      if(this.applicantProfileInfo) {
      this.applicantProfileFormGroup.patchValue(this.applicantProfileInfo);
      }
      // if( response.data.addresses != null) {
      //   this.addresses = response.data.addresses;
      // }
      if (response.data.emails != null) {
        this.emails = response.data.emails;
      }
      if (response.data.phones != null) {
        this.phones = response.data.phones;
      }
      if (response.data.addresses != null) {
        this.addresses = response.data.addresses;
      }
    },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }

  saveApplicantProfileInformation() {
    var payload = new ApplicantProfileInfo();
    payload = this.applicantProfileFormGroup.value;

    payload.applicant_id = this.providerId;

    this._commonHttpService.update('',
      payload,
      'providerapplicantportal/updateapplicantprofile'
    ).subscribe(response => {
      console.log(response)
      this._alertService.success('Profile Information Saved Successfully!');
    },
      (error) => {
        this._alertService.error('Unable to save profile information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }

  saveAddress(obj) {
    //obj.provider_applicant_profile_id = 
    obj.applicant_id = this.providerId;
    obj.provider_applicant_profile_id = this.applicantProfileInfo.provider_applicant_profile_id;
    //console.log("createOrUpdateAppointment:-" + obj);.
    if (obj.address_id) {
      this._commonHttpService.create(obj, 'providerapplicantportal/updateapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Updated successfully!');
          this.getApplicantProfileInformation();
          // this.addresses.unshift(obj);
        },
        (error) => {
          this._alertService.error('Unable to Update address, please try again.');
          console.log('Save address Error', error);
          return false;
        }
      );
    }
    else {
      this._commonHttpService.create(obj, 'providerapplicantportal/addapplicantaddress').subscribe(
        (response) => {
          this._alertService.success('Address Created successfully!');
          this.getApplicantProfileInformation();
          // this.addresses.unshift(obj);
        },
        (error) => {
          this._alertService.error('Unable to Create address, please try again.');
          console.log('Save address Error', error);
          return false;
        }
      );
    }
    this.resetForm();
    (<any>$('#intake-aptmnt')).modal('hide');
  }

  saveEmail(obj) {
    obj.object_id = this.providerId;
    obj.provider_applicant_profile_id = this.applicantProfileInfo.provider_applicant_profile_id;
    //console.log("createOrUpdateAppointment:-" + obj);
    this._commonHttpService.create(
      {
        where: {
          object_id: 'A201800500014',
          email: obj.email,
          email_type: obj.email_type
        }
      },

      'providerapplicantportal/addappemail').subscribe(
        (response) => {
          this._alertService.success('Email Created successfully!');
          this.emails.unshift(obj);
        },
        (error) => {
          this._alertService.error('Unable to Create email, please try again.');
          console.log('Save address Error', error);
          return false;
        }
      );
    (<any>$('#intake-email')).modal('hide');
    this.resetForm();

  }

  savePhone(obj) {
    // if(this.isPhoneValid){
    obj.object_id = this.providerId;
    obj.provider_applicant_profile_id = this.applicantProfileInfo.provider_applicant_profile_id;

    // Update existing record
    if (obj.object_id && obj.phone_id) {
      this._commonHttpService.update('',
        obj,
        'providerapplicantportal/updateappphone'
      ).subscribe(response => {
        console.log(response)

        this._alertService.success('Phone Updated successfully!');
        this.getApplicantProfileInformation();
      },
        (error) => {
          this._alertService.error('Unable to Create phone, please try again.');
          console.log('Save phone Error', error);
          return false;
        }
      );
    }
    // Create a new record
    else {
      this._commonHttpService.create(
        {
          where: {
            object_id: this.applicantProfileInfo.provider_applicant_profile_id,
            phone_no: obj.phone_no,
            phone_type: obj.phone_type
          }
        },
        'providerapplicantportal/addappphone').subscribe(
          (response) => {
            this._alertService.success('Phone Created successfully!');
            this.getApplicantProfileInformation();
          },
          (error) => {
            this._alertService.error('Unable to Create phone, please try again.');
            console.log('Save phone Error', error);
            return false;
          }
        );
    }
    this.resetForm();
    (<any>$('#intake-phone')).modal('hide');
    // } else   {
    //     this._alertService.error('Unable to Create phone, please enter valid number.');
    // }

  }


  EditPhone(editObj) {
    this.phoneForm.patchValue({
      object_id: editObj.object_id,
      phone_id: editObj.phone_id,
      phone_no: editObj.phone_no,
      phone_type: editObj.phone_type
    });
    (<any>$('#intake-phone')).modal('show');
  }

  DeletePhone(Obj) {
    (<any>$('#delet-phone')).modal('show');
  }


  EditAddress(editObj) {
    console.log(editObj)
    this.addressForm.patchValue({
      object_id: editObj.object_id,
      address_id: editObj.address_id,
      adr_street_nm: editObj.adr_street_nm,
      adr_line2: editObj.adr_line2,
      adr_city_nm: editObj.adr_city_nm,
      adr_state_cd: editObj.adr_state_cd,
      adr_county_cd: editObj.adr_county_cd,
      adr_zip5_no: editObj.adr_zip5_no,
      address_type: editObj.address_type
    });
    (<any>$('#intake-aptmnt')).modal('show');
  }

  DeleteAddress(Obj) {
    this.deleteAddressForm = Obj;
    (<any>$('#delete-aptmnt')).modal('show');
  }

  CancelDelete() {
    (<any>$('#delete-aptmnt')).modal('hide');
  }

  DeleteAddressConfirm() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          address_id: this.deleteAddressForm.address_id
        }
      },
      'providerapplicantportal/deleteapplicantaddress')
      .subscribe(response => {
        if (response) {
          this._alertService.success("Address Deleted successfully");
          this.getApplicantProfileInformation();
        }
      });
    (<any>$('#delete-aptmnt')).modal('hide');
  }



  resetForm() {
    this.addressForm.reset();
    this.emailForm.reset();
    this.phoneForm.reset();
  }

  phoneValidation(event: any) {
    this.isPhoneValid = false;
    const pattern = /^\d{10}$/;
    if (!pattern.test(event.target.value)) {
      this.isPhoneValid = false;
    }
    else {
      this.isPhoneValid = true;
    }
  }
  private loadDropDown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        'States?filter'
      ),
    ])
      .map((result) => {
        return {
          stateList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();

    // this.stateDropDownItems$ = source.pluck('stateList');

    this.stateList$ = this._commonDropdownService.getPickListByName('state');
    this.countyList$ = this._commonDropdownService.getPickList('328');
    this.accreditationList$ = this._commonDropdownService.getPickListByName('accreditation');
  }

  getSuggestedAddress() {
    this.suggestAddress();
  }

  suggestAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.addressForm.value.adr_street_nm,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        'People/suggestaddress'
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
            this.suggestedAddress$ = result;
          }
        }
      );
  }
  selectedAddress(model) {
    this.addressForm.patchValue({
      adr_street_nm: model.streetLine ? model.streetLine : '',
      adr_city_nm: model.city ? model.city : '',
      adr_state_cd: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        'People/validateaddress'
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.addressForm.patchValue({
              adr_zip5_no: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.addressForm.value.adr_state_cd, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  if (resultresp && resultresp.length) {
                    this.addressForm.patchValue({
                      adr_county_cd: resultresp[0].ref_key
                    });
                  }
                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  loadCounty(state) {
    console.log(state);
    // this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
    //  this.countyList$ = Observable.of(countyList);
    // });
  }

}
