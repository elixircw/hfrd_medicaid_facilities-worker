import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderCorporateInformationComponent } from './provider-corporate-information.component';

describe('ProviderCorporateInformationComponent', () => {
  let component: ProviderCorporateInformationComponent;
  let fixture: ComponentFixture<ProviderCorporateInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderCorporateInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderCorporateInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
