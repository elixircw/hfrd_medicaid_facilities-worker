import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { AlertService } from '../../../@core/services/alert.service';
import { DataStoreService } from '../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { ProviderPortalService } from '../../../provider-portal/provider-portal.service';
@Component({
  selector: 'current-private-provider',
  templateUrl: './current-private-provider.component.html',
  styleUrls: ['./current-private-provider.component.scss']
})
export class CurrentPrivateProviderComponent implements OnInit {
  complaintNo: any;
  capNo: any;
  providerId: any;
  licenseInfo: any;
  licenseLevelInputForMonitoring = new Subject<string>();
  isYSB = false;
  constructor(
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private route: ActivatedRoute,
    private _service: ProviderPortalService
  ) {


    this.providerId = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.complaintNo = this._dataStoreService.getData('ComplaintNumber');
    this.capNo = this._dataStoreService.getData('CapNumber');
    this.getLicenseInformation();
    if (this.complaintNo && this.complaintNo !== '') {
      (<any>$('#provider-document')).click();
    }
    if (this.capNo && this.capNo !== '') {
      (<any>$('#cap-provider')).click();
    }

    console.log('applications', this._service.getApplications());
    const applications = this._service.getApplications();
    if (Array.isArray(applications) && applications.length) {
      const program = applications[0].prgram;
      if (program === 'YSB') {
        this.isYSB = true;
      } else {
        this.isYSB = false;
      }

    }
  }
  getLicenseInformation() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          objectid: this.providerId
        }
      },
      'providerlicense/getproviderlicensing'
    ).subscribe(response => {
      console.log("Before", JSON.stringify(this.licenseInfo));

      this.licenseInfo = response.data[0];

      console.log("After", JSON.stringify(this.licenseInfo));

      if (this.licenseInfo) {
        this.licenseLevelInputForMonitoring.next(this.licenseInfo.license_level);
      }

    },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }

}
