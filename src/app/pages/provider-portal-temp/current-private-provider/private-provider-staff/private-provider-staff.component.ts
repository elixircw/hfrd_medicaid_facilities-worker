import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// import { ApplicantStaffDetails } from '../_entities/newApplicantModel';
import { AlertService } from '../../../../@core/services/alert.service';
import { ContactDetailsModule } from '../../../person-details/contact-details/contact-details.module';
import * as moment from 'moment';
import { ProviderPortalService } from '../../../../provider-portal/provider-portal.service';
import { MatStepper } from '@angular/material';
import { ValidationService } from '../../../../@core/services';
@Component({
  selector: 'private-provider-staff',
  templateUrl: './private-provider-staff.component.html',
  styleUrls: ['./private-provider-staff.component.scss']
})
export class PrivateProviderStaffComponent implements OnInit {

  // staffArray : ApplicantStaffDetails[] = [];
  providerId: string;
  staffForm: FormGroup;
  affiliationType = [];
  employeeType = [];
  reasonForLeaving = [];
  rccCertificateType = [];
  certType: any[] =[];
  certSubType: any[] = [];
  trainingNames: any[] = [];
  showCertification: boolean;
  showTraining: boolean;
  showCertificationForm: boolean;
  currentSelectedProviderStaffId: string;
  staffInformation = [];
  isEditStaff: boolean;
  isEndDate: boolean;
  isTrainingdate: boolean;
  currenbtStaffInfo = [];
  groupingLog: any;
  logStaffInfo: any[] = [];
  myDate = new Date();
  applications = [];
  stepper: MatStepper;
  isValid: boolean;
  url: string;
  staffClearanceForm: FormGroup;
  assignProgramForm: FormGroup;
  certificateForm: FormGroup;
  trainingForm: FormGroup;
  staffName: any;
  certificates: any[] = [];
  trainings: any[] = [];
  selectedIndex: any;
  staffClearanceHistory: any[] = [];
  showTrainingForm: boolean;
  trainingHistoryList: any[] = [];
  

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _providerPortalService: ProviderPortalService) {
    this.providerId = route.snapshot.params['id'];
    this.applications = this._providerPortalService.getApplications();
  }

 ngOnInit() {
   this.initializeStaffForm();
   this.initCertificateForm();
   this.initTrainingForm();
   this.getStaffInformation();
   this.getTrainingNames();
   this.getCertType();
   this.getCertSubType(607);
   this.affiliationType = [ 'Board Member','Employee','Intern Information', 'Temp Employee', 'Volunteer', ];
   this.employeeType = ['Other Employees','Program Administrator', 'Residential Child and youth care practitioner', 'N/A'];
   this.reasonForLeaving = ['Death', 'Resigned', 'Retired', 'Terminated'];
   this.rccCertificateType = ['Certification Pending', 'Certified Program Administrator', 'Residential Child and youth care practitioner certification'];
   this.showCertification = false;
   this.showTraining = false;
   this.isEditStaff = false;
   this.isEndDate = false;
   this.isTrainingdate = false;
 }

 trainingChange(value) {

 }

 certificationChange(value) {
   if (value === 'Certification Pending') {
    this.resetCertificateFields();
  //   this.staffForm.get('rcc_certificate_due_date').clearValidators();
  //   this.staffForm.get('rcc_date_application_mailed').clearValidators();
  //   this.staffForm.get('rcc_date_tested').clearValidators();
  //   this.staffForm.get('rcc_certification_effective_date').clearValidators();
  //   this.staffForm.get('rcc_certification_number').clearValidators();
  //   this.staffForm.get('rcc_certification_renewal_date').clearValidators();
  //   this.staffForm.get('rcc_certificate_due_date').updateValueAndValidity();
  //   this.staffForm.get('rcc_date_application_mailed').updateValueAndValidity();
  //   this.staffForm.get('rcc_date_tested').updateValueAndValidity();
  //   this.staffForm.get('rcc_certification_effective_date').updateValueAndValidity();
  //   this.staffForm.get('rcc_certification_number').updateValueAndValidity();
  //   this.staffForm.get('rcc_certification_renewal_date').updateValueAndValidity();
  //   this.staffForm.get('rcc_certificate_due_date').reset();
  //   this.staffForm.get('rcc_date_application_mailed').reset();
  //   this.staffForm.get('rcc_date_tested').reset();
  //   this.staffForm.get('rcc_certification_effective_date').reset();
  //   this.staffForm.get('rcc_certification_number').reset();
  //   this.staffForm.get('rcc_certification_renewal_date').reset();

  } else {
  //   this.staffForm.get('rcc_certificate_due_date').setValidators(Validators.required);
  //   this.staffForm.get('rcc_date_application_mailed').setValidators(Validators.required);
  //   this.staffForm.get('rcc_date_tested').setValidators(Validators.required);
  //   this.staffForm.get('rcc_certification_effective_date').setValidators(Validators.required);
  //   this.staffForm.get('rcc_certification_number').setValidators(Validators.required);
  //   this.staffForm.get('rcc_certification_renewal_date').setValidators(Validators.required);
  //   this.staffForm.get('rcc_certificate_due_date').updateValueAndValidity();
  //   this.staffForm.get('rcc_date_application_mailed').updateValueAndValidity();
  //   this.staffForm.get('rcc_date_tested').updateValueAndValidity();
  //   this.staffForm.get('rcc_certification_effective_date').updateValueAndValidity();
  //   this.staffForm.get('rcc_certification_number').updateValueAndValidity();
  //   this.staffForm.get('rcc_certification_renewal_date').updateValueAndValidity();
  }
 }
 private initializeStaffForm() {
   this.staffForm = this.formBuilder.group({
     object_id: this.providerId,
     first_nm: [''],
     middle_nm: [''],
     last_nm: [''],
     affiliation_type: [''],
     job_title: [''],
     employee_type: null,
     start_date: null,
     end_date: null,
     reason_for_leaving: [''],

    //  behavioral_interventions_training: null,
    //  rcc_certificate_type: null,
    //  rcc_certificate_due_date: null,
    //  rcc_date_application_mailed: null,
    //  rcc_date_tested: null,
    //  rcc_certification_effective_date: null,
    //  rcc_certification_number: [''],
    //  rcc_certification_renewal_date: null,
     staff_comment: [''],
     staff_email: ['', [ValidationService.mailFormat]],
     staff_phone_no: null,
     provider_staff_id: null
   });
   this.initializeClearanceForm();
   this.initializeAssignProgram();
 }

 private initCertificateForm() {
  this.certificateForm = this.formBuilder.group({
    behavioral_interventions_training: null,
    rcc_certificate_type: null,
    rcc_certificate_due_date: null,
    rcc_date_application_mailed: null,
    rcc_date_tested: null,
    rcc_certification_effective_date: null,
    rcc_certification_number: [''],
    rcc_certification_renewal_date: null,
    rcc_certificate_status: null,
    rcc_certificate_subtype: null,
    rcc_training_name: null,
    rcc_certificate_subtype_other: null,
    //prov_training_names: null,
    provider_certificate_id: null,
    provider_staff_id: null,
  });
 }

 private initTrainingForm() {
   this.trainingForm = this.formBuilder.group({
     //rcc_training_name: null,
     rcc_training_name: null,
     provider_training_hrs: null,
     provider_training_date: null,
     provider_staff_id: null,
     provider_training_id: null
   });
 }

 private initializeClearanceForm() {
   this.staffClearanceForm = this.formBuilder.group({
    cps_clearance_request_date: null,
    cps_clearance_result_date: null,
    cps_rcc_compliant: null,
    cps_cpa_compliant: null,
    federal_clearance_request_date: null,
    federal_clearance_result_date: null,
    federal_rcc_compliant: null,
    federal_cpa_compliant: null,
    state_clearance_request_date: null,
    state_clearance_result_date: null,
    state_rcc_compliant: null,
    state_cpa_compliant: null,
   });
 }

 private initializeAssignProgram() {
  this.assignProgramForm = this.formBuilder.group({
    program_type: null,
  });
 }

 addCertificate() {
   this.resetCertificateForm();
   this.showCertificationForm = true;
 }

 addTraining() {
   this.resetTrainingForm();
   this.showTrainingForm = true;
 }

 resetTrainingForm() {
  this.trainingForm.reset();
  this.selectedIndex = null;
  this.showTrainingForm = false;
  this.staffClearanceHistory = [];
  this.trainingHistoryList = [];
  }

 resetCertificateForm() {
  this.certificateForm.reset();
  this.selectedIndex = null;
  this.showCertificationForm = false;
  this.staffClearanceHistory = [];
 }

 deleteCertificate(index) {
   this.selectedIndex = index;
  (<any>$('#delete-certificate')).modal('show');
  this.resetCertificateForm();
 }

 confirmCertDelete() {
  this.certificates.splice(this.selectedIndex, 1);
  this.selectedIndex = null;
 }

 deleteTraining(index) {
  this.selectedIndex = index;
 (<any>$('#delete-training')).modal('show');
 this.resetTrainingForm();
}

 confirmTrainingDelete() {
   this.trainings.splice(this.selectedIndex, 1);
   this.selectedIndex = null;
 }

 resetCertificates() {
  this.certificates = [];
 }

 cancel(value) {
   if (value === 'Certificate') {
        this.resetCertificateForm();
   } else if (value === 'Training') {
        this.resetTrainingForm();
   }
 }

 save(value) {
   if (value === 'Certificate') {
    const certificateData = this.certificateForm.getRawValue(); 
    if (this.selectedIndex >= 0 && this.selectedIndex  !== null) {
    this.certificates[this.selectedIndex] = certificateData;
   } else {
  this.certificates.push(certificateData); }
  this.resetCertificateForm();
   } else if (value === 'Training') {
    const trainingData = this.trainingForm.getRawValue();
    trainingData.provider_staff_id = this.currentSelectedProviderStaffId;
    if (this.selectedIndex >= 0 && this.selectedIndex !== null) {
     this.trainings[this.selectedIndex] = trainingData;
     this.resetTrainingForm();
    } else {
      this.trainings.push(trainingData);
      this.resetTrainingForm();
    }
     this.createProviderTraining('Training', false);
   }
 }


 getTrainingNames() {
  this._commonHttpService.getArrayList({
  nolimit: true,
  where: { referencetypeid: 605 }, order: 'displayorder ASC', method: 'get'
  },
  'referencetype/gettypes?filter').subscribe(res => {
    this.trainingNames = res;
  });
 }
 
 getCertType() {    
  this._commonHttpService.getArrayList({
  nolimit: true,
  where: { referencetypeid: 606 }, order: 'displayorder ASC', method: 'get'
  },
  'referencetype/gettypes?filter').subscribe(res => {
    this.certType = res;
  });
 }

 onChangeCertType(value) {
   let id;
   switch(value) {
     case 'General': id = 607;break;
     case 'Social Worker': id = 608;break;
     case 'Teacher': id = 609;break;
     default:  id = 607;
   }
   this.getCertSubType(id);
 }

 resetCertificateFields() {
    this.certificateForm.patchValue({
      rcc_date_application_mailed : null,
      rcc_certification_effective_date: null,
      rcc_certification_renewal_date: null,
      rcc_certification_due_date: null,
      rcc_date_tested: null,
      rcc_certification_number: null
    });
 }

 getCertSubType(id) {    
   
  this._commonHttpService.getArrayList({
  nolimit: true,
  where: { referencetypeid: id }, order: 'displayorder ASC', method: 'get'
},
  'referencetype/gettypes?filter').subscribe(res => {
    this.certSubType = res;
  });
 }
 
 goToCertificate() {
  (<any>$('#staff_cert_section')).click();
 }

 createProviderStaff(section) {
   console.log(this.staffForm.value);
   if (section === 'Personnel Section' && !(this.currentSelectedProviderStaffId)) {

    if (this.staffForm.valid) {
      (<any>$('#staff_cert_section')).click();
      this._commonHttpService.create(
        this.staffForm.value,
        'providerstaff/addproviderstaff'
      ).subscribe(
        (response) => {
          this._alertService.success('Information saved, Please add clearance info!');
          if (section === 'Clearance') {
          //  stepper.next();
          }
          this.currentSelectedProviderStaffId = (response && response.length) ? response[0].providerstaff_id : null;
        },
        (error) => {
          this._alertService.error('Unable to save information');
        }
      );
    }
   } else if (section === 'Clearance') {
    // stepper.selectedIndex = 1;
    if (this.staffClearanceForm.valid) {
      if ((this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false' ) ||
      (this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === null) ||
      (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false') ||
      (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === null)) {
        this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
        return false;
      }
      if ((this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === 'false' ) ||
      (this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === null) ||
      (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === 'false') ||
      (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === null)) {
        this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
        return false;
      }
      if ((this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false' ) ||
      (this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === null) ||
      (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false') ||
      (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === null)) {
        this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
        return false;
      }
    } else {
      this._alertService.error('Please fill the mandatory values');
      return false;
    }

    if (this.staffClearanceForm.valid) {
      (<any>$('#assign_program_section')).click();
      if(this.staffClearanceForm.pristine) {
        return;
      }
    const staffformData = this.staffForm.getRawValue();
    const clearanceForm = this.staffClearanceForm.getRawValue();
    // const assignProgram = this.assignProgramForm.getRawValue();
    const data = Object.assign({}, staffformData, clearanceForm);
    data.provider_staff_id = this.currentSelectedProviderStaffId;
     this._commonHttpService.create(data, 'providerstaff/addstaffclereance').subscribe(
       (response) => {
         this._alertService.success('Information saved, Please add Assign Program info!');
         this.getStaffClearanceHistory(this.currentSelectedProviderStaffId);
       },
       (error) => {
         this._alertService.error('Unable to save information');
       }
     );
   }
   } else {
     this.nextPatchProviderStaff(section);
   }
}


nextPatchProviderCertificate(section) {
  if (section !== 'Personnel Section') {
   // stepper.selectedIndex = 1;
    if (this.staffClearanceForm.valid) {
      if ((this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false' ) ||
      (this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === null) ||
      (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false') ||
      (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === null)) {
        this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
        return false;
      }
      if ((this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === 'false' ) ||
      (this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === null) ||
      (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === 'false') ||
      (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === null)) {
        this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
        return false;
      }
      if ((this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false' ) ||
      (this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === null) ||
      (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false') ||
      (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === null)) {
        this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
        return false;
      }
    } else {
      this._alertService.error('Kindly fill the mandatory fields in Clearance Section');
      return false;
    }
   } else {
     if (this.staffForm.invalid) {
      this._alertService.error('Kindly fill the mandatory fields in Personnel Section');
      return false;
     } else {
      (<any>$('#clearance_section')).click();
     }
   }
  if (this.isEditStaff && this.groupingLog) {
    this.staffForm.value.groupinglog = this.groupingLog;
  }
  const staffformData = this.staffForm.getRawValue();
  const clearanceForm = this.staffClearanceForm.getRawValue();
  // const assignProgram = this.assignProgramForm.getRawValue();
  const formData = Object.assign({}, staffformData, clearanceForm);
 formData.provider_staff_id = this.currentSelectedProviderStaffId;
  this.url = 'providerstaff';
     this._commonHttpService.patch(
      this.currentSelectedProviderStaffId,
      formData,
      this.url
    ).subscribe(
      (response) => {
        if (section === 'Clearance') {
          (<any>$('#assign_program_section')).click();
         }
        this.getStaffInformation();
       // this.resetForm();
        this._alertService.success('Information saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
}

editCertificate(item, index) {
  this.selectedIndex = index;
  this.showCertificationForm = true;
  console.log(item);
  let certData = item;
  let itemNames = [];
  if(item.trainingnames && item.trainingnames.length) {
    item.trainingnames.forEach(tName => {
      itemNames.push(tName.rcc_training_name);
    });
  }
  certData.rcc_training_name = itemNames && itemNames.length ? itemNames : item.rcc_training_name;
  console.log(certData);
  console.log(itemNames);
  console.log(item.rcc_training_name);
  this.certificateForm.patchValue(certData);
}

editTraining(item, index) {
  this.selectedIndex = index;
  this.showTrainingForm = true;
  console.log(item);
  const trainingData = item; 
  console.log(item.rcc_training_name);
  this.trainingForm.patchValue(trainingData);  
}
getTrainingDetail() {
  const trainingData = {
   provider_staff_id : this.currentSelectedProviderStaffId
  };
  this._commonHttpService.create(
   trainingData,
   'Providerstaff/getprovidertraining'
 ).subscribe(
   (response) => {
    this.trainings = response && response.length && response[0].training_details ? response[0].training_details: [];
   },
   (error) => {
     this._alertService.error('Unable to save information');
   }
 );

}
trainingHistory(item, index) {
   (<any>$('#training-history-details')).modal('show');
   const trainingData = {
    provider_staff_id : this.currentSelectedProviderStaffId,
    provider_training_id: item.provider_training_id,
    rcc_training_name: item.rcc_training_name
   };
   this._commonHttpService.create(
    trainingData,
    'Providerstaff/getprovidertraininghistory'
  ).subscribe(
    (response) => {
      this.trainingHistoryList = ( response && response.length && response[0].training_history.length ) ? response[0].training_history : [];
    },
    (error) => {
      this._alertService.error('Unable to save information');
    }
  );
}

createProviderTraining(section, isNext) {
  const staffData = this.staffForm.value;
  staffData.provider_staff_id = this.currentSelectedProviderStaffId;
  staffData.trainings = this.trainings;
  const trainingData = { provider_staff_id: this.currentSelectedProviderStaffId,
   training_details : this.trainings};
  if(this.staffForm.valid) {
    if(isNext) {
    (<any>$('#clearance_section')).click();
    }
    this._commonHttpService.create(
      trainingData,
      'Providerstaff/addupdateprovidertraining'
    ).subscribe(
      (response) => {
        this._alertService.success('Information saved, Please add clearance info!');
        this.getTrainingDetail();
        if (section === 'Clearance') {
        //  stepper.next();
        }
        this.currentSelectedProviderStaffId = (response && response.length) ? response[0].providerstaff_id : null;
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
  }

}

createProviderCertificate(section) {
  const staffData = this.staffForm.value;
  staffData.provider_staff_id = this.currentSelectedProviderStaffId;
  staffData.certificates = this.certificates;

   if (this.staffForm.valid) {
     (<any>$('#staff_training_section')).click();
     this._commonHttpService.create(
       this.staffForm.value,
       'providerstaff/addproviderstaff'
     ).subscribe(
       (response) => {
         this._alertService.success('Information saved, Please add clearance info!');
         if (section === 'Clearance') {
         //  stepper.next();
         }
         this.currentSelectedProviderStaffId = (response && response.length) ? response[0].providerstaff_id : null;
       },
       (error) => {
         this._alertService.error('Unable to save information');
       }
     );
   }
  
}

requestDateChange(form) {
  if (form.value.cps_clearance_request_date && (form.value.cps_clearance_request_date > form.value.cps_clearance_response_date)) {
    this.staffClearanceForm.get('cps_clearance_response_date').reset();
  }
  if (form.value.state_clearance_request_date && (form.value.state_clearance_request_date > form.value.state_clearance_result_date)) {
    this.staffClearanceForm.get('state_clearance_result_date').reset();
  }
  if (form.value.federal_clearance_request_date && (form.value.federal_clearance_request_date > form.value.federal_clearance_result_date)) {
    this.staffClearanceForm.get('federal_clearance_result_date').reset();
  }
}

  patchProviderStaff(section?) {
    // if (section !== 'Personnel Section') {
      if (this.staffClearanceForm.valid) {
        if ((this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false' ) ||
        (this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === null) ||
        (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false') ||
        (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === null)) {
          this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
          return false;
        }
        if ((this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === 'false' ) ||
        (this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === null) ||
        (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === false) ||
        (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === null)) {
          this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
          return false;
        }
        if ((this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false' ) ||
        (this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === null) ||
        (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false') ||
        (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === null)) {
          this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
          return false;
        }
     // }
     } else {
      (<any>$('#clearance_section')).click();
       this._alertService.error('Please fill mandatory fileds in Clearance Section');
       return false;
     }
     if (this.isEditStaff && this.groupingLog) {
      this.staffForm.value.groupinglog = this.groupingLog;
    }
    const staffformData = this.staffForm.getRawValue();
    const clearanceForm = this.staffClearanceForm.getRawValue();
    const assignProgram = this.assignProgramForm.getRawValue();
    const formData = Object.assign({}, staffformData, clearanceForm, assignProgram);
    // if (section === 'Assign Program') {
       formData.provider_staff_id = this.currentSelectedProviderStaffId;
      this.url = 'providerstaff/updateproviderstaff';
      this._commonHttpService.create(
        formData,
        this.url
      ).subscribe(
        (response) => {
          this.getStaffInformation();
          this.resetForm();
          this._alertService.success('Information saved successfully!');
        },
        (error) => {
          this._alertService.error('Unable to save information');
        }
      );
      (<any>$('#add-staff')).modal('hide');
    // }
     /* else {
       this.url = 'providerstaff';
       this._commonHttpService.patch(
        this.currentSelectedProviderStaffId,
        formData,
        this.url
      ).subscribe(
        (response) => {
          this.getStaffInformation();
          this.resetForm();
          this._alertService.success('Information saved successfully!');
        },
        (error) => {
          this._alertService.error('Unable to save information');
        }
      );
      (<any>$('#add-staff')).modal('hide');
     } */

  }

  getStaffInformation() {
    this._commonHttpService.create(
      {
        where: { provider_id: this.providerId },
        method: 'post'
      },
      'providerstaff/getstaffdetailsbyprovider'
    ).subscribe(
      (response) => {
        if (response && response.length) {
          const data = response.map((item) => {
            item.isbuttionshow = this.getDaysDiff(item.rcc_certification_renewal_date);
            return item;
          });
          this.staffInformation = data;
        } else {
          this.staffInformation = [];
        }
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  
  private getDaysDiff(renewal_date) {
    const endDate = moment(new Date(), "DD.MM.YYYY");
    const startDate = moment(new Date(renewal_date), "DD.MM.YYYY");
    return startDate.diff(endDate, 'days');
  }
  editStaffForm(editStaff) {
    (<any>$('#personnel_section')).click();
    this.resetCertificateForm();
    this.currentSelectedProviderStaffId = editStaff.provider_staff_id;
    this.getStaffClearanceHistory(this.currentSelectedProviderStaffId);
    this.getTrainingDetail();
    (<any>$('#add-staff')).modal('show');
    this.isEditStaff = true;
    this.groupingLog = editStaff.groupinglog;
    this.staffForm.patchValue({
      object_id: this.providerId,
      first_nm: editStaff.first_nm,
      last_nm: editStaff.last_nm,
      affiliation_type: editStaff.affiliation_type,
      job_title: editStaff.job_title,
      employee_type: editStaff.employee_type,
      start_date: editStaff.start_date,
      end_date: editStaff.end_date,
      reason_for_leaving: editStaff.reason_for_leaving,
      behavioral_interventions_training: editStaff.behavioral_interventions_training,
      rcc_certificate_type: editStaff.rcc_certificate_type,
      rcc_certificate_due_date: editStaff.rcc_certificate_due_date,
      rcc_date_application_mailed: editStaff.rcc_date_application_mailed,
      rcc_date_tested: editStaff.rcc_date_tested,
      rcc_certification_effective_date: editStaff.rcc_certification_effective_date,
      rcc_certification_number: editStaff.rcc_certification_number,
      rcc_certification_renewal_date: editStaff.rcc_certification_renewal_date,
      staff_comment: editStaff.staff_comment,
      staff_email: editStaff.staff_email,
      staff_phone_no: editStaff.staff_phone_no
    });
    if(editStaff.certificates) {
      this.certificates = editStaff.certificates;
     this.certificateForm.patchValue({
      behavioral_interventions_training: editStaff.certificates.behavioral_interventions_training,
      rcc_certificate_type: editStaff.certificates.rcc_certificate_type,
      rcc_certificate_due_date: editStaff.certificates.rcc_certificate_due_date,
      rcc_date_application_mailed: editStaff.certificates.rcc_date_application_mailed,
      rcc_date_tested: editStaff.certificates.rcc_date_tested,
      rcc_certification_effective_date: editStaff.certificates.rcc_certification_effective_date,
      rcc_certification_number: editStaff.certificates.rcc_certification_number,
      rcc_certification_renewal_date: editStaff.certificates.rcc_certification_renewal_date,
      rcc_certificate_status: editStaff.certificates.rcc_certificate_status,
      rcc_certificate_subtype: editStaff.certificates.rcc_certificate_subtype,
      rcc_training_name: editStaff.certificates.rcc_training_name,
      rcc_certificate_subtype_other: editStaff.certificates.rcc_certificate_subtype_other,
      //rcc_training_name: editStaff.certificates.trainingnames
    });
    }
    //  this.trainingForm.patchValue({
    //   rcc_training_name: editStaff.trainings.trainingnames,
    //   provider_training_date: editStaff.trainings.provider_training_date,
    //   provider_training_hrs: editStaff.trainings.provider_training_hrs,
    //   provider_training_id:  editStaff.trainings.provider_training_id
    //   });

    this.staffClearanceForm.patchValue({
      cps_clearance_request_date: editStaff.cps_clearance_request_date,
      cps_clearance_result_date: editStaff.cps_clearance_result_date,
      cps_rcc_compliant: editStaff.cps_rcc_compliant,
      cps_cpa_compliant: editStaff.cps_cpa_compliant,
      federal_clearance_request_date: editStaff.federal_clearance_request_date,
      federal_clearance_result_date: editStaff.federal_clearance_result_date,
      federal_rcc_compliant: editStaff.federal_rcc_compliant,
      federal_cpa_compliant: editStaff.federal_cpa_compliant,
      state_clearance_request_date: editStaff.state_clearance_request_date,
      state_clearance_result_date: editStaff.state_clearance_result_date,
      state_rcc_compliant: editStaff.state_rcc_compliant,
      state_cpa_compliant: editStaff.state_cpa_compliant,
    });
    if (editStaff && editStaff.json_agg && editStaff.json_agg.length) {
      const programType = [];
        editStaff.json_agg.forEach(element => {
          programType.push(element.applicant_id);
      });
      this.assignProgramForm.patchValue({
        program_type: programType
      });
    }
    this.isEmployeeType(editStaff.employee_type);
  }
  isEmployeeType(employeeType) {
    if (employeeType === 'Residential Child and youth care practitioner' || employeeType === 'Program Administrator') {
      this.showCertification = true;
      this.showTraining = true;
    } else if (employeeType === 'N/A') {
      this.showCertification = false;
      this.showTraining = false;
      this.staffForm.get('rcc_certificate_type').clearValidators();
      this.staffForm.get('rcc_certificate_due_date').clearValidators();
      this.staffForm.get('rcc_date_application_mailed').clearValidators();
      this.staffForm.get('rcc_date_tested').clearValidators();
      this.staffForm.get('rcc_certification_effective_date').clearValidators();
      this.staffForm.get('rcc_certification_number').clearValidators();
      this.staffForm.get('rcc_certification_renewal_date').clearValidators();
      this.staffForm.get('rcc_certificate_type').updateValueAndValidity();
      this.staffForm.get('rcc_certificate_due_date').updateValueAndValidity();
      this.staffForm.get('rcc_date_application_mailed').updateValueAndValidity();
      this.staffForm.get('rcc_date_tested').updateValueAndValidity();
      this.staffForm.get('rcc_certification_effective_date').updateValueAndValidity();
      this.staffForm.get('rcc_certification_number').updateValueAndValidity();
      this.staffForm.get('rcc_certification_renewal_date').updateValueAndValidity();
      this.staffForm.get('rcc_certificate_type').reset();
      this.staffForm.get('rcc_certificate_due_date').reset();
      this.staffForm.get('rcc_date_application_mailed').reset();
      this.staffForm.get('rcc_date_tested').reset();
      this.staffForm.get('rcc_certification_effective_date').reset();
      this.staffForm.get('rcc_certification_number').reset();
      this.staffForm.get('rcc_certification_renewal_date').reset();
    }
  }
  deleteStaffForm(staffInfo) {
    this.staffName = staffInfo.first_nm + ' ' + staffInfo.last_nm;
    this.currentSelectedProviderStaffId = staffInfo.provider_staff_id;
    (<any>$('#delet-staff')).modal('show');
  }
  closeDeleteModal() {
    (<any>$('#delet-staff')).modal('hide');

  }
  confirmDelete() {
    this._commonHttpService.remove(
      this.currentSelectedProviderStaffId,
      {
        where: { provider_staff_id: this.currentSelectedProviderStaffId }
      },
      'providerstaff').subscribe(
        (response) => {
          this.getStaffInformation();
        },
        (error) => {
          this._alertService.error('Unable to delete Staff');
          return false;
        }
      );
    (<any>$('#delet-staff')).modal('hide');
  }
  resetForm() {
    this.staffForm.reset();
    this.certificates = [];
    this.trainings = [];
    this.staffClearanceForm.reset();
    this.assignProgramForm.reset();
    this.initializeStaffForm();
    this.getStaffInformation();
  }
  openAddStaffMembers() {
    this.resetForm();
    this.currentSelectedProviderStaffId = null;
    (<any>$('#personnel_section')).click();
    this.isEditStaff = false;
    this.showCertification = false;
    this.showTraining = false;
    this.isEndDate = false;
    this.isTrainingdate = false;
    this.resetCertificateForm();

  }
  personnelEndDate() {
    this.isEndDate = true;
}

trainingdate() {
  this.isTrainingdate = true;
}

  nextPatchProviderStaff(section) {
    if (section !== 'Personnel Section') {
     // stepper.selectedIndex = 1;
      if (this.staffClearanceForm.valid) {
        if ((this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false' ) ||
        (this.staffClearanceForm.get('cps_rcc_compliant').value === 'false' && this.staffClearanceForm.get('cps_cpa_compliant').value === null) ||
        (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === 'false') ||
        (this.staffClearanceForm.get('cps_rcc_compliant').value === null && this.staffClearanceForm.get('cps_cpa_compliant').value === null)) {
          this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
          return false;
        }
        if ((this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === 'false' ) ||
        (this.staffClearanceForm.get('state_rcc_compliant').value === 'false' && this.staffClearanceForm.get('state_cpa_compliant').value === null) ||
        (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === 'false') ||
        (this.staffClearanceForm.get('state_rcc_compliant').value === null && this.staffClearanceForm.get('state_cpa_compliant').value === null)) {
          this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
          return false;
        }
        if ((this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false' ) ||
        (this.staffClearanceForm.get('federal_rcc_compliant').value === 'false' && this.staffClearanceForm.get('federal_cpa_compliant').value === null) ||
        (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === 'false') ||
        (this.staffClearanceForm.get('federal_rcc_compliant').value === null && this.staffClearanceForm.get('federal_cpa_compliant').value === null)) {
          this._alertService.error('Each employee must complete a criminal background and CPS clearance prior to hire');
          return false;
        }
      } else {
        this._alertService.error('Kindly fill the mandatory fields in Clearance Section');
        return false;
      }
     } else {
       if (this.staffForm.invalid) {
        this._alertService.error('Kindly fill the mandatory fields in Personnel Section');
        return false;
       } else {
        (<any>$('#staff_cert_section')).click();
       }
     }
    if (this.isEditStaff && this.groupingLog) {
      this.staffForm.value.groupinglog = this.groupingLog;
    }
    const staffformData = this.staffForm.getRawValue();
    const clearanceForm = this.staffClearanceForm.getRawValue();
    // const assignProgram = this.assignProgramForm.getRawValue();
    const formData = Object.assign({}, staffformData, clearanceForm);
   formData.provider_staff_id = this.currentSelectedProviderStaffId;
    this.url = 'providerstaff';
       this._commonHttpService.patch(
        this.currentSelectedProviderStaffId,
        formData,
        this.url
      ).subscribe(
        (response) => {
          if (section === 'Clearance') {
            (<any>$('#assign_program_section')).click();
           }
          this.getStaffInformation();
         // this.resetForm();
          this._alertService.success('Information saved successfully!');
        },
        (error) => {
          this._alertService.error('Unable to save information');
        }
      );
  }

  prevSection(section) {
    if (section === 'Clearance') {
      (<any>$('#staff_training_section')).click();
    } else if (section === 'Assign Program') {
      (<any>$('#clearance_section')).click();
    } else if (section === 'Certificate')  {
      (<any>$('#personnel_section')).click();
    } else if (section === 'Training') {
      (<any>$('#staff_cert_section')).click();
    }
  }

  getStaffLogDetails(staff) {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { groupinglog: staff.groupinglog, delete_sw: 'Y' },
      },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.logStaffInfo = response;
        (<any>$('#staff-history-details')).modal('show');
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getStaffClearanceHistory(provider_staff_id) {

    this._commonHttpService.create(
      {
        provider_staff_id : provider_staff_id,
        method: 'post'
      },
      'providerstaff/getstaffclereancehistory'
    ).subscribe(
      (response) => {
        this.staffClearanceHistory = response && response.length ? response[0] : null;
      },
      (error) => {
        // this._alertService.error('Unable to retrieve information');
      }
    );
  }

  certDateChange(form) {
    if (form.value.rcc_certification_renewal_date < form.value.rcc_certification_effective_date) {
      this.staffForm.get('rcc_certification_renewal_date').reset();
    }
  }

  startDateChange(form) {
    if (form.value.start_date > form.value.end_date) {
      this.staffForm.get('end_date').reset();
    }
  }

}
