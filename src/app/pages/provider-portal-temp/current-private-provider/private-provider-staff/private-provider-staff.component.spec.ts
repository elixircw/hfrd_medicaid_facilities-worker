import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateProviderStaffComponent } from './private-provider-staff.component';

describe('PrivateProviderStaffComponent', () => {
  let component: PrivateProviderStaffComponent;
  let fixture: ComponentFixture<PrivateProviderStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateProviderStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateProviderStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
