import { Component, OnInit } from '@angular/core';
//import { ComplaintDetailsService } from '../complaint-details.service';
import { AttachmentConfig } from '../../provider-portal-temp-utils/attachment/_entities/attachment.data.models';
import { Router, ActivatedRoute } from '@angular/router';
//import { IncidentUirService } from '../incident-uir.service';
import { DataStoreService } from '../../../../@core/services';
import { AttachmentService } from '../../provider-portal-temp-utils/attachment/attachment.service';



@Component({
  selector: 'private-provider-documents',
  templateUrl: './private-provider-documents.component.html',
  styleUrls: ['./private-provider-documents.component.scss']
})
export class PrivateProviderDocumentsComponent implements OnInit {

  provider_id: string;

  constructor(private attachService: AttachmentService,
    private _router: Router, private route: ActivatedRoute
    ) {
      this.provider_id = '201900516';//route.snapshot.params['id'];
      const attachmentConfig: AttachmentConfig = new AttachmentConfig();      
      attachmentConfig.uniqueNumber = this.provider_id;
      this.attachService.setAttachmentConfig(attachmentConfig);
      }

  ngOnInit() {

    }

}
