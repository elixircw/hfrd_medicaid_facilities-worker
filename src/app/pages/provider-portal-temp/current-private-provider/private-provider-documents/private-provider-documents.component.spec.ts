import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateProviderDocumentsComponent } from './private-provider-documents.component';

describe('PrivateProviderDocumentsComponent', () => {
  let component: PrivateProviderDocumentsComponent;
  let fixture: ComponentFixture<PrivateProviderDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateProviderDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateProviderDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
