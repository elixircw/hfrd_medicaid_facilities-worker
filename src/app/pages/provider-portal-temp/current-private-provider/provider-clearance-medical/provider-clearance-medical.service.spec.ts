import { TestBed, inject } from '@angular/core/testing';

import { ProviderClearanceMedicalService } from './provider-clearance-medical.service';

describe('ProviderClearanceMedicalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProviderClearanceMedicalService]
    });
  });

  it('should be created', inject([ProviderClearanceMedicalService], (service: ProviderClearanceMedicalService) => {
    expect(service).toBeTruthy();
  }));
});
