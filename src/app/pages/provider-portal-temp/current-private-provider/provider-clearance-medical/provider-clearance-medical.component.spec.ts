import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderClearanceMedicalComponent } from './provider-clearance-medical.component';

describe('ProviderClearanceMedicalComponent', () => {
  let component: ProviderClearanceMedicalComponent;
  let fixture: ComponentFixture<ProviderClearanceMedicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderClearanceMedicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderClearanceMedicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
