import { Component, OnInit } from '@angular/core';
import { ProviderClearanceMedicalService } from '../provider-clearance-medical.service';
import { CommonDropdownsService, CommonHttpService } from '../../../../../@core/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'person-card',
  templateUrl: './person-card.component.html',
  styleUrls: ['./person-card.component.scss']
})
export class PersonCardComponent implements OnInit {
  persons: any[] = [];
  personsList: any[] = [];
  selectedPersonId: any;
  providerId: string;
  programType: any;
  constructor(
    private _service : ProviderClearanceMedicalService,
    private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute,
  ) {
    this.providerId = route.snapshot.params['id'];
   }

  ngOnInit() {
    // this.persons = [{ cjamspid : '100004606',firstname : 'A William ', lastname: 'Jack',personid: '100004606', dob: '01/31/1992', age: 27, gender: 'Male'

    // }];
    this.getCPAHomeList();
    this.getProgramType();
  }
  onChangeProgramType(value) {
    this._service.selectedPerson = null;
    const selectedValue = this.programType.find( item => { return item.program_type === value; }  );

    const siteId = selectedValue ? selectedValue.site_id : null;
 
    this._commonHttpService.getArrayList({
      where:{cpa_siteid: siteId},nolimit:true, order: 'displayorder ASC', method: 'get'
      },
      'providercpahome/listcpahomedetails?filter').subscribe(res => {
        // this.persons = res;
        this.personsList = res;
        if(siteId) {
          this.persons =  this.personsList.filter( person => { return person.cpa_siteid === siteId; } );
        }
      });
  
  }


  getProgramType() {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { provider_id: this.providerId }, method: 'get'
      },
      'providercpahome/listprogramtype?filter').subscribe(res => {
        this.programType = res;
      });
  }

  isPersonSelected(person) {

   return  ( this._service.selectedPerson &&  this._service.selectedPerson === person ) ? true : false ;
  }
  selectPerson(person) {
    this._service.selectedPerson = null;
    setTimeout(() => {
   this._service.selectedPerson = person; }, 200 );
  }

  getCPAHomeList() {

    this._commonHttpService.getArrayList({
      where:{provider_id: this.providerId},nolimit:true, order: 'displayorder ASC', method: 'get'
      },
      'providercpahome/listcpahomedetails?filter').subscribe(res => {
        // this.persons = res;
        this.personsList = res;
      });
  }
}
