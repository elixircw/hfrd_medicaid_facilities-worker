import { Component, OnInit, Input } from '@angular/core';
import { ProviderClearanceMedicalService } from './provider-clearance-medical.service';

@Component({
  selector: 'provider-clearance-medical',
  templateUrl: './provider-clearance-medical.component.html',
  styleUrls: ['./provider-clearance-medical.component.scss']
})
export class ProviderClearanceMedicalComponent implements OnInit {

  constructor(private _service : ProviderClearanceMedicalService) { }

  ngOnInit() {

  }

  isPersonSelected() {
     return ( (this._service.selectedPerson) ? true : false );
  }

}
