import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ProviderClearanceMedicalService } from '../provider-clearance-medical.service';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'clearance-medical-form',
  templateUrl: './clearance-medical-form.component.html',
  styleUrls: ['./clearance-medical-form.component.scss']
})
export class ClearanceMedicalFormComponent implements OnInit {
  formGroup: FormGroup;
  clearanceItems: any[] = [];
  medEvaluationItems: any[] =[];
  tuberculosisItems: any[] =[];
  clearanceHistory: any[] = [];
  medEvaluationHistory: any[] =[];
  tuberculosisHistory: any[] =[];
  updateClearance: boolean = false;
  providerId: string;
  selectedCleranceType: any;


  constructor(private _formBuilder: FormBuilder,private _service : ProviderClearanceMedicalService,
    private _httpService: CommonHttpService,private _alertService: AlertService,
    private route: ActivatedRoute,) {
    this.formGroup = this._formBuilder.group({ });
    this.formGroup.setControl('clearanceForm', this._formBuilder.array([]));
    this.formGroup.setControl('medEvaluationForm', this._formBuilder.array([]));
    this.formGroup.setControl('tuberculosisForm', this._formBuilder.array([]));
    this.providerId = route.snapshot.params['id'];
 }
 ngOnInit() {
     const clearanceItems = [
       { id : 1, title : 'Child Protective Services (CPS)', type: 'CPS' },
       { id : 2, title : 'Federal (CJIS) ', type: 'Federal' },
       { id : 3, title : 'State (CJIS)', type: 'State' }
      ];
      const medEvaluationItems = [
        { id : 1, title : 'Medical Evaluation', type: 'Medical' },
       ];
       const tuberculosisItems = [
        { id : 1, title : 'Tuberculosis Test', type: 'Tuberculosis' },
       ];
      this.selectedCleranceType = 'CPS';
      clearanceItems.forEach( item => {
        this.addClearance(item);
       });
       medEvaluationItems.forEach( item => {
        this.addMedEvaluation(item);
       });
       tuberculosisItems.forEach( item => {
        this.addTuberculosis(item);
       });
       this.getClearanceHistory();
       this.getMedicalHistory();
       this.getTuberculosisHistory();

 }

 showClearanceHistory() {
  (<any>$('#clearance-history')).modal('show');
  this.getClearanceHistory();
 }

 showMedicalHistory() {
  (<any>$('#medical-history')).modal('show');
  this.getMedicalHistory();
 }

 showTuberculosisHistory() {
  (<any>$('#tuberculosis-history')).modal('show');
  this.getTuberculosisHistory();
 }

 



//  private createFormGroup(modal) {
//   if (!this.updateClearance) {
//     return this._formBuilder.group({
//       requestdate: null,
//       resultdate: null,
//       is_cps_result: null,
//       has_cps_reviewed: null,
//       type: modal.type
//     });
//   } else {
//     return this._formBuilder.group(modal);
//   }
// }

saveClearance(isDraft) {
  if(this.formGroup.controls['clearanceForm']['controls'].invalid) {
    this._alertService.error('Please enter all required fields');
    return;
  }
  const FormData = this.formGroup.getRawValue();
  const selectedProgram = this._service.selectedPerson;
  if(!selectedProgram) {
    this._alertService.error('Please Select Program Type in Add CPA HOME');
    return;
  }
  let clearanceData = {};
  clearanceData['provider_id'] =  this.providerId;
	clearanceData['cpa_siteid'] =  selectedProgram.cpa_siteid;
  clearanceData['is_draft'] = isDraft;
  clearanceData['clearanceData'] = FormData.clearanceForm;
   this._httpService.create(clearanceData, 'providercpahome/addupdatecpaclearnceinfo').subscribe(
     response => {
       console.log(response);
       if(!isDraft) {
       this._alertService.success('Clearance Submitted Successfully');
       } else {
       this._alertService.success('Clearance Saved Successfully');
       }
     }
   );
  console.log(clearanceData);
}

saveMedical(isDraft) {
  if(this.formGroup.controls['medEvaluationForm']['controls'].invalid || this.formGroup.controls['tuberculosisForm']['controls'].invalid) {
    this._alertService.error('Please enter all required fields');
    return;
  }
  const FormData = this.formGroup.getRawValue();
  const selectedProgram = this._service.selectedPerson;
  if(!selectedProgram) {
    this._alertService.error('Please Select Program Type in Add CPA HOME');
    return;
  }
  let medicalData = {};
  medicalData['provider_id'] =  this.providerId;
	medicalData['cpa_siteid'] =  selectedProgram.cpa_siteid;
  medicalData['is_draft'] = isDraft;
  medicalData['medicalData'] = FormData.medEvaluationForm;
  medicalData['medicalData'].push(FormData.tuberculosisForm[0]);
   this._httpService.create(medicalData, 'providercpahome/addupdatecpamedicalinfo').subscribe(
     response => {
      if(!isDraft) {
        this._alertService.success('Medial Info Submitted Successfully');
        } else {
        this._alertService.success('Medical Info Saved Successfully');
        }
     }
   );
  console.log(medicalData);
}

selectTab(type) {
 this.selectedCleranceType = type;
}


getClearanceHistory() {
  const selectedProgram = this._service.selectedPerson;
  if(!selectedProgram) {
    this._alertService.error('Please Select Program Type in Add CPA HOME');
    return;
  }
  this._httpService.getArrayList({
    nolimit: true,
    where: { cpa_siteid: selectedProgram.cpa_siteid }, method: 'get'
    },
    'providercpahome/listcpaclearnceinfo?filter').subscribe(res => {
      this.clearanceHistory = res;
      const cpsData = res.find( data=> { return ( data.clearnce_type === 'CPS' && data.active_flag === 1); });
      const StateData = res.find( data=> { return ( data.clearnce_type === 'State' && data.active_flag === 1); });
      const Federal = res.find( data=> { return ( data.clearnce_type === 'Federal' && data.active_flag === 1); });
      if(cpsData) {
      this.formGroup.controls['clearanceForm']['controls'][0].patchValue(cpsData);
      } if(Federal) {
      this.formGroup.controls['clearanceForm']['controls'][1].patchValue(Federal);
      }
      if(StateData) {
      this.formGroup.controls['clearanceForm']['controls'][2].patchValue(StateData);
      }

    });
}

getMedicalHistory() {
  const selectedProgram = this._service.selectedPerson;
  if(!selectedProgram) {
    this._alertService.error('Please Select Program Type in Add CPA HOME');
    return;
  }
  this._httpService.getArrayList({
    nolimit: true,
    where: { cpa_siteid: selectedProgram.cpa_siteid }, method: 'get'
    },
    'providercpahome/listcpamedicalinfo?filter').subscribe(res => {
      this.medEvaluationHistory = res;
      const medical = res.find( data=> { return ( data.medical_type === "Medical" && data.active_flag === 1); });
      if(medical) {
      this.formGroup.controls['medEvaluationForm']['controls'][0].patchValue(medical);
      }
    });
}

getTuberculosisHistory() {
  const selectedProgram = this._service.selectedPerson;
  if(!selectedProgram) {
    this._alertService.error('Please Select Program Type in Add CPA HOME');
    return;
  }
  this._httpService.getArrayList({
    nolimit: true,
    where: { cpa_siteid: selectedProgram.cpa_siteid }, method: 'get'
    },
    'providercpahome/listcpamedicalinfo?filter').subscribe(res => {
     this.tuberculosisHistory = res;
     const tuber = res.find( data=> { return ( data.medical_type === "Tuberculosis" && data.active_flag === 1); });
     if(tuber) {
     this.formGroup.controls['tuberculosisForm']['controls'][0].patchValue(tuber);
     }
    });
}

reset() {
  this._service.selectedPerson = null;
  this.formGroup.reset();
}

addClearance(modal) {
 
  if (modal) {
    const control = <FormArray>this.formGroup.controls['clearanceForm'];
    control.push(this.createFormGroupNew(modal, 'clearance'));
    this.clearanceItems.push(modal);
  } else {
    const clearanceItem =   { id :  this.clearanceItems.length + 1 , title : 'Other', type: 'Other' };
    const control = <FormArray>this.formGroup.controls['clearanceForm'];
    control.push(this.createFormGroupNew(modal, 'clearance'));
    this.clearanceItems.push(clearanceItem);
  }
} 

addMedEvaluation(modal) {
  if (modal) {
    const control = <FormArray>this.formGroup.controls['medEvaluationForm'];
    control.push(this.createFormGroupNew(modal, 'medEvaluation'));
    this.medEvaluationItems.push(modal);
  } else {
    const medEvaluationItem =   { id :  this.medEvaluationItems.length + 1 , title : 'Other', type: 'Medical_other' };
    const control = <FormArray>this.formGroup.controls['medEvaluationForm'];
    control.push(this.createFormGroupNew(modal, 'medEvaluation'));
    this.medEvaluationItems.push(medEvaluationItem);
  }
} 

addTuberculosis(modal) {
  if (modal) {
    const control = <FormArray>this.formGroup.controls['tuberculosisForm'];
    control.push(this.createFormGroupNew(modal, 'tuberculosis'));
    this.tuberculosisItems.push(modal);
  } else {
    const tuberculosisItem =   { id :  this.tuberculosisItems.length + 1 , title : 'Other', type: 'Tuberculosis_other' };
    const control = <FormArray>this.formGroup.controls['tuberculosisForm'];
    control.push(this.createFormGroupNew(modal, 'tuberculosis'));
    this.tuberculosisItems.push(tuberculosisItem);
  }
}



private createFormGroupNew(modal, formType) {

  switch(formType) {
  case 'clearance':
  return this._formBuilder.group({
    cpa_request_date: null,
    cpa_result_date: null,
    isclearnce_family_record: null,
    isclearnce_comar: null,
    clearnce_type: modal ? modal.type : null
  });
  case 'medEvaluation':
    return this._formBuilder.group({
      cpa_test_date: null,
      ismedical_family_record: null,
      medical_type:  modal ? modal.type : null
    });
  case 'tuberculosis':
    return this._formBuilder.group({
      cpa_test_date: null,
      ismedical_family_record: null,
      medical_type: modal ? modal.type : null
    });
  }
}





}
