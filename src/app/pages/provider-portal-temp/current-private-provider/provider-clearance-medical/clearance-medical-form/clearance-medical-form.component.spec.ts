import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearanceMedicalFormComponent } from './clearance-medical-form.component';

describe('ClearanceMedicalFormComponent', () => {
  let component: ClearanceMedicalFormComponent;
  let fixture: ComponentFixture<ClearanceMedicalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearanceMedicalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearanceMedicalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
