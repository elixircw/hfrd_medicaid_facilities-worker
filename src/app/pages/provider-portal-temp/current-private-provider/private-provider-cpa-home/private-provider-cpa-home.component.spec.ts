import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateProviderCpaHomeComponent } from './private-provider-cpa-home.component';

describe('PrivateProviderCpaHomeComponent', () => {
  let component: PrivateProviderCpaHomeComponent;
  let fixture: ComponentFixture<PrivateProviderCpaHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateProviderCpaHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateProviderCpaHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
