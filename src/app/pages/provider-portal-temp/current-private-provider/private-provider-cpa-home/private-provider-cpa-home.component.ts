import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { ContactDetailsModule } from '../../../person-details/contact-details/contact-details.module';
import * as moment from 'moment';
import { ProviderPortalService } from '../../../../provider-portal/provider-portal.service';
import { MatStepper } from '@angular/material';
import { ValidationService, CommonDropdownsService, AuthService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { ProviderClearanceMedicalService } from '../provider-clearance-medical/provider-clearance-medical.service';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { config } from '../../../../../environments/config';
import { AppConfig } from '../../../../app.config';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { HttpHeaders } from '@angular/common/http';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';

@Component({
  selector: 'private-provider-cpa-home',
  templateUrl: './private-provider-cpa-home.component.html',
  styleUrls: ['./private-provider-cpa-home.component.scss']
})
export class PrivateProviderCpaHomeComponent implements OnInit {

  providerId: string;
  applications = [];
  personType: any[] = [];
  personGender: any[] = [];
  // primaryRace: any[] = [];
  // secondaryRace: any[] = [];
  personRace: any[] = [];
  programType: any[] = [];
  personHeritage: any[] = [];
  programTypeForm: FormGroup;
  suspendForm: FormGroup;
  householdForm: FormGroup;
  addressForm: FormGroup;
  certificateForm: FormGroup;
  showPersonAddressForm: boolean;
  addressTypes: any;
  deleteAddressForm:any;
  addresses = [];
  suggestedAddress$: Observable<any[]>;
  countyList$: Observable<DropdownModel[]>;
  stateList$: Observable<any[]>;
  showAddressForm : boolean = false;
  applicantId: any;
  siteId: any;
  cpaHomeList: any[] = [];
  addPerson: boolean = false;
  houseHoldPerson: any[] = [];
  deleteAttachmentIndex: number;
  uploadedFile: any = [];
  token: any;
  certificateList: any[] = [{id:1}];
  isAddCertificate:  boolean = false;
  entryType: string;
  certificateType: any[] = [];
  addressList: any[] = [];
  selectedAddressIndex: any;
  isTeenPGM : boolean = false;
  isViewOnly: boolean = false;
  cpaPersonList: any[] = [];
  isPersonUpdate: boolean = false;
  selectedProgram: any;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _providerPortalService: ProviderPortalService,
    private _commonDropdownService: CommonDropdownsService,
    private _cpsClearanceService : ProviderClearanceMedicalService,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService,
    ) {
      this.providerId = route.snapshot.params['id'];
    this.applications = this._providerPortalService.getApplications();
    this.token = this._authService.getCurrentUser();
    }

  ngOnInit() {
    this.initializeprogramTypeForm();
    this.initializeHouseholdForm();
    this.initializeAddressForm();
    this.initSuspendForm();
    // this.initCertificateForm();
    this.getHouseholdPerson();
    this.getPersonType();
    this.getPersonRace();
    this.getPersonHeritage();
    this.getPersonGender();
    this.loadDropDown();
    this.getProgramType();
    this.getCPAHomeList();
    // this.getAddress();
    this.certificateType = ['Initial', 'Annual'];
    // this.personType = ['Applicant', 'Co-applicant', 'Member'];
    // this.personGender = ['Male', 'Female', 'Both'];
    // this.primaryRace = ['Alaskan Native', 'American Indian', 'Asian', 'Black or African American', 'Native Hawaiian or Pacific Islander', 'Others', 'UnKnown', 'White or Caucasian'];
    // this.secondaryRace = ['Alaskan Native', 'American Indian', 'Asian', 'Black or African American', 'Native Hawaiian or Pacific Islander', 'Others', 'UnKnown', 'White or Caucasian'];

  }

  private initializeprogramTypeForm() {
    this.programTypeForm = this.formBuilder.group({
      program_type: ['']
    });

  }

  initSuspendForm() {
    this.suspendForm = this.formBuilder.group({
      siteid: [null],
      suspensionstartdate: [null],
      suspension_letter_sent_date: [null],
      suspensionenddate: [null],
      suspensioncomments: [null],
      provider_cpa_address_id: [null],
      provider_id: [null],
      cpa_siteid: [null],
      adr_type: [null],
      adr_line1: [null],
      adr_line2: [null],
      adr_state: [null],
      adr_city: [null],
      adr_county: [null],
      adr_zip: [null],
      adr_start_date: [null],
      adr_end_date: [null],
      cpa_certificate_type: [null],
      cpa_homeins_date: [null],
      cpa_fireins_date: [null],
      isinspection_home_family_record: [null],
      isinspection_fire_family_record: [null],
      ismeets_comar_requiremnt: [null],
      create_ts: [null],
      create_user_id: [null],
      update_ts: [null],
      update_user_id: [null],
      active_flag: [null],
      status: [null]
    });
  }

  addSuspend(data) {
  this.suspendForm.patchValue(data);
  }

  saveSuspend() {
    const data = this.suspendForm.getRawValue();
    data.status = 'Suspend';
  this._commonHttpService.create(data, 'tb_prov_cpa_address/addupdate').subscribe(
    (response) => {
      this._alertService.success('suspend added successfully!');
      this.suspendForm.reset();
      this.resetAddress();
      (<any>$('#suspension')).modal('hide');
    },
    (error) => {
      this._alertService.error('Unable to add suspend, please try again.');
      return false;
    });
  }

  initCertificateForm() {
    this.certificateForm = this.formBuilder.group({
      cpa_initial_date: [null],
      cpa_annual_date: [null],
      cpa_homeins_date: [null],
      cpa_fireins_date: [null],
      isinspection_home_family_record: [null],
      isinspection_fire_family_record: [null],
    });
  }

  addCertificate() {
    this.isAddCertificate = true;
  }

  resetCertificateForm() {
    this.isAddCertificate = false;
  }

  saveCertification() {
    let requestdata = {};
    this._commonHttpService.create(requestdata, 'providercpahome/addupdatecpacertificateinfo').subscribe(
      (response) => {
        this._alertService.success('Certificate added successfully!');
        this.resetCertificateForm();
      },
      (error) => {
        this._alertService.error('Unable to add certificate, please try again.');
        return false;
      });
  }



  getCertificate() {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { provider_id: this.providerId, }, method: 'get'
      },
      'providercpahome/listcpacertificateinfo').subscribe(res => {
        this.certificateList = res;
      });
  }



  private initializeHouseholdForm() {
    this.householdForm = this.formBuilder.group({
      cpahomeid: [null],
      object_id: this.providerId,
      person_first_nm: [''],
      middle_nm: [''],
      person_last_nm: [''],
      person_type: [''],
      person_sex: [''],
      primaryrace: [null],
      secondaryrace: [null],
      job_title: [''],
      employee_type: null,
      person_dob_dt: null,
      person_dod_dt: null,
      hispanic_heritage: [''],
      home_start_dt: null,
      home_end_dt: null,
      staff_comment: [''],
      person_email: ['', [ValidationService.mailFormat]],
      person_home_phone: null,
      person_mobile_phone: null
        });
    }

    addAddress() {
      this.showAddressForm = true;
    }

    private initializeAddressForm() {
      this.addressForm = this.formBuilder.group({
        provider_cpa_address_id: [null],
        // provider_applicant_profile_id: [''],
        object_id: [''],
        address_id: [''],
        adr_line1: [''],
        adr_line2: [''],
        adr_city: [''],
        adr_state: [''],
        adr_county: [''],
        adr_zip: [null],
        adr_type: [''],
        adr_start_date: [null],
        adr_end_date: [null],
        cpa_homeins_date: [null],
        isinspection_home_family_record: [null],
        cpa_fireins_date: [null],
        isinspection_fire_family_record: [null],
        cpa_initial_date: [null],
        cpa_certificate_type: [null],
        ismeets_comar_requiremnt: [null],
        cpa_annual_date: [null]

      });
      this.addressTypes = ['Main', 'Site', 'Payment'];
    }

  resetForm() {
    this.householdForm.reset();
    this.initializeHouseholdForm();
    this.addressForm.reset();
    this.showAddressForm = false;
    this.suspendForm.reset();

  }

//   saveAddress(obj) {
//     obj.applicant_id = this.providerId;
//     obj.provider_applicant_profile_id = null;
//   if (obj.address_id) {
//     this._commonHttpService.create(obj, 'providerapplicantportal/updateapplicantaddress').subscribe(
//       (response) => {
//         this._alertService.success('Address Updated successfully!');
//           },
//       (error) => {
//         this._alertService.error('Unable to Update address, please try again.');
//         console.log('Save address Error', error);
//         return false;
//       }
//     );
//     }
//     else {
//       this._commonHttpService.create(obj, 'providerapplicantportal/addapplicantaddress').subscribe(
//         (response) => {
//           this._alertService.success('Address Created successfully!');
         
//           // this.addresses.unshift(obj);
//         },
//         (error) => {
//           this._alertService.error('Unable to Create address, please try again.');
//           console.log('Save address Error', error);
//           return false;
//         }
//       );
//     }
//     this.resetForm();
//   (<any>$('#intake-aptmnt')).modal('hide');
// }

resetAddress() {
  (<any>$('#add-home-member')).modal('hide');
  this.resetAddressForm();
  this.isViewOnly = false;
  this.addressForm.enable();
  this.getCPAHomeList();
}
saveAddress() {
  //  this.addressForm.patchValue(this.addressList[this.addressList.length-1]);
   let  obj =  this.addressForm.getRawValue();
  obj['cpa_siteid'] = this.siteId;
  obj['provider_id']  = this.providerId;
  this.addressForm.reset();
   
    


  this._commonHttpService.create(obj, 'tb_prov_cpa_address/addupdate').subscribe(
          (response) => {
            this._alertService.success('Address saved successfully!');
            this.resetAddressForm();
            this.resetAddress();
              },(error) => {
                          this._alertService.error('Unable to Create address, please try again.');
                          console.log('Save address Error', error);
                          return false;
                          
                        }
  );
}

editCPAHome(data) {
  this.getHouseholdPerson();
  this.addressForm.patchValue(data);
}

viewCPAHome(data) {
  this.addressForm.patchValue(data);
  this.addressForm.disable();
  this.isViewOnly = true;
}

addAddressInfo() {
  const address =  this.addressForm.getRawValue();
  if(this.selectedAddressIndex >= 0) {
    this.addressList[this.selectedAddressIndex]  = address;
    this.showAddressForm = false;
    this._alertService.success('Address updated successfully!');
} 
  else {

    this.addressList.push(address);
    this.showAddressForm = false;
    this._alertService.success('Address added successfully!');
  }
  this.resetAddressForm();
}


resetAddressForm() {
  this.addressForm.reset();
  this.showAddressForm = false;
  this.selectedAddressIndex = null;
}

EditAddress(editObj,index){
  this.selectedAddressIndex = index;
  console.log(editObj)
  this.addressForm.patchValue(editObj);
  this.showAddressForm = true;
}

// getAddress() {
//   this._commonHttpService.getArrayList({
//     nolimit: true,
//     where: { cpa_siteid: this.siteId}, method: 'get'
//     },
//     'providercpahome/listcpaaddressinfo?filter').subscribe(res => {
//       this.addressList = res;
//     });
// }

DeleteAddress(index){
  this.selectedAddressIndex=index;
  (<any>$('#delete-address')).modal('show');
}

CancelDelete(){
  (<any>$('#delete-address')).modal('hide');
}

DeleteAddressConfirm(){
       if(this.selectedAddressIndex >= 0) {
         this.addressList.splice(this.selectedAddressIndex, 1);
       }
      (<any>$('#delete-address')).modal('hide');
      this.resetAddressForm();
}

  getPersonType() {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { referencetypeid: 700 }, order: 'displayorder ASC', method: 'get'
      },
      'referencetype/gettypes?filter').subscribe(res => {
        this.personType = res;
      });
  }


  getHouseholdPerson() {
     this._commonHttpService.getArrayList({
      where:{cpa_siteid: this.siteId},nolimit:true, order: 'displayorder ASC', method: 'get'
      },
      'providercpahome/listcpahomedetails?filter').subscribe(res => {
        this.cpaPersonList = res;
      });
  }

  getCPAHomeList() {

    // this._commonHttpService.getArrayList({
    //   where:{provider_id: this.providerId},nolimit:true, order: 'displayorder ASC', method: 'get'
    //   },
    //   'providercpahome/listcpahomedetails?filter').subscribe(res => {
    //     this.cpaHomeList = res;
    //   });

    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { cpa_siteid: this.siteId}, method: 'get'
      },
      'providercpahome/listcpaaddressinfo?filter').subscribe(res => {
        this.cpaHomeList = ( res && res.length ) ? res[0].getcpaaddressinfo : [];
      });
  }

  

  getPersonGender() {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { referencetypeid: 702 }, order: 'displayorder ASC', method: 'get'
      },
      'referencetype/gettypes?filter').subscribe(res => {
        this.personGender = res;
      });
  }

  getPersonRace() {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { referencetypeid: 701 }, order: 'displayorder ASC', method: 'get'
      },
      'referencetype/gettypes?filter').subscribe(res => {
        this.personRace = res;
      });
  }

  getPersonHeritage() {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { referencetypeid: 703 }, order: 'displayorder ASC', method: 'get'
      },
      'referencetype/gettypes?filter').subscribe(res => {
        this.personHeritage = res;
      });
  }

  getProgramType() {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { provider_id: this.providerId }, method: 'get'
      },
      'providercpahome/listprogramtype?filter').subscribe(res => {
        this.programType = res;
      });
  }

  onChangeProgramType(value) {
    const selectedValue = this.programType.find( item => { return item.program_type === value; }  );
    this.applicantId = selectedValue.applicant_id;
    this.selectedProgram = selectedValue;
    this.siteId = selectedValue.site_id;
    this._cpsClearanceService.selectedProgram = selectedValue;
   // console.log(selectedValue.applicant_id);
   // console.log(selectedValue.site_id);
    switch (value) {
      case 'Traditional Foster Care': this.entryType = 'CPA_HOME'; this.isTeenPGM = false; break;
      case 'Treatment Foster Care': this.entryType = 'CPA_HOME'; this.isTeenPGM = false ; break;
      case 'Medically Fragile Foster Care': this.entryType = 'CPA_HOME'; this.isTeenPGM = false ;break;
      case 'Teen Parent Foster Care': this.entryType = 'CPA_HOME'; this.isTeenPGM = true ; break;
      case 'Independent Living Program': this.entryType = 'ILP_SITE'; this.isTeenPGM = false; break;
      case 'Independent Living Program Teen Parent': this.entryType = 'ILP_SITE'; this.isTeenPGM = true; break;
    }
    this.getCPAHomeList();

  }

  addCPA() {
    // this.getAddress();
    let isActiveAddress = this.cpaHomeList && this.cpaHomeList.length ? this.cpaHomeList.filter(data =>  ((!data.adr_end_date || data.adr_end_date== null) && (!data.status ||data.status === null) )) : [];
    if(isActiveAddress && isActiveAddress.length) {
      this._alertService.warn('Please Suspend / End Current active address to add new one.');
    } else {
    (<any>$('#add-home-member')).modal('show');
    this.getHouseholdPerson();
    }
  }

 

  nextSection(section) {
    if (section === 'Household') {
      (<any>$('#address_info_section')).click();
      this.showPersonAddressForm = true;
    } else if (section === 'Addressinfo') {
      (<any>$('#certificates_section')).click();
    }
  }

  prevSection(section) {
    if (section === 'Addressinfo') {
      (<any>$('#household_section')).click();
    } else if (section === 'Certificateinfo') {
      (<any>$('#address_info_section')).click();
    } 
  }

  save(section, requestdata) {
    requestdata.applicant_id = this.applicantId;
    requestdata.cpa_siteid = this.siteId;
    // requestdata.provider_applicant_profile_id = null;
    requestdata.provider_id = this.providerId;
    if (section === 'Household' && this.householdForm.valid) {
        this._commonHttpService.create(requestdata, 'providercpahome/addupdatecpahomeinfo').subscribe(
          (response) => {
            if(this.isPersonUpdate) {
              this._alertService.success('Household person updated successfully!');
            } else {
            this._alertService.success('Household person added successfully!');
            }
           this.resetPerson();
          },
          (error) => {
            this._alertService.error('Unable to Create Household person, please try again.');
            console.log('Save household Error', error);
            return false;
          });
    } else if (section === 'Addressinfo' && this.addressForm.valid) {
        if (requestdata.address_id) {
          this._commonHttpService.create(requestdata, 'providerapplicantportal/updateapplicantaddress').subscribe(
            (response) => {
              this._alertService.success('Address Updated successfully!');
                },
            (error) => {
              this._alertService.error('Unable to Update address, please try again.');
              console.log('Save address Error', error);
              return false;
            }
          );
        } else {
          this._commonHttpService.create(requestdata, 'providerapplicantportal/addapplicantaddress').subscribe(
            (response) => {
              this._alertService.success('Address Created successfully!');
              // this.addresses.unshift(requestdata);
            },
            (error) => {
              this._alertService.error('Unable to Create address, please try again.');
              console.log('Save address Error', error);
              return false;
            }
          );
        }
        this.resetForm();
        (<any>$('#intake-aptmnt')).modal('hide');
    }



  }
  cancel(value) {
    if (value === 'Household') {
        this.resetPerson();
    }

  }

  private loadDropDown() {
    const source = forkJoin([
       this._commonHttpService.getArrayList(
         {
             method: 'get',
             nolimit: true
         },
         'States?filter'
     ),
    ])
      .map((result) => {
        return {
          stateList: result[0].map(
            (res) =>
                new DropdownModel({
                    text: res.statename,
                    value: res.stateabbr
                })
        )
        };
      })
      .share();
  
    // this.stateDropDownItems$ = source.pluck('stateList');
    
    this.stateList$ = this._commonDropdownService.getPickListByName('state');
    this.countyList$ = this._commonDropdownService.getPickList('328');
  
  }
  
  getSuggestedAddress() {
    this.suggestAddress();
  }
  
  suggestAddress() {
  this._commonHttpService
    .getArrayListWithNullCheck(
      {
        method: 'post',
        where: {
          prefix: this.addressForm.value.adr_line1,
          cityFilter: '',
          stateFilter: '',
          geolocate: '',
          geolocate_precision: '',
          prefer_ratio: 0.66,
          suggestions: 25,
          prefer: 'MD'
        }
      },
      'People/suggestaddress'
    ).subscribe(
      (result: any) => {
        if (result.length > 0) {
          this.suggestedAddress$ = result;
        }
      }
    );
  }
  selectedAddress(model) {
    this.addressForm.patchValue({
      adr_line1: model.streetLine ? model.streetLine : '',
      adr_city: model.city ? model.city : '',
      adr_state: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        'People/validateaddress'
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.addressForm.patchValue({
              adr_zip: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.addressForm.value.adr_state_cd, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  if (resultresp && resultresp.length) {
                    this.addressForm.patchValue({
                      adr_county: resultresp[0].ref_key
                    });
                  }
                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  addHouseholdPerson() {
      this.addPerson = true;
      this.isPersonUpdate = false;
   }

   editHouseholdPerson(person,index) {
    this.addPerson = true;
    this.householdForm.patchValue(person);
    this.isPersonUpdate = true;
   }

   resetPerson() {
    this.householdForm.reset();
    this.addPerson = false;
    this.isPersonUpdate = false;
    this.getHouseholdPerson();
   }

   uploadFile(file: File | FileError): void {
    if (!(file instanceof Array)) {
        return;
    }
    file.map((item, index) => {
        const fileExt = item.name
            .toLowerCase()
            .split('.')
            .pop();
        if (
            fileExt === 'mp3' ||
            fileExt === 'ogg' ||
            fileExt === 'wav' ||
            fileExt === 'acc' ||
            fileExt === 'flac' ||
            fileExt === 'aiff' ||
            fileExt === 'mp4' ||
            fileExt === 'mov' ||
            fileExt === 'avi' ||
            fileExt === '3gp' ||
            fileExt === 'wmv' ||
            fileExt === 'mpeg-4' ||
            fileExt === 'pdf' ||
            fileExt === 'txt' ||
            fileExt === 'docx' ||
            fileExt === 'doc' ||
            fileExt === 'xls' ||
            fileExt === 'xlsx' ||
            fileExt === 'jpeg' ||
            fileExt === 'jpg' ||
            fileExt === 'png' ||
            fileExt === 'ppt' ||
            fileExt === 'pptx' ||
            fileExt === 'gif'
        ) {
            this.uploadedFile.push(item);
            const uindex = this.uploadedFile.length - 1;
            if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
                this.uploadedFile[uindex].percentage = 1;
            }

            this.uploadAttachment(uindex);
            const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
            const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
            if (audio_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Audio';
            } else if (video_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Video';
            } else {
                this.uploadedFile[uindex].attachmenttypekey = 'Document';
            }
        } else {
            // tslint:disable-next-line:quotemark
            this._alertService.error(fileExt + " format can't be uploaded");
            return;
        }
    });
}
uploadAttachment(index) {
    console.log('check');
    const currTimestamp =new Date();
    const workEnv = config.workEnvironment;
    let uploadUrl = '';
    if (workEnv === 'state') {
        uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
            + '?access_token=' + this.token.id + '&' + 'srno=' + currTimestamp + '&' + 'docsInfo='; // Need to discuss about the docsInfo
        console.log('state', uploadUrl);
    } else {
        uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
            '&' + 'srno=' + currTimestamp;
        console.log('local', uploadUrl);
    }

    this._uploadService
        .upload({
            url: uploadUrl,
            headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
            filesKey: ['file'],
            files: this.uploadedFile[index],
            process: true,
        })
        .subscribe(
            (response) => {
                if (response.status) {
                    this.uploadedFile[index].percentage = response.percent;
                }
                if (response.status === 1 && response.data) {
                    const doucumentInfo = response.data;
                    doucumentInfo.documentdate = doucumentInfo.date;
                    doucumentInfo.title = doucumentInfo.originalfilename;
                    doucumentInfo.objecttypekey = 'Court';
                    doucumentInfo.rootobjecttypekey = 'Court';
                    doucumentInfo.activeflag = 1;
                    doucumentInfo.servicerequestid = null;
                    this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                    console.log(index, this.uploadedFile[index]);
                }

            }, (err) => {
                console.log(err);
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                this.uploadedFile.splice(index, 1);
            }
        );
}
deleteAttachment() {
    this.uploadedFile.splice(this.deleteAttachmentIndex, 1);
    (<any>$('#delete-attachment-popup')).modal('hide');
}

downloadFile(s3bucketpathname) {
    const workEnv = config.workEnvironment;
    let downldSrcURL;
    if (workEnv === 'state') {
        downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
    } else {
        // 4200
        downldSrcURL = s3bucketpathname;
    }
    console.log('this.downloadSrc', downldSrcURL);
    window.open(downldSrcURL, '_blank');
}

confirmDeleteAttachment(index: number) {
    (<any>$('#delete-attachment-popup')).modal('show');
    this.deleteAttachmentIndex = index;
}
  


}
