import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentPrivateProviderComponent } from './current-private-provider.component';



const routes: Routes = [
    {
        path: ':id',
        component: CurrentPrivateProviderComponent,
        children: [
            // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
            // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
            // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
            // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
            // { path: 'cap', loadChildren: './cap/cap.module#CapModule' }
        ]
        // canActivate: [RoleGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CurrentPrivateProviderRoutingModule {}
