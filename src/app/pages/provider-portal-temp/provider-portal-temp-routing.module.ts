import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderPortalTempComponent } from './provider-portal-temp.component'
import { ProviderApplicationsComponent } from './provider-applications/provider-applications.component';
// import { RoleGuard } from '../../@core/guard/role.guard';


const routes: Routes = [
    {
        path: '',
        component: ProviderPortalTempComponent,
        children: [
            {
                path: 'current-application',
                loadChildren: './current-application/current-application.module#CurrentApplicationModule'
            },
            // {
            //     path: 'current-private-provider',
            //     loadChildren: './current-private-provider/current-private-provider.module#CurrentPrivateProviderModule'
            // }     
        ]
    },   
    {
        path: '',
        component: ProviderPortalTempComponent,
        children: [           
            {
                path: 'current-private-provider',
                loadChildren: './current-private-provider/current-private-provider.module#CurrentPrivateProviderModule'
            }
        ]
    }, 
    {
        path: 'provider-applications',
        component: ProviderApplicationsComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Provider Applications'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProviderPortalTempRoutingModule {}

