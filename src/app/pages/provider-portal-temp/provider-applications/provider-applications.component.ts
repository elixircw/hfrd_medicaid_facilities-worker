import { Component, OnInit } from '@angular/core';

import { CommonHttpService } from '../../../@core/services/common-http.service';

@Component({
  selector: 'provider-applications',
  templateUrl: './provider-applications.component.html',
  styleUrls: ['./provider-applications.component.scss']
})
export class ProviderApplicationsComponent implements OnInit {
  applicants:any;
  reverseApplicant: any;
  constructor(private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.getProviderApplicant('PENDING');
  }

  private getProviderApplicant(type:String) {
        
    if(type == 'PENDING'){
      this._commonHttpService.getArrayList(
        {
            method: 'get',
            nolimit: true,
            order: 'provider_id desc'
        },
        'providerapplicant?filter'
    ).subscribe(applicants => {
      this.applicants=applicants;
      // this.reverseApplicant = applicants;
      // console.log("Rahul portal reverse: before",this.reverseApplicant);
      // this.applicants=this.reverseApplicant.reverse();
      // console.log("Rahul portal applicants",this.applicants);
    });
      
    } else {
      this.applicants = [];
    }
      
    
    }

}
