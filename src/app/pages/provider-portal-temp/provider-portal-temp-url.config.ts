// Referral endpoints
export class ApplicantUrlConfig {
    public static EndPoint = {
        Applicant : {
            getapplicantstaff:'providerapplicant/getapplicantstaff', 
            intakeappointment:'providerapplicant/schdapplicantappointment',
            getappointmentdetails:'providerapplicant/getapplappiontment',
            getapplicantInfo:'providerapplicant/getproviderapplicant',
            getapplicantchecklist:'providerapplicant/getchecklist'
        },
        Attachment: {
            UploadAttachmentUrl: 'attachments/uploadsFile',
            AttachmentGridUrl: 'Documentproperties/getintakeattachments',
            AttachmentUploadUrl: 'attachments/uploadsFile',
            AttachmentTypeUrl: 'Intake/attachmenttype',
            AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype',
            SaveAttachmentUrl: 'Documentproperties/addintakeattchment',
            DeleteAttachmentUrl: 'Documentproperties/delete'         
        },
        Assessment: {
            ListAssessment: 'admin/assessment/list',
            InvolvedPersonList: 'people/getinvolvedperson?filter',
            RoutingInfoList: 'routing/getroutingdetails?filter'
        },
        Portal: {
            Addapplicantstaff: 'providerapplicantportal/addapplicantstaff'
        },
        Cap: {
            ListCap: 'tb_provider_complaint_deficiency/list?filter',
            SubmitCap: 'tb_provider_complaint_deficiency/caproutingupdate'
        }
    }
}


export class ProviderPortalUrlConfig {
    public static EndPoint = {
        Uir: {
            GetNextNumberUrl: `Nextnumbers/getNextNumber?apptype=providerIncident`,
            // CREATE_UPDATE_URL : `tb_provider_complaint/addupdate`,
             DETAIL_URL : `tb_provider_complaint/providercomplaintdetail?filter`,
             CREATE_UPDATE_URL: `provider_uir/addupdate`,
             CREATE_UPDATE_INCIDENT: `provider_uir_classincident/addupdate`,
             CREATE_UPDATE_WITNESS: `provider_uir_witness/addupdate`,
             LIST_URL : `provider_uir/listdashboard`,
             LIST_PROGRAMMES_URL: 'providerreferral/listprogramnames',
             PROVIDER_PROGRAM_NAMES_URL: 'provider_uir/getproviderprogramsinformation',
             GET_SELECTED_PROGRAM_INFO: 'providerlicense/getproviderlicensing',
            // DECISION_URL: `tb_provider_complaint/routingupdate`,
            // DECISION_LIST_URL: `tb_provider_complaint/routinglist?filter`,
            // DEFIICENCY_URL : `tb_provider_complaint_deficiency/list?filter`,
            // CREATE_UPDATE_DEFIICENCY_URL : `tb_provider_complaint_deficiency/addupdate`,
            // CONTACT_URL : `tb_provider_complaint_contacts/addupdate`,
            // CONTACT_NOTE_CREATE_UPDATE_URL : `tb_provider_complaint_notes/addupdate`,
            // CONTACT_LIST_URL : `tb_provider_complaint_contacts/list?filter`,
            // CONTACT_NOTES_LIST_URL : `tb_provider_complaint_notes/list?filter`,
        },
        DSDSAction: {
            Attachment: {
                UploadAttachmentUrl: 'attachments/uploadsFile',
                AttachmentGridUrl: 'Documentproperties/getintakeattachments',
                AttachmentUploadUrl: 'attachments/uploadsFile',
                AttachmentTypeUrl: 'Intake/attachmenttype/',
                AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype/',
                SaveAttachmentUrl: 'Documentproperties/addintakeattchment',
                DeleteAttachmentUrl: 'Documentproperties/delete'
            }
        }
    };
}

// To maintain intake functionality
export class NewUrlConfig {
    public static EndPoint = {
        Intake: {
            GetNextNumberUrl: `Nextnumbers/getNextNumber?apptype=IntakeNumber`,
            ServiceRequestIncidentTypeUrl: `manage/Servicerequestincidenttype?filter={"where" :{"activeflag":1}}`,
            IntakeServiceRequestIllegalActivityTypeUrl: `Intakeservicerequestillegalactivitytypes?filter={"where" :{"activeflag":1}}`,
            IntakeServiceRequestInputTypeUrl: `Intakeservicerequestinputtypes/list?filter={"where" :{"activeflag":1},"order" : "description"}`,
            GlobalPersonSearchUrl: 'globalpersonsearches/getPersonSearchData',
            InvolvedEnititesSearchUrl: 'gloabalagencysearches/getAgencySearchData',
            AgencyCategoryUrl: 'admin/agencycategory',
            CountryListUrl: 'admin/county',
            StateListUrl: 'States',
            MaritalStatusUrl: 'admin/maritalstatustype',
            ReligionTypeUrl: 'admin/religiontype',
            RegionListUrl: 'admin/region/list',
            NextnumbersUrl: 'Nextnumbers/getNextNumber?apptype=servicerequestauthorizationnumber',
            DADispositionUrl: 'admin/intakeservicerequesttype/listdadisposition',
            DAStatusUrl: 'admin/intakeservicerequesttype/listdastatus',
            AllegationsIndicaorUrl: 'admin/allegation/getallegationsandindicaors',
            DATypeUrl: 'admin/intakeservicerequesttype',
            EthnicGroupTypeUrl: 'admin/ethnicgrouptype',
            GenderTypeUrl: 'admin/gendertype',
            LivingArrangementTypeUrl: 'admin/livingarrangementtype',
            LanguageTypeUrl: 'admin/languagetype',
            RaceTypeUrl: 'admin/racetype',
            RelationshipTypesUrl: 'Relationshiptypes',
            ActorTypeUrl: 'admin/actortype',
            UserActorTypeUrl: 'admin/actortype/listActortype',
            ActorTypeListUrl: 'admin/actortype/listactortype',
            EntitesTypeUrl: 'admin/agencytype',
            EntityRoletypeUrl: 'admin/agencyroletype',
            IntakeserviceRequestCrossReferenceReasonTypesUrl: 'Intakeservicerequestcrossreferencereasontypes',
            ServiceRequestSearchesUrl: 'servicerequestsearches/getDetails',
            SameRACrossRefDAsUrl: 'GetSameRACrossRefDAs/getDetails',
            ProgressNoteURL: 'admin/progressnotetype/listprognotetypes',
            ProgressNoteSubTypeUrl: 'admin/progressnotetype/listprognotesubtypes',
            IntakeServiceRouteDAUrl: 'Intakeservicerequests/routeda',
            SendtoSupervisorreviewUrl: 'Intakedastagings/reviewIntake',
            PersonRelativeUrl: 'personrelations',
            PersonAddressesUrl: 'personaddresses',
            TemporarySavedIntakeUrl: 'Intakedastagings/listdadetails',
            CompleteIntakeUrl: 'Intakedastagings/completeIntake',
            IntakeServiceRequestsUrl: 'Intakeservicerequests/getdsdsactions',
            MyIntakeListUrl: 'Intakedastagings/listintakeda',
            PersonUrl: 'People/getwholepersondetials',
            GetIntakeAssessmentUrl: 'admin/assessmenttemplate/getlist',
            ValidateAddressUrl: 'People/validateaddress',
            CommunicationUrl: 'Intakeservicerequestinputsources?filter={"order":"description"}',
            SuggestAddressUrl: 'People/suggestaddress',
            IntakePurposes: 'Intakeservicerequestpurposes',
            IntakeAgencies: 'admin/teamtype/list?filter={"order":"description"}',
            Intakeservs: 'Intakeservs',
            ResourceTooltipUrl: 'Resources/resourcelist',
            AttachmentGridUrl: 'Documentproperties/getintakeattachments',
            SaveAttachmentUrl: 'Documentproperties/addintakeattchment',
            DeleteAttachmentUrl: 'Documentproperties/delete',
            PersonInvolvedManageUrl: 'People/intakeperson',
            AddressTypeUrl: 'admin/personaddresstype',
            MDCountryListUrl: 'admin/county',
            OffenceCategoryListUrl: 'offensecategories',
            PhoneTypeUrl: 'admin/personphonetype',
            EmailTypeUrl: 'admin/personemailtype', 
            saveIntakeUrl: 'providerreferral/getproviderreferral',
            //saveIntakeUrl: 'Intakedastagings/listdadetails',
            SupervisorApprovalUrl: 'Intakedastagings/approveIntake',
            PriorAuditLogUrl: 'Intakeservicerequests/priors',
            EducationTypeUrl: 'educationtype',
            GradeTypeUrl: 'gradetype',
            TestingTypeUrl: 'testingtype',
            ListProgressTypeUrl: 'admin/progressnotetype/listprognotetypes',
            ContactRoleTypeUrl: 'Contactroletypes',
            ContactSubTypeUrl: 'admin/progressnotetype/listprognotesubtypes',
            AddContcatTypeUrl: 'admin/progressnote/addRecordings',
            GetAllNotesUrl: 'admin/progressnote/getalldarecording',
            SdmCountyListUrl : 'admin/county',
            allegationsUrl: 'admin/allegation/getallegationsindicators',
            allegationsSubTypes : 'admin/allegation/getcasesubtype',
            EvaluationSourceTypeList : 'evaluationsourcetypes',
            EvaluationAgencyTypeList : 'evaluationsourceagencies',
            EvaluationSourceSearchList : 'evaluationsources/list',
            validateComplaintId: 'Intakeservicerequestevaluations/isComplaintIdExists',
            intakeWorkerList: 'Intakedastagings/getIntakeUsers',
            hearingType: 'hearingtype?filter={"nolimit":true}',
            conditionType: 'conditiontype?filter={"nolimit":true}',
            courtActionType: 'courtactiontype?filter={"nolimit":true}',
            findingType: 'findingtype?filter={"nolimit":true}',
            courtOrderType: 'courtordertype?filter={"nolimit":true}',
            adjuctedDecisions: 'adjudicateddecisiontype?filter={"nolimit":true}',
            addpetition: 'intakeservicerequestpetition/add',
            addcourtdetails: 'intakeservicerequestcourtaction/add',
            saoComplete: 'Intakedastagings/clwClosedIntake',
            contactType: 'admin/progressnotetype/listprognotetypes?filter={"nolimit":true}',
            purposeType: 'progressnotepurposetype?filter={"nolimit":true}',
            typeofLocation: 'admin/progressnotetype/listprognotesubtypes',
            personsInvolvedType: 'contactroletypes?filter={"nolimit":true}',
            notesSave: 'admin/progressnote/addRecordings',
            notesEdit: 'admin/progressnote/getdarecordingdetails',
            notesList: 'admin/progressnote/getalldarecording',
            informationsourcetype: 'informationsourcetype',
            prescriptionreasontype: 'prescriptionreasontype',
            healthprofessiontype: 'healthprofessiontype',
            healthdomaintype: 'healthdomaintype',
            healthassessmenttype: 'healthassessmenttype',
            medicalconditiontype: 'medicalconditiontype',
            physicianspecialtytype: 'physicianspecialtytype',
            dentalspecialtytype: 'dentalspecialtytype',
            personservicetype: 'personservicetype',
            medicationtype: 'medicationtype',
            listprogramnamesUrl:'providerreferral/listprogramnames'

        },
        DSDSAction: {
            Attachment: {
                UploadAttachmentUrl: 'attachments/uploadsFile',
                AttachmentGridUrl: 'Documentproperties/getattachments',
                AttachmentUploadUrl: 'attachments/uploadsFile',
                AttachmentTypeUrl: 'Intake/attachmenttype',
                AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype',
                SaveAttachmentUrl: 'Documentproperties/addattchment'
            },
            Disposition: {
                StatusUrl: 'Intakeservicerequests/statuslist',
                DispositionUrl: 'Intakeservicerequests/dispositionlist',
                DispositionListUrl: 'Intakeservicerequestdispositioncodes/GetDispositon',
                DispositionAddUrl: 'Intakeservicerequestdispositioncodes/Add'
            }
        }
    };
    
}
