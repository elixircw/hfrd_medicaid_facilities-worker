import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AttachmentRoutingModule } from './attachment/attachment-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AttachmentRoutingModule
  ],
 
})
export class ProviderPortalTempUtilsModule { }
