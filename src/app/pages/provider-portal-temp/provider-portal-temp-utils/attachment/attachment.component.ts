import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { map } from 'rxjs/operators';

import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { GenericService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ProviderPortalUrlConfig } from '../../provider-portal-temp-url.config';
import { Attachment } from './_entities/attachment.data.models';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import {GeneratedDocuments} from './_entities/attachment.data.models';
import {AppUser} from '../../../../@core/entities/authDataModel';
import {AuthService} from '../../../../@core/services/auth.service';
import {DataStoreService} from '../../../../@core/services/data-store.service';
import { config } from '../../../../../environments/config';
import { AttachmentService } from './attachment.service';
import * as jsPDF from 'jspdf';



const SCREENING_WORKER = 'SCRNW';
declare var $: any;
import * as ALL_DOCUMENTS from './_configurtions/documents.json';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attachment',
    templateUrl: './attachment.component.html',
    styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit {
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    daNumber: string;
    id: string;
    baseUrl: string;
    filteredAttachmentGrid: Attachment[] = [];
    @ViewChild(EditAttachmentComponent) editAttach: EditAttachmentComponent;
    documentPropertiesId: any;
    documentId: any;
    generatedDocuments: GeneratedDocuments[] = [];
    allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;
    token: AppUser;
    config = {isGenerateUploadTabNeeded: false};
    downldSrcURL: any;
    downloadInProgress: boolean;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    curDate = new Date();
    searchContactCritera: any;
    contactList = [];
    deficiencyList = [];
    providerNm = '';
    summary = '';
    outcomes = '';
    constructor(
        private router: Router,
        private _dropDownService: CommonHttpService,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _service: GenericService<Attachment>,
        private _authService: AuthService,
        private _dataStoreService: DataStoreService,
        private _attachmentService: AttachmentService
    ) {
        this.id = this._attachmentService.getAttachmentConfig().uuid;
        this.daNumber = this._attachmentService.getAttachmentConfig().uniqueNumber;
        this.token = this._authService.getCurrentUser();
    }

    ngOnInit() {
        this.attachment();
        this.token = this._authService.getCurrentUser();
        this.prepareConfig();
        if (this.config.isGenerateUploadTabNeeded) {
            this.loadGeneratedDocumentList(this.token);
            this._dataStoreService.currentStore.subscribe((store) => {
                if (store['caseSummary']) {
                    const actionSummary = store['caseSummary'];
                    const jsonData = actionSummary['intake_jsondata'];
                    this.generatedDocuments = jsonData['generatedDocuments'] ? jsonData['generatedDocuments'] : [];
                }
                this.loadGeneratedDocumentList(this.token);
            });
        }
    }

    checkFileType(file: string, accept: string): boolean {
        if (accept) {
            const acceptedFilesArray = accept.split(',');
            return acceptedFilesArray.some(type => {
                const validType = type.trim();
                if (validType.charAt(0) === '.') {
                    return file.toLowerCase().endsWith(validType.toLowerCase());
                }
                return false;
            });
        }
        return true;
    }
   editAttachment(modal) {
       this.editAttach.editForm(modal);
       (<any>$('#edit-attachment')).modal('show');
    }
    confirmDelete(modal) {
        this.documentPropertiesId = modal.documentpropertiesid;
        this.documentId = modal.filename;
        (<any>$('#delete-attachment')).modal('show');
    }
    deleteAttachment() {
        var workEnv = config.workEnvironment
        if (workEnv=== 'state'){
                this._service.endpointUrl =
                ProviderPortalUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
                const id = this.documentPropertiesId + '&' + this.documentId;
                this._service.remove(id).subscribe(
                result => {
                    (<any>$('#delete-attachment')).modal('hide');
                    this.attachment();
                    this._alertService.success('Attachment Deleted successfully!');
                },
                err => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
            } else {
                this._service.endpointUrl =
                ProviderPortalUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
                this._service.remove(this.documentPropertiesId).subscribe(
                    result => {
                        (<any>$('#delete-attachment')).modal('hide');
                        this.attachment();
                        this._alertService.success('Attachment Deleted successfully!');
                    },
                    err => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            }
        
    }

    downloadFile(s3bucketpathname)
    {
        var workEnv = config.workEnvironment;
        if(workEnv == 'state') {
           this.downldSrcURL = this.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            //4200
          this.downldSrcURL = s3bucketpathname;
        }
        console.log("this.downloadSrc", this.downldSrcURL);
        window.open(this.downldSrcURL, '_blank');
    }

    private attachment() {
        this._dropDownService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get',
                    order:"originalfilename",
                }),
                ProviderPortalUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/' + this.daNumber + '?data'
            )
            .filter(result => result !== null)
            .subscribe((result) => {
                result.map(item => {
                    item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                });
                this.filteredAttachmentGrid = result;
            });
    }
    private humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        if (!bytes) {
            return '';
         }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    private loadGeneratedDocumentList(user: AppUser) {
        this.allDocuments.forEach(document => {
            if (document.access.indexOf(user.role.name) !== -1) {
                const isExist = this.generatedDocuments.find(gDocument => document.id === gDocument.id);
                if (!isExist) {
                    this.generatedDocuments.push(document);
                }
            }

        });

    }

    downloadDocument(document) {
        if (document.documentpath) {
            window.open(document.documentpath, '_blank');
        } else {
            this._dataStoreService.setData('documentsToDownload', [document.key]);
        }
    }

    downloadSelectedDocuments() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            this._dataStoreService.setData('documentsToDownload', selectedDocuments.map(document => document.key));

        }

    }

    getSelectedDocuments() {
        return this.generatedDocuments.filter(document => document.isSelected);
    }

    isFileSelected() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            return selectedDocuments.length > 0;
        }

        return false;
    }

    generateNewDocument(document) {
        document.isInProgress = true;
        setTimeout(() => {
            document.isGenerated = true;
            document.generatedBy = this.token.user.userprofile.displayname;
            document.generatedDateTime = new Date();
            document.isInProgress = false;
        }, 1000);

    }

    generateSelectedDocuments() {
        this.generatedDocuments.forEach(document => {
            if (document.isSelected) {
                this.generateNewDocument(document);
            }

        });
    }

    prepareConfig() {
        const isDjs = this._authService.isDJS();
        if (isDjs) {
           this.config.isGenerateUploadTabNeeded = true;
        } else {
            this.config.isGenerateUploadTabNeeded = false;
        }
    }

    toggleTable(id) {
        (<any>$('#' + id)).collapse('toggle');
    }

    collectivePdfCreator(element: string) {
        this.downloadCasePdf(element);
    }

    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            // console.log(pages.item(i).getAttribute('data-page-name'));
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
            });
        }
        this.convertImageToPdf();
    }

    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        this.goBack();
        this.pdfFiles = [];
        this.downloadInProgress = false;
    }
    goBack(): void {
        (<any>$('#payment-slip-document')).modal('hide');
        this.router.navigate(['../'], { relativeTo: this.route });
    }
}
