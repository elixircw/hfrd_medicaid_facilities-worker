import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services';
import { Router } from '@angular/router';
import { AttachmentConfig } from './_entities/attachment.data.models';
@Injectable()
export class AttachmentService {
  attachmentConfig: AttachmentConfig;
  constructor(private _router: Router, private _commonHttpService: CommonHttpService) {

  }

  setAttachmentConfig(attachmentConfig:AttachmentConfig) {
      this.attachmentConfig = attachmentConfig;
  }

  getAttachmentConfig():AttachmentConfig {
      return this.attachmentConfig;
  }
//   getAttachmentId(id) {
//     return this.attachmentId = id;
//   }   
}
