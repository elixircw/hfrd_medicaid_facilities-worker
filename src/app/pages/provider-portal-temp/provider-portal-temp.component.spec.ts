import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderPortalTempComponent } from './provider-portal-temp.component';

describe('ProviderApplicantComponent', () => {
  let component: ProviderPortalTempComponent;
  let fixture: ComponentFixture<ProviderPortalTempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderPortalTempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderPortalTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
