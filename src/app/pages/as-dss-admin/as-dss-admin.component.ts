import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs/Rx';
import { PaginationInfo, PaginationRequest } from '../../@core/entities/common.entities';
import { CommonHttpService } from '../../@core/services/common-http.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'as-dss-admin',
    templateUrl: './as-dss-admin.component.html',
    styleUrls: ['./as-dss-admin.component.scss']
})
export class AsDssAdminComponent implements OnInit {
    private pageStream$ = new Subject<number>();
    paginationInfo: PaginationInfo = new PaginationInfo();
    dssDashboard$: Observable<any[]>;
    totalRecord$: Observable<number>;
    inputRequest: {};
    searchForm: FormGroup;
    constructor(private _commonService: CommonHttpService, private router: Router,private _formBuild: FormBuilder,) {}

    ngOnInit() {
        this.pageStream$.subscribe(data => {
            this.paginationInfo.pageNumber = data;
            this.getPage(data);
        });
        this.initializeForm();
        this.getPage(1);
    }
    private initializeForm() {
        this.searchForm = this._formBuild.group({
            // countyid: [''],
            // dateofmeeting: ['', Validators.required],
            startdate: [null],
            enddate: [null],
            meetingstatus: [''],
            meetingdate: ['']
        });
    }
    searchMeeting(modal) {
        status = 'all';
        this.inputRequest = modal;
        this.getPage(1);
    }

    getPage(page: number) {
        const source = this._commonService
            .getArrayList(
                new PaginationRequest({
                    where: this.inputRequest,
                    limit: this.paginationInfo.pageSize,
                    page: this.paginationInfo.pageNumber,
                    // status: status
                    method: 'post'
                }),
                'Guardinshipmeeting/dashboardlist'
            )
            .map((result: any) => {
                const list = result.data.map(item => {
                    return item;
                });
                return {
                    data: list,
                    count: result.data.length ? result.data[0].totalcount : ''
                };
            });

        this.dssDashboard$ = source.pluck('data');
        if (page === 1) {
            this.totalRecord$ = source.pluck('count');
        }
    }

    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    redirectToCase(caseDeatils) {
      if (caseDeatils) {
        this.router.navigate(['/pages/case-worker/' + caseDeatils.intakeserviceid + '/' + caseDeatils.servicerequestnumber + '/dsds-action/as-recording/as-notes']);
      }
    }
}
