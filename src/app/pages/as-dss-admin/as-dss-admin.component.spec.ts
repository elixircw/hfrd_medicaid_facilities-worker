import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsDssAdminComponent } from './as-dss-admin.component';

describe('AsDssAdminComponent', () => {
  let component: AsDssAdminComponent;
  let fixture: ComponentFixture<AsDssAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsDssAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsDssAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
