import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsDssAdminComponent } from './as-dss-admin.component';

const routes: Routes = [{
  path: '',
  component: AsDssAdminComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsDssAdminRoutingModule { }
