import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsDssAdminRoutingModule } from './as-dss-admin-routing.module';
import { AsDssAdminComponent } from './as-dss-admin.component';
import { PaginationModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatCardModule, MatInputModule, MatNativeDateModule, MatRadioModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    AsDssAdminRoutingModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule
  ],
  declarations: [AsDssAdminComponent]
})
export class AsDssAdminModule { }
