import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpService } from '../../@core/services/http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertService, SessionStorageService, CommonHttpService, GenericService, AuthService } from '../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { PaginationRequest, PaginationInfo } from '../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../app.config';
import { environment } from '../../../environments/environment.dev';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { config } from '../../../environments/config';
import { HttpHeaders } from '@angular/common/http';
import { AppUser } from '../../@core/entities/authDataModel';
import { GLOBAL_MESSAGES } from '../../@core/entities/constants';
import { GetintakAssessment } from '../case-worker/_entities/caseworker.data.model';
import * as moment from 'moment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../case-worker/case-worker-url.config';
import { Placement } from '../case-worker/dsds-action/placement/_entities/placement.model';
import { Subject } from 'rxjs/Subject';

declare var $: any;
declare var Formio: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'medical-practitioner',
  templateUrl: './medical-practitioner.component.html',
  styleUrls: ['./medical-practitioner.component.scss']
})
export class MedicalPractitionerComponent implements OnInit {
  showUpload = false;
  toSave = false;
  id: any;
  attachmentTypeForm: FormGroup;
  startAssessment$: Observable<any[]>;
  totalRecord$: Observable<number>;
  paginationInfo: PaginationInfo = new PaginationInfo();
  showAssesment = -1;
  getAsseesmentHistory: GetintakAssessment[] = [];
  assessmmentName: any;
  currentTemplateId: any;
  attachmentType: string;
  placement: Placement;
  selectedSafeCDangerInfluence: any[] = [];
  medicalDashboard$: Observable<any[]>;
  uploadedFile = [];
  currentUser: AppUser;
  fileToSave = [];
  attachmentResponse: any;
  daNumber: any;
  intakeserviceid: any;
  personId: any;
  selectedAssessment: any;
  selectedStatus = '';
  intakeSummary: any;
  involvedPersons: any;
  private pageStream$ = new Subject<number>();
  assToCopy: any;
  intakeNumber = '';



  constructor(private route: ActivatedRoute,
    private _authService: AuthService,
    private _service: GenericService<any>,
    private _commonService: CommonHttpService,
    private storage: SessionStorageService,
    private _alertService: AlertService,
    public sanitizer: DomSanitizer,
    private _formbuilder: FormBuilder,
    private _http: HttpService,
    private _uploadService: NgxfUploaderService,
    private _cd: ChangeDetectorRef) {
    this.currentUser = this._authService.getCurrentUser();
    this.daNumber = '201800411047';
  }

  ngOnInit() {
    this.intializeForm();
    this.pageStream$.subscribe((data) => {
      this.paginationInfo.pageNumber = data;
      this.getPage(this.selectedStatus, data);
    });
    this.getPage('Pending', 1);
  }

  intializeForm() {
    this.attachmentTypeForm = this._formbuilder.group({
      assessmentsubmissiontypekey: [null, Validators.required]
    });
  }

  toggleUpload(event) {
    if (event === '1') {
      this.showUpload = true;
    } else {
      this.showUpload = false;
    }
  }

  getPage(status, page: number) {
    this.showAssesment = -1;
    this.selectedStatus = status;
    if (this.currentUser.role.name === 'Provider Applicant') {
      const source = this._commonService
      .getArrayList(
        new PaginationRequest({
          limit: this.paginationInfo.pageSize,
          page: this.paginationInfo.pageNumber,
          where: {
            status: status
          },
          method: 'get'
        }),
        'Assignedassessments/getproviderassessmentdashboard?filter'
      )
      .map((result: any) => {
        const list = result.data.map(item => {
          item.isNew = this.toShowPlay(item);
          return item;
        });
        console.log(list);
        return {
          data: list, count: result.data.length ? result.data[0].totalcount : ''
        };
      });

      this.medicalDashboard$ = source.pluck('data');
      if (page === 1) {
        this.totalRecord$ = source.pluck('count');
      }
    } else {
      const source = this._commonService
      .getArrayList(
        new PaginationRequest({
          limit: this.paginationInfo.pageSize,
          page: this.paginationInfo.pageNumber,
          where: {
            status: status
          },
          method: 'get'
        }),
        'Assignedassessments/getassessmentdashboard?filter'
      )
      .map((result: any) => {
        const list = result.data.map(item => {
          item.isNew = this.toShowPlay(item);
          return item;
        });
        console.log(list);
        return {
          data: list, count: result.data.length ? result.data[0].totalcount : ''
        };
      });

      this.medicalDashboard$ = source.pluck('data');
      if (page === 1) {
        this.totalRecord$ = source.pluck('count');
      }
    }

  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.pageStream$.next(this.paginationInfo.pageNumber);
  }

  startAssessment(assessment) {
    this.selectedAssessment = assessment;
    this.getIntakeSummary();
    this.assessmmentName = assessment.titleheadertext;
    this.currentTemplateId = assessment.external_templateid;
    this.intakeserviceid = assessment.intakeserviceid;
    this.intakeNumber = assessment.intakenumber;
    const _self = this;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`).then(function (form) {
      // Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/5bbd15292229906dcc0e14b1`).then(function (form) {
      form.components = form.components.map((item) => {
        if (item.key === 'Complete' && item.type === 'button') {
          item.action = 'submit';
        }
        return item;
      });
      form.submission = {
        data: _self.getFormPrePopulation(_self.assessmmentName, form.data, assessment.displayname, assessment.dob)
      };
      form.on('submit', (submission) => {
        // this.getPerson();
        if (_self.assessmmentName === 'SAFE-C') {
          submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
        }
        let status = '';
        if (submission.data.submit) {
          status = 'InProcess';
        } else {
          status = 'InProcess';
        }

        if (_self.currentUser.role.name === 'Provider Applicant') {
        _self._http
          .post('admin/assessment/Add', {
            externaltemplateid: _self.currentTemplateId,
            assessmentstatustypekey1: status,
            objectid: _self.intakeNumber ? _self.intakeNumber : _self.intakeserviceid,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0
          })
          .subscribe((response) => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            _self.getPage('Pending', 1);
            (<any>$('#iframe-popup')).modal('hide');
          });
        } else {
          _self._http
          .post('admin/assessment/Add', {
            externaltemplateid: _self.currentTemplateId,
            assessmentstatustypekey1: status,
            objectid: _self.intakeserviceid,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0
          })
          .subscribe((response) => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            _self.getPage('Pending', 1);
            (<any>$('#iframe-popup')).modal('hide');
          });
        }
      });
      form.on('change', (formData) => {
        // _self.safeCProcess(formData);
      });
      form.on('render', (formData) => {
        (<any>$('#iframe-popup')).modal('show');
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });

      form.on('error', (error) => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
        _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
      });
    });
  }

  private getFormPrePopulation(formName: string, submissionData: any, name: string, dob: string) {
    let assToCopy = {};
    if (formName === 'C.A.R.E. Home - Provider Back-up Medical Form') {
      if (this.selectedAssessment.address && this.selectedAssessment.countyname && this.selectedAssessment.zipcode) {
        submissionData['countyDetailsText'] = this.selectedAssessment.countyname + ', ' + this.selectedAssessment.address + ', ' + this.selectedAssessment.zipcode;
      }
    }
    if (this.selectedAssessment && this.selectedAssessment.intakeassessment && this.selectedAssessment.intakeassessment.length > 0) {
      assToCopy = this.selectedAssessment.intakeassessment[0].submissiondata;
      submissionData = assToCopy;
    } else {
      console.log(submissionData, assToCopy);
      submissionData['ResidentsName'] = this.selectedAssessment ? this.selectedAssessment.displayname : submissionData['ResidentsName'];
      submissionData['date'] = moment(new Date()).format('YYYY-MM-DD');
      submissionData['mainDate'] = moment(new Date()).format('YYYY-MM-DD');
      submissionData['ResidentsDOB'] = moment(this.selectedAssessment.dob).format('YYYY-MM-DD');
      if (this.reportedAdultDetails && this.reportedAdultDetails.length > 0) {
        submissionData['PhoneNumber'] = this.reportedAdultDetails[0].phonenumber ? this.reportedAdultDetails[0].phonenumber : '';
        submissionData['panel5000110429470297Columns2PhoneNumber'] = this.reportedAdultDetails[0].phonenumber ? this.reportedAdultDetails[0].phonenumber : '';
      }
      submissionData['Resident'] = this.selectedAssessment ? this.selectedAssessment.displayname : submissionData['Resident'];
      if (this.intakeSummary && this.intakeSummary.providername) {
        let providerName = '';
        this.intakeSummary.providername.forEach(element => {
          providerName += element.providername + ', ';
        });

        //   if (this.placement) {
        //     submissionData['PlacementDate'] = this.placement.placedatetime;
        //     submissionData['panel5000110429470297Columns2PhoneNumber'] = this.placement.providerinfo.phonenumber;
        // }
        submissionData['CareProvider'] = providerName;
        submissionData['CAREProvider'] = providerName;
        submissionData['DateCompleted'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['RESIDENTSNAME'] = this.selectedAssessment ? this.selectedAssessment.displayname : submissionData['RESIDENTSNAME'];
        submissionData['ResidentsDOB1'] = moment(this.selectedAssessment.dob).format('YYYY-MM-DD');
        submissionData['NurseConsDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['NextReviewDate'] = moment(new Date()).format('YYYY-MM-DD');
      }
      submissionData['RegisteredNurseConsultant'] = this.currentUser.user.userprofile.displayname ? this.currentUser.user.userprofile.displayname : submissionData['RegisteredNurseConsultant'];
      submissionData['NurseConsultant'] = this.currentUser.user.userprofile.displayname ? this.currentUser.user.userprofile.displayname : submissionData['NurseConsultant'];
      if (this.reportedAdultDetails && this.reportedAdultDetails.length > 0 && this.reportedAdultDetails[0].medicationinformation) {
        submissionData['panel06311922059450592Columns2DataGrid'] = [];
        this.reportedAdultDetails[0].medicationinformation.forEach((data) => {
          submissionData['panel06311922059450592Columns2DataGrid'].push({
            TreatmentsWithDirections: data.medicationname + ', ' + data.frequency + ', ' + data.dosage,
            ReasonforMedicationorTreatment: data.prescriptionreason,
            RelatedTestingorMonitoring: data.monitoring
          });
        });
        submissionData['subPanel2DataGrid'] = [];
        this.reportedAdultDetails[0].medicationinformation.forEach((data) => {
          submissionData['subPanel2DataGrid'].push({
            Medicationname: data.medicationname,
            Dosage: data.dosage,
            Frequency: data.frequency,
            Prescriptionreason: data.prescriptionreason
          });
        });
      }
      submissionData['applicantname'] = (name) ? name : submissionData['applicantname'];
      submissionData['Date'] = moment(new Date()).format('YYYY-MM-DD');
      submissionData['NextDate'] = moment(new Date()).format('YYYY-MM-DD');
      submissionData['DateCompleted'] = moment(new Date()).format('YYYY-MM-DD');
      submissionData['applicantdob'] = (dob) ? moment(new Date(dob)).format('YYYY-MM-DD') : submissionData['applicantdob'];
    }
    return submissionData;
  }

  get reportedAdultDetails() {
    if (this.involvedPersons) {
      return this.involvedPersons.filter(item => {
        return item.rolename === 'RA' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RA').length);
      });
    } else {
      return null;
    }
  }

  private setPhysicalAttr(personHeight, personWeight) {
    this._commonService
      .getSingle(
        {
          personId: this.selectedAssessment.personid,
          height: personHeight,
          weight: personWeight,
          method: 'post'
        },
        'people/updatepersonphysicalattribute'
      ).subscribe(data => {
        // this.involvedPersons = data[0]['data'];
      });
  }
  getIntakeSummary() {
    const source = forkJoin(
      this._commonService.getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: { intakeservreqid: this.selectedAssessment.intakeserviceid }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
      ),
      this._commonService.getPagedArrayList(
        {
          where: {
            intakeserviceid: this.id
          },
          page: 1,
          limit: 50,
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementListUrl + '?filter'
      ),
      this._commonService
        .getSingle(
          {
            intakeserviceid: this.selectedAssessment.intakeserviceid,
            method: 'post'
          },
          'Investigations/getinvestigationsummary'
        )).subscribe((data) => {
          this.involvedPersons = data[0]['data'];
          this.placement = data[1]['data'][0];
          this.intakeSummary = data[2][0];
        });
  }

  showAssessment(id: number, row) {
    this.getAsseesmentHistory = row;
    if (this.showAssesment !== id) {
      this.showAssesment = id;
    } else {
      this.showAssesment = -1;
    }
  }


  // file upload functions
  uploadFile(file: File | FileError): void {
    if (!(file instanceof Array)) {
      return;
    }
    file.map((item, index) => {
      const fileExt = item.name
        .toLowerCase()
        .split('.')
        .pop();
      if (
        fileExt === 'mp3' ||
        fileExt === 'ogg' ||
        fileExt === 'wav' ||
        fileExt === 'acc' ||
        fileExt === 'flac' ||
        fileExt === 'aiff' ||
        fileExt === 'mp4' ||
        fileExt === 'mov' ||
        fileExt === 'avi' ||
        fileExt === '3gp' ||
        fileExt === 'wmv' ||
        fileExt === 'mpeg-4' ||
        fileExt === 'pdf' ||
        fileExt === 'txt' ||
        fileExt === 'docx' ||
        fileExt === 'doc' ||
        fileExt === 'xls' ||
        fileExt === 'xlsx' ||
        fileExt === 'jpeg' ||
        fileExt === 'jpg' ||
        fileExt === 'png' ||
        fileExt === 'ppt' ||
        fileExt === 'pptx' ||
        fileExt === 'gif'
      ) {
        this.uploadedFile.push(item);
        this.uploadAttachment(index);
      } else {
        // tslint:disable-next-line:quotemark
        this._alertService.error(fileExt + " format can't be uploaded");
        return;
      }
    });
  }

  uploadAttachment(index) {
    let uploadURL = '';
    if (config.workEnvironment === 'state') {
      uploadURL = AppConfig.baseUrl +
        'attachment/v1/' + 'attachments/uploadsFile' + '?access_token=' + this.currentUser.id + '&' + 'srno=' + this.daNumber;
    } else {
      uploadURL = AppConfig.baseUrl +
        'attachments/uploadsFile' + '?access_token=' + this.currentUser.id + '&' + 'srno=' + this.daNumber;
    }
    this._uploadService
      .upload({
        url: uploadURL,
        headers: new HttpHeaders().set('access_token', this.currentUser.id).set('ctype', 'file'),
        filesKey: ['file'],
        files: this.uploadedFile[index],
        process: true
      })
      .subscribe(
        (response) => {
          if (response.status) {
            this.uploadedFile[index].percentage = response.percent;
          }
          if (response.status === 1 && response.data) {
            this.attachmentResponse = response.data;
            this.fileToSave.push(response.data);
            this.fileToSave[this.fileToSave.length - 1].documentattachment = {
              attachmenttypekey: '',
              attachmentclassificationtypekey: '',
              attachmentdate: new Date(),
              sourceauthor: '',
              attachmentsubject: '',
              sourceposition: '',
              attachmentpurpose: '',
              sourcephonenumber: '',
              acquisitionmethod: '',
              sourceaddress: '',
              locationoforiginal: '',
              insertedby: this.currentUser.user.userprofile.displayname,
              note: '',
              updatedby: this.currentUser.user.userprofile.displayname,
              activeflag: 1
            };
            this.fileToSave[this.fileToSave.length - 1].description = '';
            this.fileToSave[this.fileToSave.length - 1].documentdate = new Date();
            this.fileToSave[this.fileToSave.length - 1].title = '';
            this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
            this.fileToSave[this.fileToSave.length - 1].objecttypekey = 'ServiceRequest';
            this.fileToSave[this.fileToSave.length - 1].rootobjecttypekey = 'ServiceRequest';
            this.fileToSave[this.fileToSave.length - 1].activeflag = 1;
            this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
            this.fileToSave[this.fileToSave.length - 1].insertedby = this.currentUser.user.userprofile.displayname;
            this.fileToSave[this.fileToSave.length - 1].updatedby = this.currentUser.user.userprofile.displayname;
          }
        },
        (err) => {
          console.log(err);
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          this.uploadedFile.splice(index, 1);
        }
      );
  }

  submitAttachmentType() {
    let data = {};
    if (this.attachmentTypeForm.valid) {
      data = {
        'assessmenttemplateid': this.selectedAssessment.assessmenttemplateid,
        'securityusersid': this.currentUser.user.securityusersid,
        'assessmentsubmissiontypekey': this.attachmentTypeForm.value.assessmentsubmissiontypekey,
        'objectid': this.selectedAssessment.intakeserviceid,
        'submissionid': null,
        'assessmentname': this.selectedAssessment.titleheadertext
      };
      this.saveTypeAssessment(data);
      (<any>$('#type-select')).modal('hide');
      this.attachmentTypeForm.reset();
    } else {
      this._alertService.error('Please select type');
    }
  }

  completeAssessment(assessment) {
    let data = {};
    if (this.currentUser.role.name === 'Provider Applicant') {
      if (!assessment.isNew) {
      data = {
        'assessmenttemplateid': assessment.assessmenttemplateid,
        'securityusersid': assessment.securityusersid,
        'intakenumber': assessment.intakenumber,
        'objectid': assessment.intakeserviceid,
        'submissionid': assessment.intakeassessment[0].submissionid
      };
      const url = 'Assignedassessments/assessmentcompletedbyproviderapplicant';
      this._commonService.create(data, url).subscribe(result => {
        this._alertService.success('Completed Successfully');
        this.getPage('Pending', this.paginationInfo.pageNumber);
      });
    } else {
      this._alertService.warn('Please Save or Complete the assessment');
    }
    } else {
    if (assessment) {
      this.selectedAssessment = assessment;
      if (!assessment.isNew) {
        data = {
          'assessmenttemplateid': assessment.assessmenttemplateid,
          'securityusersid': this.currentUser.user.securityusersid,
          'objectid': assessment.intakeserviceid,
          'submissionid': assessment.intakeassessment[0].submissionid,
          'assessmentname': assessment.titleheadertext
        };
        this.saveTypeAssessment(data);
      } else {
        (<any>$('#type-select')).modal('show');
        }
      }
    }
  }

  calculateDueDate(givenDate) {
    const date = new Date(givenDate);
    date.setDate(date.getDate() + 10);
    return date;
  }

  saveTypeAssessment(data) {
    const url = 'Assignedassessments/assessmentcompleted';
    this._commonService.create(data, url).subscribe(result => {
      console.log(result);
      this.getPage('Pending', this.paginationInfo.pageNumber);
    });
  }

  assessmentPrintView(assessment: GetintakAssessment, needData) {
    const _self = this;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    let url = '';
    if (needData) {
      url = environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`;
    } else {
      url = environment.formBuilderHost + `/form/${assessment.external_templateid}`;
    }
    Formio.createForm(document.getElementById('assessmentForm'), url, {
      readOnly: true
    }).then(function (submission) {
      const options = {
        ignoreLayout: true
      };
      _self.viewHtml(submission._form, submission._submission, options);
    });
  }

  viewHtml(componentData, submissionData, formioOptions) {
    delete submissionData._id;
    delete submissionData.owner;
    delete submissionData.modified;
    const exporter = new FormioExport(componentData, submissionData, formioOptions);
    const appDiv = document.getElementById('assPrintView');
    exporter.toHtml().then((html) => {
      html.style.margin = 'auto';
      const iframe = this.createIframe(appDiv);
      const doc = iframe.contentDocument || iframe.contentWindow.document;
      doc.body.appendChild(html);
      window.frames['ifAssessmentView'].focus();
      window.frames['ifAssessmentView'].print();
    });
  }

  private createIframe(el) {
    _.forEach(el.getElementsByTagName('iframe'), (_iframe) => {
      el.removeChild(_iframe);
    });
    const iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'ifAssessmentView');
    iframe.setAttribute('name', 'ifAssessmentView');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('webkitallowfullscreen', '');
    iframe.setAttribute('mozallowfullscreen', '');
    iframe.setAttribute('allowfullscreen', '');
    iframe.setAttribute('style', 'width: -webkit-fill-available;height: -webkit-fill-available;');
    el.appendChild(iframe);
    return iframe;
  }

  submittedAssessment(assessment: GetintakAssessment, readOnly) {
    this.assessmmentName = assessment.titleheadertext;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    let url = '';
    if (readOnly) {
      url = environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`;
    } else {
      url = environment.formBuilderHost + `/form/${assessment.external_templateid}`;
    }
    Formio.createForm(document.getElementById('assessmentForm'), url, {
      readOnly: true
    }).then(function (submission) {
      (<any>$('#iframe-popup')).modal('show');
      submission.on('render', (formData) => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });
    });
  }

  updateAssessment(comepleteAssessment, assessment) {
    this.selectedAssessment = comepleteAssessment;
    this.getIntakeSummary();
    this.assessmmentName = assessment.titleheadertext;
    this.intakeNumber = assessment.intakenumber;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    // this.assessmmentName = assessment.description;
    // this.currentTemplateId = assessment.external_templateid;
    const _self = this;
    // Formio.setToken(this.storage.getObj('fbToken'));
    // Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
      readOnly: false
    }).then(function (form) {
      form.components = form.components.map(item => {
        if (item.key === 'Complete' && item.type === 'button') {
          item.action = 'submit';
        }
        return item;
      });
      form.submission = {
        data: _self.getFormPrePopulation(_self.assessmmentName, form.data, assessment.displayname, assessment.dob)
      };
      (<any>$('#iframe-popup')).modal('show');
      form.on('render', formData => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });
      form.on('submit', submission => {
        if (_self.assessmmentName === 'SAFE-C') {
          submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
          const height = submission.data['Height'] ? submission.data['Height'] : '';
          const weight = submission.data['Weight'] ? submission.data['Weight'] : '';
          const personId = submission.data['Weight'] ? submission.data['Weight'] : '';
          _self.setPhysicalAttr(height, weight);
        }
        // let status = '';
        // let comments = '';
        // if (_self.token.role.name === 'apcs') {
        //   status = submission.data.assessmentstatus;
        //   comments = submission.data.supervisorcomments2;
        // } else if (_self.token.role.name === 'field') {
        //   status = submission.data.assessmentreviewed;
        //   comments = submission.data.caseworkercomments;
        // }
        let status = '';
        if (submission.data.submit) {
          status = 'InProcess';
        } else {
          status = 'InProcess';
        }

        if (_self.currentUser.role.name === 'Provider Applicant') {
        _self._http
          .post('admin/assessment/Add', {
            externaltemplateid: _self.currentTemplateId,
            objectid: _self.intakeNumber,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0,
            // assessmentstatustypekey1: status,
            // comments: comments
            assessmentstatustypekey1: status,
          })
          .subscribe(response => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            _self.getPage('Pending', 1);
            _self.showAssessment(_self.showAssesment, _self.getAsseesmentHistory);
            (<any>$('#iframe-popup')).modal('hide');
          });
        } else {
          _self._http
          .post('admin/assessment/Add', {
            externaltemplateid: _self.currentTemplateId,
            objectid: _self.intakeserviceid,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0,
            // assessmentstatustypekey1: status,
            // comments: comments
            assessmentstatustypekey1: status,
          })
          .subscribe(response => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            _self.getPage('Pending', 1);
            _self.showAssessment(_self.showAssesment, _self.getAsseesmentHistory);
            (<any>$('#iframe-popup')).modal('hide');
          });
        }
      });
      form.on('change', formData => {
        // _self.safeCProcess(formData);
      });
      // form.on('render', (formData) => {
      //     (<any>$('#iframe-popup')).modal('show');
      //     setTimeout(function () {
      //         $('#assessment-popup').scrollTop(0);
      //     }, 200);
      // });

      form.on('error', error => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
        _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
      });
    });
  }

  toShowPlay(assessment) {
    if (assessment.intakeassessment && assessment.intakeassessment.length) {
      const history = assessment.intakeassessment;
      const draft = history.find(item => item.assessmentstatustypekey === 'InProcess' || item.assessmentstatustypekey === 'Review');
      // console.log('draft found', draft);
      if (draft) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }

  }

}

    // const assessmentRequest = {
    //   intakeservicerequesttypeid: '',
    //   intakeservicerequestsubtypeid: '',
    //   agencycode: this._authService.getAgencyName(),
    //   intakenumber: this.id,
    //   target: 'Intake'
    // };

    // this._http.overrideUrl = false;
    // this._http.baseUrl = AppConfig.baseUrl;
    // this.showAssesment = -1;
    // const source = this._service
    //   .getPagedArrayList(
    //     new PaginationRequest({
    //       page: this.paginationInfo.pageNumber,
    //       limit: this.paginationInfo.pageSize,
    //       where: assessmentRequest,
    //       method: 'get'
    //     }),
    //     'admin/assessmenttemplate/getintakeassessment?filter'
    //   )
    //   .map(result => {
    //     return { data: result.data, count: result.count };
    //   })
    //   .share();
    // this.startAssessment$ = source.pluck('data');
    // if (page === 1) {
    //   this.totalRecords$ = source.pluck('count');
    // }
