import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { Subject } from 'rxjs/Rx';
import * as moment from 'moment';
import { DynamicObject, PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { AuthService } from '../../../@core/services/auth.service';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { GenericService } from '../../../@core/services/generic.service';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { MyIntakeDetails } from '../my-newintake/_entities/newintakeModel';
import { BroadCostMessage, SearchCase } from '../my-newintake/_entities/newintakeSaveModel';
import { NewUrlConfig } from '../newintake-url.config';
import { IntakeConfigService } from '../my-newintake/intake-config.service';
import { ActivatedRoute } from '@angular/router';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'new-saveintake',
    templateUrl: './new-saveintake.component.html',
    styleUrls: ['./new-saveintake.component.scss']
})
export class NewSaveintakeComponent implements OnInit {
    showHistory: boolean;
    searchHistory = [];
    searchIntakeForm: FormGroup;
    paginationInfo: PaginationInfo = new PaginationInfo();
    intakes: MyIntakeDetails[];
    totalRecords: number;
    dynamicObject: DynamicObject = {};
    showBroadCostMessage: BroadCostMessage;
    roleName: string;
    isPreIntake = false;
    previousPage: number;
    narrative =  '';
    searchCriteria: SearchCase;
    private searchTermStream$ = new Subject<DynamicObject>();
    private pageStream$ = new Subject<number>();
    isDjs = false;
    status: string;
    agency: string;
    isCW: boolean;
    constructor(
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _service: GenericService<MyIntakeDetails>,
        private _commonHttpService: CommonHttpService,
        private _intakeUtils: IntakeUtils,
        private _intakeConfig: IntakeConfigService,
        private route: ActivatedRoute
    ) {
        this._intakeConfig.setPurposeList(this.route.snapshot.data.purposeList);
    }

    ngOnInit() {
        this.formInitilize();
        this.paginationInfo.sortBy = 'desc';
        this.paginationInfo.sortColumn = 'updateddate';
        const role = this._authService.getCurrentUser();
        this.agency = this._authService.getAgencyName();
        this.isDjs = role.user.userprofile.teamtypekey === 'DJS';
        this.isCW = role.user.userprofile.teamtypekey === 'CW';
        if (role.role.name === 'superuser' || role.role.name === 'cru' || role.role.name === 'ASCW') {
            this.getBroadCostMessage();
        }
        this.roleName = role.role.name;
        if (this.roleName === 'SCRNW') {
            this.isPreIntake = true;
        }
        this.showHistory = false;
        this.status = 'pending';
        this.getPage(1, 'pending');
    }
    formInitilize() {
        this.searchIntakeForm = this.formBuilder.group({
            intakenumber: ['']
        });
    }
    getPage(selectPageNumber: number, status: string) {
        this.status = status;
        const pageSource = this.pageStream$.map(pageNumber => {
            if (this.paginationInfo.pageNumber !== 1) {
                this.previousPage = pageNumber;
            } else {
                this.previousPage = this.paginationInfo.pageNumber;
            }
            return { search: this.dynamicObject, page: this.previousPage };
        });
        const searchSource = this.searchTermStream$.debounceTime(1000).map(searchTerm => {
            this.previousPage = 1;
            this.dynamicObject = searchTerm;
            if (searchTerm && searchTerm.intakenumber) {
                this.searchHistory.push({ intakeNumber: searchTerm.intakenumber.like.replace('%25', '').replace('%25', '') });
            }
            this.showHistory = false;
            return { search: searchTerm, page: this.previousPage };
        });
        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObject,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                let intakeNumber = '';
                if (this.searchIntakeForm.value.intakenumber) {
                    intakeNumber = this.searchIntakeForm.value.intakenumber;
                }
                this.searchCriteria = {
                    status: this.status,
                    intakenumber: intakeNumber,
                    ispreintake: this.isPreIntake,
                    sortcolumn: this.paginationInfo.sortColumn,
                    sortorder: this.paginationInfo.sortBy
                };
                return this._service.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.paginationInfo.pageSize,
                        page: this.previousPage,
                        method: 'post',
                        where: this.searchCriteria
                    }),
                    NewUrlConfig.EndPoint.Intake.saveIntakeUrl
                );
            })
            .subscribe(result => {
                this.intakes = result.data;
                this.totalRecords = result.count;

                // D-07003 Start
                let i = 0;
                for (const intake of this.intakes) {
                    // this.intakes[i].narrative = decodeURI(this.intakes[i].narrative);
                    // console.log('intake.datereceived ' + intake.datereceived);
                    // const datereceivedutc = moment.utc(intake.datereceived);
                    // console.log('datereceivedutc ' + datereceivedutc);
                    // const datereceivedeststr = datereceivedutc.utcOffset(-8).format('MM/DD/YYYY, h:mm A');
                    // console.log('datereceivedeststr ' + datereceivedeststr);
                    // this.intakes[i].datereceivedeststr = datereceivedeststr;
                    // console.log('this.intakes[i].datereceivedeststr ' + this.intakes[i].datereceivedeststr);

                    // this.intakes[i].datereceivedeststr = moment(intake.datereceived).format('MM/DD/YYYY, h:mm A');
                    const datereceivedutc = moment(intake.datereceived).format('MM/DD/YYYY, h:mm A');
                    // const datereceivedeststr = datereceivedutc.utcOffset(-5).format('MM/DD/YYYY, h:mm A');
                    this.intakes[i].datereceivedeststr = datereceivedutc;

                    //const updateddateutc = moment.utc(intake.updateddate);
                    //const updateddateeststr = updateddateutc;
                    //this.intakes[i].updateddateeststr = moment(updateddateutc).format('MM/DD/YYYY, h:mm A');

                    this.intakes[i].updateddateeststr = moment(intake.updateddate).format('MM/DD/YYYY, h:mm A');

                    i = i + 1;
                }
                // D-07003 End

                
                //@Simar - map the purpose for each intake
                // We are computing this Purpose object on the UI itself based on the jsondata object
                this.intakes = this.intakes.map((item) => {
                    item.purpose = {}
                    if (item.jsondata) {
                        const purposeitem = item.jsondata.General.Purpose;
                        const purposeid = purposeitem.split('~')[0];
                        item.purpose = this._intakeConfig.getSelectedPurpose(purposeid);
                    }
                    return item;
                });


            });
    }
    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortDirection;
        this.paginationInfo.sortColumn = $event.sortColumn;
        this.getPage(this.paginationInfo.pageNumber, this.status);
    }
    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.previousPage = this.paginationInfo.pageNumber;
        this.getPage(this.previousPage, this.status);
    }

    onSearch(field: string, value: string) {
        this.showHistory = true;
        if(this.isCW && value) {
            value = value.replace(' ', '');
        }        
        this.searchIntakeForm.patchValue({ intakenumber: value });
        this.dynamicObject[field] = { like: '%25' + value + '%25' };
        if (!value) {
            delete this.dynamicObject[field];
        }
        this.searchTermStream$.next(this.dynamicObject);
    }

    htmlToPlaintext(text) {
        // console.log(text);
        // console.log(String(text).replace(/<[^>]+>/gm, ''));
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    }
    getBroadCostMessage() {
        this._commonHttpService.getSingle({}, 'announcement/getuserannouncement').subscribe(result => {
            if (result !== null) {
                (<any>$('#broadcoastmessage')).modal('show');
                this.showBroadCostMessage = result;
            }
        });
    }
    showHideHistory(value) {
        this.showHistory = value;
    }
    acceptAnnouncement() {
        this._commonHttpService.endpointUrl = 'announcement/acceptannouncement';
        (<any>$('#broadcoastmessage')).modal('hide');
        this._commonHttpService.patch(this.showBroadCostMessage.userannouncementid, { id: this.showBroadCostMessage.userannouncementid }).subscribe(data => { });
    }
    openIntake(intakeNumber) {
        if (intakeNumber) {
            this._intakeUtils.redirectIntake(intakeNumber, 'edit');
        } else {
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.GetNextNumberUrl).subscribe(result => {
                this._intakeUtils.redirectIntake(result['nextNumber'], 'add');
            });
        }
    }
    openNarrativeDialog(listItem: any): void {
        if (listItem && listItem.jsondata && listItem.jsondata.General && listItem.jsondata.General.Narrative) {
            this.narrative = listItem.jsondata.General.Narrative;
        } else {
            this.narrative = 'No narrative info found!';
        }
        this.narrative = this.narrative.replace(/''/g, `'`);
        (<any>$('#narrative-dialog')).modal('show');
    }
    closeNarrativeDialog(): void {
        this.narrative = '';
        (<any>$('#narrative-dialog')).modal('hide');
    }
    validate(persons) {
        if (persons instanceof Array) {
          if (persons && persons.length) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
}
