import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeAssessmentRoutingModule } from './intake-assessment-routing.module';
import { IntakeAssessmentComponent } from './intake-assessment.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeAssessmentRoutingModule,
    FormMaterialModule
  ],
  declarations: [
    IntakeAssessmentComponent
  ]
})
export class IntakeAssessmentModule { }
