export enum AssessmentMode {
    ADD,
    EDIT,
    VIEW,
    PRINT
}
