import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeAssessmentComponent } from './intake-assessment.component';

const routes: Routes = [{
  path: '',
  component: IntakeAssessmentComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeAssessmentRoutingModule { }
