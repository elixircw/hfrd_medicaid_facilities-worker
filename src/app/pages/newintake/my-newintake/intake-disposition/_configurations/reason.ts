export class DispositionConfig {
    public static config = {
        'action': [
            {
                'value': 'No Action Taken',
                'text': 'No Action Taken'
            },
            {
                'value': 'No known resources',
                'text': 'No known resources'
            },
            {
                'value': 'Referral made to another agency',
                'text': 'Referral made to another agency'
            },
            {
                'value': 'Other',
                'text': 'Other'
            },
            {
                'value': 'No Resources Available',
                'text': 'No Resources Available'
            },
            {
                'value': 'Provided Caller with Additional Information Verbally',
                'text': 'Provided Caller with Additional Information Verbally'
            },
            {
                'value': 'Provided Caller with Additional Information in Writing',
                'text': 'Provided Caller with Additional Information in Writing'
            },
            {
                'value': 'Community Referral Offered',
                'text': 'Community Referral Offered'
            },
           
        ],
        'initialRecomendation': [
            {
                'value': 'releaseToParents',
                'text': 'Youth can be safely released to his Parents'
            },
            {
                'value': 'releaseToCaregiver',
                'text': 'Youth can be safely released to his Caregiver'
            },
            {
                'value': 'Detention',
                'text': 'Detention'
            }
        ]
    };
}
