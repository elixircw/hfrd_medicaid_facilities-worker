import value from '*.json';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject, Subscription } from 'rxjs/Rx';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService, ValidationService, AlertService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { DispositionCode, DispostionOutput, Sdm } from '../_entities/newintakeModel';
import { ComplaintTypeCase, DelayForm, DelayResponse, General, IntakeService } from '../_entities/newintakeSaveModel';
import { NewUrlConfig } from './../../newintake-url.config';
import { DispositionConfig } from './_configurations/reason';
import * as reason from './_configurations/reason.json';
import { IntakeStoreConstants, MyNewintakeConstants } from '../my-newintake.constants';
import { Router, ActivatedRoute } from '@angular/router';
import { AppConstants } from '../../../../@core/common/constants';
import * as jsPDF from 'jspdf';
import * as moment from 'moment';
import { forEach } from '@angular/router/src/utils/collection';
import { IntakeConfigService } from '../intake-config.service';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { ObjectUtils } from '../../../../@core/common/initializer';

declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-disposition',
    templateUrl: './intake-disposition.component.html',
    styleUrls: ['./intake-disposition.component.scss']
})
export class IntakeDispositionComponent implements OnInit, AfterViewInit, AfterViewChecked {
    general: General;
    scnRecommendOveride : any;
    caseDispositions: DispostionOutput[];
    statusDropdownItems$: Observable<DropdownModel[]>;
    dispositionList: DispositionCode[];
    dispositionDropDown: DropdownModel[];
    supDispositionDropDown: DropdownModel[];
    disposition: string;
    dispositionDropdownItems$: Observable<DropdownModel[]>;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    dispositionFormGroup: FormGroup;
    date = new Date();
    serviceTypeId: string;
    showReason = false;
    supervisorUser: boolean;
    timeReceived: string;
    showStatus = true;
    daTypeSubType: string;
    role: AppUser;
    agencyStatus: string;
    intakePurpose: any;
    isKinship = true;
    isSENflag = false;
    showBehalfOfRequester = false;
    actionDropdown: DropdownModel[];
    initRecomentdationDropdown: DropdownModel[];
    agencyTypeDropdown: Observable<DropdownModel[]>;
    isssta = false;
    isCW = false;
    isAS = false;
    sdm: Sdm;
    private store: any;
    selectedPurpose: any;
    disableCPSIntakeReport = false;
    dispositionStatus: any[];
    isCWIntakeWorker = false;
    isCWSupervisor = false;
    supervisourComments: string;
    isReopenCase: boolean;
    dispositioncode = '';
    isCWinfoNreff: boolean;
    addedPersons: any[] = [];
    personsDob: any[] = [];
    isEvpa = false;
    checkVPA: any[];
    isCaptureReason: boolean;
    intakeRecommond: string;
    dataStoreSubscription: Subscription;
    intakeRecomendation: string; // temp fix for monday demo
    emailForm: FormGroup;
    supStatus: any;
    selectedServiceDescriptions: string [];
    isSupervisorCommentRequired = false;
    isClosed = false;
    constructor(
        private _commonHttpService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _changeDetect: ChangeDetectorRef,
        private _dataStoreService: DataStoreService,
        private _router: Router,
        private route: ActivatedRoute,
        private _intakeService: IntakeConfigService,
        private _alertService: AlertService,
    ) {
        this.store = this._dataStoreService.getCurrentStore();
        this.sdm = this.store[IntakeStoreConstants.intakeSDM];
        this.addedPersons = this.store[IntakeStoreConstants.addedPersons];

        if (this.addedPersons && this.addedPersons.length > 0) {
            this.personsDob = this.addedPersons.filter(
                person => (this.calculateAge(person.Dob) >= 18 && this.calculateAge(person.Dob) <= 20.5));

                if (this.personsDob && this.personsDob.length > 0) {
                const countId = this._dataStoreService.getData('countyId');
                this.checkVPA = [];
                this.personsDob.forEach(person => {
                    this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            method: 'get',
                            where:  {
                                county : countId,
                                personid : person.Pid
                                },
                            limit: 10,
                            order: 'desc',
                            page: 1,
                            count: -1
                        }),
                        'Intakeservs/getvpadetails?filter'
                    )
                        .subscribe(
                            (result) => {
                                if (result && result.length && result[0].checkvpa) {
                                const checkVpaStatus = result[0].checkvpa.filter(item => item.status === true);
                                let vpaObj = { personid : person.Pid, status : false};
                                if (checkVpaStatus && checkVpaStatus.length > 0) {
                                    vpaObj = { personid : person.Pid, status : true};
                                }
                                this.checkVPA.push(vpaObj);
                                }
                            }
                        );
                });
            }
        }

        const voluntaryPlacementId = this._dataStoreService.getData('voluntryPlacementType');
        if (voluntaryPlacementId !== 'VPA') {
            this.isEvpa = false;
        } else {
            this.isEvpa = true;
        }

        this.dispositioncode = '';
        if (this.sdm) {
            if (this.sdm.scnRecommendOveride === '') {
                if (this.sdm.screeningRecommend === 'ScreenOUT') {
                    this.dispositioncode = 'ScreenOUT';
                } else if (this.sdm.screeningRecommend === 'Scrnin') {
                    this.dispositioncode = 'Scrnin';
                }
            } else if (this.sdm.scnRecommendOveride === 'OvrScrnout') {
                this.dispositioncode = 'OvrScrnout';
            } else if (this.sdm.scnRecommendOveride === 'Ovrscrnin') {
                this.dispositioncode = 'Ovrscrnin';
            } else {
                this.dispositioncode = '';
            }
        } else {
            const purpose = this._dataStoreService.getData(IntakeStoreConstants.purposeSelected);
            if (purpose && purpose.code === 'CHILD') {
                this.dispositioncode = 'ScreenOUT';
            }
        }
    }

    ngOnInit() {
        this.agencyTypeDropDownList();
        this.role = this._authService.getCurrentUser();
        if (this._authService.isCW()) {
            this.isCWIntakeWorker = (this.role.role.name === AppConstants.ROLES.INTAKE_WORKER);
            this.isCWSupervisor = (this.role.role.name === AppConstants.ROLES.SUPERVISOR);
        }
        if (this._authService.isAS()) {
            this.isAS = true;
        }
        if (this.role.user.userprofile.teammemberassignment.teammember.roletypekey === 'CWKN') {
            this.isKinship = false;
        }
        if (this.role.user.userprofile.teammemberassignment.teammember.roletypekey === 'CWKA') {
            this.isKinship = false;
        }
        if (this.role.role.name === 'apcs' || this.role.role.name === 'Kinship Supervisor') {
            this.supervisorUser = true;
            // this.dispositionFormGroup.get('intake');
        } else {
            this.supervisorUser = false;
        }
        this.selectedPurpose = this.store[IntakeStoreConstants.purposeSelected];
        console.log(' this.selectedPurpose', this.selectedPurpose);

        this.actionDropdown = DispositionConfig.config.action;
        this.initRecomentdationDropdown = DispositionConfig.config.initialRecomendation;
        // this.agencyTypeDropdown = DispositionConfig.config.agencyType;
        this.agencyStatus = this.store[IntakeStoreConstants.agency];
        this.dispositionForm();
        // this.serviceTypeId = this.store[IntakeStoreConstants.disposition];
        // to get the label for datype
        const checkInput = {
            nolimit: true,
            where: { teamtypekey: 'all' },
            method: 'get'
        };
        this._commonHttpService.getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter').subscribe(result => {
            this.intakePurpose = result;
        });
        // end of - to get the label for datype

        if (this._authService.getAgencyName() === 'CW') {
            this.isCWinfoNreff = (this.selectedPurpose && (this.selectedPurpose.code === 'Information and Referral')) ? true : false;
            this.setCaseDispositionForCW();
            // if (!this.supervisorUser) {
            if (this.caseDispositions && this.caseDispositions.length &&
                (!this.caseDispositions[0].intakeserreqstatustypekey || this.caseDispositions[0].intakeserreqstatustypekey === '')) {
                if (this.caseDispositions && this.caseDispositions.length &&
                    (!this.caseDispositions[0].intakeserreqstatustypekey ||
                    (this.selectedPurpose && (this.selectedPurpose.code === 'Request for services' && this._dataStoreService.getData(IntakeStoreConstants.clearhistory))))
                ) {
                // if (this.caseDispositions && this.caseDispositions.length && !this.caseDispositions[0].intakeserreqstatustypekey) {
                    this.dispositionFormGroup.patchValue({
                        intakeserreqstatustypekey: 'Review'
                    });
                    this.onChangeTaskStatus('Review');
                    this.changeDisp();
                }
                this.dispositionFormGroup.get('intakeserreqstatustypekey').disable();
            } else if(this.selectedPurpose && this.selectedPurpose.code !== 'ROACPS' && this.selectedPurpose.code !== MyNewintakeConstants.PURPOSE.INFORMATION_AND_REFERRAL) {

                this.dispositionFormGroup.patchValue({
                    intakeserreqstatustypekey: 'Review'
                });
                this.onChangeTaskStatus('Review');
                this.changeDisp();
                this.dispositionFormGroup.get('intakeserreqstatustypekey').enable();
            }
        } else {
            this.setCaseDispositionForAS();
        }

        if (this.selectedPurpose) {
            this.daTypeSubType = this.selectedPurpose.value;
            this.statusDropdownItems$ = Observable.empty();
            if (this.selectedPurpose.value === '619c4dcf-ef22-4fc4-9269-d7678e8a8f6a') {
                this.showBehalfOfRequester = true;
                this.disableCPSIntakeReport = false;
                const model = new DropdownModel();
                model.text = 'Recommended to Close I & R';
                model.value = 'Closed';
                this.statusDropdownItems$ = Observable.of([model]);
                this.dispositionFormGroup.patchValue({
                    intakeserreqstatustypekey: 'Closed'
                });
                this.onChangeTaskStatus('Closed');
                this._dataStoreService.setData(IntakeStoreConstants.closeintakecw, true);
                // this.dispositionFormGroup.get('intakeserreqstatustypekey').disable();
            } else if (this.selectedPurpose.code === 'ROACPS') {
                this.showBehalfOfRequester = false;
                this.disableCPSIntakeReport = false;
                let model = new DropdownModel();
                let intakeserreqstatustypekey;
                let closeintakecw;
                if(this.isCWSupervisor) {
                model.text = 'Recommended to close ROA CPS';
                model.value = 'Approved';
                intakeserreqstatustypekey = 'Approved';
                closeintakecw  = true;
                this.onChangeTaskStatus('Approved');
                } else  {
                    model.text = 'Request to close ROA CPS';
                    model.value = 'Review';
                    intakeserreqstatustypekey = 'Review';
                    closeintakecw  = false;
                    this.onChangeTaskStatus('Review');
                }
                this.statusDropdownItems$ = Observable.of([model]);
                this.dispositionFormGroup.patchValue({
                    intakeserreqstatustypekey: intakeserreqstatustypekey
                });

               
                this._dataStoreService.setData(IntakeStoreConstants.closeintakecw, closeintakecw);
                this.intakeRecomendation = 'Progress ROA';
                this.onChangeDispoType(this.intakeRecomendation, 0);
            } else if (this.selectedPurpose.code === 'Request for services' && this._dataStoreService.getData(IntakeStoreConstants.clearhistory)) {
                this.showBehalfOfRequester = false;
                this.disableCPSIntakeReport = false;
                const model = new DropdownModel();
                model.text = 'Recommended to close Request for Services';
                model.value = 'Closed';
                this.statusDropdownItems$ = Observable.of([model]);
                this.dispositionFormGroup.patchValue({
                    intakeserreqstatustypekey: 'Closed'
                });
                this.onChangeTaskStatus('Closed');
                this._dataStoreService.setData(IntakeStoreConstants.closeintakecw, true);
            } else if (this.selectedPurpose.code === 'CHILD') { // CPS
                const intakeStatus = this._dataStoreService.getData(
                    IntakeStoreConstants.reviewstatus);
                // if ((intakeStatus && intakeStatus.status) || (intakeStatus.status === ''))
                {
                    if (this.sdm) {
                        if(this.sdm.scnRecommendOveride==='OvrScrnout'){
                            this.sdm.scnRecommendOveride = 'OvrScrnout';
                            this.scnRecommendOveride='OvrScrnout'
                            this.sdm.cpsResponseType=null;
                            this.sdm.isir=false;
                            this.sdm.isar=false;
                        }
                        if(this.sdm.scnRecommendOveride === ''){
                            if (ObjectUtils.checkTrueProperty(this.sdm.screenOut)>=1) {
                                this.sdm.scnRecommendOveride = 'OvrScrnout';
                                this.scnRecommendOveride='OvrScrnout'
                                this.sdm.cpsResponseType=null;
                                this.sdm.isir=false;
                                this.sdm.isar=false;
                            } else if (ObjectUtils.checkTrueProperty(this.sdm.screenIn)>=1) {
                                this.sdm.scnRecommendOveride = 'Ovrscrnin';
                                this.scnRecommendOveride='Ovrscrnin'
                            }
                        } 
                     }
                    if (!this.sdm) {
                        this.onChangeDispoType('ScreenOUT', 0);
                        this.intakeRecomendation = 'ScreenOUT';
                    } else if (!this.sdm.screeningRecommend) {
                        this.onChangeDispoType('ScreenOUT', 0);
                        this.intakeRecomendation = 'ScreenOUT';
                    } else if (this.sdm.screeningRecommend === 'ScreenOUT' && this.sdm.scnRecommendOveride === '') {
                        this.onChangeDispoType('ScreenOUT', 0);
                        this.intakeRecomendation = 'ScreenOUT';
                    } else if (this.sdm.screeningRecommend === 'ScreenOUT' && this.sdm.scnRecommendOveride === 'Ovrscrnin') {
                        this.onChangeDispoType('Scrnin', 0);
                        this.intakeRecomendation = 'Scrnin';
                    } else if (this.sdm.screeningRecommend === 'Scrnin' && this.sdm.scnRecommendOveride === '') {
                        this.onChangeDispoType('Scrnin', 0);
                        this.intakeRecomendation = 'Scrnin';
                    } else if (this.sdm.screeningRecommend === 'Scrnin' && this.sdm.scnRecommendOveride === 'OvrScrnout') {
                        this.onChangeDispoType('ScreenOUT', 0);
                        this.intakeRecomendation = 'ScreenOUT';
                    }
                   
                }
                this.loadDropdown();
            } else if (this.selectedPurpose.value === 'd207bdd4-f281-4ec8-949c-8fd9657227f9') { // In-Home Service
                this._dataStoreService.setData(IntakeStoreConstants.closeintakecw, false);
                this.disableCPSIntakeReport = true;
                this.showBehalfOfRequester = false;
                this.dispositionFormGroup.get('intakeserreqstatustypekey').enable();
                this.onChangeDispoType('Scrnin', 0);
                this.intakeRecommond = 'Scrnin';
                this.loadDropdown();
            } else {
                this._dataStoreService.setData(IntakeStoreConstants.closeintakecw, false);
                this.disableCPSIntakeReport = false;
                this.showBehalfOfRequester = false;
                this.dispositionFormGroup.get('intakeserreqstatustypekey').enable();
                this.loadDropdown();
            }
        }

        // Reason For Delay
        this.timeReceived = this.store[IntakeStoreConstants.timeleft];
        if (this.timeReceived) {
            if (this._intakeService.isNonCPS()) {
                this.disableReasonfordelay();
            } else {
                this.enableReasonfordelay();
            }
        }

        if (this.role.user.userprofile.teamtypekey === 'CW') {
            this.isCW = true;
        } else {
            this.isCW = false;
        }
        // });
        // this.purposeCheckboxOutput$.subscribe(result => {
        //     result.forEach(item => {
        //         if (item.description === 'SSTA Request') {
        //             console.log('is ssta');
        //             this.isssta = true;
        //             this.loadDropdown();
        //         }
        //     });
        // });

        this.listenForVPEnabled();
        this.emailForm = this.formBuilder.group({
            email: ['', [ValidationService.mailFormat, Validators.required]]
        });
        if (this._intakeService.selectedPurposeIs(MyNewintakeConstants.PURPOSE.INFORMATION_AND_REFERRAL)
        || this._intakeService.selectedPurposeIs(MyNewintakeConstants.PURPOSE.REQUEST_FOR_SERVICES)) {
            const selectedServices = this.store[IntakeStoreConstants.intakeService];
            if (selectedServices && selectedServices.length) {
                this.selectedServiceDescriptions = selectedServices.map(item => item.description);
                console.log(this.selectedServiceDescriptions);
            }
        }
        const currentStatus = this._dataStoreService.getData(IntakeStoreConstants.INTAKE_STATUS);
        if (currentStatus === 'Closed' || currentStatus === 'Completed') {
            this.isClosed = true;
           } else {
            this.isClosed = false;
           }
    }

    listenForVPEnabled() {
        this._intakeService.isVoluntaryPlacement$.subscribe((data) => {
            if (data) {
                this.isEvpa = true;
            } else {
                this.isEvpa = false;
            }

            if (this.dispositionFormGroup.getRawValue().isYouthIndependentLive === 1) {
                this.dispositionFormGroup.controls['isYouthIndependentLive'].reset();
                this.dispositionFormGroup.controls['isYouthIndependentLive'].clearValidators();
                this.dispositionFormGroup.controls['isYouthIndependentLive'].updateValueAndValidity();
            }
        });
    }

    calculateAge(dob) {
        // const dob = this.selectedPerson.dob;
        let age = 0;
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            age = moment().diff(rCDob, 'years');
            const days = moment().diff(rCDob, 'days');
        }
        return age;
    }

    printCasePdf(element: string) {
        (<any>$('#intake-cps-doc1')).modal('hide');
        let printContents, popupWin;
        printContents = document.getElementById('CPS-Intake-Report').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>CPS Intake Report</title>
          <style>
          .section-header {
            height: 25px;
            border: 1px solid black;
            text-align: center;
            padding-bottom: 15px;
            background-color: #556080;
            font-weight: bold;
            margin-bottom: 5px;
        }
        .section-body table td{border: 1px solid #ccc; padding: 5px;}
        .section-lable {
            text-align: left;
            padding-right: 15px;
            font-weight: 600;
            margin-bottom: 5px;
        }.section-value {
            min-height: 22px;
            padding-left: 0px;
        }
        .sizeA4 {
                width: 21cm;
                height: 29.7cm;
            }.margin-wrapper {
                margin-left: 1.2cm;
                margin-right: 1.2cm;
            }
            .pdf-page .row {
                margin-left: 0px;
                margin-right: 0px;
            }
            .align-center {
                text-align: center
            }
            </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }

    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then(canvas => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
            });
        }
        this.convertImageToPdf();
    }
    convertImageToPdf() {
        let doc = null;
        this.pdfFiles.forEach(pdfFile => {
          doc = new jsPDF();
            const width = doc.internal['pageSize'].getWidth() - 10;
            const heigth = doc.internal['pageSize'].getHeight() - 10;
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 3, 5, width, heigth);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#intake-complaint-pdf1')).modal('hide');
        this.pdfFiles = [];
    }

    private agencyTypeDropDownList() {
        const source = forkJoin([
          this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { 'active_sw': 'Y', 'picklist_type_id': '14', 'delete_sw': 'N' }
            },
            'tb_picklist_values/getpicklist' + '?filter'
          ),

        ]).map((result) => {
          return {
            agencyTypeList: result[0].map(
              (res) =>
                new DropdownModel({
                  text: res.description_tx,
                  value: res.value_tx
                })
            )
          };
        })
        .share();
        this.agencyTypeDropdown = source.pluck('agencyTypeList');
      }

    setCaseDispositionForAS(): any {
        let dispositions = null;
        dispositions = this.store[IntakeStoreConstants.disposition] ? this.store[IntakeStoreConstants.disposition] : [];
        const complaints = this.store[IntakeStoreConstants.createdCases];
        const intakeNumber = this.store[IntakeStoreConstants.intakenumber];
        const purposeSelected = this.store[IntakeStoreConstants.purposeSelected];
        const purposeId = purposeSelected ? purposeSelected.value : '';
        let dispositioncode = '';
        if (this.dispositioncode && this.dispositioncode !== '') {
            dispositioncode = this.dispositioncode;
        }
        if (complaints) {
            this.caseDispositions = [];
            complaints.forEach((element: ComplaintTypeCase) => {
                const acase: DispostionOutput = dispositions.find(item => item.ServiceRequestNumber === element.caseID);
                const createdCase1 = {
                    ServiceRequestNumber: element.caseID ? element.caseID : intakeNumber,
                    DaTypeKey: acase && acase.serviceTypeID ? acase.serviceTypeID : purposeId,
                    subSeriviceTypeValue: element && element.subSeriviceTypeValue ? element.subSeriviceTypeValue : '',
                    DasubtypeKey: element.subServiceTypeID,
                    DAStatus: '',
                    DADisposition: '',
                    Summary: '',
                    dispositioncode: dispositioncode ? dispositioncode : (acase && acase.dispositioncode ? acase.dispositioncode : ''),
                    intakeserreqstatustypekey: acase && acase.intakeserreqstatustypekey ? acase.intakeserreqstatustypekey : '',
                    comments: acase && acase.comments ? acase.comments : '',
                    ReasonforDelay: '',
                    supStatus: acase && acase.supStatus ? acase.supStatus : '',
                    supDisposition: acase && acase.supDisposition ? acase.supDisposition : '',
                    supComments: acase && acase.supComments ? acase.supComments : '',
                    intakeMultipleDispositionDropdown: [],
                    supMultipleDispositionDropdown: [],
                    GroupNumber: acase && acase.GroupNumber ? acase.GroupNumber : null,
                    GroupReasonType: acase && acase.GroupReasonType ? acase.GroupReasonType : null,
                    GroupComment: acase && acase.GroupComment ? acase.GroupComment : null,
                    isYouthIndependentLive: '',
                    captureReason: ''
                };
                if (acase && acase.supComments) {
                    this.supervisourComments = acase.supComments ? acase.supComments : '';
                    this.isReopenCase = acase.supStatus === 'Reopen' ? true : false;
                }
                this.caseDispositions.push(createdCase1);
                // this.createdCase.dispositioncode: '',
                // this.createdCase.intakeserreqstatustypekey: '',
                if (this.caseDispositions.length > 0) {
                    if (this.caseDispositions[0].intakeserreqstatustypekey) {
                        this.onChangeTaskStatus(this.caseDispositions[0].intakeserreqstatustypekey);
                    }
                    if (this.caseDispositions[0].supStatus) {
                        this.supOnChangeTaskStatus(this.caseDispositions[0].supStatus);
                        // this.changeDisp();
                    }
                    this.dispositionFormGroup.patchValue({
                        intakeserreqstatustypekey: this.caseDispositions[0].intakeserreqstatustypekey ? this.caseDispositions[0].intakeserreqstatustypekey : '',
                        dispositioncode: this.caseDispositions[0].dispositioncode ? this.caseDispositions[0].dispositioncode : '',
                        supStatus: this.caseDispositions[0].supStatus ? this.caseDispositions[0].supStatus : '',
                        supDisposition: this.caseDispositions[0].supDisposition ? this.caseDispositions[0].supDisposition : '',
                        comments: this.caseDispositions[0].comments ? this.caseDispositions[0].comments : '',
                        supComments: this.caseDispositions[0].supComments ? this.caseDispositions[0].supComments : '',
                        isYouthIndependentLive: this.caseDispositions[0].isYouthIndependentLive,
                        captureReason: this.caseDispositions[0].captureReason ? this.caseDispositions[0].captureReason : ''
                    });
                    this.changeDisp();
                }
            });
        }
    }

    setCaseDispositionForCW(): any {
        let dispositions = null;
        dispositions = this.store[IntakeStoreConstants.disposition];
        if (dispositions && Array.isArray(dispositions) && dispositions.length) {
            this.intakeRecomendation = dispositions[0].DADisposition?dispositions[0].DADisposition:dispositions[0].dispositioncode;
        }

        const complaints = this.store[IntakeStoreConstants.createdCases];
        const intakeNumber = this.store[IntakeStoreConstants.intakenumber];
        const purposeSelected = this.store[IntakeStoreConstants.purposeSelected];
        const purposeId = purposeSelected ? purposeSelected.value : '';
        let dispositioncode = '';
        if (this.dispositioncode && this.dispositioncode !== '') {
            dispositioncode = this.dispositioncode;
        }
        if (dispositions) {
            this.caseDispositions = [];
            dispositions.map(item => {
                const createdCase1 = {
                    ServiceRequestNumber: item.caseID ? item.caseID : intakeNumber,
                    DaTypeKey: item.serviceTypeID ? item.serviceTypeID : purposeId,
                    subSeriviceTypeValue: item.subSeriviceTypeValue,
                    DasubtypeKey: item.subServiceTypeID,
                    DAStatus: '',
                    DADisposition: '',
                    Summary: '',
                    dispositioncode: dispositioncode ? dispositioncode : (item.dispositioncode ? item.dispositioncode : ''),
                    intakeserreqstatustypekey: item.intakeserreqstatustypekey ? item.intakeserreqstatustypekey : '',
                    comments: item.comments ? item.comments : '',
                    ReasonforDelay: '',
                    supStatus: item.supStatus ? item.supStatus : '',
                    supDisposition: item.supDisposition ? item.supDisposition : '',
                    supComments: item.supComments ? item.supComments : '',
                    intakeMultipleDispositionDropdown: [],
                    supMultipleDispositionDropdown: [],
                    GroupNumber: item.GroupNumber ? item.GroupNumber : null,
                    GroupReasonType: item.GroupReasonType ? item.GroupReasonType : null,
                    GroupComment: item.GroupComment ? item.GroupComment : null,
                    isYouthIndependentLive: item.isYouthIndependentLive,
                    captureReason: item.captureReason ? item.captureReason : '',
                    reason: item.reason ? item.reason : '',
                    agencyContact: item.agencyContact ? item.agencyContact : '',
                    agencyName: item.agencyName ? item.agencyName : '',
                    agencyType: item.agencyType ? item.agencyType : '',
                    intakeAction: item.intakeAction ? item.intakeAction : '',
                };

                this.isCaptureReason = (item.isYouthIndependentLive === 0) ? true : false;
                this.caseDispositions.push(createdCase1);
                // this.createdCase.dispositioncode: '',
                // this.createdCase.intakeserreqstatustypekey: '',
                if (this.caseDispositions.length > 0) {
                    if (this.caseDispositions[0].intakeserreqstatustypekey) {
                        this.onChangeTaskStatus(this.caseDispositions[0].intakeserreqstatustypekey);
                    }
                    if (this.caseDispositions[0].supStatus) {
                        this.supOnChangeTaskStatus(this.caseDispositions[0].supStatus);
                        // this.changeDisp();
                    }
                    this.dispositionFormGroup.patchValue({
                        intakeserreqstatustypekey: this.caseDispositions[0].intakeserreqstatustypekey ? this.caseDispositions[0].intakeserreqstatustypekey : '',
                        dispositioncode: this.caseDispositions[0].dispositioncode ? this.caseDispositions[0].dispositioncode : '',
                        supStatus: this.caseDispositions[0].supStatus ? this.caseDispositions[0].supStatus : '',
                        supDisposition: this.caseDispositions[0].supDisposition ? this.caseDispositions[0].supDisposition : '',
                        comments: this.caseDispositions[0].comments ? this.caseDispositions[0].comments : '',
                        supComments: this.caseDispositions[0].supComments ? this.caseDispositions[0].supComments : '',
                        isYouthIndependentLive: this.caseDispositions[0].isYouthIndependentLive,
                        captureReason: this.caseDispositions[0].captureReason ? this.caseDispositions[0].captureReason : '',
                        reason: this.caseDispositions[0].reason ? this.caseDispositions[0].reason : '',
                        agencyContact: this.caseDispositions[0].agencyContact ? this.caseDispositions[0].agencyContact : '',
                        agencyName: this.caseDispositions[0].agencyName ? this.caseDispositions[0].agencyName : '',
                        agencyType: this.caseDispositions[0].agencyType ? this.caseDispositions[0].agencyType : '',
                        intakeAction: this.caseDispositions[0].intakeAction ? this.caseDispositions[0].intakeAction : '',
                    });
                    this.changeDisp();
                }
                this.supStatus = item.supStatus ? item.supStatus : null;
            });
        } else if (purposeId) {
            this.caseDispositions = [];
            const complaintTypeCase: DispostionOutput = new DispostionOutput();
            complaintTypeCase.ServiceRequestNumber = intakeNumber;
            complaintTypeCase.DaTypeKey = purposeId;
            if (dispositioncode) {
                complaintTypeCase.dispositioncode = dispositioncode;
            }
            this.caseDispositions.push(complaintTypeCase);
        }
    }

    sendEmail () {
        const caseID = this.store[IntakeStoreConstants.intakenumber];
        if (this.emailForm.valid) {
        const request = {
            email : this.emailForm.getRawValue().email,
            caseNumber: caseID,
            body: document.getElementById('CPS-Intake-Report').innerHTML
        };
        this._commonHttpService.create(request, 'Intakeservicerequestpurposes/sendemailcontact').subscribe(
            (result) => {
                this._alertService.success('Email Sent successfully!');
            },
            (error) => {
               console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    } else {
        this._alertService.error('Please enter valid email address!');
        }
    }

    ngAfterViewInit() {
        this.general = this.store[IntakeStoreConstants.general];
        this.checkSEN();
    }
    ngAfterViewChecked() {
        this._changeDetect.detectChanges();
    }
    dispositionForm() {
        this.dispositionFormGroup = this.formBuilder.group({
            intakeserreqstatustypekey: ['', Validators.required],
            dispositioncode: ['', Validators.required],
            statusdate: [this.date],
            duedate: [this.date],
            completiondate: [this.date],
            dateseen: null,
            financial: [''],
            seenwithin: [this.date],
            edl: [''],
            jointinvestigation: [''],
            investigationsummary: [''],
            visitinfo: [''],
            supStatus: [''],
            supDisposition: [''],
            supComments: [''],
            comments: [''],
            reason: [''],
            isDelayed: [false],
            intakeAction: [''],
            agencyName: [''],
            agencyContact: [''],
            agencyType: [''],
            isYouthIndependentLive: [null],
            captureReason: ['']
        });
    }

    loadDropdown() {
        this.statusDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        intakeservreqtypeid: this.selectedPurpose.value === 'd207bdd4-f281-4ec8-949c-8fd9657227f9' ? '247a8b26-cdee-4ce8-b36e-b37e49fd0103' : this.daTypeSubType,
                        servicerequestsubtypeid: this.selectedPurpose.value === 'd207bdd4-f281-4ec8-949c-8fd9657227f9' ? '247a8b26-cdee-4ce8-b36e-b37e49fd0103' : this.daTypeSubType
                    },
                    method: 'get'
                }),
                'Intakeservicerequestdispositioncodes/getstatuslist?filter'
            )
    //     this.agencyTypeDropdown = this._commonHttpService.getArrayList(
    //             {
    //               method: 'get',
    //               nolimit: true,
    //               where: { 'active_sw': 'Y', 'picklist_type_id': '14', 'delete_sw': 'N' }
    //             },
    //             'tb_picklist_values/getpicklist' + '?filter'
    //           )
            .map(result => {

                if (result && result.length) {
                    this.dispositionList = result;
                    return result.map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeserreqstatustypekey
                            })
                    ).filter(statusItem => {
                        if (this.isCWSupervisor) {
                            if (statusItem.text === 'Accepted' || statusItem.text === 'Return to Worker' ) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return true;
                        }
                    });
                } else {
                    this.dispositionList = [];
                }
            });

        if (this._authService.isCW()) {
            this.dispositionStatus = [];
            this.statusDropdownItems$.subscribe(result => {
                if (result && result.length > 0) {
                    // if (this.isCWIntakeWorker) {
                        for (let i = 0; i < result.length; i++) {
                            // if (result[i].value === 'Review') {
                            //     this.dispositionStatus.push(result[i]);
                            // }
                            this.dispositionStatus.push(result[i]);
                        }
                    // }
                    // if (this.isCWSupervisor) {
                    if (this.caseDispositions[0].intakeserreqstatustypekey !== '' && this.isCWSupervisor) {
                        for (let i = 0; i < result.length; i++) {
                            if (['Reopen', 'Approved', 'Rejected'].includes(result[i].value)) {
                                if (!this.dispositionStatus.some(item => item.value === result[i].value)) { this.dispositionStatus.push(result[i]); }
                            }
                        }
                    }
                    /* D-11889 - CJAMS - CW -  Decision Tab
                    */
                    if (this.isCWSupervisor) {
                        const acceptedItem = result.find(item => item.text === 'Accepted');
                        if (acceptedItem) {
                            this.dispositionFormGroup.patchValue({ supStatus : acceptedItem.value});
                            this.supOnChangeTaskStatus(acceptedItem.value);
                        }
                    }
                }
            });

            this.statusDropdownItems$ = Observable.of(this.dispositionStatus);
          
        }

        if (this._authService.isAS() && this.role.role.name === 'Intake Worker') {
            this.dispositionStatus = [];
            this.statusDropdownItems$.subscribe(result => {
                if (result && result.length > 0) {
                    if (!this.isCWIntakeWorker) {
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].value === 'Review' ) {
                                this.dispositionStatus.push(result[i]);
                            }
                        }
                    }
                }
            });
            this.statusDropdownItems$ = Observable.of(this.dispositionStatus);
        }
    }

    onChangeTaskStatus(statusId: any) {
        if (this.caseDispositions && this.caseDispositions.length > 0) {
            this.caseDispositions.map(item => {
                item.DAStatus = statusId;
                item.intakeMultipleDispositionDropdown = [];
                if (item.issubtypekey) {
                    item.DasubtypeKey = null;
                }
                if (this.selectedPurpose.code === 'ROACPS') {
                    item.intakeMultipleDispositionDropdown = [
                        {
                            text: 'Progress ROA',
                            value: 'Progress ROA'
                        }
                    ];
                } else if (item.DaTypeKey === 'd207bdd4-f281-4ec8-949c-8fd9657227f9') {
                    this._commonHttpService
                        .getArrayList(
                            new PaginationRequest({
                                nolimit: true,
                                where: {
                                    intakeservreqtypeid: '247a8b26-cdee-4ce8-b36e-b37e49fd0103',
                                    servicerequestsubtypeid: '247a8b26-cdee-4ce8-b36e-b37e49fd0103',
                                    statuskey: 'Review'
                                },
                                method: 'get'
                            }),
                            'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                        )
                        .subscribe(result => {
                            if (result && result.length) {
                                item.intakeMultipleDispositionDropdown = result.map(
                                    res =>
                                        new DropdownModel({
                                            text: res.description,
                                            value: res.dispositioncode
                                        })
                                );
                            } else {
                                item.intakeMultipleDispositionDropdown = [];
                            }
                        });
                } else {
                    this._commonHttpService
                        .getArrayList(
                            new PaginationRequest({
                                nolimit: true,
                                where: {
                                    intakeservreqtypeid: item.DaTypeKey,
                                    servicerequestsubtypeid: item.DasubtypeKey ? item.DasubtypeKey : item.DaTypeKey,
                                    statuskey: (this.selectedPurpose.code === 'Request for services' && this._dataStoreService.getData(IntakeStoreConstants.clearhistory)) ? 'Closed' : statusId
                                },
                                method: 'get'
                            }),
                            'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                        )
                        .subscribe(result => {
                            if (result && result.length) {
                                item.intakeMultipleDispositionDropdown = result.map(
                                    res =>
                                        new DropdownModel({
                                            text: res.description,
                                            value: res.dispositioncode
                                        })
                                ).filter(iwdisposition => {
                                    if (!this.isCW) {
                                        return true;
                                    }
                                    if (this.isCW && this._intakeService.selectedPurposeIs('Information and Referral')) {
                                        return true;
                                    }
                                    if (iwdisposition.value === 'Scrnin' || iwdisposition.value === 'ScreenOUT') {
                                        return true;
                                    } else {
                                        return false;
                                    }

                                });
                            } else {
                                item.intakeMultipleDispositionDropdown = [];
                            }
                        });
                }
            });
        }
    }
    supOnChangeTaskStatus(id) {
        this.changeDisp();
        this.caseDispositions.map(item => {
            item.DAStatus = id;
            item.supMultipleDispositionDropdown = [];
            if (this.selectedPurpose.code === 'ROACPS') {
                item.supMultipleDispositionDropdown = [
                    {
                        text: 'Progress ROA',
                        value: 'screenout'
                    }
                ];
            } else  if (item.DaTypeKey === 'd207bdd4-f281-4ec8-949c-8fd9657227f9') {
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            nolimit: true,
                            where: {
                                intakeservreqtypeid: '247a8b26-cdee-4ce8-b36e-b37e49fd0103',
                                servicerequestsubtypeid: '247a8b26-cdee-4ce8-b36e-b37e49fd0103',
                                statuskey: 'Review'
                            },
                            method: 'get'
                        }),
                        'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                    )
                    .subscribe(result => {
                        if (result && result.length) {
                            item.supMultipleDispositionDropdown = result.map(
                                res =>
                                    new DropdownModel({
                                        text: res.description,
                                        value: res.dispositioncode
                                    })
                            ).filter(supervisorDisposition => {
                                console.log('supervisorDisposition', supervisorDisposition);
                                if (!this.isCW) {
                                    return true;
                                }
                                if (this.isCW && this._intakeService.selectedPurposeIs('Information and Referral')) {
                                    return true;
                                }
                                if (supervisorDisposition.value === 'Scrnin'
                                    || supervisorDisposition.value === 'ScreenOUT'
                                    || supervisorDisposition.value === 'OvrScrnout'
                                    || supervisorDisposition.value === 'Ovrscrnin'
                                ) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        } else {
                            item.supMultipleDispositionDropdown = [];
                        }
                    });
            } else {
                this._commonHttpService
                .getArrayList(
                    new PaginationRequest({
                        nolimit: true,
                        where: {
                            intakeservreqtypeid: item.DaTypeKey,
                            servicerequestsubtypeid: item.DasubtypeKey,
                            statuskey: id
                        },
                        method: 'get'
                    }),
                    'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
                )
                .subscribe(result => {
                    if (result && result.length) {
                        item.supMultipleDispositionDropdown = result.map(
                            res =>
                                new DropdownModel({
                                    text: res.description,
                                    value: res.dispositioncode
                                })
                        ).filter(supervisorDisposition => {
                            console.log('supervisorDisposition', supervisorDisposition);
                            if (!this.isCW) {
                                return true;
                            }
                            if (this.isCW && this._intakeService.selectedPurposeIs('Information and Referral')) {
                                return true;
                            }
                            if (supervisorDisposition.value === 'Scrnin'
                                || supervisorDisposition.value === 'ScreenOUT'
                                || supervisorDisposition.value === 'OvrScrnout'
                                || supervisorDisposition.value === 'Ovrscrnin'
                                || supervisorDisposition.value === 'Dontmetreq'
                            ) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    } else {
                        item.supMultipleDispositionDropdown = [];
                    }
                });
            }
        });
    }
    onChangeDispoType(daSubtype, index) {
        console.log(daSubtype);
        if (this.sdm) {
            if (daSubtype === 'Ovrscrnin') {
                this.sdm.scnRecommendOveride = 'Ovrscrnin';
                // this.sdm.screeningRecommend = 'Scrnin';
                this.sdm.isfinalscreenin = true;
                this._dataStoreService.setData(IntakeStoreConstants.intakeSDM, this.sdm);

            } else if (daSubtype === 'OvrScrnout') {
                this.sdm.scnRecommendOveride = 'OvrScrnout';
                // this.sdm.screeningRecommend = 'ScreenOUT';
                this.sdm.isfinalscreenin = false;
                this._dataStoreService.setData(IntakeStoreConstants.intakeSDM, this.sdm);

            } else if (daSubtype === 'Scrnin') {
                this.sdm.scnRecommendOveride = '';
                // this.sdm.screeningRecommend = 'Scrnin';
                this.sdm.isfinalscreenin = true;
                this._dataStoreService.setData(IntakeStoreConstants.intakeSDM, this.sdm);
            } else if (daSubtype === 'ScreenOUT') {
                this.sdm.scnRecommendOveride = '';
                // this.sdm.screeningRecommend = 'ScreenOUT';
                this.sdm.isfinalscreenin = false;
                this._dataStoreService.setData(IntakeStoreConstants.intakeSDM, this.sdm);
            }
            this.dispositioncode = daSubtype;
        }
        this.caseDispositions[index].DADisposition = daSubtype;
        this.caseDispositions[index].dispositioncode = daSubtype;
        this.changeDisp();
    }
    onChangeSupDispoType(daSubtype, index) {
        this.caseDispositions[index].DADisposition = daSubtype;
        this.caseDispositions[index].supDisposition = daSubtype;
        /* D-11889 - CJAMS - CW -  Decision Tab
         */
        if (this.caseDispositions[index].dispositioncode !== daSubtype) {
            this.isSupervisorCommentRequired = true;
        } else {
            this.isSupervisorCommentRequired = false;
        }
        this.changeDisp();
    }

    changeDisp() {
        if (this.caseDispositions && this.caseDispositions.length > 0) {
            const dispositionFormValues = this.dispositionFormGroup.getRawValue();
            this.caseDispositions.map(item => {
                item.intakeserreqstatustypekey = dispositionFormValues.intakeserreqstatustypekey ? dispositionFormValues.intakeserreqstatustypekey : '';
                item.comments = this.dispositionFormGroup.value.comments ? this.dispositionFormGroup.value.comments : item.comments;
                item.intakeAction = this.dispositionFormGroup.value.intakeAction ? this.dispositionFormGroup.value.intakeAction : '';
                item.supStatus = this.dispositionFormGroup.value.supStatus ? this.dispositionFormGroup.value.supStatus : item.supStatus;
                item.supComments = this.dispositionFormGroup.value.supComments ? this.dispositionFormGroup.value.supComments : item.supComments;
                item.reason = this.dispositionFormGroup.value.reason ? this.dispositionFormGroup.value.reason : item.reason;
                item.caseID = this.dispositionFormGroup.value.ServiceRequestNumber ? this.dispositionFormGroup.value.ServiceRequestNumber : '';
                item.serviceTypeID = this.dispositionFormGroup.value.DaTypeKey ? this.dispositionFormGroup.value.DaTypeKey : '';
                item.isYouthIndependentLive = (dispositionFormValues.isYouthIndependentLive !== null) ? dispositionFormValues.isYouthIndependentLive : item.isYouthIndependentLive;
                item.captureReason = this.dispositionFormGroup.value.captureReason ? this.dispositionFormGroup.value.captureReason : item.captureReason;
                item.agencyContact = dispositionFormValues.agencyContact ? dispositionFormValues.agencyContact : item.agencyContact;
                item.agencyName = dispositionFormValues.agencyName ? dispositionFormValues.agencyName : item.agencyName;
                item.agencyType = dispositionFormValues.agencyType ? dispositionFormValues.agencyType : item.agencyType;
            });
            console.log(this.caseDispositions);
            if (this.sdm && ObjectUtils.checkTrueProperty(this.sdm.screenOut) >= 1) {
                this.intakeRecomendation = 'ScreenOUT';
            }
            this._dataStoreService.setData(IntakeStoreConstants.disposition, this.caseDispositions);
        }
    }
    enableReasonfordelay() {
        if (this.timeReceived === 'Overdue') {
            this.showReason = true;
            this.dispositionFormGroup.patchValue({ isDelayed: true });
            this.dispositionFormGroup.get('reason').setValidators([Validators.required]);
            this.dispositionFormGroup.get('reason').updateValueAndValidity();
        } else {
            this.disableReasonfordelay();
        }
    }

    disableReasonfordelay() {
        this.showReason = false;
        this.dispositionFormGroup.patchValue({ isDelayed: false });
        this.dispositionFormGroup.get('reason').clearValidators();
        this.dispositionFormGroup.get('reason').updateValueAndValidity();
    }

    previewCpsDoc() {
        // const url = 'doc/cps-doc';
        // this._router.navigate([url], { relativeTo: this.route });
        Observable.timer(500).subscribe(() => {
            (<any>$('#intake-cps-doc1')).modal('show');
        });
        // (<any>$('#intake-cps-doc1')).modal('show');
    }

    isYouthIndependentLiving(value) {
       this.isCaptureReason =  (value) ? false : true;
       this.dispositionFormGroup.get('captureReason').reset();
       if (!value) {
            this.intakeRecommond = 'ScreenOUT';
       } else {
            this.intakeRecommond = 'Scrnin';
            this.isEvpa = false;
       }
       this.onChangeDispoType(this.intakeRecommond, 0);

       this._intakeService.changeHomeServiceValue(value);

    }

    checkSEN() {
        this.isSENflag = false;
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        const intakesdmcheck = this.store[IntakeStoreConstants.intakeSDM];
        if (intakesdmcheck && intakesdmcheck.cpsResponseType &&
            (intakesdmcheck.cpsResponseType === 'CPS-AR' ||
                intakesdmcheck.cpsResponseType === 'CPS-IR')) {       // @TM: CPS IR & CPS AR over-ride SEN cases
            this.isSENflag = false;
        } else if (intakesdmcheck && intakesdmcheck.riskofHarm &&
            ObjectUtils.checkTrueProperty(intakesdmcheck.riskofHarm) >= 1) {
            this.isSENflag = true;
        } else if (addedPersons) {
            addedPersons.map(item => {
                // @TM: Set ROH (Risk of Harm) flag for SEN (Substance Exposed New-born) case
                if (item.drugexposednewbornflag === 1) {
                    this.isSENflag = true;
                }
            });
        }
        // if(this.sdm.riskofHarm.isnegrh_exposednewborn) {
        //     this.isSENflag = true;
        // }
        if (this.isSENflag) {
            this.onChangeDispoType('Scrnin', 0);
            this.intakeRecomendation = 'Scrnin';
        }
    }

}
