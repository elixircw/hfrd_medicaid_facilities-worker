import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeDispositionComponent } from './intake-disposition.component';

const routes: Routes = [{
  path: '',
  component: IntakeDispositionComponent,
  children: [
    {
        path: 'doc',
        loadChildren: '../intake-document-creator/intake-document-creator.module#IntakeDocumentCreatorModule',
    }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeDispositionRoutingModule { }
