import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MyNewintakeConstants, IntakeStoreConstants } from '../../my-newintake.constants';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { Employer, Work, EmployActivity, InvolvedPerson } from '../../_entities/newintakeModel';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { INTAKE_GUARDIANSHIP } from '../guardianship/_entities/intake-guardianship-const';
import * as moment from 'moment';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'work-profile',
  templateUrl: './work-profile.component.html',
  styleUrls: ['./work-profile.component.scss']
})
export class WorkProfileComponent implements OnInit {
  @Input()
  workFormReset$ = new Subject<boolean>();
  employerForm: FormGroup;
  careerForm: FormGroup;
  employer: Employer[] = [];
  careergoals: string;
  work: Work = {};
  minDate = new Date();
  maxDate = new Date();
  modalInt: number;
  modalActivityInt: number;
  editMode: boolean;
  reportMode: string;
  constants = MyNewintakeConstants.Intake.PersonsInvolved.Work;
  enableForm: boolean;
  monthlyActivityForm: FormGroup;
  addEditLabel: string;
  activity: EmployActivity[] = [];
  isAddActivity: boolean;
  selectedPerson: any[];
  selectedPersonAge: number;
  minActivityDate: Date;
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.selectedPersonAge = 0;
    this._dataStoreService.currentStore.subscribe(store => {
      const personId = store[INTAKE_GUARDIANSHIP.PersonId];
      if (personId) {
        const personData = store[IntakeStoreConstants.addedPersons];
        const selectedPerson = personData.filter(person => (person.Pid === personId));
        this.selectedPersonAge = (selectedPerson.length > 0) ? this.calculateAge(selectedPerson[0].Dob) : 0;
      }
    });

    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.employerForm = this.formbulider.group({
      employername: '',
      currentemployer: false,
      noofhours: new FormControl('', [Validators.pattern('^[0-9]*$')]),
      duties: '',
      startdate: '',
      enddate: '',
      reasonforleaving: '',
    });
    this.careerForm = this.formbulider.group({
      careergoals: ''
    });

    this.initMonthActivityForm();

    this.workFormReset$.subscribe((res) => {
      if (res === true) {
        this.work = this._dataStoreService.getData(this.constants.Work);
        if (this.work && this.work.employer) {
          this.employer = this.work.employer;
        } else {
          this.employer = [];
        }

        this.careergoals = this.work.careergoals;
        this.careerForm = this.formbulider.group({
          careergoals: ''
        });
      }
    });
    this.work = this._dataStoreService.getData(this.constants.Work);

    this.careerForm.valueChanges.subscribe((res) => {
      this.work = this._dataStoreService.getData(this.constants.Work);
      this.work.careergoals = this.careerForm.getRawValue().careergoals;
      this._dataStoreService.setData(this.constants.Work, this.work);
    });

    if (this.work && this.work.employer) {
      this.employer = this.work.employer;
    }
    if (this.work && this.work.careergoals) {
      this.careerForm.patchValue({ 'careergoals': this.careergoals });
    }
  }

  private updateData() {
    this.work = this._dataStoreService.getData(this.constants.Work);
    this.work.employer = this.employer;
    // this._dataStoreService.setData(this.constants.Work, this.work);
    this._dataStoreService.setData(this.constants.Work, this.work);
  }

  private add() {
    this.employer.push(this.employerForm.getRawValue());
    this.updateData();
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {

    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.employerForm.reset();
    this.employerForm.enable();
    this.employerForm.get('enddate').enable();
    this.employerForm.get('enddate').setValidators([Validators.required]);
    this.employerForm.get('enddate').updateValueAndValidity();
    // this.employerForm.get('reasonforleaving').patchValue({});
    this.employerForm.get('reasonforleaving').enable();
    this.employerForm.get('reasonforleaving').setValidators([Validators.required]);
    this.employerForm.get('reasonforleaving').updateValueAndValidity();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.employer[this.modalInt] = this.employerForm.getRawValue();
    }
    this.updateData();
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.editMode = false;
    this.patchForm(modal);
    this.employerForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.employerForm.enable();
    this.isCurrentEmployerSaved(modal.currentemployer);
  }

  private delete(index) {
    this.employer.splice(index, 1);
    this.updateData();
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }



  private cancel() {
    this.resetForm();
  }

  isCurrentEmployer(control) {
    if (control.checked) {

      this.employerForm.get('enddate').disable();
      this.employerForm.get('enddate').clearValidators();
      this.employerForm.get('enddate').updateValueAndValidity();
      // this.employerForm.get('reasonforleaving').patchValue({});
      this.employerForm.get('reasonforleaving').disable();
      this.employerForm.get('reasonforleaving').clearValidators();
      this.employerForm.get('reasonforleaving').updateValueAndValidity();
    } else {

      this.employerForm.get('enddate').enable();
      this.employerForm.get('enddate').setValidators([Validators.required]);
      this.employerForm.get('enddate').updateValueAndValidity();
      // this.employerForm.get('reasonforleaving').patchValue({});
      this.employerForm.get('reasonforleaving').enable();
      this.employerForm.get('reasonforleaving').setValidators([Validators.required]);
      this.employerForm.get('reasonforleaving').updateValueAndValidity();
    }
  }

  isCurrentEmployerSaved(control) {
    if (control) {

      this.employerForm.get('enddate').disable();
      this.employerForm.get('enddate').clearValidators();
      this.employerForm.get('enddate').updateValueAndValidity();
      // this.employerForm.get('reasonforleaving').patchValue({});
      this.employerForm.get('reasonforleaving').disable();
      this.employerForm.get('reasonforleaving').clearValidators();
      this.employerForm.get('reasonforleaving').updateValueAndValidity();
    } else {

      this.employerForm.get('enddate').enable();
      this.employerForm.get('enddate').setValidators([Validators.required]);
      this.employerForm.get('enddate').updateValueAndValidity();
      // this.employerForm.get('reasonforleaving').patchValue({});
      this.employerForm.get('reasonforleaving').enable();
      this.employerForm.get('reasonforleaving').setValidators([Validators.required]);
      this.employerForm.get('reasonforleaving').updateValueAndValidity();
    }
  }

  dateChanged() {
    const empForm = this.employerForm.getRawValue();
    this.minDate = new Date(empForm.startdate);
  }

  private patchForm(modal: Employer) {
    this.employerForm.patchValue(modal);
  }

  initMonthActivityForm() {
    this.monthlyActivityForm = this.formbulider.group({
      program_name: ['', [Validators.required]],
      start_date: ['', [Validators.required]],
      end_date: [null],
      narrative: ['', [Validators.required]],
    });
  }

  isParticipateProgram(val) {
    if (val) {
      this.enableForm = true;
    } else {
      this.enableForm = false;
    }
  }

  clearForm() {
    this.monthlyActivityForm.reset();
    (<any>$('#add-monthly-activity-modal')).modal('hide');
  }

  addActivityForm() {
    this.addEditLabel = 'Add';
    this.isAddActivity = true;
    this.monthlyActivityForm.reset();
  }

  private updateActivityData() {
    this.work = this._dataStoreService.getData(this.constants.Work);
    this.work.employactivity = this.activity;
    this._dataStoreService.setData(this.constants.Work, this.work);
  }

  activitySaveOrUpdate() {
    if (this.isAddActivity) {
      this.activity.push(this.monthlyActivityForm.getRawValue());
    } else {
      if (this.modalActivityInt !== -1) {
        this.activity[this.modalActivityInt] = this.monthlyActivityForm.getRawValue();
      }
    }
    this.updateActivityData();
    this.monthlyActivityForm.reset();
    (<any>$('#add-monthly-activity-modal')).modal('hide');
    const message = (this.isAddActivity) ? 'Added' : 'Updated';
    this._alertSevice.success(message + ' Successfully');
  }

  private patchActivityForm(modal: EmployActivity) {
    this.monthlyActivityForm.patchValue(modal);
  }

  private editActivity(modal, i) {
    this.addEditLabel = 'Edit';
    this.isAddActivity = false;
    this.modalActivityInt = i;
    this.patchActivityForm(modal);
    this.monthlyActivityForm.enable();
    (<any>$('#add-monthly-activity-modal')).modal('show');
  }

  private deleteActivity(index) {
    this.activity.splice(index, 1);
    this.updateActivityData();
    this._alertSevice.success('Deleted Successfully');
    this.monthlyActivityForm.reset();
  }

  calculateAge(dob) {
    // const dob = this.selectedPerson.dob;
    let age = 0;
    if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
        const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
        age = moment().diff(rCDob, 'years');
        const days = moment().diff(rCDob, 'days');
    }
    return age;
  }

  onChangeDate(form) {
    this.minActivityDate = new Date(form.value.start_date);
    form.get('end_date').reset();
  }
}
