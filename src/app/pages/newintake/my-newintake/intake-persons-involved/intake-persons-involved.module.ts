import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakePersonsInvolvedRoutingModule } from './intake-persons-involved-routing.module';
import { IntakePersonsInvolvedComponent } from './intake-persons-involved.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';
import { PopoverModule } from 'ngx-popover';
import { QuillModule } from 'ngx-quill';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { IntakePersonSearchComponent } from './intake-person-search.component';
import { PersonHealthComponent } from '../../../../person-info/person-health/person-health.component';
import { PersonHealthSectionLogComponent } from '../../../../person-info/person-health/person-health-section-log/person-health-section-log.component';
import { EducationalProfileComponent } from '../../../../person-info/educational-profile/educational-profile.component';
import { PhysicianInformationComponent } from '../../../../person-info/person-health/person-health-sections/physician-information/physician-information.component';
import { HealthInsuranceInformationComponent } from '../../../../person-info/person-health/person-health-sections/health-insurance-information/health-insurance-information.component';
import { MedicationIncludingPsychotropicComponent } from '../../../../person-info/person-health/person-health-sections/medication-including-psychotropic/medication-including-psychotropic.component';
import { HealthExaminationComponent } from '../../../../person-info/person-health/person-health-sections/health-examination/health-examination.component';
import { MedicalConditionsComponent } from '../../../../person-info/person-health/person-health-sections/medical-conditions/medical-conditions.component';
import { BehavioralHealthInfoComponent } from '../../../../person-info/person-health/person-health-sections/behavioral-health-info/behavioral-health-info.component';
import { HistoryOfAbuseComponent } from '../../../../person-info/person-health/person-health-sections/history-of-abuse/history-of-abuse.component';
import { SubstanceAbuseComponent } from '../../../../person-info/person-health/person-health-sections/substance-abuse/substance-abuse.component';
import { ProviderInformationCwComponent } from '../../../../person-info/person-health/person-health-sections/provider-information-cw/provider-information-cw.component';
import { DentalInformationCwComponent } from '../../../../person-info/person-health/person-health-sections/dental-information-cw/dental-information-cw.component';
// tslint:disable-next-line:max-line-length
import { MedicationIncludingPsychotropicCwComponent } from '../../../../person-info/person-health/person-health-sections/medication-including-psychotropic-cw/medication-including-psychotropic-cw.component';
import { MedicalConditionsCwComponent } from '../../../../person-info/person-health/person-health-sections/medical-conditions-cw/medical-conditions-cw.component';
import { ExaminationCwComponent } from '../../../../person-info/person-health/person-health-sections/examination-cw/examination-cw.component';
import { WorkProfileComponent } from '../../../../person-info/work-profile/work-profile.component';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { PaginationModule } from 'ngx-bootstrap';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { GuardianshipComponent } from './guardianship/guardianship.component';
import { GuardianPersonComponent } from './guardianship/guardian-person/guardian-person.component';
import { GuardianPropertyComponent } from './guardianship/guardian-property/guardian-property.component';
import { AttorneyComponent } from './guardianship/attorney/attorney.component';
import { WorkerComponent } from './guardianship/worker/worker.component';
import { HssCodeStatusComponent } from './guardianship/hss-code-status/hss-code-status.component';
import { FuneralComponent } from './guardianship/funeral/funeral.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { Eighteen21Component } from './eighteen21/eighteen21.component';
import { Under5yearsCwComponent } from '../../../../person-info/person-health/person-health-sections/under-5years-cw/under-5years-cw.component';
import { SexualInformationCwComponent } from '../../../../person-info/person-health/person-health-sections/sexual-information-cw/sexual-information-cw.component';
import { HospitalizationCwComponent } from '../../../../person-info/person-health/person-health-sections/hospitalization-cw/hospitalization-cw.component';
import { PersonHealthDisabilityComponent } from '../../../../person-info/person-health/person-health-sections/person-health-disability/person-health-disability.component';
import { PersonDisabilityService } from '../../../shared-pages/person-disability/person-disability.service';
import { PersonHealthModule } from '../../../../person-info/person-health/person-health.module';


@NgModule({
  imports: [
    CommonModule,
    IntakePersonsInvolvedRoutingModule,
    FormMaterialModule,
    ImageCropperModule,
    PopoverModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    NgxMaskModule.forRoot(),
    ControlMessagesModule,
    PaginationModule,
    SharedDirectivesModule,
    SharedPipesModule,
    NgSelectModule,
    PersonHealthModule
  ],
  declarations: [IntakePersonsInvolvedComponent,
    IntakePersonSearchComponent,
    PersonHealthComponent,
    PersonHealthSectionLogComponent,
    EducationalProfileComponent,
    PhysicianInformationComponent,
    HealthInsuranceInformationComponent,
    MedicationIncludingPsychotropicComponent,
    HealthExaminationComponent,
    MedicalConditionsComponent,
    BehavioralHealthInfoComponent,
    HistoryOfAbuseComponent,
    SubstanceAbuseComponent,
    ProviderInformationCwComponent,
    DentalInformationCwComponent,
    MedicationIncludingPsychotropicCwComponent,
    MedicalConditionsCwComponent,
    WorkProfileComponent,
    GuardianshipComponent,
    GuardianPersonComponent,
    GuardianPropertyComponent,
    AttorneyComponent,
    WorkerComponent,
    HssCodeStatusComponent,
    FuneralComponent,
    ExaminationCwComponent,
    Eighteen21Component,
    Under5yearsCwComponent,
    SexualInformationCwComponent,
    HospitalizationCwComponent,
    PersonHealthDisabilityComponent,
    ],
  providers: [SpeechRecognitionService, SpeechRecognizerService, NgxfUploaderService, PersonDisabilityService]
})
export class IntakePersonsInvolvedModule { }
