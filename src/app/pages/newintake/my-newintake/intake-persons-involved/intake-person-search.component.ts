import { AfterViewChecked, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { DataStoreService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { ValidationService } from '../../../../@core/services/validation.service';
import { NewUrlConfig } from '../../newintake-url.config';
import {
    AddressDetails,
    Health,
    InvolvedPerson,
    InvolvedPersonSearch,
    InvolvedPersonSearchResponse,
    PersonDsdsAction,
    PersonRelativeDetails,
    PersonRole,
    PriorAuditLog,
    Work,
    EmergencyContactPerson,
    PersonSupport
} from '../_entities/newintakeModel';
import { MyNewintakeConstants, IntakeStoreConstants } from '../my-newintake.constants';
import * as moment from 'moment';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { IntakeConfigService } from '../intake-config.service';
import { IntakeUtils } from '../../../_utils/intake-utils.service';
import { INTAKE_GUARDIANSHIP } from './guardianship/_entities/intake-guardianship-const';

// tslint:disable-next-line:max-line-length
// import { Address } from '../../../case-worker/dsds-action/involved-person/_entities/involvedperson.data.model';
// tslint:disable-next-line:import-blacklist
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-person-search',
    templateUrl: './intake-person-search.component.html',
    styleUrls: ['./intake-person-search.component.scss']
})
export class IntakePersonSearchComponent implements OnInit, AfterViewChecked {
    selectedPerson: InvolvedPersonSearchResponse;
    intakeNumber: string;
    @Output()
    intakePersonAdd = new EventEmitter();
    @Output()
    newPersonAdd = new EventEmitter();
    @Output()
    viewPersonAdd = new EventEmitter();
    @Input() serachPersonProfileInfo$ = new Subject<boolean>();
    @Input() showGuardianship$ = new Subject<boolean>();
    @Input() isEmergencyContact: boolean;
    @Input() guardianPersonSearch: boolean;
    @Input() guardianPersonId: string;
    @Input() hasYouth: boolean;
    @Input() emergencyContactPersonInfo$ = new Subject<EmergencyContactPerson>();
    @Input() representativeDetails$ = new Subject<InvolvedPersonSearchResponse>();
    @Input() representativePerson: boolean;
    roleDropdownItems$: DropdownModel[];
    roleDropdownItems: DropdownModel[];
    stateDropdownItems$: Observable<DropdownModel[]>;
    relationShipToRADropdownItems$: DropdownModel[];
    relationShipToRADropdownItems: DropdownModel[];
    involvedPersonSearchForm: FormGroup;
    personSearchAddForm: FormGroup;
    formalSupportForm: FormGroup;
    ProtectionService: boolean;
    involvedPersondata = false;
    profileTabActive = false;
    serachResultTabActive = false;
    personRoleTabActive = false;
    showPersonDetail = -1;
    maxDate = new Date();
    involvedPersonSearchResponses$: Observable<InvolvedPersonSearchResponse[]>;
    personDSDSActions$: Observable<PersonDsdsAction[]>;
    personRelations$: Observable<PersonRelativeDetails[]>;
    personAddresses$: Observable<AddressDetails[]>;
    genderDropdownItems$: Observable<DropdownModel[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    profileDetails: InvolvedPersonSearchResponse;
    roleName: string;
    personRole: PersonRole[] = [];
    isDjs = false;
    isDjsYouth: boolean;
    private involvedPersonSearch: InvolvedPersonSearch;
    private priorAuditLogRequest = new PriorAuditLog();
    roleDetails: AppUser;
    validation_messages: any;
    @Input()
    healthFormReset$ = new Subject<boolean>();
    @Input()
    workFormReset$ = new Subject<boolean>();
    isChild: boolean;
    isSubstance: boolean;
    agency: string;
    guardianPerson: InvolvedPersonSearchResponse[] = [];
    guardianwork: InvolvedPersonSearchResponse[] = [];
    guardianProperty: InvolvedPersonSearchResponse[] = [];
    guardianAttorney: InvolvedPersonSearchResponse[] = [];
    selectedPersonsage: number;
    enableSafeHaven: boolean;
    purposeObj: any;
    dangerSelf = false;
    dangerWork = false;
    dangerApperance = false;
    dangerSign = false;
    isSDR = false;
    isAS: boolean;
    substanceClass$: Observable<DropdownModel[]>;
    babySubstanceClass$: Observable<DropdownModel[]>;
    substances = [];
    countyDropdownItems$: Observable<DropdownModel[]>;
    isCW: boolean;
    narrative: string;
    checkVPA: any;
    isCheckedCollateral = true;
    isNarrativeVisible = false;
    _dropdownService: any;
    constructor(
        private _formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private _involvedPersonSeachService: GenericService<InvolvedPersonSearchResponse>,
        private _detect: ChangeDetectorRef,
        private _dataStoreService: DataStoreService,
        private _intakeConfig: IntakeConfigService,
        private _intakeService: IntakeUtils
    ) { }

    ngOnInit() {
        this.agency = this._authService.getAgencyName();
        this.isAS = this._authService.isAS();
        this.intakeNumber = this._dataStoreService.getData(IntakeStoreConstants.intakenumber);
        if (!this.isAS) {
            this.purposeObj = this._dataStoreService.getData(IntakeStoreConstants.purposeSelected);
        }
        this.initiateFormGroup();
        this.loadSubstance();
        this.loadDropDown();
        this.getCommonDropdowns();
        this.roleDetails = this._authService.getCurrentUser();
        const teamTypeKey = this.roleDetails.user.userprofile.teamtypekey;
        this._dataStoreService.currentStore.subscribe((store) => {
            if (store) {
                this.ProtectionService = store.ChildProtection;
            }
            if (store[this.guardianPersonId]) {
                this.guardianPerson = store[this.guardianPersonId];
            }
        });
        this.isCW = this._authService.isCW();
        if (teamTypeKey === 'DJS') {
            this.isDjs = true;
            this.isDjsYouth = false;
            // this.involvedPersonSearchForm.get('Firstname').setValidators([Validators.required]);
            // this.involvedPersonSearchForm.get('Firstname').updateValueAndValidity();
            // this.personSearchAddForm.get('firstname').setValidators([Validators.required]);
            // this.personSearchAddForm.get('firstname').updateValueAndValidity();
        } else {
            this.isDjsYouth = true;
        }
        // this.getPage(1);

        // D-07269 Start
        this.spliceYouth();
        // D-07269 Start
        const quicksearchPerson = this._dataStoreService.getData(IntakeStoreConstants.PERSON_TO_SEARCH);
        if (quicksearchPerson) {
            this.involvedPersonSearchForm.patchValue({
                firstname: quicksearchPerson.firstname,
                lastname: quicksearchPerson.lastname,
                dob: moment(new Date(quicksearchPerson.dob), 'MM/DD/YYYY', true), // quicksearchPerson.dob,
                gender: quicksearchPerson.gender,
                ssn: quicksearchPerson.ssn,
                age: quicksearchPerson.age,
                email: quicksearchPerson.email,
                address1: quicksearchPerson.requesteraddress1,
                address2: quicksearchPerson.requesteraddress2,
                stateid: quicksearchPerson.requesterstate,
                city: quicksearchPerson.requestercity,
                county: quicksearchPerson.requestercounty,
                phone: quicksearchPerson.PhoneNumber,
                zip: quicksearchPerson.offenselocation,
                // maidenname: quicksearchPerson.Middlename

            });
            this._dataStoreService.setData(IntakeStoreConstants.PERSON_TO_SEARCH, null);
            this.involvedPersonSearchForm.markAsDirty();
        }
    }
    ngAfterViewChecked() {
        this._detect.detectChanges();
    }

    // D-07269 Start
    spliceYouth() {
        let i = 0;
        if (this.hasYouth) {
            for (const role of this.roleDropdownItems$) {
                if (role.text === 'Youth' || role.value === 'Youth') {
                    this.roleDropdownItems$.splice(i, 1);
                    break;
                }
                i = i + 1;
            }
        }
    }

    hasRoleYouth() {
        this.hasYouth = true;
        this.spliceYouth();
    }
    // D-07269 End

    initiateFormGroup() {
        // Start D-06389
        this.validation_messages = {
            'lastname': [
                { type: 'pattern', message: 'Enter a valid Last Name' }
            ],
            'firstname': [
                { type: 'pattern', message: 'Enter a valid First Name' }
            ],
            'dob': [
                { type: 'pattern', message: 'Enter a valid Date of Birth' }
            ]
        };


        this.personSearchAddForm = this._formBuilder.group({
            Pid: [''],
            Lastname: new FormControl('', Validators.compose([
                // Validators.pattern('^[^0-9]*$')
                Validators.pattern('^[a-zA-Z ]*$')
            ])),
            Firstname: new FormControl('', Validators.compose([
                Validators.pattern('^[a-zA-Z ]*$')
            ])),
            middlename: [''],
            Gender: [''],
            Dob: new FormControl(''),
            age: [''],
            ssn: [''],
            Role: [''],
            rolekeyDesc: [''],
            suffix: [''],
            aliasname: [''],
            address1: [''],
            Address2: [''],
            zipcode: [''],
            state: [''],
            city: [''],
            county: [''],
            RelationshiptoRA: [''],
            Dangerousself: ['', [Validators.required]],
            DangerousselfReason: [''],
            Dangerousworker: ['', [Validators.required]],
            DangerousWorkerReason: [''],
            Mentealimpair: ['', [Validators.required]],
            MentealimpairDetail: [''],
            Mentealillness: ['', [Validators.required]],
            MentealillnessDetail: [''],
            iscollateralcontact: [false],
            // ishousehold: [false],
            ishousehold: ['', [!this._authService.isDJS() ? Validators.required : Validators.pattern('')]],
            drugexposednewbornflag: [false],
            fetalalcoholspctrmdisordflag: [false],
            sexoffenderregisteredflag: [false],
            probationsearchconductedflag: [false],
            otherSubstance: [''],
            substanceClass: [''],
            source: [''],
            safehavenbabyflag: [false]
            // issafehaven: [false]
        });
        this.personSearchAddForm.addControl('personRole', this._formBuilder.array([this.createFormGroup(false)]));
        this.involvedPersonSearchForm = this._formBuilder.group({
            lastname: new FormControl('', Validators.compose([
                Validators.pattern('[a-zA-Z ]*$') // [a-zA-Z]+(\s+[a-zA-Z]+)*
            ])),
            firstname: new FormControl('', Validators.compose([
                Validators.pattern('^[a-zA-Z ]*$')
            ])),
            maidenname: [''],
            gender: [''],
            dob: new FormControl(''),
            age: [''],
            dateofdeath: [''],
            ssn: [''],
            mediasrc: [''],
            mediasrctxt: [''],
            occupation: [''],
            dl: [''],
            stateid: [''],
            address1: [''],
            address2: [''],
            zip: [''],
            city: [''],
            county: [''],
            selectedPerson: [''],
            cjisnumber: [''],
            complaintnumber: [''],
            fein: [''],
            email: ['', [ValidationService.mailFormat]],
            phone: [''],
            petitionid: [''],
            alias: [''],
            oldId: ['']
        });
        this.formalSupportForm = this._formBuilder.group({
            personsupporttypekey: [''],
            description: ['']
        });
    }
    // End D-06389
    searchPersonDetailsRow(id: number, model: PersonDsdsAction) {
        this.searchPersonDetails(id);
        this.getPersonDSDSAction(model);
    }
    formalSupprotChecked(formal) {
        if (formal) {
            this.formalSupportForm.controls['description'].setValidators([Validators.required]);
            this.formalSupportForm.controls['description'].updateValueAndValidity();
        } else {
            this.formalSupportForm.controls['description'].clearValidators();
            this.formalSupportForm.controls['description'].updateValueAndValidity();
            this.formalSupportForm.controls['description'].reset();
        }
    }
    searchPersonDetails(id: number) {
        if (this.showPersonDetail !== id) {
            this.showPersonDetail = id;
        } else {
            this.showPersonDetail = -1;
        }
    }
    private loadDropDown() {
        // const actortypeUrl = this.isDjs ? NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter' : NewUrlConfig.EndPoint.Intake.UserActorTypeUrl + '?filter';
        const actortypeUrl = NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter';
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.GenderTypeUrl + '?filter'
            ),
            // this._commonHttpService.getArrayList(
            //     {
            //         where: {
            //             activeflag: 1,
            //             datypeid: (this._authService.isCW() && this.purposeObj && this.purposeObj.code) ? this.purposeObj.value : null
            //         },
            //         method: 'get',
            //         nolimit: true
            //     },
            //     actortypeUrl
            // ),
            // this._commonHttpService.getArrayList(
            //     {
            //         // where: { activeflag: 1, teamtypekey: this._authService.getAgencyName() },
            //         // method: 'get',
            //         nolimit: true,
            //         order: 'description'
            //     },
            //     NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl // + '?filter'
            // ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'countyname asc',
                },
                NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
            )
        ])
            .map((result) => {
                // this.roleDropdownItems = result[2].map(
                //     (res) => {
                //         return {
                //             text: res.typedescription,
                //             value: res.actortype,
                //             rolegrp: res.rolegroup
                //         };
                //     }
                // );
                // this.relationShipToRADropdownItems = result[3].map(
                //     (res) =>
                //         new DropdownModel({
                //             text: res.description,
                //             value: res.relationshiptypekey
                //         })
                // );
                return {
                    ethinicities: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.ethnicgrouptypekey
                            })
                    ),
                    genders: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gendertypekey
                            })
                    ),
                    // roles: result[2].map(
                    //     (res) => {
                    //         return {
                    //             text: res.typedescription,
                    //             value: res.actortype,
                    //             rolegrp: res.rolegroup
                    //         };
                    //     }
                    // ),
                    // relationShipToRAs: result[3].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.description,
                    //             value: res.relationshiptypekey
                    //         })
                    // ),
                    states: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    counties: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyname
                            })
                    )
                };
            })
            .share();
        this.genderDropdownItems$ = source.pluck('genders');
        // this.roleDropdownItems$ = source.pluck('roles');
        // this.relationShipToRADropdownItems$ = source.pluck('relationShipToRAs');
        this.stateDropdownItems$ = source.pluck('states');
        this.countyDropdownItems$ = source.pluck('counties');
    }

    getCommonDropdowns() {
        this.roleDropdownItems$ = this._dataStoreService.getData('role');
        this.relationShipToRADropdownItems$ = this._dataStoreService.getData('relation');
        this.roleDropdownItems = this._dataStoreService.getData('role');
        this.relationShipToRADropdownItems = this._dataStoreService.getData('relation');

    }
    searchInvolvedPersons(model: InvolvedPersonSearch) {
        this.showPersonDetail = -1;
        this.selectedPerson = Object.assign({}, new InvolvedPersonSearchResponse());
        // console.log('Date value' + $('#dobirth').val().length);
        /*  if ($('#dobirth').val().length > 4) {
             if (model.dob) {
                 const event = new Date(model.dob);
                 model.dob = event.getMonth() + 1 + '/' + event.getDate() + '/' + event.getFullYear();
             }
         } else {
             model.dob = $('#dobirth').val();
         } */
        this.involvedPersondata = true;
        this.involvedPersonSearch = Object.assign(new InvolvedPersonSearch(), model);
        this.involvedPersonSearch.intakeNumber = this.intakeNumber;
        if (this.involvedPersonSearchForm.value.address1) {
            this.involvedPersonSearch.address = this.involvedPersonSearchForm.value.address1 + '' + this.involvedPersonSearchForm.value.address2;
        }
        this.involvedPersonSearch.stateid = this.involvedPersonSearchForm.value.dl;
        this._dataStoreService.setData('SearchDataInvolvedPerson', model);
        this.getPage(1);
        this.tabNavigation('searchresult');
        this.profileTabActive = true;
        this.personRoleTabActive = false;
        const roleDetails = this._authService.getCurrentUser();
        this.roleName = roleDetails.role.name;
    }
    private getPage(pageNumber: number) {
        ObjectUtils.removeEmptyProperties(this.involvedPersonSearch);
        const source = this._involvedPersonSeachService
            .getPagedArrayList(
                {
                    limit: this.paginationInfo.pageSize,
                    order: this.paginationInfo.sortBy,
                    page: pageNumber,
                    count: this.paginationInfo.total,
                    where: this.involvedPersonSearch,
                    method: 'post'
                },
                NewUrlConfig.EndPoint.Intake.GlobalPersonSearchUrl
            )
            .map((result) => {
                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.involvedPersonSearchResponses$ = source.pluck('data');
        if (pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    clearPersonSearch() {
        this.involvedPersonSearchForm.reset();
        this.involvedPersonSearchForm.patchValue({
            occupation: '',
            gender: '',
            mediasrctxt: '',
            stateid: ''
        });
        this.personSearchAddForm.reset();
        this.personSearchAddForm.patchValue({
            Role: '',
            RelationshiptoRA: ''
        });
    }

    getPersonDSDSAction(model: PersonDsdsAction) {
        const url = NewUrlConfig.EndPoint.Intake.IntakeServiceRequestsUrl + `?personid=` + model.personid + '&filter';
        const source = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    method: 'get',
                    where: { intakerequestid: null }
                }),
                url
            )
            .share();
        this.personDSDSActions$ = source.pluck('data');
        this.personDSDSActions$
            .map((data) => {
                data.map((address) => {
                    address.daDetails.map((addressdata) => {
                        if (addressdata.dasubtype === 'Peace Order') {
                            address.highLight = true;
                            return address;
                        }
                    });
                });
                return data;
            })
            .subscribe((finalValue) => console.log(finalValue));
    }

    getPersonRelations(personid) {
        const url = NewUrlConfig.EndPoint.Intake.PersonRelativeUrl + '?filter';
        this.personRelations$ = this._commonHttpService.getArrayList(
            new PaginationRequest({
                method: 'get',
                where: { personid: personid }
            }),
            url
        );
    }
    // getPersonAddress() {
    //     const url = NewUrlConfig.EndPoint.Intake.PersonAddressesUrl + '?filter';
    //     this.personAddresses$ = this._commonHttpService.getArrayList(
    //         new PaginationRequest({
    //             method: 'get',
    //             where: { personid: this.selectedPerson.personid },
    //             include: { relation: 'Personaddresstype', scope: { fields: ['typedescription'] } }
    //         }),
    //         url
    //     );
    // }
    selectPerson(row) {
        this.selectedPerson = row;
        this.enableSafeHaven = false;
        this.selectedPersonsage = this.calculateAge(this.selectedPerson.dob);
        if (this.selectedPerson.source === 'SDR') {
            this.isSDR = true;
        } else {
            this.isSDR = false;
        }
        this.serachResultTabActive = true;
        if (this.agency === 'CW') {
            this.getPersonRelations(this.selectedPerson.personid);
        }
    }
    addContactPerson(savetype: boolean) {
        if (this.selectedPerson.personid) {
            if (savetype) {
                this.guardianPerson.push(this.selectedPerson);
                this._dataStoreService.setData(this.guardianPersonId, this.guardianPerson);
                this.showGuardianship$.next(true);
            } else {
                const person: EmergencyContactPerson = Object.assign({
                    contactpersonid: this.selectedPerson.personid,
                    firstname: this.selectedPerson.firstname,
                    lastname: this.selectedPerson.lastname
                });
                this.emergencyContactPersonInfo$.next(person);
            }
            this.involvedPersonSearchForm.reset();
            this.tabNavigation('profileinfo');
            this.serachResultTabActive = false;
            (<any>$('#intake-findperson')).modal('hide');
        } else {
            this._alertService.error('Please select person');
        }
    }
    addRepresentativePerson() {
        if (this.selectedPerson.personid) {
            this.representativeDetails$.next(this.selectedPerson);
            this.tabNavigation('searchresult');
            this.serachResultTabActive = false;
        } else {
            this._alertService.error('Please select person');
        }
    }
    addSearchPerson() {
        if (this.selectedPerson.lastname) {
            this.personSearchAddForm.patchValue({
                Lastname: this.selectedPerson.lastname,
                Firstname: this.selectedPerson.firstname,
                middlename: this.selectedPerson.middlename,
                Gender: this.selectedPerson.gendertypekey,
                Dob: this.selectedPerson.dob,
                age: this.selectedPerson.age,
                ssn: this.selectedPerson.ssn,
                Pid: this.selectedPerson.personid,
                suffix: this.selectedPerson.suffix,
                aliasname: this.selectedPerson.alias,
                safehavenbabyflag: this.selectedPerson.safehavenbabyflag
            });
            if (this.selectedPerson.primaryaddress) {
                this.personSearchAddForm.patchValue({
                    address1: this.selectedPerson.primaryaddress.split('~')[0] ? this.selectedPerson.primaryaddress.split('~')[0] : '',
                    address2: this.selectedPerson.primaryaddress.split('~')[1] ? this.selectedPerson.primaryaddress.split('~')[1] : '',
                    zipcode: this.selectedPerson.primaryaddress.split('~')[3] ? this.selectedPerson.primaryaddress.split('~')[3] : '',
                    state: this.selectedPerson.primaryaddress.split('~')[5] ? this.selectedPerson.primaryaddress.split('~')[5] : '',
                    city: this.selectedPerson.primaryaddress.split('~')[2] ? this.selectedPerson.primaryaddress.split('~')[2] : '',
                    county: this.selectedPerson.primaryaddress.split('~')[4] ? this.selectedPerson.primaryaddress.split('~')[4] : ''
                });
            }
            // console.log(this.selectedPerson);
            if (this.agency === 'CW') {
                const role = {
                    description: (this.selectedPersonsage >= 18) ? 'Legal Guardian' : 'Child',
                    hidden: null,
                    // isprimary: 'true',
                    isprimary: null,
                    relationshiptorakey: (this.selectedPersonsage >= 18) ? '' : 'SELF', // D-07962
                    rolekey: (this.selectedPersonsage >= 18) ? 'LG' : 'CHILD',
                };

                const roleCode = (this.selectedPersonsage >= 18) ? 'LG' : 'CHILD';
                if (this.selectedPersonsage >= 18) {
                    this.filterrelationshipByRole('LG');
                } else {
                    this.filterrelationshipByRole('child');
                }
                this.personSearchAddForm.patchValue({
                    personRole: [role]
                });
                this.isSubstance = false;
                this.tabNavigation('add-relation-click');
            } else {
                this.tabNavigation('reporterrole');
            }
        } else {
            this._alertService.warn('Please select a person');
        }
    }
    checkSubstanceAbuse() {
        let valid = true;
        const isDrugExposed = this.personSearchAddForm.get('drugexposednewbornflag').value;
        if (isDrugExposed) {
            const substanceClass = this.personSearchAddForm.get('substanceClass').value;
            valid = ((substanceClass && substanceClass.length > 0) || this.personSearchAddForm.get('otherSubstance').value);
        }

        return valid;
    }

    addPerson(involvedPerson: InvolvedPerson) {
        const isChildValidAge = this.validateChildAge(involvedPerson);
        const isSubstanceAbuseValid: boolean = this.checkSubstanceAbuse();
        // if (this.personSearchAddForm.valid && this.personSearchAddForm.dirty && isChildValidAge && isSubstanceAbuseValid) {
        if (this.personSearchAddForm.dirty && isChildValidAge && isSubstanceAbuseValid) {
            if (this.isDjs && this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.ADULT_HOLD_DETENTION)) {
                const roleVictim = involvedPerson.personRole[0].rolekey;
                if (roleVictim === 'Youth') {
                    this._dataStoreService.setData('ResedentialPlacementStatus', false);
                    this._intakeService.getResedentialStatus(involvedPerson.Pid).subscribe(ele => {
                        if (ele && ele.length > 0) {
                            if (ele[0].residentialexists) {
                                this._dataStoreService.setData('ResedentialPlacementStatus', true);
                            }
                        }
                    });
                }
            }

            // D-06472 - For a reporter person, secondary role is mandatory
            const isUnknownReporter = this._dataStoreService.getData(IntakeStoreConstants.isUnknownReporter);
            if (!isUnknownReporter && Array.isArray(involvedPerson.personRole) && involvedPerson.personRole[0].rolekey === 'Rep' && involvedPerson.personRole.length < 2) {
                this._alertService.warn('Reporter role should have a secondary role.');
                return false;
            }

            // check for CW
            if (this.isCW) {
                const value = this.personSearchAddForm.get('iscollateralcontact').value;
                if (!value) {
                    const ishousehold = this.personSearchAddForm.get('ishousehold').value;
                    const Mentealillness = this.personSearchAddForm.get('Mentealillness').value;
                    const Mentealimpair = this.personSearchAddForm.get('Mentealimpair').value;
                    const Dangerousworker = this.personSearchAddForm.get('Dangerousworker').value;
                    const Dangerousself = this.personSearchAddForm.get('Dangerousself').value;
                    if (
                        ishousehold === null || Mentealillness === null || Mentealimpair === null || Dangerousworker === null || Dangerousself === null
                        || ishousehold === '' || Mentealillness === '' || Mentealimpair === '' || Dangerousworker === '' || Dangerousself === ''
                        ) {
                        this._alertService.warn('Please fill mandatory fields');
                        return false;
                    }
                }
            }
            if (this.formalSupportForm.valid) {
                const url = NewUrlConfig.EndPoint.Intake.PersonAddressesUrl + '?filter';
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            method: 'get',
                            where: { personid: this.selectedPerson.personid },
                            include: { relation: 'Personaddresstype', scope: { fields: ['typedescription'] } }
                        }),
                        url
                    )
                    .subscribe((result) => {
                        if (result && result.length !== 0) {
                            result.map((item) => {
                                item.addresstype = item.personaddresstypekey;
                                item.addresstypeLabel = item.Personaddresstype ? item.Personaddresstype.typedescription : '';
                                item.address1 = item.address ? item.address : '';
                                item.Address2 = item.address2 ? item.address2 : '';
                                item.zipcode = item.zipcode ? item.zipcode : '';
                                item.state = item.state ? item.state : '';
                                item.city = item.city ? item.city : '';
                                item.county = item.county ? item.county : '';
                                item.addressid = item.personaddressid;
                            });
                            involvedPerson.personAddressInput = result;
                        } else {
                            involvedPerson.personAddressInput = [];
                        }
                        involvedPerson.personRole.map((role) => {
                            if (role.isprimary === 'true') {
                                involvedPerson.Role = role.rolekey;
                                involvedPerson.rolekeyDesc = role.description;
                            }
                            if (role.relationshiptorakey) {
                                involvedPerson.RelationshiptoRA = role.relationshiptorakey;
                            }
                        });
                        involvedPerson.Pid = this.selectedPerson.personid;
                        involvedPerson.cjamspid = this.selectedPerson.cjamspid;
                        involvedPerson.assistpid = this.selectedPerson.assistpid;
                        involvedPerson.userPhoto = this.selectedPerson.userphoto;
                        involvedPerson.dateofdeath = this.selectedPerson.dateofdeath;
                        involvedPerson.isapproxdod = this.selectedPerson.isapproxdod;
                        involvedPerson.fetalalcoholspctrmdisordflag = involvedPerson.fetalalcoholspctrmdisordflag ? 1 : 0;
                        involvedPerson.drugexposednewbornflag = involvedPerson.drugexposednewbornflag ? 1 : 0;
                        involvedPerson.probationsearchconductedflag = involvedPerson.probationsearchconductedflag ? 1 : 0;
                        involvedPerson.sexoffenderregisteredflag = involvedPerson.sexoffenderregisteredflag ? 1 : 0;
                        involvedPerson.safehavenbabyflag = involvedPerson.safehavenbabyflag ? 1 : 0;
                        involvedPerson.substances = this.substances;
                        if (this.formalSupportForm.controls['personsupporttypekey'].value) {
                            this.formalSupportForm.value.personsupporttypekey = this.formalSupportForm.value.personsupporttypekey ? 'formal' : null;
                            const formalSupport: PersonSupport[] = [];
                            formalSupport.push(Object.assign({
                                personid: this.selectedPerson.personid,
                                supportername: null,
                            }, this.formalSupportForm.value));
                            involvedPerson.personsupport = formalSupport;
                        }
                        // D-07269 Start
                        if (involvedPerson.Role === 'Youth' || involvedPerson.rolekeyDesc === 'Youth') {
                            this.hasRoleYouth();
                        }
                        // D-07269 End

                        if (this.validatePrimaryRole()) {
                            this.intakePersonAdd.emit(involvedPerson);
                            this.clearSearchDetails();
                        } else {
                            this._alertService.error('Please Select One Primary Role Type');
                        }
                    });
            } else {
                this._alertService.warn('Please fill the formal support description');
            }
        } else {
            if (!isSubstanceAbuseValid) {
                this._alertService.error('Please fill substance details.');
            } else if (isChildValidAge) {
                ControlUtils.validateAllFormFields(this.personSearchAddForm);
                ControlUtils.setFocusOnInvalidFields();
                this._alertService.warn('Please fill mandatory fields');
            } else {
                this._alertService.error('Child or reported child age should be less than 18.');
            }
        }
    }

    addNewPerson() {
        this.newPersonAdd.emit(null);
        this.clearSearchDetails();
        (<any>$('#intake-findperson')).modal('hide');
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Health.Health, new Health());
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Work.Work, new Work());
        this.healthFormReset$.next(true);
        this.workFormReset$.next(true);

    }

    addPersonFromSDR() {
        this.newPersonAdd.emit(this.selectedPerson);
        this.clearSearchDetails();
        (<any>$('#intake-findperson')).modal('hide');
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Health.Health, new Health());
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Work.Work, new Work());
        this.healthFormReset$.next(true);
        this.workFormReset$.next(true);
    }

    viewPersonDetails(person: InvolvedPerson) {
        this.viewPersonAdd.emit(person);
    }
    clearSearchDetails() {
        this.personSearchAddForm.setControl('personRole', this._formBuilder.array([]));
        this.addNewRole();
        this.formalSupportForm.reset();
        this.involvedPersonSearchResponses$ = Observable.empty();
        this.tabNavigation('profileinfo');
        (<any>$('#intake-findperson')).modal('hide');
        this.clearPersonSearch();
        this.personRoleTabActive = false;
        this.serachResultTabActive = false;
        this.profileTabActive = false;
        if (this.isEmergencyContact) {
            (<any>$('#intake-findperson')).modal('hide');
            this.serachPersonProfileInfo$.next(true);
        }
    }

    viewProfileDetails(vieeDetails: InvolvedPersonSearchResponse) {
        (<any>$('#info-details')).modal('show');
        this.profileDetails = vieeDetails;
    }
    closeInformation() {
        (<any>$('#info-details')).modal('hide');
    }
    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.getPage(this.paginationInfo.pageNumber);
    }
    disabledInput(state: boolean, inputfield: string, manditory: string) {
        if (state === false && manditory === 'isManditory') {
            this.personSearchAddForm.get(inputfield).enable();
            if (inputfield === 'DangerousselfReason') {
                this.personSearchAddForm.get('Dangerousself').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerSelf = true;
                        this.personSearchAddForm.get('DangerousselfReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('DangerousselfReason').updateValueAndValidity();
                    } else {
                        this.dangerSelf = false;
                        this.personSearchAddForm.get('DangerousselfReason').clearValidators();
                        this.personSearchAddForm.get('DangerousselfReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'DangerousWorkerReason') {
                this.personSearchAddForm.get('Dangerousworker').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerWork = true;
                        this.personSearchAddForm.get('DangerousWorkerReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('DangerousWorkerReason').updateValueAndValidity();
                    } else {
                        this.dangerWork = false;
                        this.personSearchAddForm.get('DangerousWorkerReason').clearValidators();
                        this.personSearchAddForm.get('DangerousWorkerReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'MentealimpairDetail') {
                this.personSearchAddForm.get('Mentealimpair').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerApperance = true;
                        this.personSearchAddForm.get('MentealimpairDetail').setValidators([Validators.required]);
                        this.personSearchAddForm.get('MentealimpairDetail').updateValueAndValidity();
                    } else {
                        this.dangerApperance = false;
                        this.personSearchAddForm.get('MentealimpairDetail').clearValidators();
                        this.personSearchAddForm.get('MentealimpairDetail').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'MentealillnessDetail') {
                this.personSearchAddForm.get('Mentealillness').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerSign = true;
                        this.personSearchAddForm.get('MentealillnessDetail').setValidators([Validators.required]);
                        this.personSearchAddForm.get('MentealillnessDetail').updateValueAndValidity();
                    } else {
                        this.dangerSign = false;
                        this.personSearchAddForm.get('MentealillnessDetail').clearValidators();
                        this.personSearchAddForm.get('MentealillnessDetail').updateValueAndValidity();
                    }
                });
            }
        }
        if (state) {
            this.personSearchAddForm.get(inputfield).disable();
        } else if (!state && manditory === 'notManditory') {
            this.personSearchAddForm.get(inputfield).enable();
        }
    }
    tabNavigation(id) {
        (<any>$('#' + id)).click();
        if (id === 'searchresult') {
            this.personRoleTabActive = true;
        }
        if (id === 'profileinfo') {
            this.paginationInfo.pageNumber = 1;
        }
    }

    relationShipToRO(event: any, i: number) {
        console.log('D-07265 : this.isDjs' + this.isDjs);
        if (this.isDjs) {
            this.personsRoleSelected(event, i);
        } else {
            if (this.validateLegalGuardian(event.value)) {
                this._alertService.error('Legal Guardian is always the adult (18 and above) and cannot be a child.');
            }
            if (['BIOCHILD', 'CHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC'].includes(event.value)) {
                this.isChild = true;
                this.filterrelationshipByRole('child');
            } else {
                this.isChild = false;
                this.filterrelationshipByRole('LG');
            }
            const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
            const _personRole = personRoleArray.controls[i];
            _personRole.patchValue({ description: event.source.triggerValue });
            if (this.isCW) { _personRole.patchValue({ isprimary: 'true' }); }
            const roleCount = personRoleArray.controls.filter((res) => res.value.rolekey === event.value);
            if (roleCount.length > 1) {
                this.deleteRole(i);
                this._alertService.error('Role is already Added');
            } else {
                if (event.value !== 'RA' && event.value !== 'PA' && event.value !== 'RC' && event.value !== 'CLI' && event.value !== '2085') {

                    const raCount = personRoleArray.controls.filter((res) => res.value.hidden === false);
                    // if (raCount.length === 0) {
                        _personRole.patchValue({ hidden: false });
                        _personRole.get('relationshiptorakey').setValidators([Validators.required]);
                        _personRole.get('relationshiptorakey').updateValueAndValidity();
                    // } else {
                    //     _personRole.patchValue({ hidden: true });
                    //     _personRole.get('relationshiptorakey').clearValidators();
                    //     _personRole.get('relationshiptorakey').updateValueAndValidity();
                    // }
                } else {

                    _personRole.patchValue({ hidden: true });
                    _personRole.get('relationshiptorakey').clearValidators();
                    _personRole.get('relationshiptorakey').updateValueAndValidity();
                }
            }

            // D-07962 Start
            if (event.value === 'CHILD') {
                _personRole.get('relationshiptorakey').setValue('SELF');
            }
            // D-07962 End
        }
    }

     // Age calculator
     private getAge(Dob) {
        if (
            Dob &&
            moment(new Date(Dob), 'MM/DD/YYYY', true).isValid()
        ) {
            const rCDob = moment(new Date(Dob), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }

    private validateChildAge(involvedPerson: InvolvedPerson): boolean {
        let isValidAge = true;
        involvedPerson.personRole.map(role => {
            if (role.rolekey === 'CHILD' || role.rolekey === 'RC') {
                const personAge = this.getAge(involvedPerson.Dob);
                if (personAge >= 21) {
                    isValidAge = false;
                }
            }
        });
        return isValidAge;
    }

    personsRoleSelected(event: any, i: number) {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls[i];
        _personRole.patchValue({ isprimary: 'true', description: event.source.triggerValue });
        if (event.value !== 'RA' && event.value !== 'PA' && event.value !== 'RC' && event.value !== 'CLI' && event.value !== 'Youth') {
            _personRole.patchValue({ hidden: false });
            _personRole.get('relationshiptorakey').setValidators([Validators.required]);
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        } else {
            _personRole.patchValue({ hidden: true });
            _personRole.get('relationshiptorakey').clearValidators();
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        }

        // D-07265 Start
        // if (event.value !== 'Youth') {
        this.isDjsYouth = false;
        // } else {
        //     this.isDjsYouth = true;
        // }
        //  if (event.value !== 'Youth' || event.value === 'Youth') {
        this.personSearchAddForm.get('Dangerousself').clearValidators();
        this.personSearchAddForm.get('Dangerousself').updateValueAndValidity();
        this.personSearchAddForm.get('Dangerousworker').clearValidators();
        this.personSearchAddForm.get('Dangerousworker').updateValueAndValidity();
        this.personSearchAddForm.get('Mentealimpair').clearValidators();
        this.personSearchAddForm.get('Mentealimpair').updateValueAndValidity();
        this.personSearchAddForm.get('Mentealillness').clearValidators();
        this.personSearchAddForm.get('Mentealillness').updateValueAndValidity();
        // }
        // D-07265 End
    }
    priorAuditLog(item: PriorAuditLog) {
        window.open('#/pages/case-worker/' + item.intakeserviceid + '/' + item.danumber + '/dsds-action/report-summary', '_blank');
        const url = NewUrlConfig.EndPoint.Intake.PriorAuditLogUrl;
        this.priorAuditLogRequest = Object.assign({}, item);
        this.priorAuditLogRequest.priordanumber = item.danumber;
        this.priorAuditLogRequest.danumber = this.intakeNumber;
        const obj = this.priorAuditLogRequest;
        this._commonHttpService
            .getArrayList(
                {
                    method: 'post',
                    obj
                },
                url
            )
            .subscribe();
    }
    addNewRole() {
        const control = <FormArray>this.personSearchAddForm.controls['personRole'];
        control.push(this.createFormGroup(true));
    }
    deleteRole(index: number) {
        const control = <FormArray>this.personSearchAddForm.controls['personRole'];
        control.removeAt(index);
    }
    createFormGroup(isHideRA) {
        return this._formBuilder.group({
            rolekey: ['', Validators.required],
            description: [''],
            isprimary: ['', Validators.required],
            relationshiptorakey: [null],
            hidden: [isHideRA]
        });
    }
    setFormValues() {
        this.personSearchAddForm.setControl('personRole', this._formBuilder.array([]));
        const control = <FormArray>this.personSearchAddForm.controls.personRole;
        this.personRole.forEach((x) => {
            control.push(this.buildPersonRoleForm(x));
        });
    }
    private buildPersonRoleForm(x): FormGroup {
        return this._formBuilder.group({
            rolekey: x.rolekey,
            isprimary: x.isprimary,
            relationshiptorakey: x.relationshiptorakey,
            hidden: x.hidden,
            description: ['']
        });
    }
    checkPrimaryRole(i) {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach((x) => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        if (primaryCount > 1) {
            this._alertService.error('Primary role for this person is already selected');
            _personRole[i].patchValue({ isprimary: 'false' });
        }
    }
    validatePrimaryRole() {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach((x) => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        // if (primaryCount === 1) {
        //     return true;
        // } else {
        //     if (this.isCW) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // }
        //quick fix on primary rle check, this will be fixed when person profile redeisnged
        return true;        
    }
    private validateLegalGuardian(role): boolean {
        if (role === 'LG' && this.ProtectionService) {
            const dob = this.selectedPerson.dob;
            const age = this.getAge(new Date(dob));
            if (age >= 18) {
                return false;
            }
            return true;
        }
    }
    checkChildStatus(control) {
        const value = this.personSearchAddForm.get(control).value;
        if (value) {
            const dob = this.personSearchAddForm.get('Dob').value;
            let ageDays = 0;
            if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
                const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
                ageDays = moment().diff(rCDob, 'days');
            }
            if (ageDays >= 31) {
                this._alertService.warn('Person must be 30 days or less in age');
                this.personSearchAddForm.get(control).setValue(false);
                // this.isSubstance = false;
                control = '';
            } else {
                if (control === 'drugexposednewbornflag') {
                    this.isSubstance = value;
                    this.personSearchAddForm.get('otherSubstance').setValue(null);
                    this.personSearchAddForm.get('substanceClass').setValue(null);
                } else {
                    // this.isSubstance = false;
                }
            }
        }
        this.isSubstance = (control === 'drugexposednewbornflag') ? value : this.isSubstance;
        console.log(value);
    }
    calculateAge(dob) {
        // const dob = this.selectedPerson.dob;
        let age = 0;
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            age = moment().diff(rCDob, 'years');
            const days = moment().diff(rCDob, 'days');
            this.enableSafeHaven = (days <= 10);
        }
        return age;
    }

    getPrimaryRelationname(listdata) {
        const finddata = listdata.find(data => data.relationcategory === 'Primary');
        if (finddata) {
            return (finddata.relationtype + ': ' + finddata.personname);
        }
        return '';
    }

    filterRoles(item): boolean {
        if (this.selectedPersonsage < 22) {
            if (item.rolegrp === 'C' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        } else if (this.selectedPersonsage >= 22) {
            if (item.rolegrp === 'A' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        }
    }
    private loadSubstance() {
        const babysubstanceList = ['BOTH', 'BPD', 'BPCP', 'BMTD', 'BMJA', 'BHOI', 'BESY', 'BCOC', 'BBS', 'BAS', 'FASD'];
        const source = this._commonHttpService.getArrayList(
            {
                where: { teamtypekey: 'CW', referencetypeid: 55 },
                method: 'get',
                nolimit: true
            },
            CommonUrlConfig.EndPoint.Intake.adultScreenRiskMeasure
        ).map((result) => {
            return {
                response: result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                ),
                babylist: result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                ).filter(
                    (item) => babysubstanceList.includes(item.value)
                )
            };
        }).share();
        this.substanceClass$ = source.pluck('response');
        this.babySubstanceClass$ = source.pluck('babylist');
    }
    changeSubtance(item) {
        this.substances = item.map((res) => {
            return {
                subtancekey: res,
                othersubtance: ''
            };
        });
    }

    showNarrative() {
        this.isNarrativeVisible = true;
        this.narrative = this._dataStoreService.getData(IntakeStoreConstants.addNarrative).Narrative;

        // this._dataStoreService.currentStore.subscribe((item) => {
        //     if (item[IntakeStoreConstants.addNarrative]) {
        //         if (item[IntakeStoreConstants.addNarrative]) {
        //             const narrativeData = item[IntakeStoreConstants.addNarrative];
        //             if (narrativeData) {
        //                 this.narrative = narrativeData.Narrative;
        //                 // Not a good solution can be replaced by pipe
        //                 document.getElementById('narrativeElementSearch').innerHTML = ' Narrative :' + this.narrative;
        //             }
        //         }
        //     }
        // });
    }
    hideNarrative() {
        this.isNarrativeVisible = false;
    }

    filterrelationshipByRole(role) {
        const childrelationList = ['BIOBR', 'BGSISTR', 'BFRND', 'BFRNDX', 'DACRCHLD', 'fosterchild', 'FRND', 'GFRND',
            'GFRNDX', 'HLFBR', 'HLFSISTR', 'LEGLBR', 'LGLSISTR', 'MTNLCN', 'MATNLNPW',
            'MATNLNC', 'NBHR', 'NORLTN', 'OTHER', 'PRNTLCN', 'PRNTLNPW', ' PRNTLNC', 'PUTCHLD', 'RELOTHR', 'RESDNT', 'SELF', 'SNFOPAR', 'STPBR', 'STPSISTR',
            'STUDNT', 'UNKNWN'];
        if (this.agency === 'CW') {
            switch (role) {
                case 'child':
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems ?
                        this.relationShipToRADropdownItems.filter(item => childrelationList.includes(item.value)) : this.relationShipToRADropdownItems;
                    break;

                default:
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems ? this.relationShipToRADropdownItems.filter(item => true) : this.relationShipToRADropdownItems;
                    break;
            }
        }
    }

    // reset collateral chkbox
    resetCollateralChkbox() {
        if (this.isCW) {
        this.personSearchAddForm.get('iscollateralcontact').reset();
        this.resetRoleTabRadioBtn(null);
        }
    }
    // reset roletab radio btn
    resetRoleTabRadioBtn(paramValue = null) {
        if (this.isCW) {
        let value = false;
        if (paramValue !== null) {
            value = paramValue;
        } else {
            value = this.personSearchAddForm.get('iscollateralcontact').value;
        }
        const radioBtnAry = ['ishousehold', 'Mentealillness', 'Mentealimpair', 'Dangerousworker', 'Dangerousself'];
        if (value) {
            radioBtnAry.forEach(ctrlName => {
                this.personSearchAddForm.get(ctrlName).clearValidators();
                this.personSearchAddForm.get(ctrlName).updateValueAndValidity();
            });
            this.personSearchAddForm.get('ishousehold').reset();
            this.isCheckedCollateral = false;
        } else {
            this.personSearchAddForm.get('ishousehold').setValidators([Validators.required]);
            this.personSearchAddForm.get('ishousehold').updateValueAndValidity();

            this.personSearchAddForm.get('Mentealillness').setValidators([Validators.required]);
            this.personSearchAddForm.get('Mentealillness').updateValueAndValidity();

            this.personSearchAddForm.get('Mentealimpair').setValidators([Validators.required]);
            this.personSearchAddForm.get('Mentealimpair').updateValueAndValidity();

            this.personSearchAddForm.get('Dangerousworker').setValidators([Validators.required]);
            this.personSearchAddForm.get('Dangerousworker').updateValueAndValidity();

            this.personSearchAddForm.get('Dangerousself').setValidators([Validators.required]);
            this.personSearchAddForm.get('Dangerousself').updateValueAndValidity();
            this.isCheckedCollateral = true;
        }
    }
    }
}




// Start D-06389
export class DateValidator {

    constructor() {
    }

    static date(c: FormControl) {
        // const dateRegEx = new RegExp(/^(0\d|1[0-2])\/([0-2]\d|3[0-1])\/(19|20)\d{2}$/);
        // console.log('Date Value is:',c.value);
        return c.value != null ? null : {
            dateValidator: {
                valid: false
            }
        };
    }
}

 // End D-06389
