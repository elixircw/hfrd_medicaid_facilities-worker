import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AlertService } from '../../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { NewUrlConfig } from '../../../newintake-url.config';
import { Accomplishment, Education, School, Testing, Vocation, EducationDetails } from '../../_entities/newintakeModel';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AppConfig } from '../../../../../app.config';
import { AuthService } from '../../../../../@core/services/auth.service';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { MyNewintakeConstants } from '../../my-newintake.constants';
import { DataStoreService, CommonDropdownsService } from '../../../../../@core/services';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { config } from '../../../../../../environments/config';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'educational-profile',
    templateUrl: './educational-profile.component.html',
    styleUrls: ['./educational-profile.component.scss']
})
export class EducationalProfileComponent implements OnInit {
    @Input()
    addEducationSubject$ = new Subject<Education>();
    @Input()
    addEducationOutputSubject$ = new Subject<Education>();
    @Input()
    educationFormReset$ = new Subject<boolean>();
    addEducation: Education;
    schoolForm: FormGroup;
    vocationForm: FormGroup;
    testingForm: FormGroup;
    educationDetailsForm: FormGroup;
    accomplishmentForm: FormGroup;
    vocationButton: boolean;
    school: School[] = [];
    vocation: Vocation[] = [];
    testing: Testing[] = [];
    accomplishment: Accomplishment[] = [];
    educationDetails: EducationDetails[] = [];
    vocationEditInd = -1;
    schoolEditInd = -1;
    testingEditInd = -1;
    accomplishEditInd = -1;
    edcnDetailInd = -1;
    isSpecialEducation = false;
    schoolTypeDropdownItems$: Observable<DropdownModel[]>;
    specialEducationDropdownItems$: Observable<DropdownModel[]>;
    stateDropdownItems$: Observable<DropdownModel[]>;
    lastGradeDropdownItems$: Observable<DropdownModel[]>;
    currentGradeDropdownItems$: Observable<DropdownModel[]>;
    testingTypeDropdownItems$: Observable<DropdownModel[]>;
    countyDropDownItems: DropdownModel[];
    countyDropDownItems$: Observable<DropdownModel[]>;
    testingDescription: string;
    gradeDescription: string;
    schoolDescription: string;
    maxDate = new Date();
    uploadedFile: File;
    isDjs = false;
    private token: AppUser;
    private personId: string;
    schoolSearchList$ = new Observable<any>();
    baseUrl: string;
    schoolid: string;
    constructor(
        private formbulider: FormBuilder,
        private _uploadService: NgxfUploaderService,
        private _commonHttpService: CommonHttpService,
        private _alertSevice: AlertService,
        private _authService: AuthService,
        private http: HttpClient,
        private _dataStoreService: DataStoreService,
        private _commonDropdownsService: CommonDropdownsService
    ) {
        this.baseUrl = AppConfig.baseUrl;
        this.token = this._authService.getCurrentUser();
    }

    ngOnInit() {
        this.isDjs = this._authService.isDJS();
        // this.personId = this._personDetailService.person.personid;
        this.schoolForm = this.formbulider.group({
            personeducationid: [''],
            personid: [''],
            educationname: [''],
            educationtypekey: [null],
            countyid: [''],
            statecode: [''],
            startdate: [null],
            enddate: [null],
            lastgradetypekey: [null],
            currentgradetypekey: [null],
            isspecialeducation: [false],
            specialeducation: [''],
            specialeducationtypekey: [null],
            absentdate: [null],
            isreceived: [false],
            isverified: [false],
            isexcuesed: [false],
            reciveVeriExc: [''],
            extracurricular: ['']
        },
            { validator: this.checkDateRange }
        );
        this.testingForm = this.formbulider.group({
            personeducationtestingid: [''],
            personid: [''],
            testingtypekey: [null],
            readinglevel: [null],
            readingtestdate: [null],
            mathlevel: [null],
            mathtestdate: [null],
            testingprovider: ['']
        });

        this.accomplishmentForm = this.formbulider.group({
            personaccomplishmentid: [''],
            personid: [''],
            highestgradetypekey: [null],
            accomplishmentdate: [null],
            isrecordreceived: [''],
            receiveddate: [null]
        });

        this.vocationForm = this.formbulider.group({
            personeducationvocationid: [''],
            personid: [''],
            isvocationaltest: [''],
            vocationinterest: [''],
            vocationaptitude: [''],
            certificatename: [''],
            certificatepath: [''],
            uploadFile: ['']
        });
        this.loadDropDown();
        this.addEducation = {
            school: [],
            testing: [],
            accomplishment: [],
            vocation: [],
            personId: this.personId
        };
        this.educationDetailsForm = this.formbulider.group({
            typeOfClass : [''],
            lastgradetypekey: [''],
            currentgradetypekey : [''],
            splEdcLeastRestEnv : [''],
            funGradeLevel : [''],
            dateLastAttended : [''],
            edcnPerformance: this.formbulider.group({
                quater1 : [''],
                quater2 : [''],
                quater3 : [''],
                quater4 : [''],
            }),
            splEdcnNeeds : [''],
            schlExitReason : [''],
            schlExitDate : [''],
            strengths : [''],
            weakness : [''],
            edcnProgram : [''],
            homeHospEdcnServices : [''],
            edcnPrgmGoal : [''],
            extCurricularAct : [''],
            comments : [''],
            disabledQuestion : [''],
            diabilityNotes : [''],
            schlChngdPlcmnt : [''],
            reasonToChangeSchl : [''],

        });

        this.educationFormReset$.subscribe((res) => {
            if (res === true) {
                this.schoolForm.reset();
                this.testingForm.reset();
                this.accomplishmentForm.reset();
                this.vocationForm.reset();
                this.accomplishmentForm.get('receiveddate').enable();
            }
        });
        // this.addEducationOutputSubject$.subscribe((education) => {
        //     this.personId = education.personId;
        //     this.school = education.school ? education.school : [];
        //     this.accomplishment = education.accomplishment ? education.accomplishment : [];
        //     this.testing = education.testing ? education.testing : [];
        //     this.vocation = education.vocation ? education.vocation : [];
        // });
        const education = this._dataStoreService.getData(MyNewintakeConstants.Intake.PersonsInvolved.Educational);
        if (education) {
            this.personId = education.personId;
            this.school = education.school ? education.school : [];
            this.accomplishment = education.accomplishment ? education.accomplishment : [];
            this.testing = education.testing ? education.testing : [];
            this.vocation = education.vocation ? education.vocation : [];
        }
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.accomplishmentForm.get('receiveddate').enable();
        this.loadSchoolDropdown();
    }
    checkDateRange(schoolForm) {
        if (schoolForm.controls.enddate.value) {
            if (
                schoolForm.controls.enddate.value <
                schoolForm.controls.startdate.value
            ) {
                return { notValid: true };
            }
            return null;
        }
    }
    schoolTypeDescription(model) {
        this.schoolDescription = '';
        this.schoolDescription = model.text;
    }

    addSchool(model: School) {

        if (this.schoolForm.value.absentdate && this.schoolForm.value.enddate != null && this.schoolForm.value.absentdate > this.schoolForm.value.enddate) {
            return this._alertSevice.error('Absence date should not be exceed end date');
        } else if (this.schoolForm.value.absentdate && this.schoolForm.value.startdate > this.schoolForm.value.absentdate) {
            return this._alertSevice.error('Absence date should not be less than start date');
        }
        if (this.schoolDescription && this.schoolDescription !== '') {
            model.schoolTypeDescription = this.schoolDescription;
        }
        model.isspecialeducation = model.specialeducation === 'true' ? true : false;
        model.personid = this.personId;
        model.schoolid = this.schoolid;
        this.school.push(model);
        this.addEducation.school = this.school;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.schoolid = '';
        this.schoolDescription = '';
        this.schoolForm.reset();
        this._alertSevice.success('Address Added Successfully');
        $('#schoolAcdn').click();

    }
    updateSchool(model: School) {
        if (this.schoolForm.value.absentdate && this.schoolForm.value.enddate != null && this.schoolForm.value.absentdate > this.schoolForm.value.enddate) {
            return this._alertSevice.error('Absence date should not be exceed end date');
        } else if (this.schoolForm.value.absentdate && this.schoolForm.value.startdate > this.schoolForm.value.absentdate) {
            return this._alertSevice.error('Absence date should not be less than start date');
        }
        if (this.schoolDescription && this.schoolDescription !== '') {
            model.schoolTypeDescription = this.schoolDescription;
        }
        model.schoolid = this.schoolid;
        this.school[this.schoolEditInd] = model;
        this.school[this.schoolEditInd].isspecialeducation = model.specialeducation === 'true' ? true : false;
        this.addEducation.school = this.school;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.schoolEditInd = -1;
        this.schoolForm.reset();
        this.schoolDescription = '';
        this._alertSevice.success('Address Updated Successfully');
        $('#schoolAcdn').click();
    }
    editSchool(model, index) {
        this.schoolEditInd = index;
        this.schoolForm.patchValue(model);
        this.specialEducation(model.specialeducation);
        this.schoolTypeDescription({ text: model.schoolTypeDescription, value: model.currentgradetypekey });
        this.schoolForm.enable();
        this.schoolid = model.schoolid;
        this.loadCounty(model.statecode, model.countyid);
        $('#schoolAcdn').click();
    }
    viewSchool(model) {
        this.schoolForm.patchValue(model);
        this.specialEducation(model.specialeducation);
        this.schoolForm.disable();
        this.schoolEditInd = -1;
        $('#schoolAcdn').click();
    }
    deleteSchool(index) {
        this.school.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.school = this.school;
        this.schoolid = null;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.schoolForm.enable();
        this.schoolForm.reset();
        this.schoolEditInd = -1;

    }
    selectTestingDescription(model) {
        this.testingDescription = '';
        this.testingDescription = model.text;
    }
    addTesting(model: Testing) {
        if (this.testingDescription && this.testingDescription !== '') {
            model.testdescription = this.testingDescription;
        }
        this.testing.push(model);
        this.testingForm.reset();
        this.addEducation.testing = this.testing;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.testingDescription = '';
    }
    updateTesting(model: Testing) {
        if (this.testingDescription && this.testingDescription !== '') {
            model.testdescription = this.testingDescription;
        }
        model.testdescription = this.testingDescription;
        this.testing[this.testingEditInd] = model;
        this.testingForm.reset();
        this.addEducation.testing = this.testing;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.testingEditInd = -1;
        this.testingDescription = '';
    }
    editTesting(model, index) {
        this.testingEditInd = index;
        this.testingForm.patchValue(model);
        this.selectTestingDescription({ text: model.testdescription, value: model.testingtypekey });
        this.testingForm.enable();
        $('#testingAcdn').click();
    }
    viewTesting(model) {
        this.testingForm.patchValue(model);
        this.testingForm.disable();
        this.testingEditInd = -1;
        $('#testingAcdn').click();
    }
    clearSortingFilters() {
        this.schoolForm.patchValue({
         reciveVeriExc: ' ' });
    }
    deleteTesting(index) {
        this.testing.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.testing = this.testing;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.testingForm.enable();
        this.testingForm.reset();
        this.testingEditInd = -1;
    }

    addEdcnDetails(model: EducationDetails) {

        this.educationDetails.push(model);
        this.educationDetailsForm.reset();
        this.addEducation.educationDetails = this.educationDetails;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
       // this.testingDescription = '';
    }

    updateEdcnDetails(model: EducationDetails) {

        this.educationDetails[this.edcnDetailInd] = model;
       // this.educationDetails[this.edcnDetailInd].isspecialeducation = model.specialeducation === 'true' ? true : false;
        this.addEducation.educationDetails = this.educationDetails;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.edcnDetailInd = -1;
        this.educationDetailsForm.reset();
        // this.schoolDescription = '';
    }
    editEdcnDetails(model, index) {
       this.edcnDetailInd = index;
        this.educationDetailsForm.patchValue(model);
       // this.specialEducation(model.specialeducation);
       // this.schoolTypeDescription({ text: model.schoolTypeDescription, value: model.currentgradetypekey });
        this.educationDetailsForm.enable();
        $('#EdcnDetailsAcdn').click();
    }
    viewEdcnDetails(model) {
        this.educationDetailsForm.patchValue(model);
      //  this.specialEducation(model.specialeducation);
        this.educationDetailsForm.disable();
        this.edcnDetailInd = -1;
        $('#EdcnDetailsAcdn').click();
    }
    deleteEdcnDetails(index) {
        this.educationDetails.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.school = this.school;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.educationDetailsForm.enable();
        this.educationDetailsForm.reset();
        this.edcnDetailInd = -1;
    }

    highetGradeDescription(model) {
        this.gradeDescription = '';
        this.gradeDescription = model.text;
    }
    addAccomplishment(model: Accomplishment) {
        if (this.accomplishmentForm.valid) {
            if (this.gradeDescription && this.gradeDescription !== '') {
                model.gradedescription = this.gradeDescription;
            }
            this.accomplishment.push(model);
            this.accomplishmentForm.reset();
            this.addEducation.accomplishment = this.accomplishment;
            // this.addEducationSubject$.next(this.addEducation);
            this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
            this.gradeDescription = '';
        } else {
            this._alertSevice.warn('Please fill mandatory fields');
        }
    }
    setManditory(state: boolean, inputfield: string, manditory: string) {
        if (state === false && manditory === 'isManditory') {
            if (inputfield === 'receiveddate') {
                this.accomplishmentForm.get('isrecordreceived').valueChanges.subscribe((recordRecived: any) => {
                    if (recordRecived === 'Yes') {
                        this.accomplishmentForm.get('receiveddate').enable();
                        this.accomplishmentForm.get('receiveddate').setValidators([Validators.required]);
                        this.accomplishmentForm.get('receiveddate').updateValueAndValidity();
                    } else {
                        this.accomplishmentForm.get('receiveddate').clearValidators();
                        this.accomplishmentForm.get('receiveddate').updateValueAndValidity();
                        this.accomplishmentForm.get('receiveddate').reset();
                        this.accomplishmentForm.get('receiveddate').disable();
                    }
                });
            }
        }
    }
    updateAccomplishment(model: Accomplishment) {
        if (this.accomplishmentForm.valid) {
            if (this.gradeDescription && this.gradeDescription !== '') {
                model.gradedescription = this.gradeDescription;
            }
            model.gradedescription = this.gradeDescription;
            this.accomplishment[this.accomplishEditInd] = model;
            this.accomplishmentForm.reset();
            this.addEducation.accomplishment = this.accomplishment;
            // this.addEducationSubject$.next(this.addEducation);
            this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
            this.accomplishEditInd = -1;
            this.gradeDescription = '';
        } else {
            this._alertSevice.warn('Please fill mandatory fields');
        }
    }
    editAccomplishment(model, index) {
        this.accomplishEditInd = index;
        this.accomplishmentForm.patchValue(model);
        this.highetGradeDescription({ text: model.gradedescription, value: model.highestgradetypekey });
        this.accomplishmentForm.enable();
        $('#accomplishmentsAcdn').click();
    }
    viewAccomplishment(model) {
        this.accomplishmentForm.patchValue(model);
        this.accomplishmentForm.disable();
        this.accomplishEditInd = -1;
    }
    deleteAccomplishment(index) {
        this.accomplishment.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.accomplishment = this.accomplishment;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.accomplishmentForm.enable();
        this.accomplishmentForm.reset();
        this.accomplishEditInd = -1;
    }
    addVocation(model: Vocation) {
        // upload attachment
        this._uploadService
            .upload({
                url: AppConfig.baseUrl + '/' + NewUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile,
                process: true
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        // this.progress.percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        this.vocation.push(model);
                        this.vocation[this.vocation.length - 1].certificatename = response.data.originalfilename;
                        this.vocation[this.vocation.length - 1].certificatepath = response.data.s3bucketpathname;
                        this.vocationForm.reset();
                        this.addEducation.vocation = this.vocation;
                        // this.addEducationSubject$.next(this.addEducation);
                        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
                        this._alertSevice.success('File Uploaded Succesfully!');
                    }
                },
                (err) => {
                    console.log(err);
                    this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                },
                () => {
                    // console.log('complete');
                }
            );
        // end of attachment upload
    }
    updateVocation(model: Vocation) {
        this.vocation[this.vocationEditInd] = model;
        this.vocationForm.reset();
        this.addEducation.vocation = this.vocation;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.vocationEditInd = -1;
    }
    editVocation(model, index) {
        this.vocationEditInd = index;
        this.vocationForm.patchValue(model);
        this.vocationForm.enable();
        this.vocationButton = false;
        $('#voctionAcdn').click();
    }
    viewVocation(model, index) {
        this.vocationEditInd = index;
        this.vocationForm.patchValue(model);
        this.vocationForm.disable();
        this.vocationEditInd = -1;
        this.vocationButton = true;
        $('#voctionAcdn').click();
    }
    deleteVocation(index) {
        this.vocation.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.vocation = this.vocation;
        // this.addEducationSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.vocationForm.enable();
        this.vocationForm.reset();
        this.vocationEditInd = -1;
        this.vocationButton = false;
    }
    uploadFile(file: File | FileError): void {
        if (!(file instanceof File)) {
            // this.alertError(file);
            return;
        }
        this.uploadedFile = file;
        this.vocationForm.patchValue({ certificatename: file.name });
    }
    private loadDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { isspecialeducation: false },
                    order: 'typedescription ASC'
                },
                NewUrlConfig.EndPoint.Intake.EducationTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { isspecialeducation: true },
                    order: 'typedescription ASC'
                },
                NewUrlConfig.EndPoint.Intake.EducationTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { ishighergrade: false },
                    order: 'displayorder ASC'
                },
                NewUrlConfig.EndPoint.Intake.GradeTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { ishighergrade: true },
                    order: 'displayorder ASC'
                },
                NewUrlConfig.EndPoint.Intake.GradeTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.TestingTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { state: 'MD' },
                    order: 'countyname asc',
                    nolimit: true,
                    method: 'get'
                },
                NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    schoolType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.educationtypekey
                            })
                    ),
                    specialEducation: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.educationtypekey
                            })
                    ),
                    states: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    lastGrade: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gradetypekey
                            })
                    ),
                    currentGrade: result[4].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gradetypekey
                            })
                    ),
                    testingType: result[5].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.testingtypekey
                            })
                    ),
                    counties: result[6].map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyname
                            })
                    )
                };
            })
            .share();
        this.schoolTypeDropdownItems$ = source.pluck('schoolType');
        this.specialEducationDropdownItems$ = source.pluck('specialEducation');
        this.stateDropdownItems$ = source.pluck('states');
        this.lastGradeDropdownItems$ = source.pluck('lastGrade');
        this.currentGradeDropdownItems$ = source.pluck('currentGrade');
        this.testingTypeDropdownItems$ = source.pluck('testingType');
        this.countyDropDownItems$ = source.pluck('counties');
    }
    specialEducation(specialEdu) {
        if (specialEdu === 'true') {
            this.isSpecialEducation = true;
        } else {
            this.isSpecialEducation = false;
        }
    }

    loadSchoolDropdown() {

        const searchkey = this.schoolForm.get('educationname').value;
        const headers = new HttpHeaders().set('no-loader', 'true');
        const workEnv = config.workEnvironment;
        let schoolUrl = '';
        if (workEnv === 'state') {
            schoolUrl = this.baseUrl + '/' + 'School/v1/' + CommonUrlConfig.EndPoint.PERSON.EDUCATION.SCHOOL.GetSchoolUrl;
        } else {
            schoolUrl =  this.baseUrl + '/' + CommonUrlConfig.EndPoint.PERSON.EDUCATION.SCHOOL.GetSchoolUrl;
        }
        this.schoolSearchList$ = this.http.get(schoolUrl + '?filter=' + JSON.stringify({
          where: {
            searchkey: searchkey
          },
          nolimit: true, method: 'get'
        }), { headers: headers }).map((result) => {
          return result;
        });
      }

      sourceSelected(school) {
        this.loadCounty(school.statekey, school.countyid);
        setTimeout(() => {
          this.schoolid = school.schoolid;
          this.schoolForm.patchValue({
            educationtypekey: school.schooltypekey,
            statecode: school.statekey,
            address: school.address,
            zip: school.zipcode
          });
        }, 1000);
      }

      private loadCounty(countystate, countyid) {
        this._commonDropdownsService.getCountyList(countystate).subscribe(result => {
          this.countyDropDownItems = result;
          this.schoolForm.patchValue({
            countyid: countyid
          });
        });
      }
}
