import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakePersonsInvolvedComponent } from './intake-persons-involved.component';

const routes: Routes = [
  {
      path: '',
      component: IntakePersonsInvolvedComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakePersonsInvolvedRoutingModule { }
