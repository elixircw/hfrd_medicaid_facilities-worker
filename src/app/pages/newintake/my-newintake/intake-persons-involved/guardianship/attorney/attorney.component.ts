import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { INTAKE_GUARDIANSHIP } from '../_entities/intake-guardianship-const';
import { InvolvedPersonSearchResponse } from '../../../../../provider-referral/_entities/newintakeModel';
import { GuardianshipSearch } from '../../../_entities/newintakeModel';
import { Subject } from 'rxjs/Subject';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attorney',
    templateUrl: './attorney.component.html',
    styleUrls: ['./attorney.component.scss']
})
export class AttorneyComponent implements OnInit {
    @Input() gurdianPersonSearch$ = new Subject<GuardianshipSearch>();
    addedPersonList = [];
    attorneyForm: FormGroup;
    attorneyList = [];
    index: number;
    personId: string;
    personName: string[] = [];
    searchPerson: InvolvedPersonSearchResponse[] = [];
    searchPersonName: string[] = [];
    isSaveBtnDisabled: boolean;
    constructor(private _formBuider: FormBuilder, private _dataStoreService: DataStoreService) {}
    ngOnInit() {
        this.index = -1;
        this.formIntilizer();
        this._dataStoreService.currentStore.subscribe(stroe => {
            if (stroe['addedPersons'] && stroe['addedPersons'].length) {
                this.addedPersonList = stroe['addedPersons'].map((item) => {
                    this.personName[item.Pid] = item.fullName;
                    return item;
                });
            }
            if (stroe[INTAKE_GUARDIANSHIP.Attorney]) {
                this.attorneyList = stroe[INTAKE_GUARDIANSHIP.Attorney];
                this.endDateValidation(stroe[INTAKE_GUARDIANSHIP.Attorney]);
                // const data = stroe[INTAKE_GUARDIANSHIP.Attorney];
                // if (data && data.length) {
                //     const filterData = data.filter((item) => item.typeofperson === 'search');
                //     filterData.map((item) => {
                //         this.searchPersonName[item.personid] = item.firstname + ' ' + item.lastname;
                //     });
                // }
            }
            if (stroe[INTAKE_GUARDIANSHIP.PersonId]) {
                this.personId = stroe[INTAKE_GUARDIANSHIP.PersonId];
            }
            if (stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails] && stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails].length) {
                this.searchPerson = stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails].map((item) => {
                    this.searchPersonName[item.personid] = item.firstname + ' ' + item.lastname;
                    return item;
                });
                const data = this.searchPerson;
                this.attorneyForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                this.attorneyForm.controls['guadianpersonid'].disable();
            }
        });
    }
    formIntilizer() {
        this.attorneyForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    toggleSearch() {
        const search: GuardianshipSearch = Object.assign({
            personsearch: true,
            storeid: INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails
        });
        this.gurdianPersonSearch$.next(search);
    }
    guardianType() {
        this.attorneyForm.controls['guadianpersonid'].reset();
        this.attorneyForm.controls['startdate'].reset();
        this.attorneyForm.controls['enddate'].reset();
        if (this.attorneyForm.controls['typeofperson'].value === 'search') {
            this.attorneyForm.controls['guadianpersonid'].disable();
        } else {
            this.attorneyForm.controls['guadianpersonid'].enable();
        }
    }
    saveAttorney(attorney) {
        attorney.guardianpersontypekey = 'ATTY';
        attorney.personid = this.personId;
        if (this.index === -1) {
            this.attorneyList.push(attorney);
        } else {
            this.attorneyList[this.index] = attorney;
        }
        this._dataStoreService.setData(
            INTAKE_GUARDIANSHIP.Attorney,
            this.attorneyList
        );
        this.index = -1;
        this.attorneyForm.reset();
        this.attorneyForm.controls['guadianpersonid'].enable();
        this.attorneyForm.controls['typeofperson'].enable();
    }
    editAttorney(modal, index) {
        this.attorneyForm.controls['guadianpersonid'].disable();
        this.attorneyForm.controls['typeofperson'].disable();
        this.attorneyForm.patchValue(modal);
        this.index = index;
        this.isSaveBtnDisabled = false;
    }
    deleteAttorney(index) {
        this.attorneyList.splice(index, 1);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Attorney, this.attorneyList);
        this.endDateValidation(this.attorneyList);
    }
    cancelPerson() {
        this.endDateValidation(this.attorneyList);
        this.attorneyForm.controls['guadianpersonid'].enable();
        this.attorneyForm.controls['typeofperson'].enable();
        this.attorneyForm.reset();
        this.index = -1;
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
