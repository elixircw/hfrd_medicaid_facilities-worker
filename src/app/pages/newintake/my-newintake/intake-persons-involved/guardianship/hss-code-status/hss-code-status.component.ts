import { Component, OnInit } from '@angular/core';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { INTAKE_GUARDIANSHIP } from '../_entities/intake-guardianship-const';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'hss-code-status',
    templateUrl: './hss-code-status.component.html',
    styleUrls: ['./hss-code-status.component.scss']
})
export class HssCodeStatusComponent implements OnInit {
    statusCodeDeatils$: Observable<DropdownModel[]>;
    hssClientForm: FormGroup;
    statusForm: FormGroup;
    notesForm: FormGroup;
    personId: string;
    constructor(private _commonHttpService: CommonHttpService, private _dataStoreService: DataStoreService, private _formBuilder: FormBuilder) {}

    ngOnInit() {
        this.loadDropDown();
        this.formIntilizer();
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[INTAKE_GUARDIANSHIP.HSSClinetID]) {
                this.hssClientForm.patchValue({hhsclientid: item[INTAKE_GUARDIANSHIP.HSSClinetID].hhsclientid});
                this.notesForm.patchValue({notes: item[INTAKE_GUARDIANSHIP.HSSClinetID].notes});
            }
            if (item[INTAKE_GUARDIANSHIP.CodeStatus]) {
                this.statusForm.patchValue(item[INTAKE_GUARDIANSHIP.CodeStatus]);
            }
            if (item[INTAKE_GUARDIANSHIP.CodeStatus] && item[INTAKE_GUARDIANSHIP.CodeStatus].ref_key) {
                this.statusForm.controls['codestatustypekey'].patchValue(item[INTAKE_GUARDIANSHIP.CodeStatus].ref_key);
            }
            if (item[INTAKE_GUARDIANSHIP.PersonId]) {
                this.personId = item[INTAKE_GUARDIANSHIP.PersonId];
            }
        });
    }
    formIntilizer() {
        this.hssClientForm = this._formBuilder.group({
            hhsclientid: ['']
        });
        this.statusForm = this._formBuilder.group({
            codestatustypekey: [''],
            othercodestatus: ['']
        });
        this.notesForm = this._formBuilder.group({
            notes: ['']
        });
    }
    saveHssCode() {
        const statusCode = Object.assign({personid: this.personId}, this.statusForm.value);
        const hssClient = Object.assign({personid: this.personId}, this.hssClientForm.value, this.notesForm.value);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.CodeStatus, statusCode);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.HSSClinetID, hssClient);
    }
    loadDropDown() {
        this.statusCodeDeatils$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        tablename: 'GuardianCodeStatus',
                        teamtypekey: 'AS'
                    },
                    method: 'get'
                },
                'referencetype/gettypes?filter'
            )
            .map(item => {
                return item.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }
}
