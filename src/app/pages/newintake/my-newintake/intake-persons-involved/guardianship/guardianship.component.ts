import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService } from '../../../../../@core/services/alert.service';
import { InvolvedPerson } from '../../../../case-worker/_entities/caseworker.data.model';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../../@core/services/auth.service';
import { AppConstants } from '../../../../../@core/common/constants';
import { GuardianshipSearch } from '../../_entities/newintakeModel';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'guardianship',
  templateUrl: './guardianship.component.html',
  styleUrls: ['./guardianship.component.scss']
})
export class GuardianshipComponent implements OnInit {

  @Input()
  guardianshipFormReset$ = new Subject<boolean>();
  @Input() gurdianPersonSearch$ = new Subject<GuardianshipSearch>();
  token: AppUser;
  guardianshipSections: any[];
  asguardianshipSections = [
    { id: 'guardian-person', name: 'Guardian of Person', isActive: true },
    { id: 'guardian-property', name: 'Guardian of Property', isActive: false },
    { id: 'attorney', name: 'Attorney', isActive: false },
    { id: 'worker', name: 'Worker', isActive: false },
    { id: 'funeral', name: 'Funeral', isActive: false },
    { id: 'hhs-code-status', name: 'HSS Clinet ID & Code Status', isActive: false},
  ];
  selectedGuardianshipSection: any;
  physicianEditInd = -1;

  @Input()
  addedPersons: InvolvedPerson[] = [];

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _authService: AuthService) {
  }

  ngOnInit() {
    this.token = this._authService.getCurrentUser();
    const teamTypeKey = this.token.user.userprofile.teamtypekey;

    if (teamTypeKey === AppConstants.AGENCY.AS) {
      this.guardianshipSections = this.asguardianshipSections;
      if (this.guardianshipSections && this.guardianshipSections.length) {
        this.selectedGuardianshipSection = this.guardianshipSections[0];
      }

    }

    this.guardianshipFormReset$.subscribe((res) => {
      if (res === true) {
        // this.resetForm();
        if (this.guardianshipSections && this.guardianshipSections.length) {
          this.showGuardianshipSection(this.guardianshipSections[0]);
        }
      }
    });
  }
  showGuardianshipSection(guardianshipSection) {
    this.guardianshipSections.forEach(section => section.isActive = false);
    guardianshipSection.isActive = true;
    this.selectedGuardianshipSection = guardianshipSection;
  }

}
