import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardianPropertyComponent } from './guardian-property.component';

describe('GuardianPropertyComponent', () => {
  let component: GuardianPropertyComponent;
  let fixture: ComponentFixture<GuardianPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardianPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardianPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
