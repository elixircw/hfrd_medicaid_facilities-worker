import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../../../@core/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { INTAKE_GUARDIANSHIP } from '../_entities/intake-guardianship-const';
import { GuardianshipSearch, InvolvedPersonSearchResponse } from '../../../_entities/newintakeModel';
import { Subject } from 'rxjs/Subject';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'guardian-property',
    templateUrl: './guardian-property.component.html',
    styleUrls: ['./guardian-property.component.scss']
})
export class GuardianPropertyComponent implements OnInit {
    @Input() gurdianPersonSearch$ = new Subject<GuardianshipSearch>();
    addedPersonList = [];
    propertyGuardianForm: FormGroup;
    propertyGuardianList= [];
    index: number;
    personId: string;
    isSaveBtnDisabled: boolean;
    searchPerson: InvolvedPersonSearchResponse[] = [];
    personName: string[] = [];
    searchPersonName: string[] = [];

    constructor(private _dataStoreService: DataStoreService, private _formBuider: FormBuilder) {}

    ngOnInit() {
        this.index = -1;
        this.formIntilizer();
        this._dataStoreService.currentStore.subscribe(stroe => {
            if (stroe['addedPersons'] && stroe['addedPersons'].length) {
                this.addedPersonList = stroe['addedPersons'].map((item) => {
                    this.personName[item.Pid] = item.fullName;
                    return item;
                });
            }
            if (stroe[INTAKE_GUARDIANSHIP.GuardianProperty]) {
                this.propertyGuardianList = stroe[INTAKE_GUARDIANSHIP.GuardianProperty];
                this.endDateValidation(stroe[INTAKE_GUARDIANSHIP.GuardianProperty]);
            }
            if (stroe[INTAKE_GUARDIANSHIP.PersonId]) {
                this.personId = stroe[INTAKE_GUARDIANSHIP.PersonId];
            }
            if (stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails] && stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails].length) {
                this.searchPerson = stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails].map((item) => {
                    this.searchPersonName[item.personid] = item.firstname + ' ' + item.lastname;
                    return item;
                });
                const data = this.searchPerson;
                this.propertyGuardianForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                this.propertyGuardianForm.controls['guadianpersonid'].disable();
            }
        });
    }
    formIntilizer() {
        this.propertyGuardianForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    toggleSearch() {
        const search: GuardianshipSearch = Object.assign({
            personsearch: true,
            storeid: INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails
        });
        this.gurdianPersonSearch$.next(search);
    }
    guardianType() {
        this.propertyGuardianForm.controls['guadianpersonid'].reset();
        this.propertyGuardianForm.controls['startdate'].reset();
        this.propertyGuardianForm.controls['enddate'].reset();
        if (this.propertyGuardianForm.controls['typeofperson'].value === 'search') {
            this.propertyGuardianForm.controls['guadianpersonid'].disable();
        } else {
            this.propertyGuardianForm.controls['guadianpersonid'].enable();
        }
    }
    saveProperty(property) {
        property.guardianpersontypekey = 'GOPT';
        property.personid = this.personId;
        if (this.index === -1) {
            this.propertyGuardianList.push(property);
        } else {
            this.propertyGuardianList[this.index] = property;
        }
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.GuardianProperty, this.propertyGuardianList);
        this.propertyGuardianForm.reset();
        this.propertyGuardianForm.controls['guadianpersonid'].enable();
        this.propertyGuardianForm.controls['typeofperson'].enable();
    }
    editProperty(modal, i) {
        this.propertyGuardianForm.patchValue(modal);
        this.index = i;
        this.propertyGuardianForm.controls['guadianpersonid'].disable();
        this.propertyGuardianForm.controls['typeofperson'].disable();
        this.propertyGuardianForm.patchValue(modal);
        this.isSaveBtnDisabled = false;
    }
    cancelPerson() {
        this.endDateValidation(this.propertyGuardianList);
        this.propertyGuardianForm.controls['guadianpersonid'].enable();
        this.propertyGuardianForm.controls['typeofperson'].enable();
        this.propertyGuardianForm.reset();
        this.index = -1;
    }
    deleteProperty(i) {
        this.propertyGuardianList.splice(i, 1);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.GuardianProperty, this.propertyGuardianList);
        this.endDateValidation(this.propertyGuardianList);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
