import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { INTAKE_GUARDIANSHIP } from '../_entities/intake-guardianship-const';
import { GuardianshipSearch, InvolvedPersonSearchResponse } from '../../../_entities/newintakeModel';
import { Subject } from 'rxjs/Subject';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'worker',
    templateUrl: './worker.component.html',
    styleUrls: ['./worker.component.scss']
})
export class WorkerComponent implements OnInit {
    @Input() gurdianPersonSearch$ = new Subject<GuardianshipSearch>();
    workerGuardianForm: FormGroup;
    searchPerson: InvolvedPersonSearchResponse[] = [];
    workerGuardianList = [];
    index: number;
    personId: string;
    addedPersonList = [];
    personName: string[] = [];
    searchPersonName: string[] = [];
    searchPersonId: string;
    isSaveBtnDisabled: boolean;
    constructor(private _dataStoreService: DataStoreService, private _formBuider: FormBuilder) {}
    ngOnInit() {
        this.index = -1;
        this.formIntilizer();
        this._dataStoreService.currentStore.subscribe(stroe => {
            if (stroe['addedPersons'] && stroe['addedPersons'].length) {
                this.addedPersonList = stroe['addedPersons'].map((item) => {
                    this.personName[item.Pid] = item.fullName;
                    return item;
                });
            }
        });
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[INTAKE_GUARDIANSHIP.Worker] && item[INTAKE_GUARDIANSHIP.Worker].length) {
                this.workerGuardianList = item[INTAKE_GUARDIANSHIP.Worker];
                this.endDateValidation(item[INTAKE_GUARDIANSHIP.Worker]);
            }
            if (item[INTAKE_GUARDIANSHIP.PersonId]) {
                this.personId = item[INTAKE_GUARDIANSHIP.PersonId];
            }
            if (item[INTAKE_GUARDIANSHIP.GuardianWorkerSearchDetails] && item[INTAKE_GUARDIANSHIP.GuardianWorkerSearchDetails].length) {
                this.searchPerson = item[INTAKE_GUARDIANSHIP.GuardianWorkerSearchDetails].map((res) => {
                    this.searchPersonName[res.personid] = res.firstname + ' ' + res.lastname;
                    return res;
                });
                const data = this.searchPerson;
                this.workerGuardianForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                this.workerGuardianForm.controls['guadianpersonid'].disable();
            }
        });
    }
    formIntilizer() {
        this.workerGuardianForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    guardianType() {
        this.workerGuardianForm.controls['guadianpersonid'].reset();
        this.workerGuardianForm.controls['startdate'].reset();
        this.workerGuardianForm.controls['enddate'].reset();
        if (this.workerGuardianForm.controls['typeofperson'].value === 'search') {
            this.workerGuardianForm.controls['guadianpersonid'].patchValue(this.searchPersonId);
            this.workerGuardianForm.controls['guadianpersonid'].disable();
        } else {
            this.workerGuardianForm.controls['guadianpersonid'].enable();
        }
    }
    toggleSearch() {
        const search: GuardianshipSearch = Object.assign({
            personsearch: true,
            storeid: INTAKE_GUARDIANSHIP.GuardianWorkerSearchDetails
        });
        this.gurdianPersonSearch$.next(search);
    }
    workerSave(worker) {
        worker.guardianpersontypekey = 'WRK';
        worker.personid = this.personId;
        if (this.index === -1) {
            this.workerGuardianList.push(worker);
        } else {
            this.workerGuardianList[this.index] = worker;
        }
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Worker, this.workerGuardianList);
        this.workerGuardianForm.reset();
        this.workerGuardianForm.controls['guadianpersonid'].enable();
        this.workerGuardianForm.controls['typeofperson'].enable();
        this.index = -1;
    }
    cancelWorker() {
        this.endDateValidation(this.workerGuardianList);
        this.workerGuardianForm.controls['guadianpersonid'].enable();
        this.workerGuardianForm.controls['typeofperson'].enable();
        this.workerGuardianForm.reset();
        this.index = -1;
    }
    editWorker(modal, i) {
        this.workerGuardianForm.controls['guadianpersonid'].disable();
        this.workerGuardianForm.controls['typeofperson'].disable();
        this.workerGuardianForm.patchValue(modal);
        this.index = i;
        this.isSaveBtnDisabled = false;
    }
    deleteWorker(index) {
        this.workerGuardianList.splice(index, 1);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Worker, this.workerGuardianList);
        this.endDateValidation(this.workerGuardianList);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
