import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../../../@core/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { INTAKE_GUARDIANSHIP } from '../_entities/intake-guardianship-const';
import { Subject } from 'rxjs/Subject';
import { GuardianshipSearch, InvolvedPersonSearchResponse } from '../../../_entities/newintakeModel';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'guardian-person',
    templateUrl: './guardian-person.component.html',
    styleUrls: ['./guardian-person.component.scss']
})
export class GuardianPersonComponent implements OnInit {
    @Input() gurdianPersonSearch$ = new Subject<GuardianshipSearch>();
    guardianSearchType: string;
    addedPersonList = [];
    personGuardianForm: FormGroup;
    personGuardianList = [];
    searchPerson: InvolvedPersonSearchResponse[] = [];
    index: number;
    personId: string;
    personName: string[] = [];
    searchPersonName: string[] = [];
    searchPersonId: string;
    isSaveBtnDisabled: boolean;
    constructor(private _dataStoreService: DataStoreService, private _formBuider: FormBuilder) {}
    ngOnInit() {
        this.index = -1;
        this.formIntilizer();
        this._dataStoreService.currentStore.subscribe(stroe => {
            if (stroe['addedPersons'] && stroe['addedPersons'].length) {
                this.addedPersonList = stroe['addedPersons'].map((item) => {
                    this.personName[item.Pid] = item.fullName;
                    return item;
                });
            }
                if (stroe[INTAKE_GUARDIANSHIP.GuardianPerson] && stroe[INTAKE_GUARDIANSHIP.GuardianPerson].length) {
                    this.personGuardianList = stroe[INTAKE_GUARDIANSHIP.GuardianPerson];
                    this.endDateValidation(stroe[INTAKE_GUARDIANSHIP.GuardianPerson]);
                }
            if (stroe[INTAKE_GUARDIANSHIP.PersonId]) {
                this.personId = stroe[INTAKE_GUARDIANSHIP.PersonId];
            }
            if (stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails] && stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails].length) {
                this.searchPerson = stroe[INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails].map((item) => {
                    this.searchPersonName[item.personid] = item.firstname + ' ' + item.lastname;
                    return item;
                });
                const data = this.searchPerson;
                this.personGuardianForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                this.personGuardianForm.controls['guadianpersonid'].disable();
            }
        });
    }
    formIntilizer() {
        this.personGuardianForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    guardianType() {
        this.personGuardianForm.controls['guadianpersonid'].reset();
        this.personGuardianForm.controls['startdate'].reset();
        this.personGuardianForm.controls['enddate'].reset();
        if (this.personGuardianForm.controls['typeofperson'].value === 'search') {
            this.personGuardianForm.controls['guadianpersonid'].patchValue(this.searchPersonId);
            this.personGuardianForm.controls['guadianpersonid'].disable();
        } else {
            this.personGuardianForm.controls['guadianpersonid'].enable();
        }
    }
    toggleSearch() {
        const search: GuardianshipSearch = Object.assign({
            personsearch: true,
            storeid: INTAKE_GUARDIANSHIP.GuardianPersonSearchDetails
        });
        this.gurdianPersonSearch$.next(search);
    }
    savePerson(personGuardian) {
        personGuardian.guardianpersontypekey = 'GOP';
        personGuardian.personid = this.personId;
        if (this.index === -1) {
            this.personGuardianList.push(personGuardian);
        } else {
            this.personGuardianList[this.index] = personGuardian;
        }
        this._dataStoreService.setData(
            INTAKE_GUARDIANSHIP.GuardianPerson,
            this.personGuardianList
        );
        this.personGuardianForm.reset();
        this.personGuardianForm.controls['guadianpersonid'].enable();
        this.personGuardianForm.controls['typeofperson'].enable();
        this.index = -1;
        this.searchPersonId = '';
    }
    editPerson(modal, i) {
        this.personGuardianForm.controls['guadianpersonid'].disable();
        this.personGuardianForm.controls['typeofperson'].disable();
        this.personGuardianForm.patchValue(modal);
        this.index = i;
        this.isSaveBtnDisabled = false;
    }
    cancelPerson() {
        this.endDateValidation(this.personGuardianList);
        this.personGuardianForm.controls['guadianpersonid'].enable();
        this.personGuardianForm.controls['typeofperson'].enable();
        this.personGuardianForm.reset();
        this.index = -1;
    }
    deletePerson(i) {
        this.personGuardianList.splice(i, 1);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.GuardianPerson, this.personGuardianList);
        this.endDateValidation(this.personGuardianList);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
