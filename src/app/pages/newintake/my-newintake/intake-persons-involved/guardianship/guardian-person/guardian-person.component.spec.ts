import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardianPersonComponent } from './guardian-person.component';

describe('GuardianPersonComponent', () => {
  let component: GuardianPersonComponent;
  let fixture: ComponentFixture<GuardianPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardianPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardianPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
