import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { INTAKE_GUARDIANSHIP } from '../_entities/intake-guardianship-const';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'funeral',
    templateUrl: './funeral.component.html',
    styleUrls: ['./funeral.component.scss']
})
export class FuneralComponent implements OnInit {
    funeralForm: FormGroup;
    funeralList = [];
    index: number;
    personId: string;
    constructor(
        private _formBuilder: FormBuilder,
        private _dataStoreService: DataStoreService
    ) {}

    ngOnInit() {
        this.index = -1;
        this.formIntilizer();
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[INTAKE_GUARDIANSHIP.Funeral]) {
                this.funeralList = item[INTAKE_GUARDIANSHIP.Funeral];
            }
            if (item[INTAKE_GUARDIANSHIP.PersonId]) {
                this.personId = item[INTAKE_GUARDIANSHIP.PersonId];
            }
        });
    }
    formIntilizer() {
        this.funeralForm = this._formBuilder.group({
            contact: [''],
            contactnumber: [''],
            personid: [''],
            arrangementsdescription: ['']
        });
    }
    saveFuneral(funeral) {
      funeral.personid = this.personId;
      if (this.index === -1) {
          this.funeralList.push(funeral);
      } else {
          this.funeralList[this.index] = funeral;
      }
      this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Funeral, this.funeralList);
      this.index = -1;
      this.funeralForm.reset();
  }
  editFuneral(modal, index) {
      this.funeralForm.patchValue(modal);
      this.index = index;
  }
  deleteFuneral(index) {
      this.funeralList.splice(index, 1);
      this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Funeral, this.funeralList);
  }
}
