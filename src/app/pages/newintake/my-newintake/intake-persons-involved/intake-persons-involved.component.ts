import { Role } from './../../../../@core/common/models/involvedperson.data.model';
import { HttpHeaders } from '@angular/common/http';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { NgxfUploaderService } from 'ngxf-uploader';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { CheckboxModel, DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { ValidationService, DataStoreService, CommonDropdownsService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { AppConfig } from '../../../../app.config';
// tslint:disable-next-line:max-line-length
import { AddressDetails, InvolvedPerson, Work, ContactPerson, EmergencyContactPerson, Guardian, GuardianshipSearch, PersonSupport, InvolvedPersonSearchResponse, PersonPayee } from '../../../newintake/my-newintake/_entities/newintakeModel';
import { NewUrlConfig } from '../../newintake-url.config';
import { Education, Health, InvolvedPersonAlias, PersonInvolved, PersonRole, SuggestAddress, UserProfileImage } from '../_entities/newintakeModel';
import { MyNewintakeConstants, IntakeStoreConstants } from '../my-newintake.constants';
import value from '*.json';
import { IntakeUtils } from '../../../_utils/intake-utils.service';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { AppConstants } from '../../../../@core/common/constants';
import { INTAKE_GUARDIANSHIP } from './guardianship/_entities/intake-guardianship-const';
import { IntakeConfigService } from '../intake-config.service';
import { NgSelectModule } from '@ng-select/ng-select';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-persons-involved',
    templateUrl: './intake-persons-involved.component.html',
    styleUrls: ['./intake-persons-involved.component.scss']
})
export class IntakePersonsInvolvedComponent implements OnInit, AfterViewInit, AfterViewChecked {
    // @Input()
    reviewstatus: string;
    // @Input()
    addedPersons: InvolvedPerson[] = [];
    selectedPerson: InvolvedPerson;
    phoneTypeDescription: string[] = [];
    representativeName: string[] = [];
    checkVPA: any;
    isEVPA: any;
    // @Input()
    // personsList$ = new Subject<InvolvedPerson[]>();
    // tslint:disable-next-line:no-input-rename
    // @Input('changePersonEvent')
    // changePersonEvent$ = new Subject<InvolvedPerson[]>();
    @ViewChild('Dangerousself')
    Dangerousself: any;
    maxDate = new Date();
    // minDate = new Date();
    addEditLabel: string;
    imageChangedEvent: File;
    croppedImage: File;
    progress: { percentage: number } = { percentage: 0 };
    errorValidateAddress = false;
    editAliasForm = false;
    isImageHide: boolean;
    beofreImageCropeHide = false;
    isTab1Valid = false;
    afterImageCropeHide = false;
    addressAnalysis = [];
    suggestedAddress: SuggestAddress[];
    addedPersonsSubject$: Subject<InvolvedPerson[]>;
    addEducationSubject$ = new Subject<Education>();
    addEducationOutputSubject$ = new Subject<Education>();
    educationFormReset$ = new Subject<boolean>();
    addEducation: Education = {};
    addHealthSubject$ = new Subject<Health[]>();
    addHealthOutputSubject$ = new Subject<Health>();
    healthFormReset$ = new Subject<boolean>();
    workFormReset$ = new Subject<boolean>();
    gurdianPersonSearch$ = new Subject<GuardianshipSearch>();
    representativeDetails$ = new Subject<InvolvedPersonSearchResponse>();
    addHealth: any = {};
    addWork: Work = {};
    inFormalSuport: PersonSupport[] = [];
    involvedPersonFormGroup: FormGroup;
    informalForm: FormGroup;
    unkPersonFormGroup: FormGroup;
    formalSupportForm: FormGroup;
    addAliasForm: FormGroup;
    personAddressForm: FormGroup;
    personPhoneForm: FormGroup;
    personEmailForm: FormGroup;
    representativegroup: FormGroup;
    personDobTemp: String;
    selectedState: String;
    mandatoryCountyRequired: boolean;
    addedAliasPersons: InvolvedPersonAlias[] = [];
    totalRecords$: Observable<number>;
    genderDropdownItems$: Observable<DropdownModel[]>;
    stateDropdownItems$: Observable<DropdownModel[]>;
    substanceClass$: Observable<DropdownModel[]>;
    babySubstanceClass$: Observable<DropdownModel[]>;
    substances = [];
    roleDropdownItems$: DropdownModel[];
    roles: any[] = [];
    genders: any[] = [];
    ethinicityDropdownItems$: Observable<DropdownModel[]>;
    livingArrangementDropdownItems$: Observable<DropdownModel[]>;
    primaryLanguageDropdownItems$: Observable<DropdownModel[]>;
    relationShipToRADropdownItems$: DropdownModel[];
    relationShipToRADropdownItems: DropdownModel[];
    maritalDropdownItems$: Observable<DropdownModel[]>;
    religionDropdownItems$: Observable<DropdownModel[]>;
    racetypeDropdownItems$: Observable<DropdownModel[]>;
    addresstypeDropdownItems$: Observable<DropdownModel[]>;
    phoneTypeDropdownItems$: Observable<DropdownModel[]>;
    emailTypeDropdownItems$: Observable<DropdownModel[]>;
    livingSituationTypes$: Observable<DropdownModel[]>;
    licensedfacilityType$: Observable<DropdownModel[]>;
    representativePayeeType$: Observable<DropdownModel[]>;
    languageTypes$: Observable<DropdownModel[]>;
    asLanguageTypes$: Observable<DropdownModel[]>;
    countyDropDownItems$: Observable<DropdownModel[]>;
    alienStatusDropDownItems$: Observable<DropdownModel[]>;
    countyDropdown$: Observable<DropdownModel[]>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    private involvedPerson: InvolvedPerson = new InvolvedPerson();
    private involvedPersonAlias: InvolvedPersonAlias = new InvolvedPersonAlias();
    personAddressInput = [];
    copypersonAddressInput = [];
    phoneNumber = ([] = []);
    phoneNumber$: Observable<Array<any>>;
    emailID = ([] = []);
    emailID$: Observable<Array<any>>;
    token: AppUser;
    ProtectionService: boolean;
    userProfilePicture: UserProfileImage;
    finalImage: File;
    userProfile: string;
    isDefaultPhoto = true;
    isImageLoadFailed = false;
    editImage = true;
    editImagesShow: string;
    addresstypeLabel: string;
    personAddresses$: Observable<AddressDetails[]>;
    reviewStatus: string;
    personRole: PersonRole[] = [];
    raceDescriptionText: string;
    maritalDescription: string;
    roleDetails: any;
    isDjs = false;
    isDjsYouth: boolean;
    headers = new Headers();
    option = new RequestOptions();
    showAddPersonPopup = false;
    showPersonSearch = false;
    currentdate = new Date();
    store: any;
    isAS: boolean;
    isCW: boolean;
    isContactSerach: boolean;
    isContactPersonButton: boolean;
    contactPersonBasicDetails: ContactPerson = new ContactPerson();
    isChild: boolean;
    // D-07269 Start
    hasYouth: boolean;
    // D-07269 End
    isSubstance: boolean;
    substancExposed: boolean;
    unkPersonList: any[] = [];
    agency: string;
    selectedPersonsage: number;
    enableSafeHaven: boolean;
    enableSEN: boolean;
    purposeObj: any;
    dangerAddressRequired = false;
    dangerSelfRequired = false;
    dangerWorkRequired = false;
    dangerAppearance = false;
    dangerSign = false;
    requiredTab = false;
    isEmergencyContact: boolean;
    guardianPersonSearch: boolean;
    serachPersonProfileInfo$ = new Subject<boolean>();
    showGuardianship$ = new Subject<boolean>();
    emergencyContactPersonInfo$ = new Subject<EmergencyContactPerson>();
    contactPersonInfo: EmergencyContactPerson[] = [];
    youthAge: string;
    selectedDob: Date;
    dodPresent: Date;
    ROLE_ITREATION_COUNT = 0;
    RELATION_TO_RA_COUNT = 0;
    dobRequired = true;
    showDobRequired = true;
    // D-07962 Start
    changeRelationShipOnChild: boolean;
    // D-07962 End
    // D-07447 Start
    isAddressUpdate: boolean;
    selectedPersonAddressIndex: number;
    // D-07447 End
    guardianship: Guardian = new Guardian();
    guardianPersonId: string;
    inFormalIndex: number;
    narrative: string;
    isReportedAdult: boolean;
    involvedrepresentative = [];
    representativeindex: number;
    personPayeeDetails: PersonPayee[] = [];
    isSaveBtnDisabled: boolean;
    showQualifiedAlienQuestionValue = false;
    representativePerson: boolean;
    repPayContactPersonTitle: string;
    representative = [];
    representativePersontype$: Observable<DropdownModel[]>;
    cjamsUserList$: Observable<DropdownModel[]>;
    county: string;
    cjamsUserName: string[] = [];
    entityList$: Observable<DropdownModel[]>;
    providerName: string[] = [];
    repPayTyes: string[] = [];
    personNameSuffix$: Observable<any[]>;
    storeMaritalStatus = '';
    isCheckedCollateral = true;
    isAddressReq = false;
    buttonName = 'SHOW';
    isNarrativeVisible = false;
    isnoCopyAddress = true;
    eighteen21Subject$ = new Subject<boolean>();
    addedIdentifiedPersons: any = [];
    reporterPerson: any;
    persontodelete: any;
    constructor(
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _personManageService: GenericService<PersonInvolved>,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _uploadService: NgxfUploaderService,
        private _detect: ChangeDetectorRef,
        private _dataStoreService: DataStoreService,
        private _http: Http,
        private _util: IntakeUtils,
        private _intakeConfig: IntakeConfigService,
        private _dropdownService: CommonDropdownsService
    ) {
        this.store = this._dataStoreService.getCurrentStore();
    }

    ngOnInit() {
        // this.minDate.setFullYear(this.maxDate.getFullYear() - 21);
        // const controlValue = this.involvedPersonFormGroup.get('drugexposednewbornflag').value;
        // this.clearNewbornvalidation(controlValue);

        this.inFormalIndex = -1;
        this.representativeindex = -1;
        this.agency = this._authService.getAgencyName();
        this.isAS = this._authService.isAS();
        this.isCW = this._authService.isCW();
        if (!this.isAS) {
            this.purposeObj = this._dataStoreService.getData(IntakeStoreConstants.purposeSelected);
        }
        this.addedPersons = (this.store[IntakeStoreConstants.addedPersons]) ? this.store[IntakeStoreConstants.addedPersons] : [];
        console.log(this.addedPersons);
        this.unkPersonList = (this.store[IntakeStoreConstants.addedUnkPersons]) ? this.store[IntakeStoreConstants.addedUnkPersons] : [];
        this.reviewstatus = this._dataStoreService.getData(IntakeStoreConstants.reviewstatus);
        // Fix for D-06893 rolled back
        // this.reviewstatus = this._dataStoreService.getData(IntakeStoreConstants.reviewstatus).status;
        this.token = this._authService.getCurrentUser();
        this._dataStoreService.currentStore.subscribe(store => {
            if (store[IntakeStoreConstants.addedPersons]) {
                this.involvedrepresentative = store[IntakeStoreConstants.addedPersons].filter((item) => item.Role !== 'RA');
                if (this.involvedrepresentative && this.involvedrepresentative.length) {
                    this.involvedrepresentative.map((item) => {
                        this.representativeName[item.Pid] = item.Firstname + ' ' + item.Lastname;
                    });
                }
            }
            if (store[INTAKE_GUARDIANSHIP.Attorney]) {
                this.guardianship.ATTY = store[INTAKE_GUARDIANSHIP.Attorney];
            }
            if (store[INTAKE_GUARDIANSHIP.GuardianPerson]) {
                this.guardianship.GOP = store[INTAKE_GUARDIANSHIP.GuardianPerson];
            }
            if (store[INTAKE_GUARDIANSHIP.GuardianProperty]) {
                this.guardianship.GOPT = store[INTAKE_GUARDIANSHIP.GuardianProperty];
            }
            if (store[INTAKE_GUARDIANSHIP.Worker]) {
                this.guardianship.WRK = store[INTAKE_GUARDIANSHIP.Worker];
            }
            if (store[INTAKE_GUARDIANSHIP.HSSClinetID]) {
                this.guardianship.hhsdclient = store[INTAKE_GUARDIANSHIP.HSSClinetID];
            }
            if (store[INTAKE_GUARDIANSHIP.CodeStatus]) {
                this.guardianship.codestatus = store[INTAKE_GUARDIANSHIP.CodeStatus];
            }
            if (store[INTAKE_GUARDIANSHIP.Funeral]) {
                this.guardianship.funeral = store[INTAKE_GUARDIANSHIP.Funeral];
            }
            if (store['countyid']) {
                this.county = store['countyid'];
             }
        });
        const voluntaryPlacementId = this._dataStoreService.getData('voluntryPlacementType');
        if (voluntaryPlacementId !== 'VPA') {
            this.isEVPA = false;
        } else {
            this.isEVPA = true;
        }
        console.log('IS EVPA', this.isEVPA);
        const teamTypeKey = this.token.user.userprofile.teamtypekey;
        this.initiateFormGroup();
        this.initiateUnkFormGroup();
        this.getProviderList();
        this.getCjamsUserList();
        this.loadDropDown();
        this.loadSubstance();
        this.getCommonDropdowns();
        this.dodChange();
        this.dobChange();
        this.roleDetails = this._authService.getCurrentUser();
        this.involvedPersonFormGroup.controls['Firstname'].markAsTouched();
        this.involvedPersonFormGroup.controls['Lastname'].markAsTouched();
        this.involvedPersonFormGroup.controls['Gender'].markAsTouched();
        this.conditionValidation();
        this.getCountyDropdown();
        this.addEducationSubject$.subscribe((education) => {
            this.addEducation = education;
        });
        this.addHealthSubject$.subscribe((health) => {
            this.addHealth = health;
        });
        this._dataStoreService.currentStore.subscribe((store) => {
            const health = MyNewintakeConstants.Intake.PersonsInvolved.Health;
            if (this.addHealth) {
                if (store[health.Physician]) {
                    const jsonData = store[health.Physician];
                    this.addHealth.physician = jsonData ? jsonData : [];
                }

                if (store[health.Insurance]) {
                    const jsonData = store[health.Insurance];
                    this.addHealth.healthInsurance = jsonData ? jsonData : [];
                }
                if (store) {
                    this.ProtectionService = store.ChildProtection;
                }
            }
        });
        if (teamTypeKey === 'DJS') {
            this.isDjs = true;
            this.isDjsYouth = false;
            this.personNameSuffix$ = this._dropdownService.getListByTableID('134');
            if (!this._intakeConfig.getiseditIntake()) {
                (<any>$(':button')).prop('disabled', true);
            }
        } else {
            this.isDjsYouth = true;
        }
        this.serachPersonProfileInfo$.subscribe((item) => {
            if (item) {
                this.addEditLabel = 'Edit';
                (<any>$('#intake-addperson')).modal('show');
                this.isEmergencyContact = false;
                this.guardianPersonSearch = false;
            }
        });
        this.showGuardianship$.subscribe((item) => {
            if (item) {
                (<any>$('#intake-addperson')).modal('show');
                this.isEmergencyContact = false;
            }
        });
        this.emergencyContactPersonInfo$.subscribe((item) => {
            (<any>$('#intake-addperson')).modal('show');
            this.contactPersonInfo.push(item);
            this.isEmergencyContact = false;
        });
        this.gurdianPersonSearch$.subscribe((item) => {
            if (item.personsearch) {
                console.log(item);
                (<any>$('#intake-addperson')).modal('hide');
                this.showPersonSearch = true;
                this.isEmergencyContact = true;
                this.guardianPersonSearch = true;
                this.guardianPersonId = item.storeid;
                (<any>$('#intake-findperson')).modal('show');
            }
        });
        this.representativeDetails$.subscribe((item) => {
            (<any>$('#intake-findperson')).modal('hide');
            this.representative.push(item);
            this.representative.map((res) => {
                this.representativeName[res.personid] = res.firstname + ' ' + res.lastname;
            });
            this.representativegroup.controls['personrepresentativepersonid'].patchValue(item.personid);
            this.representativegroup.controls['personrepresentativepersonid'].disable();
            this.isEmergencyContact = false;
            this.representativePerson = false;
            this.showPersonSearch = false;
            (<any>$('#intake-addperson')).modal('show');
        });

        // const controlValue = this.involvedPersonFormGroup.get('drugexposednewbornflag').value;
        //  this.clearNewbornvalidation(controlValue);

        // D-07269 Start
        this.hasRoleYouth();
        // D-07269 End

        //// D-07447 Start
        this.isAddressUpdate = false;
        this.selectedPersonAddressIndex = 0;
        // D-07447 End
        this.involvedPersonFormGroup.get('primarylanguage').valueChanges.subscribe((data) => {
            if (data === 'Other' && this.isAS) {
                this.involvedPersonFormGroup.get('otherprimarylanguage').setValidators([Validators.required]);
                this.involvedPersonFormGroup.get('otherprimarylanguage').updateValueAndValidity();
            } else {
                this.involvedPersonFormGroup.get('otherprimarylanguage').setValidators([]);
                this.involvedPersonFormGroup.get('otherprimarylanguage').updateValueAndValidity();
            }
        });

        this.involvedPersonFormGroup.get('religionkey').valueChanges.subscribe((data) => {
            if (data === 'OTH' && this.isAS) {
                this.involvedPersonFormGroup.get('otherreligion').setValidators([Validators.required]);
                this.involvedPersonFormGroup.get('otherreligion').updateValueAndValidity();
            } else {
                this.involvedPersonFormGroup.get('otherreligion').setValidators([]);
                this.involvedPersonFormGroup.get('otherreligion').updateValueAndValidity();
            }
        });

        this.involvedPersonFormGroup.controls['citizenalenageflag'].valueChanges.subscribe(data => {
            console.log('Value is ' + data);
            if (data === '0') {
                if (this.showQualifiedAlienQuestion()) {
                    this.showQualifiedAlienQuestionValue = true;
                } else {
                    this.showQualifiedAlienQuestionValue = false;
                }
            } else {
                this.showQualifiedAlienQuestionValue = false;
            }
        });
        const quicksearchPerson = this._dataStoreService.getData(IntakeStoreConstants.PERSON_TO_SEARCH);
        if (quicksearchPerson) {
            this.openFindModal();
        }
        this.addedIdentifiedPersons = this._dataStoreService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
        this.prepareReportedCard();
    }
    prepareReportedCard() {
        const reporterPerson = this._dataStoreService.getData(IntakeStoreConstants.addNarrative);
        if (reporterPerson) {
            this.reporterPerson = Object.assign({}, reporterPerson);
            this.reporterPerson.firstname = reporterPerson.Firstname;
            this.reporterPerson.lastname = reporterPerson.Lastname;
            this.reporterPerson.role = 'Reporter';
            this.reporterPerson.id = new Date().getTime();
        }
    }
    ngAfterViewInit() {
        this.personAddressForm.get('Address2').valueChanges.subscribe((result) => {
            if (result) {
                this.personAddressForm.get('address1').clearValidators();
                this.personAddressForm.get('address1').updateValueAndValidity();
            }
        });
        this.personAddressForm.get('knownDangerAddress').valueChanges.subscribe((result) => {
            if (result === 'yes') {
                this.dangerAddressRequired = true;
                this.personAddressForm.get('knownDangerAddressReason').setValidators([Validators.required]);
                this.personAddressForm.get('knownDangerAddressReason').updateValueAndValidity();
            } else {
                this.dangerAddressRequired = false;
                this.personAddressForm.get('knownDangerAddressReason').clearValidators();
                this.personAddressForm.get('knownDangerAddressReason').updateValueAndValidity();
            }
            if (result === 'no') {
                this.dangerAddressRequired = false;
                this.personAddressForm.get('knownDangerAddressReason').disable();
            } else {
                this.personAddressForm.get('knownDangerAddressReason').enable();
            }
        });
        if (this.isDjs) {
            if (!this._intakeConfig.getiseditIntake()) {
                (<any>$(':button')).prop('disabled', true);
            }
        }
        this.personAddressForm.get('state').valueChanges.subscribe((result) => {
            if (this.isDjs) {
                this.mandatoryCountyRequired = true;
            } else {
                if (result === 'MD') {
                    this.mandatoryCountyRequired = true;
                    this.personAddressForm.get('county').setValidators([Validators.required]);
                    this.personAddressForm.get('county').updateValueAndValidity();
                } else {
                    this.mandatoryCountyRequired = false;
                    this.personAddressForm.get('county').clearValidators();
                    this.personAddressForm.get('county').updateValueAndValidity();
                }
            }
        });
        this.selectedState = 'MD';
    }
    onDob(event) {
        console.log('event...', event);
        this.selectedDob = event.target.value ? new Date(event.target.value) : null;
        this.getAgeInYearsAndMonth(this.currentdate, this.selectedDob);
    }



    getVoluntaryList(involvedperson) {
        const Pid = involvedperson.Pid;
        if (Pid.indexOf('tempid') >= 0) {
            const dob = involvedperson.Dob;
            const age = this.getAge(dob);
            this.checkVPA =

            [
            {'conditionname': 'placement', 'status': 'false', 'description': 'Youth is former CINA or VPA foster child who exited care after 18'},
            {'conditionname': 'county', 'status': 'false', 'description': 'Youth must be applying in the county from which they exited'},
            {'conditionname': 'permanency', 'status': 'false', 'description': 'Youth did not exit care for: Reunification, Adoption, Guardianship, Marriage or Military'},
            {'conditionname': 'age', 'status': (age && age >= 18 && age <= 20) ? 'true' : 'false', 'description': 'Youth between 18-20 years 6 months'}
           ];
        } else {
        const countId = this._dataStoreService.getData('countyId');
        const voluntaryPlacementId = this._dataStoreService.getData('voluntryPlacementType');
        if (voluntaryPlacementId !== 'VPA') {
            return false;
        }
        this._commonHttpService
        .getArrayList(
            new PaginationRequest({
                method: 'get',
                where:  {
                 county : countId,
                personid : involvedperson.Pid
                 },
                limit: 10,
                order: 'desc',
                page: 1,
                count: -1
            }),
            'Intakeservs/getvpadetails?filter'
        )
            .subscribe(
                (result) => {
                   console.log(result);
                   if (result && result.length && result[0].checkvpa) {
                   this.checkVPA = result[0].checkvpa;
                  //  (<any>$('#vpa-detail-popup')).modal('show');
                   }
                }
            );
         //  (<any>$('#voluntary-placement')).modal('show');
        }
    }

    closeVoluntaryModal() {
        (<any>$('#vpa-detail-popup')).modal('hide');
    }
    openVoluntaryModal() {
        (<any>$('#vpa-detail-popup')).modal('show');
    }

    // DJS-018 - Victim's Date of birth may not be known by the worker at the point of adding a person
    dobUnknown(event) {
        if (event.checked) {
            this.isDobRequired(null);
        } else {
            this.dobRequired = true;
            this.involvedPersonFormGroup.get('Dob').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('Dob').updateValueAndValidity();
        }
    }
    isDobRequired(role) {
        if (role && role === 'Youth') {
            this.dobRequired = true;
            this.showDobRequired = false;
            this.involvedPersonFormGroup.get('Dob').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('Dob').updateValueAndValidity();
        } else {
            this.dobRequired = false;
            this.showDobRequired = true;
            this.involvedPersonFormGroup.get('Dob').clearValidators();
            this.involvedPersonFormGroup.get('Dob').updateValueAndValidity();
        }
    }
    onDod(event) {
        console.log('event...', event.target.value);
        this.dodPresent = new Date(event.target.value);
        this.selectedDob = new Date(this.involvedPersonFormGroup.get('Dob').value);
        this.getAgeInYearsAndMonth(this.dodPresent, this.selectedDob);
    }

    // D-07269 Start
    hasRoleYouth() {
        this.hasYouth = false;
        console.log('this.addedPersons' + JSON.stringify(this.addedPersons));
        if (this.addedPersons) {
            for (const addedPerson of this.addedPersons) {
                if (addedPerson.Role === 'Youth') {
                    this.hasYouth = true;
                    break;
                }
            }
        }
    }


    spliceYouth() {
        let i = 0;
        if (this.hasYouth) {
            for (const role of this.roleDropdownItems$) {
                if (role.text === 'Youth' || role.value === 'Youth') {
                    this.roleDropdownItems$.splice(i, 1);
                    break;
                }
                i = i + 1;
            }
        }
    }
    // D-07269 End

    private getAgeInYearsAndMonth(currentDate, youthDob) {
        // const timeDiff = currentDate - youthDob;
        // const youthAge = new Date(timeDiff); // miliseconds from epoch

        // const currentMonth = currentDate.getMonth();
        // const youthMonth = youthDob.getMonth();

        // const monthDiff = currentMonth - youthMonth;

        // let months = (monthDiff >= 0) ? (monthDiff) : (monthDiff + 12);

        // months = (currentDate.getDate() < youthDob.getDate()) ? ((months - 1)) : months;

        // let years = Math.abs(youthAge.getUTCFullYear() - 1970);
        // if (months < 0) {
        //     months = 11;
        //     years = years - 1;
        // }
        // this.youthAge = years.toString() + ' Years ' + ((months > 0) ? months + ' Months' : '');
        // return years.toString() + ' Years ' + ((months > 0) ? months + ' Months' : '');
        if (youthDob) {
            const years = moment().diff(youthDob, 'years', false);
            const totalMonths = moment().diff(youthDob, 'months', false);
            const months = totalMonths - (years * 12);
            console.log(years, (years * 12), totalMonths, months);
            this.youthAge = `${years} Years ${months} month(s)`;
        }
    }
    private getProviderList() {
        this.entityList$ = this._commonHttpService.getArrayList(
            { where: { servicetype: null,
                service: null,
                servicesubtype: null,
                county: null,
                zipcode: null,
                provider: null,
                providerTaxId: null,
                distance: null,
                pagenumber: null,
                pagesize: null,
                count: null,
                childcharacteristic: null
            },
            method: 'post' },
            'provider/search'
            ).map((res) => {
                if (res && res['data'] && res['data'].length) {
                    res['data'].map((item) => {
                        console.log(item);
                        this.providerName[item.providerid] = item.providername;
                    });
                }
                return res['data'].map((item) =>
                new DropdownModel({
                    text: item.providername,
                    value: item.providerid
                }));
            });
            this.entityList$.subscribe((item) => {
                if (item && item.length) {
                    item.map((res) => {
                        this.providerName[res.value] = res.text;
                    });
                }
            });
    }
    private getCjamsUserList() {
        const conuntyid = (this.token.user.userprofile && this.token.user.userprofile.userprofileaddress[0]) ? this.token.user.userprofile.userprofileaddress[0].countyid : null;
        this.cjamsUserList$ = this._commonHttpService.getArrayList(
            { where: { pagenumber: null, pagesize: null, county: conuntyid, }, method: 'post' },
            'People/personrepresentativeworkersearch'
        ).map((res) => {
            if (res && res.length) {
                res.map((item) => {
                    this.cjamsUserName[item.securityusersid] = item.fullname;
                });
            }
            return res.map((item) =>
            new DropdownModel({
                text: item.fullname,
                value: item.securityusersid
            }));
        });
        this.cjamsUserList$.subscribe((item) => {
            if (item && item.length) {
                item.map((res) => {
                    this.cjamsUserName[res.value] = res.text;
                });
            }
        });
    }
    /* Validate DOB and escape invalid date  */
    dateOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode !== 47) && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    ngAfterViewChecked() {
        this._detect.markForCheck();
        this._detect.detectChanges();
    }
    routeToPerson(personid) {
        if (personid && this.isDjs) {
            this._util.redirectToPerson(personid);
        }
    }
    changeAddressType(event) {
        this.addresstypeLabel = event.value;
    }
    addPersonAddress() {
        if (this.personAddressForm.dirty && this.personAddressForm.valid) {
            if (this.personAddressForm.value.startDate) {
                const startDate = this.personAddressForm.value.startDate + '';
                const date1 = moment(new Date(startDate.substr(0, 16)));
                this.personAddressForm.value.startDate = date1.format('MM/DD/YYYY');
            }
            if (this.personAddressForm.value.endDate) {
                const endDate = this.personAddressForm.value.endDate + '';
                const date2 = moment(new Date(endDate.substr(0, 16)));
                this.personAddressForm.value.endDate = date2.format('MM/DD/YYYY');
            } else {
                if (!this.isDjs) {
                    const date2 = moment(new Date());
                    this.personAddressForm.value.endDate = date2.format('MM/DD/YYYY');
                }

            }
            // if (this.personAddressInput.length !== 0) {
            //     this.personAddressInput.map((item, index) => {
            //         if (item.addresstype !== this.personAddressForm.value.addresstype) {
            //             this.personAddressInput.push({ addressid: '', addresstype: this.personAddressForm.value.addresstype, addressDetail: [] });
            //             this.personAddressInput[this.personAddressInput.length - 1].addressDetail.push(this.personAddressForm.value);
            //         } else {
            //             this.personAddressInput[index].addressDetail.push(this.personAddressForm.value);
            //         }
            //     });
            // } else {
            this.personAddressInput.push(this.personAddressForm.value);
            this.personAddressInput[this.personAddressInput.length - 1].addressid = '';
            this.personAddressInput[this.personAddressInput.length - 1].addresstypeLabel = this.addresstypeLabel.split('-')[1];
            this.personAddressInput[this.personAddressInput.length - 1].addresstype = this.addresstypeLabel.split('-')[0];
            this.personAddressInput[this.personAddressInput.length - 1].activeflag = 1;
            // }
            this.personAddressForm.reset();
            this.personAddressForm.get('address1').setValidators(Validators.required);
        } else {
            ControlUtils.validateAllFormFields(this.personAddressForm);
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill mandatory fields!');
        }
    }

    // D-07447 Start
    editSelectedPersonAddress() {
        if (this.personAddressForm.dirty && this.personAddressForm.valid) {
            if (this.personAddressForm.value.startDate) {
                const startDate = this.personAddressForm.value.startDate + '';
                const date1 = moment(new Date(startDate.substr(0, 16)));
                this.personAddressForm.value.startDate = date1.format('MM/DD/YYYY');
            }
            if (this.personAddressForm.value.endDate) {
                const endDate = this.personAddressForm.value.endDate + '';
                const date2 = moment(new Date(endDate.substr(0, 16)));
                this.personAddressForm.value.endDate = date2.format('MM/DD/YYYY');
            }
            this.personAddressInput[this.selectedPersonAddressIndex] = this.personAddressForm.value;
            this.personAddressInput[this.selectedPersonAddressIndex].addressid = '';
            this.personAddressInput[this.selectedPersonAddressIndex].addresstypeLabel = this.addresstypeLabel.split('-')[1];
            this.personAddressInput[this.selectedPersonAddressIndex].addresstype = this.addresstypeLabel.split('-')[0];
            this.personAddressInput[this.selectedPersonAddressIndex].activeflag = 1;
            // }
            this.personAddressForm.reset();
            this.personAddressForm.get('address1').setValidators(Validators.required);
            this.isAddressUpdate = false;
        } else {
            ControlUtils.validateAllFormFields(this.personAddressForm);
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill mandatory fields!');
            this.isAddressUpdate = true;
        }
    }

    editPersonAddress(modal, index) {
        let endDate;
        if (this.personAddressInput[index].endDate !== null) {
            endDate = this.personAddressInput[index].endDate.split('/');
        }

        let startDate;
        if (this.personAddressInput[index].startDate !== null) {
            startDate = this.personAddressInput[index].startDate.split('/');
        }

        this.addresstypeDropdownItems$
            .subscribe(addresstypes => addresstypes.forEach(addresstype => {
                if (addresstype.value === this.personAddressInput[index].addresstype) {
                    this.personAddressForm.setValue({
                        knownDangerAddress: this.personAddressInput[index].knownDangerAddress ? this.personAddressInput[index].knownDangerAddress : '',
                        knownDangerAddressReason: this.personAddressInput[index].knownDangerAddressReason ? this.personAddressInput[index].knownDangerAddressReason : '',
                        addresstype: addresstype.value + '-' + addresstype.text,
                        phoneNo: this.personAddressInput[index].phoneNo ? this.personAddressInput[index].phoneNo : '',
                        address1: this.personAddressInput[index].address1 ? this.personAddressInput[index].address1 : '',
                        Address2: this.personAddressInput[index].Address2 ? this.personAddressInput[index].Address2 : '',
                        zipcode: this.personAddressInput[index].zipcode ? this.personAddressInput[index].zipcode : '',
                        state: this.personAddressInput[index].state ? this.personAddressInput[index].state : '',
                        city: this.personAddressInput[index].city ? this.personAddressInput[index].city : '',
                        county: this.personAddressInput[index].county ? this.personAddressInput[index].county : '',
                        startDate: this.personAddressInput[index].startDate ? new Date(startDate[2], startDate[0] - 1, startDate[1]) : '',
                        endDate: this.personAddressInput[index].endDate ? new Date(endDate[2], endDate[0] - 1, endDate[1]) : ''
                    });
                    this.addresstypeLabel = addresstype.value + '-' + addresstype.text;
                }
            }
            ));
        this.isAddressUpdate = true;
        this.selectedPersonAddressIndex = index;

    }
    // D-07447 End

    deleteAddressInput(modal, index) {
        if (modal.personaddressid) {
            this.personAddressInput.map((item) => {
                if (item.personaddressid === modal.personaddressid) {
                    item.activeflag = 0;
                }
            });
        } else {
            this.personAddressInput.splice(index, 1);
        }
    }
    editPerson(modal, index, text) {
        console.log(modal);
        this.requiredTab = false;
        this.selectedPerson = modal;
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.PersonId, modal.Pid);
        this._dataStoreService.setData(AppConstants.GLOBAL_KEY.SELECTED_PERSON_ID, modal.Pid);
        this.selectedState = 'MD';
        this.mandatoryCountyRequired = true;
        // D-07269 Start
        this.hasRoleYouth();
        if (modal.Role === 'RA') {
            this.isReportedAdult = true;
        } else {
            this.isReportedAdult = false;
        }
        if (this.selectedPerson.Role === 'Youth' || this.selectedPerson.rolekeyDesc === 'Youth') {
            this.getCommonDropdowns();
        }
        if (this.hasYouth && !(this.selectedPerson.Role === 'Youth' || this.selectedPerson.rolekeyDesc === 'Youth')) {
            this.spliceYouth();
        }
        if (!this.hasYouth) {
            this.getCommonDropdowns();
        }
        // D-07269 End

        this.enableSafeHaven = false;
        this.enableSEN = false;
        this.selectedPersonsage = this.calculateAge(this.selectedPerson.Dob ? this.selectedPerson.Dob : null);
        // if (modal.Pid) {
        //     const url = NewUrlConfig.EndPoint.Intake.PersonAddressesUrl + '?filter';
        //     this._commonHttpService
        //         .getArrayList(
        //             new PaginationRequest({
        //                 method: 'get',
        //                 where: { personid: modal.Pid },
        //                 include: { relation: 'Personaddresstype', scope: { fields: ['typedescription'] } }
        //             }),
        //             url
        //         )
        //         .subscribe((result) => {
        //             if (result && result.length !== 0) {
        //                 result.map((item) => {
        //                     item.addresstype = item.personaddresstypekey;
        //                     item.addresstypeLabel = item.Personaddresstype.typedescription;
        //                     item.address1 = item.address;
        //                     item.Address2 = item.address2;
        //                     item.zipcode = item.zipcode;
        //                     item.state = item.state;
        //                     item.city = item.city;
        //                     item.county = item.country;
        //                     item.addressid = item.personaddressid;
        //                 });
        //                 this.personAddressInput = result;
        //             }
        //         });
        // }
        this.isImageHide = false;
        this.afterImageCropeHide = false;
        this.addEditLabel = text;
        this.involvedPerson.index = index;
        this.showAddPersonPopup = true;
        (<any>$('#intake-addperson')).modal('show');
        (<any>$('#profile-click')).click();
        this.personAddressInput = modal.personAddressInput;
        this.personRole = modal.personRole;
        // if (modal.personAddressInput && modal.personAddressInput.length > 0) {
        //     modal.personAddressInput.map((item) => {
        //         this.personAddressInput.push(item);
        //     });
        // }
        if (modal.emailID) {
            this.emailID = modal.emailID;
            this.emailID$ = Observable.of(this.emailID);
        }
        if (modal.phoneNumber) {
            this.phoneNumber = modal.phoneNumber;
            this.phoneNumber$ = Observable.of(this.phoneNumber);
        }
        if (modal.userPhoto && modal.userPhoto.length) {
            this.editImagesShow = modal.userPhoto;
        } else {
            this.editImagesShow = '../../../../../assets/images/ic_silhouette.png';
            this.userProfile = '../../../../../assets/images/ic_silhouette.png';
        }
        if (this.isDjs) {
            this.isDobRequired(modal.Role);
            if (modal.Role === 'Youth') {
                this.isAddressReq = true;
                this.isDjsYouth = true;
            } else {
                this.isAddressReq = false;
                // if (modal.RelationshiptoRA === 'father' || modal.RelationshiptoRA === 'guardian' || modal.RelationshiptoRA === 'mother') {
                //     this.isAddressReq = true;
                // }
                if (!modal.Dob) {
                    this.involvedPersonFormGroup.patchValue({ isdobunknown: true });
                } else {
                    this.involvedPersonFormGroup.patchValue({ isdobunknown: false });
                }
                this.isDjsYouth = false;
            }
        } else {
            this.isDjsYouth = true;
        }
        if (modal.personRole && modal.personRole) {
            const data = modal.personRole.filter((item) => item.rolekey === 'RA');
            this.isContactSerach = data && data.length ? true : false;
        }
        if (['BIOCHILD', 'CHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC'].includes(modal.Role)) {
            this.isChild = true;
            this.filterrelationshipByRole('child');
        } else {
            this.isChild = false;
            this.filterrelationshipByRole('LG');
        }

        this.addEducation.school = modal.school;
        this.addEducation.accomplishment = modal.accomplishment;
        this.addEducation.testing = modal.testing;
        this.addEducation.vocation = modal.vocation;
        this.addEducation.personId = modal.Pid;
        // this.addEducationOutputSubject$.next(this.addEducation);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.addHealth.physician = modal.physicianinfo;
        this.addHealth.healthInsurance = modal.healthinsurance;
        this.addHealth.medication = modal.personmedicationphyscotropic;
        this.addHealth.healthExamination = modal.personhealthexam;
        this.addHealth.medicalcondition = modal.personmedicalcondition;
        this.addHealth.behaviouralhealthinfo = modal.personbehavioralhealth;
        this.addHealth.history = modal.personabusehistory;
        this.addHealth.substanceAbuse = modal.personabusesubstance;
        this.addHealth.providerType = modal.providercw;
        this.addHealth.persondentalinfo = modal.persondentalinfo;
        this.addWork.careergoals = modal.careergoals;
        this.addWork.employer = modal.employer;
        if (modal.personpayee && modal.personpayee.length) {
            this.personPayeeDetails = modal.personpayee;
        }
        if (modal.personsupport && modal.personsupport.length) {
            this.inFormalSuport = modal.personsupport.filter((item) => item.personsupporttypekey === 'informal');
            modal.personsupport.map((item) => {
                if (item.personsupporttypekey === 'formal') {
                    this.formalSupportForm.controls['personsupporttypekey'].patchValue(item.personsupporttypekey === 'formal' ? true : false);
                    this.formalSupportForm.controls['description'].patchValue(item.description);
                }
            });
        }
        if (modal.guardian) {
            this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Worker, modal.guardian.WRK);
            this._dataStoreService.setData(INTAKE_GUARDIANSHIP.GuardianProperty, modal.guardian.GOPT);
            this._dataStoreService.setData(INTAKE_GUARDIANSHIP.GuardianPerson, modal.guardian.GOP);
            this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Attorney, modal.guardian.ATTY);
            this._dataStoreService.setData(INTAKE_GUARDIANSHIP.CodeStatus, modal.guardian.codestatus);
            this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Funeral, modal.guardian.funeral);
            this._dataStoreService.setData(INTAKE_GUARDIANSHIP.HSSClinetID, modal.guardian.hhsdclient);

        }
        // this.addHealthOutputSubject$.next(this.addHealth);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Health.Health, this.addHealth);
        this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Work.Work, this.addWork);
        this.healthFormReset$.next(true);
        this.workFormReset$.next(true);
        this.storeMaritalStatus = modal.maritalstatus;
        this.involvedPersonFormGroup.patchValue({
            Lastname: modal.Lastname,
            Firstname: modal.Firstname,
            middlename: modal.middlename,
            livingsituationkey: modal.livingsituationkey,
            licensedfacilitykey: modal.licensedfacilitykey,
            otherlicensedfacility: modal.otherlicensedfacility,
            livingsituationdesc: modal.livingsituationdesc,
            primarylanguage: (modal.primarylanguage) ? modal.primarylanguage : 'ENG',
            secondarylanguage: modal.secondarylanguage,
            suffix: modal.suffix,
            maritalstatus: modal.maritalstatus,
            Dob: modal.Dob ? new Date(modal.Dob) : '',
            dateofdeath: modal.dateofdeath ? new Date(modal.dateofdeath) : null,
            isapproxdod: modal.isapproxdod === 1 ? true : false,
            isapproxdob: modal.isapproxdob === 1 ? true : false,
            Gender: modal.Gender,
            aliasname: modal.aliasname,
            religionkey: modal.religionkey,
            stateid: modal.stateid,
            tribalassociation: modal.tribalassociation,
            height: modal.height ? modal.height : '',
            weight: modal.weight ? modal.weight : '',
            tattoo: modal.tattoo ? modal.tattoo : '',
            physicalMark: modal.physicalMark ? modal.physicalMark : '',
            Ethnicity: modal.Ethnicity,
            occupation: modal.occupation,
            Role: modal.Role,
            ssn: modal.ssn,
            race: modal.race,
            RelationshiptoRA: modal.RelationshiptoRA,
            Dangerousself: modal.Dangerousself,
            DangerousselfReason: modal.DangerousselfReason,
            Dangerousworker: modal.Dangerousworker,
            DangerousWorkerReason: modal.DangerousWorkerReason,
            Mentealimpair: modal.Mentealimpair,
            MentealimpairDetail: modal.MentealimpairDetail,
            Mentealillness: modal.Mentealillness,
            MentealillnessDetail: modal.MentealillnessDetail,
            Address: modal.Address,
            Address2: modal.Address2,
            Zip: modal.Zip,
            City: modal.City,
            State: '',
            County: '',
            userPhoto: modal.userPhoto,
            Pid: modal.Pid,
            iscollateralcontact: modal.iscollateralcontact,
            ishousehold: modal.ishousehold,
            strengths: modal.strengths,
            needs: modal.needs,
            fetalalcoholspctrmdisordflag: (modal.fetalalcoholspctrmdisordflag === 1) ? true : false,
            drugexposednewbornflag: modal.drugexposednewbornflag,
            probationsearchconductedflag: (modal.probationsearchconductedflag === 1) ? true : false,
            sexoffenderregisteredflag: (modal.sexoffenderregisteredflag === 1) ? true : false,
            safehavenbabyflag: (modal.safehavenbabyflag === 1) ? true : false,
            everbeenadoptedflag: (modal.everbeenadoptedflag === 1) ? true : false,
            otherSubstance: modal.otherSubstance,
            otherreligion: modal.otherreligion,
            otherprimarylanguage: modal.otherprimarylanguage,
            cjamspid: modal.cjamspid,
            county: '',
            citizenalenageflag: modal.citizenalenageflag,
            isqualifiedalien: modal.isqualifiedalien,
            alienstatustypekey: modal.alienstatustypekey,
            alienregistrationtext: modal.alienregistrationtext,
            verificationremarks: modal.verificationremarks,
        });
        this.checkChildStatus('drugexposednewbornflag');
        this.resetRoleTabRadioBtn(modal.iscollateralcontact);
        if (modal.substances && modal.substances.length) {
            const substanc = modal.substances.map((item) => {
                return item.subtancekey;
            });
            this.involvedPersonFormGroup.controls['substanceClass'].patchValue(substanc);
        }
        this.setFormValues();
        this.changeRelationShipOnChild = false; // D-07962
        setTimeout(() => {
            this.personRole.forEach((item, i) => {
                this.relationShipToRO({ value: item.rolekey }, i);
                this.changeRelationShipOnChild = true; // D-07962
            });
        }, 100);
        if (this.agency === 'DJS') {
            this.selectedDob = modal.Dob ? new Date(modal.Dob) : null;
            if (modal.dateofdeath) {
                this.dodPresent = new Date(modal.dateofdeath);
                this.getAgeInYearsAndMonth(this.dodPresent, this.selectedDob);
            } else {
                this.getAgeInYearsAndMonth(this.currentdate, this.selectedDob);
            }
        }
    }

    callSuggestAddress() {
        // if (this.involvedPersonFormGroup.value.Address.length > 2) {
        //     this.suggestAddress();
        // }
    }
    contactPersonDetails(personid: string, title: string) {
        this.repPayContactPersonTitle = title;
        this._commonHttpService.getSingle({},
            'People/getpersonbasicdetails/' + personid
        ).subscribe((res) => {
            this.contactPersonBasicDetails = res;
            (<any>$('#intake-addperson')).modal('hide');
            (<any>$('#contact-person-details')).modal('show');
        });
    }
    closeContactPerson() {
        (<any>$('#contact-person-details')).modal('hide');
        (<any>$('#intake-addperson')).modal('show');
    }
    changeLiving() {
        this.involvedPersonFormGroup.controls['licensedfacilitykey'].reset();
    }
    getSelectedPerson(modal) {
        // console.log(modal);
        this.involvedPersonFormGroup.patchValue({
            Lastname: modal.lastname,
            Firstname: modal.firstname,
            Dob: modal.dob,
            Gender: modal.gendertypekey,
            Address: modal.primaryaddress,
            Address2: '',
            Zip: '',
            City: '',
            State: '',
            County: '',
            Role: modal.personrolesubtype
        });
    }
    getCountyDropdown() {
        this.countyDropdown$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.SdmCountyListUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }
    initiateFormGroup() {
        this.involvedPersonFormGroup = this._formBuilder.group({
            Lastname: ['', Validators.required],
            Firstname: ['', Validators.required],
            middlename: [''],
            suffix: [''],
            Dob: [null, Validators.required], // Fix for 'Invalid Date' when adding new Person
            Gender: ['', Validators.required],
            religionkey: [''],
            maritalstatus: [''],
            Dangerous: [''],
            dangerAddress: [''],
            race: [''],
            Dcn: [],
            ssn: [''],
            dateofdeath: [null],
            isapproxdod: [''],
            isapproxdob: [''],
            safehavenbabyflag: [null],
            everbeenadoptedflag: [null],
            isdobunknown: [''],
            age: [''],
            Ethnicity: [''],
            occupation: [''],
            stateid: [''],
            aliasname: [''],
            potentialSOR: [''],
            eDLHistory: [''],
            dMH: [''],
            Race: [''],
            Address: [''],
            Address2: [''],
            Zip: [''],
            City: [''],
            State: [''],
            County: [''],
            county: [''],
            DangerousWorkerReason: [''],
            DangerousAddressReason: [''],
            tribalassociation: [''],
            height: [''],
            weight: [''],
            tattoo: [''],
            primarylanguage: ['ENG'],
            secondarylanguage: [''],
            physicalMark: [''],
            Dangerousself: [''],
            DangerousselfReason: [''],
            Dangerousworker: ['', [Validators.required]],
            Mentealimpair: ['', [Validators.required]],
            MentealimpairDetail: [''],
            Mentealillness: [''],
            MentealillnessDetail: [''],
            userPhoto: [''],
            Pid: [''],
            mdm_id: [null],
            iscollateralcontact: [false],
            ishousehold: ['', [!this._authService.isDJS() ? Validators.required : Validators.pattern('')]],
            drugexposednewbornflag: [false],
            fetalalcoholspctrmdisordflag: [false],
            sexoffenderregisteredflag: [false],
            probationsearchconductedflag: [false],
            otherSubstance: [''],
            substanceClass: [''],
            physician: [''],
            healthInsurance: [''],
            medication: [''],
            persondentalinfo: [''],
            needs: [''],
            strengths: [''],
            livingsituationkey: [''],
            licensedfacilitykey: [''],
            otherlicensedfacility: [''],
            livingsituationdesc: [''],
            source: [''],
            otherreligion: [''],
            otherprimarylanguage: [''],
            cjamspid: [null],
            citizenalenageflag: [null],
            isqualifiedalien: [null],
            alienstatustypekey: [null],
            alienregistrationtext: [null],
            verificationremarks: [null]
            // issafehaven: [false]
        });
        this.involvedPersonFormGroup.addControl('personRole', this._formBuilder.array([this.createFormGroup(false)]));

        this.addAliasForm = this._formBuilder.group({
            AliasFirstName: [''],
            AliasLastName: ['']
        });
        this.personAddressForm = this._formBuilder.group({
            knownDangerAddress: [''],
            knownDangerAddressReason: [''],
            addresstype: ['', Validators.required],
            phoneNo: [''],
            address1: ['', Validators.required],
            Address2: [''], // DJS-009 : Removed 'Validators.required' - Address line 2 should not be a mandatory field
            zipcode: ['', [Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]],
            state: ['MD', Validators.required],
            city: new FormControl('', [
                Validators.required,
                Validators.pattern('^[a-zA-Z ]*$')
            ]),
            county: new FormControl('', [
                Validators.required,
                Validators.pattern('^[a-zA-Z \\\'.]*$') // D-08377
            ]),
            startDate: [''],
            endDate: ['']
        });

        if (this.agency === 'DJS') {

            this.personAddressForm.get('knownDangerAddress').clearValidators();
            this.personAddressForm.get('knownDangerAddressReason').clearValidators();
            this.personAddressForm.get('knownDangerAddress').updateValueAndValidity();
            this.personAddressForm.get('knownDangerAddressReason').updateValueAndValidity();

            this.involvedPersonFormGroup.get('race').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('race').updateValueAndValidity();
            this.involvedPersonFormGroup.get('Ethnicity').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('Ethnicity').updateValueAndValidity();
        }
        this.personPhoneForm = this._formBuilder.group({
            contactnumber: [''],
            // ismobile: ['', Validators.required],
            contacttype: ['', Validators.required]
        });
        this.personEmailForm = this._formBuilder.group({
            // EmailID: ['', [ValidationService.mailFormat, Validators.required]],
            // EmailType: ['', Validators.required]
            EmailID: ['', [ValidationService.mailFormat]],
            EmailType: ['']
        });
        this.informalForm = this._formBuilder.group({
            supportername: ['', Validators.required],
            description: ['']
        });
        this.formalSupportForm = this._formBuilder.group({
            personsupporttypekey: [''],
            description: ['']
        });
        this.representativegroup = this._formBuilder.group({
            personid: [''],
            personrepresentativeworkerid: [null],
            personrepresentativepersonid: [null],
            entityid: [null],
            persontypekey: [''],
            representativetypekey: [''],
            startdate: [null],
            enddate: [null]
        });
    }

    // clearNewbornvalidation(event: any) {
    //     if (event) {
    //         this.involvedPersonFormGroup.get('Dangerousself').clearValidators();
    //         this.involvedPersonFormGroup.get('Dangerousself').updateValueAndValidity();
    //         this.involvedPersonFormGroup.get('Mentealillness').clearValidators();
    //         this.involvedPersonFormGroup.get('Mentealillness').updateValueAndValidity();
    //         this.substancExposed = false;
    //     } else {
    //         this.involvedPersonFormGroup.get('Dangerousself').setValidators([Validators.required]);
    //         this.involvedPersonFormGroup.get('Dangerousself').updateValueAndValidity();
    //         this.involvedPersonFormGroup.get('Mentealillness').setValidators([Validators.required]);
    //         this.involvedPersonFormGroup.get('Mentealillness').updateValueAndValidity();
    //         this.substancExposed = true;




    //     }

    // }




    initiateUnkFormGroup() {
        this.unkPersonFormGroup = this._formBuilder.group({
            role: [''],
            comment: ['']
        });
    }
    validationAddressCall() {
        this.validateAddress();
        this.validateAddressResponse();
    }
    validateAddress() {
        const addressInput = {
            street: this.personAddressForm.value.address1 ? this.personAddressForm.value.address1 : '',
            street2: this.personAddressForm.value.Address2 ? this.personAddressForm.value.Address2 : '',
            city: this.personAddressForm.value.city ? this.personAddressForm.value.city : '',
            state: this.personAddressForm.value.state ? this.personAddressForm.value.state : '',
            zipcode: this.personAddressForm.value.zipCode ? this.involvedPersonFormGroup.value.zipCode : '',
            match: undefined
        };

        this.addressAnalysis = [];
        this._commonHttpService
            .getSingle(
                {
                    method: 'post',
                    where: addressInput
                },
                NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
            )
            .subscribe(
                (result) => {
                    if (result.isValidAddress === false) {
                        this.errorValidateAddress = true;
                    } else {
                        this.errorValidateAddress = false;
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    validateAddressResponse() {
        const addressInput = {
            street: this.involvedPersonFormGroup.value.Address ? this.involvedPersonFormGroup.value.Address : '',
            street2: this.involvedPersonFormGroup.value.Address2 ? this.involvedPersonFormGroup.value.Address2 : '',
            city: this.involvedPersonFormGroup.value.City ? this.involvedPersonFormGroup.value.City : '',
            state: this.involvedPersonFormGroup.value.State ? this.involvedPersonFormGroup.value.State : '',
            zipcode: this.involvedPersonFormGroup.value.Zip ? this.involvedPersonFormGroup.value.Zip : '',
            match: 'invalid'
        };

        this.addressAnalysis = [];
        this._commonHttpService
            .getSingle(
                {
                    method: 'post',
                    where: addressInput
                },
                NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
            )
            .subscribe(
                (result) => {
                    if (result[0].analysis) {
                        this.involvedPersonFormGroup.patchValue({
                            Zip: result[0].components.zipcode ? result[0].components.zipcode : '',
                            County: result[0].metadata.countyName ? result[0].metadata.countyName : '',
                            county: result[0].metadata.countyName ? result[0].metadata.countyName : ''
                        });

                        if (result[0].analysis.dpvMatchCode) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvMatchCode
                            });
                        }
                        if (result[0].analysis.dpvFootnotes) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvFootnotes
                            });
                        }
                        if (result[0].analysis.dpvCmra) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvCmra
                            });
                        }
                        if (result[0].analysis.dpvVacant) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvVacant
                            });
                        }
                        if (result[0].analysis.active) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.active
                            });
                        }
                        if (result[0].analysis.ewsMatch) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.ewsMatch
                            });
                        }
                        if (result[0].analysis.lacslinkCode) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.lacslinkCode
                            });
                        }
                        if (result[0].analysis.lacslinkIndicator) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.lacslinkIndicator
                            });
                        }
                        if (result[0].analysis.suitelinkMatch) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.suitelinkMatch
                            });
                        }
                        if (result[0].analysis.footnotes) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.footnotes
                            });
                        }
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }
    languageType() {
        if (this.involvedPersonFormGroup.controls['primarylanguage'].value && this.involvedPersonFormGroup.controls['secondarylanguage'].value) {
            if (this.involvedPersonFormGroup.controls['primarylanguage'].value === this.involvedPersonFormGroup.controls['secondarylanguage'].value) {
                this._alertService.error('Please select two different languages');
                this.involvedPersonFormGroup.controls['primarylanguage'].reset();
                this.involvedPersonFormGroup.controls['secondarylanguage'].reset();
            }
        }
    }
    private loadDropDown() {
        const actortypeUrl = this.isDjs ? NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter' : NewUrlConfig.EndPoint.Intake.UserActorTypeUrl + '?filter';
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.GenderTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.LivingArrangementTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1, teamtypekey: this.isAS ? 'AS' : null },
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.LanguageTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
            ),
            // this._commonHttpService.getArrayList(
            //     {
            //         // where: { activeflag: 1, teamtypekey: this._authService.getAgencyName() },
            //         // method: 'get',
            //         nolimit: true,
            //         order: 'description'
            //     },
            //     NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl // + '?filter'
            // ),
            // this._commonHttpService.getArrayList(
            //     {
            //         where: {
            //             activeflag: 1,
            //             datypeid: (this._authService.isCW() && this.purposeObj && this.purposeObj.code) ? this.purposeObj.value : null
            //         },
            //         method: 'get',
            //         nolimit: true
            //     },
            //     actortypeUrl
            // ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.MaritalStatusUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription'
                },
                'admin/religiontype/list?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    activeflag: 1,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    activeflag: 1,
                    order: 'typedescription'
                },
                NewUrlConfig.EndPoint.Intake.AddressTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    order: 'typedescription asc',
                    method: 'get',
                    nolimit: true,
                    filter: {}
                },
                NewUrlConfig.EndPoint.Intake.PhoneTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    filter: {}
                },
                NewUrlConfig.EndPoint.Intake.EmailTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 19,
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 20,
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 27,
                        teamtypekey: null
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 27,
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'countyname asc',
                },
                NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList({
                nolimit: true,
                where: { referencetypeid: 89 }, method: 'get'
            },
                'referencevalues?filter'),
                this._commonHttpService.getArrayList({
                    nolimit: true,
                    where: {
                        tablename: 'RepresentativePayeeType',
                        teamtypekey: 'AS'
                    }, method: 'get'
                }, 'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList({
                where: { tablename: 'RepresentativeType', teamtypekey: 'AS'}, method: 'get'
            }, 'referencetype/gettypes?filter'
        )
        ])
            .map((result) => {
                return {
                    ethinicities: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.ethnicgrouptypekey
                            })
                    ),
                    genders: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gendertypekey
                            })
                    ),
                    livingArrangements: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.livingarrangementtypekey
                            })
                    ),
                    primaryLanguages: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.languagetypename,
                                value: res.languagetypeid
                            })
                    ),
                    races: result[4].map(
                        (res) =>
                            new CheckboxModel({
                                text: res.typedescription,
                                value: res.racetypekey,
                                isSelected: false
                            })
                    ),
                    // relationShipToRAs: result[5].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.description,
                    //             value: res.relationshiptypekey
                    //         })
                    // ),
                    // roles: result[6].map(
                    //     (res) => {
                    //         return {
                    //             text: res.typedescription,
                    //             value: res.actortype,
                    //             rolegrp: res.rolegroup
                    //         };
                    //     }
                    // ),
                    states: result[5].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    maritalstatus: result[6].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.maritalstatustypekey
                            })
                    ),
                    religionkey: result[7].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.religiontypekey
                            })
                    ),
                    racetype: result[8].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.racetypekey
                            })
                    ),
                    addresstype: result[9].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personaddresstypekey
                            })
                    ),
                    phonetype: result[10].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personphonetypekey
                            })
                    ),
                    emailtype: result[11].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personemailtypekey
                            })
                    ),
                    livingsituation: result[12].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    licensedfacility: result[13].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    languagetypes: result[14].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    aslanguagetypes: result[15].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    counties: result[16].map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyname
                            })
                    ),
                    alienStatus: result[17].map(
                        (res) => new DropdownModel({
                            text: res.value_text,
                            value: res.ref_key
                        })
                    ),
                    representativePayee: result[18].map(
                        (res) => new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                    ),
                     personsType: result[19].map(
                        (res) => new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                    )
                };
            })
            .share();
        this.ethinicityDropdownItems$ = source.pluck('ethinicities');
        this.genderDropdownItems$ = source.pluck('genders');
        this.livingArrangementDropdownItems$ = source.pluck('livingArrangements');
        this.primaryLanguageDropdownItems$ = source.pluck('primaryLanguages');
        // this.relationShipToRADropdownItems$ = source.pluck('relationShipToRAs');
        // this.roleDropdownItems$ = source.pluck('roles');
        this.stateDropdownItems$ = source.pluck('states');
        this.maritalDropdownItems$ = source.pluck('maritalstatus');
        this.religionDropdownItems$ = source.pluck('religionkey');
        this.racetypeDropdownItems$ = source.pluck('racetype');
        this.addresstypeDropdownItems$ = source.pluck('addresstype');
        this.phoneTypeDropdownItems$ = source.pluck('phonetype');
        this.emailTypeDropdownItems$ = source.pluck('emailtype');
        this.livingSituationTypes$ = source.pluck('livingsituation');
        this.licensedfacilityType$ = source.pluck('licensedfacility');
        this.languageTypes$ = source.pluck('languagetypes');
        this.asLanguageTypes$ = source.pluck('aslanguagetypes');
        this.countyDropDownItems$ = source.pluck('counties');
        this.alienStatusDropDownItems$ = source.pluck('alienStatus');
        this.representativePayeeType$ = source.pluck('representativePayee');
        this.representativePersontype$ = source.pluck('personsType');
        // this.roleDropdownItems$.subscribe((roleList) => {
        //     for (const role of roleList) {
        //         this.roles[role.value] = role.text;
        //     }
        //     // console.log('Roles', this.roles);
        // });

        this.genderDropdownItems$.subscribe((genderList) => {
            for (const gender of genderList) {
                this.genders[gender.value] = gender.text;
            }
        });
        this.phoneTypeDropdownItems$.subscribe((res) => {
            if (res) {
                res.forEach(type => {
                    this.phoneTypeDescription[type.value] = type.text;
                });
            }
        });
        this.representativePayeeType$.subscribe((res) => {
            if (res) {
                res.map((item) => {
                    this.repPayTyes[item.value] = item.text;
                });
            }
        });
        this.selectedState = 'MD';
        this.mandatoryCountyRequired = true;
    }

    getRoleList() {
        console.log('Role itration count', this.ROLE_ITREATION_COUNT);
        const actortypeUrl = NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter';
        this._commonHttpService.getArrayList(
            {
                where: {
                    activeflag: 1,
                    datypeid: (this._authService.isCW() && this.purposeObj && this.purposeObj.code !== 'ROACPS') ? this.purposeObj.value : null,
                    teamtypekey: this._authService.getAgencyName()
                },
                method: 'get',
                nolimit: true
            },
            actortypeUrl
        )
            .subscribe(data => {

                if (data && data.length) {
                    this.setRoleList(data);
                } else {
                    if (this.ROLE_ITREATION_COUNT < AppConstants.ITRETOR.POLING_COUNT) {
                        this.ROLE_ITREATION_COUNT++;
                        this.getRoleList();
                    }
                }
            });
    }

    setRoleList(data) {
        for (const role of data) {
            this.roles[role.actortype] = role.typedescription;
        }
        let roleList = [];
        if (this.purposeObj && this.purposeObj.code === 'CHILD') {
            roleList = ['CHILD', 'ADV', 'AM', 'AV', 'CASAWK', 'CDCP', 'COURTTERAPST', 'FN', 'LE', 'LG', 'LR', 'MP', 'MHP', 'Other'
                , 'OTHERCHILD', 'PROVIDER', 'RELATIVE', 'RFS', 'SSP', 'TES'];
        } else {
            roleList = ['ADV', 'CASAWK', 'CDCP', 'CHILD', 'COURTTERAPST', 'FN', 'LE', 'LR', 'MP', 'MHP', 'Other', 'LG'
                , 'PROVIDER', 'RELATIVE', 'RFS', 'SSP'];
        }
        let roles;
        if (this.agency === 'CW') {
            roles = data.filter(item => roleList.includes(item.actortype));
        } else {
            roles = data;
        }
        this.roleDropdownItems$ = roles.map(res => {
            return {
                text: res.typedescription,
                value: res.actortype,
                rolegrp: res.rolegroup
            };
        });
        this._dataStoreService.setData('role', this.roleDropdownItems$);
    }

    getRelationList() {
        console.log('Realtion itration count', this.RELATION_TO_RA_COUNT);
        this._commonHttpService.getArrayList(
            {
                // where: { activeflag: 1, teamtypekey: this._authService.getAgencyName() },
                method: 'get',
                nolimit: true,
                order: 'description ASC'
            },
            NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl + '?filter'
        ).subscribe(data => {
            if (data && data.length) {
                this.setRelationList(data);
            } else {
                if (this.RELATION_TO_RA_COUNT < AppConstants.ITRETOR.POLING_COUNT) {
                    this.RELATION_TO_RA_COUNT++;
                    this.getRelationList();
                }
            }
        });
    }

    setRelationList(data) {
        for (const role of data) {
            this.roles[role.relationshiptypekey] = role.description;
        }
        if (data && data.length) {
            this.relationShipToRADropdownItems$ = data.map(res => {
                return new DropdownModel({
                    text: res.description,
                    value: res.relationshiptypekey
                });
            });
            this._dataStoreService.setData('relation', this.relationShipToRADropdownItems$);
            this.relationShipToRADropdownItems = this._dataStoreService.getData('relation');
        }
    }
    getCommonDropdowns() {

        this.getRoleList();
        this.getRelationList();

    }
    suggestAddress() {
        this._commonHttpService
            .getSingle(
                {
                    method: 'post',
                    where: {
                        prefix: this.involvedPersonFormGroup.value.Address,
                        cityFilter: '',
                        stateFilter: '',
                        geolocate: '',
                        geolocate_precision: '',
                        prefer_ratio: ''
                    }
                },
                NewUrlConfig.EndPoint.Intake.SuggestAddressUrl
            )
            .subscribe(
                (result) => {
                    this.suggestedAddress = result;
                },
                (error) => {
                    console.log(error);
                }
            );
    }
    selectedAddress(model) {
        this.suggestedAddress = [];
        this.involvedPersonFormGroup.patchValue({
            Address: model.streetLine ? model.streetLine : '',
            City: model.city ? model.city : '',
            State: model.state ? model.state : ''
        });
        this.validateAddressResponse();
        this.validateAddress();
    }

    viewAddPerson(person) {
        // this.getPersonLatestDetails(person.personid);
        this.getPersonLatestDetails(person);
    }

    searchAddPerson(involvedPerson) {
        // console.log(involvedPerson + 'involvedPerson');
        const invDob = involvedPerson.Dob + '';
        const invDate = moment(new Date(invDob.substr(0, 16)));
        const invDobFormatted = invDate.format('MM/DD/YYYY');
        const dDupPerson = this.addedPersons.filter(
            (person) => person.Firstname === involvedPerson.Firstname && person.Lastname === involvedPerson.Lastname && person.DobFormatted === invDobFormatted
        );
        if (this.isCW) {
            involvedPerson.personRole.map((item, index) => {
                item.isprimary = (index === 0) ? 'true' : 'false';
                if (index === 0) {
                    involvedPerson.Role = item.rolekey;
                    involvedPerson.RelationshiptoRA = item.relationshiptorakey;
                }
            });
        }
        if (dDupPerson.length === 0) {
            involvedPerson.Dob = moment(involvedPerson.Dob).format('MM/DD/YYYY');
            involvedPerson.dateofdeath = involvedPerson.dateofdeath ? moment(involvedPerson.dateofdeath).format('MM/DD/YYYY') : null;
            console.log(involvedPerson);
            this.addedPersons.push(involvedPerson);
            // this.addedPersons.map((item) => {
            //     if (item.Dob) {
            //         const dob = item.Dob + '';
            //         const date = moment(new Date(dob.substr(0, 16)));
            //         item.DobFormatted = date.format('MM/DD/YYYY');
            //     } else {
            //         item.DobFormatted = 'N/A';
            //     }
            //     if (item.dateofdeath) {
            //         const dod = item.dateofdeath + '';
            //         const date = moment(new Date(dod.substr(0, 16)));
            //         item.DodFormatted = date.format('MM/DD/YYYY');
            //     } else {
            //         item.DodFormatted = null;
            //     }
            // });
            // let addressDetail = [];
            // addressDetail = involvedPerson.personAddressInput;
            //     {
            //     addresstype: 'P',
            //     addresstypeLabel: 'Primary Address',
            //     address1: involvedPerson.address1,
            //     zipcode: involvedPerson.zipCode,
            //     state: involvedPerson.state,
            //     city: involvedPerson.city,
            //     county: involvedPerson.county,
            //     addressid: ''
            // });
            // const personAddressInput = [];
            // personAddressInput.push({ addressDetail: addressDetail });
            this.addedPersons[this.addedPersons.length - 1].phoneNumber = [];
            this.addedPersons[this.addedPersons.length - 1].Pid = involvedPerson.Pid ? involvedPerson.Pid : '';
            this.addedPersons[this.addedPersons.length - 1].Gender = involvedPerson.Gender ? involvedPerson.Gender : '';
            this.addedPersons[this.addedPersons.length - 1].emailID = [];
            // this.addedPersons[this.addedPersons.length - 1].personAddressInput = [];
            // this.addedPersons[this.addedPersons.length - 1].personAddressInput = Object.assign(addressDetail);
            this.addedPersons[this.addedPersons.length - 1].fullName = this.addedPersons[this.addedPersons.length - 1].Firstname + ' ' + this.addedPersons[this.addedPersons.length - 1].Lastname;
            // this.addedPersons[this.addedPersons.length - 1].userPhoto = involvedPerson.userPhoto ? involvedPerson.userPhoto : '';
            if (this.isImageHide) {
                this.addedPersons[this.addedPersons.length - 1].userPhoto = this.userProfilePicture ? this.userProfilePicture.s3bucketpathname : '';
            } else {
                this.addedPersons[this.addedPersons.length - 1].userPhoto = involvedPerson.userPhoto ? involvedPerson.userPhoto : '';
            }
            if (this.addedPersons[this.addedPersons.length - 1].personAddressInput && this.addedPersons[this.addedPersons.length - 1].personAddressInput.length > 0) {
                this.addedPersons[this.addedPersons.length - 1].fullAddress =
                    this.isEmptyOrNull(this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].address1) +
                    ' ' +
                    this.isEmptyOrNull(this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].Address2) +
                    ' ' +
                    this.isEmptyOrNull(this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].city) +
                    ' ' +
                    this.isEmptyOrNull(this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].state) +
                    ' ' +
                    this.isEmptyOrNull(this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].county) +
                    ' ' +
                    this.isEmptyOrNull(this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].zipcode);

                this.addedPersons[this.addedPersons.length - 1].Address = this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].address1;
                this.addedPersons[this.addedPersons.length - 1].Address2 = this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].Address2;
                this.addedPersons[this.addedPersons.length - 1].City = this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].city;
                this.addedPersons[this.addedPersons.length - 1].State = this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].state;
                this.addedPersons[this.addedPersons.length - 1].County = this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].county;
                this.addedPersons[this.addedPersons.length - 1].county = this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].county;
                this.addedPersons[this.addedPersons.length - 1].Zip = this.addedPersons[this.addedPersons.length - 1].personAddressInput[0].zipcode;
            }
            // this.personsList$.next(this.addedPersons);
            this._dataStoreService.setData(IntakeStoreConstants.addedPersons, this.addedPersons);
            let rcPerson = [];
            rcPerson = this.addedPersons.filter((person) => person.Role === 'RC');
            rcPerson.map((res) => {
                if (res.dateofdeath) {
                    this._dataStoreService.setData(IntakeStoreConstants.childfatality, 'yes');
                } else {
                    this._dataStoreService.setData(IntakeStoreConstants.childfatality, 'no');
                }
            });
            // this.changePersonEvent$.next(this.addedPersons);
            this._dataStoreService.setData(IntakeStoreConstants.addedPersons, this.addedPersons);
        } else {
            this._alertService.error('Person already exists');
        }
    }
    conditionValidation() {
        this.involvedPersonFormGroup.controls['dateofdeath'].valueChanges.subscribe((res) => {
            if (this.involvedPersonFormGroup.controls['Dob'].value) {
                if (res !== null && res < this.involvedPersonFormGroup.controls['Dob'].value) {
                    this._alertService.error('DOD should be greater than Date of birth');
                    return false;
                } else if (res !== null && res > this.currentdate) {
                    this._alertService.error('DOB should not be greater than current date');
                    return false;
                }
                return true;
            }
        });
    }
    dobChange() {
        this.involvedPersonFormGroup.controls['Dob'].valueChanges.subscribe((res) => {
            // this.selectedDob = new Date(res);
            this.selectedPersonsage = this.calculateAge(res ? res : null);
            this.involvedPersonFormGroup.get('safehavenbabyflag').setValue(false);
            if (res) {
                if (this.agency === 'DJS') {
                    this.selectedDob = res ? new Date(res) : null;
                    this.getAgeInYearsAndMonth(this.currentdate, this.selectedDob);
                }
            }
        });

    }

    private validateChildAge(involvedPerson: InvolvedPerson): boolean {
        let isValidAge = true;
        involvedPerson.personRole.map(role => {
            if (role.rolekey === 'CHILD' || role.rolekey === 'RC') {
                const personAge = this.getAge(involvedPerson.Dob);
                if (personAge >= 21) {
                    isValidAge = false;
                }
            }
        });
        return isValidAge;
    }

    private getAge(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }


    showNarrative() {
        this.isNarrativeVisible = true;
        this.narrative = this._dataStoreService.getData(IntakeStoreConstants.addNarrative).Narrative;

        // this._dataStoreService.currentStore.subscribe((item) => {
        //     if (item[IntakeStoreConstants.addNarrative]) {
        //         if (item[IntakeStoreConstants.addNarrative]) {
        //             const narrativeData = item[IntakeStoreConstants.addNarrative];
        //             if (narrativeData) {
        //                 this.narrative = narrativeData.Narrative;
        //                 // Not a good solution can be replaced by pipe
        //                 // document.getElementById('narrativeElement').innerHTML = ' Narrative :' + this.narrative;
        //             } else {

        //             }
        //         }
        //     }
        // });

    }

    hideNarrative() {
        this.isNarrativeVisible = false;
    }

    checkSubstanceAbuse() {
        let valid = true;
        const isDrugExposed = this.involvedPersonFormGroup.get('drugexposednewbornflag').value;
        if (isDrugExposed) {
            const substanceClass = this.involvedPersonFormGroup.get('substanceClass').value;
            valid = ((substanceClass && substanceClass.length > 0) || this.involvedPersonFormGroup.get('otherSubstance').value);
        }

        return valid;
    }

    addPerson(involvedPerson: InvolvedPerson) {

        const isChildValidAge = this.validateChildAge(involvedPerson);
        const isSubstanceAbuseValid: boolean = this.checkSubstanceAbuse();
        // for (let i = 0; i < involvedPerson.personRole.length; i++) {
        //     if (involvedPerson.personRole[i].rolekey === 'RC') {
        //         involvedPerson.personRole[i].relationshiptorakey = 'self';
        //     }
        // }

        if (this.involvedPersonFormGroup.valid && isChildValidAge && isSubstanceAbuseValid) {
            // DJS-013 Required Victim Data Elements
            if (this.isDjs) {

                const roleVictim = involvedPerson.personRole[0].rolekey;
                const roleRelation = involvedPerson.personRole[0].relationshiptorakey;
                // D-07756, D-07755 - Address should be mandatory for Youth role and Parent/Guardian role. Address should not be mandatory for Victim role option
                if (roleVictim === 'Youth' || roleRelation === 'father' || roleRelation === 'guardian' || roleRelation === 'mother') {
                    const addresform = this.personAddressInput[0];
                    if (addresform) {
                        if (addresform.address1 && addresform.zipcode
                            && addresform.state && addresform.city) {
                        } else {
                            if (roleVictim !== 'Youth' && this.isnoCopyAddress) {
                                const persondetails = this._dataStoreService.getData(IntakeStoreConstants.addedPersons);
                                if (persondetails && persondetails.length > 0) {
                                    const getYouthDetails = persondetails.find(data => data.Role === 'Youth');
                                    if (getYouthDetails && getYouthDetails.personAddressInput && getYouthDetails.personAddressInput.length > 0) {
                                        this.copypersonAddressInput = getYouthDetails.personAddressInput.map(data => {
                                            data.addressid = '';
                                            data.personid = involvedPerson.Pid;
                                            return data;
                                        });
                                        (<any>$('#intake-addperson')).modal('hide');
                                        (<any>$('#address-add-popup')).modal('show');
                                        return true;
                                    }
                                }
                            } else if (roleVictim === 'Youth') {
                                this._alertService.error('Please add Address!');
                                (<any>$('#add-address-click')).click();
                                return true;
                            }
                        }
                    } else {
                        if (roleVictim !== 'Youth' && this.isnoCopyAddress) {
                            const persondetails = this._dataStoreService.getData(IntakeStoreConstants.addedPersons);
                            if (persondetails && persondetails.length > 0) {
                                const getYouthDetails = persondetails.find(data => data.Role === 'Youth');
                                if (getYouthDetails && getYouthDetails.personAddressInput && getYouthDetails.personAddressInput.length > 0) {
                                    this.copypersonAddressInput = getYouthDetails.personAddressInput.map(data => {
                                        data.addressid = '';
                                        data.personid = involvedPerson.Pid;
                                        return data;
                                    });
                                    (<any>$('#intake-addperson')).modal('hide');
                                    (<any>$('#address-add-popup')).modal('show');
                                    return true;
                                }
                            }
                        } else if (roleVictim === 'Youth') {
                            this._alertService.error('Please add Address!');
                            (<any>$('#add-address-click')).click();
                            return true;
                        }
                    }
                }
            }
            if (this.formalSupportForm.valid) {
                if (this.formalSupportForm.controls['personsupporttypekey'].value && this.isAS) {
                    this.formalSupportForm.value.personsupporttypekey = 'formal';
                    const formalSupport = Object.assign({
                        supportername: null,
                        personid: this.selectedPerson && this.selectedPerson.Pid ? this.selectedPerson.Pid : null,
                    }, this.formalSupportForm.value);
                    this.inFormalSuport.push(formalSupport);
                }
                involvedPerson.isapproxdod = involvedPerson.isapproxdod ? 1 : null;
                involvedPerson.isapproxdob = involvedPerson.isapproxdob ? 1 : null;
                if (this.validatePerson(involvedPerson) && this.validatePrimaryRole()) {
                    if (!this.involvedPerson.index && this.involvedPerson.index !== 0) {
                        const invDob = involvedPerson.Dob + '';
                        const invDate = moment(new Date(invDob.substr(0, 16)));
                        const invDobFormatted = invDate.format('MM/DD/YYYY');
                        const dDupPerson = this.addedPersons.filter(
                            (person) => person.Firstname === involvedPerson.Firstname && person.Lastname === involvedPerson.Lastname && person.DobFormatted === invDobFormatted
                        );
                        if (dDupPerson.length === 0) {
                            this.addHealth = this._dataStoreService.getData(MyNewintakeConstants.Intake.PersonsInvolved.Health.Health);
                            this.addWork = this._dataStoreService.getData(MyNewintakeConstants.Intake.PersonsInvolved.Work.Work);
                            this.addEducation = this._dataStoreService.getData(MyNewintakeConstants.Intake.PersonsInvolved.Educational);
                            if (this.addEducation) {
                                this.involvedPerson.school = this.addEducation.school ? this.addEducation.school : [];
                                this.involvedPerson.testing = this.addEducation.testing ? this.addEducation.testing : [];
                                this.involvedPerson.accomplishment = this.addEducation.accomplishment ? this.addEducation.accomplishment : [];
                                this.involvedPerson.vocation = this.addEducation.vocation ? this.addEducation.vocation : [];
                            }
                            this.involvedPerson.physicianinfo = this.addHealth.physician ? this.addHealth.physician : [];
                            this.involvedPerson.healthinsurance = this.addHealth.healthInsurance ? this.addHealth.healthInsurance : [];
                            this.involvedPerson.personmedicationphyscotropic = this.addHealth.medication ? this.addHealth.medication : [];
                            this.involvedPerson.personhealthexam = this.addHealth.healthExamination ? this.addHealth.healthExamination : [];
                            this.involvedPerson.personmedicalcondition = this.addHealth.medicalcondition ? this.addHealth.medicalcondition : [];
                            this.involvedPerson.personbehavioralhealth = this.addHealth.behaviouralhealthinfo ? this.addHealth.behaviouralhealthinfo : [];
                            this.involvedPerson.personabusehistory = this.addHealth.history ? this.addHealth.history : [];
                            this.involvedPerson.personabusesubstance = this.addHealth.substanceAbuse ? this.addHealth.substanceAbuse : [];
                            this.involvedPerson.persondentalinfo = this.addHealth.persondentalinfo ? this.addHealth.persondentalinfo : [];
                            this.involvedPerson.employer = this.addWork.employer ? this.addWork.employer : [];
                            this.involvedPerson.careerGoals = this.addWork.careergoals ? this.addWork.careergoals : '';
                            this.involvedPerson.guardian = this.guardianship ? this.guardianship : Object.assign({});
                            this.involvedPerson.personsupport = this.inFormalSuport ? this.inFormalSuport : [];
                            this.involvedPerson.personpayee = [];
                            involvedPerson.substances = this.substances;
                            involvedPerson.Dob = involvedPerson.Dob ? moment(involvedPerson.Dob).format('MM/DD/YYYY') : null;
                            involvedPerson.dateofdeath = involvedPerson.dateofdeath ? moment(involvedPerson.dateofdeath).format('MM/DD/YYYY') : null;
                            this.addedPersons.push(Object.assign(this.involvedPerson, involvedPerson));
                            if (this.isCW) {
                                this.addedPersons[this.addedPersons.length - 1].personRole.map((item, index) => {
                                    item.isprimary = (index === 0) ? 'true' : 'false';
                                    if (index === 0) {
                                        this.addedPersons[this.addedPersons.length - 1].Role = item.rolekey;
                                        this.addedPersons[this.addedPersons.length - 1].RelationshiptoRA = item.relationshiptorakey;
                                    }
                                });
                            } else {
                                this.addedPersons[this.addedPersons.length - 1].personRole.map((role) => {
                                    if (role.isprimary === 'true') {
                                        this.addedPersons[this.addedPersons.length - 1].Role = role.rolekey;
                                        this.addedPersons[this.addedPersons.length - 1].rolekeyDesc = role.description;
                                    }
                                    if (role.relationshiptorakey) {
                                        this.addedPersons[this.addedPersons.length - 1].RelationshiptoRA = role.relationshiptorakey;
                                    }
                                });
                            }
                            console.log(this.addedPersons[this.addedPersons.length - 1]);
                            this.addedPersons[this.addedPersons.length - 1].phoneNumber = [];
                            this.addedPersons[this.addedPersons.length - 1].phoneNumber = Object.assign(this.phoneNumber);

                            this.addedPersons[this.addedPersons.length - 1].emailID = [];
                            this.addedPersons[this.addedPersons.length - 1].emailID = Object.assign(this.emailID);

                            this.addedPersons[this.addedPersons.length - 1].personAddressInput = [];
                            this.addedPersons[this.addedPersons.length - 1].personAddressInput = Object.assign(this.personAddressInput);
                            if (this.isImageHide) {
                                this.addedPersons[this.addedPersons.length - 1].userPhoto = this.userProfilePicture ? this.userProfilePicture.s3bucketpathname : '';
                            } else {
                                this.addedPersons[this.addedPersons.length - 1].userPhoto = involvedPerson.userPhoto ? involvedPerson.userPhoto : '';
                            }
                            this.addedPersons[this.addedPersons.length - 1].Pid = involvedPerson.Pid ? involvedPerson.Pid : (AppConstants.PERSON.TEMP_ID + new Date().getTime());
                            this.addedPersons[this.addedPersons.length - 1].fetalalcoholspctrmdisordflag = involvedPerson.fetalalcoholspctrmdisordflag ? 1 : 0;
                            this.addedPersons[this.addedPersons.length - 1].drugexposednewbornflag = involvedPerson.drugexposednewbornflag ? 1 : 0;
                            this.addedPersons[this.addedPersons.length - 1].probationsearchconductedflag = involvedPerson.probationsearchconductedflag ? 1 : 0;
                            this.addedPersons[this.addedPersons.length - 1].sexoffenderregisteredflag = involvedPerson.sexoffenderregisteredflag ? 1 : 0;
                            this.addedPersons[this.addedPersons.length - 1].safehavenbabyflag = involvedPerson.safehavenbabyflag ? 1 : 0;
                            this.addedPersons[this.addedPersons.length - 1].everbeenadoptedflag = involvedPerson.everbeenadoptedflag ? 1 : 0;
                            // this.addedPersons[this.addedPersons.length - 1].userPhoto = this.userProfilePicture ? this.userProfilePicture.s3bucketpathname : '';
                            this.addedPersons.map((item) => {
                                // if (item.Dob) {
                                //     const dob = item.Dob + '';
                                //     const date = moment(new Date(dob.substr(0, 16)));
                                //     item.DobFormatted = date.format('MM/DD/YYYY');
                                // } else {
                                //     item.DobFormatted = 'N/A';
                                // }
                                // if (item.dateofdeath) {
                                //     const dod = item.dateofdeath + '';
                                //     const date = moment(new Date(dod.substr(0, 16)));
                                //     item.DodFormatted = date.format('MM/DD/YYYY');
                                // } else {
                                //     item.DodFormatted = null;
                                // }
                                item.fullName = item.Firstname + ' ' + item.Lastname;
                                if (item.personAddressInput && item.personAddressInput.length !== 0) {
                                    item.fullAddress =
                                        this.isEmptyOrNull(item.personAddressInput[0].address1) +
                                        ' ' +
                                        this.isEmptyOrNull(item.personAddressInput[0].Address2) +
                                        ' ' +
                                        this.isEmptyOrNull(item.personAddressInput[0].city) +
                                        ' ' +
                                        this.isEmptyOrNull(item.personAddressInput[0].state) +
                                        ' ' +
                                        this.isEmptyOrNull(item.personAddressInput[0].county) +
                                        ' ' +
                                        this.isEmptyOrNull(item.personAddressInput[0].zipcode);

                                    item.Address = item.personAddressInput[0].address1;
                                    item.Address2 = item.personAddressInput[0].Address2;
                                    item.City = item.personAddressInput[0].city;
                                    item.State = item.personAddressInput[0].state;
                                    item.County = item.personAddressInput[0].county;
                                    item.county = item.personAddressInput[0].county;
                                    item.Zip = item.personAddressInput[0].zipcode;
                                }
                            });
                            this.addedPersons.map((item, ix) => {
                                item.index = ix;
                                return item;
                            });
                            // this.personsList$.next(this.addedPersons);
                            console.log(this.addedPersons);
                            this._dataStoreService.setData(IntakeStoreConstants.addedPersons, this.addedPersons);
                            this.clearPerson();
                            // this.mdmIntegration(involvedPerson);
                            this.showAddPersonPopup = false;
                            (<any>$('#intake-addperson')).modal('hide');
                            this._alertService.success('Involved person added successfully!');
                            // this.addedPersonsSubject$.next(this.addedPersons);
                            this.involvedPerson.index = null;
                            // this.changePersonEvent$.next(this.addedPersons);
                            this._dataStoreService.setData(IntakeStoreConstants.addedPersons, this.addedPersons);
                        } else {
                            this._alertService.error('Person already exists');
                        }
                        // });
                    } else {
                        involvedPerson.Dob = involvedPerson.Dob ? moment(involvedPerson.Dob).format('MM/DD/YYYY') : null;
                        involvedPerson.dateofdeath = involvedPerson.dateofdeath ? moment(involvedPerson.dateofdeath).format('MM/DD/YYYY') : null;
                        this.addedPersons[this.involvedPerson.index] = involvedPerson;
                        this.addedPersons[this.involvedPerson.index].school = this.addEducation.school ? this.addEducation.school : [];
                        this.addedPersons[this.involvedPerson.index].testing = this.addEducation.testing ? this.addEducation.testing : [];
                        this.addedPersons[this.involvedPerson.index].accomplishment = this.addEducation.accomplishment ? this.addEducation.accomplishment : [];
                        this.addedPersons[this.involvedPerson.index].vocation = this.addEducation.vocation ? this.addEducation.vocation : [];
                        this.addedPersons[this.involvedPerson.index].raceDescription = this.raceDescriptionText ? this.raceDescriptionText : '';
                        this.addedPersons[this.involvedPerson.index].maritalDescription = this.maritalDescription ? this.maritalDescription : '';
                        this.addedPersons[this.involvedPerson.index].physicianinfo = this.addHealth.physician ? this.addHealth.physician : [];
                        this.addedPersons[this.involvedPerson.index].healthinsurance = this.addHealth.healthInsurance ? this.addHealth.healthInsurance : [];
                        this.addedPersons[this.involvedPerson.index].personmedicationphyscotropic = this.addHealth.medication ? this.addHealth.medication : [];
                        this.addedPersons[this.involvedPerson.index].personhealthexam = this.addHealth.healthExamination ? this.addHealth.healthExamination : [];
                        this.addedPersons[this.involvedPerson.index].personmedicalcondition = this.addHealth.medicalcondition ? this.addHealth.medicalcondition : [];
                        this.addedPersons[this.involvedPerson.index].personbehavioralhealth = this.addHealth.behaviouralhealthinfo ? this.addHealth.behaviouralhealthinfo : [];
                        this.addedPersons[this.involvedPerson.index].personabusehistory = this.addHealth.history ? this.addHealth.history : [];
                        this.addedPersons[this.involvedPerson.index].personabusesubstance = this.addHealth.substanceAbuse ? this.addHealth.substanceAbuse : [];
                        this.addedPersons[this.involvedPerson.index].persondentalinfo = this.addHealth.persondentalinfo ? this.addHealth.persondentalinfo : [];
                        this.addedPersons[this.involvedPerson.index].employer = this.addWork.employer ? this.addWork.employer : [];
                        this.addedPersons[this.involvedPerson.index].careerGoals = this.addWork.careergoals ? this.addWork.careergoals : '';
                        this.addedPersons[this.involvedPerson.index].substances = this.substances;
                        this.addedPersons[this.involvedPerson.index].fetalalcoholspctrmdisordflag = involvedPerson.fetalalcoholspctrmdisordflag ? 1 : 0;
                        this.addedPersons[this.involvedPerson.index].drugexposednewbornflag = involvedPerson.drugexposednewbornflag ? 1 : 0;
                        this.addedPersons[this.involvedPerson.index].probationsearchconductedflag = involvedPerson.probationsearchconductedflag ? 1 : 0;
                        this.addedPersons[this.involvedPerson.index].sexoffenderregisteredflag = involvedPerson.sexoffenderregisteredflag ? 1 : 0;
                        this.addedPersons[this.involvedPerson.index].safehavenbabyflag = involvedPerson.safehavenbabyflag ? 1 : 0;
                        this.addedPersons[this.involvedPerson.index].everbeenadoptedflag = involvedPerson.everbeenadoptedflag ? 1 : 0;
                        this.addedPersons[this.involvedPerson.index].emergency = this.contactPersonInfo ? this.contactPersonInfo : [];
                        this.addedPersons[this.involvedPerson.index].guardian = this.guardianship ? this.guardianship : Object.assign({});
                        this.addedPersons[this.involvedPerson.index].personsupport = this.inFormalSuport ? this.inFormalSuport : [];
                        this.addedPersons[this.involvedPerson.index].personpayee = this.personPayeeDetails ? this.personPayeeDetails : [];
                        this.raceDescriptionText = '';
                        this.maritalDescription = '';
                        this.addedPersons[this.involvedPerson.index].personRole.map((role, index) => {
                            this.personRole.map((item) => {
                                if (item.rolekey === role.rolekey && !role.description) {
                                    role.description = item.description;
                                }
                            });
                            if (this.isCW) {
                                role.isprimary = (index === 0) ? 'true' : 'false';
                                if (index === 0) {
                                    this.addedPersons[this.involvedPerson.index].Role = role.rolekey;
                                    this.addedPersons[this.involvedPerson.index].RelationshiptoRA = role.relationshiptorakey;
                                }
                            } else {
                                if (role.isprimary === 'true') {
                                    this.addedPersons[this.involvedPerson.index].Role = role.rolekey;
                                }
                                if (role.relationshiptorakey) {
                                    this.addedPersons[this.involvedPerson.index].RelationshiptoRA = role.relationshiptorakey;
                                }
                            }
                        });
                        console.log(this.addedPersons[this.involvedPerson.index]);
                        // const dob = this.addedPersons[this.involvedPerson.index].Dob + '';
                        // const date = moment(new Date(dob.substr(0, 16)));
                        // if (this.addedPersons[this.involvedPerson.index].dateofdeath) {
                        //     const dod = this.addedPersons[this.involvedPerson.index].dateofdeath ? this.addedPersons[this.involvedPerson.index].dateofdeath + '' : '';
                        //     const dateOfDearth = moment(new Date(dod.substr(0, 16)));
                        //     this.addedPersons[this.involvedPerson.index].DodFormatted = dateOfDearth.format('MM/DD/YYYY');
                        // }
                        // this.addedPersons[this.involvedPerson.index].DobFormatted = date.format('MM/DD/YYYY');
                        this.addedPersons[this.involvedPerson.index].phoneNumber = [];
                        this.addedPersons[this.involvedPerson.index].phoneNumber = Object.assign(this.phoneNumber);

                        this.addedPersons[this.involvedPerson.index].emailID = [];
                        this.addedPersons[this.involvedPerson.index].emailID = Object.assign(this.emailID);

                        this.addedPersons[this.involvedPerson.index].Pid = involvedPerson.Pid;

                        this.addedPersons[this.involvedPerson.index].personAddressInput = [];
                        this.addedPersons[this.involvedPerson.index].personAddressInput = Object.assign(this.personAddressInput);
                        this.addedPersons[this.involvedPerson.index].fullName = this.addedPersons[this.involvedPerson.index].Firstname + ' ' + this.addedPersons[this.involvedPerson.index].Lastname;
                        if (this.isImageHide) {
                            this.addedPersons[this.involvedPerson.index].userPhoto = this.userProfilePicture ? this.userProfilePicture.s3bucketpathname : '';
                        } else {
                            this.addedPersons[this.involvedPerson.index].userPhoto = involvedPerson.userPhoto ? involvedPerson.userPhoto : '';
                        }

                        if (this.addedPersons[this.involvedPerson.index].personAddressInput && this.addedPersons[this.involvedPerson.index].personAddressInput.length !== 0) {
                            this.addedPersons[this.involvedPerson.index].fullAddress =
                                this.isEmptyOrNull(this.addedPersons[this.involvedPerson.index].personAddressInput[0].address1) +
                                ' ' +
                                this.isEmptyOrNull(this.addedPersons[this.involvedPerson.index].personAddressInput[0].Address2) +
                                ' ' +
                                this.isEmptyOrNull(this.addedPersons[this.involvedPerson.index].personAddressInput[0].city) +
                                ' ' +
                                this.isEmptyOrNull(this.addedPersons[this.involvedPerson.index].personAddressInput[0].state) +
                                ' ' +
                                this.isEmptyOrNull(this.addedPersons[this.involvedPerson.index].personAddressInput[0].county) +
                                ' ' +
                                this.isEmptyOrNull(this.addedPersons[this.involvedPerson.index].personAddressInput[0].zipcode);

                            this.addedPersons[this.involvedPerson.index].Address = this.addedPersons[this.involvedPerson.index].personAddressInput[0].address1;
                            this.addedPersons[this.involvedPerson.index].Address2 = this.addedPersons[this.involvedPerson.index].personAddressInput[0].Address2;
                            this.addedPersons[this.involvedPerson.index].City = this.addedPersons[this.involvedPerson.index].personAddressInput[0].city;
                            this.addedPersons[this.involvedPerson.index].State = this.addedPersons[this.involvedPerson.index].personAddressInput[0].state;
                            this.addedPersons[this.involvedPerson.index].County = this.addedPersons[this.involvedPerson.index].personAddressInput[0].county;
                            this.addedPersons[this.involvedPerson.index].county = this.addedPersons[this.involvedPerson.index].personAddressInput[0].county;
                            this.addedPersons[this.involvedPerson.index].Zip = this.addedPersons[this.involvedPerson.index].personAddressInput[0].zipcode;
                        }
                        // this.personsList$.next(this.addedPersons);
                        this._dataStoreService.setData(IntakeStoreConstants.addedPersons, this.addedPersons);
                        let rcPerson = [];
                        rcPerson = this.addedPersons.filter((person) => person.Role === 'RC');
                        rcPerson.map((res) => {
                            if (res.dateofdeath) {
                                this._dataStoreService.setData(IntakeStoreConstants.childfatality, 'yes');
                            } else {
                                this._dataStoreService.setData(IntakeStoreConstants.childfatality, 'no');
                            }
                        });
                        if (!this.isCW) {
                            this.clearPerson();
                            this.showAddPersonPopup = false;
                            (<any>$('#intake-addperson')).modal('hide');
                        }
                        this._alertService.success('Involved person updated successfully!');
                        // this.changePersonEvent$.next(this.addedPersons);
                        this._dataStoreService.setData(IntakeStoreConstants.addedPersons, this.addedPersons);
                        // this.addedPersonsSubject$.next(this.addedPersons);
                    }
                    if (this.isDjs) {
                        this.getAgeInYearsAndMonth(this.currentdate, involvedPerson.Dob);
                    }
                }
            } else {
                this._alertService.warn('Please fill the formal support description');
            }
        } else {
            if (!isSubstanceAbuseValid) {
                this._alertService.error('Please fill substance details.');
            } else if (isChildValidAge) {
                ControlUtils.validateAllFormFields(this.involvedPersonFormGroup);
                ControlUtils.setFocusOnInvalidFields();
                // Start D-06392
                if (!this.madatoryFields()) {
                    this._alertService.warn('Please fill mandatory fields!');
                } else {
                    if (this.isTab1Valid) {
                        (<any>$('#add-Reporter-click')).click();
                    }

                }
                // End D-06392
            } else {
                this._alertService.error('Child or reported child age should be less than 18.');
            }
        }
        this.involvedPersonFormGroup.patchValue({
            State: 'IN',
            TemparoryState: 'IN'
        });
        this.isnoCopyAddress = true;
    }


    // Start D-06392
    madatoryFields() {
        // Profile
        if (this.involvedPersonFormGroup.get('Lastname').hasError('required')) {
            this._alertService.warn('Please fill Last Name!');
            (<any>$('#profile-click')).click();
            return true;
        } else if (this.involvedPersonFormGroup.get('Firstname').hasError('required')) {
            this._alertService.warn('Please fill First Name!');
            (<any>$('#profile-click')).click();
            return true;
        } else if (this.involvedPersonFormGroup.controls['Dob'].invalid) {
            this._alertService.warn('Please fill Date of birth!');
            return true;
        } else if (this.involvedPersonFormGroup.get('Gender').hasError('required')) {
            this._alertService.warn('Please fill Gender!');
            (<any>$('#profile-click')).click();
            return true;
        } else if (!this.isDjs && this.involvedPersonFormGroup.get('Dangerousself').hasError('required')) {

            if (this.substancExposed) {


                return false;

            } else {
                this._alertService.warn('Please select Danger to self!');
                this.isTab1Valid = true;
                return true;
            }
        } else if (!this.isDjs && this.involvedPersonFormGroup.get('Dangerousworker').hasError('required')) {
            this._alertService.warn('Please select Danger to worker!');
            this.isTab1Valid = true;
            return true;
        } else if (!this.isDjs && this.involvedPersonFormGroup.get('Mentealimpair').hasError('required')) {
            this._alertService.warn('Please select Appearance of mentally impaired!');
            return true;
        } else if (!this.isDjs && this.involvedPersonFormGroup.get('Mentealillness').hasError('required')) {
            if (this.substancExposed) {
                return false;
            } else {
                this._alertService.warn('Please select Signs of mental illness!');
                return true;
            }
        } else if (this.involvedPersonFormGroup.get('race').hasError('required')) {
            this._alertService.warn('Please select Race!');
            (<any>$('#profile-click')).click();
            return true;

        } else if (this.involvedPersonFormGroup.get('Ethnicity').hasError('required')) {
            this._alertService.warn('Please select Ethnicity!');
            (<any>$('#profile-click')).click();
            return true;

        } else {
            const personRoleArray = <FormArray>this.involvedPersonFormGroup.controls.personRole;
            const _personRole = personRoleArray.controls;
            let role = false;
            _personRole.forEach((pr) => {
                if (pr.get('rolekey').hasError('required')) {
                    this._alertService.warn('Please select Role!');
                    (<any>$('#add-Reporter-click')).click();
                    role = true;
                } else if (pr.get('relationshiptorakey').hasError('required')) {
                    this._alertService.warn('Please select Relationship!');
                    (<any>$('#add-Reporter-click')).click();
                    role = true;
                } else if (pr.get('isprimary').hasError('required')) {
                    this._alertService.warn('Please select Role type!');
                    role = true;
                }
            });
            return role;
        }
    }
    // End D-06392

    // private mdmIntegration(involvedPerson) {
    //     const person = Object.assign({
    //         sourceSystem: 'E_E',
    //         origin: 'W',
    //         sourceKey: '800161111',
    //         firstName: involvedPerson.Firstname,
    //         lastName: involvedPerson.Lastname,
    //         dateOfBirth: '08-21-1992',
    //         gender: involvedPerson.Gender,
    //         race: 'C',
    //         ssn: involvedPerson.SSN
    //     });
    //     this.headers = new Headers({
    //         'Content-Type': 'application/json'
    //         // 'Authorization': 'Bearer 4bf93ded-e298-3017-854a-c65ebdf294b4'
    //     });

    //     this.option = new RequestOptions({
    //         headers: this.headers
    //     });
    //     // 'http://dev-apigateway-int.mdcloud.local:8280/api/cjams/dev1/Person/v1/People/addpersonmdm?access_token='
    //     return this._http
    //         .post(
    //             AppConfig.baseUrl + '/Person/v1/People/addpersonmdm?access_token=' + this.token.id,

    //             JSON.stringify(person),
    //             this.option
    //         )
    //         .map((res) => {
    //             return res.json();
    //         })
    //         .subscribe((res) => console.log(res));
    // }

    openFindModal() {
        // D-07269 Start
        this.hasRoleYouth();
        // D-07269 End
        this.showPersonSearch = true;
        (<any>$('#intake-findperson')).modal('show');
    }
    searchIdentified(person) {
        this._dataStoreService.setData(IntakeStoreConstants.PERSON_TO_SEARCH, person);
        this.openFindModal();
    }
    openAddUnkModal() {
        (<any>$('#intake-unkperson')).modal('show');
    }
    addUnkPerson() {
        const person = this.unkPersonFormGroup.getRawValue();
        person['id'] = new Date().getTime();
        person['isNew'] = true;
        this.unkPersonList.push(person);
        this._dataStoreService.setData(IntakeStoreConstants.addedUnkPersons, this.unkPersonList);
        this.unkPersonFormGroup.reset();
        (<any>$('#intake-unkperson')).modal('hide');
    }
    deleteUnkPerson(person) {
        this.unkPersonList = this.unkPersonList.filter(item => item.id !== person.id);
        this._dataStoreService.setData(IntakeStoreConstants.addedUnkPersons, this.unkPersonList);
        this._alertService.success('Person deleted successfully.');
    }
    showSerachDetailsPage(modal) {
        if (modal === 'emergency') {
            this.isEmergencyContact = true;
            this.representativePerson = false;
        } else {
            this.representativePerson = true;
            this.isEmergencyContact = false;
        }
        (<any>$('#intake-addperson')).modal('hide');
        this.showPersonSearch = true;
        (<any>$('#intake-findperson')).modal('show');
    }
    removePerson(index) {
        this.contactPersonInfo.splice(index, 1);
    }
    private validatePerson(involvedPerson): boolean {
        // if (involvedPerson.Role === 'RA' || involvedPerson.Role === 'RC') {
        //     if (!involvedPerson.reportedPersonForm.livingArrangements) {
        //         this._alertService.warn('Please select living Arrangement');
        //         return false;
        //     }
        //     if (!involvedPerson.RoutingAddress) {
        //         this._alertService.warn('Please select Routing Address');
        //         return false;
        //     }
        //     if (!involvedPerson.reportedPersonForm.Mentealillness) {
        //         this._alertService.warn('Please enter mental illness');
        //         return false;
        //     }
        //     if (involvedPerson.reportedPersonForm.Mentealillness === 'yes' && !involvedPerson.reportedPersonForm.MentealillnessDetail) {
        //         this._alertService.warn('Please enter mental illness notes');
        //         return false;
        //     }

        //     if (!involvedPerson.reportedPersonForm.Mentealimpair) {
        //         this._alertService.warn('Please enter mental impared');
        //         return false;
        //     }

        //     if (involvedPerson.reportedPersonForm.Mentealimpair === 'yes' && !involvedPerson.reportedPersonForm.MentealimpairDetail) {
        //         this._alertService.warn('Please enter mentally impared notes');
        //         return false;
        //     }
        // } else {
        //     if (!involvedPerson.RelationshiptoRA) {
        //         this._alertService.warn('Please select relationship to reported person');
        //         return false;
        //     }
        // }
        // if (involvedPerson.Dangerous === 'Yes' && !involvedPerson.DangerousWorkerReason) {
        //     this._alertService.warn('Please enter Danger Reason');
        //     return false;
        // }
        // if (involvedPerson.dangerAddress === 'Yes' && !involvedPerson.DangerousAddressReason) {
        //     this._alertService.warn('Please enter Address Danger Reason');
        //     return false;
        // }

        return true;
    }

    clearPerson() {
        this.involvedPerson = new InvolvedPerson();
        this.involvedPersonFormGroup.reset();
        this.personAddressForm.reset();
        this.formalSupportForm.reset();
        this.involvedPersonFormGroup.patchValue({
            Dob: null,
            Role: '',
            livingArrangements: '',
            Gender: '',
            RelationshiptoRA: '',
            Ethicity: '',
            PrimaryLanguage: '',
            State: 'IN',
            TemparoryState: 'IN'
            // Mentealillness: '',
            // MentealillnessDetail: '',
            // Mentealimpair: '',
            // MentealimpairDetail: ''
        });
        this.personAddressInput = [];
        this.phoneNumber = [];
        this.phoneNumber$ = Observable.empty();
        this.emailID = [];
        this.inFormalSuport = [];
        this.emailID$ = Observable.empty();
        this.imageChangedEvent = Object.assign({});
        this.isDefaultPhoto = true;
        this.isImageLoadFailed = false;
        this.isImageHide = false;
        this.beofreImageCropeHide = false;
        this.afterImageCropeHide = false;
        this.educationFormReset$.next(true);
        this.healthFormReset$.next(true);
        this.workFormReset$.next(true);
        this.youthAge = '';
        this.dobRequired = true;
        this.showDobRequired = true;
        this.personPayeeDetails = [];
        this.involvedrepresentative = [];
        this.representative = [];
        this.storeMaritalStatus = '';
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Worker, null);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.GuardianPerson, null);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.GuardianProperty, null);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Attorney, null);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.Funeral, null);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.CodeStatus, null);
        this._dataStoreService.setData(INTAKE_GUARDIANSHIP.HSSClinetID, null);
    }

    confirmDelete(involvedPerson: InvolvedPerson, index) {
        this.involvedPerson.index = index;
        (<any>$('#delete-person-popup')).modal('show');
    }
    deleteIdentified(person, action) {
        if (action === -1) {
            this.persontodelete = person;
            (<any>$('#delete-unkperson-popup')).modal('show');
        } else if (action === 0) {
            this.persontodelete = null;
        } else if (action === 1) {
            // let  persons = this._dataStoreService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
            if (this.persontodelete.role === 'Reporter') {
                const reporterPerson = this._dataStoreService.getData(IntakeStoreConstants.addNarrative);
                reporterPerson.isAdded = true;
                this.reporterPerson.isAdded = true;
                this._dataStoreService.setData(IntakeStoreConstants.addNarrative, reporterPerson);
            } else {
                this.addedIdentifiedPersons = this.addedIdentifiedPersons.filter(item => this.persontodelete.id !== item.id);
                this._dataStoreService.setData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS, this.addedIdentifiedPersons);
                this.addedIdentifiedPersons = this._dataStoreService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
            }
            (<any>$('#delete-unkperson-popup')).modal('hide');
        }
    }
    dodChange() {
        this.involvedPersonFormGroup.controls['dateofdeath'].valueChanges.subscribe((res) => {
            if (!res) {
                this.involvedPersonFormGroup.controls['isapproxdod'].reset();
            }
            // this.dodPresent = new Date(res);
            // this.getAgeInYearsAndMonth(this.currentdate, this.dodPresent);
        });
    }
    deletePerson() {
        // this.addedPersons = this.addedPersons.map((item, ix) => {
        //     item.index = ix;
        //     return item;
        // });
        // this.addedPersonsSubject$.next(this.addedPersons);
        // this._personManageService;
        // .create(
        //     {
        //         firstname: this.addedPersons[this.involvedPerson.index].Firstname,
        //         lastname: this.addedPersons[this.involvedPerson.index].Lastname,
        //         role: this.addedPersons[this.involvedPerson.index].Role,
        //         isnew: false,
        //         isedit: false,
        //         isdelete: true,
        //         obj: this.addedPersons[this.involvedPerson.index]
        //     },
        //     NewUrlConfig.EndPoint.Intake.PersonInvolvedManageUrl
        // )
        // .subscribe((result) => {
        this.addedPersons.splice(this.involvedPerson.index, 1);

        (<any>$('#delete-person-popup')).modal('hide');
        this.clearPerson();
        // this.changePersonEvent$.next(this.addedPersons);
        this._dataStoreService.setData(IntakeStoreConstants.addedPersons, this.addedPersons);
        this.involvedPerson.index = null;
        // });
    }

    addAlias(involvedPersonAlias: InvolvedPersonAlias) {
        if (this.addAliasForm.get('AliasFirstName').value) {
            this.addedAliasPersons.push(Object.assign(this.involvedPersonAlias, involvedPersonAlias));
            this.addedAliasPersons = this.addedAliasPersons.map((item, ix) => {
                item.index = ix;
                return item;
            });
        }
        this.clearAlias();
    }
    editAliasName(involvedPersonAlias: InvolvedPersonAlias) {
        this.editAliasForm = true;
        this.involvedPersonAlias = involvedPersonAlias;
        this.addAliasForm.patchValue(involvedPersonAlias);
    }
    confirmDeleteAlias(involvedPerson: InvolvedPerson) {
        this.involvedPersonAlias.index = involvedPerson.index;
        (<any>$('#delete-alias-popup')).modal('show');
    }
    deleteAliasName() {
        this.addedAliasPersons.splice(this.involvedPersonAlias.index, 1);
        this.addedAliasPersons = this.addedAliasPersons.map((item, ix) => {
            item.index = ix;
            return item;
        });
        (<any>$('#delete-alias-popup')).modal('hide');
        this.clearAlias();
    }
    updateAlias(involvedPersonAlias: InvolvedPersonAlias) {
        if (this.addAliasForm.get('AliasFirstName').value) {
            this.involvedPersonAlias[this.involvedPersonAlias.index] = Object.assign(this.involvedPersonAlias, involvedPersonAlias);
            this.addedAliasPersons = this.addedAliasPersons.map((item, ix) => {
                item.index = ix;
                return item;
            });
        }
        this.clearAlias();
    }
    clearAlias() {
        this.involvedPersonAlias = new InvolvedPersonAlias();
        this.editAliasForm = false;
        this.addAliasForm.reset();
    }

    toggleAddPopup(person) {
        this.showAddPersonPopup = true;
        this.addEditLabel = 'Add New';
        this.isImageHide = true;
        this.beofreImageCropeHide = false;
        this.youthAge = '';
        // DJS-008 Auto Populate the Add New Person screen
        /*if (person && person.source === 'SDR') {
        const searchedFormData = this._dataStoreService.getData('SearchDataInvolvedPerson');
        if (searchedFormData) {
            this.patchSearchFormData(searchedFormData);
        }
        }*/
        if (this.isDjs) {
            this.isDjsYouth = false;
        } else {
            this.isDjsYouth = true;
        }
        if (person && person.source === 'SDR') {
            this.involvedPersonFormGroup.patchValue({
                Lastname: person.lastname,
                Firstname: person.firstname,
                middlename: person.middlename,
                Gender: person.gendertypekey,
                Dob: new Date(person.dob),
                // dateofdeath: new Date(person.dateofdeath),
                ssn: person.ssn,
                mdm_id: person.mdm_id,
                Pid: null,
                suffix: person.suffix,
                aliasname: person.alias,
                source: person.source
            });
        } else {
            const searchedFormData = this._dataStoreService.getData('SearchDataInvolvedPerson');
            if (searchedFormData) {
                this.patchSearchFormData(searchedFormData);
            }
        }
        this.involvedPersonFormGroup.setControl('personRole', this._formBuilder.array([]));
        this.addNewRole();
        (<any>$('#intake-addperson')).modal('show');
        (<any>$('#profile-click')).click();
        this.userProfile = '../../../../../assets/images/ic_silhouette.png';
        this.addEducationOutputSubject$.next({});
        this.contactPersonInfo = [];
        this.isContactSerach = false;
        this.requiredTab = true;
    }
    // DJS-008 Auto Populate the Add New Person screen
    patchSearchFormData(model) {
        console.log('model...', model);
        this.involvedPersonFormGroup.patchValue({
            Lastname: model.lastname,
            Firstname: model.firstname,
            // middlename: model.maidenname,
            Gender: model.gender,
            City: model.city,
            Address: model.address,
            Zip: model.zip,
            State: model.state,
            County: model.county,
            county: model.county,
            ssn: model.ssn,
            stateid: model.dl,
            occupation: model.occupation,
            aliasname: model.alias
        });
        if (model.dob) {
            this.involvedPersonFormGroup.patchValue({
                Dob: model.dob ? new Date(model.dob) : ''
            });
        }
        if (model.dateofdeath) {
            this.involvedPersonFormGroup.patchValue({
                Dob: model.dateofdeath ? new Date(model.dateofdeath) : ''
            });
        }
        this.personAddressForm.patchValue({
            address1: model.address1,
            Address2: model.address2,
            zipcode: model.zip,
            state: model.stateid,
            county: model.county,
            city: model.city
        });
        this.personPhoneForm.patchValue({
            contactnumber: model.phone
        });
        this.personEmailForm.patchValue({
            EmailID: model.email
        });
    }
    navigateNext(modal) {
        if (modal === 'health') {
            (<any>$('#health-click')).click();
        }
        if (modal === 'INTAKE_GUARDIANSHIP') {
            (<any>$('#INTAKE_GUARDIANSHIP-click')).click();
        }
        if (modal === 'educational') {
            (<any>$('#educational-click')).click();
        }
        if (modal === 'contacts') {
            (<any>$('#add-contacts-click')).click();
        }
        if (modal === 'address') {
            (<any>$('#add-address-click')).click();
        } else if (modal === 'role') {
            (<any>$('#add-Reporter-click')).click();
        }
    }
    raceDescription(item) {
        this.raceDescriptionText = item.source.triggerValue;
    }

    maritalStatusDescription(item) {
        this.maritalDescription = item.source.triggerValue;
    }
    addPhone() {
        const phoneNumber = this.personPhoneForm.get('contactnumber').value;
        const phoneType = this.personPhoneForm.get('contacttype').value;
        if (phoneNumber && phoneType) {
            this.phoneNumber.push({
                contacttypeid: '',
                contactnumber: phoneNumber,
                contacttype: phoneType,
                isactive: 1
            });
            this.phoneNumber$ = Observable.of(this.phoneNumber);
            this.personPhoneForm.reset();
        } else {
            this._alertService.warn('Please fill mandatory fields for Phone');
            ControlUtils.validateAllFormFields(this.personPhoneForm);
            // ControlUtils.setFocusOnInvalidFields();
        }
    }
    addEmail() {
        const emailID = this.personEmailForm.get('EmailID').value;
        const emailType = this.personEmailForm.get('EmailType').value;
        if (emailID && this.personEmailForm.invalid) {
            this._alertService.warn('Please enter a valid email id');
            return;
        }
        if (emailID && emailType && this.personEmailForm.valid) {
            this.emailID.push({
                mailtypeid: '',
                mailid: emailID,
                mailtype: emailType,
                isactive: 1
            });
            this.emailID$ = Observable.of(this.emailID);
            this.personEmailForm.reset();
        } else {
            this._alertService.warn('Please fill mandatory fields for Email');
            ControlUtils.validateAllFormFields(this.personEmailForm);
            // ControlUtils.setFocusOnInvalidFields();
        }
    }

    deletePhone(i: number) {
        this.phoneNumber.splice(i, 1);
        this.phoneNumber$ = Observable.of(this.phoneNumber);
    }

    deleteEmail(i: number) {
        this.emailID.splice(i, 1);
        this.emailID$ = Observable.of(this.emailID);
    }
    saveInFormal(modal) {
        if (this.selectedPerson && this.selectedPerson.Pid) {
            modal.personid = this.selectedPerson.Pid;
        } else {
            modal.personid = null;
        }
        modal.personsupporttypekey = 'informal';
        if (this.inFormalIndex === -1) {
            this.inFormalSuport.push(modal);
        } else {
            this.inFormalSuport[this.inFormalIndex] = modal;
            this.inFormalIndex = -1;
        }
        this.informalForm.reset();

    }
    cancelInFormal() {
        this.informalForm.reset();
        this.inFormalIndex = -1;
    }
    editInFormal(modal, i) {
        this.informalForm.patchValue(modal);
        this.inFormalIndex = i;
    }
    deleteInFormal(i) {
        this.inFormalSuport.splice(i, 1);
    }
    formalSupprotChecked(formal) {
        if (formal) {
            this.formalSupportForm.controls['description'].setValidators([Validators.required]);
            this.formalSupportForm.controls['description'].updateValueAndValidity();
        } else {
            this.formalSupportForm.controls['description'].clearValidators();
            this.formalSupportForm.controls['description'].updateValueAndValidity();
            this.formalSupportForm.controls['description'].reset();
        }
    }
    personTypeChange(modal) {
        if (modal === 'PRSN') {
            this.representativegroup.controls['personrepresentativepersonid'].disable();
        } else {
            this.representativegroup.controls['personrepresentativepersonid'].enable();
        }
        this.representativegroup.controls['personrepresentativepersonid'].reset();
    }
    saveRepresentative(modal: PersonPayee) {
        modal.personid = this.selectedPerson.Pid ? this.selectedPerson.Pid : null;
        if (this.representativeindex === -1) {
            this.personPayeeDetails.push(modal);
        } else {
            this.personPayeeDetails[this.representativeindex] = modal;
        }
        this.representativegroup.reset();
        this.representativeindex = -1;
        this.endDateValidation(this.personPayeeDetails);
    }
    editPayee(modal, index) {
        this.representativeindex = index;
        this.representativegroup.patchValue(modal);
        this.isSaveBtnDisabled = false;
    }
    deletePayee(index) {
        this.personPayeeDetails.splice(index, 1);
        this.endDateValidation(this.personPayeeDetails);
    }
    cancelRepresentative() {
        this.representativegroup.reset();
        this.representativeindex = -1;
        this.endDateValidation(this.personPayeeDetails);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
    disabledInput(state: boolean, inputfield: string, manditory: string) {
        if (state === false && manditory === 'isManditory') {
            this.involvedPersonFormGroup.get(inputfield).enable();
            if (inputfield === 'DangerousselfReason') {
                this.involvedPersonFormGroup.get('Dangerousself').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerSelfRequired = true;
                        this.involvedPersonFormGroup.get('DangerousselfReason').setValidators([Validators.required]);
                        this.involvedPersonFormGroup.get('DangerousselfReason').updateValueAndValidity();
                    } else {
                        this.dangerSelfRequired = false;
                        this.involvedPersonFormGroup.get('DangerousselfReason').clearValidators();
                        this.involvedPersonFormGroup.get('DangerousselfReason').updateValueAndValidity();
                    }
                });
            }

            if (inputfield === 'DangerousWorkerReason') {
                this.involvedPersonFormGroup.get('Dangerousworker').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerWorkRequired = true;
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').setValidators([Validators.required]);
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').updateValueAndValidity();
                    } else {
                        this.dangerWorkRequired = false;
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').clearValidators();
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').updateValueAndValidity();
                    }
                });
            }

            if (inputfield === 'DangerousWorkerReason') {
                this.involvedPersonFormGroup.get('Dangerousworker').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').setValidators([Validators.required]);
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').updateValueAndValidity();
                    } else {
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').clearValidators();
                        this.involvedPersonFormGroup.get('DangerousWorkerReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'MentealimpairDetail') {
                this.involvedPersonFormGroup.get('Mentealimpair').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerAppearance = true;
                        this.involvedPersonFormGroup.get('MentealimpairDetail').setValidators([Validators.required]);
                        this.involvedPersonFormGroup.get('MentealimpairDetail').updateValueAndValidity();
                    } else {
                        this.dangerAppearance = false;
                        this.involvedPersonFormGroup.get('MentealimpairDetail').clearValidators();
                        this.involvedPersonFormGroup.get('MentealimpairDetail').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'MentealillnessDetail') {
                this.involvedPersonFormGroup.get('Mentealillness').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === 'yes') {
                        this.dangerSign = true;
                        this.involvedPersonFormGroup.get('MentealillnessDetail').setValidators([Validators.required]);
                        this.involvedPersonFormGroup.get('MentealillnessDetail').updateValueAndValidity();
                    } else {
                        this.dangerSign = false;
                        this.involvedPersonFormGroup.get('MentealillnessDetail').clearValidators();
                        this.involvedPersonFormGroup.get('MentealillnessDetail').updateValueAndValidity();
                    }
                });
            }
        } else if (state === true) {
            this.involvedPersonFormGroup.get(inputfield).disable();
        } else if (state === false && manditory === 'notManditory') {
            this.involvedPersonFormGroup.get(inputfield).enable();
        }
    }

    onRelationShipToROChange(event: any, i: number) {
        // if (this.isDjs) {
        //     if (event.value === 'father' || event.value === 'guardian' || event.value === 'mother') {
        //         this.isAddressReq = true;
        //     } else {
        //         this.isAddressReq = false;
        //     }
        // }
        if (this.agency === 'CW') {
            const rolegrp = <FormArray>this.involvedPersonFormGroup.get('personRole');
            rolegrp.controls.forEach(element => {
                element.patchValue({
                    relationshiptorakey: event.value
                });
            });
        }
    }

    relationShipToRO(event: any, i: number) {

        console.log('D-07265: this.isDjs' + this.isDjs);
        if (this.isDjs) {
            this.personsRoleSelected(event.value, i);
        } else {
            if (this.validateLegalGuardian(event.value)) {
                this._alertService.error('Legal Guardian is always the adult (18 and above) and cannot be a child.');
            }
            if (['BIOCHILD', 'CHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC'].includes(event.value)) {
                this.isChild = true;
                this.filterrelationshipByRole('child');
            } else {
                this.isChild = false;
                this.filterrelationshipByRole('LG');
            }
            const personRoleArray = <FormArray>this.involvedPersonFormGroup.controls.personRole;
            const _personRole = personRoleArray.controls[i];
            _personRole.patchValue({ description: this.roles[event.value] });
            const roleCount = personRoleArray.controls.filter((res) => res.value.rolekey === event.value);
            if (roleCount.length > 1) {
                this.deleteRole(i);
                this._alertService.error('Role is already Added');
            } else {
                if (event.value !== 'RA' && event.value !== 'PA' && event.value !== 'RC' && event.value !== 'CLI' && event.value !== '2085') {
                    const raCount = personRoleArray.controls.filter((res) => res.value.hidden === false);
                    // if (raCount.length === 0) {
                    _personRole.patchValue({ hidden: false });
                    _personRole.get('relationshiptorakey').setValidators([Validators.required]);
                    _personRole.get('relationshiptorakey').updateValueAndValidity();
                    // } else {
                    // _personRole.patchValue({ hidden: true });
                    // _personRole.get('relationshiptorakey').clearValidators();
                    // _personRole.get('relationshiptorakey').updateValueAndValidity();
                    // }
                } else {
                    _personRole.patchValue({ hidden: true });
                    _personRole.get('relationshiptorakey').clearValidators();
                    _personRole.get('relationshiptorakey').updateValueAndValidity();
                }
            }

            // D-07962 Start
            if (event.value === 'CHILD') {
                console.log('1');
                if (this.changeRelationShipOnChild) {
                    console.log('2');
                    _personRole.get('relationshiptorakey').setValue('SELF');
                }
            }
            // D-07962 End
        }
    }
    personsRoleSelected(roleValue: any, i: number) {
        const personRoleArray = <FormArray>this.involvedPersonFormGroup.controls.personRole;
        const _personRole = personRoleArray.controls[i];
        const roleList = this._dataStoreService.getData('role');
        const roleobj = roleList.find(item => item.value === roleValue);
        _personRole.patchValue({ isprimary: 'true', description: roleobj.text });
        if (roleValue !== 'RA' && roleValue !== 'PA' && roleValue !== 'RC' && roleValue !== 'CLI' && roleValue !== 'Youth') {
            _personRole.patchValue({ hidden: false });
            _personRole.get('relationshiptorakey').setValidators([Validators.required]);
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        } else {
            _personRole.patchValue({ hidden: true });
            _personRole.get('relationshiptorakey').clearValidators();
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        }
        this.isDobRequired(roleValue);

        if (roleValue !== 'Youth') {
            this.isDjsYouth = false;
            if (this.addEditLabel !== 'Edit') {
                this.isAddressReq = false;
            }
        } else {
            this.isAddressReq = true;
            this.isDjsYouth = false;
        }

        // D-07265 Start
        if (roleValue !== 'Youth' || roleValue === 'Youth') {
            this.involvedPersonFormGroup.get('Dangerousself').clearValidators();
            this.involvedPersonFormGroup.get('Dangerousself').updateValueAndValidity();
            this.involvedPersonFormGroup.get('Dangerousworker').clearValidators();
            this.involvedPersonFormGroup.get('Dangerousworker').updateValueAndValidity();
            this.involvedPersonFormGroup.get('Mentealimpair').clearValidators();
            this.involvedPersonFormGroup.get('Mentealimpair').updateValueAndValidity();
            this.involvedPersonFormGroup.get('Mentealillness').clearValidators();
            this.involvedPersonFormGroup.get('Mentealillness').updateValueAndValidity();
        }
        // D-07265 End
    }
    fileChangeEvent(file: any) {
        this.beofreImageCropeHide = true;
        this.afterImageCropeHide = true;
        this.imageChangedEvent = file;
        this.isDefaultPhoto = false;
        this.isImageLoadFailed = false;
        this.isImageHide = true;
    }
    imageCropped(file: File) {
        this.progress.percentage = 0;
        this.croppedImage = file;
        const imageBase64 = this.croppedImage;
        const blob = this.dataURItoBlob(imageBase64);
        this.finalImage = new File([blob], 'image.png');
        this.saveImage(this.finalImage);
    }
    imageLoaded() { }
    loadImageFailed() {
        this.isImageLoadFailed = true;
        this._alertService.error('Image failed to upload');
    }
    dataURItoBlob(dataURI) {
        const binary = atob(dataURI.split(',')[1]);
        const array = [];
        for (let i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: 'image/png'
        });
    }
    saveImage(data: any) {
        this._uploadService
            .upload({
                url: AppConfig.baseUrl + '/' + NewUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&srno=userprofile',
                headers: new HttpHeaders()
                    .set('access_token', this.token.id)
                    .set('srno', 'userprofile')
                    .set('ctype', 'file'),
                filesKey: ['file'],
                files: data,
                process: true
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        this.progress.percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        this.userProfilePicture = response.data;
                    }
                },
                (err) => {
                    console.log(err);
                }
            );
    }
    addNewRole() {
        const control = <FormArray>this.involvedPersonFormGroup.controls['personRole'];
        control.push(this.createFormGroup(true));
    }
    deleteRole(index: number) {
        const control = <FormArray>this.involvedPersonFormGroup.controls['personRole'];
        control.removeAt(index);
    }
    createFormGroup(isHideRA) {
        if (this.isCW) {
            return this._formBuilder.group({
                rolekey: ['', Validators.required],
                description: [''],
                isprimary: ['true', Validators.required],
                relationshiptorakey: [''],
                hidden: [isHideRA]
            });
        }
        return this._formBuilder.group({
            rolekey: ['', Validators.required],
            description: [''],
            isprimary: ['', Validators.required],
            relationshiptorakey: [''],
            hidden: [isHideRA]
        });
    }
    setFormValues() {
        this.involvedPersonFormGroup.setControl('personRole', this._formBuilder.array([]));
        const control = <FormArray>this.involvedPersonFormGroup.controls.personRole;
        this.personRole.forEach((x) => {
            control.push(this.buildPersonRoleForm(x));
        });
    }
    private buildPersonRoleForm(x): FormGroup {
        return this._formBuilder.group({
            rolekey: x.rolekey,
            isprimary: x.isprimary,
            relationshiptorakey: x.relationshiptorakey,
            hidden: false, // x.hidden,
            description: ['']
        });
    }
    checkPrimaryRole(i) {
        const personRoleArray = <FormArray>this.involvedPersonFormGroup.controls.personRole;
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach((x) => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        if (primaryCount > 1) {
            this._alertService.error('Primary role for this person is already selected');
            _personRole[i].patchValue({ isprimary: 'false' });
        }
    }
    validatePrimaryRole() {
        const personRoleArray = <FormArray>this.involvedPersonFormGroup.controls.personRole;
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach((x) => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        // if (primaryCount === 1) {
        //     return true;
        // } else {
        //     this._alertService.error('Please Select One Primary Role Type');
        //     if (this.isCW) {
        //         return true;
        //     } else {
        //         this._alertService.error('Please Select One Primary Role Type');
        //         return false;
        //     }
        // }
        // quick fix on primary rle check, this will be fixed when person profile redeisnged
        return true;
    }
    private validateLegalGuardian(role): boolean {
        if (role === 'LG' && this.ProtectionService) {
            const age = this.getAge(this.involvedPersonFormGroup.controls['Dob'].value);
            if (age >= 18) {
                return false;
            }
            return true;
        }
    }
    isEmptyOrNull(obj: string) {
        if (obj === undefined || obj === null) {
            return '';
        } else {
            return obj;
        }
    }

    checkChildStatus(control) {
        if (control) {
            const controlValue = this.involvedPersonFormGroup.get(control).value;
            // this.clearNewbornvalidation(controlValue);
            if (controlValue) {
                this._dataStoreService.setData('CPSFlag', 'disable');
                const dob = this.involvedPersonFormGroup.get('Dob').value;
                let ageDays = 0;
                if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
                    const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
                    ageDays = moment().diff(rCDob, 'days');
                }
                if (ageDays >= 31) {
                    this._alertService.warn('Person must be 30 days or less in age');
                    this.involvedPersonFormGroup.get(control).setValue(false);
                    control = '';
                } else {
                    if (control === 'drugexposednewbornflag') {
                        this.isSubstance = controlValue;
                        this.involvedPersonFormGroup.get('otherSubstance').reset();
                        this.involvedPersonFormGroup.get('substanceClass').reset();
                        // this.isSubstance = !this.isSubstance;
                    } else {
                        // this.isSubstance = false;
                    }
                }
            } else {
                this._dataStoreService.setData('CPSFlag', 'enable');
                // if (control === 'drugexposednewbornflag') {
                //     this.isSubstance = false;
                // }
            }
            this.isSubstance = (control === 'drugexposednewbornflag') ? controlValue : this.isSubstance;
        }
        // console.log(controlValue);
    }

    calculateAge(dob) {
        // const dob = this.selectedPerson.dob;
        let age = 0;
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            age = moment().diff(rCDob, 'years');
            if (this.isCW) {
                if (age <= 17) {
                    if (this.storeMaritalStatus === '' || this.storeMaritalStatus === null || this.storeMaritalStatus === undefined) {
                        this.involvedPersonFormGroup.controls['maritalstatus'].setValue('S');
                    }
                } else {
                    if (this.storeMaritalStatus === '' || this.storeMaritalStatus === null || this.storeMaritalStatus === undefined) {
                        this.involvedPersonFormGroup.controls['maritalstatus'].setValue('');
                    }
                }
            } else {
                if (age <= 15) {
                    this.involvedPersonFormGroup.controls['maritalstatus'].setValue('S');
                } else {
                    this.involvedPersonFormGroup.controls['maritalstatus'].setValue('');
                }
            }
            const days = moment().diff(rCDob, 'days');
            this.enableSafeHaven = (days <= 10);
            this.enableSEN = (days <= 30);
        }
        return age;
    }

    getPersonLatestDetails(involvedPerson) {
        // this._commonHttpService
        //     .getSingle(
        //         {
        //             method: 'get',
        //             where: {
        //                 personid: personid
        //             }
        //         },
        //         NewUrlConfig.EndPoint.Intake.PersonList + '?filter'
        //     )
        //     .subscribe(result => { });
    }

    filterRoles(item): boolean {
        if (this.selectedPersonsage < 22) {
            if (item.rolegrp === 'C' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        } else if (this.selectedPersonsage >= 22) {
            if (item.rolegrp === 'A' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        }
    }
    private loadSubstance() {
        const babysubstanceList = ['BOTH', 'BPD', 'BPCP', 'BMTD', 'BMJA', 'BHOI', 'BESY', 'BCOC', 'BBS', 'BAS', 'FASD'];
        const source = this._commonHttpService.getArrayList(
            {
                where: { teamtypekey: 'CW', referencetypeid: 55 },
                method: 'get',
                nolimit: true
            },
            CommonUrlConfig.EndPoint.Intake.adultScreenRiskMeasure
        ).map((result) => {
            return {
                response: result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                ),
                babylist: result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                ).filter(
                    (item) => babysubstanceList.includes(item.value)
                )
            };
        }).share();
        this.substanceClass$ = source.pluck('response');
        this.babySubstanceClass$ = source.pluck('babylist');
    }
    changeSubtance(item) {
        this.substances = item.map((res) => {
            return {
                subtancekey: res,
                othersubtance: ''
            };
        });
    }

    filterrelationshipByRole(role) {
        const childrelationList = ['BIOBR', 'BGSISTR', 'BFRND', 'BFRNDX', 'DACRCHLD', 'fosterchild', 'FRND', 'GFRND',
            'GFRNDX', 'HLFBR', 'HLFSISTR', 'LEGLBR', 'LGLSISTR', 'MTNLCN', 'MATNLNPW',
            'MATNLNC', 'NBHR', 'NORLTN', 'OTHER', 'PRNTLCN', 'PRNTLNPW', ' PRNTLNC', 'PUTCHLD', 'RELOTHR', 'RESDNT', 'SELF', 'SNFOPAR', 'STPBR', 'STPSISTR',
            'STUDNT', 'UNKNWN'];
        if (this.agency === 'CW') {
            switch (role) {
                case 'child':
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems.filter(item => childrelationList.includes(item.value));
                    break;

                default:
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems.filter(item => true);
                    break;
            }
        }
    }
    test(event) {
        console.log(event, this.involvedPersonFormGroup.value);
    }

    setcitizenalenageflag(event) {
        console.log(event, this.involvedPersonFormGroup.value);
        this.involvedPersonFormGroup.patchValue({
            isqualifiedalien: (event.value === '1') ? 1 : 0
        });
    }

    showQualifiedAlienQuestion(): boolean {
        let toReturn = false;
        const dobControl = this.involvedPersonFormGroup.controls['Dob'];
        if (dobControl && this.agency === 'CW') {
            if (dobControl.value) {

                if (this.calculateAge(dobControl.value) <= 18) {
                    toReturn = true;
                }
            }
        }
        return toReturn;
    }

    copyAddress(involvedPerson: InvolvedPerson, action) {
        if (action === 1) {
            this.personAddressInput = this.copypersonAddressInput;
            this.isnoCopyAddress = true;
            this.addPerson(involvedPerson);
            (<any>$('#address-add-popup')).modal('hide');
        } else {
            this.isnoCopyAddress = false;
            this.personAddressInput = [];
            this.addPerson(involvedPerson);
            (<any>$('#address-add-popup')).modal('hide');
            (<any>$('#intake-addperson')).modal('show');

        }
    }

    // reset collateral chkbox
    resetCollateralChkbox() {
        if (this.isCW) {
            this.involvedPersonFormGroup.get('iscollateralcontact').reset();
            this.resetRoleTabRadioBtn(null);
        }
    }
    // reset roletab radio btn
    resetRoleTabRadioBtn(paramValue = null) {
        if (this.isCW) {
            let value = false;
            if (paramValue !== null) {
                value = paramValue;
            } else {
                value = this.involvedPersonFormGroup.get('iscollateralcontact').value;
            }
            const radioBtnAry = ['ishousehold', 'Mentealillness', 'Mentealimpair', 'Dangerousworker', 'Dangerousself'];
            if (value) {
                radioBtnAry.forEach(ctrlName => {
                    this.involvedPersonFormGroup.get(ctrlName).clearValidators();
                    this.involvedPersonFormGroup.get(ctrlName).updateValueAndValidity();
                });
                this.involvedPersonFormGroup.get('ishousehold').reset();
                this.isCheckedCollateral = false;
            } else {
                radioBtnAry.forEach(ctrlName => {
                    this.involvedPersonFormGroup.get(ctrlName).setValidators([Validators.required]);
                    this.involvedPersonFormGroup.get(ctrlName).updateValueAndValidity();
                });
                this.isCheckedCollateral = true;
            }
        }
    }

}
