import {
    AfterContentInit,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit
} from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subscription } from 'rxjs/Subscription';
import { AppConstants } from '../../../@core/common/constants';
import { ControlUtils } from '../../../@core/common/control-utils';
import { ObjectUtils } from '../../../@core/common/initializer';
import { AppUser } from '../../../@core/entities/authDataModel';
import {
    DropdownModel,
    PaginationRequest,
    PaginationInfo
} from '../../../@core/entities/common.entities';
import {
    CommonDropdownsService,
    DataStoreService,
    GenericService,
    SessionStorageService
} from '../../../@core/services';
import { AlertService } from '../../../@core/services/alert.service';
import { AuthService } from '../../../@core/services/auth.service';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import {
    Prior,
    RoutingUser
} from '../../cjams-dashboard/_entities/dashBoard-datamodel';
import { IntakeStore, IntakeUtils } from '../../_utils/intake-utils.service';
import { NewUrlConfig } from '../newintake-url.config';
import { IntakeConfigService } from './intake-config.service';
import {
    IntakeTabConfig,
    IntakeTabOrderConfig,
    DJS_TAB_ORDER
} from './intake-tab-config';
import {
    IntakeStoreConstants,
    MyNewintakeConstants
} from './my-newintake.constants';
import {
    CpsDocInput,
    CrossReference,
    CrossReferenceSearchResponse,
    FinalIntake,
    GeneralNarative,
    IntakeDATypeDetail,
    IntakePurpose,
    IntakeTemporarySaveModel,
    InvolvedPerson,
    Narrative,
    NarrativeIntake,
    Notes,
    ReviewStatus,
    Sdm,
    SdmData,
    SubType,
    IntakeCommunication
} from './_entities/newintakeModel';
import {
    Agency,
    ApproveIntakeResponse,
    CourtDetails,
    DATypeDetail,
    DelayForm,
    DelayResponse,
    General,
    GeneratedDocuments,
    IntakeAppointment,
    IntakeScreen,
    IntakeService,
    IntakeServiceSubtype,
    Person,
    SAOResponse
} from './_entities/newintakeSaveModel';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { Subject } from 'rxjs/Subject';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';
import { DatePipe } from '@angular/common';
declare var require: any;
declare var html2pdf: any;

declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'my-newintake',
    templateUrl: './my-newintake.component.html',
    styleUrls: ['./my-newintake.component.scss']
})
export class MyNewintakeComponent
    implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {
    departmentActionIntakeFormGroup: FormGroup;
    requestSent:boolean=false;
    generalResourceFormGroup: FormGroup;
    saoResponseForm: FormGroup;
    dispositionDelayForm: FormGroup;
    draftReasonFormGroup: FormGroup;
    intakeCommunication: IntakeCommunication[] = [];
    intakeServices: IntakeService[];
    // intakeId: string;
    sdmDispositionCall: string;
    // narrativeDetails: Narrative = new Narrative();
    // addPerson: string;
    draftId: string;
    btnDraft: boolean;
    maxReceivedDate: any;
    intakeNumber: string;
    current_route: string;
    intakeNumberNarrative: string;
    isSDMdisplayed: boolean;
    isVoluntaryPlacement = false;
    isDisplayOtherAgency: boolean;
    general: General = new General();
    generalRecievedDate: DelayResponse;
    reviewstatus: ReviewStatus = new ReviewStatus();
    cwIntakeWorkerButton: boolean;
    private pageSubject$ = new Subject<number>();
    narrative: NarrativeIntake = new NarrativeIntake();
    paginationInfo: PaginationInfo = new PaginationInfo();
    // addAttachement: AttachmentIntakes[] = [];
    // intakeScreen: IntakeScreen = new IntakeScreen();
    addedCrossReference: CrossReferenceSearchResponse[] = [];
    selectedYouth: InvolvedPerson;
    focusPerson: InvolvedPerson;
    // addedPersons: InvolvedPerson[] = [];
    // addedEntities: InvolvedEntitySearchResponse[] = [];
    addedIntakeDATypeDetails: IntakeDATypeDetail[] = [];
    preIntakeSupDicision = '';
    // evalFields: EvaluationFields[];
    communicationFields: Notes[] = [];
    saoResponse: SAOResponse;
    petitionDetails: any[] = [];
    scheduledHearings: any[] = [];
    isPetionDetailsSubmited = false;
    courtDetails: CourtDetails;
    intakeAppointment: IntakeAppointment[];
    // recordings: ContactTypeAdd[] = [];
    // Person: Person[] = [];
    // addNarrative: Narrative;
    addSdm: Sdm;
    selectteamtypekey: string;
    intakeServiceGrid: boolean;
    intakeInfoNreffGrid: boolean;
    intakeservice: IntakeService[] = [];
    intakeservicesubtype: IntakeServiceSubtype[] = [];
    intakeInfoReffTypes = [];
    otherAgencyControlName: AbstractControl;
    isDjs = false;
    isAS = false;
    // isCWSelected = false;
    checkValidation: boolean;
    saveIntakeBtn: boolean;
    // intakeType: string;
    // disposition: DispostionOutput[];
    purposeList: IntakePurpose[] = [];
    // createdCases: ComplaintTypeCase[];
    selectedPurpose: DropdownModel;
    selectedAgency: DropdownModel;
    roleValue = false;
    isPurposeNotCPS = false;
    isRcvdDtBeforeDob = false;
    pdfFiles: {
        fileName: string;
        images: { image: string; height: any; name: string }[];
    }[] = [];
    resourcePoplabel: string;
    roleId: AppUser;
    submitResourceObject: GeneralNarative;
    downloadInProgress = false;
    reviewStatus: string;
    cpsdocData: CpsDocInput = new CpsDocInput();
    // showViewAssessment: boolean;
    // safeHavenAssessmentScore: number;
    // kinshipAssessmentScore: number;
    viewKinship: boolean;
    viewSafeHaven: boolean;
    isKinshipSafehaven: string;
    serviceCheckboxId: any;
    delayFormData: DelayForm;
    checkConditionForDelay = false;
    zipCode: string;
    supervisorsList: RoutingUser[];
    intakersList: RoutingUser[];
    selectedSupervisor: string;
    selectedIntaker: string;
    isManualRouting: string;
    isKinship = true;
    isSENflag = false;
    finalIntake = new FinalIntake();
    accessStatus: boolean;
    subServiceTypes: SubType[];
    // timeLeft: string;
    selectedIntakeServices: IntakeService[] = [];
    viewAscrs: boolean;
    // ascrsScore: number;
    isCW = false;
    isPreIntake = false;
    isCLW = false;
    clwStatus: any;
    signedOffDate: string;
    // intakerWorkerId = '';
    isPurposeWithAgency = true;
    // IWtoAssign: string;
    // preIntakeDisposition: PreIntakeDisposition;
    genratedDocumentList: GeneratedDocuments[];
    // intakedetailList: any;
    pathwayChange = false;
    initialSdm = new SdmData();
    showSubmit: boolean;
    // scoresSubject$: Subject<AssessmentScores> = new Subject<AssessmentScores>();
    kinshipNavigator: string = null;
    priorItem$: Observable<Prior[]>;
    isPurposeChanged = false;
    notification: string;
    currentLanguage: string;
    intakeSourceList$: Observable<DropdownModel[]>;
    intakeCommunication$: Observable<DropdownModel[]>;
    intakeAgencies$: Observable<DropdownModel[]>;
    intakePurpose$: Observable<DropdownModel[]>;
    warranttypes: any[] = [];
    saveasdraftReason: any[] = [];
    youthStatus: string;
    personid: string;
    currentPurpose: any;
    newPurpose: any;
    purposeSource: string;
    currentPurposeValue: string;
    recognizing: boolean;
    speechData: string;
    speechRecogninitionOn: boolean;
    voluntaryPlacementDropDown: any;
    // assessmentInput: IntakeDATypeDetail;
    // for HTML BINDING
    ROUTED_CLW = AppConstants.INTAKE_CONSTANTS.ROUTED_CLW;
    SAO_DOUCUMENT_GENERATED =
        AppConstants.INTAKE_CONSTANTS.SAO_DOUCUMENT_GENERATED;
    SAO_RESPONSE_CLOSED = AppConstants.INTAKE_CONSTANTS.SAO_RESPONSE_CLOSED;
    SAO_RESPONSED = AppConstants.INTAKE_CONSTANTS.SAO_RESPONSED;
    HEARING_SCHEDULED = AppConstants.INTAKE_CONSTANTS.HEARING_SCHEDULED;
    RESTITUTION_COMPLETED = AppConstants.INTAKE_CONSTANTS.RESTITUTION_COMPLETED;
    PETITIONS_SUBMITED = AppConstants.INTAKE_CONSTANTS.PETTIION_SUBMITED;
    HEARING_DETAILS = AppConstants.INTAKE_CONSTANTS.HEARING_SCHEDULED;
    COURT_ACTIONS_TO_BE_UPDATED =
        AppConstants.INTAKE_CONSTANTS.COURT_ACTIONS_TO_BE_UPDATED;
    appevent: any;
    modal: any;
    closeCWCae: boolean;
    isClearenceHistory: boolean;

    WAITNG_FOR_COURT_HEARING =
        AppConstants.INTAKE_CONSTANTS.WAITNG_FOR_COURT_HEARING;
    SAO_CLOSED = AppConstants.INTAKE_CONSTANTS.SAO_CLOSED;
    WAITNG_FOR_CASE_WORKER =
        AppConstants.INTAKE_CONSTANTS.WAITNG_FOR_CASE_WORKER;
    currentDate = new Date();
    clw = {
        documentGenerated: false,
        saoResponsed: false,
        pettionSubmited: false,
        hearingScheduled: false,
        hearingDetailUpdated: false,
        courtDetailUpdated: false
    };
    INTAKE_TABS = AppConstants.INTAKE_TABS;

    tabDisplay = {
        saoResponse: false,
        petitionDetails: false,
        scheduleHearings: false,
        courtActions: false,
        narrative: false,
        personsInvolved: false,
        sdm: false,
        evaluationFields: false,
        appointments: false,
        entities: false,
        serviceSubType: false,
        complaintsType: false,
        assessments: false,
        crossReference: false,
        notes: false,
        djsNotes: false,
        intakeReferral: false,
        attachments: false,
        disposition: false,
        decision: false,
        legalAction: false
    };
    isRFS = false;
    store: any;
    agencyTabOrder: {
        id: string;
        title: string;
        name: string;
        role: string[];
        route: string;
    }[] = [];
    intakeStore: IntakeStore;
    loadHTML: boolean;
    intake: any;
    commonInvolvedPersons: any;
    referralSubmission: boolean;
    dataStoreSubscription: Subscription;
    caseCreated = [];
    attachmentCreated = [];
    attachementRequired = [];
    categoryList = [];
    missingSubCategory = [];
    subCategoryClassificationType$: Observable<any[]>;
    subCategoryList: any[];
    subType = [];
    approveIntakeResponse: ApproveIntakeResponse[];
    isIntakeWorker = false;
    userRole: AppUser;
    intakeCountyList$: Observable<DropdownModel[]>;
    intakeCountyList = [];
    djsValidationMessages: any;
    narrativestatus: any;
    intakeErrorMessage: string;
    selectedCheckbox: string;
    selectedServiceCaseId: string = null;
    serviceCaseNumber: any;
    exitingServiceCaseList: any;
    familyCaseData: any; // holds approve intakes response which is a family case
    isIntakeFromSupervisor = false;
    serviceTypeRequested: any;
    suggestedTypeResource: any;
    isEVPA = false;
    autosaveTimmer: Subscription;
    saveInProgress = false;
    isrestricteditem = false;
    canUserRestrictItems = false;
    intakeChessieid = null;

    mergeUsersList: RoutingUser[] = [];
    getUsersList: RoutingUser[];
    selectedteamid: string;
    originalUserList: RoutingUser[];
    isSupervisor: boolean;
    selectedResponsibilityType: string;
    zipCodeIndex: number;
    selectedPerson: any;
    responsibilityTypeDropdownItems$: Observable<DropdownModel[]>;
    teamForm: FormGroup;
    teamList: Array<any> = [];
    teamid: string;
    teamtypekey: any;
    seletedUserData: any;
    assignServiceCaseForm: FormGroup;
    serviceCaseResponse: any;
    programArea: any[];
    workersList: any[] = [];
    programSubArea: any[];
    narrativeUpdatedTime: any;
    cpsResponseOffset: number; // in hours
    dispcode: any;
    unknown: boolean;
    submisionHistory: any;
    needSubmissionHistory = true;
    intakStatus: string;
    isIndependentLiving: boolean;
    isIndependentLivingInValid: boolean;
    constructor(
        private _router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private _commonDDService: CommonDropdownsService,
        private speechRecognizer: SpeechRecognizerService,
        private _genericServiceNarative: GenericService<GeneralNarative>,
        private _dataStoreService: DataStoreService,
        private _sessionStorage: SessionStorageService,
        private _intakeConfig: IntakeConfigService,
        private cd: ChangeDetectorRef,
        private _intakeService: IntakeUtils,
        private dropdownService: CommonDropdownsService,
        private _speechRecognitionService: SpeechRecognitionService,
        private _datePipe: DatePipe
    ) {
        // this._dataStoreService.clearStore();
        this.speechRecogninitionOn = false;
        this.speechData = '';
        this.recognizing = false;
        this.draftId = this._sessionStorage.getObj(
            IntakeStoreConstants.intakenumber
        );
        this._dataStoreService.setData(
            IntakeStoreConstants.intakenumber,
            this.draftId
        );
        this.store = this._dataStoreService.getCurrentStore();
        this.intakeStore = this._sessionStorage.getObj('intake');
        this._dataStoreService.setData(
            IntakeStoreConstants.IntakeAction,
            this.intakeStore.action
        );
        this.intakeNumber = this.intakeStore.number;
        this.route.data.subscribe(response => {
            this.intake = response.intake;
            this._intakeConfig.setPurposeList(response.purposeList);
            this._intakeConfig.setCommunicationList(response.communicationList); // @TM: communication drop-down resolver
            this.purposeList = this._intakeConfig.getPurposeList();
            this.intakeCommunication = this._intakeConfig.getCommunicationList();
            console.log('on resolve intake', this.intake);
            this.fixNarrativeHistoryClearanceText();
            console.log('on resolve purpose', this.purposeList);
            console.log('on resolve involed persons', response.involvedPersonsList);
            this.commonInvolvedPersons = response.involvedPersonsList;
            // Set the received date on data store for other tabs to access
            if (this.intake && this.intake.data && this.intake.data[0]) {
                this._dataStoreService.setData(
                    IntakeStoreConstants.receivedDate,
                    this.intake.data[0].daterecieved
                );
                this.intakStatus = this.intake.data[0].reviewstatus;
                this._dataStoreService.setData(
                    IntakeStoreConstants.INTAKE_STATUS,
                    this.intake.data[0].reviewstatus
                );
            }
        });
        this._intakeService.notesUpdated$.subscribe(_ => {
            this.processRecordingsList();
        });
    }

    ngOnInit() {

        // this.intakeCommunication$ = this.intakeCommunication.map(
        //     res =>
        //         new DropdownModel({
        //             text: res.description,
        //             value: res.intakeservreqinputtypeid
        //         })
        // );

        this.loadDroddowns();
        console.log('time - ' + new Date());
        this.current_route = 'narrative';
        this.userRole = this._authService.getCurrentUser();
        if (
            this.intakeStore.action === 'add' &&
            this.userRole.role.name === AppConstants.ROLES.SUPERVISOR
        ) {
            this.isIntakeFromSupervisor = true;
        }
        this.isIntakeWorker =
            this.userRole.role.name === AppConstants.ROLES.INTAKE_WORKER;
        this.dataStoreSubscription = this._dataStoreService.currentStore.subscribe(
            storeObj => {
                if (
                    storeObj[IntakeStoreConstants.statusChanged] &&
                    this.isDjs
                ) {
                    this._dataStoreService.setData(
                        IntakeStoreConstants.statusChanged,
                        false
                    );
                    this.getYouthStaus();
                }
            }
        );
        this.referralSubmission = false;
        const __this = this;
        $('#docu-View').on('hidden.bs.modal', function () {
            __this._dataStoreService.setData('loadhtml', false);
        });
        this.buildFormGroup();


        this.loadServiceTypeDropDown();
        this.loadSuggestTypeDropDown();

        this.roleId = this._authService.getCurrentUser();
        console.log(this.roleId);
        if (
            this.roleId.user.userprofile.teammemberassignment.teammember
                .roletypekey === 'CWKN'
        ) {
            this.isKinship = false;
        }
        if (
            this.roleId.user.userprofile.teammemberassignment.teammember
                .roletypekey === 'CWKA'
        ) {
            this.isKinship = false;
        }
        if (this.roleId.user.userprofile.teamtypekey === 'DJS') {
            this.isDjs = true;
        }
        if (this.roleId.user.userprofile.teamtypekey === 'AS') {
            this.departmentActionIntakeFormGroup.controls[
                'countyid'
            ].setValidators([Validators.required]);
            this.departmentActionIntakeFormGroup.controls[
                'countyid'
            ].updateValueAndValidity();
            this.isAS = true;
            if (
                this.roleId.user.userprofile.userprofileaddress &&
                this.roleId.user.userprofile.userprofileaddress.length
            ) {
                this.departmentActionIntakeFormGroup.controls[
                    'countyid'
                ].patchValue(
                    this.roleId.user.userprofile.userprofileaddress[0].countyid
                );
            }
        }
        if (this.roleId.user.userprofile.teamtypekey === 'CW') {
            this.isCW = true;
            // if (
            //     this.roleId.user.userprofile.userprofileaddress &&
            //     this.roleId.user.userprofile.userprofileaddress.length
            // ) {
            //     this.departmentActionIntakeFormGroup.controls[
            //         'countyid'
            //     ].patchValue(
            //         this.roleId.user.userprofile.userprofileaddress[0].countyid
            //     );
            // }
        }
        if (
            this.roleId &&
            this.roleId.role &&
            this.roleId.role.name === AppConstants.ROLES.COURT_WORKER
        ) {
            this.isCLW = true;
        }
        this.listPurpose({
            text: '',
            value: this._authService.getAgencyName()
        });
        if (!this.draftId) {
            this.loadDefaults();
            this.listService({ text: 'Select', value: '' });
        }
        this.otherAgencyControlName = this.departmentActionIntakeFormGroup.get(
            'otheragency'
        );
        this.communicationFields = this.store[
            IntakeStoreConstants.communicationFields
        ];
        // this.createdCases = this.store[IntakeStoreConstants.createdCases];
        // this.reviewStatus = this.store[IntakeStoreConstants.reviewStatus];
        if (
            this.roleId.role.name === AppConstants.ROLES.SUPERVISOR &&
            (this.reviewStatus === 'Review' || this.reviewStatus === 'Reopen')
        ) {
            this.departmentActionIntakeFormGroup.controls.Agency.disable();
            this.departmentActionIntakeFormGroup.controls.Purpose.disable();
            this.departmentActionIntakeFormGroup.controls.InputSource.disable();
            this.departmentActionIntakeFormGroup.controls.RecivedDate.disable();
            this.departmentActionIntakeFormGroup.controls.isOtherAgency.disable();
        }
        this.departmentActionIntakeFormGroup.patchValue({
            Agency: this._authService.getAgencyName()
        });
        if (this.intakeStore.action === 'edit') {
            this.populateIntake();
        } else if (this.intakeStore.action === 'view' && this.isDjs) {
            this.populateIntake();
        } else {
            if (this.isDjs) {
                this.onReceivedDateChange();
                this.initializeDJSTabs();
            } else {
                this.initializeTabs();
            }

            this._dataStoreService.setData(
                IntakeStoreConstants.childfatality,
                'no'
            );
        }
        this.listenForPersonChange();
        this.getSubCategory();
        this.currentLanguage = 'en-US';
        this.speechRecognizer.initialize(this.currentLanguage);
        this.notification = null;
        if (this.isDjs) {
            this._intakeConfig.setintakeAction(this.intakeStore.action);
            if (
                this.addedIntakeDATypeDetails &&
                this.addedIntakeDATypeDetails.length > 0
            ) {
                const datypedetails = this.addedIntakeDATypeDetails[0];
                if (
                    this.intakeStore.action === 'edit' &&
                    datypedetails.DAStatus === 'Approved' &&
                    this.userRole.role.name === AppConstants.ROLES.SUPERVISOR
                ) {
                    this._intakeConfig.setintakeAction('view');
                    (<any>$(':button')).prop('disabled', true);
                }
            }
        }
        this.checkSEN();
        this.listenForEAVPA();
        this.departmentActionIntakeFormGroup.get('RecivedDate')
            .valueChanges.subscribe(receiveddate => {
                if (receiveddate instanceof Date) {
                    this._dataStoreService.setData(
                        IntakeStoreConstants.receivedDate,
                        receiveddate
                    );
                } else if (receiveddate && receiveddate.format()) {
                    this._dataStoreService.setData(
                        IntakeStoreConstants.receivedDate,
                        receiveddate.format()
                    );
                }
            });

        if ((!this.reviewstatus.status) || (this.reviewstatus.status === '')) {
            this.autosaveTimmer = Observable.interval(120000)
                .subscribe((val) => {
                    console.log('called');
                    this.autoSaveAsDraft();
                    // this.draftIntake(this.departmentActionIntakeFormGroup.value, 'DRAFT', false);
                });
        }
        if (this.isCW) {
            this.listenForQuickAddPerson();
        }

        // Decide who can restrict cases
        if (
            this.userRole.role.name === AppConstants.ROLES.SUPERVISOR
        ) {
            this.canUserRestrictItems = true;
        }

        if (this.intakeNumber) {
            this.getIsRestrictItem();
            this.restrictedItemAuditLog();
        }

        this.teamForm = this.formBuilder.group({
            teamid: ['']
        });
        this.assignServiceCaseForm = this.formBuilder.group({
            programkey: [null],
            subprogramkey: [null]
        });
        
        this._dataStoreService.setData('isCPSHistoryClearanceChecked', false);
    }

    fixNarrativeHistoryClearanceText(){
        if (this.intake && Array.isArray(this.intake.data) && this.intake.data.length > 0)        {
            let n: string = this.intake.data[0].jsondata.General.Narrative;
            let h: string = this.intake.data[0].jsondata.General.cpsHistoryClearance;
            if (n) {
                n = n.replace(/(\\n)/g, '<br>');
                n = n.replace(/(\\r)/g, '');
            }
            if (h) {
                h = h.replace(/(\\n)/g, '<br>');
                h = h.replace(/(\\r)/g, '');
            }
            
            
            this.intake.data[0].narrative = n;
            this.intake.data[0].jsondata.General.Narrative = n;
            
            this.intake.data[0].jsondata.General.cpsHistoryClearance = h;
            
        }
    }

    listenForEAVPA() {
        this._intakeConfig.intakeDecision$.subscribe((data) => {
            // this.departmentActionIntakeFormGroup.controls[
            //     'IntakeServiceSubtype'
            //    ].setValue(data);
            let selectedIntakeService = [];
            selectedIntakeService = this.selectedIntakeServices.filter(item => item.description === 'In Home Services');
            if (selectedIntakeService && selectedIntakeService.length > 0) {
                const selectedServiceTypeItem = selectedIntakeService[0].intakeservsubtype.filter(item => item.intakeservsubtypekey === data);
                const removeServiceTypeItem = selectedIntakeService[0].intakeservsubtype.filter(item => item.intakeservsubtypekey === 'VP');
                if (selectedServiceTypeItem) {
                    this.intakeservicesubtype = [];
                    this.intakeservicesubtype = selectedServiceTypeItem;
                    this.isSelectedSubtypeItems(selectedServiceTypeItem[0]);
                    if (removeServiceTypeItem) {
                        this.intakeServiceSubtypeUnchecked(removeServiceTypeItem[0]);
                        this.intakeServiceSubtypeChecked(selectedServiceTypeItem[0]);
                    }
                }
            }
        });
    }

    ngAfterViewInit() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        this.cd.detectChanges();
        if (this.isDjs) {
            if (this.intakeStore.action === 'view') {
                (<any>$(':button')).prop('disabled', true);
            }
        }
    }

    ngAfterContentInit() {
        $('.btnNext').click(function () {
            $('.click-triggers > .active')
                .next('li')
                .find('a')
                .trigger('click');
        });

        $('.btnPrevious').click(function () {
            $('.click-triggers > .active')
                .prev('li')
                .find('a')
                .trigger('click');
        });

        $('#intake-cps-doc').on('shown.bs.modal', () => {
            this.genCpsIntakeDoc('generate');
        });
    }

    getYouthStaus() {
        if (this.selectedYouth) {
            let personid = null;
            if (this.selectedYouth.Pid) {
                personid = this.selectedYouth.Pid.startsWith(
                    AppConstants.PERSON.TEMP_ID
                )
                    ? null
                    : this.selectedYouth.Pid;
            }

            this._intakeService
                .getFocusPersonStatus(personid, null, this.intakeNumber)
                .subscribe(statuses => {
                    const status = statuses.map(s => s.description);
                    this.youthStatus = status.toString();
                    this._dataStoreService.setData(
                        IntakeStoreConstants.youthStatus,
                        status
                    );
                });
        }
    }
    // DJS-017 Complaint Received Date - should be auto-populated with the Received date from the referral screen
    onReceivedDateChange() {
        this.departmentActionIntakeFormGroup
            .get('RecivedDate')
            .valueChanges.subscribe(receiveddate => {
                if (receiveddate instanceof Date) {
                    this._dataStoreService.setData(
                        IntakeStoreConstants.receivedDate,
                        receiveddate
                    );
                } else if (receiveddate && receiveddate.format()) {
                    this._dataStoreService.setData(
                        IntakeStoreConstants.receivedDate,
                        receiveddate.format()
                    );
                }
            });
    }

    navigateToStatusPage() {
        const url = `/pages/person-details/edit/${
            this.selectedYouth.Pid
            }/youth-status`;
        this._router.navigate([url]);
    }

    buildFormGroup() {
        this.departmentActionIntakeFormGroup = this.formBuilder.group(
            {
                Source: [''],
                InputSource: ['', Validators.required],
                RecivedDate: [
                    new Date(),
                    [Validators.required, Validators.minLength(1)]
                ],
                CreatedDate: [new Date()],
                Author: [''],
                LegalGuardian: [''],
                HeadofHousehold: [''],
                IntakeNumber: [''],
                Agency: ['all', Validators.required],
                Purpose: ['', Validators.required],
                countyid: [''],
                IntakeService: [''],
                IntakeServiceSubtype: [''],
                voluntaryPlacementType: [''],
                otheragency: ['', Validators.maxLength(50)],
                isOtherAgency: false,
                islocalreferal: [0],
                referalcomments: [''],
                nonreferalreason: [''],
                queAdditionDate: [
                    new Date(),
                    [Validators.required, Validators.minLength(1)]
                ],
                receiveddelay: [''],
                submissiondelay: [''],
                /* Kinship Navigator */
                servicerequest: [[]],
                suggestedresource: [[]]
            },
            { validator: this.dateFormat }
        );
        this.generalResourceFormGroup = this.formBuilder.group({
            helpDescription: ['']
        });
        this.saoResponseForm = this.formBuilder.group({
            signedOffDate: ['', Validators.required]
        });
        this.dispositionDelayForm = this.formBuilder.group({
            fiveDaysDelay: [''],
            twentyFivedaysDelay: ['']
        });
        this.draftReasonFormGroup = this.formBuilder.group({
            draftReason: ['']
        });
    }
    dateFormat(group: FormGroup) {
        if (
            group.controls.RecivedDate.value !== '' &&
            group.controls.RecivedDate.value !== null
        ) {
            if (group.controls.RecivedDate.value > new Date()) {
                return { futureDate: true };
            }
            return null;
        }
    }
    private loadDroddowns() {

            this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    where: { intakenumber: this.intakeNumber },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.RoutingInfoList
            ).subscribe(data => {
                if (data && Array.isArray(data) && data.length) {
                    const history = data[0].routinginfo;
                    this.submisionHistory = history;
                }
            });
                this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 154, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            )
            .subscribe(data => {
                this.voluntaryPlacementDropDown = data;
            });
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {},
                NewUrlConfig.EndPoint.Intake.CommunicationUrl
            ),
            // this._commonHttpService.getArrayList(
            //     {},
            //     NewUrlConfig.EndPoint.Intake.IntakeServiceRequestInputTypeUrl
            // ),
            this._commonHttpService.getArrayList(
                {},
                NewUrlConfig.EndPoint.Intake.IntakeAgencies
            ),
            this._commonHttpService.getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.MDCountryListUrl + '?filter'
            )
        ])
            .map(result => {
                return {
                    sourceList: result[0].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeservreqinputsourceid
                            })
                    ),
                    // communicationList: result[1].map(
                    //     res =>
                    //         new DropdownModel({
                    //             text: res.description,
                    //             value: res.intakeservreqinputtypeid
                    //         })
                    // ),
                    agenciesList: result[1].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.teamtypekey,
                                additionalProperty: res.ismanualrouting
                            })
                    ),
                    countyList: result[2].map(
                        res =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyid
                            })
                    )
                };
            })
            .share();
        this.intakeSourceList$ = source.pluck('sourceList');
        // this.intakeCommunication$ = source.pluck('communicationList');
        this.intakeCountyList$ = source.pluck('countyList');
        // this.intakeCommunication$.subscribe(data => {
        //     this.intakeCommunication = data;
        // });
        this.intakeCountyList$.subscribe(data => {
            this.intakeCountyList = data;
            if (
                this.roleId.user.userprofile.userprofileaddress &&
                this.roleId.user.userprofile.userprofileaddress.length
            ) {
                const usercounty =  this.intakeCountyList.find(county => county.text === this.roleId.user.userprofile.userprofileaddress[0].county);
                console.log(usercounty, 'usercountyusercountyusercounty');
                if (usercounty) {
                this.departmentActionIntakeFormGroup.controls[
                    'countyid'
                ].patchValue(
                    usercounty.value
                );
                }
            }
        });
        this.intakeAgencies$ = source.pluck('agenciesList');
        this.intakeAgencies$.subscribe(data => { });
        // this.getcommunicationdd(0);
    }
    // getcommunicationdd(tryCount: number) {
    //     this.intakeCommunication$ = this._commonHttpService.getArrayList(
    //         {},
    //         NewUrlConfig.EndPoint.Intake.IntakeServiceRequestCWInputTypeUrl
    //     ).map(
    //         item =>
    //             item.map(res => new DropdownModel({
    //                 text: res.description,
    //                 value: res.intakeservreqinputtypeid
    //             }))
    //     );

    //     this.intakeCommunication$.subscribe(data => {
    //         this.intakeCommunication = data;
    //         console.log('communications', data);
    //         if (!(data && Array.isArray(data) && data.length > 0) && tryCount <= 5) {
    //             this.getcommunicationdd(tryCount + 1);
    //         }
    //     });
    // }

    selectVoluntaryPlacement(event) {
        this.setVolountaryStoreValues();
    }
    onChangeCounty(event) {
        this.setVolountaryStoreValues();
    }
    resetVPAStoreValues() {
        this._dataStoreService.setData('countyId', null);
        this._dataStoreService.setData('voluntryPlacementType', null);
        this._dataStoreService.setData('isEVPA', null);
    }
    getCountyId(id) {
        this.intakeCountyList$.subscribe(data => {
            this.intakeCountyList = data;
            const selectedCounty = this.intakeCountyList.find(county => county.value === id);
            if (selectedCounty) {
                console.log('SCCCCCCC', selectedCounty);
                this._dataStoreService.setData(
                    'countyId',
                    selectedCounty.text ? selectedCounty.text : null
                );
            } else {
                this._dataStoreService.setData(
                    'countyId',
                    null
                );
            }

        });
        return true;
    }
    setVolountaryStoreValues() {
        const countyId = this.departmentActionIntakeFormGroup.getRawValue()
            .countyid;
        const countyIdDesc = this.getCountyId(countyId);
        // this._dataStoreService.setData(
        //     "countyId",
        //     countyIdDesc ? countyIdDesc.trim() : null
        // );
        const voluntryPlacementType = this.departmentActionIntakeFormGroup.getRawValue()
            .voluntaryPlacementType;
        this._dataStoreService.setData(
            'voluntryPlacementType',
            voluntryPlacementType
        );
        const placementFlag = (voluntryPlacementType === 'VPA') ? true : false;
        this._intakeConfig.isVoluntaryPlacementEnabled(placementFlag);

    }
    private listenForPersonChange() {
        this._dataStoreService.currentStore.subscribe(store => {
            if (store) {
                if (store[IntakeStoreConstants.addedPersons]) {
                    this.processSelectedYouth();
                    this.processLegalGuardian();
                    this.processFocusPerson();
                    this.checkSEN();
                }
                this.loadHTML = store[IntakeStoreConstants.loadHTML];
            }
        });
    }

    private loadDefaults() {
        this._dataStoreService.setData('intakenumber', this.intakeNumber);
        this.departmentActionIntakeFormGroup.patchValue({
            IntakeNumber: this.intakeNumber
        });
        this._authService.currentUser.subscribe(userInfo => {
            if (userInfo && userInfo.user) {
                this.departmentActionIntakeFormGroup.patchValue({
                    Author: userInfo.user.userprofile.displayname
                        ? userInfo.user.userprofile.displayname
                        : ''
                });
            }
        });
    }
    changeOtherAgency(event: any) {
        if (event.target.checked) {
            // this.otherAgencyControlName.enable();
            this.isDisplayOtherAgency = true;
        } else {
            // this.otherAgencyControlName.disable();
            this.isDisplayOtherAgency = false;
            this.departmentActionIntakeFormGroup.patchValue({
                otheragency: ''
            });
        }
    }
    listPurpose(agency: DropdownModel) {
        if (agency.value && agency.value !== 'all') {
            this.selectedAgency = Object.assign({}, new DropdownModel());
            const items = agency.value.split('~');
            this.selectedAgency.value = items[0];
            this.isManualRouting = items[1];
            if (this.selectedAgency.value === 'CW') {
                this.isManualRouting = this.roleId.user.userprofile
                    .ismanualrouting
                    ? this.roleId.user.userprofile.ismanualrouting + ''
                    : 'true';
            }
        } else {
            this.selectedAgency = agency;
        }

        this._dataStoreService.setData(
            IntakeStoreConstants.agency,
            this.selectedAgency.value
        );
        const teamtypekey = this.selectedAgency.value;
        this.departmentActionIntakeFormGroup.patchValue({
            Purpose: ''
        });
        this.intakeServiceGrid = false;
        this.intakeInfoNreffGrid = false;
        const checkInput = {
            nolimit: true,
            where: { teamtypekey: teamtypekey },
            method: 'get',
            order: 'description'
        };
        this.intakePurpose$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter'
            )
            .map(result => {
                // this.purposeList = result;
                if (this.selectteamtypekey) {
                    this.departmentActionIntakeFormGroup.controls[
                        'Purpose'
                    ].setValue(this.selectteamtypekey);
                    const items = this.selectteamtypekey.split('~');
                    const serDescription = items[0];

                    if (items.length === 1) {
                        this.isPurposeWithAgency = false;
                    }
                }
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.intakeservreqtypeid,
                            additionalProperty: res.teamtype.teamtypekey
                        })
                );
            });
        this._dataStoreService.setData(
            IntakeStoreConstants.intakeServiceGrid,
            this.intakeServiceGrid
        );
    }

    listService(purpose: any) {
        this.currentPurposeValue = purpose.value;
        if (purpose.label && purpose.label !== '') {
            this.isRFS =
                this.isAS && purpose.label === 'Request for services'
                    ? true
                    : false;
        }
        this.intakeServiceGrid = false;
        this.intakeInfoNreffGrid = false;
        this._dataStoreService.setData(
            IntakeStoreConstants.ChildProtection,
            false
        );
        if (purpose && purpose.label === 'Child Protective Services') {
            // this.isSDMdisplayed = true;
        } else if (purpose && purpose.label === 'Request for services') {
            (<any>$('#intakeNarrative')).click();
            // this.isSDMdisplayed = false;
            this._dataStoreService.setData(
                IntakeStoreConstants.ChildProtection,
                true
            );
        } else {
            // this.isSDMdisplayed = true;
        }
        if (purpose.value) {
            this.isPurposeChanged = true;
            const items = purpose.value.split('~');
            const serDescription = items[0];
            const teamtypekey = items[1];
            const purposeobj = this.getSelectedPurpose(serDescription);
            this._dataStoreService.setData(
                IntakeStoreConstants.purposeSelected,
                {
                    text: purposeobj.description,
                    value: serDescription,
                    code: purposeobj.intakeservreqtypekey
                }
            );
            this.selectteamtypekey = serDescription;
            this.needSubmissionHistory =  !(this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.INFORMATION_AND_REFERRAL)
            || this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.ROA_CPS));
            if (this.isCW) {
                this.listServiceSubtype(serDescription);
            }

            // if (this.selectedAgency) {
            //     if (this.isCW && serDescription !== 'd207bdd4-f281-4ec8-949c-8fd9657227f9') {
            //         this.isCWSelected = true;
            //     } else {
            //         this.isCWSelected = false;
            //     }
            // }
            if (
                serDescription === 'd207bdd4-f281-4ec8-949c-8fd9657227f9' ||
                serDescription === '9d1c2be9-72af-4527-a603-45913feff080'
            ) {
                this.intakeservice = [];
                this.intakeServiceGrid = true;
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            nolimit: true,
                            where: {
                                teamtypekey: this.selectedAgency
                                    ? this.selectedAgency.value
                                    : '',
                                activeflag: 1,
                                intakeservreqtypeid: serDescription
                            },
                            method: 'get'
                        }),
                        NewUrlConfig.EndPoint.Intake.Intakeservs +
                        '/list?filter'
                    )
                    .subscribe(
                        result => {
                            if (result && result.length > 4) {
                                const temp = result[2];
                                result[2] = result[3];
                                result[3] = temp;
                            }
                            this.intakeServices = result;
                            console.log(
                                'this.intakeServices',
                                this.intakeServices
                            );
                            console.log('result', result);
                            const general = this.store[
                                IntakeStoreConstants.general
                            ];
                            if (
                                this.isCW &&
                                general &&
                                general.intakeservice.length
                            ) {
                                if( general.intakeservice.length > 0 &&  
                                    general.intakeservice[0].intakeservtypekey &&
                                    general.intakeservice[0].intakeservtypekey == 'NONCPS'){
                                    console.log("DISABLING NONCPS KIDS");
                                    // this.intakeServiceGrid = true;
                                    this.departmentActionIntakeFormGroup.disable();
                                }
                                this.selectedIntakeServices = [];
                                this.intakeServices.map(res => {
                                    if (
                                        res.description ===
                                        general.intakeservice[0].description
                                    ) {
                                        this.selectedIntakeServices.push(res);
                                        this.intakeservicesubtype =
                                            general.intakeservice[0].intakesubservice;
                                        if (this.intakeservicesubtype &&
                                            this.intakeservicesubtype.find(
                                                subtype =>
                                                    subtype.intakeservsubtypekey ===
                                                    'VP'
                                            )
                                        ) {
                                            this.isVoluntaryPlacement = true;
                                        } else {
                                            this.isVoluntaryPlacement = false;
                                        }
                                        this._intakeConfig.isVoluntaryPlacementEnabled(this.isVoluntaryPlacement);
                                        if (
                                            this.intakeservicesubtype &&
                                            (this.intakeservicesubtype[0]
                                                .intakeservsubtypekey ===
                                                'INRL' ||
                                                this.intakeservicesubtype[0]
                                                    .intakeservsubtypekey ===
                                                'FKC' ||
                                                this.intakeservicesubtype[0]
                                                    .intakeservsubtypekey ===
                                                'SGP')
                                        ) {
                                            this.kinshipNavigator = this.intakeservicesubtype[0].typedescription;
                                        } else {
                                            this.kinshipNavigator = null;
                                        }

                                        if (this.intakeservicesubtype &&
                                            this.intakeservicesubtype.find(
                                                subtype =>
                                                    subtype.intakeservsubtypekey ===
                                                    'ILAC'
                                            )
                                        ) {
                                            this.isIndependentLiving = true;
                                        } else {
                                            this.isIndependentLiving = false;
                                        }
                                    }
                                });
                            }
                        },
                        err => {
                            console.log(err);
                        }
                    );
            } else if (
                serDescription === '619c4dcf-ef22-4fc4-9269-d7678e8a8f6a'
            ) {
                this.intakeInfoReffTypes = [];
                this.intakeInfoNreffGrid = true;
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest({
                            nolimit: true,
                            where: {
                                teamtypekey: this.selectedAgency.value,
                                intakeservreqtypeid: serDescription
                            },
                            method: 'get'
                        }),
                        NewUrlConfig.EndPoint.Intake.Intakeservs +
                        '/list?filter'
                    )
                    .subscribe(
                        result => {
                            this.intakeInfoReffTypes = result;
                            this.kinshipNavigator = null;
                        },
                        err => {
                            console.log(err);
                        }
                    );
            }
        } else {
            this.intakeServiceGrid = false;
        }
        this.initializeTabs();
        this._dataStoreService.setData(
            IntakeStoreConstants.intakeServiceGrid,
            this.intakeServiceGrid
        );
    }

    selectService(event: any, selectedItem: any, Type: any) {
        this.serviceCheckboxId = '';
        this.selectedCheckbox = selectedItem.description;
        if (event.target.checked) {
            if (this.isCW) {
                // this.intakeservice = [];
                this.intakeservicesubtype = [];
                if (Type === 'intakeServices') { this.intakeservice = []; }
                this.intakeservice.push(selectedItem);
                if(this.intakeservice.find(item => item.description == 'CPS History Clearance')){
                    this._dataStoreService.setData('isCPSHistoryClearanceChecked', true);
                }
                else{
                    this._dataStoreService.setData('isCPSHistoryClearanceChecked', false);
                }
                this.selectedIntakeServices = [];
                this.kinshipNavigator = null;
                if (
                    selectedItem.intakeservsubtype &&
                    selectedItem.intakeservsubtype.length
                ) {
                    this.selectedIntakeServices.push(<IntakeService>(
                        selectedItem
                    ));
                }
                if (selectedItem.description === 'In Home Services') {
                    // (<any>$('#ihm-sub-type-popup')).modal('show');
                    // this.departmentActionIntakeFormGroup.controls[
                    //     'IntakeServiceSubtype'
                    // ].setValidators([Validators.required]);
                    this.departmentActionIntakeFormGroup.controls[
                        'IntakeServiceSubtype'
                    ].updateValueAndValidity();
                } else {
                    this.departmentActionIntakeFormGroup.controls[
                        'IntakeServiceSubtype'
                    ].clearValidators();
                    this.departmentActionIntakeFormGroup.controls[
                        'IntakeServiceSubtype'
                    ].updateValueAndValidity();
                }
                if (selectedItem.description === 'CPS History Clearance') {
                    this.isClearenceHistory = true;
                    this.agencyTabOrder.splice(
                        2,
                        0,
                        IntakeTabConfig.find(
                            item => item.id === 'history-clearance'
                        )
                    );
                    /* const dispIndex = this.agencyTabOrder.findIndex(
                        item => item.id === 'disposition'
                    );
                    if (dispIndex > -1) {
                        this.agencyTabOrder.splice(dispIndex, 1);
                    } */

                    // this.agencyTabOrder.forEach(item=>{console.log("item id ==>" + item.id) })
                    this.showSubmit = false;
                } else {
                    if (
                        this.agencyTabOrder.length &&
                        this.agencyTabOrder.length > 1 &&
                        this.agencyTabOrder[2].id === 'history-clearance'
                    ) {
                        this.agencyTabOrder.splice(2, 1);
                        this.showSubmit = false;
                    }
                }
            } else {
                this.intakeservice.push(selectedItem);
            }
            const role = this._authService.getCurrentUser();
            if (role.role.name !== AppConstants.ROLES.SUPERVISOR) {
                if (selectedItem.assessmenttemplateid) {
                    this.serviceCheckboxId = selectedItem.intakeservid;
                    (<any>$('#assessment-tab')).click();
                    this._dataStoreService.setData(
                        IntakeStoreConstants.intakeService,
                        this.intakeservice
                    );
                }
            }
        } else {
            if (
                this.isCW &&
                selectedItem.description === 'CPS History Clearance'
            ) {
                if (
                    this.agencyTabOrder.length &&
                    this.agencyTabOrder.length > 1 &&
                    this.agencyTabOrder[2].id === 'history-clearance'
                ) {
                    this.agencyTabOrder.splice(2, 1);
                    this.showSubmit = false;
                }
            }
            if (
                selectedItem.intakeservsubtype &&
                selectedItem.intakeservsubtype.length
            ) {
                const selectedService = <IntakeService>selectedItem;
                const selServIndex = this.selectedIntakeServices.indexOf(
                    selectedService
                );
                if (selServIndex !== -1) {
                    this.selectedIntakeServices.splice(selServIndex, 1);
                }
            }
            this.intakeservice = this.intakeservice.map(item => {
                if (item.intakeservid !== selectedItem.intakeservid) {
                    return item;
                } else {
                    return new IntakeService();
                }
            });

        }
        this.departmentActionIntakeFormGroup.controls['voluntaryPlacementType'].reset();
        this.departmentActionIntakeFormGroup.controls['voluntaryPlacementType'].clearValidators();
        this.departmentActionIntakeFormGroup.controls['voluntaryPlacementType'].updateValueAndValidity();
        this._intakeConfig.isVoluntaryPlacementEnabled(false);
        this.isVoluntaryPlacement = false;
        this.isIndependentLiving = false;
        const intakeServices = this.intakeservice.filter(item => item.intakeservid);
            this._dataStoreService.setData(
                IntakeStoreConstants.intakeService,
                intakeServices
            );
    }

    selectIntakeServiceSubType(
        event,
        selectedServiceTypeItem,
        selectedServiceSubTypeItem
    ) {
        // console.log(selectedServiceSubTypeItem.intakeservsubtypekey === 'VP');
        if (event.target.checked) {
            this.intakeServiceSubtypeChecked(selectedServiceSubTypeItem);
        } else {
            this.intakeServiceSubtypeUnchecked(selectedServiceSubTypeItem);
        }
        this.departmentActionIntakeFormGroup.controls['voluntaryPlacementType'].reset();
        this.departmentActionIntakeFormGroup.controls['voluntaryPlacementType'].clearValidators();
        this.departmentActionIntakeFormGroup.controls['voluntaryPlacementType'].updateValueAndValidity();
        this._intakeConfig.isVoluntaryPlacementEnabled(false);
    }

    intakeServiceSubtypeUnchecked(selectedServiceSubTypeItem) {
        if (selectedServiceSubTypeItem.intakeservsubtypekey === 'VP') {
            this.isVoluntaryPlacement = false;
            this._dataStoreService.setData('voluntryPlacementType', null);
        }
        if (selectedServiceSubTypeItem.intakeservsubtypekey === 'ILAC') {
            this.isIndependentLiving = false;
        }
        this.intakeservicesubtype = [];
        this.kinshipNavigator = null;
        this.emptyKinship();
    }

    intakeServiceSubtypeChecked(selectedServiceSubTypeItem) {
        if (selectedServiceSubTypeItem.intakeservsubtypekey === 'VP') {
            this.isVoluntaryPlacement = true;
        } else {
            this.isVoluntaryPlacement = false;
            this._dataStoreService.setData('voluntryPlacementType', null);
        }

        if (selectedServiceSubTypeItem.intakeservsubtypekey === 'ILAC') {
            this.isIndependentLiving = true;
        } else {
            this.isIndependentLiving = false;
        }
        this.intakeservicesubtype = [];
        this.intakeservicesubtype.push(selectedServiceSubTypeItem);
        this.intakeservice[0].intakesubservice = Object.assign(
            [],
            this.intakeservicesubtype
        );
        if (
            selectedServiceSubTypeItem.intakeservsubtypekey === 'INRL' ||
            selectedServiceSubTypeItem.intakeservsubtypekey === 'FKC' ||
            selectedServiceSubTypeItem.intakeservsubtypekey === 'SGP'
        ) {
            this.kinshipNavigator =
                selectedServiceSubTypeItem.typedescription;
        } else {
            this.kinshipNavigator = null;
            this.emptyKinship();
        }
    }

    emptyKinship() {
        this.departmentActionIntakeFormGroup.patchValue({
            islocalreferal: '',
            referalcomments: '',
            nonreferalreason: '',
            servicerequest: '',
            suggestedresource: ''
        });
    }

    isSelectedSubtypeItems(modal) {
        const index = this.intakeservicesubtype
            ? this.intakeservicesubtype.findIndex(
                item =>
                    item.intakeservsubtypekey === modal.intakeservsubtypekey
            )
            : -1;
        if (index >= 0) {
            return true;
        } else {
            return false;
        }
    }

    viewAssessment(modal) {
        (<any>$('#assessment-tab')).click();
    }

    isSelectedItems(modal) {
        const index = this.intakeservice.findIndex(
            item => item.intakeservtypekey === modal.intakeservtypekey
        );
        if (index >= 0) {
            return true;
        } else {
            return false;
        }
    }

    savenAssignPreIntake() {
        const data = this.store[IntakeStoreConstants.preIntakeSupDicision];
        if (data === 'Approved' || data === 'Rejected') {
            this.preIntakeSupDicision = data;
        }
        if (this.preIntakeSupDicision === 'Approved') {
            const Istatus = 10; // Approved
            this.referralSubmission = this._intakeConfig.isFlowToCaseSupervisor();
            if (this.referralSubmission) {
                this.submitIntakeFromReferral(
                    this._intakeConfig.selectedPurpose,
                    'INTR'
                );
            } else {
                const intakeWorkerId = this.store[
                    IntakeStoreConstants.assingedIntakeWokerId
                ];
                if (intakeWorkerId) {
                    this.draftIntake(
                        this.departmentActionIntakeFormGroup.value,
                        'DRAFT', true
                    );
                    setTimeout(() => this.assignIntaker('SITR', Istatus), 3000);
                } else {
                    this._alertService.warn('Please select a intake worker');
                }
            }
        } else if (this.preIntakeSupDicision === 'Rejected') {
            const Istatus = 3; // Rejected
            this.draftIntake(
                this.departmentActionIntakeFormGroup.value,
                'DRAFT', true
            );
            setTimeout(() => this.assignIntaker('SITR', Istatus), 3000);
        } else {
            this._alertService.warn('Please select a status.');
        }
    }

    submitIntakeFromReferral(selectedPurpose, appevent: string) {
        const disposition = this.store[IntakeStoreConstants.createdCases];
        if (
            selectedPurpose ===
            MyNewintakeConstants.REFERRAL.WAIVER_FROM_ADUL_COURT
        ) {
            disposition.forEach(dis => {
                dis.DADisposition = 'FPTSAO';
                dis.supDisposition = 'FPTSAO';
                dis.dispositioncode = 'FPTSAO';
                dis.supStatus = 'Approved';
                dis.DAStatus = 'Approved';
                dis.intakeserreqstatustypekey = 'Approved';
                dis.DaTypeKey = dis.serviceTypeID;
                dis.DasubtypeKey = dis.subServiceTypeID;
                dis.ServiceRequestNumber = dis.caseID;
            });
        } else if (
            selectedPurpose ===
            MyNewintakeConstants.REFERRAL.ADULT_HOLD_DETENTION
        ) {
            disposition.forEach(dis => {
                dis.DADisposition = 'PCS';
                dis.supDisposition = 'PCS';
                dis.dispositioncode = 'PCS';
                dis.supStatus = 'Approved';
                dis.DAStatus = 'Approved';
                dis.intakeserreqstatustypekey = 'Review';
                dis.DaTypeKey = dis.serviceTypeID;
                dis.DasubtypeKey = dis.subServiceTypeID;
                dis.ServiceRequestNumber = dis.caseID;
            });
        } else if (
            selectedPurpose === MyNewintakeConstants.REFERRAL.INTERSTATE_COMPACT
        ) {
            disposition.forEach(dis => {
                dis.DADisposition = 'PCSICJRP';
                dis.supDisposition = 'PCSICJRP';
                dis.dispositioncode = 'PCSICJRP';
                dis.supStatus = 'Approved';
                dis.DAStatus = 'Approved';
                dis.intakeserreqstatustypekey = 'Review';
                dis.DaTypeKey = dis.serviceTypeID;
                dis.DasubtypeKey = dis.subServiceTypeID;
                dis.ServiceRequestNumber = dis.caseID;
            });
        }
        this._dataStoreService.setData(
            IntakeStoreConstants.disposition,
            disposition
        );
        this.approveIntake(new General(), appevent);
    }

    checkfordelay(modal, appeventcode) {
        const recivedDate = new Date(modal.RecivedDate);
        const currentDate = new Date();
        const status = appeventcode === 'INTR' ? 'supreview' : '';
        const receiveddelayreason = '';
        if ((status === 'supreview' && appeventcode === 'INTR') || appeventcode === 'DRAFT') {
            if (recivedDate.getTime() + 5 * 24 * 60 * 60 * 1000 <= currentDate.getTime()) {
                this.generalRecievedDate = {
                    isreceiveddelay: true,
                    issubmitdelay: false,
                    message: 'Received date greater than 5 days'
                };
            }
            if (recivedDate.getTime() + 25 * 24 * 60 * 60 * 1000 <= currentDate.getTime()) {
                this.generalRecievedDate = {
                    isreceiveddelay: false,
                    issubmitdelay: true,
                    message: 'Submitted date greater than 25 days'
                };
            }
        }
    }
    submitIntake(modal: General, appevent: string) {

        if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.CHILD_PROTECTION_SERVICES)) {
            this.checkfordelay(modal, appevent);
            if (this.generalRecievedDate && (this.generalRecievedDate.isreceiveddelay || this.generalRecievedDate.issubmitdelay)) {
                const form = this.dispositionDelayForm.getRawValue();
                if (this.generalRecievedDate.isreceiveddelay && (form.fiveDaysDelay.length === 0)) {
                    (<any>$('#reason-for-delay')).modal('show');
                    return;
                } else if (this.generalRecievedDate.issubmitdelay && !(form.twentyFivedaysDelay.length === 0)) {
                    (<any>$('#reason-for-delay')).modal('show');
                    return;
                }
            }
        } else {
            modal.receiveddelay = 'N/A';
            modal.submissiondelay = 'N/A';
        }
        if (this.store[IntakeStoreConstants.addNarrative]) {
            modal.requesteraddress1 = this.store[
                IntakeStoreConstants.addNarrative
            ].requesteraddress1;
            modal.requesteraddress2 = this.store[
                IntakeStoreConstants.addNarrative
            ].requesteraddress2;
            modal.requestercity = this.store[
                IntakeStoreConstants.addNarrative
            ].requestercity;
            modal.requesterstate = this.store[
                IntakeStoreConstants.addNarrative
            ].requesterstate;
            modal.requestercounty = this.store[
                IntakeStoreConstants.addNarrative
            ].requestercounty;
            modal.isacknowledgementletter = this.store[
                IntakeStoreConstants.addNarrative
            ].isacknowledgementletter
                ? 1
                : 0;
        }
        this.checkValidation = this.conditionalValidation();
        const disposition = this.store[IntakeStoreConstants.disposition];

        /*
        // Family case validtion now happeing on approve intake api itself
         if (this.checkValidation && this.roleId.user.userprofile.teamtypekey === 'CW' && this.roleId.role.name === AppConstants.ROLES.SUPERVISOR) {
            if (this.intakeservicesubtype && this.intakeservicesubtype.filter((item) => item.intakeservsubtypekey === 'FPS').length && disposition[0].supStatus === 'Approved') {
                this.getPrior(this.intakeNumber);
                (<any>$('#priorDetails')).modal('show');
                this.checkValidation = false;
            }
        } */
        if (this.checkValidation) {
            // Added rejected status (to visible in intake worker dashboard)
            if (
                disposition[0].supStatus === 'Approved' ||
                disposition[0].supStatus === 'Closed' ||
                disposition[0].supStatus === 'Rejected'
            ) {
                this.approveIntake(modal, appevent);
            } else {
                this.mainIntake(modal, appevent, true, true);
            }
        }
    }

    submitReviewIntake() {
        const disposition = this.store[IntakeStoreConstants.disposition];
        if (this.selectedSupervisor) {
            this.finalIntake.review.assignsecurityuserid = this.selectedSupervisor;
            this.finalIntake.review.ismanualrouting = true;
            if (this.roleId.role.key) {
                this.finalIntake.intake.userrole = this.roleId.role.key;
            }
            if (disposition[0].intakeserreqstatustypekey === 'Closed') {
                this.approveIntake(
                    this.departmentActionIntakeFormGroup.value,
                    'INTR'
                );
            } else {
                this.createIntake(this.finalIntake, true).subscribe(result => {
                    console.log('create intake response', result);
                });
            }
            (<any>$('#list-supervisor')).modal('hide');
        } else {
            this._alertService.error('Please select Supervisor');
        }
    }

    submitIntakewithIntakers(Intaker: string) {
        if (Intaker === '1') {
            if (this.selectedIntaker) {
                this.finalIntake.review.assignIntakeuserid = this.selectedIntaker;
                this.finalIntake.review.ismanualrouting = true;
                this.createIntake(this.finalIntake, true).subscribe(result => {
                    console.log('create intake response', result);
                });
                (<any>$('#list-intaker')).modal('hide');
            } else {
                this._alertService.error('Please select Intertaker');
            }
        } else {
            this.createIntake(this.finalIntake, true).subscribe(result => {
                console.log('create intake response', result);
            });
            (<any>$('#list-intaker')).modal('hide');
        }
    }

    submitPreIntake(modal: General, appevent: string) {
        if (this.isDjs) {
            const message = this._intakeConfig.djsInfoValidation();
            if (message.status) {
                this.djsValidationMessages = message;
                const nojury = message.No_Jurisdiction.map((data, index) => {
                    if (index === 0) {
                        return data;
                    }
                    return '<br>' + data;
                });
                const insuffinfo = message.Insufficient_Information.map(
                    (data, index) => {
                        if (index === 0) {
                            return data;
                        }
                        return '<br>' + data;
                    }
                );
                const nojuryhtml = `<div class="col-sm-12" *ngIf="message?.No_Jurisdiction?.length > 0">
                <h4> No Jurisdiction</h4>
                <ul class="mat-error" >
                        ${nojury.toString()}
                </ul>
        </div>`;
                const insufficehtml = `
        <div class="col-sm-12" *ngIf="message?.Insufficient_Information?.length > 0">
                <h4>Insufficient Information</h4>
                <ul class="mat-error" >
                   ${insuffinfo.toString()}
                </ul>
        </div>`;
                this.narrativestatus =
                    (message.No_Jurisdiction && message.No_Jurisdiction.length
                        ? nojuryhtml
                        : '') +
                    (message.Insufficient_Information &&
                        message.Insufficient_Information.length
                        ? insufficehtml
                        : '');
                (<any>$('#djs-validation-popup')).modal('show');
                return false;
            }
        }
        this.mainIntake(modal, appevent, false, true);
    }

    submitPreIntakecaseClose(modal: General, narrative) {
        const disposition = this.store[IntakeStoreConstants.createdCases];
        if (disposition && disposition.length > 0) {
            disposition.forEach(dis => {
                dis.DADisposition = 'ISI';
                dis.supDisposition = 'ISI';
                dis.dispositioncode = 'ISI';
                dis.supStatus = 'Closed';
                dis.DAStatus = 'Closed';
                dis.intakeserreqstatustypekey =
                    '642f18b0-ef6e-4d4b-9871-acc0734f3f5a';
                dis.DaTypeKey = dis.serviceTypeID;
                dis.DasubtypeKey = dis.subServiceTypeID;
                dis.ServiceRequestNumber = dis.caseID;
            });
            this._dataStoreService.setData(
                IntakeStoreConstants.disposition,
                disposition
            );
        }
        this._dataStoreService.setData(
            IntakeStoreConstants.insufficientInfoNarrative,
            narrative
        );
        this.approveIntake(new General(), 'INTR');
    }
    draftIntake(modal: General, appevent: string, toshowmessage: boolean) {
        if (this.store[IntakeStoreConstants.addNarrative]) {
            modal.requesteraddress1 = this.store[
                IntakeStoreConstants.addNarrative
            ].requesteraddress1;
            modal.requesteraddress2 = this.store[
                IntakeStoreConstants.addNarrative
            ].requesteraddress2;
            modal.requestercity = this.store[
                IntakeStoreConstants.addNarrative
            ].requestercity;
            modal.requesterstate = this.store[
                IntakeStoreConstants.addNarrative
            ].requesterstate;
            modal.requestercounty = this.store[
                IntakeStoreConstants.addNarrative
            ].requestercounty;
            modal.isacknowledgementletter = this.store[
                IntakeStoreConstants.addNarrative
            ].isacknowledgementletter
                ? 1
                : 0;
        }
        this.mainIntake(modal, appevent, false, toshowmessage);
    }
    mainIntake(modal: General, appevent: string, isSubmitReview: boolean, toshowmessage: boolean) {
        this.modal = modal;
        this.appevent = appevent;
        if (this.roleId.role.name === AppConstants.ROLES.SUPERVISOR) {
            modal = this.departmentActionIntakeFormGroup.getRawValue();
        }
        if (this.isCW && !this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.CHILD_PROTECTION_SERVICES)) {
            modal.receiveddelay = 'N/A';
            modal.submissiondelay = 'N/A';
            }
        // this.intakeType = appevent;
        if (this.departmentActionIntakeFormGroup.status !== 'INVALID') {
            // Changes for UAT Defect 06781
            if (appevent === 'DRAFT' || appevent === 'CLWDRAFT') {
                this.checkValidation = true;
            } else {
                this.checkValidation = this.conditionalValidation();
            }
            if (this.checkValidation) {
                this.general = Object.assign(new General(), modal);
                this.general.offenselocation = this.zipCode;
                this.general.intakeservice = this.intakeservice;
                if (
                    this.isCW &&
                    this.intakeservicesubtype &&
                    this.intakeservicesubtype.length
                ) {
                    this.general.intakeservice[0].intakesubservice = Object.assign(
                        [],
                        this.intakeservicesubtype
                    );
                }
                const recDate = new Date(modal.RecivedDate);
                this.general.RecivedDate = recDate.toLocaleString(); // MAR - Removed UTC reference
                ObjectUtils.removeEmptyProperties(this.general);
                const disposition = this.store[
                    IntakeStoreConstants.disposition
                ];
                // @DP - For In Home Service
                if (
                    this.intakeservice &&
                    this.intakeservice.length &&
                    this.intakeservice[0].description === 'In Home Services'
                ) {
                    if (this.subServiceTypes) {
                        this.subServiceTypes.forEach(item => {
                            if (
                                this.intakeservice[0].intakeservtypekey ===
                                item.classkey
                            ) {
                                if (disposition && disposition.length) {
                                    disposition[0].DasubtypeKey =
                                        item.servicerequestsubtypeid;
                                }
                            }
                        });
                    }
                }
                if (this.isCW) {
                    if (this.addSdm) {
                        if (
                            disposition &&
                            disposition.length &&
                            disposition[0]
                        ) {
                            disposition[0].issubtypekey = true;
                        }
                        this.general.Iscps = this.addSdm.iscps
                            ? this.addSdm.iscps
                            : null;
                        if (
                            this.subServiceTypes &&
                            disposition &&
                            disposition.length &&
                            disposition[0]
                        ) {
                            this.subServiceTypes.map(item => {
                                if (
                                    this.addSdm.cpsResponseType ===
                                    item.classkey
                                ) {
                                    disposition[0].DasubtypeKey =
                                        item.servicerequestsubtypeid;
                                }
                            });
                        }
                    } else {
                        this.general.Iscps = null;
                    }
                }
                const intake = this.mapIntakeScreenInfo(this.general);
                const role = this._authService.getCurrentUser();
                const validateFocuspersonCaseDJS =
                    this.isDjs &&
                    (role.role.name === AppConstants.ROLES.SUPERVISOR ||
                        role.role.name === AppConstants.ROLES.INTAKE_WORKER ||
                        role.role.name ===
                        AppConstants.ROLES.OFFICE_PROFFESSIONAL);
                const reviewstatus = {
                    appevent: appevent,
                    status: appevent === 'INTR' ? 'supreview' : '',
                    commenttext: appevent === 'INTR' ? '' : '',
                    ispreintake: this.isPreIntake
                };
                if (
                    (appevent === 'SITR' || appevent === 'DRAFT') &&
                    role.role.name === AppConstants.ROLES.OFFICE_PROFFESSIONAL
                ) {
                    reviewstatus.appevent = appevent;
                    reviewstatus.status = 'supreview';
                    reviewstatus.ispreintake = true;
                }
                if (
                    this.roleId.role.name ===
                    AppConstants.ROLES.KINSHIP_INTAKE_WORKER ||
                    this.roleId.role.name ===
                    AppConstants.ROLES.KINSHIP_SUPERVISOR
                ) {
                    reviewstatus.appevent = 'KINR';
                }
                if (disposition && disposition.length > 0) {
                    if (role.role.name === AppConstants.ROLES.SUPERVISOR) {
                        reviewstatus.commenttext = disposition[0].supComments
                            ? disposition[0].supComments
                            : '';
                        reviewstatus.status = disposition[0].supStatus;
                    } else {
                        reviewstatus.commenttext = disposition[0].comments
                            ? disposition[0].comments
                            : '';
                    }
                }
                if (intake) {
                    if (this.genratedDocumentList) {
                        this.genratedDocumentList.forEach(document => {
                            document.isSelected = false;
                        });
                    }
                    // this.general.Narrative = encodeURI(this.general.Narrative);
                    // this.general.Narrative = this.general.Narrative.replace(/'/g, "%27");
                    this._dataStoreService.setData(
                        IntakeStoreConstants.general,
                        this.general
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.reviewstatus,
                        reviewstatus
                    );
                    // const clwStatus = (appevent === 'CLWDRAFT') ? this.clwStatus : null;
                    const intakeSaveModel = new IntakeTemporarySaveModel({
                        reasonforDraft: this.saveasdraftReason,
                        crossReference: this.addedCrossReference,
                        persons: this.store[IntakeStoreConstants.addedPersons],
                        entities: this.store[
                            IntakeStoreConstants.addedEntities
                        ],
                        agency: this.store[IntakeStoreConstants.agency],
                        intakeDATypeDetails: this.addedIntakeDATypeDetails, // what is the name for this in store constants?
                        recordings: this.store[
                            IntakeStoreConstants.communications
                        ],
                        General: this.store[IntakeStoreConstants.general],
                        narrative: this.store[
                            IntakeStoreConstants.addNarrative
                        ],
                        clwStatus: this.clwStatus,
                        signedOffDate: this.signedOffDate,
                        disposition: this.store[
                            IntakeStoreConstants.disposition
                        ],
                        attachement: this.store[
                            IntakeStoreConstants.attachments
                        ],
                        evaluationFields: this.isDjs
                            ? this.modifyEvalFields()
                            : null,
                        appointments: this.store[
                            IntakeStoreConstants.intakeappointment
                        ],
                        reviewstatus: reviewstatus,
                        createdCases: this.store[
                            IntakeStoreConstants.createdCases
                        ],
                        sdm: this.store[IntakeStoreConstants.intakeSDM],
                        saoResponseDetail: this.store[
                            IntakeStoreConstants.saoResponse
                        ],
                        petitionDetails: this.store[
                            IntakeStoreConstants.petitionDetails
                        ],
                        scheduledHearings: this.store[
                            IntakeStoreConstants.scheduledHearings
                        ],
                        courtDetails: this.store[
                            IntakeStoreConstants.courtDetails
                        ],
                        communicationFields: this.store[
                            IntakeStoreConstants.communicationFields
                        ],
                        preIntakeDispo: this.store[
                            IntakeStoreConstants.preIntakeDisposition
                        ],
                        generatedDocuments: this.store[
                            IntakeStoreConstants.generatedDocuments
                        ],
                        adultScreenTool: this._dataStoreService.getData(
                            IntakeStoreConstants.adultScreenTool
                        ),
                        focuspersoncasedetails: validateFocuspersonCaseDJS
                            ? this.modifyinterStateCompactDetails()
                            : [],
                        paymentSchedule: this._dataStoreService.getData(
                            IntakeStoreConstants.paymentSchedule
                        ),
                        unknownPersons: this._dataStoreService.getData(
                            IntakeStoreConstants.addedUnkPersons
                        ),
                        identifiedPersons: this._dataStoreService.getData(
                            IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS
                        ),
                        placement: this._dataStoreService.getData(
                            IntakeStoreConstants.addedPlacement
                        ),
                        reasonintakeinterview: this._dataStoreService.getData(
                            IntakeStoreConstants.reasonintakeinterview
                        ),
                        complaintInfoReview: this._dataStoreService.getData(
                            IntakeStoreConstants.complaintInfoReview
                        ),
                        detentionOpened: this._dataStoreService.getData(
                            IntakeStoreConstants.detentionOpened
                        ),
                        roacps: this._dataStoreService.getData(
                            IntakeStoreConstants.roacps
                        ),
                        clearhistory: this._dataStoreService.getData(
                            IntakeStoreConstants.clearhistory
                        ),
                        userrole: this.roleId.role.key ? this.roleId.role.key : ''
                    });

                    if (disposition && disposition.length > 0) {
                        if (!this.isDjs) {
                            // added for retaining initial recommendation during update draft.
                            disposition.map(item => {
                                // D07649: CW-Intake - Intake recommendation balnks out
                                // item.intakeMultipleDispositionDropdown = [];
                                item.supMultipleDispositionDropdown = [];
                            });
                        }
                        intakeSaveModel.DAType = {
                            DATypeDetail: disposition
                        };
                    }
                    // intakeSaveModel.preIntakeDispo = this.preIntakeDisposition;
                    intakeSaveModel.narrative = Object.assign(
                        intake.NarrativeIntake
                    );
                    if (this.delayFormData && this.delayFormData.fiveDays) {
                        intakeSaveModel.General.receiveddelay = this.delayFormData.fiveDays;
                    }

                    if (
                        this.delayFormData &&
                        this.delayFormData.twentyFiveDays
                    ) {
                        intakeSaveModel.General.submissiondelay = this.delayFormData.twentyFiveDays;
                    }

                    if (
                        this.delayFormData &&
                        this.delayFormData.fiveDays &&
                        this.delayFormData.twentyFiveDays
                    ) {
                        intakeSaveModel.General.receiveddelay = this.delayFormData.fiveDays;
                        intakeSaveModel.General.submissiondelay = this.delayFormData.twentyFiveDays;
                    }
                    const finalIntake = {
                        intake: intakeSaveModel,
                        review: reviewstatus
                    };
                    if (
                        this.isDjs &&
                        (role.role.name === AppConstants.ROLES.INTAKE_WORKER ||
                            role.role.name ===
                            AppConstants.ROLES.KINSHIP_INTAKE_WORKER)
                    ) {
                        this.isManualRouting = 'true';
                    }
                    if (
                        this.isAS &&
                        role.role.name === AppConstants.ROLES.INTAKE_WORKER
                    ) {
                        this.isManualRouting = 'true';
                    }

                    this.closeCWCae = this._dataStoreService.getData(
                        IntakeStoreConstants.closeintakecw
                    );
                    if (this.closeCWCae) {
                        // if (this.isCW && this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.ROA_CPS)) {
                        //     this.finalIntake = Object.assign({}, finalIntake);
                        //     this.loadSupervisor();
                        //  }
                        // else {
                        this.createIntake(finalIntake, toshowmessage).subscribe(result => {
                            console.log('create intake response', result);
                        });
                        // this._alertService.success('Intake Submitted Successfully!', true);
                        if (toshowmessage) {
                            this._alertService.success('Completed successfully.');
                        } 
                    // }
                    } else if (
                        this.isManualRouting === 'true' &&
                        isSubmitReview &&
                        (role.role.name === AppConstants.ROLES.INTAKE_WORKER ||
                            role.role.name === AppConstants.ROLES.CASE_WORKER ||
                            role.role.name === AppConstants.ROLES.KINSHIP_INTAKE_WORKER)
                    ) {
                        this.finalIntake = Object.assign({}, finalIntake);
                        this.loadSupervisor();
                    } else {
                        if (
                            appevent !== 'DRAFT' &&
                            appevent !== 'CLWDRAFT' &&
                            this.isDjs &&
                            (this._intakeConfig.selectedPurposeIs(
                                MyNewintakeConstants.REFERRAL
                                    .WAIVER_FROM_ADUL_COURT
                            ) ||
                                this._intakeConfig.selectedPurposeIs(
                                    MyNewintakeConstants.REFERRAL
                                        .ADULT_HOLD_DETENTION
                                ))
                        ) {
                            this.submitIntakeFromReferral(
                                this._intakeConfig.getIntakePurpose()
                                    .intakeservreqtypekey,
                                'INTR'
                            );
                        } else {
                            this.createIntake(finalIntake, toshowmessage).subscribe(result => {
                                console.log('create intake response', result);
                            });
                        }
                    }
                }
            }
        } else {
            ControlUtils.validateAllFormFields(
                this.departmentActionIntakeFormGroup
            );
            if (
                this.departmentActionIntakeFormGroup
                    .get('InputSource')
                    .hasError('required')
            ) {
                this._alertService.error('Please enter the communication');
            } else {
                this._alertService.warn('Please enter the mandatory fields.');
            }
        }
    }
    private CompleteSAO(finalIntake) {
        this._commonHttpService
            .create(finalIntake, NewUrlConfig.EndPoint.Intake.saoComplete)
            .subscribe(
                response => {
                    this.onReload();
                    this._alertService.success(
                        'Sao Details Closed successfully!'
                    );
                },
                error => {
                    this._alertService.error(
                        'Unable to close, please try again.'
                    );
                    return false;
                }
            );
    }

    checkSEN() {
        this.isSENflag = false;
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        const intakesdmcheck = this.store[IntakeStoreConstants.intakeSDM];

        if (intakesdmcheck && intakesdmcheck.cpsResponseType &&
            (intakesdmcheck.cpsResponseType === 'CPS-AR' ||
                intakesdmcheck.cpsResponseType === 'CPS-IR')) {       // @TM: CPS IR & CPS AR over-ride SEN cases
            this.isSENflag = false;
        } else if (intakesdmcheck && intakesdmcheck.riskofHarm &&
            ObjectUtils.checkTrueProperty(intakesdmcheck.riskofHarm) >= 1) {
            this.isSENflag = true;
        } else if (addedPersons) {
            addedPersons.map(item => {
                // @TM: Set ROH (Risk of Harm) flag for 'strictly' SEN (Substance Exposed New-born) case
                if (item.drugexposednewbornflag === 1) {
                    this.isSENflag = true;
                }
            });
        }
    }

    createIntake(finalIntake, toshowmessage: boolean): Observable<string> {
        finalIntake.intake.sdm = finalIntake.intake.sdm
            ? finalIntake.intake.sdm
            : { isar: true };
        this.saveInProgress = true;

        this._commonHttpService
            .create(
                finalIntake,
                NewUrlConfig.EndPoint.Intake.SendtoSupervisorreviewUrl
            )
            .subscribe(
                response => {
                    this.saveInProgress = false;
                    this.checkSEN();
                    if (this.closeCWCae && this.appevent !== 'DRAFT') {
                        this.approveIntake(this.modal, this.appevent);
                    } else if (
                        !this.isCLW &&
                        (response.data.isreceiveddelay ||
                            response.data.issubmitdelay)
                    ) {
                        (<any>$('#disposition-tab')).click();
                        this.generalRecievedDate = response.data;

                        // (<any>$('#reason-for-delay')).modal('show');
                    } else {
                        if (finalIntake.review.appevent === 'CLWDRAFT') {
                            this.onReload();
                            this.btnDraft = true;
                            this._alertService.success(
                                'Sao Details saved successfully!'
                            );
                        } else if (finalIntake.review.appevent !== 'DRAFT') {
                            if (
                                this.isCW &&
                                finalIntake &&
                                finalIntake.intake.sdm
                            ) {
                                if (
                                    (this.intakeservice &&
                                    this.intakeservice.length &&
                                    this.intakeservice[0].description !==
                                    'In Home Services')
                                ) {
                                    // @TM: By-pass SDM for In-Home Services
                                    this.saveInitialSdm(
                                        finalIntake.intake.sdm
                                    ).subscribe(data => {
                                        this.onReload();
                                    });
                                } else {
                                    this.onReload();
                                }
                            } else {
                                this.onReload();
                            }
                            this.btnDraft = false;
                        } else {
                            // if (this.isCW) {
                            //     this.onReload();
                            // }
                            if (toshowmessage) {
                                if (this.btnDraft === true) {
                                    this._alertService.success(
                                        'Intake updated successfully!'
                                    );
                                } else {
                                    this._alertService.success(
                                        'Intake saved successfully!'
                                    );
                                }
                            }
                            const intakeStore = this._intakeService.getIntakeStore();
                            intakeStore.action = 'edit';
                            this._intakeService.setIntakeStore(intakeStore);
                            this.btnDraft = true;
                        }
                        return Observable.of('success');
                    }
                },
                error => {
                    this.saveInProgress = false;
                    this._alertService.error(
                        'Unable to save intake, please try again.'
                    );
                    console.log('Save Intake Error', error);
                    return Observable.empty();
                }
            );
        return Observable.empty();
    }

    saveInitialSdm(sdm) {
        sdm.reportdate = null;
        const initialSdm = {
            sdmdata: this.store[IntakeStoreConstants.intakeSDM],
            intakenumber: this.intakeNumber,
            servicerequestid: null
        };
        this._dataStoreService.setData(
            IntakeStoreConstants.intakeSDM,
            initialSdm
        );
        return this._commonHttpService.create(
            initialSdm,
            NewUrlConfig.EndPoint.Intake.IntakeSdmCreateUrl
        );
    }

    approveIntake(modal: General, appevent: string) {
        modal = this.departmentActionIntakeFormGroup.getRawValue();
        modal.Source = modal.Source ? modal.Source : modal.InputSource;
        modal.AgencyCode = modal.AgencyCode ? modal.AgencyCode : modal.Agency;
        // const intakeModel = response.data[0].jsondata;
        // this.intakeType = appevent;
        if (this.departmentActionIntakeFormGroup.status !== 'INVALID') {
            // Changes for UAT Defect 06781
            if (appevent !== 'DRAFT') {
                this.checkValidation = this.conditionalValidation();
            } else {
                this.checkValidation = this.conditionalValidation();
            }
            if (this.checkValidation) {
                const general = Object.assign(new General(), modal);
                general.offenselocation = this.zipCode;
                general.intakeservice = this.intakeservice;
                if (
                    this.isCW &&
                    this.intakeservicesubtype &&
                    this.intakeservicesubtype.length
                ) {
                    general.intakeservice[0].intakesubservice = Object.assign(
                        [],
                        this.intakeservicesubtype
                    );
                }
                const recDate = new Date(modal.RecivedDate);
                general.RecivedDate = recDate.toLocaleString();
                general.Purpose = this.store[
                    IntakeStoreConstants.purposeSelected
                ].value;
                const General_OBJ = this.store['general'];
                if (General) {
                    general.requesteraddress1 = General_OBJ['requesteraddress1'] ? General_OBJ['requesteraddress1'] : null;
                    general.requesteraddress2 = General_OBJ['requesteraddress2'] ? General_OBJ['requesteraddress2'] : null;
                    general.requestercity = General_OBJ['requestercity'] ? General_OBJ['requestercity'] : null;
                    general.requesterstate = General_OBJ['requesterstate'] ? General_OBJ['requesterstate'] : null;
                    general.requestercounty = General_OBJ['requestercounty'] ? General_OBJ['requestercounty'] : null;
                    general.offenselocation = General_OBJ['offenselocation'] ? General_OBJ['offenselocation'] : null;
                }
                ObjectUtils.removeEmptyProperties(general);
                const intake = this.mapIntakeScreenInfo(general);
                const disposition = this.store[
                    IntakeStoreConstants.disposition
                ];
                if (
                    this.roleId.role.name ===
                    AppConstants.ROLES.KINSHIP_INTAKE_WORKER ||
                    this.roleId.role.name ===
                    AppConstants.ROLES.KINSHIP_SUPERVISOR
                ) {
                    appevent = 'KINR';
                }
                const reviewstatus = {
                    appevent: appevent,
                    status: disposition[0].supStatus,
                    commenttext: '',
                    youthstatuseventcode: '',
                    ispreintakeapproved: false
                };
                if (disposition && disposition.length > 0) {
                    const role = this._authService.getCurrentUser();
                    if (role.role.name === AppConstants.ROLES.SUPERVISOR) {
                        reviewstatus.commenttext = disposition[0].supComments
                            ? disposition[0].supComments
                            : '';
                    } else {
                        reviewstatus.commenttext = disposition[0].comments
                            ? disposition[0].comments
                            : '';
                    }
                }

                if (
                    this._intakeConfig.getIntakePurpose()
                        .intakeservreqtypekey ===
                    MyNewintakeConstants.REFERRAL.INTERSTATE_COMPACT
                ) {
                    const createdCases = this.store[
                        IntakeStoreConstants.createdCases
                    ];
                    createdCases.forEach(createdCase => {
                        if (createdCase.isyouthincustody) {
                            reviewstatus.youthstatuseventcode = 'InCustody';
                        }
                    });
                } else if (
                    this._intakeConfig.getIntakePurpose()
                        .intakeservreqtypekey ===
                    MyNewintakeConstants.REFERRAL.ADULT_HOLD_DETENTION ||
                    this._intakeConfig.getIntakePurpose()
                        .intakeservreqtypekey ===
                    MyNewintakeConstants.REFERRAL.WAIVER_FROM_ADUL_COURT
                ) {
                    reviewstatus.ispreintakeapproved = true;
                }
                const addedPersons = this.store[
                    IntakeStoreConstants.addedPersons
                ];
                addedPersons.map(item => {
                    item.Pid = item.Pid ? item.Pid : '';
                    if (item.emailID) {
                        item.contactsmail = item.emailID;
                    } else {
                        item.contactsmail = [];
                    }
                    if (item.phoneNumber) {
                        item.contacts = item.phoneNumber;
                    } else {
                        item.contacts = [];
                    }
                    if (item.personAddressInput) {
                        item.address = item.personAddressInput;
                    } else {
                        item.address = [];
                    }
                });
                if (!this.isDjs && disposition && disposition.length > 0) {
                    disposition.map(item => {
                        item.intakeMultipleDispositionDropdown = [];
                        item.supMultipleDispositionDropdown = [];
                    });
                }
                if (this.isCW) {
                    if (
                        disposition[0].dispositioncode ===
                        disposition[0].supDisposition
                    ) {
                        general.isDisposition = false;
                    } else {
                        general.isDisposition = true;
                    }
                    if (this.addSdm) {
                        disposition[0].issubtypekey = true;
                        general.Iscps = this.addSdm.iscps
                            ? this.addSdm.iscps
                            : null;
                        if (this.subServiceTypes) {
                            this.subServiceTypes.map(item => {
                                if (
                                    this.addSdm.cpsResponseType ===
                                    item.classkey
                                ) {
                                    disposition[0].DasubtypeKey =
                                        item.servicerequestsubtypeid;
                                }
                            });
                        }
                    } else {
                        general.Iscps = null;
                    }
                }
                this._dataStoreService.setData(
                    IntakeStoreConstants.disposition,
                    disposition
                );
                const roleId = this._authService.getCurrentUser();
                const validateFocuspersonCaseDJS =
                    this.isDjs &&
                    (roleId.role.name === AppConstants.ROLES.SUPERVISOR ||
                        roleId.role.name === AppConstants.ROLES.INTAKE_WORKER ||
                        roleId.role.name ===
                        AppConstants.ROLES.OFFICE_PROFFESSIONAL);
                if (intake) {
                    const intakeSaveModel = {
                        CrossReferences: this.addedCrossReference
                            ? this.addedCrossReference.map(
                                item => new CrossReference(item)
                            )
                            : [],
                        persondetails: {
                            Person: this.store[
                                IntakeStoreConstants.addedPersons
                            ]
                        },
                        entities: this.store[
                            IntakeStoreConstants.addedEntities
                        ],
                        createdCases: this.store[
                            IntakeStoreConstants.createdCases
                        ],
                        intakeDATypeDetails: this.store[
                            IntakeStoreConstants.disposition
                        ],
                        recordings: this.store[
                            IntakeStoreConstants.communications
                        ],
                        attachement: this.store[
                            IntakeStoreConstants.attachements
                        ],
                        disposition: disposition,
                        General: general,
                        narrative: null,
                        evaluationFields: this.isDjs
                            ? this.modifyEvalFields()
                            : null,
                        appointments: this.store[
                            IntakeStoreConstants.intakeappointment
                        ],
                        DAType: {
                            DATypeDetail: this.store[
                                IntakeStoreConstants.disposition
                            ]
                        },
                        reviewstatus: reviewstatus,
                        Allegations: this.isDjs ? this.formatAllegations() : [],
                        sdm: this.store[IntakeStoreConstants.intakeSDM],
                        clwStatus: null,
                        generatedDocuments: this.store[
                            IntakeStoreConstants.generatedDocuments
                        ],
                        communicationFields: this.store[
                            IntakeStoreConstants.communicationFields
                        ],
                        adultScreenTool: this._dataStoreService.getData(
                            IntakeStoreConstants.adultScreenTool
                        ),
                        focuspersoncasedetails: validateFocuspersonCaseDJS
                            ? this.modifyinterStateCompactDetails()
                            : [],
                        paymentSchedule: this._dataStoreService.getData(
                            IntakeStoreConstants.paymentSchedule
                        ),
                        unknownPersons: this._dataStoreService.getData(
                            IntakeStoreConstants.addedUnkPersons
                        ),
                        identifiedPersons: this._dataStoreService.getData(
                            IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS
                        ),
                        placement: this._dataStoreService.getData(
                            IntakeStoreConstants.addedPlacement
                        ),
                        reasonintakeinterview: this._dataStoreService.getData(
                            IntakeStoreConstants.reasonintakeinterview
                        ),
                        insufficientInfoNarrative: this._dataStoreService.getData(
                            IntakeStoreConstants.insufficientInfoNarrative
                        ),
                        complaintInfoReview: this._dataStoreService.getData(
                            IntakeStoreConstants.complaintInfoReview
                        ),
                        detentionOpened: this._dataStoreService.getData(
                            IntakeStoreConstants.detentionOpened
                        ),
                        roacps: this._dataStoreService.getData(
                            IntakeStoreConstants.roacps
                        ),
                        clearhistory: this._dataStoreService.getData(
                            IntakeStoreConstants.clearhistory
                        )
                    };

                    if (intakeSaveModel.sdm && intakeSaveModel.sdm.isfinalscreenin
                        && intakeSaveModel.sdm.isfinalscreenin === 'Ovr_as_noncps') {
                        intakeSaveModel.sdm.isfinalscreenin = 'true';
                    }
                    const addNarrative = this.store[
                        IntakeStoreConstants.addNarrative
                    ];
                    if (addNarrative) {
                        const Narrative = [{
                            Firstname: addNarrative.Firstname ? addNarrative.Firstname : '',
                            Lastname: addNarrative.Lastname ? addNarrative.Lastname : '',
                            Middlename: addNarrative.Middlename ? addNarrative.Middlename : '',
                            PhoneNumber: addNarrative.PhoneNumber ? addNarrative.PhoneNumber : '',
                            PhoneNumberExt: addNarrative.PhoneNumberExt ? addNarrative.PhoneNumberExt : '',
                            ZipCode: addNarrative.ZipCode ? addNarrative.ZipCode : '',
                            Role: addNarrative.Role ? addNarrative.Role : '',
                            organization: addNarrative.organization ? addNarrative.organization : '',
                            title: addNarrative.title ? addNarrative.title : '',
                            incidentlocation: addNarrative.incidentlocation ? addNarrative.incidentlocation : '',
                            incidentdate: addNarrative.incidentdate ? addNarrative.incidentdate : '',
                            isapproximate: addNarrative.isapproximate ? addNarrative.isapproximate : false,
                            email: addNarrative.email ? addNarrative.email : ''
                        }];
                        intakeSaveModel.narrative = Narrative;
                        intakeSaveModel.General.Firstname = addNarrative.Firstname
                            ? addNarrative.Firstname
                            : '';

                        intakeSaveModel.General.Middlename = addNarrative.Middlename
                            ? addNarrative.Middlename
                            : '';
                        intakeSaveModel.General.Lastname = addNarrative.Lastname
                            ? addNarrative.Lastname
                            : '';
                        intakeSaveModel.General.AgencyCode = this.store[
                            IntakeStoreConstants.agency
                        ];
                        intakeSaveModel.General.Source = intakeSaveModel.General
                            .InputSource
                            ? intakeSaveModel.General.InputSource
                            : '';
                    }
                    const finalIntake = {
                        intake: intakeSaveModel,
                        review: reviewstatus
                    };
                    if (this.selectteamtypekey) {
                        const selectedPurpose = this.getSelectedPurpose(
                            this.selectteamtypekey
                        );
                        if (selectedPurpose) {
                            if (
                                selectedPurpose.description ===
                                'Information and Referral'
                            ) {
                                finalIntake.review['isfromintake'] = true;
                                finalIntake.review['isclosecase'] = true;
                            }
                        }
                    }

                    // @DP - For In Home Service
                    if (
                        this.intakeservice &&
                        this.intakeservice.length &&
                        this.intakeservice[0].description === 'In Home Services'
                    ) {
                        if (this.subServiceTypes) {
                            this.subServiceTypes.forEach(item => {
                                if (
                                    this.intakeservice[0].intakeservtypekey ===
                                    item.classkey
                                ) {
                                    if (disposition && disposition.length) {
                                        disposition[0].DasubtypeKey =
                                            item.servicerequestsubtypeid;
                                    }
                                }
                            });
                        }
                    }
                    this.saveInProgress = true;
                    this._commonHttpService
                        .create(
                            finalIntake,
                            NewUrlConfig.EndPoint.Intake.SupervisorApprovalUrl
                        )
                        .subscribe(
                            response => {
                                this.saveInProgress = false;
                                if (
                                    response &&
                                    response.data &&
                                    response.data.data
                                ) {
                                    if (this.isrestricteditem) {
                                        const caseinfo = response.data.data[0];
                                        const userinfo = this._authService.getCurrentUser();
                                        const activeflag = 1;
                                        const selectedcaseworkerid = [];
                                        selectedcaseworkerid.push(userinfo.user.securityusersid);
                                        this._intakeService.createRestrictedItem(caseinfo.responseintakeserviceid, 'SERVICE', selectedcaseworkerid, activeflag)
                                            .subscribe(
                                                (response) => {
                                                    this._alertService.success('The generated case ' + caseinfo.responseservicereqnum + ' will be restricted as well.');
                                                }
                                            );
                                    }
                                    if (this.pathwayChange) {
                                        if (
                                            this.intakeservice[0]
                                                .description !==
                                            'In Home Services'
                                        ) {
                                            // @TM: By-pass SDM for In-Home Services
                                            this.saveInProgress = true;
                                            this.saveInitialSdm(
                                                this.addSdm
                                            ).subscribe(data => {
                                                this.saveInProgress = false;
                                                // this.onReload();

                                                this.showApproveIntakeAckmnt(
                                                    response.data.data
                                                );
                                                this.btnDraft = false;
                                            });
                                        }
                                    } else {
                                        // this.onReload();
                                        console.log('finalIntake', finalIntake);
                                        if (this.isCW && finalIntake.intake.disposition && finalIntake.intake.disposition.length) {
                                            const supervisorDecisionCode = finalIntake.intake.disposition[0].supDisposition;
                                            if (supervisorDecisionCode === 'OvrScrnout' ||
                                                supervisorDecisionCode === 'ScreenOUT' ||
                                                supervisorDecisionCode === 'rejected'
                                            ) {
                                                (<any>$('#screenout-intake-ackmt')).modal('show');
                                            } else {
                                                this.showApproveIntakeAckmnt(
                                                    response.data.data
                                                );
                                            }
                                        } else {
                                            this.showApproveIntakeAckmnt(
                                                response.data.data
                                            );
                                        }

                                        this.btnDraft = false;
                                    }
                                } else {
                                    this._alertService.error(
                                        'Unable to approve intake, please try again.'
                                    );
                                    console.log(
                                        'Approve Intake Error',
                                        response
                                    );
                                }
                            },
                            error => {
                                this.saveInProgress = false;
                                this._alertService.error(
                                    'Unable to approve intake, please try again.'
                                );
                                console.log('Approve Intake Error', error);
                                return false;
                            }
                        );
                }
            }
        } else {
            ControlUtils.validateAllFormFields(
                this.departmentActionIntakeFormGroup
            );
            if (
                this.departmentActionIntakeFormGroup
                    .get('InputSource')
                    .hasError('required')
            ) {
                this._alertService.error('Please input communication');
            } else {
                this._alertService.warn('Please input the mandatory fields.');
            }
        }
    }
    onReload() {
        let url = '';
        if (this.isCLW) {
            url = '/pages/sao-dashboard';
        } else if (this.roleId.role.name === AppConstants.ROLES.SUPERVISOR) {
            if (this.isIntakeFromSupervisor) {
                url = '/pages/cjams-dashboard/my-intake-summary';
            } else {
                url = this._intakeService.loadIntakeDashboard();
            }
        } else {
            url = '/pages/newintake/new-saveintake';
        }
        this._alertService.success('Intake Submitted Successfully!', true);
        this._router.navigate([url]);
        /*  Observable.timer(500).subscribe(() => {
             this._router.routeReuseStrategy.shouldReuseRoute = function () {
                 return false;
             };
             this._router.navigate([url]);
         }); */
    }

    getSubCategory() {
        if (this.store[IntakeStoreConstants.purposeSelected]) {
            const purpose = this.store[IntakeStoreConstants.purposeSelected];
            this.subType = this.store[IntakeStoreConstants.createdCases];
            // if (this.store['purposesubtype']) {
            // const subType =  this.store['purposesubtype'];
            if (this.subType && this.subType.length > 0) {
                const purposeSubType = this.subType[0].subServiceTypeID;
                this.subCategoryClassificationType$ = this._commonHttpService
                    .getArrayList(
                        {
                            where: {
                                intakeservicerequesttypeid: purpose.value,
                                intakeservicerequestsubtypeid: purposeSubType,
                                agencycode: 'AS',
                                target: 'Intake'
                            },
                            method: 'get'
                        },
                        'admin/assessmenttemplate/listassessmenttemplate?filter'
                    )
                    .map(result => {
                        return result;
                    });
                this.subCategoryClassificationType$.subscribe(result => {
                    console.log('result', result);
                    if (result && result.length > 0) {
                        this.subCategoryList = [];
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].isrequired === true) {
                                this.subCategoryList.push(result[i]);
                            }
                        }
                    }
                    this._dataStoreService.setData(
                        'categorySubType',
                        this.subCategoryList
                    );
                });
            }
        }
    }

    processLegalGuardian() {
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        let isLegalGardianPresent = false;
        const legalGuardian = [];
        if (addedPersons && addedPersons.length > 0) {
            addedPersons.map(item => {
                if (item && item.personRole && Array.isArray(item.personRole)) {
                    const roleArray = item.personRole;
                    roleArray.forEach(role => {
                        if (role.rolekey === 'LG') {
                            isLegalGardianPresent = true;
                            // D-13414: The legal guardian name should display first name then last name
                            // In item.fullName the name reversed last then first, so manually forming it
                            // legalGuardian.push(item.fullName);
                            const firstname = item.Firstname ? item.Firstname : '';
                            const lastname = item.Lastname ? item.Lastname : '';
                            const fullname = firstname + ' ' + lastname;
                            legalGuardian.push(fullname);
                        }
                    });
                    if (item.isheadofhousehold) {
                        this.departmentActionIntakeFormGroup.patchValue({
                            HeadofHousehold: item.fullName
                        });
                    }
                }

            });
        }
        this.departmentActionIntakeFormGroup.patchValue({
            LegalGuardian: legalGuardian.toString()
        });
    }

    /* isOtherThanRiskHarmSelected() {
        const sdmForm = this.store[IntakeStoreConstants.intakeSDM];
        const phyAbuse = sdmForm.physicalAbuse;
        const sexAbuse = sdmForm.sexualAbuse;
        const genNeglect = sdmForm.generalNeglect;
        const unattChild = sdmForm.unattendedChild;

        const isPhyAbuse = (phyAbuse.ismalpa_suspeciousdeath || phyAbuse.ismalpa_nonaccident ||
            phyAbuse.ismalpa_injuryinconsistent || phyAbuse.ismalpa_insjury || phyAbuse.ismalpa_childtoxic
            || phyAbuse.ismalpa_caregiver) ? true : false;
        const isSexAbuse = (sexAbuse.ismalsa_sexualmolestation || sexAbuse.ismalsa_sexualact
            || sexAbuse.ismalsa_sexualexploitation || sexAbuse.ismalsa_physicalindicators) ? true : false;
        const isGenNeglect = (sdmForm.isnegfp_cargiverintervene || sdmForm.isnegab_abandoned ||
            sdmForm.isnegmn_unreasonabledelay ||
            unattChild.isneguc_leftunsupervised || unattChild.isneguc_leftaloneinappropriatecare || unattChild.isneguc_leftalonewithoutsupport ||
            genNeglect.isneggn_suspiciousdeath || genNeglect.isneggn_signsordiagnosis || genNeglect.isneggn_inadequatefood || genNeglect.isneggn_childdischarged) ? true : false;
        return isPhyAbuse || isSexAbuse || isGenNeglect;
    } */

    conditionalValidation(): boolean {
        // @TM - mandatory check for alleged incident date
        const narrative = this.store[IntakeStoreConstants.addNarrative];
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        const addedUnkPersons = this.store[IntakeStoreConstants.addedUnkPersons];
        const intakeSDMObj = this.store[IntakeStoreConstants.intakeSDM];
        const roacps = this.store[IntakeStoreConstants.roacps];
        this.caseCreated = this.store[IntakeStoreConstants.createdCases];
        let isInHomeService = false;
        let isChildPresent = false;
        let isLegalGardianPresent = false;
        let isHeadofHousehold = false;
        const DADisposition = this.store[IntakeStoreConstants.disposition];
        if (DADisposition && DADisposition.length > 0) {
            DADisposition.forEach(disp => {
                this.dispcode = disp.dispositioncode;
            });
        }

        const isSDMConfigured = this.agencyTabOrder.find(tab => tab.id === 'sdm');

        if (isSDMConfigured && intakeSDMObj) {
            if (!intakeSDMObj.maltreatment) {
                this.intakeErrorMessage =
                    'Provider Involved Maltreatment under SDM must have a selection!';
                (<any>$('#intake-error')).modal('show');
                return false;
            }

            if (intakeSDMObj.isfinalscreenin === true && intakeSDMObj.immediate === '') {
                this.intakeErrorMessage =
                    'Response Time Decision under SDM must be updated!';
                (<any>$('#intake-error')).modal('show');
                return false;
            }
        }

        /* if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.RISK_OF_HARM_INTAKE)) {
            if (intakeSDMObj && this.isOtherThanRiskHarmSelected()) {
                this.intakeErrorMessage = 'Please change purpose as Child Protection Services as the SDM Maltreatment have selection that makes this Intake irrelevant to Risk of Harm';
                (<any>$('#intake-error')).modal('show');
                return false;
            }
        } */

        if (this.intakeservice.length > 0) {
            const service = this.intakeservice[0];
            isInHomeService =
                service.description === 'In Home Services' ? true : false;
            if (isInHomeService) {
                this.departmentActionIntakeFormGroup.controls[
                    'IntakeServiceSubtype'
                ].setValidators([Validators.required]);
                // this.departmentActionIntakeFormGroup.controls[
                //     'IntakeServiceSubtype'
                // ].updateValueAndValidity();
            } else {
                this.departmentActionIntakeFormGroup.controls[
                    'IntakeServiceSubtype'
                ].clearValidators();
                this.departmentActionIntakeFormGroup.controls[
                    'IntakeServiceSubtype'
                ].updateValueAndValidity();
            }
        }
        if (addedPersons && addedPersons.length > 0) {
            const headofhouseholdlist = addedPersons.filter( person => person.isheadofhousehold === true);
            if (headofhouseholdlist && headofhouseholdlist.length === 0) {
                isHeadofHousehold = true;
            } else {
                isHeadofHousehold = false;
            }
            addedPersons.map(item => {
                // @TM: Set ROH (Risk of Harm) flag for SEN (Substance Exposed New-born) case
                if (item.drugexposednewbornflag === 1) {
                    this.isSENflag = true;
                } else {
                    this.isSENflag = false;
                }
                if (!this.isDjs) {
                    const roles = item.personRole;
                    if (item.Role === undefined) {
                        // quick for a blocker issue
                        item.Role =
                            item.personRole !== undefined
                                ? item.personRole.role
                                : '';
                        if (roles && roles.length > 0) {
                            item.Role = roles[0].rolekey;
                        }
                    }
                    const unknownPersonsTemp = this._dataStoreService.getData(
                        IntakeStoreConstants.addedUnkPersons);
                    if(unknownPersonsTemp){
                        unknownPersonsTemp.forEach(element => {
                            if(element.role == 'Child'){
                                isChildPresent = true;
                                this.roleValue = true;
                            }
                        });
                    }
                    const personrole = item.personRole;
                    personrole.map(person => {
                        if (
                            [
                                'BIOCHILD',
                                'CHILD',
                                'NVC',
                                'OTHERCHILD',
                                'PAC',
                                'RC'
                            ].includes(person.rolekey)
                        ) {
                            isChildPresent = true;
                            this.roleValue = true;
                        }
                        if (person.rolekey === 'AV') {
                            isChildPresent = true;
                            this.roleValue = true;
                        }
                        if (person.rolekey === 'RA' || item.Role === 'CLI') {
                            this.roleValue = true;
                        }
                    });
                    // if (item.Role === 'RA' || ['BIOCHILD', 'CHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC'].includes(item.Role) || item.Role === 'CLI') {
                    //     this.roleValue = true;
                    // }
                    if (
                        item.Role === 'RC' &&
                        (item.drugexposednewbornflag ||
                            item.fetalalcoholspctrmdisordflag)
                    ) {
                        const purposeSelected = this.store[
                            IntakeStoreConstants.purposeSelected
                        ];
                        if (
                            !this._intakeConfig.selectedPurposeIs(
                                MyNewintakeConstants.PURPOSE
                                    .CHILD_PROTECTION_SERVICES
                            )
                        ) {
                            this.isPurposeNotCPS = true;
                        }

                        if (!_.isEmpty(item.Dob)) {
                            const receivedDt = this.departmentActionIntakeFormGroup.getRawValue()
                                .RecivedDate;
                            const DobDt = moment(item.Dob, 'MM/DD/YYYY');
                            if (moment(receivedDt).isBefore(DobDt)) {
                                this.isRcvdDtBeforeDob = true;
                            }
                        }
                    }

                    const isLG = roles.some(role => role.rolekey === 'LG');

                    if (isLG) {
                        isLegalGardianPresent = true;
                        this.departmentActionIntakeFormGroup.patchValue({
                            LegalGuardian: item.fullName
                        });
                    }
                    if (this.caseCreated && this.caseCreated.length > 0) {
                        if (
                            this.caseCreated[0].serviceTypeValue === 'Provider'
                        ) {
                            if (this.isAS) {
                                if (item.Role === 'PA') {
                                    this.roleValue = true;
                                } else {
                                    this.roleValue = false;
                                    this._alertService.error(
                                        'Please add Provider Applicant as primary role.'
                                    );
                                    return false;
                                }
                            }
                        }
                    }
                } else {
                    if (item.Role === 'Youth') {
                        this.roleValue = true;
                    }
                }
            });
        } else {
            if (addedUnkPersons && addedUnkPersons.length > 0) {
              addedUnkPersons.map(person => {
                if (person.role === 'Child' || person.role === 'Alleged Victim') {
                    this.unknown = true;
                } else {
                    this.unknown = false;
                    if (this.dispcode === 'ScreenOUT') {
                    } else {
                        this._alertService.error('Please add a person to submit intake');
                        return false;
                    }
                }
              });
            } else {
                if (this.dispcode === 'ScreenOUT') {
                } else {
                    this._alertService.error('Please add a person to submit intake');
                    return false;
                }
            }
        }

        if (intakeSDMObj && intakeSDMObj.cpsResponseType &&
            (intakeSDMObj.cpsResponseType === 'CPS-AR' ||
                intakeSDMObj.cpsResponseType === 'CPS-IR')) {       // @TM: CPS IR & CPS AR over-ride SEN cases
            this.isSENflag = false;
        } else if (intakeSDMObj && intakeSDMObj.riskofHarm &&
            ObjectUtils.checkTrueProperty(intakeSDMObj.riskofHarm) >= 1) {
            this.isSENflag = true;
        }

        if (this.isCW) {
            console.log(this.store);
            // if (!(isLegalGardianPresent)) {
            //     this._alertService.error('Please add Legal Gardian.');
            //     return false;
            // }
            const purpose = this.store[IntakeStoreConstants.purposeSelected];
            // const clearHistory = this.store[IntakeStoreConstants.clearhistory];
            // if (purpose && purpose.code === 'Request for services') {
            //     // if (clearHistory && ( !clearHistory.reason  || clearHistory.reason === '')) {
            //     //     this._alertService.error('Please fill Reason for CPS History Clearance.');
            //     //     return false;
            //     // }
            // }
            //D-15457/Aug22:-CHESIE/Business teams do not want 'add child' validation for 'Request for services'
            if (purpose && purpose.code !== 'Information and Referral' && purpose.code !== 'ROACPS' && purpose.code !== 'Request for services') {
                if (!isChildPresent && !this.isClearenceHistory) {
                    if (this.dispcode === 'ScreenOUT' || this.unknown) {
                    } else {
                        this._alertService.error('Please add Child.');
                        return false;
                    }
                }
                if (this.roleValue === false && !this.isClearenceHistory && !this.unknown && !(this.dispcode === 'ScreenOUT')) {
                    this._alertService.error(
                        'Please add Reported Child as primary role.'
                    );
                    return false;
                }
            }
            // if (purpose.code === 'CHILD' && isHeadofHousehold) {
            //     this._alertService.error(
            //         'Please add Head of Household'
            //     );
            //     return false;
            // }
            if (this.isPurposeNotCPS) {
                this.isPurposeNotCPS = false; // reset the value
                this._alertService.error(
                    'Choose Child Protective Services as the Purpose'
                );
                return false;
            }
            if (this.isRcvdDtBeforeDob) {
                this.isRcvdDtBeforeDob = false; // reset the value
                this._alertService.error(
                    'Received Date has to be after the Date Of Birth of the Child'
                );
                return false;
            }
            // if (this.departmentActionIntakeFormGroup.get('countyid').value == '' || this.departmentActionIntakeFormGroup.get('countyid').value == null) {
            //     this._alertService.error('Please add a jurisdiction to submit intake');
            //     return false;
            // }
            // }

            // @TM: Date of Alleged Incident validation
            if (purpose && purpose.code === 'CHILD') {
                if (!narrative.incidentdate || (narrative.incidentdate && narrative.incidentdate === '')) {
                    this._alertService.error(
                        'Please enter Date of Alleged Incident under Narrative tab to submit intake'
                    );
                    return false;
                }
            }

            if (purpose && purpose.code === 'Request for services') {
                if (!(this.intakeservice.length > 0)) {
                    if (!this.departmentActionIntakeFormGroup.get('IntakeService').value) {
                        this._alertService.error(
                            'Please select atleast one Service type to submit intake'
                        );
                        return false;
                    }
                } else if (
                    this.intakeservice &&
                    this.intakeservice.some(
                        item => (item.description === 'In Home Services' && !item.intakesubservice)
                    )
                ) {
                    if (!this.departmentActionIntakeFormGroup.get('IntakeService').value) {
                        this._alertService.error(
                            'Please select atleast one Service type to submit intake'
                        );
                        return false;
                    } else {
                        (<any>$('#ihm-sub-type-popup')).modal('show');
                        this._alertService.error(
                            'Please select In-Home Service subtype to submit intake'
                        );
                        return false;
                    }
                }
                if (this.isIndependentLiving) {
                    const validPersonsForIPL = addedPersons.filter(person => {
                        const validAge = moment().diff(person.Dob, 'years', true) >= 16;
                        const validRoles = person.personRole.find(item => item.rolekey === 'CHILD');
                        if (validAge && validRoles) {
                            return true;
                        } else {
                            return false;
                        }
                        });
                    if (!validPersonsForIPL.length) {
                        this._alertService.error(
                            'Independent Living is allowed only for Child(ren) greater than 16 years old.'
                        );
                        return false;
                    }
                }
            }
            
            if (purpose && purpose.code !== 'Information and Referral'){
                if(!narrative.IsAnonymousReporter && !narrative.IsUnknownReporter && (!narrative.Role || (narrative.Role && (narrative.Role === '' || narrative.Role === 'Rep')))) {
                    this._alertService.error(
                        'Please select Role under Narrative tab to submit intake'
                    );
                    return false;
                }
            }

            if (
                this.departmentActionIntakeFormGroup.get('IntakeService')
                    .value &&
                this.selectedCheckbox === 'In Home Services' &&
                !this.departmentActionIntakeFormGroup.get(
                    'IntakeServiceSubtype'
                ).value
            ) {
                this._alertService.error(
                    'Please select In-Home Service subtype to submit intake'
                );
                return false;
            }

            if(purpose.code === 'ROACPS') { 

                if (roacps &&  !roacps.servicerequested ) {
                    this._alertService.error('Please fill Type of Services Requested  under ROA CPS tab.');
                    return false;
                }
                if (roacps && !roacps.statetype ) {
                    this._alertService.error('Please fill state type  under ROA CPS tab.');
                    return false;
                }
                if(roacps && roacps.statetype === 'instate' && !roacps.cpsid ) {
                    this._alertService.error('Please fill cpsid  under ROA CPS tab.');
                    return false;
                }
            }
        } else if (this.isDjs) {
            if (this.roleValue === false) {
                this._alertService.error('Please add user of role Youth.');
                return false;
            }
        } else if (this.isAS) {
            if (this.roleValue === false) {
                if (
                    this.caseCreated &&
                    this.caseCreated.length > 0 &&
                    this.caseCreated[0].serviceTypeValue !== 'Provider'
                ) {
                    this._alertService.error(
                        'Please add Reported Adult as primary role.'
                    );
                }
                return false;
            }
        }

        const roleId = this._authService.getCurrentUser();
        const disposition = this.store[IntakeStoreConstants.disposition];
        if (roleId.role.name !== AppConstants.ROLES.SUPERVISOR) {
            if (roleId.role.name !== AppConstants.ROLES.OFFICE_PROFFESSIONAL) {
                if (disposition && disposition.length > 0) {
                    if (
                        !disposition[0].dispositioncode ||
                        !disposition[0].intakeserreqstatustypekey
                    ) {
                        this._alertService.error(
                            'Please fill status and disposition'
                        );
                        return false;
                    }
                    if (disposition[0].restitution) {
                        const scheduledPayments = this.store[
                            IntakeStoreConstants.paymentSchedule
                        ];
                        if (
                            !scheduledPayments ||
                            scheduledPayments.length === 0
                        ) {
                            this._alertService.error(
                                'Please add payment schedules.'
                            );
                            return false;
                        }
                    }
                } else {
                    this._alertService.error('Please fill status and decision');
                    return false;
                }
            }
        } else if (roleId.role.name === AppConstants.ROLES.SUPERVISOR) {
            if (disposition && disposition.length > 0) {
                const timeLeft = this.store[IntakeStoreConstants.timeleft];
                if (!disposition[0].supStatus) {
                    this._alertService.error('Please select the status');
                    return false;
                }

                // Non CPS we don't want the delay reason
                if (!this._intakeConfig.isNonCPS()) {
                    if (!disposition[0].reason && timeLeft === 'Overdue') {
                        this._alertService.error(
                            'Please input the reason for delay'
                        );
                        return false;
                    }
                }

                if (!disposition[0].supDisposition) {
                    this._alertService.error(
                        'Please select case, decision and then save'
                    );
                    return false;
                }
                // D-11889 - CJAMS - CW -  Decision Tab
                if (disposition[0].supDisposition !== disposition[0].dispositioncode) {
                    if (!disposition[0].supComments) {
                        this._alertService.error(
                            'Please fill the comments in decision'
                        );
                        return false;
                    }
                }
                if (disposition[0].supRestitution) {
                    const scheduledPayments = this.store[
                        IntakeStoreConstants.paymentSchedule
                    ];
                    if (!scheduledPayments || scheduledPayments.length === 0) {
                        this._alertService.error(
                            'Please add payment schedules.'
                        );
                        return false;
                    }
                }
                if (this.roleId.user.userprofile.teamtypekey === 'CW') {
                    if (
                        this.intakeservicesubtype &&
                        this.intakeservicesubtype.filter(
                            item => item.intakeservsubtypekey === 'FPS'
                        ).length &&
                        disposition[0].supStatus === 'Approved'
                    ) {
                        const isAdultValidAge = this.validateChildAge(
                            addedPersons
                        );
                        if (!isAdultValidAge) {
                            this._alertService.error(
                                'To be able to accept, there must be one adult(18 and over) and one child(birth to 18 years) in the case'
                            );
                            return false;
                        }
                    }
                }
            } else {
                this._alertService.error('Please fill status and disposition');
                return false;
            }
        }
        if (this.departmentActionIntakeFormGroup.value.isOtherAgency) {
            if (this.departmentActionIntakeFormGroup.value.otheragency === '') {
                this._alertService.error(
                    'Please fill request from other agency'
                );
                return false;
            }
        }

        if (this.isAS && this.isIntakeWorker) {
            this.attachmentCreated = this.store[
                IntakeStoreConstants.attachements
            ];
            if (this.caseCreated && this.caseCreated.length > 0) {
                if (
                    this.caseCreated[0].serviceTypeValue === 'Provider' ||
                    this.caseCreated[0].subSeriviceTypeValue ===
                    'Project Home ' ||
                    this.caseCreated[0].subSeriviceTypeValue ===
                    'Adult Foster Care'
                ) {
                    this.getSubCategory();
                    if (
                        this.attachmentCreated &&
                        this.attachmentCreated.length > 0
                    ) {
                        this.attachementRequired = this.store[
                            'categorySubType'
                        ];
                        this.categoryList = [...this.attachementRequired];
                        this.attachmentCreated.forEach(created => {
                            const foundDoc = this.attachementRequired.find(
                                required =>
                                    required.assessmenttemplateid ===
                                    created.documentattachment
                                        .assessmenttemplateid
                            );
                            if (foundDoc) {
                                this.categoryList.splice(
                                    this.categoryList.indexOf(foundDoc),
                                    1
                                );
                            }
                        });
                        if (this.categoryList && this.categoryList.length > 0) {
                            this.missingSubCategory = [];
                            for (let i = 0; i < this.categoryList.length; i++) {
                                this.missingSubCategory.push(
                                    this.categoryList[i].titleheadertext
                                );
                            }
                            (<any>$('#not-generated-subcategories')).modal(
                                'show'
                            );
                            // this._alertService.error('Please add respective subcategories');
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        if (
                            this.subCategoryList &&
                            this.subCategoryList.length > 0
                        ) {
                            this.missingSubCategory = [];
                            for (
                                let i = 0;
                                i < this.subCategoryList.length;
                                i++
                            ) {
                                this.missingSubCategory.push(
                                    this.subCategoryList[i].titleheadertext
                                );
                            }
                            (<any>$('#not-generated-subcategories')).modal(
                                'show'
                            );
                            // this._alertService.error('Please add attachments');
                            return false;
                        } else {
                            this._alertService.error('Please add attachments');
                            return false;
                        }
                    }
                }
            }
        }

        if (!this.isDjs) {
            const addNarrative = this.store[IntakeStoreConstants.addNarrative];
            if (addNarrative.Narrative === '') {
                this._alertService.error('Please fill narrative');
                return false;
            }

            if (addNarrative.requestercity === '') {
                this._alertService.error('Please fill city');
                return false;
            }

            if (addNarrative.requesterstate === '') {
                this._alertService.error('Please fill state');
                return false;
            }

            if (addNarrative.requestercounty === '') {
                this._alertService.error('Please fill county');
                return false;
            }

            if (
                !addNarrative.IsAnonymousReporter &&
                !addNarrative.IsUnknownReporter &&
                !this.intakeInfoNreffGrid
            ) {
                if (
                    addNarrative.Firstname === '' ||
                    addNarrative.Lastname === ''
                ) {
                    this._alertService.error(
                        'Please enter reporter details to submit this intake.'
                    );
                    return false;
                }
            } else if (
                !addNarrative.RefuseToShareZip &&
                this.intakeInfoNreffGrid
            ) {
                if (addNarrative.ZipCode === '') {
                    this._alertService.error(
                        'Please fill zip code in narrative'
                    );
                    return false;
                }
            }
        }
        if (
            this.isDjs &&
            !this._intakeConfig.selectedPurposeIs(
                MyNewintakeConstants.REFERRAL.ADULT_HOLD_DETENTION
            ) &&
            (roleId.role.name === AppConstants.ROLES.INTAKE_WORKER ||
                (roleId.role.name === AppConstants.ROLES.SUPERVISOR &&
                    this.intakeStore.action === 'add'))
        ) {
            const evalFields = this._dataStoreService.getData(
                IntakeStoreConstants.evalFields
            );
            if (evalFields && evalFields.length > 0) {
                const mcaspreq = evalFields.filter(data => data.isMcapsReq);
                if (mcaspreq && mcaspreq.length > 0) {
                    const mcaspcompleted = evalFields.filter(
                        data => data.MCASPCompleted
                    );
                    if (mcaspcompleted && mcaspcompleted.length > 0) {
                    } else {
                        this._alertService.error(
                            'Please complete MCASP RISK ASSESSMENT'
                        );
                        return false;
                    }
                }
            }
        }
        if (
            this.isDjs &&
            (roleId.role.name === AppConstants.ROLES.SUPERVISOR ||
                roleId.role.name === AppConstants.ROLES.INTAKE_WORKER ||
                roleId.role.name === AppConstants.ROLES.OFFICE_PROFFESSIONAL)
        ) {
            if (
                this._intakeConfig.selectedPurposeIs(
                    MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT
                )
            ) {
                const lawenforcementdetails = this.store[
                    IntakeStoreConstants.FocuspersonCaseDetails
                ];
                if (lawenforcementdetails) {
                    if (lawenforcementdetails.iswritwarrant) {
                        if (!lawenforcementdetails.warranttype) {
                            this._alertService.error(
                                'Please fill Youth Warrant Details!'
                            );
                            return false;
                        }
                        if (
                            lawenforcementdetails.warranttype ===
                            'Non-Delinquent' &&
                            (lawenforcementdetails.isdetaintheyouth === null ||
                                lawenforcementdetails.isdetaintheyouth ===
                                undefined ||
                                lawenforcementdetails.isdetaintheyouth === '')
                        ) {
                            this._alertService.error(
                                'Please fill Youth Warrant Details!'
                            );
                            return false;
                        }
                        if (!lawenforcementdetails.warranttypedetails) {
                            this._alertService.error(
                                'Please fill Youth Warrant Details!'
                            );
                            return false;
                        }
                        if (lawenforcementdetails.warranttypedetails <= 0) {
                            this._alertService.error(
                                'Please fill Youth Warrant Details!'
                            );
                            return false;
                        }
                    }
                } else {
                    this._alertService.error(
                        'Please fill Youth Warrant Details!'
                    );
                    return false;
                }
            }
            if (
                this._intakeConfig.selectedPurposeIs(
                    MyNewintakeConstants.REFERRAL.INTERSTATE_COMPACT
                )
            ) {
                const createdCases = this.store[
                    IntakeStoreConstants.createdCases
                ];
                if (createdCases && createdCases.length > 0) {
                    if (
                        createdCases[0].subSeriviceTypeValue ===
                        MyNewintakeConstants.Intake
                            .ICJRequisitionDuetoWarrant ||
                        createdCases[0].subSeriviceTypeValue ===
                        MyNewintakeConstants.Intake.ICJReceivingProbation ||
                        createdCases[0].subSeriviceTypeValue ===
                        MyNewintakeConstants.Intake.ICJReentry
                    ) {
                        const focusPersoncasedetails = this.store[
                            IntakeStoreConstants.FocuspersonCaseDetails
                        ];
                        if (focusPersoncasedetails) {
                            const icjformvalid = this.store[
                                IntakeStoreConstants
                                    .FocuspersonCaseDetailsFormValid
                            ];
                            if (!icjformvalid) {
                                this._alertService.error(
                                    'Please fill Interstate compact ' +
                                    createdCases[0].subSeriviceTypeValue +
                                    ' details!'
                                );
                                return false;
                            }
                            if (
                                createdCases[0].subSeriviceTypeValue ===
                                MyNewintakeConstants.Intake
                                    .ICJRequisitionDuetoWarrant
                            ) {
                                if (
                                    !focusPersoncasedetails.requisitiontypekey
                                ) {
                                    this._alertService.error(
                                        'Please fill Interstate compact ' +
                                        createdCases[0]
                                            .subSeriviceTypeValue +
                                        ' details!'
                                    );
                                    return false;
                                }
                            } else if (
                                !focusPersoncasedetails.maxdateofexpiration
                            ) {
                                this._alertService.error(
                                    'Please fill Interstate compact ' +
                                    createdCases[0].subSeriviceTypeValue +
                                    ' details!'
                                );
                                return false;
                            }
                            if (
                                !focusPersoncasedetails.demandingstate &&
                                !focusPersoncasedetails.countyid
                            ) {
                                this._alertService.error(
                                    'Please fill Interstate compact ' +
                                    createdCases[0].subSeriviceTypeValue +
                                    ' details!'
                                );
                                return false;
                            }
                            if (!focusPersoncasedetails.residingwithdetails) {
                                this._alertService.error(
                                    'Please fill Interstate compact ' +
                                    createdCases[0].subSeriviceTypeValue +
                                    ' details!'
                                );
                                return false;
                            }
                            if (
                                focusPersoncasedetails.residingwithdetails &&
                                focusPersoncasedetails.residingwithdetails
                                    .length <= 0
                            ) {
                                this._alertService.error(
                                    'Please fill Interstate compact ' +
                                    createdCases[0].subSeriviceTypeValue +
                                    ' details!'
                                );
                                return false;
                            }
                        } else {
                            this._alertService.error(
                                'Please fill Interstate compact ' +
                                createdCases[0].subSeriviceTypeValue +
                                ' details!'
                            );
                            return false;
                        }
                    }
                }
            }
            if (
                this._intakeConfig.selectedPurposeIs(
                    MyNewintakeConstants.REFERRAL.ADULT_HOLD_DETENTION
                )
            ) {
                const resedentialStatus = this._dataStoreService.getData(
                    'ResedentialPlacementStatus'
                );
                if (resedentialStatus) {
                    this._alertService.warn(
                        'Youth already in Residential Placement. Adult Hold cannot be processed.'
                    );
                    return false;
                }
                const addedPlacement = this.store[
                    IntakeStoreConstants.addedPlacement
                ];
                if (!addedPlacement) {
                    this._alertService.warn(
                        'Please add placement for Adult Hold.'
                    );
                    return false;
                }
            }
        }
        if (this.addSdm && this.pathwayChange && !this.addSdm.comments) {
            this._alertService.error('Please fill pathway change comments.');
            return false;
        }
        return true;
    }

    getSelectedPurpose(purposeID) {
        if (this.purposeList) {
            return this.purposeList.find(
                puroposeItem => puroposeItem.intakeservreqtypeid === purposeID
            );
        }
        return null;
    }

    private mapIntakeScreenInfo(model: General): IntakeScreen {
        const intakeScreen = new IntakeScreen();
        const addNarrative = this.store[IntakeStoreConstants.addNarrative];
        if (addNarrative) {
            model.cpsHistoryClearance = addNarrative.cpsHistoryClearance;
            model.Narrative = addNarrative.Narrative;
            model.IsAnonymousReporter = addNarrative.IsAnonymousReporter;
            model.IsUnknownReporter = addNarrative.IsUnknownReporter;
            model.RefuseToShareZip = addNarrative.RefuseToShareZip;
            model.offenselocation = addNarrative.offenselocation;
            model.narrativeUpdatedDate = addNarrative.narrativeUpdatedDate;
            intakeScreen.NarrativeIntake = [].concat.apply(
                Object.assign({
                    Firstname: addNarrative.Firstname,
                    Lastname: addNarrative.Lastname,
                    Middlename: addNarrative.Middlename,
                    PhoneNumber: addNarrative.PhoneNumber,
                    PhoneNumberExt: addNarrative.PhoneNumberExt,
                    ZipCode: addNarrative.ZipCode,
                    Role: addNarrative.Role,
                    organization: addNarrative.organization,
                    title: addNarrative.title,
                    incidentlocation: addNarrative.incidentlocation,
                    incidentdate: addNarrative.incidentdate,
                    isapproximate: addNarrative.isapproximate,
                    email: addNarrative.email,
                })
            );
        }

        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (addedPersons) {
            intakeScreen.Person = addedPersons.map(item => {
                ObjectUtils.removeEmptyProperties(item);
                return new Person(item);
            });
        }

        intakeScreen.EvaluationField = this.store[
            IntakeStoreConstants.evalFields
        ];
        intakeScreen.Appointments = this.store[
            IntakeStoreConstants.intakeappointment
        ];

        if (this.addedIntakeDATypeDetails) {
            intakeScreen.DAType.DATypeDetail = this.addedIntakeDATypeDetails.map(
                item => new DATypeDetail(item)
            );
        }

        const addedCrossReference = this.store[
            IntakeStoreConstants.addedCrossReference
        ];
        if (addedCrossReference) {
            intakeScreen.CrossReferences = addedCrossReference.map(
                item => new CrossReference(item)
            );
        }

        intakeScreen.Recording.Recordings = this.store[
            IntakeStoreConstants.communications
        ];
        intakeScreen.General = model;
        intakeScreen.Allegations = [].concat.apply(
            [],
            this.addedIntakeDATypeDetails.map(item => {
                return item.Allegations;
            })
        );
        const addedEntities = this.store[IntakeStoreConstants.addedEntities];
        if (addedEntities) {
            intakeScreen.Agency = addedEntities.map(item => new Agency(item));
        }
        intakeScreen.AttachmentIntake = this.store[
            IntakeStoreConstants.attachments
        ];

        return intakeScreen;
    }
    private populateIntake() {
        const roleDls = this._authService.getCurrentUser();
        const intakeId = this.store[IntakeStoreConstants.intakenumber];
        if (intakeId) {
            if (this.intake.count) {
                const response = this.intake;
                // .subscribe(
                //     response => {
                this.btnDraft = true;
                // this.timeLeft = response.data[0].timeleft;
                this.clwStatus = response.data[0].clwstatus;
                this.signedOffDate = response.data[0].signedOffDate;
                this.isPreIntake = response.data[0].ispreintake;
                this.intakeChessieid = response.data[0].intakechessieid;
                let isPreIntake = false;
                let status = '';
                this._dataStoreService.setData(
                    IntakeStoreConstants.ispreintake,
                    response.data[0].ispreintake
                );
                this._dataStoreService.setData(
                    IntakeStoreConstants.timeleft,
                    response.data[0].timeleft
                );
                if (response.data[0].jsondata) {
                    const intakeModel = response.data[0].jsondata;
                    this._dataStoreService.setData(
                        IntakeStoreConstants.intakeModel,
                        intakeModel
                    );
                    // this.intakerWorkerId = intakeModel.securityuserid;
                    this.addedCrossReference = intakeModel.crossReference;
                    this.saveasdraftReason = intakeModel.reasonforDraft;
                    let addedPersons = [];
                    if (this.isCW) {
                        if (this.commonInvolvedPersons
                            && this.commonInvolvedPersons.data
                            && this.commonInvolvedPersons.data.length) {
                            addedPersons = this.commonInvolvedPersons.data.map(person => {
                                return this._intakeConfig.mapOldJsonData(person);
                            });
                        }

                    } else {
                        addedPersons = intakeModel.persons
                            ? intakeModel.persons
                            : intakeModel.persondetails
                                ? intakeModel.persondetails.Person
                                : [];
                    }

                    this.addedIntakeDATypeDetails =
                        intakeModel.intakeDATypeDetails;
                    // this.recordings = intakeModel.recordings;
                    if (
                        !this.isDjs &&
                        intakeModel.General.Purpose.indexOf('~') === -1
                    ) {
                        intakeModel.General.Purpose =
                            intakeModel.General.Purpose +
                            '~' +
                            intakeModel.General.AgencyCode;
                    }
                    if (this.isDjs) {
                        this._dataStoreService.setData(
                            IntakeStoreConstants.reasonintakeinterview,
                            intakeModel.reasonintakeinterview
                        );
                        this._dataStoreService.setData(
                            IntakeStoreConstants.insufficientInfoNarrative,
                            intakeModel.insufficientInfoNarrative
                        );
                        this._dataStoreService.setData(
                            IntakeStoreConstants.complaintInfoReview,
                            intakeModel.complaintInfoReview
                        );
                        this._dataStoreService.setData(
                            IntakeStoreConstants.detentionOpened,
                            intakeModel.detentionOpened
                        );
                    }
                    const general = intakeModel.General;
                    if (this.isCW) {
                        this._dataStoreService.setData(
                            IntakeStoreConstants.roacps,
                            intakeModel.roacps
                        );
                        const isCPSHistoryClearance =
                            general &&
                            general.intakeservice &&
                            general.intakeservice.length &&
                            general.intakeservice.find(
                                service =>
                                    service.description ===
                                    'CPS History Clearance'
                            );

                        if (isCPSHistoryClearance) {
                            this.isClearenceHistory = true;
                            this._dataStoreService.setData(
                                IntakeStoreConstants.clearhistory,
                                intakeModel.clearhistory
                            );
                            this.agencyTabOrder.splice(
                                2,
                                0,
                                IntakeTabConfig.find(
                                    item => item.id === 'history-clearance'
                                )
                            );
                            this.showSubmit = true;
                        }
                    }
                    this.onReceivedDateChange();
                    this.intakeNumber = intakeModel.General.IntakeNumber;
                    // this.preIntakeDisposition = intakeModel.preIntakeDispo;
                    this._dataStoreService.setData(
                        IntakeStoreConstants.preIntakeDisposition,
                        intakeModel.preIntakeDispo
                    );
                    this.reviewstatus = intakeModel.reviewstatus;
                        this.cwIntakeWorkerButton = (intakeModel.reviewstatus && intakeModel.reviewstatus.status) === 'supreview';
                    // this.evalFields = intakeModel.evaluationFields;
                    // this.createdCases = intakeModel.createdCases;
                    this.communicationFields = intakeModel.communicationFields;
                    // this._dataStoreService.setData(IntakeStoreConstants.createdCases, this.createdCases);

                    if (intakeModel) {
                        if (intakeModel.reviewstatus) {
                            isPreIntake = intakeModel.reviewstatus.ispreintake;
                            status = intakeModel.reviewstatus.status;
                        }
                        if (
                            intakeModel.DAType &&
                            intakeModel.DAType.DATypeDetail &&
                            intakeModel.DAType.DATypeDetail.length
                        ) {
                            // disposition = intakeModel.DAType.DATypeDetail;
                            this.reviewStatus = intakeModel.DAType.DATypeDetail[0].DAStatus;
                            // Resetting review status if intake event is draft
                            if (intakeModel.reviewstatus && intakeModel.reviewstatus.appevent === 'DRAFT') {
                                this.reviewStatus = intakeModel.reviewstatus.status;
                            }
                            // if (this.isDjs) {
                            //     this.reviewStatus = intakeModel.reviewstatus ? intakeModel.reviewstatus.status : intakeModel.DAType.DATypeDetail[0].DAStatus;
                            // } else {
                            //     this.reviewStatus = intakeModel.DAType.DATypeDetail[0].DAStatus;
                            // }
                            this._dataStoreService.setData(
                                IntakeStoreConstants.disposition,
                                intakeModel.DAType.DATypeDetail
                            );
                        } else if (intakeModel.reviewstatus) {
                            this.reviewStatus = intakeModel.reviewstatus.status;
                            // disposition = intakeModel.disposition;
                            this._dataStoreService.setData(
                                IntakeStoreConstants.disposition,
                                intakeModel.disposition
                            );
                        }
                    }
                    const narrativeDetails = new Narrative();
                    // if (
                    //     (this.roleId.role.name ===
                    //         AppConstants.ROLES.SUPERVISOR ||
                    //         this.roleId.role.name ===
                    //         AppConstants.ROLES.INTAKE_WORKER) &&
                    //     (this.reviewStatus === 'Closed' ||
                    //         this.reviewStatus === 'Approved')
                    // ) {
                    //     narrativeDetails.Firstname = general.Firstname;
                    //     narrativeDetails.Middlename = general.Middlename;
                    //     narrativeDetails.Lastname = general.Lastname;
                    //     narrativeDetails.ZipCode = general.offenselocation;
                    // } else {
                    if (
                        intakeModel.narrative &&
                        intakeModel.narrative.length > 0
                    ) {
                        narrativeDetails.Firstname = intakeModel.narrative
                            ? intakeModel.narrative[0].Firstname
                            : '';
                        narrativeDetails.Middlename = intakeModel.narrative
                            ? intakeModel.narrative[0].Middlename
                            : '';
                        narrativeDetails.Lastname = intakeModel.narrative
                            ? intakeModel.narrative[0].Lastname
                            : '';
                        narrativeDetails.ZipCode = intakeModel.narrative
                            ? intakeModel.narrative[0].ZipCode
                            : '';
                        narrativeDetails.PhoneNumber = intakeModel.narrative
                            ? intakeModel.narrative[0].PhoneNumber
                            : '';
                        narrativeDetails.PhoneNumberExt = intakeModel.narrative
                            ? intakeModel.narrative[0].PhoneNumberExt
                            : '';
                        narrativeDetails.incidentlocation = intakeModel.narrative
                            ? intakeModel.narrative[0].incidentlocation
                            : '';
                        narrativeDetails.incidentdate = intakeModel.narrative
                            ? intakeModel.narrative[0].incidentdate
                            : '';
                        narrativeDetails.isapproximate = intakeModel.narrative
                            ? intakeModel.narrative[0].isapproximate
                            : false;
                        narrativeDetails.email = intakeModel.narrative
                            ? intakeModel.narrative[0].email
                            : '';
                        narrativeDetails.Role = intakeModel.narrative
                            ? intakeModel.narrative[0].Role
                            : '';
                        narrativeDetails.organization = intakeModel.narrative
                            ? intakeModel.narrative[0].organization
                            : '';
                        narrativeDetails.title = intakeModel.narrative
                            ? intakeModel.narrative[0].title
                            : '';
                    }

                    narrativeDetails.Narrative = general.Narrative;
                    narrativeDetails.draftId = this.draftId;
                    narrativeDetails.IsAnonymousReporter =
                        general.IsAnonymousReporter === true ? true : false;
                    narrativeDetails.IsUnknownReporter =
                        general.IsUnknownReporter === true ? true : false;
                    narrativeDetails.RefuseToShareZip =
                        general.RefuseToShareZip === true ? true : false;
                    narrativeDetails.requesteraddress1 =
                        general.requesteraddress1;
                    narrativeDetails.requesteraddress2 =
                        general.requesteraddress2;
                    narrativeDetails.requestercity = general.requestercity;
                    narrativeDetails.requesterstate = general.requesterstate;
                    narrativeDetails.requestercounty = general.requestercounty;
                    narrativeDetails.offenselocation = general.offenselocation;
                    narrativeDetails.isacknowledgementletter = general.isacknowledgementletter
                        ? true
                        : false;
                    narrativeDetails.narrativeUpdatedDate = general.narrativeUpdatedDate ? general.narrativeUpdatedDate : null;
                    narrativeDetails.cpsHistoryClearance = general.cpsHistoryClearance? general.cpsHistoryClearance : null;
                    this.narrativeUpdatedTime = general.narrativeUpdatedDate ? general.narrativeUpdatedDate : null;
                    this.intakeAppointment = intakeModel.appointments;
                    this.genratedDocumentList = intakeModel.generatedDocuments;
                    this._dataStoreService.setData(
                        IntakeStoreConstants.reviewstatus,
                        this.reviewstatus
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.general,
                        general
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.addNarrative,
                        narrativeDetails
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.preIntakeDisposition,
                        intakeModel.preIntakeDispo
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.createdCases,
                        intakeModel.createdCases
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.intakenumber,
                        intakeModel.General.IntakeNumber
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.addedPersons,
                        addedPersons
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.evalFields,
                        intakeModel.evaluationFields
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.intakeappointment,
                        intakeModel.appointments
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.communications,
                        intakeModel.recordings
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.generatedDocuments,
                        intakeModel.generatedDocuments
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.saoResponse,
                        intakeModel.saoResponseDetail
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.courtDetails,
                        intakeModel.courtDetails
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.petitionDetails,
                        intakeModel.petitionDetails
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.scheduledHearings,
                        intakeModel.scheduledHearings
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.completedHearings,
                        intakeModel.completedHearings
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.communicationFields,
                        intakeModel.communicationFields
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.addedEntities,
                        intakeModel.entities
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.adultScreenTool,
                        intakeModel.adultScreenTool
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.paymentSchedule,
                        intakeModel.paymentSchedule
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.addedUnkPersons,
                        intakeModel.unknownPersons
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS,
                        intakeModel.identifiedPersons
                    );
                    this._dataStoreService.setData(
                        IntakeStoreConstants.addedPlacement,
                        intakeModel.placement
                    );
                    if (this.isCW) {
                        this._dataStoreService.setData(
                            IntakeStoreConstants.intakeSDM,
                            intakeModel.sdm
                        );
                        if (intakeModel.sdm) {
                            this.cpsResponseOffset = this.calculateResponseOffset(intakeModel.sdm);
                            this.processRecordingsList();
                            this._dataStoreService.setData(
                                IntakeStoreConstants.childfatality,
                                intakeModel.sdm.ischildfatality
                            );
                        }
                    }
                    this.processSelectedYouth();
                    this.processFocusPerson();
                    this.afterDataStoreSet();
                    // if (this.intakeAppointment && this.intakeAppointment.length > 0) {
                    //     this.IWtoAssign = this.intakeAppointment[0].intakeWorkerId;
                    // }
                    if (intakeModel.preIntakeDispo) {
                        this.preIntakeSupDicision =
                            intakeModel.preIntakeDispo.status;
                    }
                    if (intakeModel.sdm) {
                        intakeModel.sdm.datesubmitted =
                            response.data[0].datesubmitted;
                        this._dataStoreService.setData(
                            IntakeStoreConstants.intakeSDM,
                            intakeModel.sdm
                        );
                    }

                    if (general.Agency) {
                        const purpose =
                            general.Purpose &&
                                general.Purpose.indexOf('~') !== -1
                                ? general.Purpose.split('~')[0]
                                : general.Purpose;
                        const selectedPurpose = this.getSelectedPurpose(
                            purpose
                        );
                        const purposeDescription = selectedPurpose
                            ? selectedPurpose.description
                            : '';
                        this.showSubmit =
                            this.showSubmit ||
                            ['Information and Referral'
                            // 'ROACPS'
                        ].includes(
                                selectedPurpose.intakeservreqtypekey
                            );
                        if(this.isCW && this.isSupervisor  && this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.ROA_CPS)) {
                            this.showSubmit = true;
                        }
                        if (this.isDjs) {
                            this.listService({
                                text: purposeDescription,
                                value: general.Purpose,
                                label: general.Purpose.label
                                    ? general.Purpose.label
                                    : purposeDescription
                            });
                        } else {
                            this.listService({
                                text: '',
                                value: general.Purpose
                            });
                        }
                        this.selectteamtypekey = general.Purpose;
                        this.intakeservice = general.intakeservice;
                        this._dataStoreService.setData(
                            IntakeStoreConstants.intakeService,
                            this.intakeservice
                        );
                    }
                    if (
                        (this.reviewstatus &&
                            this.reviewstatus.appevent === 'INTR') ||
                        (this.reviewstatus &&
                            this.reviewstatus.appevent === 'KINR')
                    ) {
                        this.saveIntakeBtn = true;
                    }
                    if (
                        this.reviewstatus &&
                        this.reviewstatus.appevent === 'SITR' &&
                        roleDls.role.name ===
                        AppConstants.ROLES.OFFICE_PROFFESSIONAL
                    ) {
                        this.saveIntakeBtn = true;
                    }
                    if (
                        general.safeHavenAssessmentScore &&
                        general.safeHavenAssessmentScore >= 1
                    ) {
                        this.viewSafeHaven = true;
                    }
                    if (
                        general.kinshipAssessmentScore &&
                        general.kinshipAssessmentScore >= 1
                    ) {
                        this.viewKinship = true;
                    }
                    if (general.ascrsScore && general.ascrsScore >= 1) {
                        this.viewAscrs = true;
                    }
                    this.intakeservice = general.intakeservice;
                    this._dataStoreService.setData(
                        IntakeStoreConstants.intakeService,
                        this.intakeservice
                    );

                    if (
                        this.reviewstatus &&
                        this.reviewstatus.appevent === 'INTR'
                    ) {
                        this.saveIntakeBtn = true;
                    }
                    if (
                        this.reviewstatus &&
                        this.reviewstatus.appevent === 'SITR' &&
                        roleDls.role.name ===
                        AppConstants.ROLES.OFFICE_PROFFESSIONAL
                    ) {
                        this.saveIntakeBtn = true;
                    }
                    if (
                        general.safeHavenAssessmentScore &&
                        general.safeHavenAssessmentScore >= 1
                    ) {
                        this.viewSafeHaven = true;
                    }
                    if (
                        general.kinshipAssessmentScore &&
                        general.kinshipAssessmentScore >= 1
                    ) {
                        this.viewKinship = true;
                    }
                    if (general.ascrsScore && general.ascrsScore >= 1) {
                        this.viewAscrs = true;
                    }
                    // this.safeHavenAssessmentScore = this.general.safeHavenAssessmentScore;
                    // this.kinshipAssessmentScore = this.general.kinshipAssessmentScore;
                    // this.ascrsScore = this.general.ascrsScore;
                    this.patchFormGroup(general);
                    this.prepareCpsDocDetails(general.InputSource);
                    ControlUtils.markFormGroupTouched(
                        this.departmentActionIntakeFormGroup
                    );
                    let rcPerson = [];
                    rcPerson = addedPersons.filter(
                        person => person.Role === 'RC'
                    );
                    rcPerson.map(res => {
                        if (res.dateofdeath) {
                            this._dataStoreService.setData(
                                IntakeStoreConstants.childfatality,
                                'yes'
                            );
                        } else {
                            this._dataStoreService.setData(
                                IntakeStoreConstants.childfatality,
                                'no'
                            );
                        }
                    });

                    if (this.isDjs && this.selectedYouth) {
                        this.getYouthStaus();
                        if (
                            intakeModel.focuspersoncasedetails &&
                            intakeModel.focuspersoncasedetails.length >= 0
                        ) {
                            this._dataStoreService.setData(
                                IntakeStoreConstants.FocuspersonCaseDetailsFormValid,
                                true
                            );
                            this._dataStoreService.setData(
                                IntakeStoreConstants.FocuspersonCaseDetails,
                                intakeModel.focuspersoncasedetails[0]
                            );
                        }
                    }
                    this.checkHeaderEditability(isPreIntake, status); // Changes for UAT Defect 06781
                } else {
                    this.loadDefaults();
                }
            } else {
                this._alertService.error(
                    'Unable to fetch saved intake details.'
                );
                this.loadDefaults();
            }
            if (this.isDjs) {
                this.initializeDJSTabs();
            } else {
                this.initializeTabs();
            }
        } else {
            this.loadDefaults();
        }
    }

    // Added for UAT Defect 06781
    checkHeaderEditability(isPreIntake: boolean, status: string) {
        if (
            this.roleId.role.name === AppConstants.ROLES.INTAKE_WORKER &&
            (isPreIntake || status === 'supreview')
        ) {
            this.departmentActionIntakeFormGroup.disable();
        } else if (this.roleId.role.name === AppConstants.ROLES.INTAKE_WORKER) {
            this.departmentActionIntakeFormGroup.enable();
        } else if (
            this.roleId.role.name !== AppConstants.ROLES.OFFICE_PROFFESSIONAL
        ) {
            this.departmentActionIntakeFormGroup.disable();
        }
    }

    private patchFormGroup(general: General = new General()) {
        const recDate = new Date(general.RecivedDate);

        this.general.RecivedDate = recDate.toLocaleString();
        recDate.setUTCHours(0);
        recDate.setHours(23, 59, 59, 0);

        this.maxReceivedDate = new Date(general.CreatedDate);
       // this.maxReceivedDate = this._datePipe.transform(maxReceivedDate, "short");
        this.departmentActionIntakeFormGroup.patchValue({
            Source: general.Source ? general.Source : '',
            InputSource: general.InputSource ? general.InputSource : '',
            RecivedDate: new Date(this.general.RecivedDate),
            CreatedDate: general.CreatedDate ? general.CreatedDate : '',
            Author: general.Author ? general.Author : '',
            LegalGuardian: general.LegalGuardian ? general.LegalGuardian : '',
            IntakeNumber: general.IntakeNumber ? general.IntakeNumber : '',
            Agency: general.Agency ? general.Agency : '',
            countyid: general.countyid ? general.countyid : null,
            Purpose: general.Purpose ? general.Purpose : '',
            otheragency: general.otheragency ? general.otheragency : '',
            isOtherAgency: general.isOtherAgency
                ? general.isOtherAgency
                : false,
            islocalreferal: general.islocalreferal ? general.islocalreferal : 0,
            referalcomments: general.referalcomments
                ? general.referalcomments
                : '',
            nonreferalreason: general.nonreferalreason
                ? general.nonreferalreason
                : '',
            receiveddelay: general.receiveddelay ? general.receiveddelay : '',
            submissiondelay: general.submissiondelay
                ? general.submissiondelay
                : '',
            servicerequest: general.servicerequest
                ? general.servicerequest
                : '',
            suggestedresource: general.suggestedresource
                ? general.suggestedresource
                : '',
            voluntaryPlacementType: general.voluntaryPlacementType
                ? general.voluntaryPlacementType
                : ''
        });
        if (general.voluntaryPlacementType === 'EVPA') {
            this.isEVPA = true;
            this._dataStoreService.setData('isEVPA', this.isEVPA);
        } else {
            this._dataStoreService.setData('isEVPA', null);
        }
        this.setVolountaryStoreValues();
        if (this.departmentActionIntakeFormGroup.value.isOtherAgency) {
            this.otherAgencyControlName.enable();
        }
    }

    genCpsIntakeDoc(action: string) {
        this._dataStoreService.setData(
            IntakeStoreConstants.cpsDocument,
            action
        );
    }

    collectivePdfCreator() {
        this.downloadInProgress = true;
        const pdfList = [
            'Appeal-Letter',
            'Formal-Action-Letter',
            'Formal-Action-Letter-Complaint',
            'Process-Letter'
        ];
        pdfList.forEach(element => {
            this.downloadCasePdf(element);
        });
    }
    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then(canvas => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({
                        fileName: pageName,
                        images: pageImages
                    });
                    pageImages = [];
                }
            });
        }
        this.convertImageToPdf();
    }
    convertImageToPdf() {
        this.pdfFiles.forEach(pdfFile => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#intake-complaint-pdf1')).modal('hide');
        this.pdfFiles = [];
        this.downloadInProgress = false;
    }

    submitResource(resourcePoplabel) {
        this.submitResourceObject = Object.assign({}, new GeneralNarative());
        if (this.resourcePoplabel === 'CONTACT NUMBER') {
            this.submitResourceObject.controlindex = 0;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get(
                'helpDescription'
            ).value;
        } else if (this.resourcePoplabel === 'CREATED DATE') {
            this.submitResourceObject.controlindex = 1;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get(
                'helpDescription'
            ).value;
        } else if (this.resourcePoplabel === 'COMMUNICATION') {
            this.submitResourceObject.controlindex = 2;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get(
                'helpDescription'
            ).value;
        } else if (this.resourcePoplabel === 'REQUEST FROM OTHER AGENCY') {
        } else if (this.resourcePoplabel === 'AGENCY') {
            this.submitResourceObject.controlindex = 3;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get(
                'helpDescription'
            ).value;
        } else if (this.resourcePoplabel === 'PURPOSE') {
            this.submitResourceObject.controlindex = 4;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get(
                'helpDescription'
            ).value;
        } else if (this.resourcePoplabel === 'RECEIVED DATE') {
            this.submitResourceObject.controlindex = 5;
            this.submitResourceObject.helptext = this.generalResourceFormGroup.get(
                'helpDescription'
            ).value;
        }
        const jsonData = Object.assign({}, this.submitResourceObject);
        const submitResourceURL = 'Helptexts/addupdate';
        this._genericServiceNarative
            .create(jsonData, submitResourceURL)
            .subscribe(response => {
                (<any>$('#save-edit-resource-popup')).modal('hide');
                this.loadDefaults();
            });
    }

    prepareCpsDocDetails(input: string) {
        this.cpsdocData.intakePurpose = this.intakeServiceGrid;
        if (this.intakeCommunication) {
            this.intakeCommunication.map(data => {
                // if (data.value === input) {
                //     this.cpsdocData.InputSource = data.text;
                // }
                if (data.intakeservreqinputtypeid === input) {
                    this.cpsdocData.InputSource = data.description;
                    this._dataStoreService.setData(
                        IntakeStoreConstants.REFERALSOURCE,
                        {label: this.cpsdocData.InputSource || input}
                    );
                }
            });
        }
    }

    onChangeSupervisor(supervisor: RoutingUser) {
        console.log(supervisor);
        this.selectedSupervisor = supervisor.userid;
    }

    private listServiceSubtype(intakeservreqtypeid) {
        const checkInput = {
            include: 'servicerequestsubtype',
            nolimit: true,
            where: { intakeservreqtypeid: intakeservreqtypeid },
            method: 'get'
        };
        this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.DATypeUrl + '/?filter'
            )
            .subscribe(result => {
                this.subServiceTypes = result[0].servicerequestsubtype;
            });
    }

    private loadSupervisor() {
        this.selectedSupervisor = '';
        let appEvent = 'INTR';
        if (
            this.roleId.role.name ===
            AppConstants.ROLES.KINSHIP_INTAKE_WORKER ||
            this.roleId.role.name === AppConstants.ROLES.KINSHIP_SUPERVISOR
        ) {
            appEvent = 'KINR';
        }
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorsList = result.data;
                this.supervisorsList = this.supervisorsList.filter(
                    res => res.agencykey === this.selectedAgency.value
                );
                if (
                    this.isDjs &&
                    this.supervisorsList &&
                    this.supervisorsList.length === 1
                ) {
                    this.selectedSupervisor = this.supervisorsList[0].userid;
                    this.submitReviewIntake();
                } else {
                    (<any>$('#list-supervisor')).modal('show');
                }
            });
    }

    onChangeIntaker(intaker: RoutingUser) {
        this.selectedIntaker = intaker['userid'];
    }

    private loadIntaker() {
        this._commonHttpService
            .getAll('Intakedastagings/getIntakeUsers?filter={}')
            .subscribe(result => {
                this.intakersList = result;
            });
    }

    accessRights() {
        console.log('SIMAR ---> This is the role at Intake ', this.roleId);
        if (this.roleId.role.name === AppConstants.ROLES.SUPERVISOR) {
            if (this.isPreIntake) {
                this.accessStatus = this.reviewStatus !== 'supreview';
            } else {
                const acceptableStatusList = ['Review', 'Approved'];
                const notFound =
                    acceptableStatusList.indexOf(this.reviewStatus) === -1;
                this.accessStatus = notFound;
                if (this.reviewStatus === 'Approved' || this.reviewStatus === 'Closed') {
                    this.accessStatus = true;
                }
            }
            if (this.isIntakeFromSupervisor && this.isDjs) {
                this.accessStatus = false;
            }
        } else if (this.roleId.role.name === AppConstants.ROLES.INTAKE_WORKER
            || this.roleId.role.name === AppConstants.ROLES.CASE_WORKER) {
            this.accessStatus =
                (this.reviewStatus === 'Review' &&
                    this.reviewstatus.appevent === 'INTR') ||
                this.reviewStatus === 'Approved' ||
                this.reviewStatus === 'Closed';
        } else if (
            this.roleId.role.name === AppConstants.ROLES.OFFICE_PROFFESSIONAL
        ) {
            this.accessStatus =
                this.reviewStatus === 'Approved' ||
                this.reviewStatus === 'Closed';
        }
        // if (this.reviewStatus === 'Review') {
        //     this.accessStatus = false;
        // }
        if (!this._intakeConfig.getiseditIntake()) {
            this.accessStatus = false;
        }
    }

    validateIRAR(sdm) {
        if (sdm.scnRecommendOveride !== '') {
            if (sdm.scnRecommendOveride === 'Ovrscrnin') {
                sdm.isir = true;
                sdm.isar = false;
            }
            if (sdm.scnRecommendOveride === 'OvrScrnout') {
                sdm.isir = false;
                sdm.isar = true;
            }
        } else {
            if (sdm.cpsResponseType === 'CPS-AR') {
                sdm.isir = false;
                sdm.isar = true;
            }
        }
    }

    goToHome() {
        let url = '';

        if (this.isCLW) {
            url = '/pages/sao-dashboard';
        } else {
            url = '/pages/newintake/new-saveintake';
        }

        this._router.navigate([url]);
    }

    processSelectedYouth() {
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (addedPersons) {
            const personYouth = addedPersons.find(
                person => person.Role === 'Youth'
            );
            if (personYouth) {
                if (
                    !this.selectedYouth ||
                    personYouth.fullName !== this.selectedYouth.fullName
                ) {
                    this.selectedYouth = personYouth;
                    this._dataStoreService.setData(
                        IntakeStoreConstants.youthid,
                        personYouth.Pid
                    );
                }
                return personYouth.fullName;
            } else {
                this.selectedYouth = null;
            }
        }

        return null;
    }

    processFocusPerson() {
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (addedPersons) {
            let key = '';
            if (this.isAS) {
                key = 'RA';
            }
            const personFound = addedPersons.find(
                person => person.Role === key
            );
            if (personFound) {
                this.focusPerson = personFound;
                return personFound.fullName;
            } else {
                this.focusPerson = null;
            }
        }
        return null;
    }

    listAllegations(purposeID) {
        const checkInput = {
            where: { intakeservreqtypeid: purposeID },
            method: 'get',
            nolimit: true,
            order: 'name'
        };
        this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter'
            )
            .map(result => {
                return result;
            })
            .subscribe();
    }

    assignIntaker(modal, status) {
        const intakeWorkerId = this.store[
            IntakeStoreConstants.assingedIntakeWokerId
        ];
        if (intakeWorkerId) {
            this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        where: {
                            appeventcode: modal,
                            intakenumber: this.intakeNumber,
                            assigneduserid: intakeWorkerId,
                            status: status
                        },
                        method: 'post'
                    }),
                    'Intakedastagings/assignintake'
                )
                .subscribe(result => {
                    this._alertService.success('Intake assigned successfully!');
                    const dashboardURL = '/pages/cjams-dashboard';
                    setTimeout(() => {
                        this._router.navigate([dashboardURL]);
                    }, 2000);
                });
        } else {
            this._alertService.warn('Please select a intake worker');
        }
    }
    submitDocuments() {
        if (this.saoResponseForm.invalid) {
            this._alertService.warn('Please select a SAO response date');
            return false;
        }
        this.genratedDocumentList = this.store[
            IntakeStoreConstants.generatedDocuments
        ];
        if (!this.genratedDocumentList) {
            this.showDocumentGenerateErrorMessage();
            return false;
        }
        const notGeneratedRequiredDocs = this.genratedDocumentList.filter(
            document => document.isRequired && !document.isGenerated
        );
        if (notGeneratedRequiredDocs.length !== 0) {
            this.showDocumentGenerateErrorMessage();
            return false;
        }
        this.signedOffDate = this.saoResponseForm.getRawValue().signedOffDate;
        this.clwStatus = this.SAO_DOUCUMENT_GENERATED;
        (<any>$('#document-complete-modal')).modal('hide');
        this.draftIntake(
            this.departmentActionIntakeFormGroup.value,
            'CLWDRAFT', true
        );
    }

    private showDocumentGenerateErrorMessage() {
        this._alertService.error('Please generate required Documents');
        (<any>$('#document-complete-modal')).modal('hide');
    }

    private submitSAOResponse() {
        const saoResponseData: SAOResponse = this._dataStoreService.getData(
            IntakeStoreConstants.saoResponse
        );
        if (!saoResponseData) {
            this._alertService.error('Please fill the required sao responses');
            return false;
        }

        if (!saoResponseData.saoresponsedate) {
            this._alertService.error('Please select the sao response date');
            return false;
        }

        if (!saoResponseData.saoresponsestatustypekey) {
            this._alertService.error('Please select the sao response status');
            return false;
        }

        if (!saoResponseData.saoresponseconditiontypekey) {
            this._alertService.error('Please select the sao condition');
            return false;
        }
        if (saoResponseData.saoresponsestatustypekey === 'PF') {
            this.clwStatus = this.SAO_RESPONSED;
        } else {
            this.clwStatus = AppConstants.INTAKE_CONSTANTS.SAO_RESPONSE_CLOSED;
        }
        this.draftIntake(
            this.departmentActionIntakeFormGroup.value,
            'CLWDRAFT', true
        );
    }

    submitPetitionDetails() {
        this.isPetionDetailsSubmited = true;
        this.clwStatus = this.PETITIONS_SUBMITED;
        this.draftIntake(
            this.departmentActionIntakeFormGroup.value,
            'CLWDRAFT', true
        );
    }

    submitScheduledHearings() {
        this.clwStatus = this.HEARING_SCHEDULED;
        this.draftIntake(
            this.departmentActionIntakeFormGroup.value,
            'CLWDRAFT', true
        );
    }
    submitCompletedHearings() {
        this.clwStatus = this.HEARING_SCHEDULED;
        this.draftIntake(
            this.departmentActionIntakeFormGroup.value,
            'CLWDRAFT', true
        );
    }

    backToDashboard() {
        this._router.navigate(['/cjams-dashboard/assign-case']);
    }

    modifyinterStateCompactDetails() {
        let interstatecompactedetails = [
            this.store[IntakeStoreConstants.FocuspersonCaseDetails]
        ];
        if (
            !interstatecompactedetails ||
            !Array.isArray(interstatecompactedetails)
        ) {
            interstatecompactedetails = [];
        } else {
            if (
                interstatecompactedetails &&
                interstatecompactedetails.length > 0
            ) {
                if (interstatecompactedetails[0]) {
                    interstatecompactedetails[0].intakenumber = this.intakeNumber;
                    if (
                        this._intakeConfig.selectedPurposeIs(
                            MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT
                        )
                    ) {
                        if (
                            interstatecompactedetails[0].warranttype &&
                            interstatecompactedetails[0].warranttype ===
                            'Delinquent'
                        ) {
                            interstatecompactedetails[0].isdetaintheyouth = true;
                        }
                        if (
                            interstatecompactedetails[0].warranttype &&
                            interstatecompactedetails[0].warranttypedetails &&
                            interstatecompactedetails[0].warranttypedetails
                                .length > 0
                        ) {
                            const warranttypedetails =
                                interstatecompactedetails[0].warranttypedetails;
                            if (
                                warranttypedetails &&
                                warranttypedetails.length > 0
                            ) {
                                const modified = warranttypedetails.map(key => {
                                    if (
                                        typeof key === 'string' ||
                                        key instanceof String
                                    ) {
                                        return {
                                            warranttypekey: key,
                                            warranttype:
                                                interstatecompactedetails[0]
                                                    .warranttype
                                        };
                                    } else {
                                        return {
                                            warranttypekey: key.warranttypekey,
                                            warranttype:
                                                interstatecompactedetails[0]
                                                    .warranttype
                                        };
                                    }
                                });
                                interstatecompactedetails[0].warranttypedetails = modified;
                            }
                        }
                    }
                    if (
                        this._intakeConfig.selectedPurposeIs(
                            MyNewintakeConstants.REFERRAL.INTERSTATE_COMPACT
                        )
                    ) {
                        const persondetails =
                            interstatecompactedetails[0].residingwithdetails;
                        if (persondetails && persondetails.length > 0) {
                            interstatecompactedetails[0].residingwithdetails = persondetails.map(
                                key => {
                                    if (
                                        typeof key === 'string' ||
                                        key instanceof String
                                    ) {
                                        const person = this.getAddedPersonNames(
                                            key
                                        );
                                        return {
                                            personid: key,
                                            firstName: person.Firstname,
                                            middlename: person.Middlename,
                                            lastName: person.Lastname
                                        };
                                    } else {
                                        const person = this.getAddedPersonNames(
                                            key.personid
                                        );
                                        return {
                                            personid: key.personid,
                                            firstName: person.Firstname,
                                            middlename: person.Middlename,
                                            lastName: person.Lastname
                                        };
                                    }
                                }
                            );
                        }
                    }
                }
            }
        }
        const modifiedinterstatecompactedetails = JSON.parse(
            JSON.stringify(interstatecompactedetails)
        );
        if (
            !modifiedinterstatecompactedetails ||
            modifiedinterstatecompactedetails.length <= 0
        ) {
            return [];
        }
        return modifiedinterstatecompactedetails;
    }

    getAddedPersonNames(key) {
        const personsList = this.store[IntakeStoreConstants.addedPersons];
        const person = personsList.find((item, index) => {
            const personid = item.Pid
                ? item.Pid
                : AppConstants.PERSON.TEMP_ID + index;
            if (personid === key) {
                return item;
            }
        });
        return person;
    }

    modifyEvalFields() {
        let evalFields = this.store[IntakeStoreConstants.evalFields];
        if (!evalFields || !Array.isArray(evalFields)) {
            evalFields = [];
        }
        const modifiedEvalFields = JSON.parse(JSON.stringify(evalFields));
        if (!modifiedEvalFields || modifiedEvalFields.length <= 0) {
            return [];
        }
        modifiedEvalFields.forEach(evalField => {
            if (evalField && evalField.offenselocation) {
                this.zipCode = '' + evalField.offenselocation;
            }
            const allegations = evalField.allegedoffense;
            if (allegations) {
                evalField.allegedoffense = allegations.map(allegation => {
                    if (allegation.allegationid) {
                        return {
                            allegationid: allegation.allegationid,
                            allegationtype:
                                MyNewintakeConstants.Intake.allegedOffenseType
                        };
                    } else {
                        return {
                            allegationid: allegation,
                            allegationtype:
                                MyNewintakeConstants.Intake.allegedOffenseType
                        };
                    }
                });
            }
            if (evalField.waiverpetitionid && evalField.waivedoffense) {
                evalField.waivedoffense = allegations.map(waivedoffense => {
                    if (waivedoffense.allegationid) {
                        return {
                            allegationid: waivedoffense.allegationid,
                            allegationtype:
                                MyNewintakeConstants.Intake.waivedOffenseType
                        };
                    } else {
                        return {
                            allegationid: waivedoffense,
                            allegationtype:
                                MyNewintakeConstants.Intake.waivedOffenseType
                        };
                    }
                });
            } else {
                evalField.waivedoffense = [];
            }
            evalField.yearsofage = evalField.yearsofage
                ? evalField.yearsofage
                : null;
            evalField.arrestdate = evalField.arrestdate
                ? evalField.arrestdate
                : null;
            evalField.complaintreceiveddate = evalField.complaintreceiveddate
                ? evalField.complaintreceiveddate
                : null;
            evalField.begindate = evalField.begindate
                ? evalField.begindate
                : null;
            evalField.enddate = evalField.enddate ? evalField.enddate : null;
            evalField.zipcode = evalField.zipcode ? evalField.zipcode : null;
            switch (evalField.allegedoffenseknown) {
                case 0: {
                    evalField.enddate = null;
                    break;
                }
                case 1: {
                    evalField.enddate = null;
                    evalField.begindate = null;
                    break;
                }
                case 2: {
                    break;
                }
            }
        });
        return modifiedEvalFields;
    }

    formatAllegations(): Array<any> {
        const allegations = [];
        const createdCases = this.store[IntakeStoreConstants.createdCases];
        createdCases.forEach(createdCase => {
            createdCase.choosenAllegation.forEach(allegation => {
                allegations.push({
                    DaNumber: createdCase.caseID,
                    AllegationId: allegation.allegationID,
                    AllegationName: allegation.allegationValue,
                    Indicators: allegation.indicators
                });
            });
        });

        return allegations;
    }

    submitDelayForm() {
        if (this.dispositionDelayForm.dirty) {
            const delayReason = {
                fiveDays: this.dispositionDelayForm.get('fiveDaysDelay').value,
                twentyFiveDays: this.dispositionDelayForm.get(
                    'twentyFivedaysDelay'
                ).value
            };
            this.delayFormData = delayReason;
            this.checkConditionForDelay = true;
            (<any>$('#reason-for-delay')).modal('hide');
            this.submitIntake(this.departmentActionIntakeFormGroup.value, 'INTR');
        } else {
            this.checkConditionForDelay = false;
            this._alertService.warn('Please enter a valid reason.');
        }
    }
    private getCLWTabOrder() {
        let tabOrder = ['intake-refferal', 'document'];
        switch (this.clwStatus) {
            case AppConstants.INTAKE_CONSTANTS.SAO_DOUCUMENT_GENERATED:
            case AppConstants.INTAKE_CONSTANTS.SAO_RESPONSE_CLOSED:
                tabOrder = ['intake-refferal', 'document', 'sao-response'];
                break;
            case AppConstants.INTAKE_CONSTANTS.SAO_RESPONSED:
                tabOrder = [
                    'intake-refferal',
                    'document',
                    'sao-response',
                    'sao-petition'
                ];
                break;
            case AppConstants.INTAKE_CONSTANTS.PETTIION_SUBMITED:
                tabOrder = [
                    'intake-refferal',
                    'document',
                    'sao-response',
                    'sao-petition',
                    'sao-schedule'
                ];
                break;
            case AppConstants.INTAKE_CONSTANTS.HEARING_SCHEDULED:
            case AppConstants.INTAKE_CONSTANTS.COURT_HEARING_CONTINUANCE:
            case AppConstants.INTAKE_CONSTANTS.COURT_ACTION_SUSTAINED:
            case AppConstants.INTAKE_CONSTANTS.SAO_CLOSED:
            case AppConstants.INTAKE_CONSTANTS.RESTITUTION_COMPLETED:
                tabOrder = [
                    'intake-refferal',
                    'document',
                    'sao-response',
                    'sao-petition',
                    'sao-schedule',
                    'sao-hearing'
                ];
                break;
            default:
                tabOrder = ['intake-refferal', 'document'];
                break;
        }
        return tabOrder;
    }
    private initializeDJSTabs() {
        const user = this._authService.getCurrentUser();

        let tabOrder = this._intakeConfig.getDJSTabOrder();

        if (user.role.name === AppConstants.ROLES.COURT_WORKER) {
            tabOrder = this.getCLWTabOrder();
        }
        this.agencyTabOrder = tabOrder
            .map(tabId => {
                return IntakeTabConfig.find(
                    item =>
                        item.id === tabId &&
                        item.role.indexOf(user.role.name) !== -1
                );
            })
            .filter(tab => tab);

        const purpose = this._intakeConfig.getIntakePurpose();
        console.log(purpose);
        if (
            purpose &&
            purpose.description ===
            MyNewintakeConstants.REFERRAL.ADULT_HOLD_DETENTION &&
            user.role.name === AppConstants.ROLES.OFFICE_PROFFESSIONAL
        ) {
            this.agencyTabOrder.splice(
                2,
                0,
                IntakeTabConfig.find(item => item.id === 'assessment')
            ); // For enabling assessment tab for Office Professional in Adult Hold.
        }

        if (
            this._router.url.endsWith('my-newintake') &&
            user.role.name === AppConstants.ROLES.COURT_WORKER
        ) {
            const indexToLoad = this.agencyTabOrder.length - 1;
            this._router.navigate([this.agencyTabOrder[indexToLoad].route], {
                relativeTo: this.route
            });
        } else if (
            this._router.url.endsWith('my-newintake') ||
            this.isPurposeChanged
        ) {
            this.isPurposeChanged = false;
            this._router.navigate([this.agencyTabOrder[0].route], {
                relativeTo: this.route
            });
        }
        this.accessRights();
    }
    private initializeTabs() {
        const agency = this._authService.getAgencyName();
        const user = this._authService.getCurrentUser();
        let agencyTabOrder;
        if (agency === 'CW') {
            agencyTabOrder = this._intakeConfig.getCWTabOrder();
        } else {
            agencyTabOrder = IntakeTabOrderConfig.find(
                item => item.agency === agency
            ).order;
        }
        // const agencyTabOrder = IntakeTabOrderConfig.find(item => item.agency === agency);
        const tabOrder = agencyTabOrder;

        this.agencyTabOrder = tabOrder
            .map(tabId => {
                return IntakeTabConfig.find(
                    item =>
                        item.id === tabId &&
                        item.role.indexOf(user.role.name) !== -1
                );
            })
            .filter(tab => tab);
        const isCPSHistoryClearance = this.intakeservice.find(
            service => service.description === 'CPS History Clearance'
        );
        if (isCPSHistoryClearance) {
            this.agencyTabOrder.splice(
                2,
                0,
                IntakeTabConfig.find(item => item.id === 'history-clearance')
            );
        }
        if (
            this._router.url.endsWith('my-newintake') ||
            this.isPurposeChanged
        ) {
            this.isPurposeChanged = false;
            const toPerson = this._dataStoreService.getData(IntakeStoreConstants.NAVIGATE_TO_PERSON);
            if (toPerson) {
                const tab = this.agencyTabOrder.find(item => item.id === 'person-cw');
                this._dataStoreService.setData(IntakeStoreConstants.NAVIGATE_TO_PERSON, false);
                this._router.navigate([(tab && tab.route) ? tab.route : ''], {
                    relativeTo: this.route
                });
            } else {
                this._router.navigate([this.agencyTabOrder[0].route], {
                    relativeTo: this.route
                });
            }
        }
        this.accessRights();
    }
    private afterDataStoreSet() {
        let sdm = this.store[IntakeStoreConstants.intakeSDM];
        if (sdm) {
            sdm = Object.assign(sdm, sdm.physicalAbuse);
            sdm = Object.assign(sdm, sdm.sexualAbuse);
            sdm = Object.assign(sdm, sdm.generalNeglect);
            sdm = Object.assign(sdm, sdm.arGeneralNeglect);
            sdm = Object.assign(sdm, sdm.unattendedChild);
            sdm = Object.assign(sdm, sdm.riskofHarm);
            sdm = Object.assign(sdm, sdm.screenOut);
            sdm = Object.assign(sdm, sdm.screenIn);
            sdm = Object.assign(sdm, sdm.immediateList);
            sdm = Object.assign(sdm, sdm.noImmediateList);
            sdm = Object.assign(sdm, sdm.disqualifyingCriteria);
            sdm = Object.assign(sdm, sdm.disqualifyingFactors);

            if (sdm.cpsResponseType) {
                if (sdm.cpsResponseType === 'CPS-IR') {
                    sdm.isir = true;
                    sdm.isar = false;
                } else {
                    this.validateIRAR(sdm);
                }
            } else {
                this.validateIRAR(sdm);
            }

            if (sdm.maltreatment === 'yes') {
                sdm.ismaltreatment = true;
            } else {
                sdm.ismaltreatment = false;
            }
            if (this.store[IntakeStoreConstants.childfatality] === true
                || this.store[IntakeStoreConstants.childfatality] === 'yes') {
                sdm.ischildfatality = true;
            } else {
                sdm.ischildfatality = false;
            }

            if (sdm.screeningRecommend === 'ScreenOUT') {
                sdm.isrecsc_screenout = true;
                sdm.isrecsc_scrrenin = false;
            } else {
                sdm.isrecsc_screenout = false;
                sdm.isrecsc_scrrenin = true;
            }

            this.addSdm = sdm;
            if (this.addSdm.changePathway) {
                this.pathwayChange = true;
            }
            const createdCases = this.store[IntakeStoreConstants.createdCases];
            if (this.roleId.role.name !== AppConstants.ROLES.SUPERVISOR) {
                if (
                    sdm.screeningRecommend &&
                    createdCases &&
                    createdCases.length
                ) {
                    createdCases[0].dispositioncode = sdm.screeningRecommend;
                    createdCases[0].intakeserreqstatustypekey = 'Review';
                    if (sdm.screeningRecommend !== this.sdmDispositionCall) {
                        this.sdmDispositionCall = sdm.screeningRecommend;
                        this._dataStoreService.setData(
                            IntakeStoreConstants.createdCases,
                            createdCases
                        );
                    }
                }
            } else {
                if (
                    sdm.scnRecommendOveride &&
                    createdCases &&
                    createdCases.length
                ) {
                    createdCases[0].intakeserreqstatustypekey = 'Review';
                    createdCases[0].dispositioncode = sdm.screeningRecommend;
                    createdCases[0].supDisposition = sdm.scnRecommendOveride;
                    createdCases[0].supStatus = 'Approved';
                    if (sdm.scnRecommendOveride !== this.sdmDispositionCall) {
                        this.sdmDispositionCall = sdm.scnRecommendOveride;
                        this._dataStoreService.setData(
                            IntakeStoreConstants.createdCases,
                            createdCases
                        );
                    }
                }
            }
        }
        // this.recordings = this.store[IntakeStoreConstants.communications];
        this.genratedDocumentList = this.store[
            IntakeStoreConstants.generatedDocumentDownloadKey
        ];
    }
    changeLocalDept(model) {
        if (model && model === '0') {
            this.departmentActionIntakeFormGroup.controls[
                'nonreferalreason'
            ].setValidators([Validators.required]);
            this.departmentActionIntakeFormGroup.controls[
                'nonreferalreason'
            ].updateValueAndValidity();
        } else {
            this.departmentActionIntakeFormGroup.controls[
                'nonreferalreason'
            ].clearValidators();
            this.departmentActionIntakeFormGroup.controls[
                'nonreferalreason'
            ].updateValueAndValidity();
        }
    }

    ngOnDestroy(): void {
        this._speechRecognitionService.destroySpeechObject();
        // this._sessionStorage.removeItem('intake');
        console.log(this.reviewStatus);
       this.autoSaveAsDraft();
        this.dataStoreSubscription.unsubscribe();
        // this._intakeConfig.intakeDecision$.unsubscribe();
        // this._intakeConfig.isVoluntaryPlacement$.unsubscribe();
        if (this.autosaveTimmer) {
            this.autosaveTimmer.unsubscribe();
        }
    }
    private validateChildAge(involvedPerson: InvolvedPerson[]): boolean {
        let isValidAge = true;

        const getPerson = involvedPerson.filter(
            person =>
                person.Role !== 'CHILD' &&
                person.Role !== 'RC' &&
                this.getAge(person.Dob) > 18
        );
        if (getPerson.length) {
            isValidAge = true;
        } else {
            isValidAge = false;
        }
        // involvedPerson.map(role => {
        //     if (role.Role !== 'CHILD' && role.Role !== 'RC') {
        //         const personAge = this.getAge(role.Dob);
        //         if (personAge > 18) {
        //             return isValidAge = true;
        //         }
        //     } else {
        //         isValidAge = false;
        //     }
        // });
        return isValidAge;
    }

    private getAge(dateValue) {
        if (
            dateValue &&
            moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()
        ) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }
    getPrior(intakenumber) {
        this.priorItem$ = this._commonHttpService
            .getArrayList(
                {
                    where: { intakenumber: intakenumber },
                    method: 'get'
                },
                NewUrlConfig.EndPoint.Intake.GetPrior + '?filter'
            )
            .map(res => {
                if (res.length) {
                    return res
                        .filter(item => item.role === 'Reported Child')
                        .map(service => {
                            service.servicecases = service.servicecases
                                ? service.servicecases.filter(
                                    servicecase =>
                                        servicecase.subservice ===
                                        'Family Preservation Services'
                                )
                                : [];
                            service.cpsfindings = service.cpsfindings
                                ? service.cpsfindings.filter(
                                    servicecase =>
                                        servicecase.subservice ===
                                        'Family Preservation Services'
                                )
                                : [];
                            return service;
                        });
                }
            });
    }
    routToCase(daNumber: string, intakeserviceid) {
        (<any>$('#priorDetails')).modal('hide');
        this._commonHttpService
            .getById(
                daNumber,
                NewUrlConfig.EndPoint.Intake.DsdsActionSummaryUrl
            )
            .subscribe(response => {
                const dsdsActionsSummary = response[0];
                this._dataStoreService.setData(
                    'da_status',
                    dsdsActionsSummary.da_status
                );
                this._dataStoreService.setData(
                    'teamtypekey',
                    dsdsActionsSummary.teamtypekey
                );
                const currentUrl =
                    '/pages/case-worker/' +
                    intakeserviceid +
                    '/' +
                    daNumber +
                    '/dsds-action/report-summary';
                this._router.navigate([currentUrl]);
            });
    }
    createNewCase(modal: General, appevent: string) {
        (<any>$('#priorDetails')).modal('hide');
        this.approveIntake(modal, appevent);
    }

    onRefferalDropDownChange(purpose) {
        this.currentPurposeValue = purpose.value;
        console.log('purpose change triggered', purpose);
        this.isPurposeChanged = true;
        this._dataStoreService.setData(IntakeStoreConstants.purposeSelected, {
            text: purpose.label,
            value: purpose.value
        });
        this.initializeDJSTabs();
    }

    purposeChange(newPurpose: any, purposeSource: string) {
        this.newPurpose = newPurpose;
        this.purposeSource = purposeSource;
        if (this.currentPurposeValue) {
            this.currentPurpose = this._dataStoreService.getData(
                IntakeStoreConstants.purposeSelected
            );
            $('#purpose-change').modal('show');
        } else {
            this.changePurpose();
        }
    }

    changePurpose() {
        this._dataStoreService.setData(IntakeStoreConstants.intakeSDM, null);
        this._dataStoreService.setData(IntakeStoreConstants.evalFields, null);
        this._dataStoreService.setData(IntakeStoreConstants.createdCases, null);
        this._dataStoreService.setData(IntakeStoreConstants.disposition, null);
        this._dataStoreService.setData(IntakeStoreConstants.intakeService, null);
        this.intakeservice = [];
        if (this.purposeSource === 'referral') {
            this.onRefferalDropDownChange(this.newPurpose);
        } else {
            if (this.isCW) {
                const purpose = this.newPurpose.value.split('~')[0];
                const selectedPurpose = this.getSelectedPurpose(purpose);
                this.showSubmit = [
                    'Information and Referral'
                    // 'ROACPS'
                ].includes(selectedPurpose.intakeservreqtypekey);
                if(this.isCW && this.isSupervisor  && this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.ROA_CPS)) {
                    this.showSubmit = true;
                }
            }
            this.listService(this.newPurpose);
        }
        $('#purpose-change').modal('hide');
    }

    resetToPreviosPurpose() {
        this.newPurpose = null;
        this.departmentActionIntakeFormGroup.patchValue(
            {
                Purpose: this.currentPurposeValue
            },
            { emitEvent: false }
        );
        $('#purpose-change').modal('hide');
    }

    showApproveIntakeAckmnt(approveIntakeResponse: any) {
        this.approveIntakeResponse = approveIntakeResponse;
        const djsmessage = this._intakeConfig.djsInfoValidation();
        if (this.isDjs && djsmessage.status) {
            (<any>$('#close-intake-ackmt')).modal('show');
            return true;
        }
        const roaCPS = this.store[IntakeStoreConstants.roacps];
        // const isRoaCPS = this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.ROA_CPS);
        if (this.closeCWCae || ( roaCPS && roaCPS.statetype === 'instate')) {
            this._alertService.success('Completed successfully.',true);
            const url = `/pages/newintake/new-saveintake`;
            this._router.navigate([url]);
        } else {
            if (
                approveIntakeResponse &&
                Array.isArray(approveIntakeResponse) &&
                approveIntakeResponse.length
            ) {
                const caseData = approveIntakeResponse.find(
                    caseData =>
                        (caseData.hasOwnProperty('isfamilycase') &&
                            caseData.isfamilycase === 1) || this.isSENflag || ( roaCPS && roaCPS.statetype !== 'instate')
                    // (caseData.hasOwnProperty('drugexposednewbornflag') && //@TM: Create service case for SEN
                    // caseData.drugexposednewbornflag === 1)
                );
                if (caseData) {
                    this.validateFamilyCaseAvailable(caseData);
                    // exit the flow;
                    return;
                } else {
                    this.getRoutingUser();
                    this.teamtypekey = this._authService.getCurrentUser().user.userprofile.teamtypekey;
                    this.teamid = this.roleId.user.userprofile.teammemberassignment.teammember.teamid;
                    this.selectedteamid = this.teamid;
                    this.teamForm.controls['teamid'].setValue(this.selectedteamid);
                    this.getTeamList();
                    (<any>$('#approve-intake-ackmt')).modal('show');
                }
            }
        }
    }

    approveIntakeAcknowledged() {
        (<any>$('#approve-intake-ackmt')).modal('hide');
        if (!this.isDjs) {
            this._intakeService.loadCaseAssignDashboard();
        } else {
            // for DJS, there is  flow will go CLW
            this.onReload();
        }
    }

    selectCommunication($event) {
        console.log($event);
        this._dataStoreService.setData(
            IntakeStoreConstants.REFERALSOURCE,
            $event
        );
    }

    activateSpeechToText(): void {
        this.recognizing = true;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
            this._speechRecognitionService.record().subscribe(
                // listener
                value => {
                    this.speechData = value;
                    const comments = this.draftReasonFormGroup.getRawValue()
                        .draftReason;
                    this.draftReasonFormGroup.patchValue({
                        draftReason: comments + ' ' + this.speechData
                    });
                },
                // errror
                err => {
                    console.log(err);
                    this.recognizing = false;
                    if (err.error === 'no-speech') {
                        this.notification = `No speech has been detected. Please try again.`;
                        this._alertService.warn(this.notification);
                        this.activateSpeechToText();
                    } else if (err.error === 'not-allowed') {
                        this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    } else if (err.error === 'not-microphone') {
                        this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    }
                },
                // completion
                () => {
                    this.speechRecogninitionOn = true;
                    console.log('--complete--');
                    this.activateSpeechToText();
                }
            );
        } else {
            this.recognizing = false;
            this.deActivateSpeechRecognition();
        }
    }
    deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
    }

    submitDraftReason() {
        // const comments = this.draftReasonFormGroup.getRawValue().draftReason;
        const currTime = new Date().getTime();
        this.saveasdraftReason = this.saveasdraftReason
            ? this.saveasdraftReason
            : [];
        const draftReasonObj = { comments: undefined, time: currTime };
        this.saveasdraftReason.push(draftReasonObj);
        //  this.draftReasonFormGroup.reset();
        this.draftIntake(this.departmentActionIntakeFormGroup.value, 'DRAFT', true);
        // Commenting as per the HOTFIX start
        // (<any>$('#save-as-draft-reason')).modal('hide');

        // Commenting as per the HOTFIX End

        // this.pageSubject$.subscribe(pageNumber => {
        //     this.paginationInfo.pageNumber = pageNumber;
        //     this.getPage(this.paginationInfo.pageNumber);
        //   });
        //   this.getPage(1);
    }

    // getPage(page: number) {
    //   this.paginationInfo.pageNumber = page;
    //   const source = this.saveasdraftReason.filter((person) => person.id > 5);
    //   }

    // pageChanged(event: any) {
    //     this.paginationInfo.pageNumber = event.page;
    //     this.paginationInfo.pageSize = event.itemsPerPage;
    //     this.pageSubject$.next(this.paginationInfo.pageNumber);
    //   }

    closeIntakeError() {
        (<any>$('#intake-error')).modal('hide');
        this.intakeErrorMessage = null;
    }

    selectCase(caseId) {
        this.selectedServiceCaseId = caseId;
    }

    createOrMergeServiceCase() {
        let isNewCase;
        if (!this.selectedServiceCaseId) {
            this._alertService.error('Please select an option to proceed');
            return;
        }
        if (this.selectedServiceCaseId === 'NEW_CASE') {
            this.selectedServiceCaseId = null;
            isNewCase = 1;
        } else {
            isNewCase = 0;
        }
        const serviceCaseData = {
            servicecaseid: this.selectedServiceCaseId,
            intakeserviceid: this.familyCaseData.responseintakeserviceid,
            isnewcase: isNewCase,
            subtypekey: 'IHM', // INHOME SERVICES,
            source: 'intake'
        };
        this._commonHttpService
            .create(serviceCaseData, 'servicecase/createservicecase')
            .subscribe(response => {
                console.log('service case create', response);
                this.serviceCaseResponse = response;
                this.serviceCaseNumber = response[0].servicecaseno;
                this.hideHistoryOfFmailyCase();
                if (isNewCase) {
                    this.openServiceCaseAcknowledge(response[0].servicecaseno);
                } else {
                    this.openMergeServiceCaseAcknowledge(response[0].servicecaseno);
                }


            });

        console.log(
            'serviceCaseid',
            this.selectedServiceCaseId,
            'isNewCase',
            isNewCase
        );
    }
    
    ngAfterViewChecked(){
        (<any>$('#NONCPS')).prop('disabled', true);
    }

    validateFamilyCaseAvailable(caseData) {
        this.familyCaseData = caseData;
        this._commonHttpService
            .getSingle(
                {
                    where: {
                        intakeserviceid: caseData.responseintakeserviceid,
                        subtypekey: 'IHM',
                        source: 'intake'
                    },
                    method: 'get'
                },
                'intakeservreqchildremoval/servicecasevalidation?filter'
            )
            .subscribe(scResponse => {
                console.log('service case validate Response', scResponse);
                if (scResponse && scResponse.data && scResponse.data.length) {
                    if (scResponse.isavailable === 1) {
                        this.openHistoryOfFamilyCase(scResponse.data);
                    } else {
                        this.serviceCaseResponse = scResponse.data;
                        this.openServiceCaseAcknowledge(
                            scResponse.data[0].servicecaseno
                        );
                        console.log(
                            'Child serviceCaseNumber',
                            this.serviceCaseNumber
                        );
                    }
                }
            });
    }

    openServiceCaseAcknowledge(serviceCaseNumber) {
        this.serviceCaseNumber = serviceCaseNumber;
        this.getRoutingUser();
        this.teamtypekey = this._authService.getCurrentUser().user.userprofile.teamtypekey;
        this.teamid = this.roleId.user.userprofile.teammemberassignment.teammember.teamid;
        this.selectedteamid = this.teamid;
        this.teamForm.controls['teamid'].setValue(this.selectedteamid);
        this.getTeamList();
        this.loadProgramAreaDropdowns('IHM');
        if(this.serviceCaseResponse && this.serviceCaseResponse[0]){
            this.assignServiceCaseForm.patchValue({
                programkey: this.serviceCaseResponse[0].progrmkey,
                subprogramkey: this.serviceCaseResponse[0].subprogrmkey
            });
        }
        (<any>$('#approve-intake-ackmt-service-case')).modal('show');
    }

    openMergeServiceCaseAcknowledge(serviceCaseNumber) {
        this.serviceCaseNumber = serviceCaseNumber;
        (<any>$('#approve-intake-ackmt-merge-service-case')).modal('show');
    }

    openHistoryOfFamilyCase(caseList) {
        this.exitingServiceCaseList = caseList;
        (<any>$('#serviceCaseHistory')).modal('show');
    }

    hideHistoryOfFmailyCase() {
        (<any>$('#serviceCaseHistory')).modal('hide');
    }

    approveIntakeAcknowledgedWithServiceCase() {
        (<any>$('#approve-intake-ackmt-service-case')).modal('hide');
        (<any>$('#approve-intake-ackmt-merge-service-case')).modal('hide');
        this._router.navigate([
            '/pages/cjams-dashboard/cw-assign-service-case'
        ]);
    }
    openServiceCase(serviceCase) {
        this._sessionStorage.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + serviceCase.intakeserviceid + '/casetype';
        this._commonHttpService.getAll(url).subscribe((response) => {
          const dsdsActionsSummary = response[0];
          if (dsdsActionsSummary) {
            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            // common person for cw
            const currentUrl = '#/pages/case-worker/' + serviceCase.servicecaseid + '/' + serviceCase.servicecasenumber + '/dsds-action/person-cw';
            window.open(currentUrl);
          }
        });
    }



    loadServiceTypeDropDown() {
        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 155, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            )
            .subscribe(data => {
                this.serviceTypeRequested = data;
            });
    }

    loadSuggestTypeDropDown() {
        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 156, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            )
            .subscribe(data => {
                this.suggestedTypeResource = data;
            });
    }
    screenOutAcknowledged() {
        (<any>$('#screenout-intake-ackmt')).modal('hide');
        this._alertService.success('Intake Submitted Successfully!', true);
        this._router.navigate(['/pages/cjams-dashboard/cw-intake-referals']);
        // this.onReload();
    }
    // redirectToServiceCase(item) {
    //     this.hideHistoryOfFmailyCase();
    //     const redirectUrl = '/pages/case-worker/' + item.servicecaseid + '/' + item.servicecasenumber + '/dsds-action/report-summary';
    //     this._router.navigate([redirectUrl]);
    // }

    onTabClick(tab) {
        console.log('change tab', tab);
        if (this.isCW) {

            // CHECK to see if intake communication and purpose fields are filled in. If not disable tab clicks
            ControlUtils.validateAllFormFields(this.departmentActionIntakeFormGroup);

            if (this.departmentActionIntakeFormGroup
                .get('InputSource')
                .hasError('required')) {
                this._alertService.error('Please update required fields before switching to another tab.');
            } else if (tab.id === 'person-cw') {
                // @Simar: Multirole issue-This is not necessary since now Case Worker can swith context to take Intakes
                // && this._authService.selectedRoleIs(AppConstants.ROLES.INTAKE_WORKER)

                // this.draftIntake(this.departmentActionIntakeFormGroup.value, 'DRAFT', false);
                this.autoSaveAsDraft();

                // this route should happen only on success. draftintake should return and observable.
                this._router.navigate([tab.route], { relativeTo: this.route });
                this.current_route = tab.route;
                console.log('Current route==============>', tab.route);
            } else {
                // if form is valid move to next tab
                this._router.navigate([tab.route], { relativeTo: this.route });
                this.current_route = tab.route;
                console.log('Current route==============>', tab.route);
            }
        } else {
            // if not a CW, no route restriction
            this._router.navigate([tab.route], { relativeTo: this.route });
        }
    }

    listenForQuickAddPerson() {
        this._intakeConfig.quickAddPersonListener$.subscribe(data => {
            this.autoSaveAsDraft();
        });
    }

    getIsRestrictItem() {
        this._intakeService.isRestrictedItem(this.intakeNumber)
            .subscribe(
                (response) => {
                    if (response.length > 0) {
                        this.isrestricteditem = true;
                    }
                }
            );
    }

    restrictedItemAuditLog() {
        this._intakeService.restrictedItemAuditLog(this.intakeNumber)
            .subscribe(
                (response) => {

                }
            );
    }

    confirmRestrictItem() {
        const userinfo = this._authService.getCurrentUser();
        let activeflag = 0;
        if (!this.isrestricteditem) {
            activeflag = 1;
        }
        const selectedcaseworkerid = [];
        selectedcaseworkerid.push(userinfo.user.securityusersid);
        this._intakeService.createRestrictedItem(this.intakeNumber, 'INTAKE', selectedcaseworkerid, activeflag)
            .subscribe(
                (response) => {
                    // If originally false then we just successfully applied restriction
                    if (!this.isrestricteditem) {
                        this._alertService.success('Restriction applied successfully.');
                    } else {
                        this._alertService.success('Restriction removed successfully.');
                    }
                    // Revert the restriction flag
                    this.isrestricteditem = !this.isrestricteditem;
                    (<any>$('#confirm-restrict-item')).modal('hide');
                },
                (error) => {
                    this._alertService.warn('Error in updating restriction.');
                }
            );

    }

    cancelRestrictItem() {
        // this.returnFormGroup.reset();
        (<any>$('#confirm-restrict-item')).modal('hide');
    }

    getRoutingUser() {
        this.getResponsibilityType();
        let appEvent = 'INVR';
        if (this.roleId.role.name === AppConstants.ROLES.KINSHIP_INTAKE_WORKER || this.roleId.role.name === AppConstants.ROLES.KINSHIP_SUPERVISOR) {
            appEvent = 'KINR';
        }
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent, teamid: this.selectedteamid || null },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
    }
    getResponsibilityType() {
        this.responsibilityTypeDropdownItems$ = this._commonHttpService.getArrayList({}, 'responsibilitytype/').map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.typedescription,
                        value: res.responsibilitytypekey
                    })
            );
        });
    }

    listUser(assigned: string) {
        this.selectedPerson = '';
        this.getUsersList = [];
        this.mergeUsersList = [];
        this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === false) {
                    this.isSupervisor = false;
                    return res;
                }
            });
            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        } else {
            this.selectedResponsibilityType = null;
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === true) {
                    this.isSupervisor = true;
                    return res;
                }
            });

            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        }
    }

    selectResponsibilityType(typevalue) {
        this.selectedResponsibilityType = typevalue;
    }

    selectPerson(row) {
        this.selectedPerson = row;
    }

    assignUser() {
        if (this.selectedPerson) {
            if (this.selectedResponsibilityType && !this.requestSent) {
                this.requestSent = true;
                this.assignCaseToUser();
            }
            else if(this.requestSent){
                this._alertService.warn('Request already submitted');
            }  else {
                this._alertService.warn('Please select Responsibility');
            }
        } else {
            this._alertService.warn('Please select a person');
        }
    }
    assignCaseToUser() {
        if (this.approveIntakeResponse && this.approveIntakeResponse.length) {
            const caseid = this.approveIntakeResponse[0].responseintakeserviceid;
            this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        where: {
                            appeventcode: 'INVT',
                            serreqid: caseid,
                            assigneduserid: this.selectedPerson.userid,
                            isgroup: false,
                            responsibilitytypekey: this.selectedResponsibilityType
                        },
                        method: 'post'
                    }),
                    'Intakedastagings/routeda'
                )
                .subscribe((result) => {
                    this.requestSent = false;
                    this._alertService.success('Case assigned successfully!', true);
                    (<any>$('#approve-intake-ackmt')).modal('hide');
                    this.checkAndAssignRestrictedCase(caseid);
                    const url = `/pages/newintake/new-saveintake`;
                    this._router.navigate([url]);

                });
        }
    }

    checkAndAssignRestrictedCase(caseid) {
        let activeflag = 0;
        const selectedcaseworkerid = [];
        selectedcaseworkerid.push(this.selectedPerson.userid);
        this._intakeService.isRestrictedItem(caseid)
            .subscribe(
                (response) => {
                    if (response.length > 0) {
                        activeflag = 1;
                        // Means it's in restricted items list
                        this._intakeService.createRestrictedItem(caseid, 'SERVICE', selectedcaseworkerid, activeflag)
                            .subscribe(
                                (response) => {
                                    (<any>$('#restrict-item-assign-ack')).modal('show');
                                    this._alertService.success('Case is restricted for assigned case woker!');

                                });
                    }
                }
            );
    }

    getTeamList() {
        this._commonHttpService.getArrayList({
            method: 'get',
            page: 1,
            order: 'teamnumber asc',
            where: {
                activeflag: 1,
                teamtypekey: this.teamtypekey,
                teamid: null
            }
        }, 'manage/team/getteamlist?filter').subscribe((items) => {
            this.teamList = items.map(item => item);

        });
    }
    teamChange() {
        this.selectedteamid = this.teamForm.controls['teamid'].value;
        this.getRoutingUser();
    }
    assignServiceCaseUser() {
        if (this.workersList && this.workersList.length) {

            this.assignServiceCaseToUser();

        } else {
            this._alertService.warn('Please select a person');
        }
    }

    assignServiceCaseToUser() {
        const serviceCaseId = this.serviceCaseResponse[0].caseid;
        const model = {
            appeventcode: 'SRVC',
            servicecaseid: serviceCaseId,
            assigneduserid: this.selectedPerson.userid,
            programkey: (this.assignServiceCaseForm.get('programkey').value) ? this.assignServiceCaseForm.get('programkey').value : null,
            subprogramkey: (this.assignServiceCaseForm.get('subprogramkey').value) ? this.assignServiceCaseForm.get('subprogramkey').value : null,
            assignedusers: this.workersList
        };

        const data = [];
        this.workersList.forEach((item) => {
            if (!item.responsibilitytypekey) {
                data.push(item);
            }
        });
        if (!data.length) {
            this._commonHttpService
                .create(model,
                    'servicecase/assigncase'
                )
                .subscribe((result) => {
                    this._alertService.success('Service Case assigned successfully!', true);
                    (<any>$('#approve-intake-ackmt-service-case')).modal('hide');
                    const url = `/pages/newintake/new-saveintake`;
                    this._router.navigate([url]);
                });
        } else {
            this._alertService.error('Please Fill Responsibility');
        }
    }

    changeResponsibility(event, user) {
        console.log(event, user);
        const isWokerAvailable = this.workersList.find(worker => worker.userid === user.userid);
        if (isWokerAvailable) {
            this.workersList.forEach(worker => {
                if (worker.userid === user.userid) {
                    worker.responsibilitytypekey = event.value;
                }
            });
            this.workersList = this.workersList;
        } else {
            this._alertService.error('please select the worker');
            event.source.value = null;
        }
        console.log(this.workersList);

    }
    private loadProgramAreaDropdowns(servicerequesttypekey: string) {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { servicerequestsubtypekey: servicerequesttypekey },
                    method: 'get',
                    nolimit: true
                }),
                'agencyprogramarea/list?filter'
            )
            .subscribe((result) => {
                if (result && Array.isArray(result) && result.length) {
                    this.programArea = result[0].programarea;
                    this.programSubArea = result[0].subprogram;
                }
            });
    }

    selectPersonForAssign(checkBox, row) {
        // this.selectedPerson = row;
        if (checkBox.checked) {
            const userData = { userid: row.userid, username: row.username };
            this.workersList.push(userData);
        } else {
            this.workersList = this.workersList.filter(worker => worker.userid !== row.userid);
        }
        console.log(this.workersList);

    }

    calculateResponseOffset(sdmInfo) {
        if (sdmInfo) {
            /* SDM Counter info
             isnoimmed_physicalabuse-- Physical abuse-response within 24 hours
             isnoimmed_sexualabuse -- Sexual abuse-response within 24 hours
             isnoimmed_neglectresponse --- Neglect-response within 5 days
             isnoimmed_mentalinjury --- Mental injury-response within 5 days
             isnoimmed_screeninoverride --- Screen-in Override
             isnoimmed_risk_harm --There is Risk ofHarm. Response within 5 days.
             isnoimmed_substantial_risk -- There is Risk of Harm for a Substance Exposed Newborn. Response within 48 hours.
             isnoimmed_risk_harm -- There is Risk of Harm. Response within 5 days.
             */

            if (sdmInfo.isnoimmed_physicalabuse || sdmInfo.isnoimmed_sexualabuse) {
                return 24;
            }

            if (sdmInfo.isnoimmed_substantial_risk) {
                return 48;
            }

            if (sdmInfo.isnoimmed_neglectresponse || sdmInfo.isnoimmed_mentalinjury || sdmInfo.isnoimmed_risk_harm) {
                return 24 * 5;  // 5 days
            }
        }
        return 0;
    }

    processRecordingsList() {
        const neededRoles = ['AV', 'CHILD', 'LG'];
        if (this.commonInvolvedPersons && this.commonInvolvedPersons.data
            && Array.isArray(this.commonInvolvedPersons.data)) {
            const needToContactPersons = this.commonInvolvedPersons.data.filter(person => {
                if (person.roles.find(role => neededRoles.indexOf(role.intakeservicerequestpersontypekey) !== -1)) {
                    return true;
                } else {
                    return false;
                }
            }).map(filteredPerson => filteredPerson.roles[0].intakeservicerequestactorid);
            console.log('need to contacts', needToContactPersons);
            this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        page: this.paginationInfo.pageNumber,
                        limit: 100,
                        where: { intakeservicerequestactorids: needToContactPersons },
                        method: 'get'
                    }),
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetAllDaRecordingUrl + '/' + this.intakeNumber + '?data'
                )
                .subscribe((result) => {
                    if (result && Array.isArray(result.data)) {
                        const acceptedRecordings = result.data.filter(recording => {
                            const isAccepted = (recording.recordingtype === 'Face To Face' ||
                                recording.recordingtype === 'Initialfacetoface' ||
                                recording.recordingtype === 'Phone')
                                && (recording.attemptind !== null);
                            console.log('rt', (recording.recordingtype === 'Face To Face' ||
                                recording.recordingtype === 'Initialfacetoface' ||
                                recording.recordingtype === 'Phone'));

                            console.log('ate-com', recording.attemptind !== null);
                            console.log(recording.recordingtype, recording.attemptind, 'isAccepted', isAccepted);
                            return isAccepted;
                        }
                        );
                        const recordingInfo = needToContactPersons.map(requestactorid => {
                            let isCompleted = false;
                            for (const recodringInfo of acceptedRecordings) {
                                const found = recodringInfo.contactparticipant.filter(participant => participant.intakeservicerequestactorid === requestactorid);
                                if (found && found.length > 0) {
                                    isCompleted = true;
                                    break;
                                } else {
                                    isCompleted = false;
                                }
                            }
                            return { requestactorid: requestactorid, completed: isCompleted };
                        });

                        if (needToContactPersons.length && needToContactPersons.length === recordingInfo.filter(info => info.completed).length) {
                            this.cpsResponseOffset = 0;
                        } else {
                            const intakeModel = this._dataStoreService.getData(
                                IntakeStoreConstants.intakeModel
                            );
                            this.cpsResponseOffset = this.calculateResponseOffset(intakeModel.sdm);
                        }

                        console.log('recordingInfo', recordingInfo);

                        console.log('acceptedRecordings', acceptedRecordings);

                    }
                });
        }

    }

    autoSaveAsDraft() {
        console.log('sd', this.appevent, this.reviewStatus, this.reviewstatus);
        console.log ('is draft', (!this.reviewstatus.appevent || this.reviewstatus.appevent === 'DRAFT'));
        /* if (this.isCW
            && this._authService.selectedRoleIs(AppConstants.ROLES.INTAKE_WORKER)
            && this.departmentActionIntakeFormGroup.status !== 'INVALID'
            && !this.selectedSupervisor && this.reviewStatus !== 'Approved' && this.reviewStatus !== 'Review') {
            this.draftIntake(this.departmentActionIntakeFormGroup.value, 'DRAFT', false);
        } */
        if (this.isCW
            && this._authService.selectedRoleIs(AppConstants.ROLES.INTAKE_WORKER)
            && this.departmentActionIntakeFormGroup.status !== 'INVALID'
            && !this.selectedSupervisor
            && !this.closeCWCae
            && (!this.reviewstatus.appevent || this.reviewstatus.appevent === 'DRAFT' )) {
            this.draftIntake(this.departmentActionIntakeFormGroup.value, 'DRAFT', false);
        }
    }


}
