import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '../../../../../../node_modules/@angular/forms';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { NewUrlConfig } from '../../newintake-url.config';
import { IntakeConfigService } from '../intake-config.service';
import { IntakeStoreConstants } from '../my-newintake.constants';
import { InvolvedPerson, MaltreatorsName, ProviderName, Sdm, VictimName, Disqualifyingcriteria } from '../_entities/newintakeModel';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-sdm',
    templateUrl: './intake-sdm.component.html',
    styleUrls: ['./intake-sdm.component.scss']
})
export class IntakeSdmComponent implements OnInit {
    sdmFormGroup: FormGroup;
    isDisplayProvider = false;
    disableAbuseNeg = false;
    truePropertyPAFlag = false;
    truePropertyGNFlag = false;
    isSENFlag= false;
    isIRFlag: boolean;
    isChildInderOneYear: string;
    isScreenOutIN = 'No';
    isImmediate: boolean;
    isNoImmediate: boolean;
    scrninFlag: boolean;
    disableDQ: boolean = false;
    sdm = new Sdm();
    savedSDMs: Sdm[] = [];
    populateSdm: Sdm;
    reporMinDateValdiation: any;
    allegedVictim: VictimName[] = [];
    allegedMaltreator: MaltreatorsName[] = [];
    provider: ProviderName[] = [];
    // disablecheckboxFields = [];
    maxDate = new Date();
    searchprovider: any = {};
    providersList = [];
    selectedProvider: any = [];
    holdData: string;
    CPSFlag: string;
    patchIntakeModelSdm: Sdm;
    sdmCountyValuesDropdownItems$: Observable<DropdownModel[]>;
    roleId: AppUser;
    dayToOverride: Number;
    sdmSettings: {
        issexualabuse: boolean;
        isoutofhome: boolean;
        isdeathorserious: boolean;
        issignordiagonises: boolean;
        isPopulate: boolean;
        isDisableOverride: boolean;
        isSupervisor: boolean;
        isDisablescrnin: boolean;
        isDisablescrnout: boolean;
    };
    addSdm: Sdm;
    private submittedDate?: Date;
    private intakeNumber: string;
    pathwayChange = false;
    childFatality = 'no';
    private store: any;
    involvedPersons: InvolvedPerson[] = [];
    showCpsResponseType: boolean;
    isAcptNonCPS: boolean;
    cpsIRArray = [];
    childunderoneyearflag: boolean;
    childfatalityflag = false;
    constructor(
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute,
        private _store: DataStoreService,
        private _alertService: AlertService,
        private _dataStore: DataStoreService,
        private _intakeConfig: IntakeConfigService
    ) {
        this.sdmSettings = {
            issexualabuse: false,
            isoutofhome: false,
            isdeathorserious: false,
            issignordiagonises: false,
            isPopulate: false,
            isDisableOverride: false,
            isSupervisor: false,
            isDisablescrnin: false,
            isDisablescrnout: false,
        };
        this.store = this._store.getCurrentStore();
        this.intakeNumber = this.store[IntakeStoreConstants.intakenumber];
    }

    ngOnInit() {
        this.CPSFlag = this._dataStore.getData('CPSFlag');
        this.showCpsResponseType = true;
        this.setsearchprovider();
        this.buildFormGroup();
        this.roleId = this._authService.getCurrentUser();

        if (this.store[IntakeStoreConstants.IntakeAction] === 'add') {
            this.getInvolvedPerson('add');
        } else {
            this.getInvolvedPerson('');
        }

        this.setsearchprovider();

        this.sdmFormGroup.valueChanges.subscribe(() => {
            this.isAcptNonCPS = false;
            this.sdm = Object.assign({}, this.sdmFormGroup.value);

            // @TM: Screening Recommendation
            if (this.sdmFormGroup.get('screeningRecommend').value === 'ScreenOUT') {
                this.sdmFormGroup.patchValue({ screeningRecommend: 'ScreenOUT' }, { emitEvent: false });
                this.sdmSettings.isDisablescrnin = false;
                this.sdmSettings.isDisablescrnout = true;
                this.resetOverrides();
            } else if (this.sdmFormGroup.get('screeningRecommend').value === 'Scrnin') {
                this.sdmFormGroup.patchValue({ screeningRecommend: 'Scrnin' }, { emitEvent: false });
                this.sdmSettings.isDisablescrnin = true;
                this.sdmSettings.isDisablescrnout = false;
            } else if (this.sdmFormGroup.get('screeningRecommend').value === 'accept_as_noncps') {
                // this.isAcptNonCPS = true;
                this.sdmFormGroup.patchValue({ screeningRecommend: 'accept_as_noncps' }, { emitEvent: false });
                this.sdmSettings.isDisablescrnin = false;
                this.sdmSettings.isDisablescrnout = false;
            }

            this.sdmFormGroup.patchValue({ isfinalscreenin: 'true' }, { emitEvent: false });
            if (this.sdmFormGroup.get('scnRecommendOveride').value === 'OvrScrnout' ||
                this.sdmFormGroup.get('screeningRecommend').value === 'ScreenOUT') {
                this.sdmFormGroup.patchValue({ 
                    isfinalscreenin: 'false',
                    cpsResponseType: null
                 }, { emitEvent: false });

            }
            //----

            if (!this.sdmSettings.isPopulate) {
                this.cpsResponseValidation(this.sdmFormGroup.value);
            }
            
            this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
            this.validateIRAR(this.sdmFormGroup.value);
            
            this.validateSDM(this.sdmFormGroup.value);
            this._store.setData(IntakeStoreConstants.intakeSDM, this.addSdm);

            if (this.sdmFormGroup.get('isfinalscreenin').value === 'true') {
                this.sdmFormGroup.get('immediate').setValidators([Validators.required]);
            } else {
                this.sdmFormGroup.get('immediate').clearValidators();
            }

        });

        if (this.store[IntakeStoreConstants.intakeSDM]) {
            this.patchSDM(this.store[IntakeStoreConstants.intakeSDM], false, true);
        }
        
        this.getCountyDropdown();

        const childfatality = this.store[IntakeStoreConstants.childfatality];
        if (childfatality) {
            this.sdmFormGroup.patchValue({ childfatality: childfatality });
            this.childFatality = childfatality;
        } else {
            this.sdmFormGroup.patchValue({ childfatality: 'no' });
            this.childFatality = 'no';
        }
        this.reporMinDateValdiation = moment().subtract(6, 'months');
      
        this.involvedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (this.involvedPersons) {
            this.updateReferralName();
            this.updateSubstantialRisk();
            const childInfoArr =    this.involvedPersons.filter(p => p.personRole.filter(rl => rl.rolekey === 'CHILD'));
            if (childInfoArr && childInfoArr.length >= 1) {
                childInfoArr.filter(chld => {
                if (chld.dateofdeath) {

                    this.sdmFormGroup.patchValue({ childfatality: 'yes' });
                    this.childFatality = 'yes';
                    this.childfatalityflag = true;
                }
                });
                if (this.childfatalityflag === false) {
                    this.sdmFormGroup.patchValue({ childfatality: 'no' });
                    this.childFatality = 'no';
                }
            }
        }
        if (this.CPSFlag === 'disable') {
            this.sdmFormGroup.get('disqualifyingCriteria').reset();
            this.sdmFormGroup.get('disqualifyingCriteria').disable();
            this.sdmFormGroup.get('disqualifyingFactors').reset();
            this.sdmFormGroup.get('disqualifyingFactors').disable();

        } else if (this.CPSFlag === 'enable') {
            this.sdmFormGroup.get('disqualifyingCriteria').enable();
            this.sdmFormGroup.get('disqualifyingFactors').enable();
        }
    }

    setsearchprovider() {
        this.searchprovider.providerid = '';
        this.searchprovider.providernm = '';
        this.searchprovider.providerfname = '';
        this.searchprovider.providerlname = '';
        this.providersList = [];
    }

    resetOverrides(): any {
        this.sdmFormGroup.patchValue({
            scnRecommendOveride: '',
            isfinalscreenin: null,
            immediate: '',
        }, { emitEvent: false });

        this.sdmFormGroup.get('screenOut').patchValue({
            isscrnoutrecovr_insufficient: false,
            isscrnoutrecovr_information: false,
            isscrnoutrecovr_historicalinformation: false,
            isscrnoutrecovr_otherspecify: false,
            scrnout_description: ''
        }, { emitEvent: false });

        this.sdmFormGroup.get('screenIn').patchValue({
            isscrninrecovr_courtorder: false,
            isscrninrecovr_otherspecify: false,
            scrnin_description: ''
        }, { emitEvent: false });

        this.sdmFormGroup.get('immediateList').patchValue({
            isimmed_childfaatility: false,
            isimmed_seriousinjury: false,
            isimmed_childleftalone: false,
            isimmed_allegation: false,
            isimmed_otherspecify: false,
            immediateList6: '',
        }, { emitEvent: false });

        this.sdmFormGroup.get('noImmediateList').patchValue({
            isnoimmed_physicalabuse: false,
            isnoimmed_sexualabuse: false,
            isnoimmed_neglectresponse: false,
            isnoimmed_mentalinjury: false,
            isnoimmed_screeninoverride: false,
            isnoimmed_substantial_risk: false,
            isnoimmed_risk_harm: false,
        }, { emitEvent: false });
    }

    // changeFinalScreenDecision(event) {
    //     if (event === 'false') {
    //         // this.sdmFormGroup.get('immediate').disable();
    //         this.sdmFormGroup.get('immediate').reset();
    //         this.sdmFormGroup.get('immediateList').reset();
    //         this.sdmFormGroup.get('noImmediateList').reset();
    //         this.isNoImmediate = false;
    //         this.isImmediate = false;
    //     } else {
    //         // this.sdmFormGroup.get('immediate').enable();
    //         // this.isImmediate = true;
    //     }
    // }

    searchProvider() {
        // this.providersList = [];
        this._commonHttpService.getArrayList(
            {
              method: 'post',
              nolimit: true,
            //   providercategorycd: this.searchprovider.providerid,
              providerid: this.searchprovider.providerid,
              providername: this.searchprovider.providernm,
              filter: {}
            },
            'providerreferral/providersearch'
        // )
        ).subscribe(providers => {
            console.log('Providers List' , providers );
            // if (providers && providers.length) {
            this.providersList = providers;
            // }
            // this.setsearchprovider();
        });
    }

    selectProvider(provider: any) {
        // this.selectedProvider.push(provider);
        const provider1 = {providername : provider.provider_nm , providerid : provider.provider_id, providerphone : provider.adr_work_phone_tx};

        const providerControl = <FormArray>this.sdmFormGroup.controls.provider;
        if (providerControl) {
            const isProviderExist = providerControl.controls.find(item => item.get('providername').value === provider1.providername);
            console.log('Provider exist ' + isProviderExist);
            if (!isProviderExist) {
                providerControl.push(this.buildProviderForm(provider1));
            }
        }
        // const providerControl = <FormArray>this.sdmFormGroup.controls.provider;
        // if (this.provider) {
        //     this.provider.forEach((x) => {
        //         providerControl.push(this.buildProviderForm(x));
        //     });
        // }
    }

    onConfirmProvider() {
        if (this.selectedProvider) {
            (<any>$('#providersearch')).modal('hide');
            this.setsearchprovider();
            console.log('in selected provider');
        } else {
          this._alertService.warn('Please select any provider');
        }
    }

    getInvolvedPerson(mode) {
        this.sdmFormGroup.get('allegedvictim').disable();
        this.sdmFormGroup.get('allegedmaltreator').disable();
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (addedPersons && addedPersons.length) {
            this.childunderoneyearflag = false;
            addedPersons.map((item) => {
                item.personRole.map((roleval) => {
                    if (roleval.rolekey === 'AV') {
                        this.sdmFormGroup.setControl('allegedvictim', this.formBuilder.array([]));
                        this.allegedVictim = this.allegedVictim && this.allegedVictim.length !== 0 ? this.allegedVictim : [{ victimname: item.fullName }];
                        const avData = this.allegedVictim.filter((name) => name.victimname === item.fullName);
                        if (avData.length === 0) {
                            const allegedV = {
                                victimname: item.fullName
                            };
                            this.allegedVictim.push(allegedV);
                        }
                        this.sdmFormGroup.get('allegedvictim').disable();
                    }
                    if (roleval.rolekey === 'AM') {

                        this.sdmFormGroup.setControl('allegedmaltreator', this.formBuilder.array([]));
                        this.allegedMaltreator = this.allegedMaltreator && this.allegedMaltreator.length !== 0 ? this.allegedMaltreator : [{ maltreatorsname: item.fullName }];
                        const amData = this.allegedMaltreator.filter((name) => name.maltreatorsname === item.fullName);
                        if (amData.length === 0) {
                            const allegedM = {
                                maltreatorsname: item.fullName
                            };
                            this.allegedMaltreator.push(allegedM);
                        }
                        this.sdmFormGroup.get('allegedmaltreator').disable();
                    }

                    const roles = item.personRole;
                    if (roles && Array.isArray(roles)) {
                        const valid = roles.some(role => {
                            if (['CHILD', 'AV'].includes(role.rolekey)) { return role.rolekey; }
                        });

                        if (valid) {
                            const age = this.calculateAge(item.Dob);
                            if (age >= 0 && age <= 1) {
                                this.childunderoneyearflag = true;
                            }
                        }
                        return valid;
                    } else {
                        return false;
                    }
                });
            });
            if (mode === 'add') {
                this.setFormValues();
            }
        }
    }

    calculateAge(dob) {
        let age = 0;
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            age = moment().diff(rCDob, 'years');
        }
        return age;
    }

    private patchSDM(sdm, changePathway, loadSDMs) {
        if (sdm) {
            this.sdmSettings.isPopulate = true;
            this.populateSdm = Object.assign({}, sdm);
            this.sdmFormGroup.setControl('allegedvictim', this.formBuilder.array([]));
            this.sdmFormGroup.setControl('allegedmaltreator', this.formBuilder.array([]));
            this.sdmFormGroup.setControl('provider', this.formBuilder.array([]));
            this.populateSdm.referraldob = new Date(this.populateSdm.referraldob);
            this.populateSdm.reportdate = this.populateSdm.reportdate ? new Date(this.populateSdm.reportdate) : null;
            this.populateSdm.workerdate = this.populateSdm.workerdate ? new Date(this.populateSdm.workerdate) : null;
            this.populateSdm.supervisordate = this.populateSdm.supervisordate ? new Date(this.populateSdm.supervisordate) : null;

            /* Patch start */
            this.populateSdm.physicalAbuse = Object.assign({
                ismalpa_suspeciousdeath: sdm.ismalpa_suspeciousdeath,
                ismalpa_nonaccident: sdm.ismalpa_nonaccident,
                ismalpa_injuryinconsistent: sdm.ismalpa_injuryinconsistent,
                ismalpa_insjury: sdm.ismalpa_insjury,
                ismalpa_childtoxic: sdm.ismalpa_childtoxic,
                ismalpa_caregiver: sdm.ismalpa_caregiver
            });
            this.populateSdm.sexualAbuse = Object.assign({
                ismalsa_sexualmolestation: sdm.ismalsa_sexualmolestation,
                ismalsa_sexualact: sdm.ismalsa_sexualact,
                ismalsa_sexualexploitation: sdm.ismalsa_sexualexploitation,
                ismalsa_physicalindicators: sdm.ismalsa_physicalindicators,
                ismalsa_sex_trafficking: sdm.ismalsa_sex_trafficking
            });
            this.populateSdm.generalNeglect = Object.assign({
                isneggn_suspiciousdeath: sdm.isneggn_suspiciousdeath,
                isneggn_signsordiagnosis: sdm.isneggn_signsordiagnosis,
                isneggn_inadequatefood: sdm.isneggn_inadequatefood,
                isneggn_childdischarged: sdm.isneggn_childdischarged
            });
            this.populateSdm.arGeneralNeglect = Object.assign({
                isneggn_exposuretounsafe: sdm.isneggn_exposuretounsafe,
                isneggn_inadequateclothing: sdm.isneggn_inadequateclothing,
                isneggn_inadequatesupervision: sdm.isneggn_inadequatesupervision,
                isnegrh_treatmenthealthrisk: sdm.isnegrh_treatmenthealthrisk
            });
            this.populateSdm.unattendedChild = Object.assign({
                isneguc_leftunsupervised: sdm.isneguc_leftunsupervised,
                isneguc_leftaloneinappropriatecare: sdm.isneguc_leftaloneinappropriatecare,
                isneguc_leftalonewithoutsupport: sdm.isneguc_leftalonewithoutsupport
            });
            this.populateSdm.riskofHarm = Object.assign({
                isnegrh_priordeath: sdm.isnegrh_priordeath,
                isnegrh_exposednewborn: sdm.isnegrh_exposednewborn,
                // isnegrh_domesticviolence: sdm.isnegrh_domesticviolence,
                // isnegrh_sexualperpetrator: sdm.isnegrh_sexualperpetrator,
                isnegrh_basicneedsunmet: sdm.isnegrh_basicneedsunmet,
                // isnegrh_substantial_risk: sdm.isnegrh_substantial_risk
                isnegrh_sex_offender: sdm.isnegrh_sex_offender,
                isnegrh_risk_dv: sdm.isnegrh_risk_dv,
                isnegrh_sex_trafficking: sdm.isnegrh_sex_trafficking,
                isnegrh_fatality_can: sdm.isnegrh_fatality_can,
                isnegrh_indicated_unsub: sdm.isnegrh_indicated_unsub,
                isnegrh_survivor: sdm.isnegrh_survivor,
                isnegrh_birth_match: sdm.isnegrh_birth_match
            });
            this.populateSdm.screenOut = Object.assign({
                isscrnoutrecovr_insufficient: sdm.isscrnoutrecovr_insufficient,
                isscrnoutrecovr_information: sdm.isscrnoutrecovr_information,
                isscrnoutrecovr_historicalinformation: sdm.isscrnoutrecovr_historicalinformation,
                isscrnoutrecovr_otherspecify: sdm.isscrnoutrecovr_otherspecify,
                scrnout_description: sdm.scrnout_description
            });
            this.populateSdm.screenIn = Object.assign({
                isscrninrecovr_courtorder: sdm.isscrninrecovr_courtorder,
                isscrninrecovr_otherspecify: sdm.isscrninrecovr_otherspecify,
                scrnin_description: sdm.scrnin_description
            });
            this.populateSdm.immediateList = Object.assign({
                isimmed_childfaatility: sdm.isimmed_childfaatility,
                isimmed_seriousinjury: sdm.isimmed_seriousinjury,
                isimmed_childleftalone: sdm.isimmed_childleftalone,
                isimmed_allegation: sdm.isimmed_allegation,
                isimmed_otherspecify: sdm.isimmed_otherspecify,
                immediateList6: sdm.immediateList6
            });
            this.populateSdm.noImmediateList = Object.assign({
                isnoimmed_physicalabuse: sdm.isnoimmed_physicalabuse,
                isnoimmed_sexualabuse: sdm.isnoimmed_sexualabuse,
                isnoimmed_neglectresponse: sdm.isnoimmed_neglectresponse,
                isnoimmed_mentalinjury: sdm.isnoimmed_mentalinjury,
                isnoimmed_screeninoverride: sdm.isnoimmed_screeninoverride,
                isnoimmed_substantial_risk: sdm.isnoimmed_substantial_risk,
                isnoimmed_risk_harm: sdm.isnoimmed_risk_harm
            });
            this.populateSdm.disqualifyingCriteria = Object.assign({
                issexualabuse: sdm.issexualabuse,
                isoutofhome: sdm.isoutofhome,
                isdeathorserious: sdm.isdeathorserious,
                isrisk: sdm.isrisk,
                isreportmeets: sdm.isreportmeets,
                issignordiagonises: sdm.issignordiagonises,
                ismaltreatment3yrs: sdm.ismaltreatment3yrs,
                ismaltreatment12yrs: sdm.ismaltreatment12yrs,
                ismaltreatment24yrs: sdm.ismaltreatment24yrs,
                isactiveinvestigation: sdm.isactiveinvestigation
            });
            this.populateSdm.disqualifyingFactors = Object.assign({
                isreportedhistory: sdm.isreportedhistory,
                ismultiple: sdm.ismultiple,
                isdomesticvoilence: sdm.isdomesticvoilence,
                iscriminalhistory: sdm.iscriminalhistory,
                isthread: sdm.isthread,
                islawenforcement: sdm.islawenforcement,
                iscourtiinvestigation: sdm.iscourtiinvestigation
            });

            if (sdm.ismaltreatment) {
                if(sdm.ismaltreatment === true) {
                    this.populateSdm.maltreatment = 'yes';
                } else if (sdm.ismaltreatment === false) {
                    this.populateSdm.maltreatment = 'no';
                } else {
                    this.populateSdm.maltreatment = null;
                }
            }

            if (sdm.ischildfatality) {
                if (sdm.ischildfatality === '') {
                    this.populateSdm.childfatality = '';
                } else if (sdm.ischildfatality === 'yes') {
                    this.populateSdm.childfatality = 'yes';
                } else {
                    this.populateSdm.childfatality = 'no';
                }
            }

            if (sdm.isrecsc_screenout) {
                this.populateSdm.screeningRecommend = 'ScreenOUT';
            } else if (sdm.isrecsc_scrrenin) {
                this.populateSdm.screeningRecommend = 'Scrnin';
            }

            if (sdm.isir) {
                this.populateSdm.cpsResponseType = 'CPS-IR';
            } else if (sdm.isar) {
                this.populateSdm.cpsResponseType = 'CPS-AR';
            }
            this.populateSdm.isnegfp_cargiverintervene = sdm.isnegfp_cargiverintervene;
            this.populateSdm.isnegab_abandoned = sdm.isnegab_abandoned;

            if (sdm.isnoimmed_substantial_risk) {
                this.populateSdm.immediate = 'No Immediate';
            } else if (sdm.isnoimmed_physicalabuse || sdm.isnoimmed_sexualabuse || sdm.isnoimmed_neglectresponse || sdm.isnoimmed_mentalinjury || 
                    sdm.isnoimmed_risk_harm || sdm.isnoimmed_screeninoverride) {
                this.populateSdm.immediate = 'No Immediate';
            } else if (sdm.isimmed_childfaatility || sdm.isimmed_seriousinjury || sdm.isimmed_childleftalone || sdm.isimmed_allegation || sdm.isimmed_otherspecify) {
                this.populateSdm.immediate = 'Immediate';
            } else {
                this.populateSdm.immediate = '';
            }
            if (this.populateSdm.immediate === 'Immediate') {
                this.isImmediate = true;
                this.isNoImmediate = false;
            } else if (this.populateSdm.immediate === 'No Immediate') {
                this.isImmediate = false;
                this.isNoImmediate = true;
            }

            if (sdm.isscrnoutrecovr_insufficient || sdm.isscrnoutrecovr_information || sdm.isscrnoutrecovr_historicalinformation || sdm.isscrnoutrecovr_otherspecify) {
                this.populateSdm.scnRecommendOveride = 'OvrScrnout';
                this.populateSdm.isfinalscreenin = false;
                this.populateSdm.cpsResponseType = null;
                this.populateSdm.isar = false;
                this.populateSdm.isir = false;
            } else if (sdm.isscrninrecovr_courtorder || sdm.isscrninrecovr_otherspecify) {
                this.populateSdm.scnRecommendOveride = 'Ovrscrnin';
                this.populateSdm.isfinalscreenin = true;
            } else {
                this.populateSdm.scnRecommendOveride = '';
            }

            this.isScreenOutIN = this.populateSdm.scnRecommendOveride;

            this.populateSdm.county = sdm.countyid;

            /* Patch end */

            this.sdmFormGroup.patchValue(this.populateSdm);

            if (this.populateSdm.isfinalscreenin) {
                this.sdmFormGroup.patchValue({ isfinalscreenin: 'true' }, { emitEvent: false });
            } else {
                this.sdmFormGroup.patchValue({ 
                    isfinalscreenin: 'false',
                    cpsResponseType: null
                 }, { emitEvent: false });
            }
            this.provider = this.populateSdm.provider;
            if (this.populateSdm.maltreatment === 'yes') {
                this.isDisplayProvider = true;
            }
            this.cpsResponseValidation(this.populateSdm);
            this.getInvolvedPerson('edit');
            this.setFormValues();
            this.roleId = this._authService.getCurrentUser();
            this.populateSdm.datesubmitted = this.populateSdm.datesubmitted ? this.populateSdm.datesubmitted : this.submittedDate;
            if (this.roleId.role.name === 'apcs' && this.populateSdm.datesubmitted) {
                this.submittedDate = this.populateSdm.datesubmitted;
                this.sdmSettings.isSupervisor = true;
                this.validateOverrideDays(this.populateSdm.datesubmitted);
                if (loadSDMs) {
                    this.listSDMs(this.intakeNumber).subscribe((data) => {
                        console.log(data);
                        this.savedSDMs = data[0].getintakeservicerequestsdm;
                        // this.sdmFormGroup.disable();
                    });
                }
                if (changePathway) {
                    this.sdmFormGroup.enable();
                    this.pathwayChange = true;
                    this.populateSdm.changePathway = true;
                    this._store.setData(IntakeStoreConstants.intakeSDM, this.populateSdm);
                    // this.sdmInputSubject$.next(this.populateSdm);
                    this.sdmSettings.isPopulate = false;
                } else {
                    // New requirement => Supervisor can edit SDM
                    //this.sdmFormGroup.disable();
                }
            } else {
                this.sdmSettings.isPopulate = false;
            }
        }
        // this.sdmSettings.isPopulate = false;
    }

    switchPathway(sdmData, changePathway, loadSDMs) {
        this.patchSDM(sdmData, changePathway, loadSDMs);
        this.sdmFormGroup.get('disqualifyingCriteria').disable();
        (<any>$('#tab-step3')).click();
    }

    private listSDMs(intakeNumber) {
        return this._commonHttpService.getArrayList(
            new PaginationRequest({
                where: { servicerequestid: null, intakenumber: intakeNumber },
                method: 'get'
            }),
            NewUrlConfig.EndPoint.Intake.IntakeSdmListUrl
        );
    }

    buildFormGroup() {
        this.sdmFormGroup = this.formBuilder.group({
            referralname: [''],
            referraldob: [new Date()],
            referralid: [''],
            countyid: [null],
            ismaltreatment: [false],
            maltreatment: [null, [Validators.required]],
            providerKnown: [null],
            providerunknowndetail: [null],
            childfatality: [''],
            ischildfatality: [false],
            isfcplacementsetting: [false],
            isprivateplacement: [false],
            islicenseddaycare: [false],
            isschool: [false],
            physicalAbuse: this.formBuilder.group({
                ismalpa_suspeciousdeath: [false],
                ismalpa_nonaccident: [false],
                ismalpa_injuryinconsistent: [false],
                ismalpa_insjury: [false],
                ismalpa_childtoxic: [false],
                ismalpa_caregiver: [false]
            }),
            sexualAbuse: this.formBuilder.group({
                ismalsa_sexualmolestation: [false],
                ismalsa_sexualact: [false],
                ismalsa_sexualexploitation: [false],
                ismalsa_physicalindicators: [false],
                ismalsa_sex_trafficking: [false]
            }),
            generalNeglect: this.formBuilder.group({
                isneggn_suspiciousdeath: [false],
                isneggn_signsordiagnosis: [false],
                isneggn_inadequatefood: [false],
                isneggn_childdischarged: [false]
            }),
            arGeneralNeglect: this.formBuilder.group({
                isneggn_exposuretounsafe: [false],
                isneggn_inadequateclothing: [false],
                isneggn_inadequatesupervision: [false],
                isnegrh_treatmenthealthrisk: [false]
            }),
            isnegfp_cargiverintervene: [false],
            isnegab_abandoned: [false],
            unattendedChild: this.formBuilder.group({
                isneguc_leftunsupervised: [false],
                isneguc_leftaloneinappropriatecare: [false],
                isneguc_leftalonewithoutsupport: [false]
            }),
            riskofHarm: this.formBuilder.group({
                isnegrh_priordeath: [false],
                isnegrh_exposednewborn: [{value: false, disabled: true}],
                isnegrh_basicneedsunmet: [false],
                isnegrh_sex_offender: [false],
                isnegrh_risk_dv: [false],
                isnegrh_fatality_can: [false],
                isnegrh_indicated_unsub: [false],
                isnegrh_survivor: [false],
                isnegrh_birth_match: [false],
                isnegrh_sex_trafficking: [false]
            }),
            isnegmn_unreasonabledelay: [false],
            ismenab_psycologicalability: [false],
            ismenng_psycologicalability: [false],
            screeningRecommend: [''],
            scnRecommendOveride: [''],
            screenOut: this.formBuilder.group({
                isscrnoutrecovr_insufficient: [false],
                isscrnoutrecovr_information: [false],
                isscrnoutrecovr_historicalinformation: [false],
                isscrnoutrecovr_otherspecify: [false],
                scrnout_description: ['']
            }),
            screenIn: this.formBuilder.group({
                isscrninrecovr_courtorder: [false],
                isscrninrecovr_otherspecify: [false],
                scrnin_description: ['']
            }),
            immediate: [''],
            immediateList: this.formBuilder.group({
                isimmed_childfaatility: [false],
                isimmed_seriousinjury: [false],
                isimmed_childleftalone: [false],
                isimmed_allegation: [false],
                isimmed_otherspecify: [false],
                immediateList6: ['']
            }),
            noImmediateList: this.formBuilder.group({
                isnoimmed_physicalabuse: [false],
                isnoimmed_sexualabuse: [false],
                isnoimmed_neglectresponse: [false],
                isnoimmed_mentalinjury: [false],
                isnoimmed_screeninoverride: [false],
                isnoimmed_substantial_risk: [false],
                isnoimmed_risk_harm: [false]
            }),
            childunderoneyear: [''],
            childUnderOneYear: [''],
            officerfirstname: [''],
            officermiddlename: [''],
            officerlastname: [''],
            badgenumber: [''],
            recordnumber: [''],
            reportdate: [null],
            worker: [''],
            comments: [''],
            workerdate: [null],
            supervisor: [''],
            supervisordate: [null],
            disqualifyingCriteria: this.formBuilder.group({
                issexualabuse: [false],
                isoutofhome: [false],
                isdeathorserious: [false],
                isrisk: [false],
                isreportmeets: [false],
                issignordiagonises: [false],
                ismaltreatment3yrs: [false],
                ismaltreatment12yrs: [false],
                ismaltreatment24yrs: [false],
                isactiveinvestigation: [false]
            }),
            disqualifyingFactors: this.formBuilder.group({
                isreportedhistory: [false],
                ismultiple: [false],
                isdomesticvoilence: [false],
                iscriminalhistory: [false],
                isthread: [false],
                islawenforcement: [false],
                iscourtiinvestigation: [false]
            }),
            cpsResponseType: [null],
            isar: [true],
            isir: [false],
            isfinalscreenin: [null]
        });
        this.sdmFormGroup.addControl('allegedvictim', this.formBuilder.array([this.createFormGroup('allegedvictim')]));
        this.sdmFormGroup.addControl('allegedmaltreator', this.formBuilder.array([this.createFormGroup('allegedmaltreator')]));
        this.sdmFormGroup.addControl('provider', this.formBuilder.array([]));
    }

    updateReferralName() {
        if (this.involvedPersons && this.involvedPersons.length > 0) {
            for (let i = 0; i < this.involvedPersons.length; i++) {
                if (this.involvedPersons[i].Role === 'RC') {
                    this.sdmFormGroup.patchValue({
                        referralname: this.involvedPersons[i].fullName
                    });
                }
            }
        }
    }

    updateSubstantialRisk() {
        if (this.involvedPersons && this.involvedPersons.length) {
            this.sdmFormGroup.get('riskofHarm').patchValue({'isnegrh_exposednewborn': false });
            this.sdmFormGroup.get('riskofHarm').get('isnegrh_exposednewborn').disable();
            this.isSENFlag = false;
            this.validateIRARScreenIn();
            if (this.sdmFormGroup.get('scnRecommendOveride').value === 'OvrScrnout') {
                this.sdmFormGroup.patchValue({ 
                    isfinalscreenin: 'false',
                    cpsResponseType: null
                 }, { emitEvent: false });
            }
            const hasSubstantialRiskPerson = this.involvedPersons.find(person => (person.drugexposednewbornflag === 1 || person.fetalalcoholspctrmdisordflag === 1 ));
            if (hasSubstantialRiskPerson) {
                this.sdmFormGroup.get('riskofHarm').patchValue({'isnegrh_exposednewborn': true });
                this.sdmFormGroup.patchValue({ screeningRecommend: 'accept_as_noncps' }, { emitEvent: false });
                this.sdmFormGroup.patchValue({ isfinalscreenin: 'Ovr_as_noncps' }, { emitEvent: false });

                this.isSENFlag = true;
                this.validateIRARScreenIn();
                if (this.sdmFormGroup.get('scnRecommendOveride').value === 'OvrScrnout') {
                    this.sdmFormGroup.patchValue({ 
                        isfinalscreenin: 'false',
                        cpsResponseType: null
                     }, { emitEvent: false });
                }
            }
        }
    }

    getCountyDropdown() {
        this.sdmCountyValuesDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.SdmCountyListUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }

    setFormValues() {
        const control = <FormArray>this.sdmFormGroup.controls.allegedvictim;
        if (this.allegedVictim) {
            this.allegedVictim.forEach((x) => {
                control.push(this.buildAllegedVictimForm(x));
            });
        }

        const allegedMaltreatorControl = <FormArray>this.sdmFormGroup.controls.allegedmaltreator;
        if (this.allegedMaltreator) {
            this.allegedMaltreator.forEach((x) => {
                allegedMaltreatorControl.push(this.buildAllegedMaltreatorForm(x));
            });
        }

        const providerControl = <FormArray>this.sdmFormGroup.controls.provider;
        if (this.provider) {
            this.provider.forEach((x) => {
                providerControl.push(this.buildProviderForm(x));
            });
        }
    }

    private buildAllegedVictimForm(x): FormGroup {
        return this.formBuilder.group({
            victimname: x.victimname ? x.victimname : ''
        });
    }
    private buildAllegedMaltreatorForm(x): FormGroup {
        return this.formBuilder.group({
            maltreatorsname: x.maltreatorsname ? x.maltreatorsname : ''
        });
    }
    private buildProviderForm(x): FormGroup {
        return this.formBuilder.group({
            providername: x.providername ? x.providername : '',
            providerid: x.providerid ,
            providerphone: x.providerphone ? x.providerphone : ''
        });
    }

    createFormGroup(formGroupName) {
        if (formGroupName === 'allegedvictim') {
            return this.formBuilder.group({
                victimname: ['']
            });
        } else if (formGroupName === 'allegedmaltreator') {
            return this.formBuilder.group({
                maltreatorsname: ['']
            });
        } else if (formGroupName === 'provider') {
            return this.formBuilder.group({
                providername: ['']
            });
        }
    }

    addNewFormGroup(formGroupName) {
        const control = <FormArray>this.sdmFormGroup.controls[formGroupName];
        control.push(this.createFormGroup(formGroupName));
    }

    deleteFormGroup(index: number, formGroupName) {
        const control = <FormArray>this.sdmFormGroup.controls[formGroupName];
        control.removeAt(index);
        }


    // @TM: D-07481
    onChangeMaltreatment(item) {
        this.sdmFormGroup.patchValue({ providerKnown : null});
        if (item === 'yes') {
            this.isDisplayProvider = true;
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isoutofhome: true }, { emitEvent: false });
            this.sdmSettings.isoutofhome = true;
        } else {
            this.isDisplayProvider = false;
            this.disableAbuseNeg = false;
            const control = <FormArray>this.sdmFormGroup.controls['provider'];
            control.controls = [];
            // control.push(this.createFormGroup('provider'));
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isoutofhome: false }, { emitEvent: false });
            this.sdmSettings.isoutofhome = false;
        }
    }

    changeScreenOutIN(item) {
        this.isScreenOutIN = item;
    }

    changeOverScreenOutIN(item) {
        this.isScreenOutIN = item;
    }

    changeImmediate(item) {
        if (item === 'Immediate') {
            this.sdmFormGroup.get('noImmediateList').reset();
            this.isImmediate = true;
            this.isNoImmediate = false;
        } else if (item === 'No Immediate') {
            this.sdmFormGroup.get('immediateList').reset();
            this.isImmediate = false;
            this.isNoImmediate = true;
        }
    }

    goToNextPage(pageToGo: string) {
        // this.setRecomNOverds();
        const pageId = $('#' + pageToGo);
        pageId.click();
        $('html,body').animate({ scrollTop: pageId.offset().top }, 'slow');
    }

    clearAllImmediateSelections() {
        this.sdmFormGroup.get('noImmediateList').patchValue({
            isnoimmed_physicalabuse: false,
            isnoimmed_sexualabuse: false,
            isnoimmed_neglectresponse: false,
            isnoimmed_mentalinjury: false,
            isnoimmed_substantial_risk: false,
            isnoimmed_risk_harm: false
        });
    }

    setImmediates() {
        let sdmForm = this.sdmFormGroup.getRawValue();
        
        this.cpsImmediatesResponseValidation(sdmForm);
        const noImmediateList = sdmForm.noImmediateList;
        
        if (noImmediateList.isnoimmed_screeninoverride) {
            this.sdmFormGroup.get('noImmediateList').get('isnoimmed_screeninoverride').disable();
        }
    }

    changeChildInderOneYear(item) {
        this.isChildInderOneYear = item;
        this.validateIRAR(this.sdmFormGroup.value);
    }

    validateRiskForm(event: any) {
        console.log(event);
        const sdmForm = this.sdmFormGroup.getRawValue();
        const disqualifyingCriteria = sdmForm.disqualifyingCriteria;
        const riskofHarm = sdmForm.riskofHarm;
        if (riskofHarm.isnegrh_exposednewborn || riskofHarm.isnegrh_priordeath) {
            if (disqualifyingCriteria.issexualabuse) {
                this.sdmFormGroup.get('disqualifyingCriteria').enable();
            } else {
                this.sdmFormGroup.get('disqualifyingCriteria').disable();
            }
        }
        if (riskofHarm.isnegrh_exposednewborn) {
            this.isSENFlag = true;
        } else {
            this.isSENFlag = false;
        }
    }

    // D-07475: re-writing validations as indicated
    private cpsResponseValidation(sdm) {
        this.populateSdm = Object.assign({}, sdm);
        this.sdmFormGroup.patchValue({ 
            screeningRecommend: 'ScreenOUT', 
            isfinalscreenin: 'false',
         }, { emitEvent: false });  
        this.scrninFlag = false;

        if (ObjectUtils.checkTrueProperty(this.populateSdm.sexualAbuse) >= 1) { // Sexual Abuse - any selection
            this.sdmFormGroup.patchValue({ 
                screeningRecommend: 'Scrnin',
                isfinalscreenin: 'true',
            }, { emitEvent: false });
            this.scrninFlag = true;

            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issexualabuse: true }, { emitEvent: false });
        } else {
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issexualabuse: false }, { emitEvent: false });
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.physicalAbuse) >= 1) { // Physical Abuse
            this.sdmFormGroup.patchValue({ 
                screeningRecommend: 'Scrnin',
                isfinalscreenin: 'true',
            }, { emitEvent: false });   
            this.scrninFlag = true;

            if (this.populateSdm.physicalAbuse.ismalpa_suspeciousdeath === true) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: true }, { emitEvent: false });
                this.truePropertyPAFlag = true;
            } else {
                if (!this.truePropertyGNFlag) {
                    this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
                }
                this.truePropertyPAFlag = false;
            }
        } else {

            if (!this.truePropertyGNFlag) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
            }
            this.truePropertyPAFlag = false;
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.generalNeglect) >= 1) { // General Neglect
            this.sdmFormGroup.patchValue({ 
                screeningRecommend: 'Scrnin',
                isfinalscreenin: 'true',
            }, { emitEvent: false });  
            this.scrninFlag = true;

            if (this.populateSdm.generalNeglect.isneggn_suspiciousdeath === true) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: true }, { emitEvent: false });
                this.truePropertyGNFlag = true;
            } else {
                if (!this.truePropertyPAFlag) {
                    this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
                }
                this.truePropertyGNFlag = false;
            }

            if (this.populateSdm.generalNeglect.isneggn_signsordiagnosis === true) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issignordiagonises: true }, { emitEvent: false });
            } else {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issignordiagonises: false }, { emitEvent: false });
            }

        } else {

            if (!this.truePropertyPAFlag) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
            }
            this.truePropertyGNFlag = false;

            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issignordiagonises: false }, { emitEvent: false });
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.arGeneralNeglect) >= 1) { // AR General Neglect
            this.sdmFormGroup.patchValue({ 
                screeningRecommend: 'Scrnin',
                isfinalscreenin: 'true',
            }, { emitEvent: false });  
            this.scrninFlag = true;
        } 
        if (this.populateSdm.ismenab_psycologicalability === true || this.populateSdm.ismenng_psycologicalability === true) { // Mental Abuse or Neglect
            this.sdmFormGroup.patchValue({ 
                screeningRecommend: 'Scrnin',
                isfinalscreenin: 'true',
            }, { emitEvent: false });  
            this.scrninFlag = true;

            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isreportmeets: true }, { emitEvent: false });
        } else {
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isreportmeets: false }, { emitEvent: false });
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.riskofHarm) >= 1) { // Risk of Harm
            if(!this.scrninFlag) {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'accept_as_noncps',
                    isfinalscreenin: 'Ovr_as_noncps',
                }, { emitEvent: false });
                this.disableDQ = true;
            } else if(this.scrninFlag){
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'Scrnin',
                    isfinalscreenin: 'true',
                }, { emitEvent: false }); 
                this.disableDQ = false;
            } else {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'ScreenOUT',
                    isfinalscreenin: 'false',
                }, { emitEvent: false }); 
                this.disableDQ = true;
            }
        }
        this.validateCPSIRAR(this.sdmFormGroup.value);
        this.validateIRARScreenIn();
    }

     // Immediates auto-selection - unless it is a manual override
     private cpsImmediatesResponseValidation(sdm) {
        this.populateSdm = Object.assign({}, sdm);
        this.sdmFormGroup.controls['noImmediateList'].enable();
        this.scrninFlag = false;

        if (ObjectUtils.checkTrueProperty(this.populateSdm.sexualAbuse) >= 1) { // Sexual Abuse - any selection
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            this.scrninFlag = true;
            
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_sexualabuse: true }, { emitEvent: false });
        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_sexualabuse: false }, { emitEvent: false });
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.physicalAbuse) >= 1) { // Physical Abuse
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });   
            this.scrninFlag = true;
            
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_physicalabuse: true }, { emitEvent: false });

        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_physicalabuse: false }, { emitEvent: false });
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.generalNeglect) >= 1 || ObjectUtils.checkTrueProperty(this.populateSdm.arGeneralNeglect) >= 1) { // General Neglect
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            this.scrninFlag = true;
           
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_neglectresponse: true }, { emitEvent: false });

        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_neglectresponse: false }, { emitEvent: false });            
        }
        if (this.populateSdm.ismenab_psycologicalability === true || this.populateSdm.ismenng_psycologicalability === true) { // Mental Abuse or Neglect
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });  
            this.scrninFlag = true;
            
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_mentalinjury: true }, { emitEvent: false });            
        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_mentalinjury: false }, { emitEvent: false });                        
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.riskofHarm) >= 1) { // Risk of Harm
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            
            if(!this.scrninFlag) {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'accept_as_noncps',
                    isfinalscreenin: 'Ovr_as_noncps',
                }, { emitEvent: false }); 
                this.disableDQ = true;
            } else if(this.scrninFlag){
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'Scrnin',
                    isfinalscreenin: 'true',
                }, { emitEvent: false }); 
                this.disableDQ = false;
            } else {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'ScreenOUT',
                    isfinalscreenin: 'false',
                }, { emitEvent: false });  
                this.disableDQ = true;
            }
            
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_risk_harm: true }, { emitEvent: false });

        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_risk_harm: false }, { emitEvent: false });
        }

        if (this.isSENFlag === true) {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_substantial_risk: true }, { emitEvent: false });                        
        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_substantial_risk: false }, { emitEvent: false });                        
        }

        this.validateSDM(this.sdmFormGroup.value);
        this._store.setData(IntakeStoreConstants.intakeSDM, this.addSdm);
    }

    private validateOverrideDays(date) {
        const start_date = moment(date, 'YYYY-MM-DD');
        const end_date = moment(new Date(), 'YYYY-MM-DD');
        const duration = moment.duration(end_date.diff(start_date)).asDays();
        if (duration) {
            this.dayToOverride = 60 - Math.ceil(duration);
            if (this.dayToOverride > 0) {
                this.sdmSettings.isDisableOverride = false;
            } else {
                this.sdmSettings.isDisableOverride = true;
            }
        }
    }

    // @TM : Changes to SDM validations to identify CPS Response as AR or IR
    validateIRAR(sdm: Sdm) {
        if (sdm.scnRecommendOveride !== '') { // validation for Screen-In/Out override
            if (sdm.scnRecommendOveride === 'Ovrscrnin') {
                this.disableDQ = false;
                this.validateIRARScreenIn();
                this.sdmFormGroup.patchValue({ isfinalscreenin: 'true' }, { emitEvent: false });    
            } else if (sdm.scnRecommendOveride === 'OvrScrnout') {
                this.disableDQ = true;
                this.validateIRARScreenOut();
                this.sdmFormGroup.patchValue({ isfinalscreenin: 'false', cpsResponseType: null }, { emitEvent: false });    
            } else {
                this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
            }
        } else if (sdm.screeningRecommend === 'Scrnin') {
            this.disableDQ = false;
            this.validateIRARScreenIn();
        } else if (ObjectUtils.checkTrueProperty(sdm.disqualifyingCriteria)) { // This is redundant but avoids validating all other checkboxes if none selected under SDM
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        } else if (ObjectUtils.checkTrueProperty(sdm.disqualifyingFactors)) {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        } else {
            this.disableDQ = true;
            this.validateIRARScreenOut();
        }
        if (this.isChildInderOneYear === 'Yes' || (this.childFatality === 'yes' &&  this.sdmFormGroup.getRawValue().childfatality === 'yes')) {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        }
    }

    validateIRARScreenIn() { // if Screen In or Over-ride Screen In
        const disqualifyingCriteria = (this.sdm.disqualifyingCriteria) ? this.sdm.disqualifyingCriteria : new Disqualifyingcriteria();
        if (this.CPSFlag === 'disable') {       //@TM: not sure why this condition was applied, verify
            if (                                                                              // ----------IR Conditions
                !this.sdm.physicalAbuse.ismalpa_suspeciousdeath &&
                !this.sdm.generalNeglect.isneggn_suspiciousdeath &&
                !this.sdm.generalNeglect.isneggn_signsordiagnosis &&
                !this.sdm.ismenab_psycologicalability &&
                !this.sdm.ismenng_psycologicalability &&
                !this.sdm.sexualAbuse.ismalsa_physicalindicators &&
                !this.sdm.sexualAbuse.ismalsa_sex_trafficking &&
                !this.sdm.sexualAbuse.ismalsa_sexualact &&
                !this.sdm.sexualAbuse.ismalsa_sexualexploitation &&
                !this.sdm.sexualAbuse.ismalsa_sexualmolestation
            ) {
                this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
                this.isIRFlag = false;
            } else if
            (
                this.sdm.physicalAbuse.ismalpa_suspeciousdeath ||
                this.sdm.generalNeglect.isneggn_suspiciousdeath ||
                this.sdm.generalNeglect.isneggn_signsordiagnosis ||
                this.sdm.ismenab_psycologicalability ||
                this.sdm.ismenng_psycologicalability ||
                this.sdm.sexualAbuse.ismalsa_physicalindicators ||
                this.sdm.sexualAbuse.ismalsa_sex_trafficking ||
                this.sdm.sexualAbuse.ismalsa_sexualact ||
                this.sdm.sexualAbuse.ismalsa_sexualexploitation ||
                this.sdm.sexualAbuse.ismalsa_sexualmolestation
            ) {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'Scrnin',
                    isfinalscreenin: 'true',
                    cpsResponseType: 'CPS-IR', }, { emitEvent: false });
                this.isIRFlag = true;
            }
        } else {
            if (                                                                              // ----------IR Conditions
                !this.sdm.physicalAbuse.ismalpa_suspeciousdeath &&
                !this.sdm.generalNeglect.isneggn_suspiciousdeath &&
                !this.sdm.generalNeglect.isneggn_signsordiagnosis &&
                !this.sdm.ismenab_psycologicalability &&
                !this.sdm.ismenng_psycologicalability &&
                !this.sdm.sexualAbuse.ismalsa_physicalindicators &&
                !this.sdm.sexualAbuse.ismalsa_sex_trafficking &&
                !this.sdm.sexualAbuse.ismalsa_sexualact &&
                !this.sdm.sexualAbuse.ismalsa_sexualexploitation &&
                !this.sdm.sexualAbuse.ismalsa_sexualmolestation &&
                // Mandatory Disqualifying Criteria - this is still required to ensure correct precedence
                !disqualifyingCriteria.isrisk &&
                !disqualifyingCriteria.ismaltreatment3yrs &&
                !disqualifyingCriteria.ismaltreatment12yrs &&
                !disqualifyingCriteria.ismaltreatment24yrs &&
                !disqualifyingCriteria.isactiveinvestigation &&
                // Discretionary Disqualifying Factors - this is still required to ensure correct precedence
                !this.sdm.disqualifyingFactors.isreportedhistory &&
                !this.sdm.disqualifyingFactors.ismultiple &&
                !this.sdm.disqualifyingFactors.isdomesticvoilence &&
                !this.sdm.disqualifyingFactors.iscriminalhistory &&
                !this.sdm.disqualifyingFactors.isthread &&
                !this.sdm.disqualifyingFactors.islawenforcement &&
                !this.sdm.disqualifyingFactors.iscourtiinvestigation
            ) {
                this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
                this.isIRFlag = false;
            } else if
            (
                this.sdm.physicalAbuse.ismalpa_suspeciousdeath ||
                this.sdm.generalNeglect.isneggn_suspiciousdeath ||
                this.sdm.generalNeglect.isneggn_signsordiagnosis ||
                this.sdm.ismenab_psycologicalability ||
                this.sdm.ismenng_psycologicalability ||
                this.sdm.sexualAbuse.ismalsa_physicalindicators ||
                this.sdm.sexualAbuse.ismalsa_sex_trafficking ||
                this.sdm.sexualAbuse.ismalsa_sexualact ||
                this.sdm.sexualAbuse.ismalsa_sexualexploitation ||
                this.sdm.sexualAbuse.ismalsa_sexualmolestation ||
                // Disqualifying Criteria - this is still required to ensure correct precedence
                disqualifyingCriteria.isrisk ||
                disqualifyingCriteria.ismaltreatment3yrs ||
                disqualifyingCriteria.ismaltreatment12yrs ||
                disqualifyingCriteria.ismaltreatment24yrs ||
                disqualifyingCriteria.isactiveinvestigation ||
                // Discretionary Disqualifying Factors - this is still required to ensure correct precedence
                this.sdm.disqualifyingFactors.isreportedhistory ||
                this.sdm.disqualifyingFactors.ismultiple ||
                this.sdm.disqualifyingFactors.isdomesticvoilence ||
                this.sdm.disqualifyingFactors.iscriminalhistory ||
                this.sdm.disqualifyingFactors.isthread ||
                this.sdm.disqualifyingFactors.islawenforcement ||
                this.sdm.disqualifyingFactors.iscourtiinvestigation
            ) {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'Scrnin',
                    isfinalscreenin: 'true',
                    cpsResponseType: 'CPS-IR', }, { emitEvent: false });
                this.isIRFlag = true;
            }
        }

        if (                                                              // ------------------------AR Conditions
            !this.isIRFlag && 
            (
            this.sdm.physicalAbuse.ismalpa_caregiver ||
            this.sdm.physicalAbuse.ismalpa_childtoxic ||
            this.sdm.physicalAbuse.ismalpa_injuryinconsistent ||
            this.sdm.physicalAbuse.ismalpa_insjury ||
            this.sdm.physicalAbuse.ismalpa_nonaccident ||
            this.sdm.generalNeglect.isneggn_inadequatefood ||
            this.sdm.arGeneralNeglect.isneggn_exposuretounsafe ||
            this.sdm.arGeneralNeglect.isneggn_inadequateclothing ||
            this.sdm.arGeneralNeglect.isneggn_inadequatesupervision ||
            this.sdm.arGeneralNeglect.isnegrh_treatmenthealthrisk ||
            this.sdm.generalNeglect.isneggn_childdischarged ||
            this.sdm.isnegfp_cargiverintervene ||
            this.sdm.isnegab_abandoned ||
            this.sdm.unattendedChild.isneguc_leftaloneinappropriatecare ||
            this.sdm.unattendedChild.isneguc_leftalonewithoutsupport ||
            this.sdm.unattendedChild.isneguc_leftunsupervised ||
            this.sdm.isnegmn_unreasonabledelay
            )
        ) {
            this.sdmFormGroup.patchValue({ 
                screeningRecommend: 'Scrnin',
                cpsResponseType: 'CPS-AR', }, { emitEvent: false });
            
        } else if (this.isIRFlag) {
            this.sdmFormGroup.patchValue({ 
                screeningRecommend: 'Scrnin',
                cpsResponseType: 'CPS-IR', }, { emitEvent: false });
        } else {
            this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
        }
    }

    validateIRARScreenOut() {
        this.sdmFormGroup.patchValue({ 
            cpsResponseType: null,
        }, { emitEvent: false });
    }

    validateChildFatality(childfatality) {
        if (this.childFatality !== childfatality && childfatality === 'yes') {
            this._alertService.error('Please update child profile with valid D.O.D to proceed.');
            this.sdmFormGroup.patchValue({ childfatality: 'no' });
        } else if (this.childFatality !== childfatality && childfatality === 'no') {
            this._alertService.error('Child profile have a valid D.O.D please update child profile to proceed.');
            this.sdmFormGroup.patchValue({ childfatality: 'yes' });
        }
    }

    validateSDM(sdm) {
        sdm = Object.assign(sdm, sdm.physicalAbuse);
        sdm = Object.assign(sdm, sdm.sexualAbuse);
        sdm = Object.assign(sdm, sdm.generalNeglect);
        sdm = Object.assign(sdm, sdm.arGeneralNeglect);
        sdm = Object.assign(sdm, sdm.unattendedChild);
        sdm = Object.assign(sdm, sdm.riskofHarm);
        sdm = Object.assign(sdm, sdm.screenOut);
        sdm = Object.assign(sdm, sdm.screenIn);
        sdm = Object.assign(sdm, sdm.immediateList);
        sdm = Object.assign(sdm, sdm.noImmediateList);
        sdm = Object.assign(sdm, sdm.disqualifyingCriteria);
        sdm = Object.assign(sdm, sdm.disqualifyingFactors);

        if (sdm.cpsResponseType) {
            if (sdm.cpsResponseType === 'CPS-IR') {
                sdm.isir = true;
                sdm.isar = false;
            } else if (sdm.cpsResponseType === 'CPS-AR') {
                sdm.isir = false;
                sdm.isar = true;
            } else {
                this.validateCPSIRAR(sdm);
            }
        } else {
            this.validateCPSIRAR(sdm);
        }

        if (sdm.maltreatment === 'yes') {
            sdm.ismaltreatment = true;
        } else {
            sdm.ismaltreatment = false;
        }

        if (sdm.childfatality === 'yes') {
            sdm.ischildfatality = true;
        } else {
            sdm.ischildfatality = false;
        }

        if (sdm.screeningRecommend === 'ScreenOUT') {
            sdm.isrecsc_screenout = true;
            sdm.isrecsc_scrrenin = false;
        } else if (sdm.screeningRecommend === 'Scrnin') {
            sdm.isrecsc_screenout = false;
            sdm.isrecsc_scrrenin = true;
        } else if (sdm.screeningRecommend === 'accept_as_noncps') {
            sdm.isrecsc_screenout = false;
            sdm.isrecsc_scrrenin = false;
        }

        this.addSdm = sdm;
    }

    validateCPSIRAR(sdm) {
        if (sdm.scnRecommendOveride !== '') {
            if (sdm.scnRecommendOveride === 'Ovrscrnin') {
                this.sdmFormGroup.patchValue({ 
                    isfinalscreenin: 'true'
                }, { emitEvent: false });
                sdm.isir = true;
                sdm.isar = false;
                this.disableDQ = false;
            } else if (sdm.scnRecommendOveride === 'OvrScrnout') {
                this.sdmFormGroup.patchValue({ 
                    isfinalscreenin: 'false'
                }, { emitEvent: false });
                sdm.isir = false;
                sdm.isar = false;
                this.disableDQ = true;
            }
        } else {
            if (sdm.cpsResponseType === 'CPS-IR') {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'Scrnin',
                }, { emitEvent: false }); 
                sdm.isir = true;
                sdm.isar = false;
                this.disableDQ = false;
            } else if (sdm.cpsResponseType === 'CPS-AR') {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'Scrnin',
                }, { emitEvent: false });
                sdm.isir = false;
                sdm.isar = true;
                this.disableDQ = false;
            } else {
                this.sdmFormGroup.patchValue({ 
                    screeningRecommend: 'ScreenOUT',
                }, { emitEvent: false });
                sdm.isir = false;
                sdm.isar = false;
                this.disableDQ = true;
            }
        }

        if (this.isSENFlag) {
            this.sdmFormGroup.patchValue({
                screeningRecommend: 'accept_as_noncps',
                isfinalscreenin: 'Ovr_as_noncps'
            }, { emitEvent: false });
            sdm.isir = false;
            sdm.isar = false;
            this.disableDQ = true;
        }

    }

    // setRecomNOverds() {
    //     const immediateValue = this.sdmFormGroup.get('immediate').value;
    //     if (immediateValue !== 'Immediate') {
    //         this.setImmediates();
    //     }
    // }

    onMaltreatorChange(key, event) {
        const sdm = this.sdmFormGroup.getRawValue();
        if (event.checked) {
            this.sdmFormGroup.patchValue({
                isfcplacementsetting: false,
                isprivateplacement: false,
                islicenseddaycare: false,
                isschool: false,
            }, { emitEvent: false });
            this.sdmFormGroup.get(key).setValue(event.checked);
        }
    }
    allegedVictimReset() {

        //this.sdmFormGroup.get('allegedvictim').disable();

        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (addedPersons && addedPersons.length) {
            this.childunderoneyearflag = false;
            addedPersons.map((item) => {
                item.personRole.map((roleval) => {
                    if (roleval.rolekey === 'AV') {
                        this.sdmFormGroup.setControl('allegedvictim', this.formBuilder.array([]));
                        this.allegedVictim = this.allegedVictim && this.allegedVictim.length !== 0 ? this.allegedVictim : [{ victimname: item.fullName }];
                        const avData = this.allegedVictim.filter((name) => name.victimname === item.fullName);
                        if (avData.length === 0) {
                            const allegedV = {
                                victimname: item.fullName
                            };
                            this.allegedVictim.push(allegedV);
                        }
                        this.sdmFormGroup.get('allegedvictim').disable();
                    } else {

                    }
                });
            });
        }
        const control = <FormArray>this.sdmFormGroup.controls.allegedvictim;
        if (this.allegedVictim) {
            this.allegedVictim.forEach((x) => {
                control.push(this.buildAllegedVictimForm(x));
            });
        }

        
    }
    allegedMaltreatorReset() {
        //this.sdmFormGroup.get('allegedmaltreator').disable();
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (addedPersons && addedPersons.length) {
            this.childunderoneyearflag = false;
            addedPersons.map((item) => {
                item.personRole.map((roleval) => {

                    if (roleval.rolekey === 'AM') {

                        this.sdmFormGroup.setControl('allegedmaltreator', this.formBuilder.array([]));
                        this.allegedMaltreator = this.allegedMaltreator && this.allegedMaltreator.length !== 0 ? this.allegedMaltreator : [{ maltreatorsname: item.fullName }];
                        const amData = this.allegedMaltreator.filter((name) => name.maltreatorsname === item.fullName);
                        if (amData.length === 0) {
                            const allegedM = {
                                maltreatorsname: item.fullName
                            };
                            this.allegedMaltreator.push(allegedM);
                        }
                        this.sdmFormGroup.get('allegedmaltreator').disable();
                    }
                });
            });
        }
        const allegedMaltreatorControl = <FormArray>this.sdmFormGroup.controls.allegedmaltreator;
        if (this.allegedMaltreator) {
            this.allegedMaltreator.forEach((x) => {
                allegedMaltreatorControl.push(this.buildAllegedMaltreatorForm(x));
            });
        }
    }
}
