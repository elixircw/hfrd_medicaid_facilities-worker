import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeLegalActionComponent } from './intake-legal-action.component';

const routes: Routes = [
  {
      path: '',
      component: IntakeLegalActionComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeLegalActionRoutingModule { }
