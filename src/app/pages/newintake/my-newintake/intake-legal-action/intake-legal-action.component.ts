import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { FormGroup, ValidatorFn, ValidationErrors, Validators, FormBuilder } from '@angular/forms';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { LegalActionHistory } from '../_entities/newintakeModel';
import { NewUrlConfig } from '../../newintake-url.config';
import { IntakeStoreConstants } from '../my-newintake.constants';
@Component({
  selector: 'intake-legal-action',
  templateUrl: './intake-legal-action.component.html',
  styleUrls: ['./intake-legal-action.component.scss']
})
export class IntakeLegalActionComponent implements OnInit {
  hearingTypes$: Observable<DropdownModel[]>;
  courtActions$: Observable<DropdownModel[]>;
  courtOrders$: Observable<DropdownModel[]>;
  adjudicatedDecision$: Observable<DropdownModel[]>;
  adjudicatedOffense$: Observable<DropdownModel[]>;
  legalActionHistoryForm: FormGroup;
  viewHistoryForm: FormGroup;
  id: string;
  courtActionDescription: string[] = [];
  courtOrderDescription: string[] = [];
  histories: LegalActionHistory[] = [];
  intakeNumber: string;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalRecords: number;
  store: any;

  constructor(private _commonHttpService: CommonHttpService, private _route: ActivatedRoute, private _formBuilder: FormBuilder,
    private _dataStore: DataStoreService) {
    this.store = this._dataStore.getCurrentStore();
  }

  ngOnInit() {
    this.id = this._route.snapshot.parent.parent.parent.parent.parent.params['id'];
    this.loadDropdowns();
    this.initFormGroup();
    // this._route.params.subscribe((item) => {
    this.intakeNumber = this.store[IntakeStoreConstants.intakenumber];
    if (this.intakeNumber) {
      this.loadInitialLegalActions();
    }
    // });
  }

  loadInitialLegalActions() {
    const filter = this.legalActionHistoryForm.getRawValue();
    console.log('filter', filter);
    this._commonHttpService.create({
      where: {
        intakenumber: this.intakeNumber,
        type: 'legal'
      },
      filter: {
        petitionID: filter.petitionID ? filter.petitionID : null,
        complaintID: filter.complaintID ? filter.complaintID : null,
        hearingType: filter.hearingType ? filter.hearingType : null,
        worker: filter.worker ? filter.worker : null,
        hearingDate: filter.hearingDate ? filter.hearingDate : null,
        courtActions: filter.courtActions ? filter.courtActions : null,
        courtOrder: filter.courtOrder ? filter.courtOrder : null,
        courtDate: filter.courtDate ? filter.courtDate : null,
        terminationDate: filter.terminationDate ? filter.terminationDate : null,
        adjudicationDate: filter.adjudicationDate ? filter.adjudicationDate : null,
        adjudicationDecision: filter.adjudicationDecision ? filter.adjudicationDecision : null,
        adjudicatedOffense: filter.adjudicatedOffense ? filter.adjudicatedOffense : null
      },
      page: this.paginationInfo.pageNumber,
      limit: 10,
      sortcolumn: 'intakenumber',
      sortorder: 'asc'
    }, NewUrlConfig.EndPoint.Intake.loadLegalActionHistory).subscribe(
      (response) => {
        console.log('response', response);
        if (response && response.data) {
          const type = Array.isArray(response.data);
          if (response.data && response.data.length > 0) {
            this.totalRecords = response.data[0].totalcount;
          } else {
            this.totalRecords = 0;
          }
          this.histories = [];
          response.data.forEach(history => {
            this.histories.push({
              petitionID: history.petitionid,
              complaintID: history.complaintid,
              hearingType: history.hearingtype,
              worker: history.worker,
              hearingDate: history.hearingdate,
              courtActions: history.courtactions,
              courtOrder: history.courtorder,
              courtDate: history.courtDate,
              terminationDate: history.terminationdate,
              adjudicationDate: history.adjudicationdate,
              adjudicationDecision: history.adjudicationdecision,
              adjudicatedOffense: history.adjudicatedoffense
            });
          });
        }
      });

  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadInitialLegalActions();
  }

  loadDropdowns() {
    const courtActionUrl = NewUrlConfig.EndPoint.DSDSAction.Court.CourtActionTypeUrl + '?filter={"nolimit":true,"where": {"intakeservicerequestid": "' + this.id + '"}}';
    const hearing = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.DSDSAction.legalActionHistory.hearingType)
      .map(hearingType => {
        return {
          hearingTypes: hearingType.map(res =>
            new DropdownModel({
              text: res.description,
              value: res.hearingtypekey
            }))
        };
      }).share();

    const courtAction = this._commonHttpService.getArrayList({}, courtActionUrl)
      .map(courtActions => {
        return {
          courtActions: courtActions.map(res => new DropdownModel({
            text: res.description,
            value: res.courtactiontypekey
          }))
        };
      }).share();

    const courtOrder = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.DSDSAction.Court.CourtOrderTypeUrl + '?filter={"nolimit":true}')
      .map(courtOrders => {
        return {
          courtOrders: courtOrders.map(res => new DropdownModel({
            text: res.description,
            value: res.courtordertypekey
          }))
        };
      }).share();

    const adjudicatedDecision = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.DSDSAction.Court.AdjudicatedDecisionTypeUrl + '?filter={"nolimit":true}')
      .map(decision => {
        return {
          decision: decision.map(res => new DropdownModel({
            text: res.description,
            value: res.adjudicateddecisiontypekey
          }))
        };
      }).share();

    const adjudicatedOffense = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.DSDSAction.Allegation.AllegationURL + '?filter={"nolimit":true,"order":"name asc"}')
      .map(offense => {
        return {
          offense: offense['data'].map(res => new DropdownModel({
            text: res.name,
            value: res.allegationid
          }))
        };
      }).share();

    this.hearingTypes$ = hearing.pluck('hearingTypes');
    this.courtActions$ = courtAction.pluck('courtActions');
    this.courtOrders$ = courtOrder.pluck('courtOrders');
    this.adjudicatedDecision$ = adjudicatedDecision.pluck('decision');
    this.adjudicatedOffense$ = adjudicatedOffense.pluck('offense');
  }

  initFormGroup() {
    this.legalActionHistoryForm = this._formBuilder.group({
      petitionID: '',
      complaintID: '',
      hearingType: '',
      worker: '',
      hearingDate: '',
      courtActions: '',
      courtOrder: '',
      courtDate: '',
      terminationDate: '',
      adjudicationDate: '',
      adjudicationDecision: '',
      adjudicatedOffense: ''
    }, { validator: this.atLeastOne(Validators.required) });

    this.viewHistoryForm = this._formBuilder.group({
      petitionID: '',
      complaintID: '',
      hearingType: '',
      worker: '',
      hearingDate: '',
      courtActions: '',
      courtOrder: '',
      courtDate: '',
      terminationDate: '',
      adjudicationDate: '',
      adjudicationDecision: '',
      adjudicatedOffense: ''
    });
  }

  selectCourtActionType(event) {
    if (event) {
      const courtActionType = event.map(res => {
        return { courtactiontypekey: res };
      });
      this.courtActions$.subscribe(items => {
        if (items) {
          const getActiontems = items.filter(item => {
            if (event.includes(item.value)) {
              return item;
            }
          });
          this.courtActionDescription = getActiontems.map(res => res.text);
        }
      });
    }
  }

  selectCourtOrderType(event) {
    if (event) {
      const courtOrderType = event.map(res => {
        return { courtordertypekey: res };
      });
      this.courtOrders$.subscribe(items => {
        if (items) {
          const getOrdertems = items.filter(item => {
            if (event.includes(item.value)) {
              return item;
            }
          });
          this.courtOrderDescription = getOrdertems.map(res => res.text);
        }
      });
    }
  }

  atLeastOne = (validator: ValidatorFn) => (
    group: FormGroup,
  ): ValidationErrors | null => {
    const hasAtLeastOne = group && group.controls && Object.keys(group.controls)
      .some(k => !validator(group.controls[k]));
    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
  }

  // filterHistory(): void {
  //   this.histories = [];
  //   this.histories.push({
  //     petitionID: 'QA1234',
  //     complaintID: 'AQ4321',
  //     hearingType: 'Arrangement',
  //     worker: 'John Doe',
  //     hearingDate: '9/7/2018',
  //     courtActions: ['Dismissed'],
  //     courtOrder: ['Nolle Pros'],
  //     courtDate: '9/7/2018',
  //     terminationDate: '9/7/2018',
  //     adjudicationDate: '9/7/2018',
  //     adjudicationDecision: 'Sustained',
  //     adjudicatedOffense: 'Burglary 4th Degree'
  //   });
  // }

  resetForm() {
    this.histories = [];
    this.legalActionHistoryForm.reset();
    this.loadInitialLegalActions();
  }

  view(history: LegalActionHistory) {
    // history = {
    //   petitionID: 'QA1234',
    //   complaintID: 'AQ4321',
    //   hearingType: 'Arraign',
    //   worker: 'John Doe',
    //   hearingDate: '2018-09-27T00:00:00.000Z',
    //   courtActions: ['AdjudiDismiss'],
    //   courtOrder: ['NP'],
    //   courtDate: '2018-09-27T00:00:00.000Z',
    //   terminationDate: '2018-09-27T00:00:00.000Z',
    //   adjudicationDate: '2018-09-27T00:00:00.000Z',
    //   adjudicationDecision: 'S',
    //   adjudicatedOffense: '4d520133-cc4c-400e-81bc-5ecc50973418'
    // };
    // this.courtActionDescription = ['Dismissed'];
    // this.courtOrderDescription = ['Dismissed'];
    this.viewHistoryForm.patchValue(history);
    this.viewHistoryForm.disable();
    (<any>$('#view-history')).modal('show');
  }
}
