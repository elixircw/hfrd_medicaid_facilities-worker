import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeLegalActionComponent } from './intake-legal-action.component';

describe('IntakeLegalActionComponent', () => {
  let component: IntakeLegalActionComponent;
  let fixture: ComponentFixture<IntakeLegalActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeLegalActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeLegalActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
