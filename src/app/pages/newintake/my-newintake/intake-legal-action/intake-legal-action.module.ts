import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeLegalActionRoutingModule } from './intake-legal-action-routing.module';
import { IntakeLegalActionComponent } from './intake-legal-action.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    IntakeLegalActionRoutingModule,
    FormMaterialModule,
    PaginationModule
  ],
  declarations: [IntakeLegalActionComponent]
})
export class IntakeLegalActionModule { }
