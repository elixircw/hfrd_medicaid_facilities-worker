import { Injectable } from '@angular/core';
import { IntakeStore } from '../../_utils/intake-utils.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionStorageService, AuthService, DataStoreService, CommonHttpService } from '../../../@core/services';
import { IntakeStoreConstants } from './my-newintake.constants';
import { NewUrlConfig } from '../newintake-url.config';

@Injectable()
export class MyNewintakeResolverService {
  intakeStore: IntakeStore;
  intakeNumber: string;
  constructor(private _authService: AuthService, private _commonHttpService: CommonHttpService, private _dataStoreService: DataStoreService, private _sessionStorage: SessionStorageService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.intakeStore = this._sessionStorage.getObj('intake');
    this.intakeNumber = this.intakeStore.number;
    console.log('load intake', this.intakeNumber, this._authService.getAgencyName());

    if (this.intakeStore.action === 'edit' || this.intakeStore.action === 'view') {
      return this.populateIntake();
    }
  }

  populateIntake() {
    return this._commonHttpService
      .create(
        {
          page: 1,
          limit: 10,
          where: {
            status: 'intake',
            intakenumber: this.intakeNumber
          }
        },
        NewUrlConfig.EndPoint.Intake.TemporarySavedIntakeUrl
      );
  }
}
