import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeAppointmentsRoutingModule } from './intake-appointments-routing.module';
import { IntakeAppointmentsComponent } from './intake-appointments.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    CommonModule,
    IntakeAppointmentsRoutingModule,
    FormMaterialModule,
    QuillModule
  ],
  declarations: [
    IntakeAppointmentsComponent
  ]
})
export class IntakeAppointmentsModule { }
