import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IntakeAppointmentsComponent } from './intake-appointments.component';

const routes: Routes = [{
  path: '',
  component: IntakeAppointmentsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeAppointmentsRoutingModule { }
