import { Component, Input, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs/Rx';
import { AuthService, CommonHttpService, AlertService, DataStoreService, CommonDropdownsService } from '../../../../@core/services';
import { InvolvedPerson } from '../_entities/newintakeModel';
import { IntakeAppointment, MembersInMeeting } from '../_entities/newintakeSaveModel';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { RoutingUser } from '../../../cjams-dashboard/_entities/dashBoard-datamodel';
import { NewUrlConfig } from '../../newintake-url.config';
import * as moment from 'moment';
import { RouteDA } from '../_entities/newintakeModel';
import { IntakeStoreConstants, MyNewintakeConstants } from '../my-newintake.constants';
import { AppConstants } from '../../../../@core/common/constants';
import { IntakeConfigService } from '../intake-config.service';
declare var $: any;
const APPOINTMENT_SCHEDULED = 'Scheduled';
const APPOINTMENT_COMPLETED = 'Completed';
const APPOINTMENT_RESCHEDULED = 'Rescheduled';
const DEFAULT_APPOINTMENT_TITLE = 'Intake Interview';
const SCREENING_WORKER = 'SCRNW';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-appointments',
    templateUrl: './intake-appointments.component.html',
    styleUrls: ['./intake-appointments.component.scss']
})
export class IntakeAppointmentsComponent implements OnInit, AfterViewInit {
    // tslint:disable-next-line:no-input-rename
    // @Input('appointmentInputSubject') appointmentInputSubject$ = new Subject<IntakeAppointment[]>();
    // tslint:disable-next-line:no-input-rename
    // @Input('appointmentOutputSubject') appointmentOutputSubject$ = new Subject<IntakeAppointment[]>();
    // tslint:disable-next-line:no-input-rename
    // @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
    // tslint:disable-next-line:no-input-rename
    // @Input('preIntakeSupDicision') preIntakeSupDicision$ = new Subject<string>();

    addedPersons: InvolvedPerson[] = [];
    appointmentForm: FormGroup;
    currentUser: AppUser;
    intakeWorkers$: Observable<RoutingUser[]>;
    intakeWorkerList: RoutingUser[] = [];
    times: any[] = [];
    appointments: IntakeAppointment[] = [];
    isEditAppointment = false;
    isViewAppointment = false;
    isNotesRequired = false;
    actionText = 'Create';
    titleText = 'Create';
    appointmentInAction: IntakeAppointment;
    maxDate = new Date();
    minDate = new Date();
    relations: any = [];
    roles: any = [];
    InterviewList: any[] = [];
    completionNotes = '';
    appointmentHistoryObj: IntakeAppointment[];
    notes = 'Notes...';
    readOnly = false;
    isIntakeInterView: boolean;
    isCustomInterviewTitle: boolean;
    store: any;
    intakeworker: string;
    workerLocation: string;
    assignment: string;
    reason: any[] = [];
    intakeWorkerLocationList: any[] = [];
    assignmentList: any[] = [];
    showFields = false;
    isDJS = false;
    isAppointmentNotRequired: boolean;
    parentSelection: boolean;

    constructor(private formBuilder: FormBuilder, private _authService: AuthService, private _store: DataStoreService,
        private _commonHttpService: CommonHttpService, private _alertService: AlertService,
        private _dropDownService: CommonDropdownsService, private _intakeConfig: IntakeConfigService) {
        this.store = this._store.getCurrentStore();
    }

    ngOnInit() {
        this.loadIntakeWorkers();
        this.isDJS = this._authService.isDJS();
        this.isIntakeInterView = false;
        this.isCustomInterviewTitle = false;
        // console.log(this._authService.getCurrentUser());
        this.currentUser = this._authService.getCurrentUser();
        this.initializeAppointmentForm();
        this.setFormValidity();
        this.InterviewList = [
            'Intake Interview',
            'Youth Orientation Program',
            'Other'
        ];
        this.times = this.generateTimeList(false);
        this.maxDate.setDate(new Date().getDate() + 10);

        this.appointments = this.store[IntakeStoreConstants.intakeappointment] ? this.store[IntakeStoreConstants.intakeappointment] : [];




        this.addedPersons = this.store[IntakeStoreConstants.addedPersons] ? this.store[IntakeStoreConstants.addedPersons] : [];
        // Need to validate if persons changed related with appointment
        this.addedPersons.forEach((person, index) => {
            if (!person.Pid) {
                person.Pid = AppConstants.PERSON.TEMP_ID + index;
            }
        });
        console.log(this.addedPersons);
        this.loadMapList();
        if (this.isDJS && this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT)) {
            const reasonforAppointment = this._store.getData(IntakeStoreConstants.reasonintakeinterview);
            if (reasonforAppointment) {
                this.intakeworker = reasonforAppointment.intakesecurityusersid;
                this.workerLocation = reasonforAppointment.userprofileaddressid;
                this.assignment = reasonforAppointment.reasonforassignmenttypekey;
                this.onIntakeWorkerChanged();
            }
            {
                const evaluation = this._store.getData(IntakeStoreConstants.evalFields);
                const allegation = { list: [] };
                if (evaluation && evaluation.length > 0) {
                    evaluation.forEach(data => {
                        if (data.allegedoffense && data.allegedoffense.length) {
                            allegation.list = [...allegation.list, ...data.allegedoffense];
                        }
                    });
                    this.loadReasonforAppointment(allegation.list);
                }
                // else {
                //     this.reason = [{reasonintakeinterview: 'Please Add Complaints'}];
                // }
            }
            this._dropDownService.getDropownsByTable('RFA').subscribe(data => {
                this.assignmentList = data;
            });
            setTimeout(() => {
                if (this.currentUser.role.name === SCREENING_WORKER) {
                    this.showFields = true;
                }
            }, 500);
        }
        if (this.isDJS) {
            this._dropDownService.getListByTableID('135').subscribe(result => {
                this.InterviewList = result.map(data => {
                    return data.description;
                });
            });
        }
    }

    ngAfterViewInit() {
        if (this.isDJS) {
            if (!this._intakeConfig.getiseditIntake()) {
                (<any>$(':button')).prop('disabled', true);
            }
        }
    }

    loadReasonforAppointment(offencelist) {
        const personId = this.addedPersons.find(data => data.Role === 'Youth');
        if (personId && personId.Pid) {
            if (!(personId && personId.Pid && personId.Pid.slice(0, 6) === 'tempid')) {
                this._commonHttpService.create(
                    {
                        offenselist: offencelist,
                        personid: personId.Pid
                    },
                    NewUrlConfig.EndPoint.Intake.ReasonforIntakeInterview
                ).subscribe((element) => {
                    const reason = (element.data && element.data.length > 0) ? element.data[0].reasonintakeinterviewdetails : [];
                    if (reason && reason.length > 0) {
                        this.reason = [...this.reason, ...reason.filter(data => ((data.isrequired === true || data.isrequired === 'true')))];
                        this.isAppointmentNotRequired = !!reason.find(data => (data.reasonintakeinterview === 'Person Date Of Death' && data.isrequired));
                        if (this.isAppointmentNotRequired) {
                            this.InterviewList = [
                                'Youth Orientation Program',
                                'Other'
                            ];
                        }
                        this.reason = this.reason.map(data => {
                            let description;
                            switch (data.reasonintakeinterview) {
                                case 'Placement':
                                    description = 'Likely in Placement past 25th day';
                                    break;
                                case 'Person Alert':
                                    description = 'Likely to be a Runaway past 25th day';
                                    break;
                                case 'Person Date Of Death':
                                    description = 'Deceased';
                                    break;
                                case 'Youth Status':
                                    description = 'Formal Supervision for Felony Allegation';
                                    break;
                            }
                            return { description: description };
                        });
                        console.log(this.reason);
                    }
                });
            }
        }
    }

    onIntakeWorkerChanged() {
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1, securityusersid: this.intakeworker },
                method: 'get',
                nolimit: true
            },
            NewUrlConfig.EndPoint.Intake.UserProfileAddress + '?filter'
        ).subscribe(data => {
            if (data && data.length > 0 && data.length === 1) {
                this.workerLocation = data[0].userprofileaddressid;
            }
            this.intakeWorkerLocationList = data;
        });
        if (this.currentUser.role.name === SCREENING_WORKER) {
            this.loadIntakeworkerBasicDetails();
        }
    }

    broadCastAppointmentUpdated() {
        this._store.setData(IntakeStoreConstants.intakeappointment, this.appointments);
        // this.appointmentInputSubject$.next(this.appointments);
    }

    private loadMapList() {
        Observable.forkJoin([this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true,
                order: 'description'
            },
            NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true
            },
            NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter'
        )]).map(([relations, roles]) => {

            this.relations = relations;
            this.roles = roles;
            // console.log(this.relations, this.roles);

        }).subscribe();
    }

    private initializeAppointmentForm() {
        this.appointmentForm = this.formBuilder.group({
            id: [''],
            title: [''],
            customtitle: [''],
            appointmentDate: [''],
            appointmentTime: ['08:00'],
            intakeWorkerId: [''],
            actors: [[]],
            notes: ['']
        });
    }

    private setFormValidity() {

        if (this.isEditAppointment && !this.isScreeningWorker()) {
            this.isNotesRequired = true;
        } else {
            this.isNotesRequired = false;
        }
    }

    private setDefaultValues() {
        if (this.currentUser.role.name === SCREENING_WORKER) {
            this.selectAppointmentType(DEFAULT_APPOINTMENT_TITLE);
            this.appointmentForm.patchValue({ title: DEFAULT_APPOINTMENT_TITLE });
            this.appointmentForm.patchValue({ title: DEFAULT_APPOINTMENT_TITLE, appointmentTime: '08:00' });
            this.appointmentForm.get('title').disable();
        }

    }

    private loadIntakeWorkers() {

        this._commonHttpService.getPagedArrayList(
            {

                method: 'get',
                nolimit: true,
                page: 1,
                limit: 50,
                order: 'username'
            },
            NewUrlConfig.EndPoint.Intake.intakeWorkerList + '?filter'
        ).map(response => {
            this.intakeWorkerList = response.data;
            return response.data;
        }).subscribe();


    }

    isDateTimeChanged(appointment) {
        if (appointment.appointmentDate === this.appointmentInAction.appointmentDate && appointment.appointmentTime === this.appointmentInAction.appointmentDate) {
            return false;
        }

        return true;
    }

    updateRescheduledAppointmentWithHistory(rescheduledAppointment) {
        const appointmentIndex = this.appointments.findIndex(appointment => rescheduledAppointment.id === appointment.id);
        const histories = [...this.appointmentInAction.history];
        this.appointmentInAction.history = [];
        histories.push(this.appointmentInAction);
        rescheduledAppointment.history = [...histories];
        this.appointments[appointmentIndex] = rescheduledAppointment;
        // console.log('updated', this.appointments);
    }

    updateScheduledAppointment(scheduledAppointment) {
        const appointmentIndex = this.appointments.findIndex(appointment => scheduledAppointment.id === appointment.id);
        this.appointments[appointmentIndex] = scheduledAppointment;
        // console.log('updated', this.appointments);
    }



    isPersonEligibleforAppointment(person) {
        if (person.Role === 'Youth') {
            return true;
        } else if (person.RelationshiptoRA === 'father' || person.RelationshiptoRA === 'mother' || person.RelationshiptoRA === 'guardian') {
            return true;
        }
        return false;
    }


    getAppointmentPerson(persons) {
        const appointmentPersons = [];
        const AppmntForm = this.appointmentForm.getRawValue();
        if (!AppmntForm.title) {
            return appointmentPersons;
        }
        persons.forEach(person => {
            const isEligible = this.isPersonEligibleforAppointment(person);
            if (isEligible && this.isIntakeInterView) {
                appointmentPersons.push(person);
            } else if (!this.isIntakeInterView) {
                appointmentPersons.push(person);
            }
        });
        return appointmentPersons;
    }

    selectAppointmentType(type) {
        if (type === 'Other') {
            this.isCustomInterviewTitle = true;
            this.appointmentForm.get('customtitle').setValidators([Validators.required]);
            this.appointmentForm.get('customtitle').updateValueAndValidity();
        } else {
            this.isCustomInterviewTitle = false;
            this.appointmentForm.get('customtitle').clearValidators();
            this.appointmentForm.get('customtitle').updateValueAndValidity();
        }

        if (type === 'Intake Interview') {
            this.isIntakeInterView = true;
        } else {
            this.isIntakeInterView = false;
        }
    }

    createOrUpdateAppointment() {
        if (this.appointmentForm.valid) {
            if (this.addedPersons && this.addedPersons.length) {
                const person = this.addedPersons.find(p => p.Role === 'Youth');
                if (person) {
                    const years = moment().diff(person.Dob, 'years', false);
                    if (+years < 18) {
                        this.parentSelection = true;
                    } else {
                        this.parentSelection = false;
                    }
                }
            }
            const appointmentForm = this.appointmentForm.getRawValue();
             if (!this.hasYouthAndParentOrGaurdian() && this.parentSelection) {
                this._alertService.error('Please Choose Parent / Guardian and Youth');
            } else {
                if (this.isEditAppointment) {
                    const editAppointmentObject = this.appointments.find(appointment => appointment.id === this.appointmentInAction.id);

                    if (this.isDateTimeChanged(editAppointmentObject) && !this.isScreeningWorker()) {
                        const rescheduledAppointment = this.createAppointmentObject(APPOINTMENT_RESCHEDULED);
                        rescheduledAppointment.id = this.appointmentInAction.id;
                        this.updateRescheduledAppointmentWithHistory(rescheduledAppointment);
                    } else {
                        const scheduledAppointment = this.createAppointmentObject(APPOINTMENT_SCHEDULED);
                        scheduledAppointment.id = this.appointmentInAction.id;
                        this.updateScheduledAppointment(scheduledAppointment);
                    }
                } else {
                    const newAppointment = this.createAppointmentObject(APPOINTMENT_SCHEDULED);
                    newAppointment.id = this.generateAppointmentID();
                    newAppointment.history = [];
                    this.appointments.push(newAppointment);
                }
                this.updateIntakeWorker(appointmentForm.intakeWorkerId);
                this.resetForm();

                this.broadCastAppointmentUpdated();
                (<any>$('#intake-appointment')).modal('hide');
           }
        } else {
            this._alertService.error('Please fill the required fields');
        }
    }

    createAppointmentObject(status) {

        const actors = this.createActors();
        const appointmentObject = new IntakeAppointment();
        const appointmentForm = this.appointmentForm.getRawValue();
        if (appointmentForm.title === 'Other' && appointmentForm.customtitle) {
            appointmentForm.title = appointmentForm.customtitle;
        }
        appointmentObject.title = appointmentForm.title;
        appointmentObject.status = status;
        appointmentObject.notes = appointmentForm.notes;
        appointmentObject.isChanged = false;
        appointmentObject.intakeWorkerId = appointmentForm.intakeWorkerId;
        appointmentObject.scheduledBy = this.currentUser.user.userprofile.fullname;

        const appointmentDate: any = new Date(appointmentForm.appointmentDate);
        const timeSplit = appointmentForm.appointmentTime.split(':');
        let appointmentTimeHour: number = Number(timeSplit[0]);
        const appointmentTimeMinPlusMeridiem = timeSplit[1];
        const appointmentTimeMinAndMeridiem = appointmentTimeMinPlusMeridiem.split(' ');
        const appointmentTimeMin = appointmentTimeMinAndMeridiem[0];
        const meridiem = appointmentTimeMinAndMeridiem[1];

        if (meridiem === 'PM') {
            appointmentTimeHour += 12;
        }

        appointmentDate.setHours(appointmentTimeHour);
        appointmentDate.setMinutes(appointmentTimeMin);

        appointmentObject.appointmentDate = appointmentDate;
        appointmentObject.appointmentTime = appointmentForm.appointmentTime;
        appointmentObject.actors = actors;
        return appointmentObject;
    }

    createActors() {
        return this.appointmentForm.value.actors.map(actor => {
            const person = this.getPerson(actor);
            return { 'actorid': actor, 'isoptional': false, firstName: person.Firstname, lastName: person.Lastname };
        });
    }

    isCreateAppointmentVisible() {

        if (!this.isPersonsAdded()) {
            return false;
        }
        if (!this.isScreeningWorker()) {
            return true;
        }
        if (this.isScreeningWorker() && this.appointments.length === 0) {
            return true;
        }

        // otherwise
        return false;

    }

    isAppointmentListVisible() {
        if (this.appointments) {
            return (this.appointments.length > 0);
        } else {
            return false;
        }


    }

    isPersonsAdded() {
        if (this.addedPersons) {
            return this.addedPersons.length > 0;
        }
        return false;
    }

    isScreeningWorker() {
        return (this.currentUser.role.name === SCREENING_WORKER);
    }

    getIntakeWorkerName(intakeWorkerID: string) {
        const intakeWorker = this.intakeWorkerList.find(iw => iw.userid === intakeWorkerID);
        if (intakeWorker) {
            return intakeWorker.username;
        }
        return null;
    }

    getPersonName(personId: string) {
        const person = this.addedPersons.find(p => p.Pid === personId);
        if (person) {
            return person.fullName;
        }
    }

    getPerson(personId: string) {
        const person = this.addedPersons.find(p => p.Pid === personId);
        if (person) {
            return person;
        }
        return null;
    }

    onNewAppointment() {
        if (this.currentUser.role.name === SCREENING_WORKER && this.isDJS &&
            this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT) &&
            !(this.intakeworker && this.workerLocation && this.assignment)) {
            this._alertService.warn('Please fill mandatory details');
            return false;
        }
        this.isEditAppointment = false;
        this.resetForm();
        this.resetActions();
        this.setFormValidity();
        this.isCustomInterviewTitle = false;
        this.isIntakeInterView = false;
        this.setDefaultValues();
        this.loadIntakeWorkers();
        this.chooseIwIfSelecte();
        if (this.currentUser.role.name === SCREENING_WORKER && this.isDJS &&
            this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT)) {
            this.loadIntakeworkerBasicDetails();
        } else {
            this.appointmentForm.get('intakeWorkerId').enable();
        }
        (<any>$('#intake-appointment')).modal('show');
    }

    loadIntakeworkerBasicDetails() {
        const reasonintakeinterview = {
            intakesecurityusersid: this.intakeworker,
            userprofileaddressid: this.workerLocation,
            reasonforassignmenttypekey: this.assignment
        };
        this._store.setData(IntakeStoreConstants.reasonintakeinterview, reasonintakeinterview);
        this.appointmentForm.patchValue({
            intakeWorkerId: this.intakeworker
        });
        if (this.appointments && this.appointments.length > 0) {
            this.appointments = this.appointments.map(data => {
                data.intakeWorkerId = this.intakeworker;
                return data;
            });
        }
        this.appointmentForm.get('intakeWorkerId').disable();
    }

    onEditAppointment(appointment) {
        this.actionText = 'Update';
        this.titleText = 'Edit';
        const checkTitle = this.InterviewList.find(data => data === appointment.title);
        if (!checkTitle) {
            appointment.customtitle = appointment.title;
            appointment.title = 'Other';
        }
        this.selectAppointmentType(appointment.title);
        this.isEditAppointment = true;
        this.appointmentInAction = appointment;
        this.appointmentForm.patchValue(appointment);
        setTimeout(() => {
            this.appointmentForm.patchValue({ 'actors': appointment.actors.map(actor => actor.actorid) });
        }, 1000);
        this.setFormValidity();
        this.loadIntakeWorkers();
        if (this.currentUser.role.name === SCREENING_WORKER && this.isDJS &&
            this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT)) {
            this.appointmentForm.get('intakeWorkerId').disable();
        } else {
            this.appointmentForm.get('intakeWorkerId').enable();
        }
    }

    resetForm() {
        this.appointmentForm.enable();
        this.appointmentForm.reset();
        this.appointmentForm.patchValue({ appointmentTime: '08:00' });
        this.isEditAppointment = false;
        this.isViewAppointment = false;
        this.isCustomInterviewTitle = false;
        this.isIntakeInterView = false;
    }

    resetActions() {
        this.actionText = 'Create';
        this.titleText = 'Create';
    }

    chooseIwIfSelecte() {
        if (this.appointments && this.appointments.length > 0) {
            this.appointmentForm.patchValue({ 'intakeWorkerId': this.appointments[0].intakeWorkerId });
            // setTimeout(() => this.appointmentForm.get('intakeWorkerId').disable(), 10);
        } else {
            this.appointmentForm.get('intakeWorkerId').enable();
        }
    }

    generateAppointmentID() {
        return new Date().getTime();
    }

    confirmDelete(appointment) {
        this.appointmentInAction = appointment;
    }

    deleteAppointmentConfirm() {
        this.appointments = this.appointments.filter(appointment => appointment.id !== this.appointmentInAction.id);
        this.broadCastAppointmentUpdated();
    }

    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = (tt % 60); // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }

    getPersonRole(person) {
        // typedescription,actortype
        const personRole = this.roles.find(role => role.actortype === person.Role);
        if (personRole) {
            console.log('personRole', personRole);
            return personRole.typedescription;
        }

    }

    getPersonRelation(person) {
        // relationshiptypekey -- description
        const personRelation = this.relations.find(relation => relation.relationshiptypekey === person.RelationshiptoRA);
        if (personRelation) {
            return personRelation.description;
        }
        // console.log(this.relations);
    }



    isCompleteVisible(appointment) {
        return (!this.isScreeningWorker() && appointment.status !== APPOINTMENT_COMPLETED);
    }
    isEditVisible(appointment) {
        return appointment.status !== APPOINTMENT_COMPLETED;
    }

    isHistoryVisible(appointment) {
        if (appointment.history) {
            return (appointment.history.length > 0);
        }
        return false;

    }

    isDeleteVisible(appointment) {
        return appointment.status !== APPOINTMENT_COMPLETED;
    }

    onCompleteAppointment(appointment) {
        this.completionNotes = '';
        this.appointmentInAction = appointment;
    }

    openAppointmentComments(appointment) {
        this.completionNotes = appointment.notes;
        this.readOnly = true;
    }

    appointmentCompleteConfirm() {
        if (this.appointmentInAction) {
            const appointmentIndex = this.appointments.findIndex(appointment => this.appointmentInAction.id === appointment.id);
            if (this.completionNotes !== '') {
                this.appointments[appointmentIndex].notes = this.completionNotes;
                this.appointments[appointmentIndex].status = APPOINTMENT_COMPLETED;
                this.broadCastAppointmentUpdated();
                (<any>$('#complete-appointment-popup')).modal('hide');
            } else {
                this._alertService.error('Completion notes is required');
            }
        }


    }

    updateIntakeWorker(intakeWorkerId) {
        this.appointments.forEach(appointment => {
            appointment.intakeWorkerId = intakeWorkerId;
        });
        this._store.setData(IntakeStoreConstants.assingedIntakeWokerId, intakeWorkerId);
    }

    showHistory(appointment: IntakeAppointment) {
        // console.log(appointment);
        this.appointmentHistoryObj = appointment.history;
    }

    viewAppointment(appointment) {
        this.appointmentInAction = appointment;
        (<any>$('#appointment-history-pu')).modal('hide');
        this.titleText = 'View';
        this.isViewAppointment = true;
        this.appointmentForm.patchValue(appointment);
        this.appointmentForm.patchValue({ 'actors': appointment.actors.map(actor => actor.actorid) });
        this.appointmentForm.disable();
        this.setFormValidity();
        (<any>$('#intake-appointment')).modal('show');

    }

    openHistory() {
        this.resetForm();
        (<any>$('#intake-appointment')).modal('hide');
        (<any>$('#appointment-history-pu')).modal('show');

    }

    hasYouthAndParentOrGaurdian() {
        return this.hasYouth() && this.hasParentOrGaurdian();
    }
    hasYouth() {
        return this.hasPersonObject('Role', AppConstants.INVOLVED_PERSON_ROLE.YOUTH);
    }

    hasParentOrGaurdian() {
        const father = this.hasPersonObject('RelationshiptoRA', AppConstants.INVOLVED_PERSON_ROLE.FATHER);
        const mother = this.hasPersonObject('RelationshiptoRA', AppConstants.INVOLVED_PERSON_ROLE.MOTHER);
        const guardian = this.hasPersonObject('RelationshiptoRA', AppConstants.INVOLVED_PERSON_ROLE.GUARDIAN);
        return father || mother || guardian;
    }
    hasPersonObject(propertyKey: string, role: string) {
        const persons = this.getSelectedPersons();
        let isRoleFound = false;
        if (persons) {
            const involvedperson = persons.find(person => person[propertyKey] && person[propertyKey].toLowerCase() === role.toLowerCase());
            if (involvedperson) {
                isRoleFound = true;
            } else {
                isRoleFound = false;
            }
        }
        return isRoleFound;

    }
    getSelectedPersons() {
        const appointmentForm = this.appointmentForm.getRawValue();
        const selectedActors = appointmentForm.actors;
        if (selectedActors) {
            return selectedActors.map(actorid => this.getPerson(actorid));
        }
        return null;
    }


}
