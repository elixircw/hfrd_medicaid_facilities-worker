import 'trumbowyg/dist/trumbowyg.min.js';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatAutocompleteModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PaginationModule } from 'ngx-bootstrap';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';
import { PopoverModule } from 'ngx-popover';
import { QuillModule } from 'ngx-quill';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { TrumbowygNgxModule } from 'trumbowyg-ngx';

import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SortTableModule } from '../../../shared/modules/sortable-table/sortable-table.module';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { MyNewintakeRoutingModule } from './my-newintake-routing.module';
import { MyNewintakeComponent } from './my-newintake.component';
import { IntakeDocumentCreatorModule } from './intake-document-creator/intake-document-creator.module';
import { NoticePreintakeLetterComponent } from './intake-document-creator/notice-preintake-letter/notice-preintake-letter.component';
import { AcknowledgementLetterComponent } from './intake-document-creator/acknowledgement-letter/acknowledgement-letter.component';
import { MyNewintakeResolverService } from './my-newintake-resolver.service';
import { CommunicationResolverService } from './communication-resolver.service';
import { PurposeResolverService } from './purpose-resolver.service';
import { IntakeConfigService } from './intake-config.service';
import { PersonResolverService } from './person-resolver.service';
import { InvolvedPersonsModule } from '../../shared-pages/involved-persons/involved-persons.module';
import { RelationshipModule } from '../../case-worker/dsds-action/relationship/relationship.module';
import { RelationshipNewModule } from '../../shared-pages/relationship-new/relationship-new.module';
import { RecordingModule } from '../../case-worker/dsds-action/recording/recording.module';
import { DsdsService } from '../../case-worker/dsds-action/_services/dsds.service';

// tslint:disable-next-line:max-line-length
@NgModule({
    imports: [
        CommonModule,
        SharedDirectivesModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatRadioModule,
        MatTabsModule,
        MatTooltipModule,
        MatCheckboxModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatChipsModule,
        MatIconModule,
        MyNewintakeRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        TimepickerModule,
        ControlMessagesModule,
        SharedDirectivesModule,
        SharedPipesModule,
        NgSelectModule,
        ImageCropperModule,
        SortTableModule,
        RelationshipModule,
        RelationshipNewModule,
        RecordingModule,
        MatAutocompleteModule,
        NgxMaskModule.forRoot(),
        TrumbowygNgxModule.withConfig({
            svgPath: '../../../../assets/images/icons.svg'
        }),
        A2Edatetimepicker,
        NgxMaskModule.forRoot(),
        PopoverModule,
        NgxfUploaderModule.forRoot(),
        QuillModule,
        PdfViewerModule,
        IntakeDocumentCreatorModule,
        InvolvedPersonsModule
    ],
    declarations: [
        MyNewintakeComponent,
        NoticePreintakeLetterComponent,
        AcknowledgementLetterComponent
    ],
    exports: [MyNewintakeComponent],
    providers: [SpeechRecognitionService, SpeechRecognizerService, NgxfUploaderService, MyNewintakeResolverService, IntakeConfigService, CommunicationResolverService, PurposeResolverService, PersonResolverService,DsdsService]
})
export class MyNewintakeModule { }
