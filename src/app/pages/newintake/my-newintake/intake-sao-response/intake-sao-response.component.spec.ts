import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeSaoResponseComponent } from './intake-sao-response.component';

describe('IntakeSaoResponseComponent', () => {
  let component: IntakeSaoResponseComponent;
  let fixture: ComponentFixture<IntakeSaoResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeSaoResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeSaoResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
