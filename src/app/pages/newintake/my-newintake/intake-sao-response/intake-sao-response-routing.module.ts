import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeSaoResponseComponent } from './intake-sao-response.component';

const routes: Routes = [
  {
      path: '',
      component: IntakeSaoResponseComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeSaoResponseRoutingModule { }
