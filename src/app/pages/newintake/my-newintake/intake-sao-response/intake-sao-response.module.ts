import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeSaoResponseRoutingModule } from './intake-sao-response-routing.module';
import { IntakeSaoResponseComponent } from './intake-sao-response.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeSaoResponseRoutingModule,
    FormMaterialModule
  ],
  declarations: [IntakeSaoResponseComponent]
})
export class IntakeSaoResponseModule { }
