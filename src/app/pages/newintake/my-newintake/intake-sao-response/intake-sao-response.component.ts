import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataStoreService, AlertService, CommonHttpService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { MyNewintakeConstants } from '../my-newintake.constants';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { NewUrlConfig } from '../../newintake-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'intake-sao-response',
  templateUrl: './intake-sao-response.component.html',
  styleUrls: ['./intake-sao-response.component.scss']
})
export class IntakeSaoResponseComponent implements OnInit {

  saoResponseForm: FormGroup;
  SAOStatuses = [];
  SAOConditions = [];
  saoStatus$: Observable<any[]>;
  saoConditions$: Observable<any[]>;
  currentDate = new Date();
  signedOffDate;
  constructor(private formBuilder: FormBuilder,
    private _dropDownService: CommonHttpService,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.buildForms();
    this.listenFormChanges();
    this.initialzeValues();
    this.loadSAOConditions();
    this.loadSAOStatuses();
  }
  buildForms() {

    this.saoResponseForm = this.formBuilder.group({
      saoresponsedate: ['', Validators.required],
      saoresponsestatustypekey: ['', Validators.required],
      saoresponseconditiontypekey: ['', Validators.required]
    });
  }

  listenFormChanges() {
    this.saoResponseForm.valueChanges.subscribe((data) => {
      this._dataStoreService.setData(MyNewintakeConstants.Intake.saoResponse, data);
    });
  }

  initialzeValues() {
    const saoResponseData = this._dataStoreService.getData(MyNewintakeConstants.Intake.saoResponse);
    if (saoResponseData) {
      this.saoResponseForm.patchValue(this._dataStoreService.getData(MyNewintakeConstants.Intake.saoResponse));
    }
    const intakeModel = this._dataStoreService.getData(MyNewintakeConstants.Intake.intakeModel);
    this.signedOffDate = intakeModel.signedOffDate;
  }

  loadSAOConditions() {
    const conditionType = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.saoresponseconditiontype)
      .map(condition => {
        return {
          condition: condition.map(res =>
            new DropdownModel({
              text: res.description,
              value: res.saoresponseconditiontypekey
            }))
        };
      }).share();

    this.saoConditions$ = conditionType.pluck('condition');
   }

  loadSAOStatuses() {

    const statusType = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.saoresponsestatustype)
      .map(status => {
        return {
          status: status.map(res =>
            new DropdownModel({
              text: res.description,
              value: res.saoresponsestatustypekey
            }))
        };
      }).share();

    this.saoStatus$ = statusType.pluck('status');
 }

}
