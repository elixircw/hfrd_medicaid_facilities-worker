import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeSocialHistoryInrRoutingModule } from './intake-social-history-inr-routing.module';
import { IntakeSocialHistoryInrComponent } from './intake-social-history-inr.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeSocialHistoryInrRoutingModule,
    FormMaterialModule
  ],
  declarations: [IntakeSocialHistoryInrComponent]
})
export class IntakeSocialHistoryInrModule { }
