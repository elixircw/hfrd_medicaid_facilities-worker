import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirOffenceCircumstancesComponent } from './shir-offence-circumstances.component';

describe('ShirOffenceCircumstancesComponent', () => {
  let component: ShirOffenceCircumstancesComponent;
  let fixture: ComponentFixture<ShirOffenceCircumstancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirOffenceCircumstancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirOffenceCircumstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
