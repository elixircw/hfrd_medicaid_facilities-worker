import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataStoreService } from '../../../../../@core/services';
import { InvolvedPerson, School } from '../../_entities/newintakeModel';
import { Work } from '../../_entities/newintakeModel';
import { MyNewintakeConstants } from '../../my-newintake.constants';

@Component({
  selector: 'shir-education-employment',
  templateUrl: './shir-education-employment.component.html',
  styleUrls: ['./shir-education-employment.component.scss']
})
export class ShirEducationEmploymentComponent implements OnInit {
  eduWorkForm: FormGroup;
  persons: InvolvedPerson[];
  youth: InvolvedPerson;
  work: Work = new Work();
  lastSchool: School;
  constructor(private _formBuilder: FormBuilder, private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this._dataStoreService.currentStore.subscribe(data => {
      this.extractData(data);
    });
  }

  extractData(intake): void {
    if (intake) {
      this.persons = intake[MyNewintakeConstants.Intake.addedPersons];
      this.youth = this.getPerson('Youth');

      if (this.youth) {
        if (this.youth.school && this.youth.school.length > 0) {
          this.lastSchool = this.youth.school[this.youth.school.length - 1];
        }
      }

      this.work = intake[MyNewintakeConstants.Intake.PersonsInvolved.Work.Work];

      this.initializeForm();
    }
  }

  initializeForm() {
    this.eduWorkForm = this._formBuilder.group({
      school: this.lastSchool ? this.lastSchool.educationname : '',
      grade: this.lastSchool ? this.lastSchool.lastgradetypekey : '',
      gradeFailed: '',
      otherSchools: '',
      specialEducation: this.lastSchool ? this.lastSchool.schoolTypeDescription : '',
      summaryOfCurrentGrades: '',
      summaryDisciplinary: '',
      attendance: '',
      historyOfEmployment: '',
      employmentAndHourDetails: '',
      currentlyEmployed: '',
      summary: '',
      careerGoals: ''
    });
  }

  getPerson(Role: string): InvolvedPerson {
    if (this.persons) {
      return this.persons.find((person) => person.Role === Role);
    }
    return null;
  }

}
