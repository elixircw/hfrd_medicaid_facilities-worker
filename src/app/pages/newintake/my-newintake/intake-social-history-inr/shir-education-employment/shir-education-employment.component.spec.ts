import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirEducationEmploymentComponent } from './shir-education-employment.component';

describe('ShirEducationEmploymentComponent', () => {
  let component: ShirEducationEmploymentComponent;
  let fixture: ComponentFixture<ShirEducationEmploymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirEducationEmploymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirEducationEmploymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
