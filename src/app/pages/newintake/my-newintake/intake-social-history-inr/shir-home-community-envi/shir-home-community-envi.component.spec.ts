import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirHomeCommunityEnviComponent } from './shir-home-community-envi.component';

describe('ShirHomeCommunityEnviComponent', () => {
  let component: ShirHomeCommunityEnviComponent;
  let fixture: ComponentFixture<ShirHomeCommunityEnviComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirHomeCommunityEnviComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirHomeCommunityEnviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
