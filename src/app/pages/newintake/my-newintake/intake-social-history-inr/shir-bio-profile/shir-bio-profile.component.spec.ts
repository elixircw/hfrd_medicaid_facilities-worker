import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirBioProfileComponent } from './shir-bio-profile.component';

describe('ShirBioProfileComponent', () => {
  let component: ShirBioProfileComponent;
  let fixture: ComponentFixture<ShirBioProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirBioProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirBioProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
