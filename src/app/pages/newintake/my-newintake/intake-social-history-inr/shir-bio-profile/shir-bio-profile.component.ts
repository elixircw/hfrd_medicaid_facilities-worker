import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataStoreService } from '../../../../../@core/services';
import { InvolvedPerson } from '../../_entities/newintakeModel';
import { MyNewintakeConstants } from '../../my-newintake.constants';

@Component({
  selector: 'shir-bio-profile',
  templateUrl: './shir-bio-profile.component.html',
  styleUrls: ['./shir-bio-profile.component.scss']
})
export class ShirBioProfileComponent implements OnInit {
  bioForm: FormGroup;
  persons: InvolvedPerson[];
  youth: InvolvedPerson;
  parents: string;
  constructor(private _formBuilder: FormBuilder, private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this._dataStoreService.currentStore.subscribe(data => {
      this.extractData(data);
    });
  }

  extractData(intake): void {
    this.persons = intake[MyNewintakeConstants.Intake.addedPersons];
    this.youth = this.getPerson('Youth');
    this.setParents();

    this.initializeForm();
  }

  initializeForm() {
    this.bioForm = this._formBuilder.group({
      youthName: this.youth ? this.youth.fullName : '',
      date: this.youth ? this.youth.Dob : '',
      sex: this.youth ? this.youth.Gender : '',
      race: this.youth ? this.youth.raceDescription : '',
      weight: this.youth ? this.youth.weight : '',
      hair: this.youth ? this.youth : '',
      complexion: this.youth ? this.youth : '',
      religiousAffiliation: this.youth ? this.youth.religionkey : '',
      bioParents: this.parents,
      placeOfBIrth: '',
      delivery: '',
      height: this.youth ? this.youth.height : '',
      eyes: this.youth ? this.youth : '',
      identifyingFeatures: this.youth ? this.youth.physicalMark : '',
      teenParent: ''
    });
  }

  getPerson(Role: string): InvolvedPerson {
    if (this.persons) {
      return this.persons.find((person) => person.Role === Role);
    }
    return null;
  }

  getPersonByRelation(Relationship: string): InvolvedPerson {
    if (this.persons) {
      return this.persons.find((person) => person.RelationshiptoRA === Relationship);
    }
    return null;
  }

  setParents() {
    if (this.persons) {
      const father = this.getPersonByRelation('father');
      const mother = this.getPersonByRelation('mother');

      const parenstArr: string[] = [];
      if (father) {
        parenstArr.push(father.fullName);
      }

      if (mother) {
        parenstArr.push(mother.fullName);
      }

      this.parents = parenstArr.toString();
    }
  }

}
