import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { InvolvedPerson } from '../../_entities/newintakeModel';
import { DataStoreService } from '../../../../../@core/services';
import { MyNewintakeConstants } from '../../my-newintake.constants';
import { CourtDetails, PetitionDetails } from '../../_entities/newintakeSaveModel';

@Component({
  selector: 'shir-info',
  templateUrl: './shir-info.component.html',
  styleUrls: ['./shir-info.component.scss']
})
export class ShirInfoComponent implements OnInit {
  basicInfoForm: FormGroup;
  persons: InvolvedPerson[];
  youth: InvolvedPerson;
  courtDetails: CourtDetails;
  petitionDetails: PetitionDetails;
  constructor(private _formBuilder: FormBuilder, private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this._dataStoreService.currentStore.subscribe(data => {
      this.extractData(data);
    });
  }

  extractData(intake): void {
    this.persons = intake[MyNewintakeConstants.Intake.addedPersons];
    this.youth = this.getPerson('Youth');

    this.courtDetails = intake[MyNewintakeConstants.Intake.courtDetails];
    this.petitionDetails = intake[MyNewintakeConstants.Intake.petitionDetails];

    this.initializeForm();
  }

  initializeForm() {
    this.basicInfoForm = this._formBuilder.group({
      circuitCourtTo: '',
      circuitCourtFrom: '',
      date: [{ value: new Date(), disabled: true }],
      youthDOB: [{ value: this.youth ? this.youth.Dob : '', disabled: this.youth ? this.youth.Dob : false }],
      re: '',
      pertionID: [{ value: this.petitionDetails ? this.petitionDetails.petitionid : '', disabled: this.petitionDetails ? this.petitionDetails.petitionid : false }],
      courtDate: [{ value: this.courtDetails ? this.courtDetails.hearingdatetime : '', disabled: this.courtDetails ? this.courtDetails.hearingdatetime : false }],
      typeOfReport: '',
      youthName: [{ value: this.youth ? this.youth.fullName : '', disabled: this.youth ? this.youth.fullName : false }],
      address: [{ value: this.youth ? this.youth.fullAddress : '', disabled: this.youth ? this.youth.fullAddress : false }],
      telephone: [{
        value: (this.youth && this.youth.phoneNumber && this.youth.phoneNumber.length > 0) ? this.youth.phoneNumber[0].contactnumber : '',
        disabled: (this.youth && this.youth.phoneNumber && this.youth.phoneNumber.length > 0) ? this.youth.phoneNumber[0].contactnumber : false
      }],
      dob: [{ value: this.youth ? this.youth.Dob : '', disabled: this.youth ? this.youth.Dob : false }],
      driverLicense: [{ value: this.youth ? this.youth.stateid : '', disabled: this.youth ? this.youth.stateid : false }],
      cjamsID: '',
      currentOffense: '',
      petitionNumber: [{ value: this.petitionDetails ? this.petitionDetails.petitionid : '', disabled: this.petitionDetails ? this.petitionDetails.petitionid : false }],
      adjudicatedOffense: [{ value: this.courtDetails ? this.courtDetails.allegationid : '', disabled: this.courtDetails ? this.courtDetails.allegationid : false }],
      dispositionDate: [{ value: this.courtDetails ? this.courtDetails.adjudicationdatetime : '', disabled: this.courtDetails ? this.courtDetails.adjudicationdatetime : false }]
    });
  }

  getPerson(Role: string): InvolvedPerson {
    if (this.persons) {
      return this.persons.find((person) => person.Role === Role);
    }
    return null;
  }

}
