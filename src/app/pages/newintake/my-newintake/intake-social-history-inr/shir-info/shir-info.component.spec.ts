import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirInfoComponent } from './shir-info.component';

describe('ShirInfoComponent', () => {
  let component: ShirInfoComponent;
  let fixture: ComponentFixture<ShirInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
