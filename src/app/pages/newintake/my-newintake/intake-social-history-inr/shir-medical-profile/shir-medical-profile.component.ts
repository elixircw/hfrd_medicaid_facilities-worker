import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  Health, Physician, BehaviouralHealthInfo, SubstanceAbuse, InvolvedPerson,
  HealthInsuranceInformation, Medication, HealthExamination, MedicalConditions, HistoryofAbuse
} from '../../_entities/newintakeModel';
import { DataStoreService } from '../../../../../@core/services';
import { MyNewintakeConstants } from '../../my-newintake.constants';

@Component({
  selector: 'shir-medical-profile',
  templateUrl: './shir-medical-profile.component.html',
  styleUrls: ['./shir-medical-profile.component.scss']
})
export class ShirMedicalProfileComponent implements OnInit {
  medicalForm: FormGroup;
  healthProfile: Health;
  physician: Physician[];
  behaviouralhealthinfo: BehaviouralHealthInfo[];
  healthInsurance: HealthInsuranceInformation[];
  medication: Medication[];
  healthExamination: HealthExamination[];
  medicalcondition: MedicalConditions[];
  history: HistoryofAbuse;
  substanceAbuse: SubstanceAbuse;

  constructor(private _formBuilder: FormBuilder, private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this._dataStoreService.currentStore.subscribe(data => {
      this.extractData(data);
    });
  }

  extractData(intake): void {
    const addedPersons = intake[MyNewintakeConstants.Intake.addedPersons];
    const youth: InvolvedPerson = addedPersons ? addedPersons.find((person) => person.Role === 'Youth') : undefined;

    if (youth) {
      this.physician = youth.physicianinfo;
      this.behaviouralhealthinfo = youth.personbehavioralhealth;
      this.healthInsurance = youth.healthinsurance;
      this.medication = youth.personmedicationphyscotropic;
      this.healthExamination = youth.personhealthexam;
      this.medicalcondition = youth.personmedicalcondition;
      this.history = youth.personabusehistory;
      this.substanceAbuse = youth.personabusesubstance;
    }

    this.initializeForm();
  }

  initializeForm() {
    this.medicalForm = this._formBuilder.group({
      healthConditions: '',
      healthConditionComments: '',
      primaryPhysicianFirstName: (this.physician && this.physician.length > 0) ? this.physician[0].name : '',
      primaryPhysicianLastName: (this.physician && this.physician.length > 0) ? this.physician[0].name : '',
      primaryPhysicianAddressLine1: (this.physician && this.physician.length > 0) ? this.physician[0].address1 : '',
      primaryPhysicianAddressLine2: (this.physician && this.physician.length > 0) ? this.physician[0].address2 : '',
      primaryPhysicianState: (this.physician && this.physician.length > 0) ? this.physician[0].state : '',
      primaryPhysicianCounty: (this.physician && this.physician.length > 0) ? this.physician[0].county : '',
      primaryPhysicianZipcode: (this.physician && this.physician.length > 0) ? this.physician[0].zip : '',
      primaryPhysicianTelephoneNumber: (this.physician && this.physician.length > 0) ? this.physician[0].phone : '',
      healthInsuranceAndNumber: (this.healthInsurance && this.healthInsurance.length > 0) ? this.healthInsurance[0].medicalinsuranceprovider + ' ' + this.healthInsurance[0].policyname : '',
      currentMedications: '',
      evaluationAssesments: '',
      behavioralHealthFirstName: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].clinicianname : '',
      behavioralHealthLastName: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].clinicianname : '',
      behavioralHealthAddressLine1: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].address1 : '',
      behavioralHealthAddressLine2: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].address2 : '',
      behavioralHealthState: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].state : '',
      behavioralHealthCounty: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].county : '',
      behavioralHealthZipcode: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].zip : '',
      behavioralHealthTelephoneNumber: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].phone : '',
      currentDiagnoses: (this.behaviouralhealthinfo && this.behaviouralhealthinfo.length > 0) ? this.behaviouralhealthinfo[0].currentdiagnoses : '',
      documents: '',
      neglect: this.history ? this.history.isneglect : '',
      physicalAbuse: this.history ? this.history.isphysicalabuse : '',
      emotionalAbuse: this.history ? this.history.isemotionalabuse : '',
      sexualAbuse: this.history ? this.history.issexualabuse : '',
      summary: this.history ? this.history.neglectnotes + '\n' + this.history.physicalnotes + '\n' + this.history.emotionalnotes + '\n' + this.history.sexualnotes : '',
      substanceAbuseHistory: '',
      tobaccoUse: this.substanceAbuse ? this.substanceAbuse.isusetobacco : '',
      currentDrugOrAlcoholUse: this.substanceAbuse ? this.substanceAbuse.isusedrugoralcohol : '',
      drugs: this.substanceAbuse ? this.substanceAbuse.isusedrug : '',
      drugTypeOfDrug: '',
      drugFrequency: this.substanceAbuse ? this.substanceAbuse.drugfrequencydetails : '',
      drugAgeAtFirstUse: this.substanceAbuse ? this.substanceAbuse.drugageatfirstuse : '',
      alcohol: this.substanceAbuse ? this.substanceAbuse.isusealcohol : '',
      alcoholTypeOfDrug: '',
      alcoholAgeAtFirstUse: this.substanceAbuse ? this.substanceAbuse.alcoholageatfirstuse : '',
      familyAlcoholOrDrugUse: ''
    });
  }
}
