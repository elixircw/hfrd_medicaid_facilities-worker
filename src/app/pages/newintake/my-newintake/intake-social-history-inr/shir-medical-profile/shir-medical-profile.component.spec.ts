import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirMedicalProfileComponent } from './shir-medical-profile.component';

describe('ShirMedicalProfileComponent', () => {
  let component: ShirMedicalProfileComponent;
  let fixture: ComponentFixture<ShirMedicalProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirMedicalProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirMedicalProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
