import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirPpiRelationshipComponent } from './shir-ppi-relationship.component';

describe('ShirPpiRelationshipComponent', () => {
  let component: ShirPpiRelationshipComponent;
  let fixture: ComponentFixture<ShirPpiRelationshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirPpiRelationshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirPpiRelationshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
