import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakePlacementAddEditComponent } from './intake-placement-add-edit.component';

describe('IntakePlacementAddEditComponent', () => {
  let component: IntakePlacementAddEditComponent;
  let fixture: ComponentFixture<IntakePlacementAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakePlacementAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakePlacementAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
