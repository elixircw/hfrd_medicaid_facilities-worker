import { Component, OnInit } from '@angular/core';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, AuthService, DataStoreService } from '../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DSDS_STORE_CONSTANTS } from '../../../../case-worker/dsds-action/dsds-action.constants';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import * as moment from 'moment';
import { IntakeStoreConstants } from '../../my-newintake.constants';

@Component({
  selector: 'intake-placement-add-edit',
  templateUrl: './intake-placement-add-edit.component.html',
  styleUrls: ['./intake-placement-add-edit.component.scss']
})
export class IntakePlacementAddEditComponent implements OnInit {
  id: string;
  userInfo: AppUser;
  caseTypes$: Observable<DropdownModel>;
  categoryTypes$: Observable<DropdownModel>;
  categorySubTypes$: Observable<DropdownModel[]>;
  paymentTypes$: Observable<DropdownModel>;
  exitTypes$: Observable<DropdownModel>;
  courtApproved$: Observable<DropdownModel>;
  admissionClassification$: Observable<DropdownModel>;
  admissionType$: Observable<DropdownModel>;
  admissionAuthorization$: Observable<DropdownModel>;
  admissionReason$: Observable<DropdownModel>;
  addPlacementForm: FormGroup;
  reportMode: string;
  showCop = false;
  countyDropdown$: Observable<DropdownModel[]>;
  providerId: string;
  placementAdd: any;
  provider: any;
  placement: any;
  placementid: string;
  isTempPlacement: boolean;
  maxDate = new Date();
  admissionAuthorizationList = [];
  constructor(
    private _httpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _authService: AuthService,
    private router: Router,
    private _dataStoreService: DataStoreService
  ) { }

  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    this.provider = this._dataStoreService.getData('SELECTED_PROVIDER');
    this.placement = this._dataStoreService.getData(IntakeStoreConstants.addedPlacement);
    this.addPlacementForm = this._formBuilder.group({
      providername: new FormControl({ value: null, disabled: true }),
      providerid: new FormControl({ value: null, disabled: true }),
      providerunit: new FormControl({ value: null, disabled: true }),
      placementadmissionclassificationkey: new FormControl({ value: null, disabled: true }),
      PlacementAdmissionTypekey: new FormControl({ value: null, disabled: true }),
      parentorg: new FormControl({ value: null, disabled: true }),
      addate: [null],
      adtime: [null],
      releasedate: [null, Validators.required],
      detainer: [null],
      PlacementAdmissionAuthorizationTypekey: [null],
      PlacementPrimaryAdmissionReasonTypekey: [null],
      PlacementPrimaryApprovedAltTypekey: [null],
      county: [null],
      jlocation: new FormControl({ value: null, disabled: true }),
      jcounty: new FormControl({ value: null, disabled: true }),
      fieldworker: new FormControl({ value: null, disabled: true }),
      cop: [null],
      certifiedAd: [null],
      resourceworker: [null],
      effectivedatetime: [{ value: null, disabled: true }]
    });
    this.reportMode = this.route.snapshot.paramMap.get('operation');
    if (this.reportMode === 'update' && this.placement && this.placement.provider) {
      this.provider = this.placement.provider;
      this.addPlacementForm.patchValue(this.placement);
      this.addPlacementForm.patchValue(this.provider);
      this.addPlacementForm.patchValue({
        placementadmissionclassificationkey: this.provider.placementadmissionclassificationdesc,
        PlacementAdmissionTypekey: this.provider.placementadmissiontypedesc,
        releasedate: moment(this.placement.releasedate).format(),
        addate: moment(this.placement.addate).format(),
      });
      this.showCop = this.placement.cop;
      this.addPlacementForm.get('certifiedAd').disable();
      this.addPlacementForm.get('resourceworker').disable();
      this.addPlacementForm.get('cop').disable();
      this.getCountyDropdown();
      this.loadDropdown();
      //   });
    } else if (this.reportMode === 'add' && this.provider) {
      this.providerId = this.provider.providerid;
      this.provider.providerid = this.provider.providercode;
      this.placementid = this.provider.placementid;
      this.addPlacementForm.patchValue(this.provider);
      this.addPlacementForm.patchValue({
        placementadmissionclassificationkey: this.provider.placementadmissionclassificationdesc,
        PlacementAdmissionTypekey: this.provider.placementadmissiontypedesc
      });
      this.getCountyDropdown();
      this.loadDropdown();
    } else {
      this.router.navigate(['../list'], { relativeTo: this.route });
    }
  }

  countyPatchValue(event) {
    if (event) {
      this.countyDropdown$.subscribe((data) => {
        data.map((list) => {
          if (list.value === event.value) {
            this.addPlacementForm.patchValue({
              jlocation: 'Head Quarters',
              jcounty: list.text
            });
          }
        });
      });
    }
  }

  loadDropdown() {
    const source = Observable.forkJoin([this.getCourtApproved(), this.getAdmissionAuthorization(), this.getPrimaryAdmissionReason()])
      .map((item: any) => {
        return {
          courtApproved: item[0].map((res) => {
            return new DropdownModel({
              text: res.description,
              value: res.placementprimaryapprovedalttypekey
            });
          }),
          admissionAuthorization: item[1].map((itm) => {
            return new DropdownModel({
              text: itm.description,
              value: itm.placementadmissionauthorizationtypekey
            });
          }),
          admissionReason: item[2].map((itm) => {
            return new DropdownModel({
              text: itm.description,
              value: itm.placementprimaryadmissionreasontypekey
            });
          })
        };
      })
      .share();
    this.courtApproved$ = source.pluck('courtApproved');
    // this.admissionClassification$ = source.pluck('admissionClassification');
    // this.admissionType$ = source.pluck('admissionType');
    this.admissionAuthorization$ = source.pluck('admissionAuthorization');
    this.admissionAuthorization$.subscribe((res: any) => {
      this.admissionAuthorizationList = res;
    })
    this.admissionReason$ = source.pluck('admissionReason');
  }

  showCOP(event) {
    console.log(event.checked);
    if (event.checked) {
      this.showCop = true;
      this.addPlacementForm.get('certifiedAd').setValidators([Validators.required]);
      this.addPlacementForm.get('resourceworker').setValidators([Validators.required]);
      this.addPlacementForm.updateValueAndValidity();
    } else {
      this.showCop = false;
      this.addPlacementForm.get('certifiedAd').clearValidators();
      this.addPlacementForm.get('resourceworker').clearValidators();
      this.addPlacementForm.updateValueAndValidity();
      this.addPlacementForm.enable();
      this.addPlacementForm.get('providername').disable();
      this.addPlacementForm.get('providerid').disable();
      this.addPlacementForm.get('providerunit').disable();
      this.addPlacementForm.get('placementadmissionclassificationkey').disable();
      this.addPlacementForm.get('PlacementAdmissionTypekey').disable();
      this.addPlacementForm.get('jlocation').disable();
      this.addPlacementForm.get('jcounty').disable();
      this.addPlacementForm.get('fieldworker').disable();
    }
  }

  getCountyDropdown() {
    this.countyDropdown$ = this._httpService
      .getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmCountyListUrl + '?filter'
      )
      .map((result) => {
        return result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyid
            })
        );
      });
  }

  private getCourtApproved() {
    return this._httpService.getAll('placement/primaryapproved');
  }

  private getAdmissionAuthorization() {
    return this._httpService.getAll('placement/admissionauthorization');
  }

  private getPrimaryAdmissionReason() {
    return this._httpService.getAll('placement/admissionreason');
  }

  placementSearchAdd(modal) {
    modal.providerid = this.providerId;
    modal.remarks = 'djs placement';
    modal.addate = moment(modal.addate).format('MM/DD/YYYY').toString();
    if (moment.isMoment(modal.adtime)) {
      modal.adtime = modal.adtime.format('HH:mm').toString();
    }
    modal.releasedate = moment(modal.releasedate).format('MM/DD/YYYY').toString();
    const authorizationList = this.admissionAuthorizationList.find(item => item.value === modal.PlacementAdmissionAuthorizationTypekey);
    modal.placementadmissionauthorizationtypedesc = authorizationList ? authorizationList.text : null;
    modal.placementadmissionclassificationtypedesc = this.provider.placementadmissionclassificationdesc;
    modal.placementadmissiontypedesc = this.provider.placementadmissiontypedesc;
    modal.placementadmissionclassificationkey = this.provider.placementadmissionclassificationkey;
    modal.PlacementAdmissionTypekey = this.provider.PlacementAdmissionTypekey;
    modal.placementadmissiontypekey = this.provider.PlacementAdmissionTypekey;
    modal.istempplacement = this.isTempPlacement;
    this.placementAdd = Object.assign(modal);
    this.addPlacement();
  }

  addPlacement() {
    this.placementAdd.provider = this.provider;
    this._dataStoreService.setData(IntakeStoreConstants.addedPlacement, this.placementAdd);
    this._alertService.success(`Placement ${this.reportMode === 'add' ? 'Added' : 'Updated'} Successfully`);
    setTimeout(() => {
      this.router.navigate(['../list'], { relativeTo: this.route });
    }, 2000);
  }


  placementUpdate(modal) {
    modal.placementid = this.placementid;
    modal.providerid = this.providerId;
    modal.remarks = 'djs placement';
    modal.addate = moment(modal.addate).format('MM/DD/YYYY').toString();
    if (moment.isMoment(modal.adtime)) {
      modal.adtime = modal.adtime.format('HH:mm').toString();
    }
    modal.releasedate = moment(modal.releasedate).format('MM/DD/YYYY').toString();
    modal.placementadmissionclassificationtypedesc = this.provider.placementadmissionclassificationdesc;
    modal.placementadmissiontypedesc = this.provider.placementadmissiontypedesc;
    this.placementAdd = Object.assign(modal);
    this.placementAdd.placementadmissionclassificationkey = this.placement.placementadmissionclassificationtypekey;
    this.placementAdd.placementadmissiontypekey = this.placement.placementadmissiontypekey;
    this.placementAdd.placementadmissionauthorizationtypedesc = this.admissionAuthorizationList.find(item => item.value  === modal.PlacementAdmissionAuthorizationTypekey).text;
    this.placementAdd.placementadmissionauthorizationtypekey = modal.PlacementAdmissionAuthorizationTypekey;
    this.placementAdd.placementprimaryadmissionreasontypekey = modal.PlacementPrimaryAdmissionReasonTypekey;
    this.placementAdd.placementprimaryapprovedalttypekey = modal.PlacementPrimaryApprovedAltTypekey;
    this.placementAdd.county = modal.county;
    this.placementAdd.certifiedad = modal.certifiedAd;
    this.placementAdd.jcounty = modal.county;
    modal.istempplacement = this.isTempPlacement;
    delete this.placementAdd.certifiedAd;
    this.addPlacement();
  }

  goToProviderSearchResult() {
    this.router.navigate(['../provider-search'], { relativeTo: this.route });
  }

  cancel() {
    this.router.navigate(['../list'], { relativeTo: this.route });
  }
}
