import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakePlacementsRoutingModule } from './intake-placements-routing.module';
import { IntakePlacementsComponent } from './intake-placements.component';
import { IntakePlacementAddEditComponent } from './intake-placement-add-edit/intake-placement-add-edit.component';
import { IntakePlacementListComponent } from './intake-placement-list/intake-placement-list.component';
import { IntakePlacementProvideSearchComponent } from './intake-placement-provide-search/intake-placement-provide-search.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatExpansionModule, MatRadioModule, MatTooltipModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { AgmCoreModule } from '@agm/core';
import { PaginationModule } from 'ngx-bootstrap';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    IntakePlacementsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatExpansionModule,
    A2Edatetimepicker,
    AgmCoreModule,
    PaginationModule,
    MatRadioModule,
    MatTooltipModule,
    SharedDirectivesModule
  ],
  declarations: [IntakePlacementsComponent, IntakePlacementAddEditComponent, IntakePlacementListComponent, IntakePlacementProvideSearchComponent]
})
export class IntakePlacementsModule { }
