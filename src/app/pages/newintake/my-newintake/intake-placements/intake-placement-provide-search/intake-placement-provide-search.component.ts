import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { SearchPlan } from '../../../../case-worker/dsds-action/service-plan/_entities/service-plan.model';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { CommonHttpService, AuthService, DataStoreService } from '../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DSDS_STORE_CONSTANTS } from '../../../../case-worker/dsds-action/dsds-action.constants';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';

@Component({
  selector: 'intake-placement-provide-search',
  templateUrl: './intake-placement-provide-search.component.html',
  styleUrls: ['./intake-placement-provide-search.component.scss']
})
export class IntakePlacementProvideSearchComponent implements OnInit {
  id: string;
  searchPlacementForm: FormGroup;
  SearchPlacement$: Observable<SearchPlan[]>;
  countyDropdown$: Observable<DropdownModel[]>;
  SearchPlacementRes$: Observable<number>;
  providerInfo: { 'providerId': number; 'name': string; };
  nextDisabled = true;
  selectedProvider: any;
  providerId: string;
  userInfo: AppUser;
  paginationInfo: PaginationInfo = new PaginationInfo();
  folderTypeKey: string;
  resedentialType: string;
  needOperatedBy = false;
  transferProviderId: string;
  constructor(
    private _httpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private _authService: AuthService,
    private _dataStoreService: DataStoreService
  ) { }

  panelconfig = {
    panels: [
      {
        id: 1,
        name: 'Profile',
        selector: 'profile-info'
      },
      {
        id: 2,
        name: 'Contract',
        selector: 'contract-info'
      },
      {
        id: 3,
        name: 'Additional Information',
        selector: 'additional-info'
      }
    ]
  };

  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    this.folderTypeKey = 'Detention';
    this.searchPlacementForm = this._formBuilder.group({
      jurisdiction: [null],
      zipcode: [null],
      serviceType: [null],
      categoryType: [null],
      categorySubType: [null],
      paymentType: [null],
      vendor: [null],
      providerTaxId: [null],
      distance: [null],
      isoperatedbydjs: [null]
    });
    this.getCountyDropdown();
    this.searchPlacement(this.searchPlacementForm.getRawValue(), 1);
  }

  getCountyDropdown() {
    this.countyDropdown$ = this._httpService
      .getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmCountyListUrl + '?filter'
      )
      .map((result) => {
        return result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyid
            })
        );
      });
  }

  // otherPlacements(page: number) {
  //   // Need to call /api/foldertypeproviderconfig/listProviderByFoldertype? for only
  //   // commited and detention placement othre placement can be searched
  //   this.searchPlacement(this.searchPlacementForm.getRawValue(), 1);
  // }

  searchPlacement(model, page: number) {
    this._dataStoreService.setData(DSDS_STORE_CONSTANTS.PROVIDER_SEARCH_MODEL, model);
    let folderTypeKey;
    if (this.folderTypeKey) {
      folderTypeKey = `{${this.folderTypeKey}}`;
    }

    const source = this._httpService
      .getPagedArrayList(
        new PaginationRequest({

          where: {
            servicetype: model.serviceType ? model.serviceType : null,
            service: model.categoryType ? model.categoryType : null,
            servicesubtype: model.categorySubType ? model.categorySubType : null,
            county: model.jurisdiction ? model.jurisdiction : null,
            zipcode: model.zipcode ? model.zipcode : null,
            provider: model.vendor ? model.vendor : null,
            providerTaxId: model.providerTaxId ? model.providerTaxId : null,
            distance: model.distance ? model.distance : null,
            foldertype: folderTypeKey,
            isoperatedbydjs: model.isoperatedbydjs,
            pagenumber: page,
            pagesize: this.paginationInfo.pageSize,
          },
          method: 'post'
        }),
        'provider/search'
      )
      .map((res) => {
        return {
          data: res.data,
          count: res.count
        };
      })
      .share();
    this.SearchPlacement$ = source.pluck('data');
    if (page === 1) {
      this.SearchPlacementRes$ = source.pluck('count');
    }
    // this.childDropdown = [];
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.searchPlacement(this.searchPlacementForm.getRawValue(), pageInfo.page);
  }

  searchClearPlacement() {
    this.searchPlacementForm.reset();
    this.SearchPlacement$ = Observable.empty();
    this.SearchPlacementRes$ = Observable.empty();
  }

  getProvider(providerInformation) {
    console.log('Get Provider' + providerInformation);
    this.providerInfo = providerInformation;
  }

  selectedProv(provId) {
    this.nextDisabled = false;
    this.selectedProvider = provId;
    this._dataStoreService.setData('SELECTED_PROVIDER', {
      providername: this.selectedProvider.providername,
      providerid: this.selectedProvider.providerid,
      providercode: this.selectedProvider.providercode,
      providerunit: this.selectedProvider.providerunit,
      parentorg: this.selectedProvider.providerorganizationtype,
      placementadmissionclassificationkey: this.selectedProvider.placementadmissionclassificationkey,
      PlacementAdmissionTypekey: this.selectedProvider.placementadmissiontypekey,
      fieldworker: this.userInfo.user.userprofile.displayname,
      placementadmissionclassificationdesc: this.selectedProvider.placementadmissionclassificationdesc,
      placementadmissiontypedesc: this.selectedProvider.placementadmissiontypedesc
    });
  }

  navigateToAddPlacement() {
    this.router.navigate(['../add'], { relativeTo: this.route });
  }

  ResidentialTypeChange(value) {
    this.resedentialType = value;
  }
}
