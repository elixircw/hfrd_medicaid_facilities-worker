import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakePlacementProvideSearchComponent } from './intake-placement-provide-search.component';

describe('IntakePlacementProvideSearchComponent', () => {
  let component: IntakePlacementProvideSearchComponent;
  let fixture: ComponentFixture<IntakePlacementProvideSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakePlacementProvideSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakePlacementProvideSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
