import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakePlacementsComponent } from './intake-placements.component';
import { IntakePlacementAddEditComponent } from './intake-placement-add-edit/intake-placement-add-edit.component';
import { IntakePlacementListComponent } from './intake-placement-list/intake-placement-list.component';
import { IntakePlacementProvideSearchComponent } from './intake-placement-provide-search/intake-placement-provide-search.component';

const routes: Routes = [
  {
    path: '',
    component: IntakePlacementsComponent,
    children: [
      {
        path: 'provider-search',
        component: IntakePlacementProvideSearchComponent
      },
      {
        path: 'list',
        component: IntakePlacementListComponent
      },
      {
        path: ':operation',
        component: IntakePlacementAddEditComponent
      },
      {
        path: '**',
        redirectTo: 'list'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakePlacementsRoutingModule { }
