import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakePlacementsComponent } from './intake-placements.component';

describe('IntakePlacementsComponent', () => {
  let component: IntakePlacementsComponent;
  let fixture: ComponentFixture<IntakePlacementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakePlacementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakePlacementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
