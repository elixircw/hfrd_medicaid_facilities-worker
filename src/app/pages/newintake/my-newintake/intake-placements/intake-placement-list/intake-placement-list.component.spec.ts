import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakePlacementListComponent } from './intake-placement-list.component';

describe('IntakePlacementListComponent', () => {
  let component: IntakePlacementListComponent;
  let fixture: ComponentFixture<IntakePlacementListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakePlacementListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakePlacementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
