import { Component, OnInit } from '@angular/core';
import { CommonHttpService, DataStoreService, AuthService, AlertService } from '../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { IntakeStoreConstants } from '../../my-newintake.constants';
import { AppConstants } from '../../../../../@core/common/constants';

@Component({
  selector: 'intake-placement-list',
  templateUrl: './intake-placement-list.component.html',
  styleUrls: ['./intake-placement-list.component.scss']
})
export class IntakePlacementListComponent implements OnInit {
  involvedYouth: any;
  reportMode: string;
  isAddPlacement = false;
  selectedProvider: any;
  detainerFlag: boolean;
  resedentialStatus: boolean;
  personId: string;
  isBlockParentPlacement = false;
  userInfo: AppUser;
  store: any;
  addedPlacement: any;
  constructor(
    private _httpService: CommonHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _alertService: AlertService
  ) {
    this.store = this._dataStoreService.getCurrentStore();
  }

  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    this.getPlacement();
    this.getInvolvedPerson();
  }

  private getDefaults() {
    this.getResedentialStatus().subscribe(element => {
      this.resedentialStatus = element[0].residentialexists;
      this.detainerFlag = element[0].detainerflag;
    });
  }

  getResedentialStatus() {
    return this._httpService.getArrayList({ where: { personid: this.personId }, nolimit: true, method: 'get' }, 'placement/placementresidential?filter');
  }

  searchClearPlacement() {
    this.router.navigate(['../provider-search'], { relativeTo: this.route });
  }

  getPlacement() {
    this.addedPlacement = this.store[IntakeStoreConstants.addedPlacement] ? this.store[IntakeStoreConstants.addedPlacement] : null;
  }

  deletePlacement() {
    this._alertService.success('Placement deleted successfully');
    this.addedPlacement = null;
    this._dataStoreService.setData(IntakeStoreConstants.addedPlacement, this.addedPlacement);
  }

  getInvolvedPerson() {
    const addedPersons = this.store[IntakeStoreConstants.addedPersons] ? this.store[IntakeStoreConstants.addedPersons] : [];
    this.involvedYouth = addedPersons.find((p) => p.Role === 'Youth');
    this.personId = (this.involvedYouth && this.involvedYouth.Pid) ? this.involvedYouth.Pid : null;
    if (this.personId && (this.personId.indexOf(AppConstants.PERSON.TEMP_ID) === -1)) {
      this.getDefaults();
    } else {
      this.resedentialStatus = false;
      this.detainerFlag = false;
    }
  }

  SelectedForModify(model) {
    this.reportMode = 'update';
    this.selectedProvider = model;
    this.router.navigate(['../update'], { relativeTo: this.route });
  }
}
