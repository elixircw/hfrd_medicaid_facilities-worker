import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeDocumentCreatorRoutingModule } from './intake-document-creator-routing.module';

import { NoticePreintakeLetterComponent } from './notice-preintake-letter/notice-preintake-letter.component';
import { AcknowledgementLetterComponent } from './acknowledgement-letter/acknowledgement-letter.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { SharedComponentsModule } from '../../../../shared/shared-components/shared-components.module';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeDocumentCreatorRoutingModule,
    FormMaterialModule,
    SharedComponentsModule,
    SharedDirectivesModule
  ]
})
export class IntakeDocumentCreatorModule { }
