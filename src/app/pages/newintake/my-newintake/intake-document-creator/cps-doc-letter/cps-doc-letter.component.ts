import { sdmData } from './../../_data/sdm';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {
    InvolvedPerson,
    CpsDocInput,
    ReviewStatus,
    SDMDescription,
    Sdm,
    Narrative
} from '../../_entities/newintakeModel';
import * as _ from 'lodash';
import { HttpService } from '../../../../../@core/services/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { General, EvaluationFields } from '../../_entities/newintakeSaveModel';
import { Subject } from 'rxjs/Subject';
import * as jsPDF from 'jspdf';
import { AuthService, DataStoreService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { IntakeStoreConstants } from '../../my-newintake.constants';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cps-doc-letter',
    templateUrl: './cps-doc-letter.component.html',
    styleUrls: ['./cps-doc-letter.component.scss']
})
export class CpsDocLetterComponent implements OnInit {
    persons: any[];
    general: General;
    evalFields: EvaluationFields;
    @Input() cpsdocData: CpsDocInput;
    // @Input() cpsdocGenAction: Subject<string>;
    reviewStatus: string;
    // @Input() sdmReport$ = new Subject<Sdm>();
    // @Input() narrativeOutputSubject$ = new Subject<Narrative>();
    casehead: InvolvedPerson;
    addedPersons: any[];
    householdMem: InvolvedPerson[] = [];
    nonHouseholdMem: InvolvedPerson[] = [];
    imgname: any;
    currentDate = new Date();
    narrative = '';
    cpsHistoryInfo = '';
    cpsHistPresent = false;
    narrativePresent = false;
    id: any;
    sdmJsonData: SDMDescription;
    supervisorApprovalDetails = [];
    sdmFormData: Sdm;
    narrativeFormData: Narrative;
    roleId: AppUser;
    pdfFiles: {
        fileName: string;
        images: { image: string; height: any; name: string }[];
    }[] = [];
    PAGE_HEIGHT = 920;
    private store: any;
    intakenumber: any;
    typeOfMaltreatment: string[];
    referralSource: any;
    constructor(
        private _authService: AuthService,
        private _commonService: CommonHttpService,
        private _store: DataStoreService,
        private _http: HttpService,
        private route: ActivatedRoute,
        private _alertService: AlertService,
    ) {
        this.store = this._store.getCurrentStore();
        this.id = route.snapshot.parent.parent.parent.params['id'];
    }

    ngOnInit() {
        this.loadPersons();

    }



    ngAfterViewChecked() {

        if(this.cpsHistPresent)
        {
            var ch = document.getElementById('cps-history-div').children;
            // if(ch.length > 0){
            var i=0;
            for(i=0;i<ch.length;i++){
                if(ch[i].tagName == 'pre' || ch[i].tagName == 'PRE'){
                    ch[i].setAttribute('style','white-space: pre-line;');
                }
            }
        }
        if(this.narrativePresent){
            var cn = document.getElementById('narrative-div').children;
            
            var i=0;
            for(i=0;i<cn.length;i++){
                if(cn[i].tagName == 'pre' || cn[i].tagName == 'PRE'){
                    cn[i].setAttribute('style','white-space: pre-line;');
                }
            }
        }
    }
    mapdata() {
        this.persons = this.addedPersons; // this.store[IntakeStoreConstants.addedPersons];
        this.general = this.store[IntakeStoreConstants.general];
        this.evalFields = this.store[IntakeStoreConstants.evalFields];
        this.reviewStatus = this.store[IntakeStoreConstants.reviewStatus];
        this.referralSource = this.store[IntakeStoreConstants.REFERALSOURCE];
        if (_.has(this.store, 'addNarrative.Narrative')) {
            this.narrative = this.store.addNarrative.Narrative;
            this.narrativePresent = true;
        }
        if(this.store.addNarrative.cpsHistoryClearance != '' && this.store.addNarrative.cpsHistoryClearance != null){
            this.cpsHistoryInfo = this.store.addNarrative.cpsHistoryClearance;
            this.cpsHistPresent = true;
        }
        
        // this.cpsHistPresent = this.store.addNarrative.cpsHistPresent;
        // if(this.cpsHistPresent){
        //     this.cpsHistoryInfo = this.store.addNarrative.cpsHistoryClearance;
        // }
        this.cpsdocData = new CpsDocInput();
        this.cpsdocData.intakePurpose = this.store[IntakeStoreConstants.intakeServiceGrid];
        // this.getSupervisorApprovalInfo();
        // const intakeCommunication= this._store[IntakeStoreConstants.comm];
        // if (this.intakeCommunication) {
        //     this.intakeCommunication.map(data => {
        //         if (data.value === input) {
        //             this.cpsdocData.InputSource = data.text;
        //         }
        //     });
        // }
        this.sdmJsonData = sdmData;
        this.imgname = require('assets/images/draft.png');
        this.roleId = this._authService.getCurrentUser();
        const cpsDocument = this.store[IntakeStoreConstants.cpsDocument];
        if (cpsDocument === 'generate') {
            if ($('#vir-csp').children().length === 0) {
                this.createpreview();
            }
        } else if (cpsDocument === 'download') {
            this.downloadCasePdf('vir-csp');
        }
        // this.cpsdocGenAction.subscribe((data) => {

        //     if (data === 'generate') {
        //         if ($('#vir-csp').children().length === 0) {
        //             this.createpreview();
        //         }
        //     } else if (data === 'download') {
        //         this.downloadCasePdf('vir-csp');
        //     }
        // });

        this.sdmFormData = this.store[IntakeStoreConstants.intakeSDM];
        this.getTypeOfReferral();
        // this.sdmReport$.subscribe((data) => {
        //     this.sdmFormData = data;
        // });

        this.narrativeFormData = this.store[IntakeStoreConstants.addNarrative];
        console.log("STORE", this.store);
        // this.narrativeOutputSubject$.subscribe((data) => {
        //     this.narrativeFormData = data;
        // });
        this.getCaseHead();
        this.getMembersOfHousehold();
        if ( this.general && this.general['IntakeNumber']) {
            this.getSupervisorApprovalInfo();
        }
        if (_.has(this.store, 'da_intakenumber')) {
            // this.general = {};
           // this.general['IntakeNumber'] = this.store.da_intakenumber;
            this.intakenumber = this.store.da_intakenumber;
            this.getSupervisorApprovalInfo();
        }

    }


    loadPersons() {

        // if ((intakenumb === null || intakenumb === undefined || intakenumb === '') && this.id) {
        //     this.intakenumber = this.store.da_intakenumber;
        //     condition = {
        //         intakeserviceid: this.id
        //         };
        //     // this.intakenumber = this.store[IntakeStoreConstants.communications];
        // } else {
        //     this.intakenumber = intakenumb;
        //     condition = {
        //         intakenumber: this.intakenumber
        //         };
        // }
        this.intakenumber = this.store.da_intakenumber;

        this._commonService
        .getPagedArrayList(
          new PaginationRequest({
            page: 1,
            limit: 20,
            method: 'get',
            //where: condition
            where: {intakenumber: this.intakenumber}
          }),
          'People/getpersondetailcw?filter'
        ).subscribe(response => {
          this.addedPersons = response.data;
          this.mapdata();
        });
    }


    sdmKeys(obj) {
        if (obj) {
            const sdmObject = [];
            Object.keys(obj).forEach(key => {
                if (obj[key] === true) {
                    sdmObject.push(key);
                }
            });
            return sdmObject;
        }
    }
    generatePageDiv() {
        const pageDiv = document.createElement('div');
        pageDiv.className = 'pdf-page sizeA4 page-paddings';
        return pageDiv;
    }

    genrateContainerDiv() {
        const container = document.createElement('div');
        container.className = 'pdf-container';
        return container;
    }

    getSections() {
        const list = [
            'record-of-contact',
            'rocommended-response-time',
            'referral-casehead',
            'add-n-contact',
            'referral-narrative',
            'other-mem-of-household',
            'ref-parti-in-household',
            'oth-mem-not-in-household',
            'out-home-maltrtmt',
            'order-of-shelter',
            'law-enf',
            'allg-pert-chld',
            'recomm-n-over',
            'work-approvals',
            'related-cases',
            'source-referral'
        ];
        const containers = [];
        list.forEach(element => {
            if (element === 'ref-parti-in-household') {
                const householdMeminfos = document.getElementsByClassName(
                    'householdMeminfo'
                );
                const householdMemaddinfos = document.getElementsByClassName(
                    'householdMemaddinfo'
                );
                const householdMemconinfos = document.getElementsByClassName(
                    'householdMemconinfo'
                );
                const householdMeminfoheader = document.getElementsByClassName(
                    'householdMeminfoheader'
                );

                for (let i = 0; i < this.householdMem.length; i++) {
                    containers.push(householdMeminfos[i]);
                    containers.push(householdMeminfoheader[i]);
                    containers.push(householdMemaddinfos[i]);
                    containers.push(householdMemconinfos[i]);
                }
            } else if (element === 'ref-parti-in-household') {
                const householdMeminfos = document.getElementsByClassName(
                    'outhouseholdMeminfo'
                );
                const householdMemaddinfos = document.getElementsByClassName(
                    'outhouseholdMemaddinfo'
                );
                const householdMemconinfos = document.getElementsByClassName(
                    'outhouseholdMemconinfo'
                );
                const householdMeminfoheader = document.getElementsByClassName(
                    'outhouseholdMeminfoheader'
                );

                for (let i = 0; i < this.householdMem.length; i++) {
                    containers.push(householdMeminfos[i]);
                    containers.push(householdMeminfoheader[i]);
                    containers.push(householdMemaddinfos[i]);
                    containers.push(householdMemconinfos[i]);
                }
            } else {
                containers.push(document.getElementById(element));
            }
        });
        return containers;
    }

    createpreview() {
        const virelement = document.getElementById('vir-csp');
        const header = document.getElementById('cps-intake-header');
        const footer = document.getElementById('cps-intake-footer');
        footer.className = 'mt-10 cps-footer';
        let divele = this.generatePageDiv();
        let wraper = this.genrateContainerDiv();
        divele.appendChild(header);
        divele.appendChild(wraper);
        let pageHeight = this.PAGE_HEIGHT;
        this.getSections().forEach(element => {
            const offsetheight = element.offsetHeight;
            if (pageHeight - offsetheight > 0) {
                wraper.appendChild(element);
                pageHeight = pageHeight - offsetheight;
            } else {
                pageHeight = this.PAGE_HEIGHT;
                divele.appendChild(footer.cloneNode(true));
                virelement.appendChild(divele);
                divele = this.generatePageDiv();
                wraper = this.genrateContainerDiv();
                divele.appendChild(header.cloneNode(true));
                wraper.appendChild(element);
                pageHeight = pageHeight - offsetheight;
                divele.appendChild(wraper);
            }
        });

        divele.appendChild(footer);
        virelement.appendChild(divele);
        const pages = virelement.getElementsByClassName('page-index');
        for (let i = 0; i < pages.length; i++) {
            pages.item(i).innerHTML = i + 1 + ' of ' + pages.length;
        }
        document.getElementById('CPS-Intake-Letter').hidden = true;
    }

    downloadCPSIntakePdf() {
        const req = {
            ...this.store,
            ...this.general,
            'username' : (this.roleId && this.roleId.user && this.roleId.user.username) ? this.roleId.user.username : '',
            ...this.casehead,
            'sdmJsonData' : this.sdmJsonData,
            'sdmFormData' : this.sdmFormData,
            'narrativeFormData': this.narrativeFormData,
            'typeOfMaltreatment' : this.typeOfMaltreatment,
            'nonHouseholdMem' : this.nonHouseholdMem,
            'householdMem' : this.householdMem,
            'supervisorApprovalDetails': this.supervisorApprovalDetails
        };

        const payload = {
            method: 'post',
            count: -1,
            page: 1,
            limit: 20,
            where: req,
            documntkey: [
                    'cpsintakereport'
                ]
          };

        this._commonService.create(payload, 'Intakeservicerequestdispositioncodes/getreportcpsintake').subscribe(
            response => {
              console.log(response, 'pdf response');
              setTimeout(() => window.open(response.data.documentpath), 2000);
        });

    }


    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            await html2canvas(<HTMLElement>pages.item(i)).then(canvas => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
            });
        }
        const pageName = 'pageName';
        this.pdfFiles.push({ fileName: pageName, images: pageImages });
        pageImages = [];
        this.convertImageToPdf();
    }
    convertImageToPdf() {
        this.pdfFiles.forEach(pdfFile => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            if (
                this.reviewStatus !== 'Review' &&
                this.reviewStatus !== 'Accepted'
            ) {
                this.addWaterMark(doc);
            }
            doc.save(pdfFile.fileName);
        });
        (<any>$('#intake-complaint-pdf1')).modal('hide');
        this.pdfFiles = [];
    }

    cpsPdfCreator() {
        const element = document.getElementById('CPS-Intake-Letter');
        const header = document.getElementById('cps-intake-header');
        const footer = document.getElementById('cps-intake-footer');

        const pdf = new jsPDF('p', 'pt', 'a4');
        pdf.internal.scaleFactor = 3.75;

        const w = element.clientWidth;
        const h = element.clientHeight;
        const newCanvas = document.createElement('canvas');
        newCanvas.width = w * 2;
        newCanvas.height = h * 2;
        newCanvas.style.width = w + 'px';
        newCanvas.style.height = h + 'px';
        const context = newCanvas.getContext('2d');
        context.scale(2, 2);

        const doc = new jsPDF();

        doc.setFontSize(14);
        const list = [
            'cps-intake-header',
            'cps-intake-footer',
            'record-of-contact',
            'rocommended-response-time',
            'referral-casehead',
            'add-n-contact',
            'referral-narrative',
            'other-mem-of-household',
            'end'
        ];
    }

    addWaterMark(doc: jsPDF) {
        const totalPages = doc.internal.getNumberOfPages();

        for (let i = 1; i <= totalPages; i++) {
            doc.setPage(i);
            const canvas = document.createElement('canvas');

            doc.addImage(this.imgname, 'PNG', 0, 0);
        }

        return doc;
    }

    getCaseHead() {
        if (this.persons) {
            this.persons.forEach(element => {
                if (element.roles) {
                    const hascasehead = element.roles.some(
                        item => item.intakeservicerequestpersontypekey === 'LG'
                    );
                    if (hascasehead) {
                        element.displayMultipleRole = element.roles.map(
                            role => role.typedescription
                        );
                        this.casehead = element;
                        if (this.casehead && this.casehead.race && this.casehead.race.length > 0) {
                            let caseheadrace = Array.isArray(this.casehead.race) ? this.casehead.race : [];
                            caseheadrace = caseheadrace.map(item => item.value_text);
                            const caseheadracelist = new Set(caseheadrace);
                            this.casehead['raceList'] = [...Array.from(caseheadracelist)];
                            // this.casehead.race = race;
                        }
                    }
                }
            });
        }
    }

    getSupervisorApprovalInfo() {
        this._http.post( CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CpsIntakeReport, {
            'intakenumber': this.intakenumber
        }).subscribe((response) => {
            // this._alertService.success(response);
            this.supervisorApprovalDetails = response.data.getsupervisorapprovaldetails;
            });
    }

    getMembersOfHousehold() {
        if (this.persons) {
            this.persons
                .filter(item => item.rolename !== 'LG')
                .forEach(element => {
                    if (element.roles) {
                        const casehead = element.roles.filter(
                            item => item.intakeservicerequestpersontypekey === 'LG'
                        );
                        if (!casehead.length) {
                            element.displayMultipleRole = element.roles.map(
                                role => role.typedescription
                            );

                            if (element.ssn !== null && element.ssn !== undefined && element.ssn !== '') {
                                element.ssnAvailable = 'Yes';
                            } else {
                                element.ssnAvailable = 'No';
                            }

                            if (element.RelationshiptoRA === 'A3') {
                                this.nonHouseholdMem.push(element);
                            } else {
                                this.householdMem.push(element);
                            }
                        }
                    }
                });
        }
    }

    getTypeOfReferral() {
        this.typeOfMaltreatment = [];
        if (this.sdmFormData) {
            if (this.sdmFormData.ismalpa_suspeciousdeath || this.sdmFormData.ismalpa_nonaccident ||
                this.sdmFormData.ismalpa_injuryinconsistent || this.sdmFormData.ismalpa_insjury || this.sdmFormData.ismalpa_childtoxic
                || this.sdmFormData.ismalpa_caregiver) {
                this.typeOfMaltreatment.push('Physical Abuse');
            } if (this.sdmFormData.ismalsa_sexualmolestation || this.sdmFormData.ismalsa_sexualact
                || this.sdmFormData.ismalsa_sexualexploitation || this.sdmFormData.ismalsa_physicalindicators) {
                this.typeOfMaltreatment.push('Sexual Abuse');
            } if (this.sdmFormData.isnegfp_cargiverintervene || this.sdmFormData.isnegab_abandoned ||
                this.sdmFormData.isnegmn_unreasonabledelay ||
                this.sdmFormData.isneguc_leftunsupervised || this.sdmFormData.isneguc_leftaloneinappropriatecare || this.sdmFormData.isneguc_leftalonewithoutsupport ||
                this.sdmFormData.isneggn_suspiciousdeath || this.sdmFormData.isneggn_signsordiagnosis || this.sdmFormData.isneggn_inadequatefood || this.sdmFormData.isneggn_childdischarged) {
                this.typeOfMaltreatment.push('General Neglect');
            } if (this.sdmFormData.ismenab_psycologicalability || this.sdmFormData.ismenng_psycologicalability) {
                this.typeOfMaltreatment.push('mental injury');
            } if (this.sdmFormData.isnegrh_exposednewborn || this.sdmFormData.isnegrh_priordeath || this.sdmFormData.isnegrh_domesticviolence
                || this.sdmFormData.isnegrh_sexualperpetrator || this.sdmFormData.isnegrh_basicneedsunmet || this.sdmFormData.isnegrh_treatmenthealthrisk) {
                this.typeOfMaltreatment.push('Risk of harm');
            }
        }
    }
}
