import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpsDocLetterComponent } from './cps-doc-letter.component';

describe('CpsDocLetterComponent', () => {
  let component: CpsDocLetterComponent;
  let fixture: ComponentFixture<CpsDocLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpsDocLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpsDocLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
