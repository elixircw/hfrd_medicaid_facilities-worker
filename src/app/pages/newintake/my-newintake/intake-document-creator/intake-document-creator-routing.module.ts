import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CpsDocLetterComponent } from './cps-doc-letter/cps-doc-letter.component';
import { NoticePreintakeLetterComponent } from './notice-preintake-letter/notice-preintake-letter.component';
 /* tslint:disable:no-unused-variable */
import { AcknowledgementLetterComponent } from './acknowledgement-letter/acknowledgement-letter.component';

const routes: Routes = [
    {
        path: 'cps-doc',
        component: CpsDocLetterComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IntakeDocumentCreatorRoutingModule {}
