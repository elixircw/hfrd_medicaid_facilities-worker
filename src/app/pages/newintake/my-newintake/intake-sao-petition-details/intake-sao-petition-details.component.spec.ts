import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeSaoPetitionDetailsComponent } from './intake-sao-petition-details.component';

describe('IntakeSaoPetitionDetailsComponent', () => {
  let component: IntakeSaoPetitionDetailsComponent;
  let fixture: ComponentFixture<IntakeSaoPetitionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeSaoPetitionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeSaoPetitionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
