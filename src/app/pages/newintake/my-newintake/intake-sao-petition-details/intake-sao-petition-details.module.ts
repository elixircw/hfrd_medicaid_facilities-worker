import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeSaoPetitionDetailsRoutingModule } from './intake-sao-petition-details-routing.module';
import { IntakeSaoPetitionDetailsComponent } from './intake-sao-petition-details.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    IntakeSaoPetitionDetailsRoutingModule,
    FormMaterialModule,
    PaginationModule
  ],
  declarations: [IntakeSaoPetitionDetailsComponent]
})
export class IntakeSaoPetitionDetailsModule { }
