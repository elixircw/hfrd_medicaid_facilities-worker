import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeSaoPetitionDetailsComponent } from './intake-sao-petition-details.component';

const routes: Routes = [
  {
      path: '',
      component: IntakeSaoPetitionDetailsComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeSaoPetitionDetailsRoutingModule { }
