import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { Observable, Subject } from 'rxjs/Rx';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../newintake-url.config';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PetitionDetails, EvaluationFields, Petition } from '../_entities/newintakeSaveModel';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../../@core/services';
import { AppConstants } from '../../../../@core/common/constants';
import { MyNewintakeConstants, IntakeStoreConstants } from '../my-newintake.constants';
import * as moment from 'moment';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'intake-sao-petition-details',
  templateUrl: './intake-sao-petition-details.component.html',
  styleUrls: ['./intake-sao-petition-details.component.scss']
})
export class IntakeSaoPetitionDetailsComponent implements OnInit {

  hearingType$: Observable<any[]>;
  petitionType$: Observable<any[]>;
  petitionDetailsForm: FormGroup;
  hearingDetailsForm: FormGroup;
  times: string[] = [];
  intakeNumber: string;
  selectedPetitionId: string;
  selectedOffences: string;
  PetitionDetails: PetitionDetails;
  serverPetition: Petition;
  saoTabIndex: string;
  complaintDetails: any = {};
  selectedVictims: any[] = [];
  evalFields: any[] = [];
  petitions = [];
  forwardedPetitions = [];
  addedPersons = [];
  evalFieldsInfo = [];
  pettitionsAvailableToAssociate = [];
  pettitiontypes = [];
  complaintIDs = [];
  filteredComplaintIDs = [];
  hasMultipleComplaintID = false;
  isAssociateComplaintAvailable = false;
  totalRecords: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  primaryPetition = '';
  associateComplaint: boolean;
  associatedComplaintList = [];
  constructor(private _commonHttpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _router: Router) { }
  showPetitonAddForm = true;
  selectedPetition: any = {};
  associatedPetitions: any[] = [];
  associatePetition: any = {};
  selectedComplaintIDs: any[] = [];
  fromPopup: boolean;
  fromCounty = '';
  toCounty = '';
  transferNotes = '';
  selectedProbation: any;

  ngOnInit() {
    this.loadDroddowns();
    this.initiateFormGroup();
    this.fromPopup = false;
    this.selectedPetitionId = '';
    this.route.params.subscribe((item) => {
      this.intakeNumber = this._dataStoreService.getData(IntakeStoreConstants.intakenumber);
      this.loadAddedPetitions();
      this.loadAssociatedPetitions();
    });
    this.petitionDetailsForm.valueChanges.subscribe((val) => {
      // this.petitionDetailsInputSubject$.next(this.petitionDetailsForm.getRawValue());
      const petitionDetails = this.petitionDetailsForm.getRawValue();
      this.selectedComplaintIDs = [];
      if ((petitionDetails.petitionid === null || petitionDetails.petitionid === '') && (petitionDetails.complaintids)) {
        this.isAssociateComplaintAvailable = true;
        if (Array.isArray(petitionDetails.complaintids)) {
          this.selectedComplaintIDs = petitionDetails.complaintids.map(complaint => {
            return {
              intakenumber: this.intakeNumber,
              intakeservicerequestevaluationid: complaint.intakeservicerequestevaluationid,
              complaintID: complaint.complaintid
            };
          });
        } else {
          this.selectedComplaintIDs = [{
            intakenumber: this.intakeNumber,
            intakeservicerequestevaluationid: petitionDetails.complaintids.intakeservicerequestevaluationid,
            complaintID: petitionDetails.complaintids.complaintid
          }];
        }
      } else {
        this.isAssociateComplaintAvailable = false;
      }
    });

    this._dataStoreService.currentStore.subscribe(store => {
      // if (store[MyNewintakeConstants.Intake.petitionDetails] && this.petitions.length === 0) {
      //   this.petitions = store[MyNewintakeConstants.Intake.petitionDetails];
      //   this.route.params.subscribe((item) => {
      //     this.intakeNumber = item['id'];
      //     this.loadEvaluationFeildList();
      //   });
      // }
      if (store[IntakeStoreConstants.evalFields] && this.evalFieldsInfo.length === 0) {
        this.evalFieldsInfo = store[IntakeStoreConstants.evalFields];
      }
      if (store[MyNewintakeConstants.Intake.addedPersons] && this.addedPersons.length === 0) {
        this.addedPersons = store[MyNewintakeConstants.Intake.addedPersons];
      }
    });
    this.filterComplaintIDs();
    this.times = this.generateTimeList(false);



    this.getForwardedPetitions();
    this.getTransferPetitionDetails();
  }

  getTransferPetitionDetails() {
    this._commonHttpService.getArrayList({
      where: {
        intakenumber: this.intakeNumber
      },
      method: 'get'
    }, NewUrlConfig.EndPoint.Intake.getForwardedPetitionsDetails).subscribe(
      (response) => {
        if (response.length > 0) {
          const data = response[0];
          this.fromCounty = data.fromcounty;
          this.toCounty = data.countyname;
          this.transferNotes = data.saotransfernotes;
        }
      });
  }

  getForwardedPetitions() {
    this._commonHttpService.getArrayList({
      page: 1,
      limit: 10,
      sortcolumn: 'intakenumber',
      sortorder: 'asc',
      where: {
        intakenumber: this.intakeNumber
      },
      method: 'get'
    }, NewUrlConfig.EndPoint.Intake.getForwardedPetitions).subscribe(
      (response) => {
        this.forwardedPetitions = response;
      });
  }

  loadAssociatedPetitions() {
    this._commonHttpService.getAllFilter({
      where: {
        intakenumber: this.intakeNumber
      }
    }, NewUrlConfig.EndPoint.Intake.getAssociatedPetitions).subscribe(
      (response) => {
        this.pettitionsAvailableToAssociate = response.data;
      });
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadAssociatedPetitions();
  }

  loadAddedPetitions() {
    this._commonHttpService.getArrayList({
      page: 1,
      limit: 10,
      sortcolumn: 'createddate',
      sortorder: 'desc',
      where: {
        intakenumber: this.intakeNumber
      },
      method: 'get'
    }, NewUrlConfig.EndPoint.Intake.getAddedPetitions).subscribe(
      (response) => {
        this.petitions = response;
        // this.loadEvaluationFeildList();
        this.broadCastPettions();
      });
  }

  initiateFormGroup() {

    this.petitionDetailsForm = this._formBuilder.group({
      complaintids: [[''], Validators.required],
      petitionid: ['', Validators.required],
      pettitiontype: ['', Validators.required]
    });

    this.hearingDetailsForm = this._formBuilder.group({

      intakenumber: '',
      petitionid: '',
      associatedattorneys: '',
      complaintid: '',
      transferpetitionid: '',
      petitionfiled: 0,
      hearingdatetime: '',
      hearingtypekey: '',
      hearingnotes: '',
      hearingDate: '',
      hearingTime: '08:00'

    });
  }

  openAssociatePopup(petition) {
    if (petition) {
      this.selectedPetition = petition;
      this.primaryPetition = this.selectedPetition.petitionid;
      this.associateComplaint = false;
    } else {
      this.primaryPetition = '';
      this.associateComplaint = true;
    }
    this.pettitionsAvailableToAssociate.forEach(petitionObj => {
      petitionObj.isChecked = false;
    });
    this.loadAssociatedPetitions();
    setTimeout(() => {
      (<any>$('#associate-petition')).modal('show');
    }, 500);
  }

  resetPrimaryPetition() {
    if (this.associateComplaint) {
      this.primaryPetition = '';
    } else {
      this.primaryPetition = this.selectedPetition.petitionid;
    }
  }

  associatePetitions() {
    console.log(this.primaryPetition);
    let associatePetition: any = {};
    let associate = [];
    this.associatedPetitions = [];
    this.pettitionsAvailableToAssociate.forEach(petition => {
      if (petition.isChecked && petition.petitionid !== this.primaryPetition && !this.associateComplaint) {
        this.associatedPetitions.push(petition.petitionid);
        associate.push({
          intakeservicerequestpetitionid: petition.intakeservicerequestpetitionid,
          intakenumber: petition.intakenumber
        });
      } else if (petition.petitionid === this.primaryPetition) {
        associatePetition = {
          intakeservicerequestpetitionid: petition.intakeservicerequestpetitionid,
          intakenumber: petition.intakenumber
        };
      }
    });

    if (this.selectedPetition.petitionid !== this.primaryPetition && !this.associateComplaint) {
      this.associatedPetitions.push(this.selectedPetition.petitionid);
      associate.push({
        intakeservicerequestpetitionid: this.selectedPetition.intakeservicerequestpetitionid,
        intakenumber: this.intakeNumber
      });
    } else if (this.selectedPetition.petitionid === this.primaryPetition) {
      associatePetition = {
        intakeservicerequestpetitionid: this.selectedPetition.intakeservicerequestpetitionid,
        intakenumber: this.intakeNumber
      };
    }

    if (this.associateComplaint) {
      associate = this.selectedComplaintIDs;
    }

    if (associate.length === 0) {
      this._alertService.warn('Please select an available petition to Associate.');
      return;
    } else if (this.primaryPetition === '') {
      this._alertService.warn('Please select a primary petition.');
      return;
    }

    associatePetition.associate = associate;
    this.associatePetition = associatePetition;

    (<any>$('#associate-petition')).modal('hide');
    (<any>$('#associate-confirmation-petition')).modal('show');

    console.log(associatePetition);
  }

  backToAssociation() {
    (<any>$('#associate-petition')).modal('show');
    (<any>$('#associate-confirmation-petition')).modal('hide');
  }

  confirmAssociation() {
    const url = this.associateComplaint ? NewUrlConfig.EndPoint.Intake.addAssociateComplaint : NewUrlConfig.EndPoint.Intake.addAssociatePetition;
    this._commonHttpService.create(this.associatePetition, url)
      .subscribe(res => {
        console.log(res);
        this._alertService.success('Petition(s) associated successfully.');
        this.loadAddedPetitions();
        this.filterComplaintIDs();
        this.petitionDetailsForm.reset();
        (<any>$('#associate-confirmation-petition')).modal('hide');
      });
  }

  private generateTimeList(is24hrs = true) {
    const x = 15; // minutes interval
    const times = []; // time array
    let tt = 0; // start time
    const ap = [' AM', ' PM']; // AM-PM

    // loop to increment the time and push results in array
    for (let i = 0; tt < 24 * 60; i++) {
      const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
      const mm = (tt % 60); // getting minutes of the hour in 0-55 format
      if (is24hrs) {
        times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
      } else {
        times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
      }
      tt = tt + x;
    }
    return times;
  }
  private loadDroddowns() {

    const source = forkJoin([
      this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.hearingType),
      this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.petitionTypes),
    ])
      .map(([hearingType, petitionTypes]) => {
        return {
          hearingType: hearingType.map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.hearingtypekey
              })
          ),
          petitionTypes: petitionTypes.map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.petitiontypekey
              })
          )
        };
      })
      .share();
    this.hearingType$ = source.pluck('hearingType');
    this.petitionType$ = source.pluck('petitionTypes');
    this.pettitiontypes = [{ 'key': 'Detention', 'value': 'Detention' },
    { 'key': 'Shelter', 'value': 'Shelter' },
    { 'key': 'Delinquency', 'value': 'Delinquency' },
    { 'key': 'Child in need of Supervision (CINS)', 'value': 'Child in need of Supervision (CINS)' },
    { 'key': 'Waiver', 'value': 'Waiver' },
    { 'key': 'Transfer of Jurisdiction/Adult Court', 'value': 'Transfer of Jurisdiction/Adult Court' }];

  }
  patchForm(data: PetitionDetails) {
    if (data) {
      if (data.hearingTime === '') {
        data.hearingTime = '08:00';
      }
      this.petitionDetailsForm.patchValue(data);
    }
  }

  toggleTable(id) {
    (<any>$('#' + id)).collapse('toggle');
  }

  addPetition() {
    if (this.petitionDetailsForm.invalid) {
      this._alertService.warn('Please fill required feilds');
      return false;
    }
    const pettionObject = this.petitionDetailsForm.getRawValue();
    if (!this.isPetitionExist(pettionObject.petitionid)) {
      if (!this.hasMultipleComplaintID) {
        pettionObject.complaintids = [pettionObject.complaintids];
      }
      this.savePetition(pettionObject);
    } else {
      this._alertService.error('petition already exists');
    }
  }

  savePetition(petitionObject) {
    const serverPetition = this.createServerPettionObject(petitionObject);

    this._commonHttpService.create(serverPetition, NewUrlConfig.EndPoint.Intake.createPetition).subscribe(
      (response) => {
        console.log('response', response);
        this._alertService.success('Pettion added sucessfully');
        // petitionObject.intakeservicerequestpetitionid = response.intakeservicerequestpetition.intakeservicerequestpetitionid;
        // petitionObject.offenses = this.reMapOffencesInEvaluationFeilds(petitionObject.complaintids);
        // petitionObject.offensedate = this.reMapOffenceDateInEvaluationFeilds(petitionObject.complaintids);
        // this.petitions.push(petitionObject);
        this.loadAddedPetitions();
        this.filterComplaintIDs();
        this.petitionDetailsForm.reset();
      });
  }

  reMapOffencesInEvaluationFeilds(complaintids: string[]) {
    let offences = [];
    if (complaintids && complaintids.length > 0) {
      complaintids.forEach(complaintid => {
        const evalFeild = this.evalFields.find(evalField => evalField.complaintid === complaintid);
        const evaloffences = evalFeild.intakeservicerequestevaluationconfig.map(offence => offence.allegation);
        offences = [...offences, ...evaloffences];
      });
    }
    return offences;
  }

  reMapOffenceDateInEvaluationFeilds(complaintids: string[]) {
    const offencesDate = [];
    if (complaintids && complaintids.length > 0) {
      complaintids.forEach(complaintid => {
        const evalFeilds = this.evalFieldsInfo.filter(evalField => evalField.complaintid === complaintid);
        evalFeilds.map(evalField => evalField.allegedoffensedate).forEach(date => offencesDate.push(moment(date).format('MM/DD/YYYY')));
      });
    }
    return offencesDate;

  }

  createServerPettionObject(petitionObject) {
    const serverPetition: Petition = new Petition();
    serverPetition.intakenumber = this.intakeNumber;
    serverPetition.petitionid = petitionObject.petitionid;
    serverPetition.petitiontypekey = petitionObject.pettitiontype;
    serverPetition.evaluationfields = petitionObject.complaintids.map(complaint => {
      return { 'intakeservicerequestevaluationid': complaint.intakeservicerequestevaluationid };
    });
    const intakeJson = this._dataStoreService.getData(MyNewintakeConstants.Intake.intakeModel);
    let youth = null;
    if (intakeJson && intakeJson.persons && intakeJson.persons.length) {
      youth = intakeJson.persons.find(person => person.Role);
    }
    serverPetition.youth = youth;
    return serverPetition;
  }

  reMapEvalutionFeildIds(complaintids: string[]) {
    const evaluationFeildIds: { intakeservicerequestevaluationid: string }[] = [];
    if (complaintids && complaintids.length > 0) {
      complaintids.forEach(complaintid => {
        const evalFeild = this.evalFields.find(evalField => evalField.complaintid === complaintid);
        evaluationFeildIds.push({ intakeservicerequestevaluationid: evalFeild.intakeservicerequestevaluationid });
      });
    }

    return evaluationFeildIds;
  }

  deletePetition(deletePettion) {

    this.petitions = this.petitions.filter(pettion => pettion.petitionid !== deletePettion.petitionid);
    this.filterComplaintIDs();

  }

  isPetitionExist(pettionId) {
    const foundPetition = this.petitions.find(petition => petition.petitionid === pettionId);
    if (foundPetition) {
      return true;
    }

    return false;
  }

  prepareComplaintIDs() {
    if (this.evalFields && this.evalFields.length > 0) {
      this.complaintIDs = this.evalFields.map(evalFeild => evalFeild.complaintid);
    } else {
      this.complaintIDs = ['test01', 'test02'];
    }
    this.filterComplaintIDs();
  }

  checkForMultipleComplaintIdAvailable() {
    this.hasMultipleComplaintID = this.filteredComplaintIDs.length > 1;
  }

  getAssociatedComplaintPetitionList() {
    this._commonHttpService
      .getArrayList({
        where: {
          intakenumber: this.intakeNumber
        },
        nolimit: true, method: 'get'
      },
        NewUrlConfig.EndPoint.Intake.complaintAssociatedList + '?filter').subscribe(response => {
          console.log('asccl', response);
          this.associatedComplaintList = response;
        });
  }

  filterComplaintIDs() {
    const addedComplaintIDs = [];
    this._commonHttpService
      .getArrayList({
        where: {
          intakenumber: this.intakeNumber
        },
        nolimit: true, method: 'get'
      },
        NewUrlConfig.EndPoint.Intake.complaintList + '?filter').subscribe(response => {
          console.log('cl', response);
          this.filteredComplaintIDs = response;
          this.checkForMultipleComplaintIdAvailable();

        });
    this.getAssociatedComplaintPetitionList();
    /* this.petitions.map(petttion => {
      if (petttion.complaints) {
        petttion.complaints.map(complaintID => {
          addedComplaintIDs.push(complaintID.complaintid);
        });
      }
    });
    this.filteredComplaintIDs = this.complaintIDs.filter(complaintID => {
      const isNotExist = addedComplaintIDs.indexOf(complaintID) === -1;
      if (isNotExist) {
        return true;
      } else {
        return false;
      }

    });*/
  }
  loadEvaluationFeildList() {
    this._commonHttpService
      .getArrayList({
        where: {
          intakenumber: this.intakeNumber
        },
        nolimit: true, method: 'get'
      },
        NewUrlConfig.EndPoint.Intake.evaluationFieldList + '?filter').subscribe(response => {
          this.evalFields = response;
          this.prepareComplaintIDs();
        });
  }
  getCompalintIdDetails(complaintid, petition, fromPopup) {
    this.selectedPetitionId = petition.petitionid;
    this.fromPopup = fromPopup ? fromPopup : false;
    // petition.offenses = petition && petition.offenses ? petition.offenses : [];
    // this.complaintDetails = this.evalFieldsInfo.find(evalField => evalField.complaintid === complaintid);
    // this.complaintDetails.offenses = petition.offenses.map(offence => offence.name);
    // this.selectedVictims = this.getVictims(this.complaintDetails.victims);
    this._commonHttpService
      .getArrayList({
        where: {
          intakeservicerequestevaluationid: complaintid
        },
        method: 'post'
      },
        NewUrlConfig.EndPoint.Intake.getComplaintDetails).subscribe(response => {
          const complaints: any = response;
          this.complaintDetails = complaints.data[0];
          console.log('complaint details', this.complaintDetails);
          if (fromPopup) {
            (<any>$('#associate-petition')).modal('hide');
          }
          (<any>$('#view-petition')).modal('show');
        });
  }

  closeComplaintPopup() {
    if (this.fromPopup) {
      (<any>$('#associate-petition')).modal('show');
    }
    (<any>$('#view-petition')).modal('hide');
  }

  getPerson(personId: string) {
    const person = this.addedPersons.find(p => p.Pid === personId);
    if (person) {
      return person;
    }
    return null;
  }
  getVictims(victims) {
    const victimsList = [];
    if (!victims) {
      return victimsList;
    }
    victims.forEach(victimid => {
      const victim = this.getPerson(victimid);
      victimsList.push(victim);
    });
    return victimsList;
  }
  broadCastPettions() {
    this._dataStoreService.setData(MyNewintakeConstants.Intake.petitionDetails, this.petitions);
  }

  routToIntake(intakenumber) {
    const currentUrl = '/pages/newintake/my-newintake/' + intakenumber;
    this._router.navigate([currentUrl]);
  }

  showPettionDetails(pettion) {
    if (this.isVOP(pettion)) {
      this.selectedPetition = pettion;

      this.getVOPPettionDetials(pettion).subscribe(response => {
        if (response && response.length > 0) {
          this.selectedProbation = response[0];
          (<any>$('#probation-popup')).modal('show');
        }
      });
    }

  }

  isVOP(petition) {
    if (petition && petition.petitionid.indexOf('VOP') !== -1) {
      return true;
    }
    return false;
  }

  getVOPPettionDetials(pettion) {
    return this._commonHttpService
      .getArrayList({
        where: {
          intakenumber: this.intakeNumber,
          intakeservicerequestpetitionid: pettion.intakeservicerequestpetitionid
        },
        nolimit: true, method: 'get'
      },
        CommonUrlConfig.EndPoint.Intake.VOPPetitionDetails + '?filter');
  }
}
