import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ResourceConsultComponent } from './resource-consult.component';


describe('ResourceConsultComponent', () => {
  let component: ResourceConsultComponent;
  let fixture: ComponentFixture<ResourceConsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceConsultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
