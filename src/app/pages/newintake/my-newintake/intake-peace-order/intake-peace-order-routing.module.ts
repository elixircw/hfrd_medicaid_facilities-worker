import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakePeaceOrderComponent } from './intake-peace-order.component';

const routes: Routes = [{
  path: '',
  component: IntakePeaceOrderComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakePeaceOrderRoutingModule { }
