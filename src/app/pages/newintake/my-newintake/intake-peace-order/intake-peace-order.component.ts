import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService, AuthService, AlertService, DataStoreService } from '../../../../@core/services';
import { IntakeConfigService } from '../intake-config.service';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { EvaluationFields } from '../_entities/newintakeSaveModel';
import { Subscription } from 'rxjs/Subscription';
import { IntakeStoreConstants } from '../my-newintake.constants';
import { NewUrlConfig } from '../../newintake-url.config';
import { EvaluationOffenseLocationTypeList, InvolvedPerson } from '../_entities/newintakeModel';
import * as moment from 'moment';
import { AppConstants } from '../../../../@core/common/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'intake-peace-order',
  templateUrl: './intake-peace-order.component.html',
  styleUrls: ['./intake-peace-order.component.scss']
})
export class IntakePeaceOrderComponent implements OnInit, AfterViewInit {

  evalForm: FormGroup;
  deleteIndex: number;
  incidentDateOption: number;
  mdCountys$: Observable<DropdownModel[]>;
  offenceCategories$: Observable<any[]>;
  offenceCategoryList = [];
  victimArray: any[] = [];
  oldDate = {
    beginDate: '',
    endDate: ''
  };
  maxDate = new Date();
  roleName: AppUser;
  evals: EvaluationFields[] = [];
  evalAction: string;
  isViewMode: boolean;
  editIndex: number;
  youthAge: string;
  victims: any = [];
  baseUrl = '';
  token: AppUser;
  purposeID = '';
  selectedPurpose = '';
  enableAdd = false;
  enableWaiverField = false;
  store: any;
  dataStroeSubscription: Subscription;
  addedPersons: InvolvedPerson[] = [];
  evaluationOffenseLocationTypeList$: Observable<EvaluationOffenseLocationTypeList[]>;
  constructor(private _router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _datastore: DataStoreService,
    private _intakeConfig: IntakeConfigService) {
    this.store = this._datastore.getCurrentStore();
  }

  ngOnInit() {
    this.initEvalForm();
    this.processSavedPeaceOrder();
    this.processPersons();
    this.dataStroeSubscription = this._datastore.currentStore.subscribe(store => {
      if (store[IntakeStoreConstants.purposeSelected]) {
        const purposeSelected = store[IntakeStoreConstants.purposeSelected];
        const storePurposeId = purposeSelected.value;
        if (storePurposeId !== this.purposeID) {
          this.purposeID = storePurposeId;
          this.listAllegations(this.purposeID);
          this.selectedPurpose = purposeSelected.text;
          this.enableAdd = true;
        }
      }

    });
  }

  ngAfterViewInit() {
      if (this._authService.isDJS() && !this._intakeConfig.getiseditIntake()) {
          (<any>$(':button')).prop('disabled', true);
          (<any>$('span')).css({'pointer-events': 'none',
                      'cursor': 'default',
                      'opacity': '0.5',
                      'text-decoration': 'none'});
      }
  }
  initEvalForm() {
    this.evalForm = this.formBuilder.group({

      complaintid: [''],
      offenselocation: [''],
      complaintreceiveddate: [''],
      zipcode: [''],
      countyid: [null],
      countyname: [''],
      allegedoffense: [[]],
      allegedoffenseknown: [0],
      allegedoffensedate: [''],
      allegedoffensetime: ['08:00'],
      victims: [[]],
      begindate: [''],
      begintime: ['08:00'],
      enddate: [''],
      endtime: ['08:00'],
      evaluationsourceagencyname: [''],
      evaluationsourcetypename: [''],
      victimname: [[]],
      unknownrange: false,
      dateRange: false,
      evaluationsourceid: null, // temp id need to change once api deployed with id
      offencelocationtypekey: '',
      narrative: [''],
      age: [this.youthAge ? this.youthAge : '']
    });

  }
  resetEvalForm() {
    this.deleteIndex = -1;
    this.victimArray = [];
    this.incidentDateOption = 0;
    this.isViewMode = false;
    this.evalForm.reset();
  }
  openPeaceOrderForm() {

    this.resetEvalForm();
    this.initEvalForm();
    this.loadDropdowns();
    this.listAllegations(this.purposeID);
    this.loadOffenseLocationType();
    setTimeout(() => {
      this.evalForm.enable();
      this.evalAction = 'add';
    }, 10);
    (<any>$('#peace-order-form-popup')).modal('show');
  }

  processPersons() {
    const persons = this.store[IntakeStoreConstants.addedPersons];
    if (persons && Array.isArray(persons)) {
      this.addedPersons = persons;
      this.setYouthAge();
      this.findAndLoadVictims();
    } else {
      this.addedPersons = [];
    }

  }

  private findAndLoadVictims() {
    const victimPersons = this.addedPersons.filter(person => person.Role === 'Victim');
    if (victimPersons.length > 0) {
      this.victims = victimPersons.map((person, index) => {
        if (person.Role === 'Victim') {
          return {
            personid: person.Pid ? person.Pid : AppConstants.PERSON.TEMP_ID + index,
            firstName: person.Firstname,
            lastName: person.Lastname,
            fullName: person.fullName
          };
        }
      });
    } else {
      this.victims = [];
    }
  }
  onIncidentDateOptionChange(event: boolean, option: string) {
    if (option === 'range' && event) {
      this.evalForm.get('allegedoffensedate').clearValidators();
      this.evalForm.get('allegedoffensedate').updateValueAndValidity();
      this.evalForm.get('allegedoffensetime').clearValidators();
      this.evalForm.get('allegedoffensetime').updateValueAndValidity();
      this.incidentDateOption = 2;
      this.evalForm.patchValue({
        unknownrange: false,
        allegedoffenseknown: 2,
        begindate: this.oldDate.beginDate,
        enddate: this.oldDate.endDate,
        begintime: '',
        endtime: ''
      });
    } else if (option === 'range' && !event) {
      this.evalForm.get('allegedoffensedate').setValidators([Validators.required]);
      this.evalForm.get('allegedoffensedate').updateValueAndValidity();
      this.evalForm.get('allegedoffensetime').setValidators([Validators.required]);
      this.evalForm.get('allegedoffensetime').updateValueAndValidity();
      this.incidentDateOption = 0;
      this.evalForm.patchValue({
        unknownrange: false,
        allegedoffenseknown: 0,
        allegedoffensedate: this.oldDate.beginDate,
        allegedoffensetime: ''
      });
    }
    this.setYouthAge();
  }

  loadDropdowns() {
    this.mdCountys$ = this._commonHttpService.getArrayList({
      where: { state: 'MD' }, order: 'countyname asc',
      nolimit: true, method: 'get'
    }, NewUrlConfig.EndPoint.Intake.MDCountryListUrl + '?filter');
  }
  CreateOrUpdate() {
    const val = this.evalForm.getRawValue();
    if (!val.complaintid) {
      val.complaintid = `PO-${new Date().getTime()}`;
    }
    val.ispeaceorder = true;
    val.victims = val.victims.map(victimid => this.victims.find(victim => victim.personid === victimid));
    if (val.dateRange) {
      const begintimeSplit = val.begintime.split(':');
      if (!(val.begindate instanceof Date)) {
        val.begindate = new Date(val.begindate);
      }
      val.begindate.setHours(begintimeSplit[0]);
      val.begindate.setMinutes(begintimeSplit[1]);
      const endtimeSplit = val.endtime.split(':');
      if (!(val.enddate instanceof Date)) {
        val.enddate = new Date(val.enddate);
      }
      val.enddate.setHours(endtimeSplit[0]);
      val.enddate.setMinutes(endtimeSplit[1]);
    } else {
      const allegedoffensetimeSplit = val.allegedoffensetime.split(':');
      if (!(val.allegedoffensedate instanceof Date)) {
        val.allegedoffensedate = new Date(val.allegedoffensedate);
      }
      val.allegedoffensedate.setHours(allegedoffensetimeSplit[0]);
      val.allegedoffensedate.setMinutes(allegedoffensetimeSplit[1]);
    }
    if (this.evalForm.valid) {
      if (this.evalAction === 'edit') {
        if (this.editIndex >= 0) {
          this.evals[this.editIndex] = val;
        }
      } else if (this.evalAction === 'add') {
        this.evals.push(val);
      }
      this.broadCastEvaluationFields();
      (<any>$('#peace-order-form-popup')).modal('hide');
      this.resetEvalForm();
    } else {
      this._alertService.warn('Please fill mandatory details');
    }
  }
  viewEdit(data: any, isView: boolean, index: number) {

    this.resetEvalForm();
    this.loadDropdowns();
    const EvalData = JSON.parse(JSON.stringify(data));
    this.onIncidentDateOptionSet(EvalData.dateRange, 'range');
    this.onIncidentDateOptionSet(EvalData.unknownrange, 'unknown');
    if (EvalData.dateRange) {
      EvalData.begintime = moment(EvalData.begindate).format('HH:mm');
      EvalData.endtime = moment(EvalData.enddate).format('HH:mm');
      this.evalForm.get('allegedoffensedate').clearValidators();
      this.evalForm.get('allegedoffensedate').updateValueAndValidity();
      this.evalForm.get('allegedoffensetime').clearValidators();
      this.evalForm.get('allegedoffensetime').updateValueAndValidity();
    } else {
      this.evalForm.get('allegedoffensedate').setValidators([Validators.required]);
      this.evalForm.get('allegedoffensedate').updateValueAndValidity();
      this.evalForm.get('allegedoffensetime').setValidators([Validators.required]);
      this.evalForm.get('allegedoffensetime').updateValueAndValidity();
      EvalData.allegedoffensetime = moment(EvalData.allegedoffensedate).format('HH:mm');
    }
    EvalData.victims = data.victims.map(vicitm => vicitm.personid);
    this.evalForm.patchValue(EvalData);
    this.evalForm.patchValue({ 'victims': EvalData.victims });
    this.evalForm.patchValue({ 'narrative': EvalData.narrative });
    this.editIndex = index;
    this.loadOffenseLocationType();
    this.listAllegations(this.purposeID);
    setTimeout(() => {
      if (isView) {
        this.evalForm.disable();
        this.evalAction = 'view';
      } else {
        this.evalForm.enable();
        this.evalAction = 'edit';
      }
    }, 10);
    (<any>$('#peace-order-form-popup')).modal('show');

  }
  private loadOffenseLocationType() {
    this.evaluationOffenseLocationTypeList$ = this._commonHttpService
      .getArrayList(
        {
          order: 'description asc',
          method: 'get'
        },
        NewUrlConfig.EndPoint.Intake.OffenseLoactionType + '?filter'
      );
  }
  setCounty(county) {
    this.evalForm.patchValue({
      countyname: county.countyname,
    });
  }
  setLocationOfOffence(location) {
    this.evalForm.patchValue({
      offenselocation: location.description,
    });
  }
  onIncidentDateOptionSet(event: boolean, option: string) {
    if (option === 'range' && event) {
      this.incidentDateOption = 2;
    } else if (option === 'unknown' && event) {
      this.incidentDateOption = 1;
    }
    this.setYouthAge();
  }
  broadCastEvaluationFields() {
    this._datastore.setData(IntakeStoreConstants.evalFields, this.evals);
  }

  processSavedPeaceOrder() {
    const evaluations = this._datastore.getData(IntakeStoreConstants.evalFields);
    if (evaluations && Array.isArray(evaluations)) {
      evaluations.forEach(evalFeilds => {
        if (evalFeilds.allegedoffense) {
          evalFeilds.allegedoffense = evalFeilds.allegedoffense.map(offence => {
            if (offence.allegationid) {
              return offence.allegationid;
            } else {
              return offence;
            }
          });
        }
      });
      this.evals = evaluations;
    } else {
      this.evals = [];
    }

  }

  listAllegations(purposeID) {
    const checkInput = {
      where: { intakeservreqtypeid: purposeID },
      method: 'get',
      nolimit: true,
      order: 'name'
    };
    this._commonHttpService
      .getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter')
      .subscribe(offenceCategories => {
        if (offenceCategories) {
          offenceCategories.forEach(offence => {
            this.offenceCategoryList[offence.allegationid] = offence.name;
          });
          this.offenceCategories$ = Observable.of(offenceCategories);
        }
      });
  }
  deleteEval(deleteIndex) {
    this.deleteIndex = deleteIndex;
    // this.evals.splice(deleteIndex, 1);
    // this.broadCastEvaluationFields();
  }

  onDeleteEval() {
    if (this.deleteIndex !== -1) {
      this.evals.splice(this.deleteIndex, 1);
      this.broadCastEvaluationFields();
      this.resetEvalForm();
      (<any>$('#eval-delete-popup')).modal('hide');
      this._alertService.success('Peace order Deleted Successfully');
    }
  }

  private setYouthAge() {
    this.youthAge = this.getYouthAge();
    if (this.youthAge) {
      this.evalForm.patchValue({
        age: this.youthAge
      });
    }
    // this.evalForm.patchValue({
    //     age: youthAge,
    // });
  }

  private getYouthAge() {
    const intakeEvalForm = this.evalForm ? this.evalForm.getRawValue() : undefined;
    if (this.addedPersons && this.addedPersons.length > 0) {
      const youth = this.addedPersons.find((p) => p.Role === 'Youth');
      if (!youth || (youth && !youth.Dob)) {
        return null;
      }
      let youthDob;
      let offenceDate;
      youthDob = new Date(youth.Dob);
      if (!intakeEvalForm.unknownrange && (intakeEvalForm.allegedoffensedate || intakeEvalForm.beginDate)) {
        offenceDate = intakeEvalForm.dateRange ? intakeEvalForm.begindate : intakeEvalForm.allegedoffensedate;
        offenceDate = new Date(offenceDate);
      } else {
        offenceDate = new Date();
      }

      return this.getAgeInYearsAndMonth(offenceDate, youthDob);
    } else {
      return null;
    }
  }

  private getAgeInYearsAndMonth(offenceDate, youthDob) {
    // const timeDiff = offenceDate - youthDob;
    // const youthAge = new Date(timeDiff); // miliseconds from epoch

    // const offenseMonth = offenceDate.getMonth();
    // const youthMonth = youthDob.getMonth();

    // const monthDiff = offenseMonth - youthMonth;

    // let months = (monthDiff >= 0) ? (monthDiff) : (monthDiff + 12);

    // months = (offenceDate.getDate() < youthDob.getDate()) ? ((months - 1)) : months;

    // let years = Math.abs(youthAge.getUTCFullYear() - 1970);
    // if (months < 0) {
    //   months = 11;
    //   years = years - 1;
    // }

    // return years.toString() + ' Years ' + ((months > 0) ? months + ' Months' : '');

    const years = offenceDate ? moment(offenceDate).diff(youthDob, 'years', false) : moment().diff(youthDob, 'years', false);
    const totalMonths = offenceDate ? moment(offenceDate).diff(youthDob, 'months', false) : moment().diff(youthDob, 'months', false);
    const months = totalMonths - (years * 12);
    console.log(years, (years * 12), totalMonths, months);
    return `${years} Years ${months} month(s)`;
  }



}
