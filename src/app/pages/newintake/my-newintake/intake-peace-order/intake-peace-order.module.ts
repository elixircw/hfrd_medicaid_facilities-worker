import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakePeaceOrderRoutingModule } from './intake-peace-order-routing.module';
import { IntakePeaceOrderComponent } from './intake-peace-order.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxMaskModule } from 'ngx-mask';
import { QuillModule } from 'ngx-quill';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    IntakePeaceOrderRoutingModule,
    FormMaterialModule,
    NgxMaskModule.forRoot(),
    QuillModule,
    MatTooltipModule
  ],
  declarations: [IntakePeaceOrderComponent]
})
export class IntakePeaceOrderModule { }
