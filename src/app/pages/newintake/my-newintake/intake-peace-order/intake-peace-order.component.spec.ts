import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakePeaceOrderComponent } from './intake-peace-order.component';

describe('IntakePeaceOrderComponent', () => {
  let component: IntakePeaceOrderComponent;
  let fixture: ComponentFixture<IntakePeaceOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakePeaceOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakePeaceOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
