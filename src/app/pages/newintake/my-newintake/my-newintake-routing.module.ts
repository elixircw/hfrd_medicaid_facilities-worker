import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyNewintakeComponent } from './my-newintake.component';
import { MyNewintakeResolverService } from './my-newintake-resolver.service';
import { PurposeResolverService } from './purpose-resolver.service';
import { AppConstants } from '../../../@core/common/constants';
import { RoleGuard } from '../../../@core/guard';
import { PersonResolverService } from './person-resolver.service';
import { CommunicationResolverService } from './communication-resolver.service';

const routes: Routes = [
    {
        path: '',
        component: MyNewintakeComponent,
        resolve: {
            intake: MyNewintakeResolverService,
            communicationList: CommunicationResolverService,
            purposeList: PurposeResolverService,
            involvedPersonsList : PersonResolverService
        },
        canActivate: [RoleGuard],
        children: [
            {
                path: 'ast',
                loadChildren: './intake-adult-screen-tool/intake-adult-screen-tool.module#IntakeAdultScreenToolModule',
            },
            {
                path: 'appointment',
                loadChildren: './intake-appointments/intake-appointments.module#IntakeAppointmentsModule',
            },
            {
                path: 'assessment',
                loadChildren: './intake-assessment/intake-assessment.module#IntakeAssessmentModule',
            },
            {
                path: 'attachment',
                loadChildren: './intake-attachments/intake-attachments.module#IntakeAttachmentsModule',
            },
            {
                path: 'contact',
                loadChildren: '../../case-worker/dsds-action/recording/recording.module#RecordingModule',
            },
            {
                path: 'complaint-type',
                loadChildren: './intake-complaint-type/intake-complaint-type.module#IntakeComplaintTypeModule',
            },
            {
                path: 'cross-reference',
                loadChildren: './intake-cross-refference/intake-cross-refference.module#IntakeCrossRefferenceModule',
            },
            {
                path: 'decision',
                loadChildren: './intake-decision/intake-decision.module#IntakeDecisionModule',
            },
            {
                path: 'disposition',
                loadChildren: './intake-disposition/intake-disposition.module#IntakeDispositionModule',
            },
            {
                path: 'djs-notes',
                loadChildren: './intake-djs-notes/intake-djs-notes.module#IntakeDjsNotesModule',
            },
            {
                path: 'doc-creator',
                loadChildren: './intake-document-creator/intake-document-creator.module#IntakeDocumentCreatorModule',
            },
            {
                path: 'entity',
                loadChildren: './intake-entities/intake-entities.module#IntakeEntitiesModule',
            },
            {
                path: 'evaluation',
                loadChildren: './intake-evaluation-fields/intake-evaluation-fields.module#IntakeEvaluationFieldsModule',
            },
            {
                path: 'peace-order',
                loadChildren: './intake-peace-order/intake-peace-order.module#IntakePeaceOrderModule',
            },
            {
                path: 'legal-action',
                loadChildren: './intake-legal-action/intake-legal-action.module#IntakeLegalActionModule',
            },
            {
                path: 'person',
                loadChildren: './intake-persons-involved/intake-persons-involved.module#IntakePersonsInvolvedModule',
            },
            {
                path: 'person-cw',
                loadChildren: '../../shared-pages/involved-persons/involved-persons.module#InvolvedPersonsModule',
                data: { source: AppConstants.MODULE_TYPE.INTAKE }
            },
            {
                path: 'sao-hearing',
                loadChildren: './intake-sao-hearing-details/intake-sao-hearing-details.module#IntakeSaoHearingDetailsModule',
            },
            {
                path: 'sao-petition',
                loadChildren: './intake-sao-petition-details/intake-sao-petition-details.module#IntakeSaoPetitionDetailsModule',
            },
            {
                path: 'sao-response',
                loadChildren: './intake-sao-response/intake-sao-response.module#IntakeSaoResponseModule',
            },
            {
                path: 'sao-schedule',
                loadChildren: './intake-sao-scheduled-hearing/intake-sao-scheduled-hearing.module#IntakeSaoScheduledHearingModule',
            },
            {
                path: 'sdm',
                loadChildren: './intake-sdm/intake-sdm.module#IntakeSdmModule',
            },
            {
                path: 'service-type',
                loadChildren: './intake-service-type/intake-service-type.module#IntakeServiceTypeModule',
            },
            {
                path: 'social-history',
                loadChildren: './intake-social-history-inr/intake-social-history-inr.module#IntakeSocialHistoryInrModule',
            },
            {
                path: 'intake-referral',
                loadChildren: './intake-referral/intake-referral.module#IntakeReferralModule',
            },
            {
                path: 'narrative',
                loadChildren: './newintake-narrative/newintake-narrative.module#NewintakeNarrativeModule',
            },
            {
                path: 'payment-schedule',
                loadChildren: 'app/pages/shared-pages/payment-schedule/payment-schedule.module#PaymentScheduleModule',
                data: {
                    pageSource: AppConstants.PAGES.INTAKE_PAGE
                }
            },
            {
                path: 'placement',
                loadChildren: './intake-placements/intake-placements.module#IntakePlacementsModule'
            },
            {
                path: 'roa',
                loadChildren: './intake-roa/intake-roa.module#IntakeRoaModule',
            },
            {
                path: 'history-clearance',
                loadChildren: './intake-history-clearance/intake-history-clearance.module#IntakeHistoryClearanceModule'
            },
            { path: 'relationship', loadChildren: '../../shared-pages/relationship-new/relationship-new.module#RelationshipNewModule' }
            ],
            data: {
                title: ['MDTHINK - Case Intake'],
                desc: 'Maryland department of human services',
                screen: { current: 'intake', key: 'myintake-module', includeMenus: true,  modules: [], skip: false }
            }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MyNewintakeRoutingModule { }
