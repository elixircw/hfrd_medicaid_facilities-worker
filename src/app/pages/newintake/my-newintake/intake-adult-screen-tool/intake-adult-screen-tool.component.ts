import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService, CommonHttpService, AuthService, CommonDropdownsService, AlertService } from '../../../../@core/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IntakeStoreConstants } from '../my-newintake.constants';
import { InvolvedPerson, SubType } from '../_entities/newintakeModel';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { Subject } from 'rxjs/Subject';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { NewUrlConfig } from '../../newintake-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as moment from 'moment';
import { CountyDetail } from '../../../case-worker/dsds-action/as-maltreatment-information/_entities/investigation-finding.data.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'intake-adult-screen-tool',
  templateUrl: './intake-adult-screen-tool.component.html',
  styleUrls: ['./intake-adult-screen-tool.component.scss']
})
export class IntakeAdultScreenToolComponent implements OnInit {
  adultScreenFormGroup: FormGroup;
  userIntakeInfo: AppUser;
  currentDate: Date;
  daTypeKey: any;
  store: any;
  invlovedPersons: InvolvedPerson[] = [];
  allegedVictimList: InvolvedPerson[] = [];
  otherPersonList: InvolvedPerson[] = [];
  personNameList = [];
  formComplete: boolean;
  isMaltreatorRequried = true;
  isUnknowRequried: boolean;
  intakeWorker: string;
  roleId: AppUser;
  isPreIntake = false;
  reviewStatus$ = new Subject<string>();
  responseDropdownList$ = new Observable<DropdownModel[]>();
  subServiceTypes: SubType[];
  @Input() reviewStatus: string;
  subServiceTypes$: Observable<DropdownModel[]>;
  subServiceDropDownTypes$: Observable<DropdownModel[]>;
  disableButton: boolean;
  intakeServId: string;
  subTypeKey: string;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  genderDropdownItems$: Observable<DropdownModel[]>;
  racetypeDropdownItems$: Observable<DropdownModel[]>;
  maritalDropdownItems$: Observable<DropdownModel[]>;
  localDssCountys$: Observable<CountyDetail[]>;
  houseHold = ([] = []);
  houseHold$: Observable<Array<any>>;
  loadAnonymousYesorNo = [
    { 'id': 'Y', 'name': 'Yes' },
    { 'id': 'N', 'name': 'No' }
  ];
  AdultScreenYesOrNo = [
    { 'id': 'Y', 'name': 'Yes' },
    { 'id': 'N', 'name': 'No' },
    { 'id': 'U', 'name': 'Unknown' }
  ];
  adultRisks$: Observable<DropdownModel[]>;
  supportNetwork$: Observable<DropdownModel[]>;
  supportNetworkScore= 0;
  environmentalRiskScore= 0;
  healthcareScore= 0;
  transportationScore= 0;
  clutterScore= 0;
  foodScore= 0;
  utilitiesScore= 0;
  supervisionScore= 0;
  eatingSelectScore = 0;
  mobilityScore = 0;
  bathingScore = 0;
  dressingScore = 0;
  toiletingScore = 0;
  mentalRiskScore = 0;
  physicalRiskScore = 0;
  dementiaScore = 0;
  disorderScore = 0;
  substanceAbuseScore = 0;
  moodDisorderScore = 0;
  behavioralScore = 0;
  takingMedicationScore= 0;
  riskScore: any;
  riskLevel: string;

  riskLevels = [
    { 'id': 'L', 'name': 'Low' },
    { 'id': 'M', 'name': 'Moderate' },
    { 'id': 'H', 'name': 'Value' }
  ];
  physicalAbuse: any;
  selfNeglect: any;
  neglectedByOthers: any;
  exploitation: any;
  sexExploitation: any;
  physicalEnvironment: any;
  intakeRecommendation: string;
  isAS: boolean;
  isUnknow: boolean;


  constructor(private _dataStoreService: DataStoreService, private _commonHttpService: CommonHttpService,
    private formBuilder: FormBuilder, private _authService: AuthService, private dropdownService: CommonDropdownsService, private _alertService: AlertService) {
    this.store = this._dataStoreService.getCurrentStore();
  }

  ngOnInit() {
    this.buildFormGroup();
    this.disabledClintInfo();
    const purposeSelected = this._dataStoreService.getData(IntakeStoreConstants.purposeSelected);
    const narrativeData = this.store.addNarrative;
    if (narrativeData) {
      this.adultScreenFormGroup.patchValue({
        referralname: narrativeData.Firstname + ', ' + narrativeData.Lastname,
        referraladdress1: narrativeData.requesteraddress1,
        referraladdress2: narrativeData.requesteraddress2,
        referralphone: narrativeData.PhoneNumber,
        referralremainanonymous: narrativeData.IsAnonymousReporter,
        detailsofreferralcomments: narrativeData.Narrative? narrativeData.Narrative.replace(/<(.|\n)*?>/g, '') : ''
      });
    }
    if (purposeSelected) {
      this.daTypeKey = purposeSelected.value;
      this.loadResponseTime();
    }

    this.invlovedPersons = this._dataStoreService.getData(IntakeStoreConstants.addedPersons);
    this.clientLoadDropDown();
    this.loadlocalDssCountys();
    this.loadRiskMeasure();
    this.loadSupportNetwork();
    this.riskScoreUpdate();
    const astData = this.store[IntakeStoreConstants.adultScreenTool];
    this._dataStoreService.currentStore.subscribe((item) => {
      if (item[IntakeStoreConstants.addedPersons]) {
        this.patchClintInfo(item[IntakeStoreConstants.addedPersons]);
        this.patchHouseHold(item[IntakeStoreConstants.addedPersons]);
      }
      if (item[IntakeStoreConstants.adultScreenTool]) {
        this.patchForm(item[IntakeStoreConstants.adultScreenTool]);
        if (item[IntakeStoreConstants.adultScreenTool].unknow) {
          this.isUnknow = true;
          this.isUnknowRequried = true;
         }
         if (item[IntakeStoreConstants.adultScreenTool].allegedMaltreator && item[IntakeStoreConstants.adultScreenTool].allegedMaltreator.length) {
           this.isMaltreatorRequried = false;
           this.isUnknowRequried = false;
           this.isUnknow = true;
         }
      }
    });
    if (this.userIntakeInfo.user.userprofile.userprofileaddress && this.userIntakeInfo.user.userprofile.userprofileaddress.length) {
      this.adultScreenFormGroup.controls['countyid'].patchValue(this.userIntakeInfo.user.userprofile.userprofileaddress[0].countyid);
    }
    this.disableButton = (astData && astData.isComplete) ? true : false;
    if (astData && astData.isComplete) {
      this.adultScreenFormGroup.disable();
      // this.formComplete = true;
    }
    this.adultScreenFormGroup.get('astdate').disable();
    this.adultScreenFormGroup.get('intakeworker').disable();
    if (this.userIntakeInfo.user.userprofile.teamtypekey === 'AS') {
      this.isAS = true;
      const invlovedPersons = this._dataStoreService.getData(IntakeStoreConstants.addedPersons);
      if (invlovedPersons && invlovedPersons.length) {
        this.allegedVictimList = invlovedPersons.filter((item) => item.Role === 'RA' || item.Role === 'AV');
        this.otherPersonList = invlovedPersons.filter((item) => !(item.Role === 'RA' || item.Role === 'AV'));
      }
    }
  }

  buildFormGroup() {
    this.adultScreenFormGroup = this.formBuilder.group({
      // DefaultTop
      astdate: '',
      intakeworker: [''],
      countyid: [''],
      // REFERRAL SOURCE
      referralname: [''],
      referraladdress1: [''],
      referraladdress2: [''],
      referralphone: [''],
      referralremainanonymous: [null],
      referralrelationship: [''],
      // CLIENT INFORMATION
      clientinfoname: [''],
      clientinfodob: [''],
      clientinfoage: [''],
      gendertypekey: [''],
      clientinfoaddress1: [''],
      clientinfoaddress2: [''],
      clientinfophone: [''],
      maritalstatuskey: [''],
      monthlyincome: [''],
      monthlyincomesource: [''],
      totalassets: [''],
      totalassetssource: [''],
      racetypekey: [''],
      ethnicitytypekey: [''],
      additionalinfo: [''],
      // OTHERS IN HOUSEHOLD/INTERESTED OTHERS
      othersName: [''],
      othersRelationship: [''],
      othersPhone: [''],
      // Risk/Safety Factors for Investigator
      riskaggrpets: [''],
      riskfireharms: [''],
      riskcdsabuse: [''],
      riskdomviolence: [''],
      riskhomehazards: [''],
      riskpsychiatric: [''],
      riskmedical: [''],
      riskother: [''],
      riskcomments: [''],
      // DETAILS OF REFERRAL (NARRATIVE)
      detailsofreferralcomments: [''],
      // Maltreatment Type
      allegedVictim: [[]],
      allegedMaltreator: [[]],
      phyAbuseSelect: [''],
      selfNeglectSelect: [''],
      neglectedByOthersSelect: [''],
      exploitationSelect: [''],
      sexExploitationSelect: [''],
      physicalEnvironmentSelect: [''],
      subServiceType: [''],
      locationOfMalTreatment: [''],
      responseDetail: [''],
      unknow: [''],
      // ENVIRONMENTAL/EXTERNAL RISKS
      healthcarekey: [''],
      cluttertypekey: [''],
      housingtypekey: [''],
      transportationkey: [''],
      foodtypekey: [''],
      supervisiontypekey: [''],
      individualvulnerable: [''],
      // PHYSICAL/FUNCTIONAL RISKS
      eatingfeedingkey: [''],
      walkingkey: [''],
      dressingkey: [''],
      takingmedicationkey: [''],
      bathingkey: [''],
      toiletkey: [''],
      physicalrisksnotes: [''],
      // MENTAL/EMOTIONAL CHALLENGES
      dementiakey: [''],
      substanceabusekey: [''],
      behavioralissueskey: [''],
      thoughtdisorderskey: [''],
      mooddisorderskey: [''],
      mentalchallengesnotes: [''],
      // STABILITY OF SUPPORT/CAREGIVING NETWORK
      supportnetworkkey: [''],
      supportnetworknotes: [''],
      riskscore: [''],
      risklevel: [''],
      intakerecommendation: [''],
    });
    this.userIntakeInfo = this._authService.getCurrentUser();
    if (this.userIntakeInfo.user.userprofile.displayname) {
      this.intakeWorker = this.userIntakeInfo.user.userprofile.displayname;
      this.updateAdultScreen();
    }

  }

  updateAdultScreen() {
    this.currentDate = new Date();
    this.adultScreenFormGroup.patchValue({
      astdate: this.currentDate,
      intakeworker: this.intakeWorker
    });
  }

  // tslint:disable-next-line:member-ordering
  public allegationDropdownList = [
    { 'id': 1, 'name': 'INVESTIGATION RECOMMENDED' },
    { 'id': 2, 'name': 'NO INVESTIGATION' },
    { 'id': 3, 'name': 'UNKNOWN' }
  ];

  /**
   * Local Dss - County - Start
   */

  private loadlocalDssCountys() {
    this.localDssCountys$ = this._commonHttpService.getArrayList({
      where: { state: 'MD' }, order: 'countyname asc', nolimit: true, method: 'get' },
      CommonUrlConfig.EndPoint.Listing.CountryListUrl + '?filter').map(result => {
      return result;
    });
  }

   /**
    * Local Dss - County - End
    */

  /**
   * Client Information Tab - start
   */

  private clientLoadDropDown() {
    // const actortypeUrl = this.isDjs ? NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter' : NewUrlConfig.EndPoint.Intake.UserActorTypeUrl + '?filter';
    const source = forkJoin([
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true
            },
            NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true
            },
            NewUrlConfig.EndPoint.Intake.GenderTypeUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true,
                order: 'typedescription'
            },
            NewUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                order: 'typedescription'
            },
            NewUrlConfig.EndPoint.Intake.MaritalStatusUrl + '?filter'
        )
    ])
        .map((result) => {
            return {
                ethinicities: result[0].map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.ethnicgrouptypekey
                        })
                ),
                genders: result[1].map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.gendertypekey
                        })
                ),
                racetype: result[2].map(
                  (res) =>
                      new DropdownModel({
                          text: res.typedescription,
                          value: res.racetypekey
                      })
              ),
              maritalstatus: result[3].map(
                (res) =>
                    new DropdownModel({
                        text: res.typedescription,
                        value: res.maritalstatustypekey
                    })
            )


            };
        })
        .share();
        this.ethinicityDropdownItems$ = source.pluck('ethinicities');
        this.genderDropdownItems$ = source.pluck('genders');
        this.racetypeDropdownItems$ = source.pluck('racetype');
        this.maritalDropdownItems$ = source.pluck('maritalstatus');
      }

   /**
    * Client Information Tab - End
    */
   /**
    * House Hold Tab - Start
    */
   addHouseHold() {
    const name = this.adultScreenFormGroup.get('othersName').value;
    const relationship = this.adultScreenFormGroup.get('othersRelationship').value;
    const contactnumber = this.adultScreenFormGroup.get('othersPhone').value;
    if (name && relationship && contactnumber) {
        this.houseHold.push({
            contacttypeid: '',
            name: name,
            relationship: relationship,
            phone: contactnumber,
            isactive: 1
        });
        this.houseHold$ = Observable.of(this.houseHold);
        this.adultScreenFormGroup.get('othersName').reset();
        this.adultScreenFormGroup.get('othersRelationship').reset();
        this.adultScreenFormGroup.get('othersPhone').reset();
    } else {
        this._alertService.warn('Please fill all the fields');
       // ControlUtils.validateAllFormFields(this.adultScreenFormGroup);
        // ControlUtils.setFocusOnInvalidFields();
    }
  }
  deleteHouseHold(i: number) {
    this.houseHold.splice(i, 1);
    this.houseHold$ = Observable.of(this.houseHold);
  }
  /**
  * House Hold Tab - End
  */

  /**
   * Loading remaining tab - drop down - start
   *
   */

  private loadRiskMeasure() {

    const source = this._commonHttpService.getArrayList(
      {
        where: { teamtypekey: 'AS', referencetypeid: 43 },
        method: 'get',
        nolimit: true
      },
      CommonUrlConfig.EndPoint.Intake.adultScreenRiskMeasure
    ).map((result) => {
      return {
        response: result.map(
          (res) =>
            new DropdownModel({
              text: res.value_text,
              value: res.ref_key
            })
        )
      };
    }).share();

    this.adultRisks$ = source.pluck('response');
  }

  private loadSupportNetwork() {

    const source = this._commonHttpService.getArrayList(
      {
        where: { teamtypekey: 'AS', referencetypeid: 43 },
        method: 'get',
        nolimit: true
      },
      CommonUrlConfig.EndPoint.Intake.adultScreenRiskMeasure
    ).map((result) => {
      return {
        response: result.map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.ref_key
            })
        )
      };
    }).share();

    this.supportNetwork$ = source.pluck('response');
  }


  onHealthCare(item) {
    if (item.value === 'MINORP') {
      this.healthcareScore = 4;

     } else if (item.value === 'MODPRO') {
      this.healthcareScore = 10;

     } else if (item.value === 'NOP') {
      this.healthcareScore = 0;

     } else if (item.value === 'SERPRO') {
      this.healthcareScore = 16;

     } else if (item.value === 'UNKNOWN') {
      this.healthcareScore = 0.5;
     }
     this.onEnvironmentalRisk();
  }

  onTransportation(item) {
    if (item.value === 'MINORP') {
      this.transportationScore = 1;

     } else if (item.value === 'MODPRO') {
      this.transportationScore = 3;

     } else if (item.value === 'NOP') {
      this.transportationScore = 0;

     } else if (item.value === 'SERPRO') {
      this.transportationScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.transportationScore = 0.5;
     }
     this.onEnvironmentalRisk();
  }

  onClutter(item) {
    if (item.value === 'MINORP') {
      this.clutterScore = 1;

     } else if (item.value === 'MODPRO') {
      this.clutterScore = 3;

     } else if (item.value === 'NOP') {
      this.clutterScore = 0;

     } else if (item.value === 'SERPRO') {
      this.clutterScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.clutterScore = 0.5;
     }
     this.onEnvironmentalRisk();
  }

  onSelectFood(item) {
    if (item.value === 'MINORP') {
      this.foodScore = 1;

     } else if (item.value === 'MODPRO') {
      this.foodScore = 3;

     } else if (item.value === 'NOP') {
      this.foodScore = 0;

     } else if (item.value === 'SERPRO') {
      this.foodScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.foodScore = 0.5;
     }
     this.onEnvironmentalRisk();
  }

  onSelectSupervision(item) {
    if (item.value === 'MINORP') {
      this.supervisionScore = 1;

     } else if (item.value === 'MODPRO') {
      this.supervisionScore = 3;

     } else if (item.value === 'NOP') {
      this.supervisionScore = 0;

     } else if (item.value === 'SERPRO') {
      this.supervisionScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.supervisionScore = 0.5;
     }
     this.onEnvironmentalRisk();
  }

  onSelectUtilities(item) {
    if (item.value === 'MINORP') {
      this.utilitiesScore = 1;

     } else if (item.value === 'MODPRO') {
      this.utilitiesScore = 3;

     } else if (item.value === 'NOP') {
      this.utilitiesScore = 0;

     } else if (item.value === 'SERPRO') {
      this.utilitiesScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.utilitiesScore = 0.5;
     }
     this.onEnvironmentalRisk();
  }

  onEnvironmentalRisk() {
   this.environmentalRiskScore = this.healthcareScore + this.transportationScore + this.clutterScore + this.foodScore + this.utilitiesScore + this.supervisionScore;
   this.riskScoreUpdate();
  }

  onEatingSelect(item) {
    if (item.value === 'MINORP') {
      this.eatingSelectScore = 4;

     } else if (item.value === 'MODPRO') {
      this.eatingSelectScore = 10;

     } else if (item.value === 'NOP') {
      this.eatingSelectScore = 0;

     } else if (item.value === 'SERPRO') {
      this.eatingSelectScore = 16;

     } else if (item.value === 'UNKNOWN') {
      this.eatingSelectScore = 0.5;
     }
     this.onPhysicalRisk();
  }

  onTakingMedicationSelect(item) {
    if (item.value === 'MINORP') {
      this.takingMedicationScore = 4;

     } else if (item.value === 'MODPRO') {
      this.takingMedicationScore = 12;

     } else if (item.value === 'NOP') {
      this.takingMedicationScore = 0;

     } else if (item.value === 'SERPRO') {
      this.takingMedicationScore = 20;

     } else if (item.value === 'UNKNOWN') {
      this.takingMedicationScore = 0.5;
     }
     this.onPhysicalRisk();
  }

  onMobility(item) {
    if (item.value === 'MINORP') {
      this.mobilityScore = 4;

     } else if (item.value === 'MODPRO') {
      this.mobilityScore = 10;

     } else if (item.value === 'NOP') {
      this.mobilityScore = 0;

     } else if (item.value === 'SERPRO') {
      this.mobilityScore = 16;

     } else if (item.value === 'UNKNOWN') {
      this.mobilityScore = 0.5;
     }
     this.onPhysicalRisk();
  }

  onBathingSelect(item) {
    if (item.value === 'MINORP') {
      this.bathingScore = 4;

     } else if (item.value === 'MODPRO') {
      this.bathingScore = 12;

     } else if (item.value === 'NOP') {
      this.bathingScore = 0;

     } else if (item.value === 'SERPRO') {
      this.bathingScore = 20;

     } else if (item.value === 'UNKNOWN') {
      this.bathingScore = 0.5;
     }
     this.onPhysicalRisk();
  }

  onDressingSelect(item) {
    if (item.value === 'MINORP') {
      this.dressingScore = 4;

     } else if (item.value === 'MODPRO') {
      this.dressingScore = 12;

     } else if (item.value === 'NOP') {
      this.dressingScore = 0;

     } else if (item.value === 'SERPRO') {
      this.dressingScore = 20;

     } else if (item.value === 'UNKNOWN') {
      this.dressingScore = 0.5;
     }
     this.onPhysicalRisk();
  }

  onToileting(item) {
    if (item.value === 'MINORP') {
      this.toiletingScore = 4;

     } else if (item.value === 'MODPRO') {
      this.toiletingScore = 12;

     } else if (item.value === 'NOP') {
      this.toiletingScore = 0;

     } else if (item.value === 'SERPRO') {
      this.toiletingScore = 20;

     } else if (item.value === 'UNKNOWN') {
      this.toiletingScore = 0.5;
     }
     this.onPhysicalRisk();
  }

  onPhysicalRisk() {
   this.physicalRiskScore = this.eatingSelectScore + this.takingMedicationScore + this.mobilityScore + this.bathingScore + this.dressingScore + this.toiletingScore;
   this.riskScoreUpdate();
  }

  onDementiaSelect(item) {
    if (item.value === 'MINORP') {
      this.dementiaScore = 4;

     } else if (item.value === 'MODPRO') {
      this.dementiaScore = 10;

     } else if (item.value === 'NOP') {
      this.dementiaScore = 0;

     } else if (item.value === 'SERPRO') {
      this.dementiaScore = 16;

     } else if (item.value === 'UNKNOWN') {
      this.dementiaScore = 0.5;
     }
     this.onMentalRisk();
  }

  onDisorders(item) {
    if (item.value === 'MINORP') {
      this.disorderScore = 4;

     } else if (item.value === 'MODPRO') {
      this.disorderScore = 10;

     } else if (item.value === 'NOP') {
      this.disorderScore = 0;

     } else if (item.value === 'SERPRO') {
      this.disorderScore = 16;

     } else if (item.value === 'UNKNOWN') {
      this.disorderScore = 0.5;
     }
     this.onMentalRisk();
  }

  onSubstanceAbuse(item) {
    if (item.value === 'MINORP') {
      this.substanceAbuseScore = 1;

     } else if (item.value === 'MODPRO') {
      this.substanceAbuseScore = 3;

     } else if (item.value === 'NOP') {
      this.substanceAbuseScore = 0;

     } else if (item.value === 'SERPRO') {
      this.substanceAbuseScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.substanceAbuseScore = 0.5;
     }
     this.onMentalRisk();
  }

  onMoodDisorder(item) {
    if (item.value === 'MINORP') {
      this.moodDisorderScore = 1;

     } else if (item.value === 'MODPRO') {
      this.moodDisorderScore = 3;

     } else if (item.value === 'NOP') {
      this.moodDisorderScore = 0;

     } else if (item.value === 'SERPRO') {
      this.moodDisorderScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.moodDisorderScore = 0.5;
     }
     this.onMentalRisk();
  }

  onBehavioral(item) {
    if (item.value === 'MINORP') {
      this.behavioralScore = 1;

     } else if (item.value === 'MODPRO') {
      this.behavioralScore = 3;

     } else if (item.value === 'NOP') {
      this.behavioralScore = 0;

     } else if (item.value === 'SERPRO') {
      this.behavioralScore = 8;

     } else if (item.value === 'UNKNOWN') {
      this.behavioralScore = 0.5;
     }
     this.onMentalRisk();
  }

  onMentalRisk() {
   this.mentalRiskScore = this.dementiaScore + this.disorderScore + this.substanceAbuseScore + this.moodDisorderScore + this.behavioralScore;
   this.riskScoreUpdate();
  }



  onSelectSupportNetwork(item) {
   if (item.value === 'MINORP') {
     this.supportNetworkScore = 5;

   } else if (item.value === 'MODPRO') {
     this.supportNetworkScore = 15;

   } else if (item.value === 'NOP') {
     this.supportNetworkScore = 0;

   } else if (item.value === 'SERPRO') {
     this.supportNetworkScore = 25;

   } else if (item.value === 'UNKNOWN') {
     this.supportNetworkScore = 3;
   }
   this.riskScoreUpdate();
  }

  riskScoreUpdate() {
    if (!this.disableButton) {
    if (this.adultScreenFormGroup.get('clientinfoname').value !== '' && this.adultScreenFormGroup.get('clientinfoname').value !== undefined ) {
    this.adultScreenFormGroup.patchValue({
      riskscore: this.environmentalRiskScore + this.physicalRiskScore + this.mentalRiskScore + this.supportNetworkScore
    });
    this.riskScore = this.adultScreenFormGroup.get('riskscore').value;
    if (this.riskScore <= 49.5) {
      this.riskLevel = 'Low';
    } else if (this.riskScore > 49.5 && this.riskScore <= 99.5) {
      this.riskLevel = 'Moderate';
    } else if (this.riskScore >= 100) {
      this.riskLevel = 'High';
    }
    if (this.riskLevel) {
      this.adultScreenFormGroup.patchValue({
        risklevel : this.riskLevel
      });
    }
      if (this.physicalAbuse && this.selfNeglect && this.neglectedByOthers && this.exploitation
        && this.sexExploitation && this.physicalEnvironment) {
        if (this.physicalAbuse === 1 || this.selfNeglect === 1 || this.neglectedByOthers === 1 ||
          this.exploitation === 1 || this.sexExploitation === 1 || this.physicalEnvironment === 1) {
            this.intakeRecommendation = 'POTENTIAL APS INVESTIGATION';
        } else {
              this.intakeRecommendation = 'NO INVESTIGATION WARRANTED';
          }
      } else {
        this.intakeRecommendation = 'You DID NOT Answer All Questions';
      }
      if (this.intakeRecommendation) {
        this.adultScreenFormGroup.patchValue({
          intakerecommendation : this.intakeRecommendation
        });
      }
  }
  }
  }


  /**
   * Loading remaining tab - drop down - end
   *
   */
  onPhysicalAbuse(items) {
    this.physicalAbuse = items.value;
    this.riskScoreUpdate();
  }
  onSelfNeglect(items) {
    this.selfNeglect = items.value;
    this.riskScoreUpdate();
  }

  onNeglectByOthers(items) {
    this.neglectedByOthers = items.value;
    this.riskScoreUpdate();
  }

  onExploitation(items) {
    this.exploitation = items.value;
    this.riskScoreUpdate();
  }

  onSexExploitation(items) {
    this.sexExploitation = items.value;
    this.riskScoreUpdate();
  }

  onPhysicalEnvironment(items) {
    this.physicalEnvironment = items.value;
    this.riskScoreUpdate();
  }

  private loadResponseTime() {

    const source = this._commonHttpService.getArrayList(
      {
        where: { teamtypekey: 'AS', intakeservreqtypeid: this.daTypeKey }, // intakeservid: this.intakeNumber
        method: 'get',
        nolimit: true
      },
      CommonUrlConfig.EndPoint.Intake.responseTimeLoad
    ).map((result) => {
      return {
        response: result.map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.intakeservid
            })
        )
      };
    }).share();

    this.subServiceTypes$ = source.pluck('response');
  }

  loadSelectedSubType(subServiceType: DropdownModel) {
    this.getResponseTime(subServiceType.value);
  }

  getSelectedSubtype(subTypeID): SubType {
    return this.subServiceTypes.find(
      subServiceType =>
        subServiceType.servicerequestsubtypeid === subTypeID
    );
  }
  allegedVictimChnge(allegedVictim) {
    if (allegedVictim && allegedVictim.length && this.isAS) {
      this.adultScreenFormGroup.controls['phyAbuseSelect'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['phyAbuseSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['selfNeglectSelect'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['selfNeglectSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['neglectedByOthersSelect'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['neglectedByOthersSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['exploitationSelect'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['exploitationSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['sexExploitationSelect'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['sexExploitationSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['physicalEnvironmentSelect'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['physicalEnvironmentSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['allegedMaltreator'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['allegedMaltreator'].updateValueAndValidity();
      this.isMaltreatorRequried = false;
      this.isUnknowRequried = false;
      this.isUnknow = true;
    } else {
      this.adultScreenFormGroup.controls['phyAbuseSelect'].clearValidators();
      this.adultScreenFormGroup.controls['phyAbuseSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['selfNeglectSelect'].clearValidators();
      this.adultScreenFormGroup.controls['selfNeglectSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['neglectedByOthersSelect'].clearValidators();
      this.adultScreenFormGroup.controls['neglectedByOthersSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['exploitationSelect'].clearValidators();
      this.adultScreenFormGroup.controls['exploitationSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['sexExploitationSelect'].clearValidators();
      this.adultScreenFormGroup.controls['sexExploitationSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['physicalEnvironmentSelect'].clearValidators();
      this.adultScreenFormGroup.controls['physicalEnvironmentSelect'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['allegedMaltreator'].clearValidators();
      this.adultScreenFormGroup.controls['allegedMaltreator'].updateValueAndValidity();
      this.isMaltreatorRequried = true;
      this.isUnknowRequried = false;
      this.isUnknow = false;
      this.adultScreenFormGroup.controls['unknow'].reset();
      this. adultScreenFormGroup.controls['unknow'].clearValidators();
      this.adultScreenFormGroup.controls['unknow'].updateValueAndValidity();
      this.adultScreenFormGroup.controls['allegedMaltreator'].enable();
    }
  }
  unknownMaltreator(event) {
    this.isMaltreatorRequried = event;
    this.isUnknowRequried = event;
    if (event) {
      this.adultScreenFormGroup.controls['allegedMaltreator'].reset();
      this.adultScreenFormGroup.controls['allegedMaltreator'].disable();
      this.adultScreenFormGroup.controls['allegedMaltreator'].clearValidators();
      this.adultScreenFormGroup.controls['allegedMaltreator'].updateValueAndValidity();
    } else {
      this.adultScreenFormGroup.controls['allegedMaltreator'].enable();
      this.adultScreenFormGroup.controls['allegedMaltreator'].setValidators([Validators.required]);
      this.adultScreenFormGroup.controls['allegedMaltreator'].updateValueAndValidity();
    }
  }
  private getResponseTime(intakeServId) {
    this._dataStoreService.getCurrentStore();
    const source = this._commonHttpService.getArrayList(
      {
        where: { teamtypekey: 'AS', intakeservreqtypeid: this.daTypeKey, intakeservid: intakeServId },
        method: 'get',
        nolimit: true
      },
      CommonUrlConfig.EndPoint.Intake.responseTimeGet
    ).map((result) => {
      return {
        response: result.map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.servicerequestsubtypeid
            })
        )
      };
    }).share();

    this.subServiceDropDownTypes$ = source.pluck('response');
    this.subServiceDropDownTypes$.subscribe(result => {
      result.forEach((res) => {
        this.subTypeKey = res.value;
      });
    });
  }
  saveAdultScreen() {
    const astData = this.adultScreenFormGroup.getRawValue();
    astData.adultscreentoolhouseholdconfig = this.houseHold;
    astData['isComplete'] = this.disableButton;
    astData['subTypeId'] = this.subTypeKey;
    if (astData.referralremainanonymous === false || astData.referralremainanonymous === true) {
        astData.referralremainanonymous = null;
    }
    this._dataStoreService.setData(IntakeStoreConstants.adultScreenTool, Object.assign(astData));
    this._alertService.success('saved successfully');
  }

  reset() {
    // this.formComplete = false;
    this.adultScreenFormGroup.reset();
  }

  completeScreen() {
    if (this.daTypeKey) {
      this.adultScreenFormGroup.disable();
      this.disableButton = true;
      this.saveAdultScreen();
    } else {
      this._alertService.error(
        'Please select Purpose value on the screen'
      );
    }
  }
  disabledClintInfo() {
    this.adultScreenFormGroup.controls['clientinfoname'].disable();
    this.adultScreenFormGroup.controls['clientinfodob'].disable();
    this.adultScreenFormGroup.controls['clientinfoage'].disable();
    this.adultScreenFormGroup.controls['gendertypekey'].disable();
    this.adultScreenFormGroup.controls['clientinfoaddress1'].disable();
    this.adultScreenFormGroup.controls['clientinfoaddress2'].disable();
    this.adultScreenFormGroup.controls['clientinfophone'].disable();
    this.adultScreenFormGroup.controls['maritalstatuskey'].disable();
    this.adultScreenFormGroup.controls['racetypekey'].disable();
    this.adultScreenFormGroup.controls['ethnicitytypekey'].disable();
  }
  patchHouseHold( person ) {
    if (person && person.length) {
      const clientInfo = person.filter((item) => item.ishousehold === true);
      if (clientInfo && clientInfo.length) {
        this.adultScreenFormGroup.get('othersName').setValue(clientInfo[0].fullName);
        this.adultScreenFormGroup.get('othersRelationship').setValue(clientInfo[0].RelationshiptoRA);
        if (clientInfo[0].phoneNumber.length > 0 ) {
          this.adultScreenFormGroup.get('othersPhone').setValue(clientInfo[0].phoneNumber[0].contactnumber);
        }
      }
    }
  }
  patchClintInfo(person) {
    if (person && person.length) {
        const clientInfo = person.filter((item) => item.Role === 'RA');
        if (clientInfo && clientInfo.length) {
          const RaClintInfo = clientInfo[0];
          this.adultScreenFormGroup.patchValue({
            clientinfoname: RaClintInfo.fullName,
            clientinfodob: new Date(RaClintInfo.Dob),
            clientinfoage: this.calculateAge(RaClintInfo.Dob),
            gendertypekey: RaClintInfo.Gender,
            clientinfoaddress1: this.getAddressLine(RaClintInfo.personAddressInput, 'line1'),
            clientinfoaddress2: this.getAddressLine(RaClintInfo.personAddressInput, 'line2'),
            clientinfophone: this.getPhoneNumber(RaClintInfo.phoneNumber),
            maritalstatuskey: RaClintInfo.maritalstatus,
            monthlyincome: '',
            monthlyincomesource: '',
            totalassets: '',
            totalassetssource: '',
            racetypekey: RaClintInfo.race,
            ethnicitytypekey: RaClintInfo.Ethnicity,
            additionalinfo: '',
          });
        }
    }
  }
  patchForm(modal) {
    if (modal) {
      this.adultScreenFormGroup.patchValue(modal);
      this.adultScreenFormGroup.controls['riskscore'].patchValue(modal.riskscore);
      this.houseHold = modal.adultscreentoolhouseholdconfig;
      this.houseHold$ = Observable.of(this.houseHold);
    }
  }
  calculateAge(dob) {
    let age = 0;
    if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
        const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
        age = moment().diff(rCDob, 'years');
    }
    return age;
  }
  getAddressLine(address, type: string): string {
    let line  = [];
    if (address && address.length) {
      line = address.filter((item) => item.addresstypeLabel === 'Permanent Home');
    }
    if ((line && line.length) && type === 'line1' ) {
      return line.length && line[0].address1 ? line[0].address1 : '';
    } else {
      return line.length && line[0].Address2 ? line[0].Address2 : '';
    }
  }
  getPhoneNumber(phone): string {
    let phoneNumber = [];
    if (phone && phone.length) {
      phoneNumber = phone.filter((item) => item.contacttype === 'P');
    }
    if (phoneNumber && phoneNumber.length) {
      return phoneNumber.length && phoneNumber[0].contactnumber ? phoneNumber[0].contactnumber : '';
    }
  }
}
