import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeAdultScreenToolRoutingModule } from './intake-adult-screen-tool-routing.module';
import { IntakeAdultScreenToolComponent } from './intake-adult-screen-tool.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxMaskModule } from 'ngx-mask';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeAdultScreenToolRoutingModule,
    FormMaterialModule,
    SharedPipesModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    IntakeAdultScreenToolComponent
  ]
})
export class IntakeAdultScreenToolModule { }
