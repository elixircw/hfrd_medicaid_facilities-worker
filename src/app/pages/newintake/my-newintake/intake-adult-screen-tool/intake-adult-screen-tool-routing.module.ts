import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeAdultScreenToolComponent } from './intake-adult-screen-tool.component';

const routes: Routes = [{
  path: '',
  component: IntakeAdultScreenToolComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeAdultScreenToolRoutingModule { }
