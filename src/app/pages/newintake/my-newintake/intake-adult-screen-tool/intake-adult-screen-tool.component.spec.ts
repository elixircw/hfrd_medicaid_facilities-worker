import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeAdultScreenToolComponent } from './intake-adult-screen-tool.component';

describe('IntakeAdultScreenToolComponent', () => {
  let component: IntakeAdultScreenToolComponent;
  let fixture: ComponentFixture<IntakeAdultScreenToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeAdultScreenToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeAdultScreenToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
