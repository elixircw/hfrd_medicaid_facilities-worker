import { AppConstants } from '../../../@core/common/constants';

export const IntakeTabConfig = [
    {
        id: 'narrative',
        title: 'Narrative',
        name: 'Narrative',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_INTAKE_WORKER, AppConstants.ROLES.KINSHIP_SUPERVISOR],
        route: 'narrative',
        securityKey: 'myintake-narrative-screen'
    },
    {
        id: 'person',
        title: 'Persons Involved',
        name: 'Persons Involved',
        // tslint:disable-next-line:max-line-length
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER, AppConstants.ROLES.KINSHIP_INTAKE_WORKER, AppConstants.ROLES.KINSHIP_SUPERVISOR],
        route: 'person',
        securityKey: 'myintake-persons-involved'
    },
    {
        id: 'relation',
        title: 'Relationship',
        name: 'Relationship',
        // tslint:disable-next-line:max-line-length
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER, AppConstants.ROLES.KINSHIP_INTAKE_WORKER, AppConstants.ROLES.KINSHIP_SUPERVISOR],
        route: 'relationship',
        securityKey: 'myintake-persons-involved'
    },
    {
        id: 'person-cw',
        title: 'Persons Involved',
        name: 'Persons Involved',
        // tslint:disable-next-line:max-line-length
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER, AppConstants.ROLES.KINSHIP_INTAKE_WORKER, AppConstants.ROLES.KINSHIP_SUPERVISOR],
        route: 'person-cw'
    },
    {
        id: 'ast',
        // D-07004 Start
        title: 'Adult Screen',
        name: 'Adult Screen',
        // D-07004 End
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'ast',
        securityKey: ''
    },
    {
        id: 'appointment',
        title: 'Appointment',
        name: 'Appointment',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'appointment',
        securityKey: ''
    },
    {
        id: 'assessment',
        title: 'Assessment',
        name: 'Assessment',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'assessment',
        securityKey: ''
    },
    {
        id: 'attachment',
        title: 'Document',
        name: 'Document',
        // tslint:disable-next-line:max-line-length
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER, AppConstants.ROLES.KINSHIP_INTAKE_WORKER, AppConstants.ROLES.KINSHIP_SUPERVISOR],
        route: 'attachment',
        securityKey: 'myintake-attachement'
    },
    {
        id: 'document',
        title: 'Document',
        name: 'Document',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'attachment',
        securityKey: ''
    },
    {
        id: 'contact',
        title: 'Contact',
        name: 'contact',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER, AppConstants.ROLES.KINSHIP_INTAKE_WORKER, AppConstants.ROLES.KINSHIP_SUPERVISOR],
        route: 'contact/notes',
        securityKey: 'myintake-contact'
    },
    {
        id: 'complaint-type',
        title: 'Case',
        name: 'Complaints',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'complaint-type',
        securityKey: ''
    },
    {
        id: 'cross-reference',
        title: 'Cross Reference',
        name: 'Cross Reference',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'cross-reference',
        securityKey: ''
    },
    {
        id: 'decision',
        title: 'Decision',
        name: 'Decision',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'decision',
        securityKey: ''
    },
    {
        id: 'disposition',
        title: 'Decision',
        name: 'Disposition',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_INTAKE_WORKER, AppConstants.ROLES.KINSHIP_SUPERVISOR],
        route: 'disposition',
        securityKey: 'myintake-disposition'
    },
    {
        id: 'djs-notes',
        title: 'Notes',
        name: 'DJS Notes',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'djs-notes',
        securityKey: ''
    },
    {
        id: 'entity',
        title: 'Entity',
        name: 'Entity',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'entity',
        securityKey: ''
    },
    {
        id: 'evaluation',
        title: 'Complaints',
        name: 'Evaluation',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'evaluation',
        securityKey: ''
    },
    {
        id: 'peace-order',
        title: 'Peace Order',
        name: 'Peace Order',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'peace-order',
        securityKey: ''
    },
    {
        id: 'legal-action',
        title: 'Legal Actions',
        name: 'Legal Actions',
        role: [AppConstants.ROLES.SUPERVISOR],
        route: 'legal-action',
        securityKey: ''
    },
    {
        id: 'sao-hearing',
        title: 'Hearing Details',
        name: 'SAO Hearing',
        role: [AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'sao-hearing',
        securityKey: ''
    },
    {
        id: 'sao-petition',
        title: 'Petition Details',
        name: 'SAO Petition',
        role: [AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'sao-petition',
        securityKey: ''
    },
    {
        id: 'sao-response',
        title: 'SAO Response',
        name: 'SAO Response',
        role: [AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'sao-response',
        securityKey: ''
    },
    {
        id: 'sao-schedule',
        title: 'Schedule Hearings',
        name: 'SAO Schedule',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL, AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.COURT_WORKER],
        route: 'sao-schedule',
        securityKey: ''
    },
    {
        id: 'sdm',
        title: 'SDM',
        name: 'SDM',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'sdm',
        securityKey: 'myintake-sdm'
    },
    {
        id: 'service-type',
        title: 'Service/Sub-type',
        name: 'Service Type',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'service-type',
        securityKey: ''
    },
    {
        id: 'social-history',
        title: 'Social History',
        name: 'Social History',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'social-history',
        securityKey: ''
    },
    {
        id: 'intake-refferal',
        title: 'Intake Referral',
        name: 'Intake Refferal',
        role: [AppConstants.ROLES.COURT_WORKER],
        route: 'intake-referral',
        securityKey: ''
    },
    {
        id: 'payment-schedule',
        title: 'Payment Schedule',
        name: 'Payment Schedule',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'payment-schedule',
        securityKey: ''
    },
    {
        id: 'placement',
        title: 'Placement',
        name: 'Placement',
        role: [AppConstants.ROLES.OFFICE_PROFFESSIONAL],
        route: 'placement',
        securityKey: ''
    },
    {
        id: 'roa',
        title: 'ROA CPS',
        name: 'ROA CPS',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'roa'
    },
    {
        id: 'history-clearance',
        title: 'History Clearance',
        name: 'History Clearance',
        role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
        route: 'history-clearance'
    }
];

export const IntakeTabOrderConfig = [
    {
        agency: 'CW',
        order: ['narrative', 'person-cw', 'relation','sdm', 'contact', 'attachment', 'disposition']
    },
    {
        agency: 'DJS',
        order: ['person', 'evaluation', 'appointment', 'complaint-type', 'assessment', /*'cross-reference',*/ 'djs-notes', 'document', 'decision', 'legal-action'],
    },
    {
        agency: 'AS',
        order: ['narrative', 'person', 'ast', 'entity', 'service-type', 'assessment', 'contact', 'attachment', 'disposition']
    }
];
// const  person_tab_name = 'person';
const  person_tab_name = 'person-cw';
export const CW_TAB_ORDER = {
    COMMON_ORDER: ['narrative', person_tab_name,'relation', 'sdm', 'contact', 'attachment', 'disposition'],
    CPS_ORDER: ['narrative', person_tab_name,'relation', 'sdm', 'contact', 'attachment', 'disposition'],
    ROA_CPS: ['narrative', person_tab_name,'relation', 'roa', 'contact', 'attachment', 'disposition'],
    RISK_HARM_ORDER: ['narrative', person_tab_name,'relation', 'sdm', 'contact', 'attachment', 'disposition'],
    NON_CPS_ORDER: ['narrative', person_tab_name,'relation', 'contact', 'attachment', 'disposition']
};

export const DJS_TAB_ORDER = {
    COMMON_ORDER: ['person', 'evaluation', 'appointment', 'complaint-type', 'assessment', /*'cross-reference',*/ 'djs-notes', 'document', 'decision', 'payment-schedule', 'legal-action'],
    // Peace order
    PO_ORDER: ['person', 'peace-order', 'appointment', 'complaint-type', 'assessment', /*'cross-reference',*/ 'djs-notes', 'document', 'decision', 'legal-action'],
    WAIVER_FROM_ADUL_COURT_ORDER: ['person', 'evaluation', 'complaint-type', 'djs-notes', 'document', 'decision'],
    ADULT_HOLD_DETENTION: ['person', 'complaint-type', 'assessment', 'placement', 'djs-notes', 'document', 'decision'],
    INTERSTATE_COMPACT: ['person', 'complaint-type', 'djs-notes', 'document', 'decision']
};




