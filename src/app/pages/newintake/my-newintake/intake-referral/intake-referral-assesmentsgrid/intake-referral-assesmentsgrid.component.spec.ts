import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeReferralAssesmentsgridComponent } from './intake-referral-assesmentsgrid.component';

describe('IntakeReferralAssesmentsgridComponent', () => {
  let component: IntakeReferralAssesmentsgridComponent;
  let fixture: ComponentFixture<IntakeReferralAssesmentsgridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeReferralAssesmentsgridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeReferralAssesmentsgridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
