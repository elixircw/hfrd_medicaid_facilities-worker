import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeReferralCasegridComponent } from './intake-referral-casegrid.component';

describe('IntakeReferralCasegridComponent', () => {
  let component: IntakeReferralCasegridComponent;
  let fixture: ComponentFixture<IntakeReferralCasegridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeReferralCasegridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeReferralCasegridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
