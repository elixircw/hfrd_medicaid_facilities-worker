import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeReferralPersongridComponent } from './intake-referral-persongrid.component';

describe('IntakeReferralPersongridComponent', () => {
  let component: IntakeReferralPersongridComponent;
  let fixture: ComponentFixture<IntakeReferralPersongridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeReferralPersongridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeReferralPersongridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
