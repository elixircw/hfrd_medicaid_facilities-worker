import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeReferralAppointmentsgridComponent } from './intake-referral-appointmentsgrid.component';

describe('IntakeReferralAppointmentsgridComponent', () => {
  let component: IntakeReferralAppointmentsgridComponent;
  let fixture: ComponentFixture<IntakeReferralAppointmentsgridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeReferralAppointmentsgridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeReferralAppointmentsgridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
