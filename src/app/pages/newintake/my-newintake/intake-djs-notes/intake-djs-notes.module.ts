import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeDjsNotesRoutingModule } from './intake-djs-notes-routing.module';
import { IntakeDjsNotesComponent } from './intake-djs-notes.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeDjsNotesRoutingModule,
    FormMaterialModule
  ],
  declarations: [IntakeDjsNotesComponent]
})
export class IntakeDjsNotesModule { }
