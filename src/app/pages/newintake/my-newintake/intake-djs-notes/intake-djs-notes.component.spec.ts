import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeDjsNotesComponent } from './intake-djs-notes.component';

describe('IntakeDjsNotesComponent', () => {
  let component: IntakeDjsNotesComponent;
  let fixture: ComponentFixture<IntakeDjsNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeDjsNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeDjsNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
