import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
// tslint:disable-next-line:import-blacklist
import { Observable, Subject } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../newintake-url.config';
import { ContactType, PurposeType, TypeofLocation, PersonsInvolvedType, InvolvedPerson, Notes } from '../_entities/newintakeModel';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PaginationRequest, DynamicObject, DropdownModel } from '../../../../@core/entities/common.entities';
import { AlertService } from '../../../../@core/services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../../@core/services/auth.service';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { IntakeStoreConstants } from '../my-newintake.constants';
import { IntakeConfigService } from '../intake-config.service';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-djs-notes',
    templateUrl: './intake-djs-notes.component.html',
    styleUrls: ['./intake-djs-notes.component.scss']
})
export class IntakeDjsNotesComponent implements OnInit, AfterViewInit {
    contactType$: Observable<ContactType[]>;
    purposeType$: Observable<PurposeType[]>;
    typeofLocation$: Observable<TypeofLocation[]>;
    personsInvolvedType$: Observable<PersonsInvolvedType[]>;
    notesForm: FormGroup;
    savednotes: Notes[] = [];
    purposeType: PurposeType[] = [];
    contactType: ContactType[] = [];
    typeofLocation: TypeofLocation[] = [];
    personsInvolvedType: PersonsInvolvedType[] = [];

    addedPersons: InvolvedPerson[];
    youthList: InvolvedPerson[];
    youth: InvolvedPerson;
    intakeId: string;
    notesId: string;
    editMode: boolean;
    userInfo: any;
    enableAdd = false;
    contact: any;
    notesHistory: any[] = [];
    currentDate: Date = new Date();
    private store: DynamicObject;
    maxDate = new Date();
    notesfilterForm: FormGroup;
    staffTypeList: any[] = [];
    constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _authService: AuthService,
        private _alertService: AlertService, private route: ActivatedRoute, private _storeService: DataStoreService, private _intakeConfig: IntakeConfigService) {
        this.store = this._storeService.getCurrentStore();
    }


    ngOnInit() {
        this.loadDropdowns();
        this.userInfo = this._authService.getCurrentUser();
        this.notesId = null;
        this.editMode = false;
        this.notesfilterForm = this.formBuilder.group({
            stafftype: '',
            startdate: '',
            enddate: '',
            workername: '',
            keyword: ''
        });
        this.notesForm = this.formBuilder.group({
            progressnoteid: '',
            progressnotetypeid: '',
            progressnotesubtypeid: '',
            contactdate: '',
            date: '',
            location: '',
            contactname: '',
            contactroletypekey: [''],
            contactphone: '',
            contactemail: '',
            attemptindicator: false,
            description: '',
            starttime: '',
            staff: [''],
            endtime: '',
            entitytypeid: 'a426ee09-ed7f-4b37-aead-60925f13425b',
            entitytype: 'intakeservicerequest',
            savemode: 1,
            stafftype: '',
            notes: '',
            instantresults: 1,
            contactstatus: true,
            drugscreen: true,
            progressnotepurposetypekey: '',
            progressnoteroletype: [''],
            progressnoterole: ['']
        });
        // this.getNotesList();
        // this.loadLocationTypeDropdown('8d25a26b-91a1-4423-947e-e451e8eff6b7');
        this.addedPersons = this.store[IntakeStoreConstants.addedPersons];
        if (this.addedPersons) {
            this.youthList = this.addedPersons.filter((p) => p.Role === 'Youth');
            if (this.youthList && this.youthList.length > 0) {
                this.enableAdd = true;
            }
        }


        // this.notesForm.valueChanges.subscribe((val) => {
        //     if (this.savednotes) {
        //     this.communicationInputSubject$.next(this.savednotes);
        //     }
        //   });
        this.loadStaffType();
        this.notesForm.patchValue({ 'staff': this.userInfo ? this.userInfo.user.userprofile.displayname : '' });
        this.notesForm.get('staff').disable();
        this.notesForm.get('staff').updateValueAndValidity();
        this.savednotes = this.store[IntakeStoreConstants.communicationFields] ? this.store[IntakeStoreConstants.communicationFields] : [];
        // this.communicationOutputSubject$.subscribe((data) => {
        //     this.savednotes = data && data.length > 0 ? data : [];
        // });
    }

    ngAfterViewInit() {
            if (this._authService.isDJS() && !this._intakeConfig.getiseditIntake()) {
                (<any>$(':button')).prop('disabled', true);
                (<any>$('span a i')).css({'pointer-events': 'none',
                            'cursor': 'default',
                            'opacity': '0.5',
                            'text-decoration': 'none'});
            }
    }


    private loadDropdowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.contactType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.purposeType),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.personsInvolvedType)
        ])
            .map(([contactType, purposeType, personsInvolvedType]) => {
                return {
                    contactType,
                    purposeType,
                    personsInvolvedType
                };
            })
            .share();

        this.contactType$ = source.pluck('contactType');
        this.purposeType$ = source.pluck('purposeType');
        this.personsInvolvedType$ = source.pluck('personsInvolvedType');
        this.contactType$.subscribe((data) => { this.contactType = data; });
        this.purposeType$.subscribe((data) => { this.purposeType = data; });
        this.personsInvolvedType$.subscribe((data) => { this.personsInvolvedType = data; });

    }
    private loadLocationTypeDropdown(progressnotetypeid: string, contactdescription: string) {
        this.notesForm.patchValue({
            progressnotetypeid: progressnotetypeid,
        });
        this.contact = contactdescription;
        if (this.contact === 'Email') {
            this.notesForm.get('progressnotesubtypeid').reset();
           this.notesForm.get('progressnotesubtypeid').disable();
        } else {
            this.notesForm.get('progressnotesubtypeid').enable();
        const locationType = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    'nolimit': true,
                    'order': 'description'
                }),
                NewUrlConfig.EndPoint.Intake.typeofLocation + '?prognotetypeid=' + progressnotetypeid
            )
            .map((typeofLocation) => {
                return {
                    typeofLocation
                };
            })
            .share();

        this.typeofLocation$ = locationType.pluck('typeofLocation');
        this.typeofLocation$.subscribe((data) => { this.typeofLocation = data; });
        }
    }

    loadStaffType() {
        this._commonHttpService
        .getArrayList(
            new PaginationRequest({
                nolimit: true,
                method: 'get',
                where: { 'tablename' : 'ProgressNoteStaffType', 'teamtypekey' : 'DJS' }
            }),
            'referencetype/gettypes?filter'
        )
        .map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.value_text,
                        value: res.ref_key
                    })
            );
        }).subscribe(result => {
            this.staffTypeList = result;
        });
    }

    private getNotesList() {
        this.route.params.subscribe((item) => {
            this.intakeId = item['id'];
        });
        const NotesList = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    count: -1,
                    page: 1,
                    limit: 10,
                    where: {},
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.Intake.notesList + '/' + this.intakeId + '?data'
            )
            .map((notes) => {
                return {
                    notes
                };
            })
            .share();
            this.notesForm.get('staff').disable();
            this.notesForm.get('staff').updateValueAndValidity();
        // this.savedNotes$ = NotesList.pluck('notes');

    }

    notesfilter(model) {
        this.savednotes = this.store[IntakeStoreConstants.communicationFields] ? this.store[IntakeStoreConstants.communicationFields] : [];
        if (model.stafftype) {
            this.savednotes = this.savednotes.filter(data => data.stafftype === model.stafftype);
        }
        if (model.startdate) {
            this.savednotes = this.savednotes.filter(data => new Date(data.date) >= new Date(model.startdate));
        }
        if (model.enddate) {
            this.savednotes = this.savednotes.filter(data => new Date(data.date) <= new Date(model.enddate));
        }
        if (model.workername) {
            this.savednotes = this.savednotes.filter(data => data.staff.toLowerCase().includes(model.workername.toLowerCase()));
        }
        if (model.keyword) {
            this.savednotes = this.savednotes.filter(data => data.notes.toLowerCase().includes(model.keyword.toLowerCase()));
        }
    }

    clearFilter() {
        this.savednotes = this.store[IntakeStoreConstants.communicationFields] ? this.store[IntakeStoreConstants.communicationFields] : [];
        this.notesfilterForm.reset();
    }

    private saveNote() {
        if (this.notesForm.valid) {
            this.notesForm.patchValue({ progressnoteid: this.savednotes.length + 1 });
            const data = this.notesForm.getRawValue();
            data.description = data.notes;
            data.contactname = data.staff;
            data.contactdate = data.date;
            data.histories = [];
            data.notesid = this.generateNotesID();
            this.savednotes.push(data);
            this.broadCastCommunicationUpdated();
            (<any>$('#add-djs-notes')).modal('hide'); this.resetForm();
        } else {
            this._alertService.error('Please fill the required fields');
        }
    }

    resetForm() {
        this.notesForm.reset();
        this.notesId = null;
        this.notesForm.enable();
        this.notesForm.patchValue({ 'staff': this.userInfo ? this.userInfo.user.userprofile.displayname : '' });
        this.notesForm.get('staff').disable();
        // D-07269 Start 
        if(this.youthList){
            this.youth =  this.youthList[0];
            this.notesForm.patchValue({ 'contactroletypekey': this.youth.Firstname ? this.youth.Firstname : '' });
        }
        this.notesForm.controls["contactroletypekey"].disable();
        // D-07269 End 
    }
    private updateNotes() {
        if (this.notesForm.valid) {
            this.notesForm.patchValue({ progressnoteid: this.notesId });
            const formdata = this.notesForm.getRawValue();
            const index = this.savednotes.findIndex((p) => p.progressnoteid === this.notesId);
            formdata.contactname = formdata.staff;
            formdata.contactdate = formdata.date;
            if (index !== -1) {
                formdata.histories = this.updateNotesHistory(index);
                formdata.notesid = this.generateNotesID();
                this.savednotes[index] = formdata;
            }
            // this.notesForm.reset();
            this.broadCastCommunicationUpdated(); (<any>$('#add-djs-notes')).modal('hide'); this.resetForm();
        } else {
            this._alertService.error('Please fill the required fields');
        }

    }

    generateNotesID() {
        return new Date().getTime();
    }

    updateNotesHistory(index) {
        const oldNote = {...this.savednotes[index]};
        const histories = oldNote.histories ? [...oldNote.histories] : [];
        oldNote.histories = [];
        histories.push(oldNote);
        return [...histories];
    }


    private getNotes(note: Notes, editFlag) {
        this.resetForm();
        this.loadLocationTypeDropdown(note.progressnotetypeid, note.docdescription);
        this.notesHistory = note.histories;
        this.notesId = note.progressnoteid;
        this.editMode = editFlag;
        if (editFlag === 0) {
            this.notesForm.disable();
        } else {
            this.notesForm.enable();
        }
        // const data = this.savednotes.filter((p) => p.progressnoteid === id);

        this.patchForm(note);
    }

    closeHistory(note: Notes) {
        (<any>$('#notes-history')).modal('hide');
        this.getNotes(note, 0);
        (<any>$('#add-djs-notes')).modal('show');
    }

    private broadCastCommunicationUpdated() {
        const notes = JSON.parse(JSON.stringify(this.savednotes));
        notes.forEach(note => {
            note.progressnoteroletype = [];
            note.progressnoterole.forEach(role => {
                note.progressnoteroletype.push({
                    contactroletypekey: role
                });
            });
        });
        this._storeService.setData(IntakeStoreConstants.communicationFields, notes);
        // this.communicationInputSubject$.next(notes);
    }

    private getSavedNote(id) {
        const NotesList = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    count: -1,
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.Intake.notesEdit + '/' + id
            )
            .map((notes) => {
                return {
                    notes
                };
            })
            .share();

        return NotesList.pluck('notes');
    }

    private getValue(value, name) {

        switch (name) {
            case 'contype':

                const cindex = this.contactType.findIndex((p) => p.progressnotetypeid === value);
                if (cindex !== -1) {
                    return this.contactType[cindex].description;
                } else {
                    return '';
                }
            case 'stafftype': const staffdata = this.staffTypeList.find(data => data.value === value);
            return staffdata ? staffdata.text : '';
            case 'constatus': if (value === true) {
                return 'Yes';
            } else if (value === false) {
                return 'No';
            } else {
                return '';
            }
            case 'location':
                if (this.typeofLocation$ && this.typeofLocation$ !== null) {

                    const i = this.typeofLocation.findIndex((p) => p.progressnotetypeid === value);
                    if (i !== -1) {
                        return this.typeofLocation[i].description;
                    } else {
                        return '';
                    }

                } else {
                    return '';
                }
            case 'drugscrn': if (value === true) {
                return 'Yes';
            } else if (value === false) {
                return 'No';
            } else {
                return '';
            }
            case 'instresult': if (value === '1') {
                return 'Positive';
            } else if (value === '2') {
                return 'Negative';
            } else {
                return '';
            }

            case 'purpose':
                const index = this.purposeType.findIndex((p) => p.progressnotepurposetypeid === value);
                if (index !== -1) {
                    return this.purposeType[index].description;
                } else {
                    return '';
                }

            default: return '';
        }
    }

    private setLocation(desc) {
        this.notesForm.patchValue({ 'location': desc });
    }

    private patchForm(data) {
        if (data) {
            this.notesForm.patchValue(data);
            this.notesForm.patchValue({
                staff: data.contactname,
                date: data.contactdate
              });
        }
    }

    onChangeDrugScreen(value: boolean) {
        if (value) {
            this.notesForm.get('instantresults').setValidators([Validators.required]);
            this.notesForm.updateValueAndValidity();
        } else {
            this.notesForm.get('instantresults').clearValidators();
            this.notesForm.updateValueAndValidity();
        }
    }
}
