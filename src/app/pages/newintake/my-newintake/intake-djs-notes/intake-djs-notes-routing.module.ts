import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeDjsNotesComponent } from './intake-djs-notes.component';

const routes: Routes = [{
  path: '',
  component: IntakeDjsNotesComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeDjsNotesRoutingModule { }
