import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeEvaluationFieldsComponent } from './intake-evaluation-fields.component';

const routes: Routes = [
  {
      path: '',
      component: IntakeEvaluationFieldsComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeEvaluationFieldsRoutingModule { }
