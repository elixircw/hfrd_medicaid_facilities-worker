import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTabsModule,
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule, PopoverModule, TimepickerModule } from 'ngx-bootstrap';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { TrumbowygNgxModule } from 'trumbowyg-ngx';

import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { SortTableModule } from '../../../../shared/modules/sortable-table/sortable-table.module';
import { MyNewintakeRoutingModule } from '../my-newintake-routing.module';
import { MyNewintakeComponent } from '../my-newintake.component';
import { IntakeEvaluationFieldsComponent } from './intake-evaluation-fields.component';

describe('IntakeEvaluationFieldsComponent', () => {
    let component: IntakeEvaluationFieldsComponent;
    let fixture: ComponentFixture<IntakeEvaluationFieldsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                MatDatepickerModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatButtonModule,
                MatRadioModule,
                MatTabsModule,
                MyNewintakeRoutingModule,
                FormsModule,
                ReactiveFormsModule,
                PaginationModule,
                TimepickerModule,
                ControlMessagesModule,
                SharedDirectivesModule,
                SharedPipesModule,
                NgSelectModule,
                ImageCropperModule,
                SortTableModule,
                NgxMaskModule.forRoot(),
                TrumbowygNgxModule.withConfig({
                    svgPath: '../../../../assets/images/icons.svg'
                }),
                A2Edatetimepicker,
                NgxMaskModule.forRoot(),
                PopoverModule,
                NgxfUploaderModule.forRoot(),
                MyNewintakeComponent
            ],
            declarations: [IntakeEvaluationFieldsComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IntakeEvaluationFieldsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
