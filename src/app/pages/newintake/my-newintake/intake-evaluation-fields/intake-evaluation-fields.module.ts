import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeEvaluationFieldsRoutingModule } from './intake-evaluation-fields-routing.module';
import { IntakeEvaluationFieldsComponent } from './intake-evaluation-fields.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxMaskModule } from 'ngx-mask';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeEvaluationFieldsRoutingModule,
    FormMaterialModule,
    NgxMaskModule.forRoot(),
    SharedDirectivesModule
  ],
  declarations: [IntakeEvaluationFieldsComponent]
})
export class IntakeEvaluationFieldsModule { }
