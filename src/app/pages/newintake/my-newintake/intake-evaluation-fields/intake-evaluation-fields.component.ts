import { Component, OnInit, Input, ViewChild, ElementRef, OnDestroy, ChangeDetectorRef, AfterViewChecked, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Observable, Subject, Subscription } from 'rxjs/Rx';
import { DropdownModel, PaginationRequest, ListDataItem } from '../../../../@core/entities/common.entities';
import { NewUrlConfig } from '../../newintake-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { EvaluationFields } from '../_entities/newintakeSaveModel';
import { AuthService, AlertService, DataStoreService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import {
    InvolvedPerson, EvaluationSourceType, EvaluationSourceObject,
    EvaluationSourceAgency, IntakePurpose, CrossReferenceResponse, EvaluationOffenseLocationTypeList, CrossReferenceSearchResponse, IntakeCrossReferenceSearchResponse
} from '../_entities/newintakeModel';
import { AppConfig } from '../../../../app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IntakeStoreConstants, MyNewintakeConstants } from '../my-newintake.constants';
import { IntakeConfigService } from '../intake-config.service';
import { Router, ActivatedRoute } from '@angular/router';
import { config } from '../../../../../environments/config';
import { AppConstants } from '../../../../@core/common/constants';
import * as moment from 'moment';
import { ControlUtils } from '../../../../@core/common/control-utils';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';

const CR_COMPLAINT_EXIST_SUCESS_MESSAGE = 'Initial disapproved Intake for the given complaint id is added in Cross Reference.';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-evaluation-fields',
    templateUrl: './intake-evaluation-fields.component.html',
    styleUrls: ['./intake-evaluation-fields.component.scss']
})
export class IntakeEvaluationFieldsComponent implements OnInit, OnDestroy, AfterViewChecked, AfterViewInit {

    // @Input() evalFieldsInputSubject$ = new Subject<EvaluationFields[]>();
    // @Input() evalFieldsOutputSubject$ = new Subject<EvaluationFields[]>();
    // @Input() complaintCrossReferenceSubject$ = new Subject<CrossReferenceResponse>();
    // @Input() purposeInputSubject$ = new Subject<IntakePurpose>();
    // tslint:disable-next-line:no-input-rename
    // @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
    addedPersons: InvolvedPerson[] = [];
    // tslint:disable-next-line:no-input-rename
    // @Input() reviewStatus$ = new Subject<string>();
    evalForm: FormGroup;
    lawEnforcementSearchForm: FormGroup;
    deleteIndex: number;
    incidentDateOption: number;
    mdCountys$: Observable<DropdownModel[]>;
    offenceCategories$: Observable<any[]>;
    offenceCategoryList = [];
    offenceCategoryListData = [];
    victimArray: any[] = [];
    // @Input() offenceCategoriesInputSubject$ = new Subject<any>();
    evaluationSourceTypeList$: Observable<EvaluationSourceType[]>;
    evaluationAgencyTypeList$: Observable<EvaluationSourceAgency[]>;
    evaluationSourceSearchList$: Observable<any>;
    evaluationOffenseLocationTypeList$: Observable<EvaluationOffenseLocationTypeList>;
    sourceData: EvaluationSourceObject;
    oldDate = {
        beginDate: '',
        endDate: ''
    };
    maxDate = new Date();
    roleName: AppUser;
    evals: EvaluationFields[] = [];
    evalAction: string;
    isViewMode: boolean;
    editIndex: number;
    youthAge: string;
    youthAgeInMonths: number;
    victims: any = [];
    baseUrl = '';
    token: AppUser;
    purposeID = '';
    selectedPurpose = '';
    enableAdd = false;
    enableWaiverField = false;
    store: any;
    dataStroeSubscription: Subscription;
    addedIntakeCrossReference: IntakeCrossReferenceSearchResponse[] = [];
    addedCrossReference: CrossReferenceSearchResponse[] = [];
    intakeCrossReferenceAdded: boolean;
    crossReferenceAdded: boolean;
    complaintid: string;
    placeholderOffenseDate = 'Offense Date';
    isLawEnforcement = false;
    isvictimrequired = false;
    constructor(private _router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _datastore: DataStoreService,
        private _intakeConfig: IntakeConfigService,
        private _detect: ChangeDetectorRef,
        private http: HttpClient) {
        this.baseUrl = AppConfig.baseUrl;
        this.store = this._datastore.getCurrentStore();
    }

    ngOnInit() {
        this.editIndex = -1;
        this.evalAction = 'add';
        this.isViewMode = false;
        this.initEvalForm();
        this.deleteIndex = -1;
        this.incidentDateOption = 0;
        this.loadDropdowns();
        // this.findAndLoadVictims();
        this.processSavedEvaluations();
        this.processPersons();
        this.roleName = this._authService.getCurrentUser();
        this.token = this._authService.getCurrentUser();


        this.dataStroeSubscription = this._datastore.currentStore.subscribe(store => {
            console.log('eval ', store[IntakeStoreConstants.purposeSelected]);
            if (store[IntakeStoreConstants.purposeSelected]) {
                console.log('eval if', store[IntakeStoreConstants.purposeSelected]);
                const purposeSelected = store[IntakeStoreConstants.purposeSelected];
                const storePurposeId = purposeSelected.value;
                if (storePurposeId !== this.purposeID) {
                    this.purposeID = storePurposeId;
                    this.listAllegations(this.purposeID);
                    this.selectedPurpose = purposeSelected.text;
                    this.enableAdd = true;
                    // D-07789 Law Enforcement (Police Source) Information
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT)) {
                        this.isLawEnforcement = true;
                    }
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.ADULT_HOLD_DETENTION)) {
                        // No complaint tab for ADULT HOLD DETENTION
                        // this._router.navigate(['../../person'], { relativeTo: this.route });
                    }
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.WAIVER_FROM_ADUL_COURT)) {
                        this.enableWaiverField = true;
                        this.evalForm.get('waiverpetitionid').setValidators([Validators.required]);
                        this.evalForm.get('waivedoffense').setValidators([Validators.required]);
                        this.evalForm.get('victims').clearValidators();
                        this.evalForm.get('victims').updateValueAndValidity();
                    } else {
                        this.evalForm.get('victims').setValidators([Validators.required]);
                        this.evalForm.get('victims').updateValueAndValidity();
                        this.enableWaiverField = false;
                    }
                }
            }

        });
    }

    ngAfterViewInit() {
        if (this._authService.isDJS() && !this._intakeConfig.getiseditIntake()) {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('a[class *="aut-tab*"]')).css('');
        }
    }
    ngAfterViewChecked() {
        this._detect.markForCheck();
        this._detect.detectChanges();
    }

    processSavedEvaluations() {
        const evaluations = this._datastore.getData(IntakeStoreConstants.evalFields);
        if (evaluations && Array.isArray(evaluations)) {
            evaluations.forEach(evalFeilds => {
                if (evalFeilds.allegedoffense) {
                    evalFeilds.allegedoffense = evalFeilds.allegedoffense.map(offence => {
                        if (offence.allegationid) {
                            return offence.allegationid;
                        } else {
                            return offence;
                        }
                    });
                }
                if (evalFeilds.waiverpetitionid && evalFeilds.waivedoffense) {
                    evalFeilds.waivedoffense = evalFeilds.waivedoffense.map(offence => {
                        if (offence.allegationid) {
                            return offence.allegationid;
                        } else {
                            return offence;
                        }
                    });
                }
            });
            this.evals = evaluations;
        } else {
            this.evals = [];
        }

    }

    processPersons() {
        const persons = this.store[IntakeStoreConstants.addedPersons];
        if (persons && Array.isArray(persons)) {
            this.addedPersons = persons;
            this.setYouthAge();
            this.findAndLoadVictims();
        } else {
            this.addedPersons = [];
        }

    }

    private findAndLoadVictims() {
        const victimPersons = this.addedPersons.filter(person => person.Role === 'Victim');
        if (victimPersons.length > 0) {
            this.victims = victimPersons.map((person, index) => {
                if (person.Role === 'Victim') {
                    return {
                        personid: person.Pid ? person.Pid : AppConstants.PERSON.TEMP_ID + index,
                        firstName: person.Firstname,
                        lastName: person.Lastname,
                        fullName: person.fullName
                    };
                }
            });
        } else {
            this.victims = [];
        }
    }

    private loadDefault() {
        const intakeEvalForm = this.evalForm.getRawValue();
        if (intakeEvalForm.sourceTitle) {
            const source = {
                title: intakeEvalForm.sourceTitle,
                badgeno: intakeEvalForm.sourceBadgeNo,
                lastname: intakeEvalForm.sourcelastname,
                firstname: intakeEvalForm.sourcefirstname,
                streetno: intakeEvalForm.sourceStreetno,
                street1: intakeEvalForm.sourceStreet1,
                street2: intakeEvalForm.sourceStreet2,
                totalcount: intakeEvalForm.totalcount,
                evaluationsourcekey: intakeEvalForm.evaluationsourcekey
            };
            this.sourceData = source;
        }
        // if (intakeEvalForm.evaluationsourcetypekey) {
        //     this.loadSourceAgencyTypeDropdown(intakeEvalForm.evaluationsourcetypekey);
        // }
    }
    private loadDropdowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({ where: { state: 'MD' }, order: 'countyname asc', nolimit: true, method: 'get' }, NewUrlConfig.EndPoint.Intake.MDCountryListUrl + '?filter'),
            // this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.OffenceCategoryListUrl + '?filter={"nolimit":true,"order":"description"}'),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.EvaluationSourceTypeList + '?filter={"nolimit":true,"order":"description"}')
            // this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.IntakeAgencies)
        ])
            .map(([sourseListResult, evaluationSourceTypeList]) => {
                return {
                    sourceList: sourseListResult.map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyid
                            })
                    ),
                    evaluationSourceTypeList
                };
            })
            .share();
        this.mdCountys$ = source.pluck('sourceList');
        this.evaluationSourceTypeList$ = source.pluck('evaluationSourceTypeList');

    }

    private setCounty(county) {
        this.evalForm.patchValue({
            countyname: county.text,
        });
    }

    private setSourceAgencyTypeDropdown(evaluationsourceagency) {
        this.evalForm.patchValue({
            evaluationsourceagencykey: evaluationsourceagency.evaluationsourceagencykey,
            evaluationsourceagencyname: evaluationsourceagency.description
        });
    }

    private loadSourceAgencyTypeDropdown() {
        // this.evalForm.patchValue({
        //     evaluationsourcetypekey: evaluationsourcetype.evaluationsourcetypekey,
        //     evaluationsourcetypename: evaluationsourcetype.description
        // });
        const sourceAgencyType = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    'where': { 'intakeservicerequesttypeid': this.purposeID },
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.Intake.AgencyTypeList + '?filter'
            )
            .map((evaluationAgencyTypeList) => {
                return {
                    evaluationAgencyTypeList
                };
            })
            .share();

        this.evaluationAgencyTypeList$ = sourceAgencyType.pluck('evaluationAgencyTypeList');
    }

    private loadOffenseLocationType() {
        const sourceAgencyType = this._commonHttpService
            .getArrayList(
                {
                    order: 'description asc',
                    method: 'get'
                },
                NewUrlConfig.EndPoint.Intake.OffenseLoactionType + '?filter'
            )
            .map((evaluationOffenseLoactionType) => {
                return {
                    evaluationOffenseLoactionType
                };
            })
            .share();

        this.evaluationOffenseLocationTypeList$ = sourceAgencyType.pluck('evaluationOffenseLoactionType');
    }

    loadSourceDropDown() {
        const workEnv = config.workEnvironment;
        if (workEnv === 'state') {
            const intakeEvalForm = this.evalForm.getRawValue();
            if (intakeEvalForm.sourcesearch) {
                const headers = new HttpHeaders().set('no-loader', 'true');
                // const source = this._commonHttpService
                //     .getArrayList({
                //         where: {
                //             searchkey: intakeEvalForm.sourcesearch,
                //             evaluationsourceagencykey: intakeEvalForm.evaluationsourceagencykey
                //         },
                //         nolimit: true, method: 'get'
                //     },
                //         NewUrlConfig.EndPoint.Intake.EvaluationSourceSearchList + '?filter')
                //     .map((evaluationSourceSearchList) => {
                //         return {
                //             evaluationSourceSearchList
                //         };
                //     })
                //     .share();

                this.evaluationSourceSearchList$ = this.http.get(this.baseUrl + '/Evaluationsource/v1' + '/' + NewUrlConfig.EndPoint.Intake.EvaluationSourceSearchList + '?filter=' + JSON.stringify({
                    where: {
                        searchkey: intakeEvalForm.sourcesearch,
                        evaluationsourceagencykey: intakeEvalForm.evaluationsourceagencykey
                    },
                    nolimit: true, method: 'get'
                }), { headers: headers }).map((result) => {
                    return result;
                });
            } else {
                this.evaluationSourceSearchList$ = Observable.of([]);
            }
        } else {
            const intakeEvalForm = this.evalForm.getRawValue();
            if (intakeEvalForm.sourcesearch) {
                const headers = new HttpHeaders().set('no-loader', 'true');
                // const source = this._commonHttpService
                //     .getArrayList({
                //         where: {
                //             searchkey: intakeEvalForm.sourcesearch,
                //             evaluationsourceagencykey: intakeEvalForm.evaluationsourceagencykey
                //         },
                //         nolimit: true, method: 'get'
                //     },
                //         NewUrlConfig.EndPoint.Intake.EvaluationSourceSearchList + '?filter')
                //     .map((evaluationSourceSearchList) => {
                //         return {
                //             evaluationSourceSearchList
                //         };
                //     })
                //     .share();

                this.evaluationSourceSearchList$ = this.http.get(this.baseUrl + '/' + NewUrlConfig.EndPoint.Intake.EvaluationSourceSearchList + '?filter=' + JSON.stringify({
                    where: {
                        searchkey: intakeEvalForm.sourcesearch,
                        evaluationsourceagencykey: intakeEvalForm.evaluationsourceagencykey
                    },
                    nolimit: true, method: 'get'
                }), { headers: headers }).map((result) => {
                    return result;
                });
            } else {
                const intakeEvalFormData = this.evalForm.getRawValue();
                if (intakeEvalFormData.sourcesearch) {
                    const headers = new HttpHeaders().set('no-loader', 'true');
                    // const source = this._commonHttpService
                    //     .getArrayList({
                    //         where: {
                    //             searchkey: intakeEvalForm.sourcesearch,
                    //             evaluationsourceagencykey: intakeEvalForm.evaluationsourceagencykey
                    //         },
                    //         nolimit: true, method: 'get'
                    //     },
                    //         NewUrlConfig.EndPoint.Intake.EvaluationSourceSearchList + '?filter')
                    //     .map((evaluationSourceSearchList) => {
                    //         return {
                    //             evaluationSourceSearchList
                    //         };
                    //     })
                    //     .share();
                    this.evaluationSourceSearchList$ = this.http.get(this.baseUrl + '/' + NewUrlConfig.EndPoint.Intake.EvaluationSourceSearchList + '?filter=' + JSON.stringify({
                        where: {
                            searchkey: intakeEvalFormData.sourcesearch,
                            evaluationsourceagencykey: intakeEvalFormData.evaluationsourceagencykey
                        },
                        nolimit: true, method: 'get'
                    }), { headers: headers }).map((result) => {
                        return result;
                    });
                } else {
                    this.evaluationSourceSearchList$ = Observable.of([]);
                }

            }

        }
    }

    clearLawEnforcementSearch() {
        this.evalForm.patchValue({
            sourceTitle: '',
            sourceBadgeNo: '',
            sourcelastname: '',
            sourcefirstname: '',
            sourceStreetno: '',
            sourceStreet1: '',
            sourceStreet2: '',
            sourceKey: '', // source.evaluationsourcekey,
            sourceCount: '',
            evaluationsourceid: '', // temp id need to change once api deployed with id
            sourcesearch: ''
        });
    }

    private sourceSelected(source: EvaluationSourceObject) {
        this.sourceData = source;
        this.evalForm.patchValue({
            sourceTitle: source.title,
            sourceBadgeNo: source.badgeno,
            sourcelastname: source.lastname,
            sourcefirstname: source.firstname,
            sourceStreetno: source.streetno,
            sourceStreet1: source.street1,
            sourceStreet2: source.street2,
            sourceKey: source.evaluationsourcekey, // source.evaluationsourcekey,
            sourceCount: source.totalcount,
            evaluationsourceid: source.evaluationsourceid, // temp id need to change once api deployed with id
            sourcesearch: ''
        });

        this.evaluationSourceSearchList$ = Observable.of(new Array<EvaluationSourceObject>());
    }
    onIncidentDateOptionSet(event: boolean, option: string) {
        if (option === 'range' && event) {
            this.incidentDateOption = 2;
        } else if (option === 'unknown' && event) {
            this.incidentDateOption = 0;
        }
        this.setYouthAge();
    }

    onIncidentDateOptionChange(event: boolean, option: string) {
        this.updateReuiredOffenseDate(option);
        this.placeholderOffenseDate = 'Offense Date';
        if (option === 'range' && event) {
            this.evalForm.get('allegedoffensedate').clearValidators();
            this.evalForm.get('allegedoffensedate').updateValueAndValidity();
            this.incidentDateOption = 2;
            this.evalForm.patchValue({
                unknownrange: false,
                allegedoffenseknown: 2,
                begindate: this.oldDate.beginDate,
                enddate: this.oldDate.endDate
            });
        } else if (option === 'range' && !event) {
            this.evalForm.get('allegedoffensedate').setValidators([Validators.required]);
            this.evalForm.get('allegedoffensedate').updateValueAndValidity();
            this.evalForm.get('begindate').clearValidators();
            this.evalForm.get('begindate').updateValueAndValidity();
            this.evalForm.get('enddate').clearValidators();
            this.evalForm.get('enddate').updateValueAndValidity();
            if (this.evalForm.value.unknownrange) {
                this.incidentDateOption = 1;
                this.evalForm.patchValue({
                    allegedoffenseknown: 1,
                    begindate: '',
                    enddate: ''
                });
            } else {
                this.incidentDateOption = 0;
                this.evalForm.patchValue({
                    allegedoffenseknown: 0,
                    allegedoffensedate: this.oldDate.beginDate
                });
            }
        } else if (option === 'unknown' && event) {
            this.evalForm.get('allegedoffensedate').setValidators([Validators.required]);
            this.evalForm.get('allegedoffensedate').updateValueAndValidity();
            this.evalForm.get('begindate').clearValidators();
            this.evalForm.get('begindate').updateValueAndValidity();
            this.evalForm.get('enddate').clearValidators();
            this.evalForm.get('enddate').updateValueAndValidity();
            this.incidentDateOption = 0;
            this.placeholderOffenseDate = 'Please estimate the date of the alleged offense';
            this.evalForm.patchValue({
                dateRange: false,
                allegedoffenseknown: 1,
                begindate: '',
                enddate: ''
            });
        } else if (option === 'unknown' && !event) {
            this.incidentDateOption = 0;
            this.evalForm.patchValue({
                allegedoffenseknown: 0,
                allegedoffensedate: this.oldDate.beginDate
            });
        }
        this.setYouthAge();
    }

    private updateReuiredOffenseDate(option: string) {
        if (option === 'range') {
            this.evalForm.get('allegedoffensedate').clearValidators();
            this.evalForm.get('begindate').setValidators(Validators.required);
            this.evalForm.get('enddate').setValidators(Validators.required);
        } else {
            this.evalForm.get('allegedoffensedate').setValidators(Validators.required);
            this.evalForm.get('begindate').clearValidators();
            this.evalForm.get('enddate').clearValidators();
        }
        this.evalForm.get('allegedoffensedate').updateValueAndValidity();
        this.evalForm.get('begindate').updateValueAndValidity();
        this.evalForm.get('enddate').updateValueAndValidity();
    }

    patchForm(data: EvaluationFields) {
        if (data) {
            this.evalForm.patchValue(data);
            this.loadDefault();
            let offenceList = [];
            if (data.allegedoffense) {
                offenceList = data.allegedoffense.map(offence => offence.allegationid);
            }
            this.evalForm.patchValue({
                complaintrcddate: data.complaintreceiveddate ? new Date(data.complaintreceiveddate) : '',
                arrestdate: data.arrestdate ? new Date(data.arrestdate) : '',
                allegedoffensedate: data.allegedoffensedate ? new Date(data.allegedoffensedate) : '',
                beginDate: data.begindate ? new Date(data.begindate) : '',
                endDate: data.enddate ? new Date(data.enddate) : '',
                allegedoffense: offenceList
            });
            this.incidentDateOption = data.allegedoffenseknown;
            this.oldDate.beginDate = data.begindate;
            this.oldDate.endDate = data.enddate;
            if (data.allegedoffenseknown === 0) {
                this.evalForm.patchValue({
                    unknownrange: false,
                    dateRange: false
                });
            } else if (data.allegedoffenseknown === 1) {
                this.evalForm.patchValue({
                    dateRange: false,
                    unknownrange: true
                });
            } else if (data.allegedoffenseknown === 2) {
                this.evalForm.patchValue({
                    dateRange: true,
                    unknownrange: false
                });
            }
        }
    }


    private getYouthAge() {
        const intakeEvalForm = this.evalForm ? this.evalForm.getRawValue() : undefined;
        if (this.addedPersons && this.addedPersons.length > 0) {
            const youth = this.addedPersons.find((p) => p.Role === 'Youth');
            if (!youth || (youth && !youth.Dob)) {
                return null;
            }
            let youthDob;
            let offenceDate;
            youthDob = new Date(youth.Dob);
            if (!intakeEvalForm.unknownrange && (intakeEvalForm.allegedoffensedate || intakeEvalForm.beginDate)) {
                offenceDate = intakeEvalForm.dateRange ? intakeEvalForm.begindate : intakeEvalForm.allegedoffensedate;
                offenceDate = new Date(offenceDate);
            } else {
                offenceDate = new Date();
            }

            return this.getAgeInYearsAndMonth(offenceDate, youthDob);
        } else {
            return null;
        }
    }

    private getAgeInYearsAndMonth(offenceDate, youthDob) {
        // const timeDiff = offenceDate - youthDob;
        // const youthAge = new Date(timeDiff); // miliseconds from epoch

        // const offenseMonth = offenceDate.getMonth();
        // const youthMonth = youthDob.getMonth();

        // const monthDiff = offenseMonth - youthMonth;

        // let months = (monthDiff >= 0) ? (monthDiff) : (monthDiff + 12);

        // months = (offenceDate.getDate() < youthDob.getDate()) ? ((months - 1)) : months;

        // let years = Math.abs(youthAge.getUTCFullYear() - 1970);
        // if (months < 0) {
        //     months = 11;
        //     years = years - 1;
        // }

        // return years.toString() + ' Years ' + ((months > 0) ? months + ' Months' : '');
        const years = offenceDate ? moment(offenceDate).diff(youthDob, 'years', false) : moment().diff(youthDob, 'years', false);
        const totalMonths = offenceDate ? moment(offenceDate).diff(youthDob, 'months', false) : moment().diff(youthDob, 'months', false);
        this.youthAgeInMonths =  totalMonths;
        const months = totalMonths - (years * 12);
        console.log(years, (years * 12), totalMonths, months);
        return `${years} Years ${months} month(s)`;
    }

    private setYouthAge() {
        this.youthAge = this.getYouthAge();
        if (this.youthAge) {
            this.evalForm.patchValue({
                age: this.youthAge
            });
        }
        // this.evalForm.patchValue({
        //     age: youthAge,
        // });
    }

    checkYouthValidation() {
        const youthage = this.youthAgeInMonths;
        if (youthage < 84) {
            this._alertService.error('Age at time of the offense is less than 7 years');
        } else if (youthage > 216) {
            this._alertService.error('Age at time of the offense is greater than 18 years');
        }
    }

    // need refinement story for cross refference after multi compliants(Evals)
    isComplaintIdExists(addEval: boolean) {
        const intakeEvalForm = this.evalForm.getRawValue();
        const complaintid = intakeEvalForm.complaintid;
        const evaluation = this.evals.find(e => e.complaintid === complaintid);
        if (intakeEvalForm.countyid && !evaluation) {
            this._commonHttpService.getSingle(
                {
                    'method': 'get',
                    'where': {
                        'countyid': intakeEvalForm.countyid,
                        'complaintid': complaintid
                    }
                }, NewUrlConfig.EndPoint.Intake.validateComplaintId + '?filter')
                .subscribe((result) => {
                    if (result.isexists) {
                        if (result.crossrefs.length > 0 || result.intakecrossrefs.length > 0) {
                            const crossReference = result.crossrefs[0];
                            const intakeCrossReference = result.intakecrossrefs[0];
                            if ((crossReference && crossReference.srstatus === 'Rejected') || (intakeCrossReference && intakeCrossReference.status === 'rejected')) {
                                // this.complaintCrossReferenceSubject$.next(result);
                                this.broadcastCrossreference(result);
                            } else {
                                (<any>$('#active-intake-popup')).modal('show');
                                this.clearComplaintID();
                                // this.complaintCrossReferenceSubject$.next(new CrossReferenceResponse());
                                this.broadcastCrossreference(new CrossReferenceResponse());
                            }

                        } else {
                            (<any>$('#active-intake-popup')).modal('show');
                            this.clearComplaintID();
                            // this.complaintCrossReferenceSubject$.next(new CrossReferenceResponse());
                            this.broadcastCrossreference(new CrossReferenceResponse());
                        }

                    } else {
                        // this.complaintCrossReferenceSubject$.next(new CrossReferenceResponse());
                        this.broadcastCrossreference(new CrossReferenceResponse());
                    }

                    if (addEval) {
                        this.CreateOrUpdate();
                    }
                });
        } else if (evaluation) {
            this._alertService.error('Complaint ID already added for the Intake.');
            this.clearComplaintID();
        } else {
            this._alertService.error('Choose County to validate complaint id');
        }
    }

    broadcastCrossreference(crossRef: any) {
        const crossReference = crossRef.crossrefs;
        const intakeCrossReference = crossRef.intakecrossrefs;
        if (this.crossReferenceAdded) {
            const CR = this.addedCrossReference.find(cr => cr.complaintid === this.complaintid);
            if (CR) {
                this.addedCrossReference.splice(this.addedCrossReference.indexOf(CR), 1);
            } else {
                this.addedCrossReference.pop();
            }
            this.crossReferenceAdded = false;
        }
        if (this.intakeCrossReferenceAdded) {
            const CR = this.addedIntakeCrossReference.find(cr => cr.complaintid === this.complaintid);
            if (CR) {
                this.addedIntakeCrossReference.splice(this.addedIntakeCrossReference.indexOf(CR), 1);
            } else {
                this.addedIntakeCrossReference.pop();
            }
            this.intakeCrossReferenceAdded = false;
        }
        if (crossReference && crossReference.length > 0) {
            const reasonForCrossReff = 'Complaint ID Exists';
            this.addCrossReference(reasonForCrossReff, crossReference[0], crossReference[0].servicerequestnumber);
            this.crossReferenceAdded = true;
        } else if (intakeCrossReference && intakeCrossReference.length > 0) {
            this.addIntakeCrossReference(intakeCrossReference);
            this.intakeCrossReferenceAdded = true;
        } else {
            this.crossReferenceAdded = false;
            this.intakeCrossReferenceAdded = false;
        }
        const allCrossReference = {
            intakeCrossReference: this.addedIntakeCrossReference,
            crossReference: this.addedCrossReference
        };
        this._datastore.setData(IntakeStoreConstants.crossReferenceFromEvals, allCrossReference);
    }

    addCrossReference(ReasonsofCrossref: string,
        responseModel: CrossReferenceSearchResponse, servicerequestnumber: string) {
        const isAdded = this.addedCrossReference.filter(
            item =>
                item.servicerequestnumber ===
                responseModel.servicerequestnumber &&
                item.ReasonsofCrossref === ReasonsofCrossref
        );
        if (!isAdded.length) {
            // responseModel.crossRefwith = this.crossRefernceQuickSearchForm.get(
            //     'servicerequestnumber'
            // ).value;
            responseModel.crossRefwith = servicerequestnumber;
            responseModel.ReasonsofCrossref = ReasonsofCrossref;
            responseModel = Object.assign({}, responseModel);
            this.addedCrossReference.push(responseModel);
            this._alertService.success(CR_COMPLAINT_EXIST_SUCESS_MESSAGE);
        } else {
            this._alertService.error('Cross reference already exists');
        }
    }

    addIntakeCrossReference(responseModel: IntakeCrossReferenceSearchResponse[]) {
        const isAdded = this.addedIntakeCrossReference.filter(
            item =>
                responseModel.filter(model => model.intakenumber === item.intakenumber)
        );
        if (!isAdded.length) {
            this.addedIntakeCrossReference = responseModel;
            this._alertService.success(CR_COMPLAINT_EXIST_SUCESS_MESSAGE);
        } else {
            this._alertService.error('Intake Cross reference already exists');
        }
    }

    clearComplaintID() {
        this.evalForm.patchValue({ complaintid: '' });
    }

    broadCastEvaluationFields() {
        const nojusdication = {list : []};
        const youthage = this.youthAgeInMonths;
        const complaintInfoReview = { message: [], status: false, No_Jurisdiction: [], Insufficient_Information: [] };
        if (youthage < 84) {
            nojusdication.list.push('Age at time of the offense is less than 7 years');
        } else if (youthage > 216) {
            nojusdication.list.push('Age at time of the offense is greater than 18 years');
        }
        this.evals.map(data => {
            data.allegedoffense.map(ele => {
                const agecutoff = (this.offenceCategoryListData[ele] ? this.offenceCategoryListData[ele].agecutoff : 0) * 12;
                if (youthage >= agecutoff) {
                    nojusdication.list.push(`Youth's age is greater than the ${this.offenceCategoryListData[ele].name} age cutoff`);
                }
            });
        });
        complaintInfoReview.No_Jurisdiction = nojusdication.list;
        this._datastore.setData(IntakeStoreConstants.complaintInfoReview, complaintInfoReview);
        this._datastore.setData(IntakeStoreConstants.evalFields, this.evals);
    }

    CreateOrUpdate() {
        const val = this.evalForm.getRawValue();
        ControlUtils.validateAllFormFields(this.evalForm);
        // D-07789 Law Enforcement (Police Source) Information
        if (this.isLawEnforcement) {
            if (!(val.sourceTitle && val.sourcelastname && val.sourcefirstname && val.evaluationsourceid)) {
                this._alertService.warn('Please fill Law Enforcement Search');
                return false;
            }
        }
        if (this.evalForm.valid) {
            val.victims = val.victims.map(victimid => this.victims.find(victim => victim.personid === victimid));
            if (this.enableWaiverField) {
                val.iscreatepetition = true;
                if (val.complaintid && val.complaintid.length > 3) {
                    val.complaintid = val.complaintid.substr(-3, 3) === 'WVR' ? val.complaintid : (val.complaintid + 'WVR');
                } else {
                    val.complaintid = val.complaintid + 'WVR';
                }
            } else {
                val.iscreatepetition = false;
            }
            if (this.evalAction === 'edit') {
                if (this.editIndex >= 0) {
                    if (this.enableWaiverField) {
                        if (this.editIndex === 0 && this.evals.length > 1) {
                            const adultpetitionid = this.evals[this.editIndex].adultpetitionid;
                            const waiverpetitionid = this.evals[this.editIndex].waiverpetitionid;
                            this.evals.map(item => {
                                if (adultpetitionid !== val.adultpetitionid) {
                                    item.adultpetitionid = val.adultpetitionid;
                                }
                                if (waiverpetitionid !== val.waiverpetitionid) {
                                    item.waiverpetitionid = val.waiverpetitionid;
                                }
                            });
                        }
                    }
                    this.evals[this.editIndex] = val;
                }
            } else if (this.evalAction === 'add') {
                if (this.isValidEvals(val)) {
                    this.evals.push(val);
                }
            }
            this.broadCastEvaluationFields();
            (<any>$('#eval-form-popup')).modal('hide');
            this.resetEvalForm();
        } else {
            this._alertService.warn('Please fill mandatory details');
        }
    }

    openEvalForm() {
        this.crossReferenceAdded = false;
        this.intakeCrossReferenceAdded = false;
        if (!this.purposeID || this.purposeID === '') {
            this._alertService.warn('Please select Purpose.');
            return;
        }
        this.resetEvalForm();
        this.initEvalForm();
        this.loadSourceAgencyTypeDropdown();
        this.loadOffenseLocationType();
        // DJS-017 Complaint Received Date - should be auto-populated with the Received date from the referral screen
        const receivedDate = this.store[IntakeStoreConstants.receivedDate];
        setTimeout(() => {
            this.evalForm.patchValue({ complaintreceiveddate: receivedDate ? new Date(receivedDate) : new Date() });
        }, 100);
        setTimeout(() => {
            this.evalForm.enable();
            this.evalForm.get('age').disable();
            this.evalAction = 'add';
            this.evalForm.get('evaluationsourcetype').setValue(this.selectedPurpose);
            this.evalForm.get('evaluationsourcetypeid').setValue(this.purposeID);
            this.evalForm.get('evaluationsourcetype').disable();
            if (this.enableWaiverField) {
                if (this.evals && this.evals.length > 0) {
                    this.evalForm.patchValue({
                        adultpetitionid: this.evals[0].adultpetitionid,
                        waiverpetitionid: this.evals[0].waiverpetitionid
                    });
                    this.evalForm.get('adultpetitionid').disable();
                    this.evalForm.get('waiverpetitionid').disable();
                }
            }
        }, 10);

        this.updateReuiredOffenseDate('notRange');

        // this.initEvalForm(null, false);
        //  this.setYouthAge();
        (<any>$('#eval-form-popup')).modal('show');
    }
    resetEvalForm() {
        this.deleteIndex = -1;
        this.victimArray = [];
        this.incidentDateOption = 0;
        this.isViewMode = false;
        // this.evalForm.reset();
        // this.evalForm.enable();
        // this.initEvalForm();
        this.placeholderOffenseDate = 'Offense Date';
    }
    isValidEvals(form: any) {

        const foundObj = this.evals.find(item => item.complaintid === form.complaintid);
        if (foundObj) {
            this._alertService.error('Complaint ID already exist');
            return false;
        }
        return true;
    }

    viewEdit(data: any, isView: boolean, index: number) {
        this.crossReferenceAdded = false;
        this.intakeCrossReferenceAdded = false;
        this.resetEvalForm();
        this.initEvalForm();
        const EvalData = JSON.parse(JSON.stringify(data));

        EvalData.victims = data.victims.map(vicitm => vicitm.personid);
        this.onIncidentDateOptionSet(EvalData.dateRange, 'range');
        this.onIncidentDateOptionSet(EvalData.unknownrange, 'unknown');
        if (this.enableWaiverField) {
            if (EvalData.complaintid && EvalData.complaintid.length > 3) {
                EvalData.complaintid = EvalData.complaintid.substr(-3, 3) === 'WVR' ? (EvalData.complaintid.slice(0, -3)) : EvalData.complaintid;
            }
            this.evalForm.patchValue({ complaintid: EvalData.complaintid });
        }
        this.complaintid = EvalData.complaintid;
        this.evalForm.patchValue(EvalData);
        this.evalForm.patchValue({ 'victims': EvalData.victims });
        console.log('form victim', this.evalForm.getRawValue().victims);
        console.log('data', EvalData.victims);
        this.editIndex = index;
        // this.isViewMode = isView;
        this.loadSourceAgencyTypeDropdown();
        this.loadOffenseLocationType();
        setTimeout(() => {
            if (isView) {
                this.evalForm.disable();
                this.evalAction = 'view';
            } else {
                this.evalForm.enable();
                this.evalAction = 'edit';
            }
            this.loadDefault();

            this.evalForm.get('age').disable();
            this.evalForm.get('evaluationsourcetype').disable();
            if (this.evals && this.evals.length > 0 && this.editIndex !== 0) {
                this.evalForm.patchValue({
                    adultpetitionid: this.evals[0].adultpetitionid,
                    waiverpetitionid: this.evals[0].waiverpetitionid
                });
                this.evalForm.get('adultpetitionid').disable();
                this.evalForm.get('waiverpetitionid').disable();
            }
        }, 10);


    }

    initEvalForm() {
        this.evalForm = this.formBuilder.group({
            isdetention: false,
            petitionid: [''],
            adultpetitionid: [''],
            evaluationsourceagencyid: [''],
            evaluationsourceagencykey: [''],
            evaluationsourcetypeid: [''],
            evaluationsourcetypekey: [''],
            complaintid: [''],
            wvrnumber: [''],
            offenselocation: [''],
            complaintreceiveddate: [''],
            arrestdate: [''],
            zipcode: [''],
            countyid: [null],
            countyname: [''],
            allegedoffense: [[]],
            allegedoffenseknown: [0],
            allegedoffensedate: [''],
            victims: [[]],
            begindate: [''],
            enddate: [''],
            evaluationsourceagencyname: [''],
            evaluationsourcetypename: [''],
            victimname: [[]],
            unknownrange: false,
            dateRange: false,
            // isdrai: false,
            // ismcasp: false,
            age: [this.youthAge ? this.youthAge : ''],
            yearsofage: [0],
            sourcesearch: '',
            sourceTitle: '',
            sourceBadgeNo: '',
            sourcelastname: '',
            sourcefirstname: '',
            sourceStreetno: '',
            sourceStreet1: '',
            sourceStreet2: '',
            sourceKey: '',
            sourceCount: '',
            evaluationsourceid: null, // temp id need to change once api deployed with id
            offencelocationtypekey: '',
            evaluationsourcetype: '',
            waivedoffense: '',
            waiverpetitionid: '',
            isMcapsReq: false
        });
        this.lawEnforcementSearchForm = this.formBuilder.group({
            title: '',
            badgeno: '',
            lastname: '',
            firstname: '',
            streetno: '',
            street1: '',
            street2: '',
            evaluationsourceid: ''
        });
    }
    deleteEval(deleteIndex) {
        this.deleteIndex = deleteIndex;
        // this.evals.splice(deleteIndex, 1);
        // this.broadCastEvaluationFields();
    }

    onDeleteEval() {
        if (this.deleteIndex !== -1) {
            this.evals.splice(this.deleteIndex, 1);
            this.broadCastEvaluationFields();
            this.resetEvalForm();
            (<any>$('#eval-delete-popup')).modal('hide');
            this._alertService.success('Complaint Deleted Successfully');
        }
    }

    listAllegations(purposeID) {
        const checkInput = {
            where: { intakeservreqtypeid: purposeID },
            method: 'get',
            nolimit: true,
            order: 'name'
        };
        this._commonHttpService
            .getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter')
            .subscribe(offenceCategories => {
                if (offenceCategories) {
                    offenceCategories.forEach(offence => {
                        this.offenceCategoryList[offence.allegationid] = offence.name;
                        this.offenceCategoryListData[offence.allegationid] = offence;
                    });
                    this.offenceCategories$ = Observable.of(offenceCategories);
                }
            });
    }

    validateYouthAge(value) {
        const youthage = this.youthAgeInMonths;
        if (value && value.length > 0 && value[value.length - 1]) {
            const allegation = this.offenceCategoryListData[value[value.length - 1]];
            const offenceLists = {mcaspList: [], victimList: []};
            value.forEach(element => {
                const offence = this.offenceCategoryListData[element];
                if (offence && offence.mcaprequired) {
                    offenceLists.mcaspList.push(offence);
                }
                if (offence && offence.isvictimrequired) {
                    offenceLists.victimList.push(offence);
                }
            });

            if (offenceLists.mcaspList && offenceLists.mcaspList.length > 0) {
                this.evalForm.patchValue({
                isMcapsReq: true
                });
            }

            if (offenceLists.victimList && offenceLists.victimList.length > 0) {
                this.isvictimrequired = true;
                this.evalForm.get('victims').setValidators(Validators.required);
                this.evalForm.get('victims').updateValueAndValidity();
            } else {
                this.isvictimrequired = false;
                this.evalForm.get('victims').clearValidators();
                this.evalForm.get('victims').updateValueAndValidity();
            }
            const agecutoff = (allegation ? allegation.agecutoff : 0) * 12;
            if (youthage >= agecutoff) {
                this._alertService.error(`Youth's age is greater than the offense age cutoff`);
            }
        }
    }

    addLawEnforcementSearch(index: number) {
        if (index === 0) {
            this.lawEnforcementSearchForm.reset();
            (<any>$('#eval-form-popup')).modal('hide');
            (<any>$('#lawenforcement-search-popup')).modal('show');
        } else if (index === 3) {
            this.lawEnforcementSearchForm.reset();
            (<any>$('#eval-form-popup')).modal('show');
            (<any>$('#lawenforcement-search-popup')).modal('hide');
        } else {
            const lawEnforcementModel = this.lawEnforcementSearchForm.getRawValue();
            lawEnforcementModel.evaluationsourceagencykey = this.evalForm.get('evaluationsourceagencykey').value;
            this._commonHttpService.create(lawEnforcementModel,
              CommonUrlConfig.EndPoint.Intake.lawEnforcementSearchAdd).subscribe(response => {
                this._alertService.success('Lawenforcement Search Submitted successfully.');
                (<any>$('#eval-form-popup')).modal('show');
                this.evalForm.patchValue({
                    sourceTitle: response.title,
                    sourceBadgeNo: response.badgeno,
                    sourcelastname: response.lastname,
                    sourcefirstname: response.firstname,
                    sourceStreetno: response.streetno,
                    sourceStreet1: response.street1,
                    sourceStreet2: response.street2,
                    sourceKey: response.evaluationsourcekey,
                    sourceCount: response.sourceCount,
                    evaluationsourceid: response.evaluationsourceid
                });
                (<any>$('#lawenforcement-search-popup')).modal('hide');
              }, error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                (<any>$('#eval-form-popup')).modal('show');
                (<any>$('#lawenforcement-search-popup')).modal('hide');
              });
        }
    }

    ngOnDestroy(): void {
        console.log('router url', this._router.url);
        this.dataStroeSubscription.unsubscribe();
    }
}
