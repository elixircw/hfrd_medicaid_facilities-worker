import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeAttachmentsRoutingModule } from './intake-attachments-routing.module';
import { IntakeAttachmentsComponent } from './intake-attachments.component';
import { AudioRecordComponent } from './audio-record/audio-record.component';
import { VideoRecordComponent } from './video-record/video-record.component';
import { ImageRecordComponent } from './image-record/image-record.component';
import { AttachmentUploadComponent } from './attachment-upload/attachment-upload.component';
import { AttachmentDetailComponent } from './attachment-detail/attachment-detail.component';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    IntakeAttachmentsRoutingModule,
    SharedPipesModule,
    FormMaterialModule,
    PdfViewerModule,
    NgxfUploaderModule,
    ControlMessagesModule,
    A2Edatetimepicker,
    NgSelectModule

  ],
  declarations: [IntakeAttachmentsComponent,
    AudioRecordComponent,
    VideoRecordComponent,
    ImageRecordComponent,
    AttachmentDetailComponent,
    AttachmentUploadComponent,
    AttachmentDetailComponent,
    EditAttachmentComponent

  ],
  providers: [SpeechRecognitionService, SpeechRecognizerService, NgxfUploaderService]
})
export class IntakeAttachmentsModule { }
