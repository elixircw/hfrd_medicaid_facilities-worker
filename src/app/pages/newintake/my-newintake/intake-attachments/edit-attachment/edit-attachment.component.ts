import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NewUrlConfig } from '../../../newintake-url.config';
import { GenericService, AlertService, CommonHttpService, AuthService, DataStoreService } from '../../../../../@core/services';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Rx';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { Attachment } from '../_entities/attachmnt.model';
import { IntakeStoreConstants } from '../../my-newintake.constants';
import { AttachmentSubCategory } from '../../_entities/newintakeModel';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'edit-attachment',
    templateUrl: './edit-attachment.component.html',
    styleUrls: ['./edit-attachment.component.scss']
})
export class EditAttachmentComponent implements OnInit {
    attachmentClassificationTypeDropDown$: Observable<DropdownModel[]>;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    fileUpdate: FormGroup;
    token: AppUser;

    @Input() intakeNumber: string;
    attachmentDetail: Attachment;
    @Output() attachment = new EventEmitter();
    subCategoryClassificationType$: Observable<AttachmentSubCategory[]>;
    store: any;
    createdCases = [];
    subCategoryId: any;
    subCategoryList = [];
    subCategoryList$: Observable<AttachmentSubCategory[]>;
    subType= [];
    attachmentClassificationtypelookup = [];
    attachmentClassificationtype = [];
    isCW: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _dropDownService: CommonHttpService,
        private _service: GenericService<Attachment>,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService
    ) {
        this.token = this._authService.getCurrentUser();
        this.store = this._dataStoreService.getCurrentStore();
    }

    ngOnInit() {
        this.isCW = this._authService.isCW();
        this.loadDropdown();
        // console.log(this.attachmentDetail);
        this.fileUpdate = this.formBuilder.group({
            title: [''],
            description: ['', Validators.maxLength(150)],
            attachmentTypeKey: [''],
            attachmentClassificationTypeKey: [''],
            assessmenttemplateid: [''],
        });
        this.getSubCategory();
    }
    editForm(attachmentDetail) {
        console.log('attachmentDetail', attachmentDetail);
        this.fileUpdate.markAsPristine();
        // console.log(attachmentDetail);
        this.fileUpdate.patchValue({
            title: attachmentDetail.title ? attachmentDetail.title : '',
            description: attachmentDetail.description ? attachmentDetail.description : '',
            attachmentTypeKey: '',
            attachmentClassificationTypeKey: '',
            assessmenttemplateid: ''
        });
        if (attachmentDetail.documentattachment) {
            if (attachmentDetail.documentattachment.assessmenttemplateid) {
                this.subCategoryId = attachmentDetail.documentattachment.assessmenttemplateid;
            }
            this.fileUpdate.patchValue({
                attachmentTypeKey: attachmentDetail.documentattachment.attachmenttypekey ? attachmentDetail.documentattachment.attachmenttypekey : '',
                attachmentClassificationTypeKey: attachmentDetail.documentattachment.attachmentclassificationtypekey ? attachmentDetail.documentattachment.attachmentclassificationtypekey : '',
                assessmenttemplateid: attachmentDetail.documentattachment.assessmenttemplateid ? attachmentDetail.documentattachment.assessmenttemplateid : ''
            });
        } else {
            attachmentDetail.documentattachment = Object.assign({});
        }
        // this.currDate = new Date();
        this.attachmentDetail = attachmentDetail;
    }

    resetForm() {
        // this.fileUpdate.reset();
        // this.fileUpdate.patchValue({
        //     attachmentTypeKey: '',
        //     attachmentClassificationTypeKey: ''
        // });
        (<any>$('#edit-attachment')).modal('hide');
    }
    saveAttachmentDetails() {
        if (this.fileUpdate.value.title !== '' && this.fileUpdate.value.attachmentTypeKey !== '' && this.fileUpdate.value.attachmentClassificationTypeKey !== '') {
            this.attachmentDetail.title = this.fileUpdate.value.title;
            this.attachmentDetail.description = this.fileUpdate.value.description;
            this.attachmentDetail.documentattachment.attachmenttypekey = this.fileUpdate.value.attachmentTypeKey;
            this.attachmentDetail.documentattachment.attachmentclassificationtypekey = this.fileUpdate.value.attachmentClassificationTypeKey;
            this.attachmentDetail.documentattachment.assessmenttemplateid = this.fileUpdate.value.assessmenttemplateid;
            this.attachmentDetail.documentattachment.updatedby = this.token.user.userprofile.displayname;
            this.attachmentDetail.documentattachment.attachmentdate = new Date();
            this.attachmentDetail.documentdate = new Date();
            this._service.endpointUrl = NewUrlConfig.EndPoint.Intake.SaveAttachmentUrl;
            this._service.createArrayList([this.attachmentDetail]).subscribe(
                (response) => {
                    // console.log(response);
                    this._alertService.success('Attachment updated successfully!');
                    (<any>$('#edit-attachment')).modal('hide');
                    this.attachment.emit('all');
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._alertService.error('Please fill all mandatory fields');
        }
    }
    private loadDropdown() {

        this._dropDownService
        .getSingle(
            {},
            NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
        )
        .subscribe(data => {
            const dp_att_arr = [];
            if (data && data.length > 0) {
                this.attachmentClassificationtypelookup = data;
                for (let i = 0; i < this.attachmentClassificationtypelookup.length; i++) {
                    if (this.attachmentClassificationtypelookup[i].typedescription && dp_att_arr.indexOf(this.attachmentClassificationtypelookup[i].typedescription) < 0 ) {
                        if (this.isCW) {
                            if (this.attachmentClassificationtypelookup[i].typedescription.startsWith('CW-')) {
                                this.attachmentClassificationtype.push({typedescription: this.attachmentClassificationtypelookup[i].typedescription});
                                dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                            }
                        } else {
                            this.attachmentClassificationtype.push({typedescription:this.attachmentClassificationtypelookup[i].typedescription});
                            dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                        }                    }
                }
            }
        });

        const source = forkJoin(
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentTypeUrl + '?filter={"nolimit": true}'
            ),
            // this._dropDownService.getArrayList(
            //     {
            //         nolimit: true
            //     },
            //     NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
            // )
        )
            .map((result) => {
                return {
                    attachmentType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmenttypekey
                            })
                    ),
                    // attachmentClassificationType: result[1].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.typedescription,
                    //             value: res.attachmentclassificationtypekey
                    //         })
                    // )
                };
            })
            .share();
        this.attachmentTypeDropdown$ = source.pluck('attachmentType');
        // this.attachmentClassificationTypeDropDown$ = source.pluck('attachmentClassificationType');
    }

    categoryUpdate(event) {
        console.log('event...', event.target.value);
        if (this._authService.getCurrentUser().user.userprofile.teamtypekey === 'AS') {
            // if (this.createdCases.length > 0) {
                // for (let i = 0; i < this.createdCases.length; i++) {
                    // if (this.createdCases[i].subSeriviceTypeValue === 'Project Home' || this.createdCases[i].subSeriviceTypeValue === 'Adult Foster Care') {
                        if (event.target.value === 'Assessment Document') {
                            this.getSubCategory();
                        } else {
                            this.subCategoryList$ = null;
                        }
                   // }
                // }
          //  }
        }
    }

    getSubCategory() {
        console.log('store...', this.store);
        if (this.store[IntakeStoreConstants.purposeSelected]) {
            const purpose = this.store[IntakeStoreConstants.purposeSelected];
            this.subType = this.store[IntakeStoreConstants.createdCases];
            // if (this.store['purposesubtype']) {
                // const subType =  this.store['purposesubtype'];
                if (this.subType && this.subType.length > 0) {
                   const purposeSubType = this.subType[0].subServiceTypeID;
                this.subCategoryClassificationType$ = this._dropDownService
            .getArrayList(
                {
                    where: {intakeservicerequesttypeid: purpose.value,
                        intakeservicerequestsubtypeid: purposeSubType,
                        agencycode: 'AS',
                        target: 'Intake'},
                        method: 'get'
                },
                'admin/assessmenttemplate/listassessmenttemplate?filter'
                )
                .map(result => {
                    return result;
                });
                this.subCategoryClassificationType$.subscribe(result => {
                    console.log('result', result);
                    if (result && result.length > 0) {
                        this.subCategoryList = [];
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].isrequired === true) {
                            this.subCategoryList.push(result[i]);
                             }
                        }
                    }
                    this._dataStoreService.setData('categorySubType', this.subCategoryList);
                    // this._dataStoreService.setData(IntakeStoreConstants.attachements, this.subCategoryList);
                    this.subCategoryList$ = Observable.of(this.subCategoryList);
                });
            }
        }
    }
}
