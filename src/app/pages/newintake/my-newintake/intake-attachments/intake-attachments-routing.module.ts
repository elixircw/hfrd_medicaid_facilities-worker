import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeAttachmentsComponent } from './intake-attachments.component';
import { AudioRecordComponent } from './audio-record/audio-record.component';
import { VideoRecordComponent } from './video-record/video-record.component';
import { ImageRecordComponent } from './image-record/image-record.component';
import { AttachmentUploadComponent } from './attachment-upload/attachment-upload.component';

const routes: Routes = [{
  path: '',
  component: IntakeAttachmentsComponent,
  children: [
    { path: 'audio-record', component: AudioRecordComponent },
    { path: 'video-record', component: VideoRecordComponent },
    { path: 'image-record', component: ImageRecordComponent },
    { path: 'attachment-upload', component: AttachmentUploadComponent }
  ]
},
{ path: 'generate', loadChildren: './intake-attachments-generate/intake-attachments-generate.module#IntakeAttachmentsGenerateModule' }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeAttachmentsRoutingModule { }
