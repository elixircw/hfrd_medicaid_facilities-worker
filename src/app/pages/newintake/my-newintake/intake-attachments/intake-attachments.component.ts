import { Component, Input, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
// import { FinanceUrlConfig } from '../../../../finance.url.config';
 import { Observable } from 'rxjs/Observable';
 import * as jsPDF from 'jspdf';
// import { AddForm } from '../../../../../_entities/finance-entity.module';
// import { Observable, Subject } from 'rxjs/Rx';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { HttpHeaders } from '@angular/common/http';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
// import { AttachmentUpload } from '../../_entities/caseworker.data.model';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, CommonHttpService, GenericService, DataStoreService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { NewUrlConfig } from '../../newintake-url.config';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import { Attachment } from './_entities/attachmnt.model';
import { InvolvedPerson } from '../_entities/newintakeModel';
import { GeneratedDocuments } from '../_entities/newintakeSaveModel';
import { HttpService } from '../../../../@core/services/http.service';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';

const SCREENING_WORKER = 'SCRNW';
const SUPERVISOR = 'apcs';
const CLW = 'Court Liaison Worker';
const CASE_WORKER = 'Case Worker';
declare var $: any;
import * as ALL_DOCUMENTS from './_configurtions/documents.json';
import { SafeResourceUrl } from '@angular/platform-browser';
import { IntakeStoreConstants } from '../my-newintake.constants';
import { AppConstants } from '../../../../@core/common/constants';
import { IntakeConfigService } from '../intake-config.service';
import * as moment from 'moment';
import { connect } from 'http2';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-attachments',
    templateUrl: './intake-attachments.component.html',
    styleUrls: ['./intake-attachments.component.scss']
})
export class IntakeAttachmentsComponent implements OnInit, AfterViewInit {
    beforeSubmit: boolean;
    fileToSave = [];
    uploadedFile = [];
    petitions: any;

    documentPropertiesId: any;
    documentId: any;
    filteredAttachmentGrid: Attachment[] = [];
    intakeNumber: string;
    // formAdd: AddForm;
    token: AppUser;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    daNumber: string;
    reviewStatus: string;
    id: string;
    attachmentType: FormGroup;
    generateDocForm: FormGroup;
    baseUrl: string;
    showScreens = { uploadDocument: false, uploadedDocument: false, createDocument: true };
    searchText = '';
    private allAttachmentGrid: Attachment[];
    @ViewChild(EditAttachmentComponent) editAttach: EditAttachmentComponent;
    generatedDocuments: GeneratedDocuments[] = [];
    allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;
    config = { isGenerateUploadTabNeeded: false };
    downldSrcURL: any;
    generatedDocListCW: any[];
    generatedCWDocUrl: SafeResourceUrl;
    generatedDocUrl: SafeResourceUrl;
    isCLW: boolean;
    complaints = [];
    isGenerateByVictim: boolean;
    isGenerateByComplaint: boolean;
    isGenerateByPetition: boolean;
    victims = [];
    selectedDocument: any;
    isacknowledgementletter: any;
    selectedComplaint: any;
    offenceCategoryList: any;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];

    private store: any;
    contactrecording: any[] = [];
    reasonForContact: any[];
    isClosed = false;
    constructor(
        private formBuilder: FormBuilder,
        private _commonService: CommonHttpService,
        private _service: GenericService<Attachment>,
        private route: ActivatedRoute,
        private _uploadService: NgxfUploaderService,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _http: HttpService,
        // private _ativityService: GenericService<AddForm>,
        private _store: DataStoreService,

        private _intakeConfig: IntakeConfigService
    ) {
        this.id = route.snapshot.params['id'];
        this.baseUrl = AppConfig.baseUrl;
        this.store = this._store.getCurrentStore();
    }

    ngOnInit() {
        this.intakeNumber = this.store[IntakeStoreConstants.intakenumber] ? this.store[IntakeStoreConstants.intakenumber] : this.route.snapshot.parent.parent.parent.params['intakenumber'];
        this.isacknowledgementletter = this.store[IntakeStoreConstants.addNarrative] ? this.store[IntakeStoreConstants.addNarrative].isacknowledgementletter : [];
        this.chooseTab(1);
        this.loadDropdown();
        this.token = this._authService.getCurrentUser();
        this.buildForms();
        this.initGenerateDocForm();
        this.generatedDocuments = this.store[IntakeStoreConstants.generatedDocuments] ? this.store[IntakeStoreConstants.generatedDocuments] : [];
        this.loadGeneratedDocumentList(this.token, true);
        this.prepareConfig();
        this.getContactRecordings();
        this.attachment('All');
        this.getReasonForContact();
        if (this._intakeConfig.getIntakePurpose()) {
            this.listAllegations(this._intakeConfig.getIntakePurpose().intakeservreqtypeid);
        }
        const currentStatus = this._store.getData(IntakeStoreConstants.INTAKE_STATUS);
        if (currentStatus === 'Closed' || currentStatus === 'Completed') {
            this.isClosed = true;
           } else {
            this.isClosed = false;
           }
    }

    ngAfterViewInit() {
            if (!this._intakeConfig.getiseditIntake()) {
                (<any>$(':button')).prop('disabled', true);
            }
    }

    listAllegations(purposeID) {
        const checkInput = {
            where: { intakeservreqtypeid: purposeID },
            method: 'get',
            nolimit: true,
            order: 'name'
        };
        this._commonService
            .getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter')
            .subscribe(offenceCategories => {
                if (offenceCategories) {
                    this.offenceCategoryList = [];
                    offenceCategories.forEach(offence => {
                        this.offenceCategoryList[offence.allegationid] = offence.name;
                    });
                }
            });
    }

    buildForms() {

        this.attachmentType = this.formBuilder.group({
            selectedAttachment: ['All']
        });
    }
    initGenerateDocForm() {
        this.generateDocForm = this.formBuilder.group({
            complaint: [''],
            victim: [''],
            petition: ['']
        });
    }
    chooseTab(tabId) {
        if (tabId === 1) {
            this.showScreens = { uploadDocument: true, uploadedDocument: false, createDocument: false };
        } else if (tabId === 2) {
            this.showScreens = { uploadDocument: false, uploadedDocument: true, createDocument: false };
        } else if (tabId === 3) {
            this.showScreens = { uploadDocument: false, uploadedDocument: false, createDocument: true };
        }
    }
    // getIntakeNumber(intakeNumber) {
    //     this.intakeNumber = intakeNumber;
    //     this.attachment('All');
    // }
    filterAttachment(attachType) {
        if (attachType.selectedAttachment === 'All') {
            this.filteredAttachmentGrid = this.allAttachmentGrid;
        } else if (attachType.selectedAttachment === 'Exhibit') {
            this.filteredAttachmentGrid = this.allAttachmentGrid.filter(
                item => item.documenttypekey === attachType.selectedAttachment
            );
        } else {
            this.filteredAttachmentGrid = this.allAttachmentGrid.filter(
                item =>
                    item.documentattachment &&
                    item.documentattachment.attachmenttypekey ===
                    attachType.selectedAttachment
            );
        }
    }
    checkFileType(file: string, accept: string): boolean {
        if (accept) {
            const acceptedFilesArray = accept.split(',');
            return acceptedFilesArray.some(type => {
                const validType = type.trim();
                if (validType && validType.charAt(0) === '.') {
                    return file.toLowerCase().endsWith(validType.toLowerCase());
                }
                return false;
            });
        }
        return true;
    }
    editAttachment(modal) {
        this.editAttach.editForm(modal);
        (<any>$('#edit-attachment')).modal('show');
    }
    confirmDelete(modal) {
        this.documentPropertiesId = modal.documentpropertiesid;
        this.documentId = modal.filename;
        (<any>$('#delete-attachment')).modal('show');
    }
    deleteAttachment() {
        const workEnv = config.workEnvironment;
        if (workEnv === 'state') {
            this._service.endpointUrl =
                NewUrlConfig.EndPoint.Intake.DeleteAttachmentUrl;
            const id = this.documentPropertiesId + '&' + this.documentId;
            this._service.remove(id).subscribe(
                result => {
                    (<any>$('#delete-attachment')).modal('hide');
                    this.attachment('All');
                    this.getContactRecordings();
                    this._alertService.success('Attachment Deleted successfully!');
                },
                err => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        } else {
            this._service.endpointUrl =
                NewUrlConfig.EndPoint.Intake.DeleteAttachmentUrl;
            this._service.remove(this.documentPropertiesId).subscribe(
                result => {
                    (<any>$('#delete-attachment')).modal('hide');
                    this.attachment('All');
                    this.getContactRecordings();
                    this._alertService.success('Attachment Deleted successfully!');
                },
                err => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    attachment(attachType) {
        this._commonService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    order: 'originalfilename',
                    method: 'get'
                }),
                NewUrlConfig.EndPoint.Intake.AttachmentGridUrl +
                '/' +
                this.intakeNumber +
                '?data'
            )
            .subscribe(result => {
                this.allAttachmentGrid = result;
                // this.filteredAttachmentGrid = this.allAttachmentGrid;
                result.map(item => {
                    item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                });
                this.filteredAttachmentGrid = result;
                this._store.setData(IntakeStoreConstants.attachements, result);
            });
    }

    getContactRecordings() {
        const source = this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 100,
                    where: {},
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetAllDaRecordingUrl + '/' + this.intakeNumber + '?data'
            )
            .subscribe((result) => {
                this.contactrecording = result.data;
                this.setcontacttype();
            });
    }
    getReasonForContact() {
        this._commonService.getSingle({}, 'Progressnotereasontypes?filter={"nolimit":true}').map((itm) => {
            return itm;
        }).subscribe(data => {
            this.reasonForContact = data;
            this.setcontacttype();
        });
    }
    setcontacttype() {
        if (Array.isArray(this.reasonForContact) && Array.isArray(this.contactrecording)) {
            this.contactrecording.forEach(item => {
                const pr = item.progressnotereasontypekey.split(',');
                let description = '';
                pr.forEach((element, index) => {
                    const reason = this.reasonForContact.find(re => re.progressnotereasontypekey === element);
                    description = ((index === (pr.length - 1))) ? description + reason.typedescription : description + reason.typedescription + ', ';
                });
                item.progressnotereasontypedescription = description;
            });
        }
    }


    downloadFile(s3bucketpathname) {
        const workEnv = config.workEnvironment;
        if (workEnv === 'state') {
            this.downldSrcURL = this.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            // 4200
            this.downldSrcURL = s3bucketpathname;
        }
        console.log('this.downloadSrc', this.downldSrcURL);
        window.open(this.downldSrcURL, '_blank');
    }


    private loadDropdown() {
        this.attachmentTypeDropdown$ = this._commonService
            .getArrayList(
                {},
                NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentTypeUrl
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.attachmenttypekey
                        })
                );
            });
    }
    private humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        if (!bytes) {
            return '';
         }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }


    loadCWGeneratedDocumentList() {
        this._commonService.getAllPaged(
            new PaginationRequest({
                limit: 20,
                count: 1,
                page: 1,
                method: 'get'
            }),
            NewUrlConfig.EndPoint.Intake.CWGeneratedDocumentListUrl
        ).subscribe((res: any) => {
            if (res.length > 0) {
                res.data.forEach(document => {
                    document.isGenerated = false;
                });
                this.generatedDocListCW = res.data;
            }
        }, (err) => {
            console.log(err);
        });
    }
    generateCWDoc(document) {
        const documentKey = [];
        documentKey.push(document.documenttemplatekey);
        this._commonService.getPagedArrayList(
            new PaginationRequest({
                where: {
                    intakenumber: this.id,
                    documenttemplatekey: documentKey,
                    reportername: '',
                    reporteddate: new Date(),
                    screenername: this.token.user.userprofile.displayname
                },
                method: 'post'
            }), 'evaluationdocument/generateintakedocument')
            .subscribe(res => {
                if (res.data && res.data.length) {
                    this.generatedCWDocUrl = res.data[0].documentpath;
                    this.generatedDocListCW.forEach(doc => {
                        if (doc === document) {
                            doc.generatedBy = this.token.user.userprofile.displayname;
                            doc.generatedDateTime = new Date();
                            doc.isGenerated = true;
                        }
                    });
                }
            }, (err) => {
                // console.log(err);
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            });
    }
    downloadCWDoc(document) {
        (<any>$('#document-view')).modal('show');
    }
    private documentsListFromJSON(user: AppUser) {
        // this.generatedDocuments = [];
        // this.allDocuments.forEach(document => {
        //     if (document.access.indexOf(user.role.name) !== -1) {
        //         // const isExist = this.generatedDocuments.find(gDocument => document.id === gDocument.id);
        //         // if (!isExist) {
        //         this.generatedDocuments.push(document);
        //         // }
        //     }
        // });
    }
    private loadGeneratedDocumentList(user: AppUser, loadAPI: boolean) {
        // this.isCLW = (user.role.name === AppConstants.ROLES.COURT_WORKER);
        // this.isCLW = false; // Since Doc generation is in progress, we are falling back to previous flow
        // if (this.isCLW) {
        const isDJS = this._authService.isDJS();
        this.documentsListFromJSON(user);
        setTimeout(() => {
            if (isDJS && loadAPI) {
                this._commonService.getAllPaged({
                    where: {
                        roletypekey: this._authService.getCurrentUser().user.userprofile.teammemberassignment.teammember.roletypekey,
                        intakenumber: this.intakeNumber
                    },
                    nolimit: true
                },
                    NewUrlConfig.EndPoint.Intake.GenerateDocumentList).subscribe((res: any) => {
                        const generatedDocuments = res;
                        generatedDocuments.forEach((document: any) => {
                            document.isGenerated = document.generatedDateTime ? true : false;
                            document.fromAPI = true;
                        });
                        this.generatedDocuments = [...generatedDocuments, ...this.generatedDocuments];
                    });
            }
        }, 1000);
        // } else {
        if (!this._authService.isDJS()) {
            this.allDocuments = this.allDocuments
                .filter(document => document.team === 'CW')
                .filter(document => this.acknowledgementletterfilter(document));
        }
        this.allDocuments.forEach(document => {
            if (document.access.indexOf(user.role.name) !== -1) {
                const isExist = this.generatedDocuments.find(gDocument => document.id === gDocument.id);
                if (!isExist) {
                    this.generatedDocuments.push(document);
                }
            }

        });
        // }
    }

    acknowledgementletterfilter(document) {
        // if (this.isacknowledgementletter !== true) {
        //     if (document.title === 'Acknowledgement Letter') { return false; }
        // }
        return true;
    }

    downloadDocument(document) {
        // let headers = new HttpHeaders();
        // headers = headers.set('Accept', 'application/pdf');
        // return this.http.get(documenturl, { headers: headers, responseType: 'blob' });
        // this.downloadFile(document.url);
        this._store.setData(IntakeStoreConstants.generatedDocumentDownloadKey, [document.key]);
        this._store.setData('loadhtml', true);
    }

    downloadSelectedDocuments() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            this._store.setData(IntakeStoreConstants.generatedDocumentDownloadKey, selectedDocuments.map(document => document.key));
            this._store.setData('loadhtml', true);
        }
    }

    getSelectedDocuments() {
        return this.generatedDocuments.filter(document => document.isSelected);
    }

    isFileSelected() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            return selectedDocuments.length > 0;
        }

        return false;
    }

    generateNewDocument(document) {
        this.selectedDocument = document;
        document.isInProgress = true;
        if (document.fromAPI) {
            if (document.inputfields === 'intakenumber' || !document.inputfields) {
                this.generateDocumnet(document);
            } else {
                this.initGenerateDocForm();
                this.isGenerateByVictim = false;
                const inputs = document.inputfields.split(',');
                if (inputs.includes('complaintid')) {
                    this.beforeSubmit = document.beforesubmit;
                    this.victims = [];
                    if (this.beforeSubmit) {
                        this.complaints = this.store[IntakeStoreConstants.evalFields];
                        if (this.complaints && this.complaints.length > 0) {
                            this.complaints.forEach(complaint => {
                                complaint.intakeservicerequestevaluationid = complaint.complaintid;
                            });
                        }
                    } else {
                        this.generateDocForm.get('complaint').setValidators(Validators.required);
                        this._commonService.getArrayList({
                            where: { intakenumber: this.intakeNumber },
                            limit: 10,
                            nolimit: true,
                            page: 1,
                            method: 'get'
                        },
                            NewUrlConfig.EndPoint.Intake.Intakeservicerequestevaluations)
                            .subscribe((res: any) => {
                                this.complaints = res;
                            });
                    }
                    this.isGenerateByComplaint = true;
                } else {
                    this.isGenerateByComplaint = false;
                    this.generateDocForm.get('complaint').clearValidators();
                }
                this.generateDocForm.get('complaint').updateValueAndValidity();

                if (inputs.includes('victimid')) {
                    this.isGenerateByVictim = true;
                    this.generateDocForm.get('victim').setValidators(Validators.required);
                } else {
                    this.generateDocForm.get('victim').clearValidators();
                }
                this.generateDocForm.get('victim').updateValueAndValidity();

                if (inputs.includes('petitionid')) {
                    this.isGenerateByPetition = true;
                    this.generateDocForm.get('petition').setValidators(Validators.required);
                    this._commonService.getArrayList({
                        page: 1,
                        limit: 10,
                        nolimit: true,
                        sortcolumn: 'intakenumber',
                        sortorder: 'asc',
                        where: {
                            intakenumber: this.intakeNumber
                        },
                        method: 'get'
                    }, NewUrlConfig.EndPoint.Intake.getAddedPetitions).subscribe(
                        (response) => {
                            this.petitions = response;
                        });
                } else {
                    this.isGenerateByPetition = false;
                    this.generateDocForm.get('petition').clearValidators();
                }
                this.generateDocForm.get('petition').updateValueAndValidity();
                (<any>$('#generate-details')).modal('show');
                $('#generate-details').on('hidden.bs.modal', function () {
                    document.isInProgress = false;
                });
            }
        } else {
            setTimeout(() => {
                document.isGenerated = true;
                document.generatedBy = this.token.user.userprofile.displayname;
                document.generatedDateTime = new Date();
                document.isInProgress = false;
                this._store.setData(IntakeStoreConstants.generatedDocuments, this.generatedDocuments);
            }, 1000);
        }
    }

    // sendMail() {
    //    console.log(this.store.addNarrative);
    //         this._http.post('admin/assessment/sendemailassessment', {
    //             tomail: this.store.addNarrative.email,
    //              sub: 'acknowledgementletter'
    //         }).subscribe((response) => {
    //             this._alertService.success(response);
    //         });

    // }

  // AppConfig.baseUrl + '/' +
    uploadAttachment(filedata) {
        this._http.post( CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=daNumber', {
            headers: new HttpHeaders(

            ).set('access_token', this.token.id).set('ctype', 'file'),
            filesKey: ['file'],
            files: filedata,
            process: true
        }).subscribe((response) => {
            this._alertService.success(response);
           // this.uploadAttachment(content);
        });
       /* this._uploadService
            .upload({
                url: AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: filedata,
                process: true
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        //this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                       // this.attachmentResponse = response.data;
                        this.fileToSave.push(response.data);
                        this.fileToSave[this.fileToSave.length - 1].documentattachment = {
                            attachmenttypekey: '',
                            attachmentclassificationtypekey: '',
                            attachmentdate: new Date(),
                            sourceauthor: '',
                            attachmentsubject: '',
                            sourceposition: '',
                            attachmentpurpose: '',
                            sourcephonenumber: '',
                            acquisitionmethod: '',
                            sourceaddress: '',
                            locationoforiginal: '',
                            insertedby: this.token.user.userprofile.displayname,
                            note: '',
                            updatedby: this.token.user.userprofile.displayname,
                            activeflag: 1
                        };
                        this.fileToSave[this.fileToSave.length - 1].description = '';
                        this.fileToSave[this.fileToSave.length - 1].documentdate = new Date();
                        this.fileToSave[this.fileToSave.length - 1].title = '';
                        this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
                        this.fileToSave[this.fileToSave.length - 1].objecttypekey = 'ServiceRequest';
                        this.fileToSave[this.fileToSave.length - 1].rootobjecttypekey = 'ServiceRequest';
                        this.fileToSave[this.fileToSave.length - 1].activeflag = 1;
                        this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
                        this.fileToSave[this.fileToSave.length - 1].insertedby = this.token.user.userprofile.displayname;
                        this.fileToSave[this.fileToSave.length - 1].updatedby = this.token.user.userprofile.displayname;
                    }
                },
                (err) => {
                    console.log(err);
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    //this.uploadedFile.splice(index, 1);
                }
            );*/
    }
    async downloadCasePdf(element: string) {
        const source = document.getElementById('docu-View');
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            // console.log(pages.item(i).getAttribute('data-page-name'));
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
            });
        }
       return  this.convertImageToPdf();
    }

    convertImageToPdf() {
        let doc = null;

        this.pdfFiles.forEach((pdfFile) => {

          doc = new jsPDF();
            const width = doc.internal['pageSize'].getWidth() - 10;
            const height = doc.internal['pageSize'].getHeight() - 10;
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 3, 5, width, height);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            // this.sendMail(pdfFile.images[0]);
            // doc.save(pdfFile.fileName);
        });
        this.pdfFiles = [];
        return doc;
       // (<any>$('#docu-View')).modal('hide');

    }
    sendMail() {
        const content = this.downloadCasePdf('acknowledgementletter');
        // let content = document.getElementById('acknowledgementletter');
        console.log(content);
        console.log(this.store.addNarrative);
             this._http.post('admin/assessment/sendemailattchment', {
                 tomail:  this.store.addNarrative.email,
                //  this.store.addNarrative.email
                 subject: 'acknowledgementletter',
                 body: 'Dear Reportee',
                 filename: 'acknowledgement.pdf',
                 content: 'acknowledgementletter'
             }).subscribe((response) => {
                 this._alertService.success(response);
                this.uploadAttachment(content);
             });

            }


    loadVictims() {
        const complaintid = this.generateDocForm.get('complaint').value;
        if (this.beforeSubmit) {
            this.selectedComplaint = this.complaints.find(complaint => complaint.complaintid === complaintid);
            this.victims = this.selectedComplaint.victims;
            this.victims.forEach(victim => {
                victim.firstname = victim.firstName;
                victim.lastname = victim.lastName;
            });
        } else {
            this._commonService
                .getArrayList({
                    where: {
                        intakeservicerequestevaluationid: complaintid
                    },
                    method: 'post'
                }, NewUrlConfig.EndPoint.Intake.getComplaintDetails).subscribe((response: any) => {
                    const victims = [];
                    response.data.forEach(complaint => {
                        complaint.victim.forEach(victim => {
                            victims.push(victim);
                        });
                    });
                    this.victims = Array.from(new Set(victims));
                });
        }
    }

    generateJSONForDocumnet(document: any) {
        switch (document.documenttemplatekey) {
            case 'VictimImpact':
                document.json = this.generateVictimImpactJSON(document);
                break;
            case 'AppointLetter':
                document.json = this.generateAppointLetterJSON(document);
                break;
            case 'CompNotif':
                document.json = this.generateCompNotifJSON(document);
                break;
        }
        console.log(document);
        this.generateDocumnet(document);
    }

    generateCompNotifJSON(document: any) {
        let allegationHTML = '';
        for (const offense of this.selectedComplaint.allegedoffense) {
            allegationHTML += '<tr><td>' + this.offenceCategoryList[offense.allegationid ? offense.allegationid : offense] +
                '</td><td>' + moment(this.selectedComplaint.allegedoffensedate).format('MM-DD-YYYY') + '</td></tr>';
        }
        const formattedJSON = {
            complaintid: this.selectedComplaint.complaintid,
            complaintdate: this.selectedComplaint.complaintreceiveddate,
            policename: this.selectedComplaint.sourcefirstname + ' ' + this.selectedComplaint.sourcelastname,
            additionalcomments: '',
            allegedoffense: allegationHTML,
            agency: this.selectedComplaint.evaluationsourceagencyname,
            policenumber: '(410) 230-3333',
            policeaddress: this.selectedComplaint.sourceStreetno + ' ' + this.selectedComplaint.sourceStreet1 + ', ' + this.selectedComplaint.sourceStreet2,
            datewithaddition: moment().add(10, 'd').format('MM-DD-YYYY'),
            officername: this._authService.getCurrentUser().user.userprofile.displayname,
            role: this._authService.getCurrentUser().role.name
        };
        console.log(formattedJSON);
        return formattedJSON;
    }

    generateAppointLetterJSON(document: any) {
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        const youth = addedPersons.find((person) => person.Role === 'Youth');
        const address = this._authService.getCurrentUser().user.userprofile.userprofileaddress[0];
        const appointment = this.store[IntakeStoreConstants.intakeappointment];
        let allegationHTML = '';
        for (const offense of this.selectedComplaint.allegedoffense) {
            allegationHTML += '<tr><td>' + this.offenceCategoryList[offense.allegationid] + '</td><td>' + moment(this.selectedComplaint.allegedoffensedate).format('MM-DD-YYYY') + '</td></tr>';
        }
        const formattedJSON = {
            complaintid: this.selectedComplaint.complaintid,
            complaintdate: this.selectedComplaint.complaintreceiveddate,
            youthname: youth.fullName,
            youthid: youth.cjamspid,
            policename: this.selectedComplaint.sourcefirstname + ' ' + this.selectedComplaint.sourcelastname,
            appointmentdate: appointment.length ? appointment.appointmentDate : new Date(),
            appointmentaddress: address.address + ', ' + address.city + ', ' + address.state + ', ' + address.county + ' ' + address.zipcode,
            additionalcomments: '',
            allegedoffense: allegationHTML,
            officername: this._authService.getCurrentUser().user.userprofile.displayname,
            role: this._authService.getCurrentUser().role.name
        };
        console.log(formattedJSON);
        return formattedJSON;
    }

    generateVictimImpactJSON(document: any) {
        const addedPersons = this.store[IntakeStoreConstants.addedPersons];
        const youth = addedPersons.find((person) => person.Role === 'Youth');
        const victimid = this.generateDocForm.value.victim;
        const victim = addedPersons.find((person) => person.Pid === victimid);
        const vAddress = victim.personAddressInput;
        for (const address of vAddress) {
            if (address.addresstype === '31') {
                victim.address = address.address1 + '<br>';
                if (address.Address2) {
                    victim.address += address.Address2 + '<br>';
                }
                victim.address += address.city + ' ' + address.state
                    + '<br>' + address.county + ' ' + address.zipcode;
                break;
            } else {
                victim.address = address.address1 + '<br>';
                if (address.Address2) {
                    victim.address += address.Address2 + '<br>';
                }
                victim.address += address.city + ' ' + address.state
                    + '<br>' + address.county + ' ' + address.zipcode;
                break;
            }
        }
        for (const phone of victim.phoneNumber) {
            victim.phonenumber = phone.contactnumber;
        }
        const formattedJSON = {
            complaintid: this.selectedComplaint.complaintid,
            complaintdate: this.selectedComplaint.complaintreceiveddate,
            youthname: youth.fullName,
            youthid: youth.cjamspid,
            victimname: victim.fullName,
            victimaddress: victim.address,
            victimphno: victim.phonenumber,
            officername: this._authService.getCurrentUser().user.userprofile.displayname,
            role: this._authService.getCurrentUser().role.name
        };
        console.log(formattedJSON);
        return formattedJSON;
    }

    private generateDocumnet(document: any) {
        const documentKey = [];
        documentKey.push(document.documenttemplatekey);
        const generateDocDetails = this.generateDocForm.getRawValue();
        const complaintid = generateDocDetails.complaint ? [].concat(generateDocDetails.complaint) : [];
        const victimid = generateDocDetails.victim ? [].concat(generateDocDetails.victim) : [];
        const petitionid = generateDocDetails.petition ? [].concat(generateDocDetails.petition) : [];
        if (document.title === 'Detention Shelter Authorization' &&
        (this.token.role.name === AppConstants.ROLES.DJS_PLACEMENT_WORKER || this.token.role.name === AppConstants.ROLES.DJS_ATD_PLACEMENT_WORKER)) {
            document.json = this.complaints;
        }
        let restitutionno = null;
        const addedPaymentSchedules = this._store.getData(IntakeStoreConstants.restitution);
        if (addedPaymentSchedules) {
            restitutionno = addedPaymentSchedules.restitutionnumber;
        }
        this._commonService.getPagedArrayList(new PaginationRequest({
            where: {
                intakenumber: this.intakeNumber,
                restitutionno: restitutionno,
                documenttemplatekey: documentKey,
                reportername: this.token.user.userprofile.displayname,
                reporteddate: new Date(),
                screenername: this.token.user.userprofile.displayname,
                isdraft: false,
                downloadtype: document.downloadtype,
                intakeservicerequestevaluationid: complaintid,
                victim: victimid,
                petition: petitionid,
                beforesubmit: document.beforesubmit,
                json: document.json,
                isheaderrequired: document.isheaderrequired
            },
            method: 'post'
        }), 'evaluationdocument/generateintakedocument')
            .subscribe(res => {
                if (res.data && res.data.length) {
                    this._store.setData(IntakeStoreConstants.generatedDocuments, this.generatedDocuments);
                    (<any>$('#generate-details')).modal('hide');
                    this.loadGeneratedDocumentList(this.token, true);
                }
            }, (err) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                document.isInProgress = false;
            });
    }

    generateSelectedDocuments() {
        this.generatedDocuments.forEach(document => {
            if (document.isSelected) {
                this.generateNewDocument(document);
            }

        });
    }





    isCourtLiaisonWorker() {

        if (this.token) {
            return this.token.role.name === CLW;
        }
        return false;

    }

    isCompleteVisible() {
        return this.isCourtLiaisonWorker();
    }

    prepareConfig() {
        const isDjs = this._authService.isDJS();
        if (isDjs) {
            this.config.isGenerateUploadTabNeeded = true;
        } else {
            this.config.isGenerateUploadTabNeeded = false;
        }
    }

    checkScan() {
        const _self = this;
        const scanDocs = window.setInterval(function () {
            const isScanDoc = window.localStorage.getItem('scanDoc');
            if (isScanDoc === 'true') {
                _self.attachment('All');
                window.localStorage.removeItem('scanDoc');
                window.clearInterval(scanDocs);
                console.log('successful scan');
            } else {
                console.log('wait scan in progress');
            }
        }, 1000);
    }
}
