import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { AlertService, GenericService, DataStoreService } from '../../../../@core/services';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { NewUrlConfig } from '../../newintake-url.config';
import { CrossReferenceSearch, CrossReferenceSearchResponse, IntakeDATypeDetail, InvolvedPerson, CrossReferenceResponse, IntakeCrossReferenceSearchResponse } from '../_entities/newintakeModel';
import { ComplaintTypeCase } from '../_entities/newintakeSaveModel';
import { IntakeStoreConstants } from '../my-newintake.constants';
declare var $: any;
const CR_SUCESS_MESSAGE = 'Cross reference added successfully';
const CR_COMPLAINT_EXIST_SUCESS_MESSAGE = 'Initial disapproved Intake for the given complaint id is added in Cross Reference.';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-cross-refference',
    templateUrl: './intake-cross-refference.component.html',
    styleUrls: ['./intake-cross-refference.component.scss']
})
export class IntakeCrossRefferenceComponent implements OnInit {
    @Input() addedIntakeDATypeDetails: IntakeDATypeDetail[] = [];
    @Input() addedCrossReference: CrossReferenceSearchResponse[] = [];
    @Input() addedIntakeCrossReference: IntakeCrossReferenceSearchResponse[] = [];
    @Input() addedPersons: InvolvedPerson[] = [];
    @Input() createdCaseInputSubject$ = new Subject<ComplaintTypeCase[]>();
    @Input() complaintCrossReferenceSubject$ = new Subject<CrossReferenceResponse>();
    createdCaseInput: ComplaintTypeCase[];
    crossReferenceIndex: number;
    crossReferenceType: string;
    paginationInfo: PaginationInfo = new PaginationInfo();
    CrossReferncedata: Boolean = false;
    crossRefernceSearchForm: FormGroup;
    crossRefernceQuickSearchForm: FormGroup;
    reasonToCrossRefernceForm: FormGroup;
    reasonCrossReferenceDropDownItems$: Observable<DropdownModel[]>;
    crossReferenceSearchResponse$: Observable<CrossReferenceSearchResponse[]>;
    totalRecords$: Observable<number>;
    sucessMessage: string = CR_SUCESS_MESSAGE;
    private pageSubject$ = new Subject<number>();
    private crossReferenceSearch: CrossReferenceSearch;
    private searchUrl: string;
    private DaStartDate: string;
    private DaEndDate: string;
    private store: any;
    constructor(
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _crossReferenceSearch: GenericService<CrossReferenceSearchResponse>,
        private formBuilder: FormBuilder,
        private _datastoreService: DataStoreService
    ) {
        this._crossReferenceSearch.endpointUrl =
            NewUrlConfig.EndPoint.Intake.ServiceRequestSearchesUrl;
    }

    ngOnInit() {
        this.DaStartDate = moment()
            .subtract(1, 'd')
            .format();
        this.DaEndDate = moment()
            .subtract(1, 'd')
            .format();
        this.crossRefernceQuickSearchForm = this.formBuilder.group({
            servicerequestnumber: ['']
        });

        this.crossRefernceSearchForm = this.formBuilder.group(
            {
                servicerequestnumber: [''],
                reporteddate: [''],
                reportedenddate: [''],
                status: ['']
            },
            { validator: this.checkDateRange }
        );

        this.reasonToCrossRefernceForm = this.formBuilder.group({
            ReasonsofCrossref: ['']
        });
        this.reasonCrossReferenceDropDownItems$ = this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake
                    .IntakeserviceRequestCrossReferenceReasonTypesUrl +
                '?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.typedescription,
                            value:
                                res.intakeservicerequestcrossreferencereasontypekey
                        })
                );
            });

        this.pageSubject$.subscribe(pageNo => {
            this.getPage(pageNo);
        });
        this.createdCaseInputSubject$.subscribe(data => {
            this.createdCaseInput = data;
        });

        // this.complaintCrossReferenceSubject$.subscribe(data => {
        //     const crossReference = data.crossrefs;
        //     const intakeCrossReference = data.intakecrossrefs;
        //     if (crossReference && crossReference.length > 0 ) {
        //         const reasonForCrossReff = 'Complaint ID Exists';
        //         this.sucessMessage = CR_COMPLAINT_EXIST_SUCESS_MESSAGE;
        //         this.addCrossReference(reasonForCrossReff, crossReference[0], crossReference[0].servicerequestnumber);
        //         this.addedIntakeCrossReference = [];
        //     } else if (intakeCrossReference && intakeCrossReference.length > 0 ) {
        //         this.sucessMessage = CR_COMPLAINT_EXIST_SUCESS_MESSAGE;
        //         this.addIntakeCrossReference(intakeCrossReference);
        //         this.addedCrossReference = [];
        //     } else {
        //         this.addedIntakeCrossReference = [];
        //         this.addedCrossReference = [];
        //     }
        // });
        // this.store = this._datastoreService.getCurrentStore();
        // const evalCrossReferece = this.store[IntakeStoreConstants.crossReferenceFromEvals];

        // if (evalCrossReferece.intakeCrossReference) {
        //     this.addedIntakeCrossReference = this.addedIntakeCrossReference.concat(evalCrossReferece.intakeCrossReference);
        // }

        // if (evalCrossReferece.crossReference) {
        //     this.addedCrossReference = this.addedCrossReference.concat(evalCrossReferece.crossReference);
        // }
    }
    checkDateRange(crossRefernceSearchForm) {
        if (crossRefernceSearchForm.controls.reportedenddate.value) {
            if (
                crossRefernceSearchForm.controls.reportedenddate.value <
                crossRefernceSearchForm.controls.reporteddate.value
            ) {
                return { notValid: true };
            }
            return null;
        }
    }

    samePersonSearch(persontype: string) {
        this.CrossReferncedata = true;
        const getPerson = this.addedPersons.filter(
            item => item.Role === persontype
        );
        this.crossReferenceSearch = Object.assign(new CrossReferenceSearch());
        this.crossReferenceSearch.firstname = '';
        this.crossReferenceSearch.lastname = '';
        if (getPerson[0]) {
            this.crossReferenceSearch.firstname = getPerson[0].Firstname;
            this.crossReferenceSearch.lastname = getPerson[0].Lastname;
        }
        this.crossReferenceSearch.role = persontype;
        this.crossReferenceSearch.status = null;
        this.crossReferenceSearch.activeflag = null;
        this.crossReferenceSearch.activeflag1 = null;
        this.search(
            1,
            this.crossReferenceSearch,
            NewUrlConfig.EndPoint.Intake.SameRACrossRefDAsUrl
        );
    }

    sameDepartmentProviderSearch(
        pageNumber: number,
        model: CrossReferenceSearch
    ) {
        this.search(
            pageNumber,
            model,
            NewUrlConfig.EndPoint.Intake.SameRACrossRefDAsUrl
        );
    }

    searchCrossReference(pageNumber: number, model: CrossReferenceSearch) {
        this.CrossReferncedata = true;
        if (model.reporteddate) {
            model.reporteddate = new Date(
                model.reporteddate
            ).toLocaleDateString();
        }
        if (model.reportedenddate) {
            model.reportedenddate = new Date(
                model.reportedenddate
            ).toLocaleDateString();
        }
        this.search(
            pageNumber,
            model,
            NewUrlConfig.EndPoint.Intake.ServiceRequestSearchesUrl
        );
    }

    addCrossReference(ReasonsofCrossref: string,
        responseModel: CrossReferenceSearchResponse, servicerequestnumber: string) {
        const isAdded = this.addedCrossReference.filter(
            item =>
                item.servicerequestnumber ===
                responseModel.servicerequestnumber &&
                item.ReasonsofCrossref === ReasonsofCrossref
        );
        if (!isAdded.length) {
            // responseModel.crossRefwith = this.crossRefernceQuickSearchForm.get(
            //     'servicerequestnumber'
            // ).value;
            responseModel.crossRefwith = servicerequestnumber;
            responseModel.ReasonsofCrossref = ReasonsofCrossref;
            responseModel = Object.assign({}, responseModel);
            this.addedCrossReference.push(responseModel);
            this._alertService.success(this.sucessMessage);
            this.sucessMessage = CR_SUCESS_MESSAGE;
        } else {
            this._alertService.error('Cross reference already exists');
        }
    }

    addIntakeCrossReference(responseModel: IntakeCrossReferenceSearchResponse[]) {
        const isAdded = this.addedIntakeCrossReference.filter(
            item =>
                responseModel.filter(model => model.intakenumber === item.intakenumber)
        );
        if (!isAdded.length) {
            // responseModel.crossRefwith = this.crossRefernceQuickSearchForm.get(
            //     'servicerequestnumber'
            // ).value;
            this.addedIntakeCrossReference = responseModel;
            this._alertService.success(this.sucessMessage);
            this.sucessMessage = CR_SUCESS_MESSAGE;
        } else {
            this._alertService.error('Intake Cross reference already exists');
        }
    }

    selectCrossReference(
        searchModel: CrossReferenceSearch,
        responseModel: CrossReferenceSearchResponse,
        control: any
    ) {
        if (
            this.crossRefernceQuickSearchForm.get('servicerequestnumber')
                .value &&
            searchModel.ReasonsofCrossref
        ) {
            // tslint:disable-next-line:max-line-length
            this.addCrossReference(searchModel.ReasonsofCrossref, responseModel, this.crossRefernceQuickSearchForm.get('servicerequestnumber').value);

            this.reasonToCrossRefernceForm.reset();
            this.reasonToCrossRefernceForm.patchValue({
                ReasonsofCrossref: ''
            });
        } else {
            if (
                !this.crossRefernceQuickSearchForm.get('servicerequestnumber')
                    .value
            ) {
                this._alertService.warn('Please select agency action number');
                return false;
            }
            if (!searchModel.ReasonsofCrossref) {
                this._alertService.warn(
                    'Please select reason to crossreference'
                );
                return false;
            }
        }
    }
    confirmDelete(model: CrossReferenceSearchResponse) {
        this.crossReferenceIndex = this.addedCrossReference.indexOf(model);
        this.crossReferenceType = 'complaint';
        (<any>$('#delete-cross-reference-popup')).modal('show');
    }

    confirmIntakeDelete(model: IntakeCrossReferenceSearchResponse) {
        this.crossReferenceIndex = this.addedIntakeCrossReference.indexOf(model);
        this.crossReferenceType = 'intake';
        (<any>$('#delete-cross-reference-popup')).modal('show');
    }

    deleteCrossReference() {
        this.addedCrossReference.splice(this.crossReferenceIndex, 1);
        (<any>$('#delete-cross-reference-popup')).modal('hide');
        this.crossReferenceIndex = null;
    }

    deleteIntakeCrossReference() {
        this.addedIntakeCrossReference.splice(this.crossReferenceIndex, 1);
        (<any>$('#delete-cross-reference-popup')).modal('hide');
        this.crossReferenceIndex = null;
    }

    changePage(event: any): void {
        this.pageSubject$.next(event.page);
    }
    clearCrossReference() {
        this.crossRefernceSearchForm.reset();
    }
    private search(
        pageNumber: number,
        model: CrossReferenceSearch,
        url: string
    ) {
        this.searchUrl = url;
        this.crossReferenceSearch = Object.assign(
            new CrossReferenceSearch(),
            model
        );
        this.getPage(pageNumber);
    }

    private getPage(pageNumber: number) {
        ObjectUtils.removeEmptyProperties(this.crossReferenceSearch);
        const source = this._crossReferenceSearch
            .getPagedArrayList(
                {
                    limit: this.paginationInfo.pageSize,
                    order: '',
                    page: this.paginationInfo.pageNumber,
                    count: this.paginationInfo.total,
                    where: this.crossReferenceSearch,
                    method: 'post'
                },
                this.searchUrl
            )
            .map(item => {
                item.data.map(res => {
                    res.servicerequestnumber = res.servicerequestnumber.replace(
                        '(New)',
                        ''
                    );
                    return res;
                });
                return item;
            })
            .share();
        this.crossReferenceSearchResponse$ = source.pluck('data');
        if (pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
        }
    }
}
