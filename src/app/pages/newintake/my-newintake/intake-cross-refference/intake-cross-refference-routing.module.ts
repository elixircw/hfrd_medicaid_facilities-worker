import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeCrossRefferenceComponent } from './intake-cross-refference.component';

const routes: Routes = [{
  path: '',
  component: IntakeCrossRefferenceComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeCrossRefferenceRoutingModule { }
