import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeCrossRefferenceRoutingModule } from './intake-cross-refference-routing.module';
import { IntakeCrossRefferenceComponent } from './intake-cross-refference.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    IntakeCrossRefferenceRoutingModule
  ],
  declarations: [IntakeCrossRefferenceComponent]
})
export class IntakeCrossRefferenceModule { }
