import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService, AlertService, AuthService, CommonDropdownsService } from '../../../../@core/services';
import { NewUrlConfig } from '../../newintake-url.config';
import { MyNewintakeConstants, IntakeStoreConstants } from '../my-newintake.constants';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { IntakeConfigService } from '../intake-config.service';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { AppConfig } from '../../../../app.config';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { HttpHeaders } from '@angular/common/http';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'intake-sao-hearing-details',
  templateUrl: './intake-sao-hearing-details.component.html',
  styleUrls: ['./intake-sao-hearing-details.component.scss']
})
export class IntakeSaoHearingDetailsComponent implements OnInit {
  hearingForm: FormGroup;
  editOffenseForm: FormGroup;
  purposeID: any;
  hearingTypes$: Observable<DropdownModel[]>;
  courtActions$: Observable<any[]>;
  courtActionsFiltered$: Observable<any[]>;
  courtActionsFiltered: Array<any> = [];
  allegations: any[] = [];
  decisions$: Observable<DropdownModel[]>;
  petitionList = [];
  selectedPetitionToEdit: any;
  selectedAction: any;
  isAdjudi: boolean;
  isDisposition: boolean;
  loggedInUser: string;
  conditionTypes: DropdownModel[] = [];
  courtOrder: DropdownModel[] = [];
  hearingList = [];
  intakeNumber: string;
  complaintIDs = [];
  evaluationInfo: any = null;
  selectedHearing: any = {};
  isSustained: boolean;
  allNotSustained = false;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalRecords: number;
  maxSize = 10;
  isOther: boolean;
  mdcounty = [];
  isTransferOfJuridication = false;
  maxDate: Date = new Date();
  expirationMinDate: Date = new Date();
  createDisposition: boolean;
  isPeaceOrder = false;
  isRestitution = false;
  expirationMonths = [];
  showExpiration = true;
  adultPettitionId = '';
  restitutionupload: File;
  restitutionuploadedFile: any;
  private token: AppUser;
  intakeModel: any;
  intakeDate: Date;
  maxAdjudicatedDate: Date = new Date();
  constructor(private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _alertService: AlertService,
    private route: ActivatedRoute,
    private commonDropdownService: CommonDropdownsService,
    private _intakeConfig: IntakeConfigService,
    private _uploadService: NgxfUploaderService) {
    this.token = this._authService.getCurrentUser();
    this.expirationMonths = Array(12).fill(0).map((x, i) => i);
  }

  ngOnInit() {
    this.isOther = false;
    this.isSustained = false;
    this.initFormGroup();
    // this.isPeaceOrder = this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.PEACE_ORDER);
    this._dataStoreService.currentStore.subscribe(data => {
      this.loadAllegations();
      // this.petitionList = this._dataStoreService.getData(MyNewintakeConstants.Intake.scheduledHearings);
    });
    // this.loadAllegations();
    this.loadDropDowns();
    // this.hearingForm.valueChanges.subscribe(res => console.log(res));
    /*  this.route.params.subscribe((item) => {
       this.intakeNumber = item['id'];
       this.loadHearings();
     }); */
    this.intakeNumber = this._dataStoreService.getData(IntakeStoreConstants.intakenumber);
    this.intakeModel = this._dataStoreService.getData(IntakeStoreConstants.general);
    if (this.intakeModel && this.intakeModel.RecivedDate) {
      this.intakeDate = new Date(this.intakeModel.RecivedDate);
    }
    this.loadHearings();
    this.hearingForm.valueChanges.subscribe(hearing => {
      this.isSustained = false;
      this.allNotSustained = false;
      this.createDisposition = false;
      if (hearing.petitions.length > 0) {
        let count = 0;
        let offenseCount = 0;
        loop:
        for (const petition of hearing.petitions) {
          for (const complaint of petition.complaints) {
            for (const offense of complaint.offenses) {
              offenseCount++;
              if (offense.decision === 'S') {
                this.isSustained = true;
                if (offense.courtActions) {
                  for (const courtAction of offense.courtActions) {
                    if (courtAction === 'AdjudiInvolved') {
                      this.createDisposition = true;
                      break loop;
                    }
                  }
                }
              } else if (offense.decision === 'NS') {
                count++;
              }
            }
          }
        }

        if (count === offenseCount) {
          this.allNotSustained = true;
          if (this.hearingForm.value.createDisposition) {
            this.hearingForm.patchValue({
              createDisposition: false
            });
          }
        }
      }

      if (this.isPeaceOrder) {
        this.showExpiration = true;
        if (hearing.courtActions && hearing.courtActions.length > 0) {
          this.showExpiration = !hearing.courtActions.includes('DBC'); // Debied By Court
          if (!this.showExpiration) {
            this.hearingForm.get('expirationdate').clearValidators();
          } else {
            this.hearingForm.get('expirationdate').setValidators(Validators.required);
          }
          this.hearingForm.get('expirationdate').updateValueAndValidity({ emitEvent: false });
        }
      }
    });
    this.hearingForm.get('terminationDate').valueChanges.subscribe(value => {
      if (value instanceof Date) {
        this.maxAdjudicatedDate = value;
      } else {
        this.maxAdjudicatedDate = new Date();
      }
    });
  }

  loadHearings() {
    this._commonHttpService.getArrayList({
      page: this.paginationInfo.pageNumber,
      limit: this.paginationInfo.pageSize,
      where: {
        intakenumber: this.intakeNumber
      },
      method: 'get'
    }, NewUrlConfig.EndPoint.Intake.getScheduledHearingsForCourt).subscribe(
      (response) => {
        console.log('response', response);
        this.petitionList = response;
        if (this.petitionList.length > 0) {
          this.totalRecords = this.petitionList[0].totalcount;
        }
      });
  }

  pageChanged(pageInfo) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadHearings();
  }


  loadAllegations() {
    this.purposeID = this._dataStoreService.getData(IntakeStoreConstants.purposeSelected);
    if (this.purposeID) {
      const purposeID = this.purposeID.value;
      const checkInput = {
        where: { intakeservreqtypeid: purposeID },
        method: 'get',
        nolimit: true,
        order: 'name'
      };
      if (this.allegations && this.allegations.length === 0) {
        const allegations = this._commonHttpService
          .getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter')
          .subscribe(data => {
            this.allegations = data;
          });
      }

    }
    if (this.courtOrder && this.courtOrder.length === 0) {
      const courtOrder = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.courtOrderType)
        .map(order => {
          return {
            order: order.map(res =>
              new DropdownModel({
                text: res.description,
                value: res.courtordertypekey
              }))
          };
        }).subscribe(data => {
          this.courtOrder = data.order;
        });
    }
    if (this.conditionTypes && this.conditionTypes.length === 0) {
      const conditionTypes = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.conditionType)
        .map(condition => {
          return {
            condition: condition.map(res =>
              new DropdownModel({
                text: res.description,
                value: res.conditiontypekey
              }))
          };
        }).subscribe(data => {
          this.conditionTypes = data.condition;
        });
    }
  }

  initFormGroup(): void {
    this.hearingForm = this._formBuilder.group({
      petitionID: [],
      hearingDate: '',
      hearingTime: '08:00',
      hearingType: '',
      associatedAttorneys: '',
      transferNotes: null,
      saocountyid: null,
      saotransfernotes: null,
      legalCounselName: '',
      judgeName: '',
      worker: '',
      courtActions: [],
      intakeAllegation: '',
      adjudicatedOffense: [],
      adjudicatedDecision: '',
      adjudicatedDate: '',
      adjuctedTime: '08:00',
      offenses: [],
      courtordertypekey: [],
      orderDate: '',
      orderTime: '08:00',
      conditiontypekey: [],
      conditiontypedescription: '',
      completionDate: '',
      completionTime: '08:00',
      terminationDate: '',
      expirationdate: '',
      expiryMonth: '',
      terminationTime: '08:00',
      otherhearingtypenotes: '',
      othercourtactiontypenotes: '',
      createDisposition: false,
      issendtoccu: false
    });

    this.hearingForm.addControl('petitions', this._formBuilder.array([]));

    this.editOffenseForm = this._formBuilder.group({
      complaintID: '',
      offenses: [''],
      offensesName: ['']
    });
    // this.hearingForm.patchValue({ worker: this._authService.getCurrentUser().user.userprofile.displayname});
  }

  loadDropDowns(): void {
    const hearing = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.hearingType)
      .map(hearingType => {
        return {
          hearingTypes: hearingType.map(res =>
            new DropdownModel({
              text: res.description,
              value: res.hearingtypekey
            }))
        };
      }).share();
    const courtActionsType = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.courtActionType)
      .map(courtAction => {
        return {
          courtActions: courtAction.map(res => res)
        };
      }).share();

    this.commonDropdownService.getCountyList('MD').subscribe(response => {
      this.mdcounty = response;
    });



    const decisionsType = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.adjuctedDecisions)
      .map(decision => {
        return {
          decisions: decision.map(res =>
            new DropdownModel({
              text: res.description,
              value: res.adjudicateddecisiontypekey
            }))
        };
      }).share();

    this.hearingTypes$ = hearing.pluck('hearingTypes');
    this.courtActions$ = courtActionsType.pluck('courtActions');
    this.decisions$ = decisionsType.pluck('decisions');

    this.hearingTypes$.subscribe(hearingTypes => {
      for (const hearingType of hearingTypes) {
        this.hearingList[hearingType.value] = hearingType.text;
      }
    });
  }

  // viewHearingDetails() {
  //   this.hearingForm.reset();
  //   this.hearingForm.patchValue({
  //     petitionID: '1',
  //     hearingDate: '2018-09-27T00:00:00.000Z',
  //     hearingTime: '13:00',
  //     hearingType: 'Arraign',
  //     associatedAttorneys: 'John',
  //     transferPetitionID: 'P2-AQPLED',
  //     transferNotes: 'Transfered',
  //     offenses: 'aeb5bf68-3e3f-4015-a1f4-13c7bcd96e9a',
  //     courtordertypekey: [],
  //     orderDate: '',
  //     orderTime: '08:00',
  //     conditiontypekey: '',
  //     conditiontypedescription: '',
  //     completionDate: '',
  //     completionTime: '08:00',
  //     terminationDate: '',
  //     terminationTime: '08:00'
  //   });
  //   this.hearingForm.disable();
  //   (<any>$('#view-hearing')).modal('show');
  // }

  checkHearingType(hearingType) {
    this.isOther = (hearingType === 'Other');
    if (this.isOther) {
      this.hearingForm.controls['othercourtactiontypenotes'].setValidators(Validators.required);
      this.hearingForm.controls['othercourtactiontypenotes'].updateValueAndValidity();
    } else {
      this.hearingForm.controls['othercourtactiontypenotes'].clearValidators();
      this.hearingForm.controls['othercourtactiontypenotes'].updateValueAndValidity();
    }
  }

  viewEditHearing(item, action) {
    this.selectedAction = action;
    this.hearingForm.reset();
    this.selectedPetitionToEdit = item;
    // this.selectedPetitionToEdit.petitionids.offenseList = this.selectedPetitionToEdit.petitionids.map(petition => petition.offenses.map(offense => offense.name));

    // this.selectedPetitionToEdit.petitions.forEach(petition => {
    //   const offenseList = [];
    //   petition.offenses.forEach(offense => {
    //     offenseList.push(offense.name);
    //   });
    //   petition.offenseList = offenseList;
    // });

    const petitionIds = item.petitions.map(ele => ele.petitionid);
    const adultPettitionId = item.petitions.map(ele => ele.adultpetitionid);
    if (adultPettitionId && adultPettitionId.length > 0) {
      this.adultPettitionId = adultPettitionId[0];
    }
    let offences = [];
    item.petitions.forEach(petition => {
      if (Array.isArray(petition.complaints)) {
        const offenceOfPid = petition.complaints.map(complaint => complaint.offences.map(offence => offence.allegationid));
        // offences = Array.from(new Set([...offences, ...offenceOfPid]));
        offenceOfPid.forEach(offence => {
          offence.forEach(o => {
            offences.push(o);
          });
        });
      }
    });
    offences = Array.from(new Set(offences));
    if (item.hearingtypekey === 'Adjudi') {
      this.isAdjudi = true;
    } else {
      this.isAdjudi = false;
    }

    if (item.hearingtypekey === 'Disp') {
      this.isDisposition = true;
    } else {
      this.isDisposition = false;
    }

    if (item.hearingtypekey === 'PO') {
      this.isPeaceOrder = true;
    } else {
      this.isPeaceOrder = false;
    }

    if (item.hearingtypekey === 'Resti') {
      this.isRestitution = true;
      if (item.restitutiondocpath) {
        this.restitutionuploadedFile = { s3bucketpathname: item.restitutiondocpath, originalfilename: 'Court Document' };
        // this.restitutionuploadedFile.s3bucketpathname = item.restitutiondocpath;
        // this.restitutionuploadedFile.originalfilename = 'Court Document';
      }
    } else {
      this.isRestitution = false;
    }

    this.showExpiration = true;

    this.courtActionsFiltered = [];
    this.courtActionsFiltered$ = this.courtActions$.map(courtActions => {
      return courtActions.filter(courtAction => {
        if (item.hearingtypekey === courtAction.hearingtypekey) {
          this.courtActionsFiltered.push({
            activeflag: courtAction.activeflag,
            courtactiontypeid: courtAction.courtactiontypeid,
            Intakeservicerequestcourtactiontypeid: courtAction.Intakeservicerequestcourtactiontypeid ? courtAction.Intakeservicerequestcourtactiontypeid : null,
            courtactiontypekey: courtAction.courtactiontypekey,
            description: courtAction.description,
            effectivedate: courtAction.effectivedate,
            hearingtypekey: courtAction.hearingtypekey
          });
        }
        return item.hearingtypekey === courtAction.hearingtypekey;
      });
    });

    item.hearingtime = moment(item.hearingdatetime).format('HH:mm');
    item.orderTime = moment(item.courtorderdatetime).format('HH:mm');

    this.checkHearingType(item.hearingtypekey);

    setTimeout(() =>
      this.hearingForm.patchValue({
        petitionID: petitionIds,
        hearingDate: item.hearingdatetime,
        hearingTime: item.hearingtime,
        hearingType: item.hearingtypekey,
        associatedAttorneys: item.associatedattorneys,
        transferNotes: item.transfernotes,
        offenses: [offences],

        legalCounselName: item.legalcounselname,
        judgeName: item.magistratename,
        worker: item.workername ? item.workername : this._authService.getCurrentUser().user.userprofile.displayname,
        courtActions: item.courtactiontype ? item.courtactiontype.map(courtaction => courtaction.courtactiontypekey) : [],
        adjudicatedOffense: [''],
        adjudicatedDecision: [item.adjudicationdecision],
        adjudicatedDate: item.adjudicationdatetime,
        adjuctedTime: '',
        courtordertypekey: item.courtordertype ? item.courtordertype.map(courtorder => courtorder.courtordertypekey) : [],
        orderDate: item.courtorderdatetime,
        orderTime: (item.orderTime && item.orderTime !== '') ? item.orderTime : '08:00',
        conditiontypekey: item.conditiontype ? item.conditiontype.map(condition => condition.conditiontypekey) : [],
        conditiontypedescription: item.conditiontypedescription,
        completionDate: item.conditiontypecompletiondatetime,
        completionTime: (item.completionTime && item.completionTime !== '') ? item.completionTime : '08:00',
        terminationDate: item.terminationdatetime,
        expirationdate: item.expirationdate,
        expiryMonth: item.expirytmonth,
        terminationTime: (item.terminationTime && item.terminationTime !== '') ? item.terminationTime : '08:00',
        otherhearingtypenotes: item.otherhearingtypenotes,
        othercourtactiontypenotes: item.othercourtactiontypenotes,
        saotransfernotes: item.saotransfernotes,
        saocountyid: item.saocountyid,
        createDisposition: item.createdisposition,
        issendtoccu: item.issendtoccu
      }), 100);
    this.onCourtOrderChange();
    this.hearingForm.setControl('petitions', this._formBuilder.array([]));
    this.setPetitions(this.selectedPetitionToEdit.petitions);
    setTimeout(() => {
      this.hearingForm.patchValue({
        courtActions: item.courtactiontype ? item.courtactiontype.map(courtaction => courtaction.courtactiontypekey) : [],
        courtordertypekey: item.courtordertype ? item.courtordertype.map(courtorder => courtorder.courtordertypekey) : [],
        conditiontypekey: item.conditiontype ? item.conditiontype.map(condition => condition.conditiontypekey) : []
      });
      console.log(this.hearingForm.getRawValue());
      if (action === 1) {
        this.hearingForm.enable();
      } else if (action === 0) {
        this.hearingForm.disable();
      }
      this.hearingForm.get('petitionID').disable();
      this.hearingForm.get('hearingDate').disable();
      this.hearingForm.get('hearingTime').disable();
      this.hearingForm.get('hearingType').disable();
      this.hearingForm.get('associatedAttorneys').disable();
      this.hearingForm.get('transferNotes').disable();
      this.hearingForm.get('offenses').disable();
      this.hearingForm.get('otherhearingtypenotes').disable();
      this.hearingForm.get('offenses').patchValue(offences);
      (<any>$('#view-hearing')).modal('show');
    }, 1000);
  }

  setPetitions(petitions) {
    const control = <FormArray>this.hearingForm.controls.petitions;
    petitions.forEach(petition => {
      control.push(this._formBuilder.group({
        petitionid: petition.petitionid,
        complaints: this.setComplaintDetais(petition.complaints),
        createddt: petition.insertedon
      }));
    });

    // return control;
  }

  setComplaintDetais(complaints) {
    const arr = new FormArray([]);
    complaints.forEach(complaint => {
      arr.push(this._formBuilder.group({
        complaintid: complaint.complaintid,
        complainttype: complaint.complainttype,
        offenses: this.setOffenses(complaint.offences, complaint.intakeservicerequestevaluationid)
      }));
    });
    return arr;
  }

  addConditomDescription(conditionType, i, j, k) {
    const arr = new FormArray([]);
    this.conditionTypes.forEach(condition => {
      if (conditionType.includes(condition.value)) {
        arr.push(this._formBuilder.group({
          conditiontypekey: condition.value,
          conditiontype: condition.text,
          conditiontypedescription: '',
          completiondate: null,
          Intakeservicerequestcourtconditiontypeconfig: ''
        }));
      }
    });
    // console.log(this.hearingForm.get(['petitions', i, 'complaints', j, 'offenses', k, 'conditions']));
    const control = <FormGroup>this.hearingForm.get(['petitions', i, 'complaints', j, 'offenses', k]);
    control.removeControl('conditions');
    control.addControl('conditions', arr);
    // control.push(arr);
  }

  setOffenses(offenses, intakeservicerequestevaluationid) {
    const arr = new FormArray([]);
    offenses.forEach(offense => {
      const offenseGroup = this._formBuilder.group({
        allegationid: offense.allegationid,
        name: offense.name,
        source: offense.offenceaddedtype,
        status: offense.activeflag,
        decision: offense.adjudicateddecisiontypekey,
        intakeservicerequestevaluationid: intakeservicerequestevaluationid,
        intakeservicerequestevaluationconfigid: offense.intakeservicerequestevaluationconfigid,
        courtactionallegationconfigid: offense.courtactionallegationconfigid,
        courtActions: [],
        courtordertypekey: [],
        conditions: this.setConditions(offense.conditiontype ? offense.conditiontype : []),
        conditiontypekey: [],
        conditiontypedescription: offense.conditiontypedescription
      });
      setTimeout(() => {
        offenseGroup.patchValue({
          courtActions: offense.courtactiontype ? offense.courtactiontype.map(courtaction => courtaction.courtactiontypekey) : [],
          courtordertypekey: offense.courtordertype ? offense.courtordertype.map(courtorder => courtorder.courtordertypekey) : [],
          conditiontypekey: offense.conditiontype ? offense.conditiontype.map(condition => condition.conditiontypekey) : []
        });
      }, 100);

      arr.push(offenseGroup);
    });
    console.log(arr);
    return arr;
  }

  setConditions(condtions) {
    const arr = new FormArray([]);
    condtions.forEach(condition => {
      const conditionGroup = this._formBuilder.group({
        conditiontypekey: condition.conditiontypekey,
        conditiontype: condition.conditiontype,
        conditiontypedescription: condition.conditiontypedescription,
        completiondate: condition.completiondate,
        Intakeservicerequestcourtconditiontypeconfig: condition.Intakeservicerequestcourtconditiontypeconfig
      });
      arr.push(conditionGroup);
    });

    return arr;
  }

  editHearingOffense(hearing) {
    this.editOffenseForm.reset();
    this.evaluationInfo = null;

    this.complaintIDs = [];
    hearing.petitions.forEach(petition => {
      petition.complaints.forEach(complaint => { this.complaintIDs.push(complaint); });
    });

    (<any>$('#edit-offense-hearing')).modal('show');
  }

  updateOffense(complaint) {
    const offenses = complaint.offences.map(offense => {
      if (offense.activeflag === 1) {
        return offense.allegationid;
      }
    });
    const offensesName = complaint.offences.filter(offense => {
      if (offense.offenceaddedtype !== 'Court') {
        return offense.name;
      }
    });
    if (complaint.intakeservicerequestevaluationid) {
      this.getCompalintIdDetails(complaint.intakeservicerequestevaluationid);
    }
    setTimeout(() => {
      this.editOffenseForm.get('offenses').patchValue(offenses);
      this.editOffenseForm.get('offensesName').patchValue(offensesName);
    }, 10);
  }

  getCompalintIdDetails(intakeservicerequestevaluationid) {
    this._commonHttpService
      .getArrayList({
        where: {
          intakeservicerequestevaluationid: intakeservicerequestevaluationid
        },
        method: 'post'
      },
        NewUrlConfig.EndPoint.Intake.getComplaintDetails).subscribe(response => {
          const complaints: any = response;
          if (complaints && complaints.data && complaints.data.length > 0) {
            this.evaluationInfo = complaints.data[0];
            console.log('complaint details', this.evaluationInfo);
          }
        });
  }

  saveOffenses() {
    if (this.editOffenseForm.get('complaintID').value) {
      this._commonHttpService.create(this.formatSaveOffensesObject(), NewUrlConfig.EndPoint.DSDSAction.Court.AddUpdateOffense).subscribe(
        (response) => {
          this._alertService.success('Offenses Updated Successfully.');
          this.loadHearings();
          (<any>$('#edit-offense-hearing')).modal('hide');
        }, (error) => {
          this._alertService.error('Unable to save offenses, please try again.');
          console.log('Unable to save offenses', error);
          return false;
        });
    } else {
      this._alertService.warn('Select a Complaint to update Offenses.');
    }
  }

  formatSaveOffensesObject() {
    const saveOffenseObject = {
      intakeservicerequestevaluationid: this.editOffenseForm.get('complaintID').value,
      offences: []
    };
    this.editOffenseForm.get('offenses').value.forEach(offense => {
      if (offense) {
        saveOffenseObject.offences.push({ 'allegationid': offense });
      }
    });

    return saveOffenseObject;
  }

  completeHearing(action) {
    console.log(this.hearingForm.getRawValue());
    const hearingForm = this.hearingForm.getRawValue();
    const petition = this.selectedPetitionToEdit;
    petition.legalCounselName = this.hearingForm.value.legalCounselName;
    petition.judgeName = this.hearingForm.value.judgeName;
    petition.worker = this.hearingForm.value.worker;
    petition.courtActions = this.hearingForm.value.courtActions;
    petition.intakeAllegation = this.hearingForm.value.intakeAllegation;
    petition.adjudicatedOffense = this.hearingForm.value.adjudicatedOffense;
    petition.adjudicatedDecision = this.hearingForm.value.adjudicatedDecision;
    petition.adjudicatedDate = this.hearingForm.value.adjudicatedDate;
    petition.adjuctedTime = this.hearingForm.value.adjuctedTime;
    petition.courtordertypekey = this.hearingForm.value.courtordertypekey;
    petition.orderDate = this.hearingForm.value.orderDate;
    petition.orderTime = this.hearingForm.value.orderTime;
    petition.conditiontypekey = this.hearingForm.value.conditiontypekey;
    petition.conditiontypedescription = this.hearingForm.value.conditiontypedescription;
    petition.completionDate = this.hearingForm.value.completionDate;
    petition.completionTime = this.hearingForm.value.completionTime;
    petition.terminationDate = this.hearingForm.value.terminationDate;
    petition.expirationdate = hearingForm.expirationdate;
    petition.expirymonth = hearingForm.expiryMonth;
    petition.terminationTime = this.hearingForm.value.terminationTime;
    petition.petitions = this.hearingForm.value.petitions;
    petition.othercourtactiontypenotes = this.hearingForm.value.othercourtactiontypenotes;
    petition.saocountyid = hearingForm.saocountyid;
    petition.saotransfernotes = hearingForm.saotransfernotes;
    petition.createDisposition = hearingForm.createDisposition;
    petition.issendtoccu = hearingForm.issendtoccu;
    petition.restitutiondocpath = this.restitutionuploadedFile ? this.restitutionuploadedFile.s3bucketpathname : '';
    if (action === 1) {
      this.findInvalidControls();
      if (this.hearingForm.valid && this.validateDecision(petition.petitions)) {
        this.createCourt(petition);
        // petition.isComplete = true;
        // (<any>$('#view-hearing')).modal('hide');
      } else {
        this._alertService.warn('Please fill mandatory details');
      }
    } else if (action === 0) {
      // petition.isComplete = false;
      this.saveCourt(petition);
      (<any>$('#view-hearing')).modal('hide');
    }
    this._dataStoreService.setData(MyNewintakeConstants.Intake.scheduledHearings, this.petitionList);
    // console.log(this.petitionList);
  }

  validateDecision(petitions): boolean {
    if (this.isAdjudi || this.isDisposition) {
      for (const petition of petitions) {
        for (const complaint of petition.complaints) {
          for (const offense of complaint.offenses) {
            if (offense.status === 1 && this.isDisposition && (!offense.decision || offense.courtActions.length === 0 || offense.courtordertypekey.length === 0)) {
              return false;
            } else if (offense.status === 1 && this.isAdjudi && (!offense.decision || offense.courtActions.length === 0)) {
              return false;
            }
            for (const condition of offense.conditions) {
              if (!condition.conditiontypedescription) {
                return false;
              }
            }
          }
        }
      }
    }

    return true;
  }

  saveCourt(data) {
    const reqData = this.formatRequestData(data);
    this._commonHttpService.create(reqData, NewUrlConfig.EndPoint.DSDSAction.Court.SaveCourt).subscribe(
      (response) => {
        this._alertService.success('Court details saved successfully.');
        this.loadHearings();
        (<any>$('#view-hearing')).modal('hide');
      }, (error) => {
        this._alertService.error('Unable to save court details, please try again.');
        console.log('Save court details Error', error);
        return false;
      });
  }

  createCourt(data) {
    const reqData = this.formatRequestData(data);
    this._commonHttpService.create(reqData, NewUrlConfig.EndPoint.DSDSAction.Court.CreateCourt).subscribe(
      (response) => {
        this._alertService.success('Court details saved successfully.');
        this.loadHearings();
        this._dataStoreService.setData(IntakeStoreConstants.statusChanged, true);
        (<any>$('#view-hearing')).modal('hide');
      }, (error) => {
        this._alertService.error('Unable to save court details, please try again.');
        console.log('Save court details Error', error);
        return false;
      });
  }

  private formatRequestData(data: any) {
    const courtActionsArray = [];
    this.courtActionsFiltered.forEach(courtAction => {
      if (data.courtActions.includes(courtAction.courtactiontypekey)) {
        courtActionsArray.push(courtAction);
        console.log(courtAction);
      }
    });
    const courtActionsArrayUnique = Array.from(new Set(courtActionsArray));
    const hearingForm = this.hearingForm.getRawValue();
    const intakeJson = this._dataStoreService.getData(MyNewintakeConstants.Intake.intakeModel);
    let youth = null;
    if (intakeJson && intakeJson.persons && intakeJson.persons.length) {
      youth = intakeJson.persons.find(person => person.Role);
    }
    const reqData = {
      'intakenumber': this._dataStoreService.getData(MyNewintakeConstants.Intake.intakenumber),
      'intakeservicerequestcourtactionid': data.intakeservicerequestcourtactionid,
      'legalcounselname': data.legalCounselName,
      'magistratename': data.judgeName,
      'workername': data.worker,
      'hearingdatetime': hearingForm.hearingDate ? new Date(hearingForm.hearingDate) : null,
      'courtaction': courtActionsArrayUnique,
      'courtorder': this.getFormatOrder(data.courtordertypekey),
      'courtcondition': this.getFormatContions(data.conditiontypekey),
      'petitions': this.arrayToOBjArry(data.petitionids, 'intakeservicerequestpetitionid', 'intakeservicerequestpetitionid'),
      'courtorderdatetime': data.orderDate ? new Date(data.orderDate) : null,
      'expirationdate': data.expirationdate,
      'expirymonth': data.expiryMonth,
      'conditiontypedescription': data.conditiontypedescription,
      'conditiontypecompletiondatetime': data.completionDate ? new Date(data.completionDate) : null,
      'terminationdatetime': data.terminationDate ? new Date(data.terminationDate) : null,
      'adjudicationdatetime': data.adjudicatedDate ? new Date(data.adjudicatedDate) : null,
      'adjudicationdecision': data.adjudicatedDecision,
      'intakeservicerequestcourthearingid': data.intakeservicerequestcourthearingid,
      'hearingtypekey': data.hearingtypekey,
      'allegations': this.formatAdjudicatedOffenses(data.petitions),
      'othercourtactiontypenotes': data.othercourtactiontypenotes,
      'saocountyid': data.saocountyid,
      'saotransfernotes': data.saotransfernotes,
      'petitionid': data.petitions.map(petition => petition.petitionid).toString(),
      'youth': youth,
      'createDisposition': data.createDisposition,
      'intakeserviceid': data.intakeserviceid,
      'issendtoccu': data.issendtoccu,
      'restitutiondocpath': data.restitutiondocpath
    };
    console.log(reqData);
    return reqData;
  }

  formatAdjudicatedOffenses(petitions) {
    const offenses = [];
    if (this.isAdjudi || this.isDisposition) {
      for (const petition of petitions) {
        for (const complaint of petition.complaints) {
          for (const offense of complaint.offenses) {
            const courtActionsArray = [];
            this.courtActionsFiltered.forEach(courtAction => {
              if (offense.courtActions.includes(courtAction.courtactiontypekey)) {
                courtActionsArray.push(courtAction);
              }
            });
            if (offense.status === 1) {
              offenses.push({
                'adjudicationdecision': offense.decision,
                'allegationid': offense.allegationid,
                'intakeservicerequestevaluationid': offense.intakeservicerequestevaluationid,
                'intakeservicerequestevaluationconfigid': offense.intakeservicerequestevaluationconfigid,
                'courtactionallegationconfigid': offense.courtactionallegationconfigid,
                'courtaction': courtActionsArray,
                'courtorder': this.getFormatOrder(offense.courtordertypekey),
                'courtcondition': this.getFormatContions(offense.conditions),
                'conditiontypedescription': offense.conditiontypedescription,
                'completiondate  ': offense.completiondate
              });
            }
          }
        }
      }
    }
    return offenses;
  }

  getFormatOrder(orders) {
    const ordersArray = [];
    orders.forEach(order => {
      ordersArray.push({
        courtordertypekey: order,
        Intakeservicerequestcourtordertypeconfigid: null
      });
    });
    return ordersArray;
  }

  getFormatContions(conditions) {
    const conditionsArray = [];
    conditions.forEach(condition => {
      (this.isAdjudi || this.isDisposition) ?
        conditionsArray.push({
          conditiontypekey: condition.conditiontypekey,
          conditiontype: condition.conditiontype,
          conditiontypedescription: condition.conditiontypedescription,
          completiondate: condition.completiondate,
          Intakeservicerequestcourtconditiontypeconfig: condition.Intakeservicerequestcourtconditiontypeconfig
        }) : conditionsArray.push({
          conditiontypekey: condition,
          Intakeservicerequestcourtconditiontypeconfig: condition.Intakeservicerequestcourtconditiontypeconfig
        });
    });

    return conditionsArray;
  }

  toggleTable(id) {
    (<any>$('#' + id)).collapse('toggle');
  }

  convertToDate(date, time) {
    let resultantDate;
    if (date) {
      resultantDate = new Date(date);
      if (time) {
        const timeSplit = time.split(':');
        const appointmentTimeHour = timeSplit[0];
        const appointmentTimeMin = timeSplit[1];

        resultantDate.setHours(appointmentTimeHour);
        resultantDate.setMinutes(appointmentTimeMin);
      }
    }
    return resultantDate;
  }

  arrayToOBjArry(list, key, objkey) {
    let result;
    if (list) {
      result = list.map((data) => {
        const obj = {};
        if (objkey) {
          obj[key] = data[objkey];
        } else {
          obj[key] = data;
        }
        return obj;
      });
    }
    return result;
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.hearingForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log(invalid);
    return invalid;
  }
  onCourtOrderChange() {
    const courtOrders = this.hearingForm.getRawValue().courtordertypekey;
    let isTransferOfJuridication = false;
    if (courtOrders && courtOrders.length) {
      isTransferOfJuridication = courtOrders.indexOf('TROJ') !== -1;
    }

    this.isTransferOfJuridication = isTransferOfJuridication;


  }

  setValidityControl() {
    if (this.isTransferOfJuridication) {
      this.hearingForm.get('saocountyid').setValidators(Validators.required);
      this.hearingForm.updateValueAndValidity();
    } else {
      this.hearingForm.get('saocountyid').clearValidators();
      this.hearingForm.updateValueAndValidity();
    }
  }

  orderDateChanged() {
    const orderDate = this.hearingForm.getRawValue().orderDate;
    if (orderDate) {
      this.expirationMinDate = new Date(orderDate);
      this.changeExpirationDate();
    }

  }
  changeExpirationDate() {
    const orderDate = this.hearingForm.getRawValue().orderDate;
    const expiryMonth = this.hearingForm.getRawValue().expiryMonth;
    if (orderDate && expiryMonth) {
      const orderDateValue = new Date(orderDate);
      const expirationDate = new Date(orderDateValue.setMonth(orderDateValue.getMonth() + expiryMonth));
      this.hearingForm.patchValue({ expirationdate: expirationDate });
      console.log('cal order from', expirationDate);
    }

  }

  uploadFile(file: File | FileError): void {
    if (!(file instanceof File)) {
      this._alertService.error('Invalid File!');
      return;
    } else {
      this.restitutionupload = file;
      this.upload();
    }
  }

  private upload() {
    this._uploadService
      .upload({
        url: AppConfig.baseUrl + '/' + CommonUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
        headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
        filesKey: ['file'],
        files: this.restitutionupload,
        process: true
      })
      .subscribe(
        (response) => {
          if (response.status === 1 && response.data) {
            this.restitutionuploadedFile = response.data;
            this._alertService.success('File Uploaded Succesfully');
          }
        },
        (err) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        },
        () => {
          // console.log('complete');
        }
      );
  }

}
