import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeSaoHearingDetailsRoutingModule } from './intake-sao-hearing-details-routing.module';
import { IntakeSaoHearingDetailsComponent } from './intake-sao-hearing-details.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeSaoHearingDetailsRoutingModule,
    FormMaterialModule,
    PaginationModule,
    NgxfUploaderModule,
    SharedPipesModule
  ],
  declarations: [IntakeSaoHearingDetailsComponent]
})
export class IntakeSaoHearingDetailsModule { }
