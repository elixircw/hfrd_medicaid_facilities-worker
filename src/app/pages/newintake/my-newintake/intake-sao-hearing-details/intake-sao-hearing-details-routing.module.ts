import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeSaoHearingDetailsComponent } from './intake-sao-hearing-details.component';

const routes: Routes = [
  {
      path: '',
      component: IntakeSaoHearingDetailsComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeSaoHearingDetailsRoutingModule { }
