import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeSaoHearingDetailsComponent } from './intake-sao-hearing-details.component';

describe('IntakeSaoHearingDetailsComponent', () => {
  let component: IntakeSaoHearingDetailsComponent;
  let fixture: ComponentFixture<IntakeSaoHearingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeSaoHearingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeSaoHearingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
