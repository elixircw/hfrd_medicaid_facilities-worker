import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewUrlConfig } from '../../newintake-url.config';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { MyNewintakeConstants, IntakeStoreConstants } from '../my-newintake.constants';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'intake-sao-scheduled-hearing',
  templateUrl: './intake-sao-scheduled-hearing.component.html',
  styleUrls: ['./intake-sao-scheduled-hearing.component.scss']
})
export class IntakeSaoScheduledHearingComponent implements OnInit {
  scheduledHearingForm: FormGroup;
  hearingTypes$: Observable<DropdownModel[]>;
  popupButtonLabel: string;
  submitedPetitions = [];
  scheduledHearings = [];
  hearingList = [];
  hasMultiplePetitionID = false;
  minDate;
  editIndex: number;
  isDisplay = false;
  filterPetitionIDList = [];
  addedPersons = [];
  intakeNumber: string;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalRecords: number;
  maxSize = 10;
  isOther: boolean;
  constructor(private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.isOther = false;
    this.editIndex = -1;
    this.initFormGroup();
    this.loadDropDowns();

    /*  this.route.params.subscribe((item) => {
       this.intakeNumber = item['id'];
       this.loadScheduledHearings();
     }); */
    this.intakeNumber = this._dataStoreService.getData(IntakeStoreConstants.intakenumber);
    this.loadScheduledHearings();
    this.loadAddedPetitions();

    const today = new Date();
    today.setDate(today.getDate() + 1);
    this.minDate = today;
  }

  loadAddedPetitions() {
    this._commonHttpService.getArrayList({
      page: 1,
      limit: 10,
      sortcolumn: 'createddate',
      sortorder: 'asc',
      where: {
        intakenumber: this.intakeNumber
      },
      method: 'get'
    }, NewUrlConfig.EndPoint.Intake.getAddedPetitions).subscribe(
      (response) => {
        this.submitedPetitions = response;
        this.hasMultiplePetitionID = this.submitedPetitions.length > 1;
      });
  }

  loadScheduledHearings() {
    this._commonHttpService.getArrayList({
      page: this.paginationInfo.pageNumber,
      limit: this.paginationInfo.pageSize,
      sortcolumn: 'intakenumber',
      sortorder: 'asc',
      where: {
        intakenumber: this.intakeNumber
      },
      method: 'get'
    }, NewUrlConfig.EndPoint.Intake.getScheduledHearings).subscribe(
      (response) => {
        console.log('scheduled hearing response', response);
        this.scheduledHearings = [];
        response.forEach(scheduledHearing => {
          this.scheduledHearings.push(this.extractDetailsFromPetitionArray(scheduledHearing));
        });
        if (this.scheduledHearings.length > 0) {
          this.totalRecords = this.scheduledHearings[0].totalcount;
        }
      });
  }

  checkHearingType(hearingType) {
    this.isOther = (hearingType === 'Other');
    if (this.isOther) {
      this.scheduledHearingForm.controls['otherhearingtypenotes'].setValidators(Validators.required);
      this.scheduledHearingForm.controls['otherhearingtypenotes'].updateValueAndValidity();
    } else {
      this.scheduledHearingForm.controls['otherhearingtypenotes'].clearValidators();
      this.scheduledHearingForm.controls['otherhearingtypenotes'].updateValueAndValidity();
    }
  }

  pageChanged(pageInfo) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadScheduledHearings();
  }

  initFormGroup(): void {
    this.scheduledHearingForm = this._formBuilder.group({
      petitions: ['', Validators.required],
      hearingdatetime: ['', Validators.required],
      hearingtime: ['08:00', Validators.required],
      hearingtypekey: ['', Validators.required],
      associatedattorneys: '',
      transferpetitionid: '',
      transfernotes: '',
      petitionidsString: '',
      hearingID: '',
      petitionuuids: '',
      otherhearingtypenotes: ''
    });
  }

  loadDropDowns(): void {
    const hearing = this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.hearingType)
      .map(hearingType => {
        return {
          hearingTypes: hearingType.map(res =>
            new DropdownModel({
              text: res.description,
              value: res.hearingtypekey
            }))
        };
      }).share();

    this.hearingTypes$ = hearing.pluck('hearingTypes');

    this.hearingTypes$.subscribe(hearingTypes => {
      for (const hearingType of hearingTypes) {
        this.hearingList[hearingType.value] = hearingType.text;
      }
    });
  }

  addNewHearing(): void {
    this.filterPetitionIDs();
    this.isDisplay = false;
    this.scheduledHearingForm.reset();
    this.scheduledHearingForm.get('hearingtime').setValue('08:00');
    this.scheduledHearingForm.enable();
    this.popupButtonLabel = 'Add';
    (<any>$('#add-schedule')).modal('show');
  }

  view(scheduledHearing): void {
    this.isDisplay = true;
    if (Array.isArray(scheduledHearing.petitions)) {
      scheduledHearing.petitionidsString = scheduledHearing.petitions.map(petitionid => petitionid.petitionid);
    } else {
      scheduledHearing.petitionidsString = scheduledHearing.petitions.petitionid;
    }

    this.checkHearingType(scheduledHearing.hearingtypekey);

    const hearingDateTimeObj = new Date(scheduledHearing.hearingdatetime);
    scheduledHearing.hearingtime = moment(hearingDateTimeObj).format('HH:mm');
    setTimeout(() =>
      this.scheduledHearingForm.patchValue(scheduledHearing), 100);
    this.popupButtonLabel = 'Close';
    this.scheduledHearingForm.disable();
    (<any>$('#add-schedule')).modal('show');
  }

  edit(scheduledHearing, index): void {
    this.editIndex = index;
    this.isDisplay = true;
    if (Array.isArray(scheduledHearing.petitions)) {
      scheduledHearing.petitionidsString = scheduledHearing.petitions.map(petitionid => petitionid.petitionid);
    } else {
      scheduledHearing.petitionidsString = scheduledHearing.petitions.petitionid;
    }
    this.checkHearingType(scheduledHearing.hearingtypekey);
    setTimeout(() =>
      this.scheduledHearingForm.patchValue(scheduledHearing), 100);
    this.popupButtonLabel = 'Update';
    this.scheduledHearingForm.enable();
    this.scheduledHearingForm.get('petitionidsString').disable();
    (<any>$('#add-schedule')).modal('show');
  }

  openDeletePopup(index) {
    this.editIndex = index;
    (<any>$('#delete-popup')).modal('show');
  }

  delete(): void {
    if (this.editIndex !== -1) {
      this.deleteHearing(this.scheduledHearings[this.editIndex]);
    }
  }

  createOrUpdateScheduleHearing() {
    if (this.scheduledHearingForm.invalid) {
      this._alertService.warn('please fill required feilds');
      return false;
    }
    const scheduledHearing = this.scheduledHearingForm.getRawValue();
    if (!this.hasMultiplePetitionID && !Array.isArray(scheduledHearing.petitions)) {
      scheduledHearing.petitions = [scheduledHearing.petitions];
    }

    if (this.popupButtonLabel === 'Update') {
      if (this.editIndex >= 0) {
        this.updateHearing(scheduledHearing);
      }
    } else if (this.popupButtonLabel !== 'Close') {
      this.saveHearing(scheduledHearing);
    }
    this.filterPetitionIDs();

    (<any>$('#add-schedule')).modal('hide');
  }

  deleteHearing(scheduledHearing) {
    this._commonHttpService.remove(scheduledHearing.hearingID, {}, NewUrlConfig.EndPoint.Intake.deleteHearing).subscribe(
      (response) => {
        console.log('delete response', response);
        this.scheduledHearings.splice(this.editIndex, 1);
        this.broadCastHearingsScheduled();
        (<any>$('#delete-popup')).modal('hide');
        this._alertService.success('Scheduled Hearing Deleted Successfully');
        this.checkForMultiplePetitionIdAvailable();
      });
  }

  updateHearing(scheduledHearing) {
    const serverHearing = this.createServerHearingObj(scheduledHearing);
    this._commonHttpService.patch(scheduledHearing.hearingID, serverHearing, NewUrlConfig.EndPoint.Intake.updateHearing).subscribe(
      (response) => {
        console.log('response', response);
        this._alertService.success('Hearing scheduled for the pettion(s) successfully');
        this.loadScheduledHearings();
        this.broadCastHearingsScheduled();
      });
  }

  saveHearing(scheduledHearing) {
    const serverHearing = this.createServerHearingObj(scheduledHearing);
    this._commonHttpService.create(serverHearing, NewUrlConfig.EndPoint.Intake.createHearing).subscribe(
      (response) => {
        console.log('response', response);
        this._alertService.success('Hearing scheduled for the pettion(s) successfully');
        scheduledHearing.hearingID = response.intakeservicerequestcourthearing.intakeservicerequestcourthearingid;
        this.loadScheduledHearings();
        this.broadCastHearingsScheduled();
      });
  }

  createServerHearingObj(scheduledHearing) {
    if (scheduledHearing.hearingtime) {
      const timeSplit = scheduledHearing.hearingtime.split(':');

      if (!(scheduledHearing.hearingdatetime instanceof Date)) {
        scheduledHearing.hearingdatetime = moment(scheduledHearing.hearingdatetime).toDate();
      }
      scheduledHearing.hearingdatetime.setHours(timeSplit[0]);
      scheduledHearing.hearingdatetime.setMinutes(timeSplit[1]);
    }
    const hearing = {
      intakenumber: this.intakeNumber,
      associatedattorneys: scheduledHearing.associatedattorneys,
      transferpetitionid: scheduledHearing.transferpetitionid,
      hearingtypekey: scheduledHearing.hearingtypekey,
      hearingdatetime: scheduledHearing.hearingdatetime,
      transfernotes: scheduledHearing.transfernotes,
      otherhearingtypenotes: scheduledHearing.otherhearingtypenotes,
      petitions: []
    };

    hearing.petitions = scheduledHearing.petitions.map(petition => {
      return { intakeservicerequestpetitionid: petition.intakeservicerequestpetitionid };
    });

    return hearing;
  }

  checkForMultiplePetitionIdAvailable() {
    this.hasMultiplePetitionID = this.filterPetitionIDList.length > 1;
  }

  filterPetitionIDs() {
    const addedPetitions = [];
    this.scheduledHearings.map(hearing => {
      if (hearing.petitionids && Array.isArray(hearing.petitionids)) {
        hearing.petitionids.map(petition => {
          addedPetitions.push(petition.petitionid);
        });
      } else if (hearing.petitionids && !Array.isArray(hearing.petitionids)) {
        addedPetitions.push(hearing.petitionids.petitionid);
      }
    });

    /*
    // disabling the filter to pass the user story schedule multiple heraring  to single petition
    this.filterPetitionIDList = this.submitedPetitions.filter(petition => {
       return addedPetitions.indexOf(petition.petitionid) === -1;
     }); */

    this.filterPetitionIDList = this.submitedPetitions;

    this.checkForMultiplePetitionIdAvailable();
  }

  broadCastHearingsScheduled() {
    console.log('sh', this.scheduledHearings);
    this._dataStoreService.setData(MyNewintakeConstants.Intake.scheduledHearings, this.scheduledHearings);
  }

  extractDetailsFromPetitionArray(scheduledHearing): any {
    const petitionids = scheduledHearing.petitions;
    scheduledHearing.petitionidsString = petitionids.map(petitionid => petitionid.petitionid);
    scheduledHearing.petitionuuids = petitionids.map(petitionid => petitionid.intakeservicerequestpetitionid);
    scheduledHearing.offencesString = petitionids.map(petition => petition.complaints.map(complaint => complaint.offences.map(offense => offense.name)));

    const complaintIDs = [];
    const victims = [];
    petitionids.map(petition => petition.complaints.map(complaint => {
      complaintIDs.push(complaint.complaintid);
      if (complaint.victims) {
        complaint.victims.map(victim => {
          victims.push(victim.firstname + ' ' + victim.lastname);
        });
      }
    }));

    scheduledHearing.selectedVictims = victims;

    const datesArray = [];

    petitionids.map(petition => petition.complaints.forEach(complaint => {
      const date = complaint.offensedate;
      if (date !== null) {
        datesArray.push(moment(date).format('MM/DD/YYYY'));
      }
    }));

    scheduledHearing.offenceDates = datesArray;
    scheduledHearing.hearingID = scheduledHearing.intakeservicerequestcourthearingid;

    if (scheduledHearing.hearingtime) {
      const timeSplit = scheduledHearing.hearingtime.split(':');

      if (!(scheduledHearing.hearingdatetime instanceof Date)) {
        scheduledHearing.hearingdatetime = new Date(scheduledHearing.hearingdatetime);
      }
      scheduledHearing.hearingdatetime.setHours(timeSplit[0]);
      scheduledHearing.hearingdatetime.setMinutes(timeSplit[1]);
    }

    return scheduledHearing;
  }

}
