import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntakeSaoScheduledHearingComponent } from './intake-sao-scheduled-hearing.component';

describe('IntakeSaoScheduledHearingComponent', () => {
  let component: IntakeSaoScheduledHearingComponent;
  let fixture: ComponentFixture<IntakeSaoScheduledHearingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntakeSaoScheduledHearingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntakeSaoScheduledHearingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
