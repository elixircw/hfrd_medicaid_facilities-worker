import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeSaoScheduledHearingRoutingModule } from './intake-sao-scheduled-hearing-routing.module';
import { IntakeSaoScheduledHearingComponent } from './intake-sao-scheduled-hearing.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    IntakeSaoScheduledHearingRoutingModule,
    FormMaterialModule,
    PaginationModule
  ],
  declarations: [IntakeSaoScheduledHearingComponent]
})
export class IntakeSaoScheduledHearingModule { }
