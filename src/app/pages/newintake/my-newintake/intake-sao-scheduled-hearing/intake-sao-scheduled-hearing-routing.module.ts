import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeSaoScheduledHearingComponent } from './intake-sao-scheduled-hearing.component';

const routes: Routes = [
  {
      path: '',
      component: IntakeSaoScheduledHearingComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeSaoScheduledHearingRoutingModule { }
