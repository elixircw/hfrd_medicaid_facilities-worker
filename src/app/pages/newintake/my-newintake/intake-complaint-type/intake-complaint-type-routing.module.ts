import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntakeComplaintTypeComponent } from './intake-complaint-type.component';

const routes: Routes = [{
  path: '',
  component: IntakeComplaintTypeComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeComplaintTypeRoutingModule { }
