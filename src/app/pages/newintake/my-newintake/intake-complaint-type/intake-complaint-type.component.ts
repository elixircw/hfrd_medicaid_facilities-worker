import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormControl
} from '@angular/forms';
import {
    DropdownModel,
    PaginationRequest
} from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { NewUrlConfig } from '../../newintake-url.config';
import { Subject } from 'rxjs/Rx';
import {
    ComplaintTypeCase,
    ChoosenAllegation,
    EvaluationFields
} from '../_entities/newintakeSaveModel';
import {
    IntakePurpose,
    SubType,
    AllegationItem,
    InvolvedPerson
} from '../_entities/newintakeModel';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService, DataStoreService, CommonDropdownsService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { REGEX } from '../../../../@core/entities/constants';
import * as reason from './_configurations/reason.json';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { IntakeStoreConstants, MyNewintakeConstants } from '../my-newintake.constants';
import { IntakeConfigService } from '../intake-config.service';
import { AppConstants } from '../../../../@core/common/constants';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'intake-complaint-type',
    templateUrl: './intake-complaint-type.component.html',
    styleUrls: ['./intake-complaint-type.component.scss']
})
export class IntakeComplaintTypeComponent implements OnInit, AfterViewInit {
    serviceTypes$: Observable<DropdownModel[]>;
    subServiceTypes$: Observable<DropdownModel[]>;
    selectedAllegation$: Observable<any[]>;
    filteredAllegations$: Observable<any[]>;
    allegations: any = [];
    selectedAllegations: any = [];
    servicetype: DropdownModel;
    createdCases: ComplaintTypeCase[] = [];
    caseCreationFormGroup: FormGroup;
    caseEditFormGroup: FormGroup;
    ICJFormGroup: FormGroup;
    LawEnforcementFormGroup: FormGroup;
    selectedPurpose: IntakePurpose;
    purposeID: string;
    purposeList: IntakePurpose[];
    subServiceTypes: SubType[];
    editCase: ComplaintTypeCase;
    deleteCaseID: string;
    pdfFiles: { fileName: string; images: string[] }[] = [];
    images: string[] = [];
    allegationDropDownItems: DropdownModel[];
    allegationItems: AllegationItem[];
    /* @Input() purposeInputSubject$ = new Subject<IntakePurpose>();
    @Input() createdCaseInputSubject$ = new Subject<ComplaintTypeCase[]>();
    @Input() createdCaseOuptputSubject$ = new Subject<ComplaintTypeCase[]>();
    @Input() reviewstatus: string;
    @Input() offenceCategoriesInputSubject$ = new Subject<any>();
    @Input() evalFieldsInputSubject$ = new Subject<EvaluationFields[]>();
    @Input() evalFieldsOutputSubject$ = new Subject<EvaluationFields[]>(); */
    filteredAllegationItems: AllegationItem[] = [];
    choosenAllegation: ChoosenAllegation;
    roleId: AppUser;
    // selectedDaTypeKeyforEdit: string;
    selectedcaseIndex: number;
    groupSetupForm: FormGroup;
    groupDAReasonForm: FormGroup;
    reasonDropdown: DropdownModel[];
    groupSelectedDAIds: string[];
    ungroupSelectedGroupIds: string[];
    token: AppUser;
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = false;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    @ViewChild('allegationInput') allegationInput: ElementRef;
    allegationCtrl = new FormControl();
    allegation: string[];
    indicators: any[];
    offenceCategories: any[] = [];
    selectedAllegationList$: Observable<any[]>;
    stateDropdownItems$: Observable<any[]>;
    mdCountys$: Observable<DropdownModel[]>;
    requisitionType$: Observable<DropdownModel[]>;
    deliquenttypes$: Observable<any[]>;
    isComplaintTypeDisabled = false;
    evaluationFields: EvaluationFields[];
    complaintIds = [];
    filteredComplaintIds = [];
    store: any;
    isComplaintNeeded: boolean;
    isInterstateCompactPurpose = false;
    isCaseSubTypeDependancy = false;
    isReqdueWarrant = false;
    persons: any = [];
    addedPersons: InvolvedPerson[] = [];
    showICJFields = false;
    showLawFileds = false;
    showWarrantTypeDetails = false;
    notes = 'Notes';
    AllegationsPlaceholder = 'Allegations';
    isinNCIC = false;
    warranttypemodel = true;
    warranttypedetailskey: any[];
    icjHeading = '';
    deliquentTypePlaceholder = 'Delinquent Type';
    currentDate = new Date();
    isLawEnforcement = false;
    selectedsubServiceType = '';
    constructor(
        private _commonHttpService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _datastore: DataStoreService,
        private _intakeConfig: IntakeConfigService,
        private dropdownService: CommonDropdownsService
    ) {
        this.store = this._datastore.getCurrentStore();

    }

    ngOnInit() {
        this.token = this._authService.getCurrentUser();

        this.buildFormGroup();
        this.processSavedCases();
        this.reasonDropdown = <any>reason;
        this.roleId = this._authService.getCurrentUser();
        this._datastore.currentStore.subscribe(store => {
            if (store[IntakeStoreConstants.purposeSelected]) {
                const purposeSelected = store[IntakeStoreConstants.purposeSelected];
                const storePurposeId = purposeSelected.value;
                if (storePurposeId !== this.purposeID) {
                    this.purposeID = storePurposeId;
                    this.listAllegations(this.purposeID);
                    this.isComplaintTypeDisabled = false;
                    this.caseCreationFormGroup.get('serviceType').enable();
                    this.listServicetypes(this._authService.getAgencyName());
                    this.caseCreationFormGroup.patchValue({
                        serviceType: this.purposeID
                    });
                    this.isComplaintNeeded = this._intakeConfig.isComplaintNeeded();
                    this.isCaseSubTypeDependancy = this._intakeConfig.isCaseHasSubTypeDependancy();
                    if (!this.isCaseSubTypeDependancy) {
                        // this.listAllegations(this.purposeID);
                    } else {
                        this.loadDropdown();
                    }
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.INTERSTATE_COMPACT)) {
                        this.isInterstateCompactPurpose = true;
                        this.formValueChanges();
                        setTimeout(() => {
                            if (this.createdCases && this.createdCases.length > 0) {
                                this.loadFocusPersonCaseDetails(this.createdCases[0].subServiceTypeID);
                            }
                        }, 500);
                        const ICJFormdata = this._datastore.getData(IntakeStoreConstants.FocuspersonCaseDetails);
                        if (ICJFormdata) {
                            this.ICJFormGroup.patchValue(ICJFormdata);
                            const persondetails = ICJFormdata.residingwithdetails ? ICJFormdata.residingwithdetails.map(res => {
                                return res.personid;
                            }) : null;
                            setTimeout(() => {
                                // this.loadFocusPersonCaseDetails(this.createdCases[0].subServiceTypeID);
                                this.ICJFormGroup.patchValue({ residingwithdetails: persondetails });
                            }, 1000);
                        }
                    }
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.PEACE_ORDER)) {
                        this.isLawEnforcement = true;
                    }
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT)) {
                        this.isLawEnforcement = true;
                        this.formValueChanges();
                        const LawEnforcementFormdata = this._datastore.getData(IntakeStoreConstants.FocuspersonCaseDetails);
                        if (LawEnforcementFormdata) {
                            this.loadFocusPersonCaseDetails('');
                        }
                    }
                    this.isComplaintTypeDisabled = true;
                    this.setCaseFormValidity();
                    this.caseCreationFormGroup.get('serviceType').disable();
                }
            }

        });
        /* this.purposeInputSubject$.subscribe(purpose => {
            this.selectedPurpose = purpose;
            this.isComplaintTypeDisabled = false;
            this.caseCreationFormGroup.get('serviceType').enable();
            if (purpose.teamtype) {
                this.listServicetypes(purpose.teamtype.teamtypekey);
            }
            this.caseCreationFormGroup.patchValue({
                serviceType: this.selectedPurpose.intakeservreqtypeid
            });
            this.isComplaintTypeDisabled = true;
            this.caseCreationFormGroup.get('serviceType').disable();
        }); */


        this.listenAllegationList();
        this.listenSelectedAllegationList();
        this.listServiceSubtype([]);
    }

    ngAfterViewInit() {
        if (!this._intakeConfig.getiseditIntake()) {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
        }
    }

    formValueChanges() {
        this.ICJFormGroup.valueChanges.subscribe(res => {
            this._datastore.setData(IntakeStoreConstants.FocuspersonCaseDetailsFormValid, this.ICJFormGroup.valid);
            this._datastore.setData(IntakeStoreConstants.FocuspersonCaseDetails, this.ICJFormGroup.getRawValue());
        });
        this.LawEnforcementFormGroup.valueChanges.subscribe(res => {
            this._datastore.setData(IntakeStoreConstants.FocuspersonCaseDetailsFormValid, this.LawEnforcementFormGroup.valid);
            this._datastore.setData(IntakeStoreConstants.FocuspersonCaseDetails, this.LawEnforcementFormGroup.getRawValue());
        });
    }

    loadICJDropdownLists() {
        this.stateDropdownItems$ = this.dropdownService.getStateList();
        this.getCounty();
        this.getPersonList();
    }

    loadDropdown() {
        this.loadSubServiceTypesDependancy(this.purposeID);
    }

    getPersonList() {
        const personsList = this.store[IntakeStoreConstants.addedPersons];
        this.persons = [];
        if (personsList && personsList.length > 0) {
            this.addedPersons = personsList.filter(person => person.Role !== 'Youth');
            if (this.addedPersons && this.addedPersons.length > 0) {
                this.persons = this.addedPersons.map((person, index) => {
                    if (person.Role !== 'Youth') {
                        return {
                            personid: person.Pid ? person.Pid : AppConstants.PERSON.TEMP_ID + index,
                            firstName: person.Firstname,
                            lastName: person.Lastname,
                            fullName: person.fullName
                        };
                    }
                });
            }
        }
    }

    getCounty() {
        this.mdCountys$ = this.dropdownService.getCountyList('MD').map((data) => {
            return data.map((res) => {
                return new DropdownModel({
                    text: res.countyname,
                    value: res.countyid
                });
            });
        });
    }

    getRequisitionType() {
        this.requisitionType$ = this.dropdownService.getDropownsByTable('Requisition Type').map((data) => {
            return data.map((res) => {
                return new DropdownModel({
                    text: res.description,
                    value: res.ref_key
                });
            });
        });
    }

    onWarrentTypeChange(value) {
        this.showWarrantTypeDetails = true;
        this.deliquentTypePlaceholder = value + ' Type';
        this.warranttypemodel = (value === 'Delinquent') ? true : false;
        const tablename = (value === 'Delinquent') ? 'Warrant Delinquent Type' : 'Warrant Non Delinquent Type';
        this.deliquenttypes$ = this.dropdownService.getDropownsByTable(tablename).map((data) => {
            return data.map((res) => {
                return ({
                    warranttype: res.description,
                    warranttypekey: res.ref_key
                });
            });
        });
    }

    processSavedCases() {
        const createdCases = this.store[IntakeStoreConstants.createdCases];
        if (createdCases && Array.isArray(createdCases)) {
            this.createdCases = createdCases;
        } else {
            this.createdCases = [];
        }
    }

    buildFormGroup() {
        this.caseCreationFormGroup = this.formBuilder.group({
            serviceType: ['', Validators.required],
            complaintids: [''],
            selectedAllegations: [[]],
            subServiceType: ['', Validators.required],
            allegationId: [{ value: '', disabled: true }],
            isyouthincustody: false,
        });
        this.ICJFormGroup = this.formBuilder.group({
            demandingstate: ['', Validators.required],
            maxdateofexpiration: '',
            countyid: ['', Validators.required],
            residingwithdetails: [],
            notes: '',
            requisitiontypekey: ''
        });
        this.LawEnforcementFormGroup = this.formBuilder.group({
            notes: '',
            iswritwarrant: ['', Validators.required],
            warranttype: '',
            warranttypedetails: [],
            isdetaintheyouth: null
        });
        this.caseEditFormGroup = this.formBuilder.group({
            serviceType: ['', Validators.required],
            subServiceType: ['', Validators.required],
            isyouthincustody: false
        });
        this.groupSetupForm = this.formBuilder.group({
            DAItems: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            selectedDAItemsGroupNo: ['']
        });
        this.groupDAReasonForm = this.formBuilder.group({
            reasonType: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR]],
            reasonComments: ['']
        });
    }

    listServicetypes(teamtypekey) {
        const checkInput = {
            nolimit: true,
            where: { teamtypekey: teamtypekey },
            method: 'get',
            order: 'description'
        };

        this.serviceTypes$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter'
            )
            .map(result => {
                this.purposeList = result;
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.intakeservreqtypeid
                        })
                );
            });
    }

    getSelectedPurpose(purposeID): IntakePurpose {
        return this.purposeList.find(
            puroposeItem => puroposeItem.intakeservreqtypeid === purposeID
        );
    }

    getSelectedSubtype(subTypeID): SubType {
        return this.subServiceTypes.find(
            subServiceType =>
                subServiceType.servicerequestsubtypeid === subTypeID
        );
    }

    setCaseFormValidity() {
        if (this.isComplaintNeeded) {
            this.caseCreationFormGroup.get('complaintids').setValidators([Validators.required]);
            this.caseCreationFormGroup.updateValueAndValidity();
        } else {
            this.caseCreationFormGroup.get('complaintids').clearValidators();
            this.caseCreationFormGroup.updateValueAndValidity();
        }

    }

    listServiceSubtype(allegationIDs: string[]) {
        const servicetypeid = this.caseCreationFormGroup ? this.caseCreationFormGroup.get('serviceType').value : '';
        const checkInput = {
            where: { 'type': 'AL', 'allegationids': allegationIDs, intakeservicereqtypeid: servicetypeid },
            method: 'get',
            order: 'description'
        };
        this.subServiceTypes$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsSubTypes + '?filter'
            )
            .map(result => {
                this.subServiceTypes = result;
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.servicerequestsubtypeid
                        })
                );
            });
            if (this.isLawEnforcement) {
                this.subServiceTypes$.subscribe(result => {
                    if (result && result.length > 0) {
                        if (allegationIDs && allegationIDs.length > 0) {
                            this.selectedsubServiceType = result[0].text;
                            this.caseCreationFormGroup.patchValue({
                                subServiceType: result[0].value
                            });
                        }
                    }
                });
            }
    }

    loadSubServiceTypesDependancy(serviceType) {
        this.listServiceSubtypeDependancy(serviceType);
    }

    listServiceSubtypeDependancy(intakeservreqtypeid) {
        const checkInput = {
            include: 'servicerequestsubtype',
            nolimit: true,
            where: { intakeservreqtypeid: intakeservreqtypeid },
            method: 'get'
        };
        this.subServiceTypes$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.DATypeUrl + '/?filter'
            )
            .map(result => {
                this.subServiceTypes = result[0].servicerequestsubtype;
                return result[0].servicerequestsubtype.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.servicerequestsubtypeid
                        })
                );
            });
    }

    listAllegationsonChange(servicerequestsubtypeid) {
        const checkInput = {
            where: { intakeservreqtypeid: this.purposeID, servicerequestsubtypeid: servicerequestsubtypeid },
            nolimit: true,
            method: 'get',
            order: 'name'
        };
        this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter'
            ).subscribe(response => {
                this.allegations = response;
                this.offenceCategories = response;
                this.selectedAllegationList$ = Observable.of(response);
            });
        const selectedsubtype = this.getSelectedSubtype(servicerequestsubtypeid);
        if (MyNewintakeConstants.Intake.ICJReceivingProbation === selectedsubtype.classkey || MyNewintakeConstants.Intake.ICJReentry === selectedsubtype.classkey) {
            this.AllegationsPlaceholder = 'Adjudicated Offense';
        } else {
            this.AllegationsPlaceholder = 'Allegations';
        }
        if (MyNewintakeConstants.Intake.ICJRequisitionDuetoWarrant === selectedsubtype.classkey) {
            this.isReqdueWarrant = true;
        }
    }

    listAllegations(intakeservreqtypeid) {
        const checkInput = {
            where: { intakeservreqtypeid: intakeservreqtypeid },
            nolimit: true,
            method: 'get',
            order: 'name'
        };
        this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter'
            ).subscribe(response => {
                this.allegations = response;
                this.offenceCategories = response;
                this.selectedAllegationList$ = Observable.of(response);
            });
    }

    loadAllegations(serviceType: DropdownModel) {
        this.listAllegations(serviceType.value);
    }
    isSubTypeExists(servicerequestsubtypeid: string) {
        if (this.createdCases) {
            return this.createdCases.find(createdCase => {
                if (createdCase.caseID !== this.editCase.caseID) {
                    return createdCase.subServiceTypeID === servicerequestsubtypeid;
                }
            }
            );
        }
        return false;
    }

    loadAllegationSubtypes() {
        if (this.caseCreationFormGroup.value.selectedAllegations.length > 0) {
            this.listServiceSubtype(this.caseCreationFormGroup.value.selectedAllegations);
        }
    }

    addCase() {
        if (this.caseCreationFormGroup.valid) {
            /* if (
                 this.isSubTypeExists(
                     this.caseCreationFormGroup.value.subServiceType
                 )
             ) {
                 this._alertService.error(
                     'Already Case created for this sub type '
                 );
                 return false;
             }*/
            const caseForm = this.caseCreationFormGroup.getRawValue();
            this.loadFocusPersonCaseDetails(caseForm.subServiceType);
            this._commonHttpService
                .getArrayList({}, NewUrlConfig.EndPoint.Intake.NextnumbersUrl)
                .subscribe(result => {
                    const complaintTypeCase: ComplaintTypeCase = new ComplaintTypeCase();
                    complaintTypeCase.caseID = result['nextNumber'];
                    const selectedPurpose = this.getSelectedPurpose(
                        caseForm.serviceType
                    );
                    const selectedSubtype = this.getSelectedSubtype(
                        caseForm.subServiceType
                    );
                    complaintTypeCase.serviceTypeID = caseForm.serviceType;
                    complaintTypeCase.serviceTypeValue =
                        selectedPurpose.description;
                    complaintTypeCase.subServiceTypeID =
                        selectedSubtype.servicerequestsubtypeid;
                    complaintTypeCase.subSeriviceTypeValue =
                        selectedSubtype.classkey;
                    complaintTypeCase.complaintids = caseForm.complaintids;
                    complaintTypeCase.isyouthincustody = caseForm.isyouthincustody;
                    complaintTypeCase.isYouthinCustodyShow = this.isReqdueWarrant;
                    complaintTypeCase.showICJFields = this.showICJFields;
                    complaintTypeCase.showLawFileds = this.showLawFileds;
                    complaintTypeCase.choosenAllegation = this.caseCreationFormGroup.value.selectedAllegations.map(allegationId => {
                        const allegation = this.findAllegationById(allegationId);
                        return {
                            allegationID: allegation.allegationid,
                            allegationValue: allegation.name,
                            indicators: []
                        };
                    });

                    this.createdCases.push(complaintTypeCase);
                    this.updateCreatedCases();
                    this.resetCaseCreationForm();

                });
        }
    }

    loadFocusPersonCaseDetails(subServiceType) {
        if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.INTERSTATE_COMPACT)) {
            const selectedsubtype = this.getSelectedSubtype(subServiceType);
            this.notes = 'Notes';
            this.showICJFields = false;
            this.icjHeading = selectedsubtype.description + ' details';
            if (MyNewintakeConstants.Intake.ICJRequisitionDuetoWarrant === selectedsubtype.classkey) {
                this.isReqdueWarrant = true;
                this.showICJFields = true;
                this.notes = 'Content of Requisition Form';
                this.loadICJDropdownLists();
                this.getRequisitionType();
            } else {
                this.isReqdueWarrant = false;
            }
            if (MyNewintakeConstants.Intake.ICJReceivingProbation === selectedsubtype.classkey) {
                this.AllegationsPlaceholder = 'Adjudicated Offense';
                this.showICJFields = true;
                this.loadICJDropdownLists();
            }
            if (MyNewintakeConstants.Intake.ICJReentry === selectedsubtype.classkey) {
                this.AllegationsPlaceholder = 'Adjudicated Offense';
                this.showICJFields = true;
                this.loadICJDropdownLists();
            }
        }
        if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.REFERRAL.LAW_ENFORCEMENT)) {
            this.showLawFileds = true;
            this.notes = 'Notes';
            this.deliquenttypes$ = Observable.empty();
            const LawEnforcementFormdata = this._datastore.getData(IntakeStoreConstants.FocuspersonCaseDetails);
            if (LawEnforcementFormdata) {
                this.isinNCIC = LawEnforcementFormdata.iswritwarrant;
                this.warranttypemodel = LawEnforcementFormdata.warranttype === 'Delinquent' ? true : false;
                this.LawEnforcementFormGroup.patchValue(LawEnforcementFormdata);
                if (LawEnforcementFormdata.warranttype) {
                    this.onWarrentTypeChange(LawEnforcementFormdata.warranttype);
                    if (LawEnforcementFormdata.warranttypedetails && LawEnforcementFormdata.warranttypedetails.length > 0) {
                        const warrantdetails = LawEnforcementFormdata.warranttypedetails.map(res => {
                            return res.warranttypekey;
                        });
                        setTimeout(() => {
                            this.LawEnforcementFormGroup.patchValue({ warranttypedetails: warrantdetails });
                        }, 500);
                    }
                }
            } else {
                this.LawEnforcementFormGroup.patchValue({ iswritwarrant: false });
            }
        }
    }

    deleteCase(intakeCase: ComplaintTypeCase) {
        this.deleteCaseID = intakeCase.caseID;
    }

    onEditCase(intakeCase: ComplaintTypeCase) {
        this.caseEditFormGroup.patchValue({
            serviceType: intakeCase.serviceTypeID,
            subServiceType: intakeCase.subServiceTypeID,
            isyouthincustody: intakeCase.isyouthincustody
        });
        this.caseEditFormGroup.controls['serviceType'].disable();
        this.isReqdueWarrant = intakeCase.isYouthinCustodyShow;
        this.showICJFields = intakeCase.showICJFields;
        this.showLawFileds = intakeCase.showLawFileds;
        this.editCase = intakeCase;
    }

    changeSubTypeEdit(value) {
        const selectedsubtype = this.getSelectedSubtype(value);
        if (MyNewintakeConstants.Intake.ICJRequisitionDuetoWarrant === selectedsubtype.classkey) {
            this.isReqdueWarrant = true;
        } else {
            this.isReqdueWarrant = false;
            this.caseEditFormGroup.patchValue({
                isyouthincustody: false
            });
        }
    }

    updateCreatedCases() {
        this._datastore.setData(IntakeStoreConstants.createdCases, this.createdCases);
    }
    isUpdateDisabled() {
        // if (this.editCase) {
        //     if (this.caseEditFormGroup.value.subServiceType === this.editCase.subServiceTypeID &&
        //         this.caseEditFormGroup.value.isyouthincustody === this.editCase.isyouthincustody) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // }
        return false;
    }
    updateSubTypes() {
        if (this.caseEditFormGroup.valid) {
            if (this.isSubTypeExists(this.caseEditFormGroup.value.subServiceType)) {
                this._alertService.error('Already Case created for this sub type ');
                return false;
            }

            this.createdCases.find(complaintTypeCase => {
                if (complaintTypeCase.caseID === this.editCase.caseID) {
                    const selectedPurpose = this.getSelectedPurpose(
                        this.caseEditFormGroup.getRawValue().serviceType
                    );
                    const selectedSubtype = this.getSelectedSubtype(
                        this.caseEditFormGroup.value.subServiceType
                    );
                    complaintTypeCase.serviceTypeID = this.caseEditFormGroup.getRawValue().serviceType;
                    complaintTypeCase.serviceTypeValue =
                        selectedPurpose.description;
                    complaintTypeCase.subServiceTypeID =
                        selectedSubtype.servicerequestsubtypeid;
                    complaintTypeCase.subSeriviceTypeValue =
                        selectedSubtype.classkey;
                    complaintTypeCase.isYouthinCustodyShow = this.isReqdueWarrant;
                    complaintTypeCase.showICJFields = this.showICJFields;
                    complaintTypeCase.showLawFileds = this.showLawFileds;
                    complaintTypeCase.isyouthincustody = this.caseEditFormGroup.value.isyouthincustody;
                    this._alertService.success('Case updated successfully ');
                    $('#editClose').click();
                    return true;
                }
            });
        } else {
            this._alertService.error('Please fill required fields ');
        }
    }

    deleteCaseConfirm() {
        this.createdCases = this.createdCases.filter(
            createdCase => createdCase.caseID !== this.deleteCaseID
        );
        this.updateCreatedCases();
        (<any>$('#delete-case-popup')).modal('hide');
    }

    isPeaceOrder(complaintCase: ComplaintTypeCase): boolean {
        return complaintCase.subSeriviceTypeValue === 'Peace Order';
    }

    loadAllegation(caseIndex: number, daTypeKey: string) {
        this.selectedcaseIndex = caseIndex;
        this.choosenAllegation = new ChoosenAllegation();

        this.allegationItems = [];
        this.allegationDropDownItems = [];
        this.filteredAllegationItems = [];
        this.caseCreationFormGroup.patchValue({
            allegationId: ''
        });
        const url =
            NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter';
        this._commonHttpService
            .getArrayList(
                {
                    where: { intakeservreqtypeid: daTypeKey },
                    method: 'get',
                    order: 'name',
                    nolimit: true
                },
                url
            )
            .subscribe(result => {
                this.allegationItems = result.map(
                    item => new AllegationItem(item)
                );
                this.allegationDropDownItems = this.allegationItems.map(
                    item =>
                        new DropdownModel({
                            text: item.allegationname,
                            value: item.allegationid
                        })
                );
            });
    }

    allegationOnChange(option: any) {
        this.loadSelectedAllegation(option.value);
        this.choosenAllegation.allegationID = option.value;
        this.choosenAllegation.allegationValue = option.text;
    }

    loadSelectedAllegation(selectedAllegation: string) {
        this.filteredAllegationItems = this.allegationItems.filter(item => {
            return item.allegationid === selectedAllegation;
        });
        // console.log(this.filteredAllegationItems[0]);
    }

    indicatorsChecked(model: any, control) {
        if (!this.choosenAllegation) {
            this.choosenAllegation = new ChoosenAllegation();
        }
        const Index = this.choosenAllegation.indicators.findIndex(indicator => indicator.indicatorid === model.indicatorid);
        if (Index === -1) {
            this.choosenAllegation.indicators.push(model);
        } else {
            this.choosenAllegation.indicators.splice(Index, 1);
        }

    }

    saveAllegations() {
        let ispresent = false;
        this.createdCases[this.selectedcaseIndex].choosenAllegation.forEach(
            element => {
                if (
                    this.choosenAllegation.allegationID === element.allegationID
                ) {
                    element.indicators = this.choosenAllegation.indicators;
                    ispresent = true;
                }
            }
        );
        if (!ispresent) {
            this.createdCases[this.selectedcaseIndex].choosenAllegation.push(
                this.choosenAllegation
            );
        }
    }


    deleteAllegation(caseIndex: number, aligationIndex: number) {
        // console.log(caseIndex, aligationIndex);
        this.createdCases[caseIndex].choosenAllegation.splice(
            aligationIndex,
            1
        );
    }

    openGroupSetup() {
        if (
            this.groupSetupForm.value.DAItems &&
            this.groupSetupForm.value.DAItems.length >= 2 &&
            this.createdCases.filter(item => !item.GroupNumber).length >= 2
        ) {
            (<any>$('#groupReason')).modal('show');
        } else {
            this._alertService.error('Please select at least 2 Case');
        }
    }

    cancelGroupSetup() {
        this.groupDAReasonForm.reset();
        this.groupSetupForm.reset();
        (<any>$('#groupReason')).modal('hide');
    }

    saveGroupSetup() {
        if (
            this.groupSetupForm.value.DAItems &&
            (this.groupDAReasonForm.valid && this.groupDAReasonForm.dirty)
        ) {
            this.groupSelectedDAIds = this.groupSetupForm.value.DAItems;
            this._commonHttpService
                .getArrayList(
                    {},
                    'Nextnumbers/getNextNumber?apptype=GroupAuthorizationNumber'
                )
                .subscribe(result => {
                    this.createdCases = this.createdCases.map(item => {
                        this.groupSelectedDAIds.map(da => {
                            if (item.caseID === da) {
                                item.GroupNumber = result['nextNumber'];
                                item.GroupReasonType = this.groupDAReasonForm.value.reasonType;
                                item.GroupComment = this.groupDAReasonForm.value.reasonComments;
                            }
                        });
                        return item;
                    });
                    this.updateCreatedCases();
                    this.cancelGroupSetup();
                    return { nextNumber: result };
                });
        } else {
            this._alertService.error('Please select Grouping reason type');
        }
    }

    unGroupDAItems() {
        if (this.groupSetupForm.value.selectedDAItemsGroupNo) {
            this.ungroupSelectedGroupIds = this.groupSetupForm.value.selectedDAItemsGroupNo;
            this.createdCases = this.createdCases.map(item => {
                this.ungroupSelectedGroupIds.map(da => {
                    if (item.GroupNumber === da) {
                        item.GroupNumber = '';
                        item.GroupReasonType = '';
                        item.GroupComment = '';
                    }
                });
                return item;
            });
            this.updateCreatedCases();
        }
    }


    removeAllegation(allegationToRemove: any): void {
        this.selectedAllegations = this.selectedAllegations.filter(allegation => allegation.allegationid !== allegationToRemove.allegationid);
        this.loadAllegationSubtypes();
    }

    allegationSelected(event: MatAutocompleteSelectedEvent): void {

        if (!this.isAllegationExist(event.option.value)) {
            this.selectedAllegations.push(event.option.value);
            this.loadAllegationSubtypes();
        }
        this.allegationInput.nativeElement.value = '';
        this.allegationCtrl.setValue(null);
    }

    isAllegationExist(searchAllegation: any) {
        return this.selectedAllegations.find(allegation => allegation.allegationid === searchAllegation.allegationid) ? true : false;
    }

    isIndicatorExist(searchIndicator: any) {
        const Index = this.indicators.findIndex(indicator => indicator.indicatorid === searchIndicator.indicatorid);
        return Index !== -1 ? true : false;
    }

    editAllegation(intakeservreqtypeid: string, allegation: any) {
        this.allegation = allegation;
        this.choosenAllegation = new ChoosenAllegation();
        this.choosenAllegation.allegationID = allegation.allegationID;
        this.choosenAllegation.allegationValue = allegation.allegationValue;
        this.choosenAllegation.indicators = allegation.indicators;
        // console.log(allegation);
        this.indicators = allegation.indicators;
        const checkInput = {
            where: { intakeservreqtypeid: intakeservreqtypeid },
            method: 'get',
            order: 'name',
            nolimit: true
        };
        this.selectedAllegation$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest(checkInput),
                NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter'
            )
            .map(result => {
                this.allegations = result;
                return result;
            });
        this.selectedAllegation$ = this.selectedAllegation$.map(allegations => {
            return allegations.filter(algn => algn.allegationid === allegation.allegationID);
        });

    }

    resetSelectedAllegations() {
        this.selectedAllegations = [];
    }

    resetCaseCreationForm() {
        this.resetSelectedAllegations();
        this.caseCreationFormGroup.patchValue({
            subServiceType: null, selectedAllegations: [[]],
            isyouthincustody: false,
        });
    }

    private _filter(value: any): any[] {

        if (value && value !== '') {
            const filterValue = value.toLowerCase();
            return this.allegations.filter(allegation => allegation.name.toLowerCase().indexOf(filterValue) !== -1);
        } else {
            return this.allegations;
        }
    }



    private listenAllegationList() {
        this.loadSelectedAllegationList();

    }


    private listenSelectedAllegationList() {
        this.evaluationFields = this.store[IntakeStoreConstants.evalFields];
        this.loadSelectedAllegationList();

    }

    private loadSelectedAllegationList() {
        if (this.evaluationFields && this.evaluationFields.length > 0) {
            const allComplaintIds = [];
            this.evaluationFields.forEach(evalField => {
                allComplaintIds.push(evalField.complaintid);
            });
            this.complaintIds = allComplaintIds;

        }

    }

    loadOffenses() {
        const caseCreationForm = this.caseCreationFormGroup.getRawValue();
        const selectedComplaintIds = caseCreationForm.complaintids;
        let selectedOffenses = [];
        if (selectedComplaintIds && selectedComplaintIds.length > 0) {
            this.evaluationFields.forEach(evalFeild => {

                if (selectedComplaintIds.indexOf(evalFeild.complaintid) !== -1) {
                    if (evalFeild.allegedoffense && evalFeild.allegedoffense.length > 0) {
                        selectedOffenses = [...selectedOffenses, ...evalFeild.allegedoffense];
                    }
                }
            });
        }
        if (selectedOffenses.length > 0) {
            const selectedAllegations = this.offenceCategories.filter(allegation => {
                const found = selectedOffenses.indexOf(allegation.allegationid);
                if (found === - 1) {
                    return false;
                } else {
                    return true;
                }

            });
            this.caseCreationFormGroup.patchValue({ selectedAllegations: selectedOffenses });
            this.selectedAllegationList$ = Observable.of(selectedAllegations);
            this.loadAllegationSubtypes();
        }
        console.log('so', selectedOffenses);
    }

    private findAllegationById(allegationID) {
        return this.offenceCategories.find(allegation => allegation.allegationid === allegationID);

    }

    onAllegationWindow(windowOpened) {
        if (!this.isCaseSubTypeDependancy) {
            if (!windowOpened) {
                this.loadAllegationSubtypes();
            }
        }
    }


}

