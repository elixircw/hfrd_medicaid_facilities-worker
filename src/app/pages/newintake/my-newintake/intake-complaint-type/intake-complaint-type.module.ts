import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeComplaintTypeRoutingModule } from './intake-complaint-type-routing.module';
import { IntakeComplaintTypeComponent } from './intake-complaint-type.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    IntakeComplaintTypeRoutingModule,
    QuillModule
  ],
  declarations: [IntakeComplaintTypeComponent]
})
export class IntakeComplaintTypeModule { }
