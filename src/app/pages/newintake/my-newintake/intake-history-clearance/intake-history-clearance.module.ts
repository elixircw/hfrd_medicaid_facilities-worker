import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeHistoryClearanceRoutingModule } from './intake-history-clearance-routing.module';
import { IntakeHistoryClearanceComponent } from './intake-history-clearance.component';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { MatCheckboxModule } from '@angular/material';
import { DatepickerModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    IntakeHistoryClearanceRoutingModule,
    SharedDirectivesModule,
    MatCheckboxModule,
    DatepickerModule,
    FormMaterialModule
  ],
  declarations: [IntakeHistoryClearanceComponent]
})
export class IntakeHistoryClearanceModule { }
