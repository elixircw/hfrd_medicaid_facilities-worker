import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaskModule } from 'ngx-mask';
import { PopoverModule } from 'ngx-popover';
import { NewintakeNarrativeRoutingModule } from './newintake-narrative-routing.module';
import { NewintakeNarrativeComponent } from './newintake-narrative.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { QuillModule } from 'ngx-quill';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    SharedDirectivesModule,
    CommonModule,
    NewintakeNarrativeRoutingModule,
    FormsModule,
    FormMaterialModule,
    PopoverModule,
    NgxMaskModule.forRoot(),
    ControlMessagesModule,
    SharedDirectivesModule,
    QuillModule

  ],
  declarations: [NewintakeNarrativeComponent]
})
export class NewintakeNarrativeModule { }
