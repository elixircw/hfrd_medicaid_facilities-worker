import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    Input,
    NgZone,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';
import { ActivatedRoute, Router } from '@angular/router';
import { Popover } from 'ngx-popover';
import { NgxfUploaderService } from 'ngxf-uploader';
import { ControlUtils } from '../../../../@core/common/control-utils';

import { AppUser } from '../../../../@core/entities/authDataModel';
import { DynamicObject, DropdownModel } from '../../../../@core/entities/common.entities';
import { REGEX } from '../../../../@core/entities/constants';
import { DataStoreService, GenericService, CommonDropdownsService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService } from '../../../../@core/services/auth.service';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import { ActionContext } from '../../../../shared/modules/web-speech/shared/model/strategy/action-context';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { NewUrlConfig } from '../../newintake-url.config';
import { AttachmentUpload, Narrative, ResourcePermission, SuggestAddress, ReviewStatus } from '../_entities/newintakeModel';
import { IntakeStoreConstants, MyNewintakeConstants } from '../my-newintake.constants';

import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { IntakeUtils } from '../../../_utils/intake-utils.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { IntakeConfigService } from '../intake-config.service';
import * as moment from 'moment';
import { PersonInfoService } from '../../../shared-pages/person-info/person-info.service';


declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'newintake-narrative',
    templateUrl: './newintake-narrative.component.html',
    styleUrls: ['./newintake-narrative.component.scss', '../../../../../styles/trumbowyg.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NewintakeNarrativeComponent implements OnInit, AfterViewInit, OnDestroy {
    // @Input()
    // narrativeInputSubject$ = new Subject<Narrative[]>();
    // @Input()
    // intakeNumberNarrative: string;
    // @Input()
    // narrativeOutputSubject$ = new Subject<Narrative>();
    // @Input()
    // draftId: string;
    // @Input()
    // finalNarrativeText$ = new Subject<string>();
    @Input()
    // reviewStatus$ = new Subject<string>();
    // tslint:disable-next-line:no-input-rename
    // @Input('purposeStatus')
    // purposeStatus$ = new Subject<string>();
    // @Input()
    // purposeSubject$: Subject<string>;
    // @Input()
    // zipCodeSubject$: Subject<number>;
    // @Input()
    // agencyType$: Subject<string>;
    // @Input()
    // general$: Subject<General>;
    CountyValuesDropdownItems$: Observable<DropdownModel[]>;
    stateValuesDropdownItems$: Observable<DropdownModel[]>;
    offenceLocation: number;
    audioCollection: AttachmentUpload[] = [];
    intakeNarrativeForm: FormGroup;
    addIdentifiedFormGroup: FormGroup;
    speechRecogninitionOn: boolean;
    isCPRPurposeSelected: boolean;
    speechData: string;
    notification: string;
    finalTranscript = '';
    recognizing = false;
    roleName: AppUser;
    addressAnalysis = [];
    actionContext: ActionContext = new ActionContext();
    currentLanguage: string;
    firstNameControlName: AbstractControl;
    middleNameControlName: AbstractControl;
    lastNameControlName: AbstractControl;
    ZipCodeControlName: AbstractControl;
    suggestedAddress$: Observable<any[]>;
    maxDate = new Date();
    tooltip: string;
    @ViewChild('myPopover')
    myPopover: Popover;
    @ViewChild('clearancePopover')
    clearancePopover: Popover;
    showRequesterDetails: boolean;
    agencyType: string;
    showZipCode = false;
    selectedPurpose: string;
    stateId: string;
    Countydropdown: boolean;
    reviewstatus: ReviewStatus = new ReviewStatus();
    quillToolbar: {
        toolbar: (
            | string[]
            | { header: number }[]
            | { list: string }[]
            | { script: string }[]
            | { indent: string }[]
            | { direction: string }[]
            | { size: (string | boolean)[] }[]
            | { header: (number | boolean)[] }[]
            | ({ color: any[]; background?: undefined } | { background: any[]; color?: undefined })[]
            | { font: any[] }[]
            | { align: any[] }[])[];
    };
    store: DynamicObject;
    dataStroeSubscription: Subscription;
    baseUrl: string;
    addedIdentifiedPersons: any;
    agency: string;
    genderList: any[] = [];
    narrativeCont: any;
    cpsHistoryCont: any;
    reporterRoles: any[];
    currentNarrativeText: any;
    currentCPSHistoryText: any;
    narrativeUpdatedDate: Date;
    currentStatus: string;
    isClosed: Boolean;
    constructor(
        private formBuilder: FormBuilder,
        private _commonDropdownService:CommonDropdownsService,
        private speechRecognizer: SpeechRecognizerService,
        private _alertService: AlertService,
        private _changeDetect: ChangeDetectorRef,
        private _uploadService: NgxfUploaderService,
        private _authService: AuthService,
        private route: ActivatedRoute,
        private _speechRecognitionService: SpeechRecognitionService,
        private _resourceService: GenericService<ResourcePermission>,
        private zone: NgZone,
        private _storeService: DataStoreService,
        private _httpService: CommonHttpService,
        private intakeUtils: IntakeUtils,
        private _intakeConfig: IntakeConfigService,
        private _commonHttpService: CommonHttpService,
        private _router: Router
    ) {
        this.speechRecogninitionOn = false;
        this.speechData = '';
        this.store = this._storeService.getCurrentStore();
    }

    ngOnInit() {
        this.agency = this._authService.getAgencyName();
        this.quillToolbar = {
            toolbar: [
                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                ['blockquote', 'code-block'],

                [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
                [{ 'direction': 'rtl' }],                         // text direction

                [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                [{ 'font': [] }],
                [{ 'align': [] }],

                ['clean'],                                         // remove formatting button

                //   ['link', 'image', 'video']                         // link and image, video
            ]
        };
        // this.myPopover.show();
        this.narrativeForm();
        this.currentLanguage = 'en-US';
        this.speechRecognizer.initialize(this.currentLanguage);
        this.notification = null;
        //this.getCountyDropdown();
        this.getStateDropdown();
        this.getGenderList();
        this.getReporterRolesList();

        this.roleName = this._authService.getCurrentUser();
        $('#divNonEditNarrative').hide();
        $('#divEditNarrative').show();
        // this.reviewStatus$.subscribe((res) => {
        const reviewStatus = this.store[IntakeStoreConstants.reviewStatus];
        if (this.roleName.role.name === 'apcs' && (reviewStatus === 'Review' || reviewStatus === 'Reopen' || reviewStatus === 'Accepted' || reviewStatus === 'Closed')) {
            $('#divNonEditNarrative').show();
            $('#divEditNarrative').hide();
            this.intakeNarrativeForm.controls.IsAnonymousReporter.disable();
            this.intakeNarrativeForm.controls.IsUnknownReporter.disable();
            this.intakeNarrativeForm.controls.Firstname.disable();
            this.intakeNarrativeForm.controls.MiddleName.disable();
            this.intakeNarrativeForm.controls.Lastname.disable();
            this.intakeNarrativeForm.controls.PhoneNumber.disable();
            this.intakeNarrativeForm.controls.PhoneNumberExt.disable();
            this.intakeNarrativeForm.controls.ZipCode.disable();
            this.intakeNarrativeForm.controls.RefuseToShareZip.disable();
            this.intakeNarrativeForm.controls.offenselocation.disable();
            this.intakeNarrativeForm.controls.isacknowledgementletter.disable();
        }
        // });
        /* this.intakeNarrativeForm.get('Narrative').valueChanges.subscribe((text) => {
            this.finalTranscript = text;
            console.log('Narrative valueChanges', this.intakeNarrativeForm.getRawValue());
            this._storeService.setData(IntakeStoreConstants.addNarrative, this.intakeNarrativeForm.getRawValue());
            // this.narrativeInputSubject$.next(this.intakeNarrativeForm.getRawValue());
            // this.finalNarrativeText$.next(this.finalTranscript);
            this._storeService.setData(IntakeStoreConstants.narrativeText, this.finalTranscript);
        }); */

        this.intakeNarrativeForm.get('requesterstate').valueChanges.subscribe((result) => {
            if (result === 'MD') {
                this.Countydropdown = true;
            } else {
                this.Countydropdown = false;
            }
        });
        this.intakeNarrativeForm.get('offenselocation').valueChanges.subscribe((zipCode) => {
            console.log('offenselocation valueChanges', this.intakeNarrativeForm.getRawValue());
            this._storeService.setData(IntakeStoreConstants.zipcode, zipCode);
            // this.zipCodeSubject$.next(zipCode);
        });

        this.intakeNarrativeForm.get('IsUnknownReporter').valueChanges.subscribe((IsUnknownReporter) => {
            this._storeService.setData(IntakeStoreConstants.isUnknownReporter, IsUnknownReporter);
        });

        this.agencyType = this.store[IntakeStoreConstants.agency];
        // this.agencyType$.subscribe((data) => {
        //     this.agencyType = data;
        // });

        // this.general$.subscribe((data) => {
        const general = this.store[IntakeStoreConstants.general];
        if (general) {
            this.intakeNarrativeForm.patchValue({
                offenselocation: general.offenselocation
            });
        }
        // });

        this.firstNameControlName = this.intakeNarrativeForm.get('Firstname');
        this.middleNameControlName = this.intakeNarrativeForm.get('Middlename');
        this.lastNameControlName = this.intakeNarrativeForm.get('Lastname');
        this.ZipCodeControlName = this.intakeNarrativeForm.get('ZipCode');
        // if (this.draftId !== '0') {
        if (this.store[IntakeStoreConstants.intakenumber] !== '0') {
            this.narrativeTooltip();
            const narrative = this.store[IntakeStoreConstants.addNarrative] ? this.store[IntakeStoreConstants.addNarrative] : {};
            // console.log('patch narrative value', narrative);
            // narrative.value.Narrative = decodeURI(narrative.value.Narrative);
            // this.intakeNarrativeForm.value.Narrative = decodeURI(this.intakeNarrativeForm.value.Narrative);
            narrative.incidentdate = narrative.incidentdate ? new Date(narrative.incidentdate) : '';
            this.intakeNarrativeForm.patchValue(narrative);
            if (narrative.requesterstate) {
                this.loadCounty(narrative.requesterstate);
            }
            this.narrativeCont = narrative.Narrative;
            this.cpsHistoryCont = narrative.cpsHistoryClearance;
            this.narrativeUpdatedDate = narrative.narrativeUpdatedDate ? narrative.narrativeUpdatedDate : null;
            if (this.narrativeCont) {
                this.narrativeCont = this.narrativeCont.replace(/''/g, `'`);
                this.intakeNarrativeForm.patchValue({
                    Narrative: this.narrativeCont
                });
            }
            if (this.cpsHistoryCont) {
                this._storeService.setData('isCPSHistoryClearanceChecked', true);
                this.cpsHistoryCont = this.cpsHistoryCont.replace(/''/g, `'`);
                this.intakeNarrativeForm.patchValue({
                    cpsHistoryClearance: this.cpsHistoryCont
                });
            }
            else {
                this._storeService.setData('isCPSHistoryClearanceChecked', false);
            }
            if (narrative.IsUnknownReporter === true) {
                this.firstNameControlName.disable();
                this.middleNameControlName.disable();
                this.lastNameControlName.disable();
            }
            if (narrative.RefuseToShareZip === true) {
                this.ZipCodeControlName.disable();
            }
            // this.narrativeOutputSubject$.subscribe((data) => {
            //     this.intakeNarrativeForm.patchValue(data);
            //     if (data.IsUnknownReporter === true) {
            //         this.firstNameControlName.disable();
            //         this.lastNameControlName.disable();
            //     }
            //     if (data.RefuseToShareZip === true) {
            //         this.ZipCodeControlName.disable();
            //     }
            //     // this.narrativeInputSubject$.next(this.intakeNarrativeForm.getRawValue());
            // });
        }

        // });
        this.listenForPurposeChanges();
        this.detectFormValueChanges();
        this.addedIdentifiedPersons = this._storeService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
        this.currentStatus = this._storeService.getData(IntakeStoreConstants.INTAKE_STATUS);
        if (this.currentStatus === 'Closed') {
            this.intakeNarrativeForm.disable();
            this.addIdentifiedFormGroup.disable();
            this.isClosed = true;
           } else {
            this.intakeNarrativeForm.enable();
            this.addIdentifiedFormGroup.enable();
            this.isClosed = false;
           }
    }

    listenForPurposeChanges() {
        this.dataStroeSubscription = this._storeService.currentStore.subscribe(store => {
            // console.log('narrative ', store[IntakeStoreConstants.purposeSelected]);
            if (store[IntakeStoreConstants.purposeSelected]) {
                console.log('narrative constant', store[IntakeStoreConstants.addNarrative]);
                const purposeSelected = store[IntakeStoreConstants.purposeSelected];
                const storePurposeId = purposeSelected.value;
                if (storePurposeId !== this.selectedPurpose) {
                    this.selectedPurpose = storePurposeId;
                    // below line is resetting anonymous reporter and unknown reporter on page init
                    // this.resetAnoymousorUnknownUser();
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.CHILD_PROTECTION_SERVICES)) {
                        this.isCPRPurposeSelected = true;
                    } else {
                        this.isCPRPurposeSelected = false;
                    }
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.INFORMATION_AND_REFERRAL)) {
                        this.showRequesterDetails = true;
                        this.firstNameControlName.enable();
                        this.middleNameControlName.enable();
                        this.lastNameControlName.enable();
                        this.intakeNarrativeForm.patchValue({IsUnknownReporter : false});
                    } else {
                        this.showRequesterDetails = false;
                    }
                    if (this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.INFORMATION_AND_REFERRAL)
                        // || this._intakeConfig.selectedPurposeIs(MyNewintakeConstants.PURPOSE.REQUEST_FOR_SERVICES)
                    ) {
                        this.showZipCode = false;
                    } else {
                        this.showZipCode = true;
                    }

                }
            }

        });
    }

    detectFormValueChanges() {
        this.intakeNarrativeForm.valueChanges.subscribe(data => {
            this.intakeNarrativeForm.get('Narrative').valueChanges.subscribe((text) => { this.speechData = text; });
            // this.intakeNarrativeForm.validateAll();

            ControlUtils.validateAllFormFields(this.intakeNarrativeForm);
            this._storeService.setData(IntakeStoreConstants.addNarrative, this.intakeNarrativeForm.getRawValue());
            console.log('detectFormValueChanges narrative', this._storeService.getData(IntakeStoreConstants.addNarrative));
        });
    }
    ngAfterViewInit() {
        const self = this;
        this.zone.run(() => {
            $('.trumbowyg-textarea')
                .trumbowyg()
                .on('tbwfocus', function () {
                    self.openPopover();
                });
        });
    }
    loadCounty(state) {
        this._commonDropdownService.getPickListByMdmcode(state).subscribe(countyList => {
          this.CountyValuesDropdownItems$ = Observable.of(countyList);
        });
      }
    getCountyDropdown() {
        this.CountyValuesDropdownItems$ = this._httpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                'admin/county?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyname
                        })
                );
            });
    }
    getStateDropdown() {
        this.stateValuesDropdownItems$ = this._httpService
            .getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                'States?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.statename,
                            value: res.stateabbr
                        })
                );
            });
    }
    openPopover() {
        this.myPopover.show();
    }
    closePopover() {
        this.myPopover.hide();
    }
    openClearancePopover() {
        this.clearancePopover.show();
    }
    closeClearancePopover() {
        this.clearancePopover.hide();
    }
    private narrativeForm() {
        this.intakeNarrativeForm = this.formBuilder.group({
            Firstname: ['', [Validators.compose([Validators.pattern('^[^0-9]*$')])]],
            Middlename: [''],
            Lastname: ['', [Validators.compose([Validators.pattern('^[^0-9]*$')])]],
            // Lastname: ['',[Validators.compose([Validators.pattern('^[a-zA-Z]*$')])]],
            Narrative: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR, Validators.maxLength(6000)]],
            cpsHistoryClearance: ['', [Validators.required, REGEX.NOT_EMPTY_VALIDATOR, Validators.maxLength(6000)]],
            Role: ['Rep'],
            IsAnonymousReporter: false,
            IsUnknownReporter: false,
            PhoneNumber: [''],
            PhoneNumberExt: [''],
            requesteraddress1: [null],
            requesteraddress2: [null],
            requestercity: [null],
            requesterstate: [null],
            requestercounty: [null],
            isacknowledgementletter: true,
            ZipCode: [''],
            RefuseToShareZip: false,
            offenselocation: [''],
            incidentlocation: [''],
            incidentdate: ['', Validators.required],
            isapproximate: false,
            email: [''],
            reporterrole: [''],
            organization: [null],
            title: [null],
        });
        this.addIdentifiedFormGroup = this.formBuilder.group({
            firstname: [''],
            lastname: [''],
            dob: [''],
            gender: [''],
            ssn: [''],
            age: [''],
        });
    }
    onChange(event) {
        if (!this.intakeNarrativeForm.get('Firstname').valid) {
            this.intakeNarrativeForm.get('Firstname').setValue('');
        }
        if (!this.intakeNarrativeForm.get('Lastname').valid) {
            this.intakeNarrativeForm.get('Lastname').setValue('');
        }
        console.log('onchange event', event);
        this._storeService.setData(IntakeStoreConstants.addNarrative, this.intakeNarrativeForm.getRawValue());
        // this.narrativeInputSubject$.next(this.intakeNarrativeForm.getRawValue());
    }
    resetAnoymousorUnknownUser() {
        this.intakeNarrativeForm.get('IsUnknownReporter').setValue(null);
        this.intakeNarrativeForm.get('IsAnonymousReporter').setValue(null);
    }
    isAnoymousorUnknownUser() {
        const IsUnknownReporter = this.intakeNarrativeForm.getRawValue().IsUnknownReporter;
        const IsAnonymousReporter = this.intakeNarrativeForm.getRawValue().IsAnonymousReporter;
        if (IsUnknownReporter || IsAnonymousReporter) {
            return true;
        } else {
            return false;
        }
    }

    changeReport(event: any, type: string) {
        if (type === 'Anonymous') {
            if (event.target.checked) {
                this.intakeNarrativeForm.value.IsAnonymousReporter = true;
                this.intakeNarrativeForm.value.IsUnknownReporter = false;
                // this.firstNameControlName.disable();
                // this.lastNameControlName.disable();
                this.firstNameControlName.enable();
                this.middleNameControlName.enable();
                this.lastNameControlName.enable();
                this.intakeNarrativeForm.patchValue({
                    Role: '',
                    organization: '',
                    title: '',
                    // Firstname: '',
                    // Lastname: '',
                    IsUnknownReporter: false
                });
            } else {
                this.firstNameControlName.enable();
                this.middleNameControlName.enable();
                this.lastNameControlName.enable();
                this.intakeNarrativeForm.patchValue({
                    Role: 'Rep'
                });
                this.intakeNarrativeForm.value.IsAnonymousReporter = false;
            }
        } else if (type === 'Unknown') {
            if (event.target.checked) {
                this.intakeNarrativeForm.value.IsUnknownReporter = true;
                this.intakeNarrativeForm.value.IsAnonymousReporter = false;
                this.firstNameControlName.disable();
                this.middleNameControlName.disable();
                this.lastNameControlName.disable();
                this.intakeNarrativeForm.patchValue({
                    Role: '',
                    Firstname: '',
                    Middlename: '',
                    Lastname: '',
                    organization: '',
                    title: '',
                    IsAnonymousReporter: false
                });
            } else {
                this.firstNameControlName.enable();
                this.middleNameControlName.enable();
                this.lastNameControlName.enable();
                this.intakeNarrativeForm.patchValue({
                    Role: '',
                    Firstname: '',
                    Middlename: '',
                    Lastname: '',
                    organization: '',
                    title: '',
                    IsAnonymousReporter: false
                });
                this.intakeNarrativeForm.value.IsUnknownReporter = false;
            }
        } else if (type === 'RefuseZip') {
            if (event.target.checked) {
                this.ZipCodeControlName.disable();
                this.intakeNarrativeForm.patchValue({
                    ZipCode: ''
                });
            } else {
                this.ZipCodeControlName.enable();
                this.intakeNarrativeForm.patchValue({
                    ZipCode: ''
                });
            }
        }
        if (type === 'Unknown' && event.target.checked) {
            this.intakeNarrativeForm['controls'].isacknowledgementletter.setValue(false);
            this.intakeNarrativeForm['controls'].isacknowledgementletter.disable();
        } else {
            this.intakeNarrativeForm['controls'].isacknowledgementletter.enable();
        }
        console.log('change report', this.intakeNarrativeForm.getRawValue());
        this._storeService.setData(IntakeStoreConstants.addNarrative, this.intakeNarrativeForm.getRawValue());
        // this.narrativeInputSubject$.next(this.intakeNarrativeForm.getRawValue());
    }
    ngOnDestroy() {
        this.checkForNarrativeContent();
        this.checkForCPSHistoryContent();
        this._speechRecognitionService.destroySpeechObject();
        // (<any>$('#show-myModal')).modal('show');
    }

    activateSpeechToText(): void {
        this.recognizing = true;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
            let tempNarrative = '';
            this._speechRecognitionService.record().subscribe(
                // listener
                (value) => {
                    console.log(value);
                    tempNarrative = this.speechData.concat(value);
                    this.intakeNarrativeForm.patchValue({ Narrative: tempNarrative });
                },
                // errror
                (err) => {
                    console.log(err);
                    this.recognizing = false;
                    if (err.error === 'no-speech') {
                        this.notification = `No speech has been detected. Please try again.`;
                        this._alertService.warn(this.notification);
                        this.activateSpeechToText();
                    } else if (err.error === 'not-allowed') {
                        this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    } else if (err.error === 'not-microphone') {
                        this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    }
                },
                // completion
                () => {
                    this.speechRecogninitionOn = true;
                    console.log('--complete--');
                    this.activateSpeechToText();
                }
            );
        } else {
            this.recognizing = false;
            this.deActivateSpeechRecognition();
        }
    }
    deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
    }
    getSuggestedAddress() {
        if (this.intakeNarrativeForm.value.requesteraddress1 &&
            this.intakeNarrativeForm.value.requesteraddress1.length >= 3) {
            this.suggestAddress();
        }
    }

    suggestAddress() {
        this._commonHttpService
            .getArrayListWithNullCheck(
                {
                    method: 'post',
                    where: {
                        prefix: this.intakeNarrativeForm.value.requesteraddress1,
                        cityFilter: '',
                        stateFilter: '',
                        geolocate: '',
                        geolocate_precision: '',
                        prefer_ratio: 0.66,
                        suggestions: 25,
                        prefer: 'MD'
                    }
                },
                NewUrlConfig.EndPoint.Intake.SuggestAddressUrl
            ).subscribe(
                (result: any) => {
                    if (result.length > 0) {
                        this.suggestedAddress$ = result;
                    }
                }
            );
    }
    selectedAddress(model) {
        this.intakeNarrativeForm.patchValue({
            requesteraddress1: model.streetLine ? model.streetLine : '',
            requestercity: model.city ? model.city : '',
            requesterstate: model.state ? model.state : ''
        });
        const addressInput = {
            street: model.streetLine ? model.streetLine : '',
            street2: '',
            city: model.city ? model.city : '',
            state: model.state ? model.state : '',
            zipcode: '',
            match: 'invalid'
        };
        this._commonHttpService
            .getSingle(
                {
                    method: 'post',
                    where: addressInput
                },
                NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
            )
            .subscribe(
                (result) => {
                    if (result[0].analysis) {
                        this.intakeNarrativeForm.patchValue({
                            offenselocation: result[0].components.zipcode ? result[0].components.zipcode : ''
                        });
                        this.loadCounty(this.intakeNarrativeForm.value.state);
                        if (result[0].metadata.countyName) {
                            this._commonHttpService.getArrayList(
                                {
                                    nolimit: true,
                                    where: { referencetypeid: 306, mdmcode: this.intakeNarrativeForm.value.state, description: result[0].metadata.countyName }, method: 'get'
                                },
                                'referencevalues?filter'
                            ).subscribe(
                                (resultresp) => {
                                    if (resultresp[0]) {
                                        this.intakeNarrativeForm.patchValue({
                                            requestercounty: resultresp[0].ref_key
                                        });
                                    }
                                }
                            );
                        }
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    validateAddressResponse() {
        const addressInput = {
            street: this.intakeNarrativeForm.value.requesteraddress1 ? this.intakeNarrativeForm.value.requesteraddress1 : '',
            street2: this.intakeNarrativeForm.value.requesteraddress2 ? this.intakeNarrativeForm.value.requesteraddress2 : '',
            city: this.intakeNarrativeForm.value.City ? this.intakeNarrativeForm.value.City : '',
            state: this.intakeNarrativeForm.value.State ? this.intakeNarrativeForm.value.State : '',
            zipcode: this.intakeNarrativeForm.value.Zip ? this.intakeNarrativeForm.value.Zip : '',
            match: 'invalid'
        };

        this.addressAnalysis = [];
        this._commonHttpService
            .getSingle(
                {
                    method: 'post',
                    where: addressInput
                },
                NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
            )
            .subscribe(
                (result) => {
                    if (result[0].analysis) {
                        this.intakeNarrativeForm.patchValue({
                            Zip: result[0].components.zipcode ? result[0].components.zipcode : '',
                            County: result[0].metadata.countyName ? result[0].metadata.countyName : '',
                            county: result[0].metadata.countyName ? result[0].metadata.countyName : ''
                        });

                        if (result[0].analysis.dpvMatchCode) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvMatchCode
                            });
                        }
                        if (result[0].analysis.dpvFootnotes) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvFootnotes
                            });
                        }
                        if (result[0].analysis.dpvCmra) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvCmra
                            });
                        }
                        if (result[0].analysis.dpvVacant) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.dpvVacant
                            });
                        }
                        if (result[0].analysis.active) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.active
                            });
                        }
                        if (result[0].analysis.ewsMatch) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.ewsMatch
                            });
                        }
                        if (result[0].analysis.lacslinkCode) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.lacslinkCode
                            });
                        }
                        if (result[0].analysis.lacslinkIndicator) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.lacslinkIndicator
                            });
                        }
                        if (result[0].analysis.suitelinkMatch) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.suitelinkMatch
                            });
                        }
                        if (result[0].analysis.footnotes) {
                            this.addressAnalysis.push({
                                text: result[0].analysis.footnotes
                            });
                        }
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }




    private narrativeTooltip() {
        this._resourceService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        resourcetype: [3],
                        parentid: '5c70b495-4a13-4b70-83fa-ab0e2b341cb8'
                        // parentid: 'f9c6ea93-5699-4df2-b7a9-92c32b9b325c'
                    }
                },
                NewUrlConfig.EndPoint.Intake.ResourceTooltipUrl + '?filter'
            )
            .subscribe((result) => {
                if (result) {
                    result.map((item) => {
                        if (item.name === 'Narrative') {
                            this.tooltip = item.tooltip;
                            return;
                        }
                    });
                }
            });
    }
    getGenderDescription(genderKey) {
        if (this.genderList && Array.isArray(this.genderList)) {
            const gender = this.genderList.filter(item => item.gendertypekey === genderKey);
            if (gender && gender.length) {
                return gender[0].typedescription;
            } else {
                return '';
            }
        }
    }
    addIdentified() {
        const person = this.addIdentifiedFormGroup.getRawValue();
        if (
            person.firstname.length > 0 ||
            person.lastname.length > 0 ||
            person.dob.length > 0 ||
            person.gender.length > 0 ||
            person.ssn.length > 0 ||
            person.age.length > 0) {
            let persons = this._storeService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
            if (!persons) {
                persons = [];
            }
            person.id = new Date().getTime();
            person.isAdded = false;
            persons.push(person);
            this._storeService.setData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS, persons);
            this.addedIdentifiedPersons = this._storeService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
            (<any>$('#add-identified')).modal('hide');
            this.addIdentifiedFormGroup.reset();
            this.addIdentifiedFormGroup.patchValue({
                firstname: '',
                lastname: '',
                dob: '',
                gender: '',
                ssn: '',
                age: '',
            });
            this._intakeConfig.quickAddPersonListener$.next('QAD');
        } else {
            this._alertService.warn('Fill any field for quick add');
        }
    }
    deleteIdentified(person) {
        let persons = this._storeService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
        persons = persons.filter(item => person.id !== item.id);
        this._storeService.setData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS, persons);
        this.addedIdentifiedPersons = this._storeService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
    }
    searchIdentified(person) {
        this._storeService.setData(IntakeStoreConstants.PERSON_TO_SEARCH, person);
        // this._router.navigate(['../person'], { relativeTo: this.route });
        this._router.navigate(['pages/newintake/my-newintake/person-cw/find-individual/search']);
    }
    getGenderList() {
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1 },
                method: 'get',
                nolimit: true
            },
            NewUrlConfig.EndPoint.Intake.GenderTypeUrl + '?filter'
        ).subscribe(data => {
            this.genderList = data;
        });
    }

    getReporterRolesList() {
        this._commonHttpService.getArrayList({
            method: 'get',
            where: { tablename: 'reporterroles', teamtypekey: 'CW' }
        }, NewUrlConfig.EndPoint.Intake.GetTypes + '?filter').subscribe(data => {
            this.reporterRoles = data;
        });
    }

    calculateAge(dob) {
        console.log(dob);
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            const age = { years: 0, months: 0, days: 0, totalMonths: 0, duration: null };
            age.years = (moment().diff(rCDob, 'years', false)) ? moment().diff(rCDob, 'years', false) : 0;
            age.totalMonths = (moment().diff(rCDob, 'months', false)) ? moment().diff(rCDob, 'months', false) : 0;
            age.months = (age.totalMonths - (age.years * 12)) ? age.totalMonths - (age.years * 12) : 0;
            age.days = (moment().diff(rCDob, 'days', false)) ? moment().diff(rCDob, 'days', false) : 0;
            age.duration = moment.duration(moment(Date.now()).diff(moment(rCDob)));
            const ddays = (age.duration.days()) ? age.duration.days() : 0;
            const personAge = `${age.years} years ${age.months} month(s) ${ddays} day(s)`;
            this.addIdentifiedFormGroup.controls['age'].setValue(personAge);
        }
        // return age;
    }

    onCPSHistorySelectionChanged(event) {
        if (event.range == null) {
            this.checkForCPSHistoryContent();
        }
    }

    checkForCPSHistoryContent() {
        this.currentCPSHistoryText = this.intakeNarrativeForm.getRawValue().cpsHistoryClearance;
        if (this.cpsHistoryCont !== this.currentCPSHistoryText && this.currentCPSHistoryText !== '') {
            console.log('narrative changed', new Date());
            this.cpsHistoryCont = this.currentCPSHistoryText;
        }
    }

    onNarrativeSelectionChanged(event) {
        if (event.range == null) {
           this.checkForNarrativeContent();
          }
    }

    checkForNarrativeContent() {
        this.currentNarrativeText = this.intakeNarrativeForm.getRawValue().Narrative;
        if (this.narrativeCont !== this.currentNarrativeText && this.currentNarrativeText !== '') {
            console.log('narrative changed', new Date());
            this.narrativeCont = this.currentNarrativeText;
            this.narrativeUpdatedDate = new Date();
            const narrativeInfo = this._storeService.getData(IntakeStoreConstants.addNarrative);
            narrativeInfo.narrativeUpdatedDate = this.narrativeUpdatedDate;
            this._storeService.setData(IntakeStoreConstants.addNarrative, narrativeInfo);
        }
    }


    isCPSHistoryClearanceChecked() {
        return this._storeService.getData('isCPSHistoryClearanceChecked');
    }

}
