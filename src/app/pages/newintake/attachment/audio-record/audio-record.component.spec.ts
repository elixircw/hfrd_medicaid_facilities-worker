import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';

import { AudioRecordComponent } from './audio-record.component';
import { AttachmentDetailComponent } from '../attachment-detail/attachment-detail.component';
import { CoreModule } from '../../../../@core/core.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';

describe('AudioRecordComponent', () => {
    let component: AudioRecordComponent;
    let fixture: ComponentFixture<AudioRecordComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                A2Edatetimepicker,
                SharedPipesModule,
                RouterModule,
                AttachmentDetailComponent
            ],
            declarations: [AudioRecordComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AudioRecordComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
