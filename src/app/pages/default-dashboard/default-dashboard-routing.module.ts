import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultDashboardComponent } from './default-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DefaultDashboardComponent
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DefaultDashboardRoutingModule { }
