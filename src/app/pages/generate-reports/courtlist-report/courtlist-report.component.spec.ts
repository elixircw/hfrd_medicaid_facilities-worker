import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourtlistReportComponent } from './courtlist-report.component';

describe('CourtlistReportComponent', () => {
  let component: CourtlistReportComponent;
  let fixture: ComponentFixture<CourtlistReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourtlistReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourtlistReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
