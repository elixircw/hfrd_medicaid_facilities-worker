import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcuCaseReportComponent } from './ccu-case-report.component';

describe('CcuCaseReportComponent', () => {
  let component: CcuCaseReportComponent;
  let fixture: ComponentFixture<CcuCaseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcuCaseReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcuCaseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
