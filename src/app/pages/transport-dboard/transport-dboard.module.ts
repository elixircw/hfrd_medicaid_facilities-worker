import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransportDboardRoutingModule } from './transport-dboard-routing.module';
import { TransportDboardComponent } from './transport-dboard.component';
import { TransportListComponent } from './transport-list/transport-list.component';
import { TransportRosterComponent } from './transport-roster/transport-roster.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonControlsModule } from '../../shared/modules/common-controls/common-controls.module';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule,
  MatAutocompleteModule
} from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    TransportDboardRoutingModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    CommonControlsModule
  ],
  declarations: [TransportDboardComponent, TransportListComponent, TransportRosterComponent]
})
export class TransportDboardModule { }
