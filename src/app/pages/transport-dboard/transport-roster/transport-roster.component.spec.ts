import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportRosterComponent } from './transport-roster.component';

describe('TransportRosterComponent', () => {
  let component: TransportRosterComponent;
  let fixture: ComponentFixture<TransportRosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportRosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportRosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
