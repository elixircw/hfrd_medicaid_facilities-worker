import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportDboardComponent } from './transport-dboard.component';

describe('TransportDboardComponent', () => {
  let component: TransportDboardComponent;
  let fixture: ComponentFixture<TransportDboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportDboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportDboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
