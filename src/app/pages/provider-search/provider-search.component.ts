import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../@core/services';
import { PaginationRequest, PaginationInfo } from '../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../case-worker/case-worker-url.config';
import * as moment from 'moment';
import { ServiceCasePlacementsService } from '../case-worker/dsds-action/service-case-placements/service-case-placements.service';
import { FinanceService } from '../finance/finance.service';
@Component({
  selector: 'provider-search',
  templateUrl: './provider-search.component.html',
  styleUrls: ['./provider-search.component.scss']
})
export class ProviderSearchComponent implements OnInit {
  providerSearchForm: FormGroup;
  childCharacteristics: any[];
  otherLocalDeptmntType: any[];
  placementStrType: any[];
  bundledPlcmntServicesType: any[];
  genderDropdownItems: any[];
  paginationInfo: PaginationInfo = new PaginationInfo();
  reportedChildDob: any;
  fcProviderSearch: any[];
  fcTotal: number;
  currProcess: string;
  selectedProvider: any;
  selectedViewProvider: any;

  washingtoncounty: any;

  constructor(
    private formBuilder: FormBuilder,
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _ServiceCasePlacementsService: ServiceCasePlacementsService,
    private _providerpopup: FinanceService
  ) { }

  ngOnInit() {
    this.initProviderSearchForm();
    this.getChildCharacteristics();
    this.getOtherLocalDeptmntType();
    this.getPlacementStrType(null);
    this.getBundledPlcmntServicesType();
    this.loadGenderDropdownItems();
  }

  initProviderSearchForm() {
    this.providerSearchForm = this.formBuilder.group({
      childcharacteristics: [null],
      bundledplacementservices: [null],
      otherLocalDeptmntTypeId: [null],
      placementstructures: [null],
      zipcode: null,
      isLocalDpt: [true],
      firstname: null,
      providername: null,
      middlename: null,
      lastname: null,
      isgender: [null],
      isAge: [null],
      providerid: null,
      taxId: null,
      agemin: null,
      agemax: null,
      gender: null
    });
  }

  getRangeArray(n: number): any[] {
    return Array(n);
  }

  getChildCharacteristics() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '43'
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.childCharacteristicsUrl).subscribe(result => {
      this.childCharacteristics = result;
    });
  }

  getOtherLocalDeptmntType() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '104'
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.otherLocalDeptmntTypeUrl).subscribe(result => {
      this.otherLocalDeptmntType = result;
      this.washingtoncounty = this.otherLocalDeptmntType.find(el => el.value_tx == 'Washington');
      this.providerSearchForm.controls['otherLocalDeptmntTypeId'].setValue([this.washingtoncounty.picklist_value_cd]);
    });
  }

  getPlacementStrType(providerId) {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'structure_service_cd': 'P',
        'provider_id': providerId
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.placementStrTypeUrl).subscribe(result => {
      this.placementStrType = result;
    });
  }

  getBundledPlcmntServicesType() {
    this._commonService.getArrayList(new PaginationRequest({
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.bundledPlcmntServicesTypeUrl).subscribe(result => {
      this.bundledPlcmntServicesType = result;
    });
  }

  loadGenderDropdownItems() {
    this._commonService.getArrayList(
      {
        where: { activeflag: 1 },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.GenderTypeUrl + '?filter'
    ).subscribe((genderList) => {
      this.genderDropdownItems = genderList;
    });
    // text: res.typedescription,
    // value: res.gendertypekey
  }

  getFcProviderSearch() {
    document.getElementById('search-list').scrollIntoView();
    if (this.providerSearchForm.invalid) {
      this._alertService.error('Please fill required fields');
      return false;
    }


    // if (this._ServiceCasePlacementsService.selectedChildren.length === 0) {
    //   this._alertService.error('Please select the children');
    //   return false;
    // }

    const formValues = this.providerSearchForm.getRawValue();
    if (formValues.isgender && !formValues.gender) {
      this._alertService.error('Please select gender');
      return false;
    }
    // formValues.agemin = formValues.isAge ? ( this.minAge ? this.minAge : null ) : null;
    // formValues.agemax = formValues.isAge ? ( this.maxAge ? this.maxAge : null ) : null;
    formValues.gender = formValues.isgender ? (formValues.gender ? formValues.gender : null) : null;
    console.log(formValues);

    Object.keys(this.providerSearchForm.controls).forEach(key => {
      formValues[key] = this.CheckFormControlValue(formValues[key]);
    });
    const body = {};
    Object.assign(body, formValues);
    body['isLocalDpt'] = formValues.isLocalDpt ? formValues.isLocalDpt : false;
    body['localdepartmenthomecaregiver'] = formValues.isLocalDpt ? formValues.isLocalDpt : false;
    body['childcharacteristics'] = formValues.childcharacteristics ? formValues.childcharacteristics.join() : null;
    body['otherLocalDeptmntTypeId'] = formValues.otherLocalDeptmntTypeId && formValues.otherLocalDeptmntTypeId.length ? formValues.otherLocalDeptmntTypeId.join() : null;
    body['placementstructures'] = formValues.placementstructures && formValues.placementstructures.length ? formValues.placementstructures.join() : null;
    body['bundledplacementservices'] = formValues.bundledplacementservices && formValues.bundledplacementservices.length ? formValues.bundledplacementservices.join() : null;
    body['global'] = true;

    const dob = moment.utc(this.reportedChildDob);
    const age16 = dob.clone().add(16, 'years');
    const age21 = dob.clone().add(21, 'years');
    const isAgeBtwn16And18 = moment.utc().isBetween(age16, age21, null, '[]');
    if (isAgeBtwn16And18 && formValues.placementStrTypeId === '1') {// Independent Living Residential Program
      this._alertService.error('A child between the age of 16 and 21 years cannot be placed in \'Independent Living Residential Program\' Placement Structure.');
      return false;
    }
   
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: body,
      page: this.paginationInfo.pageNumber,
      limit: 10,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fcProviderSearchUrl).subscribe(result => {
      this.fcProviderSearch = result.data;
      this.fcTotal = result.count;
      (<any>$('#fc_list')).click();
      this.currProcess = 'select';
    });
  }

  CheckFormControlValue(formControl) {
    return (formControl && formControl !== '') ? formControl : null;
  }

  backToSearch() {
    this.currProcess = 'search';
    this.selectedProvider = null;
  }

  localdptSelected(event) {
    console.log(event);

    if (event.value) {

      this.providerSearchForm.get('firstname').enable();
      this.providerSearchForm.get('middlename').enable();
      this.providerSearchForm.get('lastname').enable();
      this.providerSearchForm.get('otherLocalDeptmntTypeId').enable();
    } else {
      
      this.providerSearchForm.get('providername').enable();
      //this.providerSearchForm.get('firstname').disable();
     // this.providerSearchForm.get('middlename').disable();
     // this.providerSearchForm.get('lastname').disable();
      this.providerSearchForm.get('otherLocalDeptmntTypeId').disable();
    }
  }

  resetproviderSearchForm() {
    this.providerSearchForm.reset();
    this.fcProviderSearch = null;
    this.fcTotal = 0;
  }

  fcPageChanged(pageEvent) {
    this.paginationInfo.pageNumber = pageEvent.page;
    this.getFcProviderSearch();
  }

  selectedViewProv(provId) {
    this.selectedViewProvider = provId;
  }

  formatPhoneNumber(phoneNumberString) {

    const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
    const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      return '(' + match[1] + ') ' + match[2] + '-' + match[3];
    }
    return null;

  }
  getLicenseCoordinatorName(obj) {
    let name = '';
    if (obj && obj.length > 0) {
      obj.forEach((namObj, index) => {
        name = name + namObj.license_cordinator;
        name = ((index + 1) < obj.length) ? name + ',' : name;
      });
      return name;
    } else {
      return name;
    }

  }
  showProviderPopup(providerID) {
    this._dataStoreService.setData('ProviderInfoID', providerID);
    (<any>$('#provider-info-popup')).modal('show');
    this._providerpopup.getProviderDetails(providerID);
    this._providerpopup.page = 'Both';
  }

}
