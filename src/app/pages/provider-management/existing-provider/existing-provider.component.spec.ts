import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingProviderComponent } from './existing-provider.component';

describe('ExistingProviderComponent', () => {
  let component: ExistingProviderComponent;
  let fixture: ComponentFixture<ExistingProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
