import { Component, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CommonHttpService } from '../../../@core/services/common-http.service';
import { EventEmitter } from 'protractor';
import { DropdownModel, PaginationInfo } from '../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AuthService, DataStoreService, CommonDropdownsService } from '../../../@core/services';
import { ProviderStoreConstants } from '../provider-constants';
import { ReferralUrlConfig } from '../../provider-referral/provider-referral-url.config';
import { arrayLength } from 'ng4-validators/src/app/array-length/validator';

@Component({
  selector: 'existing-provider',
  templateUrl: './existing-provider.component.html',
  styleUrls: ['./existing-provider.component.scss']
})
export class ExistingProviderComponent implements OnInit {
  providers: any;
  providerForm: FormGroup;
  advanceSearchForm: FormGroup;
  searchProviderId = '';
  searchProviderName = '';
  searchStatus = '';
  saerchJurisdiction = '';
  pageInfo: PaginationInfo = new PaginationInfo();
  accordianData: Array<any> = [];
  totalCount: Number;
  eventSelect: Array<any> = [];
  newObj: any = {};
  countyFilter: any;
  countyList$: Observable<DropdownModel[]>;
  statusList = [{text: 'Open', value : 1791}, {text: 'Closed', value : 1792}];
  typeOfProviderValue: any;
  typeOfProvider = '';

  categoryCodeRef: any = {
    '1782': 'CPA Home',
    '1783': 'Local Department Home',
    // '1784' : 'LDSS Kinship Home',
    '1785': 'ICPC Home Study',
    '3049': 'Private Organization',
    '3274': 'RCC Facility',
    '3302': 'CPA Office',
    '3304': 'Vendor',
    '3305': 'Community Provider'
  };

  categoryCodeFilter = [
    { 'text': 'Public', 'value': '1783' },
    { 'text': 'Private', 'value': '3049' },
    { 'text': 'Vendor', 'value': '3304' },
  ];
  agency: any;
  roleId: any;
  providerTypeList$: Observable<any[]>;
  statusList$: Observable<any[]>;
  programList: any[] = [];

  constructor(private _commonHttpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _dataservice: DataStoreService, private _ddservice: CommonDropdownsService) {
    this.roleId = this._authService.getCurrentUser();
  }

  ngOnInit() {
    this.searchStatus = null;
    this.saerchJurisdiction = null;
    this.pageInfo.pageSize = 10;
    this.pageInfo.pageNumber = 1;
    if (this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
    || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
    || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA' ) {
      this.agency = 'DJS';
    } else {
      this.agency = 'OLM';
    }
    this.eventSelect = ['3049', '1782', '3274', '3302'];
    this.providerForm = this._formBuilder.group({
      category_code_filter: ['3049']
    });
    this.advanceSearchForm = this._formBuilder.group({
      tax_id: [null],
      providername: [null],
      providerid: [null],
      program_type: [null],
      provider_status: [null],
      affiliation_type: [null],
      rcc_certificate_type: [null],
      county: [null],
      zip: [null],
      address: [null],
      email: [null],
      program_name: [null]

    });
    this.loadCountyDropdown();
    this.getExistingProvider(['1783', '3049', '3304']);
  }

  private getExistingProvider(param: any, providerId: any = '', providerName: String = '') {
    this.providers = [];
    this.accordianData = [];
    this.newObj = {};
    /* if(this.typeOfProviderValue === '1783'){
      this.typeOfProvider = 'public'
    }else if(this.typeOfProviderValue === '3049'){
      this.typeOfProvider = 'private'
    }else if(this.typeOfProviderValue === '3304'){
      this.typeOfProvider = 'vendor'
    } */
    this.typeOfProvider = 'private';
    const baseObj = {
      method: 'post',
      nolimit: true,
      // order: ['create_ts DESC'],
      providercategorycd: '3049',
      providerid: providerId,
      providername: providerName,
      providerstatuscd: this.searchStatus,
      county: this.saerchJurisdiction,
      providercategorystr: this.typeOfProvider,
      filter: {},
      pagenumber: this.pageInfo.pageNumber,
      pagesize: this.pageInfo.pageSize,
      agency: this.agency
    };
    const advSearchObj = this.advanceSearchForm.getRawValue();
    const derivedObj = Object.assign(baseObj, advSearchObj);
    this._commonHttpService.getArrayList(
     derivedObj,
      'providerreferral/providersearch'
    ).subscribe(providers => {
      (<any>$('#advancesearch')).modal('hide');
      this.accordianData = providers;
      this.totalCount =  providers && providers.length ? providers[0].totalcount : 0;
      this.accordianData.forEach((x) => {
        x.provider_category_cd_tx = this.categoryCodeRef[x.provider_category_cd];
        if (x.sites) {
          x.sites.forEach(item => {
            item.provider_category_cd_tx = this.categoryCodeRef[item.provider_category_cd];
          });
        }
      });
    });
  }

  toggleTable(id, index, mode) {
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle');
    (<any>$('.provider-details tr')).removeClass('selected-bg');
    (<any>$(`#provider-details-${index}`)).addClass('selected-bg');
  }

  pageChanged(pageInfo: any) {
    this.pageInfo.pageNumber = pageInfo.page;
    this.pageInfo.pageSize = pageInfo.itemsPerPage;
    this.getExistingProvider(['1783', '3049', '3304']);
  }

  onOptionChange(event: any) {
    this.typeOfProviderValue = event.value;
    if (event.value === 3049) {
      this.eventSelect = ['3049', '1782', '3274', '3302'];
    } else {
      this.eventSelect = [event.value];
    }
    this.getExistingProvider(this.eventSelect);
  }

  onSearch(searchType: string, searchValue: string) {
    // Preferably use switch
    if (searchType === 'providerid') {
      this.searchProviderId = searchValue;
    }
    if (searchType === 'providername') {
      this.searchProviderName = searchValue;
    }
    if (searchType === 'providerstatus') {
      this.searchStatus = searchValue;
    }
    if (searchType === 'jurisdiction') {
      this.saerchJurisdiction = searchValue;
    }
    this.getExistingProvider(this.eventSelect, this.searchProviderId, this.searchProviderName);
  }

  loadCountyDropdown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        { 'nolimit': true, order: 'description', 'where': { 'referencetypeid': 306, 'mdmcode': 'MD' }, 'method': 'get' },
        'referencevalues' + '?filter'
      )
    ]).map(res => {
      return {
        countyList: res[0].map(
          item =>
            new DropdownModel({
              value: item.ref_key,
              text: item.value_text
            })
        )
      };
    });
    this.countyList$ = source.pluck('countyList');
    this.statusList$ = this._ddservice.getPickList(159);
    this.providerTypeList$ = this._ddservice.getPickList(160);
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        providertype: 'Private'
      }, ReferralUrlConfig.EndPoint.Referral.listprogramnamesUrl).subscribe(data => {
        this.programList = data;
        let list = [];
        data.forEach(element => {
          list = list.concat(element.programnames);
        });
        this.programList = list;
      });
  }

  navigatetosite(provider, obj) {
    this._dataservice.setData(ProviderStoreConstants.PROVIDER_DETAILS, provider);
    this._dataservice.setData(ProviderStoreConstants.SITE_DETAILS, obj);
  }

  opensearchmodal() {
    this.advanceSearchForm.reset();
  }

  refreshsearch() {
    this.advanceSearchForm.reset();
    this.getExistingProvider(['1783', '3049', '3304']);

  }
}
