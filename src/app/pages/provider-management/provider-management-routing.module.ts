import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExistingProviderComponent } from '../provider-management/existing-provider/existing-provider.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProviderManagementComponent } from './provider-management.component'

const routes: Routes = [
    {
        path: '',
        component: ProviderManagementComponent,
        children: [
            {
                path: 'new-provider',
                loadChildren: './new-provider/new-provider.module#NewProviderModule'
            }
        ]
    }, 
    {
        path: '',
        component: ProviderManagementComponent,
        children: [
            {
                path: 'public-provider-reconsideration',
                loadChildren: './public-provider-reconsideration/public-provider-reconsideration.module#PublicProviderReconsiderationModule'
            }
        ]
    },
    {
        path: '',
        component: ProviderManagementComponent,
        children: [
            {
                path: 'new-public-provider',
                loadChildren: './new-public-provider/new-public-provider.module#NewPublicProviderModule'
            }
        ]
    },
    {
        path: '',
        component: ProviderManagementComponent,
        children: [
            {
                path: 'license-management',
                loadChildren: './license-management/license-management.module#LicenseManagementModule'
            }
        ]
    },  
    {
        path: '',
        component: ProviderManagementComponent,
        children: [
            {
                path: 'provider-request-management',
                loadChildren: './provider-request-management/provider-request-management.module#ProviderRequestManagementModule'
            }
        ]
    },
    {
        path: 'existing-provider',
        component: ExistingProviderComponent,
        // canActivate: [RoleGuard],
        data: {
            title: ['MDTHINK - Saved Intakes'],
            desc: 'Maryland department of human services',
            screen: { current: 'new', modules: [], skip: false }
        }
    },
    {
        path: '',
        component: ProviderManagementComponent,
        children: [
            {
                path: 'contract-management',
                loadChildren: './contract-management/contract-management.module#ContractManagementModule'
            }
        ]
    },
];

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes)
   
],
  exports: [RouterModule]
})
export class ProviderManagementRoutingModule { }
