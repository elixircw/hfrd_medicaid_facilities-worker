
export enum ProviderStoreConstants {
    PROVIDER_DETAILS = 'PROVIDER_DETAILS',
    SITE_DETAILS = 'SITE_DETAILS'
}
