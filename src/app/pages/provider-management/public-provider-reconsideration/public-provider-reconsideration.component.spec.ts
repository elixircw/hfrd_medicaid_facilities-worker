import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderReconsiderationComponent } from './public-provider-reconsideration.component';

describe('PublicProviderReconsiderationComponent', () => {
  let component: PublicProviderReconsiderationComponent;
  let fixture: ComponentFixture<PublicProviderReconsiderationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderReconsiderationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderReconsiderationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
