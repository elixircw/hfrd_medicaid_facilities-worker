import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderReconsiderationContactComponent } from './public-provider-reconsideration-contact.component';

describe('PublicProviderReconsiderationContactComponent', () => {
  let component: PublicProviderReconsiderationContactComponent;
  let fixture: ComponentFixture<PublicProviderReconsiderationContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderReconsiderationContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderReconsiderationContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
