import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as jsPDF from 'jspdf';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { AuthService } from '../../../../@core/services/auth.service';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { getDate } from 'date-fns';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { applinfo } from '../add-document/_entities/attachmnt.model';

const APPOINTMENT_COMPLETED = 'Completed'
@Component({
  selector: 'public-provider-recon-doc-creator',
  templateUrl: './public-provider-recon-doc-creator.component.html',
  styleUrls: ['./public-provider-recon-doc-creator.component.scss']
})
export class PublicProviderReconDocCreatorComponent implements OnInit {
  @Input()
    profileOutputSubject$ = new Subject<applinfo>();
    licenseTypeName: string;
    licenseLevel: string;

    executiveDirectorSecurityUserId: string;
    executiveDirectorSignImage: any;
    // Provider License Documnet Generator

    applicationId: string;


    isViewable: boolean;

    // tslint:disable-next-line:no-input-rename
    persons: any[];
    general: any;
    id: string;
    daNumber: string;
    preIntakeDisposition: any;
    documentsToDownload: string[] = [];
    supComments = '';
    reason = '';
    comments = '';
    addedPersons: any[];
    youth: any;
    downloadInProgress: boolean;
    complaintID: string;
    complaintReceiveDate: string;
    selectedAllegedOffenseIDs: any[];
    allegedOffenseDate: string;
    allAllegedOffense: string;
    offenses: any[];
    loggedInUser: string;
    preintakeAppointmentDate: string;
    CurrentDate: Date;
    youthName: string;
    youthDob;
    youthPhoneNumber: string;
    youthId: string;
    victimName: string;
    victimAddress: string;
    victimPhoneNumber: string;
    currentDateString;
    mother: { name: string; phoneNumber: string; address: string };
    father: { name: string; phoneNumber: string; address: string };
    guardian: { name: string; phoneNumber: string; address: string };
    appointmentHeld = false;
    appointmentNotes = '';
    offenseString: string;
    parentOrGaurdianAddress: string;
    parentOrGaurdianName: string;

    petitionID: string;
    evalFields: any;
    appointments: any = [];

    fatherObj: any;
    motherObj: any;
    guardianObj: any;
    youthLastSchool: any;
    appointment: any;

    applicantInfo: any;

    courtDetails: any;
    youthAge: number;
    applicantNumber: string;

    //DHS certification
    individualAppPrefix: string;
    individualAppfirstNm: string;
    individualAppMiddleNm: string;
    individualAppLastNm: string;
    individualAppSuffix: string;
    coAppPrefix: string;
    coAppFirstNm: string;
    coAppMiddleNm: string;
    coAppLastNm: string;
    coAppSuffix: string;
    isAnd: string;
    approvalType: string;
    jurisdiction: string;

    //Home Study Document

    individualAppDob: string;
    coAppDob: string;
    individualAppEmployer: string;
    coAppEmployer: string;

    // Home visit
    dateOfContact: any[];
    sonsDaughters: any[];
    othersResiding: any[];
    individualExtendedFamily:any[];
    coExtendedFamily:any[];
    references:any[];
    coApplicantResponse = [];
    individualApplicantResponse = [];

    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    constructor(private route: ActivatedRoute,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private _dataStoreService: DataStoreService,
        private _reportSummaryService: GenericService<any>) {
        this.applicantNumber = route.snapshot.params['id'];
        this.applicationId = route.snapshot.params['id'];
    }

    ngOnInit() {
        this.dateOfContact = new Array(5);
        this.sonsDaughters = new Array(5);
        this.othersResiding = new Array(5);
        this.individualExtendedFamily = new Array(5);
        this.coExtendedFamily = new Array(5);
        this.references = new Array(5);
        this.getHomeStudyInformation();
        this.getReferenceCheckInformation();
        this.getInformation();
        this.getProviderHousehold();
        const currentDate = new Date();
        //this.getLetterInformation();
        currentDate.setDate(currentDate.getDate() + 10);
        this.CurrentDate = currentDate;
        this.loggedInUser = this._authService.getCurrentUser().user.userprofile.displayname;

        this.executiveDirectorSecurityUserId = 'c0597522-01ed-40a2-b85d-7d83eb8c6768';
        //   this.getLicenseInformation();
        this.isViewable = false;


        this._dataStoreService.currentStore.subscribe((store) => {

            if (store['documentsToDownload']) {
                this.documentsToDownload = store['documentsToDownload'];
                this.id = this.route.snapshot.parent.parent.params['id'];
                this.daNumber = this.route.snapshot.parent.parent.params['daNumber'];
                //   this.listReportSummary();
                // this.collectivePdfCreator();
            }
            if (store['dsdsActionsSummary']) {
                const actionSummary = store['dsdsActionsSummary'];
                const jsonData = actionSummary['intake_jsondata'];
                if (jsonData) {
                    //   this.processIntakeData(jsonData);
                }
            }
        });
    }
    collectivePdfCreator() {
        this.downloadInProgress = true;
        const pdfList = this.documentsToDownload;
        pdfList.forEach((element) => {
            this.downloadCasePdf(element);
        });
    }
    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            // console.log(pages.item(i).getAttribute('data-page-name'));
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
            });
        }
        this.convertImageToPdf();
    }

    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            // const doc = new jsPDF('landscape');
            var doc = null;
            if (pdfFile.fileName == "RCC license certificate" || pdfFile.fileName == "DJS license certificate") {
                doc = new jsPDF('landscape');
            } else {
                doc = new jsPDF();
            }

            var width = doc.internal.pageSize.getWidth() - 10;
            var heigth = doc.internal.pageSize.getHeight() - 10;
            // alert(width+","+heigth);
            // doc.setFontSize(30);
            pdfFile.images.forEach((image, index) => {

                doc.addImage(image, 'JPEG', 3, 5, width, heigth);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#docu-View')).modal('hide');
        this.pdfFiles = [];
        this.downloadInProgress = false;
    }

    isNotSelected(key) {
        let toHide = true;
        if (this.documentsToDownload) {
            toHide = this.documentsToDownload.indexOf(key) === -1;
        }
        return toHide;
    }

    //get Certificate info
     getInformation() {
        this._commonHttpService.getById(
            this.applicantNumber,
            'publicproviderapplicant'
        ).subscribe(
            (response) => {
                console.log('get info response', response);
                this.individualAppPrefix= response.individual_applicant_prefix ;
                this.individualAppfirstNm= response.individual_applicant_first_nm ;
                this.individualAppMiddleNm= response.individual_applicant_middle_nm ;
                this.individualAppLastNm= response.individual_applicant_last_nm ;
                this.individualAppSuffix= response.individual_applicant_suffix ;
                this.coAppPrefix= response.co_applicant_prefix ;
                this.coAppFirstNm= response.co_applicant_first_nm ;
                this.coAppMiddleNm= response.co_applicant_middle_nm ;
                this.coAppLastNm= response.co_applicant_last_nm ;
                this.coAppSuffix= response.co_applicant_suffix ;
                this.approvalType= response.provider_program_type ;
                // this.jurisdiction= response.jurisdiction ; //Needs to get the actual value instead of id
                
                this.individualAppDob= response.individual_applicant_dob ;
                this.coAppDob= response.co_applicant_dob ;
                this.individualAppEmployer= response.individual_applicant_employer_nm ;
                this.coAppEmployer= response.co_applicant_employer_nm ;                


                if(this.coAppFirstNm!=null || this.coAppLastNm!=null){
                    this.isAnd= '&';
                }
            },
            (error) => {
                this._alertService.error("Unable to retrieve information");
            }

        );
    }

    //Getting home study document details
    getReferenceCheckInformation() {
        this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where : { object_id : this.applicantNumber }
                },
                'publicproviderreferencecheck?filter'
            ).subscribe(response => {
                console.log('this.references',this.references);
                for(var i=0; i<this.references.length;i++){
                    this.references[i] = response[i];
                }
                console.log('this.references1',this.references);
                console.log('response',response);

    
            });
      }

      private getHomeStudyInformation() {
        this._commonHttpService.getArrayList(
                {
                    method: 'post',
                    where : { object_id : this.applicantNumber }
                },
                'publicproviderhomestudyhouseholdmapping/gethomestudyhousehold'
            ).subscribe(response => {
          for(var i=0; i<this.dateOfContact.length;i++){
            this.dateOfContact[i] = response[i];
        }
            });
      }

      getProviderHousehold() {
        this._commonHttpService.getArrayList(
          {
            method: 'get',
            nolimit: true,
            where: { object_id: this.applicantNumber },
            order: 'household_member_id desc'
          },
          'publicproviderapplicanthousehold?filter'
        ).subscribe(
          (response) => {
              
            for(var i=0; i<response.length;i++){
                if(response[i].household_member_relation === 'Co Applicant' ){
                    this.coApplicantResponse=response[i];
                    console.log('this.coApplicantResponse',this.coApplicantResponse);
                }
                else if (response[i].household_member_relation === 'Individual Applicant'){
                    this.individualApplicantResponse=response[i];
                    console.log('this.individualApplicantResponse',this.individualApplicantResponse)

                }
            }
          },
          (error) => {
            this._alertService.error("Unable to retrieve Household member");
          }
        );
      }
}
