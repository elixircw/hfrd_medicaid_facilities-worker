import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderReconDocCreatorComponent } from './public-provider-recon-doc-creator.component';

describe('PublicProviderReconDocCreatorComponent', () => {
  let component: PublicProviderReconDocCreatorComponent;
  let fixture: ComponentFixture<PublicProviderReconDocCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderReconDocCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderReconDocCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
