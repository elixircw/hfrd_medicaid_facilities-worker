import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicProviderReconsiderationComponent } from './public-provider-reconsideration.component';

const routes: Routes = [
  {
    path: '',
    component: PublicProviderReconsiderationComponent,
    // canActivate: [RoleGuard],
    children: [
    ]
},
{
    path: ':id',
    component: PublicProviderReconsiderationComponent,
    children: [
    ]
    // canActivate: [RoleGuard]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicProviderReconsiderationRoutingModule { }
