import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderReconsiderationDecisionComponent } from './public-provider-reconsideration-decision.component';

describe('PublicProviderReconsiderationDecisionComponent', () => {
  let component: PublicProviderReconsiderationDecisionComponent;
  let fixture: ComponentFixture<PublicProviderReconsiderationDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderReconsiderationDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderReconsiderationDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
