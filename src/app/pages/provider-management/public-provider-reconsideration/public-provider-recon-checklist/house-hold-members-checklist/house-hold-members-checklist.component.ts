import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ChangeDetectorRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, AuthService, AlertService, DataStoreService } from '../../../../../@core/services';

@Component({
  selector: 'house-hold-members-checklist',
  templateUrl: './house-hold-members-checklist.component.html',
  styleUrls: ['./house-hold-members-checklist.component.scss']
})
export class HouseHoldMembersChecklistComponent implements OnInit {
  applicantId: string;
  id: string;
  taskList = [];
  tasktypestatus = ["Yes","No","N/A"];
  tmpAccordionKey = [];
  activityList = [];
  activityCompleteStatus = 'Completed';
  houseHoldMemberChecklistFormGroup: FormGroup;
  config = {
    panels: [

    ]
  };
  programType:string;

  constructor(private _cfr: ComponentFactoryResolver,
    private route: ActivatedRoute,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _changeDetect: ChangeDetectorRef,
    private _dataStoreService: DataStoreService) { 
    this.applicantId = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.houseHoldMemberChecklistFormGroup = this._formBuilder.group({
      task: this._formBuilder.array([this.createTaskForm()])
    });
    this.getpublicproviderhouseholdmember();
  }



  getpublicproviderhouseholdmember(){
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        page: 1,
        limit: 20,
        where: { applicant_id: this.applicantId }
      },'publicproviderhouseholdmember/memberchecklist?filter'
    ).subscribe(res => {
      if (res && res.length>0) {
        this.taskList = res;
        this.setFormValues();
      }
    });
  }





  createTaskForm() {
    return this._formBuilder.group({
      household_member_first_name: [''],
      household_member_middle_name: [''],
      household_member_last_name: [''],
      applicant_id: [''],
      household_member_id: [''],
      checklist_task: [''],
      comment: [''],
      review_date: [''],
      date_type: [''],
      status: ['']
    });
  }

  private buildTaskForm(x): FormGroup {
    return this._formBuilder.group({
      household_member_first_name: x.household_member_first_name,
      household_member_middle_name: x.household_member_middle_name,
      household_member_last_name: x.household_member_last_name,
      applicant_id: x.applicant_id,
      household_member_id: x.household_member_id,
      checklist_task: x.checklist_task,
      comment: x.comment,
      review_date: x.review_date,
      date_type: x.date_type,
      status: x.status
    });
  }



  setFormValues() {
    let newObj = {};

    this.taskList.forEach((x) => {
      if (!newObj[x.household_member_id]) {
        newObj[x.household_member_id] = [x];
      } else {
        newObj[x.household_member_id].push(x);
      }
    }
    );

    Object.keys(newObj).forEach((key, index) => {
      var first_name  = newObj[key][0].household_member_first_name === null ? '' : newObj[key][0].household_member_first_name ;
      var middle_name  = newObj[key][0].household_member_middle_name === null ? '' : newObj[key][0].household_member_middle_name ;
      var last_name  = newObj[key][0].household_member_last_name === null ? '' : newObj[key][0].household_member_last_name ;

      var name = first_name + ' '+ middle_name +' '+last_name;

      if (!this.tmpAccordionKey.includes(key)) {
        this.houseHoldMemberChecklistFormGroup.setControl('task' + index, this._formBuilder.array([]));
        this.config.panels.push({ name: key, text: name, description: this.activityCompleteStatus });
        this.tmpAccordionKey.push(key);
      }

      const control = <FormArray>this.houseHoldMemberChecklistFormGroup.controls['task' + index];

      newObj[key].forEach((x) => {
        control.push(this.buildTaskForm(x));
        if (x.status === '' ||  x.status === null) {
          this.config.panels[index].description = 'Incomplete';
        }
      });


    });
  }



  saveTask(index) {
    const control = <FormArray>this.houseHoldMemberChecklistFormGroup.controls['task' + index];
    const  req = {
      checklist: control.getRawValue(),
      applicant_id: this.applicantId
    };

    this._commonHttpService.create(req, 'publicproviderhouseholdmember/updatememberchecklist').subscribe(
      (response) => {
        this._alertService.success('CheckList Updated successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        return false;
      }
    );

  }

}
