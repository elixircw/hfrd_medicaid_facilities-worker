import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseHoldMembersChecklistComponent } from './house-hold-members-checklist.component';

describe('HouseHoldMembersChecklistComponent', () => {
  let component: HouseHoldMembersChecklistComponent;
  let fixture: ComponentFixture<HouseHoldMembersChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseHoldMembersChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseHoldMembersChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
