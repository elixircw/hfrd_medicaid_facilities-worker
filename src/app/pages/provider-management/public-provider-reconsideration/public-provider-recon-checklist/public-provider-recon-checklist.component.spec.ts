import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderReconChecklistComponent } from './public-provider-recon-checklist.component';

describe('PublicProviderReconChecklistComponent', () => {
  let component: PublicProviderReconChecklistComponent;
  let fixture: ComponentFixture<PublicProviderReconChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderReconChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderReconChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
