import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, AuthService, DataStoreService } from '../../../@core/services';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingUser } from '../../provider-referral/_entities/existingreferralModel';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { AppUser } from '../../../@core/entities/authDataModel';
import { ProviderReferral } from '../new-provider/_entities/newApplicantModel';
declare var $: any;
@Component({
  selector: 'public-provider-reconsideration',
  templateUrl: './public-provider-reconsideration.component.html',
  styleUrls: ['./public-provider-reconsideration.component.scss']
})
export class PublicProviderReconsiderationComponent implements OnInit {

   
  constructor(private _commonHttpService: CommonHttpService,private _router: Router,
    private _authService: AuthService, private _alertService: AlertService,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService
    ) {

     }
  
  referralInfoDetails: ProviderReferral = new ProviderReferral() ;
  reconId: string;
  applicants: any;
  assignedInSection= [];
  isLengthGrater:boolean;
  currentData: any;
  selectedPerson: any;
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  isSupervisor: boolean;
  asssignedDetails:any;
  currentUser: AppUser;
  providerId: string;


  ngOnInit() {
    this.currentUser = this._authService.getCurrentUser();
    this.reconId = this.route.snapshot.params['id'];
    // this.getProviderDetails();
    this.assignDetails();
    this.getInformation() ;

   this.userNames();
  }
  private getProviderDetails() {
            
    this._commonHttpService.getArrayList(
      {
          method: 'get',
          nolimit: true,
          where: {"provider_id":this.providerId}  
              },'providerinfo?filter', 
      ).subscribe(applicants => {
        this.applicants = applicants
        this._dataStoreService.setData('publicProviderInfo', applicants);

        console.log("Applicants:"+JSON.stringify(this.applicants));

        this.referralInfoDetails.provider_referral_nm=this.applicants[0].provider_nm;
        this.referralInfoDetails.provider_first_nm=this.applicants[0].provider_first_nm;
        this.referralInfoDetails.provider_middle_nm=this.applicants[0].provider_middle_nm;
        this.referralInfoDetails.provider_last_nm=this.applicants[0].provider_last_nm;
        this.referralInfoDetails.provider_status_cd=this.applicants[0].provider_status_cd;
        this.referralInfoDetails.is_taxid=this.applicants[0].tax_id_no;
        this.referralInfoDetails.adr_cell_phone_tx=this.applicants[0].adr_work_phone_tx;
        this.referralInfoDetails.adr_county_cd=this.applicants[0].county_cd;
        this.referralInfoDetails.referral_status=this.applicants[0].provider_status_cd =="1791"?"Open":"Closed";
      }); 
       }

      private assignDetails(){
        this.asssignedDetails={
          eventcode:'PAOW',
          fromsecurityusersid:'',
          tosecurityusersid:'',
          fromroleid:'',
          toroleid:'',
          objectid:this.reconId,
        }
      }

       private userNames() {
        this.assignedInSection = [];
        
        this._commonHttpService.create(
            {
              method:'post',
              where: {
                        eventcode : 'PAOW',
                        objectid:this.reconId
              } 
            },
            'providerassignmentownership/getassignownership'
            )
          .subscribe(
                (response) => { 
                  console.log("$$$$$$$$$$", response[0]); 
                  console.log("$$$$$$$$$$", response.length); 
                  if(response.length>=2){
                    this.isLengthGrater=true
                  };
                  console.log("Rahul before assigned in section"+this.assignedInSection);
                   for(var i=0;i<=response.length;i++){ 
                  this.currentData={"username": response[i].ownername};
                   this.assignedInSection.push(this.currentData);
                   console.log("$$$$$$$$$$", this.assignedInSection); 
                }
                  // this.assignedInSection=[{"username": response[0].ownername}];
                  // this.assignedInSection=[{"username": response[1].ownername}];
             },(error) => {
                        this._alertService.error('Unable to fetch saved Applicant details.');
                        console.log('Get applicant details Error', error);
                        return false;
                            }
                            
                        );
             }
    

             listUser(assigned: string) {
              this.selectedPerson = '';
              this.getUsersList = [];
              this.getUsersList = this.originalUserList;
              if (assigned === 'TOBEASSIGNED') {
                this.getUsersList = this.getUsersList.filter(res => {
                  if (res.issupervisor === false) {
                    this.isSupervisor = false;
                    return res;
                  }
                });
              }
          }
      

          getRoutingUser(modal) {
            //this.getServicereqid = modal.provider_referral_id;
          //  this.getServicereqid = "R201800200374";
            // console.log(modal.provider_referral_id);
            // this.zipCode = modal.incidentlocation;
            this.getUsersList = [];
            this.originalUserList = [];
            // this.isGroup = modal.isgroup;
            this._commonHttpService
              .getPagedArrayList(
                new PaginationRequest({
                  where: { appevent: 'PTA' },
                  method: 'post'
                }),
                'providerassignmentownership/getroutingusers'
              )
              .subscribe(result => {
                // this.userNames();
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
              });
          }
        

          selectPerson(row) {
            this.selectedPerson = row;
            
            // THIS IS THE PERSON LOGGED IN WHICH IS (FROM)

            this.asssignedDetails.fromsecurityusersid=this.currentUser.user.securityusersid;
            this.asssignedDetails.fromroleid=this.currentUser.role.name;
            console.log("@@@@@@@@@@@",this.currentUser );
            
            // THIS IS THE PERSON WE SELECTED ON THE SCREEN WHICH IS (TO)
            this.asssignedDetails.tosecurityusersid=row.userid;
            this.asssignedDetails.toroleid=row.userrole;
          
        }      
        private getInformation() {
          this._commonHttpService.getArrayList(
            {
              method: 'get',
              where : { recon_number : this.reconId }
            },
            'publicproviderreconsideration?filter'
          ).subscribe(response => {
            this.providerId = response[0].object_id	
            this.getProviderDetails();
          });
        }
       
}
