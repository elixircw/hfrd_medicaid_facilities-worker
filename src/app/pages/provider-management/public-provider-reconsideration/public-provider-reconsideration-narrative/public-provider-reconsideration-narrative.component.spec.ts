import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderReconsiderationNarrativeComponent } from './public-provider-reconsideration-narrative.component';

describe('PublicProviderReconsiderationNarrativeComponent', () => {
  let component: PublicProviderReconsiderationNarrativeComponent;
  let fixture: ComponentFixture<PublicProviderReconsiderationNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderReconsiderationNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderReconsiderationNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
