import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicProviderReconsiderationRoutingModule } from './public-provider-reconsideration-routing.module';
import { PublicProviderReconsiderationComponent } from './public-provider-reconsideration.component';
import { PublicProviderReconsiderationContactComponent } from './public-provider-reconsideration-contact/public-provider-reconsideration-contact.component';
import { PublicProviderReconsiderationDecisionComponent } from './public-provider-reconsideration-decision/public-provider-reconsideration-decision.component';
import { PublicProviderReconsiderationNarrativeComponent } from './public-provider-reconsideration-narrative/public-provider-reconsideration-narrative.component';
import {
  MatCardModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatStepperModule
} from '@angular/material';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaskModule } from 'ngx-mask';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { PublicProviderReconChecklistComponent } from './public-provider-recon-checklist/public-provider-recon-checklist.component';
import { HouseHoldChecklistComponent } from './public-provider-recon-checklist/house-hold-checklist/house-hold-checklist.component';
import { HouseHoldMembersChecklistComponent } from './public-provider-recon-checklist/house-hold-members-checklist/house-hold-members-checklist.component';
import { PublicProviderReconDocCreatorComponent } from './public-provider-recon-doc-creator/public-provider-recon-doc-creator.component';
import { AttachmentDetailComponent } from './add-document/attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './add-document/attachment-upload/attachment-upload.component';
import { AudioRecordComponent } from './add-document/audio-record/audio-record.component';
import { EditAttachmentComponent } from './add-document/edit-attachment/edit-attachment.component';
import { ImageRecordComponent } from './add-document/image-record/image-record.component';
import { VideoRecordComponent } from './add-document/video-record/video-record.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { HomeStudySurveyComponent } from './home-study-survey/home-study-survey.component';

@NgModule({
  imports: [
    CommonModule,
    PublicProviderReconsiderationRoutingModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    NgxfUploaderModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgSelectModule,
    NgxPaginationModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    NgxMaskModule.forRoot(),
    MatStepperModule
  ],
  declarations: [PublicProviderReconsiderationComponent, PublicProviderReconsiderationContactComponent, PublicProviderReconsiderationDecisionComponent, PublicProviderReconsiderationNarrativeComponent, PublicProviderReconChecklistComponent, HouseHoldChecklistComponent, HouseHoldMembersChecklistComponent, PublicProviderReconDocCreatorComponent,
    AttachmentDetailComponent, 
    AttachmentUploadComponent, 
    AudioRecordComponent,
    EditAttachmentComponent,
    ImageRecordComponent,
    VideoRecordComponent,
    AddDocumentComponent,
    HomeStudySurveyComponent,
    ],
  providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService],

})
export class PublicProviderReconsiderationModule { }
