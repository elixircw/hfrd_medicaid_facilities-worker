import { expand } from 'rxjs/operators/expand';

export class Attachment {
    filename: string;
    Documentproperties: string;
    Documentattachment: string;
    documentpropertiesid: string;
    documenttypekey: string;
    documentdate: Date;
    title: string;
    mime: string;
    insertedby: string;
    insertedon: Date;
    documentattachment: AttachmentType;
    userprofile: UserProfile;
    s3bucketpathname: string;
    description: string;
    originalfilename: string;
}
export class AttachmentType {
    documentpropertiesid: string;
    attachmenttypekey: string;
    attachmentclassificationtypekey: string;
    updatedby: string;
    attachmentdate: Date;
}

export class UserProfile {
    securityusersid: string;
    firstname: string;
    lastname: string;
    displayname: string;
}
export class applinfo
{
    applicant_id: string;
    program_name: string;
    corporation_name: string;
    license_type: string;
    contact_person: string;
    mailing_address: string;
    contact_phonenumber: string;
    contact_email: string;
    contact_fax: string;
    program_type: string;
    program_type_other: string;
    prgram_size: string;
    corporation_type: string;
    current_license: string;
    no_of_youth_male: string;
    no_of_youth_female: string;
    age_range_of_youth_from: string;
    age_range_of_youth_to: string;
    population_served: string;
    iq_range_from: string;
    iq_range_to: string;
    licenced_by_dda: string;
    facesheet_dev_by_consultant: string;
    minimum_age_no : string ;
    maximum_age_no : string;
    gender_cd : string;
    children_no : string;
    
    application_status: string;
    license_no: string;
}
