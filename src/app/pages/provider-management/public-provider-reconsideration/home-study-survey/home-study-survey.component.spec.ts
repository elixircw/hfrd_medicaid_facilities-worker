import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeStudySurveyComponent } from './home-study-survey.component';

describe('HomeStudySurveyComponent', () => {
  let component: HomeStudySurveyComponent;
  let fixture: ComponentFixture<HomeStudySurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeStudySurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeStudySurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
