import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService, CommonHttpService, AuthService, CommonDropdownsService } from '../../../../@core/services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';

@Component({
  selector: 'license-requests',
  templateUrl: './license-requests.component.html',
  styleUrls: ['./license-requests.component.scss']
})
export class LicenseRequestsComponent implements OnInit {
  changeRequestForm: FormGroup;
  requestNarrativeForm: FormGroup;
  requestList = [];
  providerId: string;
  requestType = [];
  availableSiteList: any[] = [];
  licenseInformation: any;
  selectedSiteLicenseInformation: any;
  selectedSiteId: string;
  selectedLicenseNo: string;
  tableData = [];
  existingLicenseInfo = [];
  isLicenseChange: boolean;
  hasFormSignature: Boolean = false;
  isWaiver: Boolean = false;
  isVariance: Boolean = false;
  isClousure = false;
  stateList$: Observable<any[]>;
  variancetypeList: any = ['New variance request', 'Renewal of current variance'];
  waivertypeList: any = ['New waiver request', 'Renewal of current waiver'];
  genderList: any = ['Male', 'Female'];
  currentDate = new Date();
  expirationMinDate: any;
  effectiveMaxDate: any;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _authService: AuthService,
    private _commonDropdownService: CommonDropdownsService) {
    this.providerId = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.initChangeRequestForm();
    this.initRequestNarrativeForm()
    this.getChangeRequestList();
    this.getGenderList();
    this.getProviderContractLicenseTypes();
    this.loadDropDown();
    this.requestType = ["Voluntary Closure", "License Change", "Waiver", "Variance"];
    this.isLicenseChange = false;
    //this.getChangeRequestNarrative();
  }

  initChangeRequestForm() {
    this.changeRequestForm = this.formBuilder.group({
      request_no: [''],
      provider_id: this.providerId,
      request_comments: [''],
      request_type: [''],
      license_no: [''],
      site_id: [''],

      min_age_to: [''],
      max_age_to: [''],
      gender_to: [''],
      children_no_to: [''],
      // min_age_from: [{value:'',disabled:true}],
      // max_age_from: [{value:'',disabled:true}],
      // gender_from: [{value:'',disabled:true}],
      // children_no_from: [{value:'',disabled:true}]

      min_age_from: [''],
      max_age_from: [''],
      gender_from: [''],
      children_no_from: [''],
      signimage: [''],
      program_address: [''],
      comar_citation: [''],
      program_phone: [''],
      program_fax: [''],
      variance_type: [''],
      waiver_type: [''],
      state: [''],
      city: [''],
      zip_no: [null],
      request_date: new Date(),
      completing_person_name: [''],
      person_name: [''],
      dob: [null],
      gender: [''],
      reason_for_request_variance: [''],
      reason_for_request_waiver: [''],
      alternate_measures: [''],
      request_effectivedate: new Date(),
      request_expirationdate: new Date(),
      continuous: ['']
    });
  }

  initRequestNarrativeForm() {
    this.requestNarrativeForm = this.formBuilder.group({
      request_no: [''],
      request_comments: [''],
      request_narrative_by: this.providerId
    });
  }



  openChangeRequest() {
    this.changeRequestForm.reset();
    this.changeRequestForm.patchValue({
      provider_id: this.providerId
    });
    this._commonHttpService.getArrayList(
      {},
      'Nextnumbers/getNextNumber?apptype=providerportalrequest'
    ).subscribe(result => {
      //console.log('Request Number', result);
      this.changeRequestForm.patchValue({
        request_no: result['nextNumber']
      });
    });
    this.isLicenseChange = false;
    this.hasFormSignature = false;
    this.isVariance = false;
    this.isWaiver = false;
    this.isClousure = false;
  }

  getChangeRequestList() {
    this._commonHttpService.getPagedArrayList(
      {
        method: 'post',
        nolimit: true,
        where: {
          provider_id: null,
          site_id: this.providerId
        },
        order: 'create_ts desc'
      },
      'providerportalrequest/getlicenseinfo?filter'
    ).subscribe(
      (response) => {
        if (Array.isArray(response.data) && response.data.length) {
          this.requestList = response.data[0].getlicenseinfo;
        }
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }

  private getProviderContractLicenseTypes() {
    this.licenseInformation = {};
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          provider_id: this.providerId
        }
      },
      'providercontract/getproviderlicenseinformation'
    ).subscribe(response => {
      this.licenseInformation = response;
      if (this.licenseInformation.data) {
        this.licenseInformation.data.map((item) => {
          let availableSite = {};
          availableSite['text'] = item.site_id + ' - ' + item.provider_nm + '-' + item.license_type;
          availableSite['value'] = item.site_id;
          this.availableSiteList.push(availableSite);
        });
      }
    });
  }
  changeSiteId(event) {
    this.selectedSiteId = event.value;
    //this.selectedLicenseNo;
    this.getSiteLicenseInformation();
    this.getExistingLicenseInformation();
  }
  selectRequestType(event) {
    if (event.value === 'License Change') {
      this.isLicenseChange = true;
      this.hasFormSignature = false;
      this.isWaiver = false;
      this.isVariance = false;
    } else if (event.value === 'Waiver') {
      this.isWaiver = true;
      this.isVariance = false;
      this.isLicenseChange = false;
      this.hasFormSignature = true;
    } else if (event.value === 'Variance') {
      this.isWaiver = false;
      this.isVariance = true;
      this.isLicenseChange = false;
      this.hasFormSignature = true;
    } else if (event.value === 'Voluntary Closure') {
      this.isWaiver = false;
      this.isVariance = false;
      this.isLicenseChange = false;
      this.hasFormSignature = false;
      this.isClousure = true;
    } else {
      this.isWaiver = false;
      this.isVariance = false;
      this.isLicenseChange = false;
      this.hasFormSignature = false;
    }
  }

  getSiteLicenseInformation() {
    this.selectedSiteLicenseInformation = {};
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          objectid: this.selectedSiteId
        }
      },
      'providerlicense/getproviderlicensing'
    ).subscribe(response => {
      this.selectedSiteLicenseInformation = response;
      if (this.selectedSiteLicenseInformation.data[0]) {
        this.selectedSiteLicenseInformation = this.selectedSiteLicenseInformation.data[0];
        this.selectedLicenseNo = this.selectedSiteLicenseInformation.license_no;
        this.patchSiteLicenseInformation();
      }
    },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }

  patchSiteLicenseInformation() {
    this.changeRequestForm.patchValue({
      license_no: this.selectedSiteLicenseInformation.license_no,
      program_address: this.selectedSiteLicenseInformation.formatted_address,
      program_phone: this.selectedSiteLicenseInformation.adr_work_phone_tx,
      program_fax: this.selectedSiteLicenseInformation.contact_fax,
      // gender_from : this.selectedSiteLicenseInformation.gender_cd,
      // max_age_from : this.selectedSiteLicenseInformation.maximum_age_no,
      // min_age_from : this.selectedSiteLicenseInformation.minimum_age_no,
      // children_no_from  : this.selectedSiteLicenseInformation.children_no
    });
  }

  saveInformation() {
    console.log('this.changeRequestForm.value', this.changeRequestForm.value);
    let changeRequestForm = this.changeRequestForm.getRawValue();
    changeRequestForm.request_effectivedate = changeRequestForm.request_effectivedate ? changeRequestForm.request_effectivedate : new Date();
    changeRequestForm.request_date = changeRequestForm.request_date ? changeRequestForm.request_date : new Date();
    changeRequestForm.request_expirationdate = changeRequestForm.request_expirationdate ? changeRequestForm.request_expirationdate : new Date();
    changeRequestForm.dob = changeRequestForm.dob ? changeRequestForm.dob : new Date();
    this._commonHttpService.create(
      changeRequestForm,
      'providerportalrequest'
    ).subscribe(
      (response) => {
        this.routeToUser(response);
        (<any>$('#change-request')).modal('hide');
        this.getChangeRequestList();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    this.requestNarrativeForm.patchValue(this.changeRequestForm.getRawValue());
    this.sendCommentsToWorker();
    this.changeRequestForm.reset();
    this.changeRequestForm.patchValue({
      provider_id: this.providerId
    });
    this.isLicenseChange = false;
  }

  sendCommentsToWorker() {

    var payload = this.requestNarrativeForm.value;
    // payload['request_no'] = currentRequestNo;
    // payload['request_comments'] = currentRequestNarrative;
    // payload['request_narrative_by'] = this.providerId;

    this._commonHttpService.create(
      payload,
      'providerportalrequestnarrative'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        (<any>$('#additional-info')).modal('hide');
        //this.getChangeRequestNarrative();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
  }


  // Additional info narrative
  requestNarrative(request) {
    this.getChangeRequestNarrative(request.request_no);
  }

  getChangeRequestNarrative(selectedRequestNo: string) {

    this.requestNarrativeForm.patchValue({
      request_no: selectedRequestNo
    });

    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: {
          request_no: selectedRequestNo //this.changeRequestForm.value.request_no
        },
        order: 'create_ts asc'
      },
      'providerportalrequestnarrative?filter'
    ).subscribe(
      (response) => {
        this.tableData = response;
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }

  getExistingLicenseInformation() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          objectid: this.selectedSiteId
        }
      },
      'providerlicense/getproviderlicensing'
    ).subscribe(response => {
      this.existingLicenseInfo = response.data;
      if (this.existingLicenseInfo[0]) {
        this.changeRequestForm.patchValue({
          min_age_from: this.existingLicenseInfo[0].minimum_age_no,
          max_age_from: this.existingLicenseInfo[0].maximum_age_no,
          gender_from: this.existingLicenseInfo[0].gender_cd,
          children_no_from: this.existingLicenseInfo[0].children_no
        });
      }
      console.log("Rahul get license info", JSON.stringify(this.existingLicenseInfo));
    },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }
  private loadDropDown() {
    const source = forkJoin([

      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        'States?filter'
      )
    ])
      .map((result) => {
        return {

          stateList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();

    this.stateList$ = this._commonDropdownService.getPickListByName('state');
  }

  getGenderList() {
    this._commonHttpService.getArrayList(
      {
        where: { tablename: 'gender', teamtypekey: 'OLM' },
        method: 'get',
        nolimit: true
      },
      'referencetype/gettypes?filter'
    ).subscribe(result => {
      this.genderList = result;
    });
  }

  onChangeEffectiveDate(value) {
    const formData = this.changeRequestForm.getRawValue();
    const expirationMinDate = formData.request_effectivedate;
    this.expirationMinDate = expirationMinDate;
  }

  onChangeExpirationDate(value) {
    const formData = this.changeRequestForm.getRawValue();
    const effectiveMaxDate = formData.request_expirationdate;
    this.effectiveMaxDate = effectiveMaxDate;
  }

  routeToUser(requestDetail) {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          eventcode: 'LIC',
          objectid: requestDetail.request_no,
          fromroleid: this._authService.getCurrentUser().role.name,
          toroleid: 'OLMQA',
          remarks: 'Provider initated the request',
          status : 1
        }
      },
      'providerportalrequest/providerrouting'
    ).subscribe(response => {
      console.log(response);
    });

  }
}
