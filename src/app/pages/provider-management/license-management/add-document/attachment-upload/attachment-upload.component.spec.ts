import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { NgxMaskModule } from 'ngx-mask';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';

import { CoreModule } from '../../../../../@core/core.module';
import { SharedPipesModule } from '../../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../../shared/modules/control-messages/control-messages.module';
import { AttachmentRoutingModule } from '../../../../case-worker/dsds-action/attachment/attachment-routing.module';
import { AttachmentUploadComponent } from './attachment-upload.component';

describe('AttachmentUploadComponent', () => {
    let component: AttachmentUploadComponent;
    let fixture: ComponentFixture<AttachmentUploadComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                CommonModule,
                AttachmentRoutingModule,
                FormsModule,
                ReactiveFormsModule,
                A2Edatetimepicker,
                ControlMessagesModule,
                NgSelectModule,
                SharedPipesModule,
                NgxfUploaderModule.forRoot(),
                NgxMaskModule.forRoot()
            ],
            declarations: [AttachmentUploadComponent],
            providers: [NgxfUploaderService]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AttachmentUploadComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
