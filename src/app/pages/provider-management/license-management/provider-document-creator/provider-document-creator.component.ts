import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as jsPDF from 'jspdf';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { AuthService } from '../../../../@core/services/auth.service';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { getDate } from 'date-fns';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { applinfo } from '../_entities/newApplicantModel';

const APPOINTMENT_COMPLETED = 'Completed';
@Component({
  selector: 'provider-document-creator',
  templateUrl: './provider-document-creator.component.html',
  styleUrls: ['./provider-document-creator.component.scss']
})
export class ProviderDocumentCreatorComponent implements OnInit {

  @Input()
  profileOutputSubject$ = new Subject<applinfo>();
  licenseTypeName:string;
  licenseLevel: string;

  executiveDirectorSecurityUserId: string;
  executiveDirectorSignImage: any;
  // Provider License Documnet Generator
  licenseInfo : any;
  letterInfo : any;
  providerId: any;


  licensing_administrator: string;
  license_number: string;
  license_effective_date: string;

  license_to: string;
  license_located_at: string;
  license_age: string;
  license_sex: string;
  license_number_of_children: string;
  license_other: string;
  license_by: string;
  license_title: string;
  license_expiration_date: string;
  license_issue_date: string;


  //DJS liscense 
  DJS_liscense_Issued_to:string;
  DJS_liscense_Program:string;
  DJS_liscense_Capacity:string;
  DJS_liscense_Premises_name:string;
  DJS_liscense_Address:string;
  DJS_liscense_Valid_years:string;
  DJS_liscense_Beginning_date:string;
  DJS_liscense_Ending_date:string;

//Memo Appleal hearing
MAH_name_of_organization:string;
MAH_license_type:string;
MAH_action:string;
MAH_program_manager_name:string;
MAH_program_manager_phone:string;


//Memo Appleal Notice
MAN_name_of_organization:string;
MAN_license_type:string;
MAN_action:string;
MAN_program_manager_name:string;
MAN_program_manager_phone:string;

//office admin hearing
OAH_name_of_agency:string;
OAH_name_of_program:string;
OAH_licensing_coordinator_name:string;
OAH_licensing_coordinator_phone:string;
OAH_licensing_coordinator_email:string;
OAH_qa_coordinator:string;
OAH_OLM_program_manager:string;

//Appeal request form
ARF_program:string;
ARF_license_action:string;

  


  isViewable: boolean;

  // tslint:disable-next-line:no-input-rename
  persons: any[];
  general: any;
  id: string;
  daNumber: string;
  preIntakeDisposition: any;
  documentsToDownload: string[] = [];
  supComments = '';
  reason = '';
  comments = '';
  addedPersons: any[];
  youth: any;
  downloadInProgress: boolean;
  complaintID: string;
  complaintReceiveDate: string;
  selectedAllegedOffenseIDs: any[];
  allegedOffenseDate: string;
  allAllegedOffense: string;
  offenses: any[];
  loggedInUser: string;
  preintakeAppointmentDate: string;
  finalNotificationDate: Date;
  youthName: string;
  youthDob;
  youthPhoneNumber: string;
  youthId: string;
  victimName: string;
  victimAddress: string;
  victimPhoneNumber: string;
  currentDateString;
  mother: { name: string; phoneNumber: string; address: string };
  father: { name: string; phoneNumber: string; address: string };
  guardian: { name: string; phoneNumber: string; address: string };
  appointmentHeld = false;
  appointmentNotes = '';
  offenseString: string;
  parentOrGaurdianAddress: string;
  parentOrGaurdianName: string;

  petitionID: string;
  evalFields: any;
  appointments: any = [];

  fatherObj: any;
  motherObj: any;
  guardianObj: any;
  youthLastSchool: any;
  appointment: any;

  applicantInfo: any;

  courtDetails: any;
  youthAge: number;
  applicantNumber: string;

  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
  constructor(private route: ActivatedRoute, 
      private _alertService: AlertService,
      private _authService: AuthService, 
      private _commonHttpService: CommonHttpService,
      private _dataStoreService: DataStoreService, 
      private _reportSummaryService: GenericService<any>) {
      this.applicantNumber = route.snapshot.params['id'];
      this.providerId = route.snapshot.params['id'];
  }

  ngOnInit() {
      const currentDate = new Date();
      //this.getLetterInformation();
      currentDate.setDate(currentDate.getDate() + 10);
      this.finalNotificationDate = currentDate;
      this.loggedInUser = this._authService.getCurrentUser().user.userprofile.displayname;

      this.executiveDirectorSecurityUserId = 'c0597522-01ed-40a2-b85d-7d83eb8c6768';
      this.getLicenseInformation();
      this.isViewable = false;

      // this.license_to= this.licenseInfo.contact_person;
      // this.license_located_at= "2601 Nprth Howard Street, Suite 200, Baltimore, Maryland 21218";
      // this.license_age= this.licenseInfo.minimum_age_no + '-' + this.licenseInfo.maximum_age_no;
      // this.license_sex= this.licenseInfo.gender_cd;
      // this.license_number_of_children= this.licenseInfo.children_no;
      // this.license_other= this.licenseInfo.provider_nm;
      // this.license_by= "Jim Butler";
      // this.license_title= "Executive Director";
      // this.license_expiration_date= this.licenseInfo.license_expiry_dt;
      // this.license_effective_Date= this.licenseInfo.license_issue_dt;

      this.DJS_liscense_Issued_to="Rite of Passage, Inc.";
      this.DJS_liscense_Program="Private Residential Child Care Program";
      this.DJS_liscense_Capacity="96 male";
      this.DJS_liscense_Premises_name="Silver Oak Acadamy";
      this.DJS_liscense_Address="999 Crouse Mill Road, Keymar,Maryland 21757";
      this.DJS_liscense_Valid_years="Two (2)";
      this.DJS_liscense_Beginning_date="July 17, 2017";
      this.DJS_liscense_Ending_date="July 16,2019,";

      
      // this.profileOutputSubject$.subscribe((data) => {
      //     if(data){
      //       this.applicantInfo = data; 
      //     }
      //   });
      //this.MAH_name_of_organization= this.applicantInfo.provider_nm;
      //this.MAH_license_type="CHILD PLACEMENT AGENCY";
      this.MAH_action="SANCTION";
      this.MAH_program_manager_name="IAN WIGGS";
      this.MAH_program_manager_phone="999-999-9999";


      //this.MAN_name_of_organization="NAME OF PRIVATE ORGANIZATION";
      //this.MAN_license_type=this.MAH_license_type;
      this.MAN_action="SANCTION";
      this.MAN_program_manager_name="IAN WIGGS";
      this.MAN_program_manager_phone="999-999-9999";


      //this.OAH_name_of_agency="NAME OF AGENCY";
      //this.OAH_name_of_program="Child Placement Agency";
      this.OAH_licensing_coordinator_name="TOM LITTLE";
      this.OAH_licensing_coordinator_phone="000-000-0000";
      this.OAH_licensing_coordinator_email="abcd@gmail.com";
      this.OAH_qa_coordinator="JASON WEST";
      this.OAH_OLM_program_manager="Name of Licensing Coordinator’s OLM Program Manager";

      //this.ARF_program= "child placement agency";
      this.ARF_license_action= "impose sanction";


      //Get Licensing information
      // this.currentDateString = new Date();
      // this.licensing_administrator  = 'Ian Wiggs';
      // this.license_number = 'DHS12345';
      // this.license_effective_date = '03/03/2019';
      // this.license_end_date = 'N/A';

      this._dataStoreService.currentStore.subscribe((store) => {
          
          if (store['documentsToDownload']) {
              this.documentsToDownload = store['documentsToDownload'];
              this.id = this.route.snapshot.parent.parent.params['id'];
              this.daNumber = this.route.snapshot.parent.parent.params['daNumber'];
              this.listReportSummary();
              // this.collectivePdfCreator();
          }
          if (store['dsdsActionsSummary']) {
              const actionSummary = store['dsdsActionsSummary'];
              const jsonData = actionSummary['intake_jsondata'];
              if (jsonData) {
                  this.processIntakeData(jsonData);
              }
          }
      });
  }

  private listReportSummary() {
      this._reportSummaryService.getSingle(new PaginationRequest({}), CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.ReportSummary + this.id).subscribe((result) => {
          this.offenseString = result.intakeservicerequestevaluation.allegations;
      });
  }
  processIntakeData(intakeData: any) {
      if (intakeData) {
          this.persons = intakeData.persons;
          this.evalFields = intakeData.evaluationFields;
          this.general = intakeData.General;
          this.courtDetails = intakeData.courtDetails;
          if (intakeData.appointments && intakeData.appointments.length > 0) {
              this.appointments = intakeData.appointments;
              this.preintakeAppointmentDate = intakeData.appointments[0].appointmentDate;

              this.appointment = this.appointments[0];
          }

          this.resetInputs();
          this.processInputs();
      }
  }

  resetInputs() {
      this.mother = { name: '', address: '', phoneNumber: '' };
      this.father = { name: '', address: '', phoneNumber: '' };
      this.guardian = { name: '', address: '', phoneNumber: '' };
  }

  processInputs() {
      const mother = this.getPersonByRelation('mother');
      const father = this.getPersonByRelation('father');
      const guardian = this.getPersonByRelation('guardian');
      this.currentDateString = this.currentDate();

      const person = this.getPerson('Youth');
      if (person) {
          this.youthName = person.fullName;
          this.youthDob = person.Dob;
          this.youthPhoneNumber = person.primaryPhoneNumber;
          if (person) {
              this.youthId = person.Pid.substr(person.Pid.length - 8).toUpperCase();
          }
      }

      if (mother) {
          this.mother.name = mother.fullName;
          this.mother.address = this.getPersonAddress(mother);
          this.mother.phoneNumber = mother.primaryPhoneNumber;
      }
      if (father) {
          this.father.name = father.fullName;
          this.father.address = this.getPersonAddress(father);
          this.father.phoneNumber = father.primaryPhoneNumber;
      }
      if (guardian) {
          this.guardian.name = guardian.fullName;
          this.guardian.address = this.getPersonAddress(guardian);
          this.guardian.phoneNumber = guardian.primaryPhoneNumber;
      }
      if (this.appointments && this.appointments.length > 0) {
          this.preintakeAppointmentDate = this.appointments[0].appointmentDate;
          this.appointmentHeld = this.appointments[0].status === APPOINTMENT_COMPLETED;
          this.appointmentNotes = this.appointments[0].notes;
      }
      const victim = this.getPerson('Victim');
      if (victim) {
          this.victimName = victim.fullName;
          this.victimAddress = this.getPersonAddress(victim);
          this.victimPhoneNumber = victim.primaryPhoneNumber;
      }

      this.parentOrGaurdianAddress = this.getParentOrGaurdianAddress();
      this.parentOrGaurdianName = this.getParentOrGaurdianName();

      this.youth = this.getPerson('Youth');
      this.youthAge = this.getYouthAge();
      console.log('youth', this.youth);

      this.fatherObj = this.getPersonByRelation('father');
      this.motherObj = this.getPersonByRelation('mother');
      this.guardianObj = this.getPersonByRelation('guardian');

      if (this.youth) {
          const youthSchools = this.youth.school;
          if (youthSchools && youthSchools.length > 0) {
              this.youthLastSchool = youthSchools[youthSchools.length - 1];
          }
      }
  }

  private getYouthAge() {
      if (!this.youth || (this.youth && !this.youth.Dob)) {
          return null;
      }
      let youthDob;
      let offenceDate;
      youthDob = new Date(this.youth.Dob);

      offenceDate = new Date();

      const timeDiff = offenceDate - youthDob;
      const youthAge = new Date(timeDiff); // miliseconds from epoch
      return Math.abs(youthAge.getUTCFullYear() - 1970);
  }

  getPersonAddress(person) {
      return `${
          person.personAddressInput[0]
              ? `${person.personAddressInput[0].address}, ${person.personAddressInput[0].address2}<br>
                  ${person.personAddressInput[0].city}, ${person.personAddressInput[0].county}<br>
                  ${person.personAddressInput[0].zipcode}`
              : `<br><br><br>`
      }`;
  }

  generateOffenseString(): string {
      if (this.evalFields && this.evalFields.allegedoffense && this.offenses) {
          const selectedIDs = this.evalFields.allegedoffense.map((offense) => offense.allegationid);

          return this.offenses
              .filter((offense) => {
                  return selectedIDs.indexOf(offense.allegationid) !== -1;
              })
              .map((offense) => offense.name)
              .toString()
              .replace(',', ', ');
      }

      return '';
  }

  getVictimName() {
      const person = this.getPerson('Victim');
      if (person) {
          return person.fullName;
      }
      return '';
  }

  getPerson(Role: string): any {
      if (this.persons) {
          return this.persons.find((person) => person.Role === Role);
      }
      return null;
  }

  getPersonByRelation(Relationship: string): any {
      if (this.persons) {
          return this.persons.find((person) => person.RelationshiptoRA === Relationship);
      }
      return null;
  }

  getParentOrGaurdianName() {
      const father = this.getPersonByRelation('father');
      const mother = this.getPersonByRelation('mother');
      const guardian = this.getPersonByRelation('guardian');

      if (mother) {
          return mother.fullName;
      } else if (father) {
          return father.fullName;
      } else if (guardian) {
          return guardian.fullName;
      }
  }

  getParentOrGaurdianAddress() {
      const father = this.getPersonByRelation('father');
      const mother = this.getPersonByRelation('mother');
      const guardian = this.getPersonByRelation('guardian');

      if (mother) {
          return `${this.getParentOrGaurdianName()}<br>
                  ${
                      mother.personAddressInput[0]
                          ? `${mother.personAddressInput[0].address}, ${mother.personAddressInput[0].address2}<br>
                  ${mother.personAddressInput[0].city}, ${mother.personAddressInput[0].county}<br>
                  ${mother.personAddressInput[0].zipcode}`
                          : `<br><br><br>`
                  }`;
      } else if (father) {
          return `${this.getParentOrGaurdianName()}<br>
                  ${
                      father.personAddressInput[0]
                          ? `${father.personAddressInput[0].address}, ${father.personAddressInput[0].address2}<br>
                  ${father.personAddressInput[0].city}, ${father.personAddressInput[0].county}<br>
                  ${father.personAddressInput[0].zipcode}`
                          : `<br><br><br>`
                  }`;
      } else if (guardian) {
          return `${this.getParentOrGaurdianName()}<br>
                  ${
                      guardian.personAddressInput[0]
                          ? `${guardian.personAddressInput[0].address}, ${guardian.personAddressInput[0].address2}<br>
                  ${guardian.personAddressInput[0].city}, ${guardian.personAddressInput[0].county}<br>
                  ${guardian.personAddressInput[0].zipcode}`
                          : `<br><br><br>`
                  }`;
      }
  }

  getYouthName() {
      const person = this.getPerson('Youth');
      if (person) {
          return person.fullName;
      }
      return '';
  }
  getYouth(key) {
      const person = this.getPerson('Youth');
      let result = '';
      if (person) {
          result = person[key] ? person[key] : '-';
      }
      return result;
  }

  getVictimAddress() {
      const person = this.getPerson('Alleged Victim');
      if (person) {
          return person.fullAddress;
      }
      return '';
  }

  getMaltreatorName() {
      const person = this.getPerson('Alleged Maltreator');
      if (person) {
          return person.fullName;
      }
      return '';
  }

  getMaltreatorAddress() {
      const person = this.getPerson('Alleged Maltreator');
      if (person) {
          return person.fullAddress;
      }
      return '';
  }

  getPersonID(role) {
      const person = this.getPerson(role);
      if (person && person.Pid) {
          return person.Pid.substr(person.Pid.length - 8).toUpperCase();
      }
      return '';
  }

  appointmentDate() {
      const RecivedDate = new Date(this.general.RecivedDate);
      return RecivedDate.setDate(RecivedDate.getDate() + 7);
  }

  currentDate() {
      return new Date();
  }
  collectivePdfCreator() {
      this.downloadInProgress = true;
      const pdfList = this.documentsToDownload;
      pdfList.forEach((element) => {
          this.downloadCasePdf(element);
      });
  }
  async downloadCasePdf(element: string) {
      const source = document.getElementById(element);
      const pages = source.getElementsByClassName('pdf-page');
      let pageImages = [];
      for (let i = 0; i < pages.length; i++) {
          // console.log(pages.item(i).getAttribute('data-page-name'));
          const pageName = pages.item(i).getAttribute('data-page-name');
          const isPageEnd = pages.item(i).getAttribute('data-page-end');
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
              const img = canvas.toDataURL('image/png');
              pageImages.push(img);
              if (isPageEnd === 'true') {
                  this.pdfFiles.push({ fileName: pageName, images: pageImages });
                  pageImages = [];
              }
          });
      }
      this.convertImageToPdf();
  }

  convertImageToPdf() {
      this.pdfFiles.forEach((pdfFile) => {
          // const doc = new jsPDF('landscape');
          var doc =null;
          if(pdfFile.fileName=="RCC license certificate" || pdfFile.fileName=="DJS license certificate"){
              doc=new jsPDF('landscape');
              }else {
                  doc= new jsPDF();
              }

              var width = doc.internal.pageSize.getWidth()-10;
              var heigth = doc.internal.pageSize.getHeight()-10;
              // alert(width+","+heigth);
              // doc.setFontSize(30);
          pdfFile.images.forEach((image, index) => {
              
              doc.addImage(image, 'JPEG',3,5,width,heigth);
              if (pdfFile.images.length > index + 1) {
                  doc.addPage();
              }
          });
          doc.save(pdfFile.fileName);
      });
      (<any>$('#docu-View')).modal('hide');
      this.pdfFiles = [];
      this.downloadInProgress = false;
  }

  isNotSelected(key) {
      let toHide = true;
      if (this.documentsToDownload) {
          toHide = this.documentsToDownload.indexOf(key) === -1;
      }
      return toHide;
  }

  getLicenseInformation() {
      this._commonHttpService.create(
        {
          method:'post',
          where: 
          {
            objectid :this.providerId
        }        
        },
        'providerlicense/getproviderlicensing'
    ).subscribe(response => {
      console.log(response)
      this.licenseInfo = response.data[0];
      console.log("SIMAR LICENSE INFORMATION")
      console.log(JSON.stringify(this.licenseInfo));
      //console.log(this.licenseInfo.license_no, "license no");

      if(this.licenseInfo.license_level == 'RCC'){
        this.licenseLevel = 'Residential Child Care';
      } else if (this.licenseInfo.license_level == 'CPA') {
        this.licenseLevel = 'Child Placement Agency';
      }
      this.license_to= this.licenseInfo.provider_nm;
      this.license_located_at= this.licenseInfo.formatted_address;
      this.license_age= this.licenseInfo.minimum_age_no + '-' + this.licenseInfo.maximum_age_no;
      this.license_sex= this.licenseInfo.gender_cd;
      this.license_number_of_children= this.licenseInfo.children_no;
      this.license_other= this.licenseInfo.license_type;
      
      // Fix this to be Executive Director who approved the license
      this.license_by= "Jim Butler";
      this.license_title= "Executive Director";
      this.loadSignature();

      
      this.license_expiration_date= this.licenseInfo.license_expiry_dt;
      this.license_effective_date= this.licenseInfo.license_issue_dt;
      this.license_issue_date= this.licenseInfo.license_issue_dt;

      // Approval letter
      this.currentDateString = new Date();
      this.licensing_administrator  = 'Ian Wiggs';
      this.license_number = this.licenseInfo.license_no;
    },
        (error) => {
          this._alertService.error('Unable to get license information, please try again.');
          console.log('get license information Error', error);
          return false;
        }
      );
  }

  updateLicenseDateValues(event) {
      this.license_effective_date = event.license_issue_dt;
      this.license_expiration_date = event.license_expiry_dt;
      console.log("triggered");
      console.log(event);
      console.log(this.license_effective_date, this.license_expiration_date);
  }

  loadSignature() {
    this._commonHttpService.getSingle({method: 'get', securityusersid: this.executiveDirectorSecurityUserId}, 'admin/userprofile/listusersignature?filter')
      .subscribe(res => {
        this.executiveDirectorSignImage = res.usersignatureurl;
        console.log(this.executiveDirectorSignImage);
      });
  }


}
