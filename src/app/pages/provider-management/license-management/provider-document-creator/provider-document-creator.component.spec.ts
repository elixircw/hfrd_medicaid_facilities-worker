import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderDocumentCreatorComponent } from './provider-document-creator.component';

describe('ProviderDocumentCreatorComponent', () => {
  let component: ProviderDocumentCreatorComponent;
  let fixture: ComponentFixture<ProviderDocumentCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderDocumentCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderDocumentCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
