import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { LicenseManagementRoutingModule } from './license-management-routing.module';
import { LicenseManagementComponent } from './license-management.component';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatStepperModule
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill';
import { LicenseInformationComponent } from './license-information/license-information.component';
import { ProviderDocumentCreatorComponent } from './provider-document-creator/provider-document-creator.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { AttachmentDetailComponent } from './add-document/attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './add-document/attachment-upload/attachment-upload.component';
import { AudioRecordComponent } from './add-document/audio-record/audio-record.component';
import { EditAttachmentComponent } from './add-document/edit-attachment/edit-attachment.component';
import { ImageRecordComponent } from './add-document/image-record/image-record.component';
import { VideoRecordComponent } from './add-document/video-record/video-record.component';
import { AttachmentNarrativeComponent } from './add-document/attachment-narrative/attachment-narrative.component';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { AddChecklistComponent } from './add-checklist/add-checklist.component';
import { ProviderMonitorLicensingComponent } from './provider-monitor-licensing/provider-monitor-licensing.component';
import { LicenseSanctionsComponent } from './license-sanctions/license-sanctions.component';
import { LicenseChangeRequestComponent } from './license-change-request/license-change-request.component';
import { LicenseRequestsComponent } from './license-requests/license-requests.component';
import { VoluntaryClosureComponent } from './voluntary-closure/voluntary-closure.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { CommonControlsModule } from '../../../shared/modules/common-controls/common-controls.module';


@NgModule({
  imports: [
    CommonModule, LicenseManagementRoutingModule,
    FormMaterialModule,
    CommonControlsModule,
    QuillModule,
    NgSelectModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatStepperModule,
    NgxPaginationModule,
    SharedPipesModule,
    NgxfUploaderModule.forRoot()
  ],
  declarations: [LicenseManagementComponent,
    LicenseInformationComponent,
    AttachmentDetailComponent,
    AttachmentUploadComponent,
    AddDocumentComponent,
    AudioRecordComponent,
    EditAttachmentComponent,
    ImageRecordComponent,
    VideoRecordComponent,
    AttachmentNarrativeComponent,
    ProviderDocumentCreatorComponent,
    AddChecklistComponent,
    ProviderMonitorLicensingComponent,
    LicenseSanctionsComponent,
    LicenseChangeRequestComponent,
    LicenseRequestsComponent,
    VoluntaryClosureComponent],
  entryComponents: [AddChecklistComponent],
  providers: [NgxfUploaderService]

})
export class LicenseManagementModule { }
