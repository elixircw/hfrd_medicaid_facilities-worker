import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { AddChecklistComponent } from '../add-checklist/add-checklist.component';

import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
// import { ApplicantUrlConfig } from '../../provider-applicant-url.config';
import { Subject } from 'rxjs/Subject';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { ProviderStoreConstants } from '../../provider-constants';
@Component({
  selector: 'provider-monitor-licensing',
  templateUrl: './provider-monitor-licensing.component.html',
  styleUrls: ['./provider-monitor-licensing.component.scss']
})
export class ProviderMonitorLicensingComponent implements OnInit {

  id: string;

  @ViewChild('addCheckList', { read: ViewContainerRef }) container: ViewContainerRef;
  config = { panels: [] };
  tmpAccordionKey = [];
  activityCompleteStatus = 'Completed';
  visitationList: any[] = [];

  constructor(private _cfr: ComponentFactoryResolver, private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private _alertService: AlertService,
    private _datastore: DataStoreService, private _router: Router) {
    this.id = route.snapshot.params['id'];
  }

  monitorForm: FormGroup;
  visitationForm: FormGroup;
  isMonthDisabled = true;
  isYearDisabled = true;
  activityTaskStatusFormGroup: FormGroup;
  tasktypestatus = [];
  taskList = [];
  @Input()
  licenseLevelInputForMonitoring = new Subject<string>();
  licenseLevel: string;


  periodicDropDown = ['Initial', 'Monthly', 'Quarterly', 'Periodic', 'Mid-Year', 'Re-Licensure'];
  timeOfVisit = ['Night', 'Weekend', 'Regular'];
  visitSchedule = ['Announced', 'Unannounced'];
  monthDropDown = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  yearDropDown = [];

  ngOnInit() {
    for (let i = 2000; i <= new Date().getFullYear(); ++i) {
      this.yearDropDown.push(i);
    }
    this.monitorForm = this.formBuilder.group({
      periodic: [''],
      monthly: [''],
      yearly: null
    });
    this.visitationForm = this.formBuilder.group({
      monitoring_id: [''],
      applicant_id: [''],
      site_id: [''],
      provider_id: [''],
      periodic: [''],
      monitoring_dt: [''],
      time_visit: [''],
      announced_cd: ['']
    });
    this.activityTaskStatusFormGroup = this.formBuilder.group({
      task: this.formBuilder.array([])
    });
    // this.getTaskList();
    this.licenseLevelInputForMonitoring.subscribe((data) => {
      this.licenseLevel = data;
    });
    this.tasktypestatus = ['Completed', 'Incomplete', 'N/A'];
    // this.getChecklist();
    this.getVisitationList();
  }

  createTaskForm() {
    return this.formBuilder.group({
      checklist_id: [''],
      checklist_task: [''],
      commnts: [''],
      completeddate: [''],
      status: ['']
    });
  }

  private getTaskList() {
    this.taskList.length = 0;
    this._commonHttpService.getPagedArrayList(
      {
          method: 'post',
          applicant_id: this.id,
          category: 'Monitoring'
      },
      'providerapplicant/getchecklist'
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      this.setFormValues();
    });
  }

  private buildTaskForm(x): FormGroup {
    return this.formBuilder.group({
      checklist_id: x.checklist_id,
      checklist_task: x.checklist_task,
      commnts: x.commnts,
      completeddate: x.completeddate,
      status: x.status
    });
  }

  setFormValues() {
    // this.activityTaskStatusFormGroup.setControl('task', this.formBuilder.array([]));
    // const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    // this.taskList.forEach((x) => {
    //   control.push(this.buildTaskForm(x));
    // })
    // console.log(this.activityTaskStatusFormGroup.value);
    // console.log(this.activityTaskStatusFormGroup.controls);
    this.config.panels = [];
    this.tmpAccordionKey = [];
    const newObj = {};

    this.taskList.forEach((x) => {
      if (!newObj[x.subcategory]) {
        newObj[x.subcategory] = [x];
      } else {
        newObj[x.subcategory].push(x);
      }
    }
    );

    Object.keys(newObj).forEach((key, index) => {
      if (!this.tmpAccordionKey.includes(key)) {
        this.activityTaskStatusFormGroup.setControl('task' + index, this.formBuilder.array([]));
        this.config.panels.push({ name: key, description: this.activityCompleteStatus });
        this.tmpAccordionKey.push(key);
      }

      const control = <FormArray>this.activityTaskStatusFormGroup.controls['task' + index];

      newObj[key].forEach((x) => {
        control.push(this.buildTaskForm(x));
        if (x.status === 'INCOMPLETE' || x.status === 'Incomplete') {
          this.config.panels[index].description = 'Incomplete';
        }
      });
    });
  }

  openMonitoringCheckList() {
    // check and resolve the component
    const comp = this._cfr.resolveComponentFactory(AddChecklistComponent);
    // Create component inside container
    const expComponent = this.container.createComponent(comp);
    // see explanations
    expComponent.instance._ref = expComponent;
    expComponent.instance.category = 'Monitoring';
    expComponent.instance.type = this.licenseLevel;
    expComponent.instance.onSaveEventEmitter.subscribe(() => {
      // console.log('Event Captured');
      this.getTaskList();
    });
  }

  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.monitorForm.controls['monthly'].setValue('');
    this.monitorForm.controls['yearly'].setValue('');
  }

  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
    const objVal = Object.values(this.activityTaskStatusFormGroup.value);
    objVal.shift();
    const merged = [].concat.apply([], objVal);
    this._commonHttpService.endpointUrl = 'providerapplicant/updatechecklist';
    const requestData = {
      'applicant_id': this.id,
      'task': merged
    };
    this._commonHttpService.create(requestData).subscribe(
      (response) => {
        if (response) {
          this._alertService.success('Task updated successfully');
          this.getTaskList();
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  onChange() {
    this.resetDropDowns();
    if (this.monitorForm.value.periodic === 'Monthly') {
      this.isMonthDisabled = false;
      this.isYearDisabled = false;
    } else {
      this.isYearDisabled = false;
    }
  }

  viewActivity() {
    this.getTaskList();
  }

  getVisitationList() {
    const provider = this._datastore.getData(ProviderStoreConstants.PROVIDER_DETAILS);
    const obj = { where: { provider_id: (provider) ? provider.provider_id : '' }, method: 'get' };
    this._commonHttpService.getArrayList(obj, 'prov_monitoring?filter').subscribe(data => {
      this.visitationList = data;
     });
  }

  addVisitation() {
    const obj = this.visitationForm.getRawValue();
    const provider = this._datastore.getData(ProviderStoreConstants.PROVIDER_DETAILS);
    const site = this._datastore.getData(ProviderStoreConstants.PROVIDER_DETAILS);
    obj.provider_id = (provider) ? provider.provider_id : '';
    obj.site_id = this.id;
    this._commonHttpService.create(obj, 'prov_monitoring/addupdate').subscribe(data => {
      this._alertService.success('Visitation added sucessfully');
      (<any>$('#visitation')).modal('hide');
      this.getVisitationList();
     });
  }

  navigatetomonitor(obj) {
    const currentUrl = '/pages/provider/monitoring/' + obj.monitoring_id;
    this._router.navigate([currentUrl]);
  }
}
