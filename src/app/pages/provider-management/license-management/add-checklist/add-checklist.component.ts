import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { CommonHttpService, GenericService, AlertService } from '../../../../@core/services';
// import { ApplicantUrlConfig } from '../../../provider-applicant-url.config';
// import { AddCheckList } from '../../_entities/newApplicantModel';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'add-checklist',
  templateUrl: './add-checklist.component.html',
  styleUrls: ['./add-checklist.component.scss']
})
export class AddChecklistComponent implements OnInit, OnDestroy {

  activityDropdownItems$: DropdownModel[];
  activityDropdownItems1$: Observable<DropdownModel[]>;
  formGroup: FormGroup;
  paginationInfo: PaginationInfo = new PaginationInfo();
  leftTasks: any[] = [];
  rightTasks: any[] = [];
  private leftSelectedTasks = [];
  private rightSelectedTasks = [];
  applicantNumber: string;
  type: string;
  defaultArray = [];
  monthDropDown = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  yearDropDown = [];
  periodicDropDown = ['Initial', 'Monthly', 'Quarterly', 'Periodic', 'Mid-Year', 'Re-Licensure'];
  timeOfVisit = ['Night', 'Weekend', 'Regular'];
  visitSchedule = ['Announced', 'Unannounced'];
  isMonthDisabled = true;
  isYearDisabled = true;
  _ref: any;
  category: string;
  newObj = {};
  @Output('onSaveEventEmitter') onSaveEventEmitter = new EventEmitter();

  constructor(private _commonHttpService: CommonHttpService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _alertService: AlertService) {
    this.applicantNumber = route.snapshot.params['id'];
  }

  removeObject() {
    this.onSaveEventEmitter.emit();
    this._ref.destroy();
  }

  ngOnInit() {
    this.loadDropDown();
    this.initializeFormGroup();
    this.initializeYearCalendar();
  }

  initializeYearCalendar() {
    for (let i = 2000; i <= new Date().getFullYear(); ++i) {
      this.yearDropDown.push(i);
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.formBuilder.group({
      leftSelectedTask: [''],
      rightSelectedTask: [''],
      leftSelectedGoals: [''],
      rightSelectedGoals: [''],
      selectActivityList: [''],
      periodic: [''],
      monthly: [''],
      yearly: [''],
      schedule: [''],
      visitTime: ['']
    });
  }

  private loadDropDown() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          programname: this.type,
          agency: 'OLM',
          category: this.category
        }
      },
      'providerapplicant/gettasklist'
    ).subscribe(taskList => {
      console.log('Tasklist monitoring', taskList);
      console.log('Tasklist monitoring', JSON.stringify(taskList));

      taskList.forEach((x) => {
        if (x.subcategory === 'Default') {
          this.defaultArray.push(x);
        } else {
          if (!this.newObj[x.subcategory]) {
            this.newObj[x.subcategory] = [x];
          } else {
            this.newObj[x.subcategory].push(x);
          }
        }
      });
      console.log(this.newObj);
      const arr = [];
      this.activityDropdownItems$ = Object.keys(this.newObj).map((res, index) => {
        return new DropdownModel({ text: res, value: this.newObj[res] });
      });
    });
  }

  onChangeActivity(event: any) {
    console.log(this.newObj[event.source.selected._element.nativeElement.innerText.trim()]);
    this.rightTasks.length = 0;
    this.leftTasks = this.newObj[event.source.selected._element.nativeElement.innerText.trim()];
  }

  ngOnDestroy() {
    console.log('Destroy');
  }

  onClose() {
    console.log('onClose');
    this.removeObject();
  }

  toggleTask(position: string, selectedItem: any, control: any) {
    if (position === 'left') {
      if (control.target.checked) {
        this.leftSelectedTasks.push(selectedItem);
      } else {
        const index = this.leftSelectedTasks.indexOf(selectedItem);
        this.leftSelectedTasks.splice(index, 1);
      }
    } else {
      if (control.target.checked) {
        this.rightSelectedTasks.push(selectedItem);
      } else {
        const index = this.rightSelectedTasks.indexOf(selectedItem);
        this.rightSelectedTasks.splice(index, 1);
      }
    }
  }

  selectAllTask() {
    if (this.leftTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, Object.assign([], this.leftTasks));
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  selectTask() {
    if (this.leftSelectedTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, this.leftSelectedTasks);
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  unSelectTask() {
    if (this.rightSelectedTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, this.rightSelectedTasks);
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }
  unSelectAllTask() {
    if (this.rightTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, Object.assign([], this.rightTasks));
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  private clearSelectedTasks(position: string) {
    if (position === 'left') {
      this.leftSelectedTasks = [];
    } else {
      this.rightSelectedTasks = [];
    }
  }

  private moveTasks(source: any[], target: any[], selectedItems: any[]) {
    selectedItems.forEach((item: any, i) => {
      const selectedIndex = source.indexOf(item);
      if (selectedIndex !== -1 /*selectedItem.length*/) {
        source.splice(selectedIndex, 1);
        target.push(item);
      }
    });
    // this._changeDetect.detectChanges();
  }

  activitySave() {
    console.log('Save ');
    // this.defaultArray = this.defaultArray.concat(this.rightTasks);
    console.log(this.rightTasks);
    console.log(this.defaultArray);
    const taskList = [];
    if (this.category === 'Monitoring') {
      this.rightTasks.forEach((element) => {
        const newTask = {
          task: element.task,
          agency: element.agency,
          programname: element.programname,
          category: element.category,
          commnts: element.description,
          subcategory: element.task,
          description: element.description
        };
        taskList.push(newTask);
      });
    }

    const requestObj = {
      'applicant_id': this.applicantNumber,
      'task': taskList
    };
    this._commonHttpService.create(requestObj, 'providerapplicant/addchecklist').subscribe(
      (response) => {
        this._alertService.success('CheckList Added successfully!');
        this.removeObject();
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        console.log('Save Checklist Error', error);
        return false;
      }
    );

  }

  onPeriodChange() {
    this.resetDropDowns();
    if (this.formGroup.value.periodic === 'Monthly') {
      this.isMonthDisabled = false;
      this.isYearDisabled = false;
    } else {
      this.isYearDisabled = false;
    }
  }

  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.formGroup.controls['monthly'].setValue('');
    this.formGroup.controls['yearly'].setValue('');
  }

}
