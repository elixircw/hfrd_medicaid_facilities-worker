import {Component,OnInit,Input} from '@angular/core';
import {FormGroup,FormBuilder} from '@angular/forms';
import {AuthService,CommonHttpService,AlertService} from '../../../../@core/services';
import {applinfo} from '../_entities/newApplicantModel';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'license-sanctions',
	templateUrl: './license-sanctions.component.html',
	styleUrls: ['./license-sanctions.component.scss']
})
export class LicenseSanctionsComponent implements OnInit {

	licenseSanctionsForm: FormGroup;
	sanctions:any[];
	suspensions:any[];
	revocations:any[];
	limitations:any[];

	isSuspension: boolean;
	isRevocation: boolean;
	isLimitation: boolean;
	
	licenseLevel: string;
	@Input() licenseInfo: applinfo;
	
	limitationType: any;
	selectedLimitationType: string;

	siteId: string;

	constructor(private formbuilder: FormBuilder,
		private _alertService: AlertService,
		private _commonHttpService: CommonHttpService,
		private route: ActivatedRoute
	) {
		this.siteId = route.snapshot.params['id'];
	}

	ngOnInit() {
		this.initializeLicensSanctionsForm();
		this.getSanctions();

		this.licenseLevel = this.licenseInfo.license_level;

		if(this.licenseLevel==='RCC'){	
			this.limitationType=["No. of youth served","Populations","Moratorium"];
		}
		else if(this.licenseLevel==='CPA'){
			this.limitationType=["Closing referrals/intake","Age","Authority of chief administrator","Authority of Governing Board","Activity of agency employee","Distribution of publicity"];
		}
	}

	private initializeLicensSanctionsForm() {
		this.licenseSanctionsForm = this.formbuilder.group({
			license_sanction_type: [''],
			sanction_effective_dt: null,
			sanction_end_dt: null,
			limitation_type:[''],
			limitation_min_age:null,
			limitation_max_age:null,
			limitation_gender:null,
			limitation_reduced_youth_capacity:null
		})
	}

	getSanctions() {
		this._commonHttpService.getArrayList({
			method: 'get',
			where: {
				'site_id': this.siteId
			}
		}, 'providerlicensesanctions?filter')
		.subscribe( response => {
			console.log('all sanctions',JSON.stringify(response));
			this.suspensions = response.filter(a => a.license_sanction_type==='Suspension');
			this.revocations = response.filter(a => a.license_sanction_type==='Revocation');
			this.limitations = response.filter(a => a.license_sanction_type==='Limitation');
			console.log('suspensions',this.suspensions);
			console.log('revocation',this.revocations);
			console.log('limitations',this.limitations);
		});
	}
	
	checkSelectedSanction(selectedSanction) {
		console.log('selected status', selectedSanction);
		console.log('selected status value', selectedSanction.value);
		//this.providerId=this.licenseInfo.provider_id;
		//this.programId=this.licenseInfo.program_id;
		console.log(this.licenseSanctionsForm.value.limitation_type);

		if (selectedSanction.value == "Suspension") {
			this.isSuspension = true;
			this.isRevocation = false;
			this.isLimitation = false;
		} else if (selectedSanction.value == "Revocation") {
			this.isRevocation = true;
			this.isLimitation = false;
			this.isSuspension = false;
		} else if (selectedSanction.value == "Limitation") {
			this.isLimitation = true;
			this.isSuspension = false;
			this.isRevocation = false;
		}
	}

	changeLimitationType(event) {
		console.log(event);
		this.selectedLimitationType = event.value;
	}

	saveAddedLimitations(){
		var payload = {}
		console.log (this.licenseSanctionsForm.getRawValue());
		payload = this.licenseSanctionsForm.getRawValue();
		payload['site_id'] = this.siteId;
		payload['license_no'] = this.licenseInfo.license_no;
		payload['provider_id'] = this.licenseInfo.provider_id;

		this._commonHttpService.create({
		 	where: payload
		},
		'providerlicensesanctions/addlicensesanction')
		.subscribe(
			(response) => {
			this._alertService.success('Saction Limitation added successfully!');
			this.getSanctions();
			(<any>$('#sanctions-limitations')).modal('hide');
		  },
		  (error) => {
			this._alertService.error("Unable to save limitation");
		  }
		);
	}
}