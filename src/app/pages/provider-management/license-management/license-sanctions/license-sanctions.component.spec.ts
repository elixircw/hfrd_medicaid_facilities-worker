import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseSanctionsComponent } from './license-sanctions.component';

describe('LicenseSanctionsComponent', () => {
  let component: LicenseSanctionsComponent;
  let fixture: ComponentFixture<LicenseSanctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseSanctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseSanctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
