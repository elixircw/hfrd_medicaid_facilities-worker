import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseChangeRequestComponent } from './license-change-request.component';

describe('LicenseChangeRequestComponent', () => {
  let component: LicenseChangeRequestComponent;
  let fixture: ComponentFixture<LicenseChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
