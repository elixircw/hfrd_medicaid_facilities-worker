import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { AuthService, CommonHttpService, AlertService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
//import { ApplicantDocumentCreatorComponent } from '../applicant-document-creator/applicant-document-creator.component';
import * as ALL_DOCUMENTS from '../applicant-attachments/_configurtions/documents.json'
// import { GeneratedDocuments, applinfo } from '../../new-provider/_entities/newApplicantModel';
import {applinfo } from '../_entities/newApplicantModel';

@Component({
  selector: 'license-information',
  templateUrl: './license-information.component.html',
  styleUrls: ['./license-information.component.scss']
})
export class LicenseInformationComponent implements OnInit {

  providerId: string;
  licenseInfo: applinfo = new applinfo();
  licenseFormGroup: FormGroup;
  editLicenseFormGroup: FormGroup;
  token: AppUser;
  isEdited: boolean
  // @Input() generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
  // generatedDocuments: GeneratedDocuments[] = [];
  
  //@Output() licenseDateUpdateEvent = new EventEmitter();
  
  //allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;

  @Output() generateLicenseEvent = new EventEmitter();
  genderList: any[];


  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService, 
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute
    //private comp: ApplicantDocumentCreatorComponent
    ) { 
      this.providerId = route.snapshot.params['id'];
    }

  ngOnInit() {
    this.initializeLicenseForm();
    this.initilizeEditLicenseFormGroup();
    this.getLicenseInformation();
    this.getGenderList();
    this.token = this._authService.getCurrentUser();
    this.isEdited=false;
  }

  private initilizeEditLicenseFormGroup(){
    this.editLicenseFormGroup = this.formBuilder.group({
      adr_street_nm: [''],
      adr_city_nm: [''],
      adr_state_cd: [''],    
      adr_zip5_no: [''],
      adr_country_tx:[''],
      adr_county_cd:[''],
      adr_street_no:[''],
      provider_nm: ['']
    });
  }

  private initializeLicenseForm() {
    this.licenseFormGroup = this.formBuilder.group({
      license_no: [{value:'',disabled:true}],
      license_issue_dt: [{value:'',disabled:true}],
      license_expiry_dt: [{value:'',disabled:true}],
      
      provider_nm: [{value:'',disabled:true}],
      license_type: [{value:'',disabled:true}],
      prgram: [{value:'',disabled:true}],
      
      contact_first_nm: [{value:'',disabled:true}],
      contact_last_nm: [{value:'',disabled:true}],

      minimum_age_no: [{value:'',disabled:true}],
      maximum_age_no: [{value:'',disabled:true}],

      gender_cd: [{value:'',disabled:true}],
      mailing_address: [{value:'',disabled:true}],
      children_no: [{value:'',disabled:true}]
    });
  }
  
  saveLicenseInformation() {
    var obj = this.editLicenseFormGroup.value;
    console.log("Rahul Provider Name", obj.provider_nm);
    this._commonHttpService.create(
      {
        method:'post',
        where: 
        {
          provider_id :this.providerId,
          provider_nm: obj.provider_nm,
          adr_street_nm: obj.adr_street_nm,
          adr_city_nm: obj.adr_city_nm,
          adr_state_cd: obj.adr_state_cd,    
          adr_zip5_no: obj.adr_zip5_no,
          adr_country_tx:obj.adr_country_tx,
          adr_county_cd:obj.adr_county_cd,
          adr_street_no:obj.adr_street_no,
      }        
      },
      'providerlicense/updateproviderinfo'
  ).subscribe(response => {
    this.getLicenseInformation();
    console.log(response)
    this._alertService.success('License Information Saved Successfully!');
  
  },
      (error) => {
        this._alertService.error('Unable to update license information, please try again.');
        console.log('update program information Error', error);
        return false;
      }
    );
    (<any>$('#edit-license-info')).modal('hide');

}
 


  getLicenseInformation() {
    this._commonHttpService.create(
      {
        method:'post',
        where: 
        {
          objectid :this.providerId
      }        
      },
      'providerlicense/getproviderlicensing'
  ).subscribe(response => {
    this.licenseInfo = response.data[0];

    this.licenseFormGroup.patchValue(this.licenseInfo);
    this.editLicenseFormGroup.patchValue(this.licenseInfo);
    //this.editLicenseFormGroup['provider_nm'].patchValue(this.licenseInfo.provider_nm);


    console.log("Rahul get license info",JSON.stringify(this.licenseInfo));
  },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
}
  

  openEditLicense(){
    console.log("rahul,open edit license");
    (<any>$('#edit-license-info')).modal('show');
    this.isEdited=false;
  }

  getGenderList() {
    this._commonHttpService.getArrayList(
      {
        where: { tablename: 'gender', teamtypekey: 'OLM' },
        method: 'get',
        nolimit: true
      },
      'referencetype/gettypes?filter'
    ).subscribe(result => {
      this.genderList = result;
    });
  }

}

