import { Component, OnInit, Input } from '@angular/core';
import { AttachmentIntakes, GeneratedDocuments, ProviderReferral } from '../../provider-management/new-provider/_entities/newApplicantModel';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { ApplicantUrlConfig } from '../provider-management-url.config';
import { AlertService } from '../../../@core/services/alert.service';
import { ReferralUrlConfig } from '../../provider-referral/provider-referral-url.config';
import {applinfo } from './_entities/newApplicantModel';

@Component({
  selector: 'license-management',
  templateUrl: './license-management.component.html',
  styleUrls: ['./license-management.component.scss']
})
export class LicenseManagementComponent implements OnInit {

  // Documents related
  addAttachementSubject$ = new Subject<AttachmentIntakes[]>();
  generatedDocuments$ = new Subject<string[]>();
  generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
  generateDocumentInput$ = new Subject<GeneratedDocuments[]>();
  profileOutputSubject$ = new Subject<applinfo>();
  referralinfooutputsubject$ = new Subject<ProviderReferral>();
  
  
  referralInfoDetails: ProviderReferral = new ProviderReferral() ;
  licenseInfo: applinfo = new applinfo();

  providerId: string;

  applicants:any;

  licenseNumber: string;
  programTypeInput = new Subject<string>();
  licenseLevelInputForMonitoring = new Subject<string>();

  constructor(private _commonHttpService: CommonHttpService,private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute
  ) { 
    this.providerId = route.snapshot.params['id'];
  }

  ngOnInit() {
    // Getting the provider details for displaying in the banner
    this.getProviderDetails();

    // Getting the license information once, and pass to other components
    this.getLicenseInformation();
  }

    private getProviderDetails() {
      this._commonHttpService.getArrayList(
        {
            method: 'get',
            nolimit: true,
            where: {
              "provider_id":this.providerId
            }  
          },'providerinfo?filter', 
        ).subscribe(applicants => {
          this.applicants = applicants
          console.log("Applicants:"+this.applicants);

          this.referralInfoDetails.provider_referral_nm=this.applicants[0].provider_nm;
          this.referralInfoDetails.is_taxid=this.applicants[0].tax_id_no;
          this.referralInfoDetails.adr_cell_phone_tx=this.applicants[0].adr_work_phone_tx;
          this.referralInfoDetails.adr_county_cd=this.applicants[0].county_cd;
          this.referralInfoDetails.referral_status=this.applicants[0].provider_status_cd =="1791"?"Open":"Closed";
        }); 
      }
    
  
    getLicenseInformation() {
        this._commonHttpService.create(
          {
            method:'post',
            where: 
            {
              objectid :this.providerId
          }        
          },
          'providerlicense/getproviderlicensing'
      ).subscribe(response => {
        console.log("Before",JSON.stringify(this.licenseInfo));

        this.licenseInfo = response.data[0];

        console.log("After",JSON.stringify(this.licenseInfo));

        this.licenseLevelInputForMonitoring.next(this.licenseInfo.license_level);

        },
          (error) => {
            this._alertService.error('Unable to get license information, please try again.');
            console.log('get license information Error', error);
            return false;
          }
        );
    }
}