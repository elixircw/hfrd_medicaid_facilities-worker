import {Component,OnInit,Input} from '@angular/core';
import {FormGroup,FormBuilder} from '@angular/forms';
import {applinfo} from '../_entities/newApplicantModel';
import { ActivatedRoute } from '@angular/router';
import { AlertService, CommonHttpService } from '../../../../@core/services';
@Component({
  selector: 'voluntary-closure',
  templateUrl: './voluntary-closure.component.html',
  styleUrls: ['./voluntary-closure.component.scss']
})
export class VoluntaryClosureComponent implements OnInit {
	voluntaryClosureForm: FormGroup;
  siteId: string;
  voluntaryClosure: any[];
  @Input() licenseInfo: applinfo;
  constructor(private formbuilder: FormBuilder,
		private _alertService: AlertService,
		private _commonHttpService: CommonHttpService,
		private route: ActivatedRoute
	) {
		this.siteId = route.snapshot.params['id'];
	}

  ngOnInit() {
    this.initializeVoluntaryClosureForm();
    this.getVoluntaryclosureInfo();
    this.voluntaryClosure=[];

  }
  private initializeVoluntaryClosureForm() {
		this.voluntaryClosureForm = this.formbuilder.group({
			sanction_end_dt: null,
		})
  }
  getVoluntaryclosureInfo() {
		this._commonHttpService.getArrayList({
			method: 'get',
			where: {
				'site_id': this.siteId
      }
		}, 'providerlicensesanctions?filter')
		.subscribe( response => {
			console.log('all sanctions',JSON.stringify(response));
			this.voluntaryClosure = response.filter(a => a.license_sanction_type==='Voluntary Closure');			
		});
	}
  saveVoluntaryClosureForm(){
    //this.voluntaryClosure.push(this.voluntaryClosureForm.value);
    var payload = {}

		console.log ('voluntary closure',this.voluntaryClosureForm.getRawValue());
    payload = this.voluntaryClosureForm.getRawValue();
		payload['site_id'] = this.siteId;
		payload['license_no'] = this.licenseInfo.license_no;
    payload['provider_id'] = this.licenseInfo.provider_id; 
    payload['license_sanction_type'] = 'Voluntary Closure';

		this._commonHttpService.create({
		 	where: payload
		},
		'providerlicensesanctions/addlicensesanction')
		.subscribe(
			(response) => {
			this._alertService.success('Saction Limitation added successfully!');
			this.getVoluntaryclosureInfo();
			(<any>$('#sanctions-limitations')).modal('hide');
		  },
		  (error) => {
			this._alertService.error("Unable to save limitation");
		  }
		);
  }
 
}
