import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoluntaryClosureComponent } from './voluntary-closure.component';

describe('VoluntaryClosureComponent', () => {
  let component: VoluntaryClosureComponent;
  let fixture: ComponentFixture<VoluntaryClosureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoluntaryClosureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoluntaryClosureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
