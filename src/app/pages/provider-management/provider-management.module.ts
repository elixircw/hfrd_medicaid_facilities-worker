import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProviderManagementComponent } from './provider-management.component';
import { ExistingProviderComponent } from './existing-provider/existing-provider.component';
import { ProviderManagementRoutingModule } from './provider-management-routing.module'
import { NgxPaginationModule } from 'ngx-pagination';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatSlideToggleModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewPublicProviderComponent } from './new-public-provider/new-public-provider.component';
import { PublicProviderServicesComponent } from './new-public-provider/public-provider-services/public-provider-services.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
  imports: [
    CommonModule, ProviderManagementRoutingModule,NgxPaginationModule,  MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    FormsModule, 
    ReactiveFormsModule,
    MatTableModule,
    PaginationModule,
    MatExpansionModule,
    MatSlideToggleModule
  ],
  declarations: [ProviderManagementComponent, ExistingProviderComponent]
})
export class ProviderManagementModule { }
