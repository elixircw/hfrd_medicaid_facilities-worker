import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';

@Component({
  selector: 'public-provider-home-placement',
  templateUrl: './public-provider-home-placement.component.html',
  styleUrls: ['./public-provider-home-placement.component.scss']
})
export class PublicProviderHomePlacementComponent implements OnInit {

  homePlacementSpecificationForm: FormGroup;
  providerId: string;
  homePlacementSpecificationInfo= [];
  currentHomePlacementSpecificationId: string;
  isEditHomePlacementSpecification: boolean=false;
  genderTypes=[];

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.providerId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.isEditHomePlacementSpecification = false;
    this.formIntilizer();
    this.getInformation();
    this.genderTypes=["Male","Female", "Male/Female"];
  }

  formIntilizer() {
    this.homePlacementSpecificationForm = this.formBuilder.group(
      {
        home_placement_specification_id: null,
        object_id: [''],
        min_age_yr: null,
        max_age_yr: null,
        min_age_months: null,
        max_age_months: null,
        capacity: null,
        gender: null,
        is_interested_respite: null

      }
    );
  }

addHomePlacementSpecification(){
  this.isEditHomePlacementSpecification=false;
}

  private getInformation() {
    this._commonHttpService.getArrayList(
			{
				method: 'get',
				where : { object_id : this.providerId }
			},
      'publicproviderhomeplacementspecification?filter'
		).subscribe(response => {
      this.homePlacementSpecificationInfo = response	
      // console.log('this.homePlacementSpecificationInfo',this.homePlacementSpecificationInfo[0].home_placement_specification_id);
      if(this.homePlacementSpecificationInfo.length>0)	{
        this.isEditHomePlacementSpecification=true;
        this.currentHomePlacementSpecificationId=this.homePlacementSpecificationInfo[0].home_placement_specification_id;
        // console.log('this.homePlacementSpecificationInfo2',this.currentHomePlacementSpecificationId);

        this.homePlacementSpecificationForm.patchValue({
          home_placement_specification_id: null,
          object_id: this.homePlacementSpecificationInfo[0].object_id,
          min_age_yr: this.homePlacementSpecificationInfo[0].min_age_yr,
          max_age_yr:this.homePlacementSpecificationInfo[0].max_age_yr,
          min_age_months:this.homePlacementSpecificationInfo[0].min_age_months,
          max_age_months: this.homePlacementSpecificationInfo[0].max_age_months,
          capacity: this.homePlacementSpecificationInfo[0].capacity,
          gender:this.homePlacementSpecificationInfo[0].gender,
          is_interested_respite : this.homePlacementSpecificationInfo[0].is_interested_respite
        });
      }
      else{
        this.isEditHomePlacementSpecification=false;
      } 
		});
  }

  patchHomePlacementSpecificationDetails() {
    console.log('this.homePlacementSpecificationInfo3',this.currentHomePlacementSpecificationId);
    this.homePlacementSpecificationForm.value.object_id=this.providerId;
    this.homePlacementSpecificationForm.value.home_placement_specification_id=this.currentHomePlacementSpecificationId;
    
    this._commonHttpService.patch(
      this.currentHomePlacementSpecificationId,
      this.homePlacementSpecificationForm.value,
      'publicproviderhomeplacementspecification'
    ).subscribe(
      (response) => {
        this._alertService.success("Information updated successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to update information");
      }

    );
    (<any>$('#add-homePlacementSpecification')).modal('hide');
  }

  saveHomePlacementSpecificationDetails() {
    this.homePlacementSpecificationForm.value.object_id=this.providerId;
    this._commonHttpService.create(
      this.homePlacementSpecificationForm.value,
      'publicproviderhomeplacementspecification/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-homePlacementSpecification')).modal('hide');
  }

}
