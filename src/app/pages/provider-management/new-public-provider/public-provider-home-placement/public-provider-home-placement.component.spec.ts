import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderHomePlacementComponent } from './public-provider-home-placement.component';

describe('PublicProviderHomePlacementComponent', () => {
  let component: PublicProviderHomePlacementComponent;
  let fixture: ComponentFixture<PublicProviderHomePlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderHomePlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderHomePlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
