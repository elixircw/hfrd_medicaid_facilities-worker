import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule, Routes } from '@angular/router';
import { NewPublicProviderComponent } from './new-public-provider.component';
import { PublicProviderProfileComponent } from './public-provider-profile/public-provider-profile.component';
import { PublicProviderApprovalComponent } from './public-provider-approval/public-provider-approval.component';
import { PublicProviderCharacteristicsComponent } from './public-provider-characteristics/public-provider-characteristics.component';
import { PublicProviderHouseholdComponent } from './public-provider-household/public-provider-household.component';
import { PublicProviderServiceTrainingComponent } from './public-provider-service-training/public-provider-service-training.component';
import { PublicProviderPlacementStructuresComponent } from './public-provider-placement-structures/public-provider-placement-structures.component';
import { PublicReconsiderationComponent } from './public-reconsideration/public-reconsideration.component';
import { ProviderStatusManagementComponent } from './provider-status-management/provider-status-management.component';
import { PublicProviderHomePlacementComponent } from './public-provider-home-placement/public-provider-home-placement.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { PublicProviderServicesComponent } from './public-provider-services/public-provider-services.component';
import { AppConstants } from '../../../@core/common/constants';
import { ChangeHomeApprovalComponent } from './change-home-approval/change-home-approval.component';
import { PublicProviderChildPlacementComponent } from './public-provider-child-placement/public-provider-child-placement.component';



const routes: Routes = [
    {
        path: '',
        component: NewPublicProviderComponent,
        // canActivate: [RoleGuard],
        children: [
            // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
            // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
            // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
            // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
        ]
    },
    {
        path: ':id',
        component: NewPublicProviderComponent,
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full'
            },
            {
                path: 'profile',
                component: PublicProviderProfileComponent
            },
            {
                path: 'approval',
                component: PublicProviderApprovalComponent
            },
            {
                path: 'services',
                component: PublicProviderServicesComponent
            },
            {
                path: 'characteristics',
                component: PublicProviderCharacteristicsComponent
            },
            {
                path: 'household-member',
                component: PublicProviderHouseholdComponent
            },
            {
                path: 'household-members',
                loadChildren: '../../shared-pages/involved-persons/involved-persons.module#InvolvedPersonsModule',
                data: { source: AppConstants.MODULE_TYPE.PUBLIC_PROVIDER }
              },
            {
                path: 'background-checks',
                loadChildren: '../../shared-pages/person-background-check/person-background-check.module#PersonBackgroundCheckModule',
            },
            {
                path: 'service-training',
                component: PublicProviderServiceTrainingComponent
            },
            {
                path: 'placement-structure',
                component: PublicProviderPlacementStructuresComponent
            },
            {
                path: 'placement-spec',
                component: PublicProviderHomePlacementComponent
            },
            {
                path: 'child-placement',
                component: PublicProviderChildPlacementComponent
            },
            {
                path: 'status-change',
                component: ProviderStatusManagementComponent
            },
            {
                path: 'documents',
                component: AddDocumentComponent
            },
            {
                path: 'reconsideration',
                component: PublicReconsiderationComponent
            },
            {
                path: 'change-home-approval',
                component: ChangeHomeApprovalComponent
            }
        ]
        // canActivate: [RoleGuard]
    }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewPublicProviderRoutingModule { }
