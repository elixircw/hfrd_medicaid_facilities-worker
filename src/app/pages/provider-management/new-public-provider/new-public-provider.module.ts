import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule, MatButtonToggleModule, MatSlideToggleModule, MatStepperModule,
} from '@angular/material';

import { PublicProviderServicesComponent } from '../new-public-provider/public-provider-services/public-provider-services.component';
import { NewPublicProviderComponent } from '../new-public-provider/new-public-provider.component';

import { NewPublicProviderRoutingModule } from './new-public-provider-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaskModule } from 'ngx-mask';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { PublicProviderAddCharacteristicsComponent } from './public-provider-add-characteristics/public-provider-add-characteristics.component';
import { PublicProviderCharacteristicsComponent } from './public-provider-characteristics/public-provider-characteristics.component';
import { PublicProviderSuspensionsComponent } from './public-provider-suspensions/public-provider-suspensions.component';
import { PublicProviderHouseholdComponent } from './public-provider-household/public-provider-household.component';
import { PublicProviderServiceTrainingComponent } from './public-provider-service-training/public-provider-service-training.component';
import { PublicProviderPlacementStructuresComponent } from './public-provider-placement-structures/public-provider-placement-structures.component';
import { PublicProviderHomePlacementComponent } from './public-provider-home-placement/public-provider-home-placement.component';
import { ProviderStatusManagementComponent } from './provider-status-management/provider-status-management.component';
import { PublicReconsiderationComponent } from './public-reconsideration/public-reconsideration.component';
import { PublicProviderDocumentCreatorComponent } from './public-provider-document-creator/public-provider-document-creator.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { AttachmentUploadComponent } from './add-document/attachment-upload/attachment-upload.component';
import { AudioRecordComponent } from './add-document/audio-record/audio-record.component';
import { EditAttachmentComponent } from './add-document/edit-attachment/edit-attachment.component';
import { ImageRecordComponent } from './add-document/image-record/image-record.component';
import { VideoRecordComponent } from './add-document/video-record/video-record.component';
import { AttachmentDetailComponent } from './add-document/attachment-detail/attachment-detail.component';
import { PublicProviderProfileComponent } from './public-provider-profile/public-provider-profile.component';
import { PublicProviderApprovalComponent } from './public-provider-approval/public-provider-approval.component';
import { IntakeConfigService } from '../../newintake/my-newintake/intake-config.service';
import { ChangeHomeApprovalComponent } from './change-home-approval/change-home-approval.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { PublicProviderChildPlacementComponent } from './public-provider-child-placement/public-provider-child-placement.component';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule, NewPublicProviderRoutingModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    QuillModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgSelectModule,
    NgxPaginationModule,
    PaginationModule
  ],
  declarations: [NewPublicProviderComponent, PublicProviderServicesComponent, PublicProviderAddCharacteristicsComponent,
    PublicProviderCharacteristicsComponent, PublicProviderSuspensionsComponent, PublicProviderHouseholdComponent,
    PublicProviderServiceTrainingComponent, PublicProviderPlacementStructuresComponent, PublicProviderHomePlacementComponent,
    ProviderStatusManagementComponent, PublicReconsiderationComponent, PublicProviderDocumentCreatorComponent, AttachmentDetailComponent,
    AttachmentUploadComponent,
    AudioRecordComponent,
    EditAttachmentComponent,
    ImageRecordComponent,
    VideoRecordComponent,
    AddDocumentComponent,
    PublicProviderProfileComponent,
    PublicProviderApprovalComponent,
    ChangeHomeApprovalComponent,
    PublicProviderChildPlacementComponent,
  ],
  entryComponents: [PublicProviderAddCharacteristicsComponent],

  providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService, IntakeConfigService],
  //exports:[MatDatepickerModule,MatNativeDateModule]
})
export class NewPublicProviderModule { }
