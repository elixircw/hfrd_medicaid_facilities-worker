import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeHomeApprovalComponent } from './change-home-approval.component';

describe('ChangeHomeApprovalComponent', () => {
  let component: ChangeHomeApprovalComponent;
  let fixture: ComponentFixture<ChangeHomeApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeHomeApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeHomeApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
