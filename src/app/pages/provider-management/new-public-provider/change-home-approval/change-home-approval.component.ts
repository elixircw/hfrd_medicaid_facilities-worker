import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonDropdownsService, CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { Action } from 'rxjs/scheduler/Action';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'change-home-approval',
  templateUrl: './change-home-approval.component.html',
  styleUrls: ['./change-home-approval.component.scss']
})
export class ChangeHomeApprovalComponent implements OnInit {
  requestForm: FormGroup;
  approvalTypes: any = [];
  communication_mediums: { id: number; type: string; }[];
  maxDate = new Date();
  usersList: any[];
  providerId: any;
  changeRequestList: any[] = [];
  providerInfo: any;
  constructor(private formBuilder: FormBuilder,
    private _commonDropDownService: CommonDropdownsService,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService) {
    this.providerId = this.route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.initForm();
    const providerInfo = this._dataStoreService.getData('publicProviderInfo');
    this.providerInfo = (Array.isArray(providerInfo) && providerInfo.length) ? providerInfo[0] : null;
    this.loadChangeRequestList();
  }

  loadUserList() {
    this.usersList = [];
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PRASS' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.usersList = result.data;
      });
  }

  initForm() {
    this.requestForm = this.formBuilder.group(
      {
        approval_type: [null, [Validators.required]],
        communication: [null, [Validators.required]],
        requested_date: [null, [Validators.required]],
        comments: [''],
        tosecurityuserid: [null, [Validators.required]]
      }
    );
  }
  loadDropDowns() {

    this._commonDropDownService.getPickListByName('homeapprovaltypes').subscribe(response => {
      if (Array.isArray(response)) {
        if(this.providerInfo && this.providerInfo.program === 'Kinship') {
          this.approvalTypes = response.filter(approvalType => approvalType.ref_key === 'RH');
        } else {
          this.approvalTypes = response.filter(approvalType => approvalType.ref_key === 'TR');
        }
      }
    });
    this.communication_mediums = [
      { id: 1, type: 'Phone' },
      { id: 2, type: 'Mail' },
      { id: 3, type: 'E-mail' },
      { id: 4, type: 'Fax' },
      { id: 5, type: 'Face-to-Face' },

    ];
  }

  openRequestForm() {
    this.requestForm.reset();
    (<any>$('#home-approval-request-form')).modal('show');
    this.loadDropDowns();
    this.loadUserList();
  }

  close() {
    this.requestForm.reset();
    (<any>$('#home-approval-request-form')).modal('hide');
  }

  createRequest() {

    if (this.requestForm.invalid) {
      this._alertService.error('Please fill required feilds');
    }
    const data = { ...this.requestForm.getRawValue(), ... { providerid: this.providerId } };
    console.log(data);
    this._commonHttpService.create(data, 'publicproviderreferral/createproviderapplicant').subscribe(response => {
      this._alertService.success('Application initatated successfully');
      this.close();
      this.loadChangeRequestList();
      console.log('response', response);
    });

  }

  loadChangeRequestList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { "providerid": this.providerId }
      }, 'providerapprovetypeconfig/details?filter',
    ).subscribe(response => {
      this.changeRequestList = response;
    });
  }


}
