import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'public-provider-child-placement',
  templateUrl: './public-provider-child-placement.component.html',
  styleUrls: ['./public-provider-child-placement.component.scss']
})
export class PublicProviderChildPlacementComponent implements OnInit {
  childPlacementInfo = [];
  providerId: any;
  totalRecords: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  private pageSubject$ = new Subject<number>();

  constructor(
    private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute
  ) {
    this.providerId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.pageSubject$.subscribe(pageNumber => {
      this.paginationInfo.pageNumber = pageNumber;
      this.getChildPlacementInfo(this.paginationInfo.pageNumber);
    });
    this.getChildPlacementInfo(1);
  }

  private getChildPlacementInfo(page: number) {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: { providerid: this.providerId },
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
      },
      'tb_provider_services/getproviderchilddetails?filter'
    ).subscribe(response => {
      if (response && response.length) {
        this.totalRecords = response[0].totalcount;
        this.childPlacementInfo = response;
      }
    });
  }
  pageChanged(event: any) {
    this.paginationInfo.pageNumber = event.page;
    this.paginationInfo.pageSize = event.itemsPerPage;
    this.pageSubject$.next(this.paginationInfo.pageNumber);
  }
}
