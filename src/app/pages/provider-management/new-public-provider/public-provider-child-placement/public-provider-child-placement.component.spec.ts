import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderChildPlacementComponent } from './public-provider-child-placement.component';

describe('PublicProviderChildPlacementComponent', () => {
  let component: PublicProviderChildPlacementComponent;
  let fixture: ComponentFixture<PublicProviderChildPlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderChildPlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderChildPlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
