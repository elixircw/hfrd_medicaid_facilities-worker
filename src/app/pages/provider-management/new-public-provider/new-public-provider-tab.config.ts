import { AppConstants } from '../../../@core/common/constants';

export const PublicPorviderTabs = [
    {
        id: 'profile',
        title: 'Profile',
        name: 'Profile',
        role: [AppConstants.ROLES.PROVIDER, AppConstants.ROLES.PROVIDER_STAFF_ADMIN],
        route: 'profile',
        active: false,
        securityKey: 'profile-screen'
    },
    {
        id: 'approval',
        title: 'Approval',
        name: 'Approval',
        role: [],
        route: 'approval',
        active: false,
        securityKey: 'Approval-screen'
    },
    {
        id: 'services',
        title: 'Services',
        name: 'Services',
        role: [],
        route: 'services',
        active: false,
        securityKey: 'services-screen'
    },
    {
        id: 'characteristics',
        title: 'Characteristics',
        name: 'Characteristics',
        role: [],
        route: 'characteristics',
        active: false,
        securityKey: 'characteristics-screen'
    },
    {
        id: 'house-hold-members',
        title: 'Households',
        name: 'Households',
        role: [],
        route: 'household-members',
        active: false,
        securityKey: 'house-hold-members-screen'
    },
    {
        id: 'background-checks',
        title: 'Background Checks',
        name: 'Background Checks',
        role: [],
        route: 'background-checks',
        active: false,
        securityKey: 'household-info-screen'
    },
    {
        id: 'service-training',
        title: 'Service Training',
        name: 'Service Training',
        role: [],
        route: 'service-training',
        active: false,
        securityKey: 'service-training-screen'
    },
    {
        id: 'placement-structure',
        title: 'Placement Structure',
        name: 'Placement Structure',
        role: [],
        route: 'placement-structure',
        active: false,
        securityKey: 'aplacement-structure-screen'
    },
    {
        id: 'placement-spec',
        title: 'Placement Spec',
        name: 'Placement Spec',
        role: [],
        route: 'placement-spec',
        active: false,
        securityKey: 'placement-spec-screen'
    },
    {
        id: 'child-placement',
        title: 'Child Placements',
        name: 'Child Placements',
        role: [],
        route: 'child-placement',
        active: false,
        securityKey: 'child-placement-screen'
    },
    {
        id: 'status-change',
        title: 'Status Change',
        name: 'Status Change',
        role: [],
        route: 'status-change',
        active: false,
        securityKey: 'status-change'
    },
    {
        id: 'documents',
        title: 'Documents',
        name: 'Documents',
        role: [],
        route: 'documents',
        active: false,
        securityKey: 'documents-screen'
    },
    {
        id: 'reconsideration',
        title: 'Reconsideration',
        name: 'Reconsideration',
        role: [],
        route: 'reconsideration',
        active: false,
        securityKey: 'reconsideration-screen'
    },
    {
        id: 'change-home-approval',
        title: 'Change Home Approval',
        name: 'Change Home Approval',
        role: [],
        route: 'change-home-approval',
        active: false,
        securityKey: 'change-home-approval-screen'
    }
];


