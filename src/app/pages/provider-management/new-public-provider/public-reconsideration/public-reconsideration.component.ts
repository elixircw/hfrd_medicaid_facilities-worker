import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
@Component({
  selector: 'public-reconsideration',
  templateUrl: './public-reconsideration.component.html',
  styleUrls: ['./public-reconsideration.component.scss']
})
export class PublicReconsiderationComponent implements OnInit {

 
  reconsiderationForm: FormGroup;
  providerId: string;
  reconsiderationInfo= [];
  currentReconId: string;
  isEditReconsideration: boolean=false;
  reconNumber: string
  reconTypes=[];
  nextReconDate: any;
 
  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.providerId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.formIntilizer();
    this.getInformation();
    this.reconTypes=['Annual Reconsideration','Significant Change'];
    this.getInfoFromProviderApproval();
  }

  formIntilizer() {
    this.reconsiderationForm = this.formBuilder.group(
      {
        object_id: [''],
        recon_date: [''],  
        recon_number:[''],
        recon_type:['']    
      }
    );
  }
addRecon(){
  this.isEditReconsideration=false;
  // get recon_number
  this._commonHttpService.getArrayList({}, 
    `Nextnumbers/getNextNumber?apptype=providerTraining`)
    .subscribe(
    (result) => {
      this.reconNumber = result['nextNumber'];
      this.reconsiderationForm.patchValue({
        'recon_number': this.reconNumber,
      });
      (<any>$('#add-training')).modal('show');
      
    },
    (error) => {
      this._alertService.warn('Unable to create training session!');
    }
  )
}
  private getInformation() {
    this._commonHttpService.getArrayList(
			{
				method: 'get',
				where : { object_id : this.providerId }
			},
    	'publicproviderreconsideration?filter'

		).subscribe(response => {
      this.reconsiderationInfo = response	

		});
  }
  private getInfoFromProviderApproval() {
    this._commonHttpService.getArrayList(
			{
				method: 'get',
				where : {provider_id:this.providerId }
			},
      'providerapproval?filter'
		).subscribe(response => {
      this.nextReconDate = response[0].next_recon_dt ;	


		});
  }
  editReconsiderationInfo(recon){
    this.isEditReconsideration=true;
    this.currentReconId=recon.recon_id;
    this.reconsiderationForm.patchValue(recon);
    (<any>$('#add-reconsideration')).modal('show');
  }

  patchReconsiderationDetails() {
    this.reconsiderationForm.value.object_id=this.providerId;
    this._commonHttpService.patch(
      this.currentReconId,
      this.reconsiderationForm.value,
      'publicproviderreconsideration'
    ).subscribe(
      (response) => {
        this._alertService.success("Information updated successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to update information");
      }

    );
    (<any>$('#add-reconsideration')).modal('hide');

  }

  saveReconsiderationDetails() {
    this.reconsiderationForm.value.object_id=this.providerId;
    this._commonHttpService.create(
      this.reconsiderationForm.value,
      'publicproviderreconsideration/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-reconsideration')).modal('hide');
  }
  deleteReconDetails(recon){
  this.currentReconId=recon.recon_id;
  (<any>$('#delete-recon')).modal('show');
  }
  cancelDelete() {
    this.currentReconId = null;
    (<any>$('#delete-recon')).modal('hide');
  }
  conformDeleteReconDetails() {
    this._commonHttpService.remove(
      this.currentReconId,
      this.reconsiderationForm.value,
      'publicproviderreconsideration/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information Deleted successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to Delete information");
      }

    );
    (<any>$('#delete-recon')).modal('hide');
  }


}
