import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReconsiderationComponent } from './public-reconsideration.component';

describe('PublicReconsiderationComponent', () => {
  let component: PublicReconsiderationComponent;
  let fixture: ComponentFixture<PublicReconsiderationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicReconsiderationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReconsiderationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
