import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderServicesComponent } from './public-provider-services.component';

describe('PublicProviderServicesComponent', () => {
  let component: PublicProviderServicesComponent;
  let fixture: ComponentFixture<PublicProviderServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
