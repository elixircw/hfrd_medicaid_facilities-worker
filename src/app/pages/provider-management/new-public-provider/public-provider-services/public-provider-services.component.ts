import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import {  AlertService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { ProviderService } from '../_entities/newApplicantModel';

@Component({
  selector: 'public-provider-services',
  templateUrl: './public-provider-services.component.html',
  styleUrls: ['./public-provider-services.component.scss']
})
export class PublicProviderServicesComponent implements OnInit {servicePaid = [];
  serviceClassification = [];
  serviceUnit = [];
  serviceStatus = [];
  activityDropdownItems$: DropdownModel[];
  activityDropdownItems1$: Observable<DropdownModel[]>;
  servicesObj:any = {};
  availableTasks: any[] = [];
  tableData = [];
  currentData =[];
  FormMode:string;
  providerId:any;
  providerServiceId: any = '';
  serviceId: any = '';
  servicesList = [];
  providerServicesDataList: ProviderService[] = [];
  isEdit: Boolean = false;
  
    minDate = new Date();
    
    programId: string;
    applicantServiceForm: FormGroup;

  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute,private formBuilder: FormBuilder,private _alertService: AlertService) { 
    this.providerId = route.parent.snapshot.params['id'];
  }
  
  ngOnInit() {this.initServiceForm();
    this.loadDropDown(); 
    this.initServiceForm(); 
    this.getServicesList(); 
    this.getProviderservices();
    this.servicePaid = ['DJS','SSA','LDSS','MSDE','Medicaid','DJS-SSA'];
    this.serviceClassification = ['Educational','Residential'];
    this.serviceUnit = ['Annually','Daily','Hourly'];
    this.serviceStatus = ['Active', 'Expired'];
  }
  private initServiceForm() {
    this.FormMode="Add";
    this.applicantServiceForm = this.formBuilder.group({
      provider_service_id :0,
      provider_id :this.providerId,
      service_id :[''],
      start_dt :new Date(),
      end_dt:new Date(),
      paid_cd :[''],
      primary_sw :[''],
      location_sw :[''],
      create_ts :[''],
      create_user_id :[''],
      update_ts :[''],
      update_user_id :[''],
      delete_sw :[''],
      service_status :[''],
      service_category:[''],
      service_classification :[''],
      service_cost :null,
      service_description :[''],
      service_paid_by :[''],
      service_unit :[''],


    });
  }
  private loadDropDown() {
    this._commonHttpService.getArrayList(
      {
      },
      'providerservice/listservicecategories'
    ).subscribe(taskList => {
      console.log("@@@@@"+taskList);
      this.activityDropdownItems$ = taskList;
    });
  }
  
  private getProviderservices() {
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            provider_id: this.providerId,
          }
      },
      'Publicprovider/getpublicproviderservices')
      .subscribe((response: any) => 
        {
          let responseData = response.data;
          this.providerServicesDataList = responseData.filter(x => x.structure_service_cd === 'S');
          // this.tableData = licenseservices;
          // this.tableData.unshift(this.currentData);
          // console.log("Final table data", this.tableData);

        });      
  }

  onChangeActivity(param: any) {
    this.availableTasks = [];
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          service_category_cd: param
        }
      },
      'providerservice/getserviceslist'
    ).subscribe(res => {
      this.servicesObj = res;
      //console.log(this.servicesObj.data);      
      this.servicesObj.data.forEach(element => {
        var currentData = {};
        currentData['service_id'] = element.service_id;
        currentData['service_nm'] = element.service_nm;
        this.availableTasks.push(currentData);
        console.log("$$$$ " + this.availableTasks);
      });
    });

  }

  private deletepublicproviderservices(obj){
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          provider_service_id: obj.provider_service_id
        }
      },
      'Publicprovider/deletepublicproviderservices'
    ).subscribe(res => {
      this.servicesObj = res;
      this.getProviderservices()
      //console.log(this.servicesObj.data);      
      // this.servicesObj.data.forEach(element => {
      //   var currentData = {};
      //   currentData['service_id'] = element.service_id;
      //   currentData['service_nm'] = element.service_nm;
      //   this.availableTasks.push(currentData);
      //   console.log("$$$$ " + this.availableTasks);
        
      // });
    });
    // (<any>$('#contract-license')).modal('show');
  }


  addServices() {
  this.FormMode = 'Add';
  this.isEdit = false;
  this.availableTasks = [];
  this.applicantServiceForm.reset();
  this.applicantServiceForm.patchValue({
    provider_id : this.providerId,
    start_dt : new Date(),
    end_dt: new Date()
  });
  }

  editServices(obj) {
    console.log(obj);
    this.availableTasks = [];
    this.isEdit = true;
    this.providerServiceId = obj.provider_service_id;
    this.serviceId = obj.service_id;
    this.FormMode = 'Edit';
    this.applicantServiceForm.reset();
    var currentData = {};
    currentData['service_id'] = obj.service_id;
    currentData['service_nm'] = obj.service_nm;
    this.availableTasks.push(currentData);
    this.applicantServiceForm.patchValue({
      provider_id : this.providerId,
      start_dt : obj.start_dt,
      end_dt: obj.end_dt,
      service_category: obj.service_category,
      service_description : obj.service_description,
      service_id : obj.service_id.toString()
    });
    (<any>$('#contract-license')).modal('show');
  }

  updateServices() {
    this.FormMode = "Edit";
    let service = {};
    let serviceId = '';
    let endDate = this.applicantServiceForm.getRawValue().end_dt;
    service['provider_service_id'] = this.providerServiceId;
    service['service_id'] = this.serviceId;
    service['end_dt'] = endDate ? endDate : null;
    console.log(service['end_dt']);
    this._commonHttpService
        .patch(
          serviceId,
          service,
          'tb_provider_services')
        .subscribe(response => {
            if (response) {
              this._alertService.success('Services updated Successfully');
                this.getProviderservices();
                this.isEdit = false;
                (<any>$('#contract-license')).modal('hide');
              }
          }, (error) => {
              this._alertService.error('Unable to update services');
            }
        );
  }

  createNewService(obj) {
    if (this.FormMode === 'Edit') {
      this.updateServices();
    } else {
      this._commonHttpService.create(obj, "Publicprovider/addpublicproviderservices").subscribe(
        (response) => {
          this._alertService.success("Services saved Successfully");
          this.getProviderservices();
          (<any>$('#contract-license')).modal('hide');
        },
        (error) => {
          this._alertService.error("Unable to save services");
        }
      );
    }
  }

  getServicesList() {

    this._commonHttpService.getArrayList({
      where: {structure_service_cd: 'S'},
      method: 'get',
      nolimit: true
    },
      'tb_services?filter'
    ).subscribe(servicesList => {
      console.log("Services", servicesList);
      this.servicesList = servicesList;
    },
      (error) => {
        return false;
      }
    );

  }
}
