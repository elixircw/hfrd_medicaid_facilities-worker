import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderDocumentCreatorComponent } from './public-provider-document-creator.component';

describe('PublicProviderDocumentCreatorComponent', () => {
  let component: PublicProviderDocumentCreatorComponent;
  let fixture: ComponentFixture<PublicProviderDocumentCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderDocumentCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderDocumentCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
