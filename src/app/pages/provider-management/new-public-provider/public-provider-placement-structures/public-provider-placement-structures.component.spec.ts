import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderPlacementStructuresComponent } from './public-provider-placement-structures.component';

describe('PublicProviderPlacementStructuresComponent', () => {
  let component: PublicProviderPlacementStructuresComponent;
  let fixture: ComponentFixture<PublicProviderPlacementStructuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderPlacementStructuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderPlacementStructuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
