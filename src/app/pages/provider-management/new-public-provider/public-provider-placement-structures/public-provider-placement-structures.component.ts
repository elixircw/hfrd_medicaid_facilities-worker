import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import {  AlertService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { ProviderService } from '../_entities/newApplicantModel';

@Component({
  selector: 'public-provider-placement-structures',
  templateUrl: './public-provider-placement-structures.component.html',
  styleUrls: ['./public-provider-placement-structures.component.scss']
})

export class PublicProviderPlacementStructuresComponent implements OnInit {

  serviceClassification = [];
  serviceUnit = [];
  serviceStatus = [];
  servicesObj:any = {};
  availableTasks: any[] = [];
  currentData =[];
  FormMode:string;
  providerId:any;
  servicePaid=[];
  placementStructuresList = [];
  providerPlacementStructuresDataList: ProviderService[] = [];
  
    minDate = new Date();
    
    programId: string;
    applicantServiceForm: FormGroup;

  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute,private formBuilder: FormBuilder,private _alertService: AlertService) { 
    this.providerId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.initServiceForm();
    this.getPlacementList();  
    this.getProviderservices();
    this.servicePaid = ['DJS','SSA','LDSS','MSDE','Medicaid','DJS-SSA'];
    this.serviceClassification = ['Educational','Residential'];
    this.serviceUnit = ['Annually','Daily','Hourly'];
    this.serviceStatus = ['Active', 'Expired'];
  }
  
  private initServiceForm() {
    this.FormMode="Add";
    this.applicantServiceForm = this.formBuilder.group({
      provider_service_id :0,
      provider_id :this.providerId,
      service_id :null,
      // start_dt :new Date(),
      // end_dt:new Date(),
      service_description :[''],
      structure_service_cd: [''],

    });
  }

  private getProviderservices() {
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            provider_id: this.providerId
          }
      },
      'Publicprovider/getpublicproviderservices')
      .subscribe(
        (response: any) => 
        {
          let responseData = response.data;
          this.providerPlacementStructuresDataList = responseData.filter(x => x.structure_service_cd === 'P');
        });      
  }

  private deletepublicproviderservices(obj){
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          provider_service_id: obj.provider_service_id
        }
      },
      'Publicprovider/deletepublicproviderservices'
    ).subscribe(res => {
      this.servicesObj = res;
      this.getProviderservices()
    });
    // (<any>$('#contract-license')).modal('show');
  }

  addServices(){
    this.FormMode="Add";
  }

  createNewService(obj){
    this._commonHttpService.create(obj, "Publicprovider/addpublicproviderservices").subscribe(
      (response) => {
        this.getProviderservices();
        this._alertService.success("Placement Structure saved successfully");
        (<any>$('#add-placement-structure')).modal('hide');
      },
      (error) => {
        this._alertService.error("Unable to save Placement Structures");
      }
    );
   
  }  

  getPlacementList() {

    this._commonHttpService.getArrayList({
      where: {structure_service_cd: 'P'},
      method: 'get',
      nolimit: true
    },
      'tb_services?filter'
    ).subscribe(placementStructuresList => {
      console.log("Placement", placementStructuresList);
      this.placementStructuresList = placementStructuresList;
    },
      (error) => {
        return false;
      }
    );

  }

  closewindow() {
    this.applicantServiceForm.reset();
  }

}