import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { DropdownModel, DynamicObject, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'public-provider-approval',
  templateUrl: './public-provider-approval.component.html',
  styleUrls: ['./public-provider-approval.component.scss']
})
export class PublicProviderApprovalComponent implements OnInit {

  providerApprovalForm: FormGroup;
  providerId: string;
  providerApprovalInfo$: Observable<any>;
  totalRecords$: Observable<number>;
  currentApprovalId: string;
  isEditApprovalInfo: boolean=false;
  paginationInfo: PaginationInfo = new PaginationInfo();
  canDisplayPager$: Observable<boolean>;
  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.providerId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.formIntilizer();
    this.getInformation(1);
   
  }

  formIntilizer() {
    this.providerApprovalForm = this.formBuilder.group(
      {
        provider_id: [''],
        approved_beds_no: null,
        approval_dt: null,
        approval_comments_tx: [''], 
        recommended_children_tx: [''],
        checklist_comments_tx: [''],
        training_completion_dt: null,
        fire_request_dt: null,
        fire_complete_dt: null,
      }
    );
  }
addApprovalInfo(){
  this.isEditApprovalInfo=false;
}

  getInformation(page: number) {
    const source = this._commonHttpService
        .getPagedArrayList(
            new PaginationRequest(
                {
                    method: 'get',
                    where: { providerid : this.providerId },
                    page: this.paginationInfo.pageNumber,
                    limit: this.paginationInfo.pageSize,
                }),
          'providerapproval/getapprovaltype?filter'
        ).map((result: any) => {
            return {
                data: result.data,
                count: result.length > 0 ? result[0].totalcount : 0,
                canDisplayPager: result.length > 0 ? result[0].totalcount > this.paginationInfo.pageSize : false
            };
        }).share();
        this.providerApprovalInfo$ = source.pluck('data');
        console.log(this.providerApprovalInfo$)
    if (page === 1) {
        this.totalRecords$ = source.pluck('count');
        this.canDisplayPager$ = source.pluck('canDisplayPager');
    }
  }


  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    // this.pageStream$.next(this.paginationInfo.pageNumber);
  }

  editProviderApprovalInfo(approval){
    this.isEditApprovalInfo=true;
    this.currentApprovalId=approval.providerapprovalid;
    this.providerApprovalForm.patchValue(approval);
    (<any>$('#add-approval')).modal('show');
  }

  patchProviderApprovalDetails() {
    this.providerApprovalForm.value.provider_id=this.providerId;
    this._commonHttpService.patch(
      this.currentApprovalId,
      this.providerApprovalForm.value,
      'providerapproval'
    ).subscribe(
      (response) => {
        this._alertService.success("Information updated successfully!");
        this.getInformation(1);
      },
      (error) => {
        this._alertService.error("Unable to update information");
      }

    );
    (<any>$('#add-approval')).modal('hide');

  }

  saveApprovalDetails() {
    this.providerApprovalForm.value.provider_id=this.providerId;
    this._commonHttpService.create(
      this.providerApprovalForm.value,
      'providerapproval'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation(1);
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-approval')).modal('hide');
  }
  deleteApprovalDetails(approval){
  this.currentApprovalId=approval.providerapprovalid;
  (<any>$('#delete-approval')).modal('show');
  }
  cancelDelete() {
    this.currentApprovalId = null;
    (<any>$('#delete-approval')).modal('hide');
  }
  conformDeleteApprovalDetails() {
    this._commonHttpService.remove(
      this.currentApprovalId,
      this.providerApprovalForm.value,
      'providerapproval'
    ).subscribe(
      (response) => {
        this._alertService.success("Information Deleted successfully!");
        this.getInformation(1);
      },
      (error) => {
        this._alertService.error("Unable to Delete information");
      }

    );
    (<any>$('#delete-approval')).modal('hide');
  }


}
