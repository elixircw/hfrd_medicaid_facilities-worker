import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderApprovalComponent } from './public-provider-approval.component';

describe('PublicProviderApprovalComponent', () => {
  let component: PublicProviderApprovalComponent;
  let fixture: ComponentFixture<PublicProviderApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
