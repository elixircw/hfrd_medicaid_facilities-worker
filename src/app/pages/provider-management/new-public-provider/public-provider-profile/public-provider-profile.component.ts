import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
@Component({
  selector: 'public-provider-profile',
  templateUrl: './public-provider-profile.component.html',
  styleUrls: ['./public-provider-profile.component.scss']
})
export class PublicProviderProfileComponent implements OnInit {


  providerProfileForm: FormGroup;
  providerId: string;
  profileInfo = [];
  currentProviderId: string;
  isEditProfileInfo: boolean = false;
  addressInfo = [];
  locationAddress: string;
  paymentAddress: string;

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.providerId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    this.formIntilizer();
    this.getInformation();
    this.getProviderAddress();
  }

  formIntilizer() {
    this.providerProfileForm = this.formBuilder.group(
      {
        provider_id: [''],
        tax_id_no: [''],
        admission_comments_tx: [''],
        business_start_tm: [''],
        business_end_tm: [''],
        medical_license_no_tx: [''],
        medical_speciality_tx: [''],
        school_district_tx: [''],
        dob_dt: [''],
        adr_work_phone_tx: [''],
        adr_home_phone_tx: [''],
        adr_cell_phone_tx: [''],
        co_first_nm: [''],
        co_last_nm: [''],
        co_ssn_no: [''],
      }
    );
  }
  addProfileInfo() {
    this.isEditProfileInfo = false;
  }
  private getInformation() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: { provider_id: this.providerId }
      },
      'providerinfo/getproviderinfo?filter'
    ).subscribe(response => {
      if (response && response.length) {
        this.profileInfo = response[0].listproviderinfo;
        }
    });
  }
  private getProviderAddress() {
    this.locationAddress = '';
    this.paymentAddress = '';
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: { providerid: this.providerId },
        page: 1,
        limit: 10
      },
      'tb_provider_services/getprovideraddress?filter'
    ).subscribe(response => {
      if (response[0].getpersonaddressphonenumber[0].addressjson && response[0].getpersonaddressphonenumber[0].addressjson.length) {
        let providerAdd = response[0].getpersonaddressphonenumber[0].addressjson;
        providerAdd.map((address) => {
          if (address.typedescription === 'Provider Location') {
            if (this.locationAddress === '') {
              this.locationAddress = address.address;
              if (address.city) {
                this.locationAddress = this.locationAddress + ', ' + address.city;
              }
              if (address.state) {
                this.locationAddress = this.locationAddress + ', ' + address.state;
              }
              if (address.country) {
                this.locationAddress = this.locationAddress + ', ' + address.country;
              }
              if (address.zipcode) {
                this.locationAddress = this.locationAddress + ', ' + address.zipcode;
              }
            }
          }
          if (address.typedescription === 'Provider Payment') {
            if (this.paymentAddress === '') {
              this.paymentAddress = address.address;
              if (address.city) {
                this.paymentAddress = this.paymentAddress + ', ' + address.city;
              }
              if (address.state) {
                this.paymentAddress = this.paymentAddress + ', ' + address.state;
              }
              if (address.country) {
                this.paymentAddress = this.paymentAddress + ', ' + address.country;
              }
              if (address.zipcode) {
                this.paymentAddress = this.paymentAddress + ', ' + address.zipcode;
              }
            }
          }
      });
        this.addressInfo = response;
      }
    });
  }
  editProfileInfo(profile) {
    this.isEditProfileInfo = true;
    this.currentProviderId = profile.provider_id;
    this.providerProfileForm.patchValue(profile);
    (<any>$('#add-profile')).modal('show');
  }

  patchProfileDetails() {
    this.providerProfileForm.value.provider_id = this.providerId;
    this._commonHttpService.patch(
      this.currentProviderId,
      this.providerProfileForm.value,
      'providerinfo'
    ).subscribe(
      (response) => {
        this._alertService.success('Information updated successfully!');
        this.getInformation();
        this.getProviderAddress();
      },
      (error) => {
        this._alertService.error('Unable to update information');
      }

    );
    (<any>$('#add-profile')).modal('hide');

  }

  saveProfileDetails() {
    this.providerProfileForm.value.provider_id = this.providerId;
    this._commonHttpService.create(
      this.providerProfileForm.value,
      'providerinfo'
    ).subscribe(
      (response) => {
        this._alertService.success('Information saved successfully!');
        this.getInformation();
        this.getProviderAddress();
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
    (<any>$('#add-profile')).modal('hide');
  }
  deleteProfileDetails(profile) {
    this.currentProviderId = profile.provider_id;
    (<any>$('#delete-profile')).modal('show');
  }
  cancelDelete() {
    this.currentProviderId = null;
    (<any>$('#delete-profile')).modal('hide');
  }
  conformDeleteProfileDetails() {
    this._commonHttpService.remove(
      this.currentProviderId,
      this.providerProfileForm.value,
      'providerinfo'
    ).subscribe(
      (response) => {
        this._alertService.success('Information Deleted successfully!');
        this.getInformation();
        this.getProviderAddress();
      },
      (error) => {
        this._alertService.error('Unable to Delete information');
      }

    );
    (<any>$('#delete-profile')).modal('hide');
  }


}

