import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderProfileComponent } from './public-provider-profile.component';

describe('PublicProviderProfileComponent', () => {
  let component: PublicProviderProfileComponent;
  let fixture: ComponentFixture<PublicProviderProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
