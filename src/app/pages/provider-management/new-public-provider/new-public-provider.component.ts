import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, AuthService, DataStoreService } from '../../../@core/services';
import { ProviderReferral } from './_entities/newApplicantModel';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingUser } from '../../provider-referral/_entities/existingreferralModel';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { AppUser } from '../../../@core/entities/authDataModel';
import { PublicPorviderTabs } from './new-public-provider-tab.config';
declare var $:any;


@Component({
  selector: 'new-public-provider',
  templateUrl: './new-public-provider.component.html',
  styleUrls: ['./new-public-provider.component.scss']
})
export class NewPublicProviderComponent implements OnInit {
  tabs = PublicPorviderTabs;

  constructor(private _commonHttpService: CommonHttpService, private _router: Router,
    private _authService: AuthService, private _alertService: AlertService,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService
  ) {

  }

  referralInfoDetails: ProviderReferral = new ProviderReferral();
  provInfo: any;
  providerId: string;
  applicants: any;
  assignedInSection = [];
  isLengthGrater: boolean;
  currentData: any;
  selectedPerson: any;
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  isSupervisor: boolean;
  asssignedDetails: any;
  currentUser: AppUser;
  isPaymentWithhold: boolean;


  ngOnInit() {
    this.currentUser = this._authService.getCurrentUser();
    this.providerId = this.route.snapshot.params['id'];
    this._dataStoreService.setData('PROVIDER_ID', this.providerId);
    this.getProviderDetails();
    this.assignDetails();

    // this.userNames();
    this.refreshScrollingTabs();
  }

  private getProviderDetails() {

    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { "providerid": this.providerId }
      }, 'providerinfoconfig/list?filter',
    ).subscribe(response => {
      if(response && Array.isArray(response) && response.length) {
        this.applicants = response[0].providerhomeapprovelist;
        this.provInfo = response[0].providerhomeapprovelist;
        this._dataStoreService.setData('publicProviderInfo', response[0].providerhomeapprovelist);
        if (this.provInfo && this.provInfo.withhold_payment_sw) {
          if (this.provInfo.withhold_payment_sw === 'Y') {
            this.isPaymentWithhold = true;
          }
        }
        this.referralInfoDetails = response[0].providerhomeapprovelist[0];
      }
    });
  }

  private assignDetails() {
    this.asssignedDetails = {
      eventcode: 'PAOW',
      fromsecurityusersid: '',
      tosecurityusersid: '',
      fromroleid: '',
      toroleid: '',
      objectid: this.providerId,
    }
  }

  private userNames() {
    this.assignedInSection = [];

    this._commonHttpService.create(
      {
        method: 'post',
        where: {
          eventcode: 'PAOW',
          objectid: this.providerId
        }
      },
      'providerassignmentownership/getassignownership'
    )
      .subscribe(
        (response) => {
          if (response && Array.isArray(response) && response.length) {
            console.log("$$$$$$$$$$", response[0]);
            console.log("$$$$$$$$$$", response.length);
            if (response.length >= 2) {
              this.isLengthGrater = true
            };
            console.log("Rahul before assigned in section" + this.assignedInSection);
            for (var i = 0; i <= response.length; i++) {
              this.currentData = { "username": response[i].ownername };
              this.assignedInSection.push(this.currentData);
              console.log("$$$$$$$$$$", this.assignedInSection);
            }
            // this.assignedInSection=[{"username": response[0].ownername}];
            // this.assignedInSection=[{"username": response[1].ownername}];
          }
        }, (error) => {
          this._alertService.error('Unable to fetch saved Applicant details.');
          console.log('Get applicant details Error', error);
          return false;
        }

      );
  }


  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
  }


  getRoutingUser(modal) {
    //this.getServicereqid = modal.provider_referral_id;
    //  this.getServicereqid = "R201800200374";
    // console.log(modal.provider_referral_id);
    // this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PTA' },
          method: 'post'
        }),
        'providerassignmentownership/getroutingusers'
      )
      .subscribe(result => {
        // this.userNames();
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }


  selectPerson(row) {
    this.selectedPerson = row;

    // THIS IS THE PERSON LOGGED IN WHICH IS (FROM)

    this.asssignedDetails.fromsecurityusersid = this.currentUser.user.securityusersid;
    this.asssignedDetails.fromroleid = this.currentUser.role.name;
    console.log("@@@@@@@@@@@", this.currentUser);

    // THIS IS THE PERSON WE SELECTED ON THE SCREEN WHICH IS (TO)
    this.asssignedDetails.tosecurityusersid = row.userid;
    this.asssignedDetails.toroleid = row.userrole;

  }

  assignNewUser() {
    if (this.selectedPerson) {
      var payload = {}
      payload = this.asssignedDetails;
      this._commonHttpService.create(
        payload,
        'providerassignmentownership'
      ).subscribe(
        (response) => {
          this._alertService.success("Ownership assigned successfully!");
          this.userNames();
          (<any>$('#intake-caseassignnew')).modal('hide');
        },
        (error) => {
          this._alertService.error("Unable to save Ownership");
        });
    }

  }

  refreshScrollingTabs() {
    const __this = this;
    this.getCurrentTabDetails();
    const tabsLiContent = this.tabs.map(function (tab) {
      return '<li role="presentation" title="' + tab.title + '" data-trigger="hover" class="custom-li" routerLinkActive="active"></li>';
    });
    // this.tabs.forEach(tab => tab.active = false);
    const tabsPostProcessors = this.tabs.map(function (tab) {
      return function ($li, $a) {
        $a.attr('href', '');
        if (tab.active) {
          $a.attr('class', 'active');
        }
        $a.click(function () {
          __this.tabs.forEach(filteredTab => {
            filteredTab.active = false;
          });
          tab.active = true;
          __this._router.navigate([tab.route], { relativeTo: __this.route });
        });
      };
    });
    (<any>$('#public-applicant-tabs')).html('');
    (<any>$('#public-applicant-tabs')).scrollingTabs({
      tabs: __this.tabs,
      propPaneId: 'path', // optional - pass in default value for demo purposes
      propTitle: 'name',
      disableScrollArrowsOnFullyScrolled: true,
      tabsLiContent: tabsLiContent,
      tabsPostProcessors: tabsPostProcessors,
      scrollToActiveTab: true,
      tabClickHandler: function (e) {
        $(__this).addClass('active');
        $(__this).parent().addClass('active');
      }
    });


  }

  private getCurrentTabDetails() {
    this.tabs.forEach(tab => tab.active = false);
    const urlSegments = this._router.url.split('/');
    let tab: any;
    this.tabs.forEach(tabObj => {
      if (urlSegments.includes(tabObj.route)) {
        tab = tabObj;
        tab.active = true;
        return;
      }
    });
    return tab;
  }

  changePaymentWithholding() {
    let payload = {};
    payload['provider_id'] = this.providerId;
    if (this.isPaymentWithhold == true) {
      // Reverse to the desired value
      payload['withhold_payment_sw'] = 'N';
    } else {
      payload['withhold_payment_sw'] = 'Y';
    }
    this._commonHttpService.patch(
      this.providerId,
      payload,
      'providerinfo'
    ).subscribe(
      (response) => {
        this.isPaymentWithhold = !this.isPaymentWithhold;
        this.triggerReleasePaymentSP();
        this._alertService.success("Payment withhold changed!");
        (<any>$('#confirm-withhold-payment-change')).modal('hide');
      }
    );

  }

  triggerReleasePaymentSP() {
    this._commonHttpService.getArrayList(
      {
        where: { provider_id: this.providerId },
        method: 'get',
        nolimit: true
      },
      'tb_provider/updateReleasePayment?filter'
    ).subscribe();
  }
}
