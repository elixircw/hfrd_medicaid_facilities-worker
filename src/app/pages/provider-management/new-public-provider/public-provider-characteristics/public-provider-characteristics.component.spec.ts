import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderCharacteristicsComponent } from './public-provider-characteristics.component';

describe('PublicProviderCharacteristicsComponent', () => {
  let component: PublicProviderCharacteristicsComponent;
  let fixture: ComponentFixture<PublicProviderCharacteristicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderCharacteristicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderCharacteristicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
