import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Input, Output, EventEmitter } from '@angular/core';
import { PublicProviderAddCharacteristicsComponent } from '../public-provider-add-characteristics/public-provider-add-characteristics.component';

import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
// import { ApplicantUrlConfig } from '../../provider-management-url.config';
import { Subject } from 'rxjs';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
@Component({
  selector: 'public-provider-characteristics',
  templateUrl: './public-provider-characteristics.component.html',
  styleUrls: ['./public-provider-characteristics.component.scss']
})
export class PublicProviderCharacteristicsComponent implements OnInit {

  tableData = [];
  currentData =[];
  FormMode:string;
    id: string;

  @ViewChild('addServices', { read: ViewContainerRef }) container: ViewContainerRef;
  @Output() setActivePlacements = new EventEmitter<boolean>();

    minDate = new Date();    
    // providerId: string;
    applicantId: string;
    picklistTypeId:string;
    contractInfoForm: FormGroup;
    applChildCharId:any;
  // constructor(private _cfr: ComponentFactoryResolver, private formBuilder: FormBuilder,
  //   private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private _alertService: AlertService) {
  //   this.id = route.snapshot.params['id'];
  // }

  monitorForm: FormGroup;
  isMonthDisabled: boolean = true;
  isYearDisabled: boolean = true;
  activityTaskStatusFormGroup: FormGroup;
  tasktypestatus = [];
  taskList = [];
  @Input()
  programTypeInputForMonitoring = new Subject<string>();
  programType: string;
  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute,private formBuilder: FormBuilder,
    private _alertService: AlertService,private _cfr: ComponentFactoryResolver,
    private _dataStoreService: DataStoreService) {
    this.applicantId = route.snapshot.parent.params['id'];
    // this.providerId = '5000543';
    this.picklistTypeId= '43';
    this.id = route.parent.snapshot.parent.params['id'];
   } 

  ngOnInit() {
  this.getProviderContractChar();
    this.activityTaskStatusFormGroup = this.formBuilder.group({
      task: this.formBuilder.array([])
    });
    // this.getTaskList();
    this.programTypeInputForMonitoring.subscribe((data) => {
      this.programType = data;
    })
    this.tasktypestatus = ['Completed', 'Incomplete', 'N/A'];
  }

  createTaskForm() {
    return this.formBuilder.group({
      paid_non_paid_cd: [''],
      service_id: [''],
      service_nm: [''],
      structure_service_cd: ['']
    });
  }

  private getTaskList() {
    this.taskList.length = 0;
    this._commonHttpService.create(
      {
        method: 'post',
        applicant_id: this.id,
        category: 'Monitoring',
        from: 'Monitoring',
        monitoring_type: this.monitorForm.value.periodic,
        monitoring_year: this.monitorForm.value.yearly,
        monitoring_period: this.monitorForm.value.monthly
      },
     'providerapplicant/getchecklist'
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      this.setFormValues();
    });
  }

  private buildTaskForm(x): FormGroup {
    return this.formBuilder.group({
      // paid_non_paid_cd: x.paid_non_paid_cd,
      picklist_value_cd: x.picklist_value_cd,
      value_tx: x.value_tx,
      // structure_service_cd: x.structure_service_cd
    });
  }

  setFormValues() {
    this.activityTaskStatusFormGroup.setControl('task', this.formBuilder.array([]));
    const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    this.taskList.forEach((x) => {
      control.push(this.buildTaskForm(x));
    })
    console.log(this.activityTaskStatusFormGroup.value);
    console.log(this.activityTaskStatusFormGroup.controls);
  }

 
  openMonitoringCheckList() {
    // check and resolve the component
    let comp = this._cfr.resolveComponentFactory(PublicProviderAddCharacteristicsComponent);
    // Create component inside container
    let expComponent = this.container.createComponent(comp);
    // see explanations
    expComponent.instance._ref = expComponent;
    expComponent.instance.category = 'Monitoring';
    expComponent.instance.type = this.programType;
    expComponent.instance.onSaveEventEmitter.subscribe(($event) => {
      // $event.forEach((x) => {
      //   this.taskList.push(x);
      // })
      this.setFormValues();
      this.getProviderContractChar();  
    });
  }


  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.monitorForm.controls['monthly'].setValue('');
    this.monitorForm.controls['yearly'].setValue('');
  }

  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
   
  }
  private getProviderContractChar() {
    this.tableData = [];
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            applicant_id: this.applicantId,
            picklist_type_id: this.picklistTypeId
          }
      },
      'publicproviderapplicant/getapplicantchildcharacteristics')
      .subscribe(licenseservices =>
        {
          if (licenseservices) {
            this.tableData = licenseservices;
            this._dataStoreService.setData('addedCharacteristics', this.tableData);
            // this.tableData.unshift(this.currentData);
            console.log("Final table data", this.tableData);
          }
        });
  }

  DeleteChar(applChildCharId){
    this.applChildCharId=applChildCharId;
    (<any>$('#delet-char')).modal('show');
  }
  deleteConfirm(){
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            applicant_id: this.applicantId,
            picklist_type_id: this.applChildCharId
          }
      },
      'publicproviderapplicant/deleteapplicantchildcharacteristics')
      .subscribe(licenseservices => 
        {
          if (licenseservices) {
            this._alertService.success("Characteristics Deleted successfully");
          this.getProviderContractChar();
          this.applChildCharId="";
          (<any>$('#delet-char')).modal('hide');
          }
        }); 
  }

  viewActivity() {
    // this.getTaskList();
  }

}
