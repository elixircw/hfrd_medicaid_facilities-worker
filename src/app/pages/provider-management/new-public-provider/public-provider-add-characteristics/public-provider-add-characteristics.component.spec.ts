import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderAddCharacteristicsComponent } from './public-provider-add-characteristics.component';

describe('PublicProviderAddCharacteristicsComponent', () => {
  let component: PublicProviderAddCharacteristicsComponent;
  let fixture: ComponentFixture<PublicProviderAddCharacteristicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderAddCharacteristicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderAddCharacteristicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
