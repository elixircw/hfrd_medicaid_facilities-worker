import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderServiceTrainingComponent } from './public-provider-service-training.component';

describe('PublicProviderServiceTrainingComponent', () => {
  let component: PublicProviderServiceTrainingComponent;
  let fixture: ComponentFixture<PublicProviderServiceTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderServiceTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderServiceTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
