import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { AlertService } from '../../../../@core/services/alert.service';
import { MatTableDataSource } from '@angular/material';
import { DataStoreService } from '../../../../@core/services';
export interface orientationListTable {
	trainingNo: string;
	trainingDate: string;
	trainingDescription: string;
	trainingStartTime: string;
	trainingEndTime: string;
	sessionType: string;
	
}
@Component({
  selector: 'public-provider-service-training',
  templateUrl: './public-provider-service-training.component.html',
  styleUrls: ['./public-provider-service-training.component.scss']
})
export class PublicProviderServiceTrainingComponent implements OnInit {

  selection = new SelectionModel<orientationListTable>(true, []);
	dataSource: any;
  providerId: string;
  duration: number=0;
	trainingList = [];
	assignedTrainingList = [];
	assignedAppliantTrainingList=[];
	publicProviderInfo: any;
	attendee_first_nm: string;
	attendee_last_nm: string;
	applicantId: string;
	displayedColumns: string[] = ['select', 'trainingNo', 'trainingDate', 'trainingDescription', 'trainingStartTime', 'trainingEndTime', 'sessionType'];

	constructor(private formBuilder: FormBuilder,
		private _commonHttpService: CommonHttpService,
		private _alertService: AlertService,
		private _router: Router, private route: ActivatedRoute,
		private _dataStoreService: DataStoreService
		) {
		this.providerId = route.parent.snapshot.params['id'];
	}
	ngOnInit() {
		this.getTrainingList();
		this.getassignedTrainingSessions();
		this.getApplicantNumber();
	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}
	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}
	getTrainingList() {
		this._commonHttpService.getArrayList(
      {
        method: 'get',
				where : { training_type : 'In-Service Training' }
			},
			'providerorientationtraining?filter'
		).subscribe(response => {
			this.trainingList = response
			console.log('this.trainingList', this.trainingList);
			this.dataSource = new MatTableDataSource<orientationListTable>(this.trainingList);

		});
	}

	getApplicantInformation(): boolean {
		this.publicProviderInfo = this._dataStoreService.getData('publicProviderInfo');
		this.attendee_first_nm = this.publicProviderInfo[0].provider_first_nm;
		this.attendee_last_nm = this.publicProviderInfo[0].provider_last_nm;
	
		if (!this.attendee_first_nm || this.attendee_first_nm==='' || !this.attendee_last_nm || this.attendee_last_nm==='') {
			this._alertService.error('Applicant name is missing!')
			return false;
		} else {
			return true;
		}
	}

	assignSelectedTrainingSessions() {
		const numSelected = this.selection.selected.length;
		var payload = {};
		payload = this.selection.selected;
		if (this.getApplicantInformation()) {
			for (var i = 0; i < numSelected; i++) {
				payload[i]['object_id'] = this.providerId;
				payload[i]['phase'] = 'Provider';
				payload[i]['attendee_first_nm'] = this.attendee_first_nm;
				payload[i]['attendee_last_nm'] = this.attendee_last_nm;
			}
			this._commonHttpService.create(
				payload,
				'providerorientationtrainingattendance/'
			).subscribe(
				(response) => {
					this._alertService.success("Orientations assigned successfully!");
					this.getassignedTrainingSessions();
					(<any>$('#assign-orientation')).modal('hide');
				},
				(error) => {
					this._alertService.error("Unable to assign Orientations");
				}
			);
		}
	}
	getassignedTrainingSessions() {
		this._commonHttpService.create(
			{
				where: {
           object_id: this.providerId,
           training_type: 'In-Service Training'
          },
				method: 'post'
			},
			'providerorientationtrainingattendance/getassignedorientations'

		).subscribe(response => {
			this.assignedTrainingList = response;
      console.log('this.assignedTrainingList', this.assignedTrainingList);
      this.calculateDuration();
		},
			(error) => {
				this._alertService.error('Unable to get assigned sessions, please try again.');
				console.log('get contact Error', error);
				return false;
			}
		);
  }
 calculateDuration(){
   for(var i=0;i<this.assignedTrainingList.length;i++){
     if(this.assignedTrainingList[i].is_attended){
      var x=this.assignedTrainingList[i].duration;
      this.duration=this.duration+x;
      console.log('this.duration', this.duration);
     }     
   }
 } 
	patchIntentToAttend(session) {	
		console.log('session', session);
		var isIntentAttend: boolean;
		if (session.is_intent_to_attend === false) {
			isIntentAttend = true;
		} else if (session.is_intent_to_attend === true) {
			isIntentAttend = false;
		};
		var payload = {}
		payload['is_intent_to_attend'] = isIntentAttend;
		console.log('payload', payload);
		this._commonHttpService.patch(
			session.orientation_training_attendance_id,
			payload,
			'providerorientationtrainingattendance'
		).subscribe(
			(response) => {
				this._alertService.success("Orientations assigned successfully!");
				this.getassignedTrainingSessions();
			},
			(error) => {
				this._alertService.error("Unable to assign Orientations");
			});
	}

	 getApplicantNumber() {
		this._commonHttpService.getArrayList(
				{
					method: 'get',
					where : { provider_id : this.providerId }
				},
				'providerapprovalphaserecord?filter'
			).subscribe(response => {
				this.applicantId = response[0].applicant_id;
				this.getApplicantAssignedTraining();	
			});
	  }


	getApplicantAssignedTraining() {
		this._commonHttpService.create(
			{
				where: {
					object_id: this.applicantId,
					training_type: 'Pre-Service Training'
				},
				method: 'post'
			},
			'providerorientationtrainingattendance/getassignedorientations'

		).subscribe(response => {
			this.assignedAppliantTrainingList = response;
			console.log('this.assignedAppliantTrainingList', this.assignedAppliantTrainingList);
			this.calculateDuration();
			this.getTrainingList();
		},
			(error) => {
				this._alertService.error('Unable to get assigned sessions, please try again.');
				console.log('get contact Error', error);
				return false;
			}
		);
	}
}
