import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPublicProviderComponent } from './new-public-provider.component';

describe('NewPublicProviderComponent', () => {
  let component: NewPublicProviderComponent;
  let fixture: ComponentFixture<NewPublicProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPublicProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPublicProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
