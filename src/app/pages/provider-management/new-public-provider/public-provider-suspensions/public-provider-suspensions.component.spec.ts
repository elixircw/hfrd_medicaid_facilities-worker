import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderSuspensionsComponent } from './public-provider-suspensions.component';

describe('PublicProviderSuspensionsComponent', () => {
  let component: PublicProviderSuspensionsComponent;
  let fixture: ComponentFixture<PublicProviderSuspensionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderSuspensionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderSuspensionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
