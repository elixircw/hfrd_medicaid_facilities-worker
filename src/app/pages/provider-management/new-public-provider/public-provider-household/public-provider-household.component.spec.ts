import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicProviderHouseholdComponent } from './public-provider-household.component';

describe('PublicProviderHouseholdComponent', () => {
  let component: PublicProviderHouseholdComponent;
  let fixture: ComponentFixture<PublicProviderHouseholdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicProviderHouseholdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicProviderHouseholdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
