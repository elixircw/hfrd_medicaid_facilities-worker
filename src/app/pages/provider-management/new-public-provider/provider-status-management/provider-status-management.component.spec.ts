import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderStatusManagementComponent } from './provider-status-management.component';

describe('ProviderStatusManagementComponent', () => {
  let component: ProviderStatusManagementComponent;
  let fixture: ComponentFixture<ProviderStatusManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderStatusManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderStatusManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
