import { Component, Input, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { DataStoreService, CommonDropdownsService } from '../../../../@core/services';
import { PaginationRequest, DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
@Component({
  selector: 'provider-status-management',
  templateUrl: './provider-status-management.component.html',
  styleUrls: ['./provider-status-management.component.scss']
})
export class ProviderStatusManagementComponent implements OnInit {
 
  statusManagementForm: FormGroup;
  requestForm: FormGroup;
  providerId: string;
  statusManagementInfo= [];
  currentStatusManagementId: string;
  isEditStatusManagement: boolean=false;
  providerStatus=[];
  approveReq: boolean;
  isReject: boolean;
  dirtyStatus: boolean;
  currentFormValue={};
  is3yearsOld: boolean = false;
  program: any;
  providerInfo: any;
  usersList: any[];
  approvalTypes: any[];
  communication_mediums: { id: number; type: string; }[];
  programsTypes$: Observable<DropdownModel[]>;
  maxDate = new Date();
  
  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private _commonDropDownService: CommonDropdownsService,) {
    this.providerId = route.parent.snapshot.params['id'];
  }

  ngOnInit() {
    const providerInfo = this._dataStoreService.getData('publicProviderInfo');
    this.is3yearsOld = (providerInfo) ? providerInfo[0].is3yrsold: false;
    this.program = (providerInfo) ? providerInfo[0].program : '';
    this.formIntilizer();
    this.initRequestForm();
    this.getInformation();
    this.providerStatus=["Open","Closed","On-Hold"];
    this.approveReq= false;
    this.isReject = false;
    this.dirtyStatus = false;
  }

  formIntilizer() {
    this.statusManagementForm = this.formBuilder.group(
      {
        object_id: [''],
        narrative: [''],
        provider_status: [''],
        approval_status:[''],
        dirty_status:['']
      }
    );
  }

  addStatusManagement(){
    this.isEditStatusManagement=false;
    this.approveReq= false;
    this.dirtyStatus= false;
  }
  private getInformation() {
    this._commonHttpService.getArrayList(
			{
				method: 'get',
				where : { object_id : this.providerId }
			},
			'publicproviderstatusmanagement?filter'
		).subscribe(response => {
			this.statusManagementInfo = response	

		});
  }
  editInfo(status){
    this.isEditStatusManagement=true;
    this.currentStatusManagementId=status.provider_status_management_id;
    this.statusManagementForm.patchValue(status);
    (<any>$('#add-status')).modal('show');
  }

  patchDetails() {
    this.currentFormValue['dirty_status'] = this.dirtyStatus;
    this.statusManagementForm.value.object_id = this.providerId;
    this._commonHttpService.patch(
      this.currentStatusManagementId,
      this.currentFormValue,
      'publicproviderstatusmanagement'
    ).subscribe(
      (response) => {
        this._alertService.success("Information updated successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to update information");
      }

    );
    (<any>$('#add-status')).modal('hide');

  }
  approveRequest(status,decision){
    this.currentStatusManagementId=status.provider_status_management_id;
    this.currentFormValue= status;
    this.dirtyStatus=true;
    // this.patchDetails();
    // this.updateStatusManagement(status.provider_status_management_id);
    this.dirtyStatus=true;
    this.approveReq=true;
    if(decision==='Approval'){
      this.updateStatusManagement(status);
      var payload= {};
      if(status.provider_status==='Open'){
        payload['provider_status_cd']='1791'
      }
      else if(status.provider_status==='Closed'){
        payload['provider_status_cd']='1792'
      }
      this._commonHttpService.patch(
        this.providerId,
        payload,
        'providerinfo'
      ).subscribe(
        (response) => {         
        },
        (error) => {
          this._alertService.error("Unable to update information");
        }  
      );
    }
    else if (decision ==='Reject'){
      this.isReject=true;
      this.updateStatusManagement(status);
    }
    }

  updateStatusManagement(status) {
    if (this.approveReq === true) {
      if (this.isReject === true) {
        this.statusManagementForm.value.approval_status = 'Rejected';
      } else if (this.isReject === false){
        this.statusManagementForm.value.approval_status = 'Approved';
      }
    } else if (this.approveReq === false) {
      this.statusManagementForm.value.approval_status = 'Requested';
    }
    this.statusManagementForm.value.narrative = status.narrative;
    this.statusManagementForm.value.provider_status = status.provider_status;
    this.statusManagementForm.value.object_id = this.providerId;
    this.statusManagementForm.value.dirty_status = this.dirtyStatus;
    this.statusManagementForm.value.provider_status_management_id = status.provider_status_management_id;
    this._commonHttpService.update(
      status.provider_status_management_id,
      this.statusManagementForm.value,
      'publicproviderstatusmanagement'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-status')).modal('hide');
    this.statusManagementForm.reset();
    this.isReject = false;

    // this.currentFormValue['dirty_status'] = this.dirtyStatus;
    // this.currentFormValue['provider_status_management_id'] = statusManagementId;
    // this.statusManagementForm.value.object_id = this.providerId;
    // this._commonHttpService.update(
    //   this.currentStatusManagementId,
    //   this.currentFormValue,
    //   'publicproviderstatusmanagement'
    // ).subscribe(
    //   (response) => {
    //     this._alertService.success("Information updated successfully!");
    //     this.getInformation();
    //   },
    //   (error) => {
    //     this._alertService.error("Unable to update information");
    //   }

    // );
    // (<any>$('#add-status')).modal('hide');
  }
    saveDetails(status) { 
    if(this.approveReq === true){
      if(this.isReject === true){
        this.statusManagementForm.value.approval_status= 'Rejected';
      } else if (this.isReject === false){
        this.statusManagementForm.value.approval_status= 'Approved';
      }
    }
    else if(this.approveReq === false){
      this.statusManagementForm.value.approval_status= 'Requested';
    }
    this.statusManagementForm.value.narrative=status.narrative;
    this.statusManagementForm.value.provider_status= status.provider_status;
    this.statusManagementForm.value.object_id=this.providerId;
    this.statusManagementForm.value.dirty_status=this.dirtyStatus;

    this._commonHttpService.create(
      this.statusManagementForm.value,
      'publicproviderstatusmanagement/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-status')).modal('hide');
    this.statusManagementForm.reset();
    this.isReject=false;
  }
  deleteDetails(status){
  this.currentStatusManagementId=status.provider_status_management_id;
  (<any>$('#delete-status')).modal('show');
  }
  cancelDelete() {
    this.currentStatusManagementId = null;
    (<any>$('#delete-status')).modal('hide');
  }
  conformDeleteDetails() {
    this._commonHttpService.remove(
      this.currentStatusManagementId,
      this.statusManagementForm.value,
      'publicproviderstatusmanagement/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information Deleted successfully!");
        this.getInformation();
      },
      (error) => {
        this._alertService.error("Unable to Delete information");
      }

    );
    (<any>$('#delete-status')).modal('hide');
  }
  
  initRequestForm() {
    this.requestForm = this.formBuilder.group(
      {
        approval_type: [null, [Validators.required]],
        communication: [null, [Validators.required]],
        requested_date: [null, [Validators.required]],
        comments: [''],
        tosecurityuserid: [null, [Validators.required]],
        isclosereopen: [null]
      }
    );
  }

  loadUserList() {
    this.usersList = [];
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PRASS' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.usersList = result.data;
      });
  }

  

  loadDropDowns() {
    // const source = forkJoin([
    //   this._commonHttpService.getArrayList(
    //     {
    //       method: 'post',
    //       nolimit: true,
    //       providertype:'Public'
    //     },
    //     'providerreferral/listprogramnames'
    //     ),   
    // ])
    //   .map((result) => {
    //     return {
    //       programsTypes: result[0].map(
    //         (res) =>
    //           new DropdownModel({
    //             text: res.programtype,
    //             value: res.programnames
    //           })
    //       ),
    //     };
    //   })
    //   .share();
  
    // this.programsTypes$ = source.pluck('programsTypes');
    // this.programsTypes$.forEach(programsType =>{
    //   programsType.forEach(element => {
    //     if(element.text === 'Resource Home'){
    //       this.approvalTypes = element.value;
    //     }
    //   })
    // });

    this._commonDropDownService.getPickListByName('homeapprovaltypes').subscribe(response => {
      if (Array.isArray(response)) {
        if(this.program && this.program === 'Resource Home') {
           this.approvalTypes = response;
        }
      }
    });

    this.communication_mediums = [
      { id: 1, type: 'Phone' },
      { id: 2, type: 'Mail' },
      { id: 3, type: 'E-mail' },
      { id: 4, type: 'Fax' },
      { id: 5, type: 'Face-to-Face' },

    ];
  }

  createRequest() {

    if (this.requestForm.invalid) {
      this._alertService.error('Please fill required feilds');
      return false;
    }
    const data = { ...this.requestForm.getRawValue(), ... { providerid: this.providerId } };
    data.isclosereopen = true;
    this._commonHttpService.create(data, 'publicproviderreferral/createproviderapplicant').subscribe(response => {
      this._alertService.success('Application initatated successfully');
      this.close();
      this.getInformation();
    });
  }

  close() {
    this.requestForm.reset();
    (<any>$('#home-approval-request-form')).modal('hide');
  }

  openRequestForm() {
    this.requestForm.reset();
    (<any>$('#home-approval-request-form')).modal('show');
    this.loadDropDowns();
    this.loadUserList();
  }
}
