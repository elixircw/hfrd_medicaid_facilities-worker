import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AlertService, AuthService, CommonHttpService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonDatePickerComponent } from '../../../../shared/modules/common-controls/common-date-picker/common-date-picker.component';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { config } from '../../../../../environments/config';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'change-request',
  templateUrl: './change-request.component.html',
  styleUrls: ['./change-request.component.scss']
})
export class ChangeRequestComponent implements OnInit {
  requestNo: string;
  changeLicenseInfoForm: FormGroup;
  updateChangeRequestForm: FormGroup;
  changedLicenseInfo=[];
  requestDetail: any;
  signimage: any;
  typeOfRequest: any;
  uploadedFile: any;
  token: AppUser;
  deleteAttachmentIndex: number;
  changeRequestNo: any;
  requestId: any;

  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService, 
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute
    ) { 
      this.requestNo = route.snapshot.params['id'];
    }

  ngOnInit() {
    this.getChangeRequestDetails();
    this.initializeEditLicenseForm();
    this.getChangedLicenseInfo();
    this.typeOfRequest = ['Personnel', 'Non-Personnel'];
    this.token = this._authService.getCurrentUser();
    

  }

  private initializeEditLicenseForm() {
    this.changeLicenseInfoForm = this.formBuilder.group({
      minimum_age_no: [''],
      maximum_age_no: [''],
      gender_cd: [''],
      children_no: ['']
    });
    this.updateChangeRequestForm = this.formBuilder.group({
      type_of_request : [''],
      reason: [''],
      expiration_date: [''],
      how_pgm_allowed_to_oprt: ['']
    });
  }
  
  getChangeRequestDetails() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : {
            request_no : this.requestNo
            },
      }, 
      'providerportalrequest?filter'
    ).subscribe(
      (response) => {
        if (Array.isArray(response) && response.length) {
          this.requestDetail = response[0];
          if (this.requestDetail) {
            this.updateChangeRequestForm.patchValue({ 
              expiration_date:  this.requestDetail.expiration_date ? this.requestDetail.expiration_date : this.requestDetail.request_expirationdate,
              reason: this.requestDetail.reason,
              type_of_request: this.requestDetail.type_of_request,
              how_pgm_allowed_to_oprt: this.requestDetail.how_pgm_allowed_to_oprt
            });
            this.signimage = this.requestDetail.signimage;
            this.requestId = this.requestDetail.request_id;
            this.getAttachment(this.requestId);

          }
        }
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }
  
  patchChangedLicenseInfo(obj){
    //console.log('change request payload', obj);
    this._commonHttpService.create(
      {
        where: {
          request_no: this.requestNo,
          minimum_age_no: obj.minimum_age_no,
          maximum_age_no: obj.maximum_age_no,
          gender_cd: obj.gender_cd,
          children_no: obj.children_no
        },
        method: 'post'
      },
      'providerportalrequest/changerequestlicenseinfo'
    ).subscribe(result => {
      this.getChangedLicenseInfo();
      this._alertService.success('License info changed successfully!');     
    },
      (error) => {
        this._alertService.error('Unable to change license info, please try again.');
        return false;
      }
    );    
    (<any>$('#change-license-request')).modal('hide');
    this.changeLicenseInfoForm.reset();
    }

    getChangedLicenseInfo(){
      this._commonHttpService.create(
        {
          where: {
            request_no: this.requestNo,           
          },
          method: 'post'
        },
        'providerportalrequest/getchangerequestlicenseinfo'
      ).subscribe(result => {
        this.changedLicenseInfo=result.data;
        this.changeLicenseInfoForm.patchValue({
          minimum_age_no:this.changedLicenseInfo[0].minimum_age_no,
          maximum_age_no:this.changedLicenseInfo[0].maximum_age_no,
          gender_cd:this.changedLicenseInfo[0].gender_cd,
          children_no:this.changedLicenseInfo[0].children_no
        });
        //console.log('changed licnese info', this.changedLicenseInfo[0]);
      },
        (error) => {
          this._alertService.error('Unable to get information, please try again.');
          return false;
        }
      );    
      (<any>$('#change-license-request')).modal('hide');
      this.changeLicenseInfoForm.reset();
      }

      loadRequest(type) {
        if (type === 'Waiver' || type === 'Variance') {
          return type;
        } else {
          return 'NA';
        }
        
      }

      updateChangeRequest(){
        const changeRequestForm = this.updateChangeRequestForm.getRawValue();
        this._commonHttpService.create(
          changeRequestForm,
          'providerportalrequest/updateproviderportalrequest'
        ).subscribe(
          (response) => {
            this._alertService.success(response);
           this.getChangeRequestDetails();
          },
          (error) => {
            this._alertService.error("Unable to save information");
          }
        );
      }

      downloadFile(s3bucketpathname) {
        const workEnv = config.workEnvironment;
        let downldSrcURL;
        if (workEnv === 'state') {
            downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            // 4200
            downldSrcURL = s3bucketpathname;
        }
        console.log('this.downloadSrc', downldSrcURL);
        window.open(downldSrcURL, '_blank');
      }

      getAttachment(request_id) {
        this._commonHttpService.getArrayList(
          {
            method: 'post',
            nolimit: true,
            where : {
                requestid : request_id
                },
          },

          'providerportalrequest/getattachment'
        ).subscribe((response) => {
          console.log('Attachment Response is ------------>', response);
                      if (response && response['data'] && response['data'].length) {
                            this.uploadedFile = response['data'][0]['attachments'];
                         }

        }, (error) => {
          this._alertService.error('Unable to Download Document');
        });

      }
      
}
