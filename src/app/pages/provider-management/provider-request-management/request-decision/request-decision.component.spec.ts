import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestDecisionComponent } from './request-decision.component';

describe('RequestDecisionComponent', () => {
  let component: RequestDecisionComponent;
  let fixture: ComponentFixture<RequestDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
