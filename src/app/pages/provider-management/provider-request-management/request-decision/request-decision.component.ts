import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplicationDecision } from '../_entities/newApplicantModel';
import { AuthService, CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { RoutingUser } from '../_entities/existingreferralModel';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { HttpHeaders } from '@angular/common/http';
import { NgxfUploaderService } from 'ngxf-uploader';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
@Component({
  selector: 'request-decision',
  templateUrl: './request-decision.component.html',
  styleUrls: ['./request-decision.component.scss']
})
export class RequestDecisionComponent implements OnInit {


  decisionFormGroup: FormGroup;
  currentDecision: ApplicationDecision;
  roleId: AppUser;
  decisions = [];


  applicantNumber: string;
  eventcode = 'PTA';


  requestDetail: any;

  // Assigning / Routing
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  selectedPerson: any;
  //isFinalDecisoin: boolean = false;
  isFinalDecisoin: boolean;

  isSupervisor: boolean;
  isGroup = false;

  //statusDropdownItems$: Observable<DropdownModel[]>;
  statusDropdownItems: any;
  signimage: any;
  isSignPresent: boolean;
  disSubmit: boolean;
  constructor(private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private _uploadService: NgxfUploaderService,
    private _dataStoreService: DataStoreService) {
    this.applicantNumber = route.snapshot.params['id'];
    //this.applicantNumber = 'A201800300025';
  }

  ngOnInit() {
    this.initializeDecisionForm();
    this.getChangeRequestDetails();
    this.roleId = this._authService.getCurrentUser();
    this.loadSignature();
    console.log(this.roleId);
    this.statusDropdownItems = [
      { "text": "Reject", "value": "Rejected" },
      { "text": "Approve", "value": "Submitted" },
      { "text": "Return", "value": "Incomplete" },
    ];
    //this.roleId.user.userprofile.teamtypekey;

    if (this.roleId.role.name === 'Executive Director' || this.roleId.role.name === 'Provider_DJS_SecretaryDesignee') {
      this.isFinalDecisoin = true;
    } else {
      this.isFinalDecisoin = false;
    }
    if (this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSSD'
    || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSRS' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSR'
    || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSPD' || this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey === 'PVRDJSQA' ) {
      this.eventcode = 'PTADJS';
    }
    this.getDecisions();
  }

  loadSignature() {
    this._commonHttpService.getSingle({ method: 'get', securityusersid: this.roleId.user.securityusersid }, 'admin/userprofile/listusersignature?filter')
      .subscribe(res => {
        this.signimage = res.usersignatureurl;
        this.checkIfSignPresent();
      });
  }

  private checkIfSignPresent() {
    this.isSignPresent = Boolean(this.signimage);
    this._dataStoreService.setData('isSignPresent', this.isSignPresent);
  }

  initializeDecisionForm() {
    this.decisionFormGroup = this.formBuilder.group({
      status: [null, Validators.required],
      reason: [null, Validators.required]
    });
  }


  getDecisions() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          eventcode: 'LIC',
          objectid: this.applicantNumber
        }
      },
      'providerapplicant/getapplicationdecisions'
    ).subscribe(decisionsarray => {

      this.decisions = decisionsarray.data;
    },
      (error) => {
        this._alertService.error('Unable to get decisions, please try again.');
        console.log('get decisions Error', error);
        return false;
      }
    );
  }

  submitDecision() {
    this.disSubmit = true;
    

    console.log("Submitting decision");
    const decision = this.decisionFormGroup.getRawValue();

    if (this.decisionFormGroup.invalid) {
      this._alertService.error('Please fill required feilds');
      this.disSubmit = false;
      return;
    }

    if (this.isFinalDecisoin && decision.status === 'Submitted') {
      decision.status = 'Approved';
    }


    const fromRole = this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey;
    let toroleid = fromRole;
    if(fromRole === "OLMED" &&  decision.status === 'Approved' && this.requestDetail && this.requestDetail.request_type === 'License Extend') {
      toroleid = 'PPROSA';
    } else  if(fromRole === "PVRDJSSD" &&  decision.status === 'Approved' && this.requestDetail && this.requestDetail.request_type === 'License Extend') {
      toroleid = 'PPROSA';
    }
    this._commonHttpService.create(
      {
        where: {
          eventcode: 'LIC',
          objectid: this.applicantNumber,
          tosecurityusersid: this.roleId.user.securityusersid,
          fromroleid: fromRole,
          toroleid: fromRole,
          remarks: decision.reason, //.get('reason'),
          status: decision.status, //.get('status')
          isfinalapproval: this.isFinalDecisoin
        },
        method: 'post'
      },
      'providerportalrequest/providerrouting'
    ).subscribe(result => {
      this.disSubmit = false;
      this._alertService.success('Decision Submitted successfully!');
      this.decisions.length = 0;
      this.getDecisions();
    },
      (error) => {
        this.disSubmit = false;
        this._alertService.error('Unable to submit decision, please try again.');
        console.log('submit decision Error', error);
        return false;
      }
    );
  }

  // Assignment logic follows

  getChangeRequestDetails() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : {
            request_no : this.applicantNumber
            },
      }, 
      'providerportalrequest?filter'
    ).subscribe(
      (response) => {
        if (Array.isArray(response) && response.length) {
          this.requestDetail = response[0];
        }
        
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }

  listUser(assigned: string) {
    const ele = document.getElementsByName('selectedPerson');
    for (let i = 0; i < ele.length; i++) {
      const element = ele[i] as HTMLInputElement;
      element.checked = false;
     }
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
  }

  getRoutingUser() {
    //this.getServicereqid = modal.provider_referral_id;
    //  this.getServicereqid = "R201800200374";
    // console.log(modal.provider_referral_id);
    // this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: this.eventcode },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  selectPerson(row) {
    this.selectedPerson = row;
    //console.log(this.selectedPerson);
  }

  assignUser() {
    // Doing the update of Status and assigning at the same time
    // In the same flow, first 'Submit' then 'Assign' will do them both
    // TODO: SIMAR - Put validations that status value has been selected before submitting
    console.log(this.isFinalDecisoin);
    const decision = this.decisionFormGroup.getRawValue();

    if ( decision.status === 'Rejected' && this.roleId.role.name === 'Quality Assurance' ) {
      decision.status = 'Rejectedqa';
    }
    if (this.selectedPerson) {
      const fromRole = this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey;
      this._commonHttpService
        .getPagedArrayList(
          new PaginationRequest({
            where: {
              eventcode: 'LIC',
              objectid: this.applicantNumber,
              fromroleid: fromRole,
              tosecurityusersid: this.selectedPerson.userid,
              toroleid:  this.selectedPerson.agencykey + this.selectedPerson.rolecode,
              remarks: decision.reason, //.get('reason'),
              status: decision.status, //.get('status')
              isfinalapproval: this.isFinalDecisoin
              // isreviewrequest: ,
            },
            method: 'post'
          }),
          'providerportalrequest/providerrouting'
          //'Providerreferral/routereferralda'
        )
        .subscribe(result => {
          console.log("Inside subscribe", result);
          this._alertService.success('Application assigned successfully!');
          //  this.getAssignCase(1, false);
          this.closePopup();


          //Reset the decision table and get all new results
          this.decisions.length = 0;
          this.getDecisions();
        });
    } else {
      this._alertService.warn('Please select a person');
    }
  }

  closePopup() {
    (<any>$('#intake-caseassign')).modal('hide');
    (<any>$('#assign-preintake')).modal('hide');
    (<any>$('#reopen-intake')).modal('hide');
  }

  uploadAttachment() {
    console.log(this.signimage);
    const blob = this.dataURItoBlob(this.signimage);
    const finalImage = new File([blob], 'image.png');
    this._uploadService
      .upload({
        url:
          AppConfig.baseUrl +
          '/' +
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment
            .UploadAttachmentUrl +
          '?access_token=' +
          this.roleId.id +
          '&srno=userprofile',
        headers: new HttpHeaders()
          .set('access_token', this.roleId.id)
          .set('srno', 'userprofile')
          .set('ctype', 'file'),
        filesKey: ['file'],
        files: finalImage,
        process: true
      })
      .subscribe(
        response => {
          if (response && response.data && response.data.s3bucketpathname) {
            this._commonHttpService.create({
              usersignatureurl: response.data.s3bucketpathname
            }, 'admin/userprofile/updateusersignature').subscribe(res => {
              if (res.count) {
                this._alertService.success('Signature added successfully.');
                this.loadSignature();
              }
            });
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  dataURItoBlob(dataURI) {
    const binary = atob(dataURI.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/png'
    });
  }

  submitAndAssign() {

    if (this.decisionFormGroup.invalid) {
      this._alertService.error('Please fill required fields');
      return false;
    }
    (<any>$('#intake-caseassign')).modal('show');
    this.getRoutingUser();
  }

  

}
