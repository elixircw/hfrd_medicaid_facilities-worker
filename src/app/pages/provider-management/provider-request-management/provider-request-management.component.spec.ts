import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderRequestManagementComponent } from './provider-request-management.component';

describe('ProviderRequestManagementComponent', () => {
  let component: ProviderRequestManagementComponent;
  let fixture: ComponentFixture<ProviderRequestManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderRequestManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderRequestManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
