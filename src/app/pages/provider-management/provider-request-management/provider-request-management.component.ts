import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { AlertService } from '../../../@core/services/alert.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AttachmentIntakes, GeneratedDocuments, applinfo, ProviderReferral } from './_entities/newApplicantModel';

@Component({
  selector: 'provider-request-management',
  templateUrl: './provider-request-management.component.html',
  styleUrls: ['./provider-request-management.component.scss']
})
export class ProviderRequestManagementComponent implements OnInit {

  requestID: string;
  requestDetail: any;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) { 
    this.requestID = route.snapshot.params['id'];
  }

  ngOnInit() {
   this.getChangeRequestDetails();
  }

  getChangeRequestDetails() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : {
            request_no : this.requestID
            },
      }, 
      'providerportalrequest?filter'
    ).subscribe(
      (response) => {
        if (Array.isArray(response) && response.length) {
          this.requestDetail = response[0];
        }
        
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }

  openProvider(providerid) {
    this._router.navigateByUrl('/pages/provider-management/new-provider/' + providerid);
  }

  openSite(siteid) {
    this._router.navigateByUrl('/pages/provider-management/license-management/' + siteid);
  }

}
