import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderRequestManagementComponent } from './provider-request-management.component';

const routes: Routes = [

  {
    path: '',
    component: ProviderRequestManagementComponent,
    // canActivate: [RoleGuard],
    children: [
        // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
        // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
        // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
        // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
    ]
},
{
    path: ':id',
    component: ProviderRequestManagementComponent,
    children: [
        // { path: 'audio-record/:intakeNumber', component: AudioRecordComponent },
        // { path: 'video-record/:intakeNumber', component: VideoRecordComponent },
        // { path: 'image-record/:intakeNumber', component: ImageRecordComponent },
        // { path: 'attachment-upload/:intakeNumber', component: AttachmentUploadComponent }
    ]
    // canActivate: [RoleGuard]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRequestManagementRoutingModule { }
