import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService, AlertService, CommonHttpService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'request-additional-info',
  templateUrl: './request-additional-info.component.html',
  styleUrls: ['./request-additional-info.component.scss']
})
export class RequestAdditionalInfoComponent implements OnInit {
tableData=[];
additionalInfoForm: FormGroup;
requestNumber: string;

constructor(private formBuilder: FormBuilder,
  private _alertService: AlertService,
  private _authService: AuthService, 
  private _commonHttpService: CommonHttpService,
  private _router: Router,
  private route: ActivatedRoute
  //private comp: ApplicantDocumentCreatorComponent
  ) { 
    this.requestNumber = route.snapshot.params['id'];
  }
  ngOnInit() {
    this.initilizeAdditionalInfoFormGroup();
    this.getChangeRequestNarrative();
  }

  private initilizeAdditionalInfoFormGroup(){
    this.additionalInfoForm = this.formBuilder.group({
      request_comments: [''],     
    });
  }
  sendCommentsToProvider(){
    var payload={};
    payload=this.additionalInfoForm.value;
    payload['request_narrative_by']="QA worker";
    payload['request_no']=this.requestNumber;
    this._commonHttpService.create(
      payload,
      'providerportalrequestnarrative'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        (<any>$('#change-request')).modal('hide');
        this.getChangeRequestNarrative();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    this.additionalInfoForm.patchValue({    
      request_comments:['']
    });
  }

  getChangeRequestNarrative(){
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : {request_no : this.requestNumber
              },
        order : 'create_ts asc'
      }, 
      'providerportalrequestnarrative?filter'
    ).subscribe(
      (response) => {
        this.tableData=response;
        //console.log("Rahul get request list",JSON.stringify(this.tableData));
        // this.staffForm.patchValue(response);
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }
  

}
