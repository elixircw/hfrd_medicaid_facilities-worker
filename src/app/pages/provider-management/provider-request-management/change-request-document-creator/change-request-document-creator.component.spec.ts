import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeRequestDocumentCreatorComponent } from './change-request-document-creator.component';

describe('ChangeRequestDocumentCreatorComponent', () => {
  let component: ChangeRequestDocumentCreatorComponent;
  let fixture: ComponentFixture<ChangeRequestDocumentCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeRequestDocumentCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRequestDocumentCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
