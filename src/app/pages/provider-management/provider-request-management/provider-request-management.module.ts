import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatSlideToggleModule
} from '@angular/material';

import { ProviderRequestManagementRoutingModule } from './provider-request-management-routing.module';
import { ProviderRequestManagementComponent } from './provider-request-management.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChangeRequestComponent } from './change-request/change-request.component';
import { RequestDecisionComponent } from './request-decision/request-decision.component';
import { RequestAdditionalInfoComponent } from './request-additional-info/request-additional-info.component';
import { SignatureFieldComponent } from './request-decision/signature-field/signature-field.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { AttachmentDetailComponent } from './add-document/attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './add-document/attachment-upload/attachment-upload.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { AudioRecordComponent } from './add-document/audio-record/audio-record.component';
import { EditAttachmentComponent } from './add-document/edit-attachment/edit-attachment.component';
import { ImageRecordComponent } from './add-document/image-record/image-record.component';
import { VideoRecordComponent } from './add-document/video-record/video-record.component';
import { AttachmentNarrativeComponent } from './add-document/attachment-narrative/attachment-narrative.component';
import { ChangeRequestDocumentCreatorComponent } from './change-request-document-creator/change-request-document-creator.component';
import { RequestSignatureFieldComponent } from '../../provider-portal-temp/current-private-provider/provider-change-request/signature-field/signature-field.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
@NgModule({
  imports: [
    CommonModule,
    ProviderRequestManagementRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgxPaginationModule,
    SignaturePadModule,
    MatSlideToggleModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot()
    

  ],
  declarations: [ProviderRequestManagementComponent,
     ChangeRequestComponent,
     RequestDecisionComponent,
     RequestAdditionalInfoComponent,
    //  SignatureFieldComponent,
     AttachmentDetailComponent,
     AttachmentUploadComponent,
     AddDocumentComponent, 
     AudioRecordComponent,
     EditAttachmentComponent, 
     ImageRecordComponent,
     VideoRecordComponent,
     AttachmentNarrativeComponent,
     ChangeRequestDocumentCreatorComponent,
     RequestSignatureFieldComponent
      ],
      providers: [NgxfUploaderService]

})
export class ProviderRequestManagementModule { }
