import { Component, OnInit, ViewContainerRef, ViewChild, Input } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AlertService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { ContractManagementSerivce } from '../contract-management.service';
@Component({
  selector: 'contract-license-services',
  templateUrl: './contract-license-services.component.html',
  styleUrls: ['./contract-license-services.component.scss']
})
export class ContractLicenseServicesComponent implements OnInit {
  servicePaid = [];
  serviceClassification = [];
  serviceUnit = [];
  serviceStatus = [];
  activityDropdownItems$: DropdownModel[];
  activityDropdownItems1$: Observable<DropdownModel[]>;
  servicesObj: any = {};
  availableTasks: any[] = [];
  tableData = [];
  currentData = [];
  FormMode: string;




  minDate = new Date();
  programId: string;
  contractForm: FormGroup;
  constructor(private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _service: ContractManagementSerivce) {
    this.programId = route.snapshot.parent.params['id'];
  }

  ngOnInit() {
    this.initServiceForm();
    this.loadDropDown();
    this.getProviderservices();
    this.servicePaid = ['DJS', 'SSA', 'LDSS', 'MSDE', 'Medicaid', 'DJS-SSA'];
    this.serviceClassification = ['Educational', 'Residential'];
    this.serviceUnit = ['Annually', 'Daily', 'Hourly'];
    this.serviceStatus = ['Active', 'Expired'];

  }

  private initServiceForm() {
    this.FormMode = 'Add';
    this.contractForm = this.formBuilder.group({
      provider_service_id: 0,
      provider_id: this._service.lisenseInfo ? this._service.lisenseInfo.provider_id : null,
      program_id: this.programId,
      service_category: [''],
      // service_tasks:[''],
      service_id: [''],
      start_dt: new Date(),
      end_dt: null,
      service_description: [''],
      service_paid_by: [''],
      service_classification: [''],
      service_unit: [''],
      service_status: [''],
      service_cost: null
    });
  }
  private loadDropDown() {
    this._commonHttpService.getArrayList(
      {
      },
      'providerservice/listservicecategories'
    ).subscribe(taskList => {
      console.log('@@@@@' + taskList);
      this.activityDropdownItems$ = taskList;
    });
  }


  private getProviderservices() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          provider_id: this._service.lisenseInfo ? this._service.lisenseInfo.provider_id : null,
          program_id: this.programId
        }
      },
      'providercontract/getprovidercontractlicenseservices')
      .subscribe(licenseservices => {
        this.tableData = licenseservices;
        // this.tableData.unshift(this.currentData);
        console.log('Final table data', this.tableData);

      });
  }




  onChangeActivity(param: any, text: any) {

    this.availableTasks = [];

    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          service_category_cd: param
        }
      },
      'providerservice/getserviceslist'
    ).subscribe(res => {
      this.servicesObj = res;
      //console.log(this.servicesObj.data);      
      this.servicesObj.data.forEach(element => {
        var currentData = {};
        currentData['service_id'] = element.service_id;
        currentData['service_nm'] = element.service_nm;
        this.availableTasks.push(currentData);
        console.log('$$$$ ' + this.availableTasks);

      });
    });


  }
  private editServices(obj) {
    this.FormMode = 'Edit';
    this.contractForm.patchValue({
      provider_service_id: obj.provider_service_id,
      provider_id: this._service.lisenseInfo ? this._service.lisenseInfo.provider_id : null,
      program_id: this.programId,
      service_id: obj.service_id,
      start_dt: obj.start_dt,
      end_dt: obj.end_dt,

    });

    (<any>$('#contract-license')).modal('show');
  }


  addServices() {
    this.FormMode = 'Add';
  }

  createNewService(obj) {
    console.log('@@@ ' + obj);
    obj.provider_id = this._service.lisenseInfo ? this._service.lisenseInfo.provider_id : null;
    this._commonHttpService.create(obj, 'providercontract/addprovidercontractlicenseservices').subscribe(
      (response) => {

        this.getProviderservices();
        this.closewindow();

      },
      (error) => {
        this._alertService.error('Unable to save services');
      }
    );

    
  }

  closewindow() {
    this.contractForm.reset();
    (<any>$('#contract-license')).modal('hide');
  }
}