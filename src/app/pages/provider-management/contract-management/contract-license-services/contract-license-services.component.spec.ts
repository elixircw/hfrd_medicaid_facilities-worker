import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractLicenseServicesComponent } from './contract-license-services.component';

describe('ContractLicenseServicesComponent', () => {
  let component: ContractLicenseServicesComponent;
  let fixture: ComponentFixture<ContractLicenseServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractLicenseServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractLicenseServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
