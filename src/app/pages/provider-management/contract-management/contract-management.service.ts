import { Injectable } from '@angular/core';

@Injectable()
export class ContractManagementSerivce {
    lisenseInfo : any;
    contractLicenseTypeId : any;
    setLisenseInfo(lisenseInfo) {
        this.lisenseInfo = lisenseInfo;
    }

    setcontractLicenseTypeId(id) {
        this.contractLicenseTypeId = id;
    }

    getcontractLicenseTypeId() {
       return  this.contractLicenseTypeId;
    }

    getLisenseInfo() {
        return this.lisenseInfo;
    }

}