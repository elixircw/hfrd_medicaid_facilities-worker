import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSpecificAgreementComponent } from './child-specific-agreement.component';

describe('ChildSpecificAgreementComponent', () => {
  let component: ChildSpecificAgreementComponent;
  let fixture: ComponentFixture<ChildSpecificAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSpecificAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSpecificAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
