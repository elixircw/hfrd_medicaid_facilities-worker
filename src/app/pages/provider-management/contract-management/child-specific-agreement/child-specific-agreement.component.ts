import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, ValidatorFn, ValidationErrors, Validators } from '@angular/forms';
import { AlertService, ValidationService, SessionStorageService, GenericService, CommonHttpService, CommonDropdownsService, AuthService } from '../../../../@core/services';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { People, InvolvedPerson, PersonSearch, PersonDsdsAction, InvolvedPersonSearchResponse } from '../../../../@core/common/models/involvedperson.data.model';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { ContractManagementSerivce } from '../contract-management.service';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { HttpHeaders } from '@angular/common/http';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { AppUser } from '../../../../@core/entities/authDataModel';
@Component({
  selector: 'child-specific-agreement',
  templateUrl: './child-specific-agreement.component.html',
  styleUrls: ['./child-specific-agreement.component.scss']
})
export class ChildSpecificAgreementComponent implements OnInit {
  csaAgreementForm: FormGroup;
  agreementList: any;
  involvedPersonSearchForm: FormGroup;
  isSearch: any;
  personSearchResult: any;
  showPersonDetail: any;
  selectedPersons: any;
  isViewCSAForm: any;
  totalRecords: any;
  personSearch: People;
  personSearchForm: InvolvedPerson;
  paginationInfo: PaginationInfo = new PaginationInfo();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  personSearchResult$: Observable<PersonSearch[]>;
  personDSDSActions$: Observable<PersonDsdsAction[]>;
  private involvedPersonSearch: InvolvedPerson;
  genderDropdownItems$: Observable<any[]>;
  selectedChild: any;
  actionType: any;
  csaData: any;
  isViewOnly: any;
  isUpdate: any;
  licenseInfo: any;
  lincenseTypeInfo: any;
  selectedData: any;
  uploadedFile: any = [];
  deleteAttachmentIndex: number;
  token: AppUser;
  contractLicenseTypeId : any;
  ratesForm: FormGroup;
  FormMode:string;
  perDiemRatesMap: any = {};
  programId: string;
  awardedBedsNum: any;
  licenserates: any;
  currentLicenseType: any;
  providerId: any;
  licensetyperates: any;
  selectedPersonIndex: any;
  dayMonthList: any = [];
  daysList: any = [];
  monthsList: any = [];
  isFileUploaded: any;
  constructor(
    private _formBuilder: FormBuilder,
    private alertService: AlertService,
    private _session: SessionStorageService,
    private _involvedPersonSeachService: GenericService<InvolvedPersonSearchResponse>,
    private _commonHttpService: CommonHttpService,
    private dropdownService: CommonDropdownsService,
    private route: ActivatedRoute,
    private _serivce: ContractManagementSerivce,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService,

  ) {
    this.programId = route.snapshot.parent.params['id'];
   }


  ngOnInit() {
    this.licenseInfo =this._serivce.getLisenseInfo() ;
    this.token = this._authService.getCurrentUser();
    // console.log('LINfo',this.licenseInfo);
    this.selectedPersons = [];
    this.csaData = [];
    this.actionType = 'search';
    this.agreementList = ['Co-committed','Non-TFC-Siblings','Minor Parent with children','Non-TFC Exception'];
    this.daysList = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
    this.monthsList = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    this.monthsList.forEach(month => {
      this.daysList.forEach(day => {
        const dayMonth = month + '/' + day;
        this.dayMonthList.push(dayMonth);
      });
    });
    this.csaAgreementForm = this._formBuilder.group({ 
      agreementtype : [null],
      csafiscalyear_enddt : [null],
      // person : [null],
      childspecificagreementid: [null],
      isdeletable: [null],
      csanarrative : [null],
      csaattachments: [null],
      csastatus: [null],
      person:[null]
    });
    this.csaAgreementForm.patchValue({ csafiscalyear_enddt :'06/30'});
    this.ratesForm = this._formBuilder.group({
      program_rate_id:0,
      provider_id: this.programId,
      license_type:[''],
      annual_rate: [''],
      monthly_rate: [''],
      per_diem_rate: [''],
      start_dt:new Date(),
      end_dt:new Date(),
      rate_code :[''],
      rate_status:[''],
    });
    this.initSearchForm();
    if(this.licenseInfo) {
    this.getCSAAgreementDetails(); }
    else {
      this.getLicenseTypeInformation();
    }
    this.genderDropdownItems$ = this.dropdownService.getGenders();
    this.getLicenseTypeInformation();
    this.getProviderrate();
    this.initRatesForm();
    
  }

  getLicenseTypeInformation() {
    this.contractLicenseTypeId = this._serivce.getcontractLicenseTypeId();
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          contractLicenseTypeNo: this.contractLicenseTypeId,
        }
      },
      'providerlicense/getlicenseinformation')
      .subscribe(licenseservices => {
        console.log('License type information', licenseservices);
        if (licenseservices) {
           this.lincenseTypeInfo = licenseservices;
          if (this.lincenseTypeInfo.data && this.lincenseTypeInfo.data.length) {
            this.licenseInfo = this.lincenseTypeInfo.data[0];
            this._serivce.setLisenseInfo(this.licenseInfo);
            this.getCSAAgreementDetails();
           
            this.lincenseTypeInfo = this.lincenseTypeInfo.data[0];          
            this.currentLicenseType = this.lincenseTypeInfo.license_type;
            this.providerId = this.lincenseTypeInfo.provider_id;
          }          

          this.getAvailableRateCodes();

        }
        //this._dataStoreService.setData('currentProviderId', this.providerId);    
      });
  }

  initSearchForm() {
    this.involvedPersonSearchForm = this._formBuilder.group({
      lastname: [''],
      firstname: [''],
      maidenname: [''],
      gender: [''],
      dob: [''],
      dateofdeath: [''],
      ssn: ['', [this.validateSSN]],
      mediasrc: [''],
      mediasrctxt: [''],
      occupation: [''],
      dl: [''],
      stateid: [''],
      address1: [''],
      address2: [''],
      zip: [''],
      city: [''],
      county: [''],
      selectedPerson: [''],
      cjisnumber: [''],
      complaintnumber: [''],
      fein: [''],
      age: [''],
      email: ['', [ValidationService.mailFormat]],
      phone: [''],
      petitionid: [''],
      alias: [''],
      oldId: [''],
      chessieid: [''],
      middlename: [''],
      cjamspid: ['']
    }, { validator: this.atLeastOne(Validators.required) });
  }

  openCSAForm() {
    this.resetCSAForm();
    this.csaAgreementForm.patchValue({ csafiscalyear_enddt :'06/30'});
    this.isViewCSAForm = true;
    this.csaAgreementForm.enable();
  }

  closeCSAForm() {
   this.resetCSAForm();
  }

  validateSSN(c: FormControl) {
    const invalidssn = ['000000000'];
    return invalidssn.includes(c.value) ? {
      validateSSN: {
        valid: false
      }
    } : null;
  }

  atLeastOne = (validator: ValidatorFn) => (
    group: FormGroup,
  ): ValidationErrors | null => {
    const hasAtLeastOne = group && group.controls && Object.keys(group.controls)
      .some(k => !validator(group.controls[k]));
    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
  }

  resetCSAForm() {
    this.isViewCSAForm = false;
    this.csaAgreementForm.reset();
    this.csaAgreementForm.enable;
    this.isViewOnly = false;
    this.isUpdate = false;
    this.selectedChild = [];
    this.selectedData = null;
    this.uploadedFile = [];
    this.isFileUploaded = false;
  }
  
  saveCSAAgreement(status){
    const csaAgreementForm = this.csaAgreementForm.getRawValue();
    
    const personid = [];
    if(this.selectedChild && this.selectedChild.length) {
      this.selectedChild.forEach(child => {
        const personObj = { personid : child.personid};
        personid.push(personObj);
      });
    }
    csaAgreementForm.person = personid;
    this.licenseInfo =this._serivce.getLisenseInfo() ;
    csaAgreementForm.csaattachments = this.uploadedFile;
    csaAgreementForm.csastatus = status;
    csaAgreementForm.contract_id = this.licenseInfo && this.licenseInfo.contract_id ? this.licenseInfo.contract_id : null,
    csaAgreementForm.provider_id = this.licenseInfo && this.licenseInfo.provider_id ? this.licenseInfo.provider_id : null
    csaAgreementForm.site_id = this.licenseInfo && this.licenseInfo.site_id ? this.licenseInfo.site_id : null
    // this.csaData.push(csaAgreementForm);
    // this.resetCSAForm();
    this._commonHttpService.create(
      csaAgreementForm,
      'providercontract/childspecificagreement'
    ).subscribe(
      (response) => {
        const msg = 'Child specific agreement ' + ( csaAgreementForm.childspecificagreementid  ? 'updated ' : 'saved ' )  + '  successfully';
        this.alertService.success(msg);
        this.resetCSAForm();
       this.getCSAAgreementDetails();
      },
      (error) => {
        this.alertService.error("Unable to save information");
      }
    );
  }

  pageChanged(event: any) {
    this.paginationInfo.pageNumber = event.page;
    this.getPage(this.paginationInfo.pageNumber, this.personSearchForm);
  }

  viewCSA(data) {
    this.isViewCSAForm = true;
    this.isViewOnly = true;
    this.csaAgreementForm.patchValue(data);
    this.uploadedFile  = data.csaattachments;
    // this.csaAgreementForm.patchValue({ csa_fiscal_year_end: (moment(new Date(data.csa_fiscal_year_end)))});
    this.csaAgreementForm.disable();
    this.selectedChild = data.person;
  }

  editCSA(data) {
    this.isFileUploaded = false;
    this.isViewCSAForm = true;
    this.isUpdate = true;
    this.isViewOnly = false;
    this.csaAgreementForm.patchValue(data);
    this.uploadedFile = data.csaattachments;
    // this.csaAgreementForm.patchValue({ csa_fiscal_year_end: (moment(new Date(data.csa_fiscal_year_end)))});
    this.csaAgreementForm.enable();
    this.selectedChild = data.person;

  }

  deleteChild(index) {
    this.selectedChild.splice(index,1);
  }

  uploadFile(file: File | FileError): void {
    if (!(file instanceof Array)) {
        return;
    }
    file.map((item, index) => {
        const fileExt = item.name
            .toLowerCase()
            .split('.')
            .pop();
        if (
            fileExt === 'mp3' ||
            fileExt === 'ogg' ||
            fileExt === 'wav' ||
            fileExt === 'acc' ||
            fileExt === 'flac' ||
            fileExt === 'aiff' ||
            fileExt === 'mp4' ||
            fileExt === 'mov' ||
            fileExt === 'avi' ||
            fileExt === '3gp' ||
            fileExt === 'wmv' ||
            fileExt === 'mpeg-4' ||
            fileExt === 'pdf' ||
            fileExt === 'txt' ||
            fileExt === 'docx' ||
            fileExt === 'doc' ||
            fileExt === 'xls' ||
            fileExt === 'xlsx' ||
            fileExt === 'jpeg' ||
            fileExt === 'jpg' ||
            fileExt === 'png' ||
            fileExt === 'ppt' ||
            fileExt === 'pptx' ||
            fileExt === 'gif'
        ) {
            this.uploadedFile.push(item);
            const uindex = this.uploadedFile.length - 1;
            if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
                this.uploadedFile[uindex].percentage = 1;
            }

            this.uploadAttachment(uindex);
            const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
            const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
            if (audio_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Audio';
            } else if (video_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Video';
            } else {
                this.uploadedFile[uindex].attachmenttypekey = 'Document';
            }
        } else {
            // tslint:disable-next-line:quotemark
            this.alertService.error(fileExt + " format can't be uploaded");
            return;
        }
    });
}
uploadAttachment(index) {
    console.log('check');
    const workEnv = config.workEnvironment;
    let uploadUrl = '';
    if (workEnv === 'state') {
        uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
            + '?access_token=' + this.token.id + '&' + 'srno=' + this.contractLicenseTypeId + '&' + 'docsInfo='; // Need to discuss about the docsInfo
        console.log('state', uploadUrl);
    } else {
        uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
            '&' + 'srno=' + this.contractLicenseTypeId;
        console.log('local', uploadUrl);
    }

    this._uploadService
        .upload({
            url: uploadUrl,
            headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
            filesKey: ['file'],
            files: this.uploadedFile[index],
            process: true,
        })
        .subscribe(
            (response) => {
                if (response.status) {
                    this.uploadedFile[index].percentage = response.percent;
                }
                if (response.status === 1 && response.data) {
                    const doucumentInfo = response.data;
                    doucumentInfo.documentdate = doucumentInfo.date;
                    doucumentInfo.title = doucumentInfo.originalfilename;
                    doucumentInfo.objecttypekey = 'Court';
                    doucumentInfo.rootobjecttypekey = 'Court';
                    doucumentInfo.activeflag = 1;
                    doucumentInfo.servicerequestid = null;
                    this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                    console.log(index, this.uploadedFile[index]);
                    this.alertService.success('File Upload successful.');
                    this.isFileUploaded = true;
                } 
            }, (err) => {
                console.log(err);
                //this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                this.alertService.error('Upload failed due to Server error, please try again later.');
                this.uploadedFile.splice(index, 1);
               
            }
        );
}
deleteAttachment() {
    this.uploadedFile.splice(this.deleteAttachmentIndex, 1);
    this.alertService.success('File deleted successfully.');
    this.isFileUploaded = true;
    (<any>$('#delete-attachment-popup')).modal('hide');
}

downloadFile(s3bucketpathname) {
    const workEnv = config.workEnvironment;
    let downldSrcURL;
    if (workEnv === 'state') {
        downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
    } else {
        // 4200
        downldSrcURL = s3bucketpathname;
    }
    console.log('this.downloadSrc', downldSrcURL);
    window.open(downldSrcURL, '_blank');
}

confirmDeleteAttachment(index: number) {
    (<any>$('#delete-attachment-popup')).modal('show');
    this.deleteAttachmentIndex = index;
}


  deleteCSA(data) {
     this.selectedData = data;
     this.selectedData.isdeletable = true;
     (<any>$('#delete-csa')).modal('show');
  }

  confirmDelete() {
    (<any>$('#delete-csa')).modal('hide');
    const csaAgreementForm = this.selectedData;
    this.licenseInfo =this._serivce.getLisenseInfo() ;
    csaAgreementForm.contract_id = this.licenseInfo && this.licenseInfo.contract_id ? this.licenseInfo.contract_id : null,
    csaAgreementForm.provider_id = this.licenseInfo && this.licenseInfo.provider_id ? this.licenseInfo.provider_id : null
    csaAgreementForm.site_id = this.licenseInfo && this.licenseInfo.site_id ? this.licenseInfo.site_id : null
    // this.csaData.push(csaAgreementForm);
    // this.resetCSAForm();
    this.resetCSAForm();
    this._commonHttpService.create(
      csaAgreementForm,
      'providercontract/childspecificagreement'
    ).subscribe(
      (response) => {
        this.alertService.success("Child specific agreement deleted successfully!");
        this.resetCSAForm();
       this.getCSAAgreementDetails();
      },
      (error) => {
        this.alertService.error("Unable to delete information");
      }
    );
  }

  getCSAAgreementDetails() {
    this.licenseInfo =this._serivce.getLisenseInfo() ;
    return this._commonHttpService.getArrayList({
      where: { contractid : this.licenseInfo && this.licenseInfo.contract_id ? this.licenseInfo.contract_id : null},method: 'get'
     },'providercontract/childspecificagreementlist?filter').subscribe(response => {
      this.csaData = response;
  });
  }

  onSortedPerson($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.searchInvolvedPersons(this.personSearch, 'exist');
  }

  private getPage(pageNumber: number, involvedPersonSearch) {
    this.actionType = 'select';
    // const dobdate = this._commonDropDownService.getValidDate(this.personSearchForm.dob);
    // this.personSearchForm.dob = dobdate.toString();
    this.personSearchForm = involvedPersonSearch;
    this.personSearchForm.sortcolumn = this.paginationInfo.sortColumn;
    this.personSearchForm.sortorder = this.paginationInfo.sortBy;
    ObjectUtils.removeEmptyProperties(this.personSearchForm);
    const source = this._involvedPersonSeachService
      .getPagedArrayList(
        {
          limit: this.paginationInfo.pageSize,
          order: this.paginationInfo.sortBy,
          page: pageNumber,
          count: this.paginationInfo.total,
          where: this.personSearchForm,
          method: 'post'
        },
        'globalpersonsearches/getPersonSearchData'
      )
      .map((result) => {
        return {
          data: result.data,
          count: result.count,
          canDisplayPager: result.count > this.paginationInfo.pageSize
        };
      }).subscribe(response => {
        this.personSearchResult = response.data;
        if (pageNumber === 1) {
          this.totalRecords = response.count;
        }
      });
    /* this.personSearchResult$ = source.pluck('data');
    if (pageNumber === 1) {
      this.totalRecords$ = source.pluck('count');
      this.canDisplayPager$ = source.pluck('canDisplayPager');
    } */
  }

  searchInvolvedPersons(model: People, mode) {
    this.showPersonDetail = -1;
    if (mode === 'new') {
      this.paginationInfo.sortBy = 'asc';
      this.paginationInfo.sortColumn = null;
    }
    this.personSearch = model;
    this.involvedPersonSearch = Object.assign(new InvolvedPerson(), model);
    if (this.involvedPersonSearchForm.value.address1) {
      this.involvedPersonSearch.address = this.involvedPersonSearchForm.value.address1 + '' + this.involvedPersonSearchForm.value.address2;
    }
    // this.involvedPersonSearch.stateid = this.involvedPersonSearchForm.value.dl;
    this.isSearch = true;
    this.paginationInfo.pageNumber = 1;
    this.getPage(1, this.involvedPersonSearch);
  }

  clearPersonSearch() {
    this.involvedPersonSearchForm.reset();
    this.isSearch = false;
    this.personSearchResult = [];
    this.showPersonDetail = -1;
    this._session.removeItem('personSearchParams');
  }

  clearFilter() {
    this.searchInvolvedPersons(this.personSearch, 'new');
  }

  selectPerson(event,person) {
    // const personObj = {
    //   'personname' : person.firstname + ' ' + person.lastname ,
    //   'cjamspid': person.cjamspid ,
    //   'gender': person.gendertypekey ,
    // }
    if(this.selectedPersons && this.selectedPersons.length) {
    } else {
      this.selectedPersons = [];
    }
    if(event && event.checked === true) {
      this.selectedPersons.push(person);
    } else {
      const index = this.selectedPersons &&   this.selectedPersons.length ? this.selectedPersons.findIndex(child => child.cjamspid === person.cjamspid) : -1;
      if(index > -1) {
        this.selectedPersons.splice(index,1);
      }
    }
    console.log(this.selectedPersons);
  }

  isPersonExists(cjamsPid) {
    const index = this.selectedChild &&   this.selectedChild.length ? this.selectedChild.findIndex(child => child.cjamspid === cjamsPid) : -1;
      if(index > -1) {
       return true;
      } else {
        return false;
      }
  }
  
  selectChild()
   {
    this.actionType = 'search';
    this.clearPersonSearch();
    this.selectedChild = this.selectedChild.concat(this.selectedPersons);
    this.selectedPersons = [];
    (<any>$('#search-person')).modal('hide');
   }

   backtoSearch() {
    this.actionType = 'search';
   }

   private initRatesForm() {
    this.FormMode="Add";
    this.ratesForm = this._formBuilder.group({
      program_rate_id:0,
      provider_id: this.programId,
      license_type:[''],
      annual_rate: [''],
      monthly_rate: [''],
      per_diem_rate: [''],
      start_dt:new Date(),
      end_dt:new Date(),
      rate_code :[''],
      rate_status:[''],
    });
  }

  private getProviderrate() {
      this._commonHttpService.getArrayList(
        {
            method: 'post',
            nolimit: true,
            filter: {},
            where:
            {
              provider_id: this.programId
            }
        },
        'providercontract/getprovidercontractrates')
        .subscribe(licenserate => 
          {
            this.licenserates = licenserate;
            // this.licenserates.unshift(this.licenserates);
          });      
    }
    



    private getAvailableRateCodes() {
      console.log("GETTING RATES CODES")
      this._commonHttpService.getArrayList(
        {
            method: 'post',
            nolimit: true,
            filter: {},
            where:
            {
              provider_id: this.providerId,
              license_type: this.currentLicenseType
            }
        },
        'providercontract/getproviderlicensetyperates')
        .subscribe(licensetyperate => 
          {
            this.licensetyperates = licensetyperate;

            this.licensetyperates.data.forEach((element) => {
              this.perDiemRatesMap[element.rate_code] = element.per_diem_rate;
            })
          }); 
    }
    
    //  private changeRateCodes(event){
    //     this.getAvailableRateCodes(event.target.value);
       
    //  }


    private calculateAnnualRate(perDiemRate: number) {
      return perDiemRate * 365 * this.awardedBedsNum;
    }

    private changePerdiumRate(event){
      console.log(event);
      console.log(this.perDiemRatesMap);
      console.log(this.perDiemRatesMap[event.value])
      var currentPerDiemRate = this.perDiemRatesMap[event.value];

      // Calculation for Annual and Monthly rates
      // var annualRate = this.calculateAnnualRate(currentPerDiemRate);
      
      var currentAnnualRate = Number(currentPerDiemRate) * 365 ;
      var currentMonthlyRate = currentAnnualRate / 12;
      this.ratesForm.patchValue(
        {
          per_diem_rate : currentPerDiemRate,
          annual_rate: currentAnnualRate,
          monthly_rate: currentMonthlyRate
        }
      );
    }


    selectPersonIndex(index) {
      this.selectedPersonIndex = index;
    }

    viewRateData(rate) {
      this.FormMode="View";
      this.ratesForm.patchValue(rate);
      this.ratesForm.disable();
    }

    editRateData(rate) {
      this.FormMode="Edit";
      this.ratesForm.patchValue(rate);
      this.ratesForm.enable();
    }

    resetRateForm() {
      this.FormMode=="Add"
      this.ratesForm.reset();
      this.initRatesForm();
      this.getProviderrate();
    }
  private createNewRates(req) {

    this.selectedChild[this.selectedPersonIndex].rate=req;
    let message;
    if(this.FormMode=="Edit")
      message="Rate Updated successfully";
    else
      message="Rate Added successfully";

    this.alertService.success(message);
    this.ratesForm.reset();
    this.initRatesForm();
    this.getProviderrate();
    
  //  if(req.license_type && req.license_type!=""){
    // this._commonHttpService.create(req, "providercontract/addprovidercontractrates").subscribe(
    //   (response) => {
    //     if (response) {
    //       let message;
    //       if(this.FormMode=="Edit")
    //         message="Rate Updated successfully";
    //       else
    //         message="Rate Created successfully";

    //       this.alertService.success(message);
    //       this.ratesForm.reset();
    //       this.initRatesForm();
    //       this.getProviderrate();
    //     }
    //   },
    //   (error) => {
    //     this.alertService.error("Unable to Create Rate");
    //   }
    // );

    // this.licenserates.unshift(obj);
    (<any>$('#provider-rates')).modal('hide');
    // }
    // else{
    //   this._alertService.error("Unable to Create Rate,Please check Manadatory fields");
    // }
  }



  closewindow(){
    this.initRatesForm();
    this.resetRateForm();
  }
  
  private editRates(obj) {
    // (<any>$('#provider-rates')).modal('hide');
  }


 private EditRate(Obj){
    this.FormMode="Edit";
    this.ratesForm.patchValue({
      program_rate_id:Obj.program_rate_id,
      license_type:Obj.license_type,
      per_diem_rate:Obj.per_diem_rate_no,
      start_dt:Obj.start_dt,
      end_dt:Obj.end_dt,
      monthly_rate:Obj.monthly_rate_no,
      annual_rate: Obj.annual_rate_no,
      rate_code :Obj.rate_code,
      
      
    });
    //this.licenserates.unshift(Obj);

    (<any>$('#provider-rates')).modal('show');
  }

  DeleteRate(Obj){
    
    (<any>$('Deleterate')).modal('show');
  }
}
