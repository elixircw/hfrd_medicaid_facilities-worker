import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractNarrativeComponent } from './contract-narrative.component';

describe('ContractNarrativeComponent', () => {
  let component: ContractNarrativeComponent;
  let fixture: ComponentFixture<ContractNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
