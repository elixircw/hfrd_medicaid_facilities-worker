import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractModificationComponent } from './contract-modification.component';

describe('ContractModificationComponent', () => {
  let component: ContractModificationComponent;
  let fixture: ComponentFixture<ContractModificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractModificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractModificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
