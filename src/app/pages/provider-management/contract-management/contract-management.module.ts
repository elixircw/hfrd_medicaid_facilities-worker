import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewProviderRoutingModule } from './contract-management-routing.module';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatSlideToggleModule
} from '@angular/material';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { ContractManagementComponent } from './contract-management.component';
import { ContractRatesComponent } from './contract-rates/contract-rates.component';
import { ContractNarrativeComponent } from './contract-narrative/contract-narrative.component';
import { ContractServicesComponent } from './contract-services/contract-services.component';
import { ContractAddCharacteristicsComponent } from './contract-add-characteristics/contract-add-characteristics.component';
import { ContractLicenseServicesComponent } from './contract-license-services/contract-license-services.component';
import { ContractInformationComponent } from './contract-information/contract-information.component';
import { ContractManagementSerivce } from './contract-management.service';
import { BedAllocationComponent } from './bed-allocation/bed-allocation.component';
import { ContractModificationComponent } from './contract-modification/contract-modification.component';
import { ChildSpecificAgreementComponent } from './child-specific-agreement/child-specific-agreement.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap/pagination/pagination.module';

@NgModule({
  imports: [
    CommonModule, NewProviderRoutingModule, MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    NgxfUploaderModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    FormMaterialModule,
    PaginationModule
  ],
  declarations: [ContractManagementComponent, ContractRatesComponent, ContractNarrativeComponent, ContractServicesComponent, ContractAddCharacteristicsComponent, ContractLicenseServicesComponent, ContractInformationComponent, BedAllocationComponent, ContractModificationComponent, ChildSpecificAgreementComponent
],
  entryComponents: [ContractAddCharacteristicsComponent],

  providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService,ContractManagementSerivce]
})
export class ContractManagementModule { }
