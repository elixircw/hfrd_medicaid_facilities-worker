import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContractManagementComponent } from './contract-management.component';
import { ContractInformationComponent } from './contract-information/contract-information.component';
import { ContractLicenseServicesComponent } from './contract-license-services/contract-license-services.component';
import { ContractNarrativeComponent } from './contract-narrative/contract-narrative.component';
import { ContractRatesComponent } from './contract-rates/contract-rates.component';
import { BedAllocationComponent } from './bed-allocation/bed-allocation.component';
import { ContractModificationComponent } from './contract-modification/contract-modification.component';
import { ChildSpecificAgreementComponent } from './child-specific-agreement/child-specific-agreement.component';



const routes: Routes = [
    {
        path: ':id',
        component: ContractManagementComponent,
        children: [
            {
                path: '',
                redirectTo: 'basic',
                pathMatch: 'full',
            },
            {
                path: 'basic',
                component: ContractInformationComponent
            },
            {
                path: 'service',
                component: ContractLicenseServicesComponent
            },
            {
                path: 'rates',
                component: ContractRatesComponent
            },
            {
                path: 'narrative',
                component: ContractNarrativeComponent
            },
            {
                path: 'bed-allocation',
                component: BedAllocationComponent
            },
            {
                path: 'modification',
                component: ContractModificationComponent
            },
            {
                path: 'csa',
                component: ChildSpecificAgreementComponent
            }
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewProviderRoutingModule { }
