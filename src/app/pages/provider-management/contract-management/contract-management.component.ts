import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { AlertService } from '../../../@core/services/alert.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ContractManagementSerivce } from './contract-management.service';
//import { DataStoreService } from '../../../@core/services';

@Component({
  selector: 'contract-management',
  templateUrl: './contract-management.component.html',
  styleUrls: ['./contract-management.component.scss']
})
export class ContractManagementComponent implements OnInit {


  contractLicenseTypeFormGroup: FormGroup;
  contractLicenseTypeId: String;
  placementStatus: String;

  lincenseTypeInfo: any;
  providerId: string;
  tabs = [];
  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    //private _dataStoreService: DataStoreService
    private _serivce: ContractManagementSerivce
  ) {
    this.contractLicenseTypeId = route.snapshot.params['id'];
    this._serivce.setcontractLicenseTypeId(this.contractLicenseTypeId);
  }

  ngOnInit() {
    this.placementStatus = 'No';
    this.tabs = [{
      name: 'Basic Information',
      path: 'basic'
    },
    {
      name: 'License Services',
      path: 'service'
    },
    {
      name: 'Rates',
      path: 'rates'
    },
    {
      name: 'Bed Allocation',
      path: 'bed-allocation'
    },
    {
      name: 'Narratives',
      path: 'narrative'
    },
    {
      name: 'Modification',
      path: 'modification'
    },
    {
      name: 'Child Specific Agreement',
      path: 'csa'
    }];
    this.initFormGroups();
    this.getLicenseTypeInformation();
  }

  initFormGroups() {
    this.contractLicenseTypeFormGroup = this.formBuilder.group(
      {
        contractLicenseNumber: this.contractLicenseTypeId,
        contractNumber: [''],
        placementStatus: this.placementStatus
      }
    );
  }

  setPlacements(setPlacementStatusActive: boolean) {
    if (setPlacementStatusActive == true) {
      this.contractLicenseTypeFormGroup.patchValue({
        placementStatus: 'Yes'
      });
    }
  }

  getLicenseTypeInformation() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          contractLicenseTypeNo: this.contractLicenseTypeId,
        }
      },
      'providerlicense/getlicenseinformation')
      .subscribe(licenseservices => {
        console.log('License type information', licenseservices);
        if (licenseservices) {
          this.lincenseTypeInfo = licenseservices;
          if (this.lincenseTypeInfo.data && this.lincenseTypeInfo.data.length) {
            this.lincenseTypeInfo = this.lincenseTypeInfo.data[0];
            this._serivce.setLisenseInfo(this.lincenseTypeInfo);
            this.providerId = this.lincenseTypeInfo.provider_id;
          }

        }
        //this._dataStoreService.setData('currentProviderId', this.providerId);    
      });
  }
  openTab(tab) {
    this._router.navigate([tab.path], { relativeTo: this.route });
  }

}