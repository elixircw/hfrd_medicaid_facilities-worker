import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import {  AlertService } from '../../../../@core/services';
@Component({
  selector: 'contract-rates',
  templateUrl: './contract-rates.component.html',
  styleUrls: ['./contract-rates.component.scss']
})
export class ContractRatesComponent implements OnInit {

  licenserates=Array<Object>();
  licenseTypes=[];
  licensetyperates:any=Array<Object>();
  show: boolean = true;

  minDate = new Date();
  
  providerId: string;
  programId: string;

  ratesForm: FormGroup;
  FormMode:string;

  perDiemRatesMap: any = {};
  awardedBedsNum: number;

  // Get this once in the parent
  lincenseTypeInfo: any;
  currentLicenseType: string;

  constructor(private _commonHttpService: CommonHttpService,    private route: ActivatedRoute,private formBuilder: FormBuilder,private _alertService: AlertService) {
    this.programId = route.snapshot.parent.params['id'];
   }

   ngOnInit() {

     this.licenserates=[];
    
    this.getLicenseTypeInformation();
    this.getProviderrate();
    this.initRatesForm();
    
    //TODO: Get this from the API/basic information tab
    this.awardedBedsNum = 20;
  }
  
  private initRatesForm() {
    this.FormMode="Add";
    this.ratesForm = this.formBuilder.group({
      program_rate_id:0,
      provider_id: this.programId,
      license_type:[''],
      annual_rate: [''],
      monthly_rate: [''],
      per_diem_rate: [''],
      start_dt:new Date(),
      end_dt:new Date(),
      rate_code :[''],
      rate_status:[''],
    });
  }

  private getProviderrate() {
      this._commonHttpService.getArrayList(
        {
            method: 'post',
            nolimit: true,
            filter: {},
            where:
            {
              provider_id: this.programId
            }
        },
        'providercontract/getprovidercontractrates')
        .subscribe(licenserate => 
          {
            this.licenserates = licenserate;
            // this.licenserates.unshift(this.licenserates);
          });      
    }
    

    getLicenseTypeInformation() {
      this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            contractLicenseTypeNo: this.programId,
          }
      },
      'providerlicense/getlicenseinformation')
      .subscribe(licenseservices => 
        {
          if(licenseservices){
            this.lincenseTypeInfo = licenseservices;
          }
          if(this.lincenseTypeInfo.data && this.lincenseTypeInfo.data.length){
            this.lincenseTypeInfo = this.lincenseTypeInfo.data[0];          
            this.currentLicenseType = this.lincenseTypeInfo.license_type;
            this.providerId = this.lincenseTypeInfo.provider_id;
          }          

          this.getAvailableRateCodes();
      });  
    }

    private getAvailableRateCodes() {
      console.log("GETTING RATES CODES")
      this._commonHttpService.getArrayList(
        {
            method: 'post',
            nolimit: true,
            filter: {},
            where:
            {
              provider_id: this.providerId,
              license_type: this.currentLicenseType
            }
        },
        'providercontract/getproviderlicensetyperates')
        .subscribe(licensetyperate => 
          {
            this.licensetyperates = licensetyperate;

            this.licensetyperates.data.forEach((element) => {
              this.perDiemRatesMap[element.rate_code] = element.per_diem_rate;
            })
          }); 
    }
    
    //  private changeRateCodes(event){
    //     this.getAvailableRateCodes(event.target.value);
       
    //  }


    private calculateAnnualRate(perDiemRate: number) {
      return perDiemRate * 365 * this.awardedBedsNum;
    }

    private changePerdiumRate(event){
      console.log(event);
      console.log(this.perDiemRatesMap);
      console.log(this.perDiemRatesMap[event.value])
      var currentPerDiemRate = this.perDiemRatesMap[event.value];

      // Calculation for Annual and Monthly rates
      // var annualRate = this.calculateAnnualRate(currentPerDiemRate);
      
      var currentAnnualRate = Number(currentPerDiemRate) * 365 * this.awardedBedsNum;
      var currentMonthlyRate = currentAnnualRate / 12;
      this.ratesForm.patchValue(
        {
          per_diem_rate : currentPerDiemRate,
          annual_rate: currentAnnualRate,
          monthly_rate: currentMonthlyRate
        }
      );
    }


  private createNewRates(req) {
    
  //  if(req.license_type && req.license_type!=""){
    this._commonHttpService.create(req, "providercontract/addprovidercontractrates").subscribe(
      (response) => {
        if (response) {
          let message;
          if(this.FormMode=="Edit")
            message="Rate Updated successfully";
          else
            message="Rate Created successfully";

          this._alertService.success(message);
          this.ratesForm.reset();
          this.initRatesForm();
          this.getProviderrate();
        }
      },
      (error) => {
        this._alertService.error("Unable to Create Rate");
      }
    );

    // this.licenserates.unshift(obj);
    (<any>$('#provider-rates')).modal('hide');
    // }
    // else{
    //   this._alertService.error("Unable to Create Rate,Please check Manadatory fields");
    // }
  }



  closewindow(){
    this.initRatesForm();
  }
  
  private editRates(obj) {
    // (<any>$('#provider-rates')).modal('hide');
  }


 private EditRate(Obj){
    this.FormMode="Edit";
    this.ratesForm.patchValue({
      program_rate_id:Obj.program_rate_id,
      license_type:Obj.license_type,
      per_diem_rate:Obj.per_diem_rate_no,
      start_dt:Obj.start_dt,
      end_dt:Obj.end_dt,
      monthly_rate:Obj.monthly_rate_no,
      annual_rate: Obj.annual_rate_no,
      rate_code :Obj.rate_code,
      
      
    });
    //this.licenserates.unshift(Obj);

    (<any>$('#provider-rates')).modal('show');
  }

  DeleteRate(Obj){
    
    (<any>$('Deleterate')).modal('show');
  }

}
