import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { CommonHttpService, GenericService, AlertService } from '../../../../@core/services';
import { ApplicantUrlConfig } from '../../provider-management-url.config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'contract-add-characteristics',
  templateUrl: './contract-add-characteristics.component.html',
  styleUrls: ['./contract-add-characteristics.component.scss']
})
export class ContractAddCharacteristicsComponent implements OnInit {

  activityDropdownItems$: DropdownModel[];
  activityDropdownItems1$: Observable<DropdownModel[]>;
  formGroup: FormGroup;
  leftTasks: any[] = [];
  rightTasks: any[] = [];
  private leftSelectedTasks = [];
  servicesObj:any = {};
  private rightSelectedTasks = [];
  applicantNumber: string;
  type: string;
  defaultArray = [];
  monthDropDown = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  yearDropDown = [];
  periodicDropDown = ['Monthly', 'Quarterly', 'Yearly', 'Initial', 'Periodic', 'Mid-Year', 'Re-Licensure'];
  timeOfVisit = ['Night','Weekend', 'Regular'];
  visitSchedule = ['Announced', 'Unannounced'];  
  isMonthDisabled:boolean = true;
  isYearDisabled:boolean = true;
  _ref: any;
  category: string;
  newObj = {};

  providerId: string;
  programId: string;

  @Output('onSaveEventEmitter') onSaveEventEmitter = new EventEmitter();

  constructor(private _commonHttpService: CommonHttpService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _alertService: AlertService) {
        this.programId = route.snapshot.params['id'];
  }

  removeObject() {
    this._ref.destroy();
  }

  ngOnInit() {
    // this.loadDropDown();
    this.initializeFormGroup();
    this.initializeYearCalendar();
    this.onChangeActivity();
  }

  initializeYearCalendar() {
    for (var i = 2000; i <= new Date().getFullYear(); ++i) {
      this.yearDropDown.push(i);
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.formBuilder.group({
      leftSelectedTask: [''],
      rightSelectedTask: [''],
      leftSelectedGoals: [''],
      rightSelectedGoals: [''],
      selectActivityList: [''],
      periodic: [''],
      monthly: [''],
      yearly: [''],
      schedule:[''],
      visitTime:['']
    });
  }

  // private loadDropDown() {
  //   this._commonHttpService.getArrayList(
  //     {
  //     },
  //     'providerservice/listservicecategories'
  //   ).subscribe(taskList => {
  //     console.log(taskList);
  //     this.activityDropdownItems$ = taskList;
  //   });
  // }

  private onChangeActivity() {

    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          picklist_type_id: "43"
        }
      },
      'providercontract/getpicklistvaluesbytypeid'
    ).subscribe(res => {
      this.servicesObj = res;
      this.leftTasks = this.servicesObj.data;
    });

    // console.log(this.newObj[event.source.selected._element.nativeElement.innerText.trim()]);
    // this.rightTasks.length = 0;
    // this.leftTasks = this.newObj[event.source.selected._element.nativeElement.innerText.trim()];
  }

  ngOnDestroy() {
    console.log('Destroy');
  }

  onClose() {
    console.log('onClose');
    this.removeObject();
  }

  toggleTask(position: string, selectedItem: any, control: any) {
    if (position === 'left') {
      if (control.target.checked) {
        this.leftSelectedTasks.push(selectedItem);
      } else {
        const index = this.leftSelectedTasks.indexOf(selectedItem);
        this.leftSelectedTasks.splice(index, 1);
      }
    } else {
      if (control.target.checked) {
        this.rightSelectedTasks.push(selectedItem);
      } else {
        const index = this.rightSelectedTasks.indexOf(selectedItem);
        this.rightSelectedTasks.splice(index, 1);
      }
    }
  }

  selectAllTask() {
    if (this.leftTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, Object.assign([], this.leftTasks));
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  selectTask() {
    if (this.leftSelectedTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, this.leftSelectedTasks);
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  unSelectTask() {
    if (this.rightSelectedTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, this.rightSelectedTasks);
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }
  unSelectAllTask() {
    if (this.rightTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, Object.assign([], this.rightTasks));
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  private clearSelectedTasks(position: string) {
    if (position === 'left') {
      this.leftSelectedTasks = [];
    } else {
      this.rightSelectedTasks = [];
    }
  }

  private moveTasks(source: any[], target: any[], selectedItems: any[]) {
    selectedItems.forEach((item: any, i) => {
      const selectedIndex = source.indexOf(item);
      if (selectedIndex !== -1 /*selectedItem.length*/) {
        source.splice(selectedIndex, 1);
        target.push(item);
      }
    });
    //this._changeDetect.detectChanges();
  }

  activitySave() {
    console.log('Save ');
    this.defaultArray = this.defaultArray.concat(this.rightTasks);
    console.log("@@@@@");
    console.log(this.defaultArray);

    let payload = {}

    payload['provider_id'] = this.providerId;
    payload['program_id'] = this.programId;
    payload['services'] = this.defaultArray;
    console.log(payload);

    this._commonHttpService.create(payload, "providercontract/addcontractchildcharacteristics").subscribe(
      (response) => {
        this.onSaveEventEmitter.emit(this.defaultArray);
      },
      (error) => {
        this._alertService.error("Unable to save Characteristics");
      }
    );



    this.onClose();
    //   this.defaultArray = this.defaultArray.concat(this.rightTasks);

  //   if(this.category == 'Monitoring'){
  //     this.defaultArray.forEach((element) => {
  //       element['monitoring_type'] = this.formGroup.value.periodic,
  //       element['monitoring_year'] = this.formGroup.value.yearly,
  //       element['monitoring_period'] = this.formGroup.value.monthly
  //     })
  //   }

  //   let requestObj = {
  //     "applicant_id": this.applicantNumber,
  //     "task": this.defaultArray
  //   }
  //   this._commonHttpService.create(requestObj, 'providerapplicant/addchecklist').subscribe(
  //     (response) => {
  //       this._alertService.success('CheckList Added successfully!');
  //     },
  //     (error) => {
  //       this._alertService.error('Unable to save Checklist, please try again.');
  //       console.log('Save Checklist Error', error);
  //       return false;
  //     }
  //   );

  }

  onPeriodChange() {
    this.resetDropDowns();
    if (this.formGroup.value.periodic == 'Monthly') {
      this.isMonthDisabled = false;
      this.isYearDisabled = false;
    } else {
      this.isYearDisabled = false;
    }
  }

  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.formGroup.controls['monthly'].setValue('');
    this.formGroup.controls['yearly'].setValue('');
  }


}
