import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractAddCharacteristicsComponent } from './contract-add-characteristics.component';

describe('ContractAddCharacteristicsComponent', () => {
  let component: ContractAddCharacteristicsComponent;
  let fixture: ComponentFixture<ContractAddCharacteristicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractAddCharacteristicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractAddCharacteristicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
