import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Input, Output, EventEmitter } from '@angular/core';
import { ContractAddCharacteristicsComponent } from '../contract-add-characteristics/contract-add-characteristics.component';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'contract-information',
  templateUrl: './contract-information.component.html',
  styleUrls: ['./contract-information.component.scss']
})
export class ContractInformationComponent implements OnInit {
  tableData = [];
  currentData = [];
  FormMode: string;
  id: string;
  @ViewChild('addServices', { read: ViewContainerRef }) container: ViewContainerRef;
  @Output() setActivePlacements = new EventEmitter<boolean>();

  minDate = new Date();
  providerId: string;
  programId: string;
  picklistTypeId: string;
  contractInfoForm: FormGroup;
  picklistProviderId: any;
  contractLicenseTypeStatus = [];

  // Get this once in the parent
  lincenseTypeInfo: any;
  currentLicenseType: string;

  tfc: boolean;
  monitorForm: FormGroup;
  isMonthDisabled: boolean = true;
  isYearDisabled: boolean = true;
  activityTaskStatusFormGroup: FormGroup;
  tasktypestatus = [];
  taskList = [];

  isPrepositionChecked: boolean;

  @Input()
  programTypeInputForMonitoring = new Subject<string>();
  programType: string;

  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private formBuilder: FormBuilder,
    private _alertService: AlertService, private _cfr: ComponentFactoryResolver) {
    this.programId = route.snapshot.parent.params['id'];
    this.picklistTypeId = '43';
    this.id = route.snapshot.parent.params['id'];
  }

  ngOnInit() {
    this.contractLicenseTypeStatus = ['In Process', 'Active', 'Closed', 'Sanctioned-Suspended', 'Sanctioned-Limitation', 'Sanctioned-Revocation'];
    this.initServiceForm();
    this.getLicenseTypeInformation();

    //this.getProviderContractChar();  

    this.tfc = false;
    this.isPrepositionChecked = false;


    this.activityTaskStatusFormGroup = this.formBuilder.group({
      task: this.formBuilder.array([])
    });

    // this.getTaskList();
    this.programTypeInputForMonitoring.subscribe((data) => {
      this.programType = data;
    })
  }

  getLicenseTypeInformation() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          contractLicenseTypeNo: this.programId,
        }
      },
      'providerlicense/getlicenseinformation')
      .subscribe(licenseservices => {
        if (licenseservices) {
          this.lincenseTypeInfo = licenseservices;
        }
        if (this.lincenseTypeInfo.data && this.lincenseTypeInfo.data.length) {
          this.lincenseTypeInfo = this.lincenseTypeInfo.data[0];
          // this.tableData.unshift(this.currentData);
          this.currentLicenseType = this.lincenseTypeInfo.license_type;
          this.providerId = this.lincenseTypeInfo.provider_id;
        }

        this.getProviderContractChar();
        this.contractInfoForm.patchValue(this.lincenseTypeInfo);
        this.contractInfoForm.patchValue({
          license_type: this.currentLicenseType
        });

        if (this.currentLicenseType === 'Treatment Foster Care') {
          this.tfc = true;
        }
      });
  }

  private initServiceForm() {
    this.FormMode = 'Add';
    this.contractInfoForm = this.formBuilder.group({
      provider_id: this.providerId,
      program_id: this.programId,
      license_bed: [null],
      irc_bed_capacity: [null],
      contract_beds_no: [null],
      start_dt: null,
      end_dt: null,
      fiscal_date: new Date('June 30, 2018'),
      capacity_percentage_no: [''],
      contract_license_type_status: this.contractLicenseTypeStatus[0],
      license_type: [''],
      adr_street: [''],
      adr_city: [''],
      adr_state: [''],
      adr_zip: ['']
    });
  }

  // editServices(obj){
  //   this.contractInfoForm.patchValue({
  //     provider_id:this.providerId,
  //     program_id: this.programId,
  //     contract_beds_no:obj.contract_beds_no,
  //     start_dt: obj.start_dt,
  //     end_dt :obj.end_dt,
  //     contract_info_Urate :obj.contract_info_Urate,
  //     fiscal_date :obj.fiscal_date,

  //   });
  //   //this.licenserates.unshift(obj);

  //   (<any>$('#contract-info')).modal('show');
  // }  

  saveContractInfo(obj) {

    if (obj.contract_license_type_status == 'Active') {
      if (obj.start_dt == null || obj.end_dt == null || obj.start_dt > obj.end_dt) {
        this._alertService.error('Cannot set status to ACTIVE, please fill all details');
      }
      else {
        this._alertService.success('Status set to ACTIVE');
        this.setActivePlacements.emit(true);
      }
    } else {
      this._alertService.success('Information saved!');
    }

    this._commonHttpService.create(obj, 'providercontract/addprovidercontractinfo').subscribe(
      (response) => {
        // where:{
        //   this.programId='50000027'
        // }
      },
      (error) => {
        this._alertService.error('Unable to save contract Info');
      }
    );
  }

  createTaskForm() {
    return this.formBuilder.group({
      // paid_non_paid_cd: [''],
      picklist_value_cd: [''],
      value_tx: [''],
      // structure_service_cd: ['']
    });
  }

  private buildTaskForm(x): FormGroup {
    return this.formBuilder.group({
      // paid_non_paid_cd: x.paid_non_paid_cd,
      picklist_value_cd: x.picklist_value_cd,
      value_tx: x.value_tx,
      // structure_service_cd: x.structure_service_cd
    });
  }

  setFormValues() {
    this.activityTaskStatusFormGroup.setControl('task', this.formBuilder.array([]));
    const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    this.taskList.forEach((x) => {
      control.push(this.buildTaskForm(x));
    })
    console.log(this.activityTaskStatusFormGroup.value);
    console.log(this.activityTaskStatusFormGroup.controls);
  }

  openMonitoringCheckList() {
    // check and resolve the component
    let comp = this._cfr.resolveComponentFactory(ContractAddCharacteristicsComponent);
    // Create component inside container
    let expComponent = this.container.createComponent(comp);
    // see explanations
    expComponent.instance._ref = expComponent;
    expComponent.instance.category = 'Monitoring';
    expComponent.instance.type = this.programType;
    expComponent.instance.providerId = this.providerId;

    expComponent.instance.onSaveEventEmitter.subscribe(($event) => {
      console.log(' SIMAR Event Captured' + $event);
      // $event.forEach((x) => {
      //   this.taskList.push(x);
      // })
      this.setFormValues();
      console.log('SIMAR Calling services')
      this.getProviderContractChar();
    });
  }


  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
  }
  private getProviderContractChar() {
    this.tableData = [];
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          provider_id: this.providerId,
          program_id: this.programId,
          picklist_type_id: this.picklistTypeId
        }
      },
      'providercontract/getcontractchildcharacteristics')
      .subscribe(licenseservices => {
        this.tableData = licenseservices;
        // this.tableData.unshift(this.currentData);
        console.log('Final table data', this.tableData);
      });
  }

  DeleteChar(picklistProviderId) {
    this.picklistProviderId = picklistProviderId;
    (<any>$('#delet-char')).modal('show');
  }

  deleteConfirm() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          provider_id: this.providerId,
          picklist_type_id: this.picklistProviderId
        }
      },
      'providercontract/deletecontractchildcharacteristics')
      .subscribe(licenseservices => {
        if (licenseservices) {
          this._alertService.success('Characteristics Deleted successfully');
          this.getProviderContractChar();
          this.picklistProviderId = '';
          (<any>$('#delet-char')).modal('hide');
        }
      });
  }

  togglePreposition() {
    // Patch Address when Auto complete selected
    this.contractInfoForm.patchValue({
      adr_street: this.lincenseTypeInfo.adr_street_nm,
      adr_city: this.lincenseTypeInfo.adr_city_nm,
      adr_state: this.lincenseTypeInfo.adr_state_cd,
      adr_zip: this.lincenseTypeInfo.adr_zip5_no
    });
  }
}
