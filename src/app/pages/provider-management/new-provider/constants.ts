import { AppConstants } from '../../../../app/@core/common/constants';

export class CapConstants {


public static CAP_STATUS = {
    pending: 'pending',
    cap_send: 'cap_send',
    cap_submitted: 'cap_submitted', // PRCM
    cap_accepted: 'cap_accepted',
    cap_reviewed: 'cap_reviewed',
    cap_approved: 'cap_approved',
    cap_submit_rejected: 'cap_submit_rejected',
    cap_review_rejected: 'cap_review_rejected',
    cap_approval_rejected: 'cap_approval_rejected',
    cap_all: null,
    cap_sent: 'cap_sent',
    cap_closed: 'cap_closed'
};




public static CAP_STATUS_CODE = {
    pending: 'pending',
    cap_send: 'cap_send',
    cap_submitted: '101', // PRCM
    cap_accepted: '102',
    cap_reviewed: '103',
    cap_approved: '104',
    cap_submit_rejected: '106',
    cap_review_rejected: '107',
    cap_approval_rejected: '108',
    cap_all: null,
    cap_sent: '105',
    cap_closed: '109'
};
// Description used for the notification messages
public static CAP_STATUS_DESCRIPTION = {
    cap_send: 'Cap Notified to Provider',
    cap_submitted: 'Cap Submitted', // PRCM
    cap_accepted: 'Cap Accepted By Worker',
    cap_reviewed: 'Cap Reviewed',
    cap_approved: 'Cap Approved',
    cap_submit_rejected: 'Cap Submission Rejected',
    cap_review_rejected: 'Cap Review Rejected',
    cap_approval_rejected: 'Cap Approval Rejected',
    cap_all: null,
    cap_sent: 'Cap Sent',
    cap_closed: 'Cap Closed'
};

public static CAP_STATUS_NOTIFICATION = {
    cap_send: 'Cap Notified to Provider',
    cap_submitted: 'Corrective Action Plan has been submitted by the provider', // PRCM
    cap_accepted: 'Corrective Action Plan has been Accepted by Worker',
    cap_reviewed: 'Corrective Action Plan has been Reviewed by Program Manager / Resource Specialist',
    cap_approved: 'Corrective Action Plan has been Approved by Executive Director',
    cap_submit_rejected: 'Corrective Action Plan has been rejected and returned to the provider',
    cap_review_rejected: 'Corrective Action Plan has been rejected on review process and returned to the worker',
    cap_approval_rejected: 'Corrective Action Plan has been rejected on approval process and returned to the worker',
    cap_all: null,
    cap_sent: 'Cap Sent',
    cap_closed: 'Cap Closed'
};

public static CAP_REVIEW_DECISONS = [
    {
        displayName: 'Review',
        eventcode: CapConstants.CAP_STATUS.cap_reviewed,
        roles: [
            AppConstants.ROLES.PROGRAM_MANAGER,
            AppConstants.ROLES.PROVIDER_DJS_RESOURCE,

        ],
        statuses: [
            CapConstants.CAP_STATUS_CODE.cap_submitted,
            CapConstants.CAP_STATUS_CODE.cap_accepted,
            CapConstants.CAP_STATUS_CODE.cap_reviewed,
            CapConstants.CAP_STATUS_CODE.cap_review_rejected,
            CapConstants.CAP_STATUS_CODE.cap_approval_rejected,
            CapConstants.CAP_STATUS_CODE.cap_submit_rejected,
            CapConstants.CAP_STATUS_CODE.cap_closed
        ]
    },
    {
        displayName: 'Approve',
        eventcode: CapConstants.CAP_STATUS.cap_approved,
        roles: [
            AppConstants.ROLES.EXECUTIVE_DIRECTOR,
            AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
        ],
        statuses: [
            CapConstants.CAP_STATUS_CODE.cap_submitted,
            CapConstants.CAP_STATUS_CODE.cap_accepted,
            CapConstants.CAP_STATUS_CODE.cap_reviewed,
            CapConstants.CAP_STATUS_CODE.cap_review_rejected,
            CapConstants.CAP_STATUS_CODE.cap_approval_rejected,
            CapConstants.CAP_STATUS_CODE.cap_submit_rejected,
            CapConstants.CAP_STATUS_CODE.cap_closed
        ]
    },
    {
        displayName: 'Accept',
        eventcode: CapConstants.CAP_STATUS.cap_accepted,
        roles: [
            AppConstants.ROLES.PROVIDER_DJS_QA,
            AppConstants.ROLES.QUALITY_ASSURANCE,
        ],
        statuses: [
            CapConstants.CAP_STATUS_CODE.cap_submitted,
            CapConstants.CAP_STATUS_CODE.cap_accepted,
            CapConstants.CAP_STATUS_CODE.cap_reviewed,
            CapConstants.CAP_STATUS_CODE.cap_review_rejected,
            CapConstants.CAP_STATUS_CODE.cap_approval_rejected,
            CapConstants.CAP_STATUS_CODE.cap_submit_rejected,
            CapConstants.CAP_STATUS_CODE.cap_closed
        ]
    },
    {
        displayName: 'Denies and closes',
        eventcode: CapConstants.CAP_STATUS.cap_closed,
        roles: [
            AppConstants.ROLES.PROGRAM_MANAGER,
            AppConstants.ROLES.EXECUTIVE_DIRECTOR,
            AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
            AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
        ],
        statuses: [
            CapConstants.CAP_STATUS_CODE.cap_submitted,
            CapConstants.CAP_STATUS_CODE.cap_accepted,
            CapConstants.CAP_STATUS_CODE.cap_reviewed,
            CapConstants.CAP_STATUS_CODE.cap_review_rejected,
            CapConstants.CAP_STATUS_CODE.cap_approval_rejected,
            CapConstants.CAP_STATUS_CODE.cap_submit_rejected,
            CapConstants.CAP_STATUS_CODE.cap_closed
        ]
    },
    /* {
        displayName: 'Return to Worker',
        eventcode: CapConstants.CAP_STATUS.cap_approval_rejected,
        roles: [
            AppConstants.ROLES.EXECUTIVE_DIRECTOR,
            AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
        ],
        statuses: [
            CapConstants.CAP_STATUS_CODE.cap_submitted,
            CapConstants.CAP_STATUS_CODE.cap_accepted,
            CapConstants.CAP_STATUS_CODE.cap_reviewed,
            CapConstants.CAP_STATUS_CODE.cap_review_rejected,
            CapConstants.CAP_STATUS_CODE.cap_approval_rejected,
            CapConstants.CAP_STATUS_CODE.cap_submit_rejected,
            CapConstants.CAP_STATUS_CODE.cap_closed
        ]
    },
    {
        displayName: 'Return to Worker',
        eventcode: CapConstants.CAP_STATUS.cap_review_rejected,
        roles: [
            AppConstants.ROLES.PROGRAM_MANAGER,
            AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
        ],
        statuses: [
            CapConstants.CAP_STATUS_CODE.cap_submitted,
            CapConstants.CAP_STATUS_CODE.cap_accepted,
            CapConstants.CAP_STATUS_CODE.cap_reviewed,
            CapConstants.CAP_STATUS_CODE.cap_review_rejected,
            CapConstants.CAP_STATUS_CODE.cap_approval_rejected,
            CapConstants.CAP_STATUS_CODE.cap_submit_rejected,
            CapConstants.CAP_STATUS_CODE.cap_closed
        ]
    }, */
    {
        displayName: 'Return to Provider',
        eventcode: CapConstants.CAP_STATUS.cap_review_rejected,
        roles: [
            AppConstants.ROLES.PROGRAM_MANAGER,
            AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
            AppConstants.ROLES.QUALITY_ASSURANCE,
            AppConstants.ROLES.LICENSING_ADMINISTRATOR,
            AppConstants.ROLES.PROVIDER_DJS_QA,
        ],
        statuses: [
            CapConstants.CAP_STATUS_CODE.cap_submitted,
            CapConstants.CAP_STATUS_CODE.cap_accepted,
            CapConstants.CAP_STATUS_CODE.cap_reviewed,
            CapConstants.CAP_STATUS_CODE.cap_review_rejected,
            CapConstants.CAP_STATUS_CODE.cap_approval_rejected,
            CapConstants.CAP_STATUS_CODE.cap_submit_rejected,
            CapConstants.CAP_STATUS_CODE.cap_closed
        ]
    }
];



}
