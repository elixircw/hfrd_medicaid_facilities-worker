import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderLicensesRoutingModule } from './provider-licenses-routing.module';
import { ProviderLicensesComponent } from './provider-licenses.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    ProviderLicensesRoutingModule,
    FormMaterialModule
  ],
  declarations: [ProviderLicensesComponent]
})
export class ProviderLicensesModule { }
