import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderLicensesComponent } from './provider-licenses.component';

const routes: Routes = [
  {
    path: '' ,
    component: ProviderLicensesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderLicensesRoutingModule { }
