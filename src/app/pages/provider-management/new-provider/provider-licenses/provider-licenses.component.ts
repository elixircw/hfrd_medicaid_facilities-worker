import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import {MatTableDataSource} from '@angular/material';

export interface licenseInfoTable {
  licenseType: string;
  licenseNo: number;
  siteId: number;
  providerName: string;
  providerCategoryCode: number;
}

@Component({
  selector: 'provider-licenses',
  templateUrl: './provider-licenses.component.html',
  styleUrls: ['./provider-licenses.component.scss']
})

export class ProviderLicensesComponent implements OnInit {

  providerId: string;
  licenseInformation:any={};
  dataSource:any;
  displayedColumns: string[] = ['licenseNo', 'licenseLevel', 'licenseType', 'licenseTypeStatus', 'providerName', 'officePhone', 'providerCategoryCode', 'contractId', 'contractStatus'];

  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute) {
    this.providerId = route.snapshot.parent.parent.params['id'];
  }

  ngOnInit() {
    this.getProviderContractLicenseTypes();
  }

  private getProviderContractLicenseTypes() {
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            provider_id: this.providerId
          }
      },
      'providercontract/getproviderlicenseinformation'
  ).subscribe(licenseInformation => {
    this.licenseInformation = licenseInformation;
    this.dataSource = new MatTableDataSource<licenseInfoTable>(this.licenseInformation.data);
  });
}

}
