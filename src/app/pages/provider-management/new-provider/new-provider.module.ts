import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewProviderRoutingModule } from './new-provider-routing.module';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatStepperModule
} from '@angular/material';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { NewProviderComponent } from './new-provider.component';
import { ProviderContractComponent } from './provider-contract/provider-contract.component';
import { ProviderServiceComponent } from './provider-service/provider-service.component';
import { AddServiceComponent } from './add-service/add-service.component';


import { AttachmentDetailComponent } from './add-document/attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './add-document/attachment-upload/attachment-upload.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { AudioRecordComponent } from './add-document/audio-record/audio-record.component';
import { EditAttachmentComponent } from './add-document/edit-attachment/edit-attachment.component';
import { ImageRecordComponent } from './add-document/image-record/image-record.component';
import { VideoRecordComponent } from './add-document/video-record/video-record.component';
import { AttachmentNarrativeComponent } from './attachment-narrative/attachment-narrative.component';
import { ProviderRateComponent } from './provider-rate/provider-rate.component';
import { ProviderLicensesComponent } from './provider-licenses/provider-licenses.component';
import { ProviderMonitorLicensingComponent } from './provider-monitor-licensing/provider-monitor-licensing.component';
import { AddChecklistComponent } from './add-checklist/add-checklist.component';
import { ProviderStaffComponent } from './provider-staff/provider-staff.component';
import { ProviderDocumentCreatorComponent } from './provider-document-creator/provider-document-creator.component';
import { ProviderLicenseRequestsComponent } from './provider-license-requests/provider-license-requests.component';
import {ProviderCapNotificationComponent} from './provider-cap-notification/provider-cap-notification.component';

@NgModule({
  imports: [
    CommonModule, NewProviderRoutingModule, MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    NgxfUploaderModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    MatStepperModule
  ],
  declarations: [NewProviderComponent, ProviderServiceComponent, AddServiceComponent,
    ProviderMonitorLicensingComponent, AddChecklistComponent, ProviderDocumentCreatorComponent ],
  entryComponents: [AddServiceComponent, AddChecklistComponent],
  providers: [NgxfUploaderService, SpeechRecognitionService, SpeechRecognizerService]
})
export class NewProviderModule { }
