import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewProviderComponent } from './new-provider.component';
import { ProviderMonitorLicensingComponent } from './provider-monitor-licensing/provider-monitor-licensing.component';



const routes: Routes = [
    {
        path: ':id',
        component: NewProviderComponent,
        // canActivate: [RoleGuard],
        children: [
            {
                path: 'provider-licenses',
                loadChildren: './provider-licenses/provider-licenses.module#ProviderLicensesModule'
            },
            {
                path: 'provider-contract',
                loadChildren: './provider-contract/provider-contract.module#ProviderContractModule'
            },
            {
                path: 'add-document',
                loadChildren: './add-document/add-document.module#AddDocumentModule'
            },
            {
                path: 'attachment-narrative',
                loadChildren: './attachment-narrative/attachment-narrative.module#AttachmentNarrativeModule'
            },
            {
                path: 'provider-rate',
                loadChildren: './provider-rate/provider-rate.module#ProviderRateModule'
            },
            {
                path: 'provider-staff',
                loadChildren: './provider-staff/provider-staff.module#ProviderStaffModule'
            },
            {
                path: 'provider-license-requests',
                loadChildren: './provider-license-requests/provider-license-requests.module#ProviderLicenseRequestsModule'
            },
            {
                path: 'monitoring',
                component: ProviderMonitorLicensingComponent
            },
            {
                path: 'provider-cap-notification',
                loadChildren: './provider-cap-notification/provider-cap-notification.module#ProviderCapNotificationModule'
            },
            {
                path: 'provider-sanctions',
                loadChildren: './provider-sanctions/provider-sanctions.module#ProviderSanctionsModule'
            },
            ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewProviderRoutingModule {}
