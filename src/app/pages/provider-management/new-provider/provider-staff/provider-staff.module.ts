import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderStaffRoutingModule } from './provider-staff-routing.module';
import { ProviderStaffComponent } from './provider-staff.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { MatButtonToggleModule, MatSlideToggleModule, MatStepperModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    ProviderStaffRoutingModule,
    FormMaterialModule
  ],
  declarations: [ProviderStaffComponent]
})
export class ProviderStaffModule { }
