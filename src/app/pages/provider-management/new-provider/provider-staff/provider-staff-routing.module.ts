import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderStaffComponent } from './provider-staff.component';

const routes: Routes = [
  {
    path: '' ,
    component: ProviderStaffComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderStaffRoutingModule { }
