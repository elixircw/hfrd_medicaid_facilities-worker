import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

// import { ApplicantStaffDetails } from '../_entities/newApplicantModel';
import { AlertService } from '../../../../@core/services/alert.service';
import { ContactDetailsModule } from '../../../person-details/contact-details/contact-details.module';
@Component({
  selector: 'provider-staff',
  templateUrl: './provider-staff.component.html',
  styleUrls: ['./provider-staff.component.scss']
})
export class ProviderStaffComponent implements OnInit {

  // staffArray : ApplicantStaffDetails[] = [];
  providerId: string;
  staffForm: FormGroup;
  affiliationType = [];
  employeeType = [];
  reasonForLeaving = [];
  rccCertificateType = [];
  showCertification: boolean;
  currentSelectedProviderStaffId: string;
  staffInformation = [];
  isEditStaff: boolean;
  isEndDate: boolean;
  currenbtStaffInfo = [];

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute) {
      this.providerId = route.snapshot.parent.parent.params['id'];
    }

  ngOnInit() {
    this.initializeStaffForm();
    this.getStaffInformation();
    this.affiliationType = ['Employee', 'Board Member', 'Volunteer', 'Intern Information', 'Temp Employee'];
    this.employeeType = ['Program Administrator', 'Residential Child and youth care practitioner', 'Other Employees' , 'N/A'];
    this.reasonForLeaving = ['Resigned', 'Retired', 'Death', 'Terminated'];
    this.rccCertificateType = ['Certified Program Administrator', 'Residential Child and youth care practitioner certification'];
    this.showCertification = false;
    this.isEditStaff = false;
    this.isEndDate = false;
    this.providerId = this.providerId;
  }

  private initializeStaffForm() {
    this.staffForm = this.formBuilder.group({
      provider_id: this.providerId,
      employee_first_nm: [''],
      employee_last_nm: [''],
      affiliation_type: [''],
      job_title: [''],
      employee_type: null,
      personnel_start_date: null,
      personnel_end_date: null,
      reason_for_leaving: [''],
      behavioral_interventions_training: [''],
      cps_clearance_request_date: null,
      cps_clearance_result_date: null,
      cps_rcc_compliant: [''],
      cps_cpa_compliant: [''],
      federal_clearance_request_date: null,
      federal_clearance_result_date: null,
      federal_rcc_compliant: [''],
      federal_cpa_compliant: [''],
      state_clearance_request_date: null,
      state_clearance_result_date: null,
      state_rcc_compliant: [''],
      state_cpa_compliant: [''],
      rcc_certificate_type: [''],
      rcc_certificate_due_date: null,
      rcc_date_application_mailed: null,
      rcc_date_tested: null,
      rcc_certification_effective_date: null,
      rcc_certification_number: [''],
      rcc_certification_renewal_date: null,
      comments: [''],
    });
  }


  createProviderStaff() {
    console.log(this.staffForm.value);
    this._commonHttpService.create(
      this.staffForm.value,
      'providerstaff'
    ).subscribe(
      (response) => {
                this._alertService.success('Information saved, Please add clearance info!');
        this.currentSelectedProviderStaffId = response.provider_staff_id;
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
  }


  patchProviderStaff() {
    this._commonHttpService.patch(
      this.currentSelectedProviderStaffId,
      this.staffForm.value,
      'providerstaff'
    ).subscribe(
      (response) => {
        this.getStaffInformation();
        this.resetForm();
        this._alertService.success('Information saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
    (<any>$('#add-staff')).modal('hide');
  }

   getStaffInformation() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : {object_id : this.providerId},
        order : 'provider_staff_id desc'
      },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.staffInformation = response;
        console.log('Rahul get Staff info' + this.staffInformation);
        // this.staffForm.patchValue(response);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  editStaffForm(editStaff) {
    console.log(editStaff);
    this.currentSelectedProviderStaffId = editStaff.provider_staff_id;
    (<any>$('#add-staff')).modal('show');
    this.isEditStaff = true;
    this.staffForm.patchValue({
        provider_id: this.providerId,
        employee_first_nm: editStaff.employee_first_nm,
        employee_last_nm: editStaff.employee_last_nm,
        affiliation_type: editStaff.affiliation_type,
        job_title: editStaff.job_title,
        employee_type: editStaff.employee_type,
        personnel_start_date: editStaff.personnel_start_date,
        personnel_end_date: editStaff.personnel_end_date,
        reason_for_leaving: editStaff.reason_for_leaving,
        behavioral_interventions_training: editStaff.behavioral_interventions_training,
        cps_clearance_request_date: editStaff.cps_clearance_request_date,
        cps_clearance_result_date: editStaff.cps_clearance_result_date,
        cps_rcc_compliant: editStaff.cps_rcc_compliant,
        cps_cpa_compliant: editStaff.cps_cpa_compliant,
        federal_clearance_request_date: editStaff.federal_clearance_request_date,
        federal_clearance_result_date: editStaff.federal_clearance_result_date,
        federal_rcc_compliant: editStaff.federal_rcc_compliant,
        federal_cpa_compliant: editStaff.federal_cpa_compliant,
        state_clearance_request_date: editStaff.state_clearance_request_date,
        state_clearance_result_date: editStaff.state_clearance_result_date,
        state_rcc_compliant: editStaff.state_rcc_compliant,
        state_cpa_compliant: editStaff.state_cpa_compliant,
        rcc_certificate_type: editStaff.rcc_certificate_type,
        rcc_certificate_due_date: editStaff.rcc_certificate_due_date,
        rcc_date_application_mailed: editStaff.rcc_date_application_mailed,
        rcc_date_tested: editStaff.rcc_date_tested,
        rcc_certification_effective_date: editStaff.rcc_certification_effective_date,
        rcc_certification_number: editStaff.rcc_certification_number,
        rcc_certification_renewal_date: editStaff.rcc_certification_renewal_date,
        comments: editStaff.comments
    });
  }
  isEmployeeType(employeeType) {
    if (employeeType === 'Residential Child and youth care practitioner' || 'Program Administrator' ) {
      this.showCertification = true;
    } else if (employeeType === 'N/A') {
      this.showCertification = false;
    }
  }
  deleteStaffForm(staffInfo) {
    this.currentSelectedProviderStaffId = staffInfo.provider_staff_id;
    (<any>$('#delet-staff')).modal('show');
  }
  closeDeleteModal() {
    (<any>$('#delet-staff')).modal('hide');

  }
  confirmDelete() {
    this._commonHttpService.remove(
      this.currentSelectedProviderStaffId,
      {
        where: {provider_staff_id: this.currentSelectedProviderStaffId}
      },
      'providerstaff').subscribe(
      (response) => {
        this.getStaffInformation();
      },
      (error) => {
        this._alertService.error('Unable to delete Staff');
        return false;
      }
    );
  (<any>$('#delet-staff')).modal('hide');
  }
  resetForm() {
    this.staffForm.reset();
    this.initializeStaffForm();
  }
  openAddStaffMembers() {
    this.isEditStaff = false;
    this.showCertification = false;
    this.isEndDate = false;

  }
  personnelEndDate() {
      this.isEndDate = true;
    }
}
