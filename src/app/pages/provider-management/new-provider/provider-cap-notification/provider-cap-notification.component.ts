import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DynamicObject, PaginationInfo } from '../../../../@core/entities/common.entities';
import { AlertService } from '../../../../@core/services/alert.service';
import { Subject } from 'rxjs/Subject';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { ApplicantUrlConfig } from '../../../provider-portal-temp/provider-portal-temp-url.config';
import { ProviderAuthorizationService } from '../../../providers/provider-authorization.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../@core/services/auth.service';
import { CapConstants } from '../constants';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { Subscription } from 'rxjs/Subscription';
import { RouteUserService } from '../../../providers/route-users/route-user.service';
import { AppConstants } from '../../../../@core/common/constants';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';
import * as moment from 'moment';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';


@Component({
  selector: 'provider-cap-notification',
  templateUrl: './provider-cap-notification.component.html',
  styleUrls: ['./provider-cap-notification.component.scss']
})
export class ProviderCapNotificationComponent implements OnInit {



  deficiencyForm: FormGroup;
  capForm: FormGroup;
  defciencyOrViloations = [];
  citation: string;
  comments: string;
  defciencyFrequency = [];
  defciencyImpacts = [];
  defciencyScopes = [];
  findings: string;
  providerId: string;
  complaintList = [];
  totalRecords: number;
  narrativeInfo: any;
  pageStream$ =  new Subject<number>();
  userAssingmentSubscription: Subscription;
  paginationInfo: PaginationInfo = new PaginationInfo();
  previousPage: number;
  dynamicObject: DynamicObject = {};
  dashboardService: any;
  status: any;
  timestamp: Date;
  search_complaint_number: any;
  currentComplaint: any;
  selectedDeficiency: any;
  id: any;
  decisons: any[] = [];
  roleName: string;
  userProfile: AppUser;
  assignsecurityuserid: any[] = [];
  complaintNumber: any;
  signature: any;
  isViewOnly: any;
  agencyList: any[] = [];
  userType:  string;
  showSignature = true;
  activeTab: any;
  uploadedfile: any[] = [];
  pageInfo: PaginationInfo = new PaginationInfo();
  totalCount: any;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private authService: AuthService,
    private _routeUserService: RouteUserService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService) {
      this.id = route.snapshot.params['id'];
      this.userProfile = this.authService.getCurrentUser();
      this.roleName = (this.userProfile && this.userProfile.role) ? this.userProfile.role.name : null;
     }

  ngOnInit() {

    this.initializeDeficiencyForm();
    this.initiateCapForm();
    this.activeTab = 'pending';
    this.defciencyOrViloations = ['COMAR Citation', 'Out of State regulations', 'Provider Policy'];
    this.citation = null;
    this.comments = null;
    this.timestamp = new Date();
    this.defciencyFrequency = ['Initial', 'Repeat', 'Chronic'];
    this.defciencyImpacts = ['Minor', 'Major', 'Serious'];
    this.defciencyScopes = ['Isolated', 'Narrow', 'Moderate', 'Broad'];
    this.agencyList = ['DJS', 'DHS', 'MDH', 'Other'];
    this.providerId =  this.route.snapshot.parent.parent.params['id'];
    this.providerComplaints(1);
  }

  downloadFile(s3bucketpathname) {
    const workEnv = config.workEnvironment;
    let downldSrcURL;
    if (workEnv === 'state') {
        downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
    } else {
        // 4200
        downldSrcURL = s3bucketpathname;
    }
    console.log('this.downloadSrc', downldSrcURL);
    window.open(downldSrcURL, '_blank');
  }

  private validateDecision(status) {
      this.userType = this._authService.getAgencyName();
      if (this.userType === 'OLMDJS') {
        this.agencyList = this.agencyList.filter(item => {
          if (item !== 'DJS') {
            return item;
          }
        });
        if ( status === CapConstants.CAP_STATUS_CODE.cap_submitted || status === CapConstants.CAP_STATUS_CODE.cap_review_rejected || status === CapConstants.CAP_STATUS_CODE.cap_approval_rejected
        ) {
          if (this.roleName === AppConstants.ROLES.PROVIDER_DJS_QA ) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if ( status === CapConstants.CAP_STATUS_CODE.cap_accepted || status === CapConstants.CAP_STATUS_CODE.cap_approval_rejected) {
          if (this.roleName === AppConstants.ROLES.PROVIDER_DJS_RESOURCE ) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if ( status === CapConstants.CAP_STATUS_CODE.cap_reviewed) {
          if (this.roleName === AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if (status === CapConstants.CAP_STATUS_CODE.cap_approved ) {
          this.isViewOnly = true;
        }
      } else {
        this.agencyList = this.agencyList.filter(item => {
          if (item !== 'DHS') {
            return item;
          }
        });
        if ( status === CapConstants.CAP_STATUS_CODE.cap_submitted || status === CapConstants.CAP_STATUS_CODE.cap_review_rejected || status === CapConstants.CAP_STATUS.cap_approval_rejected) {
          if (this.roleName === AppConstants.ROLES.QUALITY_ASSURANCE) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if ( status === CapConstants.CAP_STATUS_CODE.cap_accepted || status === CapConstants.CAP_STATUS_CODE.cap_approval_rejected) {
          if (this.roleName === AppConstants.ROLES.PROGRAM_MANAGER) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if (status === CapConstants.CAP_STATUS_CODE.cap_reviewed) {
          if (this.roleName === AppConstants.ROLES.EXECUTIVE_DIRECTOR) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if (status === CapConstants.CAP_STATUS_CODE.cap_approved) {
          this.isViewOnly = true;
        }
      }
  }

  private initializeDeficiencyForm() {
    this.deficiencyForm = this.formBuilder.group({

      deficiency_violationname: [' '],
      provider_complaintid: null,
      citation: null,
      comments: null,
      frequencyofdeficiency: [''],
      impactofdeficiency: [''],
      scopeofdeficiency: [''],
      findings: null,

    });
  }

  getCapDecisions(status: string) {
    if (this.isViewOnly) {
      return CapConstants.CAP_REVIEW_DECISONS;
    } else {
    return CapConstants.CAP_REVIEW_DECISONS.filter(tab => {
      if (tab.roles.indexOf(this.roleName) !== -1 && tab.statuses.indexOf(status) !== -1) {
        return true;
      } else {
        return false;
      }
    });
   }
  }

  initiateCapForm() {
    this.capForm = this.formBuilder.group({
      signimage: null,
      date: null,
      planofcorrection: null,
      commenttext: null,
      findings: null,
      disbutecomarcitation: null,
      isCapAccepted: null,
      decision: null
    });
    this.capForm.get('planofcorrection').disable();
    this.capForm.get('disbutecomarcitation').disable();
  }


  resetForm() {
    this.deficiencyForm.reset();
  }

  resetCapForm() {
    this.capForm.reset();
    this.selectedDeficiency = null;
    (<any>$('#add-cap')).modal('hide');
    this.signature = null;
  }

  addDeficiency(complaint) {
    this.currentComplaint = complaint;
    this.complaintNumber = complaint.complaint_number;
    this.reset();
    this.deficiencyForm.patchValue({provider_complaintid: complaint.provider_complaintid});
    (<any>$('#add-deficiency')).modal('show');
  }


  public addUpdate() {
    this._commonHttpService.getArrayList({}, 'Nextnumbers/getNextNumber?apptype=CAP').subscribe(result => {
      const data = this.deficiencyForm.getRawValue();
      data.complaint_number = this.complaintNumber;
      data.cap_number = result['nextNumber'];
    (<any>$('#add-deficiency')).modal('hide');
    console.log(this.deficiencyForm.value);
    this._commonHttpService.create(
      data,
      'tb_provider_complaint_deficiency/addupdate'
    ).subscribe(
      (response) => {
        console.log(response);
        this._alertService.success('Information saved');
        this.loadDeficiencyList(this.currentComplaint, true, this.activeTab);
      },
      (error) => {
        console.log(error);
        this._alertService.error('Unable to save information');
      }
    );
    });
  }

  reset() {
    this.deficiencyForm.reset();
  }


  showNarrative(narrative: string) {
    this.narrativeInfo = narrative ? narrative : 'No narrative info found!';
  }

  public providerComplaints(selectPageNumber: number) {
    const pageSource = this.pageStream$.map(pageNumber => {
      if (this.paginationInfo.pageNumber !== 1) {
        this.previousPage = pageNumber;
      } else {
        this.previousPage = this.paginationInfo.pageNumber;
      }
      return { search: this.dynamicObject, page: this.previousPage };
    });
    return this.getComplaintList(this.paginationInfo.pageSize, this.previousPage, {  provider_id: this.providerId, status: this.activeTab, pagenumber: this.paginationInfo.pageNumber,
    pagelimit: this.paginationInfo.pageSize}).subscribe(result => {
      if (result.data && result.data.length) {
        this.complaintList = result.data;
        this.totalRecords = result.data[0].totalcount ? result.data[0].totalcount : 0;
      }
    });

  }

  loadDeficiencyList(item: any, reload: boolean, status: any) {
      if (item.deficiencyList === undefined || reload) {
        this.currentComplaint = item;
    const searchCritera = { provider_complaintid: item.provider_complaintid, provider_id: null, status: status, cap_number: null,
    sortorder: this.pageInfo.sortBy, sortcolumn: this.pageInfo.sortColumn };
     this._commonHttpService.getPagedArrayList(
        new PaginationRequest({
          limit: 10,
          page: this.pageInfo.pageNumber,
          method: 'get',
          where: searchCritera
        }),
        'tb_provider_complaint_deficiency/list?filter'
        ).subscribe(response => {
          if (response.data && response.data.length) {
            item.deficiencyList = response.data;
            this.totalCount = response.data[0].totalcount ? response.data[0].totalcount: 0;
          }
      });
    }
  }

  onCapSorted($event: ColumnSortedEvent) {
    this.pageInfo.sortBy = $event.sortDirection;
    this.pageInfo.sortColumn = $event.sortColumn;
    this.loadDeficiencyList(this.currentComplaint, true, this.activeTab);
  }


  public getComplaintList(pageSize: number, pageNumber: number, searchCritera: any) {
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: pageSize,
        page: pageNumber,
        method: 'post',
        where: searchCritera
      }),
      'tb_provider_complaint/providercomplaints'
    );
  }

  deficiencyChanged(page: any) {
    this.pageInfo.pageNumber = page;
    this.loadDeficiencyList(this.currentComplaint, true, this.activeTab);
  }
  pageChanged(page: any) {
    this.paginationInfo.pageNumber = page;
    this.providerComplaints(this.paginationInfo.pageNumber);
  }

  toggleTable(id, index, item) {
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle');
    (<any>$('.provider-details tr')).removeClass('selected-bg');
    (<any>$(`#provider-details-${index}`)).addClass('selected-bg');
    this.loadDeficiencyList(item, true,  this.activeTab);
  }

  sendDeficiency(complaint_no) {
    this._commonHttpService.getArrayList({
        complaint_number: complaint_no,
        method: 'post'
    },
      'tb_provider_complaint_deficiency/sendnotification'
    ).subscribe(
      (response) => {
        console.log(response);
        this._alertService.success('Cap Notification has been sent!');
      });
  }

  setActiveTab(tab) {
    this.activeTab = tab;
    this.pageInfo.pageNumber = 1;
    this.paginationInfo.pageNumber = 1;
    this.providerComplaints(this.previousPage);
  }

  openCapForm(deficiency) {
    this.showSignature = false;
    setTimeout(() => { this.showSignature = true }, 200);
    this.signature = null;
    this.selectedDeficiency = deficiency;
    const status = this.selectedDeficiency.cap_status;
    // let status = 'cap_submitted';
    this.validateDecision(status);
    this.capForm.patchValue(deficiency);
    const cap_status = CapConstants.CAP_REVIEW_DECISONS.filter(tab => {
      if (status === CapConstants.CAP_STATUS_CODE[tab.eventcode] ) {
        return true;
      } else {
        return false;
      }

    });
    this.capForm.patchValue({commenttext : deficiency.comments, decision: ((cap_status && cap_status.length) ? cap_status[0].eventcode : null )});
    if (this.roleName && ( this.roleName === 'Quality Assurance' || this.roleName === 'Provider_DJS_QA')) {
      this.signature = deficiency.qa_signimage;
    } else if (this.roleName && ( this.roleName === 'Program Manager' || this.roleName === 'Provider_DJSResource')) {
      this.signature = deficiency.pm_signimage;
    } else if (this.roleName && ( this.roleName === 'Executive Director' || this.roleName === 'Provider_DJS_SecretaryDesignee')) {
      this.signature = deficiency.ed_signimage;
    }
    this.decisons = this.getCapDecisions(status);
    this.uploadedfile = deficiency.uploadedfile ? deficiency.uploadedfile : [];
    console.log(this.uploadedfile);
  }

  getStatusCode(decision) {
      return CapConstants.CAP_STATUS_CODE[decision];
  }



  saveCap(action) {
    const capdata = this.capForm.getRawValue();
    capdata.status = this.getStatusCode(capdata.decision);
    capdata.statusmsg = capdata.decision;
    capdata.appevent = 'PRCAP';
    capdata.providerid = this.id;
    capdata.notification = CapConstants.CAP_STATUS_NOTIFICATION[capdata.decision] + '( Cap Number :' +  this.selectedDeficiency.cap_number + ' )' ;
    capdata.date = new Date();
    capdata.assignsecurityuserid = this.assignsecurityuserid;
    capdata.objectid = this.selectedDeficiency.cap_number;
    capdata.routingdescription = CapConstants.CAP_STATUS_DESCRIPTION[capdata.decision];


    if (this.roleName && ( this.roleName === 'Quality Assurance' || this.roleName === 'Provider_DJS_QA')) {
      capdata.qa_signimage = capdata.signimage;
      capdata.qa_signdate = capdata.date;
    } else if (this.roleName && ( this.roleName === 'Program Manager' || this.roleName === 'Provider_DJSResource')) {
      capdata.pm_signimage = capdata.signimage;
      capdata.pm_signdate = capdata.date;
    } else if (this.roleName && ( this.roleName === 'Executive Director' || this.roleName === 'Provider_DJS_SecretaryDesignee')) {
      capdata.ed_signimage = capdata.signimage;
      capdata.ed_signdate = capdata.date;
    }
    const decision = {
      capdata
    };
    if (action === 'submit') {
    this._commonHttpService.create(decision, ApplicantUrlConfig.EndPoint.Cap.SubmitCap).subscribe(
      (response) => {
        console.log(response);
        this._alertService.success(CapConstants.CAP_STATUS_DESCRIPTION[capdata.decision]);
        this.resetCapForm();
        this.providerComplaints(1);
      },
      (error) => {
        console.log(error);
        this._alertService.error('Unable to save information');
      }
    );
  }  else {
    this.resetCapForm();
  }
 }


 listenOnUserAssignment() {
  this.userAssingmentSubscription = this._routeUserService.selectedUserOnAssign$.subscribe(user => {
    if (user) {

      this.assignsecurityuserid = [];
      if (user && user.userid) {
        this.assignsecurityuserid.push(user.userid);
      } else if (user && user.assigneduserid.length) {
        user.assigneduserid.forEach(item => {
          this.assignsecurityuserid.push(item.userid);
        });
      }
      this.saveCap('submit');
    }
  });
}

 openRouteUser() {
   if (this.capForm.valid) {
    const capdata = this.capForm.getRawValue();
    // this.listenOnUserAssignment();
    if (this.roleName && ( this.roleName === 'Executive Director' || this.roleName === 'Provider_DJS_SecretaryDesignee')) {
      this.assignsecurityuserid = null;
      this.saveCap('submit');
    } else if ( capdata.decision === 'cap_submit_rejected'
    || capdata.decision === 'cap_review_rejected'
    || capdata.decision === 'cap_approval_rejected'
    ) {
      this.assignsecurityuserid = null;
      this.saveCap('submit');
    } else {
    const capDecision = {       status : capdata.decision,
      // agency: ,
      agencyName: '' };
    this._dataStoreService.setData('ComplaintDecisions', capDecision);
    (<any>$('#add-cap')).modal('hide');
    this.listenOnUserAssignment();
    this._router.navigate(['assign-user'], { relativeTo: this.route });
    }
   } else {
     this._alertService.error('Please fill the mandatory fields');
   }
 }


 documentGenerate() {

  if (this.selectedDeficiency.cap_status) {
  const modal = {
    where : {
      cap_number: this.selectedDeficiency.cap_number,
      provider_id: null
    },
    method: 'post'

  };
  this._commonHttpService.download('tb_provider_complaint_deficiency/downloadcapdocument', modal)
.subscribe(res => {
  const blob = new Blob([new Uint8Array(res)]);
  const link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  const timestamp = moment(this.timestamp).format('MM/DD/YYYY HH:mm:ss');
  link.download = 'CAP ResponseDocument-' + this.selectedDeficiency.cap_number + '.' + timestamp + '.pdf';
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
});
  } else {
    this._alertService.success('CAP Response Document can be generated only after the form has been submitted.');
  }
}


 ngOnDestroy() {
  if (this.userAssingmentSubscription)
    this.userAssingmentSubscription.unsubscribe();
}

 }
