import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderCapNotificationComponent } from './provider-cap-notification.component';

describe('ProviderCapNotificationComponent', () => {
  let component: ProviderCapNotificationComponent;
  let fixture: ComponentFixture<ProviderCapNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderCapNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderCapNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
