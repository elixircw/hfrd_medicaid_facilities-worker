import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProviderCapNotificationComponent } from './provider-cap-notification.component' ;
import { ProviderCapNotificationRoutingModule } from './provider-cap-notification-routing.module';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { MatButtonToggleModule, MatSlideToggleModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProviderStaffRoutingModule } from '../provider-staff/provider-staff-routing.module';
import { PaginationModule } from 'ngx-bootstrap';
import { RouteUsersModule } from '../../../providers/route-users/route-users.module';
@NgModule({
  imports: [
    CommonModule,
    ProviderCapNotificationRoutingModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    FormMaterialModule,
    PaginationModule,
    RouteUsersModule
    
  ],
  declarations: [ProviderCapNotificationComponent]
})
export class ProviderCapNotificationModule { }
