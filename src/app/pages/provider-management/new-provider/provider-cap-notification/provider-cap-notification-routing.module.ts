import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderCapNotificationComponent} from './provider-cap-notification.component'
import { RouteUsersComponent } from '../../../providers/route-users/route-users.component';

const routes: Routes = [
  {
    path: '' ,
    component: ProviderCapNotificationComponent,
    children: [{
      path: 'assign-user',
      component: RouteUsersComponent
    },]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderCapNotificationRoutingModule { }
