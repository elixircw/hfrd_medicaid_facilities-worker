import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderMonitorLicensingComponent } from './provider-monitor-licensing.component';

describe('ProviderMonitorLicensingComponent', () => {
  let component: ProviderMonitorLicensingComponent;
  let fixture: ComponentFixture<ProviderMonitorLicensingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderMonitorLicensingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderMonitorLicensingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
