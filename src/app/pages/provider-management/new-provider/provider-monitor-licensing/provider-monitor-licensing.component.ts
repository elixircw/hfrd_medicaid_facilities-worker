import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { AddChecklistComponent } from '../add-checklist/add-checklist.component';

import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
// import { ApplicantUrlConfig } from '../../provider-applicant-url.config';
import { Subject } from 'rxjs/Subject';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { ProviderStoreConstants } from '../../provider-constants';
import { ReferralUrlConfig } from '../../../provider-referral/provider-referral-url.config';
@Component({
  selector: 'provider-monitor-licensing',
  templateUrl: './provider-monitor-licensing.component.html',
  styleUrls: ['./provider-monitor-licensing.component.scss']
})
export class ProviderMonitorLicensingComponent implements OnInit {

  id: string;

  @ViewChild('addCheckList', { read: ViewContainerRef }) container: ViewContainerRef;
  config = { panels: [] };
  tmpAccordionKey = [];
  activityCompleteStatus = 'Completed';
  visitationList: any[] = [];
  programList: any[] = [];
  compliants: any;
  isProvider: boolean = false;
  token: any;

  constructor(private _cfr: ComponentFactoryResolver, private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private _alertService: AlertService,
    private _datastore: DataStoreService, private _router: Router,
    private _authService: AuthService,) {
    this.id = route.snapshot.parent.params['id'];
    if(!this.id) {
      this.id = this._datastore.getData('ProviderPortalID');
    }
    this.token = this._authService.getCurrentUser();
    if (this.token && this.token.role) {
      this.isProvider = this.token.role.name ===  'Provider_Staff_Admin' ? true : false;
    }
   
  }

  monitorForm: FormGroup;
  visitationForm: FormGroup;
  isMonthDisabled = true;
  isYearDisabled = true;
  activityTaskStatusFormGroup: FormGroup;
  tasktypestatus = [];
  taskList = [];
  @Input()
  licenseLevelInputForMonitoring = new Subject<string>();
  licenseLevel: string;


  periodicDropDown = ['Initial', 'Monthly', 'Quarterly', 'Periodic', 'Mid-Year', 'Re-Licensure', 'Periodic', 'Special'];
  timeOfVisit = ['Night', 'Weekend', 'Regular'];
  visitSchedule = ['Announced', 'Unannounced'];
  jointinspectionlist = ['MDH', 'DJS', 'DHS', 'Not Jointly Inspected'];
  monthDropDown = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  yearDropDown = [];

  ngOnInit() {
    for (let i = 2000; i <= new Date().getFullYear(); ++i) {
      this.yearDropDown.push(i);
    }
    
    this.monitorForm = this.formBuilder.group({
      periodic: [''],
      monthly: [''],
      yearly: null
    });
    this.visitationForm = this.formBuilder.group({
      monitoring_id: [''],
      applicant_id: [''],
      site_id: [''],
      provider_id: [''],
      periodic: [''],
      monitoring_dt: [null],
      end_dt: [null],
      time_visit: [''],
      announced_cd: [''],
      inspectedwith: [''],
      prgram: [''],
      provider_complaintid: [null]
    });
    this.activityTaskStatusFormGroup = this.formBuilder.group({
      task: this.formBuilder.array([])
    });
    // this.getTaskList();
    this.licenseLevelInputForMonitoring.subscribe((data) => {
      this.licenseLevel = data;
    });
    this.tasktypestatus = ['Completed', 'Incomplete', 'N/A'];
    // this.getChecklist();
    this.getVisitationList();
    this.getProgramList();
    this.getComplaints();
  }

  download(monitoring_id) {

    const modal = {
      count: -1,
      where: {
        "monitoring_id": monitoring_id
      },
      method: 'post'
    };

    this._commonHttpService.download('prov_monitoring/downloaddocument', modal)
    .subscribe(
      (res) => {
        const blob = new Blob([new Uint8Array(res)]);
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = 'Monitoring_Notification_Report_'+monitoring_id+'.pdf';
       

        document.body.appendChild(link);

        link.click();

        document.body.removeChild(link);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
    
  }

  createTaskForm() {
    return this.formBuilder.group({
      checklist_id: [''],
      checklist_task: [''],
      commnts: [''],
      completeddate: [''],
      status: ['']
    });
  }

  private buildTaskForm(x): FormGroup {
    return this.formBuilder.group({
      checklist_id: x.checklist_id,
      checklist_task: x.checklist_task,
      commnts: x.commnts,
      completeddate: x.completeddate,
      status: x.status
    });
  }

  setFormValues() {
    // this.activityTaskStatusFormGroup.setControl('task', this.formBuilder.array([]));
    // const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    // this.taskList.forEach((x) => {
    //   control.push(this.buildTaskForm(x));
    // })
    // console.log(this.activityTaskStatusFormGroup.value);
    // console.log(this.activityTaskStatusFormGroup.controls);
    this.config.panels = [];
    this.tmpAccordionKey = [];
    const newObj = {};

    this.taskList.forEach((x) => {
      if (!newObj[x.subcategory]) {
        newObj[x.subcategory] = [x];
      } else {
        newObj[x.subcategory].push(x);
      }
    }
    );

    Object.keys(newObj).forEach((key, index) => {
      if (!this.tmpAccordionKey.includes(key)) {
        this.activityTaskStatusFormGroup.setControl('task' + index, this.formBuilder.array([]));
        this.config.panels.push({ name: key, description: this.activityCompleteStatus });
        this.tmpAccordionKey.push(key);
      }

      const control = <FormArray>this.activityTaskStatusFormGroup.controls['task' + index];

      newObj[key].forEach((x) => {
        control.push(this.buildTaskForm(x));
        if (x.status === 'INCOMPLETE' || x.status === 'Incomplete') {
          this.config.panels[index].description = 'Incomplete';
        }
      });
    });
  }

  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.monitorForm.controls['monthly'].setValue('');
    this.monitorForm.controls['yearly'].setValue('');
  }

  onChange() {
    this.resetDropDowns();
    if (this.monitorForm.value.periodic === 'Monthly') {
      this.isMonthDisabled = false;
      this.isYearDisabled = false;
    } else {
      this.isYearDisabled = false;
    }
  }

  getVisitationList() {
    const provider = this._datastore.getData(ProviderStoreConstants.PROVIDER_DETAILS);
    const obj = { where: { provider_id: this.id}, method: 'get' };
    this._commonHttpService.getArrayList(obj, 'prov_monitoring/list?filter').subscribe(data => {
      this.visitationList = data && data.length ? data[0].listmonitoring : [];
     });
  }

  addVisitation() {
    const obj = this.visitationForm.getRawValue();
    obj.provider_id = this.id;
    this._commonHttpService.create(obj, 'prov_monitoring/addupdate').subscribe(data => {
      this._alertService.success('Visitation added sucessfully');
      (<any>$('#visitation')).modal('hide');
      this.getVisitationList();
     });
  }

  navigatetomonitor(obj) {
    const currentUrl = '/pages/provider/monitoring/' + obj.monitoring_id;
    this._router.navigate([currentUrl]);
  }

  getProgramList() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        providertype: 'Private'
      },
      ReferralUrlConfig.EndPoint.Referral.listprogramnamesUrl
    ).subscribe(data => {
      this.programList = data;
     });
  }

  viewVisit(obj, action) {
    this.visitationForm.patchValue(obj);
    if (action) {
      this.visitationForm.enable();
    } else {
      this.visitationForm.disable();
    }
    (<any>$('#visitation')).modal('show');
  }

  onClose() {
    this.visitationForm.reset();
  }
  getComplaints() {
    this._commonHttpService.getArrayList({
      method: 'post',
      nolimit: true,
      count: -1,
      where: {
        'provider_id': this.id,
      },
    },
      'tb_provider_complaint/providercomplaints'
    ).subscribe(
      (response) => {
        if (response && response['data']) {
          this.compliants = response['data'];
        }
        console.log(response);
        // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }
}
