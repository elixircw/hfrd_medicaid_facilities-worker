import { Component, OnInit, Input } from '@angular/core';
import { AttachmentIntakes, GeneratedDocuments, applinfo, ProviderReferral } from './_entities/newApplicantModel';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { ApplicantUrlConfig } from '../provider-management-url.config';
import { AlertService } from '../../../@core/services/alert.service';
import { ReferralUrlConfig } from '../../provider-referral/provider-referral-url.config';
import { ExistingProviderComponent } from '../existing-provider/existing-provider.component';
import { RoutingUser } from '../../cjams-dashboard/_entities/dashBoard-datamodel';
import { AppUser } from '../../../@core/entities/authDataModel';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { AuthService } from '../../../@core/services';
import { LicenseInfo } from '../../provider-applicant/new-public-applicant/_entities/newApplicantModel';
import { AppConstants } from '../../../@core/common/constants';

@Component({
  selector: 'new-provider',
  templateUrl: './new-provider.component.html',
  styleUrls: ['./new-provider.component.scss']
})
export class NewProviderComponent implements OnInit {

  addAttachementSubject$ = new Subject<AttachmentIntakes[]>();
  generatedDocuments$ = new Subject<string[]>();
  generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
  generateDocumentInput$ = new Subject<GeneratedDocuments[]>();
  profileOutputSubject$ = new Subject<applinfo>();
  referralinfooutputsubject$ = new Subject<ProviderReferral>();
  currentProvider: any;
  currentProviderDetails: any;

  providerId: string;
  assignedInSection = [];

  isLengthGrater: boolean;
  currentData: any;
  selectedPerson: any;
  getUsersList: RoutingUser[];
  originalUserList: RoutingUser[];
  isSupervisor: boolean;
  asssignedDetails: any;
  currentUser: AppUser;
  userProfile: any;
  roleName: any;

  tabConfig = [
    {
      id: 'provider-licenses',
      title: 'Licenses Info',
      name: 'Licenses Info',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'provider-licenses'
    },
    {
      id: 'provider-monitoring',
      title: 'Monitoring',
      name: 'Monitoring',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'monitoring'
    },
    {
      id: 'provider-contract',
      title: 'Contract',
      name: 'Contract',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'provider-contract'
    },
    {
      id: 'add-document',
      title: 'Document',
      name: 'Document',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'add-document'
    },
    {
      id: 'attachment-narrative',
      title: 'Narrative',
      name: 'Narrative',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'attachment-narrative'
    },
    {
      id: 'provider-rate',
      title: 'Master Rates',
      name: 'Master Rates',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'provider-rate'
    },
    {
      id: 'provider-staff',
      title: 'Staff',
      name: 'Staff',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'provider-staff'
    },
    {
      id: 'provider-license-requests',
      title: 'Change Requests',
      name: 'Change Requests',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'provider-license-requests'
    },
    {
      id: 'provider-cap-notification',
      title: 'CAP Notification',
      name: 'CAP Notification',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'provider-cap-notification'
    },
    {
      id: 'provider-sanctions',
      title: 'Sanctions',
      name: 'Sanctions',
      // role: [AppConstants.ROLES.INTAKE_WORKER, AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
      route: 'provider-sanctions'
    }
  ];
  current_route: string;
  providerProfileInfo: any;

  constructor(private _commonHttpService: CommonHttpService, private _alertService: AlertService,
    private _router: Router, private _authService: AuthService,
    private route: ActivatedRoute
  ) {
    this.providerId = this.route.snapshot.params['id'];
    this.userProfile = this._authService.getCurrentUser();
    this.roleName = (this.userProfile && this.userProfile.role) ? this.userProfile.role.name : null;

    this.getProviderDetails();
  }

  ngOnInit() {
    this.loadTab(this.tabConfig[0]);
    this.current_route = 'provider-licenses';
    this.currentUser = this._authService.getCurrentUser();
    this.assignDetails();
    this.userNames();
    console.log(this.asssignedDetails);
    this.getProviderProfileInformation();
  }

  private assignDetails() {

    this.asssignedDetails = {
      eventcode: 'PAOW',
      fromsecurityusersid: '',
      tosecurityusersid: '',
      fromroleid: '',
      toroleid: '',
      objectid: this.providerId
    };
  }

  loadTab(tabName) {
    this._router.navigate([(tabName && tabName.route) ? tabName.route : ''], {
      relativeTo: this.route
    });
    this.current_route = tabName.route;
    // this._router.navigate(['./' + this.tabConfig[index][tabName]], { relativeTo: this.route });
  }

  private getProviderDetails() {
    this.currentProviderDetails = {};
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { 'provider_id': this.providerId }
      }, 'providerinfo?filter',
    ).subscribe(response => {
      this.currentProvider = response[0];
      console.log(this.currentProvider);
      let statusInfo;
      if (this.currentProvider.provider_status_cd == '1791') {
        statusInfo = 'Open';
      }
      if (this.currentProvider.provider_status_cd == '1792') {
        statusInfo = 'Closed';
      }
      if (this.currentProvider.provider_status_cd == '1793') {
        statusInfo = 'Pending';
      }
      this.currentProviderDetails.provider_nm = this.currentProvider.provider_nm;
      this.currentProviderDetails.taxid = this.currentProvider.tax_id_no;
      this.currentProviderDetails.adr_cell_phone_tx = this.currentProvider.adr_work_phone_tx;
      this.currentProviderDetails.adr_county_cd = this.currentProvider.county_cd;
      this.currentProviderDetails.provider_status = statusInfo;
    });
  }

  getProviderProfileInformation() {
    this._commonHttpService.create(
      {
        // Make this and api to take provider id instead of appid
        appid: this.providerId
      },
      'providerapplicantportal/getapplprofileinfo'
    ).subscribe(response => {
      this.providerProfileInfo = response.data;
      
    },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }



  private userNames() {
    this.assignedInSection = [];
    this._commonHttpService.create(
      {
        method: 'post',
        where: {
          eventcode: 'PAOW',
          objectid: this.providerId
        }
      },
      'providerassignmentownership/getassignownership'
    )
      .subscribe(
        (response) => {
          console.log("$$$$$$$$$$", response[0]);
          console.log("$$$$$$$$$$", response.length);
          if (response.length >= 2) {
            this.isLengthGrater = true
          };
          // console.log("Rahul before assigned in section"+this.assignedInSection);
          if (response && response.length) {
            for (var i = 0; i < response.length; i++) {
              this.currentData = { "username": response[i].ownername };
              this.assignedInSection.push(this.currentData);
              console.log("$$$$$$$$$$", this.assignedInSection);
            }
          }
          // this.assignedInSection=[{"username": response[0].ownername}];
          // this.assignedInSection=[{"username": response[1].ownername}];
        }, (error) => {
          this._alertService.error('Unable to fetch saved Applicant details.');
          console.log('Get applicant details Error', error);
          return false;
        }
      );
  }


  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
    this.getUsersList = this.getUsersList.filter(res => {
      if (res.issupervisor === false) {
        if ((this.roleName === AppConstants.ROLES.PROVIDER_DJS_QA &&
          res.userrole === AppConstants.ROLES.PROVIDER_DJS_QA) || (this.roleName === AppConstants.ROLES.QUALITY_ASSURANCE && res.userrole === AppConstants.ROLES.QUALITY_ASSURANCE)) {
          return res;
        } else if (this.roleName !== AppConstants.ROLES.PROVIDER_DJS_QA && this.roleName !== AppConstants.ROLES.QUALITY_ASSURANCE) {
          return res;
        }
      } else {
        if ((this.roleName === AppConstants.ROLES.PROVIDER_DJS_QA && res.userrole !== AppConstants.ROLES.PROVIDER_DJS_QA)) {
          return res;
        } else if ((this.roleName === AppConstants.ROLES.QUALITY_ASSURANCE && res.userrole !== AppConstants.ROLES.QUALITY_ASSURANCE)) {
          return res;
        }
      }
    });
    console.log('Role', this.roleName)
    console.log('User Listf', this.getUsersList);

  }


  getRoutingUser(modal) {
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PTA' },
          method: 'post'
        }),
        'providerassignmentownership/getroutingusers'
      )
      .subscribe(result => {
        // this.userNames();
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }


  selectPerson(row) {
    this.selectedPerson = row;
    // THIS IS THE PERSON LOGGED IN WHICH IS (FROM)
    this.asssignedDetails.fromsecurityusersid = this.currentUser.user.securityusersid;
    this.asssignedDetails.fromroleid = this.currentUser.role.name;
    console.log("@@@@@@@@@@@", this.currentUser);
    // THIS IS THE PERSON WE SELECTED ON THE SCREEN WHICH IS (TO)
    this.asssignedDetails.tosecurityusersid = row.userid;
    this.asssignedDetails.toroleid = row.userrole;
  }

  assignNewUser() {
    if (this.selectedPerson) {
      let payload = {};
      payload = this.asssignedDetails;
      this._commonHttpService.create(
        payload,
        'providerassignmentownership'
      ).subscribe(
        (response) => {
          this._alertService.success('Ownership assigned successfully!');
          this.userNames();
          (<any>$('#intake-caseassignnew')).modal('hide');
        },
        (error) => {
          this._alertService.error('Unable to save Ownership');
        });
    }
  }
}
