import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import {  AuthService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppConstants } from '../../../../@core/common/constants';
import { config } from '../../../../../environments/config';
import { AppConfig } from '../../../../app.config';
import { HttpHeaders } from '@angular/common/http';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import * as moment from 'moment';

@Component({
  selector: 'provider-sanctions',
  templateUrl: './provider-sanctions.component.html',
  styleUrls: ['./provider-sanctions.component.scss']
})
export class ProviderSanctionsComponent implements OnInit {
  providerId: any;
  sanctionsList: any[] = [];
  programTypeList: any[] = [];
  providerName: any;
  deficiencyList: any[] = [];
  sanctionForm: FormGroup;
  appealForm: FormGroup;
  selectedProgram: any;
  genderList: any[];
  token: any;
  showSanctionsForm = false;
  showAppealForm = false;
  isProvider = false;
  programList: any[];
  providerSanctionList: any[] = [];
  hearingStatus: any[] = [];
  Status: any[] = [];
  isView = false;
  uploadedFile: any = [];
  deleteAttachmentIndex: number;
  siteID: any;
  selectedPRogramDetails: any;
  documentList: any[];
  buttonTxt: string;
  sanctionId: any;
  sanctionNumber: any;
  providerAppealList: any[];
  appealId: any;
  formSection: boolean;
  deliveryMethod = ['FIRST CLASS MAIL', 'REGISTERED MAIL', 'HAND DELIVERY'];
  showDelivery: boolean;
  documentFilterList: any[];
  staffInformation: any[];
  workerSign: any;
  providerSign: any;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _router: Router, private route: ActivatedRoute,
    private _formbuilder: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _uploadService: NgxfUploaderService
  ) { }

  ngOnInit() {
    this.formSection = false;
    this.initiateForm();
    this.providerId =  this.route.snapshot.parent.parent.params['id'];
    this.getSanctionDetail();
    this.getGenderList();
    this.getProviderProgram();
    this.token = this._authService.getCurrentUser();
    if (this.token && this.token.role) {
         this.isProvider = this.token.role.name ===  'Provider_Staff_Admin' ? true : false;
    }
    this.hearingStatus = ['Denied', 'Postponed', 'Held'];
    this.Status = ['withdrawn', 'upHeld'];
    this.providerId = this.providerId ? this.providerId : this.route.snapshot.params['id'];
    this.getStaff();
  }

  initiateForm() {
    this.sanctionForm = this._formbuilder.group({
      license_sanction_type: null,
      suspensionstartdate: null,
      suspensionenddate: null,
      revocationdate: null,
      populationeffectivedate: null,
      populationenddate: null,
      populationlimitreason: '',
      limitation_gender: null,
      limitation_min_age: null,
      limitation_max_age: null,
      youthservelimitreason: '',
      limitation_reduced_youth_capacity: null,
      programservice: null,
      programservicestartdate: null,
      programserviceenddate: null,
      programservicelimitreason: '',
      youthserveeffectivedate: null,
      youthserveenddate: null,
      publicityenddate: null,
      publicityeffectivedate: null,
      publicitylimitreason: null,
      stafflimitreason: '',
      staffeffectivedate : null,
      assigned_staff_id: null,
      staffenddate: null,
      governingenddate: null,
      governinglimitreason: '',
      governingeffectivedate: null,
      adminenddate: null,
      adminlimitreason: '',
      admineffectivedate: null,
      adminname: null,
      agelimitenddt: null,
      agelimitreason: '',
      agelimiteffectivedt: null,
      placementlimitenddt: null,
      placementlimitreason: '',
      placementlimiteffectivedt: null,
      min_age: null,
      max_age: null,
      signdate: [new Date()],
      is_appeal: null,
      documentid: null,
      provider_sign: null,
      worker_sign: null,
      deliverymethod: null
    });

    this.appealForm = this._formbuilder.group({
      hearingdate: null,
      ishearinghappened: null,
      hearingstatus: null,
      narrative: null,
      attachment: null,
      nexthearingdate: null,
      status: null
    });
  }

  addAppeal(sanction) {
   this.formSection = true;
   this.sanctionId = this.selectedProgram === 'RCC' ? sanction.rcc_sanction_id : sanction.cpa_sanction_id;
   this.getAppealList();
  }

  resetAppeal(mode) {
    if (mode === 'add') {
      this.formSection = this.providerAppealList.length ? true : false;
      this.appealForm.reset();
      this.appealForm.enable();
      this.showAppealForm = false;
    } else {
      this.appealForm.reset();
      this.appealForm.enable();
      this.showAppealForm = false;
      this.formSection = false;
    }
  }

  hasAnyUploadInProgress() {
    if (this.uploadedFile.length === 0) {
        return false;
    }
    const inProgressAttachments = this.uploadedFile.filter(attachment => {
        if (attachment && attachment.hasOwnProperty('percentage') && attachment.percentage !== 100) {
            return true;
        } else {
            return false;
        }
    });

    if (inProgressAttachments.length > 0) {
        return true;
    }

    return false;
}

uploadFile(file: File | FileError): void {
  if (!(file instanceof Array)) {
      return;
  }
  file.map((item, index) => {
      const fileExt = item.name
          .toLowerCase()
          .split('.')
          .pop();
      if (
          fileExt === 'mp3' ||
          fileExt === 'ogg' ||
          fileExt === 'wav' ||
          fileExt === 'acc' ||
          fileExt === 'flac' ||
          fileExt === 'aiff' ||
          fileExt === 'mp4' ||
          fileExt === 'mov' ||
          fileExt === 'avi' ||
          fileExt === '3gp' ||
          fileExt === 'wmv' ||
          fileExt === 'mpeg-4' ||
          fileExt === 'pdf' ||
          fileExt === 'txt' ||
          fileExt === 'docx' ||
          fileExt === 'doc' ||
          fileExt === 'xls' ||
          fileExt === 'xlsx' ||
          fileExt === 'jpeg' ||
          fileExt === 'jpg' ||
          fileExt === 'png' ||
          fileExt === 'ppt' ||
          fileExt === 'pptx' ||
          fileExt === 'gif'
      ) {
          this.uploadedFile.push(item);
          const uindex = this.uploadedFile.length - 1;
          if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
              this.uploadedFile[uindex].percentage = 1;
          }

          this.uploadAttachment(uindex);
          const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
          const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
          if (audio_ext.indexOf(fileExt) >= 0) {
              this.uploadedFile[uindex].attachmenttypekey = 'Audio';
          } else if (video_ext.indexOf(fileExt) >= 0) {
              this.uploadedFile[uindex].attachmenttypekey = 'Video';
          } else {
              this.uploadedFile[uindex].attachmenttypekey = 'Document';
          }
      } else {
          // tslint:disable-next-line:quotemark
          this._alertService.error(fileExt + `format can't be uploaded`);
          return;
      }
  });
}
uploadAttachment(index) {
  console.log('check');
  const curr_timestamp = new Date();
  const workEnv = config.workEnvironment;
  let uploadUrl = '';
  if (workEnv === 'state') {
      uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
          + '?access_token=' + this.token.id + '&' + 'srno=' + curr_timestamp + '&' + 'docsInfo='; // Need to discuss about the docsInfo
      console.log('state', uploadUrl);
  } else {
      uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
          '&' + 'srno=' + curr_timestamp;
      console.log('local', uploadUrl);
  }

  this._uploadService
      .upload({
          url: uploadUrl,
          headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
          filesKey: ['file'],
          files: this.uploadedFile[index],
          process: true,
      })
      .subscribe(
          (response) => {
              if (response.status) {
                  this.uploadedFile[index].percentage = response.percent;
              }
              if (response.status === 1 && response.data) {
                  const doucumentInfo = response.data;
                  doucumentInfo.documentdate = doucumentInfo.date;
                  doucumentInfo.title = doucumentInfo.originalfilename;
                  doucumentInfo.objecttypekey = 'Appeal';
                  doucumentInfo.rootobjecttypekey = 'Appeal';
                  doucumentInfo.activeflag = 1;
                  doucumentInfo.servicerequestid = null;
                  this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                  console.log(index, this.uploadedFile[index]);
                  this._alertService.success('File Upload successful.');
              }
          }, (err) => {
              console.log(err);
              this._alertService.error('Upload failed due to Server error, please try again later.');
              this.uploadedFile.splice(index, 1);
          }
      );
}
deleteAttachment() {
  this.uploadedFile.splice(this.deleteAttachmentIndex, 1);
  (<any>$('#delete-attachment-popup')).modal('hide');
}

downloadFile(s3bucketpathname) {
  const workEnv = config.workEnvironment;
  let downldSrcURL;
  if (workEnv === 'state') {
      downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
  } else {
      // 4200
      downldSrcURL = s3bucketpathname;
  }
  console.log('this.downloadSrc', downldSrcURL);
  window.open(downldSrcURL, '_blank');
}

confirmDeleteAttachment(index: number) {
  (<any>$('#delete-attachment-popup')).modal('show');
  this.deleteAttachmentIndex = index;
}



  setRequriedValidtor(formControlName: string) {
    this.sanctionForm.get(formControlName).setValidators([Validators.required]);
    this.sanctionForm.get(formControlName).updateValueAndValidity();
  }
  clearValidators(formControlName: string) {
    this.sanctionForm.get(formControlName).clearValidators();
    this.sanctionForm.get(formControlName).updateValueAndValidity();
  }
  getSanctionDetail() {
    // prov_monitoring_exit_conf/getnoncompliance
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: 10,
        page: 1,
        method: 'get',
        where: {
          provider_id: +this.providerId
        }
      }), 'prov_monitoring_exit_conf/getnoncompliance?filter'
      ).subscribe( response => {
        if (response && response[0].getnoncompliance) {
          this.sanctionsList = response[0].getnoncompliance;
          this.providerName = response[0].getnoncompliance[0].provider_nm ? response[0].getnoncompliance[0].provider_nm : null;
        }

        this.programTypeList = Array.from(new Set( this.sanctionsList.map((item: any) => item.prgram)));

      });

  }

  toggleTable(id, index, program) {
    this.providerSanctionList = [];
    this.selectedProgram = program;
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle');
    (<any>$('.sanctions-details tr')).removeClass('selected-bg');
    (<any>$(`#sanctions-details-${index}`)).addClass('selected-bg');
    this.getSanctionList();
    this.getDocumentList();
  }
  viewSanction(sanction) {
    this.sanctionId = this.selectedProgram === 'RCC' ? sanction.rcc_sanction_id : sanction.cpa_sanction_id;
    this.sanctionNumber = sanction.rcc_sanction_no;
    this.buttonTxt = 'Acknowledge';
    this.isView = true;
    this.showSanctionsForm = true;
    this.documentSelection(sanction.documentid);
    this.sanctionForm.patchValue(sanction);
    this.sanctionForm.disable();
    if (this.isProvider) {
     // this.sanctionForm.disable();
      if (!sanction.isacknowledged) {
        this.isView = false;
        this.sanctionForm.get('provider_sign').enable();
        // if (!sanction.is_appeal) {
          this.sanctionForm.get('is_appeal').enable();
        // }
      }
      if (sanction.provider_sign) {
        this.providerSign = sanction.provider_sign;
      }
    } else {
      this.showDelivery = false;
      this.sanctionForm.get('documentid').enable();
      if (sanction.worker_sign) {
        this.workerSign = sanction.worker_sign;
      }
    }
    // this.isView = true;
  }

  appealCheck(checked) {
    if (checked) {
      (<any>$('#appeal-check')).modal('show');
    } else {
      this.buttonTxt = 'Acknowledge';
    }
  }

  appealConfirm(checked) {
    if (checked) {
      this.buttonTxt = 'Appeal';
    } else {
      this.buttonTxt = 'Acknowledge';
    }
  }

  resetSanction() {
    this.sanctionForm.reset();
    this.selectedProgram = null;
    this.showSanctionsForm = false;
    this.sanctionForm.enable();
    this.isView = false;
  }

  addSanction(program, providerid, providername) {
    this.sanctionForm.reset();
    this.isView = false;
    this.selectedProgram = program;
    this.selectedPRogramDetails = this.sanctionsList.find(item => item.prgram === program);
    this.showSanctionsForm = true;
    if (this.isProvider) {
     // this.sanctionForm.disable();
      this.sanctionForm.get('provider_sign').enable();
    } else {
      this.sanctionForm.enable();
    }
    this.getSanctionList();
    this.getDocumentList();
  }

  getGenderList() {
    this._commonHttpService.getArrayList(
      {
        where: { tablename: 'gender', teamtypekey: 'OLM' },
        method: 'get',
        nolimit: true
      },
      'referencetype/gettypes?filter'
    ).subscribe(result => {
      this.genderList = result;
      console.log('List of genders----->' , this.genderList);
    });
  }

  sanctionTypeChange(value) {
    if ( value === 's') {
      // this.suspenstion = true;
    }
  }

  addUpdate() {
    const sanctionForm = this.sanctionForm.getRawValue();
    let url;
    if (this.selectedProgram === 'RCC') {
      sanctionForm.rcc_sanction_id = this.sanctionId;
      url = 'tb_provider_rcc_sanctions/addupdate';
    } else if (this.selectedProgram === 'CPA') {
      sanctionForm.cpa_sanction_id = this.sanctionId;
      url = 'tb_provider_cpa_sanctions/addupdate';
    }
    sanctionForm.provider_id = +this.providerId;
    sanctionForm.site_id = this.selectedPRogramDetails ? this.selectedPRogramDetails.site_id : null;
    this._commonHttpService.create(sanctionForm, url).subscribe(response => {
      if (response) {
        this._commonHttpService.getArrayList({
          where: {
            v_provider_id: +this.providerId,
            lic_type: this.selectedProgram,
            isacknowledged: this.isProvider ? true : false
          },
          method: 'post'
        },
          'tb_provider_rcc_sanctions/sanctionnotification').subscribe( responseNotify => {
            if (responseNotify) {
              this._alertService.success('Notification has been sent');
            }
          });
        this._alertService.success('Sanction updated successfully');
        this.showSanctionsForm = false;
        this.getGenderList();
      }
    });
  }

  getProviderProgram() {
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        nolimit: true,
        method: 'get',
        where: { securityusersid: null, provider_id: +this.providerId}
      }),
      'tb_provider/providerapplicantsearch?filter'
    ).subscribe( response => {
      if (response && response.data) {
        this.programList = response.data;
      }
    });
  }

  getSanctionList() {
    let url;
    if (this.selectedProgram === 'RCC') {
      url = 'tb_provider_rcc_sanctions/list?filter';
    } else if (this.selectedProgram === 'CPA') {
      url = 'tb_provider_cpa_sanctions/list?filter';
    }
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        nolimit: true,
        method: 'get',
        where: { licensetype: this.selectedProgram, provider_id: +this.providerId}
      }),
      url
    ).subscribe( response => {
      if (response && response.data && response.data[0].listsanction) {
        this.providerSanctionList = response.data[0].listsanction;
        // this.sanctionForm.patchValue(this.providerSanctionList[0] ? this.providerSanctionList[0].listsanction[0] : null);
      }
    });
  }

  viewDeficiency(program) {
    this.deficiencyList = this.sanctionsList.filter(item => item.prgram === program);
    (<any>$('#deficiency-list')).modal('show');
  }

  getDocumentList() {
    let url;
    if (this.selectedProgram === 'CPA') {
      url = 'tb_provider_cpa_sanctions/documentlist?filter';
    } else {
      url = 'tb_provider_rcc_sanctions/documentlist?filter';
    }
    this._commonHttpService.getArrayList({
        nolimit: true,
        method: 'get',
        where: {}
      },
      url
    ).subscribe( response => {
      if (response && response.length) {
        this.documentFilterList = response;
        this.documentList = [...this.documentFilterList];
        if (this.selectedProgram === 'RCC') {
          const cpaList = ['CPA_RCC_HR', 'CPA_MORATORIUM', 'APPEAL_NOTICE', 'APPEAL_HEARING'];
          this.documentList =  this.documentList.filter(item => {
            return !cpaList.includes(item.ref_key);
          });
        } else if (this.selectedProgram === 'CPA') {
          const rccList = ['CPA_RCC_HR', 'CPA_MORATORIUM', 'APPEAL_NOTICE', 'APPEAL_HEARING'];
          this.documentList =  this.documentList.filter(item => {
            return !rccList.includes(item.ref_key);
          });
        }
      }
    });
  }

  documentSelection(documentkey) {
    const dockey = ['RCC_NOTICE', 'RCC_MORATORIUM', 'CPA_MORATORIUM'];
    if (dockey.includes(documentkey) ) {
      this.showDelivery = true;
    } else {
      this.showDelivery = false;
    }
  }

getAppealList() {
  let url;
  let data;
  if (this.selectedProgram === 'RCC') {
   
    url = 'tb_provider_rcc_appeal/list?filter';
    data = {
     licensetype: this.selectedProgram, rcc_sanction_id: this.sanctionId
    };
  } else if (this.selectedProgram === 'CPA') {
    url = 'tb_provider_cpa_appeal/list?filter';
    data = {
       licensetype: this.selectedProgram, cpa_sanction_id: this.sanctionId
    };
  }
  this._commonHttpService.getPagedArrayList(
    new PaginationRequest({
      nolimit: true,
      method: 'get',
      where: data
    }),
    url
  ).subscribe( response => {
    if (response && response.data && response.data[0].listappeal) {
      this.providerAppealList = response.data[0].listappeal;
      if (this.providerAppealList.length) {
        this.showAppealForm = false;
      } else {
        this.showAppealForm = true;
      }
      // this.sanctionForm.patchValue(this.providerSanctionList[0] ? this.providerSanctionList[0].listsanction[0] : null);
    }
  });
}

addNewAppeal() {
  this.showAppealForm = true;
}

addUpdateAppeal() {
  const appealForm = this.appealForm.getRawValue();
  let url;
  if (this.selectedProgram === 'RCC') {
    appealForm.rcc_sanction_id = this.sanctionId;
    appealForm.rcc_appeal_id = this.appealId ? this.appealId : null;
    url = 'tb_provider_rcc_appeal/addupdate';
  } else if (this.selectedProgram === 'CPA') {
    appealForm.cpa_sanction_id = this.sanctionId;
    appealForm.cap_appeal_id = this.appealId ? this.appealId : null;
    url = 'tb_provider_cpa_appeal/addupdate';
  }
  appealForm.uploadedfile = JSON.stringify(this.uploadedFile);
  appealForm.provider_id = +this.providerId;
  appealForm.site_id = this.selectedPRogramDetails ? this.selectedPRogramDetails.site_id : null;
  this._commonHttpService.create(appealForm, url).subscribe(response => {
    if (response) {
      this._alertService.success('Sanction updated successfully');
      this.showSanctionsForm = false;
      this.getAppealList();
    }
  });
}

viewAppeal(appeal, mode) {
  this.showAppealForm = true;
  this.uploadedFile = (appeal && appeal.uploadedfile) ? appeal.uploadedfile : [];
  this.appealForm.patchValue(appeal);
  this.appealForm.patchValue({
    ishearinghappened: appeal.ishearinghappened ? '1' : '0'
  });
  if (mode === 'edit') {
    if (this.selectedProgram === 'RCC') {
      this.appealId = appeal.rcc_appeal_id;
    } else {
      this.appealId = appeal.cpa_appeal_id;
    }
  } else {
    this.appealForm.disable();
  }
}

generateDocument(mode?) {
  this.generateMemoLetter();
  this.generateAppealHearing();
  if (mode === 'sanction') {
    this.generateSanctionLetter();
  }
}

generateAppealHearing() {
  const documentName = this.documentFilterList.find(item => item.ref_key === 'APPEAL_HEARING');
  const modal = {
    where : {
      provider_id: +this.providerId,
      sanction_no: this.sanctionNumber,
      licensetype: this.selectedProgram,
      documentid: 'APPEAL_HEARING'
    }
  };
  let url;
  if (this.selectedProgram === 'CPA') {
    url = 'tb_provider_cpa_sanctions/sanctionletter';
  } else {
    url = 'tb_provider_rcc_sanctions/sanctionletter';
  }
  if (this.sanctionNumber) {
    this._commonHttpService.download(url, modal)
    .subscribe(res => {
      const blob = new Blob([new Uint8Array(res)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      const timestamp = moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
      link.download = documentName.value_text + '-' + timestamp + '.pdf';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  } else {
  this._alertService.success('Document can be generated only after submitting Sanction, Please submit.');
  }
}

generateMemoLetter() {
  const documentName = this.documentFilterList.find(item => item.ref_key === 'APPEAL_NOTICE');
  const modal = {
    where : {
      provider_id: +this.providerId,
      sanction_no: this.sanctionNumber,
      licensetype: this.selectedProgram,
      documentid: 'APPEAL_NOTICE'
    }
  };
  let url;
  if (this.selectedProgram === 'CPA') {
    url = 'tb_provider_cpa_sanctions/sanctionletter';
  } else {
    url = 'tb_provider_rcc_sanctions/sanctionletter';
  }
  if (this.sanctionNumber) {
    this._commonHttpService.download(url, modal)
    .subscribe(res => {
      const blob = new Blob([new Uint8Array(res)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      const timestamp = moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
      link.download = documentName.value_text + '-' + timestamp + '.pdf';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  } else {
  this._alertService.success('Document can be generated only after submitting Sanction, Please submit.');
}
}

generateSanctionLetter() {
  const documentID = this.sanctionForm.get('documentid').value;
  const documentName = this.documentFilterList.find(item => item.ref_key === documentID);
  const modal = {
    where : {
      provider_id: +this.providerId,
      sanction_no: this.sanctionNumber,
      licensetype: this.selectedProgram,
      documentid: documentID
    }
  };
  let url;
  if (this.selectedProgram === 'CPA') {
    url = 'tb_provider_cpa_sanctions/sanctionletter';
  } else {
    url = 'tb_provider_rcc_sanctions/sanctionletter';
  }
  if (this.sanctionNumber) {
    this._commonHttpService.download(url, modal)
    .subscribe(res => {
      const blob = new Blob([new Uint8Array(res)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      const timestamp = moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
      link.download = documentName.value_text + '-' + timestamp + '.pdf';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  } else {
  this._alertService.success('Document can be generated only after submitting Sanction, Please submit.');
}
}

getStaff() {
  this._commonHttpService.getArrayList({
    method: 'get',
    nolimit: true,
    where: {
      object_id: this.providerId,
    },
  },
    'providerstaff?filter'
  ).subscribe(
    (response) => {
      this.staffInformation = response;
    }
  );
}

}

