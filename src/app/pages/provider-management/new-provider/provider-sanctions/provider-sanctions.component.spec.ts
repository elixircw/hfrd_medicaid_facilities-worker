import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderSanctionsComponent } from './provider-sanctions.component';

describe('ProviderSanctionsComponent', () => {
  let component: ProviderSanctionsComponent;
  let fixture: ComponentFixture<ProviderSanctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderSanctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderSanctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
