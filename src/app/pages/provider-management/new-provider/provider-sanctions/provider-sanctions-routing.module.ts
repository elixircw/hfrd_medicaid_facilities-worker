import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderSanctionsComponent } from './provider-sanctions.component';

const routes: Routes = [
  {
    path: '' ,
    component: ProviderSanctionsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderSanctionsRoutingModule { }
