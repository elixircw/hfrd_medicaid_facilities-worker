import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderSanctionsRoutingModule } from './provider-sanctions-routing.module';
import { ProviderSanctionsComponent } from './provider-sanctions.component';
import { PaginationModule } from 'ngx-bootstrap';
import { SortTableModule } from '../../../../shared/modules/sortable-table/sortable-table.module';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  imports: [
    CommonModule,
    ProviderSanctionsRoutingModule,
    PaginationModule,
    SortTableModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
  ],
  declarations: [ProviderSanctionsComponent]
})
export class ProviderSanctionsModule { }
