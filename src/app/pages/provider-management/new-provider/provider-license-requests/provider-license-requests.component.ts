import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService, CommonHttpService, AuthService, CommonDropdownsService } from '../../../../@core/services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { Observable } from 'rxjs/Observable';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';
import { HttpHeaders } from '@angular/common/http';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
@Component({
  selector: 'provider-license-requests',
  templateUrl: './provider-license-requests.component.html',
  styleUrls: ['./provider-license-requests.component.scss']
})
export class ProviderLicenseRequestsComponent implements OnInit {
  changeRequestForm: FormGroup;
  requestNarrativeForm: FormGroup;
  providerId: string;
  requestList=[];
  userProfile: any;
  requestType = [];
  availableSiteList: any[] = [];
  licenseInformation: any;
  selectedSiteLicenseInformation: any;
  selectedSiteId: string;
  selectedLicenseNo: string;
  tableData = [];
  existingLicenseInfo = [];
  isLicenseChange: boolean;
  hasFormSignature: Boolean = false;
  isWaiver: Boolean = false;
  isVariance: Boolean = false;
  isClousure = false;
  isLicenseExtend: boolean = false;
  stateList$: Observable<any[]>;
  variancetypeList: any = ['New variance request', 'Renewal of current variance'];
  waivertypeList: any = ['New waiver request', 'Renewal of current waiver'];
  genderList: any = ['Male', 'Female'];
  currentDate = new Date();
  expirationMinDate: any;
  effectiveMaxDate: any;
  viewForm: boolean;
  signature: any;
  uploadedFile: any = [];
  token: AppUser;
  roleName: any;
  deleteAttachmentIndex: number;
  changeRequestNo: any;
  document_id: any;
  isQA: boolean = false;
  constructor(private formBuilder: FormBuilder, 
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _authService: AuthService,
    private _uploadService: NgxfUploaderService,
    private _commonDropdownService: CommonDropdownsService) {
      this.providerId = route.snapshot.parent.parent.params['id'];
      this.userProfile = this._authService.getCurrentUser();
      this.roleName = (this.userProfile && this.userProfile.role) ? this.userProfile.role.name : null;
    }

  ngOnInit() {
    this.getChangeRequestList();
    this.initChangeRequestForm();
    this.initRequestNarrativeForm();
    this.getGenderList();
    this.getProviderContractLicenseTypes();
    this.loadDropDown();
    this.requestType = ['License Extend'];
    this.isLicenseChange = false;
    this.token = this._authService.getCurrentUser();
    if (this.roleName && ( this.roleName === 'Quality Assurance' || this.roleName === 'Provider_DJS_QA')) { 
      this.isQA = true;
    }
  }

  initRequestNarrativeForm() {
    this.requestNarrativeForm = this.formBuilder.group({
      request_no: [''],
      request_comments: [''],
      request_narrative_by: this.providerId
    });
  }


  initChangeRequestForm() {
    this.changeRequestForm = this.formBuilder.group({
      request_no: [''],
      provider_id: this.providerId,
      request_comments: [''],
      request_type: [''],
      license_no: [''],
      site_id: [''],

      min_age_to: [''],
      max_age_to: [''],
      gender_to: [''],
      children_no_to: [''],
      // min_age_from: [{value:'',disabled:true}],
      // max_age_from: [{value:'',disabled:true}],
      // gender_from: [{value:'',disabled:true}],
      // children_no_from: [{value:'',disabled:true}]

      min_age_from: [''],
      max_age_from: [''],
      gender_from: [''],
      children_no_from: [''],
      signimage: [''],
      program_address: [''],
      comar_citation: [''],
      program_phone: [''],
      program_fax: [''],
      variance_type: [''],
      waiver_type: [''],
      state: [''],
      city: [''],
      zip_no: [null],
      request_date: new Date(),
      completing_person_name: [''],
      person_name: [''],
      dob: [null],
      gender: [''],
      reason_for_request_variance: [''],
      reason_for_request_waiver: [''],
      alternate_measures: [''],
      request_effectivedate: new Date(),
      request_expirationdate: new Date(),
      continuous: [''],
      new_license_expire_date: [null],
      old_license_expire_date: [null]
    });
    this.changeRequestForm.get('license_no').disable();
  }

  openChangeRequest() {
    this.viewForm = false;
    this.changeRequestForm.enable();
    this.changeRequestForm.get('license_no').disable();
    this.changeRequestForm.reset();
    this.changeRequestForm.patchValue({
      provider_id: this.providerId
    });
    this._commonHttpService.getArrayList(
      {},
      'Nextnumbers/getNextNumber?apptype=providerportalrequest'
    ).subscribe(result => {
      //console.log('Request Number', result);
      this.changeRequestForm.patchValue({
        request_no: result['nextNumber']
      });
    });
    this.isLicenseChange = false;
    this.hasFormSignature = false;
    this.isVariance = false;
    this.isWaiver = false;
    this.isClousure = false;
    this.isLicenseExtend = false;
    this.uploadedFile = [];
  }

  getChangeRequestList() {
    this._commonHttpService.getPagedArrayList(
      {
        method: 'post',
        nolimit: true,
        where: {
          providerid: this.providerId,
          site_id: null
        },
        order: 'create_ts desc'
      },
      'providerportalrequest/getlicenseinfo?filter'
    ).subscribe(
      (response) => {
        if (Array.isArray(response.data) && response.data.length) {
          this.requestList = response.data[0].getlicenseinfo;
        }
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }

  getChangeRequestNarrative(selectedRequestNo: string) {

    this.requestNarrativeForm.patchValue({
      request_no: selectedRequestNo
    });

    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: {
          request_no: selectedRequestNo //this.changeRequestForm.value.request_no
        },
        order: 'create_ts asc'
      },
      'providerportalrequestnarrative?filter'
    ).subscribe(
      (response) => {
        this.tableData = response;
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }


  private loadDropDown() {
    const source = forkJoin([

      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        'States?filter'
      )
    ])
      .map((result) => {
        return {

          stateList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();

    this.stateList$ = this._commonDropdownService.getPickListByName('state');
  }

  getGenderList() {
    this._commonHttpService.getArrayList(
      {
        where: { tablename: 'gender', teamtypekey: 'OLM' },
        method: 'get',
        nolimit: true
      },
      'referencetype/gettypes?filter'
    ).subscribe(result => {
      this.genderList = result;
      console.log('List of genders----->' , this.genderList);
    });
  }

  onChangeEffectiveDate(value) {
    const formData = this.changeRequestForm.getRawValue();
    const expirationMinDate = formData.request_effectivedate;
    this.expirationMinDate = expirationMinDate;
  }

  onChangeExpirationDate(value) {
    const formData = this.changeRequestForm.getRawValue();
    const effectiveMaxDate = formData.request_expirationdate;
    this.effectiveMaxDate = effectiveMaxDate;
  }

  showChangeRequest(request) {
    this.viewForm = true;
    if (request) {
      this.uploadedFile = request.attachment;
      this.changeRequestForm.disable();
      this.selectRequestType(request.request_type);
      this.changeRequestForm.patchValue(request);
      this.changeRequestForm.patchValue({
        signimage: null
      });
      this.signature = request.signimage;
      (<any>$('#change-request')).modal('show');
    }

  }

  uploadAttachment(index) {
    console.log('check');
    const workEnv = config.workEnvironment;
    let uploadUrl = '';
    if (workEnv === 'state') {
        uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
            + '?access_token=' + this.token.id + '&' + 'srno=' + this.providerId + '&' + 'docsInfo='; // Need to discuss about the docsInfo
        console.log('state', uploadUrl);
    } else {
        uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
            '&' + 'srno=' + this.providerId;
        console.log('local', uploadUrl);
    }

    this._uploadService
        .upload({
            url: uploadUrl,
            headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
            filesKey: ['file'],
            files: this.uploadedFile[index],
            process: true,
        })
        .subscribe(
            (response) => {
                if (response.status) {
                    this.uploadedFile[index].percentage = response.percent;
                }
                if (response.status === 1 && response.data) {
                    const doucumentInfo = response.data;
                    doucumentInfo.documentdate = doucumentInfo.date;
                    doucumentInfo.title = doucumentInfo.originalfilename;
                    doucumentInfo.objecttypekey = 'ChangeRequestdocs';
                    doucumentInfo.rootobjecttypekey = 'ChangeRequestdocs';
                    doucumentInfo.activeflag = 1;
                    doucumentInfo.servicerequestid = null;
                    this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                    console.log(index, this.uploadedFile[index]);
                    this._alertService.success('File Upload successful.');
                } 
            }, (err) => {
                console.log(err);
                //this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                this._alertService.error('Upload failed due to Server error, please try again later.');
                this.uploadedFile.splice(index, 1);
               
            }
        );
}

  uploadFile(file: File | FileError): void {
    if (!(file instanceof Array)) {
        return;
    }
    file.map((item, index) => {
        const fileExt = item.name
            .toLowerCase()
            .split('.')
            .pop();
        if (
            fileExt === 'mp3' ||
            fileExt === 'ogg' ||
            fileExt === 'wav' ||
            fileExt === 'acc' ||
            fileExt === 'flac' ||
            fileExt === 'aiff' ||
            fileExt === 'mp4' ||
            fileExt === 'mov' ||
            fileExt === 'avi' ||
            fileExt === '3gp' ||
            fileExt === 'wmv' ||
            fileExt === 'mpeg-4' ||
            fileExt === 'pdf' ||
            fileExt === 'txt' ||
            fileExt === 'docx' ||
            fileExt === 'doc' ||
            fileExt === 'xls' ||
            fileExt === 'xlsx' ||
            fileExt === 'jpeg' ||
            fileExt === 'jpg' ||
            fileExt === 'png' ||
            fileExt === 'ppt' ||
            fileExt === 'pptx' ||
            fileExt === 'gif'
        ) {
            this.uploadedFile.push(item);
            const uindex = this.uploadedFile.length - 1;
            if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
                this.uploadedFile[uindex].percentage = 1;
            }

            this.uploadAttachment(uindex);
            const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
            const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
            if (audio_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Audio';
            } else if (video_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Video';
            } else {
                this.uploadedFile[uindex].attachmenttypekey = 'Document';
            }
        } else {
            // tslint:disable-next-line:quotemark
            this._alertService.error(fileExt + " format can't be uploaded");
            return;
        }
    });
}

downloadFile(s3bucketpathname) {
  const workEnv = config.workEnvironment;
  let downldSrcURL;
  if (workEnv === 'state') {
      downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
  } else {
      // 4200
      downldSrcURL = s3bucketpathname;
  }
  console.log('this.downloadSrc', downldSrcURL);
  window.open(downldSrcURL, '_blank');
}

confirmDeleteAttachment(index: number) {
  (<any>$('#delete-attachment-popup')).modal('show');
  this.deleteAttachmentIndex = index;
}

deleteAttachment() {
  this.uploadedFile.splice(this.deleteAttachmentIndex, 1);
  (<any>$('#delete-attachment-popup')).modal('hide');
}



  private getProviderContractLicenseTypes() {
    this.licenseInformation = {};
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        filter: {},
        where:
        {
          provider_id: this.providerId
        }
      },
      'providercontract/getproviderlicenseinformation'
    ).subscribe(response => {
      this.licenseInformation = response;
      if (this.licenseInformation.data) {
        this.licenseInformation.data.map((item) => {
          let availableSite = {};
          availableSite['text'] = item.site_id + ' - ' + item.provider_nm + '-' + item.license_type;
          availableSite['value'] = item.site_id;
          this.availableSiteList.push(availableSite);
        });
      }
    });
  }

  getExistingLicenseInformation() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          objectid: this.selectedSiteId
        }
      },
      'providerlicense/getproviderlicensing'
    ).subscribe(response => {
      this.existingLicenseInfo = response.data;
      if (this.existingLicenseInfo[0]) {
        this.changeRequestForm.patchValue({
          min_age_from: this.existingLicenseInfo[0].minimum_age_no,
          max_age_from: this.existingLicenseInfo[0].maximum_age_no,
          gender_from: this.existingLicenseInfo[0].gender_cd,
          children_no_from: this.existingLicenseInfo[0].children_no
        });
      }
      console.log("Rahul get license info", JSON.stringify(this.existingLicenseInfo));
    },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }
  changeSiteId(event) {
    this.selectedSiteId = event.value;
    //this.selectedLicenseNo;
    this.getSiteLicenseInformation();
    this.getExistingLicenseInformation();
  }
  selectRequestType(value) {
    if (value === 'License Change') {
      this.isLicenseChange = true;
      this.hasFormSignature = false;
      this.isWaiver = false;
      this.isVariance = false;
      this.isClousure = false;
      this.isLicenseExtend = false;
    } else if (value === 'Waiver') {
      this.isWaiver = true;
      this.isVariance = false;
      this.isLicenseChange = false;
      this.hasFormSignature = true;
      this.isClousure = false;
      this.isLicenseExtend = false;
    } else if (value === 'Variance') {
      this.isWaiver = false;
      this.isVariance = true;
      this.isLicenseChange = false;
      this.hasFormSignature = true;
      this.isClousure = false;
      this.isLicenseExtend = false;
    } else if (value === 'Voluntary Closure') {
      this.changeRequestForm.get('request_date').reset();
      this.isWaiver = false;
      this.isVariance = false;
      this.isLicenseChange = false;
      this.hasFormSignature = false;
      this.isClousure = true;
      this.isLicenseExtend = false;
    } else if(value === 'License Extend') {
      this.isWaiver = false;
      this.isVariance = false;
      this.isLicenseChange = false;
      this.hasFormSignature = false;
      this.isClousure = false;
      this.isLicenseExtend = true;
    } else  {
      this.isWaiver = false;
      this.isVariance = false;
      this.isLicenseChange = false;
      this.hasFormSignature = false;
      this.isClousure = false;
      this.isLicenseExtend = false;
    }
  }

  getSiteLicenseInformation() {
    this.selectedSiteLicenseInformation = {};
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          objectid: this.selectedSiteId
        }
      },
      'providerlicense/getproviderlicensing'
    ).subscribe(response => {
      this.selectedSiteLicenseInformation = response;
      if (this.selectedSiteLicenseInformation.data[0]) {
        this.selectedSiteLicenseInformation = this.selectedSiteLicenseInformation.data[0];
        this.selectedLicenseNo = this.selectedSiteLicenseInformation.license_no;
        this.patchSiteLicenseInformation();
      }
    },
      (error) => {
        this._alertService.error('Unable to get license information, please try again.');
        console.log('get license information Error', error);
        return false;
      }
    );
  }

  patchSiteLicenseInformation() {
    this.changeRequestForm.patchValue({
      license_no: this.selectedSiteLicenseInformation.license_no,
      program_address: this.selectedSiteLicenseInformation.formatted_address,
      program_phone: this.selectedSiteLicenseInformation.adr_work_phone_tx,
      program_fax: this.selectedSiteLicenseInformation.contact_fax,
      // gender_from : this.selectedSiteLicenseInformation.gender_cd,
      // max_age_from : this.selectedSiteLicenseInformation.maximum_age_no,
      // min_age_from : this.selectedSiteLicenseInformation.minimum_age_no,
      // children_no_from  : this.selectedSiteLicenseInformation.children_no
    });
  }

  saveInformation() {
    console.log('this.changeRequestForm.value', this.changeRequestForm.value);
    const changeRequestForm = this.changeRequestForm.getRawValue();
    if (changeRequestForm && changeRequestForm.request_type === 'Voluntary Closure') {
      if (changeRequestForm && !(changeRequestForm.site_id)) {
        this._alertService.error('Please select the Program site');
        return false;
      }
      if (changeRequestForm && !(changeRequestForm.request_date)) {
        this._alertService.error('Please fill the End Date');
        return false;
      }
    } else if (changeRequestForm && ((changeRequestForm.request_type === 'Variance' || changeRequestForm.request_type === 'Waiver') && this.changeRequestForm.invalid)) {
      this._alertService.error('Please fill the mandatory fields');
      return false;
    } else {
      if (changeRequestForm && !(changeRequestForm.site_id)) {
        this._alertService.error('Please select the Program site');
        return false;
      }
    }
    changeRequestForm.attachment = this.uploadedFile;
    changeRequestForm.old_license_expire_date = this.selectedSiteLicenseInformation ? this.selectedSiteLicenseInformation.license_expiry_dt : null;
    changeRequestForm.request_effectivedate = changeRequestForm.request_effectivedate ? changeRequestForm.request_effectivedate : new Date();
    changeRequestForm.request_date = changeRequestForm.request_date ? changeRequestForm.request_date : new Date();
    changeRequestForm.request_expirationdate = changeRequestForm.request_expirationdate ? changeRequestForm.request_expirationdate : new Date();
    changeRequestForm.dob = changeRequestForm.dob ? changeRequestForm.dob : new Date();
    this._commonHttpService.create(
      changeRequestForm,
      'providerportalrequest'
    ).subscribe(
      (response) => {        
        (<any>$('#change-request')).modal('hide');
        if (changeRequestForm.request_type === 'Voluntary Closure') {
          (<any>$('#voluntary-closure')).modal('show');
        }
        this.uploadAttachments(response);
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    this.requestNarrativeForm.patchValue(this.changeRequestForm.getRawValue());
    this.sendCommentsToWorker();
    this.changeRequestForm.reset();
    this.changeRequestForm.patchValue({
      provider_id: this.providerId
    });
    this.isLicenseChange = false;
  }

  routeToUser(requestDetail) {
    let toroleid = 'OLMQA';
    if (requestDetail.license_no.startsWith('DJS')) {
      toroleid = 'PVRDJSQA';
    }
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          eventcode: 'LICR',
          objectid: requestDetail.request_no,
          fromroleid: this._authService.getCurrentUser().user.userprofile.teammemberassignment.teammember.roletypekey,
          toroleid: toroleid,
          remarks: 'Provider initated the request',
          status: 'Submitted'
        }
      },
      'providerportalrequest/providerrouting'
    ).subscribe(response => {
      console.log(response);
      this.getChangeRequestList();
    });

  }


  
  uploadAttachments(data) {
      this._commonHttpService.create({
      method: 'post',
      objectid: data.request_id,
      attachment: data.attachment
    },
    'Providerportalrequest/addupload'
    ).subscribe(response => {
      console.log('Attachment upload Success!!', response);
      this.routeToUser(data);
    },
    (error) => {
      console.log('Attachment upload error!!!', error);
      return false;
    });
  }

//   deleteUploadFiles() {
//     this._commonHttpService.create({
//     method: 'post',
//     documentid: this.document_id
//     },
//   'Providerportalrequest/deleteattachment'
//   ).subscribe(response => {
//     console.log('Attachment deleted Successfully!!', response);
//   },
//   (error) => {
//     console.log('Unable to delete attachment!!!', error);
//     return false;
//   });
// }

  sendCommentsToWorker() {

    var payload = this.requestNarrativeForm.value;
    // payload['request_no'] = currentRequestNo;
    // payload['request_comments'] = currentRequestNarrative;
    // payload['request_narrative_by'] = this.providerId;

    this._commonHttpService.create(
      payload,
      'providerportalrequestnarrative'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        (<any>$('#additional-info')).modal('hide');
        //this.getChangeRequestNarrative();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
  }

  downloadChangeRequest(data) {
    const modal = {
      where: {
        objectid: data.request_no,
        status: data.remarks
      },
      method: 'post'
    };
    let fileName = 'License Change Request.pdf';

    if (data.request_type === 'Waiver') {
        fileName = 'Waiver Change Request.pdf';
    } else if (data.request_type === 'Variance') {
        fileName = 'Variance Change Request.pdf';
    } else if (data.request_type === 'Voluntary Closure') {
        fileName = 'Voluntary Closure Change Request.pdf';
    } else if (data.request_type === 'License Extend') {
      fileName = 'License Extend.pdf';
  }
    console.log('Download Change request data =====>', modal);
    this._commonHttpService.download('providerportalrequest/downloaddocument', modal).subscribe( response => {
      const pdfData = response && response !== null  ?   response : '';
      const blob = new Blob([new Uint8Array(pdfData)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  }
  disableClosureReject(data) {
    if (data.request_type === 'Voluntary Closure' && data.remarks === 'Rejected') {
      return true;
    } else {
      return false;
    }
  }





}
