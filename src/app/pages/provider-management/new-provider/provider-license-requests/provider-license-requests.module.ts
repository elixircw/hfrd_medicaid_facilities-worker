import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderLicenseRequestsRoutingModule } from './provider-license-requests-routing.module';
import { ProviderLicenseRequestsComponent } from './provider-license-requests.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { MatButtonToggleModule, MatSlideToggleModule, MatStepperModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
import { RequestSignatureFieldComponent } from '../../../provider-portal-temp/current-private-provider/provider-change-request/signature-field/signature-field.component';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    CommonModule,
    ProviderLicenseRequestsRoutingModule,
    FormMaterialModule,
  ],
  declarations: [ProviderLicenseRequestsComponent,]
})
export class ProviderLicenseRequestsModule { }
