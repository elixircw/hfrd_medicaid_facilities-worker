import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderLicenseRequestsComponent } from './provider-license-requests.component';

describe('ProviderLicenseRequestsComponent', () => {
  let component: ProviderLicenseRequestsComponent;
  let fixture: ComponentFixture<ProviderLicenseRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderLicenseRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderLicenseRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
