import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderLicenseRequestsComponent } from './provider-license-requests.component';

const routes: Routes = [
  {
    path: '' ,
    component: ProviderLicenseRequestsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderLicenseRequestsRoutingModule { }
