import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  AlertService } from '../../../../@core/services';
import * as _ from 'lodash';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { PaginationService } from 'ngx-pagination';

@Component({
  selector: 'provider-rate',
  templateUrl: './provider-rate.component.html',
  styleUrls: ['./provider-rate.component.scss']
})
export class ProviderRateComponent implements OnInit {
  licenserates = []
  licenseTypes=[];
  licenseTypesInfo:any;
    show: boolean = true;

  minDate = new Date();
  
  providerId: string;
  ratesForm: FormGroup;
  FormMode:string;
  irc: boolean;
  costSheet: boolean;
  pageInfo: PaginationInfo = new PaginationInfo();
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalCount: any;
  rate_type: string;

  constructor(private _commonHttpService: CommonHttpService,    private route: ActivatedRoute,private formBuilder: FormBuilder,private _alertService: AlertService) {
    this.providerId = route.snapshot.parent.parent.params['id'];
   }
  ngOnInit() {
    this.paginationInfo.pageNumber = 1;
    this.pageInfo.pageNumber = 1;
    this.rate_type = 'IRC';
    this.irc = true;
    this.costSheet = false;
    // TODO 
    // Call the get API to fetch data from the tb_provider_rates_master nd show on screen
    this.licenserates=[];
    this.getProviderrate();
    
    this.availablelicensetypes();
    

    this.initRatesForm();
    
  }
  
  private initRatesForm() {
    this.FormMode="Add";
    this.ratesForm = this.formBuilder.group({
      isratepreferred: null,
      ratecheck: null,
      provider_rate_id:0,
      provider_id:this.providerId,
      license_type:[''],
      annual_rate: null,
      monthly_rate: null,
      per_diem_rate: null,
      start_dt:new Date(),
      end_dt:new Date(),
      rate_code :[''],
      rate_status: [''],
      total_no_weeks: null,
      total_no_days: null,
      bed_capacity: null
    });
  }

  tabSwitch(tab) {
    this.paginationInfo.pageNumber = 1;
    this.pageInfo.pageNumber = 1;
    if (tab === 'irc') {
      this.rate_type = 'IRC';
      this.ratesForm.get('bed_capacity').setValidators(Validators.required);
      this.ratesForm.get('isratepreferred').setValidators(Validators.required);
      this.ratesForm.get('bed_capacity').updateValueAndValidity();
      this.ratesForm.get('isratepreferred').updateValueAndValidity();
      this.ratesForm.get('total_no_days').clearValidators();
      this.ratesForm.get('total_no_weeks').clearValidators();
      this.ratesForm.get('total_no_days').updateValueAndValidity();
      this.ratesForm.get('total_no_weeks').updateValueAndValidity();
      this.irc = true;
      this.costSheet = false;
    } else {
      this.rate_type = 'Cost Sheet';
      this.ratesForm.get('bed_capacity').clearValidators();
      this.ratesForm.get('isratepreferred').clearValidators();
      this.ratesForm.get('bed_capacity').updateValueAndValidity();
      this.ratesForm.get('isratepreferred').updateValueAndValidity();
      this.ratesForm.get('total_no_days').setValidators(Validators.required);
      this.ratesForm.get('total_no_weeks').setValidators(Validators.required);
      this.ratesForm.get('total_no_days').updateValueAndValidity();
      this.ratesForm.get('total_no_weeks').updateValueAndValidity();
      this.irc = false;
      this.costSheet = true;
    }
    // this.licenserates = [];
    this.getProviderrate();
  }

  addRate() {
    this.initRatesForm();
  }

  onDateChange(form) {
    if (form.value.start_dt > form.value.end_dt) {
      this.ratesForm.get('end_dt').reset();
    }
  }

  private getProviderrate() {
    let page = 1;
    const pageSize = 10;
    if (this.irc) {
      page = this.pageInfo.pageNumber;
    } else {
      page = this.paginationInfo.pageNumber;
    }
      this._commonHttpService.getPagedArrayList(new PaginationRequest({
            method: 'post',
            page: page,
            limit: pageSize,
            where:
            {
              provider_id: this.providerId,
              rate_type_switch: this.rate_type
            }
        }),
        'providercontract/getproviderlicenserates')
        .subscribe(licenserate => {
            this.licenserates = licenserate ? licenserate.data : null;
            this.totalCount = (this.licenserates && this.licenserates.length) ? this.licenserates[0].totalcount : 0;
          });
    }


    private availablelicensetypes() {
     this._commonHttpService.getPagedArrayList(new PaginationRequest({
          method: 'post',
             where:
             {
              provider_id: this.providerId
             }
         }),
         'providercontract/getproviderlicenseinformation')
         .subscribe(response => {
           if (response && response.data) {
            this.licenseTypesInfo = response.data;
            this.licenseTypesInfo.forEach(element => {
              if (!(this.licenseTypes.find(e => e.license_type === element.license_type))) {
              this.licenseTypes.push(element);
              }
            });
           }
          });
     }

     pageInfoChanged(page) {
       this.pageInfo.pageNumber = page;
       this.getProviderrate();
     }
     pageChanged(page) {
      this.paginationInfo.pageNumber = page;
      this.getProviderrate();
    }


  private createNewRates(req) {
    const formData = this.ratesForm.getRawValue();
    formData.rate_type_switch = this.rate_type;
    if (this.ratesForm.invalid) {
      this._alertService.error('Please fill the mandatory fields');
      return false;
    }
    
    if(req.license_type && req.license_type!=""){
    this._commonHttpService.create(formData, "providercontract/addproviderlicenserates").subscribe(
      (response) => {
        if (response) {
          let message;
          if(this.FormMode=="Edit")
            message="Rate Updated successfully";
          else
            message="Rate Created successfully";

          this._alertService.success(message);
          this.ratesForm.reset();
          this.initRatesForm();
          this.getProviderrate();
        }
      },
      (error) => {
        this._alertService.error("Unable to Create Rate");
      }
    );

    // this.licenserates.unshift(obj);
    (<any>$('#provider-rates')).modal('hide');
    }
    else{
      this._alertService.error("Unable to Create Rate,Please check Manadatory fields");
    }
  }


  closewindow() {
    this.ratesForm.reset();
    this.initRatesForm();
  }

  private editRates(obj) {
    // (<any>$('#provider-rates')).modal('hide');
  }


 private EditRate(Obj){
    this.FormMode="Edit";
    this.ratesForm.patchValue(Obj);
    this.ratesForm.patchValue({
      provider_rate_id:Obj.provider_rate_id,
      provider_id:this.providerId,
      per_diem_rate: Obj.per_diem_rate,
      license_type :Obj.license_type,
      rate_code:Obj.rate_code,
      start_dt:Obj.start_dt,
      end_dt:Obj.end_dt,
    });
    //this.licenserates.unshift(Obj);

    (<any>$('#provider-rates')).modal('show');
  }

  DeleteRate(Obj){
    
    (<any>$('Deleterate')).modal('show');
  }

  onRate(amount, control) {
    if ( control === 'per_diem_rate') {
      if (!_.isNaN(_.toNumber(amount))) {
        this.ratesForm.patchValue ({
          per_diem_rate : _.toNumber(amount).toFixed(2)
        });
    }
    } else if (control === 'monthly_rate') {
      if (!_.isNaN(_.toNumber(amount))) {
        this.ratesForm.patchValue ({
          monthly_rate : _.toNumber(amount).toFixed(2)
        });
      }
    } else if (control === 'annual_rate') {
      if (!_.isNaN(_.toNumber(amount))) {
        this.ratesForm.patchValue ({
          annual_rate : _.toNumber(amount).toFixed(2)
        });
      }
    }
  }

}
