import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderRateComponent } from './provider-rate.component';

describe('ProviderRateComponent', () => {
  let component: ProviderRateComponent;
  let fixture: ComponentFixture<ProviderRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
