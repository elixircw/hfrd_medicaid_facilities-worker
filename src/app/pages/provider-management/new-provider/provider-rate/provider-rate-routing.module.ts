import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderRateComponent } from './provider-rate.component';

const routes: Routes = [
  {
    path: '' ,
    component: ProviderRateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRateRoutingModule { }
