import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderRateRoutingModule } from './provider-rate-routing.module';
import { ProviderRateComponent } from './provider-rate.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { MatButtonToggleModule, MatSlideToggleModule, MatStepperModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaskModule } from 'ngx-mask';
import { PaginationModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    ProviderRateRoutingModule,
    FormMaterialModule,
    PaginationModule
  ],
  declarations: [ProviderRateComponent]
})
export class ProviderRateModule { }
