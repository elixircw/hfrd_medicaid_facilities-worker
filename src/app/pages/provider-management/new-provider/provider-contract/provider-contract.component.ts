import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material';
import { AlertService } from '../../../../@core/services';
import * as moment from 'moment';

export interface licenseInfoTable {
  licenseType: string;
  licenseNo: number;
  siteId: number;
  providerName: string;
  providerCategoryCode: number;
  
  
}

@Component({
  selector: 'provider-contract',
  templateUrl: './provider-contract.component.html',
  styleUrls: ['./provider-contract.component.scss']
})
export class ProviderContractComponent implements OnInit {
 
  results = [];
  currentData:any = {};
  contract_budget_codes: any[] =[];
  budget_code_type: any;
  contracts=[];
  contractSolicitation= [];
  contractType=[];
  contractTermType=[];
  contractPO=[];
  contractBPO=[];
  contractTermEndReason=[];
  contractTermStatus=[];
  licenseInformation:any={};
  minDate = new Date();
  maxDate= new Date();
  providerId: string;
  appointmentForm: FormGroup;
  licenseTypeForm: FormGroup;
  filteredInfo=[];
  contractLiscenseType= [];
  contractBudget= [];
  isStatusClosed:boolean = false;
  agency: string;
  referencetypeid: number;
  referencetype = [];

  contractLicenseTypeInfo: any;

  displayedColumns: string[] = ['select','licenseNo', 'licenseType', 'siteId', 'providerName' , 'providerCategoryCode'];
  selection = new SelectionModel<licenseInfoTable>(true, []);
  dataSource:any;
  budget_code_array: any;
  fiscalyeararray: any[];

  constructor(private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _alertService: AlertService) {
      this.isStatusClosed=false;
      this.providerId = route.snapshot.parent.parent.params['id'];
   }

  ngOnInit() {
    //this.getProviderContract();
    this.agency = 'OLM'
    this.initContractForm();
    this.initContractLicenseTypeForm();

    this.getProviderContractLicenseTypes();

    this.getContractWithLicenseTypes();

    this.referencetype = [601, 602, 603, 604];
    this.referencetype.forEach(id => {
      this.loadbudgetcodeDropdown(id);
    });
    

    //Dropdown constants
    this.contractSolicitation=[" EOI", "RFP", "IFB" ,"None"];
    this.contractType=[" Standard Service","Emergency","Sole source","Grant","Inter-agency agreements","Inter-Governmental agreements","MoU", "MoA"];
    //this.contractTermType=["Base","Option","Extension" ,"Modification"];
    this.contractTermType=["Base","Option"];
    this.contractPO = ['test123', 'test456'];
    this.contractBPO = ['abc123', 'abc456'];
    this.contractBudget = ['Residential PCAs', 'Residential Object Code Options', 'Non-Residential PCAs', 'Non-Residential Object Code Option'];
    this.contractTermEndReason=["Termination","Voluntary Closure"];
    this.contractTermStatus=["Active","Inactive","Expired","Closed","In process"];
    this.contractLiscenseType=["RCC","CPA"];
  }


  private initContractForm() {
    this.appointmentForm = this.formBuilder.group({
      provider_id: this.providerId,
      // contract_id: null,
      contract_solicitation: [''],
      contract_type_new_value: [''],
      start_dt: [''],
      end_dt: [''],
      contract_entry_date:new Date(),
      contract_received_date:new Date(),
      contract_term_type: [''],
      contract_po: [''],
      contract_bpo: [''],
      contract_term_end_reason: [''],
      contract_number: [''],
      contract_budget_codes: [null],
      budget_code_type: [null],
      estimate_value_no:[''],
      contract_agency:this.agency,
      total_contract_beds:[''],
      comments_tx:[''],
      contract_term_status: ['']
    });
    this.appointmentForm.setControl('fiscalyeararray', this.formBuilder.array([]));
  }

  Dropdownbudgettype(data) {
        if (data.value === 'Residential PCAs') {
      this.referencetypeid = 601;
      // return false;
    } else if (data.value === 'Residential Object Code Options') {
      this.referencetypeid = 602;
      // return false;
    } else if (data.value === 'Non-Residential PCAs') {
      this.referencetypeid = 603;
      // return false;
    } else if (data.value === 'Non-Residential Object Code Option') {
      this.referencetypeid = 604;
      // return false;
    }
    this.budget_code_array = this.contract_budget_codes[this.referencetypeid];
    console.log('Dropdown data======>', data);

    
  }

  private loadbudgetcodeDropdown(ref_id) {
    this._commonHttpService.getArrayList({
      nolimit: true,
      where: { referencetypeid: ref_id }, order: 'displayorder ASC', method: 'get'
    }, 'referencevalues?filter').subscribe(response => {
        this.contract_budget_codes[ref_id] = response;
    });
  }

  

  private getContractWithLicenseTypes() {
    
    this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
            provider_id: this.providerId
          }
      },
      'providercontract/getprovidercontractwithlicensetypes'
  ).subscribe(contractLicenseTypeInfo => {
    this.contractLicenseTypeInfo = contractLicenseTypeInfo;
    if (this.contractLicenseTypeInfo && this.contractLicenseTypeInfo.data && this.contractLicenseTypeInfo.data.length) {
      this.results = this.contractLicenseTypeInfo.data;
    }
  });

  }


  checkcontractStatus(value){
    this.isStatusClosed=value=="Closed"?true:false;
  }

  private initContractLicenseTypeForm(){
    this.licenseTypeForm = this.formBuilder.group({
      contract_license_level: [''],
    });
  }

  private getProviderContractLicenseTypes() {
      this._commonHttpService.getArrayList(
        {
            method: 'post',
            nolimit: true,
            filter: {},
            where:
            {
              provider_id: this.providerId
              //provider_id:"5000543"
            }
        },
        'providercontract/getproviderlicenseinformation'
    ).subscribe(licenseInformation => {
      this.licenseInformation = licenseInformation;
      this.dataSource = new MatTableDataSource<licenseInfoTable>(this.licenseInformation.data);
    });
  }
    
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  filterLicenceInfo(value) {
    this.filteredInfo = [];
    this.licenseInformation.data.forEach(element => {
      if(element.license_level == value){
        this.filteredInfo.push(element);
      }
    });
    this.dataSource = new MatTableDataSource<licenseInfoTable>(this.filteredInfo);
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  createNewContract(section){
    if (section === 'Information') {
      if (this.appointmentForm.valid) {
        (<any>$('#searchresult')).click();
        return false;
      } else {
        this._alertService.error('Please fill the mandatory fields');
        return false;
      }
    } else if (section === 'Previous') {
      (<any>$('#profileinfo')).click();
      return false;
    } else {
      if (this.appointmentForm.invalid) {
        (<any>$('#profileinfo')).click();
        this._alertService.error('Please fill the mandatory fields');
        return false;
      } else if (this.licenseTypeForm.invalid) {
        this._alertService.error('Please fill the mandatory fields');
        return false;
      }
    }
    console.log(this.appointmentForm.value);
    console.log(this.licenseTypeForm.value);
    console.log(this.selection.selected);

    (<any>$('#provider-contract')).modal('hide');
    // Compiling all the data
    Object.assign(this.currentData, this.appointmentForm.value);
    Object.assign(this.currentData, this.licenseTypeForm.value);
    const budget_code_array = [];

    if (this.currentData.contract_budget_codes && this.currentData.contract_budget_codes.length) {
    this.currentData.contract_budget_codes.forEach(data => {
      const obj = {
        'budget_code': data
      };
      budget_code_array.push(obj);
    });
  }
    this.currentData.contract_budget_codes = budget_code_array;

    this.currentData.contract_license_types = this.selection.selected;
    console.log('@@@@@@ current data', this.currentData );
    // this.results=[];
    // this.results.unshift(this.currentData);   
    // console.log("Final array", this.results);

    this.saveNewContractLicense(this.currentData);

  }

  saveNewContractLicense(req) {
    this._commonHttpService.create(req, "providercontract/addprovidercontractwithlicensetypes").subscribe(
      (response) => {
        if (response) {
          let message = "Contract saved successfully";
          this._alertService.success(message);
          this.appointmentForm.reset();
          this.licenseTypeForm.reset();
          (<any>$('#profileinfo')).click();
          this.selection.clear();
          this.getContractWithLicenseTypes();
        }
      },
      (error) => {
        this._alertService.error("Unable to Create contract");
      }
    );
  }

  termType(form, value) {
    if (value === 'Base' && form.value.start_dt && form.value.end_dt) {
      this.fiscalYearCal(form);
    }
  }

  termDateChange(form) {
    if (form.value.start_dt && form.value.end_dt) {
      if (form.value.star_dt > form.value.end_dt) {
        this.appointmentForm.get('end_dt').reset();
        return false;
      } else {
        if (form.value.contract_term_type === 'Base') {
          this.fiscalYearCal(form);
        }
      }
    }
  }

  fiscalYearCal(form) {
    this.fiscalyeararray = [];
    this.appointmentForm.setControl('fiscalyeararray', this.formBuilder.array([]));
      const startDate = new Date(form.value.start_dt);
      const endDate =  new Date(form.value.end_dt);
      if (startDate.getFullYear() !== endDate.getFullYear()) {
        for (let i = startDate.getFullYear(); i <= endDate.getFullYear(); i++) {
          if (i === startDate.getFullYear()) {
            if (startDate.getMonth() < 6) {
              this.fiscalyeararray.push({
                fiscalyear: i,
                fiscalstartdate : moment(new Date(startDate)).format('MM/DD/YYYY'),
                fiscalenddate: moment(new Date(i, 5, 30)).format('MM/DD/YYYY'),
                amount: null
              });
            } else {
              if (i === endDate.getFullYear() - 1) {
                this.fiscalyeararray.push({
                  fiscalyear: i + 1,
                  fiscalstartdate : moment(new Date(startDate)).format('MM/DD/YYYY'),
                  fiscalenddate : moment(new Date(endDate)).format('MM/DD/YYYY'),
                  amount: null
                });
                break;
              } else {
                this.fiscalyeararray.push({
                  fiscalyear: i + 1,
                  fiscalstartdate : moment(new Date(startDate)).format('MM/DD/YYYY'),
                  fiscalenddate : moment(new Date(i + 1, 5, 30)).format('MM/DD/YYYY'),
                  amount: null
                });
              }
            }
          } else {
            if (i === endDate.getFullYear()) {
              if (startDate.getMonth() < 6 && endDate.getMonth() <= 5) {
                this.fiscalyeararray.push({
                  fiscalyear: i,
                  fiscalstartdate : moment(new Date(i - 1, 6, 1)).format('MM/DD/YYYY'),
                  fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
                  amount: null
                });
              } else if (startDate.getMonth() < 6 && endDate.getMonth() > 5) {
                this.fiscalyeararray.push({
                  fiscalyear: i,
                  fiscalstartdate : moment(new Date(i - 1, 6, 1)).format('MM/DD/YYYY'),
                  fiscalenddate: moment(new Date(i, 5, 30)).format('MM/DD/YYYY'),
                  amount: null
                });
                this.fiscalyeararray.push({
                  fiscalyear: i + 1,
                  fiscalstartdate : moment(new Date(i, 6, 1)).format('MM/DD/YYYY'),
                  fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
                  amount: null
                });

              } else if (startDate.getMonth() >= 6 && endDate.getMonth() <= 5) {
                this.fiscalyeararray.push({
                  fiscalyear: i + 1,
                  fiscalstartdate : moment(new Date(i, 6, 1)).format('MM/DD/YYYY'),
                  fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
                  amount: null
                });
              } else if (startDate.getMonth() >= 6 && endDate.getMonth() > 5) {
                this.fiscalyeararray.push({
                  fiscalyear: i + 1,
                  fiscalstartdate : moment(new Date(i, 6, 1)).format('MM/DD/YYYY'),
                  fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
                  amount: null
                });
              }
            } else {
              if (startDate .getMonth() < 6) {
                this.fiscalyeararray.push({
                  fiscalyear: i,
                  fiscalstartdate : moment(new Date(i - 1 , 6, 1)).format('MM/DD/YYYY'),
                  fiscalenddate: moment(new Date(i, 5, 30)).format('MM/DD/YYYY'),
                  amount: null
                });
              } else {
                if (endDate.getMonth() > 5) {
                  this.fiscalyeararray.push({
                    fiscalyear: i + 1,
                    fiscalstartdate : moment(new Date(i , 6, 1)).format('MM/DD/YYYY'),
                    fiscalenddate: moment(new Date(i + 1, 5, 30)).format('MM/DD/YYYY'),
                    amount: null
                  });
                } else {
                  if (i === endDate.getFullYear() - 1) {
                    this.fiscalyeararray.push({
                      fiscalyear: i + 1,
                      fiscalstartdate : moment(new Date(i , 6, 1)).format('MM/DD/YYYY'),
                      fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
                      amount: null
                    });
                    break;
                  } else {
                    this.fiscalyeararray.push({
                      fiscalyear: i + 1,
                      fiscalstartdate : moment(new Date(i , 6, 1)).format('MM/DD/YYYY'),
                      fiscalenddate: moment(new Date(i + 1, 5, 30)).format('MM/DD/YYYY'),
                      amount: null
                    });
                  }
                }
              }
            }
          }
        }
      } else {
        if (endDate.getMonth() <= 5) {
          this.fiscalyeararray.push({
            fiscalyear: startDate.getFullYear(),
            fiscalstartdate : moment(new Date(startDate)).format('MM/DD/YYYY'),
            fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
            amount: null
          });
        } else if (startDate.getMonth() >= 6) {
          this.fiscalyeararray.push({
            fiscalyear: startDate.getFullYear() + 1,
            fiscalstartdate : moment(new Date(startDate)).format('MM/DD/YYYY'),
            fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
            amount: null
          });
        } else {
            this.fiscalyeararray.push({
              fiscalyear: startDate.getFullYear(),
              fiscalstartdate : moment(new Date(startDate)).format('MM/DD/YYYY'),
              fiscalenddate: moment(new Date(endDate.getFullYear(), 5, 30)).format('MM/DD/YYYY'),
              amount: null
            });
            this.fiscalyeararray.push({
              fiscalyear: startDate.getFullYear() + 1,
              fiscalstartdate : moment(new Date(startDate.getFullYear(), 6, 1)).format('MM/DD/YYYY'),
              fiscalenddate: moment(new Date(endDate)).format('MM/DD/YYYY'),
              amount: null
            });
          }
        }
        if (this.fiscalyeararray && this.fiscalyeararray.length > 0) {
          this.fiscalyeararray.forEach(data => {
            this.addFiscalYear(data);
          });
        }
  }

    addFiscalYear(data) {
      const control = <FormArray>this.appointmentForm.controls['fiscalyeararray'];
      control.push(this.createFiscalYear(data));
    }

    createFiscalYear(modal) {
      return this.formBuilder.group({
        fiscalyear: modal.fiscalyear ? modal.fiscalyear : null,
        fiscalstartdate: modal.fiscalstartdate ? modal.fiscalstartdate : null,
        fiscalenddate: modal.fiscalenddate ? modal.fiscalenddate : null,
        amount: modal.amount ? modal.amount : null
      });
    }
}
