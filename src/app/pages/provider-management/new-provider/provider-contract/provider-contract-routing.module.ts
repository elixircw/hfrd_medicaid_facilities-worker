import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderContractComponent } from './provider-contract.component';

const routes: Routes = [
  {
    path: '' ,
    component: ProviderContractComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderContractRoutingModule { }
