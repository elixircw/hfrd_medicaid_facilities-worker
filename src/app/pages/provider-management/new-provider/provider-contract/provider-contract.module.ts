import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
import { ProviderContractRoutingModule } from './provider-contract-routing.module';
import { ProviderContractComponent } from './provider-contract.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { MatStepperModule, MatSlideToggleModule, MatButtonToggleModule } from '@angular/material';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    ProviderContractRoutingModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    MatStepperModule
    
  ],
  declarations: [ProviderContractComponent]
})
export class ProviderContractModule { }
