import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddDocumentRoutingModule } from './add-document-routing.module';
import { AttachmentDetailComponent } from './attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './attachment-upload/attachment-upload.component';
import { AddDocumentComponent } from './add-document.component';
import { AudioRecordComponent } from './audio-record/audio-record.component';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import { ImageRecordComponent } from './image-record/image-record.component';
import { VideoRecordComponent } from './video-record/video-record.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { MatButtonToggleModule, MatSlideToggleModule, MatStepperModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    AddDocumentRoutingModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    NgSelectModule,
    SharedPipesModule,
    ControlMessagesModule,
    MatButtonToggleModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    MatStepperModule
  ],
  declarations: [
    AttachmentDetailComponent, AttachmentUploadComponent, AddDocumentComponent,
    AudioRecordComponent, EditAttachmentComponent, ImageRecordComponent, VideoRecordComponent,
  ]
})
export class AddDocumentModule { }
