import { Component, OnInit } from '@angular/core';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService, AuthService } from '../../../../@core/services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.scss']
})
export class ReviewListComponent implements OnInit {

  resitutions: any = [];
  ccuresitutions: any = [];
  totalRestiutions: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  status: string;
  restitutionPage = false;
  RESTITUION_STATUS = {
    INACTIVE: 'Inactive',
    ACTIVE: 'Active',
    CLOSED: 'Closed'
  };
  fromSearch: boolean;
  isDjs: boolean;
  constructor(private _formBuilder: FormBuilder,
    private _commonService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute,
    private _store: DataStoreService,
    private _authService: AuthService) {
    this.route.params.subscribe((item) => {
      this.fromSearch = item['review'] === 'search-result';
      if (!this.fromSearch) {
        if (this._store.getData(IntakeStoreConstants.restitutionSearchParams)) {
          this._store.setData(IntakeStoreConstants.restitutionSearchParams, null);
          this.searchRestituion();
        }
      }
    });
  }

  ngOnInit() {
    this.status = this.RESTITUION_STATUS.ACTIVE;
    this.searchRestituion();
    this.isDjs = this._authService.isDJS();
  }

  loadRestiution(status: string) {
    this.status = status;
    this.paginationInfo.pageNumber = 1;
    this.getRestiutions(this.paginationInfo.pageNumber);
    this.restitutionPage = true;
  }

  loadCCURestiution() {
    this.paginationInfo.pageNumber = 1;
    this.restitutionPage = false;
    this.loadCculist('');
  }

  loadCculist(status: string) {
    this.status = status;
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.CCU.DASHBOARD_LIST + '?data'
    ).subscribe((result: any) => {
      if (result) {
        this.ccuresitutions = result.data;
        this.totalRestiutions = result.data.length ? result.count : 0;
      }
    });
  }

  toggleTable(id) {
    (<any>$('#' + id)).collapse('toggle');
  }
  searchRestituion() {
    this.paginationInfo.pageNumber = 1;
    this.getRestiutions(this.paginationInfo.pageNumber);
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.getRestiutions(this.paginationInfo.pageNumber);
  }

  resetRestiutionList() {
    this.resitutions = [];
    this.totalRestiutions = 0;
  }

  getRestiutions(pageNumber) {
    this.restitutionPage = true;
    const searchRestituion = this._store.getData(IntakeStoreConstants.restitutionSearchParams) ? this._store.getData(IntakeStoreConstants.restitutionSearchParams) : {};
    searchRestituion.status = this.status;
    console.log(searchRestituion);
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'post',
        where: {
          intakeserviceid: null,
          searchvals: searchRestituion
        }
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.LISTING_URL + '?filter'
    ).subscribe((result: any) => {
      if (result) {
        this.resitutions = result;
        this.totalRestiutions = result.length ? result[0].totalcount : 0;
      }

    });
  }

  viewDetails(data: any) {
    this._store.setData(IntakeStoreConstants.intakenumber, data.intakenumber);
    this.router.navigate([`/pages/restitution/${data.intakeserreqrestitutionid}/${data.intakeserviceid}`],
      { relativeTo: this.route, queryParams: { status: this.status, IsCCUenable: data.isccusent, intakeFrom: data.restitutiontype, pageFrom: 'RC' } });
  }

  openPersonDetail(personid: string) {
    this.router.navigate(['/pages/person-details/view/' + personid + '/basic']);
  }

  toSeachScreen() {
    this.router.navigate(['../search'], { relativeTo: this.route });
  }

  downloadPaymentSlip(data) {

    this._store.setData(IntakeStoreConstants.restiutionDocumentInfo, this.convertRestiutionDocumentInfo(data));
    this.router.navigate(['payment-slip-document'], { relativeTo: this.route });
  }



  downloadPaymentSchedule(data) {

    this._store.setData(IntakeStoreConstants.restiutionDocumentInfo, this.convertRestiutionDocumentInfo(data));
    this.router.navigate(['payment-schedule-document'], { relativeTo: this.route });
  }

  convertRestiutionDocumentInfo(data) {
    return {
      focusperson: {
        lastname: data.yothpersonname
      },
      liableperson: {
        lastname: data.liablepersonname
      },
      restitutionnumber: data.restitutionno,
      petitionid: data.petitionid,
      intakenumber: data.intakenumber,
      duedate: data.duedate,
      amount: data.payment,
      frequency: data.frequency,
      installmentamount: data.installmentamount
    };
  }


}
