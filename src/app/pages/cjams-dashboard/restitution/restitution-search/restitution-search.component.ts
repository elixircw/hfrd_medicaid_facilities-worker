import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';

@Component({
  selector: 'restitution-search',
  templateUrl: './restitution-search.component.html',
  styleUrls: ['./restitution-search.component.scss']
})
export class RestitutionSearchComponent implements OnInit {
  restitutionSearchFrom: FormGroup;
  relations = [];
  constructor(private _formBuilder: FormBuilder,
    private _commonService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute,
    private _store: DataStoreService) { }

  ngOnInit() {
    this.initSearchForm();
    const searchRestituion = this._store.getData(IntakeStoreConstants.restitutionSearchParams);
    if (searchRestituion) {
      this.restitutionSearchFrom.patchValue(searchRestituion);
      this.restitutionSearchFrom.markAsDirty();
    }
    this.getRelations();
  }

  getRelations() {
    return this._commonService.getArrayList(
      {
        where: { activeflag: 1 },
        method: 'get',
        nolimit: true,
        order: 'description'
      },
      NewUrlConfig.EndPoint.Intake.RelationshipTypesUrl + '?filter'
    ).subscribe(relations => {
      this.relations = relations;
    });
  }

  initSearchForm() {
    this.restitutionSearchFrom = this._formBuilder.group({
      youthfirstname: [''],
      youthlastname: [''],
      youthid: [''],
      youthrelation: [''],
      victimfirstname: [''],
      victimlastname: [''],
      restitutionno: [''],
      complaintid: [''],
      petitionid: [''],
      payerfirstname: [''],
      payerlastname: [''],
      payeraddress: [''],
      payercity: [''],
      payerstatekey: [''],
      payerzipcode: [''],
      depositnumber: [''],
      accountarea: [''],
      paymentamount: [null],
      paymentnumber: [''],
      memofirstname: [''],
      memolastname: [''],
    });
  }

  clearSearchForm() {
    this.restitutionSearchFrom.reset();
  }

  searchRestituion() {
    const searchRestituion = this.restitutionSearchFrom.getRawValue();
    this._store.setData(IntakeStoreConstants.restitutionSearchParams, searchRestituion);
    this.router.navigate(['../search-result'], { relativeTo: this.route });
  }

}
