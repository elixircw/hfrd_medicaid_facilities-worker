import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestitutionSearchComponent } from './restitution-search.component';

describe('RestitutionSearchComponent', () => {
  let component: RestitutionSearchComponent;
  let fixture: ComponentFixture<RestitutionSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestitutionSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestitutionSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
