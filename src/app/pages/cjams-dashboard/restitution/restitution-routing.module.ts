import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestitutionComponent } from './restitution.component';
import { ReviewListComponent } from './review-list/review-list.component';
import { PaymentScheduleDocumentComponent } from '../../shared-pages/payment-schedule/payment-schedule-document/payment-schedule-document.component';
import { PaymentSlipDocumentComponent } from '../../shared-pages/payment-schedule/payment-slip-document/payment-slip-document.component';
import { RestitutionSearchComponent } from './restitution-search/restitution-search.component';

const routes: Routes = [
  {
    path: '',
    component: RestitutionComponent,
    children: [
      // {
      //   path: ':restituionid/:id',
      //   loadChildren: './restitution-details/restitution-details.module#RestitutionDetailsModule'
      // },
      {
        path: 'search',
        component: RestitutionSearchComponent
      },
      {
        path: ':review',
        component: ReviewListComponent,
        children: [
          {
            path: 'payment-schedule-document',
            component: PaymentScheduleDocumentComponent,
          },
          {
            path: 'payment-slip-document',
            component: PaymentSlipDocumentComponent,
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestitutionRoutingModule { }
