import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap/pagination/pagination.module';

import { RestitutionRoutingModule } from './restitution-routing.module';
import { RestitutionComponent } from './restitution.component';
import { ReviewListComponent } from './review-list/review-list.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MatTooltipModule } from '@angular/material';
import { PaymentScheduleModule } from '../../shared-pages/payment-schedule/payment-schedule.module';
import { RestitutionSearchComponent } from './restitution-search/restitution-search.component';


@NgModule({
  imports: [
    CommonModule,
    RestitutionRoutingModule,
    PaginationModule,
    FormMaterialModule,
    MatTooltipModule,
    PaymentScheduleModule
  ],
  declarations: [RestitutionComponent, ReviewListComponent, RestitutionSearchComponent]
})
export class RestitutionModule { }
