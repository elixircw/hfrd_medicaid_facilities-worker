import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from '../../@core/guard';
import { CjamsDashboardComponent } from './cjams-dashboard.component';
import { DashAssignCaseComponent } from './dash-assign-case/dash-assign-case.component';
import { DashIntakeSummaryComponent } from './dash-intake-summary/dash-intake-summary.component';
import { DashPreIntakeComponent } from './dash-pre-intake/dash-pre-intake.component';
import { CwAssignCaseComponent } from './cw-assign-case/cw-assign-case.component';
import { CwApprovalComponent } from './cw-approval/cw-approval.component';
import { CwIntakeReferalsComponent } from './cw-intake-referals/cw-intake-referals.component';
import { CwAssessmentComponent } from './cw-assessment/cw-assessment.component';
import { DjsReviewCaseComponent } from './djs-review-case/djs-review-case.component';
import { IvECasesComponent } from './iv-e-cases/iv-e-cases.component';
import { CwAssignServiceCaseComponent } from './cw-assign-service-case/cw-assign-service-case.component';
// import { AsIntakeReferralsComponent } from './as-intake-referrals/as-intake-referrals.component';
import { DashMyIntakeSummaryComponent } from './dash-my-intake-summary/dash-my-intake-summary.component';
import { CwWorkloadComponent } from './cw-workload/cw-workload.component';
import { PurposeResolverService } from '../newintake/my-newintake/purpose-resolver.service';
import { CwLdssInboxComponent } from './cw-ldss-inbox/cw-ldss-inbox.component';

const routes: Routes = [
    {
        path: '',
        component: CjamsDashboardComponent,
        canActivate: [RoleGuard],
    },
    { path: 'pre-intakes', component: DashPreIntakeComponent, canActivate: [RoleGuard] },
    { path: 'intake-summary', component: DashIntakeSummaryComponent, canActivate: [RoleGuard] },
    { path: 'my-intake-summary', component: DashMyIntakeSummaryComponent, canActivate: [RoleGuard]},
    { path: 'assign-case', component: DashAssignCaseComponent, canActivate: [RoleGuard] },
    { path: 'review-case', component: DjsReviewCaseComponent, canActivate: [RoleGuard] },
    {
        path: 'restitution',
        loadChildren: './restitution/restitution.module#RestitutionModule'
    },
    {
        path: 'djsplacement-dashboard',
        loadChildren: './djs-placement-worker/djs-placement-worker.module#DjsPlacementWorkerModule'
    },
    { path: 'cw-assign-case', component: CwAssignCaseComponent, canActivate: [RoleGuard] },
    { path: 'cw-assign-service-case', component: CwAssignServiceCaseComponent, canActivate: [RoleGuard] },
    { path: 'cw-approval', component: CwApprovalComponent, canActivate: [RoleGuard] },
    { 
        path: 'cw-intake-referals', 
        component: CwIntakeReferalsComponent,
        resolve: {
            purposeList: PurposeResolverService,
        },
        canActivate: [RoleGuard]
    },
    { path: 'cw-assessment', component: CwAssessmentComponent, canActivate: [RoleGuard] },
    { path: 'iv-e-cases', component: IvECasesComponent, canActivate: [RoleGuard] },
    { path: 'cw-workload', component: CwWorkloadComponent, canActivate: [RoleGuard] },
    { path: 'cw-ldss-inbox', component: CwLdssInboxComponent, canActivate: [RoleGuard] },
    
    // ADULT SERVICE DASHBOARDS
    // { path: 'as-intake-referrals', component: AsIntakeReferralsComponent, canActivate: [RoleGuard] },
    // { path: 'as-assign-case', loadChildren: './as-assign-case/as-assign-case.module#AsAssignCaseModule', canActivate: [RoleGuard] },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CjamsDashboardRoutingModule { }
