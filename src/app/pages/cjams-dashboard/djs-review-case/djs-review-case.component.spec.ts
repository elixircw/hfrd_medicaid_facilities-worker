import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsReviewCaseComponent } from './djs-review-case.component';

describe('DjsReviewCaseComponent', () => {
  let component: DjsReviewCaseComponent;
  let fixture: ComponentFixture<DjsReviewCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsReviewCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsReviewCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
