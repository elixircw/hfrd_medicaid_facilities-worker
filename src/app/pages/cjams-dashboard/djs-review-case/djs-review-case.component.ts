import { Component, OnInit } from '@angular/core';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { AuthService, AlertService, CommonHttpService, DataStoreService } from '../../../@core/services';
import { Router } from '@angular/router';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { DashBoard } from '../cjams-dashboard-url.config';
import { ReviewCase } from '../_entities/dashBoard-datamodel';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'djs-review-case',
  templateUrl: './djs-review-case.component.html',
  styleUrls: ['./djs-review-case.component.scss']
})
export class DjsReviewCaseComponent implements OnInit {

  reviewPaginationInfo: PaginationInfo = new PaginationInfo();
  reviewCases: ReviewCase[] = [];
  searchCriteria: any;
  totalRecordsCaseReview: number;
  previousPage: number;
  serreqno: string;
  constructor(
    private _authService: AuthService,
    private _alertService: AlertService,
    private _commonService: CommonHttpService,
    private _router: Router,
    private _intakeUtils: IntakeUtils,
    private _dataStoreService: DataStoreService
  ) { }

  ngOnInit() {
    this.serreqno = '';
    this.getReviewCase(1);
  }

  onCaseSorted($event: ColumnSortedEvent) {
    this.reviewPaginationInfo.sortBy = $event.sortDirection;
    this.reviewPaginationInfo.sortColumn = $event.sortColumn;
    this.getReviewCase(this.reviewPaginationInfo.pageNumber);
  }
  getReviewCase(selectPage: number) {
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.reviewPaginationInfo.pageSize,
        page: selectPage,
        method: 'post',
        where: {
          serreqno: this.serreqno,
          eventcode: 'INVRCSR'
        }
      }),
      DashBoard.EndPoint.AssinedCase.CaseReviewUrl + '?filter'
    ).subscribe((result: any) => {
      this.reviewCases = result.data.result;
      if (this.reviewPaginationInfo.pageNumber === 1) {
        this.totalRecordsCaseReview = result.data.count;
      }
    });
  }
  reviewPageChanged(pageInfo: any) {
    this.reviewPaginationInfo.pageNumber = pageInfo.page;
    this.reviewPaginationInfo.pageSize = pageInfo.itemsPerPage;
    if (this.reviewPaginationInfo.pageNumber !== 1) {
      this.previousPage = pageInfo.page;
    }
    this.getReviewCase(this.reviewPaginationInfo.pageNumber);
  }

  routeReviewCase(item: any) {
    const serviceRequestId = item.intakeserviceid;
    this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
      const dsdsActionsSummary = response[0];
      this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
      this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
      const currentUrl = '/pages/case-worker/' + serviceRequestId + '/' + item.servicerequestnumber + '/dsds-action/report-summary-djs';

      this._router.navigate([currentUrl]);
    });
  }

  onSearchAssignCase(serreqno: string) {
    this.serreqno = serreqno;
    this.getReviewCase(1);
  }
}
