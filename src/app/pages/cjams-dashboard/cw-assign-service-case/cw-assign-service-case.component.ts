
import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo, DynamicObject, PaginationRequest, DropdownModel } from '../../../@core/entities/common.entities';
import { AssignedCase, ReviewGridModal, ReviewCase, RoutingUser, Appeal } from '../_entities/dashBoard-datamodel';
import { DashBoard } from '../cjams-dashboard-url.config';
import { Observable } from 'rxjs/Observable';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { AuthService, AlertService, DataStoreService, GenericService, SessionStorageService } from '../../../@core/services';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cw-assign-service-case',
    templateUrl: './cw-assign-service-case.component.html',
    styleUrls: ['./cw-assign-service-case.component.scss']
})
export class CwAssignServiceCaseComponent implements OnInit {
    assignedCaseForm: FormGroup;
    assignServiceCaseForm: FormGroup;
    teamForm: FormGroup;
    selectedPerson: any;
    serviceRequestId: string;
    mergeUsersList: RoutingUser[] = [];
    assignedStatus: string;
    isSupervisor: boolean;
    statusDesc: any;
    selectedResponsibilityType: string;
    zipCodeIndex: number;
    appealStatus: boolean;
    getServicereqid: string;
    getServicecaseid: string;
    assingedServiceCaseSummary: any[];
    zipCode: string;
    isGroupId = false;
    isGroup = false;
    servreqsubtype: string;
    supervisorReviewStatus: boolean;
    totalRecordsAssingedCase: number;
    getUsersList: RoutingUser[];
    originalUserList: RoutingUser[];
    totalRecordsCaseReview: number;
    totalRecordsSupervisor: number;
    assignedSearchCriteria: any;
    reviewSearchCriteria: any;
    previousPage: number;
    statusDropdownItems$: Observable<DropdownModel[]>;
    appealPaginationInfo: PaginationInfo = new PaginationInfo();
    dynamicObjectAssignCase: DynamicObject = {};
    dynamicObjectAppeal: DynamicObject = {};
    reviewPaginationInfo: PaginationInfo = new PaginationInfo();
    dynamicObjectReviewToSupervisor: DynamicObject = {};
    dynamicObjectCaseReview: DynamicObject = {};
    assingedCaseSummary: any;
    paginationInfo: PaginationInfo = new PaginationInfo();
    responsibilityTypeDropdownItems$: DropdownModel[];
    reviewToSupervisor: ReviewGridModal[];
    tobeassignedPaginationInfo: PaginationInfo = new PaginationInfo();
    onGoingPaginationInfo: PaginationInfo = new PaginationInfo();
    reviewCases: ReviewCase[] = [];
    roleId: AppUser;
    private searchTermStreamAppeal$ = new Subject<DynamicObject>();
    private pageStreamAppeal$ = new Subject<number>();
    private pageStreamOnGoing$ = new Subject<number>();
    private searchTermStreamOnGoing$ = new Subject<DynamicObject>();
    private searchTermStreamAssignCase$ = new Subject<DynamicObject>();
    private pageStreamAssgine$ = new Subject<number>();
    private appealIntakeReqId: string;
    role: string;
    roleList: {};
    programArea: any[];
    workersList: any[] = [];
    programSubArea: any[];
    teamtypekey: string;
    teamid: string;
    teamList: Array<any> = [];
    serviceCaseDetails: any;
    supervisorList: Array<any[]> = [];
    constructor(
        private _commonService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _router: Router,
        private _dataStoreService: DataStoreService,
        private _intakeUtils: IntakeUtils,
        private _appealService: GenericService<Appeal>,
        private _alertService: AlertService,
        private route: ActivatedRoute,
        private router: Router,
        private _session: SessionStorageService
    ) { }

    ngOnInit() {
        this.teamtypekey = this._authService.getCurrentUser().user.userprofile.teamtypekey;
        this.getTeamList();
        this.roleList = AppConstants.ROLES;
        this.statusDesc = { 'OPEN': 'OPEN', 'ASSGN': 'ASSIGNED' };
        this.roleId = this._authService.getCurrentUser();
        this.teamid = this.roleId.user.userprofile.teammemberassignment.teammember.teamid; 
        this.role = this._authService.getCurrentUser().role.name;
        this.paginationInfo.sortColumn = 'servicerequestnumber';
        this.paginationInfo.sortBy = 'desc';
        this.paginationInfo.pageSize = 10;
        this.tobeassignedPaginationInfo.pageSize = 10;
        this.formAssignInitilize();
        this.loadSupervisor();
        this.getAssignServiceCase(1, 'OPEN');

        this.initAssignServiceFormGroup();
        this.teamForm.controls['teamid'].patchValue(this.teamid);
    }

    private loadProgramAreaDropdowns(servicerequesttypekey: string) {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { servicerequestsubtypekey: servicerequesttypekey },
                    method: 'get',
                    nolimit: true
                }),
                'agencyprogramarea/list?filter'
            )
            .subscribe((result) => {
                if (result && Array.isArray(result) && result.length) {
                    this.programArea = result[0].programarea;
                    this.programSubArea = result[0].subprogram;
                }
            });
    }

    getAssignCase(selectPage: number, assigned: string) {
        this.assignedCaseForm.patchValue({
            serreqno: ''
        });
        this.assignedStatus = assigned;
        this.appealStatus = false;
        this.supervisorReviewStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamAssgine$.map((pageNumber) => {
            this.tobeassignedPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectAssignCase, page: pageNumber };
        });

        const searchSource = this.searchTermStreamAssignCase$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectAssignCase = searchTerm;
            return { search: searchTerm, page: 1 };
        });
        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectAssignCase,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: '',
                        assigned: this.assignedStatus,
                        sortcolumn: this.paginationInfo.sortColumn,
                        sortorder: this.paginationInfo.sortBy
                    };
                } else {
                    this.assignedSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno,
                        assigned: this.assignedStatus,
                        sortcolumn: 'reporteddate', sortorder: 'desc'
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.paginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.assignedSearchCriteria
                    }),
                    'Intakedastagings/getroutedda'
                );
            })
            .subscribe((result) => {
                result.data.map((item) => {
                    item.isCollapsed = true;
                });
                this.assingedCaseSummary = result.data;
                if (this.paginationInfo.pageNumber === 1) {
                    this.totalRecordsAssingedCase = result.count;
                }
            });
    }
    private loadSupervisor() {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INTR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorList = result['data'];
                this.assignedCaseForm.controls['securityusersid'].patchValue(this.roleId.user.userprofile.securityusersid);
            });
    }
    supervisorChange() {
        this.getAssignServiceCase(1, this.assignedStatus);
    }
    getTeamList() {
        this._commonService.getArrayList({
            method: 'get',
            page: 1,
            order: 'teamnumber asc',
            where: {
                activeflag: 1,
                teamtypekey: this.teamtypekey,
                teamid: null
            }
        }, 'manage/team/getteamlist?filter').subscribe((item) => {
            this.teamList = item;
        });
    }
    teamChange() {
        this.getRoutingUserforServiceCase(this.serviceCaseDetails);
    }
    pageAssignedChanged(pageInfo: any) {
        this.tobeassignedPaginationInfo.pageNumber = pageInfo.page;
        this.tobeassignedPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.tobeassignedPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        // this.pageStream$.next(this.previousPage);
        this.getAssignCase(this.tobeassignedPaginationInfo.pageNumber, this.assignedStatus);
    }
    formAssignInitilize() {
        this.assignedCaseForm = this.formBuilder.group({
            serreqno: [''],
            securityusersid: ['']
        });
        this.teamForm = this.formBuilder.group({
            teamid: ['']
        });
    }
    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortDirection;
        this.paginationInfo.sortColumn = $event.sortColumn;
        this.getAssignServiceCase(this.tobeassignedPaginationInfo.pageNumber, this.assignedStatus);
        // this.getAssignCase(this.paginationInfo.pageNumber, false);
    }

    assignedToSupervisor(selectPage: number) {
        this.supervisorReviewStatus = true;
        this.appealStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamOnGoing$.map((pageNumber) => {
            this.onGoingPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectReviewToSupervisor, page: pageNumber };
        });

        const searchSource = this.searchTermStreamOnGoing$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectReviewToSupervisor = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectReviewToSupervisor,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: ''
                    };
                } else {
                    this.assignedSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.onGoingPaginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.assignedSearchCriteria
                    }),
                    DashBoard.EndPoint.AssinedCase.SupervisorReviewUrl
                );
            })
            .subscribe((result) => {
                this.reviewToSupervisor = result.data;
                if (this.onGoingPaginationInfo.pageNumber === 1) {
                    this.totalRecordsSupervisor = result.count;
                }
            });
    }
    onGoingPageChanged(pageInfo: any) {
        this.onGoingPaginationInfo.pageNumber = pageInfo.page;
        this.onGoingPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.onGoingPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        this.assignedToSupervisor(this.onGoingPaginationInfo.pageNumber);
    }
    caseReview(selectPage: number) {
        this.supervisorReviewStatus = true;
        this.appealStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamOnGoing$.map((pageNumber) => {
            this.reviewPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectCaseReview, page: pageNumber };
        });

        const searchSource = this.searchTermStreamOnGoing$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectCaseReview = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectCaseReview,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: '',
                        eventcode: 'ASST'
                    };
                } else {
                    this.reviewSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno,
                        eventcode: 'ASST'
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.reviewPaginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.reviewSearchCriteria
                    }),
                    DashBoard.EndPoint.AssinedCase.CaseReviewUrl + '?filter'
                );
            })
            .subscribe((result: any) => {
                this.reviewCases = result.data.result;
                if (this.reviewPaginationInfo.pageNumber === 1) {
                    this.totalRecordsCaseReview = result.data.count;
                }
            });
    }

    caseReviewPageChanged(pageInfo: any) {
        this.reviewPaginationInfo.pageNumber = pageInfo.page;
        this.reviewPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.reviewPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        this.caseReview(this.reviewPaginationInfo.pageNumber);
    }
    getRoutingUser(modal: AssignedCase) {
        this.getServicereqid = modal.servicereqid;
        this.zipCode = modal.incidentlocation;
        this.getUsersList = [];
        this.workersList = [];
        this.originalUserList = [];
        this.isGroup = modal.isgroup;
        this.servreqsubtype = modal.servreqsubtype;
        this.getResponsibilityType();
        let appEvent = 'INVR';
        if (this.roleId.role.name === AppConstants.ROLES.KINSHIP_INTAKE_WORKER || this.roleId.role.name === AppConstants.ROLES.KINSHIP_SUPERVISOR) {
            appEvent = 'KINR';
        }
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
    }


    initAssignServiceFormGroup() {
        this.assignServiceCaseForm = this.formBuilder.group({
            programkey: [null],
            subprogramkey: [null]
        });
    }

    getRoutingUserforServiceCase(modal) {
        this.serviceCaseDetails = modal;
        this.getServicecaseid = modal.servicecaseid;
        this.getUsersList = [];
        this.originalUserList = [];
        this.isGroup = modal.isgroup;
        this.servreqsubtype = modal.servreqsubtype;
        this.getResponsibilityType();
        const appEvent = 'INVR';
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent, teamid: this.teamForm.controls['teamid'].value || null },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
        if (modal.program && Array.isArray(modal.program) && modal.program.length) {
            this.assignServiceCaseForm.patchValue({ 'programkey': modal.program[0].programkey });
            this.assignServiceCaseForm.patchValue({ 'subprogramkey': modal.subprogram[0].subprogramkey });
        }

        this.loadProgramAreaDropdowns(modal.servicerequesttypekey);
    }
    getResponsibilityType() {
        this._commonService.getArrayList({}, 'responsibilitytype/').subscribe((result) => {
            this.responsibilityTypeDropdownItems$ = result.map(r => {
                return {
                    text: r.typedescription,
                    value: r.responsibilitytypekey
                }
            });

        });
    }
    listUser(assigned: string) {
        this.selectedPerson = '';
        this.getUsersList = [];
        this.mergeUsersList = [];
        this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === false) {
                    this.isSupervisor = false;
                    return res;
                }
            });
            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        } else {
            this.selectedResponsibilityType = null;
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === true) {
                    this.isSupervisor = true;
                    return res;
                }
            });

            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        }
    }

    selectResponsibilityType(typevalue) {
        this.selectedResponsibilityType = typevalue;
    }

    assignServiceCaseUser() {
        if (this.workersList && this.workersList.length) {

            this.assignServiceCaseToUser();

        } else {
            this._alertService.warn('Please select a person');
        }
    }
    assignServiceCaseToUser() {
        const model = {
            appeventcode: 'SRVC',
            servicecaseid: this.getServicecaseid,
            assigneduserid: this.selectedPerson.userid,
            programkey: (this.assignServiceCaseForm.get('programkey').value) ? this.assignServiceCaseForm.get('programkey').value : null,
            subprogramkey: (this.assignServiceCaseForm.get('subprogramkey').value) ? this.assignServiceCaseForm.get('subprogramkey').value : null,
            assignedusers: this.workersList
        };

        const data = [];
        this.workersList.forEach((item) => {
            if (!item.responsibilitytypekey){
                data.push(item);
            }
        });
        if (!data.length) {
            this._commonService
            .create(model,
                'servicecase/assigncase'
            )
            .subscribe((result) => {
                this._alertService.success('Service Case assigned successfully!');
                this.getAssignServiceCase(1, 'OPEN');
                this.closeServiceCAsePopup();
            });
        } else {
            this._alertService.error('Please Fill Responsibility');
        }
    }

    closeServiceCAsePopup() {
        (<any>$('#intake-servicecaseassign')).modal('hide');
    }


    assignUser() {
        if (this.selectedPerson) {
            if (this.selectedResponsibilityType) {
                this.assignCaseToUser();
            } else {
                this._alertService.warn('Please select Responsibility');
            }
        } else {
            this._alertService.warn('Please select a person');
        }
    }
    assignCaseToUser() {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: {
                        appeventcode: 'SRVC',
                        serreqid: this.getServicereqid,
                        assigneduserid: this.selectedPerson.userid,
                        isgroup: this.isGroup,
                        responsibilitytypekey: this.selectedResponsibilityType
                    },
                    method: 'post'
                }),
                'Intakedastagings/routeda'
            )
            .subscribe((result) => {
                this._alertService.success('Case assigned successfully!');
                this.getAssignCase(1, 'OPEN');
                this.closePopup();
            });
    }
    closePopup() {
        (<any>$('#intake-caseassign')).modal('hide');
    }

    selectPerson(checkBox, row) {
        // this.selectedPerson = row;
        if (checkBox.checked) {
            const userData = { userid: row.userid, username: row.username };
            this.workersList.push(userData);
        } else {
            this.workersList = this.workersList.filter(worker => worker.userid !== row.userid);
        }
        console.log(this.workersList);

    }

    routToCaseWorker(item: AssignedCase) {
        this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        if (this.isGroupId === true) {
            this.serviceRequestId = item.intakeserviceid;
        } else {
            this.serviceRequestId = item.servicecaseid;
        }
        const daNumber = item.servicecasenumber.split(' ');
        this._commonService.getAll(CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.servicecaseid + '/casetype').subscribe((response) => {
            const dsdsActionsSummary = response[0];
            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            const currentUrl = '/pages/case-worker/' + this.serviceRequestId + '/' + daNumber[0] + '/dsds-action/person-cw';
            this._router.navigate([currentUrl]);
        });
    }

    onSearchAssignCase(field: string, value: string) {
        if (this.appealStatus === true) {
            this.dynamicObjectAppeal[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectAppeal[field];
            }
            this.searchTermStreamAppeal$.next(this.dynamicObjectAppeal);
        } else if (this.supervisorReviewStatus === true) {
            this.dynamicObjectReviewToSupervisor[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectReviewToSupervisor[field];
            }
            this.searchTermStreamOnGoing$.next(this.dynamicObjectReviewToSupervisor);
        } else {
            this.dynamicObjectAssignCase[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectAssignCase[field];
            }
            this.searchTermStreamAssignCase$.next(this.dynamicObjectAssignCase);
        }
    }

    getAssignServiceCase(selectPage: number, assigned: string, stage?: string) {
        if (stage === 'INTIAL') {
            this.assignedCaseForm.patchValue({
                serreqno: ''
            });
            this.assignedCaseForm.controls['securityusersid'].patchValue(this.roleId.user.userprofile.securityusersid);
        }
        this.assingedServiceCaseSummary = [];
        this.totalRecordsAssingedCase = 0;
        this.assignedStatus = assigned;
        this.appealStatus = false;
        this.supervisorReviewStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamAssgine$.map((pageNumber) => {
            this.tobeassignedPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectAssignCase, page: pageNumber };
        });

        const searchSource = this.searchTermStreamAssignCase$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectAssignCase = searchTerm;
            return { search: searchTerm, page: 1 };
        });
        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectAssignCase,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        servicecaseno: '',
                        status: this.assignedStatus,
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                        // sortcolumn: this.paginationInfo.sortColumn,
                        // sortorder: this.paginationInfo.sortBy
                    };
                } else {
                    this.assignedSearchCriteria = {
                        servicecaseno: this.assignedCaseForm.value.serreqno,
                        status: this.assignedStatus,
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                        // sortcolumn: 'reporteddate', sortorder: 'desc'
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.paginationInfo.pageSize,
                        page: selectPage,
                        method: 'get',
                        where: this.assignedSearchCriteria

                    }),
                    'Intakedastagings/servicecaseassignlist?filter'
                );
            })
            .subscribe((result) => {
                console.log(result);
                // result.data.map((item) => {
                //     item.isCollapsed = true;
                // });
                this.assingedCaseSummary = result;
                if (this.assingedCaseSummary && this.assingedCaseSummary.length) {
                    this.totalRecordsAssingedCase = result[0].totalcount;
                }
                if (this.assingedCaseSummary) {
                    if (assigned === 'OPEN') {
                        this.assingedServiceCaseSummary = this.assingedCaseSummary.filter(data =>
                            data.statustypekey === 'OPEN'
                        );
                    } else {
                        this.assingedServiceCaseSummary = this.assingedCaseSummary.filter(data =>
                            data.statustypekey !== 'OPEN'
                        );
                    }
                    this.assingedServiceCaseSummary.map(v => {
                        if (v.legalguardian && v.legalguardian.length) {
                          v.legalguardian = v.legalguardian.map(x => x.personname).join(', ');
                        } else {
                          v.legalguardian = '';
                        }
                      })
                }
                // if (this.paginationInfo.pageNumber === 1) {
                //     this.totalRecordsAssingedCase = result.count;
                // }
            });


    }
    pageServiceCaseAssignedChanged(pageInfo: any) {
        this.tobeassignedPaginationInfo.pageNumber = pageInfo.page;
        this.tobeassignedPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.tobeassignedPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        // this.pageStream$.next(this.previousPage);
        this.getAssignServiceCase(this.tobeassignedPaginationInfo.pageNumber, this.assignedStatus);
    }
    formServiceCaseAssignInitilize() {
        this.assignedCaseForm = this.formBuilder.group({
            serreqno: ['']
        });
    }
    onServiceCaseSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortDirection;
        this.paginationInfo.sortColumn = $event.sortColumn;
        //  this.getAssignCase(this.paginationInfo.pageNumber, false);
    }
    routeToCase() {
        this.router.navigate(['../cw-assign-case'], { relativeTo: this.route });
    }

    changeResponsibility(event, user) {
        console.log(event, user);
        const isWokerAvailable = this.workersList.find(worker => worker.userid === user.userid);
        if (isWokerAvailable) {
            this.workersList.forEach(worker => {
                if (worker.userid === user.userid) {
                    worker.responsibilitytypekey = event.value;
                }
            });
            this.workersList = this.workersList;
        } else {
            this._alertService.error('please select the worker');
            event.source.value = null;
        }
        console.log(this.workersList);

    }
}



