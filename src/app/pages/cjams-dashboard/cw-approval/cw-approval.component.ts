import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo, DynamicObject, PaginationRequest, DropdownModel } from '../../../@core/entities/common.entities';
import { AssignedCase, ReviewGridModal, ReviewCase, RoutingUser } from '../_entities/dashBoard-datamodel';
import { DashBoard } from '../cjams-dashboard-url.config';
import { Observable } from 'rxjs/Observable';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { AuthService, DataStoreService, SessionStorageService } from '../../../@core/services';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cw-approval',
    templateUrl: './cw-approval.component.html',
    styleUrls: ['./cw-approval.component.scss']
})
export class CwApprovalComponent implements OnInit {

    assignedCaseForm: FormGroup;
    selectedPerson: any;
    serviceRequestId: string;
    currentUrl: string;
    mergeUsersList: RoutingUser[] = [];
    assignedStatus: boolean;
    isSupervisor: boolean;
    selectedResponsibilityType: string;
    zipCodeIndex: number;
    appealStatus: boolean;
    getServicereqid: string;
    zipCode: string;
    isGroupId = false;
    isGroup = false;
    isCPSAR: boolean;
    servreqsubtype: string;
    supervisorReviewStatus: boolean;
    totalRecordsAssingedCase: number;
    getUsersList: RoutingUser[];
    originalUserList: RoutingUser[];
    totalRecordsCaseReview: number;
    totalRecordsSupervisor: number;
    assignedSearchCriteria: any;
    reviewSearchCriteria: any;
    previousPage: number;
    dynamicObjectAssignCase: DynamicObject = {};
    reviewPaginationInfo: PaginationInfo = new PaginationInfo();
    dynamicObjectReviewToSupervisor: DynamicObject = {};
    dynamicObjectCaseReview: DynamicObject = {};
    assingedCaseSummary: AssignedCase[];
    paginationInfo: PaginationInfo = new PaginationInfo();
    responsibilityTypeDropdownItems$: Observable<DropdownModel[]>;
    reviewToSupervisor: ReviewGridModal[];
    tobeassignedPaginationInfo: PaginationInfo = new PaginationInfo();
    onGoingPaginationInfo: PaginationInfo = new PaginationInfo();
    reviewCases: ReviewCase[] = [];
    roleId: AppUser;
    supervisorList: Array<any[]> = [];
    private pageStreamOnGoing$ = new Subject<number>();
    private searchTermStreamOnGoing$ = new Subject<DynamicObject>();
    constructor(
        private _commonService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _router: Router,
        private _dataStoreService: DataStoreService,
        private _intakeUtils: IntakeUtils,
        private _session: SessionStorageService) { }

    ngOnInit() {
        this.roleId = this._authService.getCurrentUser();
        this.paginationInfo.sortColumn = 'receiveddate';
        this.paginationInfo.sortBy = 'desc';
        this.formAssignInitilize();
        this.loadSupervisor();
        this.assignedToSupervisor(1);
        this._session.removeItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        this._session.removeItem(CASE_STORE_CONSTANTS.CASE_TYPE);
    }
    formAssignInitilize() {
        this.assignedCaseForm = this.formBuilder.group({
            serreqno: [''],
            securityusersid: ['']
        });
    }

    assignedToSupervisor(selectPage: number) {
        this.supervisorReviewStatus = true;
        this.appealStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamOnGoing$.map((pageNumber) => {
            this.onGoingPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectReviewToSupervisor, page: pageNumber };
        });

        const searchSource = this.searchTermStreamOnGoing$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectReviewToSupervisor = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectReviewToSupervisor,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: '',
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                    };
                } else {
                    this.assignedSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno,
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.onGoingPaginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.assignedSearchCriteria
                    }),
                    DashBoard.EndPoint.AssinedCase.SupervisorReviewUrl
                );
            })
            .subscribe((result) => {
                this.reviewToSupervisor = result.data;
                if (this.onGoingPaginationInfo.pageNumber === 1) {
                    this.totalRecordsSupervisor = result.count;
                }
            });
    }
    onGoingPageChanged(pageInfo: any) {
        this.onGoingPaginationInfo.pageNumber = pageInfo.page;
        this.onGoingPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.onGoingPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        this.assignedToSupervisor(this.onGoingPaginationInfo.pageNumber);
    }
    getRoutingUser(modal: AssignedCase) {
        this.getServicereqid = modal.servicereqid;
        this.zipCode = modal.incidentlocation;
        this.getUsersList = [];
        this.originalUserList = [];
        this.isGroup = modal.isgroup;
        this.isCPSAR = false;
        this.servreqsubtype = modal.servreqsubtype;
        if (modal.servreqsubtype === 'CPS-AR') {
            this.isCPSAR = true;
            this.getResponsibilityType();
        }
        let appEvent = 'INTR';
        if (this.roleId.role.name === AppConstants.ROLES.KINSHIP_INTAKE_WORKER || this.roleId.role.name === AppConstants.ROLES.KINSHIP_SUPERVISOR) {
            appEvent = 'KINR';
        }
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
    }
    getResponsibilityType() {
        this.responsibilityTypeDropdownItems$ = this._commonService.getArrayList({}, 'responsibilitytype').map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.typedescription,
                        value: res.responsibilitytypekey
                    })
            );
        });
    }
    private loadSupervisor() {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INTR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorList = result['data'];
                this.assignedCaseForm.controls['securityusersid'].patchValue(this.roleId.user.userprofile.securityusersid);
            });
    }
    supervisorChange() {
        this.assignedToSupervisor(1);
    }
    listUser(assigned: string) {
        this.selectedPerson = '';
        this.getUsersList = [];
        this.mergeUsersList = [];
        this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
            if (this.servreqsubtype === 'CPS-AR') {
                this.isCPSAR = true;
            }
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === false) {
                    this.isSupervisor = false;
                    return res;
                }
            });
            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        } else {
            this.isCPSAR = false;
            this.selectedResponsibilityType = null;
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === true) {
                    this.isSupervisor = true;
                    return res;
                }
            });

            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        }
    }
    routToCaseWorker(item: AssignedCase) {
        this._dataStoreService.setData('appevent', item.appevent);
        let caseTypeUrl = 'report-summary';
        if (item.appevent === 'PPLR' || item.appevent === 'GAYR') {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            const isCW = this._authService.isCW();
            if (isCW) {
            if (item.appevent === 'PPLR') {
                caseTypeUrl = 'sc-permanency-plan';
            }  if ( item.appevent === 'GAYR') {
                this._session.setItem('Placement-Review-Id', item.objectid);
                this._session.setItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID, item.permanencyplanid);
                caseTypeUrl = 'placement/placement-gap/annual-reviews';
            }
        } else {
                caseTypeUrl = 'placement-menu/placement/permanency-plan';
            }
        } else if (item.appevent === 'CORR') {
            caseTypeUrl = 'court/petition-detail';
        } else if (item.appevent === 'TPRR') {
            caseTypeUrl = 'sc-permanency-plan';
        } else if (item.appevent === 'PWCR') {
            caseTypeUrl = 'sdm';
        } else if (item.appevent === 'CHRR') {
            caseTypeUrl = 'child-removal/details';
        } else if (item.appevent === 'IHSA') {
            caseTypeUrl = 'in-home-service/service-agreement';
        } else if (item.appevent === 'SPLAN') {
            caseTypeUrl = 'service-plan/sc-gc';
        } else if (item.appevent === 'CPLAN3') {
            caseTypeUrl = 'case-plan/case-plan-three';
        } else if (item.appevent === 'CPLAN2') {
            caseTypeUrl = 'case-plan/case-plan-two';
        } else if (item.appevent === 'YTP') {
            caseTypeUrl = 'service-plan/youth-transition-plan';
        } else if (item.appevent === 'PLTR') {
            caseTypeUrl = 'sc-placements/list';
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            if (item) {
                this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            }
        }  else if (item.appevent === 'GADR') {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem('Placement-Gap-Disclosure-Id', item.objectid);
            caseTypeUrl = 'placement/placement-gap/disclosure-checklist';
            this._session.setItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID, item.permanencyplanid);
        } else if (item.appevent === 'GASR') {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem('Placement-Suspension-Id', item.objectid);
            this._session.setItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID, item.permanencyplanid);
            caseTypeUrl = 'placement/placement-gap/assignments';
        } else if (['GAARR', 'GAAR', 'GARR'].includes(item.appevent)) {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID, item.permanencyplanid);
            this._session.setItem('Placement-Agreement-Rate-Id', item.objectid);
            this._session.setItem(CASE_STORE_CONSTANTS.APPROVAL_EVENT_CODE, item.appevent);
            caseTypeUrl = 'placement/placement-gap/agreement';
        } else if ( item.appevent === 'ABLR' ) {
            this._session.setItem('transid', item.objectid);
            this._session.setItem('transkey', 'breakthelink');
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            caseTypeUrl = 'placement/adoption/break-the-line';
            this._session.setItem(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID, item.permanencyplanid);
        } else if ( item.appevent === 'ADPR' ) {
            this._session.setItem('transid', item.objectid);
            this._session.setItem('transkey', 'planning');
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID, item.permanencyplanid);
            caseTypeUrl = 'placement/adoption/planning/checklist';
        } else if ( item.appevent === 'AARR' || item.appevent === 'ASAR'  ||  item.appevent === 'ADSR' ) {
            this._session.setItem('transid', item.objectid);
            this._session.setItem(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID, item.permanencyplanid);
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            if (item.appevent === 'ASAR') {
                if (item.servreqtype === 'Adoption Case') {
                    this.routToCaseWorker1(item);
                    this._session.setItem('transkey', 'subsidy');
                    return;
                } else {
                caseTypeUrl = 'placement/adoption/adoption-subsidy/agreement';
                this._session.setItem('transkey', 'subsidy');
                }
            }
            if (item.appevent === 'AARR') {
                caseTypeUrl = 'placement/adoption/adoption-subsidy/agreement';
                this._session.setItem('transkey', 'agreementrate');
            }
            if (item.appevent === 'ADSR') {
                if (item.servreqtype === 'Adoption Case') {
                    this.routToCaseWorker1(item);
                    this._session.setItem('transkey', 'suspension');
                    return;
                }  else {
               caseTypeUrl = 'placement/adoption/adoption-subsidy/suspention-payment';
               this._session.setItem('transkey', 'suspension');
                }
            }
        } else if ( item.appevent === 'ADYR') {
            this._session.setItem('transid', item.objectid);
            this._session.setItem('transkey', 'annualreview');
            this._session.setItem('Placement-Review-Id', item.objectid);
            this.routToCaseWorker1(item);
            return;
        } else if ( item.appevent === 'TPRR') {
            this._session.setItem('Permanency-Plan-Id', item.objectid);
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID, item.permanencyplanid);
            caseTypeUrl = 'placement/adoption/tpr-recom';
        }  else if ( item.appevent === 'PCAUTH' && item.servreqtype === 'Service Case' ) {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem('PurchaseAuthorization', item.client_id);
            this._session.setItem('PurchaseAuthorizationServiceLog', item.service_log_id);
            caseTypeUrl = 'service-plan/service-log-activity/referred-services';
        }  else if ( item.appevent === 'PCAUTH' && item.servreqtype === 'CHILD' ) {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, false);
            this._session.setItem('PurchaseAuthorization', item.client_id);
            this._session.setItem('PurchaseAuthorizationServiceLog', item.service_log_id);
            caseTypeUrl = 'service-plan/service-log-activity/referred-services';
        } else if (item.appevent === 'SCDR') {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem(CASE_STORE_CONSTANTS.DISPOSITIONID_FOR_APPROVAL, item.objectid);
             caseTypeUrl = 'disposition';
        } else if (item.appevent === 'ACDR') {
            this.routToCaseWorker1(item);
            this._session.setItem('transkey', 'suspension');
        } else if (item.appevent === 'GAAP') {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            this._session.setItem(CASE_STORE_CONSTANTS.GAP_APPLICATION_ID, item.objectid);
            this._session.setItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID, item.permanencyplanid);
            caseTypeUrl = 'placement/placement-gap/application';
        } else if (item.appevent === 'EXPR') {
            this._session.setItem('Expungement_Obj_ID', item.objectid);
            caseTypeUrl = 'investigation-findings';
        } else if (item.appevent === 'INDR') {
            caseTypeUrl = 'disposition';
        } else if (item.appevent === 'ARSM') {
            caseTypeUrl = 'alternative-response-summary';
        } else {
            caseTypeUrl = 'report-summary';
        }

        if (this.isGroupId === true) {
            this.serviceRequestId = item.intakeserviceid;
        } else {
            this.serviceRequestId = item.servicereqid;
        }
        if (item.servreqtype === 'Service Case') {
            this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        }
        const daNumber = item.servicerequestnumber.split(' ');
        this._commonService.getById(daNumber[0], CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            const currentUrl = '/pages/case-worker/' + this.serviceRequestId + '/' + daNumber[0] + '/dsds-action/' + caseTypeUrl;
            this._router.navigate([currentUrl]);
            // if (item.appevent === 'PWCR') {
            //     caseTypeUrl = 'pages/newintake/my-newintake/sdm';
            //     this._router.navigate([caseTypeUrl]);
            // }else {
            //     this._router.navigate([currentUrl]);
            // }

        });
    }


    routToCaseWorker1(item) {
        this._session.setItem(CASE_STORE_CONSTANTS.CASE_TYPE, CASE_TYPE_CONSTANTS.ADOPTION);
        this._session.setItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID, item.adoptionplanningid);
        this._session.setItem(CASE_STORE_CONSTANTS.Adoption_START_DATE, item.reporteddate);
        this._session.setObj(CASE_STORE_CONSTANTS.CASE_DASHBOARD, item);
        this._session.setItem('ISSERVICECASE', false);
        if (item) {
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.CASE_DASHBOARD, item);
        }
        this._commonService.getById(item.servicereqid, CaseWorkerUrlConfig.EndPoint.Dashboard.AdoptionActionSummary).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
                this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                // TODO : DEB As discussed with management we are parking this link but we are working on separate branch after extensive testing this will
                // be available for user
                // if (item.statustypekey === 'Closed') {
                    let  currentUrl = '';
                    if (item.appevent === 'ASAR') {
                      currentUrl = '/pages/case-worker/' + item.servicereqid + '/' + item.servicerequestnumber + '/dsds-action/placement/adoption/adoption-subsidy/agreement';
                    } else if (item.appevent === 'ADSR') {
                    currentUrl = '/pages/case-worker/' + item.servicereqid + '/' + item.servicerequestnumber + '/dsds-action/placement/adoption/adoption-subsidy/suspention-payment';
                    } else if (item.appevent === 'ADYR') {
                        currentUrl = '/pages/case-worker/' + item.servicereqid + '/' + item.servicerequestnumber + '/dsds-action/placement/adoption/adoption-subsidy/adoption-annual-reviews';
                    } else if (item.appevent === 'ACDR') {
                        this._session.setItem(CASE_STORE_CONSTANTS.DISPOSITIONID_FOR_APPROVAL, item.objectid);
                        currentUrl = '/pages/case-worker/' + item.servicereqid + '/' + item.servicerequestnumber + '/dsds-action/disposition';
                    }
                    this._router.navigate([currentUrl]);
                // } else {
                //     const currentUrl = '/pages/case-worker/' + item.servicereqid + '/' + item.servicerequestnumber + '/dsds-action/adoption-persons';
                //     this._router.navigate([currentUrl]);
                // }
            }
        });
    }

    onSearchAssignCase(event, serviceCase) {
        this.assignedToSupervisor(1);
    }

}
