import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CwApprovalComponent } from './cw-approval.component';

describe('CwApprovalComponent', () => {
  let component: CwApprovalComponent;
  let fixture: ComponentFixture<CwApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CwApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CwApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
