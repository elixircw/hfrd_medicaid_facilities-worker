export class IntakeSummary {
    totalcount: number;
    id: number;
    intakenumber: number;
    datereceived: Date;
    timereceived: Date;
    narrative: string;
    raname: string;
    entityname: string;
    cruworkername: string;
    isreview: boolean;
    issupervisor: boolean;
    datesubmitted: Date;
    dateclosed: Date;
    remarks: string;
    timeleft: string;
    timeelapsed: boolean;
    sdm: SDM[];
    datereceivedeststr: string;
    jsondata: any;
    purpose: any;
}

export class SDM {
    ischildfatality: boolean;
    ismaltreatment: boolean;
}
export class AssignedCase {
    totalcount: number;
    servicereqid: string;
    servicecaseid: string;
    servicecasenumber: string;
    servicerequestnumber: string;
    servreqtype: string;
    servreqsubtype: string;
    reporteddate: Date;
    raname: string;
    servreqstatus: string;
    routedon: string;
    assignedto: string;
    assigned: boolean;
    dadetails: Dadetails;
    isgroup: boolean;
    incidentlocation: string;
    isCollapsed?: boolean;
    acceptdate: Date;
    intakeserviceid: string;
    intakenumber?: string;
    assistid: string;
    cjamspid: string;
    youth: string;
    casecondtions: any;
    offencecounty: any[];
    sdm: SDM[];
    appevent?: string;
    objectid?: string;
    typename?: string;
    intakeservreqtypekey?: string;
    client_id: string;
    permanencyplanid?: string;
    service_log_id?: any;
}

export class ReviewGridModal {
    totalcount: number;
    servicereqid: string;
    servicerequestnumber: string;
    servreqtype: string;
    servreqsubtype: string;
    reporteddate: Date;
    raname: string;
    servreqstatus: string;
    submitteon: Date;
    submitteduser: string;
    isgroup: boolean;
    commets: string;
    dadetails: string;
    intakenumber?: string;
    sdm: SDM[];
}

export class Dadetails {
    assignedon: string;
    assingeduser: string;
    classkey: string;
    clientname: string;
    intakeserreqstatustypekey: string;
    intakeserviceid: string;
    intakeservreqtypekey: string;
    isrouted: boolean;
    reporteddate: string;
    routeddate: string;
    servicerequestnumber: string;
}
export class RoutingUser {
    userid: string;
    teamname: string;
    workloads: string;
    username: string;
    agencykey: string;
    userrole: string;
    loadnumber: string;
    email: string;
    available: string;
    issupervisor: boolean;
    zipcode: number;
    worklocationcode: string;
    homelocationcode: string;
    totalcases: number;
    rolecode: string;
}

export class AssignedPerson {
    caseworker_name: string;
    loadnumber: string;
    teamname: string;
}
export class BroadCostMessage {
    userannouncementid: string;
    details: string;
    isaccepted: boolean;
}

export class PreIntakeAssign {
    canreopen: number;
    totalcount: number;
    id: number;
    intakenumber: string;
    datereceived: Date;
    timereceived: Date;
    narrative: string;
    raname: string;
    entityname: string;
    cruworkername: string;
    isreview: boolean;
    issupervisor: boolean;
    datesubmitted: Date;
    dateclosed: Date;
    remarks: string;
    reviewstatus: string;
    disposition: string;
    intakestatus: string;
    timeleft: string;
    jsondata: any;
}
export class Appeal {
    intakenumber: string;
    appealdate: Date;
    appealstatus: string;
    remarks: string;
    activeflag: number;
}
export class ReviewCase {
    intakeserviceid: string;
    servicerequestnumber: string;
    intakenumber: string;
    intakeservreqtypekey: string;
    classkey: string;
    reporteddate: string;
    clientname: string;
    asssignedon: string;
    assingeduser: string;
    remarks: string;
    typename: string;
    appevent: string;
    sdm: SDM[];
}

export class Servicecas {
    intakeserviceid: string;
    casenumber: string;
    intakenumber: string;
    servicetype: string;
    dasubtype: string;
    role: string;
    datecreated: Date;
    dateclosed?: any;
}

export class Cpsfinding {
    intakeserviceid: string;
    casenumber: string;
    intakenumber: string;
    servicetype: string;
    dasubtype: string;
    datecreated: Date;
    dateclosed?: any;
}

export class Prior {
    personid: string;
    firstname: string;
    lastname: string;
    dob: Date;
    gendertypekey: string;
    dateofdeath?: any;
    cjamspid: number;
    role: string;
    servicecases: Servicecas[];
    cpsfindings: Cpsfinding[];
}
