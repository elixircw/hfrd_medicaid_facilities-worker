import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs/Rx';
import * as moment from 'moment';

import {
  DynamicObject,
  PaginationInfo,
  PaginationRequest,
  DropdownModel
} from '../../../@core/entities/common.entities';
import { CommonHttpService, GenericService, DataStoreService, SessionStorageService } from '../../../@core/services';
import { AlertService } from '../../../@core/services/alert.service';
import { AuthService } from '../../../@core/services/auth.service';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import {
  AssignedCase,
  BroadCostMessage,
  IntakeSummary,
  PreIntakeAssign,
  RoutingUser,
  Appeal,
  ReviewCase
} from '../_entities/dashBoard-datamodel';
import { AppUser } from '../../../@core/entities/authDataModel';
import { NewUrlConfig } from '../../newintake/newintake-url.config';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { DashBoard } from '../cjams-dashboard-url.config';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dash-assign-case',
  templateUrl: './dash-assign-case.component.html',
  styleUrls: ['./dash-assign-case.component.scss']
})
export class DashAssignCaseComponent implements OnInit {
  intakeSummaryForm: FormGroup;
  assignedCaseForm: FormGroup;
  appealForm: FormGroup;
  preIntakeForm: FormGroup;
  paginationInfo: PaginationInfo = new PaginationInfo();
  appealPaginationInfo: PaginationInfo = new PaginationInfo();
  tobeassignedPaginationInfo: PaginationInfo = new PaginationInfo();
  preIntakePaginationInfo: PaginationInfo = new PaginationInfo();
  reviewPaginationInfo: PaginationInfo = new PaginationInfo();
  intakesSummary: IntakeSummary[];
  assingedCaseSummary: AssignedCase[];
  getUsersList: RoutingUser[];
  mergeUsersList: RoutingUser[] = [];
  getIntakeUsers: RoutingUser[];
  originalUserList: RoutingUser[];
  totalRecords: number;
  totalRecordsAssingedCase: number;
  totalRecordsPreIntake: number;
  totalRecordsCaseReview: number;
  dynamicObjectIntakeSummary: DynamicObject = {};
  dynamicObjectAssignCase: DynamicObject = {};
  dynamicObjectPreIntake: DynamicObject = {};
  dynamicObjectCaseReview: DynamicObject = {};
  currentStatus: string;
  previousPage: number;
  searchCriteria: any;
  assignedStatus: boolean;
  preIntakeStatus: string;
  selectedPerson: any;
  selectIntake: any;
  isSupervisor: boolean;
  ispreIntake: boolean;
  getServicereqid: string;
  getPreIntake: PreIntakeAssign;
  showBroadCostMessage: BroadCostMessage;
  preIntakeSummary: AssignedCase[];
  statusDropdownItems$: Observable<DropdownModel[]>;
  isGroup = false;
  zipCode: string;
  zipCodeIndex: number;
  folderTypes$: Observable<DropdownModel[]>;
  folderType: string;
  folderReasonTypes$: Observable<DropdownModel[]>;
  folderReasonType: string;
  openDate: string;
  openTime: string;
  notes: string;
  assistid: string;
  cjamspid: string;
  youth: string;
  offencecounty = [];
  reviewCases: ReviewCase[] = [];
  casecondtioncourtordersdescription: string;
  casesource: string;
  casesourcendisposition: string;
  notesPlaceholder = 'Notes...';
  isDjs = false;
  private searchTermStreamIntake$ = new Subject<DynamicObject>();
  private searchTermStreamAssignCase$ = new Subject<DynamicObject>();
  private searchTermStreamPreIntake$ = new Subject<DynamicObject>();
  private searchTermStreamOnGoing$ = new Subject<DynamicObject>();
  private pageStreamOnGoing$ = new Subject<number>();
  private pageStreamIntake$ = new Subject<number>();
  private pageStreamAssgine$ = new Subject<number>();
  private pageStreamPreIntake$ = new Subject<number>();
  // appealSaveButton: boolean;
  private appealIntakeReqId: string;
  assignedCase: AssignedCase;
  constructor(
    private _authService: AuthService,
    private _alertService: AlertService,
    private _commonService: CommonHttpService,
    private _appealService: GenericService<Appeal>,
    private formBuilder: FormBuilder,
    private _router: Router,
    private _intakeUtils: IntakeUtils,
    private _dataStoreService: DataStoreService
  ) {
    this._appealService.endpointUrl = 'intakeservicerequestappeal/add';
  }

  ngOnInit() {
    this.isDjs = this._authService.isDJS();
    this.folderType = '';
    this.paginationInfo.sortColumn = 'datesubmitted';
    this.paginationInfo.sortBy = 'desc';
    this.formIntakeSummaryInitilize();
    this.appealFormInitialize();
    this.formAssignInitilize();
    this.formPreIntakeInitilize();
    this.loadFolderType();
    // this.loadFolderReasonType();
    // this.getIntakeSummary(1, 'pending');
    this.getAssignCase(1, false);
    // this.getPreIntakeSummary(1, 'PREINRU', false);
    const role = this._authService.getCurrentUser();
    if (role.role.name === 'apcs' || role.role.name === 'provider') {
      this.getBroadCostMessage();
    }
    this.appealForm
      .get('dispositioncode')
      .valueChanges.subscribe(result => {
        if (result === 'ScreenOUT') {
          this.appealForm.patchValue({ dispositioncode: '' });
          this._alertService.error('Screen out can not be selected');
        }
      });
  }

  loadReasonType(folderType) {
    this.folderReasonTypes$ = this._commonService
      .getArrayList(
        {
          nolimit: true,
          where: {
            foldertypekey: folderType
          },
          method: 'get'
        }, NewUrlConfig.EndPoint.Intake.folderReasonTypeBasedOnFolderType
      )
      .map(result => {
        return result.map(
          res =>
            new DropdownModel({
              text: res.description,
              value: res.folderreasontypekey
            })
        );
      });
  }

  loadFolderType() {
    this.folderTypes$ = this._commonService
      .getArrayList(
        {}, NewUrlConfig.EndPoint.Intake.folderTypeList
      )
      .map(result => {
        return result.map(
          res =>
            new DropdownModel({
              text: res.description,
              value: res.foldertypekey
            })
        );
      });
  }

  // loadFolderReasonType() {
  //   this.folderReasonTypes$ = this._commonService
  //     .getArrayList(
  //       {}, NewUrlConfig.EndPoint.Intake.folderReasonTypeList
  //     )
  //     .map(result => {
  //       return result.map(
  //         res =>
  //           new DropdownModel({
  //             text: res.description,
  //             value: res.folderreasontypekey
  //           })
  //       );
  //     });
  // }

  appealFormInitialize() {
    this.appealForm = this.formBuilder.group({
      appealdate: ['', [Validators.required]],
      dispositioncode: ['', [Validators.required]],
      remarks: ['', [Validators.required]]
    });
  }
  formIntakeSummaryInitilize() {
    this.intakeSummaryForm = this.formBuilder.group({
      intakenumber: ['']
    });
  }
  formAssignInitilize() {
    this.assignedCaseForm = this.formBuilder.group({
      serreqno: ['']
    });
  }
  formPreIntakeInitilize() {
    this.preIntakeForm = this.formBuilder.group({
      intakenumber: ['']
    });
  }
  getIntakeSummary(selectPage: number, status: string) {
    this.currentStatus = status;
    const pageSource = this.pageStreamIntake$.map(pageNumber => {
      this.paginationInfo.pageNumber = pageNumber;
      return {
        search: this.dynamicObjectIntakeSummary,
        page: pageNumber
      };
    });

    const searchSource = this.searchTermStreamIntake$
      .debounceTime(1000)
      .map(searchTerm => {
        this.dynamicObjectIntakeSummary = searchTerm;
        return { search: searchTerm, page: 1 };
      });
    const source = pageSource
      .merge(searchSource)
      .startWith({
        search: this.dynamicObjectIntakeSummary,
        page: this.paginationInfo.pageNumber
      })
      .flatMap((params: { search: DynamicObject; page: number }) => {
        if (this.intakeSummaryForm.value.intakenumber === '') {
          this.searchCriteria = {
            intakenumber: '',
            status: this.currentStatus,
            sortcolumn: this.paginationInfo.sortColumn,
            sortorder: this.paginationInfo.sortBy
          };
        } else {
          this.searchCriteria = {
            intakenumber: this.intakeSummaryForm.value.intakenumber,
            status: this.currentStatus,
            sortcolumn: this.paginationInfo.sortColumn,
            sortorder: this.paginationInfo.sortBy
          };
        }
        return this._commonService.getPagedArrayList(
          new PaginationRequest({
            limit: this.paginationInfo.pageSize,
            page: selectPage,
            method: 'post',
            where: this.searchCriteria
          }),
          'Intakedastagings/listdadetails'
        );
      })
      .subscribe(result => {
        this.intakesSummary = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalRecords = result.count;
        }
      });
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    // this.pageStream$.next(this.paginationInfo.pageNumber);
    this.getIntakeSummary(
      this.paginationInfo.pageNumber,
      this.currentStatus
    );
  }

  onSorted($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.getIntakeSummary(
      this.paginationInfo.pageNumber,
      this.currentStatus
    );
  }

  onCaseSorted($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.getAssignCase(
      this.tobeassignedPaginationInfo.pageNumber,
      this.assignedStatus
    );
  }

  onSearch(field: string, value: string) {
    this.dynamicObjectIntakeSummary[field] = {
      like: '%25' + value + '%25'
    };
    if (!value) {
      delete this.dynamicObjectIntakeSummary[field];
    }
    this.searchTermStreamIntake$.next(this.dynamicObjectIntakeSummary);
  }

  onSortedCaseCloded($event: ColumnSortedEvent) {
    this.tobeassignedPaginationInfo.sortBy =
      $event.sortColumn + ' ' + $event.sortDirection;
    this.pageStreamAssgine$.next(
      this.tobeassignedPaginationInfo.pageNumber
    );
  }

  getAssignCase(selectPage: number, assigned: boolean) {
    this.assignedStatus = assigned;
    const pageSource = this.pageStreamAssgine$.map(pageNumber => {
      this.tobeassignedPaginationInfo.pageNumber = pageNumber;
      return { search: this.dynamicObjectAssignCase, page: pageNumber };
    });

    const searchSource = this.searchTermStreamAssignCase$
      .debounceTime(1000)
      .map(searchTerm => {
        this.dynamicObjectAssignCase = searchTerm;
        return { search: searchTerm, page: 1 };
      });
    const source = pageSource
      .merge(searchSource)
      .startWith({
        search: this.dynamicObjectAssignCase,
        page: this.tobeassignedPaginationInfo.pageNumber
      })
      .flatMap((params: { search: DynamicObject; page: number }) => {
        if (this.assignedCaseForm.value.serreqno === '') {
          this.searchCriteria = {
            serreqno: '',
            assigned: this.assignedStatus,
            sortcolumn: this.paginationInfo.sortColumn,
            sortorder: this.paginationInfo.sortBy
          };
        } else {
          this.searchCriteria = {
            serreqno: this.assignedCaseForm.value.serreqno,
            assigned: this.assignedStatus,
            sortcolumn: this.paginationInfo.sortColumn,
            sortorder: this.paginationInfo.sortBy
          };
        }
        return this._commonService.getPagedArrayList(
          new PaginationRequest({
            limit: this.tobeassignedPaginationInfo.pageSize,
            page: selectPage,
            method: 'post',
            where: this.searchCriteria
          }),
          'Intakedastagings/getroutedda'
        );
      })
      .subscribe(result => {
        result.data.map(item => {
          item.isCollapsed = true;
        });
        this.assingedCaseSummary = result.data;
        if (this.tobeassignedPaginationInfo.pageNumber === 1) {
          this.totalRecordsAssingedCase = result.count;
        }
      });
  }

  getAppeal(selectPage: number) {
    const pageSource = this.pageStreamAssgine$.map(pageNumber => {
      this.appealPaginationInfo.pageNumber = pageNumber;
      return { search: this.dynamicObjectAssignCase, page: pageNumber };
    });

    const searchSource = this.searchTermStreamAssignCase$
      .debounceTime(1000)
      .map(searchTerm => {
        this.dynamicObjectAssignCase = searchTerm;
        return { search: searchTerm, page: 1 };
      });

    const source = pageSource
      .merge(searchSource)
      .startWith({
        search: this.dynamicObjectAssignCase,
        page: this.appealPaginationInfo.pageNumber
      })
      .flatMap((params: { search: DynamicObject; page: number }) => {
        if (this.assignedCaseForm.value.serreqno === '') {
          this.searchCriteria = {
            serreqno: ''
          };
        } else {
          this.searchCriteria = {
            serreqno: this.assignedCaseForm.value.serreqno
          };
        }
        return this._commonService.getPagedArrayList(
          new PaginationRequest({
            limit: this.appealPaginationInfo.pageSize,
            page: selectPage,
            method: 'post',
            where: this.searchCriteria
          }),
          'Intakedastagings/getappealda'
        );
      })
      .subscribe(result => {
        this.assingedCaseSummary = result.data;
        if (this.appealPaginationInfo.pageNumber === 1) {
          this.totalRecordsAssingedCase = result.count;
        }
      });
  }


  caseReview(selectPage: number) {
    const pageSource = this.pageStreamOnGoing$.map((pageNumber) => {
      this.reviewPaginationInfo.pageNumber = pageNumber;
      return { search: this.dynamicObjectCaseReview, page: pageNumber };
    });

    const searchSource = this.searchTermStreamOnGoing$.debounceTime(1000).map((searchTerm) => {
      this.dynamicObjectCaseReview = searchTerm;
      return { search: searchTerm, page: 1 };
    });

    const source = pageSource
      .merge(searchSource)
      .startWith({
        search: this.dynamicObjectCaseReview,
        page: this.paginationInfo.pageNumber
      })
      .flatMap((params: { search: DynamicObject; page: number }) => {
        if (this.assignedCaseForm.value.serreqno === '') {
          this.searchCriteria = {
            serreqno: '',
            eventcode: 'INVR'
          };
        } else {
          this.searchCriteria = {
            serreqno: this.assignedCaseForm.value.serreqno,
            eventcode: 'INVR'
          };
        }
        return this._commonService.getPagedArrayList(
          new PaginationRequest({
            limit: this.reviewPaginationInfo.pageSize,
            page: selectPage,
            method: 'post',
            where: this.searchCriteria
          }),
          DashBoard.EndPoint.AssinedCase.CaseReviewUrl + '?filter'
        );
      })
      .subscribe((result: any) => {
        this.reviewCases = result.data.result;
        if (this.reviewPaginationInfo.pageNumber === 1) {
          this.totalRecordsCaseReview = result.data.count;
        }
      });
  }

  appealPageChanged(pageInfo: any) {
    this.appealPaginationInfo.pageNumber = pageInfo.page;
    this.appealPaginationInfo.pageSize = pageInfo.itemsPerPage;
    if (this.appealPaginationInfo.pageNumber !== 1) {
      this.previousPage = pageInfo.page;
    }
    // this.pageStream$.next(this.previousPage);
    this.getAppeal(this.appealPaginationInfo.pageNumber);
  }

  reviewPageChanged(pageInfo: any) {
    this.reviewPaginationInfo.pageNumber = pageInfo.page;
    this.reviewPaginationInfo.pageSize = pageInfo.itemsPerPage;
    if (this.reviewPaginationInfo.pageNumber !== 1) {
      this.previousPage = pageInfo.page;
    }
    this.caseReview(this.reviewPaginationInfo.pageNumber);
  }

  pageAssignedChanged(pageInfo: any) {
    this.tobeassignedPaginationInfo.pageNumber = pageInfo.page;
    this.tobeassignedPaginationInfo.pageSize = pageInfo.itemsPerPage;
    if (this.tobeassignedPaginationInfo.pageNumber !== 1) {
      this.previousPage = pageInfo.page;
    }
    // this.pageStream$.next(this.previousPage);
    this.getAssignCase(
      this.tobeassignedPaginationInfo.pageNumber,
      this.assignedStatus
    );
  }
  onSearchAssignCase(field: string, value: string) {
    this.dynamicObjectAssignCase[field] = { like: '%25' + value + '%25' };
    if (!value) {
      delete this.dynamicObjectAssignCase[field];
    }
    this.searchTermStreamAssignCase$.next(this.dynamicObjectAssignCase);
  }
  getRoutingUser(modal: AssignedCase) {
    this.folderType = '';
    this.folderReasonType = '';
    this.openDate = moment().format();
    this.openTime = moment().format('HH:mm');
    this.notes = '';
    this.getServicereqid = modal.servicereqid;
    this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    this.isGroup = modal.isgroup;
    this.assistid = modal.assistid;
    this.cjamspid = modal.cjamspid;
    this.offencecounty = modal.offencecounty;
    this.youth = modal.raname;
    this.assignedCase = modal;
    const casecondtion = (modal.casecondtions && modal.casecondtions.length) > 0 ? modal.casecondtions[0] : '';
    if (casecondtion) {
      this.casecondtioncourtordersdescription = casecondtion.casecondtioncourtorders ? casecondtion.casecondtioncourtorders.map(courtOrder => courtOrder.description) : [];
      this.casesource = casecondtion.casesource;
      this.casesourcendisposition = casecondtion.casesourcendisposition;
    }
    this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'INVR' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }
  getWorkLoad() {
    this.getUsersList = [];
    this.originalUserList = [];
    this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'INVR' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }
  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.mergeUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
      this.getUsersList.map(data => {
        if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
          this.mergeUsersList.push(data);
          this.zipCodeIndex = this.getUsersList.indexOf(data);
          this.getUsersList.splice(this.zipCodeIndex, 1);
        }
      });
      if (this.mergeUsersList !== undefined) {
        this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
      }

    } else {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === true) {
          this.isSupervisor = true;
          return res;
        }
      });

      this.getUsersList.map(data => {
        if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
          this.mergeUsersList.push(data);
          this.zipCodeIndex = this.getUsersList.indexOf(data);
          this.getUsersList.splice(this.zipCodeIndex, 1);
        }
      });
      if (this.mergeUsersList !== undefined) {
        this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
      }
    }
  }
  selectPerson(row) {
    this.selectedPerson = row;
  }
  assignUser() {
    // const dateTime: any = new Date(this.openDate);
    // const timeSplit = this.openTime.split(':');
    // let timeHour: number = Number(timeSplit[0]);
    // const timeMinPlusMeridiem = timeSplit[1];
    // const timeMinAndMeridiem = timeMinPlusMeridiem.split(' ');
    // const timeMin = timeMinAndMeridiem[0];
    // const meridiem = timeMinAndMeridiem[1];

    // if (meridiem === 'PM') {
    //   timeHour += 12;
    // }

    // dateTime.setHours(timeHour);
    // dateTime.setMinutes(timeMin);
    this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          where: {
            appeventcode: 'INVT',
            serreqid: this.getServicereqid,
            assigneduserid: this.selectedPerson.userid,
            isgroup: this.isGroup,
            assignfolder: {
              foldertypekey: this.folderType,
              folderreasontypekey: this.folderReasonType,
              foldernotes: this.notes,
              folderopendatetime: this.openDate
            }
          },
          method: 'post'
        }),
        'Intakedastagings/routeda'
      )
      .subscribe(result => {
        this._alertService.success('Case assigned successfully!');
        this.getAssignCase(1, false);
        this.closePopup();
      });
  }

  openNarrative() {
    if (this.selectedPerson && this.folderType && this.folderReasonType && this.openDate && this.openTime) {
      (<any>$('#intake-caseassign')).modal('hide');
      (<any>$('#narrative')).modal('show');
    } else {
      this._alertService.warn('Please fill all fields.');
    }
  }

  getPreIntakeSummary(
    selectPage: number,
    status: string,
    ispreintake: boolean
  ) {
    this.preIntakeStatus = status;
    this.ispreIntake = ispreintake;
    const pageSource = this.pageStreamPreIntake$.map(pageNumber => {
      this.paginationInfo.pageNumber = pageNumber;
      return { search: this.dynamicObjectPreIntake, page: pageNumber };
    });

    const searchSource = this.searchTermStreamPreIntake$
      .debounceTime(1000)
      .map(searchTerm => {
        this.dynamicObjectPreIntake = searchTerm;
        return { search: searchTerm, page: 1 };
      });
    const source = pageSource
      .merge(searchSource)
      .startWith({
        search: this.dynamicObjectPreIntake,
        page: this.paginationInfo.pageNumber
      })
      .flatMap((params: { search: DynamicObject; page: number }) => {
        if (this.preIntakeForm.value.intakenumber === '') {
          this.searchCriteria = {
            ispreintake: true,
            intakenumber: '',
            status: status
          };
        } else {
          this.searchCriteria = {
            ispreintake: true,
            intakenumber: this.preIntakeForm.value.intakenumber,
            status: status
          };
        }
        return this._commonService.getPagedArrayList(
          new PaginationRequest({
            limit: this.paginationInfo.pageSize,
            page: selectPage,
            method: 'post',
            where: this.searchCriteria
          }),
          'Intakedastagings/listdadetails'
        );
      })
      .subscribe(result => {
        this.preIntakeSummary = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalRecordsPreIntake = result.count;
        }
      });
  }

  pageChangedPreIntake(pageInfo: any) {
    this.preIntakePaginationInfo.pageNumber = pageInfo.page;
    this.preIntakePaginationInfo.pageSize = pageInfo.itemsPerPage;
    this.getPreIntakeSummary(
      this.preIntakePaginationInfo.pageNumber,
      this.preIntakeStatus,
      this.ispreIntake
    );
  }

  onSortedPreIntake($event: ColumnSortedEvent) {
    this.preIntakePaginationInfo.sortBy =
      $event.sortColumn + ' ' + $event.sortDirection;
    this.pageStreamPreIntake$.next(this.preIntakePaginationInfo.pageNumber);
  }
  onSearchPreIntake(field: string, value: string) {
    this.dynamicObjectPreIntake[field] = { like: '%25' + value + '%25' };
    if (!value) {
      delete this.dynamicObjectPreIntake[field];
    }
    this.searchTermStreamPreIntake$.next(this.dynamicObjectPreIntake);
  }
  getAssignIntakeUser(modal: PreIntakeAssign) {
    this.getPreIntake = modal;
    this.selectIntake = null;
    this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'SITR' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getIntakeUsers = result.data;
        this.getIntakeUsers = this.getIntakeUsers.filter(res => {
          if (res.issupervisor === false) {
            this.isSupervisor = false;
            return res;
          }
        });
      });
  }
  selectIntaker(row) {
    this.selectIntake = row;
  }
  assignIntaker(modal) {
    if (this.selectIntake) {
      this._commonService
        .getPagedArrayList(
          new PaginationRequest({
            where: {
              appeventcode: modal,
              intakenumber: this.getPreIntake.intakenumber,
              assigneduserid: this.selectIntake.userid
            },
            method: 'post'
          }),
          'Intakedastagings/assignintake'
        )
        .subscribe(result => {
          this._alertService.success('Intake assigned successfully!');
          if (modal === 'SITR') {
            this.getPreIntakeSummary(1, 'PREINRU', false);
          } else if (modal === 'INTR') {
            this.getIntakeSummary(1, 'approved');
            $('#case-complete-tab').click();
          }
          this.closePopup();
        });
    } else {
      this._alertService.warn('Please select a person');
    }
  }

  closePopup() {
    (<any>$('#intake-caseassign')).modal('hide');
    (<any>$('#assign-preintake')).modal('hide');
    (<any>$('#reopen-intake')).modal('hide');
    (<any>$('#narrative')).modal('hide');
  }
  routToIntake(intakeId: string) {

    this._intakeUtils.redirectIntake(intakeId);
  }
  getBroadCostMessage() {
    this._commonService
      .getSingle({}, 'announcement/getuserannouncement')
      .subscribe(result => {
        if (result !== null) {
          (<any>$('#broadcoastmessage')).modal('show');
          this.showBroadCostMessage = result;
        }
      });
  }
  acceptAnnouncement() {
    this._commonService.endpointUrl = 'announcement/acceptannouncement';
    (<any>$('#broadcoastmessage')).modal('hide');
    this._commonService
      .patch(this.showBroadCostMessage.userannouncementid, {
        id: this.showBroadCostMessage.userannouncementid
      })
      .subscribe(data => { });
  }
  openAppeal(modal) {
    this.appealIntakeReqId = modal.servicereqid;
    this.statusDropdownItems$ = this._commonService
      .getArrayList(
        new PaginationRequest({
          nolimit: true,
          where: {
            intakeserviceid: '7da1e14f-6714-4cc4-b2ef-ae8d9af53959',
            statuskey: 'Approved'
          },
          method: 'get'
        }),
        'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
      )
      .map(result => {
        return result.map(
          res =>
            new DropdownModel({
              text: res.description,
              value: res.dispositioncode
            })
        );
      });
  }
  saveAppeal() {
    const appeal = this.appealForm.value;
    appeal.intakeserviceid = this.appealIntakeReqId;
    appeal.status = 'Approved';
    this._appealService.create(appeal).subscribe(
      result => {
        this._alertService.success(
          'Your intake has been appealed successfully.'
        );
        $('#case-appeal').click();
        this.appealForm.reset();
        this.getAppeal(1);
        (<any>$('#appeal-modal')).modal('hide');
      },
      err => {
        this._alertService.success(
          'Unable to appeal your intake. Please try again!'
        );
      }
    );
  }

  groupShow(row, id) {
    this.assingedCaseSummary[row].isCollapsed = !this.assingedCaseSummary[
      row
    ].isCollapsed;
  }

  routeReviewCase(item: AssignedCase) {
    const serviceRequestId = item.intakeserviceid;
    this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
      const dsdsActionsSummary = response[0];
      this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
      this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
      const currentUrl = '/pages/case-worker/' + serviceRequestId + '/' + item.servicerequestnumber + '/dsds-action/report-summary-djs';

      this._router.navigate([currentUrl]);
    });
  }
}
