import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo, DynamicObject, PaginationRequest, DropdownModel } from '../../../@core/entities/common.entities';
import { AssignedCase, ReviewGridModal, ReviewCase, RoutingUser, Appeal } from '../_entities/dashBoard-datamodel';
import { DashBoard } from '../cjams-dashboard-url.config';
import { Observable } from 'rxjs/Observable';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { AuthService, AlertService, DataStoreService, GenericService } from '../../../@core/services';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cw-assign-case',
    templateUrl: './cw-assign-case.component.html',
    styleUrls: ['./cw-assign-case.component.scss']
})
export class CwAssignCaseComponent implements OnInit {
    assignedCaseForm: FormGroup;
    appealForm: FormGroup;
    selectedPerson: any;
    serviceRequestId: string;
    mergeUsersList: RoutingUser[] = [];
    assignedStatus: boolean;
    isSupervisor: boolean;
    selectedResponsibilityType: string;
    zipCodeIndex: number;
    appealStatus: boolean;
    getServicereqid: string;
    selectedCaseForAssignment: string;
    zipCode: string;
    isGroupId = false;
    isGroup = false;
    servreqsubtype: string;
    supervisorReviewStatus: boolean;
    totalRecordsAssingedCase: number;
    getUsersList: RoutingUser[];
    originalUserList: RoutingUser[];
    totalRecordsCaseReview: number;
    totalRecordsSupervisor: number;
    assignedSearchCriteria: any;
    reviewSearchCriteria: any;
    previousPage: number;
    statusDropdownItems$: Observable<DropdownModel[]>;
    appealPaginationInfo: PaginationInfo = new PaginationInfo();
    dynamicObjectAssignCase: DynamicObject = {};
    dynamicObjectAppeal: DynamicObject = {};
    reviewPaginationInfo: PaginationInfo = new PaginationInfo();
    dynamicObjectReviewToSupervisor: DynamicObject = {};
    dynamicObjectCaseReview: DynamicObject = {};
    assingedCaseSummary: AssignedCase[];
    paginationInfo: PaginationInfo = new PaginationInfo();
    responsibilityTypeDropdownItems$: Observable<DropdownModel[]>;
    reviewToSupervisor: ReviewGridModal[];
    tobeassignedPaginationInfo: PaginationInfo = new PaginationInfo();
    onGoingPaginationInfo: PaginationInfo = new PaginationInfo();
    reviewCases: ReviewCase[] = [];
    roleId: AppUser;
    teamList: Array<any> = [];
    private searchTermStreamAppeal$ = new Subject<DynamicObject>();
    private pageStreamAppeal$ = new Subject<number>();
    private pageStreamOnGoing$ = new Subject<number>();
    private searchTermStreamOnGoing$ = new Subject<DynamicObject>();
    private searchTermStreamAssignCase$ = new Subject<DynamicObject>();
    private pageStreamAssgine$ = new Subject<number>();
    private appealIntakeReqId: string;
    role: string;
    teamid: string;
    selectedteamid: string;
    teamForm: FormGroup;
    roleList: {};
    seletedUserData: AssignedCase;
    teamtypekey: string;
    status: string;
    supervisorList: Array<any[]> = [];
    constructor(
        private _commonService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _router: Router,
        private _dataStoreService: DataStoreService,
        private _intakeUtils: IntakeUtils,
        private _appealService: GenericService<Appeal>,
        private _alertService: AlertService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.teamtypekey = this._authService.getCurrentUser().user.userprofile.teamtypekey;
        this.getTeamList();
        this.roleList = AppConstants.ROLES;
        this.roleId = this._authService.getCurrentUser();
        this.teamid = this.roleId.user.userprofile.teammemberassignment.teammember.teamid;
        this.selectedteamid = this.teamid;
        this.role = this._authService.getCurrentUser().role.name;
        this.paginationInfo.sortColumn = 'servicerequestnumber';
        this.paginationInfo.sortBy = 'desc';
        this.formAssignInitilize();
        this.loadSupervisor();
        this.getAssignCase(1, false, this.status);
        this.appealFormInitialize();
        this.appealForm.get('dispositioncode').valueChanges.subscribe((result) => {
            if (result === 'ScreenOUT') {
                this.appealForm.patchValue({ dispositioncode: '' });
                this._alertService.error('Screen out can not be selected');
            }
        });
        this.teamForm.controls['teamid'].setValue(this.selectedteamid);
    }


    calculatedays(recieveddate) {
        const date2 = new Date(recieveddate);
        const date1 = new Date();
        const diff = Math.abs(date1.getTime() - date2.getTime());
        const servicedays = Math.ceil(diff / (1000 * 3600 * 24));
        return  servicedays;
    }
    getTeamList() {
        this._commonService.getArrayList({
            method: 'get',
            page: 1,
            order: 'teamnumber asc',
            where: {
                activeflag: 1,
                teamtypekey: this.teamtypekey,
                teamid: null
            }
        }, 'manage/team/getteamlist?filter').subscribe((items) => {
            this.teamList = items.map( item  => item);

         });
    }
    teamChange() {
        this.selectedteamid = this.teamForm.controls['teamid'].value;
        this.getRoutingUser(this.seletedUserData);
    }
    onClose() {
        this.selectedteamid = this.teamid; // reset
        this.teamForm.controls['teamid'].setValue(this.selectedteamid);
    }
    appealFormInitialize() {
        this.appealForm = this.formBuilder.group({
            appealdate: ['', [Validators.required]],
            dispositioncode: ['', [Validators.required]],
            remarks: ['', [Validators.required]]
        });
    }
    getAssignCase(selectPage: number, assigned: boolean, status?: string, stage?: string) {
        if (status === 'Closed' || status === 'Completed') {
            this.status = status;
        } else {
            this.status = null;
        }
        if (stage === 'Intial') {
            this.assignedCaseForm.patchValue({
                serreqno: ''
            });
            this.assignedCaseForm.controls['securityusersid'].patchValue(this.roleId.user.userprofile.securityusersid);
        }
        this.assignedStatus = assigned;
        this.appealStatus = false;
        this.supervisorReviewStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamAssgine$.map((pageNumber) => {
            this.tobeassignedPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectAssignCase, page: pageNumber };
        });

        const searchSource = this.searchTermStreamAssignCase$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectAssignCase = searchTerm;
            return { search: searchTerm, page: 1 };
        });
        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectAssignCase,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: '',
                        assigned: this.assignedStatus,
                        sortcolumn: this.paginationInfo.sortColumn,
                        sortorder: this.paginationInfo.sortBy,
                        status: this.status,
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                    };
                } else {
                    this.assignedSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno,
                        assigned: this.assignedStatus,
                        sortcolumn: 'reporteddate', sortorder: 'desc',
                        status: this.status,
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.paginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.assignedSearchCriteria
                    }),
                    'Intakedastagings/getroutedda'
                );
            })
            .subscribe((result) => {
                result.data.map((item) => {
                    item.isCollapsed = true;
                });
                this.assingedCaseSummary = result.data;
                if (this.paginationInfo.pageNumber === 1) {
                    this.totalRecordsAssingedCase = result.count;
                }
            });
    }
    pageAssignedChanged(pageInfo: any) {
        this.tobeassignedPaginationInfo.pageNumber = pageInfo.page;
        this.tobeassignedPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.tobeassignedPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        // this.pageStream$.next(this.previousPage);
        this.getAssignCase(this.tobeassignedPaginationInfo.pageNumber, this.assignedStatus, this.status);
    }
    formAssignInitilize() {
        this.assignedCaseForm = this.formBuilder.group({
            serreqno: [''],
            securityusersid: ['']
        });
        this.teamForm = this.formBuilder.group({
            teamid: ['']
        });
    }
    onSorted($event: ColumnSortedEvent) {
        this.paginationInfo.sortBy = $event.sortDirection;
        this.paginationInfo.sortColumn = $event.sortColumn;
        this.getAssignCase(this.paginationInfo.pageNumber, false, this.status);
    }

    assignedToSupervisor(selectPage: number) {
        this.supervisorReviewStatus = true;
        this.appealStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamOnGoing$.map((pageNumber) => {
            this.onGoingPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectReviewToSupervisor, page: pageNumber };
        });

        const searchSource = this.searchTermStreamOnGoing$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectReviewToSupervisor = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectReviewToSupervisor,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: ''
                    };
                } else {
                    this.assignedSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.onGoingPaginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.assignedSearchCriteria
                    }),
                    DashBoard.EndPoint.AssinedCase.SupervisorReviewUrl
                );
            })
            .subscribe((result) => {
                this.reviewToSupervisor = result.data;
                if (this.onGoingPaginationInfo.pageNumber === 1) {
                    this.totalRecordsSupervisor = result.count;
                }
            });
    }
    onGoingPageChanged(pageInfo: any) {
        this.onGoingPaginationInfo.pageNumber = pageInfo.page;
        this.onGoingPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.onGoingPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        this.assignedToSupervisor(this.onGoingPaginationInfo.pageNumber);
    }
    private loadSupervisor() {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INTR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorList = result['data'];
                this.assignedCaseForm.controls['securityusersid'].patchValue(this.roleId.user.userprofile.securityusersid);
            });
    }
    caseReview(selectPage: number) {
        this.supervisorReviewStatus = true;
        this.appealStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamOnGoing$.map((pageNumber) => {
            this.reviewPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectCaseReview, page: pageNumber };
        });

        const searchSource = this.searchTermStreamOnGoing$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectCaseReview = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectCaseReview,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: '',
                        eventcode: 'ASST'
                    };
                } else {
                    this.reviewSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno,
                        eventcode: 'ASST'
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.reviewPaginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.reviewSearchCriteria
                    }),
                    DashBoard.EndPoint.AssinedCase.CaseReviewUrl + '?filter'
                );
            })
            .subscribe((result: any) => {
                this.reviewCases = result.data.result;
                if (this.reviewPaginationInfo.pageNumber === 1) {
                    this.totalRecordsCaseReview = result.data.count;
                }
            });
    }
    supervisorChange() {
        this.getAssignCase(1, this.assignedStatus, this.status);
    }

    caseReviewPageChanged(pageInfo: any) {
        this.reviewPaginationInfo.pageNumber = pageInfo.page;
        this.reviewPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.reviewPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        this.caseReview(this.reviewPaginationInfo.pageNumber);
    }
    getRoutingUser(modal: AssignedCase) {
        this.seletedUserData = modal;
        this.getServicereqid = modal.servicereqid;
        this.selectedCaseForAssignment = modal.servicerequestnumber;
        this.zipCode = modal.incidentlocation;
        this.getUsersList = [];
        this.originalUserList = [];
        this.isGroup = modal.isgroup;
        this.servreqsubtype = modal.servreqsubtype;
        this.getResponsibilityType();
        let appEvent = 'INVR';
        if (this.roleId.role.name === AppConstants.ROLES.KINSHIP_INTAKE_WORKER || this.roleId.role.name === AppConstants.ROLES.KINSHIP_SUPERVISOR) {
            appEvent = 'KINR';
        }
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent , teamid: this.selectedteamid || null},
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
    }
    getAppealRoutingUser(modal: AssignedCase) {
        this.seletedUserData = modal;
        this.getServicereqid = modal.servicereqid;
        this.selectedCaseForAssignment = modal.servicerequestnumber;
        this.zipCode = modal.incidentlocation;
        this.getUsersList = [];
        this.originalUserList = [];
        this.isGroup = modal.isgroup;
        this.servreqsubtype = modal.servreqsubtype;
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'APPL' , teamid: this.selectedteamid || null},
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
    }
    getResponsibilityType() {
        this.responsibilityTypeDropdownItems$ = this._commonService.getArrayList({}, 'responsibilitytype/').map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.typedescription,
                        value: res.responsibilitytypekey
                    })
            );
        });
    }
    listUser(assigned: string) {
        this.selectedPerson = '';
        this.getUsersList = [];
        this.mergeUsersList = [];
        this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === false) {
                    this.isSupervisor = false;
                    return res;
                }
            });
            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        } else {
            this.selectedResponsibilityType = null;
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === true) {
                    this.isSupervisor = true;
                    return res;
                }
            });

            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        }
    }

    selectResponsibilityType(typevalue) {
        this.selectedResponsibilityType = typevalue;
    }

    assignUser() {
        if (this.selectedPerson) {
            if (this.selectedResponsibilityType) {
                this.assignCaseToUser();
            } else {
                this._alertService.warn('Please select Responsibility');
            }
        } else {
            this._alertService.warn('Please select a person');
        }
        this.onClose();
    }
    assignCaseToUser() {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: {
                        appeventcode: 'INVT',
                        serreqid: this.getServicereqid,
                        assigneduserid: this.selectedPerson.userid,
                        isgroup: this.isGroup,
                        responsibilitytypekey: this.selectedResponsibilityType
                    },
                    method: 'post'
                }),
                'Intakedastagings/routeda'
            )
            .subscribe((result) => {
                this._alertService.success('Case assigned successfully!');
                // Check if the assigned case was restricted, if yes then give access to the assigned worker also
                this.checkAndAssignRestrictedCase();
                this.getAssignCase(1, false, this.status);
                this.closePopup();
            });
    }

    assignAppealUser() {
        if (this.selectedPerson) {
            this.assignCaseToAppealUser();
        } else {
            this._alertService.warn('Please select a person');
        }
        this.onClose();
    }

    assignCaseToAppealUser() {
        this._commonService
            .create(
                   {
                        objectid:  this.getServicereqid,
                        intakeserviceid:  this.getServicereqid,
                        eventcode: 'APPL',
                        status: 'Review',
                        comments: '',
                        notifymsg: 'Appeal Review',
                        tosecurityusersid: this.selectedPerson.userid,
                        routeddescription: 'Appeal Review'
                    }, 'routing/routingupdate'
            ).subscribe((result) => {
                this._alertService.success('Case assigned successfully!');
                // Check if the assigned case was restricted, if yes then give access to the assigned worker also
                this.checkAndAssignRestrictedCase();
                this.getAssignCase(1, true, this.status);
                this.closePopup();
            });
    }

    closePopup() {
        (<any>$('#intake-caseassign')).modal('hide');
        (<any>$('#intake-caseassign-completed')).modal('hide');
    }

    selectPerson(row) {
        this.selectedPerson = row;
    }
    getAppeal(selectPage: number, appealStatus: boolean) {
        this.assignedCaseForm.patchValue({
            serreqno: ''
        });
        this.appealStatus = appealStatus;
        this.supervisorReviewStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamAppeal$.map((pageNumber) => {
            this.appealPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectAppeal, page: pageNumber };
        });

        const searchSource = this.searchTermStreamAppeal$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectAppeal = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectAppeal,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        serreqno: ''
                    };
                } else {
                    this.assignedSearchCriteria = {
                        serreqno: this.assignedCaseForm.value.serreqno
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.paginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.assignedSearchCriteria
                    }),
                    'Intakedastagings/getappealda'
                );
            })
            .subscribe((result) => {
                this.assingedCaseSummary = result.data;
                if (this.paginationInfo.pageNumber === 1) {
                    this.totalRecordsAssingedCase = result.count;
                }
            });
    }
    appealPageChanged(pageInfo: any) {
        this.appealPaginationInfo.pageNumber = pageInfo.page;
        this.appealPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.appealPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        // this.pageStream$.next(this.previousPage);
        this.getAppeal(this.appealPaginationInfo.pageNumber, this.appealStatus);
    }
    routToCaseWorker(item: AssignedCase) {
        if (this.isGroupId === true) {
            this.serviceRequestId = item.intakeserviceid;
        } else {
            this.serviceRequestId = item.servicereqid;
        }
        const daNumber = item.servicerequestnumber.split(' ');
        this._commonService.getById(daNumber[0], CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            const currentUrl = '/pages/case-worker/' + this.serviceRequestId + '/' + daNumber[0] + '/dsds-action/report-summary';
            this._router.navigate([currentUrl]);
        });
    }
    openAppeal(modal) {
        this.appealIntakeReqId = modal.servicereqid;
        this.statusDropdownItems$ = this._commonService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        intakeserviceid: '7da1e14f-6714-4cc4-b2ef-ae8d9af53959',
                        statuskey: 'Approved'
                    },
                    method: 'get'
                }),
                'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.dispositioncode
                        })
                );
            });
    }
    saveAppeal() {
        const appeal = this.appealForm.value;
        appeal.intakeserviceid = this.appealIntakeReqId;
        appeal.status = 'Approved';
        this._appealService.create(appeal).subscribe(
            (result) => {
                this._alertService.success('Your intake has been appealed successfully.');
                $('#case-appeal').click();
                this.appealForm.reset();
                this.getAppeal(1, true);
                (<any>$('#appeal-modal')).modal('hide');
            },
            (err) => {
                this._alertService.success('Unable to appeal your intake. Please try again!');
            }
        );
    }
    onSearchAssignCase(field: string, value: string) {
        if (this.appealStatus === true) {
            this.dynamicObjectAppeal[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectAppeal[field];
            }
            this.searchTermStreamAppeal$.next(this.dynamicObjectAppeal);
        } else if (this.supervisorReviewStatus === true) {
            this.dynamicObjectReviewToSupervisor[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectReviewToSupervisor[field];
            }
            this.searchTermStreamOnGoing$.next(this.dynamicObjectReviewToSupervisor);
        } else {
            this.dynamicObjectAssignCase[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectAssignCase[field];
            }
            this.searchTermStreamAssignCase$.next(this.dynamicObjectAssignCase);
        }
    }
    routeToServiceCase () {
        this.router.navigate(['../cw-assign-service-case'], { relativeTo: this.route });
        }

    /**
     * Restricted case logic
     * If the just assigned case was restricted then 
     * the assigned case worker will be given access as well
     */
    checkAndAssignRestrictedCase() {
        let activeflag = 0 ;
        const selectedcaseworkerid = [];
        selectedcaseworkerid.push(this.selectedPerson.userid);
        this._intakeUtils.isRestrictedItem(this.getServicereqid)
        .subscribe(
            (response) => {
                if (response.length > 0) {
                    activeflag = 1;
                    // Means it's in restricted items list
                    this._intakeUtils.createRestrictedItem(this.getServicereqid, 'SERVICE', selectedcaseworkerid, activeflag)
                    .subscribe(
                        (response) => {
                            (<any>$('#restrict-item-assign-ack')).modal('show');
                            // this._alertService.success("The assigned person " + this.selectedPerson.username + " will now also have access to this restricted case: " + this.selectedCaseForAssignment);
                        }
                    )
                }
            }
        );
    }
}
