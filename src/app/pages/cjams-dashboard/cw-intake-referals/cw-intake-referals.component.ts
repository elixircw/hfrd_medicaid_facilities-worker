import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo, DynamicObject, PaginationRequest } from '../../../@core/entities/common.entities';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IntakeSummary, Prior } from '../_entities/dashBoard-datamodel';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { Observable } from 'rxjs/Rx';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import * as moment from 'moment';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';
import { SessionStorageService, AuthService } from '../../../@core/services';
import { IntakeConfigService } from '../../newintake/my-newintake/intake-config.service';
import { ActivatedRoute } from '@angular/router';
import { AppUser } from '../../../@core/entities/authDataModel';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'cw-intake-referals',
  templateUrl: './cw-intake-referals.component.html',
  styleUrls: ['./cw-intake-referals.component.scss']
})
export class CwIntakeReferalsComponent implements OnInit {
  paginationInfo: PaginationInfo = new PaginationInfo();
  intakeSummaryForm: FormGroup;
  dynamicObjectIntakeSummary: DynamicObject = {};
  currentStatus: string;
  intakesSummary: IntakeSummary[];
  totalRecords: number;
  intakeSearchCriteria: any;
  private searchTermStreamIntake$ = new Subject<DynamicObject>();
  private pageStreamIntake$ = new Subject<number>();
  priorItem$: Observable<Prior[]>;
  supervisorList: Array<any[]> = [];
  currentUser: AppUser;

  constructor(private _commonService: CommonHttpService,
    private formBuilder: FormBuilder,
    private _intakeUtils: IntakeUtils,
    private _session: SessionStorageService,
    private _intakeConfig: IntakeConfigService,
    private route: ActivatedRoute,
    private _authService: AuthService
    ) {
        this._intakeConfig.setPurposeList(this.route.snapshot.data.purposeList);
    }

  ngOnInit() {
    this.currentUser = this._authService.getCurrentUser();
    console.log(this.currentUser);
    this.paginationInfo.sortColumn = 'datesubmitted';
    this.paginationInfo.sortBy = 'desc';
    this.loadSupervisor();
    this.formIntakeSummaryInitilize();
    this.getIntakeSummary(1, 'pendingreview');
    this._session.removeItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    this._session.removeItem(CASE_STORE_CONSTANTS.CASE_TYPE);
  }
  getPrior(intakenumber) {
    this.priorItem$ = this._commonService
    .getArrayList(
        {
            where: {'intakenumber' : intakenumber},
            method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.Dashboard.GetPrior + '?filter'
    );
}
supervisorChange() {
    this.getIntakeSummary(1, this.currentStatus);
}
  getIntakeSummary(selectPage: number, status: string, stage?: string) {
      if (stage === 'Initial') {
        this.intakeSummaryForm.controls['intakenumber'].patchValue('');
        this.intakeSummaryForm.controls['securityusersid'].patchValue(this.currentUser.user.userprofile.securityusersid);
      }
    this.currentStatus = status;
    const pageSource = this.pageStreamIntake$.map((pageNumber) => {
        this.paginationInfo.pageNumber = pageNumber;
        return {
            search: this.dynamicObjectIntakeSummary,
            page: pageNumber
        };
    });

    const searchSource = this.searchTermStreamIntake$.debounceTime(1000).map((searchTerm) => {
        this.dynamicObjectIntakeSummary = searchTerm;
        return { search: searchTerm, page: 1 };
    });
    const source = pageSource
        .merge(searchSource)
        .startWith({
            search: this.dynamicObjectIntakeSummary,
            page: this.paginationInfo.pageNumber
        })
        .flatMap((params: { search: DynamicObject; page: number }) => {
            if (this.intakeSummaryForm.value.intakenumber === '') {
                this.intakeSearchCriteria = {
                    intakenumber: '',
                    status: this.currentStatus,
                    sortcolumn: this.paginationInfo.sortColumn,
                    sortorder: this.paginationInfo.sortBy,
                    securityusersid: (this.intakeSummaryForm.value.securityusersid === '') ? this.currentUser.user.userprofile.securityusersid : this.intakeSummaryForm.value.securityusersid
                };
            } else {
                this.intakeSearchCriteria = {
                    intakenumber: this.intakeSummaryForm.value.intakenumber,
                    status: this.currentStatus,
                    sortcolumn: this.paginationInfo.sortColumn,
                    sortorder: this.paginationInfo.sortBy,
                    securityusersid: (this.intakeSummaryForm.value.securityusersid === '') ? this.currentUser.user.userprofile.securityusersid : this.intakeSummaryForm.value.securityusersid
                };
            }
            return this._commonService.getPagedArrayList(
                new PaginationRequest({
                    limit: this.paginationInfo.pageSize,
                    page: selectPage,
                    method: 'post',
                    where: this.intakeSearchCriteria
                }),
                'Intakedastagings/listdadetails'
            );
        })
        .subscribe((result) => {
            const timeLevels = {
                scale: [24, 60, 60, 1],
                units: ['d ', 'h ', 'm ', 's ']
            };
            this.intakesSummary = result.data;
            let i = 0;
            for (const intake of this.intakesSummary) {
                const datereceivedutc = moment(intake.datereceived).format('MM/DD/YYYY, h:mm A');
                this.intakesSummary[i].datereceivedeststr = datereceivedutc;
                i = i + 1;
            }

            this.intakesSummary = this.intakesSummary.map((item) => {

                const timeleft = item.timeleft;
                if (timeleft) {
                    item.timeelapsed = false;
                    let _timeleft = timeleft.split(':');
                    if (_timeleft.length > 1) {
                        if (Number(_timeleft[0]) < 0) {
                            item.timeelapsed = true;
                        }
                        item.timeleft = Math.abs(Number(_timeleft[0])) + ' Hr(s) ' + _timeleft[1] + ' Min(s)';
                    } else {
                        _timeleft = timeleft.split(' ');
                        if (_timeleft.length > 1) {
                            if (Number(_timeleft[0]) < 0) {
                                item.timeelapsed = true;
                            }
                            if (_timeleft[1].startsWith('d')) {
                                item.timeleft = Math.abs(Number(_timeleft[0])) + ' Day(s)';
                            } else if (_timeleft[1].startsWith('m')) {
                                item.timeleft = Math.abs(Number(_timeleft[0])) + ' Month(s)';
                            }
                        }
                    }
                }

                //@Simar - map the purpose for each intake
                // We are computing this Purpose object on the UI itself based on the jsondata object
                item.purpose = {}

                if (item.jsondata) {
                    const purposeitem = item.jsondata.General.Purpose;
                    const purposeid = purposeitem.split('~')[0];
                    item.purpose = this._intakeConfig.getSelectedPurpose(purposeid);
                }

                return item;
            });

            if (this.paginationInfo.pageNumber === 1) {
                this.totalRecords = result.count;
            }
        });
    }
    private loadSupervisor() {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INTR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorList = result['data'];
                this.intakeSummaryForm.controls['securityusersid'].patchValue(this.currentUser.user.userprofile.securityusersid);
            });
    }
    pageChanged(pageInfo: any) {
      this.paginationInfo.pageNumber = pageInfo.page;
      this.paginationInfo.pageSize = pageInfo.itemsPerPage;
      // this.pageStream$.next(this.paginationInfo.pageNumber);
      this.getIntakeSummary(this.paginationInfo.pageNumber, this.currentStatus);
    }
    onSorted($event: ColumnSortedEvent) {
      this.paginationInfo.sortBy = $event.sortDirection;
      this.paginationInfo.sortColumn = $event.sortColumn;
      this.getIntakeSummary(this.paginationInfo.pageNumber, this.currentStatus);
    }
    onSearch(field: string, value: string) {
        this.dynamicObjectIntakeSummary[field] = {
            like: '%25' + value + '%25'
        };
        if (!value) {
            delete this.dynamicObjectIntakeSummary[field];
        }
        this.searchTermStreamIntake$.next(this.dynamicObjectIntakeSummary);
    }
    routToIntake(intakeId: string) {
      this._session.setItem('ISINTAKE', true);
      this._intakeUtils.redirectIntake(intakeId);
      console.log(this._session.getItem('ISINTAKE'));
    }
    formIntakeSummaryInitilize() {
      this.intakeSummaryForm = this.formBuilder.group({
          intakenumber: [''],
          securityusersid: ['']
      });
    }
    validate(persons) {
        if (persons instanceof Array) {
          if (persons && persons.length) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
}
