import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDatepickerModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTableModule, MatTabsModule,
    MatTooltipModule, MatExpansionModule, MatIconModule, MatCheckboxModule, MatRadioModule
} from '@angular/material';
import { PaginationModule } from 'ngx-bootstrap/pagination/pagination.module';

import { SortTableModule } from '../../shared/modules/sortable-table/sortable-table.module';
import { CjamsDashboardRoutingModule } from './cjams-dashboard-routing.module';
import { CjamsDashboardComponent } from './cjams-dashboard.component';
import { DashAssignCaseComponent } from './dash-assign-case/dash-assign-case.component';
import { DashIntakeSummaryComponent } from './dash-intake-summary/dash-intake-summary.component';
import { DashPreIntakeComponent } from './dash-pre-intake/dash-pre-intake.component';
import { QuillModule } from 'ngx-quill';
import { IntakeUtils } from '../_utils/intake-utils.service';
import { CwIntakeReferalsComponent } from './cw-intake-referals/cw-intake-referals.component';
import { CwAssignCaseComponent } from './cw-assign-case/cw-assign-case.component';
import { CwApprovalComponent } from './cw-approval/cw-approval.component';
import { CwAssessmentComponent } from './cw-assessment/cw-assessment.component';
import { DjsReviewCaseComponent } from './djs-review-case/djs-review-case.component';
import { IvECasesComponent } from './iv-e-cases/iv-e-cases.component';
import { HomeDashboardModule } from '../home-dashboard/home-dashboard.module';
import { CwAssignServiceCaseComponent } from './cw-assign-service-case/cw-assign-service-case.component';
import { NgSelectModule } from '@ng-select/ng-select';
// import { AsIntakeReferralsComponent } from './as-intake-referrals/as-intake-referrals.component';
import { DashMyIntakeSummaryComponent } from './dash-my-intake-summary/dash-my-intake-summary.component';
import { CwWorkloadComponent } from './cw-workload/cw-workload.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { IntakeConfigService } from '../newintake/my-newintake/intake-config.service';
import { PurposeResolverService } from '../newintake/my-newintake/purpose-resolver.service';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { CwLdssInboxComponent } from './cw-ldss-inbox/cw-ldss-inbox.component';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        CommonModule,
        MatTabsModule,
        MatSelectModule,
        MatTableModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatTooltipModule,
        MatExpansionModule,
        MatIconModule,
        MatCheckboxModule,
        MatRadioModule,
        CjamsDashboardRoutingModule,
        SortTableModule,
        QuillModule,
        HomeDashboardModule,
        NgSelectModule,
        HighchartsChartModule,
        SharedPipesModule
    ],
    declarations: [CjamsDashboardComponent,
        DashPreIntakeComponent,
        DashIntakeSummaryComponent,
        IvECasesComponent,
        DashAssignCaseComponent,
        CwIntakeReferalsComponent,
        CwAssignCaseComponent,
        CwApprovalComponent,
        CwAssessmentComponent,
        DjsReviewCaseComponent,
        CwAssignServiceCaseComponent,
        // AsIntakeReferralsComponent,
        DashMyIntakeSummaryComponent,
        CwWorkloadComponent,
        CwLdssInboxComponent
    ],
    providers: [IntakeUtils, IntakeConfigService, PurposeResolverService]
})
export class CjamsDashboardModule { }
