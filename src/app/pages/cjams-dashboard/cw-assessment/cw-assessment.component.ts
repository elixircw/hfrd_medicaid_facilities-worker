import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo, DynamicObject, PaginationRequest, DropdownModel } from '../../../@core/entities/common.entities';
import { AssignedCase, ReviewGridModal, ReviewCase, RoutingUser } from '../_entities/dashBoard-datamodel';
import { DashBoard } from '../cjams-dashboard-url.config';
import { Observable } from 'rxjs/Observable';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { AuthService, DataStoreService, SessionStorageService } from '../../../@core/services';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cw-assessment',
    templateUrl: './cw-assessment.component.html',
    styleUrls: ['./cw-assessment.component.scss']
})
export class CwAssessmentComponent implements OnInit {

    assignedCaseForm: FormGroup;
    selectedPerson: any;
    mergeUsersList: RoutingUser[] = [];
    serviceRequestId: string;
    currentUrl: string;
    assignedStatus: boolean;
    isSupervisor: boolean;
    selectedResponsibilityType: string;
    zipCodeIndex: number;
    appealStatus: boolean;
    getServicereqid: string;
    zipCode: string;
    isGroupId = false;
    isGroup = false;
    isCPSAR: boolean;
    servreqsubtype: string;
    supervisorReviewStatus: boolean;
    totalRecordsAssingedCase: number;
    getUsersList: RoutingUser[];
    originalUserList: RoutingUser[];
    totalRecordsCaseReview: number;
    totalRecordsSupervisor: number;
    assignedSearchCriteria: any;
    reviewSearchCriteria: any;
    previousPage: number;
    dynamicObjectAssignCase: DynamicObject = {};
    reviewPaginationInfo: PaginationInfo = new PaginationInfo();
    dynamicObjectReviewToSupervisor: DynamicObject = {};
    dynamicObjectCaseReview: DynamicObject = {};
    assingedCaseSummary: AssignedCase[];
    paginationInfo: PaginationInfo = new PaginationInfo();
    responsibilityTypeDropdownItems$: Observable<DropdownModel[]>;
    reviewToSupervisor: ReviewGridModal[];
    tobeassignedPaginationInfo: PaginationInfo = new PaginationInfo();
    onGoingPaginationInfo: PaginationInfo = new PaginationInfo();
    reviewCases: ReviewCase[] = [];
    supervisorList: Array<any> = [];
    roleId: AppUser;
    private pageStreamOnGoing$ = new Subject<number>();
    private searchTermStreamOnGoing$ = new Subject<DynamicObject>();
    constructor(
        private _commonService: CommonHttpService,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _router: Router,
        private _dataStoreService: DataStoreService,
        private _intakeUtils: IntakeUtils,
        private _session: SessionStorageService) { }

    ngOnInit() {
        this.roleId = this._authService.getCurrentUser();
        this.paginationInfo.sortColumn = 'receiveddate';
        this.paginationInfo.sortBy = 'desc';
        this.formAssignInitilize();
        this.loadSupervisor();
        this.caseReview(1);
        this._session.removeItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        this._session.removeItem(CASE_STORE_CONSTANTS.CASE_TYPE);
    }

    formAssignInitilize() {
        this.assignedCaseForm = this.formBuilder.group({
            serreqno: [''],
            securityusersid: ['']
        });
    }

    caseReview(selectPage: number) {
        this.supervisorReviewStatus = true;
        this.appealStatus = false;
        this.isGroupId = false;
        const pageSource = this.pageStreamOnGoing$.map((pageNumber) => {
            this.reviewPaginationInfo.pageNumber = pageNumber;
            return { search: this.dynamicObjectCaseReview, page: pageNumber };
        });

        const searchSource = this.searchTermStreamOnGoing$.debounceTime(1000).map((searchTerm) => {
            this.dynamicObjectCaseReview = searchTerm;
            return { search: searchTerm, page: 1 };
        });

        const source = pageSource
            .merge(searchSource)
            .startWith({
                search: this.dynamicObjectCaseReview,
                page: this.paginationInfo.pageNumber
            })
            .flatMap((params: { search: DynamicObject; page: number }) => {
                if (this.assignedCaseForm.value.serreqno === '') {
                    this.assignedSearchCriteria = {
                        servicerequestnumber: '',
                        eventcode: 'ASST',
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                    };
                } else {
                    this.assignedSearchCriteria = {
                        servicerequestnumber: this.assignedCaseForm.value.serreqno,
                        eventcode: 'ASST',
                        securityusersid: (this.assignedCaseForm.value.securityusersid === '') ? this.roleId.user.userprofile.securityusersid : this.assignedCaseForm.value.securityusersid
                    };
                }
                return this._commonService.getPagedArrayList(
                    new PaginationRequest({
                        limit: this.reviewPaginationInfo.pageSize,
                        page: selectPage,
                        method: 'post',
                        where: this.assignedSearchCriteria
                    }),
                    DashBoard.EndPoint.AssinedCase.CaseReviewUrl + '?filter'
                );
            })
            .subscribe((result: any) => {
                this.reviewCases = result.data.result;
                if (this.reviewPaginationInfo.pageNumber === 1) {
                    this.totalRecordsCaseReview = result.data.count;
                }
            });
    }

    
    onSearchCase(field: string, value: string) {
        if (this.appealStatus === true) {
            this.dynamicObjectCaseReview[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectCaseReview[field];
            }
            this.searchTermStreamOnGoing$.next(this.dynamicObjectCaseReview);
        } else if (this.supervisorReviewStatus === true) {
            this.dynamicObjectReviewToSupervisor[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectReviewToSupervisor[field];
            }
            this.searchTermStreamOnGoing$.next(this.dynamicObjectReviewToSupervisor);
        } else {
            this.dynamicObjectAssignCase[field] = { like: '%25' + value + '%25' };
            if (!value) {
                delete this.dynamicObjectAssignCase[field];
            }
            this.searchTermStreamOnGoing$.next(this.dynamicObjectAssignCase);
        }
    }

    caseReviewPageChanged(pageInfo: any) {
        this.reviewPaginationInfo.pageNumber = pageInfo.page;
        this.reviewPaginationInfo.pageSize = pageInfo.itemsPerPage;
        if (this.reviewPaginationInfo.pageNumber !== 1) {
            this.previousPage = pageInfo.page;
        }
        this.caseReview(this.reviewPaginationInfo.pageNumber);
    }
    getRoutingUser(modal: AssignedCase) {
        this.getServicereqid = modal.servicereqid;
        this.zipCode = modal.incidentlocation;
        this.getUsersList = [];
        this.originalUserList = [];
        this.isGroup = modal.isgroup;
        this.isCPSAR = false;
        this.servreqsubtype = modal.servreqsubtype;
        if (modal.servreqsubtype === 'CPS-AR') {
            this.isCPSAR = true;
            this.getResponsibilityType();
        }
        let appEvent = 'INTR';
        if (this.roleId.role.name === AppConstants.ROLES.KINSHIP_INTAKE_WORKER || this.roleId.role.name === AppConstants.ROLES.KINSHIP_SUPERVISOR) {
            appEvent = 'KINR';
        }
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
    }
    private loadSupervisor() {
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INTR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorList = result['data'];
                this.assignedCaseForm.controls['securityusersid'].patchValue(this.roleId.user.userprofile.securityusersid);
            });
    }
    supervisorChange() {
        this.caseReview(1);
    }
    getResponsibilityType() {
        this.responsibilityTypeDropdownItems$ = this._commonService.getArrayList({}, 'responsibilitytype').map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.typedescription,
                        value: res.responsibilitytypekey
                    })
            );
        });
    }
    listUser(assigned: string) {
        this.selectedPerson = '';
        this.getUsersList = [];
        this.mergeUsersList = [];
        this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
            if (this.servreqsubtype === 'CPS-AR') {
                this.isCPSAR = true;
            }
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === false) {
                    this.isSupervisor = false;
                    return res;
                }
            });
            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        } else {
            this.isCPSAR = false;
            this.selectedResponsibilityType = null;
            this.getUsersList = this.getUsersList.filter((res) => {
                if (res.issupervisor === true) {
                    this.isSupervisor = true;
                    return res;
                }
            });

            this.getUsersList.map((data) => {
                if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
                    this.mergeUsersList.push(data);
                    this.zipCodeIndex = this.getUsersList.indexOf(data);
                    this.getUsersList.splice(this.zipCodeIndex, 1);
                }
            });
            if (this.mergeUsersList !== undefined) {
                this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
            }
        }
    }

    routeReviewCase(item: AssignedCase) {
        this.serviceRequestId = item.intakeserviceid;
        const daNumber = item.servicerequestnumber.split(' ');
        this._commonService.getById(daNumber[0], CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            if (item.intakeservreqtypekey === 'Service Case') {
                this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            }
            if (item.appevent === 'GADR') {
                this.currentUrl = '/pages/case-worker/' + this.serviceRequestId + '/' + daNumber[0] + '/dsds-action/placement/placement-gap/disclosure-checklist';
            } else {
                this._dataStoreService.setData('assesment-type', item.typename);
                this.currentUrl = '/pages/case-worker/' + this.serviceRequestId + '/' + daNumber[0] + '/dsds-action/assessment';
            }
            if (item.typename === 'APPLA') {
                this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
                this.currentUrl = '/pages/case-worker/' + this.serviceRequestId + '/' + daNumber[0] + '/sc-permanency-plan/placement/appla/list';
            }
            if(item.typename === 'Shelter Care Authorization and Date of Hearing'){
                this.currentUrl = '/pages/case-worker/' + this.serviceRequestId + '/' + daNumber[0] + '/dsds-action/child-removal/details';
            }

            this._router.navigate([this.currentUrl]);
        });
    }

}
