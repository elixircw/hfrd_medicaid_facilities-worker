import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashPreIntakeComponent } from './dash-pre-intake.component';

describe('DashPreIntakeComponent', () => {
  let component: DashPreIntakeComponent;
  let fixture: ComponentFixture<DashPreIntakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashPreIntakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashPreIntakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
