import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DjsPlacementWorkerRoutingModule } from './djs-placement-worker-routing.module';
import { DjsPlacementWorkerComponent } from './djs-placement-worker.component';
import { PaginationModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    DjsPlacementWorkerRoutingModule,
    PaginationModule,
    FormMaterialModule,
    MatTooltipModule
  ],
  declarations: [DjsPlacementWorkerComponent]
})
export class DjsPlacementWorkerModule { }
