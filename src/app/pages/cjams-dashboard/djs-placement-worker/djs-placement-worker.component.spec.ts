import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPlacementWorkerComponent } from './djs-placement-worker.component';

describe('DjsPlacementWorkerComponent', () => {
  let component: DjsPlacementWorkerComponent;
  let fixture: ComponentFixture<DjsPlacementWorkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPlacementWorkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPlacementWorkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
