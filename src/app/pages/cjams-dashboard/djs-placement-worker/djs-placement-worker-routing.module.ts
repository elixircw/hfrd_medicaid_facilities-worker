import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DjsPlacementWorkerComponent } from './djs-placement-worker.component';
import { AppConstants } from '../../../@core/common/constants';

const routes: Routes = [{
  path: '',
  component: DjsPlacementWorkerComponent
},
{
  path: 'placement-details/:intakenumber',
  loadChildren: './placement-details/placement-details.module#PlacementDetailsModule',
  data: {
    pageSource: AppConstants.PAGES.DJS_PLACEMENT_WORKER
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DjsPlacementWorkerRoutingModule { }
