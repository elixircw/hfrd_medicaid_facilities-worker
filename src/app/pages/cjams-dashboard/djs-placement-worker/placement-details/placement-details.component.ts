import { Component, OnInit } from '@angular/core';
import { DataStoreService, CommonHttpService, AuthService } from '../../../../@core/services';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { IntakeConfigService } from '../../../newintake/my-newintake/intake-config.service';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';

@Component({
  selector: 'placement-details',
  templateUrl: './placement-details.component.html',
  styleUrls: ['./placement-details.component.scss']
})
export class PlacementDetailsComponent implements OnInit {
  tabs = [
    {
      id: 'placement',
      title: 'Placement',
      name: 'Placement',
      route: 'placement'
    },
    {
      id: 'Document',
      title: 'Documents',
      name: 'Documents',
      route: 'document'
    }
  ];
  selectedYouth: any;
  intakeCommunication: any[] = [];
  purposeList: any[] = [];
  constructor(private _dataStoreService: DataStoreService,
    private route: ActivatedRoute, private _intakeConfig: IntakeConfigService,
    private _commonHttpService: CommonHttpService, private _authService: AuthService) {
  }

  ngOnInit() {
    this.getPurposeList();
    this.getCoummunication();
    setTimeout(() => {
      this.processSelectedYouth();
    }, 1000);
  }

  getPurposeList() {
    const checkInput = {
      nolimit: true,
      where: { teamtypekey: this._authService.getAgencyName() },
      method: 'get',
      order: 'description'
    };
    this._commonHttpService.getArrayList(new PaginationRequest(checkInput),
      NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter').subscribe(result => {
        this.purposeList = result;
      });
  }

  getCoummunication() {
    this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.IntakeServiceRequestInputTypeUrl).subscribe(result => {
      this.intakeCommunication = result;
    });
  }

  processSelectedYouth() {
    const intakeData = this._dataStoreService.getData('placement_intake');
    const addedPersons = intakeData.jsondata.persons;
    if (addedPersons) {
      const personYouth = addedPersons.find(person => person.Role === 'Youth');
      if (personYouth) {
        if (!this.selectedYouth || (personYouth.fullName !== this.selectedYouth.fullName)) {
          this.selectedYouth = personYouth;
        }
      } else {
        this.selectedYouth = null;
      }
    }
    this.selectedYouth.Author = intakeData.jsondata.General.Author;
    this.selectedYouth.RecivedDate = intakeData.jsondata.General.RecivedDate;
    this.selectedYouth.intakenumber = intakeData.intakenumber;
    this.selectedYouth.CreatedDate = intakeData.jsondata.General.CreatedDate;
    this.selectedYouth.purpose = intakeData.jsondata.General.Purpose;
    this.selectedYouth.InputSource = intakeData.jsondata.General.InputSource;
    const purpose = this.purposeList.find(data => data.intakeservreqtypeid === intakeData.jsondata.General.Purpose);
    const Source = this.intakeCommunication.find(data => data.intakeservreqinputtypeid === intakeData.jsondata.General.InputSource);
    this.selectedYouth.purposename = purpose ? purpose.description : '';
    this.selectedYouth.Sourcename = Source ? Source.description : '';
  }

}
