import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlacementDetailsComponent } from './placement-details.component';
import { AppConstants } from '../../../../@core/common/constants';

const routes: Routes = [{
  path: '',
  component: PlacementDetailsComponent,
  data: {
    pageSource: AppConstants.PAGES.DJS_PLACEMENT_WORKER
  },
  children: [
    {
      path: 'placement',
      loadChildren: 'app/pages/case-worker/dsds-action/djs-placement/djs-placement.module#DjsPlacementModule',
      data: {
        pageSource: AppConstants.PAGES.DJS_PLACEMENT_WORKER
      }
    },
    {
      path: 'document',
      loadChildren: 'app/pages/newintake/my-newintake/intake-attachments/intake-attachments.module#IntakeAttachmentsModule',
      data: {
        pageSource: AppConstants.PAGES.DJS_PLACEMENT_WORKER
      }
    },
    {
      path: '**',
      redirectTo: 'placement',
      pathMatch: 'full'
    }
  ]
},
{
  path: '**',
  redirectTo: ''
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlacementDetailsRoutingModule { }
