import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlacementDetailsRoutingModule } from './placement-details-routing.module';
import { PlacementDetailsComponent } from './placement-details.component';
import { IntakeConfigService } from '../../../newintake/my-newintake/intake-config.service';
import {
  MatAutocompleteModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatSelectModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { PurposeResolverService } from '../../../newintake/my-newintake/purpose-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    PlacementDetailsRoutingModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  declarations: [PlacementDetailsComponent],
  providers: [IntakeConfigService]
})
export class PlacementDetailsModule { }
