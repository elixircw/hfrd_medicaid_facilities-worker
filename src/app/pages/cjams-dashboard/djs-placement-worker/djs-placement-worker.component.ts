import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { Subject } from 'rxjs/Rx';
import * as moment from 'moment';
import { DynamicObject, PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { AuthService } from '../../../@core/services/auth.service';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { GenericService } from '../../../@core/services/generic.service';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { MyIntakeDetails } from '../../newintake/my-newintake/_entities/newintakeModel';
import { SearchCase } from '../../newintake/my-newintake/_entities/newintakeSaveModel';
import { NewUrlConfig } from '../../newintake/newintake-url.config';
import { Router } from '@angular/router';
import { DataStoreService } from '../../../@core/services';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { IntakeStoreConstants } from '../../newintake/my-newintake/my-newintake.constants';

declare var $: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'djs-placement-worker',
  templateUrl: './djs-placement-worker.component.html',
  styleUrls: ['./djs-placement-worker.component.scss']
})
export class DjsPlacementWorkerComponent implements OnInit {
  searchIntakeForm: FormGroup;
  paginationInfo: PaginationInfo = new PaginationInfo();
  intakes: MyIntakeDetails[];
  totalRecords: number;
  dynamicObject: DynamicObject = {};
  roleName: string;
  searchCriteria: any;
  private searchTermStream$ = new Subject<DynamicObject>();
  private pageStream$ = new Subject<number>();
  isDjs = false;
  status: string;
  agency: string;
  showHistory: boolean;
  previousPage: number;
  searchHistory = [];
  isATDWorker = false;
  userRole: AppUser;

  constructor(
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private _service: GenericService<MyIntakeDetails>,
    private _commonHttpService: CommonHttpService,
    private route: Router,
    private _dataStoreService: DataStoreService) {
    this.userRole = this._authService.getCurrentUser();
  }

  ngOnInit() {
    this.isATDWorker = (this.userRole.role.name !== AppConstants.ROLES.DJS_PLACEMENT_WORKER);
    this.formInitilize();
    this.paginationInfo.sortBy = 'desc';
    this.paginationInfo.sortColumn = 'updateddate';
    const role = this._authService.getCurrentUser();
    this.agency = this._authService.getAgencyName();
    this.isDjs = role.user.userprofile.teamtypekey === 'DJS';
    this.showHistory = false;
    this.status = 'pending';
    this.getPage(1, this.status);
  }

  formInitilize() {
    this.searchIntakeForm = this.formBuilder.group({
      intakenumber: ['']
    });
  }
  getPage(selectPageNumber: number, status: string) {
    this.status = status;
    const pageSource = this.pageStream$.map(pageNumber => {
      if (this.paginationInfo.pageNumber !== 1) {
        this.previousPage = pageNumber;
      } else {
        this.previousPage = this.paginationInfo.pageNumber;
      }
      return { search: this.dynamicObject, page: this.previousPage };
    });
    const searchSource = this.searchTermStream$.debounceTime(1000).map(searchTerm => {
      this.previousPage = 1;
      this.dynamicObject = searchTerm;
      if (searchTerm && searchTerm.intakenumber) {
        this.searchHistory.push({ intakeNumber: searchTerm.intakenumber.like.replace('%25', '').replace('%25', '') });
      }
      this.showHistory = false;
      return { search: searchTerm, page: this.previousPage };
    });
    const source = pageSource
      .merge(searchSource)
      .startWith({
        search: this.dynamicObject,
        page: this.paginationInfo.pageNumber
      })
      .flatMap((params: { search: DynamicObject; page: number }) => {
        let intakeNumber = '';
        if (this.searchIntakeForm.value.intakenumber) {
          intakeNumber = this.searchIntakeForm.value.intakenumber;
        }
        this.searchCriteria = {
          status: this.status,
          intakenumber: intakeNumber,
          sortcolumn: this.paginationInfo.sortColumn,
          sortorder: this.paginationInfo.sortBy,
          placementworkertype: this.isATDWorker ? 'ATD' : 'DET'
        };
        return this._service.getPagedArrayList(
          new PaginationRequest({
            limit: this.paginationInfo.pageSize,
            page: this.previousPage,
            method: 'post',
            where: this.searchCriteria
          }),
          NewUrlConfig.EndPoint.Intake.DjsPlacementDashboard
        );
      })
      .subscribe(result => {
        this.intakes = result.data;
        this.totalRecords = result.count;

        let i = 0;
        for (const intake of this.intakes) {
          const datereceivedutc = moment.utc(intake.datereceived);
          const datereceivedeststr = datereceivedutc.utcOffset(-8).format('MM/DD/YYYY, h:mm A');
          this.intakes[i].datereceivedeststr = datereceivedeststr;

          const updateddateutc = moment.utc(intake.updateddate);
          const updateddateeststr = updateddateutc.utcOffset(-5).format('MM/DD/YYYY, h:mm A');
          this.intakes[i].updateddateeststr = updateddateeststr;

          i = i + 1;
        }
      });
  }
  onSorted($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.getPage(this.paginationInfo.pageNumber, this.status);
  }
  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.previousPage = this.paginationInfo.pageNumber;
    this.getPage(this.previousPage, this.status);
  }

  onSearch(field: string, value: string) {
    this.showHistory = true;
    this.searchIntakeForm.patchValue({ intakenumber: value });
    this.dynamicObject[field] = { like: '%25' + value + '%25' };
    if (!value) {
      delete this.dynamicObject[field];
    }
    this.searchTermStream$.next(this.dynamicObject);
  }

  openIntake(item) {
    if (item.intakenumber) {
      this._dataStoreService.setData('placement_intake', item);
      this._dataStoreService.setData(IntakeStoreConstants.evalFields, item.jsondata.evaluationFields);
      this.route.navigate(['/pages/cjams-dashboard/djsplacement-dashboard/placement-details/' + item.intakenumber]);
    }
  }
}
