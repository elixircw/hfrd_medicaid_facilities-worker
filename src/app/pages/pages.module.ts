import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PageHeaderModule } from '../shared/modules';
import { AlertModule } from '../shared/modules/alert/alert.module';

@NgModule({
    imports: [CommonModule, PagesRoutingModule, PageHeaderModule, AlertModule],
    declarations: [PagesComponent],
    exports: []
})
export class PagesModule {}
