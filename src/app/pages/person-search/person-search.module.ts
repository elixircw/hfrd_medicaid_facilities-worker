import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonSearchRoutingModule } from './person-search-routing.module';
import { PersonSearchComponent } from './person-search.component';
import { SearchComponent } from './search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule,
  MatAutocompleteModule
} from '@angular/material';
import { SearchResultComponent } from './search-result/search-result.component';
import { NgxMaskModule } from 'ngx-mask';
import { CommonControlsModule } from '../../shared/modules/common-controls/common-controls.module';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { CwSearchComponent } from './cw-search/cw-search.component';
import { NavigationUtils } from '../_utils/navigation-utils.service';
import { SortTableModule } from '../../shared/modules/sortable-table/sortable-table.module';
import { AuditTrailModule } from '../shared-pages/audit-trail/audit-trail.module';
import { AddressSearchComponent } from './address-search/address-search.component';
import { FormMaterialModule } from '../../@core/form-material.module';
import { ProgramParticipationModule } from '../../lib/programParticipation/programParticipation.module';
import { UIModule } from '../../lib/ui/ui.module';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    PaginationModule,
    PersonSearchRoutingModule,
    ProgramParticipationModule,
    NgxMaskModule.forRoot(),
    SharedPipesModule,
    SortTableModule,
    AuditTrailModule,
    UIModule
  ],
  declarations: [PersonSearchComponent, SearchComponent, SearchResultComponent, CwSearchComponent, AddressSearchComponent],
  providers: [NavigationUtils]
})
export class PersonSearchModule { }