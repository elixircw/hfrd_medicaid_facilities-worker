import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors, FormControl } from '@angular/forms';
import { ValidationService, DataStoreService, CommonDropdownsService, CommonHttpService, AuthService, GenericService, SessionStorageService } from '../../../@core/services';

import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { InvolvedPersonSearchResponse, PersonDsdsAction, PriorAuditLog } from '../../provider-referral/new-private-referral/_entities/newintakeModel';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { NewUrlConfig } from '../../newintake/newintake-url.config';
import { PersonSearch, People, InvolvedPerson } from '../../../@core/common/models/involvedperson.data.model';
import { ObjectUtils } from '../../../@core/common/initializer';
import { NavigationUtils } from '../../_utils/navigation-utils.service';
import { ColumnSortedEvent } from '../../../shared/modules/sortable-table/sort.service';
import { Assignments } from '../../case-worker/dsds-action/cw-assignments/assignments.data.model';
import { ProgramParticipationService } from '../../../lib/programParticipation/programParticipation.service';

@Component({
  selector: 'cw-search',
  templateUrl: './cw-search.component.html',
  styleUrls: ['./cw-search.component.scss']
})
export class CwSearchComponent implements OnInit {

  involvedPersonSearchForm: FormGroup;
  private involvedPersonSearch: InvolvedPerson;
  genderDropdownItems$: Observable<any[]>;
  stateDropdownItems$: Observable<any[]>;
  personSearchForm: InvolvedPerson;
  paginationInfo: PaginationInfo = new PaginationInfo();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  personSearchResult$: Observable<PersonSearch[]>;
  personDSDSActions$: Observable<PersonDsdsAction[]>;
  private priorAuditLogRequest = new PriorAuditLog();
  intakeNumber: string;
  showPersonDetail = -1;
  private EDITABLE_ROLES = [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR];
  isEditable = false;
  isSearch: boolean;
  personSearchResult: InvolvedPersonSearchResponse[];
  totalRecords: number;
  personSearch: People;
  selectedPerson: any;
  selectedCaseInfo: any = [];
  assignmentsList$: Observable<Assignments[]>;

  isAuditVisible = false;

  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private dropdownService: CommonDropdownsService,
    private _commonHttpService: CommonHttpService,
    private _commonDropDownService: CommonDropdownsService,
    private _involvedPersonSeachService: GenericService<InvolvedPersonSearchResponse>,
    private _authService: AuthService,
    private _navigationUtils: NavigationUtils,
    private _session: SessionStorageService,
    private programParticipationService: ProgramParticipationService
    ) {
    this.isEditable = this.hasEditAccess();
  }

  //https://eservices.paychex.com/secure/HRO_PNG/ssn_itin_fed_id_other.html
  //These cannot be valid ssns, so should be prevented.
  validateSSN(c: FormControl) {
    const invalidssn = ['000000000'];
    return invalidssn.includes(c.value) ? {
      validateSSN: {
        valid: false
      }
    } : null;
  }

  ngOnInit() {
    this.initSearchForm();
    this.loadDropdowns();
    this.isSearch = false;
    this.paginationInfo.sortBy = 'asc';
    this.paginationInfo.sortColumn = null;
    this.isAuditVisible = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this.loadFromCache();
  }
  initSearchForm() {
    this.involvedPersonSearchForm = this._formBuilder.group({
      lastname: [''],
      firstname: [''],
      maidenname: [''],
      gender: [''],
      dob: [''],
      dateofdeath: [''],
      ssn: ['', [this.validateSSN]],
      mediasrc: [''],
      mediasrctxt: [''],
      occupation: [''],
      dl: [''],
      stateid: [''],
      address1: [''],
      address2: [''],
      zip: [''],
      city: [''],
      county: [''],
      selectedPerson: [''],
      cjisnumber: [''],
      complaintnumber: [''],
      fein: [''],
      age: [''],
      email: ['', [ValidationService.mailFormat]],
      phone: [''],
      petitionid: [''],
      alias: [''],
      oldId: [''],
      chessieid: [''],
      middlename: [''],
      cjamspid: ['']
    }, { validator: this.atLeastOne(Validators.required) });
  }

  atLeastOne = (validator: ValidatorFn) => (
    group: FormGroup,
  ): ValidationErrors | null => {
    const hasAtLeastOne = group && group.controls && Object.keys(group.controls)
      .some(k => !validator(group.controls[k]));
    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
  }

  loadDropdowns() {
    this.genderDropdownItems$ = this.dropdownService.getGenders();
    this.stateDropdownItems$ = this.dropdownService.getStateList();
  }
  clearPersonSearch() {
    this.involvedPersonSearchForm.reset();
    this.isSearch = false;
    this.personSearchResult = [];
    this.showPersonDetail = -1;
    this._session.removeItem('personSearchParams');
  }
  clearFilter() {
    this.searchInvolvedPersons(this.personSearch, 'new');
  }
  searchInvolvedPersons(model: People, mode) {
    this.showPersonDetail = -1;
    if (mode === 'new') {
      this.paginationInfo.sortBy = 'asc';
      this.paginationInfo.sortColumn = null;
    }
    this.personSearch = model;
    this.involvedPersonSearch = Object.assign(new InvolvedPerson(), model);
    if (this.involvedPersonSearchForm.value.address1) {
      this.involvedPersonSearch.address = this.involvedPersonSearchForm.value.address1 + '' + this.involvedPersonSearchForm.value.address2;
    }
    // this.involvedPersonSearch.stateid = this.involvedPersonSearchForm.value.dl;
    this.isSearch = true;
    this.paginationInfo.pageNumber = 1;
    this.getPage(1, this.involvedPersonSearch);
  }

  onSortedPerson($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.searchInvolvedPersons(this.personSearch, 'exist');
  }

  hasEditAccess() {
    const user: AppUser = this._authService.getCurrentUser();
    let hasAccess = false;
    const found = this.EDITABLE_ROLES.indexOf(user.role.name);
    if (found !== -1) {
      hasAccess = true;
    }
    return hasAccess;
  }

  private getPage(pageNumber: number, involvedPersonSearch) {
    // const dobdate = this._commonDropDownService.getValidDate(this.personSearchForm.dob);
    // this.personSearchForm.dob = dobdate.toString();
    this.personSearchForm = involvedPersonSearch;
    this.personSearchForm.sortcolumn = this.paginationInfo.sortColumn;
    this.personSearchForm.sortorder = this.paginationInfo.sortBy;
    ObjectUtils.removeEmptyProperties(this.personSearchForm);
    const source = this._involvedPersonSeachService
      .getPagedArrayList(
        {
          limit: this.paginationInfo.pageSize,
          order: this.paginationInfo.sortBy,
          page: pageNumber,
          count: this.paginationInfo.total,
          where: this.personSearchForm,
          method: 'post'
        },
        'globalpersonsearches/getPersonSearchData'
      )
      .map((result) => {
        return {
          data: result.data,
          count: result.count,
          canDisplayPager: result.count > this.paginationInfo.pageSize
        };
      }).subscribe(response => {
        this.personSearchResult = response.data;
        if (pageNumber === 1) {
          this.totalRecords = response.count;
        }
      });
    /* this.personSearchResult$ = source.pluck('data');
    if (pageNumber === 1) {
      this.totalRecords$ = source.pluck('count');
      this.canDisplayPager$ = source.pluck('canDisplayPager');
    } */
  }

  searchPersonDetailsRow(id: number, model: PersonDsdsAction) {
    this.searchPersonDetails(id);
    this.getPersonDSDSAction(model);
  }

  getPersonDSDSAction(model: PersonDsdsAction) {
    const url = NewUrlConfig.EndPoint.Intake.IntakeServiceRequestsCWUrl + `?personid=` + model.personid + '&filter';
    const source = this._commonHttpService
      .getArrayList(
        new PaginationRequest({
          method: 'get',
          where: { intakerequestid: null }
        }),
        url
      )
      .share();
    this.personDSDSActions$ = source.pluck('data');
    this.personDSDSActions$
      .map((data) => {
        data.map((address) => {
          address.daDetails.map((addressdata) => {

            // filter the duplicate roles
            if (addressdata.roles) {
              const uniqueRoles = addressdata.roles.filter((elem, i, arr) => {
                if (arr.indexOf(elem) === i) {
                  return elem;
                }
              });
              addressdata.roles = uniqueRoles;
            }

            if (addressdata.dasubtype === 'Peace Order') {
              address.highLight = true;
              return address;
            }
          });
        });
        return data;
      })
      .subscribe((finalValue) => console.log(finalValue));
  }

  viewAssignmentHistory(item) {
    this.assignmentsList$ = this._commonHttpService.getArrayList(
      {
          where: { servicecaseid: item.intakeserviceid},
          method: 'get'
      },
      'Caseassignments/getworkload?filter'
  );
  }

  pageChanged(event: any) {
    this.paginationInfo.pageNumber = event.page;
    this.getPage(this.paginationInfo.pageNumber, this.personSearchForm);
  }

  searchPersonDetails(id: number) {
    if (this.showPersonDetail !== id) {
      this.showPersonDetail = id;
    } else {
      this.showPersonDetail = -1;
    }
  }

  openPersonInDetail(person, action) {
    this.router.navigate(['/pages/person-details/' + action + '/' + person.personid + '/basic']);

  }

  getPrimaryRelationname(listdata) {
    const finddata = listdata.find(data => data.relationcategory === 'Primary');
    if (finddata) {
      return (finddata.relationtype + ': ' + finddata.personname);
    }
    return '';
  }
  replaceALL(str) {
    if (str) {
      return str.replace(/~/gi, ',').replace(',,', ',');
    }
  }

  priorAuditLog(item: PriorAuditLog) {
    this._navigationUtils.openRespectiveItem(item);
  }

  showOutcome(person, caseInfo) {
    console.log(person, caseInfo);
    this.selectedPerson = person;
    this.selectedCaseInfo = caseInfo;
    switch (caseInfo.datype) {
      case 'Child Protective Services':
        (<any>$('#person-out-come')).modal('show');
        break;
    }
  }
  close() {
    (<any>$('#person-out-come')).modal('hide');
    this.selectedCaseInfo = null;
    this.selectedPerson = null;
  }

  getFindingList(da_investigationid, person) {
    this._commonHttpService
      .getArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          where: {
            investigationid: da_investigationid
          },
          method: 'get'
        }),
        'Investigationallegations/getmaltreatmentfinding?filter'
      )
      .subscribe((res) => {
        if (res) {
          console.log(res);
        }
      });
  }

  isMaltreator(roles) {
    let hasMaltreator = false;
    if (Array.isArray(roles)) {
      const maltreatorIndex = roles.indexOf('Alleged Maltreator');
      if (maltreatorIndex !== -1) {
        hasMaltreator = true;
      }
    }
    return hasMaltreator;
  }

  showAuditLog(person) {
    /* (<any>$('#person-audit-logs')).modal('show');
    this.selectedPerson = person;
     this.loadAuditLogs(1);*/
     this._session.setObj('personSearchSelectedPerson', person);
    this._session.setObj('personSearchParams', this.involvedPersonSearchForm.getRawValue());
    if(person.source === 'SDR' && person.personid === '') {
      (<any>$('#audit-log')).modal('show');
    } else {
    this.router.navigate(['../demographic-log/' + person.personid], { relativeTo: this.route });
    }

  }

  loadFromCache() {
    const searchParams = this._session.getObj('personSearchParams');
    if (searchParams) {
      this.involvedPersonSearchForm.patchValue(searchParams);
      this.searchInvolvedPersons(searchParams, 'new');

    }
  }
}
