import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AlertService, CommonHttpService, GenericService, AuthService, DataStoreService } from '../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { ObjectUtils } from '../../../@core/common/initializer';
import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { InvolvedPerson, PersonSearch, InvolvedPersonSearchResponse, PersonDsdsAction, PriorAuditLog } from '../../../@core/common/models/involvedperson.data.model';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { NewUrlConfig } from '../../newintake/newintake-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {


  personSearchForm: InvolvedPerson;
  paginationInfo: PaginationInfo = new PaginationInfo();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  personSearchResult$: Observable<PersonSearch[]>;
  personDSDSActions$: Observable<PersonDsdsAction[]>;
  private priorAuditLogRequest = new PriorAuditLog();
  intakeNumber: string;

  showPersonDetail = -1;
  private EDITABLE_ROLES = [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR];
  isEditable = false;
  constructor(private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private router: Router,
    private _involvedPersonSeachService: GenericService<InvolvedPersonSearchResponse>,
    private _authService: AuthService,
    private _dataStoreService: DataStoreService) {
      this.isEditable = this.hasEditAccess();
    }

  ngOnInit() {
    this.personSearchForm = this._dataStoreService.getData('PERSON_SEARCH_FORM');
    if (this.personSearchForm) {
      console.log(this.personSearchForm);
      this.getPage(1);
    } else {
      this.router.navigate(['/pages/person-search/search']);
    }


  }

  hasEditAccess() {
    const user: AppUser = this._authService.getCurrentUser();
    let hasAccess = false;
    const found = this.EDITABLE_ROLES.indexOf(user.role.name);
    if (found !== -1) {
      hasAccess = true;
    }
    return hasAccess;



  }

  private getPage(pageNumber: number) {
    ObjectUtils.removeEmptyProperties(this.personSearchForm);
    const source = this._involvedPersonSeachService
      .getPagedArrayList(
        {
          limit: this.paginationInfo.pageSize,
          order: this.paginationInfo.sortBy,
          page: pageNumber,
          count: this.paginationInfo.total,
          where: this.personSearchForm,
          method: 'post'
        },
        'globalpersonsearches/getPersonSearchData'
      )
      .map((result) => {
        return {
          data: result.data,
          count: result.count,
          canDisplayPager: result.count > this.paginationInfo.pageSize
        };
      })
      .share();
    this.personSearchResult$ = source.pluck('data');
    if (pageNumber === 1) {
      this.totalRecords$ = source.pluck('count');
      this.canDisplayPager$ = source.pluck('canDisplayPager');
    }
  }

  searchPersonDetailsRow(id: number, model: PersonDsdsAction) {
    this.searchPersonDetails(id);
    this.getPersonDSDSAction(model);
  }

  getPersonDSDSAction(model: PersonDsdsAction) {
    const url = NewUrlConfig.EndPoint.Intake.IntakeServiceRequestsUrl + `?personid=` + model.personid + '&filter';
    const source = this._commonHttpService
        .getArrayList(
            new PaginationRequest({
                method: 'get',
                where: { intakerequestid: null }
            }),
            url
        )
        .share();
    this.personDSDSActions$ = source.pluck('data');
    this.personDSDSActions$
        .map((data) => {
            data.map((address) => {
                address.daDetails.map((addressdata) => {
                    if (addressdata.dasubtype === 'Peace Order') {
                        address.highLight = true;
                        return address;
                    }
                });
            });
            return data;
        })
        .subscribe((finalValue) => console.log(finalValue));
}


  pageChanged(event: any) {
    this.paginationInfo.pageNumber = event.page;
    this.getPage(this.paginationInfo.pageNumber);
  }
  searchPersonDetails(id: number) {
    if (this.showPersonDetail !== id) {
      this.showPersonDetail = id;
    } else {
      this.showPersonDetail = -1;
    }
  }

  openPersonInDetail(person, action) {
    this.router.navigate(['/pages/person-details/' + action + '/' + person.personid + '/basic']);

  }

  getPrimaryRelationname(listdata) {
    const finddata = listdata.find(data => data.relationcategory === 'Primary');
    if (finddata) {
        return (finddata.relationtype + ': ' + finddata.personname);
    }
    return '';
  }
  priorAuditLog(item: PriorAuditLog) {
    window.open('#/pages/case-worker/' + item.intakeserviceid + '/' + item.danumber + '/dsds-action/report-summary', '_blank');
    const url = NewUrlConfig.EndPoint.Intake.PriorAuditLogUrl;
    this.priorAuditLogRequest = Object.assign({}, item);
    this.priorAuditLogRequest.priordanumber = item.danumber;
    this.priorAuditLogRequest.danumber = this.intakeNumber;
    const obj = this.priorAuditLogRequest;
    this._commonHttpService
        .getArrayList(
            {
                method: 'post',
                obj
            },
            url
        )
        .subscribe();
}


}
