import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseFoldersComponent } from './case-folders.component';

describe('CaseFoldersComponent', () => {
  let component: CaseFoldersComponent;
  let fixture: ComponentFixture<CaseFoldersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseFoldersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseFoldersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
