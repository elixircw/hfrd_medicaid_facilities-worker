import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaseFoldersComponent } from './case-folders.component';
import { RoleGuard } from '../../@core/guard';

const routes: Routes = [
  {
      path: '',
      component: CaseFoldersComponent,
      canActivate: [RoleGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseFoldersRoutingModule { }
