import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, AuthService } from '../../@core/services';
import { NewUrlConfig } from '../newintake/newintake-url.config';
import { Folder, Case, Youth } from './_entities/case-folder.datamodel';
import { Router } from '@angular/router';
import { CaseWorkerUrlConfig } from '../case-worker/case-worker-url.config';
import { PaginationInfo, DropdownModel, PaginationRequest } from '../../@core/entities/common.entities';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'case-folders',
  templateUrl: './case-folders.component.html',
  styleUrls: ['./case-folders.component.scss']
})
export class CaseFoldersComponent implements OnInit {
  folders: Array<Folder> = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalRecords: number;
  maxSize = 10;
  cases: Array<Case>;
  folderType: string;
  folderName: string;
  createCaseForm: FormGroup;
  types$: Observable<DropdownModel[]>;
  offenses$: Observable<DropdownModel[]>;
  subTypes$: Observable<DropdownModel[]>;
  generatedCaseNumber: number;
  youth: Youth;
  searchKey: string;
  folderList: Array<Folder> = [];
  isDjs = false;
  constructor(private _commonService: CommonHttpService, private formBuilder: FormBuilder, private _alertService: AlertService, private _authService: AuthService) { }

  ngOnInit() {
    this.isDjs = this._authService.isDJS();
    this.folderType = '';
    this.folderName = '';
    this.loadDropdowns();
    this.loadFolderType();
    this.loadCases();
    this.initCreateCaseForm();
  }

  loadDropdowns() {
    this.types$ = this._commonService.getArrayList(new PaginationRequest({
      nolimit: true,
      where: { teamtypekey: 'DJS' },
      method: 'get',
      order: 'description'
    }), NewUrlConfig.EndPoint.Intake.IntakePurposes + '/list?filter').map((result) => {
      return result.map(
        (res) =>
          new DropdownModel({
            text: res.description,
            value: res.intakeservreqtypeid,
            additionalProperty: res.teamtype.teamtypekey
          })
      );
    });
  }

  loadAllegations(type) {
    console.log(type.value);
    this.offenses$ = this._commonService
      .getArrayList(new PaginationRequest({
        where: { intakeservreqtypeid: type.value },
        method: 'get',
        nolimit: true,
        order: 'name'
      }), NewUrlConfig.EndPoint.Intake.allegationsUrl + '?filter')
      .map((result) => {
        console.log(result);
        return result.map(
          (res) =>
            res
        );
      });
  }

  loadSubTypes(event) {
    const checkInput = {
      where: { 'type': 'AL', 'allegationids': event.value },
      method: 'get',
      order: 'description'
    };
    this.subTypes$ = this._commonService
      .getArrayList(
        new PaginationRequest(checkInput),
        NewUrlConfig.EndPoint.Intake.allegationsSubTypes + '?filter'
      )
      .map(result => {
        return result.map(
          res =>
            new DropdownModel({
              text: res.description,
              value: res.servicerequestsubtypeid
            })
        );
      });
  }

  generateCaseNumber() {
    this._commonService
      .getArrayList({}, NewUrlConfig.EndPoint.Intake.NextnumbersUrl)
      .subscribe(result => {
        this.generatedCaseNumber = result['nextNumber'];
        this.createCaseForm.patchValue({ casenumber: this.generatedCaseNumber });
      });
  }

  initCreateCaseForm() {
    this.createCaseForm = this.formBuilder.group({
      foldertype: '',
      casenumber: { value: '', disabled: true },
      type: '',
      notes: '',
      youthid: { value: '', disabled: true },
      allegations: [''],
      subtype: '',
      reason: '',
      youthName: ''
    });
  }

  loadFolderType() {
    this._commonService
      .getArrayList(
        {}, NewUrlConfig.EndPoint.Intake.folderTypeList
      )
      .subscribe(result => {
        this.folders = result;
        setTimeout(() => {
          const __this = this;
          (<any>$('.foldertabs')).scrollingTabs({
            disableScrollArrowsOnFullyScrolled: true,
            scrollToActiveTab: true,
            tabClickHandler: function (e) {
              $(this).addClass('active');
              __this.folderType = $(this).attr('id');
              __this.folderName = __this.folders.find(folder => folder.foldertypekey === __this.folderType).description;
              __this.paginationInfo.pageNumber = 1;
              __this.loadCases();
            }
          });
          (<any>$('.scrtabs-tabs-fixed-container')).css('height', '150px');
          (<any>$('.scrtabs-tab-scroll-arrow')).css('top', '50px');
        }, 10);
      });
  }

  loadCases() {
    this._commonService.getArrayList(
      {
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        count: this.paginationInfo.total,
        method: 'get',
        where: {
          foldertypekey: this.folderType,
          searchkey: this.searchKey
        }
      },
      CaseWorkerUrlConfig.EndPoint.CaseFolders.GetCaseList
    ).subscribe((result: any) => {
      this.cases = result.data;
      if (this.paginationInfo.pageNumber === 1) {
        this.totalRecords = result.count;
      }
    });
  }

  loadFolderList() {
    this._commonService.getArrayList(
      {
        nolimit: true,
        method: 'get',
        where: {
          searchkey: this.searchKey
        }
      },
      CaseWorkerUrlConfig.EndPoint.CaseFolders.FolderList
    ).subscribe((result: any) => {
      this.folderList = result;
    });
  }

  loadAllCases() {
    this.folderType = '';
    this.folderName = '';
    (<any>$('.caseworkwer-widget-content')).removeClass('active');
    (<any>$('.caseworkerwidget-box')).removeClass('active');
    this.loadCases();
    this.loadFolderList();
  }

  pageChanged(pageInfo) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadCases();
  }

  openCreateCasePopup() {
    this.initCreateCaseForm();
    this.generateCaseNumber();
    this.offenses$ = Observable.of([]);
    this.subTypes$ = Observable.of([]);
    this.createCaseForm.patchValue({
      youthName: this.youth.firstname + ' ' + this.youth.lastname,
      youthid: this.youth.personid
    });
    (<any>$('#create-case')).modal('show');
  }

  createCase() {
    console.log(this.createCaseForm.getRawValue());
  }

  searchPerson() {
    this._commonService.getSingle(new PaginationRequest({
      where: {
        searchkey: this.searchKey
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.CaseFolders.PersonSeach).subscribe(result => {
      if (result.length === 1) {
        this.youth = result[0];
        this.loadAllCases();
      } else {
        this.youth = null;
        this._alertService.warn('Please enter a valid CJAMS ID / Assist ID and perform search again.');
      }
    });
  }
}
