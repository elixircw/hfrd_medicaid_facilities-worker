import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatTableModule,
  MatExpansionModule
} from '@angular/material';
import { CaseFoldersRoutingModule } from './case-folders-routing.module';
import { CaseFoldersComponent } from './case-folders.component';
import { PaginationModule } from 'ngx-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    CaseFoldersRoutingModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [CaseFoldersComponent]
})
export class CaseFoldersModule { }
