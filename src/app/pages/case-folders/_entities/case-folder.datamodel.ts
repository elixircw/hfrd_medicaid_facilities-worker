export class Folder {
    activeflag: number;
    description: string;
    effectivedate: string;
    foldertypeid: string;
    foldertypekey: string;
    old_id: string;
}

export class Case {
    intakeserviceid: string;
    servicerequestnumber: string;
    srtype: string;
    srsubtype: string;
    raname: string;
    county: string;
    datereceived: Date;
    duedate: Date;
    groupnumber: string;
    assignedto: string;
    investigationid: string;
    srstatus: string;
    focus: string;
    displayname: string;
    assignedon?: Date;
    focusperson?: string;
}

export class Youth {
    assistpid: string;
    cjamspid: string;
    firstname: string;
    lastname: string;
    personid: string;
    userphoto: string;
}
