import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule,
} from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';

import { RequestedServiceRoutingModule } from './requested-service-routing.module';
import { RequestedServiceComponent } from './requested-service.component';
import { SortTableModule } from '../../shared/modules/sortable-table/sortable-table.module';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { SharedDirectivesModule } from '../../@core/directives/shared-directives.module';
import { PaginationModule } from 'ngx-bootstrap';
import { IntakeUtils } from '../_utils/intake-utils.service';

@NgModule({
  imports: [
    CommonModule,
    RequestedServiceRoutingModule,
    MatRadioModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatChipsModule,
    MatIconModule,
    SortTableModule,
    PaginationModule,
    SharedDirectivesModule,
    SharedPipesModule
  ],
  declarations: [RequestedServiceComponent],
  providers: [IntakeUtils]
})
export class RequestedServiceModule { }
