import { Injectable } from '@angular/core';
import { AppConstants } from '../../@core/common/constants';
import { SessionStorageService, CommonHttpService, DataStoreService } from '../../@core/services';
import { Router } from '@angular/router';
import { IntakeUtils, IntakeStore } from './intake-utils.service';
import { CASE_STORE_CONSTANTS } from '../case-worker/_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../case-worker/case-worker-url.config';

export class PersonInfoStore {
  personId: string;
  sourceID: string;
  clientId: number;
  placementId: number;
  removalId: number;
  fromIVtab: boolean = false;
  source: string;
  action: string;
  data: any;
  searchData: any;
}

@Injectable()
export class NavigationUtils {

  constructor(private _sessionStorage: SessionStorageService,
    private _router: Router,
    private _intakeUtils: IntakeUtils,
    private _session: SessionStorageService,
    private _commonService: CommonHttpService,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService) { }

  public openEditPersonInfo(personId: string, caseType: string, uniqueNumber: string, data: any) {
    const personInfo = Object.create(PersonInfoStore);
    personInfo.source = caseType;
    personInfo.sourceID = uniqueNumber;
    personInfo.personId = personId;
    personInfo.action = AppConstants.ACTIONS.EDIT;
    personInfo.data = data;
    this._sessionStorage.setObj(AppConstants.GLOBAL_KEY.PERSON_NAVIGATION_INFO, personInfo);
    this.navigateToPersonInfo();
  }

  public openNewPersonInfo(caseType: string, uniqueNumber: string, data: any, searchdata = {}) {
    const personInfo = Object.create(PersonInfoStore);
    personInfo.source = caseType;
    personInfo.sourceID = uniqueNumber;
    personInfo.personId = null;
    personInfo.action = AppConstants.ACTIONS.ADD;
    personInfo.data = data;
    personInfo.searchData = searchdata;
    this._sessionStorage.setObj(AppConstants.GLOBAL_KEY.PERSON_NAVIGATION_INFO, personInfo);
    this.navigateToPersonInfo();
  }

  public openEditFinance(personId: string,clientId: number,placementId: number,removalId: number, fromIVtab: boolean, caseType: string, uniqueNumber: string, data: any) {
    const personInfo = Object.create(PersonInfoStore);
    personInfo.source = caseType;
    personInfo.sourceID = uniqueNumber;
    personInfo.clientId = clientId; 
    personInfo.placementId = placementId; 
    personInfo.removalId = removalId; 
    personInfo.fromIVtab = fromIVtab; 
    personInfo.personId = personId;
    personInfo.action = AppConstants.ACTIONS.EDIT;
    personInfo.data = data;
    this._sessionStorage.setObj(AppConstants.GLOBAL_KEY.PERSON_NAVIGATION_INFO, personInfo);
    this.navigateToFinance();
  }


  private navigateToPersonInfo() {
    this._router.navigate(['/pages/person-info-cw']);
  }

  private navigateToFinance() {
    this._router.navigate(['/pages/person-info-cw/finance']);
  }


  clearNavigationInfo() {
    this._sessionStorage.removeItem(AppConstants.GLOBAL_KEY.PERSON_NAVIGATION_INFO);
  }

  getNavigationInfo(): PersonInfoStore {
    return this._sessionStorage.getObj(AppConstants.GLOBAL_KEY.PERSON_NAVIGATION_INFO);
  }

  setNavigationInfo(personInfo: PersonInfoStore): void {
    this._sessionStorage.setObj(AppConstants.GLOBAL_KEY.PERSON_NAVIGATION_INFO, personInfo);
  }

  loadPreviousState() {
    const navigationInfo = this.getNavigationInfo();
    if (navigationInfo) {
      switch (navigationInfo.source) {
        case AppConstants.CASE_TYPE.INTAKE:
          this.loadIntakePage(); // Already intake number has stored in session storage
          break;
        case AppConstants.CASE_TYPE.CPS_CASE:
          this.loadCasePage(navigationInfo.sourceID, navigationInfo.data.caseNumber);
          break;
        case AppConstants.CASE_TYPE.SERVICE_CASE:
          if (navigationInfo.fromIVtab === true){ 
            this.loadTitleIVE(navigationInfo.clientId, navigationInfo.removalId, navigationInfo.placementId);
          }
           else{
            this._sessionStorage.setItem('ISSERVICECASE', true);
            this.loadCasePage(navigationInfo.sourceID, navigationInfo.data.caseNumber);               
           } 
          break;
        case AppConstants.CASE_TYPE.ADOPTION_CASE:
          this.loadAdoptionCasePage(navigationInfo.sourceID, navigationInfo.data.caseNumber);
          break;
        case AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_APPLICATION:
          this.loadPublicProviderApplicantPage(navigationInfo.sourceID);
          break;
        case AppConstants.MODULE_TYPE.PUBLIC_PROVIDER:
          this.loadPublicProviderPage(navigationInfo.sourceID);
          break;
        case AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_REFERRAL:
          this.loadPublicProviderReferralPage(navigationInfo.sourceID);
          break;
      }
    } else {
      // To do should go to dashboard based upon respective role
    }
  }

  getPersonRequestParam() {
    const navigationInfo = this.getNavigationInfo();
    const requestParam = {
      personid: null,
      intakeserviceid: null,
      intakenumber: null,
      objecttypekey: null,
      objectid: null,// managing service case also,
      servicecaseid: null
    };

    if (navigationInfo) {
      requestParam.personid = navigationInfo.personId;
      switch (navigationInfo.source) {
        case AppConstants.CASE_TYPE.INTAKE:
          requestParam.intakenumber = navigationInfo.sourceID;
          break;
        case AppConstants.CASE_TYPE.CPS_CASE:
          requestParam.intakeserviceid = navigationInfo.sourceID;
          break;
        case AppConstants.CASE_TYPE.SERVICE_CASE:
          requestParam.servicecaseid = navigationInfo.sourceID;
          break;
        case AppConstants.MODULE_TYPE.PUBLIC_PROVIDER:
        case AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_APPLICATION:
        case AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_REFERRAL:
          requestParam.intakenumber = navigationInfo.sourceID;
          break;
      }
    }
    console.log('request param', requestParam);
    return requestParam;
  }

  getPersonRequest() {
    const navigationInfo = this.getNavigationInfo();
    let requestParam: any;
    if (navigationInfo) {
      switch (navigationInfo.source) {
        case AppConstants.CASE_TYPE.INTAKE:
          requestParam = {
            intakenumber: navigationInfo.sourceID
          };
          break;
        case AppConstants.CASE_TYPE.CPS_CASE:
          requestParam.intakeserviceid = navigationInfo.sourceID;
          break;
        case AppConstants.CASE_TYPE.SERVICE_CASE:
          requestParam.servicecaseid = navigationInfo.sourceID;
          break;
      } 
    }
    console.log('request param', requestParam);
    return requestParam;
  }

  private loadIntakePage() {
    const url = '/pages/newintake/my-newintake/person-cw/list';
    this._router.navigate([url]);
  }

  private loadTitleIVE(clientId: number, removalId: number, placementId: number) {
    const url = 'pages/title4e/foster-car/' + clientId + '/'+ placementId + '/' + removalId;
    this._router.navigate([url]);
  }

  private loadCasePage(caseId: string, caseNumber: string) {
    const currentUrl = '/pages/case-worker/' + caseId + '/' + caseNumber + '/dsds-action/person-cw';
    this._router.navigate([currentUrl]);
  }

  private loadAdoptionCasePage(caseId: string, caseNumber: string) {
    // const currentUrl = '/pages/case-worker/' + caseId + '/' + caseNumber + '/dsds-action/person-cw';
    const currentUrl = '/pages/case-worker/' + caseId + '/' + caseNumber + '/dsds-action/adoption-persons';
    this._router.navigate([currentUrl]);
  }

  private loadPublicProviderPage(providerID: string) {
    const currentUrl = '/pages/provider-management/new-public-provider/' + providerID + '/household-members/list';
    this._router.navigate([currentUrl]);
  }

  private loadPublicProviderApplicantPage(applicantNumber: string) {
    const currentUrl = '/pages/provider-applicant/new-public-applicant/' + applicantNumber + '/household-members/list';
    this._router.navigate([currentUrl]);
  }

  private loadPublicProviderReferralPage(applicantNumber: string) {
    const currentUrl = '/pages/provider-referral/new-public-referral/' + applicantNumber + '/household-members/list';
    this._router.navigate([currentUrl]);
  }

  openRespectiveItem(item) {
    switch (item.datype) {
      case 'Service Case':
      case 'Request for services':
      case 'ROA-CPS':
        this._sessionStorage.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.intakeserviceid + '/casetype';
        this._commonHttpService.getAll(url).subscribe((response) => {
          const dsdsActionsSummary = response[0];
          if (dsdsActionsSummary) {
            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            // common person for cw
            const currentUrl = '#/pages/case-worker/' + item.intakeserviceid + '/' + item.danumber + '/dsds-action/report-summary';
            window.open(currentUrl);
          }
        });
        break;
      case 'Information and Referral': {
        this._sessionStorage.removeItem('intake');
        const intake = Object.create(IntakeStore);
        intake.number = item.danumber;
        intake.action = 'edit';
        this._sessionStorage.setObj('intake', intake);
        this._dataStoreService.clearStore();
        window.open('#/pages/newintake/my-newintake');
        break;
      }
      case 'Intake': {
        this._sessionStorage.removeItem('intake');
        const intake = Object.create(IntakeStore);
        intake.number = item.danumber;
        intake.action = 'edit';
        this._sessionStorage.setObj('intake', intake);
        this._dataStoreService.clearStore();
        window.open('#/pages/newintake/my-newintake');
        break;
      }
      case 'Child Protective Services':
        this._commonHttpService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
          const dsdsActionsSummary = response[0];
          if (dsdsActionsSummary) {
            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            const currentUrl = '#/pages/case-worker/' + item.intakeserviceid + '/' + item.danumber + '/dsds-action/report-summary';
            window.open(currentUrl);
          }
        });
        break;
    }
  }

  getModuleType() {
    const navigationInfo = this.getNavigationInfo();
    let moduleType = '';
    if (navigationInfo) {
      switch (navigationInfo.source) {
        case AppConstants.CASE_TYPE.INTAKE:
          moduleType = 'Intake';
          break;
        case AppConstants.CASE_TYPE.CPS_CASE:
          moduleType = 'servicerequest';
          break;
        case AppConstants.CASE_TYPE.SERVICE_CASE:
          moduleType = 'servicecase';
          break;
      }

      return moduleType;
    }
  }
  routToServiceCase(item: any) {
    this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
    const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.caseid + '/casetype';
    this._commonService.getById(item.casenumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
      const dsdsActionsSummary = response[0];
      if (dsdsActionsSummary) {
          this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
          this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
          const currentUrl =  '#/pages/case-worker/' + item.servicecaseid + '/' + item.casenumber + '/dsds-action/person-cw';
         // this._router.navigate([currentUrl]);
         window.open(currentUrl);
      }
  });
  }

  public loadFostcareIVE(clientId: number, removalId: number, placementId: number) {
    const url = 'pages/title4e/foster-car/' + clientId + '/'+ placementId + '/' + removalId;
    this._router.navigate([url]);
  }


}
