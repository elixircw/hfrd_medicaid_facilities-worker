import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService, CommonHttpService, AuthService, DataStoreService } from '../../@core/services';
import { CommonUrlConfig } from '../../@core/common/URLs/common-url.config';
import { AppConstants } from '../../@core/common/constants';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';

export class IntakeStore {
    number: string;
    action: string;
}
export class IntakeCaseStore {
    intakeserviceid: string;
    servicerequestnumber: string;
    action: string;
}



@Injectable()

export class IntakeUtils {

    public notesUpdated$ = new Subject<any>();
    constructor(private _router: Router,
        private _sessionStorage: SessionStorageService,
        private _commonHttpService: CommonHttpService,
        private _datastore: DataStoreService,
        private _authService: AuthService
        ) { }
    
        redirectIntake(intakeNumber: string, action?: string) {
            this._sessionStorage.removeItem('intake');
            const intake = Object.create(IntakeStore);
            intake.number = intakeNumber;
            intake.action = action ? action : 'edit';
            this._sessionStorage.setObj('intake', intake);
            const url = '/pages/newintake/my-newintake/';
            this._datastore.clearStore();
            this._router.navigate([url], { skipLocationChange: true });
    }

    redirectToCase(intakeserviceid: string, servicerequestnumber: string, action?: string) {
        this._sessionStorage.removeItem('intakeCase');
        const intake = Object.create(IntakeCaseStore);
        intake.intakeserviceid = intakeserviceid;
        intake.servicerequestnumber = servicerequestnumber;
        intake.action = action ? action : 'edit';
        this._sessionStorage.setObj('IntakeCaseStore', intake);
        const url = `/pages/case-worker/${intakeserviceid}/${servicerequestnumber}/dsds-action/report-summary-djs`;
        this._router.navigate([url], { skipLocationChange: true });
    }

    redirectToPerson(personid: string) {
        const url = `/pages/person-details/view/${personid}/basic/demographics`;
        this._router.navigate([url]);
    }

    getFocusPersonStatus(personid: string, intakeserviceid: string, intakenumber: string, status = 'Open') {
        return this._commonHttpService.getArrayList({
            where: {
                personid: personid,
                intakeserviceid: intakeserviceid,
                intakenumber: intakenumber,
                status: status
            }, method: 'get'
        }, CommonUrlConfig.EndPoint.Intake.personStatus);
    }

    getResedentialStatus(personid) {
        return this._commonHttpService.getArrayList({ where: { personid: personid }, nolimit: true, method: 'get' }, 'placement/placementresidential?filter');
    }

    loadCaseAssignDashboard() {
        let assignCaseRoute = '/pages/cjams-dashboard';
        switch (this._authService.getAgencyName()) {
            case AppConstants.AGENCY.DJS:
                assignCaseRoute += '/assign-case';
                break;
            case AppConstants.AGENCY.CW:
                assignCaseRoute += '/cw-assign-case';
                break;
            // for AS no sepearte tab avalabe to assign case
        }
        this._router.navigate([assignCaseRoute]);
    }

    loadIntakeDashboard() {
        let assignCaseRoute = '/pages/cjams-dashboard';
        switch (this._authService.getAgencyName()) {
            case AppConstants.AGENCY.DJS:
                assignCaseRoute += '/intake-summary';
                break;
            case AppConstants.AGENCY.CW:
                assignCaseRoute += '/cw-assign-case';
                break;
            case AppConstants.AGENCY.AS:
                break;
            // for AS no sepearte tab avalabe to assign case
        }
        return assignCaseRoute;
    }

    extractDateOnly(date: any): string {
        const formattedDate = moment(date).format('YYYY-MM-DD');
        return formattedDate === 'Invalid date' ? null : formattedDate;
    }

    isValidZipCode(zipcode: string): boolean {
        const regex: RegExp = new RegExp(/^[0-9]{5}(?:-[0-9]{4})?$/);
        const result = regex.test(zipcode);
        return result;
    }

    public findInvalidControls(form: FormGroup) {
        const invalid = [];
        const controls = form.controls;
        for (const name in controls) {
            if (controls[name].invalid) {
                console.log('invalid control', controls[name]);
                invalid.push(name);
            }
        }
        console.log(invalid);
        return invalid;
    }
    public getAge(dateValue: string): number {
        if (
            dateValue &&
            moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()
        ) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return 0;
        }
    }
    public getAgeFormat(dateValue: string, format: string): number {
        if (
            dateValue &&
            moment(new Date(dateValue), format, true).isValid()
        ) {
            const rCDob = moment(new Date(dateValue), format).toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return 0;
        }
    }

    public getAgeFormatBy(dateValue: string, format: string, mode): number {
        if (
            dateValue &&
            moment(new Date(dateValue), format, true).isValid()
        ) {
            const rCDob = moment(new Date(dateValue), format).toDate();
            return moment().diff(rCDob, mode);
        } else {
            return 0;
        }
    }

    public addDate(dateValue: string, format: string, count: number, mode) {
        if (
            dateValue &&
            moment(new Date(dateValue), format, true).isValid()
        ) {
            const addedDate = moment(new Date(dateValue), format).add(count, mode).toDate();
            return addedDate;
        } else {
            return null;
        }
    }

    getIntakeStore(): IntakeStore {
        return this._sessionStorage.getObj('intake');
    }

    setIntakeStore(intakeStore: IntakeStore) {
        this._sessionStorage.setObj('intake', intakeStore);
    }

    // patchRestrictedItem(itemid, isrestricted) {
    //     let payload = {};
    //     payload['intakenumber'] = itemid;
    //     payload['isrestricteditem'] = isrestricted;
    //     return this._commonHttpService.patch(
    //       itemid,
    //       payload,
    //       'Intakedastagings'
    //     );
    //   }

    createRestrictedItem(itemid, objecttype, accessuser, activeflag) {
        let payload = {};
        payload['objectid'] = itemid;
        payload['objecttypekey'] = objecttype;
        payload['caseworkerassignmentlist'] = accessuser;
        payload['activeflag'] = activeflag;
        if (activeflag === 1) {
        return this._commonHttpService.create(
            payload,
            'restricteditems/addrestriction'
        );
        } else {
         return this._commonHttpService.create(payload,
                'restricteditems/updaterestrictions'
            );
        }
    }


    isRestrictedItem(itemid) {
        return this._commonHttpService.getArrayList(
            { 
                where: { objectid: itemid }, 
                nolimit: true, 
                method: 'get'
            },
            'restricteditems?filter'
        );
    }

    restrictedItemAuditLog(itemid) {
        return this._commonHttpService.getArrayList(
            { 
                where: { objectid: itemid }, 
                nolimit: true, 
                method: 'get'
            },
            'restricteditems/restrictedauditlog?filter'
        );
    }


}
