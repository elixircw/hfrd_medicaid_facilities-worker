import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/pluck';
import { PaginationInfo, PaginationRequest } from '../../@core/entities/common.entities';
import { CommonHttpService, GenericService, DataStoreService, SessionStorageService } from '../../@core/services';
import { NotificationResult, NotificationSearch } from './_entities/notification-entity.module';
import { NotificationUrlConfig } from './notification.url.config';
import { AuthService } from '../../@core/services/auth.service';
import { AppUser } from '../../@core/entities/authDataModel';
import { Router } from '@angular/router';
import { CaseWorkerUrlConfig } from '../case-worker/case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../case-worker/_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
    @Input()
    notify: Subject<string>;
    notificationsTableData: any = [];
    isDeleteAll: Boolean = false;
    deleteBtn: Boolean;
    isProvider: Boolean;
    notificationsToBeDeleted: number = 0;
    paginationInfo: PaginationInfo = new PaginationInfo();
    notificationResultData$: Observable<NotificationResult[]>;
    totalResulRecords$: Observable<number>;
    notificationSearch: NotificationSearch;
    selectedRecord: NotificationResult;
    selectedDeleteRow: NotificationResult;
    isBroadCastMessage: boolean;
    private token: AppUser;
    providerID: any;
    teamType: string;
    constructor(
        private _dataStoreService: DataStoreService,
        private _router: Router,
        private _service: GenericService<NotificationResult>,
        private _commonService: CommonHttpService,
        private _sessionStorage: SessionStorageService,
        private _authService: AuthService
    ) {
        this._service.endpointUrl = NotificationUrlConfig.EndPoint.notification.notificationResult;
        this.notificationSearch = new NotificationSearch();
        this.selectedRecord = new NotificationResult();
        this.token = this._authService.getCurrentUser();
        this.teamType = this._authService.getAgencyName();
        if (this.token && this.token.role) {
             this.isProvider = this.token.role.name === "Provider_Staff_Admin" ? true : false;
        }
        if (this.token && this.token.role) {

        }
    }
    ngOnInit() {
        const role = this._authService.getCurrentUser();
        if (role.role.name === 'superuser') {
            this.isBroadCastMessage = true;
        }
        this.getNotificationData(1);
        this.deleteBtn = false;
    }

    transChanged(checked) {
        if (checked) {
            this.deleteBtn = true;
        } else {
            this.deleteBtn = false;
        }
    }

    getNotificationData(pageNo: number) {
        const body: PaginationRequest = {
            limit: 10,
            page: pageNo,
            where: {},
            method: 'post'
        };
        const source = this._service.getPagedArrayList(body).share();
        this.notificationResultData$ = source.pluck('data');
        if (pageNo === 1) {
            this.totalResulRecords$ = source.pluck('count');
        }

        this.notificationResultData$.subscribe((data) => {
            this.notificationsTableData = [];
            data.map((res: any) => {
                res.deleteBtn = true;
                res.isDelete = false;
                this.notificationsTableData.push(res);
            });

        });
    }

    checkCount() {
        this.notificationsToBeDeleted = 0;
        this.notificationsTableData.map((notification) => {
            if (notification.isDelete) {
                this.notificationsToBeDeleted++;
            }
        });
    }

    updateDeleteNotifications(checked) {
        if (checked) {
            this.deleteBtn = true;
        } else {
            this.deleteBtn = false;
        }
        this.notificationsTableData.map((notification) => {
            notification.isDelete = this.isDeleteAll;
        });
    }

    pageChanged(page: number) {
        this.paginationInfo.pageNumber = page;
        this.getNotificationData(page);
    }
    selectNotification(selectedData: NotificationResult) {
        this.selectedRecord = selectedData;
    }
    selectDeleteNotification(selectedRow: NotificationResult) {
        this.selectedDeleteRow = selectedRow;
    }
    updateNotification(notification: NotificationResult) {
        this._commonService.endpointUrl = NotificationUrlConfig.EndPoint.notification.updateNotification;
        const body = {
            usernotificationid: notification.usernotificationid
        };
        if (!notification.isread) {
            this._commonService.create(body).subscribe((data) => {
                if (data === 'Success') {
                    notification.isread = true;
                } else {
                    notification.isread = false;
                }
            });
        }
    }
    deleteSelectedNotification() {
        let usernotificationids = [];
        this.notificationsTableData.map((notification) => {
            if (notification.isDelete) {
                usernotificationids.push(notification.usernotificationid);
            }
        });

        this._commonService.endpointUrl = NotificationUrlConfig.EndPoint.notification.deleteNotification;
        const body = {
            usernotificationids: usernotificationids
        };

        this._commonService.create(body).subscribe(() => {
            this.isDeleteAll = false;
            this.getNotificationData(this.paginationInfo.pageNumber);
        });
    }
    routToCaseWorker(item: NotificationResult) {
        if (item.objecttype === 'servicecase') {
            this._sessionStorage.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.intakeserviceid + '/casetype';
            this._commonService.getAll(url).subscribe((response) => {
                const dsdsActionsSummary = response[0];
                if (dsdsActionsSummary) {
                    this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                    this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                    // common person for cw
                    const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/person-cw';
                    this._router.navigate([currentUrl]);
                }
            });
        } else {

            this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
                const dsdsActionsSummary = response[0];
                this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/report-summary';
                this._router.navigate([currentUrl]);
            });
        }
    }
    redirectToDoc(notification) {
        if (notification.objecttype === 'complaint') {
            this._dataStoreService.setData('ComplaintNumber', notification.intakeserviceid);
            this.providerID = this._dataStoreService.getData('ProviderPortalID');
            const currentUrl = '/provider-portal/dashboard/current-private-provider/' + this.providerID;
            this._router.navigate([currentUrl]);
        } else if (notification.objecttype === 'cap') {
            this._dataStoreService.setData('CapNumber', notification.intakeserviceid);
            this.providerID = this._dataStoreService.getData('ProviderPortalID');
            const currentUrl = '/provider-portal/dashboard/current-private-provider/' + this.providerID;
            this._router.navigate([currentUrl]);
        } else if ( notification.objecttype === 'monitoring') {
            const currentUrl = 'provider/monitoring/' + notification.intakeserviceid + '/program-info';
            this._router.navigate([currentUrl]);
        }
    }

    redirectToUser(notification) {
        if ( notification.objecttype === 'monitoring') {
            const currentUrl = 'provider/monitoring/' + notification.intakeserviceid + '/program-info';
            this._router.navigate([currentUrl]);
        } else if (notification.objecttype === 'complaint') {
            const currentUrl = '/provider/complaints/detail/' + notification.intakeserviceid + '/refferal';
            this._router.navigate([currentUrl]);
        }
    }
}
