import { Component, Input, OnInit } from '@angular/core';
import { DataStoreService, CommonHttpService, AlertService } from '../../../@core/services';
import { NotificationResult } from '../_entities/notification-entity.module';
import { PaginationRequest } from '../../../@core/entities/common.entities';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'notification-view',
    templateUrl: './notification-view.component.html',
    styleUrls: ['./notification-view.component.scss']
})
export class NotificationViewComponent implements OnInit {
    @Input()
    selectedNotification: NotificationResult;
    constructor(private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService) {}

    ngOnInit() {
        console.log('selectedNotification...', this.selectedNotification);
    }

    closePopup() {
        this._dataStoreService.setData('notification', true);
    }

    acknowledgeCap(capNumber) {
        const cap_number = {
            cap_number: capNumber
        };
        this._commonHttpService.create(cap_number, 'tb_provider_complaint_deficiency/acknowledgecap').subscribe( response =>{
            if (response) {
                this._alertService.success('Selected Cap has been acknowledged');
            }
        });

        /* this._commonHttpService.getArrayList(
            new PaginationRequest ({
              where: {
                cap_number: capNumber
              },
              nolimit: true,
              method: 'post'
          }), 'tb_provider_complaint_deficiency/acknowledgecap'
            ).subscribe(response => {
              if (response) {
                this._alertService.success('Selected Cap has been acknowledged');
              }
            }); */
    }
}
