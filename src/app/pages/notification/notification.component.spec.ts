import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap';

import { CoreModule } from '../../@core/core.module';
import { PageHeaderModule } from '../../shared';
import { ControlMessagesModule } from '../../shared/modules/control-messages/control-messages.module';
import { NotificationBroadcastMsgComponent } from './notification-broadcast-msg/notification-broadcast-msg.component';
import { NotificationResultComponent } from './notification-result/notification-result.component';
import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationViewComponent } from './notification-view/notification-view.component';
import { NotificationComponent } from './notification.component';

describe('NotificationComponent', () => {
    let component: NotificationComponent;
    let fixture: ComponentFixture<NotificationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            // tslint:disable-next-line:max-line-length
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                A2Edatetimepicker,
                RouterModule,
                CommonModule,
                NotificationRoutingModule,
                PaginationModule,
                FormsModule,
                CommonModule,
                PageHeaderModule,
                ReactiveFormsModule,
                ControlMessagesModule
            ],
            declarations: [NotificationComponent, NotificationComponent, NotificationViewComponent, NotificationResultComponent, NotificationBroadcastMsgComponent],
            providers: [PaginationConfig]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NotificationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
