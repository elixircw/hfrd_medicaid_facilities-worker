import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { AppUser } from '../../../@core/entities/authDataModel';
import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { AuthService, CommonHttpService, GenericService } from '../../../@core/services';
import { EntityNotification } from '../_entities/notification-entity.module';
import { NotificationUrlConfig } from '../notification.url.config';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'notification-entites',
    templateUrl: './notification-entites.component.html',
    styleUrls: ['./notification-entites.component.scss']
})
export class NotificationEntitesComponent implements OnInit {
    @Input() notify: Subject<string>;
    notificationResultData$: Observable<EntityNotification[]>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    totalResulRecords$: Observable<number>;
    selectedRecord: EntityNotification;
    selectedDeleteRow: EntityNotification;
    private token: AppUser;
    isBroadCastMessage = false;
    constructor(private _service: GenericService<EntityNotification>, private _commonService: CommonHttpService, private _authService: AuthService) {
        this._service.endpointUrl = NotificationUrlConfig.EndPoint.notification.notificationResult;
        this.token = this._authService.getCurrentUser();
    }
    ngOnInit() {
        const role = this._authService.getCurrentUser();
        if (role.role.name === 'superuser') {
            this.isBroadCastMessage = true;
        }
        this.getNotificationData(1);
    }

    getNotificationData(pageNo: number) {
        const body: PaginationRequest = {
            limit: 10,
            page: pageNo,
            where: { isexternalentity: true },
            method: 'post'
        };
        const source = this._service.getPagedArrayList(body).share();
        this.notificationResultData$ = source.pluck('data');
        if (pageNo === 1) {
            this.totalResulRecords$ = source.pluck('count');
        }
    }

    pageChanged(event) {
        this.paginationInfo.pageNumber = event.page;
        this.getNotificationData(this.paginationInfo.pageNumber);
    }
    selectNotification(selectedData: EntityNotification) {
        this.selectedRecord = selectedData;
    }
    selectDeleteNotification(selectedRow: EntityNotification) {
        console.log(selectedRow);
        this.selectedDeleteRow = selectedRow;
    }
    updateNotification(notification: EntityNotification) {
        this._commonService.endpointUrl = NotificationUrlConfig.EndPoint.notification.updateNotification;
        const body = {
            usernotificationid: notification.usernotificationid
        };
        if (!notification.isread) {
            this._commonService.create(body).subscribe((data) => {
                if (data === 'Success') {
                    notification.isread = true;
                } else {
                    notification.isread = false;
                }
            });
        }
    }
    deleteSelectedNotification() {
        this._commonService.endpointUrl = NotificationUrlConfig.EndPoint.notification.deleteNotification;
        const body = {
            usernotificationid: this.selectedDeleteRow.usernotificationid
        };

        this._commonService.create(body).subscribe((data) => {
            if (data === 'Success') {
                this.getNotificationData(this.paginationInfo.pageNumber);
            }
        });
    }
}
