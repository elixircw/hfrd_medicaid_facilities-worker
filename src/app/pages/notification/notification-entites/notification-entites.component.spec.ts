import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationEntitesComponent } from './notification-entites.component';

describe('NotificationEntitesComponent', () => {
  let component: NotificationEntitesComponent;
  let fixture: ComponentFixture<NotificationEntitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationEntitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationEntitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
