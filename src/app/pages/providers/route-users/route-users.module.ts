import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteUsersRoutingModule } from './route-users-routing.module';
import { RouteUsersComponent } from './route-users.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { RouteUserService } from './route-user.service';

@NgModule({
  imports: [
    CommonModule,
    RouteUsersRoutingModule,
    FormMaterialModule
  ],
  declarations: [RouteUsersComponent],
  exports: [RouteUsersComponent],
  providers: [RouteUserService]
})
export class RouteUsersModule { }
