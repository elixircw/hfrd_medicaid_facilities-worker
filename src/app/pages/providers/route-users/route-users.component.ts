import { Component, OnInit } from '@angular/core';
// need to change utilze common data model
import { RoutingUser } from '../../provider-referral/_entities/existingreferralModel';
import { Router, ActivatedRoute } from '@angular/router';
import { RouteUserService } from './route-user.service';
import { AlertService, AuthService, DataStoreService, SessionStorageService } from '../../../@core/services';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { CapConstants } from '../../provider-management/new-provider/constants';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'route-users',
  templateUrl: './route-users.component.html',
  styleUrls: ['./route-users.component.scss']
})
export class RouteUsersComponent implements OnInit {

  getUsersList: RoutingUser[];
  user: RoutingUser;
  token: AppUser;
  workerArr = ['Licensing Administrator', 'Quality Assurance', 'Support Staff'];
  managerArr = ['Executive Director', 'Deputy Director', 'Program Manager'];
  status: any;
  userType: any;
  agency: any;
  otherAgency: any;
  isMultiQA: boolean;
  multipleselectedperson = [];
  capStatus: any[] = [];
  constructor(private _router: Router,
    private route: ActivatedRoute,
    private _service: RouteUserService,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _dataStoreService: DataStoreService,
    private storage: SessionStorageService) {
      this.token = this._authService.getCurrentUser();
    }

  ngOnInit() {
    this.isMultiQA = false;
    const statusCode = this._dataStoreService.getData('ComplaintDecisions');
    if (statusCode && statusCode.status) {
      this.status = statusCode.status;
      this.agency = statusCode.agency;
      this.otherAgency = statusCode.agencyName;
    } else if (statusCode && statusCode.length) {
      this.status = statusCode[0].eventCode;
    }
    this._dataStoreService.setData('ComplaintDecisions', null);
    this.getUsersList = [];
    this.user = null;
    this.userType = this._authService.getAgencyName();
    this.routingUserList();
    this.capStatus = CapConstants.CAP_REVIEW_DECISONS;
  }

  routingUserList() {
    this._service.getRoutingUsersList('PRCM').subscribe(response => {
      if (this.status) {
        (<any>$('#route-userassign')).modal('show');
        this.getUsersList = response.data.filter(
          user => {
            if (this.userType === 'OLMDJS') {
              if ( this.status === 'refferal_approved' ||  this.status === 'complaint_approved') {
                this.isMultiQA = true;
                if (this.agency === 'DHS') {
                  if (user.rolecode === 'QA') {
                    return user;
                  }
                } else {
                  if (user.rolecode === 'PVRDJSQA' || user.userrole === 'DJS Quality Assurance Specialist(QA)') {
                    return user;
                  }
                }
              } else if ( this.status === 'complaint_submited' ||  this.status === 'referral_submitted') {
                if (this.otherAgency === 'OLM') {

                  if (user.rolecode === 'PM') {
                    return user;
                  }
                } else {
                  if (user.rolecode === 'PVRDJSR' || user.userrole === 'DJS Resource Specialist') {
                    return user;
                  }
                }
              } else if ( this.status === 'complaint_reviewed') {
                if (user.rolecode === 'PVRDJSSD' || user.userrole === 'DJS - Secretary or Designee') {
                  return user;
                }
              }
            } else {
              if ( this.status === 'refferal_approved' ||  this.status === 'complaint_approved') {
                this.isMultiQA = true;
                if (this.agency === 'DJS') {
                  if (user.rolecode === 'PVRDJSQA' || user.userrole === 'DJS Quality Assurance Specialist(QA)') {
                    return user;
                  }
                } else {
                  if (user.rolecode === 'QA') {
                    return user;
                  }
                }
              } else if ( this.status === 'complaint_submited' ||  this.status === 'referral_submitted') {
                if (this.otherAgency === 'OLMDJS') {
                  if (user.rolecode === 'PVRDJSR' || user.userrole === 'DJS Resource Specialist') {
                    return user;
                  }
                } else {
                  if (user.rolecode === 'PM') {
                    return user;
                  }
                }
              } else if ( this.status === 'complaint_reviewed') {
                if (user.rolecode === 'ED') {
                  return user;
                }
              } else {
                const isCap = this.capStatus.filter( status => { 
                    if( status.eventcode === this.status ) {
                        return status;
                    }
                 });
                 if(isCap.length) {
                   if ( this.status === CapConstants.CAP_STATUS.cap_accepted && ( user.userrole && user.userrole.trim() === AppConstants.ROLES.PROGRAM_MANAGER)) {
                    return user;
                   } else if ( this.status === CapConstants.CAP_STATUS.cap_reviewed && ( user.userrole &&  user.userrole.trim() === AppConstants.ROLES.EXECUTIVE_DIRECTOR)) {
                    return user;
                   }else {
                     // return user;
                   }
                 } else {
                this.getUsersList = response.data;}
              }
            }
        });
      } else {
        this.getUsersList = response.data;
      }
    });
  }

  close() {
    (<any>$('#route-userassign')).modal('hide');
    this._router.navigate(['../'], { relativeTo: this.route });
  }

  selectPerson(user: RoutingUser) {
    this.user = user;
  }

  onConfirm() {
    if (this.user) {
      this._service.broardCastSelectedUsers(this.user);
      this._router.navigate(['../'], { relativeTo: this.route });
      (<any>$('#route-userassign')).modal('hide');
    } else {
      this._alertService.error('Please select the user');
    }

  }

  selectMultipleUser(modal, getperson: any, listindex) {
    if (modal) {
      this.multipleselectedperson.push({
        userid: getperson
      });
    } else {
      this.multipleselectedperson.forEach((item, index) => {
        if (item.userid === getperson) {
          this.multipleselectedperson.splice(index, 1);
        }
      });
    }
    this.user = Object.assign({ assigneduserid: this.multipleselectedperson });
  }

}
