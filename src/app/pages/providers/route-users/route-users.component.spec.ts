import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteUsersComponent } from './route-users.component';

describe('RouteUsersComponent', () => {
  let component: RouteUsersComponent;
  let fixture: ComponentFixture<RouteUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
