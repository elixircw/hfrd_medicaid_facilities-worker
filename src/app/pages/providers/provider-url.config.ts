export class ProviderUrlConfig {
    public static EndPoint = {
        Dashboard: {
            DSDSActionDetailsUrl: 'servicerequestsearches/usersservicerequest',
            SupervisorReviewListUrl: 'servicerequestsearches/pendingreviewdalist',
            SupervisorClosedReviewListUrl: 'servicerequestsearches/closeddalist',
            MyIntakeListUrl: 'Intakedastagings/listintakeda',
            DsdsActionSummaryUrl: 'Intakeservicerequests/getdsdsactionsummarydtls',
            GetPrior: 'intakedastagings/getpriors'
        },
        CaseFolders: {
            GetCaseList: 'servicerequestsearches/usersservicerequestfolders?data',
            PersonSeach: 'intakedastagings/personsearchfolders?filter',
            FolderList: 'intakedastagings/servicerequestsearchfolders?filter'
        },
        DSDSAction: {
            ReportSummary: {
                IncidentType: 'manage/Servicerequestincidenttype',
                PossibleCheckList: 'Intakeservicerequestillegalactivitytypes',
                Source: 'Intakeservicerequestinputtypes',
                ReportSummary: 'Intakeservicerequests/reportsummary/list/',
                UpdateReportSummary: 'Intakeservicerequests/update'
            },
            Assessment: {
                ListAssessment: 'admin/assessment/list',
                InvolvedPersonList: 'people/getinvolvedperson?filter',
                RoutingInfoList: 'routing/getroutingdetails?filter',
                AssessmentByName: 'admin/assessment/getassessmentdetailsbytitleheadertext'
            },
            CrossReference: {
                CrossReferenceUrl: 'Intakeservicerequestcrossreferences/getAllCrossReferencebySerReqid',
                SameRaReporterApUrl: 'Intakeservicerequests/getCrossRefByPersonType',
                CrossRefDaUrl: 'servicerequestsearches/getDetails',
                CrossRefProviderUrl: 'Intakeservicerequests/getCrossRefBySameProvider',
                CrossRefAddUrl: 'Intakeservicerequestcrossreferences',
                CrossReferenceReasonTypesUrl: 'Intakeservicerequestcrossreferencereasontypes/'
            },
            Disposition: {
                StatusUrl: 'Intakeservicerequestdispositioncodes/getstatuslist',
                DispositionUrl: 'daconfig/servicerequesttypeconfigdispositioncode/getdispositionlist',
                DispositionListUrl: 'Intakeservicerequestdispositioncodes/GetDispositon',
                DispositionAddUrl: 'Intakeservicerequestdispositioncodes/Add',
                DispositionValidation: 'Intakeservicerequestdispositioncodes/taskvalidationdisposition'
            },
            InvestigationPlan: {
                PlanSummaryUrl: 'Investigations/GetTaskSummary',
                ActivityTaskUrl: 'Investigations/GetActivityTask',
                ActivityGoalUrl: 'Investigations/GetActivityGoal',
                AmmappingListUrl: 'admin/ammapping/list',
                GetActivitiesUrl: 'Activities',
                GetActvitiesAddUrl: 'Activities/add',
                GetActivitiesListUrl: 'Activities',
                GetActivityTaskUrl: 'Intake/activitytask',
                ActivityGoalTypeUrl: 'Intake/activitygoaltype',
                ActivityGoalStatusTypeUrl: 'Intake/activitygoalstatustype',
                InvestigationDispositionUrl: 'Intake/activitytasktypestatusdispositionmap/investigationdisposition/list',
                GoalDispositionUrl: 'Intake/amgoalstatus/investigationgoaldisposition/list',
                InvestigationActivitiesUrl: 'Intakeservicerequests/investigationactivities/list',
                ActivityAllTaskUrl: 'admin/activitytasktype',
                GoalUpdateUrl: 'Intake/activitygoal',
                MappingListUrl: 'admin/amactivity/mappinglist',
                ActivitySaveUrl: 'Intake/activitytask/add',
                AddTaskUrl: 'Intake/activitytask/addtask',
                ActivityTaskUpdateUrl: 'Intake/activitytask',
                UserProfilekUrl: 'admin/teammemberassignment/investigationplanassignto/list',
                TaskStatusTypeUrl: 'Intake/activitytaskstatustype'
            },
            InvolvedPerson: {
                InvolvedPersonListUrl: 'Intakeservicerequestactors/GetPersonList',
                YouthInvolvedPersonListUrl: 'Intakeservicerequests/prepopasmtprepostdiacharge',
                InvolvedPersonSearchUrl: 'globalpersonsearches/getPersonSearchData',
                PriorListUrl: 'Intakeservicerequests/getdsdsactions',
                GenderTypeUrl: 'admin/gendertype',
                EthnicGroupTypeUrl: 'admin/ethnicgrouptype',
                MartialStatusTypeUrl: 'admin/maritalstatustype',
                RaceTypeUrl: 'admin/racetype',
                LanguageTypeUrl: 'admin/languagetype',
                IncomeTypeUrl: 'admin/incometype',
                PersonIdentifierTypeUrl: 'admin/personidentifiertype',
                PersonPhoneTypeUrl: 'admin/Personphonetype',
                PersonAddressUrl: 'admin/personaddresstype',
                StateListUrl: 'States',
                CountyListUrl: 'admin/county/list',
                CountyList: 'admin/county',
                RelationshipTypesUrl: 'Relationshiptypes/list',
                ActorTypeUrl: 'admin/actortype',
                ZipCodeUrl: 'Regions',
                DeletePerson: 'Intakeservicerequestactors/deletewithintakeserreqnumber',
                Person: 'People/getwholepersondetials',
                Relationship: 'Relationshiptypes',
                ActorType: 'admin/actortype',
                SavePerson: 'People/add',
                UpdatePerson: 'People/personaddupdate',
                CheckPerson: 'People/checkpersonrole',
                UserActorTypeUrl: 'admin/actortype/listActortype',
                PersonAddressesUrl: 'Personaddresses',
                LivingArrangementTypeUrl: 'admin/livingarrangementtype',
                ReligionTypeUrl: 'admin/religiontype',
                EmailTypeUrl: 'admin/personemailtype',
                PhoneTypeUrl: 'admin/personphonetype',
                PersonList: 'People/getpersondetail',
                UnkPersonList: 'Intakedastagings/getintakejsondata',
                updateUnkPersonList: 'Intakedastagings/updateintakejsondata',
                personservicetype: 'personservicetype',
                dentalspecialtytype: 'dentalspecialtytype',
                healthprofessiontype: 'healthprofessiontype',
                healthdomaintype: 'healthdomaintype',
                healthassessmenttype: 'healthassessmenttype',
                medicalconditiontype: 'medicalconditiontype',
                prescriptionreasontype: 'prescriptionreasontype',
                informationsourcetype: 'informationsourcetype',
                medicationtype: 'medicationtype',
                physicianspecialtytype: 'physicianspecialtytype'
            },
            Sdmcaseworker: {
                SdmvaluesUrl: 'Intakeservicerequestsdm/getintakeservicerequestsdm',
                SdmCountyListUrl: 'admin/county',
                PathwaySdmListUrl: 'Intakeservicerequestsdm/getintakeservicerequestsdm?filter',
                PathwaySdmCreateUrl: 'Intakeservicerequestsdm/createsdm',
                PathwayAutorizationUrl: 'Intakedastagings/pathwayapprove'
            },
            InvolvedEntity: {
                EntityGridUrl: 'Intakeservicerequestagencies/list',
                EntityRoleGridUrl: 'Intakeservicerequests/entityroletypes/list',
                AddEntityRoleUrl: 'Intakeservicerequestagencies/addagency',
                DeleteEntityUrl: 'Intakeservicerequestagencies',
                UpdateEntityRoleUrl: 'Intakeservicerequestagencies/update'
            },
            Allegation: {
                AllegationInvestigationUrl: 'Investigationallegationactors/getinvestigationallegationdetail',
                AllegationListUrl: 'Investigations/getinvestigationallegationlist',
                AllegationStatusDropdownUrl: 'Investigationallegationstatustypes',
                AllegationIndicatorUrl: 'investigationallegations/allegationList',
                AllegationAddUpdateUrl: 'Investigationallegations/',
                AllegationDeleteUrl: 'Investigationallegations/deleteallegation',
                LinkPersonUrl: 'Investigationallegations/actorList/',
                AddLinkPersonUrl: 'Investigationallegations/addActors/',
                IndicatorListUrl: 'admin/allegation/indicatorsList/',
                AllegationEditUrl: 'Investigationallegationactors/getAllegations/',
                AllegationUpdateUrl: 'Investigationallegationactors/updateAllegationActor',
                AllegationURL: 'admin/allegation/list'
            },
            Referrals: {
                IntakeServiceRequestReferralListUrl: 'Intake/Intakeservicerequestreferral',
                IntakeServiceRequestReferralUrl: 'Intake/Intakeservicerequestreferral/add',
                ReferredToTypeUrl: 'admin/referredtotype',
                ReasonListUrl: 'daconfig/servicerequesttypeconfig/reasonlist',
                DispositionListUrl: 'daconfig/servicerequesttypeconfig/dispositionlist'
            },
            Attachment: {
                UploadAttachmentUrl: 'attachments/uploadsFile',
                AttachmentGridUrl: 'Documentproperties/getintakeattachments',
                AttachmentUploadUrl: 'attachments/uploadsFile',
                AttachmentTypeUrl: 'Intake/attachmenttype/',
                AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype/',
                SaveAttachmentUrl: 'Documentproperties/addintakeattchment',
                DeleteAttachmentUrl: 'Documentproperties/delete'
            },
            Recording: {
                GetAllDaRecordingUrl: 'admin/progressnote/getalldarecording',
                ListProgressSubTypeUrl: 'admin/progressnotetype/listprognotesubtypes',
                AddRecordingUrl: 'admin/progressnote/addRecordings',
                GetRecordingDetailUrl: 'admin/progressnote/getdarecordingdetails',
                UpdateRecordingUrl: 'admin/progressnote/updateRecordings',
                ListProgressTypeUrl: 'admin/progressnotetype/listprognotetypes',
                AddResourceConsultURL: 'intakeservicerequestconsultreview/addupdate',
                GetResourceConsultURL: 'intakeservicerequestconsultreview/listconsultreview',
                GetSelectTypeURL: 'consultreviewusertype/',
                DeleteResourceURL: 'intakeservicerequestconsultreview/consultreviewdelete',
                DeleteResourceUserURL: 'intakeservicerequestconsultreview/consultreviewconfigdelete'
            },
            ChildRemoval: {
                GetRemovalReason: 'removalreasontype',
                InvolvedPersonList: 'People/getinvolvedperson',
                GetState: 'States',
                GetSentTo: 'admin/agencytype',
                GetChildRemovalList: 'intakeservreqchildremoval/getchildremoval',
                SubmitChildRemoval: 'intakeservreqchildremoval/add',
                Notification: 'intakeservreqchildremoval/removalnotificationmail',
                GetRemovalNotificationRoles: 'intakeservreqchildremoval/removalnotification',
                routingUserUrl: 'routing/getldssusers',
                EmailSchoolNotification: 'Usernotifications/notificationmail',
                FourEChildRemoval: 'Intakeservreqchildremoval/addlegalstatus',
                GetTypes: 'referencetype/gettypes',
            },
            Court: {
                PetitionDetailsSave: 'intakeservicerequestpetition/add',
                PetitionDetailslist: 'intakeservicerequestpetition',
                HearingTypeUrl: 'hearingtype',
                CourtActionTypeUrl: 'courtactiontype',
                FindingTypeUrl: 'findingtype',
                ConditionTypeUrl: 'conditiontype',
                CourtOrderTypeUrl: 'courtordertype',
                AdjudicatedDecisionTypeUrl: 'adjudicateddecisiontype',
                OffenceCategoryListUrl: 'OffenceCategoryListUrl',
                CourtActionAddUrl: 'intakeservicerequestcourtaction/add',
                HearingStatusTypeUrl: 'hearingstatustype',
                PetitionTypeUrl: 'petitiontype',
                PetitionListUrl: 'intakeservicerequestpetition/getpetitiondetails',
                OutComeURL: 'referencetype/gettypes',
                HearingAddUrl: 'intakeservicerequestcourthearing/addhearing',
                HearingUpdateUrl: 'intakeservicerequestcourthearing/updatehearing',
                getHearingUrl: 'intakeservicerequestcourthearing/gethearingdetails',
                getCourtActions: 'intakeservicerequestcourtaction/getcourtaction',
                deleteHearing: 'intakeservicerequestcourthearing',
                hearingoutComeUrl: 'referencetype/gettypes',
                GetCourtOrderUrl: 'intakeservreqcourtorder/getcourtorder',
                ChecklistUrl: 'checklist',
                CourtOrderSaveUrl: 'intakeservreqcourtorder/add',
                AggravatedcircumstancesUrl: 'referencetype/gettypes'
            },
            MaltreatmentInformation: {
                MaltreatementTypeUrl: 'investigationallegations/allegationList',
                GetMaltreatementTypeUrl: 'admin/allegation/allegationWOIndicatorsList',
                IncidentLocationDropDown: 'incidentlocationtype/',
                MaltreatorUrl: 'admin/allegation/indicatorsList/',
                InjuryTypeUrl: 'injurytype',
                MaltreatmentCharactersticsTypeUrl: 'maltreatmentcharactersticstype',
                injuryCharactersticsTypeUrl: 'injurycharactersticstype',
                JurisdictionListUrl: 'admin/county',
                HouseHoldTypeUrl: 'householdtype',
                MaltreatmentInformation: 'Investigationallegations/getinvestigationallegation',
                AddMaltreatmentINformationUrl: 'investigationmaltreatment/addupdate',
                providermaltreatmenttypeUrl: 'providermaltreatmenttype/'
            },
            ServicePlan: {
                GoalUrl: 'serviceplan/goal',
                AddActivityUrl: 'serviceplan/activity',
                ActivityUrl: 'serviceplan/activity/list',
                AddServicePlanUrl: 'serviceplan/add',
                DelectServicePlanUrl: 'serviceplan/activity',
                InvolvedPersonUrl: 'People/getpersondetail',
                ServiceLog: 'serviceplan/servicelog',
                GetCategoryProgramNames: 'servicepan/serviceslog/getprogramsname',
                ClientprogramServices: 'TbServices/agencyservices',
                ClientProgramName: 'TbAgencyProgramAreas/agencyProvidedServices',
                agencyServiceLog: 'serviceLogs/list',
                Frequency: 'Picklistvalues/frequencyList',
                DelectServiceLog: 'serviceLogs',
                Duration: 'Picklistvalues/durationList',
                VendorServices: 'TbServices/vendorservices',
                VendorServiceSave: 'serviceLogs/vendorSaveServiceLog',
                vendorServiceLog: 'serviceLogs/getVendorList',
                fiscalCodes: 'fiscalCodes/agencyservices',
                VendorServiceSearch: 'providerListSearches/values',
                ServiceEndReason: 'Picklistvalues/serviceEndReason',
                ReasonServiceNotReceived: 'Picklistvalues/reasonServiceNotReceiveds',
                PurchaseAuthorizationSave: 'purchaseAuthorizations/purchaseAuthorize',
                PurchaseAuthorizationGet: 'purchaseAuthorizations/getPurchaseAuthorize',
                ApprovePurchaseAuthorization: 'tb_service_purchase_authorization/updateservicepurchaseauthorization'
            },
            AsServicePlan: {
                GoalUrl: 'serviceplan/goal/list',
                AsAddActivityUrl: 'servicetype/list',
                AsAddSubService: 'referencetype/gettypes',
                VendorServiceSearch: 'providerListSearches/values',
                ServiceEndReason: 'Picklistvalues/serviceEndReason',
                ReasonServiceNotReceived: 'Picklistvalues/reasonServiceNotReceiveds',
                PurchaseAuthorizationSave: 'purchaseAuthorizations/purchaseAuthorize',
                PurchaseAuthorizationGet: 'purchaseAuthorizations/getPurchaseAuthorize'
            },
            ArCaseClosure: {
                CaseClosureListUrl: 'caseclosureparticipant/getcaseclosurelist',
                CaseClosureServiceTypeUrl: 'closuretype',
                CaseClosureServiceSubTypeUrl: 'closuresubtype',
                CaseClosureStatusAddUpdateUrl: 'caseclosuresummary/addupdate',
                SummaryValidationUrl: 'caseclosuresummary/validatecaseclosurelist',
                InvolvedPersonListUrl: 'People/getinvolvedperson'
            },
            appointment: {
                intakeWorkerList: 'Intakedastagings/getIntakeUsers',
                addupdateAppointment: 'servicerequestappointment/addupdate',
                getAppointmentHistory: 'servicerequestappointment/appointmentlisthistory',
                getAppointmentList: 'servicerequestappointment/appointmentlisthistory',
                deleteAppointment: 'servicerequestappointment/appointmentdelete'
            },
            Placement: {
                PlacementAddUrl: 'placement/add',
                PlacementListUrl: 'placement/list',
                childcharacteristicUrl: 'childcharacteristictype',
                PlacementSearch: 'placement/search',
                PlacementSearchAdd: 'placement/addjs',
                PlacementUpdate: 'placement/updateplacement',
                PlacementLRAddUpdate: 'placementleavereturn/addupdate',
                PlacementLRList: 'placementleavereturn/list',
                PlacementReleaseAddUpdate: 'placementrelease/addvalues',
                PermanencyPlan: {
                    PermanencyPlanList: 'permanencyplan/list',
                    PermanencyPlanAdd: 'permanencyplan/add',
                    PermanancyPlanTypeSubType: 'Permanencyplantypes/list'
                },
                Adoption: {
                    TprRecommendationList: 'tprrecommendation/gettprrecommendation',
                    TprRecommendationAdd: 'tprrecommendation/add',
                    LegalCustodyAddUpdate: 'legalcustody/addupdate',
                    LegalCustodyList: 'legalcustody/getlegalcustody',
                    TPRDetail: 'tprdetails/gettprdetails',
                    AdoptionEffortAdd: 'adoptionplanning/addupdate',
                    AdoptionEffortList: 'adoptionplanning/getadoptionplanning',
                    AdoptionEmotionalTieList: 'adoptionemotional/list',
                    AdoptionEmotionalTieAdd: 'adoptionemotional/addupdate'
                }
            },
            Gap: {
                AnnualReview: 'gapannualreview/list',
                AddEditReview: 'gapannualreview/addupdate'
            },
            Fim: {
                MeetingTypesUrl: 'Meetingtypes/',
                FamilyMeetingTypesUrl: 'Familymeetingtypes/list',
                ParticipantTypesUrl: 'Participanttypes/list',
                AddParticipantUrl: 'Meetingrecordings/addupdate',
                FimListUrl: 'Meetingrecordings/getfimdetails'
            },
            legalActionHistory: {
                hearingType: 'hearingtype?filter={"nolimit":true}'
            },
            entities: {
                RegionListUrl: 'admin/region/list',
                AgencyCategoryUrl: 'admin/agencycategory',
                EntityRoletypeUrl: 'admin/agencyroletype',
                InvolvedEnititesSearchUrl: 'gloabalagencysearches/getAgencySearchData'
            },
            Transport: {
                TransportationList: 'persontransportation/list?filter',
                FromList: 'locationfromtype?filter',
                ToList: 'locationtotype?filter',
                AddTransport: 'persontransportation/add',
                DeleteTransport: 'persontransportation/deletetransport',
                TransportationById: 'persontransportation/getpersontransportationbyId?filter',
                UpdateTransport: 'persontransportation/updatetransport/'
            },
            kinship: {
                attachmentUrl: 'Documentproperties/getcaseworkerattachments/',
                checklistUrl: 'admin/amtask',
                AddKinshipUrl: 'kinshipcare/addupdate',
                KinshipListUrl: 'kinshipcare/list'
            },
            as_maltreatment: {
                getintakejsondata: 'Intakedastagings/getintakejsondata',
                AllegationsIndicaorUrl: 'admin/allegation/getallegationsandindicaors'
            },
            courtactions: {
                CourtActionsList: 'intakeservicerequestpetition/listadjdispdetails?filter'
            },
            foster_care: {
                childCharacteristicsUrl: 'tb_picklist_values/getpicklist?filter',
                fcProviderSearchUrl: 'tb_provider/fostercareprovidersearch?filter',
                otherLocalDeptmntTypeUrl: 'tb_picklist_values/getpicklist?filter',
                bundledPlcmntServicesTypeUrl: 'tb_services/getbundledplacementserviceslist?filter',
                placementStrTypeUrl: 'tb_services/getplacementstructureslist?filter',
                saveReferalUrl: 'tb_placement/fostercarereferaladd',
                fostercarereferallistUrl: 'tb_placement/fostercarereferallist',
                addPlacementRequestUrl: 'tb_placement/addplacement',
                approvePlacementRequestUrl: 'tb_placement/approverejectedplacement',
                rejectPlacementRequestUrl: 'tb_placement/approverejectedplacement',
                removeplacement: 'tb_placement/exitplacement',
                placementexitapproval: 'tb_placement/placementexit',
                approveOrReject: 'routing/routingupdate',
                ExitTypeUrl: `tb_picklist_values/getpicklist`,
                PlChildHistoryListUrl: 'tb_placement/placementchildhistorylist',
                ExitReasonTypeUrl: 'tb_picklist_values/getexitreasonlist',
                cpaHomePlacementLHistoryUrl: 'tb_placement/cpahomeplacementlist',
                cpaHomeProviderNameUrl: 'tb_provider/getcpahomeprovidername',
                addcpaHomeplacementUrl: 'tb_placement_cpa_homes/cpahomeplacementadd'
            },
            ActionLetter: {
                GetProgramType: 'referencetype/gettypes?filter',
                GetActionType: 'referencetype/gettypes?filter',
                GetProgramList: 'actionletterprogramconfig/getprogramheaderconfig?filter',
                SaveCitation: 'oasactionletter/add',
                ListCitation: 'oasactionletter/list?filter'
            },
            AdultScreenTool: {
                listAST: 'intakeservreqadultscreentool/list?filter'
            }
        }
    };
}
