export class ProvidersUrlConfig {
    public static EndPoint = {
        Complaint: {
            GetNextNumberUrl: `Nextnumbers/getNextNumber?apptype=providerComplaint`,
            CREATE_UPDATE_URL : `tb_provider_complaint/addupdate`,
            DETAIL_URL : `tb_provider_complaint/providercomplaintdetail?filter`,
            LIST_URL : `tb_provider_complaint/list`,
            DECISION_URL: `tb_provider_complaint/routingupdate`,
            DECISION_LIST_URL: `tb_provider_complaint/routinglist?filter`,
            DEFIICENCY_URL : `tb_provider_complaint_deficiency/list?filter`,
            CREATE_UPDATE_DEFIICENCY_URL : `tb_provider_complaint_deficiency/addupdate`,
            CONTACT_URL : `tb_provider_complaint_contacts/addupdate`,
            CONTACT_NOTE_CREATE_UPDATE_URL : `tb_provider_complaint_notes/addupdate`,
            CONTACT_LIST_URL : `tb_provider_complaint_contacts/list?filter`,
            CONTACT_NOTES_LIST_URL : `tb_provider_complaint_notes/list?filter`,
            SEND_MAIL : `tb_provider_complaint/sendalert`,
        },

        Training: {
            GetNextTrainingNumberUrl: `Nextnumbers/getNextNumber?apptype=providerTraining`,
            GET_TRAINING_DETAILS: 'providerorientationtraining?filter'
        },
        Monitoring : {
            VISIT_DETAIL_URL : `prov_monitoring/list?filter`,
            SITE_DETAIL_URL : `prov_monitoring/listprograminfo?filter`,
            LICENSE_INFO_BY_PROVIDER: 'providercontract/getlicenseinfobyprovider',
            REMAINING_LICENSE_FOR_MONITORING: 'prov_monitoring_site/getRemainingLicenseForMonitoring',
            GET_RECORD_QUESTION: 'prov_record_question/getrecordquestion?filter',
            GET_VIOLATED_QUESTION: 'prov_monitoring_exit_conf/getnoncompliance?filter',
            GET_STAFF_IN_SITE: 'providerstaff/getassignedstaff',
            GET_RECORD_DATA: 'prov_record_question/getrecordsite?filter',
            GET_RECORD_ANSWER : 'prov_record_question/getrecordanswer?filter',
            YOUTH_INTERVIEW: 'monitoring_youth_interview/getyouthinterview',
            ENTRANCE_CONF_URL: 'prov_monitoring_entrance_conf/listentranceconf?filter',
            ADD_MONITORING_SITE: 'prov_monitoring_site/addupdate',
            GET_LICENSE_FOR_MONITORING: 'prov_monitoring_site/getLicenseForMonitoring'
        }
    };
}
