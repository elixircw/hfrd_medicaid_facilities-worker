import { Injectable } from '@angular/core';
import { AuthService } from '../../@core/services';
import { ProviderConstants } from './constants';
import { AppUser } from '../../@core/entities/authDataModel';

@Injectable()
export class ProviderAuthorizationService {

  roleName: string;
  userProfile: AppUser;
  constructor(private authService: AuthService) {
    this.userProfile = this.authService.getCurrentUser();
      this.roleName = (this.userProfile && this.userProfile.role) ? this.userProfile.role.name : null;
  }

  /**
   * Provider Complaints Authorizations
   */

  hasNewComplaintAccess(): boolean {
    let access = false;
    if (ProviderConstants.NEW_COMPLAINT_ACCESS.indexOf(this.roleName) !== -1) {
      access = true;
    }
    return access;
  }

  getComplaintDashboardTabs() {
    return ProviderConstants.COMPLAINT_DASHBOARD_TABS.filter(tab => tab.roles.indexOf(this.roleName) !== -1);
  }

  getComplaintDetailsTabs(status: string) {
    console.log('tab status', status);
    const tabs = ProviderConstants.COMPLAINT_DETIAL_TABS.filter(tab => {
      if (tab.roles.indexOf(this.roleName) !== -1 && tab.statuses.indexOf(status) !== -1) {
        // if (tab.roles.indexOf(this.roleName) !== -1) {
        return true;
      } else {
        return false;
      }
    });
    console.log('tab status', status, tabs);
    return tabs;
  }

  getCapDecisions(status: string) {
    return ProviderConstants.COMPLAINT_REVIEW_DECISONS.filter(tab => {
      if (tab.roles.indexOf(this.roleName) !== -1 && tab.statuses.indexOf(status) !== -1) {
        return true;
      } else {
        return false;
      }
       
    });
  }
  getDecisons(status: string) {
    if ( status === 'complaint_submited') {
      return ProviderConstants.COMPLAINT_REVIEW_DECISONS.filter(tab => {
        // if (tab.roles.indexOf(this.roleName) !== -1 && tab.statuses.indexOf(status) !== -1) {
        if (tab.roles.indexOf(this.roleName) !== -1) {
          return true;
        } else {
          return false;
        }
      });

    } else if ( status === 'complaint_reviewed') {
      return ProviderConstants.COMPLAINT_REVIEW_DECISONS.filter(tab => {
        // if (tab.roles.indexOf(this.roleName) !== -1 && tab.statuses.indexOf(status) !== -1) {
        if (tab.roles.indexOf(this.roleName) !== -1) {
          return true;
        } else {
          return false;
        }
      });

    } else if ( status === 'complaint_approved') {
      return ProviderConstants.COMPLAINT_REVIEW_DECISONS.filter(tab => {
        // if (tab.roles.indexOf(this.roleName) !== -1 && tab.statuses.indexOf(status) !== -1) {
        if (tab.roles.indexOf(this.roleName) !== -1) {
          return true;
        } else {
          return false;
        }
      });

    } else {
      return ProviderConstants.COMPLAINT_DECISONS.filter(tab => {
        // if (tab.roles.indexOf(this.roleName) !== -1 && tab.statuses.indexOf(status) !== -1) {
        if (tab.roles.indexOf(this.roleName) !== -1) {
          return true;
        } else {
          return false;
        }
      });
    }
  }

  /**
   * Provider Training Authorizations
   */
  getTrainingDetailsTabs() {
    return ProviderConstants.TRAINING_DETAILS_TABS.filter(tab => tab.roles.indexOf(this.roleName) !== -1);
  }
}
