import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncidentReportComponent } from './incident-report.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: IncidentReportComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidentReportRoutingModule { }
