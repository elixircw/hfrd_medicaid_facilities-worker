import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IncidentReportRoutingModule } from './incident-report-routing.module';
import { IncidentReportComponent } from './incident-report.component';
import { IncidentReportDashboardModule } from '../../../shared/modules/provider-incident/provider-incident-report/incident-report-dashboard/incident-report-dashboard.module';


@NgModule({
  imports: [
    CommonModule,
    IncidentReportRoutingModule,
    IncidentReportDashboardModule
  ],
  declarations: [IncidentReportComponent]
})
export class IncidentReportModule { }
