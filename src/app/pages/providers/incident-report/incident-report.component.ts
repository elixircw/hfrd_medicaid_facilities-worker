import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { IncidentReportDashboardService } from '../../../shared/modules/provider-incident/provider-incident-report/incident-report-dashboard/incident-report-dashboard.service';
import { PaginationInfo, DynamicObject } from '../../../@core/entities/common.entities';


@Component({
  selector: 'incident-report',
  templateUrl: './incident-report.component.html',
  styleUrls: ['./incident-report.component.scss']
})
export class IncidentReportComponent implements OnInit {

  totalRecords: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  dynamicObject: DynamicObject = {};
  private pageStream$ = new Subject<number>();
  private searchTermStream$ = new Subject<DynamicObject>();
  previousPage: number;
  uirList = [];
  tabs = [];
  status: string;

  constructor(private _service: IncidentReportDashboardService,) { }

  ngOnInit() {
    this.paginationInfo.sortBy = 'desc';
    this.paginationInfo.sortColumn = 'updateddate';
    this.status = this.tabs.length ? this.tabs[0].filterName : 'review';
    this.loadDashboardList(1, this.status);
  }

  loadDashboardList(selectPageNumber: number, status: string) {
    this.status = status;
    const pageSource = this.pageStream$.map(pageNumber => {
      if (this.paginationInfo.pageNumber !== 1) {
        this.previousPage = pageNumber;
      } else {
        this.previousPage = this.paginationInfo.pageNumber;
      }
      return { search: this.dynamicObject, page: this.previousPage };
    });
    this._service.getDashboardList(this.paginationInfo.pageSize, this.previousPage, { status: this.status }).subscribe(result => {
      this.uirList = result.data;
      this.totalRecords = result.count;
    });
  }
}
