import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachmentRoutingModule } from './attachment/attachment-routing.module';
import { AttachmentUploadComponent } from './attachment/attachment-upload/attachment-upload.component';

@NgModule({
  imports: [
    CommonModule,
    AttachmentRoutingModule,
  ],
  declarations: [],
  exports:[]
})
export class ProviderUtilsModule { }
