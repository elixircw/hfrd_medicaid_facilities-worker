import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { map } from 'rxjs/operators';

import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { GenericService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ProviderUrlConfig } from '../../provider-url.config';
import { Attachment } from './_entities/attachment.data.models';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import {GeneratedDocuments} from './_entities/attachment.data.models';
import {AppUser} from '../../../../@core/entities/authDataModel';
import {AuthService} from '../../../../@core/services/auth.service';
import {DataStoreService} from '../../../../@core/services/data-store.service';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';
import { AttachmentService } from './attachment.service';
import * as jsPDF from 'jspdf';



const SCREENING_WORKER = 'SCRNW';
declare var $: any;
import * as ALL_DOCUMENTS from './_configurtions/documents.json';
import { ComplaintDetailsService } from '../../complaint/complaint-detail/complaint-details.service';
import { ComplaintDetail } from '../../complaint/complaint-detail/_entities/complaint-detail.model';
import { ProviderComplaintContactsService } from '../../complaint/complaint-detail/provider-complaint-contacts/provider-complaint-contacts.service';
import { DeficiencyViolationService } from '../../complaint/complaint-detail/provider-deficiency-violation/deficiency-violation.service';
import { ProviderComplaintSummaryService } from '../../complaint/complaint-detail/provider-complaint-summary/provider-complaint-summary.service';
import * as moment from 'moment';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attachment',
    templateUrl: './attachment.component.html',
    styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit {
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    daNumber: string;
    id: string;
    baseUrl: string;
    filteredAttachmentGrid: Attachment[] = [];
    @ViewChild(EditAttachmentComponent) editAttach: EditAttachmentComponent;
    documentPropertiesId: any;
    documentId: any;
    generatedDocuments: GeneratedDocuments[] = [];
    allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;
    token: AppUser;
    config = {isGenerateUploadTabNeeded: false};
    downldSrcURL: any;
    downloadInProgress: boolean;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    complaintDetail: ComplaintDetail;
    curDate = new Date();
    searchContactCritera: any;
    contactList = [];
    deficiencyList = [];
    providerNm = '';
    summary = '';
    outcomes = '';
    isDJSUser : boolean = false;
    isViewOnly: Boolean = false;
    constructor(
        private router: Router,
        private _dropDownService: CommonHttpService,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _service: GenericService<Attachment>,
        private _authService: AuthService,
        private _dataStoreService: DataStoreService,
        private _attachmentService: AttachmentService,
        private _complaintDetailsService: ComplaintDetailsService,
        private _complaintContact: ProviderComplaintContactsService,
        private _deficiencyList: DeficiencyViolationService,
        private _complaintSummary: ProviderComplaintSummaryService
    ) {
        this.id = this._attachmentService.getAttachmentConfig().uuid;
        this.daNumber = this._attachmentService.getAttachmentConfig().uniqueNumber;
        this.token = this._authService.getCurrentUser();
        this.complaintDetail = this._complaintDetailsService.complaintDetail;
    }

    ngOnInit() {
        this.attachment();
        this.token = this._authService.getCurrentUser();
        console.log(this.token);
        if(this.token  && this.token.user&& this.token.user.userprofile && this.token.user.userprofile.teammemberassignment && this.token.user.userprofile.teammemberassignment.teammember&& this.token.user.userprofile.teammemberassignment.teammember.roletypekey === "PVRDJSR" ) {
            this.isDJSUser = true;
        }
        this.prepareConfig();
        if (this.config.isGenerateUploadTabNeeded) {
            this.loadGeneratedDocumentList(this.token);
            this._dataStoreService.currentStore.subscribe((store) => {
                if (store['caseSummary']) {
                    const actionSummary = store['caseSummary'];
                    const jsonData = actionSummary['intake_jsondata'];
                    this.generatedDocuments = jsonData['generatedDocuments'] ? jsonData['generatedDocuments'] : [];
                }
                this.loadGeneratedDocumentList(this.token);
            });
        }
        this.isViewOnly = this._dataStoreService.getData('isViewOnly');
        this.complaintDetail = this._complaintDetailsService.complaintDetail;
        // this.isDJSUser = this.complaintDetail && this.complaintDetail.teamtype_key && this.complaintDetail && this.complaintDetail.teamtype_key === 'djs' ? true : false;
        if (this.complaintDetail.provider_person_name) {
            this.providerNm = (this.complaintDetail.provider_person_name.trim()) ? this.complaintDetail.provider_person_name.trim() : this.complaintDetail.provider_name.trim();
        }
        console.log(this.complaintDetail);
        console.log(this.contactList);
        this.searchContactCritera = { 'provider_complaintid': this.complaintDetail.provider_complaintid };
        this.loadContactsList(20, 1, this.searchContactCritera);
        this.loadDeficiencyList();
        this.outcomes = this._complaintDetailsService.getOutcomes();
        this._complaintSummary.getSummary().subscribe(response => {
            console.log('summary', response);
            if (response && response.length) {
              response.forEach(comment => {
                if (comment) {
                  this.summary += comment;
                }
              });
            }
          });
    }

    loadContactsList(count: number, pageNumber: number, critera) {
        this._complaintContact.getContactList(count, pageNumber, critera).subscribe(response => {
          console.log(response);
          if (response && response.data && response.data.length) {
            this.contactList = response.data;
          }
        });
    }

    loadDeficiencyList() {
        const data = { provider_complaintid: this.complaintDetail.provider_complaintid };
        this._deficiencyList.getList(20, 1, data)
          .subscribe(response => {
            this.deficiencyList = response.data;
        });
    }

    checkFileType(file: string, accept: string): boolean {
        if (accept) {
            const acceptedFilesArray = accept.split(',');
            return acceptedFilesArray.some(type => {
                const validType = type.trim();
                if (validType.charAt(0) === '.') {
                    return file.toLowerCase().endsWith(validType.toLowerCase());
                }
                return false;
            });
        }
        return true;
    }
   editAttachment(modal) {
       this.editAttach.editForm(modal);
       (<any>$('#edit-attachment')).modal('show');
    }
    confirmDelete(modal) {
        this.documentPropertiesId = modal.documentpropertiesid;
        this.documentId = modal.filename;
        (<any>$('#delete-attachment')).modal('show');
    }
    deleteAttachment() {
        var workEnv = config.workEnvironment
        if (workEnv=== 'state'){
                this._service.endpointUrl =
                ProviderUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
                const id = this.documentPropertiesId + '&' + this.documentId;
                this._service.remove(id).subscribe(
                result => {
                    (<any>$('#delete-attachment')).modal('hide');
                    this.attachment();
                    this._alertService.success('Attachment Deleted successfully!');
                },
                err => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
            } else {
                this._service.endpointUrl =
                ProviderUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
                this._service.remove(this.documentPropertiesId).subscribe(
                    result => {
                        (<any>$('#delete-attachment')).modal('hide');
                        this.attachment();
                        this._alertService.success('Attachment Deleted successfully!');
                    },
                    err => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            }
        
    }

    downloadFile(s3bucketpathname)
    {
        var workEnv = config.workEnvironment;
        if(workEnv == 'state') {
           this.downldSrcURL = this.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            //4200
          this.downldSrcURL = s3bucketpathname;
        }
        console.log("this.downloadSrc", this.downldSrcURL);
        window.open(this.downldSrcURL, '_blank');
    }

    private attachment() {
        this._dropDownService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get',
                    order:"originalfilename",
                }),
                ProviderUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/' + this.daNumber + '?data'
            )
            .filter(result => result !== null)
            .subscribe((result) => {
                result.map(item => {
                    item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                });
                this.filteredAttachmentGrid = result;
            });
    }
    private humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        if (!bytes) {
            return '';
         }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    private loadGeneratedDocumentList(user: AppUser) {
        this.allDocuments.forEach(document => {
            if (document.access.indexOf(user.role.name) !== -1) {
                const isExist = this.generatedDocuments.find(gDocument => document.id === gDocument.id);
                if (!isExist) {
                    this.generatedDocuments.push(document);
                }
            }

        });

    }

    downloadDocument(document) {
        if (document.documentpath) {
            window.open(document.documentpath, '_blank');
        } else {
            this._dataStoreService.setData('documentsToDownload', [document.key]);
        }
    }

    downloadSelectedDocuments() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            this._dataStoreService.setData('documentsToDownload', selectedDocuments.map(document => document.key));

        }

    }

    getSelectedDocuments() {
        return this.generatedDocuments.filter(document => document.isSelected);
    }

    isFileSelected() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            return selectedDocuments.length > 0;
        }

        return false;
    }

    generateNewDocument(document) {
        document.isInProgress = true;
        setTimeout(() => {
            document.isGenerated = true;
            document.generatedBy = this.token.user.userprofile.displayname;
            document.generatedDateTime = new Date();
            document.isInProgress = false;
        }, 1000);

    }

    generateSelectedDocuments() {
        this.generatedDocuments.forEach(document => {
            if (document.isSelected) {
                this.generateNewDocument(document);
            }

        });
    }

    prepareConfig() {
        const isDjs = this._authService.isDJS();
        if (isDjs) {
           this.config.isGenerateUploadTabNeeded = true;
        } else {
            this.config.isGenerateUploadTabNeeded = false;
        }
    }

    toggleTable(id) {
        (<any>$('#' + id)).collapse('toggle');
    }

    collectivePdfCreator(element: string) {
        this.downloadCasePdf(element);
    }

    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            // console.log(pages.item(i).getAttribute('data-page-name'));
            const pageName = pages.item(i).getAttribute('data-page-name');
            const isPageEnd = pages.item(i).getAttribute('data-page-end');
            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
            });
        }
        this.convertImageToPdf();
    }

    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        this.goBack();
        this.pdfFiles = [];
        this.downloadInProgress = false;
    }
    goBack(): void {
        (<any>$('#payment-slip-document')).modal('hide');
        this.router.navigate(['../'], { relativeTo: this.route });
    }

    generateComplaintForm() {
        if (this.complaintDetail && (this.complaintDetail.status === 'complaint_approved' || this.complaintDetail.status === 'complaint_sent')) {
            const modal = {
                count: -1,
                where: {
                    complaint_number: this.complaintDetail ? this.complaintDetail.complaint_number : null,
                    provider_id: null
                },
                method: 'post'
            };
            this._dropDownService.download('tb_provider_complaint/complaintreport', modal).subscribe(res => {
                const blob = new Blob([new Uint8Array(res)]);
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                const timestamp = moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
                link.download = 'Complaint-Form-' + this.complaintDetail.complaint_number + '.' + timestamp + '.pdf';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                });
        } else {
            this._alertService.error('Complaint has to be approved to generate the complaint form!');
            return false;
        }
    }
}
