import { HttpHeaders } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Rx';
import { Location } from '@angular/common';


import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, GenericService } from '../../../../../@core/services';
import { AppConfig } from '../../../../../app.config';
import { AttachmentUpload } from '../../../_entities/provider.data.model';
import { ProviderUrlConfig } from '../../../provider-url.config';
import { Attachment } from '../_entities/attachment.data.models';
//import { AttachmentDetailComponent } from '../attachment-detail/attachment-detail.component';
import { config } from '../../../../../../environments/config';
import { AttachmentService } from '../attachment.service';


declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attachment-upload',
    templateUrl: './attachment-upload.component.html',
    styleUrls: ['./attachment-upload.component.scss']
})
export class AttachmentUploadComponent implements OnInit {
    curDate: Date;
    fileToSave = [];
    uploadedFile = [];
    tabActive = false;
    daNumber: string;
    id: string;
    attachmentResponse: AttachmentUpload;
    attachmentClassificationTypeDropDown$: Observable<DropdownModel[]>;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    token: AppUser;
    constructor(
        private router: Router,
        private location: Location,
        private _service: GenericService<Attachment>,
        private _dropDownService: CommonHttpService,
        private route: ActivatedRoute,
        private _uploadService: NgxfUploaderService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _attachmentService: AttachmentService
    ) {
        this.id = this._attachmentService.getAttachmentConfig().uuid;
        this.daNumber = this._attachmentService.getAttachmentConfig().uniqueNumber;
        this.token = this._authService.getCurrentUser();
    }

    ngOnInit() {
        this.loadDropdown();
        this.curDate = new Date();
        (<any>$('#upload-attachment')).modal('show');
    }

    uploadFile(file: File | FileError): void {
        if (!(file instanceof Array)) {
            return;
        }
        file.map((item, index) => {
            const fileExt = item.name
                .toLowerCase()
                .split('.')
                .pop();
            if (
                fileExt === 'mp3' ||
                fileExt === 'ogg' ||
                fileExt === 'wav' ||
                fileExt === 'acc' ||
                fileExt === 'flac' ||
                fileExt === 'aiff' ||
                fileExt === 'mp4' ||
                fileExt === 'mov' ||
                fileExt === 'avi' ||
                fileExt === '3gp' ||
                fileExt === 'wmv' ||
                fileExt === 'mpeg-4' ||
                fileExt === 'pdf' ||
                fileExt === 'txt' ||
                fileExt === 'docx' ||
                fileExt === 'doc' ||
                fileExt === 'xls' ||
                fileExt === 'xlsx' ||
                fileExt === 'jpeg' ||
                fileExt === 'jpg' ||
                fileExt === 'png' ||
                fileExt === 'ppt' ||
                fileExt === 'pptx' ||
                fileExt === 'gif'
            ) {
                this.uploadedFile.push(item);
                this.uploadAttachment(index);
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error(fileExt + " format can't be uploaded");
                return;
            }
        });
    }

    humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    uploadAttachment(index) {
        var workEnv = config.workEnvironment;
        let uploadUrl = '';
        if(workEnv == 'state') {
            uploadUrl =  AppConfig.baseUrl +'/attachment/v1' + '/' + ProviderUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber;
            console.log("state",uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + ProviderUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber;
            console.log("local",uploadUrl);
        }

        this._uploadService
            .upload({
                url: uploadUrl,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile[index],
                process: true
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        this.attachmentResponse = response.data;
                        this.fileToSave.push(response.data);
                        this.fileToSave[this.fileToSave.length - 1].documentattachment = {
                            attachmenttypekey: '',
                            attachmentclassificationtypekey: '',
                            attachmentdate: new Date(),
                            sourceauthor: '',
                            attachmentsubject: '',
                            sourceposition: '',
                            attachmentpurpose: '',
                            sourcephonenumber: '',
                            acquisitionmethod: '',
                            sourceaddress: '',
                            locationoforiginal: '',
                            insertedby: this.token.user.userprofile.displayname,
                            note: '',
                            updatedby: this.token.user.userprofile.displayname,
                            activeflag: 1
                        };
                        this.fileToSave[this.fileToSave.length - 1].description = '';
                        this.fileToSave[this.fileToSave.length - 1].documentdate = new Date();
                        this.fileToSave[this.fileToSave.length - 1].title = '';
                        //this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
                        this.fileToSave[this.fileToSave.length - 1].objecttypekey = 'ServiceRequest';
                        this.fileToSave[this.fileToSave.length - 1].rootobjecttypekey = 'ServiceRequest';
                        this.fileToSave[this.fileToSave.length - 1].activeflag = 1;
                        this.fileToSave[this.fileToSave.length - 1].intakenumber = this.daNumber;
                        this.fileToSave[this.fileToSave.length - 1].insertedby = this.token.user.userprofile.displayname;
                        this.fileToSave[this.fileToSave.length - 1].updatedby = this.token.user.userprofile.displayname;
                    }
                },
                (err) => {
                    console.log(err);
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    this.uploadedFile.splice(index, 1);
                }
            );
    }
    deleteUpload(index) {
        this.uploadedFile.splice(index, 1);
        this.fileToSave.splice(index, 1);
    }
    clearAllUpload() {
        this.uploadedFile = [];
        this.fileToSave = [];
        //const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment';
        // this.router.navigateByUrl(currentUrl).then(() => {
        //     this.router.navigated = false;
        //     this.router.navigate([currentUrl]);
        // });
        this.location.back();
    }
    titleUpdate(event, index) {
        this.uploadedFile[index].title = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidTitle = false;
        } else {
            this.uploadedFile[index].invalidTitle = true;
        }
    }
    descUpdate(event, index) {
        this.uploadedFile[index].description = event.target.value;
    }
    docDateUpdate(event, index) {
        this.uploadedFile[index].docDate = event.target.value;
    }
    typeUpdate(event, index) {
        this.uploadedFile[index].attachmenttypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentType = false;
        } else {
            this.uploadedFile[index].invalidAttachmentType = true;
        }
    }
    categoryUpdate(event, index) {
        this.uploadedFile[index].attachmentclassificationtypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentClassify = false;
        } else {
            this.uploadedFile[index].invalidAttachmentClassify = true;
        }
    }
    saveAttachmentDetails() {
        if (this.uploadedFile.length !== this.fileToSave.length) {
            this._alertService.error('Please wait till files get uploaded');
        } else {
            this.uploadedFile.map((item, index) => {
                this.fileToSave[index].servicerequestid = this.id;
                this.fileToSave[index].title = item.title;
                this.fileToSave[index].description = item.description;
                this.fileToSave[index].documentattachment.attachmenttypekey = item.attachmenttypekey;
                this.fileToSave[index].documentattachment.attachmentclassificationtypekey = item.attachmentclassificationtypekey;
            });
            const AttachValidate = this.fileToSave.filter((wer) => !wer.documentattachment.attachmentclassificationtypekey || !wer.documentattachment.attachmenttypekey || !wer.title);
            if (AttachValidate.length === 0) {
                this._service.endpointUrl = ProviderUrlConfig.EndPoint.DSDSAction.Attachment.SaveAttachmentUrl;
                this._service.createArrayList(this.fileToSave).subscribe(
                    (response) => {
                        response.map((item, index) => {
                            if (item.documentpropertiesid) {
                                response.splice(index, 1);
                                this.fileToSave.splice(index, 1);
                                this.uploadedFile.splice(index, 1);
                            }
                            const docProp = response.filter((docId) => docId.Documentattachment);
                            if (docProp.length === 0) {
                                this._alertService.success('Attachment(s) added successfully!');
                                (<any>$('#upload-attachment')).modal('hide');
                                this.fileToSave = [];
                                this.uploadedFile = [];
                                this.router.routeReuseStrategy.shouldReuseRoute = function() {
                                    return false;
                                };
                                this.location.back();
                                //const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment';
                                // this.router.navigateByUrl(currentUrl).then(() => {
                                    // this.router.navigated = false;
                                    //this.router.navigate([currentUrl]);
                                // });
                            }
                            if (item.Documentattachment) {
                                const attPos = index + 1;
                                this._alertService.error(item.Documentattachment + ' for Attachment ' + attPos);
                            } else if (!item.documentpropertiesid) {
                                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                            }
                        });
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error('Please fill all mandatory fields');
                this.uploadedFile.map((item) => {
                    if (!item.title) {
                        item.invalidTitle = true;
                    } else {
                        item.invalidTitle = false;
                    }
                    if (!item.attachmentclassificationtypekey) {
                        item.invalidAttachmentClassify = true;
                    } else {
                        item.invalidAttachmentClassify = false;
                    }
                    if (!item.attachmenttypekey) {
                        item.invalidAttachmentType = true;
                    } else {
                        item.invalidAttachmentType = false;
                    }
                });
            }
        }
    }
    private loadDropdown() {
        const source = forkJoin(
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                ProviderUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentTypeUrl + '?filter={"nolimit": true}'
            ),
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                ProviderUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
            )
        )
            .map((result) => {
                return {
                    attachmentType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmenttypekey
                            })
                    ),
                    attachmentClassificationType: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmentclassificationtypekey
                            })
                    )
                };
            })
            .share();
        this.attachmentTypeDropdown$ = source.pluck('attachmentType');
        this.attachmentClassificationTypeDropDown$ = source.pluck('attachmentClassificationType');
    }
}
