import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProvidersComponent } from './providers.component';

const routes: Routes = [
  {
    path: '',
    component: ProvidersComponent
  },
  {
    path: 'complaints',
    loadChildren: './complaint/complaint.module#ComplaintModule'
  },
  {
    path: 'training',
    loadChildren: './training/training.module#TrainingModule'
  },
  {
    path: 'incident-report',
    loadChildren: '../../provider-portal/provider-incident-report/provider-incident-report.module#ProviderIncidentReportModule'
  },
  {
    path: 'monitoring',
    loadChildren : './monitoring/monitoring.module#MonitoringModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProvidersRoutingModule { }
