import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { PaginationRequest } from '../../../@core/entities/common.entities';

@Injectable()
export class ProviderSearchService {

  public selectedProviderOnSearch$ = new Subject<any>();
  constructor(private _commonHttpService: CommonHttpService) {

  }

  getProvidersList(categoryCode: string, providerId: string|number, providerName: string, page: number, pageSize: number, provider_status_cd: string) {
    let categoryCodes;
    if (categoryCode) {
      if (categoryCode === '3049') {
        categoryCodes = '3049';
        categoryCodes += ', 1782';
        categoryCodes += ', 3274';
        categoryCodes += ', 3302';
      } else {
        categoryCodes = [categoryCode];
      }
    }

    return this._commonHttpService.getArrayList({
        method: 'post',
        nolimit: true,
          providercategorycd: categoryCodes,
          providerid: providerId ? providerId : null,
          providername: providerName ? providerName : null,
          filter: {},
          pagenumber: page,
          pagesize: pageSize,
          providerstatuscd: provider_status_cd ? provider_status_cd : null,
          jurisdiction: null
      },
      'providerreferral/providersearch'
    );
  }

  broardCastSeletedProvider(provider) {
    this.selectedProviderOnSearch$.next(provider);
  }


}
