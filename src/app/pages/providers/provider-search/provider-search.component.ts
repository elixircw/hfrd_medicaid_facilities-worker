import { Component, OnInit } from '@angular/core';
import { ProviderSearchService } from './provider-search.service';
import { AlertService, CommonHttpService } from '../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-search',
  templateUrl: './provider-search.component.html',
  styleUrls: ['./provider-search.component.scss']
})
export class ProviderSearchComponent implements OnInit {


  providerTypes = [];
  providerType: string;
  providerID: string;
  providerName: string;
  providersList: any;
  selectedProvider: any;
  pageInfo: PaginationInfo = new PaginationInfo();
  provider_status_cd: string;
  totalCount: number;
  providerProgramList: any[] = [];
  constructor(private _service: ProviderSearchService,
    private _alertService: AlertService,
    private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute,
    private router: Router) { }


  ngOnInit() {
    this.provider_status_cd = '';
    this.pageInfo.pageNumber = 1;
    this.pageInfo.pageSize = 10;
    (<any>$('#search-provider-details')).modal('show');
    this.loadDropdowns();
  }

  loadDropdowns() {
    this.providerTypes = [
      { 'text': 'Public', 'value': '1783' },
      { 'text': 'Private ', 'value': '3049' },
      { 'text': 'Vendor', 'value': '3304' },
    ];
  }

  searchProvider() {
    this._service.getProvidersList(this.providerType, this.providerID, this.providerName, this.pageInfo.pageNumber, this.pageInfo.pageSize, this.provider_status_cd).subscribe(response => {
      console.log(response);
      if (Array.isArray(response) && response.length) {
        this.providersList = response;
        this.totalCount = response[0].totalcount;
      }
    });

  }
  pageChanged(pageInfo: any) {
    this.pageInfo.pageNumber = pageInfo.page;
    this.pageInfo.pageSize = pageInfo.itemsPerPage;
    this.searchProvider();
  }

  toggleTable(id, index, provider) {
    this.providerProgramList = [];
    this.selectedProvider = provider;
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle');
    (<any>$('.provider-search tr')).removeClass('selected-bg');
    (<any>$(`#provider-search-${index}`)).addClass('selected-bg');
    this.getProviderProgram(provider.provider_id);
  }

  getProviderProgram(providerId) {
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        nolimit: true,
        method: 'get',
        where: { securityusersid: null, provider_id: +providerId}
      }),
      'tb_provider/providerapplicantsearch?filter'
    ).subscribe( response => {
      if (response && response.data) {
        this.providerProgramList = response.data;
      }
    });
  }

  selectProivder(provider: any) {
    provider.provider_category_cd = this.selectedProvider.provider_category_cd;
    provider.provider_nm = this.selectedProvider.provider_nm;
    provider.tax_id_no = this.selectedProvider.tax_id_no;
    this.selectedProvider = provider;
  }

  onConfirmProvider() {
    if (this.selectedProvider) {
      this._service.broardCastSeletedProvider(this.selectedProvider);
      (<any>$('#search-provider-details')).modal('hide');
      this.router.navigate(['../'], { relativeTo: this.route });
    } else {
      this._alertService.warn('Please select any provider');
    }

  }

}
