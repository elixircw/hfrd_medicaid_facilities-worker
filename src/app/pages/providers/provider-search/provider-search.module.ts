import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderSearchRoutingModule } from './provider-search-routing.module';
import { ProviderSearchComponent } from './provider-search.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { ProviderSearchService } from './provider-search.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { PaginationModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ProviderSearchRoutingModule,
    FormMaterialModule,
    PaginationModule,
    NgxPaginationModule
  ],
  declarations: [ProviderSearchComponent],
  exports: [ProviderSearchComponent],
  providers: [ProviderSearchService]
})
export class ProviderSearchModule { }
