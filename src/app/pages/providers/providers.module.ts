import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProvidersRoutingModule } from './providers-routing.module';
import { ProvidersComponent } from './providers.component';
import { ProviderAuthorizationService } from './provider-authorization.service';
import { ProviderPortalService } from '../../provider-portal/provider-portal.service';
import { ProviderPortalResolverService } from '../../provider-portal/provider-portal-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    ProvidersRoutingModule
  ],
  declarations: [ProvidersComponent],
  providers: [ProviderAuthorizationService, ProviderPortalService, ProviderPortalResolverService]
})
export class ProvidersModule { }
