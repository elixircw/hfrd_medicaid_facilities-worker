import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrainingRoutingModule } from './training-routing.module';
import { TrainingComponent } from './training.component';
import { TrainingService } from './training.service';

@NgModule({
  imports: [
    CommonModule,
    TrainingRoutingModule
  ],
  declarations: [TrainingComponent],
  providers: [TrainingService]
})

export class TrainingModule { }