import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrainingComponent } from './training.component';

const routes: Routes = [
  {
    path: '',
    component: TrainingComponent
  }, 
  {
    path: 'dashboard',
    loadChildren: './training-dashboard/training-dashboard.module#TrainingDashboardModule'
  },
  {
    path: 'details/:trainingNumber',
    loadChildren: './training-details/training-details.module#TrainingDetailsModule'
  }
  ];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingRoutingModule { }
