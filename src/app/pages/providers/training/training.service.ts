import { Router } from "@angular/router";
import { CommonHttpService } from "../../../@core/services";
import { Injectable } from "@angular/core";
import { ProvidersUrlConfig } from "../providers-url.config";


@Injectable()
export class TrainingService {

    constructor(private _router: Router, 
        private _commonHttpService: CommonHttpService) {

    }
  
    getNewTrainingNumber() {
        return this._commonHttpService.getArrayList({}, 
            ProvidersUrlConfig.EndPoint.Training.GetNextTrainingNumberUrl)
    }
    
    openTraining(trainingNumber) {
        const url = '/pages/provider/training/details/' + trainingNumber + '/attendance';
        console.log("GOING to", url);
        this._router.navigate([url]);
    }
}
