import { Component, Input, OnInit, OnChanges } from '@angular/core';
//import { ApplicantUrlConfig } from '../../provider-portal-temp-url.config'
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, CommonHttpService, DataStoreService } from '../../../../@core/services';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { TrainingService } from '../training.service';
import * as moment from 'moment';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';

export interface instructorsListTable {
	instructorFirstName: string;
	instructorLastName: string;
}

@Component({
  selector: 'training-dashboard',
  templateUrl: './training-dashboard.component.html',
  styleUrls: ['./training-dashboard.component.scss']
})
export class TrainingDashboardComponent implements OnInit {

  today = new Date();
  trainingForm: FormGroup;
  instructorForm: FormGroup;
  trainingNumber: string;

  editOrientationTrainingId: string;
  deleteOrientationTrainingId: string;
  editInstructorId: string;
  deleteInstructorId: string;
  isEditInstructor: boolean = false;

  isEditTrainingSession: boolean;
  trainingSessionList: any = [];
  orientationMediums = [];
  trainingTypes = [];
  instructorsList = [];
  sessionType=[];
  countyList$: Observable<DropdownModel[]>;
  selectedJurisdiction: any;
  sessionNo = [];

  selection = new SelectionModel<instructorsListTable>(true, []);
  dataSource: any;
  displayedColumns: string[] = ['select', 'instructorFirstName', 'instructorLastName'];


  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private trainingService: TrainingService,
    private _dataService: DataStoreService) {

  }

  ngOnInit() {
    this.initializeForms();
    this.getSavedTrainingSessions();
    this.getSavedInstructors();
    this.loadDropdown();
    this.isEditTrainingSession = false;
    this.isEditInstructor = false;
    this.orientationMediums = ['In-person', 'Online'];
    this.trainingTypes = ['Information Meeting', 'Pre-Service Training', 'In-Service Training'];
    this.setupChangeSubscribers();
    this.sessionType=['PRIDE','Treatment resource home','Other'];
    this.sessionNo = ['1','2','3','4','5'];
  }

  resetTrainingForm() {
    this.trainingForm.reset({}, { emitEvent: false });
  }

  validationAlert(message: string) {
    this._alertService.error(message);
  }

  validateTimeFields(): boolean {
    if (!this.trainingForm.controls['training_date'].value) {
      this.validationAlert('Please enter training date')
      return false;
    }
    else if (!this.trainingForm.controls['start_datetime'].value) {
      this.validationAlert('Please enter start time')
      return false;
    }
    else if (!this.trainingForm.controls['end_datetime'].value &&
      this.trainingForm.controls['end_datetime'].dirty) {
      this.validationAlert('Please enter end time')
      return false;
    }
    return true;
  }

  setupChangeSubscribers() {
    this.trainingForm.controls['end_datetime'].valueChanges
      .subscribe(form => {
        if (this.validateTimeFields()) {
          this.computeDuration();
        }
      });
    this.trainingForm.controls['start_datetime'].valueChanges
      .subscribe( form => {
      if (this.validateTimeFields()) {
        this.computeDuration();
      }
    });
    this.trainingForm.controls['jurisdiction'].valueChanges
    .subscribe( form => {
      this.selectedJurisdiction = this.trainingForm.controls['jurisdiction'].value
      this.trainingForm.patchValue({
        'addr_county' : this.selectedJurisdiction
      });
  });
  }

  computeDuration() {
    let startTime = moment(moment(this.trainingForm.getRawValue().training_date).format('MM/DD/YYYY') + ' ' + this.trainingForm.getRawValue().start_datetime);
    let endTime = moment(moment(this.trainingForm.getRawValue().training_date).format('MM/DD/YYYY') + ' ' + this.trainingForm.getRawValue().end_datetime);

    if (startTime.isAfter(endTime)) {
      this.validationAlert('Start time must be before end time!');
    } else {
      let currentTrainingHours = moment.duration(endTime.diff(startTime)).asHours();
      if (currentTrainingHours) {
        this.trainingForm.patchValue({
          'duration': Math.round(currentTrainingHours * 100) / 100
        });
      }
    }
  }

  private initializeForms() {
    this.isEditTrainingSession = false;
    this.trainingForm = this.formBuilder.group({
      orientation_training_id: [null],
      training_number: [''],
      training_type: ['', [Validators.required]],
      training_date: [null, [Validators.required]],
      start_datetime: [null, [Validators.required]],
      end_datetime: [null, [Validators.required]],
      duration: [null],
      medium: [''],
      instructor_1: [null],
      instructor_2: [null],
      addr_line1: [''],
      addr_line2: [''],
      addr_city: [''],
      addr_state: [''],
      addr_county: [''],
      jurisdiction:['', [Validators.required]],
      training_topic:[''],
      room_no:[''],
      training_status:[''],
      addr_zip: [''],
      training_url: [''],
      narrative: '',
      session_type:[''],
      session_no:['1']
    });

    this.isEditInstructor = false;
    this.instructorForm = this.formBuilder.group({
      instructor_id: [null],
      instructor_first_nm: ['', [Validators.required]],
      instructor_middle_nm: [''],
      instructor_last_nm: ['', [Validators.required]],
    });
  }

  getTrainingNumber() {
    this.isEditTrainingSession = false;
    this.trainingService.getNewTrainingNumber()
      .subscribe(
        (result) => {
          this.trainingNumber = result['nextNumber'];
          this.trainingForm.patchValue({
            'training_number': this.trainingNumber,
            'training_status': 'Scheduled'
          });
          (<any>$('#add-training')).modal('show');
        },
        (error) => {
          this._alertService.warn('Unable to create training session!');
        }
      )
  }

  trainingFormPayload() {
    const trainingDetails = this.trainingForm.getRawValue();
    const startTime = moment(this.trainingForm.getRawValue().training_date).format('MM/DD/YYYY') + ' ' + this.trainingForm.getRawValue().start_datetime;
    const endTime = moment(this.trainingForm.getRawValue().training_date).format('MM/DD/YYYY') + ' ' + this.trainingForm.getRawValue().end_datetime;
    trainingDetails.start_datetime = moment(startTime).format();
    trainingDetails.end_datetime = moment(endTime).format();
    return trainingDetails;
  }

  saveTraining() {
    let payload = this.trainingFormPayload();
    this._commonHttpService.create(
      payload,
      'providerorientationtraining/'
    ).subscribe(
      (response) => {
        this._alertService.success("Training session created successfully!");
        this.getSavedTrainingSessions();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-training')).modal('hide');
    this.resetTrainingForm();
  }

  patchProviderTraining() {
    let payload = this.trainingFormPayload();
    this._commonHttpService.patch(
      this.editOrientationTrainingId,
      payload,
      'providerorientationtraining'
    ).subscribe(
      (response) => {
        this._alertService.success("Training session updated successfully!");
        this.getSavedTrainingSessions();
      },
      (error) => {
        this._alertService.error("Unable to update training session");
      }
    );
    (<any>$('#add-training')).modal('hide');
    this.resetTrainingForm();
  }

  getSavedTrainingSessions() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        order: 'insertedon desc'
      },
      'providerorientationtraining?filter'
    ).subscribe(response => {
      this.trainingSessionList = response
    });
  }

  saveInstructor() {
    this._commonHttpService.create(
      this.instructorForm.value,
      'providertraininginstructor/'
    ).subscribe(
      (response) => {
        this._alertService.success("Information saved successfully!");
        this.getSavedInstructors();
      },
      (error) => {
        this._alertService.error("Unable to save information");
      }
    );
    (<any>$('#add-instructor')).modal('hide');
    this.instructorForm.reset();
  }

  getSavedInstructors() {
    this._commonHttpService.getAll(
      'providertraininginstructor/'
    ).subscribe(response => {
      this.instructorsList = response
      this.dataSource = new MatTableDataSource<instructorsListTable>(this.instructorsList);
    });
  }

  editTrainingSession(session) {
    this.editOrientationTrainingId = session.orientation_training_id;
    this.isEditTrainingSession = true;
    (<any>$('#add-training')).modal('show');

    this.trainingForm.patchValue(session, { emitEvent: false });
    this.trainingForm.patchValue({
      start_datetime: moment(session.start_datetime).format('HH:mm:ss'),
      end_datetime: moment(session.end_datetime).format('HH:mm:ss'),
    },
      { emitEvent: false }
    );
  }

  deleteTrainingSession(training) {
    this.deleteOrientationTrainingId = training.orientation_training_id;
    (<any>$('#delete-trainingsession')).modal('show');
  }

  cancelDelete() {
    this.deleteOrientationTrainingId = null;
    (<any>$('#delete-trainingsession')).modal('hide');
  }

  deleteTrainingConfirm() {
    this._commonHttpService.remove(
      this.deleteOrientationTrainingId,
      {
        where: { orientation_training_id: this.deleteOrientationTrainingId }
      },
      'providerorientationtraining'
    ).subscribe(
      (response) => {
        this._alertService.success("Training session deleted successfully!");
        (<any>$('#delete-trainingsession')).modal('hide');
        this.getSavedTrainingSessions();
      },
      (error) => {
        this._alertService.error('Unable to delete training session');
      }
    );
  }

  loadDropdown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        'admin/county' + '?filter'
      )
    ]).map(res => {
      return {
        countyList: res[0].map(
          item =>
            new DropdownModel({
              text: item.countyname,
              value: item.countyid
            })
        )
      };
    });
    this.countyList$ = source.pluck('countyList');
  }

  openExistingTraining(selectedTrainingNumber) {
    this.trainingService.openTraining(selectedTrainingNumber);
  }
  
  /**
   * Instructor stuff
   */
  patchInstructor() {
    var payload = {};
    this.instructorForm.value.instructor_id = this.editInstructorId;
    payload = this.instructorForm.value;
    this._commonHttpService.patch(
      this.editInstructorId,
      payload,
      'providertraininginstructor'
    ).subscribe(
      (response) => {
        this._alertService.success("Instructor updated successfully!");
        this.getSavedInstructors();
      },
      (error) => {
        this._alertService.error("Unable to update Instructor information");
      }
    );
    (<any>$('#add-instructor')).modal('hide');
    this.instructorForm.reset();
  }

  editInstructor(instructor) {
    this.editInstructorId = instructor.instructor_id;
    (<any>$('#add-instructor')).modal('show');
    this.isEditInstructor = true;
    this.instructorForm.patchValue({
      instructor_id: instructor.instructor_id,
      instructor_first_nm: instructor.instructor_first_nm,
      instructor_middle_nm: instructor.instructor_middle_nm,
      instructor_last_nm: instructor.instructor_last_nm
    });

  }
  
  deleteInstructor(instructor) {
    this.deleteInstructorId = instructor.instructor_id;
    (<any>$('#delete-instructor')).modal('show');
  }

  deleteInstructorConfirm() {
    this._commonHttpService.remove(
      this.deleteInstructorId,
      {
        where : {instructor_id: this.deleteInstructorId}
      },
      'providertraininginstructor'
      ).subscribe(
        (response) => {
          this._alertService.success("Instructor deleted successfully!");
          this.getSavedInstructors();
          (<any>$('#delete-instructor')).modal('hide');
        },
        (error) => {
          this._alertService.error('Unable to delete Instructor');
        }
      );
  }

  cancelDeleteInstructor() {
    this.deleteInstructorId = null;
    (<any>$('#delete-instructor')).modal('hide');
  }

  /**
   * Mat table stuff
   */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}
	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
  }

}