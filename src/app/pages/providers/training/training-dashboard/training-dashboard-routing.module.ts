import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrainingDashboardComponent } from './training-dashboard.component';

const routes: Routes = [
  {
    path : '',
    component: TrainingDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingDashboardRoutingModule { }
