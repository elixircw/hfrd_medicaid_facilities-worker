import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrainingDashboardRoutingModule } from './training-dashboard-routing.module';
import { TrainingDashboardComponent } from './training-dashboard.component';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatCardModule,
  MatListModule,
  MatAutocompleteModule,
  MatTableModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatSlideToggleModule
} from '@angular/material';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule({
  imports: [
    CommonModule,
    TrainingDashboardRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatAutocompleteModule,
    MatTableModule,
    MatExpansionModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    SharedPipesModule,
    ControlMessagesModule,
    NgSelectModule,
    NgxPaginationModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
  ],
  declarations: [TrainingDashboardComponent]
})
export class TrainingDashboardModule { }
