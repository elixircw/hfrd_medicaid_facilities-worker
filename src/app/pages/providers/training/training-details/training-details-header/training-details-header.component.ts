import { Component, OnInit } from '@angular/core';
import { TrainingDetailsService } from '../training-details.service';

@Component({
  selector: 'training-details-header',
  templateUrl: './training-details-header.component.html',
  styleUrls: ['./training-details-header.component.scss']
})
export class TrainingDetailsHeaderComponent implements OnInit {

  training_number: string;
  trainingDetails: any;

  constructor(private _service: TrainingDetailsService) { 
    this.training_number = this._service.trainingNumber;
    this.trainingDetails = this._service.trainingDetails;
  }

  ngOnInit() {
  }

}
