import { Component, OnInit } from '@angular/core';
import { ProviderAuthorizationService } from '../../../provider-authorization.service';
import { TrainingDetailsService } from '../training-details.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navigation-tab',
  templateUrl: './navigation-tab.component.html',
  styleUrls: ['./navigation-tab.component.scss']
})
export class NavigationTabComponent implements OnInit {

  tabs = [];
  constructor(private _authorization: ProviderAuthorizationService) {
    // const status = this.complaintSerivcie.getCurrentStatus();
    this.tabs = this._authorization.getTrainingDetailsTabs();
  }

  ngOnInit() {
  }

}
