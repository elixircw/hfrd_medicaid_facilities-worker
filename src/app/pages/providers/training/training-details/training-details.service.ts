import { Injectable } from '@angular/core';
import { AuthService, CommonHttpService } from '../../../../@core/services';
import { FormBuilder } from '@angular/forms';
import { ProvidersUrlConfig } from '../../providers-url.config';

@Injectable()
export class TrainingDetailsService {
  
  trainingNumber: string;
  trainingDetails: any;
  // TODO: SIMAR - Try using the training.data.model

  constructor(private authService: AuthService,
      private _commonHttpService: CommonHttpService,
      private formBuilder: FormBuilder) { }
  
  getTrainingDetails(training_number: string) {
    let result = this._commonHttpService.getArrayList({
      method: 'get',
      where: { training_number: training_number }
    }, ProvidersUrlConfig.EndPoint.Training.GET_TRAINING_DETAILS);

    console.log("SIMAR TRAINING DETAILS", result);
    return result;
  }

}
