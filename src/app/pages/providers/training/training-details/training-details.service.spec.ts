/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TrainingDetailsService } from './training-details.service';

describe('Service: TrainingDetails', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrainingDetailsService]
    });
  });

  it('should ...', inject([TrainingDetailsService], (service: TrainingDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
