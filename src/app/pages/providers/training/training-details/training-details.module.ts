import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingDetailsComponent } from './training-details.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { TrainingDetailsRoutingModule } from './training-details-routing.module';
import { TrainingDetailsService } from './training-details.service';
import { TrainingDetailsResolverService } from './training-details-resolver.service';
import { TrainingDetailsHeaderComponent } from './training-details-header/training-details-header.component';
import { NavigationTabComponent } from './navigation-tab/navigation-tab.component';

@NgModule({
  imports: [
    CommonModule,
    TrainingDetailsRoutingModule,
    FormMaterialModule
  ],
  declarations: [TrainingDetailsComponent, TrainingDetailsHeaderComponent, NavigationTabComponent],
  providers: [TrainingDetailsService, TrainingDetailsResolverService]
})
export class TrainingDetailsModule { }
