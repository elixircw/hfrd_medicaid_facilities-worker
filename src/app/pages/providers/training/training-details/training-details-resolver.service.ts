import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TrainingDetailsService } from './training-details.service';
import { Observable } from 'rxjs';

@Injectable()
export class TrainingDetailsResolverService implements Resolve<any> {

  constructor(private _service: TrainingDetailsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const trainingNumber = route.paramMap.get('trainingNumber');
    this._service.trainingNumber = trainingNumber;
    return this._service.getTrainingDetails(trainingNumber);
  }
}
