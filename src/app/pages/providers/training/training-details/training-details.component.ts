import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TrainingDetailsService } from './training-details.service';
import { ProviderAuthorizationService } from '../../provider-authorization.service';

@Component({
  selector: 'training-details',
  templateUrl: './training-details.component.html',
  styleUrls: ['./training-details.component.scss']
})
export class TrainingDetailsComponent implements OnInit {
  
  // tabs = [];
  constructor(private route: ActivatedRoute,
    private _service: TrainingDetailsService,
    private _authorization: ProviderAuthorizationService
  ) {
    this.route.data.subscribe(data => {
      console.log('complaint resolve ', data);
      if (data && data.trainingDetails && data.trainingDetails.length) {
        console.log("GOT DATA THIS TIME", data.trainingDetails[0]);
        this._service.trainingDetails = data.trainingDetails[0];
        // this.tabs = this._authorization.getTrainingDetailsTabs();
      } else {
        console.log("WHAT TO DO HERE ?!")
        this._service.trainingDetails = {}
        // this._service.trainingDetails = this._service.getMetaData(this._service.complaintid);
      }
    });
  }

  ngOnInit() {
  }

}
