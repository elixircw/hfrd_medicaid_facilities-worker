import { Component, OnInit } from '@angular/core';
import { TrainingDetailsService } from '../training-details.service';
import { CommonHttpService, AlertService } from '../../../../../@core/services';

@Component({
  selector: 'training-attendance',
  templateUrl: './training-attendance.component.html',
  styleUrls: ['./training-attendance.component.scss']
})
export class TrainingAttendanceComponent implements OnInit {
  trainingDetails: any;
  training_number: string;
	assignedTrainingList=[];
	
	// Modes of Training, displays can change based on these
	isOrientationTraining: boolean;
	isPreServiceTraining: boolean;
	isServiceTraining: boolean;

  constructor(private _service: TrainingDetailsService,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService) { 
		this.trainingDetails = this._service.trainingDetails;
	}

  ngOnInit() {
    this.training_number=this._service.trainingDetails.training_number;
    this.getTrainingSessions();
	}
	
  patchIsAttended(session){
    var isIntentAttend: boolean;
		if (session.is_attended === false) {
			isIntentAttend = true;
		} else if (session.is_attended === true) {
			isIntentAttend = false;
		};
		var payload = {}
		payload['is_attended'] = isIntentAttend;
		console.log('payload', payload);
		this._commonHttpService.patch(
			session.orientation_training_attendance_id,
			payload,
			'providerorientationtrainingattendance'
		).subscribe(
			(response) => {
				this._alertService.success("Attendence Changed successfully!");
				this.getTrainingSessions();
			},
			(error) => {
				this._alertService.error("Unable to Take Attendance");
			});
  }
	
	getTrainingSessions(){
    this._commonHttpService.create(
			{
				where: { training_number: this.training_number },
				method: 'post'
			},
			'providerorientationtrainingattendance/getassignedorientations'

		).subscribe(response => {
			this.assignedTrainingList = response;
			console.log(JSON.stringify('assigned staff', this.assignedTrainingList));
		},
			(error) => {
				this._alertService.error('Unable to get assigned staffs, please try again.');
				console.log('get contact Error', error);
				return false;
			}
		);
  }
}
