import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingAttendanceComponent } from './training-attendance.component';
import { TrainingAttendanceRoutingModule } from './training-attendance-routing.module';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { MatButtonToggleModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    TrainingAttendanceRoutingModule,
    FormMaterialModule,
    MatButtonToggleModule,
  ],
  declarations: [TrainingAttendanceComponent]
})
export class TrainingAttendanceModule { }
