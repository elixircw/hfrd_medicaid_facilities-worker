import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TrainingAttendanceComponent } from './training-attendance.component';

const routes: Routes = [
  {
    path: '',
    component : TrainingAttendanceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingAttendanceRoutingModule {}
