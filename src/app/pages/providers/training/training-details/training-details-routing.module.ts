import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TrainingDetailsComponent } from './training-details.component';
import { TrainingDetailsResolverService } from './training-details-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: TrainingDetailsComponent,
    resolve: {
      trainingDetails: TrainingDetailsResolverService
    },
    children: [
      {
        path: 'attendance',
        loadChildren: './training-attendance/training-attendance.module#TrainingAttendanceModule'
      }
    ]
  }
];

// export const TrainingDetailsRoutes = RouterModule.forChild(routes);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingDetailsRoutingModule { }

