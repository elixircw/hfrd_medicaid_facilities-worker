import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringExitConfRoutingModule } from './monitoring-exit-conf-routing.module';
import { MonitoringExitConfComponent } from './monitoring-exit-conf.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
@NgModule({
  imports: [
    CommonModule,
    MonitoringExitConfRoutingModule,
    FormMaterialModule,
  ],
  declarations: [MonitoringExitConfComponent]
})
export class MonitoringExitConfModule { }
