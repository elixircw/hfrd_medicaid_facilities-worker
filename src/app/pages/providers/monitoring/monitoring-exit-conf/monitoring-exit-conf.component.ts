import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

import { SelectionModel } from '@angular/cdk/collections';
import { MonitoringService } from '../monitoring.service';

@Component({
  selector: 'monitoring-exit-conf',
  templateUrl: './monitoring-exit-conf.component.html',
  styleUrls: ['./monitoring-exit-conf.component.scss']
})
export class MonitoringExitConfComponent implements OnInit {
  exitConfForm: FormGroup;
  staffInformation: any;
  providerId: any;
  monitoring_id: any;
  dataSource: any;
  exitConfInfo: any;
  staffDetail: any[] = [];
  staffDetailTemp: any[] = [];
  workerDetail: any[] = [];
  workerDetailTemp: any[] = [];
  workersList: any[] = [];
  loadedViolations: any;
  compliants: any = [];
  // selection = new SelectionModel < staffInfoTable > (true, []);
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private monitorigService: MonitoringService,
    private _router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.initexitConfForm();
    this.providerId = this.monitorigService.monitorInfo ? this.monitorigService.monitorInfo.provider_id : null;
    this.monitoring_id = this.monitorigService.monitorInfo ? this.monitorigService.monitorInfo.monitoring_id : null;
    this.getStaffInformation();
    this.getExitConf();
    this.getWorkers();
    this.getComplaints();

  }

  initexitConfForm() {
    this.exitConfForm = this.formBuilder.group({
      exit_dt: [null],
      program_changes: [null],
      summary: [null],
      staffdetail: [],
      workerdetail: [null],
      exit_conf_id: [null],
      provider_complaintid: [null],
      violations: this.formBuilder.array([])
    });
  }


  openAssignStaff() {
    (<any>$('#assign-staff')).modal('show');
  }

  openAssignWorker() {
    // this.workerDetailTemp = this.workerDetail;
    (<any>$('#assign-worker')).modal('show');
  }

  getStaffInformation() {
    console.log('Provider Id in staff', this.providerId);
    this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      where: {
        object_id: this.providerId,
      },
    },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.staffInformation = response && response.length ? response.filter(staff => staff.first_nm && staff.first_nm !== '') : [];
        // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getExitConf() {
    this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      count: -1,
      where: {
        monitoring_id: this.monitoring_id,
      },
    },
      'prov_monitoring_exit_conf/listexitconf?filter'
    ).subscribe(
      (response) => {
        this.exitConfInfo = response;
        if (response && response.length) {
          this.exitConfForm.patchValue(response[response.length - 1]);
          this.staffDetail = response[response.length - 1].staffdetails ? response[response.length - 1].staffdetails : [];
          this.workerDetail = response[response.length - 1].workerdetails ? response[response.length - 1].workerdetails : [];
          this.getViolationList();
        } else {
          this.getViolationList();
        }
        // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getViolationList() {
    this.monitorigService.getViolationList().subscribe(response => {
      console.log(response);
      if (Array.isArray(response) && response.length && Array.isArray(response[0].getnoncompliance)) {
        const questions = response[0].getnoncompliance;
        this.processQuestionForm(questions);
      }
    });



  }

  processQuestionForm(questions) {
    const control = new FormArray([]);
    this.exitConfForm.removeControl('violations');
    questions.forEach(question => {
      control.push(this.createQuestion(question));
    });
    this.loadedViolations = questions;
    this.exitConfForm.addControl('violations', control);
  }

  createQuestion(quesiton) {
    return this.formBuilder.group({
      record_question_id: [quesiton.record_question_id],
      record_id: [quesiton.record_id],
      // monitoring_record_data_id: [quesiton.monitoring_record_data_id],
      // text_from_commar: null,
      // frequency_deficiency: [quesiton.frequency_deficiency],
      // impact_deficiency: [quesiton.impact_deficiency],
      // scope_deficiency: [quesiton.scope_deficiency],
      // findings: [quesiton.findings],
      violation_comments: [quesiton.violation_comments]

    });
  }



  addStaff(conf_data, staff_data) {
    const staffData = { 'staffs': [] };
    if (staff_data && staff_data.length) {
      staff_data.forEach(staff => {
        const staffObj = {
          'exit_conf_id': conf_data.exit_conf_id,
          'objectid': staff.provider_staff_id,
          'objecttype': 'staff',
          'exit_dt': conf_data.exit_dt,
        };
        staffData.staffs.push(staffObj);
      });
    }
    this._commonHttpService.create(staffData, 'prov_monitoring_exit_conf_staff/addupdate').subscribe(response => { });
  }

  addWorker(conf_data, staff_data) {
    const staffData = { 'staffs': [] };
    if (staff_data && staff_data.length) {
      staff_data.forEach(staff => {
        const staffObj = {
          'exit_conf_id': conf_data.exit_conf_id,
          'objectid': staff.userid,
          'objecttype': 'worker',
          'exit_dt': conf_data.exit_dt,
        };
        staffData.staffs.push(staffObj);
      });
    }
    this._commonHttpService.create(staffData, 'prov_monitoring_exit_conf_staff/addupdate').subscribe(response => { });
  }

  addExitConf() {
    const exitConfFormData = this.exitConfForm.getRawValue();
    const confData = {
      'monitoring_id': this.monitoring_id,
      'program_changes': exitConfFormData.program_changes,
      'summary': exitConfFormData.summary,
      'exit_dt': exitConfFormData.exit_dt,
      'exit_conf_id': exitConfFormData.exit_conf_id,
      'violations': exitConfFormData.violations,
      'provider_complaintid': exitConfFormData.provider_complaintid
    };

    this._commonHttpService.create(confData, 'prov_monitoring_exit_conf/addupdate').subscribe(response => {
      this._alertService.success('exit conference Saved Successfull');
      if (this.staffDetail && this.staffDetail.length) {
        this.addStaff(response, this.staffDetail);
      }
      if (this.workerDetail && this.workerDetail.length) {
        this.addWorker(response, this.workerDetail);
      }
    });
  }

  getComplaints() {
    this._commonHttpService.getArrayList({
      method: 'post',
      nolimit: true,
      count: -1,
      where: {
        'provider_id': this.providerId,
      },
    },
      // http://localhost:3000/api/tb_provider_complaint_deficiency/list?filter={'count':-1,'limit':99999,'page':1,'method':'get','where':{'provider_complaintid':null,'provider_id':null,'status':null,'cap_number':'C2020003331'}}
      'tb_provider_complaint/providercomplaints'
    ).subscribe(
      (response) => {
        if (response && response['data']) {
          this.compliants = response['data'];
        }
        console.log(response);
        // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }
  getWorkers() {
    this._commonHttpService.getArrayList({
      method: 'post',
      nolimit: true,
      count: -1,
      where: {
        appevent: 'PTA'
        // teamid: '3d152f0a-1bc8-4923-a685-ecaf350c8bae'
      },
    },
      'Intakedastagings/getroutingusers'
    ).subscribe(
      (response) => {
        if (response && response['data']) {
          this.workersList = response['data'];
        }
        console.log(response);
        // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  toggleWorker(checked, worker) {
    if (this.workerDetailTemp && this.workerDetailTemp.length) {

    } else {
      this.workerDetailTemp = [];
    }
    if (checked) {
      this.workerDetailTemp.push(worker);
    } else {
      const index = this.workerDetailTemp ? this.workerDetailTemp.findIndex(workerInfo => workerInfo.userid === worker.userid) : -1;
      if (index > -1) {
        this.workerDetailTemp.splice(index, 1);
      }
    }
  }

  toggleStaff(checked, staff) {
    if (this.staffDetailTemp && this.staffDetailTemp.length) {

    } else {
      this.staffDetailTemp = [];
    }
    if (checked) {
      this.staffDetailTemp.push(staff);
    } else {
      const index = this.staffDetailTemp ? this.staffDetailTemp.findIndex(staffInfo => staffInfo.provider_staff_id === staff.provider_staff_id) : -1;
      if (index > -1) {
        this.staffDetailTemp.splice(index, 1);
      }
    }
  }

  deleteWorker(index) {
    this.workerDetail.splice(index, 1);
  }

  deleteStaff(index) {
    this.staffDetail.splice(index, 1);
  }


  isWorkerSelected(worker) {
    const index = this.workerDetail && this.workerDetail.length ? this.workerDetail.findIndex(workerInfo => workerInfo.userid === worker.userid) : -1;
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }

  isStaffSelected(staff) {
    const index = this.staffDetail && this.staffDetail.length ? this.staffDetail.findIndex(staffInfo => staffInfo.provider_staff_id === staff.provider_staff_id) : -1;
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }

  assignWorker() {
    if (this.workerDetailTemp && this.workerDetailTemp.length) {
      this.workerDetailTemp.forEach(worker => {
        this.workerDetail.push(worker);
      });
    }
    this.workerDetailTemp = [];
    (<any>$('#assign-worker')).modal('hide');
  }

  assignStaff() {

    if (this.staffDetailTemp && this.staffDetailTemp.length) {
      this.staffDetailTemp.forEach(staff => {
        this.staffDetail.push(staff);
      });
    }
    this.staffDetailTemp = [];
    (<any>$('#assign-staff')).modal('hide');
  }

  SavevoilationComments(violation, index) {
    const formvalues = this.exitConfForm.getRawValue();
    const vlist = Array.isArray(formvalues.violations) ? formvalues.violations : [];
    const obj = vlist[index];
    this._commonHttpService.create(
      {
        'record_id': violation.record_id,
        'child_id': 321,
        'site_id': violation.site_id,
        'monitoring_id': this.monitoring_id,
        'record_question_id': violation.record_question_id,
        'comments': obj.violation_comments,
        'status': 'Done/Ok'
      }, 'prov_monitoring_violation/addupdate'
    ).subscribe(data => {
      this._alertService.success('Updated successfully');
    });
  }
}
