import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonitoringExitConfComponent } from './monitoring-exit-conf.component';

const routes: Routes = [
  {
    path: '',
    component: MonitoringExitConfComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringExitConfRoutingModule { }
