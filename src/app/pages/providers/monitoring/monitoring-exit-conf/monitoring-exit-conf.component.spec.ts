import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringExitConfComponent } from './monitoring-exit-conf.component';

describe('MonitoringExitConfComponent', () => {
  let component: MonitoringExitConfComponent;
  let fixture: ComponentFixture<MonitoringExitConfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringExitConfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringExitConfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
