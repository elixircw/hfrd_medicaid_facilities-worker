import { Component, OnInit } from '@angular/core';
import { ProviderConstants } from '../constants';
import { MonitoringService } from './monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService } from '../../../@core/services';


@Component({
  selector: 'monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss']
})
export class MonitoringComponent implements OnInit {

  tabs = ProviderConstants.MONITORING_TABS;
  monitorInfo: any;
  id: String;
  constructor(private route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private _service: MonitoringService, private _router: Router) {
    this.route.data.subscribe(data => {
      console.log('visit resolve ', data);
      if (Array.isArray(data.monitorDetail) && data.monitorDetail.length) {
        this._service.monitorInfo = data.monitorDetail[0].listmonitoring[0];
        console.log('service monitor data------>', this._service.monitorInfo);
        this.monitorInfo = this._service.monitorInfo;
        console.log('monitor_info---->', this.monitorInfo);
      }
    });
  }

  ngOnInit() {
    this._dataStoreService.setData('Monitoring', null);
  }

  navigatetomonitor(obj) {
    const currentUrl = '/pages/provider-management/new-provider/' + obj.provider_id + '/monitoring';    
    this._router.navigate([currentUrl]);
  }

  navigatetocomplaint(obj) {
    const url = '/pages/provider/complaints/detail/' + obj.complaint_number + '/refferal';
    this._router.navigate([url]);
    this._dataStoreService.setData('isViewOnly', false);
  }

}
