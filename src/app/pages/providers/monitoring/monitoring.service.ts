import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../@core/services';
import { ProvidersUrlConfig } from '../providers-url.config';
import { ProviderPortalUrlConfig } from '../../../provider-portal/provider-portal-url.config';

@Injectable()
export class MonitoringService {

  visitNumber: string;
  monitorInfo: any;
  constructor(private _commonHttpService: CommonHttpService) {
    //
  }


  getVisitInfo(id) {
    this.visitNumber = id;
    return this._commonHttpService.getArrayList({
      method: 'get',
      where: { monitoring_id: id }
    }, ProvidersUrlConfig.EndPoint.Monitoring.VISIT_DETAIL_URL);
  }

  getEntranceConf() {
    return this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      count: -1,
      where: {
        monitoring_id: this.monitorInfo ? this.monitorInfo.monitoring_id : null,
      },
    }, ProvidersUrlConfig.EndPoint.Monitoring.ENTRANCE_CONF_URL
    );
  }

  getSiteInfo(sitid) {
    return this._commonHttpService.getArrayList({
      method: 'get',
      where: { site_id: sitid }
    }, ProvidersUrlConfig.EndPoint.Monitoring.SITE_DETAIL_URL);
  }

  getSiteForMonitoring() {
    return this._commonHttpService.getArrayList({
      method: 'post',
      where: { monitoring_id: this.monitorInfo ? this.monitorInfo.monitoring_id : null }
    }, ProvidersUrlConfig.EndPoint.Monitoring.GET_LICENSE_FOR_MONITORING);
  }
  saveSiteForMonitoring(obj) {
    return this._commonHttpService.create(obj, ProvidersUrlConfig.EndPoint.Monitoring.ADD_MONITORING_SITE);
  }

  getSiteList() {
    return this._commonHttpService.getArrayList({
      method: 'post',
      where: {
        provider_id: this.monitorInfo ? this.monitorInfo.provider_id : null,
        monitoring_id: this.monitorInfo ? this.monitorInfo.monitoring_id : null,
        periodic: this.monitorInfo ? this.monitorInfo.periodic : null
      }
    }, ProvidersUrlConfig.EndPoint.Monitoring.REMAINING_LICENSE_FOR_MONITORING);
  }

  getYouthInterviewList() {
    return this._commonHttpService.getArrayList({
      method: 'post',
      where: { monitoring_id: this.visitNumber }
    }, ProvidersUrlConfig.EndPoint.Monitoring.YOUTH_INTERVIEW);
  }

  getQuestionList(data) {
    return this._commonHttpService.getArrayList({
      method: 'get',
      where: data,
    }, ProvidersUrlConfig.EndPoint.Monitoring.GET_RECORD_QUESTION);
  }

  getViolationList(monitoring_id: string = this.visitNumber) {
    return this._commonHttpService.getArrayList({
      method: 'get',
      where: { monitoring_id: monitoring_id },
    }, ProvidersUrlConfig.EndPoint.Monitoring.GET_VIOLATED_QUESTION);
  }

  getStaffInSiteList(id) {
    return this._commonHttpService.create({
      method: 'POST',
      where: { provider_applicant_id: id },
    }, ProvidersUrlConfig.EndPoint.Monitoring.GET_STAFF_IN_SITE);
  }

  getRecordReviewList(data) {
    return this._commonHttpService.getArrayList({
      method: 'get',
      where: data,
    }, ProvidersUrlConfig.EndPoint.Monitoring.GET_RECORD_DATA);
  }

  getRecordAnswer(data) {
    return this._commonHttpService.getArrayList({
      method: 'get',
      where: data,
    }, ProvidersUrlConfig.EndPoint.Monitoring.GET_RECORD_ANSWER);
  }

  getYouthInProviderList(request) {
    request.provider_id = this.monitorInfo.provider_id;
    return this._commonHttpService.create(
      {
        where: request
      },
      ProviderPortalUrlConfig.EndPoint.Uir.YOUTH_IN_PROVIDER
    );
  }

}
