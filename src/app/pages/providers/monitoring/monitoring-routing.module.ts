import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonitoringComponent } from './monitoring.component';
import { ProgramInfoComponent } from './program-info/program-info.component';
import { MonitoringResolverService } from './monitoring-resolver.service';
import { MonitoringDocumentsModule } from './monitoring-documents/monitoring-documents.module';

const routes: Routes = [
  {
    path: ':visitNumber',
    component: MonitoringComponent,
    resolve: {
      monitorDetail: MonitoringResolverService
    },
    children: [
      {
        path: '',
        redirectTo: 'program-info',
        pathMatch: 'full',
      },
      {
        path: 'program-info',
        component: ProgramInfoComponent
      },
      {
        path: 'documents',
        loadChildren: './monitoring-documents/monitoring-documents.module#MonitoringDocumentsModule'
      },
      {
        path: 'tools',
        loadChildren: './monitoring-tools/monitoring-tools.module#MonitoringToolsModule'
      },
      {
        path: 'interviews',
        loadChildren: './monitoring-interviews/monitoring-interviews.module#MonitoringInterviewsModule'
      },
      {
        path: 'entrance-conf',
        loadChildren: './monitoring-entrance-conf/monitoring-entrance-conf.module#MonitoringEntranceConfModule'
      },
      {
        path: 'exit-conf',
        loadChildren: './monitoring-exit-conf/monitoring-exit-conf.module#MonitoringExitConfModule'
      },
      {
        path: 'activities',
        loadChildren: './monitoring-activities/monitoring-activities.module#MonitoringActivitiesModule'
      },
      {
        path: 'review',
        loadChildren: './monitoring-review/monitoring-review.module#MonitoringReviewModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringRoutingModule { }
