import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { MonitoringDocumentsRoutingModule } from './monitoring-documents-routing.module';
import { MonitoringDocumentsComponent } from './monitoring-documents.component';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import { AttachmentUploadComponent } from './attachment-upload/attachment-upload.component';
import { NgxfUploaderService, NgxfUploaderModule } from 'ngxf-uploader';
@NgModule({
  imports: [
    CommonModule,
    MonitoringDocumentsRoutingModule,
    CommonModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot()
  ],
  declarations: [MonitoringDocumentsComponent,EditAttachmentComponent,AttachmentUploadComponent],
  providers: [NgxfUploaderService]
})
export class MonitoringDocumentsModule { }
