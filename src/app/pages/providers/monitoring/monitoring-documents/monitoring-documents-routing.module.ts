import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonitoringDocumentsComponent } from './monitoring-documents.component';

const routes: Routes = [
  {
    path: '',
    component: MonitoringDocumentsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringDocumentsRoutingModule { }
