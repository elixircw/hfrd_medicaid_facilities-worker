import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringDocumentsComponent } from './monitoring-documents.component';

describe('MonitoringDocumentsComponent', () => {
  let component: MonitoringDocumentsComponent;
  let fixture: ComponentFixture<MonitoringDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
