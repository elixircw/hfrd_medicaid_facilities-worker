import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, CommonHttpService, GenericService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { ProviderUrlConfig } from '../../provider-url.config';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import { AttachmentUploadComponent } from './attachment-upload/attachment-upload.component';
import { Attachment } from './_entities/attachmnt.model';
import { DataStoreService } from '../../../../@core/services/data-store.service';

// import * as ALL_DOCUMENTS from '../../provider-utils/attachment/_configurtions/documents.json'
import * as ALL_DOCUMENTS from '../../provider-utils/attachment/_entities/attachment.data.models';
import { GeneratedDocuments } from '../../provider-utils/attachment/_entities/attachment.data.models';
import { MonitoringService } from '../monitoring.service';

@Component({
    selector: 'monitoring-documents',
    templateUrl: './monitoring-documents.component.html',
    styleUrls: ['./monitoring-documents.component.scss']
})
export class MonitoringDocumentsComponent implements OnInit {

    documentPropertiesId: any;
    generateDocForm: FormGroup;
    @Input() addAttachementSubject$ = new Subject<Attachment[]>();
    @Input() downloadGenereatePDFSubject$ = new Subject<Attachment[]>();
    @Input() reviewstatus: string;
    // tslint:disable-next-line:no-input-rename
    filteredAttachmentGrid: Attachment[] = [];
    visitNumber: string;
    token: AppUser;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    daNumber: string;
    reviewStatus: string;
    // id: string;
    attachmentType: FormGroup;
    showScreens = { uploadDocument: false, uploadedDocument: false, createDocument: true };
    searchText = '';
    private allAttachmentGrid: Attachment[];
    @ViewChild(EditAttachmentComponent) editAttach: EditAttachmentComponent;
    generatedDocuments: GeneratedDocuments[] = [];
    allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;
    config = { isGenerateUploadTabNeeded: false };

    genDocList = [
        { name: 'RCC Quarterly report', type: 'RCC', isready: true },
        { name: 'Report Summary', type: 'CPA', isready: true, doctype: 'report_summary' },
        { name: 'Inspection Cover Letter', type: 'CPA', isready: true, doctype: 'cover_letter' },
        { name: 'CPA Quarterly report ', type: 'CPA', isready: true, doctype: 'periodic_report' },
        { name: 'YSB Eligibility Report ', type: 'YSB', isready: true, doctype: 'elegibility_report' },
        { name: 'YSB Eligibility Letter ', type: 'YSB', isready: true, doctype: 'elegibility_letter' }
    ];
    monitorInfo: any;
    programtype: any;

    constructor(
        private formBuilder: FormBuilder,
        private _dropDownService: CommonHttpService,
        private _service: GenericService<Attachment>,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _dataStoreService: DataStoreService,
        private _monitoringservice: MonitoringService,
        private _commonHttpService: CommonHttpService

    ) {
        // this.id = route.snapshot.params['id'];

        this.visitNumber = this._monitoringservice.visitNumber;
    }

    ngOnInit() {
        this.chooseTab(1);
        // this.loadDropdown();
        this.token = this._authService.getCurrentUser();
        this.attachmentType = this.formBuilder.group({
            selectedAttachment: ['All']
        });

        this.prepareConfig();
        this.attachment('All');
        this.initGenerateDocForm();
        this.monitorInfo = this._monitoringservice.monitorInfo;
        this.programtype = this.monitorInfo.prgram;
    }

    initGenerateDocForm() {
        this.generateDocForm = this.formBuilder.group({
            complaint: [null],
            victim: [null],
            petition: [null]
        });
    }

    isFileSelected() {
        const selectedDocuments = this.getSelectedDocuments();
        if (selectedDocuments) {
            return selectedDocuments.length > 0;
        }

        return false;
    }

    getSelectedDocuments() {
        return this.generatedDocuments.filter(document => document.isSelected);
    }

    downloadDocument(item) {
        const modal = {
            method: 'post',
            where:
            {
                'monitoring_id': this._monitoringservice.monitorInfo.monitoring_id,
                doctype: item.doctype
            }
        };
        this._commonHttpService.download('prov_monitoring/rccreport', modal).subscribe(res => {

            const blob = new Blob([new Uint8Array(res)]);
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = item.name + '.pdf';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        },
            (error) => {
                this._alertService.error('Unable to get decisions, please try again.');
                console.log('get decisions Error', error);
                return false;
            }
        );
    }

    chooseTab(tabId) {
        if (tabId === 1) {
            this.showScreens = { uploadDocument: true, uploadedDocument: false, createDocument: false };
        } else if (tabId === 2) {
            this.showScreens = { uploadDocument: false, uploadedDocument: true, createDocument: false };
        } else if (tabId === 3) {
            this.showScreens = { uploadDocument: false, uploadedDocument: false, createDocument: true };
        }
    }
    getIntakeNumber(visitNumber) {
        this.visitNumber = visitNumber;
        //   this.attachment('All');
    }
    filterAttachment(attachType) {
        if (attachType.selectedAttachment === 'All') {
            this.filteredAttachmentGrid = this.allAttachmentGrid;
        } else if (attachType.selectedAttachment === 'Exhibit') {
            this.filteredAttachmentGrid = this.allAttachmentGrid.filter(
                item => item.documenttypekey === attachType.selectedAttachment
            );
        } else {
            this.filteredAttachmentGrid = this.allAttachmentGrid.filter(
                item =>
                    item.documentattachment &&
                    item.documentattachment.attachmenttypekey ===
                    attachType.selectedAttachment
            );
        }
    }
    checkFileType(file: string, accept: string): boolean {
        if (accept) {
            const acceptedFilesArray = accept.split(',');
            return acceptedFilesArray.some(type => {
                const validType = type.trim();
                if (validType.charAt(0) === '.') {
                    return file.toLowerCase().endsWith(validType.toLowerCase());
                }
                return false;
            });
        }
        return true;
    }
    editAttachment(modal) {
        this.editAttach.editForm(modal);
        (<any>$('#edit-attachment')).modal('show');
    }
    confirmDelete(modal) {
        this.documentPropertiesId = modal.documentpropertiesid;
        (<any>$('#delete-attachment')).modal('show');
    }
    deleteAttachment() {
        this._service.endpointUrl =
            ProviderUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
        this._service.remove(this.documentPropertiesId).subscribe(
            result => {
                (<any>$('#delete-attachment')).modal('hide');
                this.attachment('All');
                this._alertService.success('Attachment Deleted successfully!');
            },
            err => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    attachment(attachType) {
        this._dropDownService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    order: 'originalfilename',
                    method: 'get'
                }),
                ProviderUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl +
                '/' +
                this.visitNumber +
                '?data'
            )
            .subscribe(result => {
                this.allAttachmentGrid = result;
                // this.filteredAttachmentGrid = this.allAttachmentGrid;
                result.map(item => {
                    item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                });
                this.filteredAttachmentGrid = result;
                this.addAttachementSubject$.next(result);
            });
    }

    private loadDropdown() {
        this.attachmentTypeDropdown$ = this._dropDownService
            .getArrayList(
                {},
                ProviderUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentTypeUrl
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.attachmenttypekey
                        })
                );
            });
    }
    private humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    prepareConfig() {
        const isDjs = this._authService.isDJS();
        if (isDjs) {
            this.config.isGenerateUploadTabNeeded = true;
        } else {
            this.config.isGenerateUploadTabNeeded = true;
        }
    }


}
