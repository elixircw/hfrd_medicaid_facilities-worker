import { Component, OnInit } from '@angular/core';
import { AlertService, CommonHttpService } from '../../../../../@core/services';
import { MonitoringService } from '../../monitoring.service';
import { ProviderPortalUrlConfig } from '../../../../../provider-portal/provider-portal-url.config';

@Component({
  selector: 'youth-interview',
  templateUrl: './youth-interview.component.html',
  styleUrls: ['./youth-interview.component.scss']
})
export class YouthInterviewComponent implements OnInit {
  assignedYouth: any[] = [];
  youthDetailTemp: any;
  siteList: any[] = [];
  selectedsite: any;

  constructor(private _commonHttpService: CommonHttpService, private _monitoringService: MonitoringService,
    private _alertService: AlertService) { }

  ngOnInit() {
    this.getAssignedYouthList();
    this.getSiteList();
  }

  addYouth() {

  }

  getAssignedYouthList() {
    const providerid = this._monitoringService.monitorInfo.provider_id;
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { object_id: providerid }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.GET_ASSIGED_YOUTH_LIST
    ).subscribe(
      (response) => {
        this.assignedYouth = response;
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  toggleStaff(checked, staff) {
    this.youthDetailTemp = (this.youthDetailTemp && this.youthDetailTemp.length) ? this.youthDetailTemp : [];
    if (checked) {
      staff.site_id = this.selectedsite;
      staff.monitoring_id = this._monitoringService.visitNumber;
      this.youthDetailTemp.push(staff);
    } else {
      this.youthDetailTemp = this.youthDetailTemp.filter(staffInfo => staffInfo.provider_staff_id === staff.provider_staff_id);
    }
  }

  assignStaff() {
    this._commonHttpService.create(this.youthDetailTemp, 'monitoring_youth_interview/addupdate').subscribe(data => {
      this._alertService.success('Assigned successfully');
      (<any>$('#assign-youth')).modal('hide');
    });
  }
  getSiteList() {
    this._monitoringService.getSiteList().subscribe(data => {
      this.siteList = data;
    });
  }

  getYouthInterviewList() {
    this._monitoringService.getSiteList().subscribe(data => {
      this.siteList = data;
    });
  }
}
