import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YouthInterviewComponent } from './youth-interview.component';

describe('YouthInterviewComponent', () => {
  let component: YouthInterviewComponent;
  let fixture: ComponentFixture<YouthInterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YouthInterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YouthInterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
