import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringInterviewsRoutingModule } from './monitoring-interviews-routing.module';
import { MonitoringInterviewsComponent } from './monitoring-interviews.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { YouthInterviewComponent } from './youth-interview/youth-interview.component';
import { StaffInterviewComponent } from './staff-interview/staff-interview.component';
import { BoardInterviewComponent } from './board-interview/board-interview.component';
import { FosterParentInterviewComponent } from './foster-parent-interview/foster-parent-interview.component';
@NgModule({
  imports: [
    CommonModule,
    MonitoringInterviewsRoutingModule,
    FormMaterialModule
  ],
  declarations: [MonitoringInterviewsComponent, YouthInterviewComponent, StaffInterviewComponent, BoardInterviewComponent, FosterParentInterviewComponent]
})
export class MonitoringInterviewsModule { }
