import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, CommonDropdownsService } from '../../../../@core/services';
import { MonitoringService } from '../monitoring.service';
import { ProviderPortalUrlConfig } from '../../../../provider-portal/provider-portal-url.config';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { IntakeUtils } from '../../../_utils/intake-utils.service';

@Component({
  selector: 'monitoring-interviews',
  templateUrl: './monitoring-interviews.component.html',
  styleUrls: ['./monitoring-interviews.component.scss']
})
export class MonitoringInterviewsComponent implements OnInit {
  childRecordForm: FormGroup;
  childSearchForm: FormGroup;
  siteList: any[];
  recordTitle: string;
  loadedQuestions = [];
  actionTitle: string;
  isAddEdit = false;
  recordType = 'Child';
  persons: any[];
  records: any[] = [];
  selectedItem: any;
  saveInProgress = false;
  youthInProvider: any = [];
  selectedPerson: any;

  constructor(private formBuilder: FormBuilder,
    private monitorigService: MonitoringService,
    private _commonHttpService: CommonHttpService,
    private _commonDDService: CommonDropdownsService,
    private _alertService: AlertService,
    private _intakeservice: IntakeUtils) { }

  ngOnInit() {
    this.initChildRecordForm();
    this.getSiteList();
    this.getPersonList();
    this.recordTitle = 'Youth';
    this.actionTitle = 'Add';
    this.getRecordList();
  }

  initChildRecordForm() {
    this.childRecordForm = this.formBuilder.group({
      monitoring_id: [null],
      record_id: [null],
      monitoring_record_site_id: [null],
      objectid: [null, Validators.required],
      objecttype: [null],
      applicant_id: [null, Validators.required],
      site_id: [null],
      answers: this.formBuilder.array([]),
      status: ['Draft'],
      comments: [null],
    });
    this.childSearchForm = this.formBuilder.group({
      firstname: [null],
      lastname: [null],
      site_id: [null],
      cjamspid: [null],
    });
  }



  createQuestion(question) {

    let date = null;
    if (question.monitoring_record_dt) {
      date = this._commonDDService.getValidDate(question.monitoring_record_dt);
    }
    const questionGroup = this.formBuilder.group({
      monitoring_record_value: (question.question_mandatory) ? [question.monitoring_record_value, Validators.required] : [question.monitoring_record_value],
      record_question_id: [question.record_question_id],
      monitoring_record_data_id: [question.monitoring_record_data_id],
      record_id: [question.record_id],
      comments: [null],
      answers: this.formBuilder.array([]),


    });

    if (question.is_group && Array.isArray(question.questions)) {
      this.processQuestionForm(questionGroup, question.questions, null);
    }

    return questionGroup;
  }

  addChildRecord() {
    this.recordTitle = 'Youth';
    this.isAddEdit = true;
    this.childRecordForm.enable();
  }
  addFosterParent() {
    this.recordTitle = 'Foster Parent';
    this.isAddEdit = true;
    this.childRecordForm.enable();
  }

  addStaff() {
    this.recordTitle = 'Staff';
    this.isAddEdit = true;
    this.childRecordForm.enable();
  }
  addBoard() {
    this.recordTitle = 'Board Member';
    this.isAddEdit = true;
    this.childRecordForm.enable();
  }


  onSiteChange() {
    const applicant_id = this.childRecordForm.getRawValue().applicant_id;
    const selectedSite = this.siteList.find(item => item.applicant_id === applicant_id);
    this.childRecordForm.patchValue({ site_id: selectedSite.site_id });
    // const agency = selectedSite.license_no.startsWith('DHS') ? 'DHS' : 'DJS';
    const agency = (selectedSite.agency === 'OLM') ? 'DHS' : 'DJS';
    let program = null;
    let programType = null;
    if (!['Board member', 'Staff'].includes(this.recordType)) {
      program = selectedSite.license_level;
      programType = selectedSite.license_level === 'CPA' ? selectedSite.license_type : null;
    } else if (this.recordType === 'Staff') {
      program = selectedSite.license_level;
    }
    if ((this.recordType === 'Staff' || this.recordType === 'Child') && program === 'OOS') {
      program = 'RCC';
    }
    const data = {
      'prgram': program,
      'module_name': 'Interview',
      'agency': agency,
      'records_type': this.recordType,
      'prgram_type': programType
    };
    this.resetData();
    this.getQuestionList(data);
    if (['Board Member', 'Staff'].includes(this.recordTitle) ) {
      this.getStaffinSiteList(selectedSite.applicant_id);
    }
  }

  saveRecord(status) {
    console.log(this.childRecordForm.getRawValue());
    this._intakeservice.findInvalidControls(this.childRecordForm);
    if (status === 'Draft') {
      this.addUpdateChildRecord(status);
    } else {
      if (this.childRecordForm.invalid) {
        this._alertService.error('Please fill required fields');
        return;
      } else {
        this.addUpdateChildRecord(status);
      }
    }
  }
  addUpdateChildRecord(status) {
    const data = this.childRecordForm.getRawValue();
    data.status = status;
    data.monitoring_id = this.monitorigService.monitorInfo.monitoring_id;
    data.record_id = data.answers[0].record_id;
    data.objecttype = this.recordType;
    this.saveInProgress = true;
    this._commonHttpService.create(data, 'prov_monitoring_record_site/addupdate').subscribe(response => {
      console.log(response);
      this.saveInProgress = false;
      if (status === 'Draft') {
        this._alertService.success('Record saved as draft');
      } else {
        this._alertService.success('Record completed sucessfully');
      }
      this.isAddEdit = false;
      this.getRecordList();
    });
    
  }

  getQuestionList(data) {
    this.monitorigService.getQuestionList(data).subscribe(response => {
      console.log(response);
      if (Array.isArray(response) && response.length && Array.isArray(response[0].getrecordquestion)) {
        const questions = response[0].getrecordquestion;
        /*  const questions = [{
           "question": "How long have you been in this program?",
           "question_data_type": null,
           "question_hint": null,
           "question_options": null,
           "question_type": "text",
           "record_question_id": "291946ac-d6cf-43c5-b5a3-b8d61051f344",
           "record_id": "49226096-740c-4b7d-b9ea-d25093a7bdc8",
           "question_mandatory": null,
           "question_addtional_fields": null,
           'is_group': false,
           'on_expand': null,
           'questions': null,
         },
         {
           "question": "I.Basic Needs:",
           "question_data_type": null,
           "question_hint": null,
           "question_options": null,
           "question_type": "label",
           "record_question_id": "eeb8ab59-856c-4a40-afce-ad08ff0421d4",
           "record_id": "6a56432b-f5ab-4393-aa64-9178b06e64fc",
           "question_mandatory": null,
           "question_addtional_fields": null,
           'is_group': false,
           'on_expand': null,
           'questions': null,
         },
         {
           "question": "Does the youth share bedroom ?",
           "question_data_type": null,
           "question_hint": null,
           "question_options": [
             "Yes",
             "No"
           ],
           'is_group': true,
           'on_expand': 'Yes',
           'questions': [{
             "question": "Does youth require any special accommodations?",
             "question_data_type": null,
             "question_hint": null,
             "question_options": [
               "Yes",
               "No"
             ],
             'is_group': false,
             "question_type": "radio",
             "record_question_id": "2cc6247c-bd10-4c02-9555-ef660553593b",
             "record_id": "6a56432b-f5ab-4393-aa64-9178b06e64fc",
             "question_mandatory": null,
             "question_addtional_fields": null
           }],
           "question_type": "radio",
           "record_question_id": "de0217f7-ad96-4bc8-8c0f-6f9064203a73",
           "record_id": "7aa34ae7-6caf-40e6-8c60-a1da556c45f7",
           "question_mandatory": null,
           "question_addtional_fields": null
         },
         {
           "question": "Have you been in this program?",
           "question_data_type": null,
           "question_hint": null,
           "question_options": null,
           "question_type": "text",
           "record_question_id": "291946ac-d6cf-43c5-b5a3-b8d61051f344",
           "record_id": "49226096-740c-4b7d-b9ea-d25093a7bdc8",
           "question_mandatory": null,
           "question_addtional_fields": null,
           'is_group': false,
           'on_expand': null,
           'questions': null,
         },
         ]; */
        this.processQuestionForm(this.childRecordForm, questions, null);
      }
    });
  }

  processQuestionForm(form: FormGroup, questions, id) {
    const control = new FormArray([]);
    form.removeControl('answers');
    questions.forEach(question => {
      control.push(this.createQuestion(question));
    });

    this.loadedQuestions = questions;
    form.addControl('answers', control);
    this.childRecordForm.patchValue({ monitoring_record_site_id: id });
  }


  getSiteList() {
    this.monitorigService.getSiteForMonitoring().subscribe(data => {
      this.siteList = data;
    });
  }


  loadChildRecord() {
    this.recordType = 'Child';
    this.getRecordList();
    this.reset();
  }

  loadFosterParent() {
    this.reset();
    this.recordType = 'Parent';
    this.getRecordList();
  }
  loadStaffRecord() {
    this.reset();
    this.recordType = 'Staff';
    this.getRecordList();
  }

  loadBoardRecord() {
    this.reset();
    this.recordType = 'Board member';
    this.getRecordList();
  }

  reset() {
    this.isAddEdit = false;
    this.resetData();
    // this.childRecordForm.removeControl('answers');
    this.childRecordForm.reset();
  }

  resetData() {
    this.loadedQuestions = [];
    this.records = [];
  }

  cancel() {
    this.reset();
  }

  loadRecords() {

  }

  getPersonList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { object_id: this.monitorigService.monitorInfo.provider_id }
      },
      'provider_uir_youth_detail/getyouthlistbyprovider?filter'
    ).subscribe(
      (response) => {
        this.persons = response;
        this.persons = [
          {
            'firstname': 'finance parent',
            'lastname': '',
            'cjamspid': 100003918
          },
          {
            'firstname': 'Robert Mcnamara',
            'lastname': '',
            'cjamspid': 100003509
          }
        ];
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getRecordList() {
    const data = {
      monitoring_id: this.monitorigService.monitorInfo.monitoring_id,
      records_type: this.recordType,
      module_name: 'Interview',
    };
    this.monitorigService.getRecordReviewList(data).subscribe(response => {
      console.log(response);
      if (Array.isArray(response) && response.length && Array.isArray(response[0].getrecordsite)) {
        const records = response[0].getrecordsite;
        this.records = records;
      }
    });
  }

  editRecord(item) {
    this.childRecordForm.reset();
    this.isAddEdit = true;
    this.actionTitle = 'Edit';
    this.childRecordForm.enable();
    this.selectedPerson = {
      objectname: item.objectname
    };
    this.childRecordForm.patchValue({ site_id: item.site_id, objectid: item.objectid, applicant_id: item.applicant_id });
    
    this.getRecordAnswer(item.monitoring_record_site_id);
  }

  viewRecord(item) {
    this.childRecordForm.reset();
    this.isAddEdit = true;
    this.actionTitle = 'View';
    this.childRecordForm.disable();
    this.selectedPerson = {
      objectname: item.objectname,
      cjamspid: item.cjamspid,
      gendertypekey: item.gendertypekey,
      dob: item.dob
    };
    this.childRecordForm.patchValue({ site_id: item.site_id, objectid: item.objectid, applicant_id: item.applicant_id });
    this.getRecordAnswer(item.monitoring_record_site_id);
  }

  printRecord(item) {
    const modal = {
      method: 'post',
      where:
      {
        'monitoring_record_site_id': item.monitoring_record_site_id,
        'type': 'interview'
      }
    };
    this._commonHttpService.download('prov_monitoring/recordintervievprint', modal).subscribe(res => {

      const blob = new Blob([new Uint8Array(res)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = 'record.pdf';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    },
      (error) => {
        this._alertService.error('Unable to get information, please try again.');
        return false;
      }
    );
  }


  deleteRecord(item) {
    this.selectedItem = item;
    (<any>$('#delete-popup')).modal('show');
  }

  confirmDelete() {

    this._commonHttpService.patch(this.selectedItem.monitoring_record_site_id, { delete_sw: 'Y' }, 'prov_monitoring_record_site').subscribe(response => {
      this.getRecordList();
      (<any>$('#delete-popup')).modal('hide');
      this._alertService.success('Record deleted sucessfully');
    });
  }

  getRecordAnswer(monitoring_record_site_id) {
    const data = { monitoring_record_site_id: monitoring_record_site_id };
    this.monitorigService.getRecordAnswer(data).subscribe(response => {
      if (Array.isArray(response) && response.length && Array.isArray(response[0].getrecordanswer)) {
        const questions = response[0].getrecordanswer;
        if (questions.length) {
          // const masterData = questions[0];
          // this.childRecordForm.patchValue(masterData);
          this.processQuestionForm(this.childRecordForm, questions,monitoring_record_site_id);
        }

      }
    });
  }

  selectChild() {
    (<any>$('#select-child')).modal('show');
    this.getAssignedYouthList();
  }

  getAssignedYouthList() {
    const obj = this.childSearchForm.getRawValue();
    this.monitorigService.getYouthInProviderList(obj).subscribe(
      (response) => {
        this.youthInProvider = response;
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  selectPerson(person) {
    this.selectedPerson = person;
    this.selectedPerson.objectname = this.selectedPerson.firstname + ' ' + this.selectedPerson.lastname;
    this.childRecordForm.patchValue({ objectid: person.personid});
  }
  cancelselectPerson() {
    this.selectedPerson = null;
    this.childRecordForm.patchValue({ objectid: null });
  }

  getStaffinSiteList(data) {
    this.monitorigService.getStaffInSiteList(data).subscribe(response => {
      this.persons = response.map(item => {
        item.firstname = item.first_nm;
        item.middlename = item.middle_nm;
        item.lastname = item.last_nm;
        item.objectid = item.provider_staff_id;
        return item;
      });
    });
  }

}


