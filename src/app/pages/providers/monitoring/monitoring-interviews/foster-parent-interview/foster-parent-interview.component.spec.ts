import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FosterParentInterviewComponent } from './foster-parent-interview.component';

describe('FosterParentInterviewComponent', () => {
  let component: FosterParentInterviewComponent;
  let fixture: ComponentFixture<FosterParentInterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FosterParentInterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FosterParentInterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
