import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffInterviewComponent } from './staff-interview.component';

describe('StaffInterviewComponent', () => {
  let component: StaffInterviewComponent;
  let fixture: ComponentFixture<StaffInterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffInterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffInterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
