import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { MonitoringService } from '../../monitoring.service';

@Component({
  selector: 'staff-interview',
  templateUrl: './staff-interview.component.html',
  styleUrls: ['./staff-interview.component.scss']
})
export class StaffInterviewComponent implements OnInit {
  staffInformation: any[];

  constructor(private _commonHttpService: CommonHttpService, private _monitoringService: MonitoringService,
    private _alertService: AlertService) { }

  ngOnInit() {
  }

  getStaffInformation() {
    const providerid = this._monitoringService.monitorInfo.provider_id;
    this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      where: {
        object_id: providerid,
      },
    },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.staffInformation = response && response.length ? response.filter(staff => staff.first_nm && staff.first_nm !== '') : [];
        // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

}
