import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardInterviewComponent } from './board-interview.component';

describe('BoardInterviewComponent', () => {
  let component: BoardInterviewComponent;
  let fixture: ComponentFixture<BoardInterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardInterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardInterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
