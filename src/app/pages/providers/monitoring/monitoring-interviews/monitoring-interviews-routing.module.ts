import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonitoringInterviewsComponent } from './monitoring-interviews.component';
import { YouthInterviewComponent } from './youth-interview/youth-interview.component';
import { StaffInterviewComponent } from './staff-interview/staff-interview.component';
import { BoardInterviewComponent } from './board-interview/board-interview.component';
import { FosterParentInterviewComponent } from './foster-parent-interview/foster-parent-interview.component';

const routes: Routes = [
  {
    path: '',
    component: MonitoringInterviewsComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringInterviewsRoutingModule { }
