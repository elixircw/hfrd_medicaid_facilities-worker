import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringInterviewsComponent } from './monitoring-interviews.component';

describe('MonitoringInterviewsComponent', () => {
  let component: MonitoringInterviewsComponent;
  let fixture: ComponentFixture<MonitoringInterviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringInterviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringInterviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
