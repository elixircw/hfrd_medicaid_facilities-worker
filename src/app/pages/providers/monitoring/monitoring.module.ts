import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import { MonitoringComponent } from './monitoring.component';
import { ProgramInfoComponent } from './program-info/program-info.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MonitoringService } from './monitoring.service';
import { MonitoringResolverService } from './monitoring-resolver.service';
import { MatSelectModule,MatInputModule, MatFormFieldModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    MonitoringRoutingModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [MonitoringComponent,
     ProgramInfoComponent,
 ],
  providers: [MonitoringResolverService, MonitoringService]
})
export class MonitoringModule { }
