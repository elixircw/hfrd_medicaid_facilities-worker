import { Component, OnInit } from '@angular/core';
import { MonitoringService } from '../monitoring.service';
import { AlertService } from '../../../../@core/services';

@Component({
  selector: 'program-info',
  templateUrl: './program-info.component.html',
  styleUrls: ['./program-info.component.scss']
})
export class ProgramInfoComponent implements OnInit {

  monitorInfo: any;
  programInfo: any;
  directorInfo: any;
  siteList: any[] = [];
  showInfo: boolean;
  selectedLicenses: any[] = [];
  monitoringsiteList: any[] = [];
  isinitial = false;
  constructor(private _service: MonitoringService, private _alertservice: AlertService) {
    this.monitorInfo = this._service.monitorInfo;
    this.isinitial = (this.monitorInfo.periodic === 'Initial') ? true : false;
  }

  ngOnInit() {
    this.getSiteList();
    this.getSiteForMonitoring();
  }

  getSiteInfo(id) {
    this._service.getSiteInfo(id).subscribe(data => {
      this.showInfo = true;
      if (Array.isArray(data) && data.length) {
        this.programInfo = data[0];
        if (this.programInfo && Array.isArray(this.programInfo.director_info)) {
          this.directorInfo = this.programInfo.director_info[0];
        }
      }
    });
  }

  getSiteList() {
    this._service.getSiteList().subscribe(data => {
      this.siteList = data;
    });
  }

  getSiteForMonitoring() {
    this._service.getSiteForMonitoring().subscribe(data => {
      this.monitoringsiteList = data;
    });
  }

  openLicenseList() {
    this.getSiteList();
    this.selectedLicenses = [];
    (<any>$('#site-list-selection')).modal('show');
  }

  toggleLicense(flag, item) {
    if (flag) {
      this.selectedLicenses.push(item);
    } else {
      this.selectedLicenses = this.selectedLicenses.filter(ele => item.license_no !== ele.license_no);
    }
  }
  saveSite() {
    const obj = this.selectedLicenses.map(item => {
      return { site_id: item.site_id, monitoring_id: this.monitorInfo.monitoring_id,applicant_id: item.applicant_id };
    });

    this._service.saveSiteForMonitoring(obj).subscribe(data => {
      this._alertservice.success('Saved Sucessfully');
      (<any>$('#site-list-selection')).modal('hide');
      this.getSiteForMonitoring();
    });
  }
}
