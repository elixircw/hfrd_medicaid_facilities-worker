import { TestBed, inject } from '@angular/core/testing';

import { MonitoringResolverService } from './monitoring-resolver.service';

describe('MonitoringResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MonitoringResolverService]
    });
  });

  it('should be created', inject([MonitoringResolverService], (service: MonitoringResolverService) => {
    expect(service).toBeTruthy();
  }));
});
