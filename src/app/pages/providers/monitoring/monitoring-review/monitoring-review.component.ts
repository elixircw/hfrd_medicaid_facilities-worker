import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { AuthService, AlertService, DataStoreService } from '../../../../@core/services';
import { HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { NgxfUploaderService } from 'ngxf-uploader';
import { MonitoringService } from '../monitoring.service';

@Component({
  selector: 'monitoring-review',
  templateUrl: './monitoring-review.component.html',
  styleUrls: ['./monitoring-review.component.scss']
})
export class MonitoringReviewComponent implements OnInit {
  reviewForm: FormGroup;
  relicenseForm: FormGroup;
  statusDropdownItems: any;
  roleId: any;
  toroleId: any;
  isFinalDecisoin: any;
  eventcode: any;
  selectedPerson: any;
  getUsersList: any;
  originalUserList: any;
  isSupervisor: any;
  disSubmit: any;
  decisions: any;
  signimage: any;
  visitNumber: any;
  isSignPresent: any;
  assindisbtn = true;
  program = '';
  isrelicense: boolean;
  siteList: any[];
  datechangetext = '';
  monitorInfo: any;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _uploadService: NgxfUploaderService,
    private _monitoringService: MonitoringService) {
    this.program = this._monitoringService.monitorInfo.prgram;
    const periodic = this._monitoringService.monitorInfo.periodic;
    this.isrelicense = (periodic === 'Re-Licensure') ? true : false;
    this.datechangetext = (this.program === 'YSB') ? 'Certificate' : 'License';
  }

  ngOnInit() {
    this.getSiteForMonitoring();
    this.initActivitiesForm();
    this.roleId = this._authService.getCurrentUser();
    this.visitNumber = this._monitoringService.visitNumber;
    this.monitorInfo = this._monitoringService.monitorInfo;
    this.loadSignature();
    console.log(this.roleId);
    this.statusDropdownItems = [
      { 'text': 'Reject', 'value': 'Rejected', program: ['RCC', 'CPA', 'MDH', 'OOS'] },
      { 'text': 'Approve', 'value': 'Submitted', program: ['RCC', 'CPA', 'MDH', 'OOS'] },
      { 'text': 'Return', 'value': 'Incomplete', program: ['RCC', 'CPA', 'MDH', 'OOS'] },
      { 'text': 'Eligible', 'value': 'Eligible', program: ['YSB'] },
      { 'text': 'Ineligible', 'value': 'Eligible', program: ['YSB'] },
    ];
    // this.roleId.user.userprofile.teamtypekey;
    if (this.roleId.role.name === 'Executive Director' || this.roleId.role.name === 'Provider_DJS_SecretaryDesignee') {
      this.isFinalDecisoin = true;
    } else {
      this.isFinalDecisoin = false;
    }
    this.eventcode = 'PTA';
    this.getDecisions();
  }

  initActivitiesForm() {
    this.reviewForm = this.formBuilder.group({
      status: [null],
      comment: [null],
    });
    this.relicenseForm = this.formBuilder.group({
      requested_license_effective_date: [null],
      requested_license_end_date: [null],
      applicant_id: [null]
    });
  }

  getDecisions() {
    this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          eventcode: 'MVR',
          objectid: this.visitNumber
        }
      },
      'providerapplicant/getapplicationdecisions'
    ).subscribe(decisionsarray => {

      this.decisions = decisionsarray.data;
    },
      (error) => {
        this._alertService.error('Unable to get decisions, please try again.');
        console.log('get decisions Error', error);
        return false;
      }
    );
  }


  loadSignature() {
    this._commonHttpService.getSingle({ method: 'get', securityusersid: this.roleId.user.securityusersid }, 'admin/userprofile/listusersignature?filter')
      .subscribe(res => {
        this.signimage = res.usersignatureurl;
        this.checkIfSignPresent();
      });
  }

  private checkIfSignPresent() {
    this.isSignPresent = Boolean(this.signimage);
    this._dataStoreService.setData('isSignPresent', this.isSignPresent);
  }


  listUser(assigned: string) {
    const ele = document.getElementsByName('selectedPerson');
    for (let i = 0; i < ele.length; i++) {
      const element = ele[i] as HTMLInputElement;
      element.checked = false;
    }
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter(res => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
    }
  }

  submitDecision() {
    this.disSubmit = true;
    console.log('Submitting decision');
    const decision = this.reviewForm.getRawValue();
    const reLicense= this.relicenseForm.getRawValue();


    if (this.reviewForm.invalid) {
      this._alertService.error('Please fill required feilds');
      this.disSubmit = false;
      return;
    }
    const fromRole = this.roleId.user.userprofile.teammemberassignment.teammember.roletypekey;
    let toRole = ((this.toroleId ? this.toroleId.agencykey : '') + (this.toroleId ? this.toroleId.rolecode : ''));
    if (this.isFinalDecisoin && decision.status === 'Submitted') {
      decision.status = 'Approved';
      toRole = fromRole;
    }
    this._commonHttpService.create(
      {
        where: {
          eventcode: 'MVR',
          objectid: this.visitNumber,
          tosecurityusersid: (this.toroleId ? this.toroleId.userid : ''),
          fromroleid: fromRole,
          toroleid: toRole,
          remarks: decision.comment, // .get('reason'),
          status: decision.status, // .get('status')
          isfinalapproval: this.isFinalDecisoin,
          license_expiry_dt: reLicense.requested_license_end_date

        },
        method: 'post'
      },
      'providerportalrequest/providerroutingmonitoring'
    ).subscribe(result => {
      (<any>$('#assgn-user')).modal('hide');
      this.disSubmit = false;
      this._alertService.success('Decision Submitted successfully!');
      this.decisions.length = 0;
      const data = this.relicenseForm.getRawValue();
      if ( decision.status === 'Approved') {
        const site = (Array.isArray(this.siteList) && this.siteList.length) ? this.siteList[0] : null;
        if (site) {
          data.applicant_id = site.applicant_id;
          // this.saveLicenseInformation(data).subscribe(response => {
            this._alertService.success('License Information Saved Successfully!');
          // }, (error) => {
          //   this._alertService.error('Unable to update license information, please try again.');
          //   console.log('update program information Error', error);
          //   return false;
          // }
          // );
        }
      }
      this.getDecisions();
    },
      (error) => {
        this.disSubmit = false;
        this._alertService.error('Unable to submit decision, please try again.');
        console.log('submit decision Error', error);
        return false;
      }
    );
  this.assindisbtn = false;
  }

  saveLicenseInformation(data) {
    return this._commonHttpService.update('',
      data, 'providerapplicant/updateapplicantlicenseinfo');
  }

  OptionsSelected() {
    this.assindisbtn = true;
  }

  selectPerson(person) {
    this.toroleId = person;
  }

  getRoutingUser() {
    // this.getServicereqid = modal.provider_referral_id;
    //  this.getServicereqid = "R201800200374";
    // console.log(modal.provider_referral_id);
    // this.zipCode = modal.incidentlocation;
    this.getUsersList = [];
    this.originalUserList = [];
    // this.isGroup = modal.isgroup;
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: this.eventcode },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  openAssgnForm() {
    (<any>$('#assgn-user')).modal('show');
    this.getRoutingUser();
  }

  uploadAttachment() {
    console.log(this.signimage);
    const blob = this.dataURItoBlob(this.signimage);
    const finalImage = new File([blob], 'image.png');
    this._uploadService
      .upload({
        url:
          AppConfig.baseUrl +
          '/' +
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment
            .UploadAttachmentUrl +
          '?access_token=' +
          this.roleId.id +
          '&srno=userprofile',
        headers: new HttpHeaders()
          .set('access_token', this.roleId.id)
          .set('srno', 'userprofile')
          .set('ctype', 'file'),
        filesKey: ['file'],
        files: finalImage,
        process: true
      })
      .subscribe(
        response => {
          if (response && response.data && response.data.s3bucketpathname) {
            this._commonHttpService.create({
              usersignatureurl: response.data.s3bucketpathname
            }, 'admin/userprofile/updateusersignature').subscribe(res => {
              if (res.count) {
                this._alertService.success('Signature added successfully.');
                this.loadSignature();
              }
            });
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  dataURItoBlob(dataURI) {
    const binary = atob(dataURI.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/png'
    });
  }

  getSiteForMonitoring() {
    this._monitoringService.getSiteForMonitoring().subscribe(data => {
      this.siteList = data;
      const site = (Array.isArray(this.siteList) && this.siteList.length) ? this.siteList[0] : null;
      if (site) {
        this.relicenseForm.patchValue({
          requested_license_effective_date: site.requested_license_effective_date,
          requested_license_end_date:   (this.monitorInfo && this.monitorInfo.license_expiry_dt ) ? this.monitorInfo.license_expiry_dt : site.requested_license_end_date,
        });
      }
    });
  }

}
