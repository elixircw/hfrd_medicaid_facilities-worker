import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringReviewComponent } from './monitoring-review.component';

describe('MonitoringReviewComponent', () => {
  let component: MonitoringReviewComponent;
  let fixture: ComponentFixture<MonitoringReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
