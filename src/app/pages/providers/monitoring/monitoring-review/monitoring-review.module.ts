import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringReviewRoutingModule } from './monitoring-review-routing.module';
import { MonitoringReviewComponent } from './monitoring-review.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ApplicantLicenseComponent } from '../../../provider-applicant/new-applicant/applicant-license/applicant-license.component';
@NgModule({
  imports: [
    CommonModule,
    MonitoringReviewRoutingModule,
    FormMaterialModule,
  ],
  declarations: [MonitoringReviewComponent, ApplicantLicenseComponent]
})
export class MonitoringReviewModule { }
