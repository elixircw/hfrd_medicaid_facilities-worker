import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonitoringReviewComponent } from './monitoring-review.component';

const routes: Routes = [
  {
    path: '',
    component: MonitoringReviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringReviewRoutingModule { }
