import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, CommonDropdownsService } from '../../../../@core/services';
import { MonitoringService } from '../monitoring.service';
import { ProviderPortalUrlConfig } from '../../../provider-portal-temp/provider-portal-temp-url.config';
import { IntakeUtils } from '../../../_utils/intake-utils.service';

@Component({
  selector: 'monitoring-tools',
  templateUrl: './monitoring-tools.component.html',
  styleUrls: ['./monitoring-tools.component.scss']
})
export class MonitoringToolsComponent implements OnInit {

  childRecordForm: FormGroup;
  childSearchForm: FormGroup;
  siteList: any[];
  recordTitle: string;
  loadedQuestions = [];
  actionTitle: string;
  isAddEdit = false;
  recordType = 'Child';
  persons: any[];
  records: any[] = [];
  selectedItem: any;
  saveInProgress = false;
  selectedPerson: any;
  youthInProvider: any[] = [];
  viewdetails: boolean;
  defciencyScopes: { text: string; value: string; }[];
  defciencyImpacts: { text: string; value: string; }[];
  defciencyFrequency: { text: string; value: string; }[];
  questionForm: FormGroup;
  insvectionvalid: boolean;
  isinitial: boolean;
  isysb: boolean;
  constructor(private formBuilder: FormBuilder,
    private monitorigService: MonitoringService,
    private _commonHttpService: CommonHttpService,
    private _commonDDService: CommonDropdownsService,
    private _alertService: AlertService,
    private _monitoringService: MonitoringService,
    private _intakeservice: IntakeUtils) {
    const monitorInfo = this._monitoringService.monitorInfo;
    this.isinitial = (monitorInfo.periodic === 'Initial') ? true : false;
    this.isysb = (monitorInfo.prgram === 'YSB') ? true : false;
  }

  ngOnInit() {
    this.initChildRecordForm();
    this.getSiteList();
    this.getPersonList();
    this.recordTitle = 'Child';
    this.actionTitle = 'Add';
    this.getRecordList();
    this.loadDD();

  }

  initChildRecordForm() {
    this.childRecordForm = this.formBuilder.group({
      monitoring_id: [null],
      record_id: [null],
      monitoring_record_site_id: [null],
      objectid: [null],
      objecttype: [null],
      site_id: [null],
      applicant_id: [null, Validators.required],
      answers: this.formBuilder.array([]),
      status: ['Draft'],
      comments: [null],
    });
    this.childSearchForm = this.formBuilder.group({
      firstname: [null],
      lastname: [null],
      site_id: [null],
      cjamspid: [null],
    });

    this.questionForm = this.formBuilder.group({
      question: [null, Validators.required],
      question_hint: [null]
    });
  }

  get questionArray() {
    return this.childRecordForm.get('answers') as FormArray;
  }

  addQuestion(question) {
    this.questionArray.push(this.createQuestion(question));
    this.loadedQuestions.push(question);
  }

  createQuestion(quesiton) {

    let date = null;
    if (quesiton.monitoring_record_dt) {
      date = this._commonDDService.getValidDate(quesiton.monitoring_record_dt);
    } else {
     // if (this.recordType === 'Physical Plant') {
        date = new Date();
      // }
    }
    if (!quesiton.monitoring_record_value && quesiton.question_type === 'radio') {
      quesiton.monitoring_record_value = 'Pending';
    }

    if (quesiton.question_type === 'date' && quesiton.monitoring_record_value) {
      quesiton.monitoring_record_value = this._commonDDService.getValidDate(quesiton.monitoring_record_dt);
    }
    const isDateRequired = quesiton.question_type === 'radio' ? Validators.required : Validators.nullValidator;
    return this.formBuilder.group({
      monitoring_record_value: [quesiton.monitoring_record_value],
      monitoring_record_dt: [date, isDateRequired],
      record_question_id: [quesiton.record_question_id],
      monitoring_record_data_id: [quesiton.monitoring_record_data_id],
      answers: this.formBuilder.array([]),
      record_id: [quesiton.record_id],
      text_from_commar: null,
      frequency_deficiency: [quesiton.frequency_deficiency],
      impact_deficiency: [quesiton.impact_deficiency],
      scope_deficiency: [quesiton.scope_deficiency],
      findings: [quesiton.findings],
      question: [quesiton.question],
      question_data_type: null,
      question_hint: [quesiton.question_hint],
      question_options: [quesiton.question_options],
      question_type: [quesiton.question_type],

    });
  }

  addChildRecord() {
    this.reset();
    this.recordTitle = 'Child';
    this.isAddEdit = true;
    this.childRecordForm.enable();
    this.insvectionvalid = true;
  }
  addFosterParent() {
    this.reset();
    this.recordTitle = 'Foster Parent';
    this.isAddEdit = true;
    this.childRecordForm.enable();
    this.insvectionvalid = true;
  }

  addStaff() {
    this.reset();
    this.recordTitle = 'Staff';
    this.isAddEdit = true;
    this.childRecordForm.enable();
    this.insvectionvalid = true;
  }

  addSite() {
    this.reset();
    this.recordTitle = 'Site';
    this.isAddEdit = true;
    this.childRecordForm.enable();
    this.insvectionvalid = true;
  }

  addOffice() {
    this.recordTitle = 'Office';
    this.isAddEdit = true;
    this.childRecordForm.enable();
    this.insvectionvalid = true;
  }
  addAppartment(item) {
    this.recordTitle = item; // 'Appartment';
    this.isAddEdit = true;
    this.childRecordForm.enable();
    this.insvectionvalid = true;
  }

  onSiteChange() {
    this.insvectionvalid = true;
    const applicant_id = this.childRecordForm.getRawValue().applicant_id;
    const selectedSite = this.siteList.find(item => item.applicant_id === applicant_id);
    this.childRecordForm.patchValue({ site_id: selectedSite.site_id });
    // const agency = selectedSite.license_no.startsWith('DHS') ? 'DHS' : 'DJS';
    const agency = (selectedSite.agency === 'OLM') ? 'DHS' : 'DJS';
    let programType = null;
    if (this.recordType === 'Office Inspection') {
      programType = null;
    } else if (this.recordType === 'Apartment') {
      if (selectedSite.license_type === 'Independent Living Program') {
        programType = selectedSite.license_type;
      } else {
        this.insvectionvalid = false;
        programType = null;
      }
    } else if (this.recordType !== 'Physical Plant') {
      programType = selectedSite.license_level === 'CPA' ? selectedSite.license_type : null;
    }
    let sprogram = selectedSite.license_level;
    if (this.recordType === 'Physical Plant' && sprogram === 'OOS') {
      sprogram = 'RCC';
    }
    const data = {
      'prgram': sprogram,
      'module_name': 'Record',
      'agency': agency,
      'records_type': this.recordType,
      'prgram_type': programType
    };
    this.resetData();
    this.getQuestionList(data);
    if (this.recordTitle === 'Staff') {
      this.getStaffinSiteList(selectedSite.applicant_id);
    }
  }

  saveRecord(status) {
    console.log(this.childRecordForm.getRawValue());
    this._intakeservice.findInvalidControls(this.childRecordForm);
    if (status === 'Draft') {
      this.addUpdateChildRecord(status);
    } else {
      if (this.childRecordForm.invalid) {
        this._alertService.error('Please fill required fields');
        return;
      } else {
        this.addUpdateChildRecord(status);
      }
    }
  }
  addUpdateChildRecord(status) {
    const data = this.childRecordForm.getRawValue();
    data.status = status;
    data.monitoring_id = this.monitorigService.monitorInfo.monitoring_id;
    data.record_id = data.answers[0].record_id;
    data.objecttype = this.recordType;

    if (data.objectid || (this.recordType === 'Physical Plant' || this.recordType === 'Office Inspection' || this.recordType === 'Apartment') ) {
      this.saveInProgress = true;
      this._commonHttpService.create(data, 'prov_monitoring_record_site/addupdate').subscribe(response => {
        console.log(response);
        this.saveInProgress = false;
        if (status === 'Draft') {
          this._alertService.success('Record saved as draft');
        } else {
          this._alertService.success('Record completed sucessfully');
        }
        this.isAddEdit = false;
        this.getRecordList();
      });
    } else {
      this._alertService.warn('Please select a person to save');
    }
  }

  getQuestionList(data) {
    this.monitorigService.getQuestionList(data).subscribe(response => {
      console.log(response);
      if (Array.isArray(response) && response.length && Array.isArray(response[0].getrecordquestion)) {
        const questions = response[0].getrecordquestion;
        this.processQuestionForm(questions, null);
      }
    });
  }

  getStaffinSiteList(data) {
    this.monitorigService.getStaffInSiteList(data).subscribe(response => {
      this.persons = response.map(item => {
        item.firstname = item.first_nm;
        item.middlename = item.middle_nm;
        item.lastname = item.last_nm;
        item.objectid = item.provider_staff_id;
        return item;
      });
    });
  }

  processQuestionForm(questions, id) {
    const control = new FormArray([]);
    this.childRecordForm.removeControl('answers');
    this.childRecordForm.patchValue({ monitoring_record_site_id: id });
    questions.forEach(question => {
      control.push(this.createQuestion(question));
    });
    this.loadedQuestions = questions;
    this.childRecordForm.addControl('answers', control);
    if (this.actionTitle === 'View') {
      this.childRecordForm.disable();
      this.childSearchForm.disable();
    } else {
      this.childRecordForm.enable();
      this.childSearchForm.enable();
    }
  }


  getSiteList() {
    this.monitorigService.getSiteForMonitoring().subscribe(data => {
      this.siteList = data;
    });
  }


  loadChildRecord() {
    this.recordType = 'Child';
    this.getRecordList();
    this.reset();
  }

  loadFosterParent() {
    this.reset();
    this.recordType = 'Parent';
    this.getRecordList();
  }
  loadStaffRecord() {
    this.reset();
    this.recordType = 'Staff';
    this.getRecordList();
  }

  loadSiteRecord() {
    this.reset();
    this.recordType = 'Physical Plant';
    this.getRecordList();
  }
  loadOfficeRecord() {
    this.reset();
    this.recordType = 'Office Inspection';
    this.getRecordList();
  }

  loadAppartmentRecord(item) {
    this.reset();
    this.recordType = item;
    this.getRecordList();
  }

  reset() {
    this.actionTitle = 'Add';
    this.isAddEdit = false;
    this.viewdetails = false;
    this.selectedPerson = null;
    this.resetData();
    this.childRecordForm.reset();
  }

  resetData() {
    this.loadedQuestions = [];
    this.records = [];

  }

  cancel() {
    this.reset();
    this.getRecordList();
  }

  loadRecords() {

  }

  getPersonList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { object_id: this.monitorigService.monitorInfo.provider_id }
      },
      'provider_uir_youth_detail/getyouthlistbyprovider?filter'
    ).subscribe(
      (response) => {
       // this.persons = response;
        this.persons = [ ];
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getRecordList() {
    const data = {
      monitoring_id: this.monitorigService.monitorInfo.monitoring_id,
      records_type: this.recordType,
      module_name: 'Record',
    };
    this.monitorigService.getRecordReviewList(data).subscribe(response => {
      if (Array.isArray(response) && response.length && Array.isArray(response[0].getrecordsite)) {
        const records = response[0].getrecordsite;
        this.records = records;
      }
    });
  }

  editRecord(item) {
    this.childRecordForm.reset();
    this.viewdetails = false;
    this.isAddEdit = true;
    this.actionTitle = 'Edit';
    this.childRecordForm.enable();
    this.selectedPerson = item;
    this.childRecordForm.patchValue({ site_id: item.site_id, objectid: item.objectid, applicant_id: item.applicant_id });
    this.getRecordAnswer(item.monitoring_record_site_id);
  }

  viewRecord(item) {
    this.childRecordForm.reset();
    this.viewdetails = true;
    this.isAddEdit = true;
    this.actionTitle = 'View';
    this.childRecordForm.disable();
    this.selectedPerson = item;
    this.childRecordForm.patchValue({ site_id: item.site_id, objectid: item.objectid, applicant_id: item.applicant_id });
    this.getRecordAnswer(item.monitoring_record_site_id);
  }

  deleteRecord(item) {
    this.selectedItem = item;
    (<any>$('#delete-popup')).modal('show');
  }

  printRecord(item) {
    const modal = {
      method: 'post',
      where:
      {
        'monitoring_record_site_id': item.monitoring_record_site_id,
        'type': 'record',
        agency: this._monitoringService.monitorInfo.agency,
        program: this._monitoringService.monitorInfo.prgram,
        recordtype: this.recordType
      }
    };
    this._commonHttpService.download('prov_monitoring/recordintervievprint', modal).subscribe(res => {

      const blob = new Blob([new Uint8Array(res)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = 'record.pdf';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    },
      (error) => {
        this._alertService.error('Unable to get information, please try again.');
        return false;
      }
    );
  }

  confirmDelete() {
    this._commonHttpService.patch(this.selectedItem.monitoring_record_site_id, { delete_sw: 'Y' }, 'prov_monitoring_record_site').subscribe(response => {
      this.getRecordList();
      this.reset();
      (<any>$('#delete-popup')).modal('hide');
      this._alertService.success('Record deleted sucessfully');
    });
  }

  getRecordAnswer(monitoring_record_site_id) {
    const data = { monitoring_record_site_id: monitoring_record_site_id };
    this.monitorigService.getRecordAnswer(data).subscribe(response => {
      if (Array.isArray(response) && response.length && Array.isArray(response[0].getrecordanswer)) {
        const questions = response[0].getrecordanswer;
        if (questions.length) {
          const masterData = questions[0];
          // this.childRecordForm.patchValue(masterData);
          this.processQuestionForm(questions, monitoring_record_site_id);
        }

      }
    });
  }

  selectPerson(person) {
    this.selectedPerson = person;
    this.selectedPerson.objectname = this.selectedPerson.firstname + ' ' + this.selectedPerson.lastname;
    this.childRecordForm.patchValue({ objectid: person.personid });
  }
  cancelselectPerson() {
    this.selectedPerson = null;
    this.childRecordForm.patchValue({ objectid: null });
  }

  selectChild() {
    (<any>$('#select-child')).modal('show');
    this.getAssignedYouthList();
  }

  getAssignedYouthList() {
    const obj = this.childSearchForm.getRawValue();
    this._monitoringService.getYouthInProviderList(obj).subscribe(
      (response) => {
        this.youthInProvider = response;
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  isNonCompliance(i) {
    const control = <FormGroup>this.getControls()[i];
    if (control) {
      const value = (control).getRawValue().monitoring_record_value;
      return (value === 'Non-Compliance') ? true : false;
    }
  }

  getControls() {
    return (<FormArray>this.childRecordForm.get('answers')).controls;
  }

  loadDD(): any {
    this.defciencyScopes = [{
      text: 'Isolated',
      value: 'Isolated'
    },
    {
      text: 'Narrow',
      value: 'Narrow'
    },
    {
      text: 'Moderate',
      value: 'Moderate'
    }, {
      text: 'Broad',
      value: 'Broad'
    }];

    this.defciencyImpacts = [{
      text: 'Minor',
      value: 'Minor'
    },
    {
      text: 'Major',
      value: 'Major'
    },
    {
      text: 'Serious',
      value: 'Serious'
    }];

    this.defciencyFrequency = [{
      text: 'Initial',
      value: 'Initial'
    },
    {
      text: 'Repeat',
      value: 'Repeat'
    },
    {
      text: 'Chronic',
      value: 'Chronic'
    }];
  }

  openNewRegualtion() {
    (<any>$('#new-regualtion')).modal('show');
    this.questionForm.reset();
  }

  addNewRegulation() {

    if (this.questionForm.invalid) {
      this._alertService.error(' Please fill required feilds');
      return;
    }

    const regualtion = this.questionForm.getRawValue();
    const data = this.childRecordForm.getRawValue();
    let record_id = null;
    if (data.answers && data.answers.length) {
      record_id = data.answers[0].record_id;
    }
    const quesiton = {
      question: regualtion.question,
      question_data_type: null,
      question_hint: regualtion.question_hint,
      question_options: ['Compliance', 'Non-Compliance', 'Not Applicable', 'Pending'],
      question_type: 'radio',
      record_question_id: null,
      record_id: record_id,
      monitoring_record_data_id: null,
      monitoring_record_dt: null,
      monitoring_record_value: null,
      frequency_deficiency: null,
      impact_deficiency: null,
      scope_deficiency: null,
      findings: null,
      answers: null,
    };
    this.addQuestion(quesiton);
    (<any>$('#new-regualtion')).modal('hide');
  }

}
