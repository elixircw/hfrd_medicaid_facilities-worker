import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringToolsRoutingModule } from './monitoring-tools-routing.module';
import { MonitoringToolsComponent } from './monitoring-tools.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    MonitoringToolsRoutingModule
  ],
  declarations: [MonitoringToolsComponent]
})
export class MonitoringToolsModule { }
