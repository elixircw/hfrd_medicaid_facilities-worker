import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringToolsComponent } from './monitoring-tools.component';

describe('MonitoringToolsComponent', () => {
  let component: MonitoringToolsComponent;
  let fixture: ComponentFixture<MonitoringToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
