import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonitoringToolsComponent } from './monitoring-tools.component';

const routes: Routes = [
  {
    path: '',
    component: MonitoringToolsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringToolsRoutingModule { }
