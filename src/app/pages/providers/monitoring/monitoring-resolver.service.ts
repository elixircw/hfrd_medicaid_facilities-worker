import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MonitoringService } from './monitoring.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MonitoringResolverService implements Resolve<any> {

  constructor(private _service: MonitoringService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const visitNumber = route.paramMap.get('visitNumber');
    this._service.visitNumber = visitNumber;
    return this._service.getVisitInfo(visitNumber);
  }

}
