import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import {MatTableDataSource} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { MonitoringService } from '../monitoring.service';

@Component({
  selector: 'monitoring-entrance-conf',
  templateUrl: './monitoring-entrance-conf.component.html',
  styleUrls: ['./monitoring-entrance-conf.component.scss']
})
export class MonitoringEntranceConfComponent implements OnInit {
  entranceConfForm: FormGroup;
  staffInformation: any;
  providerId: any;
  monitoring_id: any;
  dataSource: any;
  entranceConfInfo: any;
  staffDetail: any[] = [];
  staffDetailTemp: any[] = [];
  workerDetail: any[] = [];
  workerDetailTemp: any[] = [];
  workersList: any[] = [];
  // selection = new SelectionModel < staffInfoTable > (true, []);
  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService,
    private _alertService: AlertService, private _service: MonitoringService,
    private _router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.initEntranceConfForm();
    this.providerId = this._service.monitorInfo ? this._service.monitorInfo.provider_id : null;
    this.monitoring_id = this._service.monitorInfo ? this._service.monitorInfo.monitoring_id : null;
    this.getStaffInformation();
    this.getEntranceConf();
    this.getWorkers();

  }

  initEntranceConfForm() {
  this.entranceConfForm = this.formBuilder.group({
   entrance_dt: [null],
   program_changes: [null],
   summary: [null],
   staffdetail: [null],
   workerdetail: [null],
   entrance_conf_id: [null]
   });
  }


  openAssignStaff() {
    (<any>$('#assign-staff')).modal('show');
  }

  openAssignWorker() {
    // this.workerDetailTemp = this.workerDetail;
    (<any>$('#assign-worker')).modal('show');
  }

  getStaffInformation() {
    console.log('Provider Id in staff', this.providerId);
    this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      where: {
        object_id: this.providerId,
      },
    },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.staffInformation = response && response.length ? response.filter(staff => staff.first_nm && staff.first_nm !== '') : [];
        // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getEntranceConf() {
    this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      count : -1,
      where: {
        monitoring_id : this.monitoring_id,
      },
    },
    'prov_monitoring_entrance_conf/listentranceconf?filter'
  ).subscribe(
    (response) => {
      this.entranceConfInfo = response ;
      if (response && response.length) {
        this.entranceConfForm.patchValue(response[response.length - 1]);
        this.staffDetail = response[response.length - 1].staffdetails;
        this.workerDetail = response[response.length - 1].workerdetails;
      }
      // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
    },
    (error) => {
      this._alertService.error('Unable to retrieve information');
    }
  );
  }

  addStaff(conf_data, staff_data) {
    const staffData = { 'staffs': [] };
    if (staff_data && staff_data.length) {
      staff_data.forEach(staff => {
        const staffObj =   {
          'entrance_conf_id': conf_data.entrance_conf_id,
          'objectid': staff.provider_staff_id,
          'objecttype': 'staff',
          'entrance_dt': conf_data.entrance_dt,
          };
          staffData.staffs.push(staffObj);
     });
    }
     this._commonHttpService.create(staffData, 'prov_monitoring_entrance_conf_staff/addupdate').subscribe( response => {});
  }

  addWorker(conf_data, staff_data) {
    const staffData = { 'staffs': [] };
    if (staff_data && staff_data.length) {
      staff_data.forEach(staff => {
        const staffObj =   {
          'entrance_conf_id': conf_data.entrance_conf_id,
          'objectid': staff.userid,
          'objecttype': 'worker',
          'entrance_dt': conf_data.entrance_dt,
          };
          staffData.staffs.push(staffObj);
     });
    }
     this._commonHttpService.create(staffData, 'prov_monitoring_entrance_conf_staff/addupdate').subscribe( response => {});
  }

  addEntranceConf() {
    const entranceConfFormData = this.entranceConfForm.getRawValue();
    const confData = {
      'monitoring_id': this.monitoring_id,
      'program_changes': entranceConfFormData.program_changes,
      'summary': entranceConfFormData.summary,
      'entrance_dt': entranceConfFormData.entrance_dt,
      'entrance_conf_id': entranceConfFormData.entrance_conf_id
      };

    this._commonHttpService.create(confData, 'prov_monitoring_entrance_conf/addupdate').subscribe( response => {
      this._alertService.success('Entrance conference Saved Successfull');
      if (this.staffDetail && this.staffDetail.length) {
        this.addStaff(response, this.staffDetail);
      }
      if (this.workerDetail && this.workerDetail.length) {
        this.addWorker(response, this.workerDetail);
      }
    });
  }

  getWorkers() {
     this._commonHttpService.getArrayList({
      method: 'post',
      nolimit: true,
      count : -1,
      where: {
        appevent: 'PTA'
        // teamid: "3d152f0a-1bc8-4923-a685-ecaf350c8bae"
      },
    },
    'Intakedastagings/getroutingusers'
  ).subscribe(
    (response) => {
      if (response && response['data']) {
      this.workersList = response['data'];
      }
      console.log(response);
      // this.dataSource = new MatTableDataSource < staffInfoTable > (this.staffInformation);
    },
    (error) => {
      this._alertService.error('Unable to retrieve information');
    }
  );
  }

  toggleWorker(checked, worker) {
    if (this.workerDetailTemp && this.workerDetailTemp.length) {

    } else  {
      this.workerDetailTemp = [];
    }
    if (checked) {
    this.workerDetailTemp.push(worker); } else {
      const index = this.workerDetailTemp  ? this.workerDetailTemp.findIndex( workerInfo =>  workerInfo.userid === worker.userid ) : -1 ;
      if (index > -1) {
        this.workerDetailTemp.splice(index, 1);
      }
    }
  }

  toggleStaff(checked, staff) {
    if (this.staffDetailTemp && this.staffDetailTemp.length) {

    } else  {
      this.staffDetailTemp = [];
    }
    if (checked) {
    this.staffDetailTemp.push(staff); } else {
      const index = this.staffDetailTemp  ? this.staffDetailTemp.findIndex( staffInfo => staffInfo.provider_staff_id === staff.provider_staff_id) : -1 ;
      if (index > -1) {
        this.staffDetailTemp.splice(index, 1);
      }
    }
  }

  deleteWorker(index) {
    this.workerDetail.splice(index, 1);
  }

  deleteStaff(index) {
    this.staffDetail.splice(index, 1);
  }


  isWorkerSelected(worker) {
    const index = this.workerDetail && this.workerDetail.length ? this.workerDetail.findIndex( workerInfo => workerInfo.userid === worker.userid) : -1 ;
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }

  isStaffSelected(staff) {
    const index = this.staffDetail && this.staffDetail.length ? this.staffDetail.findIndex( staffInfo => staffInfo.provider_staff_id === staff.provider_staff_id) : -1 ;
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }

  assignWorker() {
    if(this.workerDetailTemp && this.workerDetailTemp.length) {
      this.workerDetailTemp.forEach(worker => {
        this.workerDetail.push(worker);
      });
    }
    this.workerDetailTemp = [];
    (<any>$('#assign-worker')).modal('hide');
  }

  assignStaff() {
    if(this.staffDetailTemp && this.staffDetailTemp.length) {
      this.staffDetailTemp.forEach(staff => {
        this.staffDetail.push(staff);
      });
    }
    this.staffDetailTemp = [];
    (<any>$('#assign-staff')).modal('hide');
  }





}
