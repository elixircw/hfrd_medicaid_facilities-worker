import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringEntranceConfComponent } from './monitoring-entrance-conf.component';

describe('MonitoringEntranceConfComponent', () => {
  let component: MonitoringEntranceConfComponent;
  let fixture: ComponentFixture<MonitoringEntranceConfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringEntranceConfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringEntranceConfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
