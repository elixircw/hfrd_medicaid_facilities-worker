import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringEntranceConfRoutingModule } from './monitoring-entrance-conf-routing.module';
import { MonitoringEntranceConfComponent } from './monitoring-entrance-conf.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
@NgModule({
  imports: [
    CommonModule,
    MonitoringEntranceConfRoutingModule,
    FormMaterialModule,
  ],
  declarations: [MonitoringEntranceConfComponent]
})
export class MonitoringEntranceConfModule { }
