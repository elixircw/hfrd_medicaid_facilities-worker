import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonitoringEntranceConfComponent } from './monitoring-entrance-conf.component';

const routes: Routes = [
  {
    path: '',
    component: MonitoringEntranceConfComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringEntranceConfRoutingModule { }
