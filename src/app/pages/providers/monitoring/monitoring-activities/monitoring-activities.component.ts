import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonHttpService, AlertService } from '../../../../@core/services';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { ReferralUrlConfig } from '../../../provider-referral/provider-referral-url.config';
import { MonitoringService } from '../monitoring.service';
@Component({
  selector: 'monitoring-activities',
  templateUrl: './monitoring-activities.component.html',
  styleUrls: ['./monitoring-activities.component.scss']
})
export class MonitoringActivitiesComponent implements OnInit {
  activitiesForm: FormGroup;
  taskForm: FormGroup;
  newObj = {};
  defaultArray = [];
  activityDropdownItems$: DropdownModel[];
  type: string;
  category: string;
  leftTasks: any[] = [];
  rightTasks: any[] = [];
  private leftSelectedTasks = [];
  private rightSelectedTasks = [];
  programList: any[] = [];
  taskList: any;
  statusList = ['Incomplete', 'Complete'];

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService,
    private _alertService: AlertService, private _monitoringService: MonitoringService) { }

  ngOnInit() {
    this.initActivitiesForm();
    this.loadDropDown();
    this.getProgramList();
    this.getTaskList();
  }

  initActivitiesForm() {
    this.activitiesForm = this.formBuilder.group({
      activity: [null],
      monitoring_period: [null],
      monitoring_date: [null],
      suggested_monitoring_activities: [null],
      selected_monitoring_activities: [null],
      leftSelectedTask: [''],
      rightSelectedTask: [''],
      leftSelectedGoals: [''],
      rightSelectedGoals: [''],
      selectActivityList: [''],
      periodic: [''],
      monthly: [''],
      yearly: [''],
      schedule: [''],
      visitTime: ['']
    });
    this.taskForm = this.formBuilder.group({
      checklist_id: [''],
      checklist_task: [''],
      commnts: [''],
      completeddate: [''],
      status: [''],
    });
  }

  private loadDropDown() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          agency: 'OLM',
          category: 'Monitoring',
          programname: 'RCC'
        }
      },
      'providerapplicant/gettasklist'
    ).subscribe(taskList => {
      console.log('Tasklist monitoring', taskList);
      console.log('Tasklist monitoring', JSON.stringify(taskList));

      taskList.forEach((x) => {
        if (x.subcategory === 'Default') {
          this.defaultArray.push(x);
        } else {
          if (!this.newObj[x.subcategory]) {
            this.newObj[x.subcategory] = [x];
          } else {
            this.newObj[x.subcategory].push(x);
          }
        }
      });
      console.log(this.newObj);
      this.rightTasks.length = 0;
      this.leftTasks = this.newObj['RCC'];
      this.filtertasklist();
      const arr = [];
      this.activityDropdownItems$ = Object.keys(this.newObj).map((res, index) => {
        return new DropdownModel({ text: res, value: this.newObj[res] });
      });
    });
  }

  filtertasklist() {
    if (Array.isArray(this.taskList) && (Array.isArray(this.leftTasks) && this.leftTasks.length)) {
      const list = this.taskList.map(item => item.checklist_task);
      this.leftTasks = this.leftTasks.filter(item => !list.includes(item.task));
    }
  }

  selectAllTask() {
    if (this.leftTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, Object.assign([], this.leftTasks));
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }
  selectTask() {
    if (this.leftSelectedTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, this.leftSelectedTasks);
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  unSelectTask() {
    if (this.rightSelectedTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, this.rightSelectedTasks);
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }
  unSelectAllTask() {
    if (this.rightTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, Object.assign([], this.rightTasks));
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }
  private clearSelectedTasks(position: string) {
    if (position === 'left') {
      this.leftSelectedTasks = [];
    } else {
      this.rightSelectedTasks = [];
    }
  }

  private moveTasks(source: any[], target: any[], selectedItems: any[]) {
    selectedItems.forEach((item: any, i) => {
      const selectedIndex = source.indexOf(item);
      if (selectedIndex !== -1 /*selectedItem.length*/) {
        source.splice(selectedIndex, 1);
        target.push(item);
      }
    });
    // this._changeDetect.detectChanges();
  }

  isTaskLeftSelected(selectedItem) {
    const index = this.leftSelectedTasks.indexOf(selectedItem);
    return (index > -1) ? true : false;
  }

  isTaskRightSelected(selectedItem) {
    const index = this.rightSelectedTasks.indexOf(selectedItem);
    return (index > -1) ? true : false;
  }

  toggleTask(position: string, selectedItem: any, control: any) {
    if (position === 'left') {
      const index = this.leftSelectedTasks.indexOf(selectedItem);
      if (index > -1) {
      this.leftSelectedTasks.splice(index, 1); } else {
        this.leftSelectedTasks.push(selectedItem);
      }
      // if (control.target.checked) {
      //   this.leftSelectedTasks.push(selectedItem);
      // } else {
      //   const index = this.leftSelectedTasks.indexOf(selectedItem);
      //   this.leftSelectedTasks.splice(index, 1);
      // }
    } else {
      const index = this.rightSelectedTasks.indexOf(selectedItem);
      if (index > -1) {
      this.rightSelectedTasks.splice(index, 1); } else {
        this.rightSelectedTasks.push(selectedItem);
      }

      // if (control.target.checked) {
      //   this.rightSelectedTasks.push(selectedItem);
      // } else {
      //   const index = this.rightSelectedTasks.indexOf(selectedItem);
      //   this.rightSelectedTasks.splice(index, 1);
      // }
    }
  }


  getProgramList() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        providertype: 'Private'
      },
      ReferralUrlConfig.EndPoint.Referral.listprogramnamesUrl
    ).subscribe(data => {
      this.programList = data;
     });
  }

  removeObject() {
    this.rightTasks = [];
  }

  activitySave() {
    const visitid = this._monitoringService.visitNumber;
    const taskList = [];

    this.rightTasks.forEach((element) => {
      const newTask = {
        task: element.task,
        agency: element.agency,
        programname: element.programname,
        category: 'Monitoring',
        commnts: element.description,
        subcategory: element.task,
        description: element.description,
      };
      taskList.push(newTask);
    });

    const requestObj = {
      'monitoring_id': visitid,
      'task': taskList
    };
    this._commonHttpService.create(requestObj, 'providerapplicant/addchecklist').subscribe(
      (response) => {
        (<any>$('#add-task')).modal('hide');
        this._alertService.success('CheckList Added successfully!');
        this.removeObject();
        this.getTaskList();
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        console.log('Save Checklist Error', error);
        return false;
      }
    );

  }
  private getTaskList() {
    const visitid = this._monitoringService.visitNumber;
    this._commonHttpService.getPagedArrayList(
      {
          method: 'post',
          monitoring_id: visitid,
          category: 'Monitoring'
      },
      'providerapplicant/getchecklist'
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      this.filtertasklist();
    });
  }

  updateTaskStatus() {
    const visitid = this._monitoringService.visitNumber;
    const taskList = [];

    const task = this.taskForm.getRawValue();

    const requestObj = {
      'monitoring_id': visitid,
      'task': [task]
    };
    this._commonHttpService.create(requestObj, 'providerapplicant/updatechecklist').subscribe(
      (response) => {
        (<any>$('#update-task')).modal('hide');
        this._alertService.success('Task updated successfully!');
        this.getTaskList();
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
      }
    );

  }

  edittask(obj, action) {
    this.taskForm.patchValue(obj);
    if (action) {
      this.taskForm.enable();
    } else {
      this.taskForm.disable();
    }
    (<any>$('#update-task')).modal('show');
  }
}
