import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringActivitiesRoutingModule } from './monitoring-activities-routing.module';
import { MonitoringActivitiesComponent } from './monitoring-activities.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    MonitoringActivitiesRoutingModule
  ],
  declarations: [MonitoringActivitiesComponent]
})
export class MonitoringActivitiesModule { }
