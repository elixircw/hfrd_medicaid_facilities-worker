import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonitoringActivitiesComponent } from './monitoring-activities.component';

const routes: Routes = [
  {
    path: '',
    component: MonitoringActivitiesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringActivitiesRoutingModule { }
