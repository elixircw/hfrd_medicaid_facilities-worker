import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringActivitiesComponent } from './monitoring-activities.component';

describe('MonitoringActivitiesComponent', () => {
  let component: MonitoringActivitiesComponent;
  let fixture: ComponentFixture<MonitoringActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
