import { AppConstants } from '../../@core/common/constants';

export class ProviderConstants {

    /**
     * Provider General Constants
     */
    public static monthDropDown = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public static yearDropDown = [];
    public static periodicDropDown = ['Initial', 'Monthly', 'Quarterly', 'Periodic', 'Mid-Year', 'Re-Licensure'];
    public static timeOfVisit = ['Night', 'Weekend', 'Regular'];
    public static visitSchedule = ['Announced', 'Unannounced'];
    
    /**
     * Provider Complaints Constants
     */
    public static EVENTS = {
        'COMPLAINT_REFFERAL_SUBMITTED': 'PRCM'
    };

    public static NEW_COMPLAINT_ACCESS = [
        AppConstants.ROLES.LICENSING_ADMINISTRATOR,
        AppConstants.ROLES.QUALITY_ASSURANCE,
        AppConstants.ROLES.PROVIDER_DJS_QA
    ];

    public static COMPLAINT_STATUS = {
        pending: 'pending',
        refferalsubmit: 'refferalsubmit', // PRCM
        refferel_rejected: 'refferel_rejected',
        refferalApproved: 'refferal_approved',
        complaint_rejected: 'complaint_rejected',
        complaint_submited: 'complaint_submited',
        complaint_reviewed: 'complaint_reviewed',
        complaint_approved: 'complaint_approved',
        complaint_approval_rejected: 'complaint_approval_rejected',
        complaint_all: null,
        complaint_sent: 'complaint_sent',
        closed: 'closed'

    };

    // Description used for the notification messages
    public static COMPLAINT_STATUS_DESCRIPTION = {
        pending: 'Complaint Referral Pending',
        refferalsubmit: 'Complaint Referral Submitted',
        refferel_rejected: 'Complaint Referral Rejected',
        refferal_approved: 'Complaint Referral Approved',
        complaint_rejected: 'Complaint Rejected',
        complaint_submited: 'Complaint Submitted',
        complaint_reviewed: 'Complaint Reviewed',
        complaint_approved: 'Complaint Approved',
        complaint_sent: 'Complaint Sent',
        complaint_approval_rejected: 'Complaint Approval Rejected',
        closed: 'Complaint Closed'

    };

    public static COMPLAINT_DASHBOARD_TABS = [
        {
            tabName: 'REFERRAL DRAFTS',
            filterName: ProviderConstants.COMPLAINT_STATUS.pending,
            roles: [
                AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                AppConstants.ROLES.QUALITY_ASSURANCE,
                AppConstants.ROLES.PROVIDER_DJS_QA
            ]
        },
        {
            tabName: 'COMPLAINT REVIEW',
            filterName: ProviderConstants.COMPLAINT_STATUS.complaint_all,
            roles: [
                AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                AppConstants.ROLES.QUALITY_ASSURANCE,
                AppConstants.ROLES.PROVIDER_DJS_QA
            ]
        },
        {
            tabName: 'REJECTED',
            filterName: ProviderConstants.COMPLAINT_STATUS.refferel_rejected,
            roles: [
                AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                AppConstants.ROLES.QUALITY_ASSURANCE,
                AppConstants.ROLES.PROVIDER_DJS_QA,
            ]
        },
        {
            tabName: 'REFERRAL INBOX',
            filterName: ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
            ]
        },
        ,
        {
            tabName: 'COMPLAINT REVIEW INBOX',
            filterName: ProviderConstants.COMPLAINT_STATUS.complaint_all,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
            ]
        },
        {
            tabName: 'REJECTED',
            filterName: ProviderConstants.COMPLAINT_STATUS.complaint_approval_rejected,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
            ]
        },
        {
            tabName: 'CLOSED',
            filterName: ProviderConstants.COMPLAINT_STATUS.closed,
            roles: [
                AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                AppConstants.ROLES.QUALITY_ASSURANCE,
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                AppConstants.ROLES.PROVIDER_DJS_QA,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
            ]
        },
    ];

    public static COMPLAINT_DETIAL_TABS =
        [
            {
                id: 'refferal',
                path: 'refferal',
                name: 'Complaint Referral',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [
                    ProviderConstants.COMPLAINT_STATUS.pending,
                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.refferel_rejected,
                    ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'review',
                path: 'review',
                name: 'Complaint Review',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [

                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'refferal',
                path: 'contacts',
                name: 'Contacts',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [
                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'monitor',
                path: 'monitor-activity',
                name: 'Monitor Activity',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [

                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'deficiency-violation',
                path: 'deficiency-violation',
                name: 'Deficiency/Violation ',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [

                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'summary',
                path: 'summary',
                name: 'Summary',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [

                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'outcomes',
                path: 'outcomes',
                name: 'outcomes',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [

                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'documents',
                path: 'documents',
                name: 'documents',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [

                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            },
            {
                id: 'decision',
                path: 'decision',
                name: 'decision',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                    AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                    AppConstants.ROLES.PROVIDER_DJS_QA,
                    AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
                ],
                statuses: [

                    ProviderConstants.COMPLAINT_STATUS.refferalApproved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
                    ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
                    ProviderConstants.COMPLAINT_STATUS.complaint_submited,
                    ProviderConstants.COMPLAINT_STATUS.closed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
                    ProviderConstants.COMPLAINT_STATUS.complaint_approved,
                    ProviderConstants.COMPLAINT_STATUS.complaint_sent
                ]
            }


        ];

    //  1. Approve and assigns to worker
    //  2. Denies and closes
    //  3. Return to worker
    //  4. Refers to another licensing agency
    public static COMPLAINT_DECISONS = [
        {
            displayName: 'Approve and Assign',
            eventcode: ProviderConstants.COMPLAINT_STATUS.refferalApproved,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Deny and close',
            eventcode: ProviderConstants.COMPLAINT_STATUS.closed,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Return to worker',
            eventcode: ProviderConstants.COMPLAINT_STATUS.refferel_rejected,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Refers to another licensing agency',
            eventcode: ProviderConstants.COMPLAINT_STATUS.refferalApproved,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Submit the complaint review',
            eventcode: ProviderConstants.COMPLAINT_STATUS.complaint_submited,
            roles: [
                AppConstants.ROLES.QUALITY_ASSURANCE,
                AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                AppConstants.ROLES.PROVIDER_DJS_QA
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        }
    ];

    public static COMPLAINT_REVIEW_DECISONS = [
        {
            displayName: 'Approve',
            eventcode: ProviderConstants.COMPLAINT_STATUS.complaint_reviewed,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Approve',
            eventcode: ProviderConstants.COMPLAINT_STATUS.complaint_approved,
            roles: [
                AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Denies and closes',
            eventcode: ProviderConstants.COMPLAINT_STATUS.closed,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
                AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Return to Worker',
            eventcode: ProviderConstants.COMPLAINT_STATUS.complaint_approval_rejected,
            roles: [
                AppConstants.ROLES.EXECUTIVE_DIRECTOR,
                AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Return to worker',
            eventcode: ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE,
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Refers to another licensing agency',
            eventcode: ProviderConstants.COMPLAINT_STATUS.closed,
            roles: [
                AppConstants.ROLES.PROGRAM_MANAGER,
                AppConstants.ROLES.PROVIDER_DJS_RESOURCE
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        },
        {
            displayName: 'Send Complaint',
            eventcode: ProviderConstants.COMPLAINT_STATUS.complaint_sent,
            roles: [
                AppConstants.ROLES.QUALITY_ASSURANCE,
                AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                AppConstants.ROLES.PROVIDER_DJS_QA,
            ],
            statuses: [
                ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
            ]
        }
    ];


    /**
     * Provider Training Constants
     */
    public static TRAINING_DETAILS_TABS =
    [
        {
            id: 'attendance',
            path: 'attendance',
            name: 'Training Attendance',
            roles: [
                AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                AppConstants.ROLES.QUALITY_ASSURANCE,
                AppConstants.ROLES.PROGRAM_MANAGER,
            ]
        }
    ];

    /**
     * Provider Monitoring Constants
     */
    public static MONITORING_TABS =
    [
        {
            id: 'program-info',
            path: 'program-info',
            name: 'Program Info'
        },
        {
            id: 'activities',
            path: 'activities',
            name: 'Activities'
        },
        {
            id: 'entrance-conf',
            path: 'entrance-conf',
            name: 'Entrance CONF.'
        },
        {
            id: 'tools',
            path: 'tools',
            name: 'Tools'
        },
        {
            id: 'interviews',
            path: 'interviews',
            name: 'Interviews'
        },
        {
            id: 'exit-conf',
            path: 'exit-conf',
            name: 'Exit CONF.'
        },
        {
            id: 'documents',
            path: 'documents',
            name: 'Documents'
        },
        {
            id: 'review',
            path: 'review',
            name: 'Review'
        }
    ];



}
