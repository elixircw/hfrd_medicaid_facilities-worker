import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintComponent } from './complaint.component';

const routes: Routes = [
  {
    path: '',
    component: ComplaintComponent
  },
  {
    path: 'detail/:complaintid',
    loadChildren: './complaint-detail/complaint-detail.module#ComplaintDetailModule'
  },
  {
    path: 'dashboard',
    loadChildren: './complaint-dashboard/complaint-dashboard.module#ComplaintDashboardModule'
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintRoutingModule { }
