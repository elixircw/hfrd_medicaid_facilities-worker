import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintDecisionComponent } from './complaint-decision.component';

describe('ComplaintDecisionComponent', () => {
  let component: ComplaintDecisionComponent;
  let fixture: ComponentFixture<ComplaintDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
