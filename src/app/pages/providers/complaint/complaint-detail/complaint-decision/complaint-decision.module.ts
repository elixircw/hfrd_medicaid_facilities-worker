import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComplaintDecisionRoutingModule } from './complaint-decision-routing.module';
import { ComplaintDecisionComponent } from './complaint-decision.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { ComplaintDecisonService } from './complaint-decison.service';
import { SharedPipesModule } from '../../../../../@core/pipes/shared-pipes.module';
import { RouteUsersModule } from '../../../route-users/route-users.module';

@NgModule({
  imports: [
    CommonModule,
    ComplaintDecisionRoutingModule,
    FormMaterialModule,
    SharedPipesModule,
    RouteUsersModule
  ],
  declarations: [ComplaintDecisionComponent],
  providers: [ComplaintDecisonService]
})
export class ComplaintDecisionModule { }
