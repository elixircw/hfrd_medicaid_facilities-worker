import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services';
import { ProvidersUrlConfig } from '../../../providers-url.config';
import { ComplaintDetailsService } from '../complaint-details.service';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { ProviderConstants } from '../../../constants';

@Injectable()
export class ComplaintDecisonService {

  constructor(private _commonHttpService: CommonHttpService,
    private _complaintDetailsService: ComplaintDetailsService) {

  }

  saveDecision(decison: any) {
    return this._commonHttpService.create(decison, ProvidersUrlConfig.EndPoint.Complaint.DECISION_URL);
  }

  sendComplaintMail(decison: any) {
    return this._commonHttpService.create(decison, ProvidersUrlConfig.EndPoint.Complaint.SEND_MAIL);
  }

  getNotificationMessage(data) {
    var statusDescription = this.getStatusDescription(data.status);
    return this._complaintDetailsService.complaintDetail.complaint_number + ' ' + statusDescription + ' with narrative of - ' + data.comments;
  }

  getStatusDescription(status: string) {
    return ProviderConstants.COMPLAINT_STATUS_DESCRIPTION[status];
  }
  
  getList() {
    const data = {
      'complaint_number': this._complaintDetailsService.complaintDetail.complaint_number
    };
    return this._commonHttpService.getArrayList(
      new PaginationRequest({
        limit: 100,
        page: 1,
        method: 'get',
        where: data
      }),
      ProvidersUrlConfig.EndPoint.Complaint.DECISION_LIST_URL
    );
  }
}
