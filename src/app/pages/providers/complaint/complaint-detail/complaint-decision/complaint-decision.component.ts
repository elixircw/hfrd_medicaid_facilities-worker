import { Component, OnInit } from '@angular/core';
import { ProviderAuthorizationService } from '../../../provider-authorization.service';
import { ComplaintDetailsService } from '../complaint-details.service';
import { ComplaintDecisonService } from './complaint-decison.service';
import { ComplaintDecison, ComplaintDetail } from '../_entities/complaint-detail.model';
import { AlertService, DataStoreService, AuthService } from '../../../../../@core/services';
import { RouteUserService } from '../../../route-users/route-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AppConstants } from '../../../../../@core/common/constants';
const OTHER_AGENCY = 'Refers to another licensing agency';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'complaint-decision',
  templateUrl: './complaint-decision.component.html',
  styleUrls: ['./complaint-decision.component.scss']
})


export class ComplaintDecisionComponent implements OnInit {

  decisons = [];
  status: string;
  statusDescription: string;
  decison: string;
  comments: string;
  signimage: string;
  agency: string;
  agencyEmail: string;
  decisonHistory = [];
  agencyList = ['DJS', 'DHS', 'MDH', 'Other'];
  onForwardToOther = false;
  cmpltSavBtnTxt: string = "Sumbit";
  userAssingmentSubscription: Subscription;
  isViewOnly: Boolean = false;
  decisionForm: FormGroup;
  hideSignature: Boolean = false;
  userProfile: AppUser;
  userName: string;
  complaintDetails: ComplaintDetail;
  userType: string;
  constructor(private _router: Router,
    private _authorization: ProviderAuthorizationService,
    private complaintSerivcie: ComplaintDetailsService,
    private _service: ComplaintDecisonService,
    private _alertService: AlertService,
    private route: ActivatedRoute,
    private _formBuild: FormBuilder,
    private _routeUserService: RouteUserService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService) {
    const status = this.complaintSerivcie.getCurrentStatus();
    this.decisons = this._authorization.getDecisons(status);
  }

  ngOnInit() {
    this.userProfile = this._authService.getCurrentUser();
    if (this.userProfile) {
      this.userName = this.userProfile.role.name;
    }
    this.complaintDetails = this.complaintSerivcie.complaintDetail;
    if (this.complaintDetails) {
      this.userType = this._authService.getAgencyName();
      this.status = this.complaintDetails.status ? this.complaintDetails.status : null;
      if (this.userType === 'OLMDJS') {
        this.agencyList = this.agencyList.filter(item => {
          if (item !== 'DJS') {
            return item;
          }
        });
        if ( this.status === 'refferal_approved' ||  this.status === 'complaint_approved' || this.status === 'refferel_rejected' || this.status === 'complaint_rejected'
         || this.status === 'complaint_approval_rejected') {
          if (this.userName === AppConstants.ROLES.PROVIDER_DJS_QA) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if ( this.status === 'complaint_submited' ||  this.status === 'refferalsubmit' || this.status === 'complaint_approval_rejected') {
          if (this.userName === AppConstants.ROLES.PROVIDER_DJS_RESOURCE) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if ( this.status === 'complaint_reviewed') {
          if (this.userName === AppConstants.ROLES.PROVIDER_DJS_SECRETARYDESIGNEE) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if (this.status === 'complaint_sent') {
          this.isViewOnly = true;
        }
      } else {
        this.agencyList = this.agencyList.filter(item => {
          if (item !== 'DHS') {
            return item;
          }
        });
        if (this.status === 'refferal_approved' ||  this.status === 'complaint_approved' || this.status === 'refferel_rejected' || this.status === 'complaint_rejected'
        || this.status === 'complaint_approval_rejected') {
          if (this.userName === AppConstants.ROLES.QUALITY_ASSURANCE) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if ( this.status === 'complaint_submited' ||  this.status === 'refferalsubmit' || this.status === 'complaint_approval_rejected') {
          if (this.userName === AppConstants.ROLES.PROGRAM_MANAGER) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if ( this.status === 'complaint_reviewed') {
          if (this.userName === AppConstants.ROLES.EXECUTIVE_DIRECTOR) {
            this.isViewOnly = false;
          } else {
            this.isViewOnly = true;
          }
        } else if (this.status === 'complaint_sent') {
          this.isViewOnly = true;
        }
      }
    }
    this._dataStoreService.setData('ComplaintDecisions', null);
    this.loadList();
    this.initDecisionForm();
    if (!this.isViewOnly) {
      this.isViewOnly = this._dataStoreService.getData('isViewOnly');
    }
  }

  initDecisionForm() {
      this.decisionForm = this._formBuild.group({ 
        status: [''],
        agency: [''],
        agencyEmail: [''],
        comments: [''],
        date: [new Date()],
        signimage: ['']
      });
  }

  openDecision() {
    this.hideSignature = true;
    setTimeout(() => this.hideSignature = false, 200);
    this.decisionForm.reset();
    this.decisionForm.patchValue({date: new Date()}); }

  
  loadList() {
    this._service.getList().subscribe(response => {
      console.log(response);
      if (response && response.length) {
        this.decisonHistory = response;
      }

    });
  }

  onStatusChange(data) {
    console.log('display name', data);
    this.cmpltSavBtnTxt = (data.source.triggerValue == "Approve and Assign") ? "Assign" : "Submit";
    if (data.source.triggerValue === OTHER_AGENCY) {
      this.onForwardToOther = true;
    } else {
      this.onForwardToOther = false;
    }

  }

  listenOnUserAssignment() {
    this.userAssingmentSubscription = this._routeUserService.selectedUserOnAssign$.subscribe(user => {
      if (user) {
        const decision = this.getDecision();
        decision.assignsecurityuserid = [];
        if (user && user.userid) {
          decision.assignsecurityuserid.push(user.userid);
        } else if (user && user.assigneduserid.length) {
          user.assigneduserid.forEach(item => {
            decision.assignsecurityuserid.push(item.userid);
          });
        }
        this.saveDecision(decision);
      }
    });
  }

  getDecision() {
    const decision: ComplaintDecison = new ComplaintDecison();
    const decisionInf = this.decisionForm.getRawValue();
    decision.provider_id = this.complaintSerivcie.complaintDetail.provider_id;
    decision.complaint_date = this.complaintSerivcie.complaintDetail.complaint_date;
    decision.complaint_number = this.complaintSerivcie.complaintDetail.complaint_number;
    decision.status = decisionInf.status;
    decision.comments = decisionInf.comments;
    if (this.userName && ( this.userName === 'Quality Assurance' || this.userName === 'Provider_DJS_QA')) {
      decision.qa_signimage = decisionInf.signimage;
      decision.qa_signdate = decisionInf.date;
    } else if (this.userName && ( this.userName === 'Program Manager' || this.userName === 'Provider_DJSResource')) {
      decision.pm_signimage = decisionInf.signimage;
      decision.pm_signdate = decisionInf.date;
    } else if (this.userName && ( this.userName === 'Executive Director' || this.userName === 'Provider_DJS_SecretaryDesignee')) {
      decision.ed_signimage = decisionInf.signimage;
      decision.ed_signdate = decisionInf.date;
    }
    // Need the status 'description' rather than the status code
    decision.notification = this._service.getNotificationMessage(decisionInf);
    decision.assignsecurityuserid = null;
    if (this.onForwardToOther) {
      decision.agency = decisionInf.agency;
      decision.agency_email = decisionInf.agencyEmail;
    }
    decision.agencyName = this.complaintDetails.teamtype_key;
    this._dataStoreService.setData('ComplaintDecisions', decision);
    return decision;
  }

  onSubmitDecison() {
    const decision: ComplaintDecison = this.getDecision();
    if (this.decisionForm.invalid) {
      this._alertService.error('Please fill required feilds');
      return;
    }

    if (decision.status === 'refferal_approved') {
      (<any>$('#add-decision')).modal('hide');
      this.openRouteUser();
    } else if (decision.status === 'complaint_approved') {
      (<any>$('#add-decision')).modal('hide');
      this.openRouteUser();
    } else if (decision.status === 'complaint_reviewed') {
      (<any>$('#add-decision')).modal('hide');
      this.openRouteUser();
    } else {
        if (decision.status === 'refferal_rejected' || decision.status === 'complaint_approval_rejected' || decision.status === 'complaint_rejected' ||
        decision.status === 'complaint_sent') {
          this.saveDecision(decision);
        } else {
          (<any>$('#add-decision')).modal('hide');
          this.openRouteUser();
        }
    }
    // check status conditon
    // create on metho saVe dceions
    // if approve open route user
    //  on if(<any>$('#add-decision')).modal('hide');
    // create listner method
    // on listner call saveDecion method decion.assingnsecurityuserid = user.userid
    // on else call savdDecison
  }

  openRouteUser() {
    this.listenOnUserAssignment();
    this._router.navigate(['assign-user'], { relativeTo: this.route });
  }

  saveDecision(decision) {
    this._service.saveDecision(decision).subscribe(response => {
      if(response) {
        if(decision.status === 'refferal_approved') {
          this.complaintSerivcie.getComplaintDetails(decision.complaint_number).subscribe( compaintDetail => {  
         if(compaintDetail && compaintDetail.length) {
            this._service.sendComplaintMail(compaintDetail[0]).subscribe(mailResponse => {
          (<any>$('#add-decision')).modal('hide');
          this._alertService.success('Decison Submitted Successfully', true);
          const url = '/pages/provider/complaints/dashboard/';
          this._router.navigate([url]);
          });
          }
         });
         } else {
          (<any>$('#add-decision')).modal('hide');
          this._alertService.success('Decison Submitted Successfully', true);
          const url = '/pages/provider/complaints/dashboard/';
          this._router.navigate([url]);
         }
      } else {
        (<any>$('#add-decision')).modal('hide');
        this._alertService.success('Decison Submitted Successfully', true);
        const url = '/pages/provider/complaints/dashboard/';
        this._router.navigate([url]); 
      }
    });
  }

  ngOnDestroy() {
    if (this.userAssingmentSubscription)
      this.userAssingmentSubscription.unsubscribe();
  }

}
