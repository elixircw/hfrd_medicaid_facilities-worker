import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintDecisionComponent } from './complaint-decision.component';
import { RouteUsersComponent } from '../../../route-users/route-users.component';

const routes: Routes = [{
  path: '',
  component: ComplaintDecisionComponent,
  children: [{
    path: 'assign-user',
    component: RouteUsersComponent
  },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintDecisionRoutingModule { }
