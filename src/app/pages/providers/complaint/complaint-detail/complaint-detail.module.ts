import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComplaintDetailRoutingModule } from './complaint-detail-routing.module';
import { ComplaintDetailComponent } from './complaint-detail.component';
import { RefferalHeaderComponent } from './refferal-header/refferal-header.component';
import { NavigationTabComponent } from './navigation-tab/navigation-tab.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ComplaintDetailsResolverService } from './complaint-details-resolver.service';
import { ComplaintDetailsService } from './complaint-details.service';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';


@NgModule({
  imports: [
    CommonModule,
    ComplaintDetailRoutingModule,
    FormMaterialModule,
  ],
  declarations: [ComplaintDetailComponent, RefferalHeaderComponent, NavigationTabComponent],
  providers: [ComplaintDetailsResolverService, ComplaintDetailsService]
})
export class ComplaintDetailModule { }
