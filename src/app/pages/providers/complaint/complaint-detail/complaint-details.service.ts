import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthService, CommonHttpService } from '../../../../@core/services';
import { ComplaintDetail } from './_entities/complaint-detail.model';
import { ProvidersUrlConfig } from '../../providers-url.config';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ProviderComplaintContactsComponent } from './provider-complaint-contacts/provider-complaint-contacts.component';
import { ProviderConstants } from '../../constants';
import { Router } from '@angular/router';

@Injectable()
export class ComplaintDetailsService {

  complaintid: string;
  complaintDetail: ComplaintDetail;
  complaintDetailForm: FormGroup;
  constructor(private authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private formBuilder: FormBuilder,
    private _router: Router) { }

  getComplaintDetails(complaint_number: string) {

    return this._commonHttpService.getArrayList({
      method: 'get',
      where: { complaint_number: complaint_number }
    }, ProvidersUrlConfig.EndPoint.Complaint.DETAIL_URL);
  }

  getMetaData(complaint_number: string): any {
    const complaintDetatail = {
      complaint_number: complaint_number,
      inserted_on: new Date(),
      created_by: this.authService.getCurrentUser().user.securityusersid,
      displayname: this.authService.getCurrentUser().user.userprofile.fullname
    };
    console.log('meta data', complaintDetatail);
    return complaintDetatail;
  }

  saveComplaintDetails(complaintDetails: ComplaintDetail) {
    return this._commonHttpService.create(complaintDetails, ProvidersUrlConfig.EndPoint.Complaint.CREATE_UPDATE_URL);
  }

  sendComplaintMail(complaintDetails: ComplaintDetail) {
    return this._commonHttpService.create(complaintDetails, ProvidersUrlConfig.EndPoint.Complaint.SEND_MAIL);
  }

  getProviderComplaints() {
    return Observable.of([]);
  }

  initComplaintDetailForm() {
    this.complaintDetailForm = this.formBuilder.group({
      'provider_complaintid': [null],
      'intakeservreqinputtypeid': [null, Validators.required],
      'complaint_source': [null],
      'source_information_type': [null],
      'source_specification': [null],
      'complaint_date': [null, Validators.required],
      'narrative': [null, Validators.required],
      'complainant_firstname': [null],
      'complainant_lastname': [null],
      'complainant_phone': [null],
      'complainant_email': [null],
      'complainant_address1': [null],
      'complainant_address2': [null],
      'complainant_city': [null],
      'complainant_state': [null],
      'complainant_county': [null],
      'complainant_zipcode': [null],
      'complaint_number': [null],
      'teamtype_key': [null],
      'provider_id': [null],
      'site_id': [null],
      'is_draft': 1,
      'inserted_by': [null],

    });
  }

  getComplaintDetailForm() {
    return this.complaintDetailForm;
  }

  getCurrentStatus() {
    if (this.complaintDetail && this.complaintDetail.status) {
      return this.complaintDetail.status;
    }
    return ProviderConstants.COMPLAINT_STATUS.pending;
  }

  getCurrentStatusDescription() {
    if (this.complaintDetail && this.complaintDetail.statusdescription) {
      return this.complaintDetail.statusdescription;
    }
    // Not sure what to return for default!
  }

  isRefferal() {
    return this.getCurrentStatus() === (ProviderConstants.COMPLAINT_STATUS.pending || ProviderConstants.COMPLAINT_STATUS.refferel_rejected);
  }

  getProviderComplaintId() {
    if (this.complaintDetail && this.complaintDetail.provider_complaintid) {
      return this.complaintDetail.provider_complaintid;
    }
    return null;
  }

  getOutcomes() {
    if (this.complaintDetail && this.complaintDetail.outcomes) {
      return this.complaintDetail.outcomes;
    }
    return null;
  }

  getSummary() {
    if (this.complaintDetail && this.complaintDetail.summary) {
      return this.complaintDetail.summary;
    }
    return null;
  }

  navigateTo(page) {
    const url = '/pages/provider/complaints/detail/' +  this.complaintid + '/' + page;
    this._router.navigate([url]);
}




}
