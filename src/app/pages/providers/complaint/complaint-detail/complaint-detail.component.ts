import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { ComplaintDetailsService } from './complaint-details.service';
import { FormGroup } from '@angular/forms';
import { ProviderAuthorizationService } from '../../provider-authorization.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'complaint-detail',
  templateUrl: './complaint-detail.component.html',
  styleUrls: ['./complaint-detail.component.scss']
})
export class ComplaintDetailComponent implements OnInit {


  complaintDetailForm: FormGroup;
  tabs = [];
  urlArray: any;
  currPage: any;
  constructor(private route: ActivatedRoute,
    private _service: ComplaintDetailsService,
    private _authorization: ProviderAuthorizationService,
    private _router: Router 
  ) {
    this.route.data.subscribe(data => {
      console.log('complaint resolve ', data);
      this._service.initComplaintDetailForm();
      this.complaintDetailForm = this._service.getComplaintDetailForm();
      if (data && data.complaintDetail && data.complaintDetail.length) {
        this._service.complaintDetail = data.complaintDetail[0];
        this.tabs = this._authorization.getComplaintDashboardTabs();
      } else {
        this._service.complaintDetail = this._service.getMetaData(this._service.complaintid);
      }
    });
  }

  ngOnInit() {
    this.urlArray =  this._router.url  ? this._router.url.split('/') : [];
    this.currPage = this.urlArray && this.urlArray.length ? this.urlArray[this.urlArray.length-1] : null;
  }

  isNextNotAvailable() {
    const complainData =  this._service.complaintDetail ;
   if(complainData && complainData.status === "FNSWOR") {
      return true;
    }
    if(complainData.status ===  null || !complainData.status || complainData.status === 'refferel_rejected') {
         
      return true;
    }
    this.urlArray =  this._router.url  ? this._router.url.split('/') : [];
    this.currPage = this.urlArray && this.urlArray.length ? this.urlArray[this.urlArray.length-1] : null;
    if(this.currPage && (this.currPage === 'decision' || this.currPage === 'assign-user')) {
      return true;
    } else {
      return false;
    }
  }
  
  goToNextTab() {
    this.urlArray =  this._router.url  ? this._router.url.split('/') : [];
    this.currPage = this.urlArray && this.urlArray.length ? this.urlArray[this.urlArray.length-1] : null;
    let url;
    switch(this.currPage) {
       case 'refferal' : 
                   url = 'review';
                   break;
      case 'review' : 
                   url = 'contacts';
                   break;
      case 'contacts' : 
                   url = 'monitor-activity';
                   break;
      case 'monitor-activity' : 
                   url = 'deficiency-violation';
                   break;
      case 'deficiency-violation' : 
                   url = 'summary';
                   break;
      case 'summary' : 
                   url = 'outcomes';
                   break;
      case 'outcomes' : 
                   url = 'documents';
                   break;
      case 'documents' : 
                   url = 'decision';
                   break;
    }
    const complainData =  this._service.complaintDetail ;
    if(complainData && complainData.status === "refferalsubmit") { 
      url = 'decision';
    }
    this._service.navigateTo(url);
  }






}
