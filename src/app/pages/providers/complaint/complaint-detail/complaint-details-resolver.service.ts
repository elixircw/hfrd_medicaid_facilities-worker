import { Injectable } from '@angular/core';
import { ComplaintDetailsService } from './complaint-details.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ComplaintDetailsResolverService implements Resolve<any> {

  constructor(private _service: ComplaintDetailsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const complaintid = route.paramMap.get('complaintid');
    this._service.complaintid = complaintid;
    return this._service.getComplaintDetails(complaintid);
  }
}
