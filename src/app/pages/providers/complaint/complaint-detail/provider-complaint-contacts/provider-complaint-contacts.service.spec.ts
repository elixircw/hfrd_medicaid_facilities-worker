import { TestBed, inject } from '@angular/core/testing';
import { ProviderComplaintContactsService } from './provider-complaint-contacts.service';

describe('ProviderComplaintContactsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProviderComplaintContactsService]
    });
  });

  it('should be created', inject([ProviderComplaintContactsService], (service: ProviderComplaintContactsService) => {
    expect(service).toBeTruthy();
  }));
});
