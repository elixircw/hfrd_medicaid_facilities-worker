import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { ProviderComplaintContactsService } from './provider-complaint-contacts.service';
import { PaginationInfo, DynamicObject } from '../../../../../@core/entities/common.entities';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ComplaintDetailsService } from '../complaint-details.service';
import { AlertService, DataStoreService } from '../../../../../@core/services';
import { Contact } from '../_entities/complaint-detail.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-complaint-contacts',
  templateUrl: './provider-complaint-contacts.component.html',
  styleUrls: ['./provider-complaint-contacts.component.scss']
})
export class ProviderComplaintContactsComponent implements OnInit {
  totalRecords: number;
  dynamicObject: DynamicObject = {};
  paginationInfo: PaginationInfo = new PaginationInfo();
  private pageStream$ = new Subject<number>();
  previousPage: number;
  public contactSource = [];
  public methodOfContact = [];
  contactList = [];
  contactNotesList = [];
  addContactForm: FormGroup;
  contactNoteForm: FormGroup;
  searchCritera: any;
  contactView: any;
  isViewOnly: Boolean = false;
  constructor(private _service: ProviderComplaintContactsService,
    private formBuilder: FormBuilder,
    private _complaintDetailServicie: ComplaintDetailsService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService) {
    this.searchCritera = { 'provider_complaintid': this._complaintDetailServicie.getProviderComplaintId() };
  }

  ngOnInit() {
    this.loadDropDowns();
    this.initAddContactForm();
    this.loadContactsList(20, 1, this.searchCritera);
    this.loadContactNotes(20, 1, this.searchCritera);
    this.isViewOnly = this._dataStoreService.getData('isViewOnly');
  }
  loadDropDowns() {
    this.contactSource = this._service.loadContactSourcesDrop();
    this.methodOfContact = this._service.loadMethodOfContactsDrop();
  }
  initAddContactForm() {
    this.addContactForm = this.formBuilder.group({
      'complaint_contacts_name': ['', Validators.required],
      'complaint_contacts_source': ['', Validators.required],
      'complainant_contacts_phone': ['']
    });
    this.contactNoteForm = this.formBuilder.group({
      'provider_complaint_contactsid': ['', Validators.required],
      'complaint_notes_date': [null, Validators.required],
      'complaint_notes_method_of_contact': [''],
      'complaint_notes_note': [''],
      'provider_complaint_notesid': [null]

    });
  }

  addContact() {
    this.addContactForm.reset();
    (<any>$('#add-contact')).modal('show');
  }
  public findInvalidControls(form: FormGroup) {
    const invalid = [];
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log(invalid);
    return invalid;
  }
  saveContact(contact: Contact) {
    contact = this.addContactForm.getRawValue();
    contact.provider_complaintid = this._complaintDetailServicie.getProviderComplaintId();
    console.log(contact);
    if (this.addContactForm.invalid) {
      this.findInvalidControls(this.addContactForm);
      this._alertService.error('Please fill required feilds');
      return;
    }
    this._service.addContact(contact).subscribe((response) => {
      console.log(response);
      this._alertService.success('Contact added successfully');
      this.loadContactsList(20, 1, this.searchCritera);
      (<any>$('#add-contact')).modal('hide');

    });
  }

  loadContactsList(count: number, pageNumber: number, critera) {
    this._service.getContactList(count, pageNumber, critera).subscribe(response => {
      console.log(response);
      if (response && response.data && response.data.length) {
        this.contactList = response.data;
      }

    });
  }

  loadContactNotes(count: number, pageNumber: number, critera) {
    this._service.getContactNotesList(count, pageNumber, critera).subscribe(response => {
      console.log(response);
      if (response && response.data && response.data.length) {
        this.contactNotesList = response.data;
      }

    });
  }

  addContactNotes(contact) {
    (<any>$('#add-notes')).modal('show');
    this.contactNoteForm.reset();
    this.contactView = contact;
    this.contactNoteForm.patchValue({ 'provider_complaint_contactsid': contact.provider_complaint_contactsid });


  }

  saveContactNotes() {
    const data = this.contactNoteForm.getRawValue();
    this._service.addContactNotes(data).subscribe(response => {
      (<any>$('#add-notes')).modal('hide');
      this.loadContactNotes(20, 1, this.searchCritera);
      console.log(response);
    });
  }

  private includeContactNoteInSummary(contactNote) {
    var includeInSummary: boolean;
    if (contactNote.include_in_summary === false) {
      includeInSummary = true;
    } else if (contactNote.include_in_summary === true) {
      includeInSummary = false;
    };

    var payload = {}
    payload['provider_complaint_notesid'] = contactNote.provider_complaint_notesid;
    payload['include_in_summary'] = includeInSummary;
    
    this._service.patchIncludeInSummary(contactNote.provider_complaint_notesid, payload).subscribe(
      (response) => {
        contactNote.include_in_summary = includeInSummary;
      },
      (error) => {
        this._alertService.error("Unable to include in summary");
      }

    );
  }
}
