import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';import { ProviderComplaintContactsRoutingModule } from './provider-complaint-contacts-routing.module';
import { ProviderComplaintContactsComponent } from './provider-complaint-contacts.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
import { ProviderComplaintContactsService } from './provider-complaint-contacts.service';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    ProviderComplaintContactsRoutingModule,
    FormMaterialModule,
    QuillModule,
    PaginationModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [ProviderComplaintContactsComponent],
  providers: [ProviderComplaintContactsService]
})
export class ProviderComplaintContactsModule { }
