import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services';
import { ProvidersUrlConfig } from '../../../providers-url.config';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { ComplaintDetail, Contact } from '../_entities/complaint-detail.model';

@Injectable()
export class ProviderComplaintContactsService {

  constructor(private _commonHttpService: CommonHttpService) { }


  patchIncludeInSummary(provider_complaint_notesid, payload) {
    return this._commonHttpService.patch(
      provider_complaint_notesid,
      payload,
      'tb_provider_complaint_notes'
    );
  }

  getContactList(pageSize: number, pageNumber: number, searchCritera: any) {
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: pageSize,
        page: pageNumber,
        method: 'get',
        where: searchCritera
      }),
      ProvidersUrlConfig.EndPoint.Complaint.CONTACT_LIST_URL
    );
  }

  getContactNotesList(pageSize: number, pageNumber: number, searchCritera: any) {
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: pageSize,
        page: pageNumber,
        method: 'get',
        where: searchCritera
      }),
      ProvidersUrlConfig.EndPoint.Complaint.CONTACT_NOTES_LIST_URL
    );
  }

  loadContactSourcesDrop() {
    return [{ key: 'Provider Administrator', value: 'Provider Administrator' },
    { key: 'CPS', value: 'CPS' },
    { key: 'Provider Staff', value: 'Provider Staff' },
    { key: 'OAG', value: 'OAG' },
    { key: 'Child', value: 'Child' },
    { key: 'DSS', value: 'DSS' },
    { key: 'DJS', value: 'DJS' },
    { key: 'MDH', value: 'MDH' },
    { key: 'other', value: 'other' }];
  }

  loadMethodOfContactsDrop() {
    return [{ key: 'Phone Call', value: 'Phone Call' },
    { key: 'Site Visit', value: 'Site Visit' },
    { key: 'Fax request for Information', value: 'Fax request for Information' },
    { key: 'Interview', value: 'Interview' },
    { key: 'E-mail request for information', value: 'E-mail request for information' },
    { key: 'Letter request for information', value: 'Letter request for information' },
    { key: 'CJAMS', value: 'CJAMS' }];
  }

  addContact(contact: Contact) {
    return this._commonHttpService.create(contact, ProvidersUrlConfig.EndPoint.Complaint.CONTACT_URL);
  }

  addContactNotes(contact: Contact) {
    return this._commonHttpService.create(contact, ProvidersUrlConfig.EndPoint.Complaint.CONTACT_NOTE_CREATE_UPDATE_URL);
  }
}
