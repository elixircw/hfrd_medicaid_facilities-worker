import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderComplaintContactsComponent } from './provider-complaint-contacts.component';

const routes: Routes = [
  {
    path : '',
    component: ProviderComplaintContactsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderComplaintContactsRoutingModule { }
