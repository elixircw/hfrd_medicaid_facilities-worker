import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderComplaintContactsComponent } from './provider-complaint-contacts.component';

describe('ProviderComplaintContactsComponent', () => {
  let component: ProviderComplaintContactsComponent;
  let fixture: ComponentFixture<ProviderComplaintContactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderComplaintContactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderComplaintContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
