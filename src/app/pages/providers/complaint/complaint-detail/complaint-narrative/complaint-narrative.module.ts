import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComplaintNarrativeRoutingModule } from './complaint-narrative-routing.module';
import { ComplaintNarrativeComponent } from './complaint-narrative.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { QuillModule } from 'ngx-quill';
import { ProviderSearchModule } from '../../../provider-search/provider-search.module';
import { RouteUsersModule } from '../../../route-users/route-users.module';
import { AppMaskDateDirective } from '../../../../../@core/directives/app-mask-date.directive';
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  imports: [
    CommonModule,
    ComplaintNarrativeRoutingModule,
    FormMaterialModule,
    QuillModule,
    ProviderSearchModule,
    RouteUsersModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [ComplaintNarrativeComponent]
})
export class ComplaintNarrativeModule { }
