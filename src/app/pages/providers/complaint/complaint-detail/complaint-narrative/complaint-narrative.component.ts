import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { AppConstants } from '../../../../../@core/common/constants';
import { CommonDropdownsService, AlertService, DataStoreService, AuthService } from '../../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ComplaintDetailsService } from '../complaint-details.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderSearchService } from '../../../provider-search/provider-search.service';
import { Subscription } from 'rxjs/Subscription';
import { RouteUserService } from '../../../route-users/route-user.service';
import { ProviderConstants } from '../../../constants';
import { ValidationService } from '../../../../../@core/services';
import { ComplaintDetail } from '../_entities/complaint-detail.model';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'complaint-nsarrative',
  templateUrl: './complaint-narrative.component.html',
  styleUrls: ['./complaint-narrative.component.scss']
})
export class ComplaintNarrativeComponent implements OnInit, OnDestroy {

  quillToolbar = AppConstants.NARRATIVE.TOOLBAR_CONFIG;
  stateDropdownItems$: any;
  Communication$: any;
  informationSources$: any;
  county$: Observable<any>;
  complaintSources = [];
  complaintDetailForm: FormGroup;
  maxDate = new Date();
  UNKONWN_SOURCE = 'Unknown';
  ANONYMOUS_SOURCE = 'Anonymous';
  isUnknownSource = false;
  isAnonymousSource: boolean;
  isSourceSpecificationNeeded = false;
  isProviderFound = false;
  isRefferal = true;
  provider: any;
  proivderSearchSubscription: Subscription;
  userAssingmentSubscription: Subscription;
  isViewOnly: Boolean = false;
  userTeam: string;
  informationSource: any[];
  constructor(private commonDropdownService: CommonDropdownsService,
    private _alertService: AlertService,
    private _compliantDetailService: ComplaintDetailsService,
    private formBuilder: FormBuilder,
    private _router: Router,
    private route: ActivatedRoute,
    private _providerSearchService: ProviderSearchService,
    private _routeUserService: RouteUserService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService
  ) {

  }

  ngOnInit() {
    this.userTeam = this._authService.getAgencyName();
    this._dataStoreService.setData('ComplaintDecisions', null);
    this.initNarrativeForm();
    this.loadDropDowns();
    this.listenOnProivderSearchChange();
    this.listenOnUserAssignment();
    this.isRefferal = this._compliantDetailService.isRefferal();
    setTimeout(() => this.loadRefferalFormValues(), 100);
    this.isViewOnly = this._dataStoreService.getData('isViewOnly');
    if (this.isViewOnly === true) {
      this.complaintDetailForm.disable();
    }

  }

  listenOnUserAssignment() {
    this.userAssingmentSubscription = this._routeUserService.selectedUserOnAssign$.subscribe(user => {
      if (user) {
        this.assingnAndSubmitReferral(user.userid, ProviderConstants.EVENTS.COMPLAINT_REFFERAL_SUBMITTED);
      }
    });
  }

  listenOnProivderSearchChange() {
    this.proivderSearchSubscription = this._providerSearchService.selectedProviderOnSearch$.subscribe(provider => {
      if (provider) {
        this.provider = provider;
        this.processProviderDisplay();
      }
    });
  }

  processProviderDisplay() {
    if (this.provider) {
      this.isProviderFound = true;
      this.complaintDetailForm.patchValue({ provider_id: this.provider.provider_id });
      this.complaintDetailForm.patchValue({ site_id: this.provider.site_id });

    } else {
      this.isProviderFound = false;
      this.complaintDetailForm.patchValue({ provider_id: null });
      this.complaintDetailForm.patchValue({ site_id: null });
    }

  }
  deleteSelectedProvider() {
    this.provider = null;
    this.processProviderDisplay();

  }

  initNarrativeForm() {
    this.complaintDetailForm = this.formBuilder.group({
      'provider_complaintid': [null],
      'intakeservreqinputtypeid': [null, Validators.required],
      'complaint_source': [null],
      'source_information_type': [null],
      'source_specification': [null],
      'complaint_date': [null, Validators.required],
      'narrative': [null, Validators.required],
      'complainant_firstname': [null],
      'complainant_lastname': [null],
      'complainant_phone': [null],
      'complainant_email': [null, [ValidationService.mailFormat]],
      'complainant_address1': [null],
      'complainant_address2': [null],
      'complainant_city': [null],
      'complainant_state': [null],
      'complainant_county': [null],
      'complainant_zipcode': [null],
      'complaint_number': [null],
      'teamtype_key': [null],
      'provider_id': [null],
      'site_id': [null],
      'is_draft': 0,
      'inserted_by': [null],
      'complaint_description': [null],
      source_type: null,
      complainant_title: null
    });

  }

  loadRefferalFormValues() {
    console.log('patch value', this._compliantDetailService.complaintDetail);
    if (this._compliantDetailService.complaintDetail) {
      const complaintDetails = this._compliantDetailService.complaintDetail;
      if (complaintDetails.source_type) {
        this.loadInformationSource(complaintDetails.source_type);
      }
      this.complaintDetailForm.patchValue(complaintDetails);
      console.log('pathced value', this.complaintDetailForm.getRawValue());
      this.decideSourceInfoNeeded();
      this.decideSourceSpecificationNeeded();
      if (complaintDetails.provider_id) {
        this.loadProviderDetails(complaintDetails.provider_id);
      }
      if (complaintDetails.complainant_state) {
        this.loadCounty(complaintDetails.complainant_state);
      }

    }

  }

  loadProviderDetails(proivderId) {
    this._providerSearchService.getProvidersList(null, proivderId, '', 1, 10, null)
    .subscribe(response => {
      if (response && response.length) {
        this.provider = response[0];
        this.processProviderDisplay();
      }
    });
  }

  loadDropDowns() {
    let complaintDetail: ComplaintDetail;
    if (this._compliantDetailService.complaintDetail) {
      complaintDetail = this._compliantDetailService.complaintDetail;
    }
    const teamtype_key = this._authService.getAgencyName();
    const teamtype = (complaintDetail && complaintDetail.teamtype_key) ? complaintDetail.teamtype_key : teamtype_key;
    Observable.forkJoin([
      this.commonDropdownService.getStateList(),
      this.commonDropdownService.getCommunicationTypes(teamtype),
      this.commonDropdownService.getDropownsByTable('Source_of_Information')
    ]).subscribe(
      ([statesList, communicationList, informationSources]) => {
        this.stateDropdownItems$ = statesList;
        this.Communication$ = communicationList;
        this.informationSources$ = informationSources;
      });
    // this.stateDropdownItems$ = this.commonDropdownService.getStateList();
    // this.Communication$ = this.commonDropdownService.getCommunicationTypes();
    // this.informationSources$ = this.commonDropdownService.getDropownsByTable('Source_of_Information');
    this.complaintSources = this.loadcomplaintSources();
  }

  onCheckSource(value) {
    if (value) {
      this.loadInformationSource(value);
    }
  }

  loadInformationSource(sourceType) {
    this.informationSource = [];
    this.commonDropdownService.getDropownsByTable('Source_of_Information').subscribe(response => {
      if (sourceType === 'internal') {
        this.informationSource = response.filter(item => {
          if (item.ref_key === 'DJS-EMP' || item.ref_key === 'DJS-EXSP') {
            return item;
          }
        });
      } else if (sourceType === 'external') {
        const interanl = ['DJS-EXSP', 'DJS-EMP'];
        this.informationSource = response.filter(item => {
          if (!interanl.includes(item.ref_key)) {
            return item;
          }
        });
      }
    });
  }



  loadCounty(state: string) {
    this.county$ = this.commonDropdownService.getCountyList(state);
  }

  loadcomplaintSources() {
    const sources = [{
      text: 'Anonymous',
      value: 'Anonymous'
    },
    {
      text: 'Unknown',
      value: 'Unknown'
    },
    {
      text: 'Known',
      value: 'Known'
    }];

    return sources;
  }

  saveAsDraft() {
    console.log(this.complaintDetailForm.getRawValue());
    const compliantDetail = this.complaintDetailForm.getRawValue();
    compliantDetail.is_draft = 0;
    this._compliantDetailService.saveComplaintDetails(compliantDetail).subscribe(response => {
      this.complaintDetailForm.patchValue({ 'provider_complaintid': response.provider_complaintid });
      console.log('cd', response);
      this._alertService.success('Complaint Referral Saved Successfully');
    });
  }

  sumbmitReferral() {
    if (this.complaintDetailForm.valid && this.isProviderFound ) {
      // && this.isProviderFound
      const decision = [];
      decision.push({eventCode: 'referral_submitted'});
      this._dataStoreService.setData('ComplaintDecisions', decision);
      console.log('valid', this.complaintDetailForm.getRawValue());
      this._router.navigate(['assign-supervisor'], { relativeTo: this.route });
    } else {
      this._alertService.error('Please fill the required feilds');
    }

  }

  assingnAndSubmitReferral(userid: string, eventCode: string) {
    const compliantDetail = this.complaintDetailForm.getRawValue();
    compliantDetail.assignsecurityuserid = [];
    compliantDetail.is_draft = 1;
    compliantDetail.assignsecurityuserid.push(userid);
    compliantDetail.eventcode = eventCode;
    this._compliantDetailService.saveComplaintDetails(compliantDetail).subscribe(response => {
      console.log('cd', response);
      this._alertService.success('Complaint Refferal Submitted Successfully', true);
      // this._compliantDetailService.sendComplaintMail(compliantDetail).subscribe(mailResponse => {
        const url = '/pages/provider/complaints/dashboard/';
        this._router.navigate([url]);
      // });
    });
  }

  decideSourceInfoNeeded() {
    const compliantDetail = this.complaintDetailForm.getRawValue();
    if (compliantDetail.complaint_source === this.UNKONWN_SOURCE) {
      this.isUnknownSource = true;
    } else {
      this.isUnknownSource = false;
    }
    if (compliantDetail.complaint_source === this.ANONYMOUS_SOURCE) {
      this.isAnonymousSource = true;
      this.complaintDetailForm.patchValue({
        // source_information_type: null,
        // source_specification: null,
        complainant_firstname:  null,
        complainant_lastname: null,
        complainant_address1: null,
        complainant_address2: null,
        complainant_city: null,
        complainant_state: null,
        complainant_county: null,
        complainant_zipcode: null,
        complainant_phone: null,
        complainant_email: null
      });
      // this.complaintDetailForm.get('source_information_type').disable();
      // this.complaintDetailForm.get('source_specification').disable();
      this.complaintDetailForm.get('complainant_firstname').disable();
      this.complaintDetailForm.get('complainant_lastname').disable();
      this.complaintDetailForm.get('complainant_address1').disable();
      this.complaintDetailForm.get('complainant_address2').disable();
      this.complaintDetailForm.get('complainant_city').disable();
      this.complaintDetailForm.get('complainant_state').disable();
      this.complaintDetailForm.get('complainant_county').disable();
      this.complaintDetailForm.get('complainant_zipcode').disable();
      this.complaintDetailForm.get('complainant_phone').disable();
      this.complaintDetailForm.get('complainant_email').disable();
    } else {
      this.isAnonymousSource = false;
      if(this.isViewOnly !== true) {
      // this.complaintDetailForm.get('source_information_type').enable();
      // this.complaintDetailForm.get('source_specification').enable();
      this.complaintDetailForm.get('complainant_firstname').enable();
      this.complaintDetailForm.get('complainant_lastname').enable();
      this.complaintDetailForm.get('complainant_address1').enable();
      this.complaintDetailForm.get('complainant_address2').enable();
      this.complaintDetailForm.get('complainant_city').enable();
      this.complaintDetailForm.get('complainant_state').enable();
      this.complaintDetailForm.get('complainant_county').enable();
      this.complaintDetailForm.get('complainant_zipcode').enable();
      this.complaintDetailForm.get('complainant_phone').enable();
      this.complaintDetailForm.get('complainant_email').enable();
      }
    }
  }

  decideSourceSpecificationNeeded() {
    const compliantDetail = this.complaintDetailForm.getRawValue();
    if (compliantDetail.source_information_type && compliantDetail.source_information_type.indexOf('-SP') !== -1) {
      this.isSourceSpecificationNeeded = true;
    } else {
      this.isSourceSpecificationNeeded = false;
    }
  }

  ngOnDestroy(): void {
    this.proivderSearchSubscription.unsubscribe();
    this.userAssingmentSubscription.unsubscribe();
  }



}
