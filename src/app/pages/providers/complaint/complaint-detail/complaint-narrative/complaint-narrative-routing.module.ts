import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintNarrativeComponent } from './complaint-narrative.component';
import { ProviderSearchComponent } from '../../../provider-search/provider-search.component';
import { RouteUsersComponent } from '../../../route-users/route-users.component';


const routes: Routes = [{
  path: '',
  component: ComplaintNarrativeComponent,
  children: [{
    path: 'provider-search',
    component: ProviderSearchComponent
  }, {
    path: 'assign-supervisor',
    component: RouteUsersComponent
  },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintNarrativeRoutingModule { }
