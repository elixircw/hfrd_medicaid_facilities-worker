import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintNarrativeComponent } from './complaint-narrative.component';

describe('ComplaintNarrativeComponent', () => {
  let component: ComplaintNarrativeComponent;
  let fixture: ComponentFixture<ComplaintNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
