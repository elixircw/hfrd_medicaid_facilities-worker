import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderDetailRoutingModule } from './provider-detail-routing.module';
import { ProviderDetailComponent } from './provider-detail.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { ProviderDetailsService } from './provider-details.service';

@NgModule({
  imports: [
    CommonModule,
    ProviderDetailRoutingModule,
    FormMaterialModule
  ],
  declarations: [ProviderDetailComponent],
  providers: [ProviderDetailsService]
})
export class ProviderDetailModule { }
