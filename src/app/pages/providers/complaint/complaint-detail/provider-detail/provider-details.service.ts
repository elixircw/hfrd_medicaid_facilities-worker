import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services';

@Injectable()
export class ProviderDetailsService {


  constructor(private _commonHttpService: CommonHttpService) { }

  private getExistingProvider(param: any, providerId: any = '', providerName: String = '') {


    return this._commonHttpService.getArrayList(
      {
        method: 'post',
        nolimit: true,
        providercategorycd: param,
        providerid: providerId,
        providername: providerName,
        filter: {}
      },
      'providerreferral/providersearch'
    );
  }
}
