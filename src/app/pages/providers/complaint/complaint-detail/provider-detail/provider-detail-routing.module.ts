import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderDetailComponent } from './provider-detail.component';

const routes: Routes = [
  {
    path: '',
    component : ProviderDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderDetailRoutingModule { }
