import { Component, OnInit } from '@angular/core';
import { ProviderAuthorizationService } from '../../../provider-authorization.service';
import { ComplaintDetailsService } from '../complaint-details.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navigation-tab',
  templateUrl: './navigation-tab.component.html',
  styleUrls: ['./navigation-tab.component.scss']
})
export class NavigationTabComponent implements OnInit {

  tabs = [];
  constructor(private _authorization: ProviderAuthorizationService,
    private complaintSerivcie: ComplaintDetailsService) {
    const status = this.complaintSerivcie.getCurrentStatus();
    this.tabs = this._authorization.getComplaintDetailsTabs(status);
  }


  ngOnInit() {
  }

}
