import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderDeficiencyViolationRoutingModule } from './provider-deficiency-violation-routing.module';
import { ProviderDeficiencyViolationComponent } from './provider-deficiency-violation.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { DeficiencyViolationService } from './deficiency-violation.service';
import { PaginationModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ProviderDeficiencyViolationRoutingModule,
    FormMaterialModule,
    PaginationModule
  ],
  declarations: [ProviderDeficiencyViolationComponent],
  providers: [DeficiencyViolationService]
})
export class ProviderDeficiencyViolationModule { }
