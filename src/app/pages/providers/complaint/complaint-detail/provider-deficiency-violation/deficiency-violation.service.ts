import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services';
import { ComplaintDetailsService } from '../complaint-details.service';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { ProvidersUrlConfig } from '../../../providers-url.config';
import { DeficiencyViolation } from '../_entities/complaint-detail.model';

@Injectable()
export class DeficiencyViolationService {

  constructor(private _commonHttpService: CommonHttpService,
    private complaintDetailsService: ComplaintDetailsService) {

  }

  getList(pageSize: number, pageNumber: number, searchCritera: any) {
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: pageSize,
        page: pageNumber,
        method: 'get',
        where: searchCritera
      }),
      ProvidersUrlConfig.EndPoint.Complaint.DEFIICENCY_URL
    );
  }

  createOrUpdate(data: DeficiencyViolation) {
    data.provider_complaintid = this.complaintDetailsService.getProviderComplaintId();
    return this._commonHttpService.create(data, ProvidersUrlConfig.EndPoint.Complaint.CREATE_UPDATE_DEFIICENCY_URL);
  }

  delete() {

  }

}
