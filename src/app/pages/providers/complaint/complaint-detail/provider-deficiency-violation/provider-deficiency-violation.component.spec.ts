import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderDeficiencyViolationComponent } from './provider-deficiency-violation.component';

describe('ProviderDeficiencyViolationComponent', () => {
  let component: ProviderDeficiencyViolationComponent;
  let fixture: ComponentFixture<ProviderDeficiencyViolationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderDeficiencyViolationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderDeficiencyViolationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
