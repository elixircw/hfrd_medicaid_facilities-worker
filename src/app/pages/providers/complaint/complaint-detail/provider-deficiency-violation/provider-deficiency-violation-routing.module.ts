import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderDeficiencyViolationComponent } from './provider-deficiency-violation.component';

const routes: Routes = [
  {
    path: '',
    component: ProviderDeficiencyViolationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderDeficiencyViolationRoutingModule { }
