import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { ComplaintDetailsService } from '../complaint-details.service';
import { DeficiencyViolationService } from './deficiency-violation.service';
import { PaginationInfo } from '../../../../../@core/entities/common.entities';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-deficiency-violation',
  templateUrl: './provider-deficiency-violation.component.html',
  styleUrls: ['./provider-deficiency-violation.component.scss']
})
export class ProviderDeficiencyViolationComponent implements OnInit {

  defciencyOrViloations = [];
  defciencyFrequency = [];
  defciencyImpacts = [];
  defciencyScopes = [];
  deficiencyForm: FormGroup;
  paginationInfo: PaginationInfo = new PaginationInfo();
  deficiencyList = [];
  providerComplaintId: string;
  isViewOnly: Boolean = false;
  constructor(private _alertService: AlertService,
    private _compliantDetailService: ComplaintDetailsService,
    private formBuilder: FormBuilder,
    private _service: DeficiencyViolationService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,) {
    this.providerComplaintId = this._compliantDetailService.getProviderComplaintId();
  }

  ngOnInit() {
    this.initDeficiencyForm();
    this.loadDropdowns();
    this.paginationInfo.sortBy = 'desc';
    this.paginationInfo.sortColumn = 'updateddate';
    this.loadList();
    this.isViewOnly = this._dataStoreService.getData('isViewOnly');

  }



  loadList() {
    const data = { provider_complaintid: this.providerComplaintId };
    this._service.getList(this.paginationInfo.pageSize,
      this.paginationInfo.pageNumber, data)
      .subscribe(response => {
        console.log(response);
        this.deficiencyList = response.data;
      });
  }

  initDeficiencyForm() {
    this.deficiencyForm = this.formBuilder.group({
      'tb_provider_complaint_deficiencyid': [null],
      'provider_complaintid': [null],
      'deficiency_violationname': [null],
      'citation': [null],
      'comments': [null],
      'frequencyofdeficiency': [null],
      'impactofdeficiency': [null],
      'scopeofdeficiency': [null],
      'findings': [null]
    });
  }

  addDeficiency() {
    this.reset();
    (<any>$('#add-deficiency')).modal('show');
  }

  editDeficiency(data) {
    this.reset();
    this.deficiencyForm.patchValue(data);
    (<any>$('#add-deficiency')).modal('show');
  }

  reset() {
    this.deficiencyForm.reset();
  }

  loadDropdowns() {
    this.loadDefciencyOrViloations();
    this.loadDefciencyFrequency();
    this.loadDefciencyImpacts();
    this.loadDefciencyScopes();

  }
  loadDefciencyScopes(): any {
    this.defciencyScopes = [{
      text: 'Isolated',
      value: 'Isolated'
    },
    {
      text: 'Narrow',
      value: 'Narrow'
    },
    {
      text: 'Moderate',
      value: 'Moderate'
    }, {
      text: 'Broad',
      value: 'Broad'
    }];
  }
  loadDefciencyImpacts(): any {
    this.defciencyImpacts = [{
      text: 'Minor',
      value: 'Minor'
    },
    {
      text: 'Major',
      value: 'Major'
    },
    {
      text: 'Serious',
      value: 'Serious'
    }];
  }
  loadDefciencyFrequency(): any {
    this.defciencyFrequency = [{
      text: 'Initial',
      value: 'Initial'
    },
    {
      text: 'Repeat',
      value: 'Repeat'
    },
    {
      text: 'Chronic',
      value: 'Chronic'
    }];
  }
  loadDefciencyOrViloations() {

    this._commonHttpService.getArrayList(
      {
          method: 'get',
          where: { referencetypeid: 999 },
          nolimit: true
      },
      'referencetype/gettypes?filter'
  ).subscribe( response => {
    this.defciencyOrViloations = response;
  });
  }

  addUpdate() {
    this._commonHttpService.getArrayList({}, 'Nextnumbers/getNextNumber?apptype=CAP').subscribe(result => {
    const data = this.deficiencyForm.getRawValue();
    data.cap_number = result['nextNumber'];
    console.log('data', data);
    if (this.deficiencyForm.invalid) {
      this._alertService.error('Please fill required fields');
      return;
    }
    this._service.createOrUpdate(data).subscribe(response => {
      console.log(response);
      this._alertService.success('Deficiency saved successfully');
      (<any>$('#add-deficiency')).modal('hide');
      this.loadList();
    });
  });
  }

  sendCAPNotification() {
    this._alertService.success('CAP Notification sends to the respective provider');
  }

}
