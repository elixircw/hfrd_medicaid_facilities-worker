import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefferalHeaderComponent } from './refferal-header.component';

describe('RefferalHeaderComponent', () => {
  let component: RefferalHeaderComponent;
  let fixture: ComponentFixture<RefferalHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefferalHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefferalHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
