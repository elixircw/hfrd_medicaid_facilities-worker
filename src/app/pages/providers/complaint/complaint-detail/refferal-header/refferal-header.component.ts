import { Component, OnInit } from '@angular/core';
import { ComplaintDetailsService } from '../complaint-details.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'refferal-header',
  templateUrl: './refferal-header.component.html',
  styleUrls: ['./refferal-header.component.scss']
})
export class RefferalHeaderComponent implements OnInit {

  complaint_number: string;
  created_by: string;
  complaintDetail: any;
  constructor(private _service: ComplaintDetailsService) {
    this.complaint_number = this._service.complaintid;
    this.complaintDetail = this._service.complaintDetail;

  }

  ngOnInit() {
  }

}
