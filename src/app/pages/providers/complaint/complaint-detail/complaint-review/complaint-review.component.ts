import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonDropdownsService, AlertService, CommonHttpService, DataStoreService } from '../../../../../@core/services';
import { ComplaintDetailsService } from '../complaint-details.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderAuthorizationService } from '../../../provider-authorization.service';
import { ComplaintReviewService } from './complaint-review.service';

@Component({
  selector: 'complaint-review',
  templateUrl: './complaint-review.component.html',
  styleUrls: ['./complaint-review.component.scss']
})
export class ComplaintReviewComponent implements OnInit {
  complaintReviewForm: FormGroup;
  methodofcontact = [];
  sourcecontact = [];
  complaintID: any;
  isViewOnly: Boolean = false;
  constructor(
    private _alertService: AlertService,
    private _compliantDetailService: ComplaintDetailsService,
    private formBuilder: FormBuilder,
    private _service: ComplaintReviewService,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.complaintReviewForm = this.formBuilder.group({
      investigationstartdate: ['', Validators.required],
      investigationenddate: ['', Validators.required],
      isphone: [null],
      isinterview: [null],
      issitevisit: [null],
      isemail_req_info: [null],
      isfax_req_info: [null],
      isletter_req_info: [null]
    });
    this.isViewOnly = this._dataStoreService.getData('isViewOnly');
    if (this.isViewOnly === true) {
      this.complaintReviewForm.disable();
    }
    this.loadDropDowns();
    this.getComplaintReviewDate();
  }
  loadDropDowns() {
    this.sourcecontact = this._service.loadContactSourcesDrop();
    this.methodofcontact = this._service.loadMethodOfContactsDrop();
  }
  saveReview() {
    const compliantDetail = this.complaintReviewForm.getRawValue();
    this.complaintID = this._compliantDetailService.getProviderComplaintId();
    this._commonHttpService.patch(
      this.complaintID,
      compliantDetail,
      'tb_provider_complaint').subscribe(
        (response) => {
            if (response) {
              console.log(response);
              this.complaintReviewForm.reset();
              this._alertService.success('Complaint review saved successfully!');
              this.getComplaintReviewDate();
            }
          });
  }

  getComplaintReviewDate() {
    this._compliantDetailService.getComplaintDetails(this._compliantDetailService.complaintid).subscribe(response => {
      if (response && response.length) {
        this.complaintReviewForm.patchValue({
          investigationstartdate: (response[0].investigationstartdate) ? response[0].investigationstartdate : null,
          investigationenddate: (response[0].investigationenddate) ? response[0].investigationenddate : null,
          isphone: (response[0].isphone) ? response[0].isphone : null,
          isinterview: (response[0].isinterview) ? response[0].isinterview : null,
          issitevisit: (response[0].issitevisit) ? response[0].issitevisit : null,
          isemail_req_info: (response[0].isemail_req_info) ? response[0].isemail_req_info : null,
          isfax_req_info: (response[0].isfax_req_info) ? response[0].isfax_req_info : null,
          isletter_req_info: (response[0].isletter_req_info) ? response[0].isletter_req_info : null,
        });
      }
    });
  }
}
