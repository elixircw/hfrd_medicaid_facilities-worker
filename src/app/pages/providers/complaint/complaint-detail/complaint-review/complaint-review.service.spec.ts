import { TestBed, inject } from '@angular/core/testing';

import { ComplaintReviewService } from './complaint-review.service';

describe('ComplaintReviewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComplaintReviewService]
    });
  });

  it('should be created', inject([ComplaintReviewService], (service: ComplaintReviewService) => {
    expect(service).toBeTruthy();
  }));
});
