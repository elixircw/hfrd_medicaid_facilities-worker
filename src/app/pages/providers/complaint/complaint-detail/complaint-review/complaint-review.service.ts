import { Injectable } from '@angular/core';

@Injectable()
export class ComplaintReviewService {

  constructor() { }
  loadContactSourcesDrop() {
    return [{ key: 'Provider Administrator', value: 'Provider Administrator' },
    { key: 'CPS', value: 'CPS' },
    { key: 'Provider Staff', value: 'Provider Staff' },
    { key: 'OAG', value: 'OAG' },
    { key: 'Child', value: 'Child' },
    { key: 'DSS', value: 'DSS' },
    { key: 'DJS', value: 'DJS' },
    { key: 'MDH', value: 'MDH' },
    { key: 'other', value: 'other' }];
  }

  loadMethodOfContactsDrop() {
    return [{ key: 'Phone Call', value: 'Phone Call' },
    { key: 'Site Visit', value: 'Site Visit' },
    { key: 'Fax request for Information', value: 'Fax request for Information' },
    { key: 'Interview', value: 'Interview' },
    { key: 'E-mail request for information', value: 'E-mail request for information' },
    { key: 'Letter request for information', value: 'Letter request for information' },
    { key: 'CJAMS', value: 'CJAMS' }];
  }
}
