import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintReviewComponent } from './complaint-review.component';

const routes: Routes = [
  {
    path: '',
    component: ComplaintReviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintReviewRoutingModule { }
