import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComplaintReviewRoutingModule } from './complaint-review-routing.module';
import { QuillModule } from 'ngx-quill';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { ComplaintReviewComponent } from './complaint-review.component';
import { NgxMaskModule } from 'ngx-mask';
import { ComplaintReviewService } from './complaint-review.service';
@NgModule({
  imports: [
    CommonModule,
    ComplaintReviewRoutingModule,
    FormMaterialModule,
    QuillModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [ComplaintReviewComponent],
  providers: [ComplaintReviewService]
})
export class ComplaintReviewModule { }
