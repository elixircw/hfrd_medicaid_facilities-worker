import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintReviewComponent } from './complaint-review.component';

describe('ComplaintReviewComponent', () => {
  let component: ComplaintReviewComponent;
  let fixture: ComponentFixture<ComplaintReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
