import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderMonitorActivityComponent } from './provider-monitor-activity.component';

describe('ProviderMonitorActivityComponent', () => {
  let component: ProviderMonitorActivityComponent;
  let fixture: ComponentFixture<ProviderMonitorActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderMonitorActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderMonitorActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
