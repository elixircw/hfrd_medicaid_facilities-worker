import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderMonitorActivityComponent } from './provider-monitor-activity.component';

const routes: Routes = [
  {
    path: '',
    component: ProviderMonitorActivityComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderMonitorActivityRoutingModule { }
