import { Component, OnInit } from '@angular/core';
import { ProviderConstants } from '../../../constants';
import { DataStoreService, CommonHttpService, AlertService } from '../../../../../@core/services';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { ActivatedRoute } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ComplaintDetailsService } from '../complaint-details.service';
import { ComplaintDetail } from '../_entities/complaint-detail.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-monitor-activity',
  templateUrl: './provider-monitor-activity.component.html',
  styleUrls: ['./provider-monitor-activity.component.scss']
})
export class ProviderMonitorActivityComponent implements OnInit {

  monthDropDown = ProviderConstants.monthDropDown;
  yearDropDown = ProviderConstants.yearDropDown;
  periodicDropDown = ProviderConstants.periodicDropDown;
  timeOfVisit = ProviderConstants.timeOfVisit;
  visitSchedule = ProviderConstants.visitSchedule;
  isViewOnly: Boolean = false;
  formGroup: FormGroup;
  activityDropdownItems$: DropdownModel[];
  defaultArray = [];
  newObj = {};
  type: any;
  category: any;
  leftTasks: any[] = [];
  rightTasks: any[] = [];
  private leftSelectedTasks = [];
  private rightSelectedTasks = [];
  applicantNumber: string;
  taskList = [];
  config = { panels: [] };
  tmpAccordionKey = [];
  activityCompleteStatus = 'Completed';
  activityTaskStatusFormGroup: FormGroup;
  tasktypestatus = [];
  isMonthDisabled:boolean = true;
  isYearDisabled:boolean = true;
  complaintDetail: ComplaintDetail;
  visitationList: any[] = [];
  visitInfo: any;

  constructor(
    private _dataStoreService: DataStoreService,
    private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private route: ActivatedRoute,
    private _complaintService: ComplaintDetailsService
  ) {
    console.log(route.snapshot);
    this.applicantNumber = route.snapshot.parent.parent.params['complaintid'];
  }

  ngOnInit() {
    this.complaintDetail = this._complaintService.complaintDetail;
    console.log('Complaint Detail :', this.complaintDetail );
    if(this.complaintDetail && this.complaintDetail.monitoractivityid) {
    this.visitInfo  =  { monitoring_id : this.complaintDetail.monitoractivityid };
    } else {
      // this.getVisit();
    }
    this.category = 'Monitoring';
    this.type = 'RCC';
    this.initializeFormGroup();
    this.initializeYearCalendar();
    this.isViewOnly = this._dataStoreService.getData('isViewOnly');
    this.loadDropDown();
    this.activityTaskStatusFormGroup = this.formBuilder.group({
      task: this.formBuilder.array([])
    });
    this.tasktypestatus = ['Completed', 'Incomplete', 'N/A'];
    this.getVisitationList(this.complaintDetail.provider_id);
    
  }

  initializeYearCalendar() {
    for (let i = 2000; i <= new Date().getFullYear(); ++i) {
      this.yearDropDown.push(i);
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.formBuilder.group({
      leftSelectedTask: [''],
      rightSelectedTask: [''],
      leftSelectedGoals: [''],
      rightSelectedGoals: [''],
      selectActivityList: [''],
      periodic: [''],
      monthly: [''],
      yearly: [''],
      schedule: [''],
      visitTime: ['']
    });
  }

  private loadDropDown() {
    this._commonHttpService.getArrayList(
      {
        method: 'post',
        where: {
          programname: this.type,
          agency: 'OLM',
          category: this.category
        }
      },
      'providerapplicant/gettasklist'
    ).subscribe(taskList => {
      console.log('Tasklist monitoring', taskList);
      console.log('Tasklist monitoring', JSON.stringify(taskList));

      taskList.forEach((x) => {
        if (x.subcategory === 'Default'){
          this.defaultArray.push(x);
        } else {
          if (!this.newObj[x.subcategory]) {
            this.newObj[x.subcategory] = [x];
          } else {
            this.newObj[x.subcategory].push(x);
          }
        }
      });
      console.log(this.newObj);
      console.log(this.defaultArray)
      let arr = [];
      if (this.newObj) {
        this.activityDropdownItems$ = Object.keys(this.newObj).map((res, index) => {
          return new DropdownModel({ text: res, value: this.newObj[res] });
        });
      }
    });
  }
  onPeriodChange() {
    this.resetDropDowns();
    if (this.formGroup.value.periodic == 'Monthly') {
      this.isMonthDisabled = false;
      this.isYearDisabled = false;
    } else {
      this.isYearDisabled = false;
    }
  }

  resetDropDowns() {
    this.isMonthDisabled = true;
    this.isYearDisabled = true;
    this.formGroup.controls['monthly'].setValue('');
    this.formGroup.controls['yearly'].setValue('');
  }

  resetForm() {
   this.formGroup.reset();
  }

  onChangeActivity(event: any) {
    console.log(this.newObj[event.source.selected._element.nativeElement.innerText.trim()]);
    this.rightTasks.length = 0;
    this.leftTasks = this.newObj[event.source.selected._element.nativeElement.innerText.trim()];
  }
  toggleTask(position: string, selectedItem: any, control: any) {
    if (position === 'left') {
      if (control.target.checked) {
        this.leftSelectedTasks.push(selectedItem);
      } else {
        const index = this.leftSelectedTasks.indexOf(selectedItem);
        this.leftSelectedTasks.splice(index, 1);
      }
    } else {
      if (control.target.checked) {
        this.rightSelectedTasks.push(selectedItem);
      } else {
        const index = this.rightSelectedTasks.indexOf(selectedItem);
        this.rightSelectedTasks.splice(index, 1);
      }
    }
  }

  selectAllTask() {
    if (this.leftTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, Object.assign([], this.leftTasks));
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  selectTask() {
    if (this.leftSelectedTasks.length) {
      this.moveTasks(this.leftTasks, this.rightTasks, this.leftSelectedTasks);
      this.clearSelectedTasks('left');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  unSelectTask() {
    if (this.rightSelectedTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, this.rightSelectedTasks);
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }
  unSelectAllTask() {
    if (this.rightTasks.length) {
      this.moveTasks(this.rightTasks, this.leftTasks, Object.assign([], this.rightTasks));
      this.clearSelectedTasks('right');
    } else {
      this._alertService.warn('No items selected.');
    }
  }

  private clearSelectedTasks(position: string) {
    if (position === 'left') {
      this.leftSelectedTasks = [];
    } else {
      this.rightSelectedTasks = [];
    }
  }

  private moveTasks(source: any[], target: any[], selectedItems: any[]) {
    selectedItems.forEach((item: any, i) => {
      const selectedIndex = source.indexOf(item);
      if (selectedIndex !== -1 /*selectedItem.length*/) {
        source.splice(selectedIndex, 1);
        target.push(item);
      }
    });
    //this._changeDetect.detectChanges();
  }

  activitySave() {
    console.log('Save ');
    // this.defaultArray = this.defaultArray.concat(this.rightTasks);
    console.log(this.rightTasks);
    console.log(this.defaultArray)
    const taskList = [];
    if (this.category === 'Monitoring') {
      this.rightTasks.forEach((element) => {
        const newTask = {
          task: element.task,
          agency: element.agency,
          programname: element.programname,
          category: element.category,
          commnts: element.description,
          subcategory: element.task,
          description: element.description
        };
        taskList.push(newTask);
      });
    }

    let requestObj = {
      "applicant_id": this.applicantNumber,
      "task": taskList
    }
    this._commonHttpService.create(requestObj, 'providerapplicant/addchecklist').subscribe(
      (response) => {
        this._alertService.success('CheckList Added successfully!');
        (<any>$('#add-activity')).modal('hide');
        this.resetForm();
        this.getTaskList();
      },
      (error) => {
        this._alertService.error('Unable to save Checklist, please try again.');
        console.log('Save Checklist Error', error);
        return false;
      }
    );
  }
  viewActivity() {
    this.getTaskList();
  }
  private getTaskList() {
    this.taskList.length = 0;
    this._commonHttpService.getPagedArrayList(
      {
          method: 'post',
          applicant_id: this.applicantNumber,
          category: 'Monitoring'
      },   
      'providerapplicant/getchecklist'
    ).subscribe(taskList => {
      this.taskList = taskList.data;
      this.setFormValues();
    });
  }

  setFormValues() {
    // this.activityTaskStatusFormGroup.setControl('task', this.formBuilder.array([]));
    // const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
    // this.taskList.forEach((x) => {
    //   control.push(this.buildTaskForm(x));
    // })
    // console.log(this.activityTaskStatusFormGroup.value);
    // console.log(this.activityTaskStatusFormGroup.controls);
    this.config.panels = [];
    this.tmpAccordionKey = [];
    let newObj = {};

    this.taskList.forEach((x) => {
      if (!newObj[x.subcategory]) {
        newObj[x.subcategory] = [x];
      } else {
        newObj[x.subcategory].push(x);
      }
    }
    );

    Object.keys(newObj).forEach((key, index) => {
      if (!this.tmpAccordionKey.includes(key)) {
        this.activityTaskStatusFormGroup.setControl('task' + index, this.formBuilder.array([]));
        this.config.panels.push({ name: key, description: this.activityCompleteStatus });
        this.tmpAccordionKey.push(key);
      }

      const control = <FormArray>this.activityTaskStatusFormGroup.controls['task' + index];

      newObj[key].forEach((x) => {
        control.push(this.buildTaskForm(x));
        if (x.status == 'INCOMPLETE' || x.status == 'Incomplete') {
          this.config.panels[index].description = 'Incomplete';
        }
      });
    });
  }
  private buildTaskForm(x): FormGroup {
    return this.formBuilder.group({
      checklist_id: x.checklist_id,
      checklist_task: x.checklist_task,
      commnts: x.commnts,
      completeddate: x.completeddate,
      status: x.status
    });
  }
  
  saveTask() {
    console.log(this.activityTaskStatusFormGroup.value);
    let objVal = Object.values(this.activityTaskStatusFormGroup.value);
    objVal.shift();
    let merged = [].concat.apply([], objVal);
    this._commonHttpService.endpointUrl = 'providerapplicant/updatechecklist';
    let requestData = {
      'applicant_id': this.applicantNumber,
      'task': merged
    };
    this._commonHttpService.create(requestData).subscribe(
      (response) => {
        if (response) {
          this._alertService.success('Task updated successfully');
          this.getTaskList();
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  getVisitationList(providerID) {
    const obj = { where: { provider_id: providerID ? providerID  : null}, method: 'get' };
    this._commonHttpService.getArrayList(obj, 'prov_monitoring/list?filter').subscribe(data => {
      this.visitationList = data && data.length ? data[0].listmonitoring : [];
     });
  }

  selectVisit(visitDetails) {
    this._dataStoreService.setData('SelectedMonitor', visitDetails);
    this.saveVisit(visitDetails);
  }

  getVisit() {
    this.visitInfo = this._dataStoreService.getData('SelectedMonitor');
  }

  saveVisit(visitDetails) {
    console.log(visitDetails);
    const complainData =  this._complaintService.complaintDetail ;
    const visitData = {provider_complaintid:complainData.provider_complaintid ,
    monitoractivityid: visitDetails.monitoring_id};
    this._commonHttpService.create(visitData, 'tb_provider_complaint/addupdate').subscribe(
      (response) => {
        if (response) {
          this._alertService.success('Monitor selected successfully');
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

}
