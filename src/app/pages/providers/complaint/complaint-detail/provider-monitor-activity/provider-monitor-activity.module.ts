import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderMonitorActivityRoutingModule } from './provider-monitor-activity-routing.module';
import { ProviderMonitorActivityComponent } from './provider-monitor-activity.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    ProviderMonitorActivityRoutingModule,
    FormMaterialModule
  ],
  declarations: [ProviderMonitorActivityComponent]
})
export class ProviderMonitorActivityModule { }
