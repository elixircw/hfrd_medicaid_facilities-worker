import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintDetailComponent } from './complaint-detail.component';
import { ComplaintDetailsResolverService } from './complaint-details-resolver.service';
const routes: Routes = [{
  path: '',
  component: ComplaintDetailComponent,
  resolve : {
    complaintDetail : ComplaintDetailsResolverService
  },
  children: [{
    path: 'refferal',
    loadChildren: './complaint-narrative/complaint-narrative.module#ComplaintNarrativeModule'
  },
  {
    path: 'provider',
    loadChildren: './provider-detail/provider-detail.module#ProviderDetailModule'
  },
  {
    path: 'decision',
    loadChildren: './complaint-decision/complaint-decision.module#ComplaintDecisionModule'
  },
  {
    path: 'contacts',
    loadChildren: './provider-complaint-contacts/provider-complaint-contacts.module#ProviderComplaintContactsModule'
  },
  {
    path: 'monitor-activity',
    loadChildren: './provider-monitor-activity/provider-monitor-activity.module#ProviderMonitorActivityModule'
  },
  {
    path: 'deficiency-violation',
    loadChildren: './provider-deficiency-violation/provider-deficiency-violation.module#ProviderDeficiencyViolationModule'
  },
  {
    path: 'summary',
    loadChildren: './provider-complaint-summary/provider-complaint-summary.module#ProviderComplaintSummaryModule'
  },
  {
    path: 'outcomes',
    loadChildren: './provider-complaint-outcomes/provider-complaint-outcomes.module#ProviderComplaintOutcomesModule'
  },
  {
    path: 'documents',
    loadChildren: './provider-complaint-documents/provider-complaint-documents.module#ProviderComplaintDocumentsModule'
  },
  {
    path: 'review',
    loadChildren: './complaint-review/complaint-review.module#ComplaintReviewModule'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintDetailRoutingModule { }
