import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderComplaintOutcomesComponent } from './provider-complaint-outcomes.component';

describe('ProviderComplaintOutcomesComponent', () => {
  let component: ProviderComplaintOutcomesComponent;
  let fixture: ComponentFixture<ProviderComplaintOutcomesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderComplaintOutcomesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderComplaintOutcomesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
