import { Component, OnInit } from '@angular/core';
import { ComplaintDetailsService } from '../complaint-details.service';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../@core/services';
import { ProvidersUrlConfig } from '../../../providers-url.config';
import { AppConstants } from '../../../../../@core/common/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-complaint-outcomes',
  templateUrl: './provider-complaint-outcomes.component.html',
  styleUrls: ['./provider-complaint-outcomes.component.scss']
})
export class ProviderComplaintOutcomesComponent implements OnInit {

  outcomes = '';
  quillToolbar = AppConstants.NARRATIVE.TOOLBAR_CONFIG;
  isViewOnly: Boolean = false;
  constructor(private _complaintDetailservice: ComplaintDetailsService,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this.loadOutcomes();
    this.isViewOnly = this._dataStoreService.getData('isViewOnly');
  }

  loadOutcomes() {
    this.outcomes = this._complaintDetailservice.getOutcomes();
  }

  saveOutcomes() {
    this._complaintDetailservice.complaintDetail.outcomes = this.outcomes;
    const data = {
      'provider_complaintid': this._complaintDetailservice.getProviderComplaintId(),
      'outcomes': this.outcomes
    };
    return this._commonHttpService.create(data, ProvidersUrlConfig.EndPoint.Complaint.CREATE_UPDATE_URL)
      .subscribe(response => {
        console.log(response);
        this._alertService.success('Outcomes saved sucessfully');
      });

  }

}
