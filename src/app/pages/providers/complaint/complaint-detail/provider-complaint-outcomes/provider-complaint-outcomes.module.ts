import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderComplaintOutcomesRoutingModule } from './provider-complaint-outcomes-routing.module';
import { ProviderComplaintOutcomesComponent } from './provider-complaint-outcomes.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    CommonModule,
    ProviderComplaintOutcomesRoutingModule,
    FormMaterialModule,
    QuillModule
  ],
  declarations: [ProviderComplaintOutcomesComponent]
})
export class ProviderComplaintOutcomesModule { }
