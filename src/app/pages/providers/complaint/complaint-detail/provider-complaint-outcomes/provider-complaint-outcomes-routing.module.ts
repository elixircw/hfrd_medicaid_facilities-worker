import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderComplaintOutcomesComponent } from './provider-complaint-outcomes.component';

const routes: Routes = [
  {
    path: '',
    component: ProviderComplaintOutcomesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderComplaintOutcomesRoutingModule { }
