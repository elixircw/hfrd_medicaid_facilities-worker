import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderComplaintDocumentsComponent } from './provider-complaint-documents.component';



const routes: Routes = [{
  path: '',
  component: ProviderComplaintDocumentsComponent,
  
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderComplaintDocumentsRoutingModule { }
