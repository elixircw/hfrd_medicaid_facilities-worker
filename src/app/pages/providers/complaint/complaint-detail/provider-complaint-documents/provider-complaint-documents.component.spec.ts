import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderComplaintDocumentsComponent } from './provider-complaint-documents.component';

describe('ProviderComplaintDocumentsComponent', () => {
  let component: ProviderComplaintDocumentsComponent;
  let fixture: ComponentFixture<ProviderComplaintDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderComplaintDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderComplaintDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
