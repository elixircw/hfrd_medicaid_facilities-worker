import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderComplaintDocumentsRoutingModule } from './provider-complaint-documents-routing.module';
import { ProviderComplaintDocumentsComponent } from './provider-complaint-documents.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { AttachmentModule } from '../../../provider-utils/attachment/attachment.module';


@NgModule({
  imports: [
    CommonModule,
    ProviderComplaintDocumentsRoutingModule,
    FormMaterialModule,
    AttachmentModule
  ],
  declarations: [ProviderComplaintDocumentsComponent]
})
export class ProviderComplaintDocumentsModule { }
