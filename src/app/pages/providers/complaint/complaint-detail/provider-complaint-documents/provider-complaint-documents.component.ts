import { Component, OnInit } from '@angular/core';
import { ComplaintDetailsService } from '../complaint-details.service';
import { AttachmentService } from '../../../provider-utils/attachment/attachment.service';
import { AttachmentConfig } from '../../../provider-utils/attachment/_entities/attachment.data.models';



@Component({
  selector: 'provider-complaint-documents',
  templateUrl: './provider-complaint-documents.component.html',
  styleUrls: ['./provider-complaint-documents.component.scss']
})
export class ProviderComplaintDocumentsComponent implements OnInit {
  complaint_number: string;
  constructor(private complaintDetailsService: ComplaintDetailsService, private attachService: AttachmentService) { 
    this.complaint_number = this.complaintDetailsService.complaintid;
    const attachmentConfig: AttachmentConfig = new AttachmentConfig();
    attachmentConfig.uniqueNumber = this.complaint_number;
    this.attachService.setAttachmentConfig(attachmentConfig);
  }

  ngOnInit() {

  }

}
