import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderComplaintSummaryRoutingModule } from './provider-complaint-summary-routing.module';
import { ProviderComplaintSummaryComponent } from './provider-complaint-summary.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { QuillModule } from 'ngx-quill';
import { ProviderComplaintSummaryService } from './provider-complaint-summary.service';
import { ProviderComplaintContactsService } from '../provider-complaint-contacts/provider-complaint-contacts.service';

@NgModule({
  imports: [
    CommonModule,
    ProviderComplaintSummaryRoutingModule,
    FormMaterialModule,
    QuillModule
  ],
  declarations: [ProviderComplaintSummaryComponent],
  providers: [ProviderComplaintSummaryService, ProviderComplaintContactsService]
})
export class ProviderComplaintSummaryModule { }
