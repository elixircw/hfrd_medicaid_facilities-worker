import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderComplaintSummaryComponent } from './provider-complaint-summary.component';

const routes: Routes = [
  {
    path: '',
    component: ProviderComplaintSummaryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderComplaintSummaryRoutingModule { }
