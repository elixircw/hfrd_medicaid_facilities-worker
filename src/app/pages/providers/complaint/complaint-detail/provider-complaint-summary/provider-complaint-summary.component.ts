import { Component, OnInit } from '@angular/core';
import { ProviderComplaintSummaryService } from './provider-complaint-summary.service';

import { AlertService, DataStoreService } from '../../../../../@core/services';
import { AppConstants } from '../../../../../@core/common/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-complaint-summary',
  templateUrl: './provider-complaint-summary.component.html',
  styleUrls: ['./provider-complaint-summary.component.scss']
})
export class ProviderComplaintSummaryComponent implements OnInit {

  summary = '';
  isViewOnly: Boolean = false;
  quillToolbar = AppConstants.NARRATIVE.TOOLBAR_CONFIG;
  constructor(private _service: ProviderComplaintSummaryService,
    private _alertService: AlertService, private _dataStoreService: DataStoreService
    ) { }

  ngOnInit() {
    this._service.getSummary().subscribe(response => {
      console.log('summary', response);
      if (response && response.length) {
        response.forEach(comment => {
          if (comment) {
            this.summary += comment;
          }

        });
      }
    });
    this.isViewOnly = this._dataStoreService.getData('isViewOnly');
  }

  saveSummary() {
    this._service.updateSummary(this.summary).subscribe(response => {
      console.log('summary saved', response);
      this._alertService.success('Summary saved successfully');
    });
  }

}
