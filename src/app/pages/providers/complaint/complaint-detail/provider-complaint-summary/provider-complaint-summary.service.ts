import { Injectable } from '@angular/core';
import { ComplaintDetailsService } from '../complaint-details.service';
import { ProviderComplaintContactsService } from '../provider-complaint-contacts/provider-complaint-contacts.service';
import { Observable } from 'rxjs/Observable';
import { ProvidersUrlConfig } from '../../../providers-url.config';
import { CommonHttpService } from '../../../../../@core/services';

@Injectable()
export class ProviderComplaintSummaryService {

  constructor(private _complaintDetailService: ComplaintDetailsService,
    private _contactService: ProviderComplaintContactsService,
    private _commonHttpService: CommonHttpService) { }

  // CJAMS to populate a Summary field with the text from all related contact notes.
  getSummary() {


    if (this._complaintDetailService.getSummary()) {
      return Observable.of([this._complaintDetailService.getSummary()]);
    } else {
      const data = { 'provider_complaintid': this._complaintDetailService.getProviderComplaintId() };
      return this._contactService.getContactNotesList(100, 1, data)
        .map(response => {
          return response.data
            .filter(note => {
              return note.include_in_summary === true;
            })
            .map(note => {
            return note.complaint_notes_note;
          });
        });
    }


  }

  updateSummary(summary) {
    this._complaintDetailService.complaintDetail.summary = summary;
    const data = {
      'provider_complaintid': this._complaintDetailService.getProviderComplaintId(),
      'summary': summary
    };
    return this._commonHttpService.create(data, ProvidersUrlConfig.EndPoint.Complaint.CREATE_UPDATE_URL);
  }







}
