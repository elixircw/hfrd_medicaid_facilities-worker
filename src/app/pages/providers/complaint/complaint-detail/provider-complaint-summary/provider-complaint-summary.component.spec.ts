import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderComplaintSummaryComponent } from './provider-complaint-summary.component';

describe('ProviderComplaintSummaryComponent', () => {
  let component: ProviderComplaintSummaryComponent;
  let fixture: ComponentFixture<ProviderComplaintSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderComplaintSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderComplaintSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
