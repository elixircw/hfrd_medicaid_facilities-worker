import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplaintDashboardRoutingModule } from './complaint-dashboard-routing.module';
import { ComplaintDashboardComponent } from './complaint-dashboard.component';
import { ComplaintDashboardService } from './complaint-dashboard.service';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ComplaintDashboardRoutingModule,
    FormMaterialModule, PaginationModule
  ],
  declarations: [ComplaintDashboardComponent],
  providers: [ComplaintDashboardService]
})
export class ComplaintDashboardModule { }
