import { Component, OnInit } from '@angular/core';
import { ComplaintService } from '../complaint.service';
import { ComplaintDashboardService } from './complaint-dashboard.service';
import { DynamicObject, PaginationInfo } from '../../../../@core/entities/common.entities';
import { Subject } from 'rxjs/Subject';
import { ProviderAuthorizationService } from '../../provider-authorization.service';
import { DataStoreService, AuthService } from '../../../../@core/services';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'complaint-dashboard',
  templateUrl: './complaint-dashboard.component.html',
  styleUrls: ['./complaint-dashboard.component.scss']
})
export class ComplaintDashboardComponent implements OnInit {
  complaintList: any = [];
  narrativeInfo: string;
  totalRecords: number;
  dynamicObject: DynamicObject = {};
  paginationInfo: PaginationInfo = new PaginationInfo();
  private searchTermStream$ = new Subject<DynamicObject>();
  private pageStream$ = new Subject<number>();
  previousPage: number;
  status: string;
  newComplaintAccess = false;
  tabs = [];
  search_complaint_number = '';
  teamType: string;
  constructor(private complaintService: ComplaintService,
    private dashboardService: ComplaintDashboardService,
    private _authorization: ProviderAuthorizationService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService) {
    this.newComplaintAccess = this._authorization.hasNewComplaintAccess();
    this.tabs = this._authorization.getComplaintDashboardTabs();
  }


  ngOnInit() {
    this.teamType = this._authService.getAgencyName();
    this.paginationInfo.sortBy = 'desc';
    this.paginationInfo.sortColumn = 'updateddate';
    this.status = this.tabs.length ? this.tabs[0].filterName : '';
    this.getPage(1, this.status);
  }

  onNewComplaint() {
    this.complaintService.newComplaint();
  }

  searchDashboard() {
    this.getPage(1, this.status);
  }

  openExistingComplaint(complaintNumber, status) {
    this.complaintService.openComplaint(complaintNumber);
    if (status === '32') {
      this._dataStoreService.setData('isViewOnly', true);
    } else {
      this._dataStoreService.setData('isViewOnly', false);
    }
  }

  showNarrative(item: any) {
    this.narrativeInfo = item.narrative ? item.narrative : 'No narrative info found!';
  }

  resetNarrative() {
    this.narrativeInfo = null;
  }
  getPage(selectPageNumber: number, status: string) {
    this.status = status;
    const pageSource = this.pageStream$.map(pageNumber => {
      if (this.paginationInfo.pageNumber !== 1) {
        this.previousPage = pageNumber;
      } else {
        this.previousPage = this.paginationInfo.pageNumber;
      }
      return { search: this.dynamicObject, page: this.previousPage };
    });
    return this.dashboardService.getComplaintList(this.paginationInfo.pageSize, this.previousPage, { status: this.status, complaint_number: this.search_complaint_number, 
      v_teamtype: this.teamType}).subscribe(result => {
      this.complaintList = result.data;
      this.totalRecords = result.count;
    });
  }
  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.previousPage = this.paginationInfo.pageNumber;
    this.getPage(this.previousPage, this.status);
  }
}
