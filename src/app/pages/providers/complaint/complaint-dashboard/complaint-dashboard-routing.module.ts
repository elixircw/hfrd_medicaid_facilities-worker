import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintDashboardComponent } from './complaint-dashboard.component';

const routes: Routes = [
  {
    path : '',
    component: ComplaintDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintDashboardRoutingModule { }
