import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services';
import { ProvidersUrlConfig } from '../../providers-url.config';
import { PaginationRequest } from '../../../../@core/entities/common.entities';

@Injectable()
export class ComplaintDashboardService {

  constructor(private _commonHttpService: CommonHttpService) { }

  getComplaintList(pageSize: number, pageNumber: number, searchCritera:any) {
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: pageSize,
        page: pageNumber,
        method: 'post',
        where: searchCritera
      }),
      ProvidersUrlConfig.EndPoint.Complaint.LIST_URL
    );
  }

}
