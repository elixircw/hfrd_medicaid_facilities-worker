import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComplaintRoutingModule } from './complaint-routing.module';
import { ComplaintComponent } from './complaint.component';
import { ComplaintService } from './complaint.service';

@NgModule({
  imports: [
    CommonModule,
    ComplaintRoutingModule
  ],
  declarations: [ComplaintComponent],
  providers: [ComplaintService]
})
export class ComplaintModule { }
