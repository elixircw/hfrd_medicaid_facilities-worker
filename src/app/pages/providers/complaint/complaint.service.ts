import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../@core/services';
import { Router } from '@angular/router';
import { ProvidersUrlConfig } from '../providers-url.config';

@Injectable()
export class ComplaintService {

  constructor(private _router: Router, private _commonHttpService: CommonHttpService) {

  }

  openComplaint(complaintNumber) {
      const url = '/pages/provider/complaints/detail/' + complaintNumber + '/refferal';
      this._router.navigate([url]);
  }

  newComplaint() {
    this._commonHttpService.getArrayList({}, ProvidersUrlConfig.EndPoint.Complaint.GetNextNumberUrl).subscribe(result => {
      const url = '/pages/provider/complaints/detail/' + result['nextNumber'] + '/refferal';
      this._router.navigate([url]);
    });
  }

}
