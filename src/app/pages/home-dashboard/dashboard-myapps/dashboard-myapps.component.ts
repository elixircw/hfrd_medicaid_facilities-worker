import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { DataStoreService, CommonHttpService, AuthService } from '../../../@core/services';
import { HomeDashboardUrlConfig } from '../home-dashbaord.url.config';
import { DSDSActionDetails } from '../../case-worker/_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { AlertService } from '../../../@core/services/alert.service';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { RoutingUser } from '../../cjams-dashboard/_entities/dashBoard-datamodel';
import { HomeDashboardService } from '../home-dashboard.service';


@Component({
  selector: 'dashboard-myapps',
  templateUrl: './dashboard-myapps.component.html',
  styleUrls: ['./dashboard-myapps.component.scss']
})
export class DashboardMyappsComponent implements OnInit {

  myApplicationList$: Observable<any>;
  @Input()
  eventSubject$: Subject<string>;
  totalRecords$: Observable<number>;
  paginationInfo: PaginationInfo = new PaginationInfo();
  getUsersList: any[];
  selectedPerson: any;
  selectedApplication: any;
  user: any;
  filterType: any;
  applicationNumber: any;
  applications: any[];
  filteredApplications: any[];
  statusDropdownItems: any[];
  canproviderapproval: boolean;
  

  //routing stuff
  //   eventCode: string;
  //   getUsersList: any[];
  //   originalUserList: any[];
  //   selectedPerson: any;
  // applicationNumber: any;




  constructor(private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonService: CommonHttpService,
    private _authService: AuthService,
    private _router: Router,
    private _dashboardService: HomeDashboardService) { }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    const resourcePermission = this._dataStoreService.getData('PERMISSION');
    this.canproviderapproval = (resourcePermission.filter(data => data.name === 'manage_provider_approval').length > 0);
    this.eventSubject$.subscribe((data) => {
      if (data === 'refresh') {
        this.getApplications(1);
      }
    });
    this.getApplications(1);
    this.loadStatusDropdown();
    this.listenForAssignment();
  }

  loadStatusDropdown() {
    this.statusDropdownItems = [
      { text: 'Pending', value: '{Pending,Rejected,Incomplete}'},
      { text: 'Accepted', value: '{Accepted}' },
      { text: 'Submitted', value: '{For Assignment,For Acceptance,For Approval}' },
      { text: 'Approved', value: '{Approved,Provisionally Approved}' }
    ];
  }

  listenForAssignment() {
    this._dashboardService.assignUserListener$.subscribe(data => {
      if (data.event === 'ASSIGNMENT') {
        this.selectedPerson = data.selectedUser;
        this.assignNewUser();
      }
    });
  }


  getApplications(pageNo: number) {
    const filterType = this.filterType ? this.filterType.value : null;
    let filterOptions;
    if (filterType) {
      filterOptions = {
        requesttype: 'dashboard',
        activeflag: 1,
        application_status: filterType 
      };
    } else {
      filterOptions = {
        requesttype: 'dashboard',
        activeflag: 1
      };
    }

    this._commonService.endpointUrl = 'publicproviderapplicant/getassignedlist'
    const body = new PaginationRequest({
      where: filterOptions,
      page: pageNo,
      nolimit: true,
      method: 'post',
      count: -1
    });
    this._commonService.getPagedArrayList(body).subscribe(result => {
      this.applications = result.data;
      this.filterApplications();
    });
    /* const source = this._commonService.getPagedArrayList(body).share();

    this.myApplicationList$. = source.pluck('data');
    if (pageNo === 1) {
      this.totalRecords$ = source.pluck('count');
    } */
  }
  filterApplications() {
    if (this.applicationNumber) {
      this.filteredApplications = this.applications.filter(item => item.applicant_id.indexOf(this.applicationNumber) >= 1);
    } else {
      this.filteredApplications = this.applications;
    }
  }

  pageChanged(pageNo: number) {
    this.getApplications(pageNo);
  }

  routToCaseWorker(item: DSDSActionDetails) {
    this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
      const dsdsActionsSummary = response[0];
      if (dsdsActionsSummary) {
        this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
        this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
        const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/report-summary';
        this._router.navigate([currentUrl]);
      }
    });
  }

  listMyApps() {
    this.getApplications(1);
  }

  // getRoutingUser(appevent, applicationNumber) {
  //   this.applicationNumber = applicationNumber;
  //   console.log("SELECTED", this.applicationNumber);
  //   this.eventCode=appevent;
  //   this.getUsersList = [];
  //   this.originalUserList = [];
  //   this._commonService
  //   .getPagedArrayList(
  //       new PaginationRequest({
  //           where: { appevent: appevent },
  //           method: 'post'
  //       }),
  //       'Intakedastagings/getroutingusers'
  //   )
  //   .subscribe(result => {
  //       this.getUsersList = result.data;
  //       this.getUsersList = this.getUsersList.filter(
  //           users => users.userrole !== 'LDSSSP'
  //       );
  //   });
  // }

  // selectPerson(row) {
  //   this.selectedPerson = row;
  // } 

  // assignNewUser() {
  //   if (this.selectedPerson){
  //     var payload ={
  //       eventcode:this.eventCode,
  //       tosecurityusersid:this.selectedPerson.userid,
  //       objectid:this.applicationNumber,
  //       typeofobj:'Application'
  //     }
  //     this._commonService.create(
  //        payload,
  //       'publicproviderapplicant/publicproviderapplicantrouting'
  //     ).subscribe(
  //       (response) => {
  //         this._alertService.success("Ownership assigned successfully!");
  //         (<any>$('#intake-caseassignnew')).modal('hide');
  //       },
  //       (error) => {
  //         this._alertService.error("Unable to save ownership");
  //       });
  //     }

  //   }
  selectPerson(row) {
    this.selectedPerson = row;
  }
  assignNewUser() {
    if (this.selectedPerson && this.selectedApplication) {
      const eventcode = this.selectedPerson.rolecode === 'LDSSHSW' ? 'PHSWS' : 'PRWS';
      var payload = {
        eventcode: eventcode,
        tosecurityusersid: this.selectedPerson.userid,
        objectid: this.selectedApplication.applicant_id,
        typeofobj: this.selectedApplication.inapplicationphase ? 'Application' : 'Pre-App'
      }
      this._commonService.create(
        payload,
        'publicproviderapplicant/publicproviderapplicantrouting'
      ).subscribe(
        (response) => {
          this._alertService.success('Assignment successful!');
          (<any>$('#application-assign')).modal('hide');
          this.sendApplicationInfo();
        },
        (error) => {
          this._alertService.error('Unable to save ownership');
        });
    } else {
      console.log('Application not selected');
    }

  }

  sendApplicationInfo() {
    this._commonService.endpointUrl = 'publicproviderapplicant';
    this._commonService
    .patch(this.selectedApplication.applicant_id, {
      application_status: 'Pending',
      application_decision: 'Pending',
      applicant_id: this.selectedApplication.applicant_id,
    })
    .subscribe((data) => { 
      this.selectedApplication = null;
      this.getApplications(1);
    });
    // this._commonService.getArrayList(
    //   {
    //     method: 'post',
    //     where: {
    //       application_status: this.selectedApplication.application_status ? this.selectedApplication.application_status : null,
    //       application_decision: this.selectedApplication.applicantion_decision ? this.selectedApplication.applicantion_decision : null,
    //       applicant_id: this.selectedApplication.applicant_id ? this.selectedApplication.applicant_id : null,
    //     }
    //   },
    //   'publicproviderapplicant'
    // ).subscribe(res => {
    //   console.log(res);
    // });
  }

  getRoutingUser(application) {
    this.selectedApplication = application;
    (<any>$('#application-assign')).modal('show');
    this.getUsersList = [];
    this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PRRWHW' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.getUsersList = result.data;
        const data = {};
        data['event'] = 'LIST_USERS';
        data['users'] = result.data.filter(
          users => users.userid !== this._authService.getCurrentUser().user.securityusersid
        );
        this._dashboardService.assignUserListener$.next(data); 
      });
  } 

  onSearchApplication() {
    console.log('appNo', this.applicationNumber);
  }

  resetFilter() {
    this.applicationNumber = null;
    this.filterApplications();
  }

}
