export class HomeDashboardUrlConfig {
    public static EndPoint = {
        slaCompliance: {
            slaComplianceURL: `/manage/team/`
        },
        teamStatistics: {
            teamStatisticsURL: `Usernotifications/getTeamCaseCountByUser`
        },
        myDsdsActions: {
            myDsdsActionsURL: `servicerequestsearches/getDetails`,
            DSDSActionDetailsUrl: 'servicerequestsearches/usersservicerequest',
            DSDSServiceDetailsUrl: 'servicerequestsearches/getservicecase',
            adoptioncaselist: 'servicerequestsearches/getadoptioncase'
        },
        myTasks: {
            myTasksURL: `Usernotifications/getActivityTasksByUser`
        },
        restoreWidget: {
            widgetURL: `Usernotifications/getCaseCountByUser`,
            taskCloseureURL: 'Usernotifications/getTasksCountByUser',
            caseworkerDashboardURL: 'Usernotifications/getcaseworkerdashboard'
        }
    };
}
