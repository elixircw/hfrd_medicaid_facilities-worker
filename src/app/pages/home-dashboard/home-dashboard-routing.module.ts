import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleGuard } from '../../@core/guard/role.guard';
import { HomeDashboardComponent } from './home-dashboard.component';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { DashboardDataResolverService } from './home-dashboard-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: HomeDashboardComponent,
    canActivate: [RoleGuard],
    // resolve: {
    //   recordData: DashboardDataResolverService
    // },
    children: [
      { path: 'home-dashboard', loadChildren: './home-dashboard.module#HomeDashboardModule' }
    ],
    data: {
      title: ['home-dashboard'],
      desc: 'Maryland department of human services',
      screen: { current: 'home-dashboard', key: 'home-dashboard', includeMenus: true, modules: [], skip: false },
      roles: ['admin', 'intakeuser', 'caseworker', 'reviewer'] 
      }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),SharedPipesModule],
  exports: [RouterModule,SharedPipesModule]
})
export class HomeDashboardRoutingModule { }
