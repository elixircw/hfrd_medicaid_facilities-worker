import { PaginationRequest } from './../../../@core/entities/common.entities';
import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { PaginationInfo } from '../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { CheckListNotificationDetails } from '../_entities/home-dash-entities';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dashboard-checklist-notification',
    templateUrl: './dashboard-checklist-notification.component.html',
    styleUrls: ['./dashboard-checklist-notification.component.scss']
})
export class DashboardChecklistNotificationComponent implements OnInit {
    paginationInfo: PaginationInfo = new PaginationInfo();
    getCheckListNotificationList$: Observable<CheckListNotificationDetails[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    constructor(private _commonService: CommonHttpService) {}

    ngOnInit() {
        this.getCheckListNotification(1);
    }
    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo;
        this.getCheckListNotification(this.paginationInfo.pageNumber);
    }
    getCheckListNotification(pageNumber: number) {
        this.paginationInfo.pageNumber = pageNumber;
        const checkList = this._commonService
            .getPagedArrayList(
                new PaginationRequest({ count: -1, where: {}, limit: this.paginationInfo.pageSize, method: 'post', page: this.paginationInfo.pageNumber }),
                'Usernotifications/getactivitytaskbyremindate'
            )
            .map((result) => {
                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.getCheckListNotificationList$ = checkList.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = checkList.pluck('count');
            this.canDisplayPager$ = checkList.pluck('canDisplayPager');
        }
    }
}
