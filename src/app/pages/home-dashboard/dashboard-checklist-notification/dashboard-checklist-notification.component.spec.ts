import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardChecklistNotificationComponent } from './dashboard-checklist-notification.component';

describe('DashboardChecklistNotificationComponent', () => {
  let component: DashboardChecklistNotificationComponent;
  let fixture: ComponentFixture<DashboardChecklistNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardChecklistNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardChecklistNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
