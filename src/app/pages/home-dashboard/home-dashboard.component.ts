import { Component, OnInit } from '@angular/core';
import { CompactType, DisplayGrid, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { Subject } from 'rxjs/Subject';

import { DashboardInputs, BroadCostMessage } from './_entities/home-dash-entities';
import { DashboardMydsdsactionsComponent } from './dashboard-mydsdsactions/dashboard-mydsdsactions.component';
import { DashboardMytasksComponent } from './dashboard-mytasks/dashboard-mytasks.component';
import { DashboardMytodoComponent } from './dashboard-mytodo/dashboard-mytodo.component';
import { DashboardTeamperformanceComponent } from './dashboard-teamperformance/dashboard-teamperformance.component';
import { DashboardTeamstatisticsComponent } from './dashboard-teamstatistics/dashboard-teamstatistics.component';
import { AuthService } from '../../@core/services/auth.service';
import { AppUser } from '../../@core/entities/authDataModel';
import { Observable } from 'rxjs/Rx';
import { CommonHttpService } from '../../@core/services/common-http.service';
import { DashboardMyirComponent } from './dashboard-myir/dashboard-myir.component';
import { DashboardMyarComponent } from './dashboard-myar/dashboard-myar.component';
import { ObjectUtils } from '../../@core/common/initializer';
import { DashboardMyappsComponent } from './dashboard-myapps/dashboard-myapps.component';
import { DashboardMyrejectedclosedappComponent } from './dashboard-myrejectedclosedapp/dashboard-myrejectedclosedapp.component';
import { DashboardMyappApprovalsComponent } from './dashboard-myappapprovals/dashboard-myappapprovals.component';
import { DashboardMyappAppealsComponent } from './dashboard-myappeals/dashboard-myappeals.component';
import { DashboardChecklistNotificationComponent } from './dashboard-checklist-notification/dashboard-checklist-notification.component';
import { HomeDashboardWidgetConfig } from './home-dashboard.widget.config';
import { DashboardMynoncpsComponent } from './dashboard-mynoncps/dashboard-mynoncps.component';
import { SessionStorageService } from '../../@core/services/storage.service';
import { CASE_STORE_CONSTANTS } from '../case-worker/_entities/caseworker.data.constants';
import { HomeDashboardService } from './home-dashboard.service';
import { AlertService } from '../../@core/services';
import { DataStoreService } from '../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { AppConstants } from '../../@core/common/constants';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'home-dashboard',
    templateUrl: './home-dashboard.component.html',
    styleUrls: ['./home-dashboard.component.scss']
})
export class HomeDashboardComponent implements OnInit {
    options: GridsterConfig;
    dashboardMain: GridsterItem[];
    remove: boolean;
    eventSubject$: Subject<string>;
    dashboardWidget: Array<GridsterItem>;
    tempName = DashboardTeamperformanceComponent;
    showtempName = true;
    component = DashboardTeamperformanceComponent;
    showBroadCostMessage: BroadCostMessage;
    activeModule;
    private token: AppUser;
    assignUsersList: any;
    selectedPerson: any;
    reassignUsersList: any;
    selectedReAssignPerson: any;
    commentInfo: any;
    constructor(
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private _session: SessionStorageService,
        private _service: HomeDashboardService,
        private _alertService: AlertService,
        private dataStore: DataStoreService,
        private route: ActivatedRoute,
       ) {
        this.token = this._authService.getCurrentUser();
    }

    static itemChange(item, itemComponent) {
        // console.log('itemChanged', item, itemComponent);
    }

    static itemResize(item, itemComponent) {
        // console.log('itemResized', item, itemComponent);
    }

    ngOnInit() {
        this.dataStore.setData(CASE_STORE_CONSTANTS.ALL_DASHBOARD_DTA_LIST, this.route.snapshot.data.recordData);
        this._session.removeItem('isServiceCase');
        this.activeModule = this._session.getItem('activeModuleNav');
        this.listenForAssignment();
        this.listenForReAssign();
        this.listenForComment();
        this.options = {
            gridType: GridType.VerticalFixed,
            compactType: CompactType.CompactLeftAndUp,
            margin: 10,
            outerMargin: true,
            outerMarginTop: null,
            outerMarginRight: null,
            outerMarginBottom: null,
            outerMarginLeft: null,
            mobileBreakpoint: 1025,
            minCols: 12,
            maxCols: 12,
            minRows: 1,
            maxRows: 4,
            maxItemCols: 100,
            minItemCols: 1,
            maxItemRows: 100,
            minItemRows: 1,
            maxItemArea: 2500,
            minItemArea: 1,
            defaultItemCols: 1,
            defaultItemRows: 1,
            fixedColWidth: 46,
            fixedRowHeight: 450,
            keepFixedHeightInMobile: false,
            keepFixedWidthInMobile: false,
            scrollSensitivity: 10,
            scrollSpeed: 20,
            enableEmptyCellClick: false,
            enableEmptyCellContextMenu: false,
            enableEmptyCellDrop: false,
            enableEmptyCellDrag: false,
            emptyCellDragMaxCols: 50,
            emptyCellDragMaxRows: 50,
            ignoreMarginInRow: true,
            draggable: {
                delayStart: 0,
                enabled: true,
                ignoreContentClass: 'gridster-item-content',
                ignoreContent: false,
                dragHandleClass: 'drag-handler'
            },
            resizable: {
                delayStart: 0,
                enabled: true,
                handles: {
                    s: true,
                    e: true,
                    n: true,
                    w: true,
                    se: true,
                    ne: true,
                    sw: true,
                    nw: true
                }
            },
            swap: true,
            pushItems: true,
            disablePushOnDrag: false,
            disablePushOnResize: false,
            pushDirections: {
                north: true,
                east: true,
                south: true,
                west: true
            },
            pushResizeItems: false,
            displayGrid: DisplayGrid.Always,
            disableWindowResize: false,
            disableWarnings: false,
            scrollToNewItems: false,
            itemChangeCallback: HomeDashboardComponent.itemChange,
            itemResizeCallback: HomeDashboardComponent.itemResize,
        };
        this.options.maxRows = (this._authService.isCW()) ? 6 : 4;
        const role = this._authService.currentUser.subscribe((x) => {
            const roleName = ObjectUtils.getNestedObject(x, ['role', 'name']);

            if (roleName !== 'superuser') {
                this.getBroadCostMessage();
            }

            const teamtypekey = ObjectUtils.getNestedObject(x, ['user', 'userprofile', 'teamtypekey']);
            console.log(teamtypekey);
            if (teamtypekey) {
                this.dashboardMain = [
                    {
                        number: 1,
                        cols: 6,
                        rows: 1,
                        y: 2,
                        x: 1,
                        component: DashboardTeamstatisticsComponent,
                        title: 'Team Statistics',
                        input: new DashboardInputs(),
                        show: true
                    },
                    {
                        number: 6,
                        cols: 12,
                        rows: 1,
                        y: 1,
                        x: 6,
                        component: DashboardMydsdsactionsComponent,
                        title: 'My Actions',
                        input: new DashboardInputs(),
                        show: true
                    },
                    {
                        number: 7,
                        cols: 5,
                        rows: 1,
                        y: 3,
                        x: 0,
                        component: DashboardMytodoComponent,
                        title: 'My To-Do',
                        input: new DashboardInputs(),
                        show: true
                    },
                    {
                        number: 8,
                        cols: 7,
                        rows: 1,
                        y: 3,
                        x: 6,
                        component: DashboardMytasksComponent,
                        title: 'My Tasks',
                        input: new DashboardInputs(),
                        show: true
                    }
                ];
                if (teamtypekey === 'AS') {
                    this.dashboardMain.push(
                        ...[
                            {
                                number: 14,
                                cols: 6,
                                rows: 1,
                                y: 1,
                                x: 3,
                                component: DashboardChecklistNotificationComponent,
                                title: 'Activity Task Reminder',
                                input: new DashboardInputs(),
                                show: true
                            }
                        ]
                    );
                }
                /* if (teamtypekey === 'CW') {
                     this.dashboardMain.push(
                         ...[
                             {
                                 number: 9,
                                 cols: 12,
                                 rows: 1,
                                 y: 0,
                                 x: 0,
                                 component: DashboardMyirComponent,
                                 title: 'My IR Intake',
                                 input: new DashboardInputs(),
                                 show: true
                             },
                             {
                                 number: 10,
                                 cols: 12,
                                 rows: 1,
                                 y: 1,
                                 x: 0,
                                 component: DashboardMyarComponent,
                                 title: 'My AR Intake',
                                 input: new DashboardInputs(),
                                 show: true
                             },
                             {
                                 number: 11,
                                 cols: 12,
                                 rows: 1,
                                 y: 2,
                                 x: 0,
                                 component: DashboardMyarComponent,
                                 title: 'My AR Intake',
                                 input: new DashboardInputs(),
                                 show: true
                             }
                         ]
                     );
                     this.dashboardMain.splice(1, 1);
                 } */

                if (teamtypekey === 'OLM') {
                    this.dashboardMain.push(
                        ...[
                            {
                                number: 12,
                                cols: 12,
                                rows: 1,
                                y: 1,
                                x: 0,
                                component: DashboardMyappApprovalsComponent,
                                title: 'My Tier Approvals',
                                input: new DashboardInputs(),
                                show: true
                            },
                            {
                                // Make these 9, 10
                                number: 13,
                                cols: 12,
                                rows: 1,
                                y: 1,
                                x: 0,
                                component: DashboardMyappsComponent,
                                title: 'My Applications',
                                input: new DashboardInputs(),
                                show: true
                            },
                            {
                                number: 13,
                                cols: 12,
                                rows: 1,
                                y: 1,
                                x: 0,
                                component: DashboardMyrejectedclosedappComponent,
                                title: 'My Jurisdiction Applications',
                                input: new DashboardInputs(),
                                show: true
                            }
                        ]
                    );
                    this.dashboardMain.splice(1, 1);
                }
            }
            if (teamtypekey === 'AS') {
                this.dashboardMain = HomeDashboardWidgetConfig.master.AS;
            }
            if (this.activeModule === 'Case Work') {
                this.dashboardMain = HomeDashboardWidgetConfig.master.CW;
            }
            if (this.activeModule === 'Provider' || this.activeModule === 'Resource Home' || this.activeModule === 'Home Study' || this.activeModule === 'Recruiter Trainer') {
                this.dashboardMain = HomeDashboardWidgetConfig.master.LDSS;
            }
            if (roleName === AppConstants.ROLES.APPEAL_USER) {
                this.dashboardMain = HomeDashboardWidgetConfig.master.APPEAL;
            }
            this.dashboardWidget = this.dashboardMain;
        });
        this._session.removeItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        this._session.removeItem(CASE_STORE_CONSTANTS.CASE_TYPE);
    }
    changedOptions() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }
    destroy() {
        this.remove = !this.remove;
    }

    removeItem(item: GridsterItem) {
        this.dashboardMain.forEach((element: GridsterItem) => {
            if (element.number === item.number) {
                element.show = false;
            }
        });
        this.dashboardWidget.splice(this.dashboardWidget.indexOf(item), 1);
    }

    refreshItem(item: GridsterItem) {
        item.input.eventSubject$.next('refresh');
    }

    addItem() {
        this.dashboardWidget.push({});
    }

    getPosition() { }
    restoreWidgets() {
        this.dashboardMain.forEach((element: GridsterItem) => {
            element.show = true;
        });
        this.dashboardWidget = Object.assign([], this.dashboardMain);
    }
    showWidget(selection: boolean, item: GridsterItem) {
        if (!selection) {
            item.show = false;
            for (let i = 0; i < this.dashboardWidget.length; i++) {
                if (item.number === this.dashboardWidget[i].number) {
                    this.dashboardWidget.splice(i, 1);
                    break;
                }
            }
        } else {
            item.show = true;
            this.dashboardWidget.push(item);
        }
    }
    getBroadCostMessage() {
        this._commonHttpService.getSingle({}, 'announcement/getuserannouncement').subscribe((result) => {
            if (result !== null) {
                (<any>$('#broadcoastmessage')).modal('show');
                this.showBroadCostMessage = result;
            }
        });
    }
    acceptAnnouncement() {
        this._commonHttpService.endpointUrl = 'announcement/acceptannouncement';
        (<any>$('#broadcoastmessage')).modal('hide');
        this._commonHttpService
            .patch(this.showBroadCostMessage.userannouncementid, {
                id: this.showBroadCostMessage.userannouncementid
            })
            .subscribe((data) => { });
    }

    listenForAssignment() {
        this._service.assignUserListener$.subscribe(data => {
            if (data.event === 'LIST_USERS') {
                this.assignUsersList = data.users;
            }
        });
    }

    selectPerson(row) {
        this.selectedPerson = row;
    }
    assignNewUser() {

        if (this.selectedPerson) {
            const data = {};
            data['event'] = 'ASSIGNMENT';
            data['selectedUser'] = this.selectedPerson;
            this._service.assignUserListener$.next(data);
        } else {
            this._alertService.error('Please Select the user');
        }
    }

    closeAssignment() {
        this.selectedPerson = null;
    }

    listenForReAssign() {
        this._service.assignSupervisorListener$.subscribe(data => {
            if (data.event === 'LIST_USERS') {
                this.reassignUsersList = data.users;
            }
        });
    }

    listenForComment() {
        this._service.assignCommentListener$.subscribe(data => {
            if (data) {
                this.commentInfo = data;
            }
        });
    }

    selectSupervisor(row) {
        this.selectedReAssignPerson = row;
    }
    assignToSupervisor() {
        if (this.selectedReAssignPerson) {
            const data = {};
            data['event'] = 'ASSIGNMENT';
            data['selectedUser'] = this.selectedReAssignPerson;
            this._service.assignSupervisorListener$.next(data);
        } else {
            this._alertService.error('Please Select the user');
        }
    }

    closeReAssignment() {
        this.selectedReAssignPerson = null;
    }
    resetComment() {
        this.commentInfo = null;
    }
}
