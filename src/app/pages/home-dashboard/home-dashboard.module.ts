import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule, MatRadioModule, MatTableModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatCard, MatCardModule, MatProgressBarModule } from '@angular/material';
import { GridsterModule } from 'angular-gridster2';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { DynamicModule } from 'ng-dynamic-component';
import { PaginationModule } from 'ngx-bootstrap';

import { DashboardClosedoverdueComponent } from './dashboard-closedoverdue/dashboard-closedoverdue.component';
import { DashboardMyappApprovalsComponent } from './dashboard-myappapprovals/dashboard-myappapprovals.component';
import { DashboardMyappAppealsComponent } from './dashboard-myappeals/dashboard-myappeals.component';
import { DashboardMyappsComponent } from './dashboard-myapps/dashboard-myapps.component';
import { DashboardMyarComponent } from './dashboard-myar/dashboard-myar.component';
import { DashboardMyclosedcasescountComponent } from './dashboard-myclosedcasescount/dashboard-myclosedcasescount.component';
import { DashboardMydsdsactionsComponent } from './dashboard-mydsdsactions/dashboard-mydsdsactions.component';
import { DashboardMyirComponent } from './dashboard-myir/dashboard-myir.component';
import { DashboardMytasksComponent } from './dashboard-mytasks/dashboard-mytasks.component';
import { DashboardMyteamoverdueComponent } from './dashboard-myteamoverdue/dashboard-myteamoverdue.component';
import { DashboardMytodoComponent } from './dashboard-mytodo/dashboard-mytodo.component';
import { DashboardRestorewidgetsComponent } from './dashboard-restorewidgets/dashboard-restorewidgets.component';
import { DashboardTeamperformanceComponent } from './dashboard-teamperformance/dashboard-teamperformance.component';
import { DashboardTeamstatisticsComponent } from './dashboard-teamstatistics/dashboard-teamstatistics.component';
import { HomeDashboardRoutingModule } from './home-dashboard-routing.module';
import { HomeDashboardComponent } from './home-dashboard.component';
import { DashboardChecklistNotificationComponent } from './dashboard-checklist-notification/dashboard-checklist-notification.component';
import { FormMaterialModule } from '../../@core/form-material.module';
import { DashboardMynoncpsComponent } from './dashboard-mynoncps/dashboard-mynoncps.component';
import { DashboardMyservicecaseComponent } from './dashboard-myservicecase/dashboard-myservicecase.component';
import { PersonInformationComponent } from './person-information/person-information.component';
import { DashboardMyadoptioncaseComponent } from './dashboard-myadoptioncase/dashboard-myadoptioncase.component';
import { DashboardLDSSMyinquiriesComponent } from './dashboard-ldss-myinquiries/dashboard-ldss-myinquiries.component';
import { DashboardMyrejectedclosedappComponent } from './dashboard-myrejectedclosedapp/dashboard-myrejectedclosedapp.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { HomeDashboardService } from './home-dashboard.service';
import { DashboardDataResolverService } from './home-dashboard-resolver.service';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { CdkTableModule } from '@angular/cdk/table';
import { ExcelService } from './excel.service';
import { DashboardMyappealcaseComponent } from './dashboard-myappealcase/dashboard-myappealcase.component';

export function highchartfactory() {
    return require('highcharts');
}

@NgModule({
    imports: [
        CommonModule,
        HomeDashboardRoutingModule,
        ChartModule,
        FormsModule,
        PaginationModule,
        MatTooltipModule,
        NgxPaginationModule,
        MatTableModule,
        ScrollDispatchModule,
        CdkTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCardModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        DynamicModule.withComponents([
            HomeDashboardComponent,
            DashboardTeamstatisticsComponent,
            DashboardMyclosedcasescountComponent,
            DashboardRestorewidgetsComponent,
            DashboardTeamperformanceComponent,
            DashboardMyteamoverdueComponent,
            DashboardClosedoverdueComponent,
            DashboardMydsdsactionsComponent,
            DashboardMytodoComponent,
            DashboardMytasksComponent,
            DashboardMyarComponent,
            DashboardMyirComponent,
            DashboardMyappsComponent,
            DashboardMyappApprovalsComponent,
            DashboardMyappAppealsComponent,
            DashboardChecklistNotificationComponent,
            DashboardMynoncpsComponent,
            DashboardMyservicecaseComponent,
            DashboardLDSSMyinquiriesComponent,
            DashboardMyrejectedclosedappComponent,
            DashboardMyappealcaseComponent
        ]),
        GridsterModule,
        FormMaterialModule,
    ],
    providers: [
        { provide: HighchartsStatic, useFactory: highchartfactory }, // add as factory to your providers
        HomeDashboardService, DashboardDataResolverService,ExcelService
    ],
    entryComponents: [
        HomeDashboardComponent,
        DashboardTeamstatisticsComponent,
        DashboardMyclosedcasescountComponent,
        DashboardRestorewidgetsComponent,
        DashboardTeamperformanceComponent,
        DashboardMyteamoverdueComponent,
        DashboardClosedoverdueComponent,
        DashboardMydsdsactionsComponent,
        DashboardMytodoComponent,
        DashboardMytasksComponent,
        DashboardMyarComponent,
        DashboardMyirComponent,
        DashboardMyappsComponent,
        DashboardMyrejectedclosedappComponent,
        DashboardMyappApprovalsComponent,
        DashboardMyappAppealsComponent,
        DashboardChecklistNotificationComponent,
        DashboardMynoncpsComponent,
        DashboardMyservicecaseComponent,
        DashboardMyadoptioncaseComponent,
        DashboardLDSSMyinquiriesComponent,
        DashboardMyappealcaseComponent
    ],

    declarations: [
        HomeDashboardComponent,
        DashboardTeamstatisticsComponent,
        DashboardMyclosedcasescountComponent,
        DashboardRestorewidgetsComponent,
        DashboardTeamperformanceComponent,
        DashboardMyteamoverdueComponent,
        DashboardClosedoverdueComponent,
        DashboardMydsdsactionsComponent,
        DashboardMytodoComponent,
        DashboardMytasksComponent,
        DashboardMyarComponent,
        DashboardMyirComponent,
        DashboardMyappsComponent,
        DashboardMyrejectedclosedappComponent,
        DashboardMyappApprovalsComponent,
        DashboardMyappAppealsComponent,
        DashboardChecklistNotificationComponent,
        DashboardMynoncpsComponent,
        DashboardMyservicecaseComponent,
        PersonInformationComponent,
        DashboardMyadoptioncaseComponent,
        DashboardLDSSMyinquiriesComponent,
        DashboardMyappealcaseComponent
    ],
    exports: [
        PersonInformationComponent
    ]
})
export class HomeDashboardModule {}
