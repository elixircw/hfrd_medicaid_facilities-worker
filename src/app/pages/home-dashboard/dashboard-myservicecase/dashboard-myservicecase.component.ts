import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AlertService } from '../../../@core/services/alert.service';
import { CommonHttpService, DataStoreService, AuthService, SessionStorageService } from '../../../@core/services';
import { Router } from '@angular/router';
import { HomeDashboardUrlConfig } from '../home-dashbaord.url.config';
import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { Observable, Subject } from 'rxjs/Rx';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { DSDSActionDetails } from '../../case-worker/_entities/caseworker.data.model';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ExcelService } from '../excel.service';
export interface ServiceCaseList {
    caseid: string;
    enddate: string;
    legalguardian: any;
    open_closed: string;
    programarea: any;
    servicecasenumber: string;
    startdate: string;
    accepteddate: string;
    isfatality: string;
    fatalityinfo: any;
    ismaltreatment: string;
}
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dashboard-myservicecase',
    templateUrl: './dashboard-myservicecase.component.html',
    styleUrls: ['./dashboard-myservicecase.component.scss', '../dashboard-common.scss']
})
export class DashboardMyservicecaseComponent implements OnInit {
    currentStatus: string;
    inputRequest: {};
    searchStrem = '';
    myCaseList$: Observable<any>;
    totalRecords$: Observable<number>;
    myserviceForm: FormGroup;
    eventSubject$: Subject<string>;
    private searchTermStream$ = new Subject<string>();
    paginationInfo: PaginationInfo = new PaginationInfo();
    agency: any;
    servicerequestnumber: string;
    myServiceCaseList: any;
    totalRecords: any;
    selection = new SelectionModel<ServiceCaseList>(true, []);
    sortData: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('input') input: ElementRef;
    export = false;

    dataSource = new MatTableDataSource<ServiceCaseList>();
    isLoading = false;
    displayedColumns: string[] = ['servicecasenumber', 'legalguardian', 'programarea', 'startdate', 'enddate', 'open_closed'];
    constructor(
        private _formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService,
        private _commonService: CommonHttpService,
        private _router: Router,
        private _authService: AuthService,
        private _session: SessionStorageService,
        private excelService: ExcelService
    ) { }

    ngOnInit() {
        this.agency = this._authService.getAgencyName();
        this.myserviceForm = this._formBuilder.group({
            filterCase: ['Open']
        });
        this.sortData = {
            active: "servicerequestnumber",
            direction: "desc"
        };
       this.getCase(1,'Open');
    }
    
    ExportTOExcel() {
        this.export =true;
        this.getCase(1,this.myserviceForm.value.filterCase);
        
    }

    customSort(event) {
        console.log('sort data custome', event);
        this.sortData = event;
        this.getCase(1, this.myserviceForm.value.filterCase);
        this.paginationInfo.pageNumber=1;
    }

    getCase(pageNo: number, status) {
        setTimeout(() => {
            this.isLoading = true;
        });
        console.log('sort data', this.sort);
        if(!this.export){
          this.dataSource.data = [];
        }
        this.currentStatus = status;
        this._commonService.endpointUrl = `${HomeDashboardUrlConfig.EndPoint.myDsdsActions.DSDSServiceDetailsUrl}?data`;
        if (status === 'Open') {
            this.inputRequest = {  actiontype: 'servicecase', status: 'Open', sort :  this.sortData };
        } else if (status === 'Closed') {
            this.inputRequest = {  actiontype: 'servicecase', status: 'Closed', sort :  this.sortData };
        } else if (status === 'All') {
           this.inputRequest = { actiontype: 'servicecase', status: null , sort :  this.sortData};
        } else {
            this.inputRequest = {  actiontype: 'servicecase', status: null , sort :  this.sortData};
         }

        if (this.searchStrem) {
            this.inputRequest['servicerequestnumber'] = this.searchStrem;
        }

        if(this.export){
            this.inputRequest['totalNumber'] = this.totalRecords;
        }
        const body = new PaginationRequest({
            where: this.inputRequest,
            page: pageNo,
            limit: 10,
            method: 'get',
            count: -1
        });

        this._commonService.getPagedArrayList(body).subscribe(data => {
            if(this.export){
                this.excelService.exportAsExcelFile(data.data, 'downloadServiceCase');
                this.export = false;
                this.isLoading = false;
            } else {
                this.myServiceCaseList = data.data;
                this.dataSource.data = this.myServiceCaseList as ServiceCaseList[];
                if (pageNo === 1) {
                    this.totalRecords = data.count;
                }
                this.isLoading = false;
            }
        });


    }
    onSearchCase() {
        this.searchStrem = this.servicerequestnumber ? this.servicerequestnumber.trim() : this.servicerequestnumber;
        this.getCase(1, 'search');
    }

    listMyCase(event) {
        const status = event.value;
        console.log(status);
        // this.myirForm.patchValue({ filterCase: status });
        this.getCase(1, status);
    }
    routToCaseWorker(item: DSDSActionDetails) {
        this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        this._session.setObj(CASE_STORE_CONSTANTS.CASE_DASHBOARD, item);
        if (item) {
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, item.objecttypekey);
        }
        this._commonService
            .getSingle(
                {
                    intakeserviceid: item.intakeserviceid,
                    isaccepted: true,
                    isrejected: false,
                    rejectreason: 'Accepted',
                    method: 'post'
                },
                'Areateammemberservicerequests/updateassignedstatus'
            )
            .subscribe(
                (result) => {
                    // this._alertService.success('Accepted Successfully');
                    // this.getCase(1, this.currentStatus);
                    this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
                        const dsdsActionsSummary = response[0];
                        if (dsdsActionsSummary) {
                            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                            // const currentUrl = '/pages/case-worker/' + item.caseid + '/' + item.servicecasenumber + '/dsds-action/involved-person';
                            // common person for cw
                            const currentUrl = '/pages/case-worker/' + item.caseid + '/' + item.servicecasenumber + '/dsds-action/person-cw';
                            this._router.navigate([currentUrl]);
                        }
                    });
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );

    }

    routToCaseWorker1(item: DSDSActionDetails) {
        this._session.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        if (item) {
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, item.objecttypekey);
        }
        const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.caseid + '/casetype';
        this._commonService.getAll(url).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
                this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                if (item.open_closed === 'Closed') {
                    const currentUrl = '/pages/case-worker/' + item.caseid + '/' + item.servicecasenumber + '/dsds-action/disposition';
                    this._router.navigate([currentUrl]);
                } else {
                    const currentUrl = '/pages/case-worker/' + item.caseid + '/' + item.servicecasenumber + '/dsds-action/report-summary';
                    this._router.navigate([currentUrl]);
                }
            }
        });
    }
    pageChanged(pageNo: number) {
        this.getCase(pageNo, this.currentStatus);
    }

    resetFilter() {
        this.searchStrem = null;
        this.servicerequestnumber = null;
        this.getCase(1, 'All');
        this.paginationInfo.pageNumber = 1;
        this.myserviceForm.patchValue({ filterCase: 'All' });
    }
}
