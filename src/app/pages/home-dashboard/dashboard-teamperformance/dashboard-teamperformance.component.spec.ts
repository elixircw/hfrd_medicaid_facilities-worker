import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTeamperformanceComponent } from './dashboard-teamperformance.component';

describe('DashboardTeamperformanceComponent', () => {
  let component: DashboardTeamperformanceComponent;
  let fixture: ComponentFixture<DashboardTeamperformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTeamperformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTeamperformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
