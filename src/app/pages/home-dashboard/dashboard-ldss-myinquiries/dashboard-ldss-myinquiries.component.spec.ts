/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardLDSSMyinquiriesComponent } from './dashboard-ldss-myinquiries.component';

describe('DashboardLDSSMyinquiriesComponent', () => {
  let component: DashboardLDSSMyinquiriesComponent;
  let fixture: ComponentFixture<DashboardLDSSMyinquiriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardLDSSMyinquiriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardLDSSMyinquiriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
