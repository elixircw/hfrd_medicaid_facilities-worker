import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/share';

import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';

import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { CommonHttpService, DataStoreService, AuthService } from '../../../@core/services';
import { AlertService } from '../../../@core/services/alert.service';
import { DSDSActionDetails } from '../../case-worker/_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { HomeDashboardUrlConfig } from '../home-dashbaord.url.config';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ExcelService } from '../excel.service';

export interface caseList {
    servicerequestnumber: string;
    classkey: string;
    maltreators: any[];
    legalguardian: any[];
    reporteddate: string;
    assigneddate: string;
    status: string;
    finding: string;
    intakeserviceid: string;

    selectedCaseforRemoval: any;

}
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dashboard-myappealcase',
    templateUrl: './dashboard-myappealcase.component.html',
    styleUrls: ['./dashboard-myappealcase.component.scss', '../dashboard-common.scss']
})
export class DashboardMyappealcaseComponent implements OnInit {
    myIRIntakeList$: Observable<any>;
    @Input()
    eventSubject$: Subject<string>;
    totalRecords$: Observable<number>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    inputRequest: {};
    currentStatus: string;
    agency: any;
    servicerequestnumber: string;
    searchStrem = '';
    private searchTermStream$ = new Subject<string>();
    myIRIntakeList: any;
    totalRecords: any;
    selection = new SelectionModel<caseList>(true, []);
    sortData: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('input') input: ElementRef;
    export = false;
    dataSource = new MatTableDataSource<caseList>();
    isLoading = false;
    selectedAppealForClosure: any;
    displayedColumns: string[] = ['servicerequestnumber', 'classkey', 'maltreators', 'legalguardian', 'reporteddate', 'assigneddate', 'action'];
    // , 'finding'

    constructor(
        private _formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService,
        private _router: Router,
        private _authService: AuthService,
        private excelService: ExcelService
    ) { }

    ngOnInit() {
        this.agency = this._authService.getAgencyName();
        this.sortData = {
            active: 'servicerequestnumber',
            direction: 'desc'
        };
        this.currentStatus = 'REVIEW';
        this.getAppeals(1);
    }

    ExportTOExcel() {
        this.export = true;
        this.getAppeals(1);

    }

    customSort(event) {
        this.sortData = event;
        this.getAppeals(1);
        this.paginationInfo.pageNumber = 1;
    }
    getAppeals(pageNo: number) {
        setTimeout(() => {
            this.isLoading = true;
        });
        this._commonHttpService.endpointUrl = `servicerequestsearches/getappeal?filter`;
        // if (status === 'inprogress') {
        //     this.inputRequest = { actiontype: 'IR', status: 'inprogress', sort: this.sortData };
        // } else if (status === 'pending') {
        //     this.inputRequest = { actiontype: 'IR', status: 'pending', sort: this.sortData };
        // } else if (status === 'closed') {
        //     this.inputRequest = { actiontype: 'IR', status: 'closed', sort: this.sortData };
        // } else if (status === 'All') {
        //     this.inputRequest = { actiontype: 'IR', sort: this.sortData };
        // } else {
        //     this.inputRequest = { actiontype: 'IR' };
        // }
        this.inputRequest = { 
            sort: this.sortData , 
            currentStatus: this.currentStatus 
        };
        if (this.searchStrem) {
            this.inputRequest['servicerequestnumber'] = this.searchStrem;
        }
        if (this.export) {
            this.inputRequest['totalNumber'] = this.totalRecords;
        }
        const body = new PaginationRequest({
            where: this.inputRequest,
            page: pageNo,
            limit: 10,
            method: 'get',
            count: -1
        });
        this._commonHttpService.getArrayList(body).subscribe(data => {
            if (this.export) {
                if (data && data.length > 0) {
                    if (data[0].getappealdashboard && data[0].getappealdashboard.length > 0) {
                        const dashboardlist = data[0].getappealdashboard.map(ele => {
                            if (ele.legalguardian && ele.legalguardian.length > 0) {
                                ele.legalguardian = ele.legalguardian.map(d => d.name).join(', ');
                            }
                            if (ele.maltreators && ele.maltreators.length > 0) {
                                ele.maltreators = ele.maltreators.map(d => d.name).join(', ');
                            }
                            return ele;
                        });
                        // console.log('Appeal Cases', dashboardlist);
                        const exportData = dashboardlist.map(data => {
                           return {
                                'CASE NUMBER': data.servicerequestnumber,
                                'TYPE': data.classkey,
                                'FOCUS':data.maltreators,
                                'HEAD OF HOUSEHOLD': data.legalguardian,
                                'INVESTIGATION CLOSED DATE': data.reporteddate,
                                'DATE ASSIGNED': data.assigneddate,
                                'STATUS': data.status
                           };
                        });
                        // console.log('Export Data', exportData);
                        this.excelService.exportAsExcelFile(exportData, 'downloadAppealCase');
                    }
                }
                this.export = false;
                this.isLoading = false;
            } else {
                if (data && data.length > 0) {
                    if (data[0].getappealdashboard && data[0].getappealdashboard.length > 0) {
                        this.myIRIntakeList = data[0].getappealdashboard.map(ele => {
                            // if (ele.legalguardian && ele.legalguardian.length > 0) {
                            //     ele.legalguardian = ele.legalguardian.map(d => d.name).join(', ');
                            // }
                            if (ele.maltreators && ele.maltreators.length > 0) {
                                ele.maltreators = ele.maltreators.map(d => d.name).join(', ');
                            }
                            return ele;
                        });
                        this.dataSource.data = this.myIRIntakeList as caseList[];
                        if (pageNo === 1 && data[0].getappealdashboard && data[0].getappealdashboard.length > 0) {
                            this.totalRecords = data[0].getappealdashboard[0].totalcount;
                        }
                    }
                }
                this.isLoading = false;
            }
        });


    }

    pageChanged(pageNo: number) {
        this.getAppeals(pageNo);
    }

    routToCaseWorker(item: DSDSActionDetails) {
        this._commonHttpService
            .getSingle(
                {
                    intakeserviceid: item.intakeserviceid,
                    isaccepted: true,
                    isrejected: false,
                    rejectreason: 'Accepted',
                    method: 'post'
                },
                'Areateammemberservicerequests/updateassignedstatus'
            )
            .subscribe(
                (result) => {
                    // this._alertService.success('Accepted Successfully');
                    // this.getAppeals(1, this.currentStatus);
                    this._commonHttpService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
                        const dsdsActionsSummary = response[0];
                        if (dsdsActionsSummary) {
                            this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                            this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                            if (item.open_closed === 'Closed') {
                                const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/disposition';
                                this._router.navigate([currentUrl]);
                            } else {
                                const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/report-summary';
                                this._router.navigate([currentUrl]);
                            }

                        }
                    });
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );

    }

    onSearchCase() {
        this.searchStrem = this.servicerequestnumber ? this.servicerequestnumber.trim() : this.servicerequestnumber;
        this.getAppeals(1);
    }

    resetFilter() {
        this.searchStrem = null;
        this.servicerequestnumber = null;
        this.getAppeals(1);
        this.paginationInfo.pageNumber = 1;
    }

    changeStatus(event) {
        this.getAppeals(1);
    }

    confirmCloseItem(item) {
        this.selectedAppealForClosure = item;
        (<any>$('#close-appeal-confirm')).modal('show');
    }

    //Closing the status for Dashboard Appeal cases 
    closeDashboardAppealCaseStatus(item){
        // const item = this.selectedAppealForClosure;
        const payload = {
            routingid: item.routingid,
            activeflag: 0,
            actiondatetime:  new Date()
        };
        // payload['routingid'] = item.routingid;
        // payload['activeflag'] = 0;
        // payload['actiondatetime'] = new Date();
    
        this._commonHttpService.patch(
        item.routingid,
          payload,
          'routing'
        ).subscribe(
          response => {
            this._alertService.success('Successfully updated status!');
            this.getAppeals(1);
            // this.selectedAppealForClosure = null;
            // (<any>$('#close-appeal-confirm')).modal('hide');
          },
          error => {
            this._alertService.error('Error in updating status!');
          }
        );
      }
}
