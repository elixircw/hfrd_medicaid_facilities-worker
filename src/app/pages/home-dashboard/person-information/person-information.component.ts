import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'person-information',
  templateUrl: './person-information.component.html',
  styleUrls: ['./person-information.component.scss']
})
export class PersonInformationComponent implements OnInit {

  @Input() persons: any;
  constructor() { }

  ngOnInit() {
    // this.persons='Reported father';
  }

  validate() {
    if (this.persons instanceof Array) {
      if (this.persons && this.persons.length) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

}
