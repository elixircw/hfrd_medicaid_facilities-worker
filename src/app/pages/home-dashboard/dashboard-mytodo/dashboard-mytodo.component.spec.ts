import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardMytodoComponent } from './dashboard-mytodo.component';

describe('DashboardMytodoComponent', () => {
  let component: DashboardMytodoComponent;
  let fixture: ComponentFixture<DashboardMytodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardMytodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardMytodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
