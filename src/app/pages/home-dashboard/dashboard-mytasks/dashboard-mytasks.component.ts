import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/share';

import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService } from '../../../@core/services';
import { HomeDashboardUrlConfig } from '../home-dashbaord.url.config';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dashboard-mytasks',
    templateUrl: './dashboard-mytasks.component.html',
    styleUrls: ['./dashboard-mytasks.component.scss']
})
export class DashboardMytasksComponent implements OnInit {

    @Input() eventSubject$: Subject<string>;

    taskList$: Observable<any>;
    totalRecord$: Observable<number>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    myirForm: FormGroup;
    constructor(private _commonService: CommonHttpService,
        private _router: Router,
        private _dataStoreService: DataStoreService, private _formBuilder: FormBuilder) { }

    ngOnInit() {
        this.myirForm = this._formBuilder.group({
            filter: ['Nex30']
        });
        this.gettaskList(1);
        this.eventSubject$.subscribe(data => {

            if (data === 'refresh') {
                this.gettaskList(1);
            }
        });
    }

    gettaskList(pageNo: number) {

        this._commonService.endpointUrl = `${HomeDashboardUrlConfig.EndPoint.myTasks.myTasksURL}`;
        const body = new PaginationRequest({
            where: {filtertype : this.myirForm.value.filter?this.myirForm.value.filter:'Nex30'},
            limit: 10,
            method: 'post',
            page: pageNo
        });

        const source = this._commonService.getPagedArrayList(body).share();
        this.taskList$ = source.pluck('data');
        if (pageNo === 1) {
            this.totalRecord$ = source.pluck('count');
        }
    }

    pageChanged(page: number) {
        this.gettaskList(page);
    }
    routToCaseWorker(item) {
        this._commonService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
                this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/report-summary';
                this._router.navigate([currentUrl]);
            }
        });
    }

}
