import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RelationshipNewService } from './collateral.new.service';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MatCardModule,  MatGridListModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { CollateralNewRoutingModule } from './collateral-new-routing.module';
import { CollateralNewComponent } from './collateral-new.component';
import { ShareFeaturesModule } from '../person-info/share-features/share-features.module';
import { NgxMaskModule } from 'ngx-mask';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
@NgModule({
  imports: [
    CommonModule,
    SharedPipesModule,
    CollateralNewRoutingModule,
    FormMaterialModule,
    MatCardModule,
    MatGridListModule,
    NgSelectModule,
    ShareFeaturesModule,
    NgxMaskModule.forRoot(),
  ],
  exports: [CollateralNewComponent],
  declarations: [CollateralNewComponent],
  providers: [RelationshipNewService]
})
export class CollateralNewModule { }
