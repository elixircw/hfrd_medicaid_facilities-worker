import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonsGridCwComponent } from './persons-grid-cw.component';

describe('PersonsGridCwComponent', () => {
  let component: PersonsGridCwComponent;
  let fixture: ComponentFixture<PersonsGridCwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonsGridCwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonsGridCwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
