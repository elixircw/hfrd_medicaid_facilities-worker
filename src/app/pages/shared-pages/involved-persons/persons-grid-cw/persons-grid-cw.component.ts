import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AlertService, AuthService, CommonDropdownsService, DataStoreService, CommonHttpService, SessionStorageService } from '../../../../@core/services';
import { IntakeConfigService } from '../../../newintake/my-newintake/intake-config.service';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import { FindIndividualService } from '../find-individual/find-individual.service';
import { InvolvedPersonsService } from '../involved-persons.service';
import { InvolvedPerson } from '../_entities/involvedperson.data.model';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';
import { PaginationRequest, DropdownModel } from '../../../../@core/entities/common.entities';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { CASE_STORE_CONSTANTS } from '../../../case-worker/_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import * as moment from 'moment';
import { AppConstants } from '../../../../@core/common/constants';


@Component({
  selector: 'persons-grid-cw',
  templateUrl: './persons-grid-cw.component.html',
  styleUrls: ['./persons-grid-cw.component.scss']
})
export class PersonsGridCwComponent implements OnInit {

  isHouseholdActive = true;
  involevedPerson$: Observable<InvolvedPerson[]>;
  involevedPerson: InvolvedPerson[];
  involvedPersonList: InvolvedPerson[]= [];

  involvedUnkPerson = [];
  agency: string;
  token: AppUser;
  reviewstatus: any;
  currentStatus: string;
  selectedIndex: any;
  selectedPerson: any;
  unkPersonFormGroup: any;
  householdroleDropdownItems$: Observable<any[]>;
  addedIdentifiedPersons: any = [];
  reporterPerson: any;
  persontodelete: any;
  /* Service case - Program Assignment */
  activepersondetails;
  addAssignmentForm: FormGroup;
  endDateForm: FormGroup;
  programArea: any;
  programSubArea: any;
  notPlacedChildList: any;
  reasonForEndList: any;
  personid: any;
  programAsssignList: any;
  minDate: Date;
  personprogramid: any;
  isAddAssignment: boolean;
  programPersonDetails: any;
  isNotPersonYouth = false;
  outhomestatus = false;
  outhomemessage = null;
  maxDate = new Date();
  currentaddressDropdownItems$: Observable<DropdownModel[]>;
  changeView = 'card';
  id: string;
  isEVPA: boolean;
  checkVPA: any;
  isrole: string[] = [];
  isServiceCase: string;
  isAdoptionCase: boolean = false;
  isCW = false;
  isValidateProgramArea: boolean;
  nonCpsCase: boolean;
  isClosed = false;
  canDelete = false;
  canSearchAdd = true;
  noSearchAddList = ['ADOPTION_CASE'];

  collateralCount = 0;
  placementList: any[];
  permanencyPlanList: any[];

  constructor(private _service: InvolvedPersonsService,
    private _router: Router,
    private route: ActivatedRoute,
    private _findIndividualService: FindIndividualService,
    private _intakeConfig: IntakeConfigService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _formBuilder: FormBuilder,
    private _commonDropdownService: CommonDropdownsService,
    private _commonHttpService: CommonHttpService,
    private storage: SessionStorageService,
  ) { }

  ngOnInit() {
    this.maxDate = new Date(this.maxDate.getTime() + (24 * 60 * 60 * 1000) * 1);
    this.loadPersons();
    this.initendDateForm();
    const unkpersons = this._dataStoreService.getData(IntakeStoreConstants.addedUnkPersons);
    this.involvedUnkPerson = (unkpersons) ? unkpersons : [];
    this.agency = this._authService.getAgencyName();
    this.token = this._authService.getCurrentUser();
    this.reviewstatus = this._dataStoreService.getData(IntakeStoreConstants.reviewstatus);
    this.currentStatus = this._dataStoreService.getData(IntakeStoreConstants.INTAKE_STATUS);
    const da_status = this.storage.getItem('da_status');
    if (da_status || this.currentStatus) {
      if (da_status === 'Closed' || da_status === 'Completed' || this.currentStatus === 'Closed' || this.currentStatus === 'Completed') {
        this.isClosed = true;
      } else {
        this.isClosed = false;
      }
    }
    this.initiateUnkFormGroup();
    this.householdroleDropdownItems$ = this._commonDropdownService.getPickListByName('cpsroles');
    this.addedIdentifiedPersons = this._dataStoreService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
    const voluntaryPlacementId = this._dataStoreService.getData('voluntryPlacementType');
    if (voluntaryPlacementId !== 'VPA') {
        this.isEVPA = false;
    } else {
        this.isEVPA = true;
    }
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.initAddAssignmentForm(); /* Service case - Program Assignment */
    this.isServiceCase = this.storage.getItem('ISSERVICECASE');
    this.isAdoptionCase = this.storage.getItem('CASE_TYPE') == "ADOPTION";
    if (this.isServiceCase) {
      this.getChildRemoval();
    }
    if (this._authService.isCW()) {
      if (this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR)) {
        this.canDelete = true;
      }
      const source = this._dataStoreService.getData(AppConstants.GLOBAL_KEY.SOURCE_PAGE);
      const intakeStatus = this._dataStoreService.getData(IntakeStoreConstants.INTAKE_STATUS);
      if (source === AppConstants.MODULE_TYPE.INTAKE) {
        if (!intakeStatus || intakeStatus === 'Draft'  || intakeStatus === 'Review') {
          this.canDelete = true;
        }
      }
      console.log('review status', this.reviewstatus);
    } else {
      this.canDelete = true;
    }

    this._intakeConfig.colletarlCount$.subscribe(count => {
      this.collateralCount = count;
    });
  }

  private getAge(dateValue) {
    if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
        const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
        return moment().diff(rCDob, 'years');
    } else {
        return '';
    }
  }

  isChild(person) {
    let isCurrentPersonisChild = false;
    if (person && person.roles) {
    const roles = person.roles;
    isCurrentPersonisChild = roles.some(item => ['CHILD', 'AV', 'OTHERCHILD'].includes(item.intakeservicerequestpersontypekey));
    }
    return isCurrentPersonisChild;
  }
  getVoluntaryList(involvedperson) {
    const Pid = involvedperson.personid;
    if (Pid.indexOf('tempid') >= 0) {
        const dob = involvedperson.dob;
        const age = this.getAge(dob);
        this.checkVPA =

        [
        {'conditionname': 'placement', 'status': 'false', 'description': 'Youth is former CINA or VPA foster child who exited care after 18'},
        {'conditionname': 'county', 'status': 'false', 'description': 'Youth must be applying in the county from which they exited'},
        {'conditionname': 'permanency', 'status': 'false', 'description': 'Youth did not exit care for: Reunification, Adoption, Guardianship, Marriage or Military'},
        {'conditionname': 'age', 'status': (age && age >= 18 && age <= 20) ? 'true' : 'false', 'description': 'Youth between 18-20 years 6 months'}
       ];
    } else {
    const countId = this._dataStoreService.getData('countyId');
    const voluntaryPlacementId = this._dataStoreService.getData('voluntryPlacementType');
    if (voluntaryPlacementId !== 'VPA') {
        return false;
    }
    this._commonHttpService
    .getArrayList(
        new PaginationRequest({
            method: 'get',
            where:  {
             county : countId,
            personid : involvedperson.personid
             },
            limit: 10,
            order: 'desc',
            page: 1,
            count: -1
        }),
        'Intakeservs/getvpadetails?filter'
    )
        .subscribe(
            (result) => {
               console.log(result);
               if (result && result.length && result[0].checkvpa) {
               this.checkVPA = result[0].checkvpa;
              //  (<any>$('#vpa-detail-popup')).modal('show');
               }
            }
        );
     //  (<any>$('#voluntary-placement')).modal('show');
    }
}

closeVoluntaryModal() {
  (<any>$('#vpa-detail-popup')).modal('hide');
}
openVoluntaryModal() {
  (<any>$('#vpa-detail-popup')).modal('show');
}


  prepareReportedCard() {
    const reporterPerson = this._dataStoreService.getData(IntakeStoreConstants.addNarrative);
    if (reporterPerson) {
      this.reporterPerson = Object.assign({}, reporterPerson);
      this.reporterPerson.firstname = reporterPerson.Firstname;
      this.reporterPerson.lastname = reporterPerson.Lastname;
      this.reporterPerson.isDel = false;
      const personList = this.involevedPerson ? this.involevedPerson.filter((item) => item.firstname === reporterPerson.Firstname && item.lastname === reporterPerson.Lastname) : [];
      if ((personList && personList.length > 0) || reporterPerson.isDel) {
        this.reporterPerson.isDel = true;
      }
      this.reporterPerson.role = 'Reporter';
      this.reporterPerson.id = new Date().getTime();
    }
  }
  loadPage() {
    this.loadPersons();
    this._dataStoreService.setData('DSDS_ACTION_UPDATE', true);
  }

getChildRemoval() {
    this._commonHttpService
      .getSingle(
        {
          where: { objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID), 'objecttypekey': 'servicecase' , isgroup: 1},
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .GetChildRemovalList + '?filter'
      ).subscribe(data => {
        if (data && data.length) {
          const list = data.filter(item => {
            if (item.childremoval && item.childremoval.length) {
              const removal = item.childremoval[0];
              return (removal.approvalstatus === 'Approved') ? true : false;
            } else {
              return false;
            }
          });
        this.placementCheck1(list);
        }
      });
}

getPlacementValidation(childId) {
  console.log('Child Id :', childId);
  return this._commonHttpService
  .getSingle(
    {
      where: {
        objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID),
        childId: childId},
      method: 'get'
    },
    CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
      .GetChildRemovalList + '?filter'
  );
}

  placementCheck1(removedChild) {
    this.permanencyPlanList = [];
    this.placementList = [];
    this.getPlacementInfoList(1, 10).subscribe(data => {
      if (data && data.data && data.data.length) {
        this.placementList = data.data;
        this.checkAbandonedChildren(removedChild);
      }
    });
    this.getPermanencyPlanList(1, 10).subscribe(data => {
      this.permanencyPlanList = data;
      this.checkAbandonedChildren(removedChild);
    });
  }

  checkAbandonedChildren(removedChild) {
    if (this.permanencyPlanList && this.placementList) {
      const placmentArray = [];
      removedChild.forEach(child => {
        const cjamsPid = child.cjamspid;
        const personName = child.personname;
        let isActivePlacement = null;
        this.placementList.forEach(placement => {
          const placements = placement.placements;
          placements.forEach(plmnt => {
            if (((child.cjamspid).toString() === placement.cjamspid) && (plmnt.routingstatus === 'Approved' || plmnt.routingstatus === 'Review') && !plmnt.enddate) {
              isActivePlacement = true;
            }
          });
        });
        const hasActivePlan = this.permanencyPlanList.some(item => {
          if (item.cjamspid === cjamsPid.toString()) {
            const list = item.permanencyplans;
            const valid = list.some(ele => ele.enddate === null);
            return valid;
          } else {
            return false;
          }
        });
        const childPlacement = {
          'cjamsPid': cjamsPid, 'personName': personName,
          'isActivePlacement': (!isActivePlacement && !hasActivePlan)
        };
        placmentArray.push(childPlacement);
      });
      this._dataStoreService.setData('placment_check', placmentArray);
      this.notPlacedChildList = placmentArray.filter(placment => placment.isActivePlacement);
    }
  }

placementCheck(removedChild) {
  this.getPlacementInfoList(1, 10).subscribe(data => {
      const placmentArray = [];
      removedChild.forEach(child => {
        this.getPlacementValidation(child.cjamspid).subscribe(
          dat => {
            console.log(dat);
        });
        const cjamsPid = child.cjamspid;
        const personName = child.personname;
        let isActivePlacement = null;
        if (data && data.data && data.data.length) {
            const placementList = data.data;
            placementList.forEach(placement => {
              const placements = placement.placements;
                placements.forEach(plmnt => {
                  if ( ( (child.cjamspid).toString() ===  placement.cjamspid ) && (plmnt.routingstatus === 'Approved' || plmnt.routingstatus === 'Review')  && !plmnt.enddate) {
                      isActivePlacement = true;
                  }
                });
           });
        }
        const childPlacement = { 'cjamsPid': cjamsPid, 'personName': personName, 'isActivePlacement': isActivePlacement};
         placmentArray.push(childPlacement);

      });
      this._dataStoreService.setData('placment_check', placmentArray);
      this.notPlacedChildList = placmentArray.filter(placment => !placment.isActivePlacement);
      // if(this.notPlacedChildList && this.notPlacedChildList.length) {
      //   (<any>$('#legal-guardian-role')).modal('show');
      // }
  });
}

checkIfPersonExpunged(person) {
  let ifExpunged = false;
  if (person && person.roles && person.roles.length) {
    person.roles.forEach(role => {
      if (role.spexpungementflag && person.roles.length === 1) {
        ifExpunged = true;
      } else {
        ifExpunged = false;
      }
    });
  }
  return ifExpunged;
}
  checkIfNoPlacement(child) {
    let isNoPlacement;
    if (this.notPlacedChildList && this.notPlacedChildList.length) {
      this.notPlacedChildList.forEach(ch => {
        if ((ch.cjamsPid).toString() === child.cjamspid) {
          if (ch.isActivePlacement) {
            isNoPlacement = 'true';
          }
        }
      });

    }
    return isNoPlacement;
  }

getPlacementInfoList(pageNumber, limit) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: limit,
          method: 'get',
          where: { servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
        }),
        'placement/getplacementbyservicecase?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );
}

getPermanencyPlanList(pageNumber, limit) {
  return this._commonHttpService
    .getArrayList(
      new PaginationRequest({
        page: pageNumber,
        limit: limit,
        method: 'get',
        where: { objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
      }),
      'permanencyplan/list?filter'
    );


}

  loadPersons() {
    this.involevedPerson$ = this._service.getInvolvedPerson(1, 20)
      .share()
      .pluck('data');
    this.involevedPerson$.subscribe(response => {
      if (response && response && Array.isArray(response)) {
        const narrative = this._dataStoreService.getData(IntakeStoreConstants.addNarrative);
        const incidentDate = (narrative) ? narrative.incidentdate : null;
        this.involevedPerson = response;
        this.involevedPerson.map(person => {
          const workEnv = config.workEnvironment;
          if (person.userphoto) {
            if (workEnv === 'state') {
              person.userphoto = AppConfig.baseUrl + '/attachment/v1' + person.userphoto;
            }
          }
          const roles = (Array.isArray(person.roles)) ? person.roles : [];
          const isVictim = roles.some(item => ['CHILD', 'AV'].includes(item.intakeservicerequestpersontypekey));
          person['ageatincident'] = (isVictim) ? this.getAgeAtIncident(incidentDate, person.dob) : null;
          person['isVictim'] = isVictim;
          if (person['programarea'] && person['programarea'].length) {
           const data = person['programarea'].map((item) => {
              return item.programname;
            });
            person['programnames'] = data.join(' / ');
          }
        });
        this.involvedPersonList = response;
        // this.callEditAndSaveOnAllPersons()
        if (this.involvedPersonList
          && this.involvedPersonList.length) {
            this._service.updateInProviderInfo();
          const addedPersons = this.involvedPersonList.map(person => {
              return this._intakeConfig.mapOldJsonData(person);
          });
          this._dataStoreService.setData(
            IntakeStoreConstants.addedPersons,
            addedPersons
        );
        this.involvedPersonList.map((item) => {
          if (item.roles && item.roles.length) {
              return item.roles.map((res) => {
                  // tslint:disable-next-line:max-line-length
                  if (res.intakeservicerequestpersontypekey === 'RC' || res.intakeservicerequestpersontypekey === 'LG' || res.intakeservicerequestpersontypekey === 'CHILD' || res.intakeservicerequestpersontypekey === 'AV' || res.intakeservicerequestpersontypekey === 'BIOCHILD') {
                      this.isrole[item.personid] = res.intakeservicerequestpersontypekey;
                  } else {
                      this.isrole[item.personid] = '';
                  }
              });
          }
         });
         this.filterHouseHold();
      }
      }
      this.prepareReportedCard();
      console.log(this.involevedPerson);
    });
  }

  getAgeAtIncident(incidentDate, persondob) {
    if (!incidentDate || !persondob) {
      return 'N/A';
    }
    const date2 = new Date(incidentDate);
    const date1 = new Date(persondob);
    let diff = (date2.getTime() - date1.getTime()) / 1000;
    diff /= (60 * 60 * 24);
    return Math.abs(Math.round(diff / 365.25)) + ' Yrs';
  }

  searchIdentified(person) {
    this._dataStoreService.setData(IntakeStoreConstants.PERSON_TO_SEARCH, person);
    // this._router.navigate(['../person'], { relativeTo: this.route });
    this._router.navigate(['pages/newintake/my-newintake/person-cw/find-individual/search']);
  }

  deleteIdentified(person, action) {
    if (action === -1) {
      this.persontodelete = person;
      (<any>$('#delete-unkperson-popup')).modal('show');
    } else if (action === 0) {
      this.persontodelete = null;
    } else if (action === 1) {
      // let  persons = this._dataStoreService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
      if (this.persontodelete.role === 'Reporter') {
        const reporterPerson = this._dataStoreService.getData(IntakeStoreConstants.addNarrative);
        reporterPerson.isAdded = true;
        reporterPerson.isDel = true;
        this.reporterPerson.isAdded = true;
        this.reporterPerson.isDel = true;
        this._dataStoreService.setData(IntakeStoreConstants.addNarrative, reporterPerson);
      } else {
        this.addedIdentifiedPersons = this.addedIdentifiedPersons.filter(item => this.persontodelete.id !== item.id);
        this._dataStoreService.setData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS, this.addedIdentifiedPersons);
        this.addedIdentifiedPersons = this._dataStoreService.getData(IntakeStoreConstants.ADDED_IDENTIFIED_PERSONS);
      }
      (<any>$('#delete-unkperson-popup')).modal('hide');
    }
  }

  filterHouseHold() {
    this.isHouseholdActive = true;
    this.involevedPerson$ = Observable.of(this.involvedPersonList.filter(person => person.ishousehold === 1));
    this.involevedPerson = this.involvedPersonList.filter(person => person.ishousehold === 1);
  }

  filterOtherPerson() {
    this.isHouseholdActive = false;
    this.involevedPerson$ = Observable.of(this.involvedPersonList.filter(person => person.ishousehold !== 1));
    this.involevedPerson = this.involvedPersonList.filter(person => person.ishousehold !== 1);
  }

  filterHouseHoldCount() {
    if (this.involvedPersonList === undefined) { return 0; }
    // return this.involvedPersonList.filter(person => person.ishousehold === 1).length;
    const household =  this.involvedPersonList.filter(person => person.ishousehold === 1);
    if (household && household.length) {
      let noOfHouseHold = 0;
      household.forEach(person => {
        noOfHouseHold = !this.checkIfPersonExpunged(person) ? noOfHouseHold + 1 : noOfHouseHold;
      });
      return noOfHouseHold;
    } else {
      return 0;
    }
  }

  filterOtherPersonCount() {
    if (this.involvedPersonList === undefined) { return 0; }
    // return this.involvedPersonList.filter(person => person.ishousehold !== 1).length;
    const others =  this.involvedPersonList.filter(person => person.ishousehold !== 1);
    if (others && others.length) {
      let noOfOthers = 0;
      others.forEach(person => {
        noOfOthers = !this.checkIfPersonExpunged(person) ? noOfOthers + 1 : noOfOthers;
      });
      return noOfOthers;
    } else {
      return 0;
    }
  }

  openFindIndividual() {
    this._findIndividualService.resetSearchCriteria();
    this._dataStoreService.setData(IntakeStoreConstants.PERSON_TO_SEARCH, null);
    this._router.navigate(['../find-individual/search'], { relativeTo: this.route });
  }

  editPerson(person) {
    this._service.editPerson(person.personid);
  }

  // callEditAndSaveOnAllPersons() {
  //   this.involevedPerson.forEach( person =>
  //     console.log(person.personid)
  //   );
  // }

  openAddUnkModal() {
    (<any>$('#intake-unkperson')).modal('show');
  }

  initiateUnkFormGroup() {
    this.unkPersonFormGroup = this._formBuilder.group({
      role: ['', Validators.required],
      comment: ['']
    });
  }

  initendDateForm() {

    this.endDateForm = this._formBuilder.group({
      endDate: ['']
    });
  }

  confirmDelete(index, person) {
    this.selectedIndex = index;
    this.selectedPerson = person;
    (<any>$('#confirm-delete-person')).modal('show');
    return;

  }

  resetSelectedPerson() {
    this.selectedIndex = -1;
    this.selectedPerson = null;
  }
  confirmPersonDelete() {
    const index = this.selectedIndex;
    const person = this.selectedPerson;
    this.involvedPersonList.splice(index, 1);
    this.involevedPerson.splice(index, 1);
    this.involevedPerson$ = Observable.of(this.involevedPerson);
    this._service.removeInvolvedPersons(person).subscribe(data => {
      console.log(data);
      this.loadPersons();
    });
    let addedPersons = [];
    if (this.involvedPersonList
      && this.involvedPersonList.length) {
       addedPersons = this.involvedPersonList.map(item => {
          return this._intakeConfig.mapOldJsonData(item);
      });
      }
      this._dataStoreService.setData(
        IntakeStoreConstants.addedPersons,
        addedPersons
    );
    this.resetSelectedPerson();
  }

  addUnkPerson() {
    const person = this.unkPersonFormGroup.getRawValue();
    person['id'] = new Date().getTime();
    person['isNew'] = true;
    this.involvedUnkPerson.push(person);
    this._dataStoreService.setData(IntakeStoreConstants.addedUnkPersons, this.involvedUnkPerson);
    this.unkPersonFormGroup.reset();
    (<any>$('#intake-unkperson')).modal('hide');
  }
  deleteUnkPerson(person) {
    this.involvedUnkPerson = this.involvedUnkPerson.filter(item => item.id !== person.id);
    this._dataStoreService.setData(IntakeStoreConstants.addedUnkPersons, this.involvedUnkPerson);
    this._alertService.success('Person deleted successfully.');
  }

  /* Service case - Program Assignment */

  checkPreviousProgramAssignmentEnded() {
    if (this.programAsssignList.personprogramarea && this.programAsssignList.personprogramarea.length) {
      const endDate = this.programAsssignList.personprogramarea.filter((item) => item.enddate === null);
      if (endDate.length) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  loadProgramAssignForm() {
    this.outhomestatus = false;
    this.outhomemessage = null;
    this.isAddAssignment = true;
    this.addAssignmentForm.reset();
    this.loadProgramAreaDropdowns(null);
    this.loadEndReasonDropDown();
    if (this.checkPreviousProgramAssignmentEnded()) {
      (<any>$('#add-program-assignment')).modal('show');
    } else {
      (<any>$('#end-previous-prog-assignment-alert')).modal('show');
    }
}

initAddAssignmentForm() {
    this.addAssignmentForm = this._formBuilder.group(
        {
            programkey: ['', Validators.required],
            subprogramkey: [''],
            ifpsatriskflag: [''],
            startdate: ['', Validators.required],
            enddate: [''],
            endreasonkey: ['']
        }
    );
}

loadEndReasonDropDown() {
    this._commonHttpService
        .getPagedArrayList(
            new PaginationRequest({
                where: { referencetypeid: 357, teamtypekey: 'CW' },
                method: 'get',
                nolimit: true
            }),
            'referencetype/gettypes?filter'
        )
        .subscribe((result) => {
            console.log(result);
            if (result && Array.isArray(result) && result.length) {
                this.reasonForEndList = result;
            }
        });
}

viewChanged() {
    this._commonHttpService.create({
        viewpreference: this.changeView
    }, NewUrlConfig.EndPoint.Intake.ViewPreference).subscribe(res => console.log(this._authService.getCurrentUser().user.userprofile.viewpreference));
}

getPersonData(person, caseType: string) {
    this.nonCpsCase = caseType === 'nonCPS';
    this.activepersondetails = person;
    this.personid = person.personid;
    this.getProgramAssignmentList();
    if (person.roles && person.roles.length) {
      const data = person.roles.filter((item) => item.intakeservicerequestpersontypekey === 'AV' || item.intakeservicerequestpersontypekey === 'CHILD');
      this.isValidateProgramArea = data.length > 0;
    }
}

getProgramAssignmentList() {
    this._commonHttpService
        .getPagedArrayList(
            new PaginationRequest({
                where: { objectid: this.id, personid: this.personid },
                method: 'get',
                nolimit: true
            }),
            'Personprogramareas/getpersonprogramarea?filter'
        )
        .subscribe((result) => {
            if (result && Array.isArray(result) && result.length) {
                this.programAsssignList = result[0];
                this.programPersonDetails = result[0].persondetails;
            }
        });
}

updateEndDate(date: string, startdate: boolean) {
  if (startdate) {
    this.minDate = new Date(date);
  }
  const data =  this._dataStoreService.getData('dsdsActionsSummary');
  const dateDiff =  moment(date).diff(moment(data.case_opendate), 'days');
  let enddateDiff = 0;
  if (data.case_closedate) {
    enddateDiff  =  moment(date).diff(moment(data.case_closedate), 'days');
  }
  if (startdate) {
    if (!(dateDiff >= 0)) {
      this._alertService.error('Program date should be greater than case open date');
      this.addAssignmentForm.controls['startdate'].reset();
      this.minDate = null;
    }
  } else {
    if (!(dateDiff >= 0) || !(enddateDiff <= 0)) {
      this._alertService.error('Program date should be greater than case open date');
      this.addAssignmentForm.controls['enddate'].reset();
    }
  }
}

editProgramAssigment(person) {
    this.outhomestatus = false;
    this.outhomemessage = null;
    this.loadProgramAreaDropdowns(null); // person.programkey
    this.loadEndReasonDropDown();
    this.checkaccesstoproceed(null, 'checkDue', person);
    this.isAddAssignment = false;
    this.minDate = new Date(person.startdate);
    this.personprogramid = person.personprogramid;
    this.addAssignmentForm.patchValue(
        {
            startdate: person.startdate,
            enddate: person.enddate,
            programkey: person.programkey,
            subprogramkey: person.subprogramkey,
            endreasonkey: person.endreasonkey,
            ifpsatriskflag: (person.ifpsatriskflag) ? 'Yes' : 'No'
        }
    );
}

checkprogramAssignmentEditble(programKey) {
  const programAreaArray = ['ADP', 'IS', 'OOH'];
  const exists = programAreaArray.includes(programKey);
  return !exists;
}

checkAge() {
  const dob = moment(this.programPersonDetails.dob);
  const age = moment().diff(dob, 'years', true);
  if (age < 18) {
      return true;
  }
  if (age > 21) {
    return true;
  }
  return false;
}

saveEndDate() {
  const model = {
    personid: this.personid,
    enddate: this.endDateForm.value.endDate
  };
  this._commonHttpService
    .create(model,
        'Personprogramareas/updateEndDate'
    )
    .subscribe((result) => {
        (<any>$('#add-program-assignment')).modal('show');
        this._alertService.success('Program assignment Ended successfully!');
    });
}

saveProgramAssignment() {

   if (this.checkAge() &&  this.addAssignmentForm.value.programkey === 'IL') {
     this._alertService.success('Independent Living(Program  Assignment cannot be selected when Client is Under 18 and Over 21 yrs of age)');
     return;
   }

    const model = {
        personid: this.personid,
        personprogramid: (this.personprogramid && !this.isAddAssignment) ? this.personprogramid : null,
        startdate: this.addAssignmentForm.value.startdate,
        enddate: this.addAssignmentForm.value.enddate,
        programkey: this.addAssignmentForm.value.programkey,
        subprogramkey: this.addAssignmentForm.value.subprogramkey,
        endreasonkey: this.addAssignmentForm.value.endreasonkey,
        clientmergeid: null,
        ifpsatriskflag: (this.addAssignmentForm.value.ifpsatriskflag === 'Yes') ? 1 : 0,
        // @Simar: changing this to lower case as the application dependencies / data migration expect 'servicecase' and not 'Servicecase'
        objecttypekey: 'servicecase',
        objectid: this.id
    };

    this._commonHttpService
        .create(model,
            'Personprogramareas/addupdate'
        )
        .subscribe((result) => {
            const status = (this.isAddAssignment) ? 'added' : 'updated';
            (<any>$('#add-program-assignment')).modal('hide');
            this._alertService.success('Program assignment ' + status + ' successfully!');
            this.getProgramAssignmentList();
            (<any>$('#program-assignment-service-case')).modal('show');
        });
}


private loadProgramAreaDropdowns(servicerequesttypekey: string) {
    this._commonHttpService
        .getPagedArrayList(
            new PaginationRequest({
                where: { servicerequestsubtypekey: servicerequesttypekey },
                method: 'get',
                nolimit: true
            }),
            'agencyprogramarea/list?filter'
        )
        .subscribe((result) => {
            if (result && Array.isArray(result) && result.length) {
                this.programArea = result[0].programarea;
                this.programArea = this.programArea.filter(function(n){
                  const progAreaArray = ['ADP', 'IS', 'OOH'];
                  const manual = progAreaArray.includes(n.programkey);
                  return !manual;
                });
            }
        });
}
checkaccesstoproceed(value, type?: string, progreamInfo?: any) {
  this._commonHttpService.getArrayList({
    where: { programkey: value} , method: 'get', nolimit: true
  }, 'agencyprogramarea/list?filter').subscribe((item) => {
    if (item && item.length) {
      this.programSubArea = item[0].subprogram;
    }
    if (value === 'OOH'  && this.activepersondetails.relationship !== 'Child') {
      this._alertService.error('OOH Program Area  Not Allowed For Other Than Child Role.' );
      this.addAssignmentForm.controls['programkey'].reset();
      return false;
    }
    // tslint:disable-next-line:radix
    if (value === 'OOH' && (parseInt(this.activepersondetails.age) > 17) && this.activepersondetails.relationship === 'Child') {
      this._alertService.error('OOH Program Area should Not Allowed Over 17 Years');
      this.addAssignmentForm.controls['programkey'].reset();
      return false;
    }
      if (((value === 'IHSFP' || value === 'OOH') && this.activepersondetails.relationship === 'Child') || (this.isValidateProgramArea && type === 'checkDue' && progreamInfo.programkey === 'OOH')) {
        let object = {};
        if (type === 'checkDue') {
          object = {
            personid: this.activepersondetails.personid,
            servicecaseid: progreamInfo.objectid
          };
        } else {
           object = {
            personid: this.activepersondetails.personid,
            servicecaseid: this.activepersondetails.servicecaseid
        };
        }
          this._commonHttpService.getPagedArrayList(new PaginationRequest({
              where: object,
              method: 'get'
          }), 'Caseassignments/validateprogramarea?filter').subscribe((result: any) => {
              if (result.count !== '0') {
                if (value !== 'OOH') {
                  this.outhomestatus = true;
                  this.outhomemessage = 'Out of home placement is not ended.';
                }
              } else {
                if (value !== 'OOH') {
                  this.outhomestatus = false;
                  this.outhomemessage = null;
                } else {
                  this.outhomemessage = 'Out of home placement is not available';
                  this.outhomestatus = true;
                }
              }
          });
      } else {
        this.outhomestatus = false;
        this.outhomemessage = null;
      }
  });
}

  searchOrAddFunctionality() {
    const source = this._dataStoreService.getData(AppConstants.GLOBAL_KEY.SOURCE_PAGE);
    if ( this.noSearchAddList.includes(source)) {
      this.canSearchAdd = false;
    }
  }
}
