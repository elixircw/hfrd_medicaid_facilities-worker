import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { InvolvedPerson } from './_entities/involvedperson.data.model';
import { InvolvedPersonsService } from './involved-persons.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationUtils } from '../../_utils/navigation-utils.service';
import { DataStoreService } from '../../../@core/services';
import { AppConstants } from '../../../@core/common/constants';

@Component({
  selector: 'involved-persons',
  templateUrl: './involved-persons.component.html',
  styleUrls: ['./involved-persons.component.scss']
})
export class InvolvedPersonsComponent implements OnInit {



  constructor(private activatedRoute: ActivatedRoute, private _dataStoreService: DataStoreService) {
    this.activatedRoute.data.subscribe(data => {
      if (data && data.hasOwnProperty('source')) {
        this._dataStoreService.setData(AppConstants.GLOBAL_KEY.SOURCE_PAGE, data.source);
      } else {
        alert('Please Configure source for persons in routing'); // configre data in respective feautre routing module
      }
      console.log('person source', data);
    });
  }

  ngOnInit() {

  }



}
