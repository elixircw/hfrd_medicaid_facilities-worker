import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvolvedPersonsRoutingModule } from './involved-persons-routing.module';
import { InvolvedPersonsComponent } from './involved-persons.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { InvolvedPersonsService } from './involved-persons.service';
import { InvolvedPersonsResolverService } from './involved-persons-resolver.service';
import { PersonsGridCwComponent } from './persons-grid-cw/persons-grid-cw.component';
import { NavigationUtils } from '../../_utils/navigation-utils.service';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { FindIndividualService } from './find-individual/find-individual.service';
import { CollateralNewModule } from '../collateral-new/collateral-new.module';

@NgModule({
  imports: [
    CommonModule,
    InvolvedPersonsRoutingModule,
    FormMaterialModule,
    CollateralNewModule
  ],
  declarations: [InvolvedPersonsComponent, PersonsGridCwComponent],
  providers: [InvolvedPersonsService,
    InvolvedPersonsResolverService,
    FindIndividualService,
    NavigationUtils,
    IntakeUtils]
})
export class InvolvedPersonsModule { }
