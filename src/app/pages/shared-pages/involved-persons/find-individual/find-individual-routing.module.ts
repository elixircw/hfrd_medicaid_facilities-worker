import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FindIndividualComponent } from './find-individual.component';
import { SearchCriteriaComponent } from './search-criteria/search-criteria.component';
import { SearchResultComponent } from './search-result/search-result.component';

const routes: Routes = [{
  path: '',
  component: FindIndividualComponent,
  children: [
    {
      path: 'search',
      component: SearchCriteriaComponent
    },
    {
      path: 'search-result',
      component: SearchResultComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FindIndividualRoutingModule { }
