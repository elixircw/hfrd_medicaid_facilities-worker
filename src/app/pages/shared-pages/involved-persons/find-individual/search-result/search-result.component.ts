import { Component, OnInit } from '@angular/core';
import { ObjectUtils } from '../../../../../@core/common/initializer';
import { FindIndividualService } from '../find-individual.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { PersonSearch } from '../../_entities/involvedperson.data.model';
import { NavigationUtils } from '../../../../_utils/navigation-utils.service';
import { InvolvedPersonsService } from '../../involved-persons.service';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { PersonDsdsAction } from '../../../../newintake/my-newintake/_entities/newintakeModel';
import { NewUrlConfig } from '../../../../newintake/newintake-url.config';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { FindUrlConfig } from '../../../../find/find.url.config';
import { IntakeStore } from '../../../../_utils/intake-utils.service';
import { CASE_STORE_CONSTANTS } from '../../../../case-worker/_entities/caseworker.data.constants';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';
import { InvolvedPersonSearchResponse } from '../../../../provider-referral/new-referral/_entities/newintakeModel';
const MAX_CRITERIA = 5;
export class IntakeCaseStore {
  intakeserviceid: string;
  servicerequestnumber: string;
  action: string;
}
@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  paginationInfo: PaginationInfo = new PaginationInfo();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  personSearchResult$: Observable<PersonSearch[]>;
  selectedPerson: any;
  showPersonDetail = -1;
  personDSDSActions$: Observable<PersonDsdsAction[]>;

  // SSN
  isSsnHidden = true;
  ssnEye = 'fa-eye';
  showSsnMask = true;
  personSearchResult: any[];
  totalRecords: number;
  selectedCaseInfo: any;
  selectedPriorPerson: any;
  searchLabels: any = [];
  hasMoreCriteria = false;
  constructor(private _router: Router, private route: ActivatedRoute,
    private _findIndividualService: FindIndividualService,
    private _involvedPersonService: InvolvedPersonsService,
    private _alertService: AlertService,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _navigationUtils: NavigationUtils) { }

  ngOnInit() {
    console.log('search', this._findIndividualService.searchCriteria);
    if (!this._findIndividualService.searchCriteria) {
      this.goBack();
    } else {
      this.searchLabels = [];
      ObjectUtils.removeEmptyProperties(this._findIndividualService.searchCriteria);
      for (const [key, value] of Object.entries(this._findIndividualService.searchCriteria)) {
        console.log(key + ':' + value);
        if (key !== 'sortorder') {
          this.searchLabels.push({ label: key, value: value });
        }
      }
      const baseList = this.searchLabels.filter(item => !['firstname', 'lastname'].includes(item.label));
      const firstnameitem = this.searchLabels.find(item => item.label === 'firstname');
      const lastnameitem = this.searchLabels.find(item => item.label === 'lastname');
      if (lastnameitem) { baseList.unshift(lastnameitem); }
      if (firstnameitem) { baseList.unshift(firstnameitem); }
      this.searchLabels = baseList;
      if (this.searchLabels.length > MAX_CRITERIA) {
        this.hasMoreCriteria = true;
        this.searchLabels = this.searchLabels.slice(0,  MAX_CRITERIA );
      } else {
        this.hasMoreCriteria = false;
      }
      this.getPage(1);
    }
  }

  goBack() {
    this._router.navigate(['../search'], { relativeTo: this.route });
  }

  private getPage(pageNumber: number) {
    this.selectedPerson = null;
    const source = this._findIndividualService.searchPerson(pageNumber, this.paginationInfo)
      .map((result) => {
        return {
          data: result.data,
          count: result.count,
          canDisplayPager: result.count > this.paginationInfo.pageSize
        };
      }).subscribe(response => {
        this.personSearchResult = response.data;
        if (pageNumber === 1) {
          this.totalRecords = response.count;
        }
      });
    /* this.personSearchResult$ = source.pluck('data');
     if (pageNumber === 1) {
       this.totalRecords$ = source.pluck('count');
       this.canDisplayPager$ = source.pluck('canDisplayPager');
     } */
  }

  clearSearchFilter() {
    this._findIndividualService.searchCriteria.sortorder = 'asc';
    this._findIndividualService.searchCriteria.sortcolumn = null;
    this.getPage(1);
    this.paginationInfo.pageNumber = 1;
  }

  onSortedPerson($event: ColumnSortedEvent) {
    this._findIndividualService.searchCriteria.sortorder = $event.sortDirection;
    this._findIndividualService.searchCriteria.sortcolumn = $event.sortColumn;
    this.paginationInfo.pageNumber = 1;
    this.getPage(1);
  }

  selectPerson(row) {
    this.selectedPerson = row;
  }

  editSelectedPerson() {
    if (this.selectedPerson) {
      if (this.selectedPerson.source === 'SDR') {
        const url = FindUrlConfig.EndPoint.PersonSearch.identifier;
        this._commonHttpService.getArrayList(
          {
            where: {
              personidentifiervalue: this.selectedPerson.mdm_id,
              personidentifiertypekey: 'MDM_ID'
            },
            method: 'get'
          }
          , url).subscribe(data => {
            if (data[0]) {
              this.selectedPerson.personid = data[0].personid;
            }
            if (this.selectedPerson.personid) {
              this._involvedPersonService.editSDRPerson(this.selectedPerson);
            } else {
              this._involvedPersonService.newSDRPerson(this.selectedPerson);
            }
          });
      } else {
        this._involvedPersonService.editPerson(this.selectedPerson.personid);
      }
    } else {
      this._alertService.error('Please select person');
    }
  }

  addNewPerson() {
    let searchdata = {};
    if (this._findIndividualService.searchCriteria) {
      searchdata = this._findIndividualService.searchCriteria;
      searchdata['exist'] = 1;
    }
    this._involvedPersonService.newPerson(searchdata);
  }

  searchPersonDetailsRow(id: number, model: PersonDsdsAction) {
    this.searchPersonDetails(id);
    if (this.showPersonDetail === id) {
      console.log('open model...', model, id);
      this.getPersonDSDSAction(model);
    }
  }

  searchPersonDetails(id: number) {
    if (this.showPersonDetail !== id) {
      this.showPersonDetail = id;
    } else {
      this.showPersonDetail = -1;
    }
  }

  getPersonDSDSAction(model: PersonDsdsAction) {
    const url = 'Intakeservicerequests/searchpriordsdsactionsbyperson' + `?personid=` + model.personid + '&filter';
    const source = this._commonHttpService
      .getArrayList(
        new PaginationRequest({
          method: 'get',
          where: { intakerequestid: null }
        }),
        url
      )
      .share();
    this.personDSDSActions$ = source.pluck('data');
    this.personDSDSActions$
      .map((data) => {
        data.map((address) => {
          address.daDetails.map((addressdata) => {
            // filter the duplicate roles
            if (addressdata.roles) {
              const uniqueRoles = addressdata.roles.filter((elem, i, arr) => {
                if (arr.indexOf(elem) === i) {
                  return elem;
                }
              });
              addressdata.roles = uniqueRoles;
            }
            if (addressdata.dasubtype === 'Peace Order') {
              address.highLight = true;
              return address;
            }
          });
        });
        return data;
      })
      .subscribe((finalValue) => console.log(finalValue));
  }


  pageChanged(event: any) {
    this.paginationInfo.pageNumber = event.page;
    this.getPage(this.paginationInfo.pageNumber);
  }

  toggleSsn = () => {
    this.isSsnHidden = !this.isSsnHidden;
    if (this.isSsnHidden) {
      this.ssnEye = 'fa-eye';
      this.showSsnMask = true;
    } else {
      this.ssnEye = 'fa-eye-slash';
      this.showSsnMask = false;
    }
  }

  openCase(item) {
    this._navigationUtils.openRespectiveItem(item);
  }

  showOutcome(person, caseInfo) {
    console.log(person, caseInfo);
    this.selectedPriorPerson = person;
    this.selectedCaseInfo = caseInfo;
    switch (caseInfo.datype) {
      case 'Child Protective Services':
        (<any>$('#person-out-come')).modal('show');
        break;
    }
  }
  close() {
    (<any>$('#person-out-come')).modal('hide');
    this.selectedCaseInfo = null;
    this.selectedPriorPerson = null;
  }

  isMaltreator(roles) {
    let hasMaltreator = false;
    if (Array.isArray(roles)) {
      const maltreatorIndex = roles.indexOf('Alleged Maltreator');
      if (maltreatorIndex !== -1) {
        hasMaltreator = true;
      }
    }
    return hasMaltreator;
  }
}
