import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindIndividualComponent } from './find-individual.component';

describe('FindIndividualComponent', () => {
  let component: FindIndividualComponent;
  let fixture: ComponentFixture<FindIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
