import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ValidationService, CommonDropdownsService } from '../../../../../@core/services';
import { FindIndividualService } from '../find-individual.service';
import { DataStoreService } from '../../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { IntakeStoreConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';

@Component({
  selector: 'search-criteria',
  templateUrl: './search-criteria.component.html',
  styleUrls: ['./search-criteria.component.scss']
})
export class SearchCriteriaComponent implements OnInit {

  involvedPersonSearchForm: FormGroup;
  validation_messages: any;
  genderDropdownItems$: Observable<any[]>;

  //SSN
  isSsnHidden = true;
  ssnEye = 'fa-eye';
  showSsnMask = true;

  stateDropdownItems$: Observable<any[]>;
  countyDropdownItems$: Observable<any[]>;
  constructor(private _router: Router,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _findIndividualService: FindIndividualService,
    private _dataStoreService: DataStoreService,
    private _commonDropdownService: CommonDropdownsService,
    private _intakUtils: IntakeUtils
  ) { }

  //https://eservices.paychex.com/secure/HRO_PNG/ssn_itin_fed_id_other.html
  //These cannot be valid ssns, so should be prevented.
  validateSSN(c: FormControl) {
    const invalidssn = ['000000000'];
    return invalidssn.includes(c.value) ? {
      validateSSN: {
        valid: false
      }
    } : null;
  }

  ngOnInit() {
    this.initForm();
    this.validation_messages = {
      'lastname': [
        { type: 'pattern', message: 'Enter a valid Last Name' }
      ],
      'firstname': [
        { type: 'pattern', message: 'Enter a valid First Name' }
      ]
    };
    this.involvedPersonSearchForm.get('county').disable();
    this.loadDropDowns();
  }

  loadDropDowns() {
    this.genderDropdownItems$ = this._commonDropdownService.getPickListByName('gender');
    this.stateDropdownItems$ = this._commonDropdownService.getPickListByName('state');
    this.countyDropdownItems$ = this._commonDropdownService.getPickListByName('county');
  }

  initForm() {
    this.involvedPersonSearchForm = this._formBuilder.group({
      lastname: new FormControl('', Validators.compose([
        Validators.pattern('[a-zA-Z-\' ]*$') // [a-zA-Z]+(\s+[a-zA-Z]+)*
      ])),
      firstname: new FormControl('', Validators.compose([
        Validators.pattern('^[a-zA-Z-\' ]*$')
      ])),
      maidenname: [''],
      gender: [''],
      dob: [''],
      dateofdeath: [''],
      ssn: ['', [this.validateSSN]],
      mediasrc: [''],
      mediasrctxt: [''],
      occupation: [''],
      dl: [''],
      stateid: [''],
      address1: [''],
      address2: [''],
      zip: ['' ],
      city: [''],
      county: [''],
      selectedPerson: [''],
      cjisnumber: [''],
      cjamspid: [''],
      complaintnumber: [''],
      fein: [''],
      age: [''],
      email: ['', [ValidationService.mailFormat]],
      phone: [''],
      petitionid: [''],
      alias: [''],
      oldId: ['']
    }, { validator: this.atLeastOne(Validators.required) });
    if (this._findIndividualService.searchCriteria) {
      this.involvedPersonSearchForm.patchValue(this._findIndividualService.searchCriteria);
      if (this._findIndividualService.searchCriteria.dob) {
        this.involvedPersonSearchForm.patchValue({
          dob: new Date(this._findIndividualService.searchCriteria.dob)
        });
      }
      this.involvedPersonSearchForm.markAsDirty();
      this.involvedPersonSearchForm.markAsTouched();
     
    }
    const quickNarattiveSearchData = this._dataStoreService.getData(IntakeStoreConstants.PERSON_TO_SEARCH);
    console.log(quickNarattiveSearchData);
    if (quickNarattiveSearchData) {
      this.involvedPersonSearchForm.patchValue({
        lastname: quickNarattiveSearchData.lastname,
        firstname: quickNarattiveSearchData.firstname,
        dob: quickNarattiveSearchData.dob ? new Date(quickNarattiveSearchData.dob) : null,
        gender: quickNarattiveSearchData.gender,
        ssn: quickNarattiveSearchData.ssn ? quickNarattiveSearchData.ssn : ''
      });
      this.involvedPersonSearchForm.markAsDirty();
      this.involvedPersonSearchForm.markAsTouched();
    }
    this.involvedPersonSearchForm.updateValueAndValidity();
    console.log(this.involvedPersonSearchForm.status);
    console.log('pristine', this.involvedPersonSearchForm.pristine);

  }
  atLeastOne = (validator: ValidatorFn) => (
    group: FormGroup,
  ): ValidationErrors | null => {
    const hasAtLeastOne = group && group.controls && Object.keys(group.controls)
      .some(k => !validator(group.controls[k]));

    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
  }

  goBack() {
    this._router.navigate(['../'], { relativeTo: this.route });
  }
  goToSearchResult() {
    this._router.navigate(['../search-result'], { relativeTo: this.route });
  }

  ssnChange(value) {
    if (value === '') {
      this.involvedPersonSearchForm.patchValue({
        ssn: value
      });
    }
  }

  searchPersons() {
    if (this.involvedPersonSearchForm.valid) {
      this._findIndividualService.searchCriteria = this.involvedPersonSearchForm.getRawValue();
      this._findIndividualService.searchCriteria.sortorder = 'asc';
      this._findIndividualService.searchCriteria.sortcolumn = null;
      // this._dataStoreService.setData('SearchDataInvolvedPerson', this._findIndividualService.searchCriteria);
      this.goToSearchResult();

    } else {
      // throw the error here
      console.log('throw the error here');
    }

  }

  clearPersonSearch() {
    this.involvedPersonSearchForm.get('county').disable();
    this.loadDropDowns();
    this._findIndividualService.searchCriteria = null;
    this.involvedPersonSearchForm.reset();
  }

  toggleSsn = () => {
    this.isSsnHidden = !this.isSsnHidden;
    if (this.isSsnHidden) {
      this.ssnEye = 'fa-eye';
      this.showSsnMask = true;
    } else {
      this.ssnEye = 'fa-eye-slash';
      this.showSsnMask = false;
    }
  }
  loadCounty() {
    this.involvedPersonSearchForm.get('county').enable();
    const stateKey = this.involvedPersonSearchForm.getRawValue().stateid;
    this._commonDropdownService.getPickListByMdmcode(stateKey).subscribe(countyList => {
      console.log('cl', countyList);
      this.countyDropdownItems$ = Observable.of(countyList);
    });
  }


  get f() {
    return this.involvedPersonSearchForm.controls;
  }

}
