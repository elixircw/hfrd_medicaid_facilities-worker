import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvolvedPersonsComponent } from './involved-persons.component';
import { PersonsGridCwComponent } from './persons-grid-cw/persons-grid-cw.component';

const routes: Routes = [{
  path: '',
  component: InvolvedPersonsComponent,
  children: [
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
    {
      path: 'list',
      component: PersonsGridCwComponent
    },
    {
      path: 'find-individual',
      loadChildren: './find-individual/find-individual.module#FindIndividualModule'
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvolvedPersonsRoutingModule { }
