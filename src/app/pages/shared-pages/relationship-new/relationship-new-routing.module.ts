import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelationshipNewComponent } from './relationship-new.component';

const routes: Routes = [{
  path: '',
  component: RelationshipNewComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelationshipNewRoutingModule { }
