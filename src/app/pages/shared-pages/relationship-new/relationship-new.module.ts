import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelationshipNewComponent } from './relationship-new.component';
import { RelationshipNewRoutingModule } from './relationship-new-routing.module';
import { RelationshipNewService } from './relationship.new.service';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MatCardModule,  MatGridListModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    RelationshipNewRoutingModule,
    FormMaterialModule,
    MatCardModule,
    MatGridListModule,
    NgSelectModule
  ],
  exports: [RelationshipNewComponent],
  declarations: [RelationshipNewComponent],
  providers: [RelationshipNewService]
})
export class RelationshipNewModule { }
