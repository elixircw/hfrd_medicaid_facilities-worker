import { Component, OnInit } from '@angular/core';
import { InvolvedPerson } from '../involved-persons/_entities/involvedperson.data.model';
import { RelationshipNewService } from './relationship.new.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { CommonHttpService, DataStoreService, AuthService, SessionStorageService } from '../../../@core/services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CASE_STORE_CONSTANTS } from '../../case-worker/_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import { DropdownModel } from '../../../@core/entities/common.entities';
import { IntakeStoreConstants } from '../../newintake/my-newintake/my-newintake.constants';

@Component({
  selector: 'relationship-new',
  templateUrl: './relationship-new.component.html',
  styleUrls: ['./relationship-new.component.scss']
})
export class RelationshipNewComponent implements OnInit {
  selectedCard: string;
  involevedPersonList: InvolvedPerson[];
  involevedPerson$: Observable<InvolvedPerson[]>;
  selectedPerson: any;
  private id: string;
  relationshipForm: FormGroup;
  personid2: any;
  relationShipToRADropdownItems: DropdownModel[];
  isClosed = false;
  careGiverFlag = false;
  isChildSelected = false;

  constructor(private _service: RelationshipNewService,
    private route: ActivatedRoute,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private _sessionStorage: SessionStorageService) { }

  ngOnInit() {
    this.loadPersons();
    this.initAll();
    const da_status = this._sessionStorage.getItem('da_status');
    const currentStatus = this._dataStoreService.getData(IntakeStoreConstants.INTAKE_STATUS);
    if (da_status || currentStatus) {
      if (da_status === 'Closed' || da_status === 'Completed' || currentStatus === 'Closed' || currentStatus === 'Completed') { 
        this.isClosed = true;
      } else {
        this.isClosed = false;
    }
    }
  }
  loadPersons() {
    this.involevedPerson$ = this._service.getInvolvedPerson(1, 20, null)
      .share()
      .pluck('data');
    this.involevedPerson$.subscribe(response => {
      if (response && response && Array.isArray(response)) {
        // Removing Collateral From list
        this.involevedPersonList = response.filter(data => data.iscollateralcontact !== 1);
        this.selectPersonCard(0, this.involevedPersonList[0]);
      }
    });
  }

  selectPersonCard(index, person) {
    this.isChildSelected = false;
    this.selectedCard = index;
    this.selectedPerson = person;
    if (person.roles.some(roleid => roleid.intakeservicerequestpersontypekey === 'CHILD')) {
      this.isChildSelected = true;
    }
    this.mergePersons();
  }

  initAll() {
    this.getRelationList();
    this.relationshipForm = this.formBuilder.group({
      actorrelationshipid: [''],
      intakeservicerequestactorid: [''],
      relationshiptypekey: [''],
      person1id: [''],
      person2id: [''],
      servicecaseid: [this.id],
      careGiverFlag: false
    });
  }



  getRelationList() {
    this._commonHttpService.getArrayList(
      {
        where: { activeflag: 1, teamtypekey: this._authService.getAgencyName() },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
        .RelationshipTypesUrl + '?filter'
    ).subscribe(data => {
      if (data && data.length) {
        this.setRelationList(data);
      }
    });
  }
  setRelationList(data) {
    if (data && data.length) {
      this.relationShipToRADropdownItems = data.map(res => {
        return new DropdownModel({
          text: res.description,
          value: res.relationshiptypekey
        });
      });
    }
  }

  UpdatePersonId(person) {
    this.personid2 = person;
    this.relationshipForm.patchValue({
      actorrelationshipid: null,
      intakeservicerequestactorid: person.intakeservicerequestactorid,
      relationshiptypekey: person.relationshiptypekey,
      person1id: this.selectedPerson.personid,
      person2id: person.personid,
      careGiverFlag: person.careGiverFlag
    });
  }
  
  updateRelation() {
    this._commonHttpService.create(this.relationshipForm.value, 'Actorrelationships/addupdate').subscribe(
      (res) => {
        this.mergePersons();
        this.reverseRelationShip();
      
      });
  }
  reverseRelationShip(){
    var opositRelationShip = '';
    if (this.relationshipForm.value.relationshiptypekey && (this.relationshipForm.value.relationshiptypekey === 'BGFTHR' || this.relationshipForm.value.relationshiptypekey === 'BGMTHR')) {
      opositRelationShip = 'BGCHLD'
    }
    if (this.relationshipForm.value.relationshiptypekey && (this.relationshipForm.value.relationshiptypekey === 'BGCHLD')) {
      if(this.selectedPerson.gender==='M'){
        opositRelationShip = 'BGFTHR'
      } else {
        opositRelationShip = 'BGMTHR'
      }
    }

    if(!opositRelationShip){
      return;
    }
    var request = {
      actorrelationshipid: null,
      intakeservicerequestactorid: this.personid2.intakeservicerequestactorid,
      person1id: this.personid2.personid,
      person2id: this.selectedPerson.personid,
      relationshiptypekey:opositRelationShip,    
    };
    this._commonHttpService.create(request, 'Actorrelationships/addupdate').subscribe(
      (res) => {
        this.mergePersons();
      });
  }

  mergePersons() {
    if (this.selectedPerson) {
      this._service.getInvolvedPersonWithPersonID(1, 20, this.selectedPerson.personid)
        .subscribe(response => {
          if (response && response && Array.isArray(response)) {
            for (var i = 0; i < this.involevedPersonList.length; i++) {
              var missing = false;
              if (response.length > 0) {
                for (var j = 0; j < response.length; j++) {
                  // @Simar: Matching every card to the respective person who appears to be person2
                  // Flipped on the API to return the selected person as person1 and others as person2
                  if (this.involevedPersonList[i].personid === response[j].person2id) {
                    this.involevedPersonList[i].relation = response[j].relation;
                    this.involevedPersonList[i].relationshiptypekey = response[j].relationshiptypekey;
                    this.involevedPersonList[i].careGiverFlag = (response[j].caregiverflag == 1);
                    missing = false;
                    break;
                  } else {
                    missing = true;
                  }
                }
                if (missing) {
                  this.involevedPersonList[i].relation = 'unknown'
                  this.involevedPersonList[i].relationshiptypekey = '';
                  this.involevedPersonList[i].careGiverFlag = false;
                }

              } else {
                this.involevedPersonList[i].relation = 'unknown';
                this.involevedPersonList[i].relationshiptypekey = '';
                this.involevedPersonList[i].careGiverFlag = false;
              }
            }
          }
        });
    }
  }

}
