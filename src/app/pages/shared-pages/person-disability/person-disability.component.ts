import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PersonDisabilityService } from './person-disability.service';

@Component({
  selector: 'person-disability',
  templateUrl: './person-disability.component.html',
  styleUrls: ['./person-disability.component.scss']
})
export class PersonDisabilityComponent implements OnInit {

  constructor(private route: ActivatedRoute, private _service: PersonDisabilityService) {
    this.route.data.subscribe(res => {
      console.log('person details', res.personDetails);
      this._service.person = res.personDetails;
    });
  }

  ngOnInit() {
  }

}
