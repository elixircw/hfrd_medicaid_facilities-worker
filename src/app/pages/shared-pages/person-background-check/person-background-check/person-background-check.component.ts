import { Component, OnInit } from '@angular/core';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'person-background-check',
  templateUrl: './person-background-check.component.html',
  styleUrls: ['./person-background-check.component.scss']
})
export class PersonBackgroundCheckComponent implements OnInit {
  id: string;
  objectID: string;
  persons = [];
  selectedPerson: any;
  selectedPersonId: any;
  constructor(private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService) {
    this.objectID = this.route.parent.parent.snapshot.params['id'];
  }

  ngOnInit() {
    console.log('refnumber', this.objectID);
    this.getHouseHolds(1, 20).subscribe(response => {
      console.log('response', response);
      if (response && response.data && response.data.length) {
        this.persons = response.data;
      }
    })
  }

  getHouseHolds(page: number, limit: number) {

    const inputRequest = {
      intakenumber: this.objectID
    };
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: page,
          limit: limit,
          method: 'get',
          where: inputRequest
        }),
        'People/getpersondetailcw?filter'
      );


  }

  selectPerson(person) {
    this.selectedPerson = null;
    this._dataStoreService.setData('SELECTED_PERSON_ID', person.personid);
    this._dataStoreService.setData('OBJECT_ID', this.objectID);
    this._dataStoreService.setData('ROLE', person.rolename);
    setTimeout(() => {
      this.selectedPerson = person;
    }, 1000);
  }

  close() {
    this.selectedPerson = null;
    this.selectedPersonId = null;
  }
}
