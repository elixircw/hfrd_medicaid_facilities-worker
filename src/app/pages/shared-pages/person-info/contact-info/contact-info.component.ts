import { Component, OnInit } from '@angular/core';
import { CommonDropdownsService, CommonHttpService, AlertService, GenericService, ValidationService } from '../../../../@core/services';
import { Validators, Form, FormBuilder, FormGroup } from '@angular/forms';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { PersonInfoService } from '../person-info.service';
import { AddressDetailsService } from '../address-details/address-details.service';
import { AddressType } from '../../../admin/general/_entities/general.data.models';

@Component({
  selector: 'contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss']
})
export class ContactInfoComponent implements OnInit {
  contactTypes: any[];
  personPhoneFormGroup: FormGroup;
  personEmailFormGroup: FormGroup;
  phoneList = [];
  emailList = [];
  personId: any;
  reportPhoneMode: string;
  personPhoneId: any;
  reportEmailMode: string;
  personEmailId: any;
  deleteType = null;
  deleteId = null;
  addedit = 'Add New';
  primaryemailList: any[];
  isprimaryemailadded= false;
  isprimaryadded: boolean;
  constructor(
    private _commonDropDownService: CommonDropdownsService,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertservices: AlertService,
    private _personService: PersonInfoService,
    private _addressService: AddressDetailsService,
    private _service_address: GenericService<AddressType>,
  ) { }

  ngOnInit() {
    this.reportPhoneMode = 'add';
    this.reportEmailMode = 'add';
    this.personId = (this._personService.getPersonId()) ? this._personService.getPersonId() : '';
    this._commonDropDownService.getPickListByName('phonetype').subscribe(contactTypes => {
      if (contactTypes && Array.isArray(contactTypes)) {
        this.contactTypes = contactTypes;
      }
    });
    this.initializePhoneForm();
    this.initializeEmailForm();
    this._addressService.getEmailList(this.personId);
    this._addressService.getPhoneList(this.personId);
    this.getPhoneNumberList();
    this.getEmailList();
  }

  initializePhoneForm() {
    this.personPhoneFormGroup = this._formBuilder.group({
      phonenumber: ['', [Validators.required]],
      personphonetypekey: ['', [Validators.required]],
      startdate: [''],
      enddate: ['']
    });
  }

  initializeEmailForm() {
    this.personEmailFormGroup = this._formBuilder.group({
      email: ['', [ValidationService.mailFormat]],
      personemailtypekey: ['', [Validators.required]],
      startdate: [''],
      enddate: ['']
    });
  }

  resetPhone() {
    this.addedit = 'Add New';
    this.reportPhoneMode = 'add';
    this.personPhoneFormGroup.reset();
  }

  resetEmail() {
    this.reportEmailMode = 'add';
    this.addedit = 'Add New';
    this.personEmailFormGroup.reset();
    if (this.emailList && this.emailList.length > 0) {
      this.primaryemailList = this.emailList.filter(email => email.personemailtypekey === 'P');
      if (this.primaryemailList.length > 0) {
        this.isprimaryadded = true;
      } else {
        this.isprimaryadded = false;
      }
    } else {
        this.isprimaryadded = false;
    }
  }

  addPhone() {
    const personPhoneId = (this.reportPhoneMode === 'add') ? null : this.personPhoneId;
    const personPhoneForm = this.personPhoneFormGroup.getRawValue();
    const addupdatePhone = [];
    addupdatePhone.push({personphonenumberid: personPhoneId,
        personid: this.personId,
        personphonetypekey: personPhoneForm.personphonetypekey,
        phonenumber: personPhoneForm.phonenumber,
        startdate: personPhoneForm.startdate ? new Date(personPhoneForm.startdate) : null,
        enddate: personPhoneForm.enddate ? new Date(personPhoneForm.enddate) : null,
        phoneextension: null
    });

    this._commonHttpService.create({'addupdatephonenumber': addupdatePhone}, CommonUrlConfig.EndPoint.PERSON.PHONE.AddUpdatePhone).subscribe(
      result => {
        this._alertservices.success('Phone details Added/Updated successfully!');
        (<any>$('#addPhone')).modal('hide');
        this.reportPhoneMode = 'add';
        this._addressService.getPhoneList(this.personId);
        this.getPhoneNumberList();
      },
      error => {
        this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      });
  }

  addEmail() {
    const personEmailForm = this.personEmailFormGroup.getRawValue();
    if (personEmailForm.personemailtypekey === 'S') {
     this.primaryemailList = this.emailList.filter(email => email.personemailtypekey === 'P');
     if (this.primaryemailList.length > 0) {
      this.isprimaryemailadded = true;
      } else {
      this._alertservices.error('Please enter primary email address!');
         }
    } else {
      this.isprimaryemailadded = true;
    }
    if (this.isprimaryemailadded === true) {
    const personEmailId = (this.reportEmailMode === 'add') ? null : this.personEmailId;
    const addupdateemail = [];
    addupdateemail.push({personemailid: personEmailId,
        personid: this.personId,
        personemailtypekey: personEmailForm.personemailtypekey,
        email: personEmailForm.email,
        startdate: personEmailForm.startdate ? new Date(personEmailForm.startdate) : null,
        enddate: personEmailForm.enddate ? new Date(personEmailForm.enddate) : null
    });

   this._commonHttpService.create({'addupdateemail': addupdateemail}, CommonUrlConfig.EndPoint.PERSON.EMAIL.AddUpdateEmail).subscribe(
      result => {
        this._alertservices.success('Email details Added/Updated successfully!');
        (<any>$('#addEmail')).modal('hide');
        this.reportEmailMode = 'add';
        this._addressService.getEmailList(this.personId);
        this.getEmailList();
      },
      error => {
        this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
    }
  }

  getPhoneNumberList() {
    this._addressService.phonePersonType$.subscribe( (data) => {
      this.phoneList = [];
      if (data.length > 0) {
        this.phoneList = data;
      }
    });
  }

  getEmailList() {
    this._addressService.emailPersonType$.subscribe( (data) => {
      this.emailList = [];
      if (data.length > 0) {
        this.emailList  = data;
      }
    });
  }

  getPhoneType(type) {
    if (this.contactTypes && type) {
      const typeObj = this.contactTypes.find(data => data.ref_key === type);
      return typeObj.description;
    }
    return '';
  }

  getEmailType(type) {
    if (type) {
      let emailType = '';
      if (type === 'P') {
        emailType = 'Primary';
      } else if (type === 'S') {
        emailType = 'Secondary';
      }
      return emailType;
    }
    return '';
  }

  editPhone(data) {
    this.addedit = 'Edit';
    this.reportPhoneMode = 'edit';
    data.startdate = this._commonDropDownService.getValidDate(data.startdate);
    data.enddate = this._commonDropDownService.getValidDate(data.enddate);
    this.personPhoneFormGroup.patchValue(data);
    this.personPhoneId = data.personphonenumberid;
  }

  editEmail(data) {
    if (data.personemailtypekey === 'P') {
      this.isprimaryadded = false;
    } else {
      if (this.emailList && this.emailList.length > 0) {
        this.primaryemailList = this.emailList.filter(email => email.personemailtypekey === 'P');
        if (this.primaryemailList.length > 0) {
          this.isprimaryadded = true;
        } else {
          this.isprimaryadded = false;
        }
      } else {
          this.isprimaryadded = false;
      }
    }
    this.addedit = 'Edit';
    this.reportEmailMode = 'edit';
    data.startdate = this._commonDropDownService.getValidDate(data.startdate);
    data.enddate = this._commonDropDownService.getValidDate(data.enddate);
    this.personEmailFormGroup.patchValue(data);
    this.personEmailId = data.personemailid;
  }

  confirmDelete(id, type, typekey) {
    this.deleteType = type;
    this.deleteId = id;
    if ((type === 'email') && (typekey === 'P')) {
    this.primaryemailList = this.emailList.filter(email => email.personemailtypekey === 'P');
     if (this.primaryemailList.length < 2) {
      this._alertservices.error('Please make sure to have atleast one primary email address!');
     }
    }
    (<any>$('#delete-popup')).modal('show');
  }

  declineDelete() {
    this.deleteType = null;
    this.deleteId = null;
    (<any>$('#delete-popup')).modal('hide');
  }

  deletePhoneEmail(i: number) {
    if (this.deleteId) {
      const url = (this.deleteType === 'email') ? CommonUrlConfig.EndPoint.PERSON.EMAIL.DeleteEmailUrl : CommonUrlConfig.EndPoint.PERSON.PHONE.DeletePhoneUrl;
      this._service_address.endpointUrl =  url;
      this._service_address.remove(this.deleteId).subscribe(
        response => {
          (<any>$('#delete-popup')).modal('hide');
          const deleteType = (this.deleteType === 'email') ? 'Email' : 'Phone';
          this._alertservices.success(deleteType + ' deleted successfully..');

          if (this.deleteType === 'email') {
            this._addressService.getEmailList(this.personId);
            this.getEmailList();
          } else {
            this._addressService.getPhoneList(this.personId);
            this.getPhoneNumberList();
          }
        },
        error => {
          this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    } else {
     // this.addressPersonList.splice(i, 1);
      // this.addressPersonType$ = Observable.of(this.addressPersonType);
      (<any>$('#delete-popup')).modal('hide');
     // this._alertService.success('Address deleted successfully');
    //  this.getAddressListPage(1);
    }
  }

  formatPhoneNumber(phoneNumber: string) {
    return this._commonDropDownService.formatPhoneNumber(phoneNumber);
  }
}
