import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormMaterialModule } from '../../../../@core/form-material.module';

import { ContactInfoRoutingModule } from './contact-info-routing.module';
import { ContactInfoComponent } from './contact-info.component';
import { HttpClientModule } from '@angular/common/http';
import { AddressDetailsService } from '../address-details/address-details.service';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    ContactInfoRoutingModule,
    FormMaterialModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [ContactInfoComponent],
  providers: [AddressDetailsService]

})
export class ContactInfoModule { }
