import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { PersonInfoService } from '../person-info.service';

@Component({
  selector: 'eighteen21',
  templateUrl: './eighteen21.component.html',
  styleUrls: ['./eighteen21.component.scss']
})
export class Eighteen21Component implements OnInit {
  secondarySchool= [];
  vocational= [];
  activity= [];
  employer= [];
  personDisabilities= [];
  answers= {};
  answerloaded= false;
  programsloaded= false;
  narrativeInfo;
  personid: string;
  narrativeuniqueInfo;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _personInfoService: PersonInfoService

    ) {

      if (this._personInfoService.personInfo && this._personInfoService.personInfo.personbasicdetails) {
        this.personid = this._personInfoService.personInfo.personbasicdetails.personid;
        this.load18to21Details(this.personid);

      }
      const _self = this;
      this._personInfoService.personInfoListener$.subscribe(personInfo => {
        if (!_self.personid || _self.personid === '') {
          this.personid = personInfo.personbasicdetails.personid;
          this.load18to21Details(this.personid);

        }
      });
    }

  ngOnInit() {
  }
  loadNarrativeDetails(personid) {
    this.getNarrativeDetails(personid).subscribe(workdetails => {
      this.programsloaded = true;
      this.narrativeInfo = workdetails;
      const narrativeuniqueInfo = [];
      this.narrativeuniqueInfo = [];
      for ( let i = 0; i < this.narrativeInfo.length; i++) {
         const promotedemploymentprogramname = this.narrativeInfo[i].promotedemploymentprogramname;
          if (promotedemploymentprogramname) {
            narrativeuniqueInfo[promotedemploymentprogramname] = {};
            narrativeuniqueInfo[promotedemploymentprogramname] = this.narrativeInfo[i]
          }
      }

      for (let k in narrativeuniqueInfo) {
        this.narrativeuniqueInfo.push(narrativeuniqueInfo[k]);
      }
    });
  }
  load18to21Details(personid) {
    this.load18to21answer(personid).subscribe(answer  => {
      this.answers = answer;
      this.answerloaded = true;

    });
    this.loadNarrativeDetails(personid);
  }
  load18to21answer(personid) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personid }
        }),
        'People/get18to21answer?filter'
      );
  }
  getNarrativeDetails(personid) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personid }
        }),
        'People/getpersonworknarrative?filter'
      );
  }
}
