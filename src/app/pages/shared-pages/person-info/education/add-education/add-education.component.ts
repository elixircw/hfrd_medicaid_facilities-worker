import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { NewUrlConfig } from '../../../../newintake/newintake-url.config';
import { Validators, Form, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DataStoreService, SessionStorageService, CommonDropdownsService, AlertService } from '../../../../../@core/services';
import { DropdownModel } from '../../../../../@core/entities/common.entities';

import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ControlUtils } from '../../../../../@core/common/control-utils';
import { PersonInfoService } from '../../person-info.service';
import { EducationInfoService } from '../education.service';
import { InvolvedPersonsConstants } from '../../../../case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
import { Employer, Work } from '../../../../case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { CASE_STORE_CONSTANTS } from '../../../../case-worker/_entities/caseworker.data.constants';
import { ActivatedRoute } from '@angular/router';
import { NavigationUtils } from '../../../../_utils/navigation-utils.service';

@Component({
  selector: 'add-education',
  templateUrl: './add-education.component.html',
  styleUrls: ['./add-education.component.scss']
})
export class AddEducationComponent implements OnInit {

  personId: any;
  data = [];

  isSpecialEducation = false;
  editflag = false;
  isEdited = false;
  id: string;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  schoolTypeDropdownItems$: Observable<DropdownModel[]>;
  schoolSettingDropdownItems$: Observable<DropdownModel[]>;
  transportmodetypeDropdownItems$: Observable<DropdownModel[]>;
  typeofclassDropdownItems$: Observable<DropdownModel[]>;
  gradeDropdownItems$: Observable<DropdownModel[]>;
  specialEducationInDensityDropdownItems$: Observable<DropdownModel[]>;
  testnameDropdownItems$: Observable<DropdownModel[]>;
  schoolSpDropdownItems$: Observable<DropdownModel[]>;
  schoolExitReasonDropdownItems$: Observable<DropdownModel[]>;
  disciplinaryActionDropdownItems$: Observable<DropdownModel[]>;
  schoolAdjustmentDropdownItems$: Observable<DropdownModel[]>;
  educationStartDate: any;

  today = new Date();
  dd = this.today.getDate();
  mm = this.today.getMonth();
  yyyy = this.today.getFullYear();
  maxDate = new Date(this.yyyy, this.mm, this.dd);
  maximumDate: Date = new Date();
  toMinDate: Date = null;

  personEducationFormGroup: FormGroup;
  @Output() addflag: EventEmitter<Boolean> = new EventEmitter();
  gradeDropdownItems: any[];



  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _commonDropdownService: CommonDropdownsService,
    private _alertSevice: AlertService,
    private _personInfoService: PersonInfoService,
    private _dataStoreService: DataStoreService,
    private _session: SessionStorageService,
    private route: ActivatedRoute,
    private _educationInfoService: EducationInfoService,
    private _navigationUtils: NavigationUtils) { }

  ngOnInit() {
    const personInfo = this._personInfoService.getPersonInfo();
    if (personInfo && personInfo.personbasicdetails) {
      this.educationStartDate = personInfo.personbasicdetails.dob ? personInfo.personbasicdetails.dob : null;
    }
    this.initiateFormGroup();
    this.loadDropDown();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    console.log(this._personInfoService, this._personInfoService.getPersonId());
    // this.personId = (this._personInfoService.getPersonId()) ? this._personInfoService.getPersonId() : '';
    this.personId = this._personInfoService.getPersonId();
    this._educationInfoService.selectedEducation$.subscribe(data => {
      if (data) {
        this.editEducation(data);
      }
    });
  }

  private initiateFormGroup() {
    this.personEducationFormGroup = this._formBuilder.group({
      personid: [null],
      personEducation: this._formBuilder.group({
        personeducationid: [null],
        educationname: [null],
        educationtypekey: [null],
        schoolsettingtypekey: [null],
        transportmodetypekey: [null],
        transportmodetypedetail: [null],
        adrcityname: [null],
        adresscounty: [null],
        statecode: [null],
        startdate: [null],
        enddate: [null],
        contactname: [null],
        adrworkphone: [null],
        adrworkxtn: [null],
        schoolschedule: [null],
        schooladjustment: [null],
        isspecialeducation: [false],
        specialeducationtypekey: [null],
        lastiepdate: [null],
        lastifspdate: [null],
        numberofabsences: [null],
        isreceived: [null],
        isverified: [null],
        isexcused: [null],
        extracurricular: [null],
        sasidno: [null],
        firstqtrabsence: [null],
        firstqtrabsenceexcused: 0,
        firstqtrabsencenotexcused: 0,
        firstqtrabsencetardy: 0,
        secondqtrabsence: [null],
        secondqtrabsenceexcused: 0,
        secondqtrabsencenotexcused: 0,
        secondqtrabsencetardy: 0,
        thirdqtrabsence: [null],
        thirdqtrabsenceexcused: 0,
        thirdqtrabsencenotexcused: 0,
        thirdqtrabsencetardy: 0,
        fourthqtrabsence: [null],
        fourthqtrabsenceexcused: 0,
        fourthqtrabsencenotexcused: 0,
        fourthqtrabsencetardy: 0,
        summerschoolname: null,
        highestgradetypekey: null,
        currentgradetypekey: null,
        lastgradetypekey: null,
        classtypetypekey: null,
        currentgradelevel: null,
        functioninggradelevel: null,
        lastgradelevel: null,
        firstqtrperformancetypekey: null,
        secondqtrperformancetypekey: null,
        thirdqtrperformancetypekey: null,
        fourthqtrperformancetypekey: null,
        speacialeducationrestrictivekey: null,
        lastattendeddate: null,
        schoolexitcomments: null,
        disciplinaryactioncomments: null,
      }),
      personAccomplishment: this._formBuilder.group({
        personaccomplishmentid: [null],
        highestgradetypekey: [null],
        accomplishmentdate: [null],
        isrecordreceived: [null],
        receiveddate: [null]
      }),

      personEducationTesting: this._formBuilder.group({
        personeducationtestingid: [null],
        testingtypekey: [null],
        readinglevel: [null],
        readingtestdate: [null],
        testingprovider: [null]
      })

    });
    if (this._personInfoService.getClosed()) {
     this.personEducationFormGroup.disable();
    }
  }

  private loadDropDown() {
    this.stateDropdownItems$ = this._commonDropdownService.getPickListByName('state');
    this.countyDropDownItems$ = this._commonDropdownService.getPickListByName('county');
    this.schoolTypeDropdownItems$ = this._commonDropdownService.getPickListByName('schooltype');
    this.schoolSettingDropdownItems$ = this._commonDropdownService.getPickListByName('schoolsetting');
    this.transportmodetypeDropdownItems$ = this._commonDropdownService.getPickListByName('educationaltransportation');
    this.typeofclassDropdownItems$ = this._commonDropdownService.getPickListByName('classtype');
    this.specialEducationInDensityDropdownItems$ = this._commonDropdownService.getPickListByName('specialeducationindensity');
    this.gradeDropdownItems$ = this._commonDropdownService.getPickListByName('gradelevel');
    this.schoolExitReasonDropdownItems$ = this._commonDropdownService.getPickListByName('schoolexitreason');
    this.disciplinaryActionDropdownItems$ = this._commonDropdownService.getPickListByName('disciplinaryactions');
    this.testnameDropdownItems$ = this._commonDropdownService.getPickListByName('assessmenttest');
    this.schoolSpDropdownItems$ = this._commonDropdownService.getPickListByName('schoolspecialeducation');
    this.schoolAdjustmentDropdownItems$ = this._commonDropdownService.getPickListByName('schooladjustment');
    this.gradeDropdownItems$.subscribe((data: any) => {
      const list = ['GDO', 'GDTW', 'GDTH', 'GDFO', 'GDFI', 'GDSI',
        'GDSE', 'GDEI', 'GDNI', 'GDTE', 'GDEL', 'GDTWL', 'COL', 'KDGN',
        'NIS', 'PSET', 'PSHS', 'UNK'];
      this.gradeDropdownItems = [];
      list.forEach(item => {
        const obj = data.find(ele => ele.ref_key === item);
        this.gradeDropdownItems.push(obj);
      });
    });
  }

  loadCounty(state) {
    console.log(state);
    this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
      this.countyDropDownItems$ = Observable.of(countyList);
    });
  }

  addOrUpdatePersonEducation(type) {
    this.editflag = false;
    this.personEducationFormGroup.patchValue({ personid: this.personId });
    const data = this.personEducationFormGroup.getRawValue();
    const personEducation = [];
    const personAccomplishment = [];
    const personEducationTesting = [];
    const objectID = this._navigationUtils.getNavigationInfo().sourceID;
    const objectType = this._navigationUtils.getModuleType();
    if (type === 'personEducation') {
      data.personEducation.objectid = objectID;
      data.personEducation.objecttype = objectType;
      personEducation.push(data.personEducation);
    } else if (type === 'personAccomplishment') {
      personAccomplishment.push(data.personAccomplishment);
    } else if (type === 'personEducationTesting') {
      personEducationTesting.push(data.personEducationTesting);
    }

    data.personEducation.firstqtrabsence = (data.firstqtrabsence) ? 1 : 0;
    data.personEducation.secondqtrabsence = (data.secondqtrabsence) ? 1 : 0;
    data.personEducation.thirdqtrabsence = (data.thirdqtrabsence) ? 1 : 0;
    data.personEducation.fourthqtrabsence = (data.fourthqtrabsence) ? 1 : 0;

    data.submittype = type;
    data.personEducation = personEducation;
    data.personAccomplishment = personAccomplishment;
    data.personEducationTesting = personEducationTesting;

    this._educationInfoService.saveEducationInfo(data).subscribe(response => {
      // this._educationInfoService.getEducationList();
      // this.personEducationFormGroup.reset();
      // this.addflag.emit(true);
      this._alertSevice.success('Education Details Saved Successfully.');
      this._educationInfoService.isShowAddEducationEnabled(false);
    },
      error => {
        this._alertSevice.error('Error In Saving Education Details.');
      }
    );
  }

  getEducation() {
    this.personEducationFormGroup.controls['personid'].setValue = this._personInfoService.getPersonId();
  }

  incrementButton(FormControlName: any) {
    // let value = this.personEducationFormGroup.controls.education.get(FormControlName).value;
    // this.personEducationFormGroup.controls.education.get(FormControlName).setValue = value +1 ;
    // this.personEducationFormGroup.controls.education['controls'][FormControlName].value++;
    this.personEducationFormGroup.controls.personEducation.patchValue({ FormControlName: 10 });
    // console.log(this.personEducationFormGroup.controls.education['controls'][FormControlName].value);
  }

  decrementButton(FormControlName: any) {

  }

  editEducation(model) {
    console.log(model);
    this.editflag = true;
    this.isEdited = true;
    const elmnt = document.getElementById('tab_content');
    elmnt.scrollIntoView();
    // setTimeout(() => {
    if (model.personEducation) {
      this.personEducationFormGroup.controls['personEducation'].patchValue(model.personEducation);
    }
    if (model.personAccomplishment) {
      this.personEducationFormGroup.controls['personAccomplishment'].patchValue(model.personAccomplishment);
    }
    if (model.personEducationTesting) {
      this.personEducationFormGroup.controls['personEducationTesting'].patchValue(model.personEducationTesting);
    }
    // }, 100);
  }

  cancelForm(type: any) {
    this.editflag = false;
    if(type === 'personEducation' && this.isEdited === false){
      this.personEducationFormGroup.controls['personEducation'].reset();
    }else if(type === 'personAccomplishment' && this.isEdited === false){
      this.personEducationFormGroup.controls['personAccomplishment'].reset();
    }else if(type === 'personEducationTesting' && this.isEdited === false){
      this.personEducationFormGroup.controls['personEducationTesting'].reset();
    }else{
      this._educationInfoService.isShowAddEducationEnabled(false);
    } 
  }

  allowPhoneNo(el) {
    if (el.target.value !== '') {
      el.target.value =  el.target.value.replace(/[a-zA-Z&\/\\#,$~%.'":*?<>{}]/g, '');
      return el.target.value;
    }
    return '';
  }

  checkNumber(el) {
    if (el.target.value !== '') {
      el.target.value =  el.target.value.replace(/[^0-9]/g, '');
      return el.target.value;
    }
    return '';
  }
  checkDecimal(el) {
    if (el.target.value !== '') {
      el.target.value =  el.target.value.replace(/[^0-9.]/g, '');
      return el.target.value;
    }
    return '';
  }
  startDateChanged(startDate: string) {
    console.log('from date', startDate);
    this.toMinDate = new Date(startDate);
  }
  endDateChanged(endDate: string) {
    console.log('from date', endDate);
    this.maximumDate = new Date(endDate);
  }
}
