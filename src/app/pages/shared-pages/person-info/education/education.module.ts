import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EducationRoutingModule } from './education-routing.module';
import { EducationComponent } from './education.component';
import { AddEducationComponent } from './add-education/add-education.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ShareFeaturesModule } from '../share-features/share-features.module';
import { HttpClientModule } from '@angular/common/http';
import {EducationInfoService} from './education.service';
import { ListEducationComponent } from './list-education/list-education.component'
import { NgxMaskModule } from 'ngx-mask';


@NgModule({
  imports: [
    CommonModule,
    EducationRoutingModule,
    FormMaterialModule,
    HttpClientModule,
    ShareFeaturesModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [EducationComponent, AddEducationComponent, ListEducationComponent],
  providers: [EducationInfoService]
})
export class EducationModule {
 }
