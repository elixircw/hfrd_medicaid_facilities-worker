import { Injectable } from '@angular/core';
import { NavigationUtils } from '../../../_utils/navigation-utils.service';
import { Observable } from 'rxjs/Observable';
import { AppConstants } from '../../../../@core/common/constants';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { PersonInfoService } from '../person-info.service';
import { Education,EmailType, PhoneType } from '../../../admin/general/_entities/general.data.models';

@Injectable()
export class EducationInfoService {
  personId: any;
  public educationPersonType$ = new Observable<Education[]>();
  public isShowAddEducation$ = new Subject<any>();
  public selectedEducation$ = new Subject<any>();

  constructor(private _navigationUtils: NavigationUtils,
    private _commonHttpService: CommonHttpService,
    private _personService: PersonInfoService) {
      this.personId = (this._personService.getPersonId()) ? this._personService.getPersonId() : '';
  }

  saveEducationInfo(data) {
    return this._commonHttpService.create(data, 'personeducation/addupdate');
  }

  getEducationList() {
    const source = this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest(
          {
            method: 'get',
            where: { personid: this._personService.getPersonId() },
            page: 1, // this.paginationInfo.pageNumber,
            limit: 10// this.paginationInfo.pageSize,
          }),
        'personeducation/educationlist' + '?filter'
      ).map((result: any) => {
        return {
          data: result,
          count: result.length > 0 ? result[0].totalcount : 0
          // canDisplayPager: result.length > 0 ? result[0].totalcount > this.paginationInfo.pageSize : false
        };
      }).share();

    this.educationPersonType$ = source.pluck('data');
  }

  isShowAddEducationEnabled(value) {
    this.isShowAddEducation$.next(value);
  }

  getEducation(model) {
    this.selectedEducation$.next(model);
  }

}