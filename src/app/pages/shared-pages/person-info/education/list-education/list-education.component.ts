import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { ActivatedRoute } from '@angular/router';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { Observable } from 'rxjs/Observable';
import { Education, EmailType , PhoneType } from '../../../../admin/general/_entities/general.data.models';
import { PersonInfoService } from '../../person-info.service';
import { EducationInfoService } from '../education.service';
import { GenericService, AlertService, CommonDropdownsService } from '../../../../../@core/services';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';


@Component({
  selector: 'list-education',
  templateUrl: './list-education.component.html',
  styleUrls: ['./list-education.component.scss']
})
export class ListEducationComponent implements OnInit {
  personId: any;
  educationPersonList = [];
  deleteEducationId: any;

  constructor(private _commonHttpService: CommonHttpService,
    private _educationService: EducationInfoService,
    private _commonDropdownService: CommonDropdownsService,
    private _personService: PersonInfoService,
    private _alertService: AlertService) { }

  ngOnInit() {
    this.personId = (this._personService.getPersonId()) ? this._personService.getPersonId() : '';
    this._educationService.getEducationList();
    this.listenEducationListChange();
  }

  listenEducationListChange() {
    this._educationService.educationPersonType$.subscribe( (data) => {
      this.educationPersonList = [];
      if (data['personEducation'] && data['personEducation'].length && data['personEducation'].length > 0) {
        data['personEducation'].map((x,index)=>{this.educationPersonList[index]={personEducation:x}});
        this.personId = data['Pid'];
      }

      if (data['personAccomplishment'] && data['personAccomplishment'].length && data['personAccomplishment'].length > 0) {
        data['personAccomplishment'].map((x,index)=>{this.educationPersonList[index].personAccomplishment = x});
        this.personId = data['Pid'];
      }


      if (data['personEducationTesting'] && data['personEducationTesting'].length && data['personEducationTesting'].length > 0 ) {
        data['personEducationTesting'].map((x,index)=>{this.educationPersonList[index].personEducationTesting = x});
        this.personId = data['Pid'];
        // console.log(this.educationPersonList);
      }
    });
  }

  editEducation(modal) {
    this._educationService.isShowAddEducationEnabled(true);
    setTimeout( () => {
      this._educationService.getEducation(modal);
    }, 500);
  }

  confirmDelete(personaddressid) {
    this.deleteEducationId = personaddressid;
    (<any>$('#delete-popup')).modal('show');
  }

  declineDelete() {
    this.deleteEducationId = null;
    (<any>$('#delete-popup')).modal('hide');
  }

  deleteAddress(i: number) {
    if (this.deleteEducationId) {
      this._commonHttpService.endpointUrl = CommonUrlConfig.EndPoint.PERSON.EDUCATION.SCHOOL.DeleteSchool;
      this._commonHttpService.remove(this.deleteEducationId).subscribe(
        response => {
          console.log(response);
        //  this.addressPersonList.splice(i, 1);
         // this.addressPersonType$ = Observable.of(this.addressPersonType);
          (<any>$('#delete-popup')).modal('hide');
          this._alertService.success('Education deleted successfully..');
          this._educationService.getEducationList();
          setTimeout( () => {
            this.listenEducationListChange();
          }, 1000);
          // this.personAddressForm.reset();
          // this.personAddressForm.enable();
        },
        error => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    } else {
     // this.addressPersonList.splice(i, 1);
      // this.addressPersonType$ = Observable.of(this.addressPersonType);
      (<any>$('#delete-popup')).modal('hide');
     // this._alertService.success('Address deleted successfully');
    //  this.getAddressListPage(1);
    }
  }

  // getDesc(key) {
  //   this._commonDropdownService.getPickListByName('schooltype').subscribe(data =>{
  //     data.forEach(item =>{
  //       if(item.ref_key == key )
  //       return item.description;
  //     })
  //   })
  // }

}


