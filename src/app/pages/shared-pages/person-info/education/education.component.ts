import { Component, OnInit } from '@angular/core';
import { EducationInfoService } from './education.service';
import { PersonInfoService } from '../person-info.service';


@Component({
  selector: 'education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  isShowAddEmployment = false;
  Flag =  true;
  constructor(
    private _educationService: EducationInfoService,
    private _personInfoService: PersonInfoService
  ) { }

  ngOnInit() {
    this.listenToEnableAddEmployment();
  }

  enableAddEmployment() {
   this.isShowAddEmployment = !this.isShowAddEmployment;
   console.log(this.isShowAddEmployment);
  }

  listenToEnableAddEmployment() {
    this._educationService.isShowAddEducation$.subscribe((data) => {
      this.isShowAddEmployment = (data) ? true : false;
    });
  }


  educationSubmit(flag) {
    if (flag) {
      this.isShowAddEmployment = false;
      this.Flag = false;
      const _this = this;
      setTimeout(() => {
        _this.Flag = true;
      });
    }

  }

}
