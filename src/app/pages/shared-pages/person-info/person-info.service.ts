import { Injectable } from '@angular/core';
import { NavigationUtils } from '../../_utils/navigation-utils.service';
import { Observable } from 'rxjs/Observable';
import { AppConstants } from '../../../@core/common/constants';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService } from '../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { CaseWorkerUrlConfig } from '../../case-worker/case-worker-url.config';
import * as moment from 'moment';
import { ObjectUtils } from '../../../@core/common/initializer';
import { SessionStorageService } from '../../../@core/services/storage.service';
import { IntakeStoreConstants } from '../../newintake/my-newintake/my-newintake.constants';

@Injectable()
export class PersonInfoService {

  personInfo: any;
  workInfo: any;
  assetInfo: any;
  incomeInfo: any;
  setPersonFlag: boolean = false;
  setpersonid: any;
  empActionText: string;
  aliasList = [];
  isClosed = false;
  public personInfoListener$ = new Subject<any>();
  public personActionListener$ = new Subject<any>();
  public personInfoWorkListener$ = new Subject<any>();
  public workInfoListener$ = new Subject<any>();
  public assetInfoListener$ = new Subject<any>();
  public incomeInfoListener$ = new Subject<any>();
  public personDobListener$ = new Subject<any>();

  constructor(private _navigationUtils: NavigationUtils,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private storage: SessionStorageService) {
      this.setPersonFlag = false;
      this.empActionText = 'ADD';
  }
  getPersonDetails(): Observable<any> {

    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: this._navigationUtils.getPersonRequestParam()
        }),
        'People/getpersondetailcw?filter'
      );
  }

  setPersonId(personId) {
    this.setpersonid = personId;
    this.setPersonFlag = true;
  }

  resetPersonId() {
    this.setpersonid = null;
    this.setPersonFlag = false;
  }

  getPersonId() {
    if (this.setPersonFlag === true) {
      return this.setpersonid;
    }
    return this._navigationUtils.getPersonRequestParam().personid;
  }

  updatePersonId(personid: string) {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    navigationInfo.personId = personid;
    navigationInfo.action = AppConstants.ACTIONS.EDIT;
    if (this.getIntakeNumber()) {
      navigationInfo.sourceID = this.getIntakeNumber();
    }
    this._navigationUtils.setNavigationInfo(navigationInfo);
  }

  getSearchData() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    if (navigationInfo && navigationInfo.searchData) {
      return navigationInfo.searchData;
    }
    return null;
  }
  getTitle(): string {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    if (navigationInfo && navigationInfo.action) {
      return navigationInfo.action;
    }
    return null;
  }


  isNew() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    if (navigationInfo && navigationInfo.action && navigationInfo.action === AppConstants.ACTIONS.ADD) {
      return true;
    }
    return false;
  }

  goBack() {
    this._navigationUtils.loadPreviousState();
  }
  setPersonDob(Dob) {
    this.personDobListener$.next(Dob);
  }
  setPersonInfo(personInfo) {
    this.personInfo = personInfo;
    this.personInfoListener$.next(this.personInfo);
  }
  getPersonInfo() {
    return this.personInfo; 
  }
  getCaseNumber() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    if (navigationInfo && navigationInfo.source === AppConstants.CASE_TYPE.CPS_CASE) {
      return navigationInfo.data.caseNumber;
    } else {
      return null;
    }
  }
  getCaseId() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    console.log('case-details-------');
    console.log(navigationInfo);
    if (navigationInfo && navigationInfo.source === AppConstants.CASE_TYPE.CPS_CASE) {
      return navigationInfo.sourceID;
    } else {
      return null;
    }
  }

  getServiceCaseId() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    console.log('case-details-------');
    console.log(navigationInfo);
    if (navigationInfo && navigationInfo.source === AppConstants.CASE_TYPE.SERVICE_CASE) {
      return navigationInfo.sourceID;
    } else {
      return null;
    }
  }

  getIntakeNumberByServiceCaseID(): Observable<any> {
    return this._commonHttpService.getArrayList({
      where: {
         serviceCaseId: this.getServiceCaseId(),
         activeflag: 1
      }, method: 'get'
    }, 'servicecase/getintakeserviceid?filter');

  }

  getIntakeNumber() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    if (navigationInfo && navigationInfo.source === AppConstants.CASE_TYPE.INTAKE) {
      return navigationInfo.sourceID;
    } else if (navigationInfo && (navigationInfo.source === AppConstants.MODULE_TYPE.PUBLIC_PROVIDER
      || navigationInfo.source === AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_REFERRAL
      || navigationInfo.source === AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_APPLICATION)) {
      return navigationInfo.sourceID;
    } else {
      return null;
    }
  }

  getPurposeId() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    if (navigationInfo && navigationInfo.data) {
      return navigationInfo.data.purposeId;
    } else {
      return null;
    }
  }
  deletePersonnarrativeDetails(data) {
    const workdetails = {
      personemploymentid: data.personemploymentid,
      delete: 1
    };
    const result = {
      pid: this.getPersonId(),
      intakeserviceid: this.getCaseId(),
      workdetails: workdetails,
      intakenumber: this.getIntakeNumber()
    };
    return this._commonHttpService.create(result, 'People/addupdatepersonnarrative');
  }
  deletePersonworkDetails(data) {
    const workdetails = {
      personworkcarrergoalid: data.personworkcarrergoalid,
      personemploymentid: data.personemploymentid,
      personemployerdetailid: data.personemployerdetailid,
      delete: 1
    };
    const result = {
      pid: this.getPersonId(),
      intakeserviceid: this.getCaseId(),
      workdetails: workdetails,
      intakenumber: this.getIntakeNumber()
    };
    return this._commonHttpService.create(result, 'People/addupdatepersonwork');
  }
  savePersonnarrativeDetails(data) {
    data.personemploymentid = data.personemploymentid;
    const result = {
      pid: this.getPersonId(),
      intakeserviceid: this.getCaseId(),
      workdetails: data,
      intakenumber: this.getIntakeNumber()
    };
    return this._commonHttpService.create(result, 'People/addupdatepersonnarrative');
  }
  saveSupportOrderDetails(data) {
    data.addupdatefinancesupportorder.personid = this.getPersonId();
    return this._commonHttpService.create(data, 'People/addupdatefinancesupportorder');
  }
  saveAssetDetails(data) {
    data.where.personid = this.getPersonId();
    return this._commonHttpService.create(data, 'People/personaddupdatefinanceasset');
  }
  savePersonIncomeDetails(data) {
    data.addupdatefinanceincome.personid = this.getPersonId();
    data.addupdatefinanceincome.deemedparent.verifiedparentpersonid = this.getPersonId();
    return this._commonHttpService.create(data, 'People/addupdatefinanceincome');
  }
  savePersonworkDetails(data) {
    console.log(data);
    const workdetails = {
      personworkcarrergoalid: data.personworkcarrergoalid,
      personemploymentid: data.personemploymentid,
      personemployerdetailid: data.personemployerdetailid,
      delete: (data.personemployerdetailid) ? 2 : 0,
      employerdetails: {
        employername: data.employername,
        currentemployer: data.currentemployer,
        noofhours: data.noofhours,
        duties: data.duties,
        startdate: data.startdate || null,
        enddate: data.enddate || null,
        reasonforleaving: data.reasonforleaving
      },
      employeraddress: {
        address1: data.address1,
        address2: data.address2,
        cityname: data.cityname,
        statetypekey: data.statetypekey,
        countytypekey: data.countytypekey,
        zip5no: data.zip5no
      },
      occupationdetails: {
        clienttitle: data.clienttitle,
        emplymenttypekey: data.emplymenttypekey,
        workschedule: data.workschedule,
        income: data.income || null,
        wagefreqtypekey: data.wagefreqtypekey
      },
      careergoaldetails: {
        careergoals: data.careergoals
      },
      supervisordetails: {
        supervisorfirstname: data.supervisorfirstname,
        supervisormiddlename: data.supervisormiddlename,
        supervisorlastname: data.supervisorlastname,
        supervisorsuffixtypekey: data.supervisorsuffixtypekey,
        supervisorprefixtypekey: data.supervisorprefixtypekey
      },
      contactdetails: {
        email: data.emailList,
        workphone: data.phoneNumberList
      }
    };
    const result = {
      pid: this.getPersonId(),
      intakeserviceid: this.getCaseId(),
      workdetails: workdetails,
      intakenumber: this.getIntakeNumber()
    };
    return this._commonHttpService.create(result, 'People/addupdatepersonwork');

  }
  savePersonDetails(personData, intakeData) {
    personData.personid = this.getPersonId();
    const personRole = [];
    let maritalstatus;
    // personData.fetalalcoholspctrmdisordflag = (personData.fetalalcoholspctrmdisordflag) ? 1 : 0;
    personData.drugexposednewbornflag = (personData.drugexposednewbornflag) ? 1 : 0;
    personData.probationsearchconductedflag = (personData.probationsearchconductedflag) ? 1 : 0;
    // personData.sexoffenderregisteredflag = (personData.sexoffenderregisteredflag) ? 1 : 0;
    personData.safehavenbabyflag = personData.safehavenbabyflag ? 'true' : 'false';
    personData.everbeenadoptedflag = (personData.everbeenadoptedflag) ? 1 : 0;
    personData.isqualifiedalien = (personData.isqualifiedalien === 'null') ? '0' : personData.isqualifiedalien;
    personData.citizenalenageflag = (personData.citizenalenageflag === 'null') ? '0' : personData.citizenalenageflag;
    personData.isapproxdob = (personData.isapproxdob) ? 1 : 0;
    personData.isapproxdod = (personData.isapproxdod) ? 1 : 0;

    // if (personData.roles && personData.roles.length) {
    //   personRole = personData.roles.map(roleKey => {
    //     return {
    //       'personroletypeid': null,
    //       'personroleid': null,
    //       'roletype': roleKey
    //     };
    //   });
    // }
    if (personData.maritalstatustypekey) {
      maritalstatus = {
        // 'informallivingcomments': 'PARENT',
        // 'adrhomephone': '1234524',
        // 'statustypekey': personData.maritalstatustypekey,
        // 'marriageplace': 'marriageplace',
        // 'divorceplace': 'divorceplace',
        // 'firstname': 'firstname'

        'statustypekey': personData.maritalstatustypekey,
        'marriageplace': personData.marriageplace,
        'divorceplace': personData.divorceplace,
        'maritalstartdate': personData.maritalstartdate,
        'maritalenddate': personData.maritalenddate,
        'childrenno': personData.numberofchildren,
        'maritalcomments': personData.maritalcomments,
        'spouseprefix': personData.spouseprefix,
        'spousefirstname': personData.spousefirstname,
        'spousemiddlename': personData.spousemiddlename,
        'spouselastname': personData.spouselastname,
        'spousesuffix': personData.spousesuffix,
        'spousehomenumber': personData.spousehomenumber,
        'spouseofficenumber': personData.spouseofficenumber,
        'spouseofficeextension': personData.spouseofficeextension,
        'spouseaddress1': personData.spouseaddress1,
        'spouseAddress2': personData.spouseAddress2,
        'spousecity': personData.spousecity,
        'spousestate': personData.spousestate,
        'spousecounty': personData.spousecounty,
        'spousezipcode': personData.spousezipcode
      };
    }
    // personData.personRole = personRole;
    personData.maritalstatus = maritalstatus;
    personData.intakenumber = this.getIntakeNumber();

    personData.caseInfo = this.getCaseInfo();


    const data = {
      pid: this.getPersonId(),
      intakeserviceid: this.getCaseId(),
      intakenumber: this.getIntakeNumber(),
      persondetails: personData,
      servicecaseid: null
     };

    if (!personData.intakenumber && intakeData) {
          data.intakenumber = intakeData.intakenumber;
          personData.intakenumber = intakeData.intakenumber;
          data.intakeserviceid = intakeData.intakeserviceid;
          data.servicecaseid = this.getServiceCaseId();
          personData.servicecaseid = this.getServiceCaseId();
     }
    return this._commonHttpService.create(data, 'People/addupdatepersoncw');
   }
  setWorkInfo(workInfo) {
    this.workInfo = workInfo;
    this.workInfoListener$.next(this.workInfo);
  }
  setAssetInfo(assetInfo) {
    this.assetInfo = assetInfo;
    this.assetInfoListener$.next(this.assetInfo);
  }
  setIncomeInfo(incomeInfo) {
    this.incomeInfo = incomeInfo;
    this.incomeInfoListener$.next(this.incomeInfo);
  }
  reloadworkDetails(flag) {
    this.personInfoWorkListener$.next(flag);
  }

  getRoleList() {
    return this._commonHttpService.getArrayList(
      {
        where: {
          activeflag: 1,
          datypeid: this.getPurposeId() // need to add purpose for both intake and case
        },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
        .UserActorTypeUrl + '?filter'
    );
  }

  calculateAge(dob) {
    let age = 0;
    if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
      const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
      age = moment().diff(rCDob, 'years');
    }
    return age;
  }

  searchPresonWithCritera(searchCriteria) {
    ObjectUtils.removeEmptyProperties(searchCriteria);
    return this._commonHttpService
      .getPagedArrayList(
        {
          where: searchCriteria,
          method: 'post'
        },
        'globalpersonsearches/getPersonSearchData'
      );
  }

  getCaseInfo() {
    const navigationInfo = this._navigationUtils.getNavigationInfo();
    const caseInfo = {objectType : null, objectNumber: null};
    if (navigationInfo && navigationInfo.source === AppConstants.CASE_TYPE.INTAKE) {
      caseInfo['objectType'] = 'Intake';
      caseInfo['objectNumber'] = navigationInfo.sourceID;
    } else if (navigationInfo && navigationInfo.source === AppConstants.CASE_TYPE.SERVICE_CASE) {
      caseInfo['objectType'] = 'Service Case';
      caseInfo['objectNumber'] = navigationInfo.data.caseNumber;
    } else if (navigationInfo && navigationInfo.source === AppConstants.CASE_TYPE.CPS_CASE) {
      caseInfo['objectType'] = 'Case';
      caseInfo['objectNumber'] = navigationInfo.data.caseNumber;
    } else {
      caseInfo['objectType'] = 'OTHER';
      caseInfo['objectNumber'] = navigationInfo.sourceID;
    }

    return caseInfo;
  }

  getClosed() {
    const currentStatus = this._dataStoreService.getData(IntakeStoreConstants.INTAKE_STATUS);
    const da_status = this.storage.getItem('da_status');
    if (da_status === 'Closed' || da_status === 'Completed' || currentStatus === 'Closed' || currentStatus === 'Completed') {
        return true;
    } else {
        return false;
    }
  }

}
