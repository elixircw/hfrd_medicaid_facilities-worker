import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonInfoRoutingModule } from './person-info-routing.module';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { PersonInfoComponent } from './person-info.component';
import { NavigationUtils } from '../../_utils/navigation-utils.service';
import { PersonInfoService } from './person-info.service';
import { ImageCropperModule } from 'ngx-image-cropper';
import { IntakeUtils } from '../../_utils/intake-utils.service';
import { ShareFeaturesModule } from './share-features/share-features.module';
import { PersonHealthInfoModule } from './person-health/person-health-info.module';


@NgModule({
  imports: [
    CommonModule,
    PersonInfoRoutingModule,
    FormMaterialModule,
    ImageCropperModule,
    PersonHealthInfoModule,
    ShareFeaturesModule    
  ],
  declarations: [PersonInfoComponent]
 
})
export class PersonInfoModule { }
