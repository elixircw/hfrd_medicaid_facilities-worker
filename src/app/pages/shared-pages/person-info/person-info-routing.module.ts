import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonInfoComponent } from './person-info.component';



const routes: Routes = [
  {
    path: '',
    component: PersonInfoComponent,
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'profile',
        loadChildren: '././person-profile/person-profile.module#PersonProfileModule'
      },
      {
        path: 'address',
        loadChildren: '././address-details/address-details.module#AddressDetailsModule'
      },
      {
        path: 'contacts',
        loadChildren: './contact-info/contact-info.module#ContactInfoModule'
      },
      {
        path: 'education',
        loadChildren: './education/education.module#EducationModule'
      },
      {
        path: 'health',
        loadChildren: './person-health/person-health-info.module#PersonHealthInfoModule'
      },
      {
        path: 'employment',
        loadChildren: './employment-profile/employment-profile.module#EmploymentProfileModule'
      },
      {
        path: '18-21',
        loadChildren: './eighteen21/eighteen21.module#Eighteen21Module'
      },
      {
        path: 'finance',
        loadChildren: './finance/person-finance.module#PersonFinanceModule'
      },
      {
        path: 'military',
        loadChildren: './military/military.module#MilitaryModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonInfoRoutingModule { }
