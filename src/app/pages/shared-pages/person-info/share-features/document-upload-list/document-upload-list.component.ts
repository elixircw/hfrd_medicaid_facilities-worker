import { Component, OnInit, Input } from '@angular/core';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { config } from '../../../../../../environments/config';
import { AppConfig } from '../../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { AlertService, AuthService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { HttpHeaders } from '@angular/common/http';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { PersonInfoService } from '../../person-info.service';

@Component({
  selector: 'document-upload-list',
  templateUrl: './document-upload-list.component.html',
  styleUrls: ['./document-upload-list.component.scss']
})
export class DocumentUploadListComponent implements OnInit {

  @Input() uploadedFiles;
  @Input() uploadNumber;
  @Input() colSize = 'col-md-12 col-lg-6 col-sm-12';
  token: AppUser;
  deleteAttachmentIndex: number;
  constructor(private _alertService: AlertService,
    private _authService: AuthService,
    private _uploadService: NgxfUploaderService,
    private _personInfoService: PersonInfoService) { }

  ngOnInit() {
    this.token = this._authService.getCurrentUser();
  }

  uploadFile(file: File | FileError): void {
    if (!(file instanceof Array)) {
      return;
    }
    file.map((item, index) => {
      const fileExt = item.name
        .toLowerCase()
        .split('.')
        .pop();
      if (
        fileExt === 'mp3' ||
        fileExt === 'ogg' ||
        fileExt === 'wav' ||
        fileExt === 'acc' ||
        fileExt === 'flac' ||
        fileExt === 'aiff' ||
        fileExt === 'mp4' ||
        fileExt === 'mov' ||
        fileExt === 'avi' ||
        fileExt === '3gp' ||
        fileExt === 'wmv' ||
        fileExt === 'mpeg-4' ||
        fileExt === 'pdf' ||
        fileExt === 'txt' ||
        fileExt === 'docx' ||
        fileExt === 'doc' ||
        fileExt === 'xls' ||
        fileExt === 'xlsx' ||
        fileExt === 'jpeg' ||
        fileExt === 'jpg' ||
        fileExt === 'png' ||
        fileExt === 'ppt' ||
        fileExt === 'pptx' ||
        fileExt === 'gif'
      ) {
        this.uploadedFiles.push(item);
        const uindex = this.uploadedFiles.length - 1;
        if (!this.uploadedFiles[uindex].hasOwnProperty('percentage')) {
          this.uploadedFiles[uindex].percentage = 1;
        }

        this.uploadAttachment(uindex);
        const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
        const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
        if (audio_ext.indexOf(fileExt) >= 0) {
          this.uploadedFiles[uindex].attachmenttypekey = 'Audio';
        } else if (video_ext.indexOf(fileExt) >= 0) {
          this.uploadedFiles[uindex].attachmenttypekey = 'Video';
        } else {
          this.uploadedFiles[uindex].attachmenttypekey = 'Document';
        }
      } else {
        // tslint:disable-next-line:quotemark
        this._alertService.error(fileExt + " format can't be uploaded");
        return;
      }
    });
  }
  uploadAttachment(index) {
    console.log('check');
    const workEnv = config.workEnvironment;
    let uploadUrl = '';
    if (workEnv === 'state') {
      uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
        + '?access_token=' + this.token.id + '&' + 'srno=' + this.uploadNumber + '&' + 'docsInfo='; // Need to discuss about the docsInfo
      console.log('state', uploadUrl);
    } else {
      uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
        '&' + 'srno=' + this.uploadNumber;
      console.log('local', uploadUrl);
    }

    this._uploadService
      .upload({
        url: uploadUrl,
        headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
        filesKey: ['file'],
        files: this.uploadedFiles[index],
        process: true,
      })
      .subscribe(
        (response) => {
          if (response.status) {
            this.uploadedFiles[index].percentage = response.percent;
          }
          if (response.status === 1 && response.data) {
            const doucumentInfo = response.data;
            doucumentInfo.documentdate = doucumentInfo.date;
            doucumentInfo.title = doucumentInfo.originalfilename;
            doucumentInfo.objecttypekey = 'person';
            doucumentInfo.rootobjecttypekey = 'person';
            doucumentInfo.activeflag = 1;
            doucumentInfo.servicerequestid = null;
            this.uploadedFiles[index] = { ...this.uploadedFiles[index], ...doucumentInfo };
            console.log(index, this.uploadedFiles[index]);
          }

        }, (err) => {
          console.log(err);
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          this.uploadedFiles.splice(index, 1);
        }
      );
  }
  deleteAttachment() {
    this.uploadedFiles.splice(this.deleteAttachmentIndex, 1);
    (<any>$('#delete-attachment-popup')).modal('hide');
  }

  downloadFile(s3bucketpathname) {
    const workEnv = config.workEnvironment;
    let downldSrcURL;
    if (workEnv === 'state') {
      downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
    } else {
      // 4200
      downldSrcURL = s3bucketpathname;
    }
    console.log('this.downloadSrc', downldSrcURL);
    window.open(downldSrcURL, '_blank');
  }

  confirmDeleteAttachment(index: number) {
    (<any>$('#delete-attachment-popup')).modal('show');
    this.deleteAttachmentIndex = index;
  }

}
