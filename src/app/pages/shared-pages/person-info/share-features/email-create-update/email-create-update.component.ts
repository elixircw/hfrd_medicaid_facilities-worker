import { Component, OnInit, Input, EventEmitter, Output  } from '@angular/core';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';

@Component({
  selector: 'email-create-update',
  templateUrl: './email-create-update.component.html',
  styleUrls: ['./email-create-update.component.scss']
})
export class EmailCreateUpdateComponent implements OnInit {

  @Input() list: any[];
  @Output() emiallistemit: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.list);

  }


  emailvalue(event)
  {
    console.log(event, this.list);
    this.emiallistemit.emit(this.list);
  }


}
