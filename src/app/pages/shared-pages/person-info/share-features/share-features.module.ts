import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneCreateUpdateComponent } from './phone-create-update/phone-create-update.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxMaskModule } from 'ngx-mask';
import { EmailCreateUpdateComponent } from './email-create-update/email-create-update.component';
import { SmartyStreetAddressComponent } from './smarty-street-address/smarty-street-address.component';
import { DocumentUploadListComponent } from './document-upload-list/document-upload-list.component';
import { NgxfUploaderModule } from 'ngxf-uploader';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    NgxMaskModule.forRoot(),
    NgxfUploaderModule.forRoot(),
  ],
  declarations: [PhoneCreateUpdateComponent, EmailCreateUpdateComponent, SmartyStreetAddressComponent, DocumentUploadListComponent],
  exports: [PhoneCreateUpdateComponent, EmailCreateUpdateComponent, SmartyStreetAddressComponent, DocumentUploadListComponent]
})
export class ShareFeaturesModule { }
