import { Component, OnInit, Input } from '@angular/core';
import { CommonHttpService, CommonDropdownsService } from '../../../../../@core/services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'smarty-street-address',
  templateUrl: './smarty-street-address.component.html',
  styleUrls: ['./smarty-street-address.component.scss']
})
export class SmartyStreetAddressComponent implements OnInit {

  @Input() address: { address1: string, address2: string, city: string, state: string, zipcode: string, county: string, disable?: false};
  suggestedAddress$: Observable<any[]>;
  stateDropdownItems$: Observable<any[]>;
  countyDropDownItems$: Observable<any[]>;
  constructor(private _commonHttpService: CommonHttpService,
    private _commonDropdownService: CommonDropdownsService) { }

  ngOnInit() {
    this.stateDropdownItems$ = this._commonDropdownService.getPickListByName('state');
    this.countyDropDownItems$ = this._commonDropdownService.getPickListByName('county');
  }

  loadCounty(sateKey) {
    console.log(sateKey);
    this._commonDropdownService.getPickListByMdmcode(sateKey).subscribe(countyList => {
      console.log('cl', countyList);
      this.countyDropDownItems$ = Observable.of(countyList);
    });
  }

  selectedAddress(suggestion) {
    console.log('suggested Address', suggestion);
    this.address.address1 = suggestion.streetLine ? suggestion.streetLine : '';
    this.address.city = suggestion.city ? suggestion.city : '';
    this.address.state = suggestion.state ? suggestion.state : '';
    this.loadCounty(this.address.state);
    const addressInput = {
      street: suggestion.streetLine ? suggestion.streetLine : '',
      street2: '',
      city: suggestion.city ? suggestion.city : '',
      state: suggestion.state ? suggestion.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
          {
              method: 'post',
              where: addressInput
          },
          'People/validateaddress'
      )
      .subscribe(
          (result) => {
              if (result[0].analysis) {
                this.address.zipcode = result[0].components.zipcode ? result[0].components.zipcode : '';
                this.address.county = result[0].metadata.countyName ? result[0].metadata.countyName : '';
               }
              } ,
              (error) => {
                  console.log(error);
              }
      );
  }
  getSuggestedAddress() {
    console.log(this.address);
    if (this.address && this.address.address1 &&
      this.address.address1.length >= 3) {
      this.suggestAddress();
    }
  }
  suggestAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.address.address1,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        'People/suggestaddress'
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
          this.suggestedAddress$ = result;
        }
        }
      );
  }

}
