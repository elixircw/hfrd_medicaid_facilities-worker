import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { HealthInsuranceInformation, Health } from '../../../involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../involved-persons/_entities/involvedPersons.constants';
import { AlertService, DataStoreService, CommonHttpService,CommonDropdownsService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonInfoService } from '../../person-info.service';
import { PersonHealthService } from '../person-health.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'insurance-information-cw',
  templateUrl: './insurance-information-cw.component.html',
  styleUrls: ['./insurance-information-cw.component.scss']
})
export class InsuranceInformationCwComponent implements OnInit {
  healthinsuranceForm: FormGroup;
  modalInt: number;
  isCustomInsuranceProvider: boolean;
  editMode: boolean;
  reportMode: string;
  minDate = new Date();
  maxDate = new Date();
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  healthInsurance: HealthInsuranceInformation[] = [];
  typeofInsuranceDropDownItem$: Observable<DropdownModel[]>;
  health: Health;
  isPrimaryExists: boolean;
  isPrimaryInsurance: boolean;
  isInsuranceAvailableforPerson: boolean;
  insuranceList: any[];
  insuranceTypeList: any[];
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  uploadedFiles = [];
  uploadNumber = '123434';
  medAssistance: boolean;
  isAddEdit = false;
  personId: string;
  insuranceStartDate: string;
  dtDisable = false;
  address = { address1: null, address2: null, city: null, state: null, zipcode: null, county: null, disable: false };
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _personInfoService: PersonInfoService,
    private _healthService: PersonHealthService,
    private _commonDropdownService: CommonDropdownsService
    ) {
  }

  ngOnInit() {
    const personInfo = this._personInfoService.getPersonInfo();
    if(personInfo && personInfo.personbasicdetails) {
      this.insuranceStartDate = personInfo.personbasicdetails.dob ? personInfo.personbasicdetails.dob : null; 
    }
    this.personId =  this._personInfoService.getPersonId();
    this.loadDropDowns();
    this.medAssistance = false;
    this.editMode = false;
    this.reportMode = 'add';
    this.isPrimaryExists = false;
    this.isPrimaryInsurance = true;
    this.isInsuranceAvailableforPerson = false;
    this.modalInt = -1;
    this.isCustomInsuranceProvider = false;
    this.healthinsuranceForm = this.formbulider.group({
      ismedicaidmedicare: [null, [Validators.required]],
      insurancetype: '',
      medicalinsuranceprovider: '',
      providertype: '',
      policyholdername: [''],
      customprovidertype: '',
      providerphone: '',
      patientpolicyholderrelation: '',
      policynumber: [''],
      groupnumber: '',
      startdate: '',
      enddate: '',
      updateddate: '',
      managedcareorganization: '',
      personhealthinsuranceid: ''
    });

    if (this._personInfoService.getClosed()) {
      this.healthinsuranceForm.disable();
    }
    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.healthInsurance) {
      this.healthInsurance = this.health.healthInsurance;

      const Index = this.healthInsurance.findIndex(c => c.providertype === 'Primary');
      if (Index !== -1) {
        this.isPrimaryExists = true;
      } else {
        this.isPrimaryExists = false;
      }
    }

    this.getInsuranceInfoList();
  }

  //   <mat-option value="Medicaid"> Medicaid</mat-option>
  // <mat-option value="Medicare"> Medicare</mat-option>
  // <mat-option value="Private"> Private</mat-option>

  setInsurance(option) {
    switch (option.value) {
      case 'MEDIC': this.isPrimaryInsurance = true;
        this.healthinsuranceForm.patchValue({ 'providertype': 'Primary' });
        break;
      default: this.isPrimaryInsurance = false;
    }
  }
  setInsuranceType(option) {
    switch (option) {
      case 'Primary': this.isPrimaryExists = true;
        break;
    }
  }
  resetInsuranceType(option) {
    switch (option) {
      case 'Primary': this.isPrimaryExists = false;
        break;
    }
  }
  modifyInsuranceType(option) {
    switch (option) {
      case 'Secondary': this.isPrimaryExists = false;
        break;
    }
  }
  private setProviderType(option) {
    if (option.value === 'other' || option === 'other') {
      this.isCustomInsuranceProvider = true;
    } else {
      this.isCustomInsuranceProvider = false;
    }
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();
    // this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
    this.typeofInsuranceDropDownItem$ = this._commonDropdownService.getDropownsByTable('Type of Insurance');
  }

  loadCounty() {
    const state = this.healthinsuranceForm.get('state').value;
    const source = this._commonHttpService.getArrayList(
      {
        where: { state: state },
        order: 'countyname asc',
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
    ).map((result) => {
      return {
        counties: result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyname
            })
        )
      };
    }).share();

    this.countyDropDownItems$ = source.pluck('counties');
  }

  selectmedicalassistance(control) {
    this.medAssistance = control;
  }
  private add() {
    const currDate = new Date();
    this.healthinsuranceForm.patchValue({ 'updateddate': currDate });
    const insuranceForm = this.healthinsuranceForm.getRawValue();
    this.setInsuranceType(insuranceForm.providertype);
    const uploadInfo = {};
    uploadInfo['uploadedFiles'] = this.uploadedFiles;
    const data = {...this.healthinsuranceForm.getRawValue(), ...uploadInfo,  ...this.address};
    data.addressline1 = this.address.address1;
    data.addressline2 = this.address.address2;
    data.zip = this.address.zipcode;
    // this.healthInsurance.push(data);
    // this.health = this._dataStoreService.getData(this.constants.Health);
    // this.health.healthInsurance = this.healthinsurance;
    // this._dataStoreService.setData(this.constants.Health, this.health);
    this._healthService.saveHealth({ 'insuranceInfo': [data] }).subscribe(response => {
      this._alertSevice.success('Insurance Information Added Successfully');
      this.resetForm();
      this.getInsuranceInfoList()
    });
  }

  private enableorDisableField(field, opt) {
    if (opt) {
      this.healthinsuranceForm.get(field).enable();
     // this.healthinsuranceForm.get(field).setValidators([Validators.required]);
      this.healthinsuranceForm.get(field).updateValueAndValidity();
    } else {
      this.healthinsuranceForm.get(field).disable();
      this.healthinsuranceForm.get(field).clearValidators();
      this.healthinsuranceForm.get(field).updateValueAndValidity();
    }
  }

  isInsuranceExsists(opt) {
    this.enableorDisableField('insurancetype', opt);
    this.enableorDisableField('medicalinsuranceprovider', opt);
    this.enableorDisableField('customprovidertype', opt);
    this.enableorDisableField('providertype', opt);
    this.enableorDisableField('policynumber', opt);
    this.enableorDisableField('startdate', opt);
    this.enableorDisableField('groupnumber', opt);
    this.isInsuranceAvailableforPerson = opt;
    this.healthinsuranceForm.reset();
    setTimeout( () => { 
      this.healthinsuranceForm.patchValue({ 'ismedicaidmedicare': opt });
    }, 200);
  }

  private resetForm() {
    this.healthinsuranceForm.reset();
    this.address = { address1: null, address2: null, city: null, state: null, zipcode: null, county: null, disable: false };
    this.dtDisable = false;
    this.isInsuranceAvailableforPerson = false;
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.healthinsuranceForm.enable();
    this.isCustomInsuranceProvider = false;
    this.medAssistance = false;
    this.healthinsuranceForm.get('ismedicaidmedicare').setValidators([Validators.required]);
    this.healthinsuranceForm.get('ismedicaidmedicare').updateValueAndValidity();
  }

  private update() {
    if (this.modalInt !== -1) {
      const currDate = new Date();
      this.healthinsuranceForm.patchValue({ 'updateddate': currDate });
      const insuranceForm = this.healthinsuranceForm.getRawValue();
      this.setInsuranceType(insuranceForm.providertype);
      this.healthInsurance[this.modalInt] = this.healthinsuranceForm.getRawValue();
      const uploadInfo = {};
      uploadInfo['uploadedFiles'] = this.uploadedFiles;
      const data = {...this.healthInsurance[this.modalInt], ...uploadInfo,  ...this.address};
      data.zip = this.address.zipcode;
      this._healthService.saveHealth({ 'insuranceInfo': [data] }, 0).subscribe(response => {
        this._alertSevice.success('Insurance Information Updated Successfully');
        this.resetForm();
        this.getInsuranceInfoList();
      });
    }
  }

  private view(modal) {
    this.isAddEdit = true;
    this.isInsuranceExsists(modal.ismedicaidmedicare);
    if (modal.medicalinsuranceprovider !== null) {
      this.setProviderType(modal.medicalinsuranceprovider);
    }
    this.resetInsuranceType(modal.providertype);
    this.medAssistance = modal.medicalassistance;
    this.reportMode = 'edit';
    this.editMode = false;
    this.patchForm(modal);
    this.address.disable = true;
    this.dtDisable = true;
    this.healthinsuranceForm.disable();
  }

  private edit(modal, i) {
    this.isAddEdit = true;
    this.isInsuranceExsists(modal.ismedicaidmedicare);
    this.resetInsuranceType(modal.providertype);
    if (modal.medicalinsuranceprovider !== null) {
      this.setProviderType(modal.medicalinsuranceprovider);
    }
    this.medAssistance = modal.medicalassistance;
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.healthinsuranceForm.enable();
    this.enableorDisableField('ismedicaidmedicare', false);
  }

  private delete(modal, index) {
    const data = {
      'personhealthinsuranceid': modal.personhealthinsuranceid,
    };
    this._healthService.saveHealth({ 'insuranceInfo': [data] }, 2).subscribe(_ => {
        this._alertSevice.success('Insurance Information Deleted Successfully');
        this.resetForm();
        this.getInsuranceInfoList();
      });

  }

  private cancel() {
    this.resetForm();
  }


  private startDateChanged() {
    this.healthinsuranceForm.patchValue({ enddate: '' });
    const empForm = this.healthinsuranceForm.getRawValue();
    this.maxDate = new Date(empForm.enddate);
  }
  private endDateChanged() {
    this.healthinsuranceForm.patchValue({ startdate: '' });
    const empForm = this.healthinsuranceForm.getRawValue();
    this.minDate = new Date(empForm.startdate);
  }

  private patchForm(modal: HealthInsuranceInformation) {
    this.healthinsuranceForm.patchValue(modal);
    this.healthinsuranceForm.patchValue({
      startdate: modal.effectivedate,
      enddate: modal.expirationdate
    });
    this.address.address1 =  modal.address1;
    this.address.address2 =  modal.address2;
    this.address.city =  modal.city;
    this.address.state =  modal.state;
    this.address.county =  modal.county;
    this.address.zipcode =  modal.zip;
    this.address.disable = false;
    this.dtDisable = false;
  }

  addInsuranceInfo() {
    this.isAddEdit = true;
  }

  getInsuranceInfoList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId}
    }, 'personhealthinsurance/list?filter').subscribe(res => {
     this.healthInsurance = res ? res.data : [];
    });
  }

}
