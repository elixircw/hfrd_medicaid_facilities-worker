import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService, CommonDropdownsService, ValidationService, SessionStorageService } from '../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { HttpHeaders } from '@angular/common/http';
import { HealthConstants } from '../health-constants';
import { Health, BehaviouralHealthInfo } from '../../../involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../involved-persons/_entities/involvedPersons.constants';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';
import { AppConstants } from '../../../../../@core/common/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'behavioral-health-info-cw',
  templateUrl: './behavioral-health-info-cw.component.html',
  styleUrls: ['./behavioral-health-info-cw.component.scss']
})
export class BehavioralHealthInfoCwComponent implements OnInit {
  behaviouralHealthInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  personBehavioralHealthId: string;
  behaviouralcw: BehaviouralHealthInfo[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  uploadedFile: File;
  private token: AppUser;
  serviceDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  fearsTypeDropdownItems$: Observable<DropdownModel[]>;
  isAddEdit = false;
  personId: any;
  tobaccoSelectionEnabled: boolean;
  alchoholSelectionEnabled: boolean;
  drugSelectionEnabled: boolean;
  uploadedFiles = [];
  uploadNumber = '123434';
  address = { address1: null, address2: null, city: null, state: null, zipcode: null, county: null, disable: false };
  abuseSubstanceId: string;
  dtDisable = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService,
    private _commonDropDownService: CommonDropdownsService,
    private _sessionStorage: SessionStorageService) {
    this.token = this._authService.getCurrentUser();
  }

  ngOnInit() {
    this.personId = this._personInfoService.getPersonId();
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
    const list = this._healthService.getHealthInfoWithKey(HealthConstants.LIST_KEY.PERSON_BEHAVIOR);
    this._sessionStorage.setObj(AppConstants.GLOBAL_KEY.PERSON_NAVIGATION_INFO, {'personId': this.personId})
    // if (list && Array.isArray(list) && list.length) {
    //   this.behaviouralcw = list;
    // }
    this.getBehavioralHealthList();
  }

  initForm() {
    this.behaviouralHealthInfoForm = this._formBuilder.group({
      isbehaviouraldiagnosis: [null, [Validators.required]],
      typeofservice: '',
      clinicianname: '',
      // addressline1: '',
      // addressline2: '',
      // city: '',
      // state: '',
      // county: '',
      // zipcode: '',
      phonenumber: '',
      currentdiagnosis: '',
      reportname: '',
     // reportpath: '',
      email: ['', [ValidationService.mailFormat]],
      dateofevaluation: null,
      evaluationby: '',
      nodiagnosisreason: '',
      uploadpath: '',
      isusetobacco: [null, Validators.required],
      //   isusedrugoralcohol: null,
      isusedrug: [null, Validators.required],
      drugfrequencydetails: '',
      drugageatfirstuse: '',
      isusealcohol: [null, Validators.required],
      alcoholfrequencydetails: '',
      alcoholageatfirstuse: '',
      drugoralcoholproblems: '',
      tobaccofrequencydetails: '',
      tobaccoageatfirstuse: '',
      phobiakey: '',
      phobiacomments: '',
    });
  }

  private resetForm() {
    this.behaviouralHealthInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.dtDisable = false;
    this.address.disable = false;
    this.uploadedFiles = [];
    this.drugSelectionEnabled = false;
    this.tobaccoSelectionEnabled = false;
    this.alchoholSelectionEnabled = false;
    this.behaviouralHealthInfoForm.enable();
  }

  private view(modal) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.address.address1 = modal.address1;
    this.address.address2 = modal.address2;
    this.address.city = modal.city;
    this.address.state = modal.state;
    this.address.zipcode = modal.zipcode;
    this.address.county = modal.county;
    this.address.disable = true;
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    this.patchSubstance(modal);
    this.patchForm(modal);
    this.editMode = false;
    this.dtDisable = true;
    this.behaviouralHealthInfoForm.disable();
  }

  private edit(modal, i) {
    this.personBehavioralHealthId = modal.personbehavioralhealthid;
    this.abuseSubstanceId = modal.parentabusesubstanceid;
    this.reportMode = 'edit';
    this.editMode = true;
    this.isAddEdit = true;
    this.patchSubstance(modal);
    this.address.address1 = modal.address1;
    this.address.address2 = modal.address2;
    this.address.city = modal.city;
    this.address.state = modal.state;
    this.address.zipcode = modal.zipcode;
    this.address.county = modal.county;
    this.address.disable = false;
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    modal.dateofevaluation = this._commonDropDownService.getValidDate(modal.dateofevaluation);
    this.patchForm(modal);
    this.dtDisable = false;
    this.behaviouralHealthInfoForm.enable();
  }

  patchSubstance(modal) {
    if (modal.isusetobacco) {
      this.tobaccoSelection('1');
    } else {
      this.tobaccoSelection('2');
    }

    if (modal.isusedrug) {
      this.drugSelection('1');
    } else {
      this.drugSelection('2');
    }

    if (modal.isusealcohol) {
      this.alchoholSelection('1');
    } else {
      this.alchoholSelection('2');
    }
  }

  private delete(modal, index) {
    const behaviouralId = {
      'personbehavioralhealthid': modal.personbehavioralhealthid,
    };
    const substanceId = {
      'personabusesubstanceid': modal.personabusesubstanceid,
    };
    this._healthService.saveHealth({ 'personBehaviour': [behaviouralId], 'personHealthSubstanceAbuse': [substanceId] }, 2).subscribe(_ => {
      this._alertSevice.success('Behavioral/Substance Details Deleted Successfully');
      this.resetForm();
      this.getBehavioralHealthList();
    });
  }

  private cancel() {
    this.isAddEdit = false;
    this.resetForm();
  }

  private patchForm(modal: BehaviouralHealthInfo) {
    this.behaviouralHealthInfoForm.patchValue(modal);
  }

  private add() {

    console.log(this.behaviouralHealthInfoForm.getRawValue());
    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;

    const modal = this.behaviouralHealthInfoForm.getRawValue();
    let behaviouralData = {};
    behaviouralData = {
      isbehaviouraldiagnosis: modal.isbehaviouraldiagnosis,
      typeofservice: modal.typeofservice,
      clinicianname: modal.clinicianname,
      phonenumber: modal.phonenumber,
      currentdiagnosis: modal.currentdiagnosis,
      reportname: modal.reportname,
      email: modal.email,
      dateofevaluation: modal.dateofevaluation,
      evaluationby: modal.evaluationby,
      nodiagnosisreason: modal.nodiagnosisreason,
      phobiakey: modal.phobiakey,
      phobiacomments: modal.phobiacomments
    };

    const behavioralHealth =  { ...behaviouralData, ...uploadInfo, ...this.address };


    let substanceAbusecw = {};
    substanceAbusecw = {
      isusetobacco: modal.isusetobacco,
      isusedrug: modal.isusedrug,
      drugfrequencydetails: modal.drugfrequencydetails,
      drugageatfirstuse: modal.drugageatfirstuse,
      isusealcohol: modal.isusealcohol,
      alcoholfrequencydetails: modal.alcoholfrequencydetails,
      alcoholageatfirstuse: modal.alcoholageatfirstuse,
      drugoralcoholproblems: modal.drugoralcoholproblems,
      tobaccofrequencydetails: modal.tobaccofrequencydetails,
      tobaccoageatfirstuse: modal.tobaccoageatfirstuse,
    };

    if (modal.uploadpath) {
      modal.uploadpath.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }

    this._healthService.saveHealth({
      'personBehaviour': [behavioralHealth],
      'personHealthSubstanceAbuse': [substanceAbusecw]
    }).subscribe(response => {
      this._alertSevice.success('Behavioral/Substance Details Added Successfully');
      this.resetAll();
      this.getBehavioralHealthList();
    });
    this.resetForm();
  }

  resetAll() {
    this.resetAddress();
    this.isAddEdit = false;
    this.resetForm();
    
    this.uploadedFiles = [];
  }

  resetAddress() {
    this.address = { address1: null, address2: null, city: null, state: null, zipcode: null, county: null, disable: false };
  }

  uploadFile(file: File | FileError): void {
    if (!(file instanceof File)) {
      return;
    }

    this.uploadedFile = file;
    this.behaviouralHealthInfoForm.patchValue({ reportname: file.name });
  }

  private update() {
    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;

    const modal = this.behaviouralHealthInfoForm.getRawValue();
    let behaviouralData = {};
    behaviouralData = {
      isbehaviouraldiagnosis: modal.isbehaviouraldiagnosis,
      typeofservice: modal.typeofservice,
      clinicianname: modal.clinicianname,
      phonenumber: modal.phonenumber,
      currentdiagnosis: modal.currentdiagnosis,
      reportname: modal.reportname,
      email: modal.email,
      dateofevaluation: modal.dateofevaluation,
      evaluationby: modal.evaluationby,
      nodiagnosisreason: modal.nodiagnosisreason,
      phobiakey: modal.phobiakey,
      phobiacomments: modal.phobiacomments,
      personbehavioralhealthid: this.personBehavioralHealthId
    };

    const behavioralHealth =  { ...behaviouralData, ...uploadInfo, ...this.address };

    let substanceAbusecw = {};
    substanceAbusecw = {
      isusetobacco: modal.isusetobacco,
      isusedrug: modal.isusedrug,
      drugfrequencydetails: modal.drugfrequencydetails,
      drugageatfirstuse: modal.drugageatfirstuse,
      isusealcohol: modal.isusealcohol,
      alcoholfrequencydetails: modal.alcoholfrequencydetails,
      alcoholageatfirstuse: modal.alcoholageatfirstuse,
      drugoralcoholproblems: modal.drugoralcoholproblems,
      tobaccofrequencydetails: modal.tobaccofrequencydetails,
      tobaccoageatfirstuse: modal.tobaccoageatfirstuse,
      parentabusesubstanceid: this.abuseSubstanceId
    };

    if (modal.uploadpath) {
      modal.uploadpath.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }

    this._healthService.saveHealth({
      'personBehaviour': [behavioralHealth],
      'personHealthSubstanceAbuse': [substanceAbusecw]
    }, 0).subscribe(response => {
      this._alertSevice.success('Behavioral/Substance Details Updated Successfully');
      this.resetAll();
      this.getBehavioralHealthList();
    });
    this.resetForm();
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { activeflag: 1 },
          order: 'description asc'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.personservicetype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'countyname asc'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '80', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        typeofservice: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.personservicetypekey
            })
        ),
        states: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.statename,
              value: res.stateabbr
            })
        ),
        counties: result[2].map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyname
            })
        ),
        fearsTypeList: result[3].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        )
      };
    })
      .share();
    this.serviceDropdownItems$ = source.pluck('typeofservice');
    this.stateDropdownItems$ = source.pluck('states');
    this.countyDropDownItems$ = source.pluck('counties');
    this.fearsTypeDropdownItems$ = source.pluck('fearsTypeList');
  }

  addBehavioralInfo() {
    this.isAddEdit = true;
  }

  getBehavioralHealthList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId }
    }, 'personabusesubstance/behavesubstancelist?filter').subscribe(res => {
        this.behaviouralcw = res ? res.data : [];
    });
  }
  test(test) {
    console.log(test);
    console.log(this.behaviouralHealthInfoForm.controls['isusedrug'].value);
  }

  drugSelection(control) {
    if (control === '1') {
      this.drugSelectionEnabled = true;
      this.behaviouralHealthInfoForm.get('drugfrequencydetails').enable();
      this.behaviouralHealthInfoForm.get('drugfrequencydetails').setValidators([Validators.required]);
      this.behaviouralHealthInfoForm.get('drugfrequencydetails').updateValueAndValidity();
      this.behaviouralHealthInfoForm.get('drugageatfirstuse').enable();
      this.behaviouralHealthInfoForm.get('drugageatfirstuse').setValidators([Validators.required]);
      this.behaviouralHealthInfoForm.get('drugageatfirstuse').updateValueAndValidity();

    } else {
      this.resetdrugfields();
      this.drugSelectionEnabled = false;
      this.behaviouralHealthInfoForm.get('drugfrequencydetails').disable();
      this.behaviouralHealthInfoForm.get('drugfrequencydetails').clearValidators();
      this.behaviouralHealthInfoForm.get('drugfrequencydetails').updateValueAndValidity();
      this.behaviouralHealthInfoForm.get('drugageatfirstuse').disable();
      this.behaviouralHealthInfoForm.get('drugageatfirstuse').clearValidators();
      this.behaviouralHealthInfoForm.get('drugageatfirstuse').updateValueAndValidity();
    }
  }

  alchoholSelection(control) {
    if (control === '1') {
      this.alchoholSelectionEnabled = true;
      this.behaviouralHealthInfoForm.get('alcoholfrequencydetails').enable();
      this.behaviouralHealthInfoForm.get('alcoholfrequencydetails').setValidators([Validators.required]);
      this.behaviouralHealthInfoForm.get('alcoholfrequencydetails').updateValueAndValidity();
      this.behaviouralHealthInfoForm.get('alcoholageatfirstuse').enable();
      this.behaviouralHealthInfoForm.get('alcoholageatfirstuse').setValidators([Validators.required]);
      this.behaviouralHealthInfoForm.get('alcoholageatfirstuse').updateValueAndValidity();
    } else {
      this.resetalcoholfields();
      this.alchoholSelectionEnabled = false;
      this.behaviouralHealthInfoForm.get('alcoholfrequencydetails').disable();
      this.behaviouralHealthInfoForm.get('alcoholfrequencydetails').clearValidators();
      this.behaviouralHealthInfoForm.get('alcoholfrequencydetails').updateValueAndValidity();
      this.behaviouralHealthInfoForm.get('alcoholageatfirstuse').disable();
      this.behaviouralHealthInfoForm.get('alcoholageatfirstuse').clearValidators();
      this.behaviouralHealthInfoForm.get('alcoholageatfirstuse').updateValueAndValidity();
    }
  }

  tobaccoSelection(control) {
    if (control === '1') {
      this.tobaccoSelectionEnabled = true;
      this.behaviouralHealthInfoForm.get('tobaccofrequencydetails').enable();
      this.behaviouralHealthInfoForm.get('tobaccofrequencydetails').setValidators([Validators.required]);
      this.behaviouralHealthInfoForm.get('tobaccofrequencydetails').updateValueAndValidity();
      this.behaviouralHealthInfoForm.get('tobaccoageatfirstuse').enable();
      this.behaviouralHealthInfoForm.get('tobaccoageatfirstuse').setValidators([Validators.required]);
      this.behaviouralHealthInfoForm.get('tobaccoageatfirstuse').updateValueAndValidity();
    } else {
      this.resettobaccorfields();
      this.tobaccoSelectionEnabled = false;
      this.behaviouralHealthInfoForm.get('tobaccofrequencydetails').disable();
      this.behaviouralHealthInfoForm.get('tobaccofrequencydetails').clearValidators();
      this.behaviouralHealthInfoForm.get('tobaccofrequencydetails').updateValueAndValidity();
      this.behaviouralHealthInfoForm.get('tobaccoageatfirstuse').disable();
      this.behaviouralHealthInfoForm.get('tobaccoageatfirstuse').clearValidators();
      this.behaviouralHealthInfoForm.get('tobaccoageatfirstuse').updateValueAndValidity();
    }
  }

  resetdrugfields() {
    this.behaviouralHealthInfoForm.patchValue({ 'drugfrequencydetails': null });
    this.behaviouralHealthInfoForm.patchValue({ 'drugageatfirstuse': null });
  }

  resetalcoholfields() {
    this.behaviouralHealthInfoForm.patchValue({ 'alcoholfrequencydetails': null });
    this.behaviouralHealthInfoForm.patchValue({ 'alcoholageatfirstuse': null });
  }

  resettobaccorfields() {
    this.behaviouralHealthInfoForm.patchValue({ 'tobaccofrequencydetails': null });
    this.behaviouralHealthInfoForm.patchValue({ 'tobaccoageatfirstuse': null });
  }

}
