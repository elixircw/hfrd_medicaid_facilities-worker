import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { PersonHealthService } from '../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonInfoService } from '../../person-info.service';
export class Sleeping {
  infoprovidedby: string;
  clientlist: string;
  collaterallist: string;
  infoprovidedname: string;
  relationship: string;
  sleepinginfounknown: boolean;
  sleepingenvironment: string;
  sleepingproblems: string;
  sleepingposition: string;
  naptime: string;
  bedtime: string;
  sleepcomments: string;
}
@Component({
  selector: 'sleeping',
  templateUrl: './sleeping.component.html',
  styleUrls: ['./sleeping.component.scss']
})

export class SleepingComponent implements OnInit {
  SleepingInfoForm : FormGroup;
  SleepEnvironment$: Observable<DropdownModel[]>;
  SleepPosition$: Observable<DropdownModel[]>;
  SleepProblem$:Observable<DropdownModel[]>;
  reportMode: string;
  editMode: boolean;
  modalInt: number;
  sleepcw: any[] = [];
  isAddEdit = false;
  personId: string;
  sleepingId: boolean;
  selected = [];
  otherSpecify: string;
  isClosed = false;

  constructor( private formbuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService
    ) { }

  ngOnInit() {
    this.personId =  this._personInfoService.getPersonId();
    this.reportMode = 'add';
    this.SleepingInfoForm = this.formbuilder.group({
      ishousehold: null,
    //  clientlist: '',
    //  collaterallist: '',
      provided_name: ['', Validators.required],
      relationship: '',
      issleepinginfoknown: false,
      sleepingenvironment: '',
      sleepingproblems: '',
      sleepingposition: '',
      otherSpecify: '',
      sleepingschedule_naptime: [null],
      sleepingschedule_bedtime: [null],
      comments: ''
    });
    this.loadDropDowns();
    this.getSleepingInfoList();
    this.isClosed = this._personInfoService.getClosed();
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '200', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '201', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '152', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          SleepEnvironmentList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          SleepPositionList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          SleepProblemList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          )
            };
          })
          .share();
      this.SleepEnvironment$ = source.pluck('SleepEnvironmentList');
      this.SleepPosition$ = source.pluck('SleepPositionList');
      this.SleepProblem$ = source.pluck('SleepProblemList');
  }

  private resetForm() {
    this.SleepingInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.SleepingInfoForm.enable();
    this.selected =  [];
  }

  private view(modal) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    modal.provided_name = modal.providedname;
    modal.sleepingproblems = modal.sleepingproblems;
    modal.otherSpecify   = modal.otherspecify;
    this.selected =  modal.sleepingproblems;
    this.patchForm(modal);
    this.editMode = false;    
    this.SleepingInfoForm.disable();
  }

  private edit(modal, i) {
    this.sleepingId = modal.personhlthsleepingid;
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    modal.provided_name = modal.providedname;
    modal.sleepingproblems = modal.sleepingproblems;
    modal.otherSpecify = modal.otherspecify;    
    this.patchForm(modal);
    this.selected =  modal.sleepingproblems;    
    this.SleepingInfoForm.enable();
  }

  private delete(modal, index) {
    const data = {
      'personhlthsleepingid': modal.personhlthsleepingid,
    };
    this._healthService.saveHealth({ 'sleepingInfo': [data] }, 2).subscribe(_ => {
        this._alertSevice.success('Sleeping Info Deleted Successfully');
        this.resetForm();
        this.getSleepingInfoList();
      });
  }

  private cancel() {
    this.resetForm();
    if(this.isClosed) {
      window.scrollTo(0,0);
      this.isAddEdit = false;
    }
  }

  private patchForm(modal: Sleeping) {
    this.SleepingInfoForm.patchValue(modal);
  }

  private add() {
    const sleepinginfo = this.SleepingInfoForm.getRawValue();
    this._healthService.saveHealth({ 'sleepingInfo': [sleepinginfo]}).subscribe(response => {
      this._alertSevice.success('Sleeping Info Added Successfully');
      this.getSleepingInfoList();
    });
    this.resetForm();
  }

  private update() {
    const sleepinginfo = this.SleepingInfoForm.getRawValue();
    sleepinginfo.personhlthsleepingid = this.sleepingId;
    this._healthService.saveHealth({ 'sleepingInfo': [sleepinginfo] }, 0).subscribe(response => {
      this._alertSevice.success('Sleeping Info Updated Successfully');
      this.getSleepingInfoList();
    });
    this.resetForm();
  }

  getSleepingInfoList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId}
    }, 'personfamilyinfo/getpersonsleeping?filter').subscribe(res => {
      this.sleepcw = res ? res.data : [];
    });
  } 

  addSleepingInfo() {
    this.isAddEdit = true;
  }

  getEnvironment(environment) {
    if (environment && Array.isArray(environment)) {
      return environment.join(',');
    } 
    return '';
  }

  getProblems(problems) {
    if (problems && Array.isArray(problems)) {
      return problems.join(',');
    }
    return '';
  }
}
