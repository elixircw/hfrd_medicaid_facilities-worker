import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService, AuthService } from '../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { MyNewintakeConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { Medication, Health } from '../../../involved-persons/_entities/involvedperson.data.model';
import { NewUrlConfig } from '../../../../newintake/newintake-url.config';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'medication-including-psychotropic-cw',
  templateUrl: './medication-including-psychotropic-cw.component.html',
  styleUrls: ['./medication-including-psychotropic-cw.component.scss']
})
export class MedicationIncludingPsychotropicCwComponent implements OnInit {
  medicationpsychotropicForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  medicationpsychotropic: Medication[] = [];
  health: Health = {};
  medicationType$: Observable<DropdownModel[]>;
  medicationType: any = [];
  isMedicationIncludes:  boolean;
  constants = MyNewintakeConstants.Intake.PersonsInvolved.Health;
  prescriptionReasonType$: Observable<DropdownModel[]>;
  informationSourceType$: Observable<DropdownModel[]>;
  frequency$: Observable<DropdownModel[]>;
  prescribedDuration$: Observable<DropdownModel[]>;
  maxDate = new Date();
  teamTypeKey: string;
  isAddEdit = false;
  personId: string;
  uploadedFiles = [];
  uploadNumber = '123434';
  medicalInfoId: any;
  dtDisable = false;
  isClosed = false;
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _authService: AuthService,
    private _personInfoService: PersonInfoService
    ) { }

  ngOnInit() {
    this.personId =  this._personInfoService.getPersonId();
    this.teamTypeKey = this._authService.getAgencyName();
    this.loadDropDowns();
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.isMedicationIncludes = false;
    this.medicationpsychotropicForm = this.formbulider.group({
      isprescribedmedication: [null, [Validators.required]],
      medicinename: ['', Validators.required],
      prescribedreason: '',
      medicationtype: '',
      dosage: '',
      frequency: '',
      complaint: '',
      informationsourcetypekey: '',
      expirationdate: [null, Validators.required],
      startdate: [null],
      enddate: null, 
      prescribingdoctor: '',
      prescriptionreasontypekey: '',
      comments: '',
      reportedby: [''],
      otherreason: '',
      prescribedduration: ''
    });
    this.isClosed = this._personInfoService.getClosed();
    this.getMedicationPsychotropicList();
  }

  ismedication(opt) {
    this.enableorDisableField('medicinename', opt);
    this.enableorDisableField('expirationdate', opt);
    this.isMedicationIncludes = opt;
    this.medicationpsychotropicForm.reset();
    this.medicationpsychotropicForm.patchValue({ 'isprescribedmedication': opt });
  }

  private enableorDisableField(field, opt) {
    if (opt) {
      this.medicationpsychotropicForm.get(field).enable();
     // this.healthinsuranceForm.get(field).setValidators([Validators.required]);
      this.medicationpsychotropicForm.get(field).updateValueAndValidity();
    } else {
      this.medicationpsychotropicForm.get(field).disable();
      this.medicationpsychotropicForm.get(field).clearValidators();
      this.medicationpsychotropicForm.get(field).updateValueAndValidity();
    }
  }
  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.prescriptionreasontype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.informationsourcetype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'description'
        },
        NewUrlConfig.EndPoint.Intake.medicationtype + '?filter'
      ),
      this._commonHttpService
      .getArrayList(
          {
              where: { "tablename":"frequencytype", "teamtypekey": this.teamTypeKey },
              method: 'get'
          },
          'referencetype/gettypes' + '?filter'
      ),
      this._commonHttpService
      .getArrayList(
        {
            where: { referencetypeid: 334, teamtypekey: this.teamTypeKey },
            method: 'get'
        },
        'referencetype/gettypes' + '?filter'
      )
    ])
      .map((result) => {
        result[2].forEach(type => {
          this.medicationType[type.medicationtypekey] = type.description;
        });
        return {
          prescriptionreasontype: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.prescriptionreasontypekey
              })
          ),
          informationsourcetype: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.informationsourcetypekey
              })
          ),
          medicationtype: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.medicationtypekey
              })
          ),
          frequency: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.ref_key
              })
          ),
          prescribedduration: result[4].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.ref_key
              })
          )
        };
      })
      .share();
    this.prescriptionReasonType$ = source.pluck('prescriptionreasontype');
    this.informationSourceType$ = source.pluck('informationsourcetype');
    this.medicationType$ = source.pluck('medicationtype');
    this.frequency$ = source.pluck('frequency');
    this.prescribedDuration$ = source.pluck('prescribedduration');
  }

  private add() {
    const uploadInfo = {};
    uploadInfo['uploadedFiles'] = this.uploadedFiles;
    const data = {...this.medicationpsychotropicForm.getRawValue(), ...uploadInfo};
    data.isprescribedmedication = this.isMedicationIncludes;
    this._healthService.saveHealth({ 'personmedicalPsychotropic': [data] }).subscribe(response => {
      this._alertSevice.success('Medication Information Added Successfully');
      this.resetForm();
      this.getMedicationPsychotropicList();
    });
  }

  private resetForm() {
    this.medicationpsychotropicForm.reset();
    this.modalInt = -1;
    this.isMedicationIncludes = false;
    this.editMode = false;
    this.reportMode = 'add';
    this.dtDisable = false;
    this.medicationpsychotropicForm.enable();
    this.medicationpsychotropicForm.get('isprescribedmedication').setValidators([Validators.required]);
    this.medicationpsychotropicForm.get('isprescribedmedication').updateValueAndValidity();
  }

  private update() {
    const uploadInfo = {};
    uploadInfo['uploadedFiles'] = this.uploadedFiles;
    const data = {...this.medicationpsychotropicForm.getRawValue(), ...uploadInfo};
    data.medicalinfoid = this.medicalInfoId;
    data.isprescribedmedication = this.isMedicationIncludes;
    this._healthService.saveHealth({ 'personmedicalPsychotropic': [data] }, 0).subscribe(response => {
      this._alertSevice.success('Medication Information Updated Successfully');
      this.resetForm();
      this.getMedicationPsychotropicList();
    });
  }

  private view(modal, i) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.ismedication(modal.isprescribedmedication);
    this.editMode = false;
    this.patchForm(modal);
    this.dtDisable = true;
    this.medicationpsychotropicForm.disable();
  }

  private edit(modal, i) {
    this.medicalInfoId = modal.medicalinfoid;
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.ismedication(modal.isprescribedmedication);
    this.editMode = true;
    this.patchForm(modal);
    this.dtDisable = false;
    this.medicationpsychotropicForm.enable();
  }

  private delete(modal, index) {
    const data = {
      'medicalinfoid': modal.medicalinfoid,
    };
    this._healthService.saveHealth({ 'personmedicalPsychotropic': [data] }, 2).subscribe(_ => {
        this._alertSevice.success('Medication Information Deleted Successfully');
        this.resetForm();
        this.getMedicationPsychotropicList();
      });
  }

  private cancel() {
    this.resetForm();
    if(this.isClosed) {
      window.scrollTo(0,0);
      this.isAddEdit = false;
    }
  }

  private patchForm(modal: Medication) {
    this.medicationpsychotropicForm.patchValue(modal);
  }

  addMedicationPsychotropic() {
    this.isAddEdit = true;
  }

  getMedicationPsychotropicList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId}
    }, 'personmedicalcondition/personmedicallist?filter').subscribe(res => {
      this.medicationpsychotropic = res ? res.data : [];
    });
  }

}
