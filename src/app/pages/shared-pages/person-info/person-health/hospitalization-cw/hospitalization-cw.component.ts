import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Hospitalization } from '../../../involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../involved-persons/_entities/involvedPersons.constants';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';
import * as moment from 'moment';
@Component({
  selector: 'hospitalization-cw',
  templateUrl: './hospitalization-cw.component.html',
  styleUrls: ['./hospitalization-cw.component.scss']
})
export class HospitalizationCwComponent implements OnInit {
  hosptializationForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  minDate = new Date();
  maxDate = new Date();
  startMaxDate = null;
  endMinDate = null;
  modalInt: number;
  hospitalcw: Hospitalization[] = [];
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  hosptializationType$: Observable<DropdownModel[]>;
  hosptializationReason$: Observable<DropdownModel[]>;
  personId: string;
  isAddEdit = false;
  uploadedFiles = [];
  uploadNumber = '123434';
  address = { address1: null, address2: null, city: null, state: null, county: null, zipcode: null, disable: false };
  hospitalizationId: string;
  selectedItem:any;
  dtDisable = false;
  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService, ) { }

  ngOnInit() {
    this.personId = this._personInfoService.getPersonId();
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
    this.getHospitalizationList();
  }

  initForm() {
    this.hosptializationForm = this._formBuilder.group({
      'hospitalizationid': null,
      'hospitalization_type': ['', Validators.required],
      'hospitalization_reason': ['', Validators.required],
      'Reason_or_diagnosis': [null],
      'Hospital_name': [null],
      'Hospital_phone': [null],
      // 'Hospital_address1': '',
      // 'Hospital_address2': '',
      // 'Hospital_city': '',
      // 'Hospital_state': '',
      // 'Hospital_county': '',
      // 'Hospital_zipcode': '',
      'start_Date':[null, Validators.required],
      'end_Date': [null],
      'has_discharge_plan': [null],
      'discharge_plan': [null]
    });
  }

  startDateChange(hosptializationForm) {

    this.endMinDate = hosptializationForm.getRawValue().start_Date;
  }

  endDateChange(hosptializationForm) {   
    if(!hosptializationForm.getRawValue().start_Date) {
      this._alertSevice.error('Please fill start date');
      hosptializationForm.patchValue({ end_Date : null});
      this.startMaxDate = null;
      this.endMinDate = null;
    } else {
    this.startMaxDate = hosptializationForm.getRawValue().end_Date;
    }
  }

  private resetForm() {
    this.dtDisable = false;
    this.address = {
      address1: null, address2: null,
      city: null, state: null,
      zipcode: null, county: null,
      disable: false
    };
    this.startMaxDate = null;
    this.endMinDate = null;
    this.hosptializationForm.reset();
    this.editMode = false;
    this.isAddEdit = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.hosptializationForm.enable();
    this.uploadedFiles = [];
    this.selectedItem = null;
  }

  private view(modal) {
    this.address = {
      address1: modal.Hospital_address1,
      address2: modal.Hospital_address2,
      city: modal.Hospital_city,
      state: modal.Hospital_state,
      zipcode: modal.Hospital_zipcode,
      county: modal.county,
      disable: true
    };
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    this.editMode = false;
    this.hosptializationForm.controls['start_Date'].disable();
    this.hosptializationForm.controls['end_Date'].disable();
    this.dtDisable = true;
    this.hosptializationForm.disable();
  }

  private edit(modal, i) {
    this.address = {
      address1: modal.Hospital_address1,
      address2: modal.Hospital_address2,
      city: modal.Hospital_city,
      state: modal.Hospital_state,
      zipcode: modal.Hospital_zipcode,
      county: modal.county,
      disable: false
    };
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    this.hospitalizationId = modal.hospitalizationid;
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.endMinDate = modal.start_Date;
    this.startMaxDate = modal.end_Date;
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.hosptializationForm.enable();
  }


  cancel() {
    this.resetForm();
  }

  private patchForm(modal: Hospitalization) {
    this.dtDisable = false;
    this.hosptializationForm.patchValue(modal);
  }

  private add() {
    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;
    const data = { ...this.hosptializationForm.getRawValue(), ...this.address, ...uploadInfo };
    data.Hospital_address1 = this.address.address1;
    data.Hospital_address2 = this.address.address2;
    data.Hospital_city = this.address.city;
    data.Hospital_state = this.address.state;
    data.Hospital_zipcode = this.address.zipcode;
    data.county = this.address.county;
    if (data.start_Date) {
      data.start_Date = moment(new Date(data.start_Date)).format('YYYY-MM-DD');
    }
    if (data.end_Date) {
      data.end_Date = moment(new Date(data.end_Date)).format('YYYY-MM-DD');
    }

    if (data.uploadpath) {
      data.uploadpath.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }
    let isNew = 1;
    if (data.hospitalizationid) {
      isNew = 0;
    }

    this._healthService.saveHealth({ 'personHospitalizationHistory': [data] }, isNew).subscribe(response => {
      this._alertSevice.success('Hospitalization Info Saved Successfully');
      this.getHospitalizationList();
    });
    this.resetForm();
  }

  private update() {
    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;
    const data = { ...this.hosptializationForm.getRawValue(), ...this.address, ...uploadInfo };
    data.Hospital_address1 = this.address.address1;
    data.Hospital_address2 = this.address.address2;
    data.Hospital_city = this.address.city;
    data.Hospital_state = this.address.state;
    data.Hospital_zipcode = this.address.zipcode;
    data.hospitalizationid = this.hospitalizationId;
    if (data.start_Date) {
      data.start_Date = moment(new Date(data.start_Date)).format('YYYY-MM-DD');
    }
    if (data.end_Date) {
      data.end_Date = moment(new Date(data.end_Date)).format('YYYY-MM-DD');
    }

    this._healthService.saveHealth({ 'personHospitalizationHistory': [data] }, 0).subscribe(response => {
      this._alertSevice.success('Hospitalization Info Updated Successfully');
      this.getHospitalizationList();
    });
    this.resetForm();
  }

  initializeHospitalization() {
    this.resetForm();
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '230', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '310', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        hosptializationTypeValues: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.picklist_value_cd
            })
        ),
        hosptializationReasonValues: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.picklist_value_cd
            })
        ),
        states: result[2].map(
          (res) =>
            new DropdownModel({
              text: res.statename,
              value: res.stateabbr
            })
        )
      };
    })
      .share();
    this.hosptializationReason$ = source.pluck('hosptializationReasonValues');
    this.stateDropdownItems$ = source.pluck('states');
    this.hosptializationType$ = source.pluck('hosptializationTypeValues');
  }

  addHospitalization() {
    this.isAddEdit = true;
  }

  getHospitalizationList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId }
    }, 'personhospitalization/list?filter').subscribe(res => {
      this.hospitalcw = res ? res.data : [];
      console.log(this.hospitalcw);
    });
  }

  deleteConfirm(item, index) {
    (<any>$('#delete-popup')).modal('show');
    this.selectedItem = item;
  }

  delete() {
    const data = {
      'hospitalizationid': this.selectedItem.hospitalizationid,
    };
    this._healthService.saveHealth({ 'personHospitalizationHistory': [data] }, 2).subscribe(_ => {
      this._alertSevice.success('Deleted Hospitalization Info Successfully');
      this.resetForm();
      // this._healthService.setPersonHealthInfo();
      this.getHospitalizationList();
    });
  }
}
