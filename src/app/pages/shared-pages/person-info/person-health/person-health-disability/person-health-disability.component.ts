import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PersonDisabilityService } from '../../../person-disability/person-disability.service';
import { DataStoreService, AlertService, CommonHttpService } from '../../../../../@core/services';
import { GUARDIANSHIP } from '../../../../case-worker/dsds-action/involved-persons/guardianship/_entities/guardianship.const';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { Subscription } from 'rxjs/Subscription';
import { AppConstants } from '../../../../../@core/common/constants';
import { IntakeStoreConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { PAGES_STORE_CONSTANTS } from '../../../../pages.constants';


@Component({
  selector: 'person-health-disability',
  templateUrl: './person-health-disability.component.html',
  styleUrls: ['./person-health-disability.component.scss']
})
export class PersonHealthDisabilityComponent implements OnInit, OnDestroy {

  @Input() removalPersonid: string;
  @Input() disabled : boolean;
  personDisabilities = [];
  personId: any;
  subscription: Subscription;
  personDisabilityForm: FormGroup;
  dateStart: boolean;
  ispersonProfile: boolean;
  isEditMode: boolean;
  personDisabilityid: string;
  disabilityTypes: any = [];
  disabilityConditions: any = [];
  disablityFlag = false;
  hasDisability: boolean;
  NoDisability: any = [];
  specialNeeds: any[] = [];
  personalHygieneList: any[] = [];
  selectedItem: any;
  SAVE_QUEUE = 0;
  isSaveQueueInprogress = false;
  disabilityhistory = [];
  constructor(private _service: PersonDisabilityService,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _personInfoService: PersonInfoService) { }

  ngOnInit() {
    const personid = this.removalPersonid ? this.removalPersonid : this._personInfoService.getPersonId();
    this.personId = personid;
    const urlSegments = this._router.url.split('/');
    if (urlSegments && urlSegments.length) {
      const routeLength = urlSegments.length;
      this.ispersonProfile =  urlSegments[routeLength - 1 ] === 'disability' ? true : false;
    }
    console.log('Segments Data', urlSegments);
    this.listenForPersonDisability();
    this.initForm();
    this.loadDropDowns();
    if (this._personInfoService.getClosed()) {
      this.disabled = true;
    }
  }

  initForm() {
    this.personDisabilityForm = this._formBuilder.group({
      persondisabilityid: [null],
      personid: [this.personId],
      disabilityconditiontypekey: [null, Validators.required],
      diagnoiseddisabilitynotes: [null],
      startdate: [null, Validators.required],
      startdateunknown: [null],
      enddate: [null],
      disabilityflag: [null],
      evaluationdate: [null],
      evaluatorname: [null],
      specialkey: [null],
      hygienekey: [null],
      disabilitytypekey: [null, Validators.required],
      comments: [null],
      isnew:[null]
    });
  }

  loadDropDowns() {
    this._service.getDisabilityTypes().subscribe(response => {
      if (response && Array.isArray(response)) {
        this.loadPersonDisabilityList();
        this.disabilityTypes = response;
        this.setDisabiltyFields();
      }
    });
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { 'active_sw': 'Y', 'picklist_type_id': '203', 'delete_sw': 'N' }
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
    ).subscribe(response => {
      this.specialNeeds = response;
    });
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { 'active_sw': 'Y', 'picklist_type_id': '137', 'delete_sw': 'N' }
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
    ).subscribe(response => {
      this.personalHygieneList = response;
    });
    this.disabilityConditions = [{
      'value_text': 'Yes',
      'description': 'Yes'
    },
    {
      'value_text': 'No',
      'description': 'No'
    },
    {
      'value_text': 'Unknown',
      'description': 'Unknown'
    }];
    /*  this._service.getDisabilityConditions().subscribe(response => {
       if (response && Array.isArray(response)) {
         this.disabilityConditions = response;
       }
     }); */
  }

  clearStartdate(event: any) {
    if (event) {
      this.personDisabilityForm.get('startdate').clearValidators();
      this.personDisabilityForm.get('startdate').updateValueAndValidity();
      this.personDisabilityForm.get('startdate').setValue(null);
      this.personDisabilityForm.controls['startdate'].disable();
      this.dateStart = true;
    } else {
      this.personDisabilityForm.get('startdate').setValidators([Validators.required]);
      this.personDisabilityForm.get('startdate').updateValueAndValidity();
      this.personDisabilityForm.controls['startdate'].enable();
      this.dateStart = false;
    }
  }


  saveDisability() {
    console.log('df', this.personDisabilityForm.getRawValue());
    if (this.personDisabilityForm.invalid) {
      this._alertService.error('please fill required fields');
      return;
    }
    const data = this.personDisabilityForm.getRawValue();
    data.personid = this.personId;
    // this.NoDisability  = data;
    this._service.createDisability(data).subscribe(response => {
      this.reset();
      this.selectedItem = null;
      this._alertService.success('Disability added successfully', true);
      this.loadPersonDisabilityList();
    });
  }




  loadPersonDisabilityList() {
    this._service.getDisabilityList(this.personId).subscribe(response => {
      this._dataStoreService.setData(PAGES_STORE_CONSTANTS.PERSON_DISABILITY_PRINSTINE, false);
      if (response && Array.isArray(response) && response.length) {
        this.personDisabilities = response;
        const yesDisablities = this.personDisabilities.filter(pd => pd.disabilityconditiontypekey === 'Yes');
              if (this.personDisabilities.length === 0) {
                this.hasDisability = null;
              } else if (yesDisablities && yesDisablities.length > 0 ) {
                this.hasDisability = true;
              } else {
                this.hasDisability = false;
              }
      } else {
        this.personDisabilities = [];
        this.hasDisability = null;
      }
      this.setDisabiltyFields();
    });
  }

  loadPersonDisabilityHistory() {
    this._service.getDisabilityHistory(this.personId).subscribe(response => {
      this.disabilityhistory = response;
      (<any>$('#disability-history')).modal('show');
    });
  }

  openDisabilityForm() {
    this._router.navigate(['disability/' + this.personId + '/create'], { relativeTo: this.route });
  }

  listenForPersonDisability() {
    this.subscription = this._dataStoreService.currentStore.subscribe(store => {
      if (store.SUBSCRIPTION_TARGET === 'DISABILITY') {
        this.loadPersonDisabilityList();
      }
    });
  }



  deleteConfirm(item, index) {
    (<any>$('#delete-popup')).modal('show');
    this.selectedItem = item;
  }

  delete() {
    this._service.deleteDisability(this.selectedItem.persondisabilityid).subscribe(response => {
      this._alertService.success('Disability deleted successfully');
      this.disabilityTypes.forEach(element => {
        if (element.ref_key === this.selectedItem.disabilitytypekey) {
          element.conditionType = null;
        }
      });
      this.loadPersonDisabilityList();
    });
  }

  // ngOnDestroy(): void {
  //   this.subscription.unsubscribe();
  // }

  addDisability(item, disabilitycondition) {
    this.selectedItem = item;
    this.personDisabilityForm.patchValue({ disabilitytypekey: item.ref_key });
    this.personDisabilityForm.patchValue({ disabilityconditiontypekey: disabilitycondition });
    if (disabilitycondition === 'Yes') {
      (<any>$('#add-disability')).modal('show');
      if (this.personDisabilityForm.get('startdateunknown').value && this.personDisabilityForm.get('startdateunknown').value === true) {
        this.personDisabilityForm.get('startdate').clearValidators();
        this.personDisabilityForm.get('startdate').updateValueAndValidity();
        this.personDisabilityForm.get('startdate').setValue(null);
        this.personDisabilityForm.controls['startdate'].disable();
        this.dateStart = true;
      } else {
        this.personDisabilityForm.get('startdate').setValidators([Validators.required]);
        this.personDisabilityForm.get('startdate').updateValueAndValidity();
        this.personDisabilityForm.controls['startdate'].enable();
        this.dateStart = false;
      }
    } else {
      this.setNoDisabilities(item, disabilitycondition);
    }
    this._dataStoreService.setData(PAGES_STORE_CONSTANTS.PERSON_DISABILITY_PRINSTINE, true);
  }
  resetDisabilityForm() {
    this.reset();
    this.disabilityTypes.forEach(element => {
      element.conditionType = null;
    });
    this.NoDisability = [];
    this.loadPersonDisabilityList();
  }
  saveDisablityQueue(data) {
    data.persondisabilityid = null;
    this._service.createDisability(data).subscribe(response => {
      if (this.SAVE_QUEUE <= this.NoDisability.length - 1) {
        this.saveDisablityQueue(this.NoDisability[this.SAVE_QUEUE++]);
      } else {
        this._alertService.success('Disability saved successfully', true);
        this.isSaveQueueInprogress = false;
        this.NoDisability = [];
        this.loadPersonDisabilityList();
      }


      /* this.NoDisability = [];
      if (index === data.length - 1) {

        const existingDisability = (this.personDisabilities && this.personDisabilities.length) ? this.personDisabilities.length : 0;
        const newDisability = (data && data.length) ? data.length : 0;
        const totalDisability = existingDisability + newDisability;
        if (totalDisability < 8) {
          this._alertService.warn('Please  answer all the questions', true);
          this.loadPersonDisabilityList();
        } else {
          this._alertService.success('Disability saved successfully', true);
          this.loadPersonDisabilityList();
        }

      } */
    });
  }
  submitDisability() {

    const totalDisabilityTypeCount = this.disabilityTypes.length;
    const hasDisabilityYes = this.personDisabilities.filter(pd => pd.disabilityconditiontypekey === 'Yes');
    let hasDisabilityYesCount = 0;
    if (hasDisabilityYes) {
      hasDisabilityYesCount = hasDisabilityYes.length;
    }
    const allNo = this.disabilityTypes.filter(pd => pd.conditionType === 'No');
    if (allNo && allNo.length === totalDisabilityTypeCount) {
      (<any>$('#reset-confirm')).modal('show');
      return true;
    }

    const noDisabilityCount = this.NoDisability.length;
    // if (noDisabilityCount === 0) {
    //   (<any>$('#reset-confirm')).modal('show');
    //   return true;
    // }
    if (noDisabilityCount > 0 && this.personDisabilities.length === totalDisabilityTypeCount) {
      if (this.NoDisability && this.NoDisability.length) {
        this.SAVE_QUEUE = 0;
        this.isSaveQueueInprogress = true;
        this.saveDisablityQueue(this.NoDisability[this.SAVE_QUEUE]);
      }
    } else {
      const allFiled = this.disabilityTypes.filter(pd => pd.conditionType === null );
      if (Array.isArray(allFiled) && allFiled.length > 0) {
        this._alertService.warn('Please  answer all the questions', true);
        return false;
      }

      if (this.NoDisability && this.NoDisability.length) {
        if (this.NoDisability.length === totalDisabilityTypeCount && this.hasDisability) {
          (<any>$('#reset-confirm')).modal('show');
          return true;
        } else {
          this.SAVE_QUEUE = 0;
          this.isSaveQueueInprogress = true;
          this.saveDisablityQueue(this.NoDisability[this.SAVE_QUEUE]);
        }
      } else {
        this._alertService.success('Disabilities Saved successfully', true);
      }
    }

  }

  savedisability() {
    this.SAVE_QUEUE = 0;
    this.isSaveQueueInprogress = true;
    if (Array.isArray(this.NoDisability) && this.NoDisability.length > 0) {
      this.saveDisablityQueue(this.NoDisability[this.SAVE_QUEUE]);
    }
  }

  setNoDisabilities(item, disabilitycondition) {
    const disabilityObj = {
      persondisabilityid: item.persondisabilityid ? item.persondisabilityid : null,
      personid: [this.personId],
      disabilityconditiontypekey: disabilitycondition,
      diagnoiseddisabilitynotes: null,
      startdate: null,
      enddate: null,
      disabilityflag: null,
      evaluationdate: null,
      evaluatorname: null,
      specialkey: null,
      hygienekey: null,
      disabilitytypekey: item.ref_key,
      comments: disabilitycondition + '_Disability',
      isnew: null

    };
    if (!this.NoDisability) {
      this.NoDisability = [];
    }
    this.NoDisability.push(disabilityObj);
  }

  closeDisabilityModal() {
    this.personDisabilityForm.reset();
    if (this.selectedItem) {
      this.disabilityTypes.forEach(disability => {
        if (disability.ref_key === this.selectedItem.ref_key) {
          const previousValue = this.personDisabilities.find(disabilityItem => disabilityItem.persondisabilityid === disability.persondisabilityid);
          if (previousValue) {
            disability.conditionType = previousValue.disabilityconditiontypekey;
          } else {
            disability.conditionType = null;
            this.NoDisability = this.NoDisability.filter(item => item.disabilitytypekey !== disability.ref_key);
          }
        }
      });
    }

    (<any>$('#add-disability')).modal('hide');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  setDisabiltyFields() {
    if (this.disabilityTypes && this.disabilityTypes.length) {
      const personDisabilities = this.personDisabilities;
      this.disabilityTypes.forEach(element => {
        element.conditionType = element.conditionType ? element.conditionType : null;
        element.persondisabilityid = null;
        if (personDisabilities && personDisabilities.length) {
          const isDisabilityAvailable = personDisabilities.find(child => child.disabilitytypekey === element.ref_key);
          if (isDisabilityAvailable) {
            element.persondisabilityid = isDisabilityAvailable.persondisabilityid;
            element.conditionType = isDisabilityAvailable.disabilityconditiontypekey;
            /* if (isDisabilityAvailable.comments === 'No_Disability' || isDisabilityAvailable.comments === 'Unknown_Disability') {
              element.conditionType = (isDisabilityAvailable.comments === 'No_Disability') ? 'No' : 'Unknown';
            } else {
              element.conditionType = isDisabilityAvailable.disabilityconditiontypekey;
            } */
            element.disabled = true;
          } else {
            element.disabled = false;
          }
        }
      });
      console.log('proccessed disablities', this.disabilityTypes);
    }
  }

  reset() {
    this.isEditMode = false;
    this.personDisabilityForm.reset();
    this.closeDisabilityModal();
  }

  resetDisabilityPopup() {
    (<any>$('#remove-disability')).modal('hide');
    this.personDisabilityForm.reset();
    this.loadPersonDisabilityList();
  }

  onDisabiltyChange(isSelected) {
    if (!isSelected) {
      this.disabilityTypes.forEach(disablity => {
        // this.setNoDisabilities(disablity,)
        this.addDisability(disablity, 'No');
      });
      (<any>$('#remove-disability')).modal('show');
    }
    this._dataStoreService.setData(PAGES_STORE_CONSTANTS.PERSON_DISABILITY_PRINSTINE, true);
  }

  closeRemoveDisabilityModal() {
    (<any>$('#remove-disability')).modal('hide');
  }

  edit(item) {
    console.log('Edit Params', item);
    this.isEditMode = true;
    item.hygienekey = Array.isArray(item.hygienekey) ? item.hygienekey : [];
    item.specialkey = Array.isArray(item.specialkey) ? item.specialkey : [];
    this.personDisabilityForm.patchValue(item);
    this.personDisabilityid = item.persondisabilityid ? item.persondisabilityid : null;
    (<any>$('#add-disability')).modal('show');
  }
  add(item) {
    console.log('add Params', item);
    this.isEditMode = false;
    item.hygienekey = [];
    item.specialkey = [];
    item.persondisabilityid = null;
    item.isnew = true;
    this.personDisabilityForm.patchValue({
      isnew: true,
      hygienekey: [],
      specialkey: [],
      persondisabilityid: null,
      disabilitytypekey: item.disabilitytypekey,
      disabilityconditiontypekey: item.disabilityconditiontypekey
    });
    this.personDisabilityid =  null;
    (<any>$('#add-disability')).modal('show');
  }

  updateDisability() {
    const data = this.personDisabilityForm.getRawValue();
    data.personDisabilityid = this.personDisabilityid ? this.personDisabilityid : null;
    if (this.personDisabilityid) {
      this._service.createDisability(data).subscribe(response => {
        this.reset();
        this.NoDisability = [];

        const totalDisability = (this.personDisabilities && this.personDisabilities.length) ? (this.personDisabilities.length + 1) : 1;
        if (totalDisability < 8) {
          this._alertService.warn('Please  answer all the questions', true);
          this.loadPersonDisabilityList();
        } else {
          this._alertService.success('Disability updated successfully', true);
          this.loadPersonDisabilityList();
        }

        (<any>$('#add-disability')).modal('hide');
      });
    }
  }



  removeAllDisability() {
    if (this.NoDisability && this.NoDisability.length) {
      this.SAVE_QUEUE = 0;
      this.isSaveQueueInprogress = true;
      this.saveDisablityQueue(this.NoDisability[this.SAVE_QUEUE]);
    }
    (<any>$('#remove-disability')).modal('hide');
  }

  resetDisability(event) {
    this.hasDisability = event;
    this.closeRemoveDisabilityModal();
    this.savedisability();
  }
  unkDateChange(event) {
    console.log(event);
    this.clearStartdate(event.checked);
  }
}
