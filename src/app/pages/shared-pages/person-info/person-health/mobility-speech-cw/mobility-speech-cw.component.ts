import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';

import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';

export class MobilitySpeech {
  infoprovidedby: string;
  clientlist: string;
  Collaterallist: string;
  infoprovidedname: string;
  relationship: string;
  mobspeechinfounknown: boolean;
  speechvalue: string;
  mobilityvalue: string;
  childsatup: string;
  childwalked: string;
  childtalked: string;
  comments: string;
  ishousehold: boolean;
  iscollateral: boolean;
}
@Component({
  selector: 'mobility-speech-cw',
  templateUrl: './mobility-speech-cw.component.html',
  styleUrls: ['./mobility-speech-cw.component.scss']
})
export class MobilitySpeechCwComponent implements OnInit {

  mobilitySpeechInfoForm: FormGroup;
  SpeechList$: Observable<DropdownModel[]>;
  MobilityList$: Observable<DropdownModel[]>;
  prenatalCareDropdownItems$: Observable<DropdownModel[]>;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  speechcw: any[] = [];
  personId: string;
  isAddEdit = false;
  isClosed = false;

  selectedMobilitySpeech: any;
  constructor(private formbuilder: FormBuilder, private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService) { }

  ngOnInit() {
    this.reportMode = 'add';
    this.personId = this._personInfoService.getPersonId();
    this.mobilitySpeechInfoForm = this.formbuilder.group({
      personhlthmobilityspeechid: [null],
      ishousehold: ['', Validators.required],
      // clientlist: '',
      // Collaterallist: '',
      provided_name: ['', Validators.required],
      relationship: ['', Validators.required],
      ismbltyspchknown: false,
      // speechvalue: '',
      // mobilityvalue: '',
      satupage: '',
      walkedage: '',
      talkedage: '',
      comments: ''
    });
    this.loadDropDowns();
    this.getSpeechInfoList();
    this.isClosed = this._personInfoService.getClosed();
  }
  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '341', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '340', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '151' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          SpeechList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          MobilityList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          prenatalCareList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          )
        };
      })
      .share();
    this.SpeechList$ = source.pluck('SpeechList');
    this.MobilityList$ = source.pluck('MobilityList');
    this.prenatalCareDropdownItems$ = source.pluck('prenatalCareList');
  }
  private resetForm() {
    this.mobilitySpeechInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.isAddEdit = false;
    this.mobilitySpeechInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.isAddEdit = true;
    this.mobilitySpeechInfoForm.disable();
  }

  private edit(modal, i) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.mobilitySpeechInfoForm.enable();
  }

  private delete() {
    if (this.selectedMobilitySpeech) {
      const data = {personhlthmobilityspeechid: this.selectedMobilitySpeech.personhlthmobilityspeechid };
      this._healthService.saveHealth({ 'mobilitySpeechInfo': [data] }, 2).subscribe(response => {
        this._alertSevice.success('Mobility/Speech Information Deleted Successfully');
        this.getSpeechInfoList();
      });
    }

  }

  private cancel() {
    this.resetForm();
    if(this.isClosed) {
      window.scrollTo(0,0);
      this.isAddEdit = false;
    }
  }

  private patchForm(modal: MobilitySpeech) {

    this.mobilitySpeechInfoForm.patchValue(modal);
    let houseHoldOrColatral = null;
    if (modal.ishousehold) {
      houseHoldOrColatral = 1;
    }
    if (modal.iscollateral) {
      houseHoldOrColatral = 2;
    }
    this.mobilitySpeechInfoForm.patchValue({ ishousehold: houseHoldOrColatral });
  }

  private add() {
    const speechinfo = this.mobilitySpeechInfoForm.getRawValue();
    // this.speechcw.push(speechinfo);

    speechinfo.iscollateral = (speechinfo.ishousehold === 2) ? true : false;
    speechinfo.ishousehold = (speechinfo.ishousehold === 1) ? true : false;
    let isNew = 1;
    if (speechinfo.personhlthmobilityspeechid) {
      isNew = 0;
    }
    this._healthService.saveHealth({ 'mobilitySpeechInfo': [speechinfo] }, isNew).subscribe(response => {
      this._alertSevice.success('Mobility/Speech Information Added Successfully');
      this.getSpeechInfoList();
    });
    this.resetForm();
  }

  private update() {
    const speechinfo = this.mobilitySpeechInfoForm.getRawValue();
    speechinfo.iscollateral = (speechinfo.ishousehold === 2) ? true : false;
    speechinfo.ishousehold = (speechinfo.ishousehold === 1) ? true : false;
    let isNew = 1;
    if (speechinfo.personhlthmobilityspeechid) {
      isNew = 0;
    }
    this._healthService.saveHealth({ 'mobilitySpeechInfo': [speechinfo] }, isNew).subscribe(response => {
      this._alertSevice.success('Mobility/Speech Information Updated Successfully');
      this.getSpeechInfoList();
    });
    this.resetForm();
  }

  getSpeechInfoList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId }
    }, 'personfamilyinfo/getpersonmobility?filter').subscribe(res => {
      this.speechcw = res ? res.data : [];
    });
  }

  addSpeechInfo() {
    this.isAddEdit = true;
  }

  deleteConfirm(item) {
    (<any>$('#delete-popup')).modal('show');
    this.selectedMobilitySpeech = item;
  }

  // checkDec(el) {
  //   if (el.target.value !== '') {
  //     el.target.value = el.target.value.replace(/-/g, '');
  //     const dot = (el.target.value.indexOf('.') !== -1) ? '.' : '';
  //     let afterDot = (el.target.value.toString().split('.')[1]);
  //     let beforeDot = (el.target.value.toString().split('.')[0]);
  //     beforeDot = ( beforeDot && beforeDot.length > 2) ? beforeDot.substring(0, 2) : beforeDot;
  //     afterDot = ( afterDot && afterDot.length > 2) ? afterDot.substring(0, 2) : afterDot;
  //     el.target.value = (afterDot) ? beforeDot + dot + afterDot : beforeDot + dot;
  //     return  el.target.value;
  //   }
  //   return '';
  // }
}
