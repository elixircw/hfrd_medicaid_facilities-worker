import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { ImmunizationConstants } from './config';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { AppConstants } from '../../../../../@core/common/constants';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'immunization-cw',
  templateUrl: './immunization-cw.component.html',
  styleUrls: ['./immunization-cw.component.scss']
})
export class ImmunizationCwComponent implements OnInit {
  immunizationInfoForm: FormGroup;
  isImmunizedforPerson: boolean;
  immunizationHeaderStructure = [];
  immunizationDataStructure = [];
  immunizationTypes = [];
  ageType = 'BIRTH_TO_15';
  immunizationComments = '';
  selectedImmunization: any;
  selectedImmunizationComments: any;
  quillToolbar = AppConstants.NARRATIVE.TOOLBAR_CONFIG;
  ageGroups = [];
  maxDate = new Date();
  store: any;
  personage: any;
  set18diableflag: string;
  set19diableflag: string;

  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,) {
    this.store = this._dataStoreService.getCurrentStore();
     }

  ngOnInit() {
    this.isImmunizedforPerson = false;
    this.ageGroups = ImmunizationConstants.ageGroups;

    this.ageType = this.ageGroups[0].key;
    this.disableagegroups();
    this.processHeader();
    this.loadVaccineList();
    this.initForm();
  }

  disableagegroups() {
   console.log(this.store['personAge'],'this.store[]');
   this.personage = this.store['personAge'];
   if (this.personage < 18) {
     this.set18diableflag = '18_M_TO_18_Y';
     this.set19diableflag = 'YEAR_19_TO_YEAR_21';
   }
   else if (this.personage < 216) {
   this.set19diableflag = 'YEAR_19_TO_YEAR_21';
   }
  }

  processHeader() {
    this.immunizationHeaderStructure = ImmunizationConstants.headerInfo.filter(header => header.ageType === this.ageType);
    console.log('headerInfo', this.immunizationHeaderStructure);

  }
  updateData() {
    this.processHeader();
    // this.processImmunizationDataStructure(null);
    this.loadVaccineList();


  }

  processImmunizationDataStructure(data) {
    this.immunizationDataStructure = [];
    this.immunizationTypes.forEach(immunizationType => {
      const immunizationTypeRow = this.getImmunizationTypeRow(immunizationType, data);
      this.immunizationDataStructure.push(immunizationTypeRow);
    });
    console.log('data structure', this.immunizationDataStructure);
  }

  getImmunizationTypeRow(immunizationType, data) {
    const columns = [];
    const column = {
      template: '',
      value: ''
    };
    column.template = 'vaccine-type';
    column.value = immunizationType;
    columns.push(column);

    immunizationType.uiconfig.forEach(immunizeCol => {
      let personimmunizationid = null;
      if (data && Array.isArray(data) && data.length && immunizeCol && immunizeCol.dose) {
        const immunizedData = data.find(item => (item.personimmunizationconfigid === immunizationType.personimmunizationconfigid
          && item.dose === immunizeCol.dose));
        if (immunizedData) {
          let immunizedDate = null;
          if (immunizedData.immunizationdate) {
            immunizedDate = new Date(immunizedData.immunizationdate);
            immunizedDate.setMinutes( immunizedDate.getMinutes() + immunizedDate.getTimezoneOffset() );
          }

          immunizeCol.date = immunizedDate;
          immunizeCol.comments = immunizedData.comments;
          personimmunizationid = immunizedData.personimmunizationid;
        }
      }
      const doseColumn = {
        template: 'data',
        value: immunizeCol,
        immunizationtypekey: immunizationType.personimmunizationconfigid,
        personimmunizationid: personimmunizationid
      };
      /* 
     if (header.title === 'Vaccine') {
       column.template = 'vaccine-type';
       column.value = immunizationType;
     } else {

       column.template = 'type1';
       column.value = header.title + 'value';
     } */

      columns.push(doseColumn);
    });
    return columns;
  }

  initForm() {
    this.immunizationInfoForm = this._formBuilder.group({
      isimmunized: [''],
      immunizationkey: '',
      date: '',
      duedate: '',
      comments: ''
    });
  }

  isImmunizedExists(opt) {

    this.enableorDisableField('immunizationkey', opt);
    this.enableorDisableField('date', opt);
    this.enableorDisableField('duedate', opt);
    this.enableorDisableField('comments', opt);
    this.isImmunizedforPerson = opt;
    this.immunizationInfoForm.reset();
    this.immunizationInfoForm.patchValue({ 'isinsuranceavailable': opt });
  }
  private enableorDisableField(field, opt) {
    if (opt) {
      this.immunizationInfoForm.get(field).enable();
    }
  }

  private loadVaccineList() {
    this._commonHttpService.getPagedArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { 'agetype': this.ageType }
      },
      'personimmunizationconfig/list?filter'
    ).subscribe(immunizationTypes => {
      console.log('immunizationTypes', immunizationTypes);

      if (immunizationTypes.data && Array.isArray(immunizationTypes.data)) {
        this.immunizationTypes = immunizationTypes.data;
        console.log(this.immunizationTypes);
        this.getImmunizedData();
        this.processImmunizationDataStructure(null);
      }

    });
  }

  private getImmunizedData() {
    this._commonHttpService.getPagedArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { 'personid': this._personInfoService.getPersonId() }
      },
      'personimmunizationconfig/immunizationlist?filter'
    ).subscribe(immunizationTypes => {
      console.log('immunizationTypes', immunizationTypes);

      if (immunizationTypes.data && Array.isArray(immunizationTypes.data)) {
        console.log(this.immunizationTypes);
        this.processImmunizationDataStructure(immunizationTypes.data);
      }

    });
  }

  openImmunizationNotes(immunization) {
    this.selectedImmunization = immunization;
    this.selectedImmunizationComments = immunization.value.comments;
    (<any>$('#immunization-notes-popup')).modal('show');
  }



  saveComments() {
    // save api
    console.log('si', this.selectedImmunization);
    this.selectedImmunization.value.comments = this.selectedImmunizationComments;
    this.saveImmunization();
    (<any>$('#immunization-notes-popup')).modal('hide');


  }

  saveImmunization() {
    const data = {
      'personimmunizationconfigid': this.selectedImmunization.immunizationtypekey,
      'dose': this.selectedImmunization.value.dose,
      'immunizationdate': this.selectedImmunization.value.date,
      'comments': this.selectedImmunization.value.comments,
      'personimmunizationid': this.selectedImmunization.personimmunizationid,
      'isnew': this.selectedImmunization.personimmunizationid ? 0 : 1

    };

    this._healthService.saveHealth({ 'personImmunization': data }).subscribe(response => {
      this._alertService.success('Immunization Info Saved Successfully');
      this.getImmunizedData();
    });
  }

  immunizationDateChanged(immunization, i, j) {
    console.log('dc', immunization);
    const column = this.immunizationDataStructure[i];
    const newDate = new Date(immunization.value.date);
    let flag = 1;

    const res: number[] = column.map((item, ind) => {
      if (ind < j && item.template === 'data' && item.value.dose) {
        if (item.value.date) {
          const date = new Date(item.value.date);
          if (newDate < date) {
            flag = 0;
            immunization.value.date = null;
            return 0;
          } else {
            return 1;
          }
        } else {
          return -1;
        }
      } else if (ind > j && item.template === 'data' && item.value.dose) {
        if (item.value.date) {
          const date = new Date(item.value.date);
          if (newDate > date) {
            flag = 0;
            immunization.value.date = null;
            return -2;
          } else {
            return 1;
          }
        }
      }
    });

    if (res.includes(-2)) {
      this.immunizationDataStructure[i][j].value.date = null;
      this._alertService.warn('Date must be less than upcoming dosage date');
    } else if (res.includes(-1)) {
      this.immunizationDataStructure[i][j].value.date = null;
      this._alertService.warn('Please update previous dosage date');
    } else if (res.includes(0)) {
      this._alertService.warn('Date must be greater than previous dosage date');
    } else {
      this.selectedImmunization = immunization;
      this.saveImmunization();
    }
    // if (flag) {
    // this.selectedImmunization = immunization;
    // this.saveImmunization();
    // } else {
    //   this._alertService.warn('Date should be less than previous date');
    // }
  }

  cancelComments() {
    (<any>$('#immunization-notes-popup')).modal('hide');
  }
}
