import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { PersonHealthService } from '../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { InvolvedPerson } from '../../../involved-persons/_entities/involvedperson.data.model';
import { NavigationUtils } from '../../../../_utils/navigation-utils.service';
import { InvolvedPersonsService } from '../../../involved-persons/involved-persons.service';
import { PersonInfoService } from '../../person-info.service';

@Component({
  selector: 'feeding-info-cw',
  templateUrl: './feeding-info-cw.component.html',
  styleUrls: ['./feeding-info-cw.component.scss']
})
export class FeedingInfoCwComponent implements OnInit {

  FeedingInfoForm: FormGroup;
  FeedingPositionList$:  Observable<DropdownModel[]>;
  DietTypeList$: Observable<DropdownModel[]>;
  EaterTypeList$: Observable<DropdownModel[]>;
  LiquidList$: Observable<DropdownModel[]>;
  SolidFood$: Observable<DropdownModel[]>;
  uploadedFiles = [];
  personRoles = ([] = []);
  uploadNumber = '123434';
  isAddEdit: boolean;
  OtherNeedsList$:  Observable<DropdownModel[]>;
  constructor(private formbuilder: FormBuilder,private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _navigationUtils: NavigationUtils,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _involvedPersonService: InvolvedPersonsService,
    private _personInfoService: PersonInfoService) { }

  ngOnInit() {
    this.FeedingInfoForm = this.formbuilder.group({
      personfeedinginfoid: null,
      infoprovidedby: [null, Validators.required],
      clientlist: '',
      Collaterallist: '',
      provided_name: ['', Validators.required],
      relationship: '',
      isfeedinginfoknown: [null],
      diettype: '',
      eatertype: '',
      liquids: '',
      typeofformula: '',
      amountperfeeding: '',
      schedule: '',
      solidfood: '',
      feeding_position: '',
      otherneeds: '',
      comments: ''
    });
    this.loadDropDowns();
    this.getInvolvedPerson();
    this.getFeedingInfo();
    if (this._personInfoService.getClosed()) {
      this.FeedingInfoForm.disable();
    }
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '81', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '229', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '219', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '114', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '202', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '130', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          FeedingPositionList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          DietTypeList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          EaterTypeList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          LiquidList: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          SolidFood: result[4].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          OtherNeedsList: result[5].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
            };
          })
            .share();
     this.FeedingPositionList$ = source.pluck('FeedingPositionList');
     this.DietTypeList$ = source.pluck('DietTypeList');
     this.EaterTypeList$ = source.pluck ('EaterTypeList');
     this.LiquidList$ = source.pluck ('LiquidList');
     this.SolidFood$ = source.pluck('SolidFood');
     this.OtherNeedsList$ =source.pluck ('OtherNeedsList');
  }
  addFeeding() {
    const FeedingData = this.FeedingInfoForm.getRawValue();
    if (FeedingData && FeedingData.infoprovidedby && FeedingData.infoprovidedby === 1) {
      FeedingData.ishousehold = true;
      FeedingData.iscollateral = false;
    } else if (FeedingData && FeedingData.infoprovidedby && FeedingData.infoprovidedby === 2) {
      FeedingData.ishousehold = false;
      FeedingData.iscollateral = true;
    } else {
      FeedingData.ishousehold = false;
      FeedingData.iscollateral = false;
    }
    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;

    const data = {...FeedingData, ...uploadInfo};

    this._healthService.saveHealth({ 'feedingInfo': [data] }, 0).subscribe(response => {
      this._alertSevice.success('Feeding Information Saved Successfully');
      this.getFeedingInfo();
    });
    this.resetFeedingForm();
  }
  resetFeeding() {
    this.uploadedFiles = [];
    this.FeedingInfoForm.reset();
  }
  resetFeedingForm() {
    this.isAddEdit = false;
  }
  getFeedingInfo() {
    this._healthService.getFeedingInfo().subscribe( res => {
      if (res && res.count && res.data && res.data.length ) {
        const dataLength = res.data.length;
       const data = res.data[dataLength - 1];
       const infoprovidedby = ( data && data.ishousehold ) ?  1 :  ( ( data && data.iscollateral) ? 2 : null ) ;
       data.infoprovidedby = infoprovidedby;
       this.FeedingInfoForm.patchValue(data);
       this.uploadedFiles = data.uploadpath ? data.uploadpath : [];
      }
      // (res && res.count ) ? res.personsexualinfo : [];

    });
}
private getInvolvedPerson() {
  // this._involvedPersonService.getInvolvedPerson(1,10).subscribe(response => {
  //   console.log('clientList',response);
  // });
  Observable.forkJoin([
    this._commonHttpService
      .getPagedArrayList(
        {
          where: this._involvedPersonService.getRequestParam(),
          page: 1,
          limit: 20,
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListCWUrl + '?filter'
      )]).subscribe((result) => {
      if (result[0].data) {
        this.personRoles = [];
        result[0].data.map((list: InvolvedPerson) => {
          return this.personRoles.push({
            intakeservicerequestactorid: list.intakeservicerequestactorid,
            displayname: list.firstname + ' ' + list.lastname,
            personname: list.firstname + ' ' + list.lastname,
            role: list.roles
          });
        });
      }
    });
}
}
