import { Injectable } from '@angular/core';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { AppConstants } from '../../../../@core/common/constants';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { PersonInfoService } from '../person-info.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PersonHealthService {

  public healthDataListener$ = new Subject<any>();
  constructor(private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _personInfoService: PersonInfoService) {

  }

  saveHealth(data, isNew = 1) {
    const personId = this._personInfoService.getPersonId();
    const saveObject = { 'health': data, 'pid': personId, 'isnew': isNew};
    return this._commonHttpService
      .create(saveObject, 'People/personhealthaddupdate');
  }

  getPersonHealthInfo() {
    const personId = this._personInfoService.getPersonId();
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personId }
        }),
        'People/listpersonhealth?filter'
      );
  }

  setPersonHealthInfo() {
    this.getPersonHealthInfo().subscribe(healthInfo => {
      console.log('setting healthInfo', healthInfo);
      if (healthInfo && Array.isArray(healthInfo) && healthInfo.length) {
        this._dataStoreService.setData(AppConstants.GLOBAL_KEY.PERSON_HEALTH_INFO, healthInfo[0]);
        this.healthDataListener$.next(healthInfo);
      }
    });
  }

  getHealthInfoWithKey(keyName: string) {
    const healthInfo = this._dataStoreService.getData(AppConstants.GLOBAL_KEY.PERSON_HEALTH_INFO);
    console.log('getting healthInfo', healthInfo);
    if (healthInfo && healthInfo.hasOwnProperty(keyName)) {
      return healthInfo[keyName];
    } else {
      return null;
    }

  }

  getExaminationList() {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: this._personInfoService.getPersonId() }
        }),
        'personexamination/list?filter'
      );
  }

  getBirthInfo() {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: this._personInfoService.getPersonId() }
        }),
        'clientunder5yearsinfo/list?filter'
      );
  }

  getSexualInfo() {
    return this._commonHttpService
      .getSingle(
        new PaginationRequest({
          method: 'get',
          where: { personid: this._personInfoService.getPersonId() }
        }),
        'personsexualinfo/list?filter'
      );
  }

  getHospitalizationInfo() {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: this._personInfoService.getPersonId() }
        }),
        'personhospitalization/list?filter'
      );
  }

  getFeedingInfo() {
    return this._commonHttpService
    .getSingle(
      new PaginationRequest({
        method: 'get',
        where: { personid: this._personInfoService.getPersonId() }
      }),
      'personfamilyinfo/getpersonfeeding?filter'
    );
  }



}
