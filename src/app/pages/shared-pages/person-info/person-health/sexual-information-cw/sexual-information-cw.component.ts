import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// tslint:disable-next-line:import-blacklist
import { forkJoin } from 'rxjs/observable/forkJoin';
import { PersonSexual } from '../../../involved-persons/_entities/involvedperson.data.model';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AlertService, DataStoreService, CommonHttpService, CommonDropdownsService } from '../../../../../@core/services';
import { PersonHealthService } from '../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
// tslint:disable-next-line:import-blacklist
import * as moment from 'moment';
import { PersonInfoService } from '../../person-info.service';


const OTHER_KEY = '7780';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sexual-information-cw',
  templateUrl: './sexual-information-cw.component.html',
  styleUrls: ['./sexual-information-cw.component.scss']
})
export class SexualInformationCwComponent implements OnInit {
  sexualInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  sexualcw: any[];
  sexuallyTransmittedDiseases$: Observable<DropdownModel[]>;
  birthControlMethods$: Observable<DropdownModel[]>;
  sexualOrientationMethods$: Observable<any[]>;
  genderIdentiyList$: Observable<any[]>;

  isAddEdit: boolean;
  showSTDSpecify = false;
  showBCMSpecify = false;
  showSexualOrientationSpecify = false;
  showGenderIdentitySpecify = false;
  uploadedFiles = [];
  uploadNumber = '123434';
  selectedItem: any;
  dtDisable = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _commonDropDownService: CommonDropdownsService,
    private _personService: PersonInfoService
  ) { }

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
    this.getSexualInfo();
  }

  processSTDSpecify() {
    const data = this.sexualInfoForm.getRawValue();
    console.log(data.sextransdis);
    if (data.sextransdis === OTHER_KEY) {
      this.showSTDSpecify = true;
    } else {
      this.showSTDSpecify = false;
    }
  }

  processBCMSpecify() {
    const data = this.sexualInfoForm.getRawValue();
    console.log(data.birthcontrol);
    if (data.birthcontrol === OTHER_KEY) {
      this.showBCMSpecify = true;
    } else {
      this.showBCMSpecify = false;
    }

  }

  processSexaulOrientationpecify() {
    const data = this.sexualInfoForm.getRawValue();
    console.log(data.sexualorientationkey);
    if (data.sexualorientationkey === 'Other (specify)') {
      this.showSexualOrientationSpecify = true;
    } else {
      this.showSexualOrientationSpecify = false;
    }

  }

  processGenderIdentitySpecify() {
    const data = this.sexualInfoForm.getRawValue();
    console.log(data.genderidentity);
    if (data.genderidentity === 'Other (specify)') {
      this.showGenderIdentitySpecify = true;
    } else {
      this.showGenderIdentitySpecify = false;
    }

  }

  initForm() {
    this.sexualInfoForm = this._formBuilder.group({
      personsexualinfoid: null,
      childrenno: [null],
      pregnancyno: [null],
      ispregnant: [null, Validators.required],
      sextransdis: '',
      birthcontrol: '',
      stdspecify: '',
      bcspecify: '',
      specify: '',
      sicomments: '',
      sexualactiveflag: [null, Validators.required],
      std_treatment_startdate: [null],
      birthcontroldate: [null],
      sexualorientationkey: '',
      sexualorientationcomments: '',
      genderidentity: '',
      genderidentityspecify: '',
    });
  }

  private resetForm() {
    this.dtDisable = false;
    this.sexualInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.sexualInfoForm.enable();
    this.isAddEdit = false;
    this.uploadedFiles = [];
    this.showSTDSpecify = false;
    this.showBCMSpecify = false;
    this.showSexualOrientationSpecify = false;
    this.showGenderIdentitySpecify = false;
    this.uploadedFiles = [];
    this.selectedItem = null;
  }

 
  processOtherControls() {
    this.processBCMSpecify();
    this.processGenderIdentitySpecify();
    this.processSTDSpecify();
    this.processSexaulOrientationpecify();
  }
  getSexualActiveFlag(modal) {
    let sexualactiveflag = null;
    if (modal.sexualactiveflag === 1 || modal.sexualactiveflag === "1") {
      sexualactiveflag = 'Yes';
    } else if (modal.sexualactiveflag === 0 || modal.sexualactiveflag === "0") {
      sexualactiveflag = 'No';
    } else {
      sexualactiveflag = 'Unknown';
    }
    return sexualactiveflag;
  }
  getIsPregnant(modal) {
    let  ispregnant = null;
    if (modal.ispregnant) {
      ispregnant = 'yes';
    } else {
      ispregnant = 'no';
    }
    return ispregnant;
  }
  reMapValues(modal) { 
 
    modal.birthcontroldate = (modal.birthcontroldate) ? this._commonDropDownService.getValidDate(modal.birthcontroldate) : null;
    modal.std_treatment_startdate = (modal.std_treatment_startdate) ? this._commonDropDownService.getValidDate(modal.std_treatment_startdate) : null;
    return modal;

  }
  edit(modal, i) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    this.patchForm(this.reMapValues(modal));
    const ispregnant = this.getIsPregnant(modal);
    const sexualactiveflag = this.getSexualActiveFlag(modal);
    this.sexualInfoForm.patchValue({ ispregnant: ispregnant, sexualactiveflag: sexualactiveflag});
    this.processOtherControls();
    this.sexualInfoForm.enable();
  }

  view(modal, i) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.editMode = false;
    this.modalInt = i;
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    this.patchForm(this.reMapValues(modal));
    const ispregnant = this.getIsPregnant(modal);
    const sexualactiveflag = this.getSexualActiveFlag(modal);
    this.sexualInfoForm.patchValue({ ispregnant: ispregnant, sexualactiveflag: sexualactiveflag});
    this.processOtherControls();
    this.dtDisable = true;
    this.sexualInfoForm.disable();
  }

  

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: PersonSexual) {
    this.dtDisable = false;
    this.sexualInfoForm.patchValue(modal);
  }

  add() {
    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;
    const sexualinfo = { ...this.sexualInfoForm.getRawValue(), ...uploadInfo };
    // if (!this.sexualcw) {
    //   this.sexualcw = [];
    // }
    if (sexualinfo.uploadpath) {
      sexualinfo.uploadpath.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }
     sexualinfo.birthcontroldate = (sexualinfo.birthcontroldate) ? moment(new Date(sexualinfo.birthcontroldate)).format('MM/DD/YYYY') : null;
     sexualinfo.std_treatment_startdate = (sexualinfo.birthcontroldate) ? moment(new Date(sexualinfo.birthcontroldate)).format('MM/DD/YYYY') : null;
    // this.sexualcw.push(sexualinfo);
    let isNew = 1;
    let message = 'Reproductive Health Info Added Successfully';
    if (this.editMode) {
      isNew = 0;
      message = 'Reproductive Health Info Updated Successfully';
    }
    this._healthService.saveHealth({ 'reproductiveHealthInfo': [sexualinfo] }, isNew).subscribe(response => {
      this._alertSevice.success(message);
      this.getSexualInfo();
    });
    this.resetForm();
  }

  addHospitalization() {
    this.isAddEdit = true;
  }

  private update() {
    if (this.modalInt !== -1) {
      this.sexualcw[this.modalInt] = this.sexualInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '344', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '23', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        sexuallyTransmittedDiseasesValues: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.picklist_value_cd
            })
        ),
        birthControlMethodValues: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.picklist_value_cd
            })
        )
      };
    })
      .share();
    this.sexuallyTransmittedDiseases$ = source.pluck('sexuallyTransmittedDiseasesValues');
    this.birthControlMethods$ = source.pluck('birthControlMethodValues');
    this.sexualOrientationMethods$ = this._commonDropDownService.getListByTableID('332');
    this.genderIdentiyList$ = this._commonDropDownService.getListByTableID('333');
  }

  getSexualInfo() {
    this.sexualcw = [];
    this._healthService.getSexualInfo().subscribe(res => {

      this.sexualcw = (res && res.count) ? res.personsexualinfo : [];

    });
  }

  deleteConfirm(item, index) {
    (<any>$('#delete-popup')).modal('show');
    this.selectedItem = item;
  }

  delete() {
    const data = {
      'personsexualinfoid': this.selectedItem.personsexualinfoid,
    };
    this._healthService.saveHealth({ 'reproductiveHealthInfo': [data] }, 2).subscribe(_ => {
      this._alertSevice.success('Reproductive Health Info Deleted Successfully');
      this.resetForm();
      // this._healthService.setPersonHealthInfo();
      this.getSexualInfo();
    });
  }
}
