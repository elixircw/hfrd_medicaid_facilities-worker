import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { MedicalConditions, Health } from '../../../../../@core/common/models/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../involved-persons/_entities/involvedPersons.constants';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonInfoService } from '../../person-info.service';
import { PersonHealthService } from '../person-health.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'medical-conditions-cw',
    templateUrl: './medical-conditions-cw.component.html',
    styleUrls: ['./medical-conditions-cw.component.scss']
})
export class MedicalConditionsCwComponent implements OnInit {
    medicalconditionForm: FormGroup;
    modalInt: number;
    editMode: boolean;
    reportMode: string;
    minDate = new Date();
    maxDate = new Date();
    isCustomMedicalCondition: boolean;
    medicalcondition: MedicalConditions[] = [];
    
    health: Health = {};
    uploadedFiles = [];
    uploadNumber = '123434';
    constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
    medicalConditionType$: Observable<DropdownModel[]>;
    allergiesTypeDropdownItems$: Observable<DropdownModel[]>;
    isAddEdit = false;
    personId: string;
    dtDisable = false;
 
    constructor(private formbulider: FormBuilder,
        private _alertSevice: AlertService,
        private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService,
        private _personInfoService: PersonInfoService,
        private _healthService: PersonHealthService) { }

    ngOnInit() {
        this.personId =  this._personInfoService.getPersonId();
        this.loadDropDowns();
        this.editMode = false;
        this.isCustomMedicalCondition = false;
        this.modalInt = -1;
        this.reportMode = 'add';
        this.medicalconditionForm = this.formbulider.group({
            personmedicalconditionid: null,
            medical_condition: [null, Validators.required],
            start_Date: [null, Validators.required],
            End_Date: null,
            allergies_adverse_reactions: [],
            medication_client_allergies: '',
            notes: '',
            severitysymptomkey: '',
            ischronic: null,
            recordedby: ''
        });

        this.health = this._dataStoreService.getData(this.constants.Health);
        this.getMedicalCondititonList();
        if (this._personInfoService.getClosed()) {
            this.medicalconditionForm.disable();
        }
    }

    setMedicalCondition(option) {
        const medicalcondition = this.medicalconditionForm.getRawValue();
        const conditions = medicalcondition.medicalconditiontypekey;
        const Index = conditions.findIndex(c => c === 'Other ');
        if (Index !== -1) {
            this.isCustomMedicalCondition = true;
            this.medicalconditionForm.get('custommedicalcondition').setValidators([Validators.required]);
            this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
        } else {
            this.isCustomMedicalCondition = false;
            this.medicalconditionForm.get('custommedicalcondition').clearValidators();
            this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
        }
    }

    setmedicalconditionflag(opt) {
        if (opt) {
            this.isCustomMedicalCondition = true;
            this.medicalconditionForm.get('custommedicalcondition').setValidators([Validators.required]);
            this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
        } else {
            this.isCustomMedicalCondition = false;
            this.medicalconditionForm.get('custommedicalcondition').clearValidators();
            this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
        }
    }
    private loadDropDowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.medicalconditiontype + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                  method: 'get',
                  nolimit: true,
                  where: { 'active_sw': 'Y', 'picklist_type_id': '15', 'delete_sw': 'N' }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    medicalconditiontype: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.description
                            })
                    ),
                    allergiesTypeList: result[1].map(
                     (res) =>
                        new DropdownModel({
                        text: res.description_tx,
                        value: res.value_tx
                        })
                    ),
                };
            })
            .share();
        this.medicalConditionType$ = source.pluck('medicalconditiontype');
        this.allergiesTypeDropdownItems$ = source.pluck('allergiesTypeList');
    }
    private add() {
        const uploadInfo = {};
        uploadInfo['uploadpath'] = this.uploadedFiles;      
        const data = {...this.medicalconditionForm.getRawValue(), ...uploadInfo};
        this._healthService.saveHealth({ 'medicalConditions': [data] }).subscribe(response => {
            this._alertSevice.success('Medical Conditions Added Successfully');
            this.resetForm();
            this.getMedicalCondititonList();
        });
    }

    private resetForm() {
        this.medicalconditionForm.reset();
        this.modalInt = -1;
        this.editMode = false;
        this.reportMode = 'add';
        this.medicalconditionForm.enable();
        this.isAddEdit = false;
        this.uploadedFiles = [];
        this.dtDisable = false;
    }

    private update() {
        if (this.modalInt !== -1) {

            this.medicalcondition[this.modalInt] = this.medicalconditionForm.getRawValue();
        }
       // this.resetForm();
       const uploadInfo = {};
       uploadInfo['uploadpath'] = this.uploadedFiles;
       const data = {...this.medicalconditionForm.getRawValue(), ...uploadInfo};

       this._healthService.saveHealth({ 'medicalConditions': [data] },0).subscribe(response => {
        this._alertSevice.success('Added Medical Conditions Successfully');
        this.resetForm();
        this.getMedicalCondititonList();
    });
        this._alertSevice.success('Updated Successfully');
    }

    private view(modal) {
        this.isAddEdit = true;
        this.reportMode = 'edit';
        const flag = modal.custommedicalcondition ? true : false;
        let medCond = JSON.parse(modal.medicalcondition);
        this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
        this.medicalconditionForm.controls['personmedicalconditionid'].setValue(modal.personmedicalconditionid);
        this.medicalconditionForm.controls['medical_condition'].setValue(medCond);
        this.medicalconditionForm.controls['start_Date'].setValue(modal.begindate)
        this.medicalconditionForm.controls['End_Date'].setValue(modal.enddate);
        let medAllCon = JSON.parse(modal.allergies_adverse_reactions);
        this.medicalconditionForm.controls['allergies_adverse_reactions'].setValue(medAllCon);
        this.medicalconditionForm.controls['recordedby'].setValue(modal.recordedby);
        this.medicalconditionForm.controls['severitysymptomkey'].setValue(modal.severitysymptomkey);
        this.medicalconditionForm.controls['medication_client_allergies'].setValue(modal.medication_client_allergies);
        this.medicalconditionForm.controls['notes'].setValue(modal.notes);
        this.medicalconditionForm.controls['ischronic'].setValue(modal.ischronic);
        // this.patchForm(modal);
        this.editMode = false;
        this.dtDisable = true;
        this.medicalconditionForm.disable();
    }

    private edit(modal, i) {
        this.isAddEdit = true;
        this.reportMode = 'edit';
        this.editMode = true;
        this.modalInt = i;
        this.dtDisable = false;
        let medCond = JSON.parse(modal.medicalcondition);
        this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
        this.medicalconditionForm.controls['personmedicalconditionid'].setValue(modal.personmedicalconditionid);
        this.medicalconditionForm.controls['medical_condition'].setValue(medCond);
        this.medicalconditionForm.controls['start_Date'].setValue(modal.begindate)
        this.medicalconditionForm.controls['End_Date'].setValue(modal.enddate);
        let medAllCon = JSON.parse(modal.allergies_adverse_reactions);
        this.medicalconditionForm.controls['allergies_adverse_reactions'].setValue(medAllCon);
        this.medicalconditionForm.controls['recordedby'].setValue(modal.recordedby);
        this.medicalconditionForm.controls['severitysymptomkey'].setValue(modal.severitysymptomkey);
        this.medicalconditionForm.controls['medication_client_allergies'].setValue(modal.medication_client_allergies);
        this.medicalconditionForm.controls['notes'].setValue(modal.notes);
        this.medicalconditionForm.controls['ischronic'].setValue(modal.ischronic);
        this.medicalconditionForm.enable();
    }

    private delete(modal,index) {
        // this.medicalcondition.splice(index, 1);
        // this._alertSevice.success('Deleted Successfully');
        // this.resetForm();
        const data = {
        'personmedicalconditionid': modal.personmedicalconditionid,
        };
        this._healthService.saveHealth({ 'medicalConditions': [data] }, 2).subscribe(_ => {
            this._alertSevice.success('Medical Condition Info Deleted Successfully');
            this.resetForm();
            this.getMedicalCondititonList();
        });
      
    }

    private cancel() {
        this.resetForm();
    }

    startDateChanged() {
        this.medicalconditionForm.patchValue({ End_Date: '' });
        const empForm = this.medicalconditionForm.getRawValue();
        this.maxDate = new Date(empForm.begindate);
    }
    endDateChanged() {
        this.medicalconditionForm.patchValue({ begindate: '' });
        const empForm = this.medicalconditionForm.getRawValue();
        this.minDate = new Date(empForm.End_Date);
    }
    private patchForm(modal: MedicalConditions) {
        this.medicalconditionForm.patchValue(modal);
    }

    addMedicalCondition() {
        this.isAddEdit = true;
    }

    getMedicalCondititonList() {
        this._commonHttpService.getPagedArrayList({
          page: 1,
          limit: 20,
          method: 'get',
          where: { personid: this.personId}
        }, 'personmedicalcondition/list?filter').subscribe(res => {
           this.medicalcondition = res ? res.data : [];
        });
      }
      getMedicalCondition(medicalCondition) {
        if (medicalCondition && medicalCondition !== 'null') {
            const medical = JSON.parse(medicalCondition).join();
            return medical;
        }
        return '';
      }

}
