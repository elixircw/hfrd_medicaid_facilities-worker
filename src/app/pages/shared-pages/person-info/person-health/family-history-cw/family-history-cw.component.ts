import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { FamilyHistory, Health, InvolvedPerson } from '../../../involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../involved-persons/_entities/involvedPersons.constants';
import { PersonHealthService } from '../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonInfoService } from '../../person-info.service';
import { NavigationUtils } from '../../../../_utils/navigation-utils.service';
import { InvolvedPersonsService } from '../../../involved-persons/involved-persons.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'family-history-cw',
  templateUrl: './family-history-cw.component.html',
  styleUrls: ['./family-history-cw.component.scss']
})
export class FamilyHistoryCwComponent implements OnInit {
  familyHistoryForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  familyHistorycw: FamilyHistory[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  relationshipTypeDropdownItems$: Observable<DropdownModel[]>;
  deathCauseTypeDropdownItems$: Observable<DropdownModel[]>;
  infoclienttypekeyDropdownItems$: Observable<DropdownModel[]>;
  id: string;
  personRoles = ([] = []);
  isAddEdit = false;
  involevedPerson$: any;
  majorHealthProblem$: Observable<DropdownModel[]>;
  teamTypeKey: any;
  personId: string;
  uploadedFiles = [];
  uploadNumber = '123434';
  selectedItem: any;
  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService,
    private _navigationUtils: NavigationUtils,
    private _involvedPersonService: InvolvedPersonsService,
    private _authService: AuthService) {
  }

  ngOnInit() {
    this.personId = this._personInfoService.getPersonId();
    this.teamTypeKey = this._authService.getAgencyName();
    this.reportMode = 'add';
    this.loadDropDowns();
    this.getInvolvedPerson();
    this.initForm();
    this.getFamilyHistoryList();
  }

  initForm() {
    this.familyHistoryForm = this._formBuilder.group({
      personfmlymdcl_hstryid: null,
      clientlist: ['', [Validators.required]],
      // Relationship: 'null',
      major_health_problems: ['', [Validators.required]],
      cause_of_death: [''],
      comments: ''
    });
    if (this._personInfoService.getClosed()) {
      this.familyHistoryForm.disable();
    }
  }

  private resetForm() {
    this.familyHistoryForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.familyHistoryForm.enable();
    this.uploadedFiles = [];
    this.isAddEdit = false;
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    this.editMode = false;
    this.familyHistoryForm.disable();
  }

  private edit(modal, i) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.editMode = true;
    console.log(modal);
    const data = Object.create(modal);
    data.comments = data.comments;
    data.clientlist = data.clientlist;
    data.personfmlymdclhstryid = data.personfmlymdclhstryid;
    data.personid = data.personid;
    data.relationship = data.relationship;
    data.major_health_problems = (data.major_health_problems && data.major_health_problems.length) ? data.major_health_problems.map(data => data.ref_key) : [];
    data.cause_of_death = (data.cause_of_death) ? data.cause_of_death.value_tx : '';
    this.uploadedFiles = modal.uploadpath ? modal.uploadpath : [];
    console.log(data);
    this.patchForm(data);
    this.familyHistoryForm.enable();
  }

  

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: FamilyHistory) {
    this.familyHistoryForm.patchValue(modal);
  }

  private add() {
    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;
    const data = { ...this.familyHistoryForm.getRawValue(), ...uploadInfo };

    if (data.uploadpath) {
      data.uploadpath.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }
    let isNew = 1;
    let message = 'Family History Added Successfully';
    if (this.editMode) {
      isNew = 0;
      message = 'Family History Updated Successfully';
    }
    data.major_health_problems = (data.major_health_problems.length) ? data.major_health_problems.join(', ') : null;
    this._healthService.saveHealth({ 'personfamilyHistroy': [data] }, isNew).subscribe(response => {
      this._alertSevice.success(message);
      this.getFamilyHistoryList();
    });
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.familyHistorycw[this.modalInt] = this.familyHistoryForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { activeflag: 1 }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.RelationshipTypesUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '34', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService
        .getArrayList(
          {
            where: { "tablename": "majorhealthprob", "teamtypekey": this.teamTypeKey },
            method: 'get'
          },
          'referencetype/gettypes' + '?filter'
        )
    ]).map((result) => {
      return {
        relationshipTypeList: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.relationshiptypekey
            })
        ),
        deathCauseTypeList: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.picklist_value_cd
            })
        ),
        majorHealthProblem: result[2].map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.ref_key
            })
        )
      };
    })
      .share();
    this.relationshipTypeDropdownItems$ = source.pluck('relationshipTypeList');
    this.deathCauseTypeDropdownItems$ = source.pluck('deathCauseTypeList');
    this.majorHealthProblem$ = source.pluck('majorHealthProblem');
  }

  private getInvolvedPerson() {
    // this._involvedPersonService.getInvolvedPerson(1,10).subscribe(response => {
    //   console.log('clientList',response);
    // });
   // const servicecaseId = (this._navigationUtils.getPersonRequestParam().servicecaseid) ? this._navigationUtils.getPersonRequestParam().servicecaseid : null;
    Observable.forkJoin([
      this._commonHttpService
        .getPagedArrayList(
          {
            where: this._involvedPersonService.getRequestParam(),
            page: 1,
            limit: 20,
            method: 'get'
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListCWUrl + '?filter'
        )]).subscribe((result) => {
          if (result[0].data) {
            this.personRoles = [];
            result[0].data.map((list: InvolvedPerson) => {
              return this.personRoles.push({
                intakeservicerequestactorid: list.intakeservicerequestactorid,
                displayname: list.firstname + ' ' + list.lastname,
                personname: list.firstname + ' ' + list.lastname,
                role: list.roles
              });
            });
          }
        });
  }

  addFamilyHistory() {
    this.isAddEdit = true;
  }

  getFamilyHistoryList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId }
    }, 'personfamilyinfo/getpersonfamilyhistory?filter').subscribe(res => {
      this.familyHistorycw = res ? res.data : [];
    });
  }

  displayMajorHealthProblems(healthProblems) {
    console.log(healthProblems);
    if (healthProblems && healthProblems.length > 0) {
      return healthProblems.map(data => data.description).join(",");
    }
    return '';
  }

  deleteConfirm(item, index) {
    (<any>$('#delete-popup')).modal('show');
    this.selectedItem = item;
  }

  delete() {
    const data = {
      'personfmlymdcl_hstryid': this.selectedItem.personfmlymdcl_hstryid,
    };
    this._healthService.saveHealth({ 'personfamilyHistroy': [data] }, 2).subscribe(_ => {
      this._alertSevice.success('Family History Deleted Successfully');
      this.resetForm();
      // this._healthService.setPersonHealthInfo();
      this.getFamilyHistoryList();
    });
  }

}
