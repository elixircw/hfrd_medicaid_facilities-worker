import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxMaskModule } from 'ngx-mask';

import {A2Edatetimepicker} from 'ng2-eonasdan-datetimepicker';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PersonHealthComponent } from './person-health.component';
import { PersonHealthService } from './person-health.service';
import { PersonHealthRoutingModule } from './person-health-routing.module';
import { BirthInformationCwComponent } from './birth-information-cw/birth-information-cw.component';
import { Under5yearsCwComponent } from './under-5years-cw/under-5years-cw.component';
import { SubstanceAbuseComponent } from './substance-abuse/substance-abuse.component';
import { PersonDetailsService } from '../../../person-details/person-details.service';
import { SexualInformationCwComponent } from './sexual-information-cw/sexual-information-cw.component';
import { HospitalizationCwComponent } from './hospitalization-cw/hospitalization-cw.component';
import { BehavioralHealthInfoCwComponent } from './behavioral-health-info-cw/behavioral-health-info-cw.component';
import { MedicationIncludingPsychotropicCwComponent } from './medication-including-psychotropic-cw/medication-including-psychotropic-cw.component';
import { ImmunizationCwComponent } from './immunization-cw/immunization-cw.component';
import { PersonInfoService } from '../person-info.service';
import { FamilyHistoryCwComponent } from './family-history-cw/family-history-cw.component';
import { InvolvedPersonsService } from '../../involved-persons/involved-persons.service';
import { PersonExaminationModule } from './person-examination/person-examination.module';
// import { SubstanceAbuseService } from './substance-abuse/substance-abuse.service';
import { QuillModule } from 'ngx-quill';
import { ProviderInformationCwComponent } from './provider-information-cw/provider-information-cw.component';
import { PersonHealthDisabilityComponent } from './person-health-disability/person-health-disability.component';
import { PersonDisabilityService } from '../../person-disability/person-disability.service';
import { FeedingInfoCwComponent } from './feeding-info-cw/feeding-info-cw.component';
import { InsuranceInformationCwComponent } from './insurance-information-cw/insurance-information-cw.component';
import { MedicalConditionsCwComponent } from './medical-conditions-cw/medical-conditions-cw.component';
import { ShareFeaturesModule } from '../share-features/share-features.module';
import { SleepingComponent } from './sleeping/sleeping.component';
import { EliminationComponent } from './elimination/elimination.component';
import { MobilitySpeechCwComponent } from './mobility-speech-cw/mobility-speech-cw.component';
@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    PersonHealthRoutingModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    A2Edatetimepicker,
    PersonExaminationModule,
    QuillModule,
    ShareFeaturesModule

  ],
  declarations: [
    PersonHealthComponent,
    // AllergiesCwComponent,
    // ChronicCwComponent,
    BehavioralHealthInfoCwComponent,
     BirthInformationCwComponent,
     Under5yearsCwComponent,
     SubstanceAbuseComponent,
     SexualInformationCwComponent,
     HospitalizationCwComponent,
     MedicationIncludingPsychotropicCwComponent,
     FamilyHistoryCwComponent,
    EliminationComponent,
    MobilitySpeechCwComponent,
    MedicalConditionsCwComponent,
    ImmunizationCwComponent,
    ProviderInformationCwComponent,
    PersonHealthDisabilityComponent,
    FeedingInfoCwComponent,
    InsuranceInformationCwComponent,
    SleepingComponent
  ],
  exports: [
    // AllergiesCwComponent,
    // ChronicCwComponent,
    // BehavioralHealthInfoCwComponent,
    // BirthInformationCwComponent,
    // FamilyHistoryCwComponent,
    // EliminationComponent,
    // SleepingComponent,
    // MobilitySpeechCwComponent,
    // FeedingInfoCwComponent,
    // InsuranceInformationCwComponent,
    // ImmunizationCwComponent
    PersonHealthDisabilityComponent
  ],
  providers: [PersonHealthService, InvolvedPersonsService,
  PersonDisabilityService]
})
export class PersonHealthInfoModule { }
