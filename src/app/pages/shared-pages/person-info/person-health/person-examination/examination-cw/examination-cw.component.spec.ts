import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationCwComponent } from './examination-cw.component';

describe('ExaminationCwComponent', () => {
  let component: ExaminationCwComponent;
  let fixture: ComponentFixture<ExaminationCwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExaminationCwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationCwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
