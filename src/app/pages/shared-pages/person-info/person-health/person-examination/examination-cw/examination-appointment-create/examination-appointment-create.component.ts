import { Component, OnInit, Input } from '@angular/core';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonHttpService } from '../../../../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { CaseWorkerUrlConfig } from '../../../../../../case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
import { PersonExaminationService } from '../../person-examination.service';



@Component({
  selector: 'examination-appointment-create',
  templateUrl: './examination-appointment-create.component.html',
  styleUrls: ['./examination-appointment-create.component.scss']
})
export class ExaminationAppointmentCreateComponent implements OnInit {

  examTypeDropdownItems$: Observable<any[]>;
  specialityExamTypeDropdownItems$: Observable<any[]>;
  labTestDropdownItems$: Observable<any[]>;
  APPOINTMENT_KEPT = 0;
  APPOINTMENT_NOT_KEPT = 1;
  APPOINTMENT_REFUSED = 2;

  appointmentActions = [];
  min: Date;
   
  @Input() appointments: any[];
  @Input() disabled = false;
  constructor(private _commonHttpService: CommonHttpService,
    private _examinationService: PersonExaminationService) { }


  ngOnInit() {
    this.min = new Date();
    this.loadDropDown();
  }
  private loadDropDown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '320' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '318' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '319' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          examTypeList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          specialityExamTypeList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          labTestList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          )
        };
      })
      .share();
    this.examTypeDropdownItems$ = source.pluck('examTypeList');
    this.specialityExamTypeDropdownItems$ = source.pluck('specialityExamTypeList');
    this.labTestDropdownItems$ = source.pluck('labTestList');
    this.appointmentActions =  [{
      ref_key: 0,
      description: 'Kept'
    },
    {
      ref_key: 1,
      description: 'Not kept'
    },
    {
      ref_key: 2,
      description: 'Refused'
    }];
  }

  removeItem(index) {
    this.appointments.splice(index, 1);
  }

  addItem() {
    this.appointments.push(this._examinationService.getNewAppointment());
  }

}
