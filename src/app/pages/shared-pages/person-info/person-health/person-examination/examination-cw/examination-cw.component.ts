import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import { InvolvedPersonsConstants } from '../../../../involved-persons/_entities/involvedPersons.constants';
import { AlertService, DataStoreService, CommonHttpService, ValidationService } from '../../../../../../@core/services';
import { PersonHealthService } from '../../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../../case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { HealthConstants } from '../../health-constants';
import { PersonExaminationService } from '../person-examination.service';
import { Subscription } from 'rxjs/Subscription';
import { PersonInfoService } from '../../../person-info.service';



@Component({
  // tslint:disable-next-line:component-selector
  selector: 'examination-cw',
  templateUrl: './examination-cw.component.html',
  styleUrls: ['./examination-cw.component.scss']
})
export class ExaminationCwComponent implements OnInit, OnDestroy {


  examinationInfoForm: FormGroup;
  // examTypeList: any[];
  // specialityExamTypeList: any[];
  // labTestList: any[];
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  examinationList: any[] = [];
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  examTypeDropdownItems$: Observable<any[]>;
  specialityExamTypeDropdownItems$: Observable<any[]>;
  labTestDropdownItems$: Observable<any[]>;
  stateDropdownItems$: Observable<any[]>;
  countyDropDownItems$: Observable<any[]>;
  appointments: any[] = [];
  isAddEdit = false;
  isProviderAvailable = null;
  healthSubscription: Subscription;
  uploadedFiles = [];
  uploadNumber = '123434';
  selectedExamination = null;
  deleteIndex = null;
  examinationDisable = false;

  address = { address1: null, address2: null, city: null, state: null, zipcode: null, county: null, disable: false};

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _healthService: PersonHealthService,
    private _service: PersonExaminationService,
    private _personService: PersonInfoService
  ) {

  }

  ngOnInit() {
    this.reportMode = 'add';
    // this.loadDropDown();
    this.examinationInfoForm = this.formbulider.group({
      affilication: '',
      speciality: '',
      physicianname: '',
      recommendations: '',
      comments: '',
      medicalreferrals: '',
      followupneeded: '',
      phone: '',
      email: ['', [ValidationService.mailFormat]]

    });

    this.loadExaminationList();
  }

  resetAddress() {
    this.address = { address1: null, address2: null, city: null, state: null, zipcode: null, county: null, disable: false};
  }



  loadExaminationList() {
    this._healthService.getExaminationList().subscribe(list => {
      if (list && Array.isArray(list) && list.length) {
        this.examinationList = list;
      }
      console.log('e list', list);
    });
  }

  private resetForm() {

    this.examinationInfoForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.address.disable = false;
    this.examinationDisable = false;
    this.examinationInfoForm.enable();

  }


  private view(examination) {
    const header = document.getElementById('examination-form-container');
    header.scrollIntoView();
    this.selectedExamination = examination;
    this.examinationDisable = false;
    this.isProviderAvailable = examination.providerinfoflag ? true : false;
    if (examination && Array.isArray(examination.appointment)) {
      this.appointments = examination.appointment;
      // this.address = examination.physician;
      this.address.address1 =  examination.physician.address1;
      this.address.address2 =  examination.physician.address2;
      this.address.city =  examination.physician.city;
      this.address.state =  examination.physician.state;
      this.address.county =  examination.physician.county;
      this.address.zipcode =  examination.physician.zip;
      this.examinationInfoForm.patchValue(examination.physician);
      this.uploadedFiles = Array.isArray(examination.uploadpath) ? examination.uploadpath : [];
    }
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.address.disable = true;
    this.editMode = false;
    this.examinationDisable = true;
    this.examinationInfoForm.disable();
  }

  edit(examination) {
    const header = document.getElementById('examination-form-container');
    header.scrollIntoView();
    this.reportMode = 'edit';
    this.editMode = true;
    this.selectedExamination = examination;
    this.isAddEdit = true;
    this.examinationDisable = false;
    this.isProviderAvailable = examination.providerinfoflag ? true : false;
    if (examination && Array.isArray(examination.appointment)) {
      this.appointments = examination.appointment;
      // this.address = examination.physician;
      this.address.address1 =  examination.physician.address1;
      this.address.address2 =  examination.physician.address2;
      this.address.city =  examination.physician.city;
      this.address.state =  examination.physician.state;
      this.address.county =  examination.physician.county;
      this.address.zipcode =  examination.physician.zip;
      this.address.disable = false;
      this.examinationInfoForm.patchValue(examination.physician);
      this.uploadedFiles = Array.isArray(examination.uploadpath) ? examination.uploadpath : [];
    }
    this.examinationInfoForm.enable();
  }



  cancel() {
    this.resetForm();
  }

  add() {
    console.log(this.appointments);
    console.log(this.isProviderAvailable);

    const appointmentDate = this.appointments[0].apptDate;
    const natureOfExam = this.appointments[0].natureofexamkey;

     if (!appointmentDate || !natureOfExam || this.isProviderAvailable == null) {
       this._alertSevice.error('Please fill required fields');
       return;
     }

    const uploadInfo = {};
    uploadInfo['uploadpath'] = this.uploadedFiles;
    if (!this.isProviderAvailable) {
      this.examinationInfoForm.reset();
      this.resetAddress();
    }
    const phyData = this.examinationInfoForm.getRawValue();
    const physicianInfo = { ...phyData, ...this.address, ...uploadInfo };
    physicianInfo.medicalreferrals = phyData.medicalreferrals;
    physicianInfo.followupneeded = phyData.followupneeded;
    physicianInfo.zip = this.address.zipcode;
    let personexaminationid = null;
    let isNew = 1;
    if (this.selectedExamination && this.selectedExamination.personexaminationid) {
      personexaminationid = this.selectedExamination.personexaminationid;
      isNew = 0;
    }

    const data = {
      'appointment': this.appointments,
      'physician': physicianInfo,
      'providerinfoflag': this.isProviderAvailable,
      'personexaminationid': personexaminationid,
      'uploadpath': this.uploadedFiles
    };

    if (data.uploadpath) {
      data.uploadpath.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }
    this._healthService.saveHealth({ 'personExamination': [data] }, isNew).subscribe(_ => {
      if(this.reportMode === 'edit'){
        this._alertSevice.success('Examination Updated Successfully');
      }else{
        this._alertSevice.success('Examination Saved Successfully');
      }      
      this.resetAll();
      this.loadExaminationList();
    });
  }

  resetAll() {
    if(this.reportMode === 'add'){
    this.appointments[0].apptkept = null;
    this.appointments[0].apptDate = null;
    this.appointments[0].nextApptDate = null;
    this.appointments[0].natureofexamkey = null;
    this.appointments[0].labtestkey = null;
    this.appointments[0].specialityexamkey = null;
    this.appointments[0].hivtestreceived = null;
    this.appointments[0].nextappointmentreason = null;
    this.appointments[0].notkeptreason = null;
    this.selectedExamination = null;
    this.resetAddress();
    this.isProviderAvailable = null;
    this.isAddEdit = true;
    this.resetForm();
    this.uploadedFiles = [];
    }else{
    this.appointments = [];
    this.selectedExamination = null;
    this.resetAddress();
    this.isProviderAvailable = null;
    this.isAddEdit = false;
    this.resetForm();
    this.uploadedFiles = [];
    }
    
  }



  addNewExamination() {
    this.isAddEdit = true;
    const appointment = this._service.getNewAppointment();
    this.appointments = [appointment];
  }

  ngOnDestroy(): void {
  }

  deleteConfirm(item, index) {
    (<any>$('#delete-popup')).modal('show');
    this.selectedExamination = item;
    this.deleteIndex = index;
  }

  delete() {
    this.reportMode = 'delete';
    this.examinationList.splice(this.deleteIndex, 1);
    const data = {
      'personexaminationid': this.selectedExamination.personexaminationid,
    };
    this._healthService.saveHealth({ 'personExamination': [data] }, 2).subscribe(_ => {
      this._alertSevice.success('Deleted Examination Successfully');
      this.resetAll();
      // this._healthService.setPersonHealthInfo();
      this.loadExaminationList();
    });
  }



}
