import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonExaminationRoutingModule } from './person-examination-routing.module';
import { ExaminationCwComponent } from './examination-cw/examination-cw.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { ExaminationAppointmentCreateComponent } from './examination-cw/examination-appointment-create/examination-appointment-create.component';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';
import { PersonExaminationService } from './person-examination.service';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxMaskModule } from 'ngx-mask';
import { ShareFeaturesModule } from '../../share-features/share-features.module';

@NgModule({
  imports: [
    CommonModule,
    PersonExaminationRoutingModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    ShareFeaturesModule

  ],
  declarations: [
    ExaminationCwComponent,
    ExaminationAppointmentCreateComponent

  ],
  providers: [PersonExaminationService]

})
export class PersonExaminationModule { }
