import { Injectable } from '@angular/core';

@Injectable()
export class PersonExaminationService {

  constructor() { }

  getNewAppointment() {
    return {
      apptkept : null,
      apptDate: null,
      nextApptDate: null,
      natureofexamkey: null,
      labtestkey: null,
      specialityexamkey: null,
      hivtestreceived: null,
      nextappointmentreason: null,
      notkeptreason: null
    };
  }

}
