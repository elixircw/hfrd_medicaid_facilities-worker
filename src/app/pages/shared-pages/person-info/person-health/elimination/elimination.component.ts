import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { HealthConstants } from '../health-constants';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonHealthService } from '../person-health.service';
import { PersonInfoService } from '../../person-info.service';

export class PersonElimination
{
  infoprovidedby: string;
  clientlist: string;
  collaterallist: string;
  infoprovidedname: string;
  relationship: string;
  eliminationinfounknown: boolean;
  currentstatus: string;
  toilettraining: string;
  otherSpecify: string;
  bowelmovement: string;
  urination: string;
  toiletcomments: string;
  considerunknown: string;
  specialcomments:string;
}
@Component({
  selector: 'elimination',
  templateUrl: './elimination.component.html',
  styleUrls: ['./elimination.component.scss']
})
export class EliminationComponent implements OnInit {

  EliminationInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  eliminationcw = [];
  StatusList$: Observable<DropdownModel[]>;
  TrainingList$: Observable<DropdownModel[]>;
  isAddEdit = false;
  personId: string;
  eliminationId: string;
  selected = [];
  isClosed = false;

  constructor(
    private formbuilder: FormBuilder,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService ) {}

  ngOnInit() {
    this.personId =  this._personInfoService.getPersonId();
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
    this.getEliminationInfoList();
    this.isClosed = this._personInfoService.getClosed();
   }

  private resetForm() {
    this.EliminationInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.EliminationInfoForm.enable();
    this.selected =  [];
  }

  private view(modal) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.editMode = false;
    modal.infoprovidedname 		  = modal.v_providedname;
    modal.eliminationinfounknown= modal.v_iseliminationinfoknown;
    modal.currentstatus         = modal.v_elimination_currentstatus;
    modal.toilettraining        = modal.v_toilettrainingmethod;
    modal.otherSpecify        = modal.v_otherspecify;
    modal.bowelmovement         = modal.v_wordforbowelmovement;
    modal.urination             = modal.v_wordforurination;
    modal.toiletcomments        = modal.v_toiletcomments;
    modal.specialcomments       = modal.v_specialcomments;
    modal.considerunknown       = modal.v_considerunknown;
    this.eliminationId          = modal.v_personhltheliminationid;
    this.selected =  modal.v_toilettrainingmethod;
    this.patchForm(modal);
    this.EliminationInfoForm.disable();
  }

  private edit(modal, i) {
    this.isAddEdit = true;
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    modal.infoprovidedname 		  = modal.v_providedname;
    modal.eliminationinfounknown= modal.v_iseliminationinfoknown;
    modal.currentstatus         = modal.v_elimination_currentstatus;
    modal.toilettraining        = modal.v_toilettrainingmethod;
    modal.otherSpecify        = modal.v_otherspecify;
    modal.bowelmovement         = modal.v_wordforbowelmovement;
    modal.urination             = modal.v_wordforurination;
    modal.toiletcomments        = modal.v_toiletcomments;
    modal.specialcomments       = modal.v_specialcomments;
    modal.considerunknown       = modal.v_considerunknown;
    this.eliminationId          = modal.v_personhltheliminationid;
    this.patchForm(modal);
    this.selected =  modal.v_toilettrainingmethod;
    this.EliminationInfoForm.enable();
  }

  private delete(modal, index) {
    // this.eliminationcw.splice(index, 1);
    // this._alertService.success('Deleted Successfully');
    // this.resetForm();
    const data = {
      'personhltheliminationid': modal.v_personhltheliminationid,
    };
    this._healthService.saveHealth({ 'personElimination': [data] }, 2).subscribe(_ => {
      this._alertService.success('Elimination Info Deleted Successfully');
        this.resetForm();
        this.getEliminationInfoList();
      });
  }

  private cancel() {
    this.resetForm();
    if(this.isClosed) {
      window.scrollTo(0,0);
      this.isAddEdit = false;
    }
  }

  private patchForm(modal: PersonElimination) {
    this.EliminationInfoForm.patchValue(modal);
  }

  private update() {
    const modal = this.EliminationInfoForm.getRawValue();
    modal.personhltheliminationid = this.eliminationId;
    this._healthService.saveHealth({ 'personElimination': [modal] }, 0).subscribe(response => {
      this._alertService.success('Elimination Info Updated Successfully');
      this.getEliminationInfoList();
    });
    this.resetForm();
  }

  add() {
    const eliminateinfo = this.EliminationInfoForm.getRawValue();
    // this.eliminationcw.push(eliminateinfo);
    // this.health = this._dataStoreService.getData(this.constants.Health);
    // this.health.personChronic = this.eliminationcw;
    // this._dataStoreService.setData(this.constants.Health, this.health);
    this._healthService.saveHealth({ 'personElimination': [eliminateinfo] }, 1).subscribe(response => {
      this._alertService.success('Elimination Info Added Successfully');
      this.getEliminationInfoList();
    });
    this.resetForm();
  }

  initForm() {
    this.reportMode = 'add';
    this.EliminationInfoForm = this.formbuilder.group({
      // infoprovidedby: '',
      // clientlist: '',
      // collaterallist: '',
      infoprovidedname: ['', Validators.required],
     // relationship: '',
      eliminationinfounknown: false,
      currentstatus: '',
      toilettraining: '',
      otherSpecify: '',
      bowelmovement: '',
      urination: '',
      toiletcomments: '',
      considerunknown: false,
      specialcomments: ''
    });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '59', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '342', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          StatusList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          TrainingList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          )
        };
      })
      .share();
    this.StatusList$ = source.pluck('StatusList');
    this.TrainingList$ = source.pluck('TrainingList');
  }

  addEliminationnfo() {
    this.isAddEdit = true;
  }

  getEliminationInfoList() {
    this._commonHttpService.getPagedArrayList({
      page: 1,
      limit: 20,
      method: 'get',
      where: { personid: this.personId}
    }, 'personfamilyinfo/getpersonelimination?filter').subscribe(res => {
      this.eliminationcw = res ? res.data : [];
    });
  }

  getCurrentStatus(data) {
    if (data && Array.isArray(data)) {
      return data.join(',');
    }
    return '';
  }
}
