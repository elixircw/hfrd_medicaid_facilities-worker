import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { InvolvedPersonsConstants } from '../../../involved-persons/_entities/involvedPersons.constants';
import { BirthInfocw, Health } from '../../../involved-persons/_entities/involvedperson.data.model';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AlertService, DataStoreService, CommonHttpService, CommonDropdownsService, ValidationService } from '../../../../../@core/services';
import { PersonHealthService } from '../person-health.service';
import { HealthConstants } from '../health-constants';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { PersonInfoService } from '../../person-info.service';
import * as moment from 'moment';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'birth-information-cw',
  templateUrl: './birth-information-cw.component.html',
  styleUrls: ['./birth-information-cw.component.scss']
})
export class BirthInformationCwComponent implements OnInit {

  birthInfoForm: FormGroup;
  examTypeList: any[];
  specialityExamTypeList: any[];
  labTestList: any[];
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  birthInfocw: BirthInfocw[] = [];
  health: Health = {};
  isAgeUnder5: boolean = false;
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  pregnatMothersUseDropdownItems$: Observable<DropdownModel[]>;
  medicalConditionsDropdownItems$: Observable<DropdownModel[]>;
  diseasesDropdownItems$: Observable<DropdownModel[]>;
  prenatalCareDropdownItems$: Observable<DropdownModel[]>;
  gestationDropdownItems$: Observable<DropdownModel[]>;
  deliveryTypeDropdownItems$: Observable<DropdownModel[]>;
  complicationTypeDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  whenBegunItems$: Observable<DropdownModel[]>;
  parityItems$: Observable<DropdownModel[]>;

  address = { address1: null, address2: null, city: null, state: null, county: null, zipcode: null };
  under5yearsInfoForm: FormGroup;
  uploadedFiles = [];
  uploadNumber = '123434';

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService,
    private _personInfoService: PersonInfoService,
    private _commonDropdownService: CommonDropdownsService
  ) {

  }

  ngOnInit() {
    console.log(this.isAgeUnder5);

    this._personInfoService.personInfoListener$.subscribe(response => {
      this.isAgeUnder5 = (this.calculateAge(this._personInfoService.personInfo.personbasicdetails.dob) < 5) ? true : false;
      console.log(this.isAgeUnder5);
    });

    if (this._personInfoService.personInfo) {
      this.isAgeUnder5 = (this.calculateAge(this._personInfoService.personInfo.personbasicdetails.dob) < 5) ? true : false;
      console.log(this.isAgeUnder5);
    }

    this.reportMode = 'add';
    this.loadDropDown();
    this.birthInfoForm = this.formbulider.group({
      mothersusepregnant: '',
      mothersusepregnantspecify: '',
      mentalcondition: '',
      mentalconditionspecify: '',
      diseasescondition: '',
      diseasesconditionspecify: '',
      birthdefects: '',
      comments: '',
    });
    this.under5yearsInfoForm = this.formbulider.group({
      parentcare: '',
      gestation: '',
      deliverytype: '',
      comments: '',
      whenbegun: null,
      parity: null,
      prenatalproblem: null,
      prenatalproblemspecify: null,
      hospitalcomments: '',
      complications: '',
      childbornin: '',
      speciality: '',
      phone: '',
      email: ['', [ValidationService.mailFormat]]
    });
    const list = this._healthService.getHealthInfoWithKey(HealthConstants.LIST_KEY.PERSON_BIRTH);
    if (list && Array.isArray(list) && list.length) {
      this.birthInfocw = list;
    }
    this.loadBirthInfo();
  }

  loadBirthInfo() {
    this._healthService.getBirthInfo().subscribe(response => {
      console.log('birthInfo', response);
      if (response && Array.isArray(response) && response.length) {
        const data = response[0].json;
        if (data.personBirth && data.personBirth.length) {
          const personBirth = data.personBirth[data.personBirth.length - 1];
          this.birthInfoForm.patchValue(personBirth);
          this.reportMode = 'edit';
          this.editMode = true;
          this.uploadedFiles = Array.isArray(personBirth.uploadpath) ? personBirth.uploadpath : [];
        }
        if (data.underfive && data.underfive.length) {
          const underfive = data.underfive[data.personBirth.length - 1];
          this.under5yearsInfoForm.patchValue(underfive);
          this.address.address1 =  underfive.address1;
          this.address.address2 =  underfive.address2;
          this.address.city =  underfive.city;
          this.address.state =  underfive.state;
          this.address.county =  underfive.county;
          this.address.zipcode =  underfive.zip5no;
          // this.under5yearsInfoForm.controls['address'].patchValue(this.address);
          this.reportMode = 'edit';
          this.editMode = true;
        }

      }
    });
  }

  resetAddress() {
    this.address = { address1: null, address2: null, city: null, state: null, county: null, zipcode: null };
  }
  private resetForm() {

    this.birthInfoForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.birthInfoForm.enable();

  }


  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.birthInfoForm.disable();
  }

  edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.birthInfoForm.enable();
  }

  private delete(index) {
    this.birthInfocw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  add() {
    const uploadInfo = {};
    const birthInfo = this.birthInfoForm.getRawValue();
    birthInfo.uploadpath = this.uploadedFiles;
    if (birthInfo.uploadpath) {
      birthInfo.uploadpath.forEach(document => {
        if (document.percentage) {
          delete document.percentage;
        }
      });
    }
    const under5 = { ...this.under5yearsInfoForm.getRawValue(), ...this.address };
    under5.cityname = this.address.city;
    under5.statetypekey = this.address.state;
    under5.zip5no = this.address.zipcode;

    const data = { 'birth': birthInfo, 'underfive': under5 };
    this._healthService.saveHealth({ 'personBirth': data }).subscribe(_ => {
      this._alertSevice.success('Birth/Neonatal Info Saved Successfully');
      this.reportMode = 'edit';
      this.editMode = true;
      this.loadBirthInfo();
    });

  }

  private update() {
    if (this.modalInt !== -1) {
      this.birthInfocw[this.modalInt] = this.birthInfoForm.getRawValue();
      this.patchForm(this.birthInfocw[0]);
    }
    
    this.reportMode = 'edit';
    this.editMode = true;
    this._alertSevice.success('Birth/Neonatal Info Updated Successfully');
  }
  private patchForm(modal: BirthInfocw) {
    this.birthInfoForm.patchValue(modal);

    // if (modal.startdate) {
    //   this.birthInfoForm.patchValue({startdate: new Date(modal.startdate)});
    // }
    // if (modal.enddate) {
    //   this.birthInfoForm.patchValue({enddate: new Date(modal.enddate)});
    // }
  }

  private loadDropDown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '125' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '48' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '71' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          pregnatMothersUseList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          medicalConditionsList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          diseasesList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          )
        };
      })
      .share();
    this.pregnatMothersUseDropdownItems$ = source.pluck('pregnatMothersUseList');
    this.medicalConditionsDropdownItems$ = source.pluck('medicalConditionsList');
    this.diseasesDropdownItems$ = source.pluck('diseasesList');
    const under5Ddsource = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '151' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '90' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '337' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'delete_sw': 'N', 'picklist_type_id': '53' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'countyname asc'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
      ),
      this._commonDropdownService.getDropownsByTable('whenbegun'),
      this._commonDropdownService.getDropownsByTable('parity'),
    ])
      .map((result) => {
        return {
          prenatalCareList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          gestationList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          deliveryTypeList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          complicationTypeList: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.picklist_value_cd
              })
          ),
          states: result[4].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),
          counties: result[5].map(
            (res) =>
              new DropdownModel({
                text: res.countyname,
                value: res.countyname
              })
          ),
          whenbegun: result[6].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.description
              })
          ),
          parity: result[7].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.description
              })
          )
        };
      })
      .share();
    this.prenatalCareDropdownItems$ = under5Ddsource.pluck('prenatalCareList');
    this.gestationDropdownItems$ = under5Ddsource.pluck('gestationList');
    this.deliveryTypeDropdownItems$ = under5Ddsource.pluck('deliveryTypeList');
    this.complicationTypeDropdownItems$ = under5Ddsource.pluck('complicationTypeList');
    this.stateDropdownItems$ = under5Ddsource.pluck('states');
    this.countyDropDownItems$ = under5Ddsource.pluck('counties');
    this.whenBegunItems$ = under5Ddsource.pluck('whenbegun');
    this.parityItems$ = under5Ddsource.pluck('parity');

  }

  calculateAge(dob) {
    // const dob = this.selectedPerson.dob;
    let age = -1;
    if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
      const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
      age = moment().diff(rCDob, 'years');
      const days = moment().diff(rCDob, 'days');
    }
    return age;
  }

}
