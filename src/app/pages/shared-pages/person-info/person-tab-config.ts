

export const PersonTabs = [{
    active: false,
    path: 'profile',
    name: 'Profile',
    enable: true
},
{
    active: false, path: 'address',
    name: 'Address',
    enable: true
},
{
    active: false, path: 'contacts',
    name: 'Contact Info',
    enable: true
},
{
    active: false, path: 'health',
    name: 'Health',
    enable: true

},
{
    active: false, path: 'education',
    name: 'Education',
    enable: true

},
// {
//     active: false, path: 'employment',
//     name: 'Employment',

// },
{
    active: false, path: 'employment',
    name: 'Employment',
    enable: true

},
{
    active: false, path: '18-21',
    name: '18-21',
    enable: false

},
{
    active: false, path: 'military',
    name: 'Military',
    enable: true
},
{
    active: false, path: 'finance',
    name: 'Finance',
    enable: true

}
];


