import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { ActivatedRoute } from '@angular/router';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { Observable } from 'rxjs/Observable';
import { EmailType , PhoneType } from '../../../../admin/general/_entities/general.data.models';
import { PersonInfoService } from '../../person-info.service';
import { MilitaryService } from '../military.service';
import { GenericService, AlertService } from '../../../../../@core/services';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { isArray } from 'util';


@Component({
  selector: 'list-military',
  templateUrl: './list-military.component.html',
  styleUrls: ['./list-military.component.scss']
})
export class ListMilitaryComponent implements OnInit {

  personId: any;
  militaryPersonList = [];
  deleteMilitaryId: any;
  @Output() addflag: EventEmitter<Boolean> = new EventEmitter();
  
  constructor(private _commonHttpService: CommonHttpService,
    private _militaryService: MilitaryService,
    private _personService: PersonInfoService,
    private _alertService: AlertService) { }

    ngOnInit() {
      this.personId = (this._personService.getPersonId()) ? this._personService.getPersonId() : '';
      this._militaryService.getMilitaryList();
      this.listenMilitaryListChange();
    }


    listenMilitaryListChange() {
      this._militaryService.militaryPersonType$.subscribe( (data) => {
        if (data) {
         console.log(data);
         this.militaryPersonList = data['getmilitarydetails'];
         console.log(this.militaryPersonList);
        }
      });
    }



    editAddress(modal) {
      this._militaryService.isShowAddMilitaryEnabled(true);
      setTimeout( () => {
        this._militaryService.getAddress(modal);
      }, 500);
    }
  
    confirmDelete(personaddressid) {
      this.deleteMilitaryId = personaddressid;
      (<any>$('#delete-popup')).modal('show');
    }
  
    declineDelete() {
      this.deleteMilitaryId = null;
      (<any>$('#delete-popup')).modal('hide');
    }
  
    deleteAddress(i: number) {
      if (this.deleteMilitaryId) {
        this._commonHttpService.endpointUrl = CommonUrlConfig.EndPoint.PERSON.MILITARY.DeleteMilitary;
        this._commonHttpService.remove(this.deleteMilitaryId).subscribe(
          response => {
            console.log(response);
          //  this.addressPersonList.splice(i, 1);
           // this.addressPersonType$ = Observable.of(this.addressPersonType);
            (<any>$('#delete-popup')).modal('hide');
            this._alertService.success('Military Information deleted successfully..');
            this._militaryService.getMilitaryList();
            this.listenMilitaryListChange();
            this.addflag.emit(true);
            // this.personAddressForm.reset();
            // this.personAddressForm.enable();
          },
          error => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          }
        );
      } else {
       // this.addressPersonList.splice(i, 1);
        // this.addressPersonType$ = Observable.of(this.addressPersonType);
        (<any>$('#delete-popup')).modal('hide');
       // this._alertService.success('Address deleted successfully');
      //  this.getAddressListPage(1);
      }

}

  getContactPhone(phone) {
    if (phone && Array.isArray(phone)) {
      const cellNo = phone.find((data) => data.personphonetypekey == 'CL');
      if (cellNo) {
        return cellNo.phonenumber;
      } else {
        return phone[0].phonenumber;
      }
    }
    return '';
  }

}



