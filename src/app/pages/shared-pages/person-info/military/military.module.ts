import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MilitaryComponent } from './military.component';
import { AddMilitaryComponent } from './add-military/add-military.component';
import { MilitaryRoutingModule } from './military-routing.module';
import { ShareFeaturesModule } from '../share-features/share-features.module';
import { HttpClientModule } from '@angular/common/http';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ProvidersComponent } from '../../../providers/providers.component';
import { MilitaryService } from './military.service';
import { ListMilitaryComponent } from './list-military/list-military.component';

@NgModule({
  imports: [
    CommonModule,
    MilitaryRoutingModule,
    FormMaterialModule,
    HttpClientModule,
    ShareFeaturesModule
  ],
  declarations: [MilitaryComponent, AddMilitaryComponent, ListMilitaryComponent],
  providers: [MilitaryService]
})
export class MilitaryModule { }
