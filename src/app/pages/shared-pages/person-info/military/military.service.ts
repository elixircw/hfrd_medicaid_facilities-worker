import { Injectable } from '@angular/core';
import { NavigationUtils } from '../../../_utils/navigation-utils.service';
import { Observable } from 'rxjs/Observable';
import { AppConstants } from '../../../../@core/common/constants';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { PersonInfoService } from '../person-info.service';
import { Military,EmailType, PhoneType } from '../../../admin/general/_entities/general.data.models';



@Injectable()
export class MilitaryService {

  personId: any;
  public militaryPersonType$ = new Observable<Military[]>();
  public isShowAddMilitary$ = new Subject<any>();
  public selectedMilitary$ = new Subject<any>();

  constructor(private _navigationUtils: NavigationUtils,
    private _commonHttpService: CommonHttpService,
    private _personService: PersonInfoService) {
      this.personId = (this._personService.getPersonId()) ? this._personService.getPersonId() : '';
  }


  saveMilitaryInfo(data) {
    return this._commonHttpService.create(data, 'personmilitaryservices/addupdate');
  }

  getMilitaryList() {
    const source = this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest(
          {
            method: 'get',
            where: { personid: this.personId },
            page: 1, // this.paginationInfo.pageNumber,
            limit: 10// this.paginationInfo.pageSize,
          }),
        'personmilitaryservices/militarylist' + '?filter'
      ).map((result: any) => {
        return {
          data: result,
          count: result.length > 0 ? result[0].totalcount : 0
          // canDisplayPager: result.length > 0 ? result[0].totalcount > this.paginationInfo.pageSize : false
        };
      }).share();
    this.militaryPersonType$ = source.pluck('data');
  }

  isShowAddMilitaryEnabled(value) {
    this.isShowAddMilitary$.next(value);
  }

  getAddress(model) {
    this.selectedMilitary$.next(model);
  }
}
