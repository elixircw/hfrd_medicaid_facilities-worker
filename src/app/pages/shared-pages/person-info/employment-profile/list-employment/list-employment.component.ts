import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonInfoService } from '../../person-info.service';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { DataStoreService, SessionStorageService, CommonDropdownsService, AlertService } from '../../../../../@core/services';
import { Validators, Form, FormBuilder, FormGroup, FormControl } from '@angular/forms';
const moment = require('moment');
@Component({
  selector: 'list-employment',
  templateUrl: './list-employment.component.html',
  styleUrls: ['./list-employment.component.scss']
})
export class ListEmploymentComponent implements OnInit {
  personid= '';
  workInfo;
  selectedemployment= '';
  personNarrative: FormGroup;
  narrativeInfo;
  narrativeuniqueInfo;
  selectedProgram= '';
  selectednarrative= '';
  age = null;
  selectedProgramName= '';
  selectedProgramType= '1';
  enableEdit= false;
  enableEndDateEdit= false;
  addedNarrative= false;
  showNarrrative= false;
  constructor(
  private route: ActivatedRoute,
  private _alertSevice: AlertService,
  private _commonHttpService: CommonHttpService,
  private _personInfoService: PersonInfoService,
  private _formBuilder: FormBuilder
) {
  console.log('person details--------------');
  this.workInfo  = [];
  this.narrativeInfo  = [];
  this.enableEdit = false;
  if (this._personInfoService.personInfo && this._personInfoService.personInfo.personbasicdetails) {
    this.personid = this._personInfoService.personInfo.personbasicdetails.personid;
    this.loadWorkDetails(this._personInfoService.personInfo.personbasicdetails.personid);
    this.loadNarrativeDetails(this._personInfoService.personInfo.personbasicdetails.personid);

    if (this._personInfoService.personInfo.personbasicdetails.dob) {
        const dob = this._personInfoService.personInfo.personbasicdetails.dob;
        this.age = this._personInfoService.calculateAge(dob);
    }

    this._personInfoService.personDobListener$.subscribe(dob => {
      this.age = this._personInfoService.calculateAge(dob);
    });

  }
  this.profileUpdateListener();
  this.reloadPersonWork();
  this.initiateFormGroup();
}
  ngOnInit() {
  }
  private initiateFormGroup() {
    this.personNarrative = this._formBuilder.group({
      personemploymentid: '',
      promotedemploymentprogramname: '',
      promotedemploymentprogramstartdate: '',
      promotedemploymentprogramenddate: '',
      promotedemploymentnarrative: ''
    }, {validator: this.dateLessThan('promotedemploymentprogramstartdate', 'promotedemploymentprogramenddate')});
  }
  dateLessThan(start: string, end: string) {
    return (group: FormGroup): {[key: string]: any} => {
      const s = group.controls[start];
      const e = group.controls[end];
      if (s.value && e.value && s.value > e.value) {
        return {
          dates: 'End should be greater than start date'
        };
      }
      return {};
    };
}
  editNarrativedetails(workdetails) {
    this.enableEdit = true;
    this.enableEndDateEdit = true;

    if (!workdetails.promotedemploymentprogramenddate) {
      this.enableEndDateEdit = false;
    }
    this.personNarrative.patchValue(workdetails);
    this.personNarrative.get('promotedemploymentprogramname').disable();

  }
  deleteNarrative(workdetails) {
    this._personInfoService.deletePersonnarrativeDetails(workdetails).subscribe(response => {
      this._alertSevice.success('Narrative details deleted successfully!');
      this.loadNarrativeDetails(this.personid);
  },
    error => {
      console.log(error);
    }
  );
  }
  addNarrative() {
    this.personNarrative.get('promotedemploymentnarrative').setValidators([Validators.required]);
    this.showNarrrative = true;
  }
  isToday(datestring) {
    if (datestring) {
      const date1 = moment(datestring).format('MM/YYYY');
      const date2 = moment().format('MM/YYYY');
      if ( date1 === date2) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  cancelForm() {
    this.enableEdit = false; this.enableEndDateEdit = false;
    this.showNarrrative = false;
    this.personNarrative.get('promotedemploymentprogramname').enable();
    this.personNarrative.get('promotedemploymentnarrative').clearValidators();
    this.personNarrative.patchValue({
      personemploymentid: '',
      promotedemploymentprogramname: '',
      promotedemploymentprogramstartdate: '',
      promotedemploymentprogramenddate: '',
      promotedemploymentnarrative: ''
    });


  }
  saveProgram() {
    const data = this.personNarrative.getRawValue();
    data.promotedemploymentprogramstartdate = data.promotedemploymentprogramstartdate || null;
    data.promotedemploymentprogramenddate = data.promotedemploymentprogramenddate || null;
    this._personInfoService.savePersonnarrativeDetails(data).subscribe(response => {
      if (response.error === 1) {
        this._alertSevice.error(response.message);
      } else {
        this._alertSevice.success('Narrative details added successfully!');
        this.enableEdit = false; this.enableEndDateEdit = false;
        this.showNarrrative = false;
        this.personNarrative.get('promotedemploymentprogramname').enable();
        this.personNarrative.get('promotedemploymentnarrative').clearValidators();
        this.personNarrative.patchValue({
          personemploymentid: '',
          promotedemploymentprogramname: '',
          promotedemploymentprogramstartdate: '',
          promotedemploymentprogramenddate: '',
          promotedemploymentnarrative: ''
        });

        this.loadNarrativeDetails(this.personid);
      }
    },
      error => {
      }
    );
  }
  confirmDelete(workdetails) {
    this._personInfoService.deletePersonworkDetails(workdetails).subscribe(response => {
      this._alertSevice.success('Employment details deleted successfully!');
      this._personInfoService.reloadworkDetails(1);
      this.selectedemployment = '';
  },
    error => {
      console.log(error);
    }
  );
  }
  onChangeProgram(programdetails) {
    this.selectedProgram = programdetails.personemploymentid;
    this.selectedProgramName = programdetails.promotedemploymentprogramname;
  }
  editEmployment(workdetails) {
    this._personInfoService.setWorkInfo(workdetails);
    this._personInfoService.empActionText = 'UPDATE';
  }
  reloadPersonWork() {
    const _self = this;
    this._personInfoService.personInfoWorkListener$.subscribe(personInfo => {
      if (personInfo === 1) {
        if (_self._personInfoService.personInfo && _self._personInfoService.personInfo.personbasicdetails) {
          _self.loadWorkDetails(_self._personInfoService.personInfo.personbasicdetails.personid);
        }
      }
    });
  }
  profileUpdateListener() {
    const _self = this;
    this._personInfoService.personInfoListener$.subscribe(personInfo => {
      console.log('person details---00000000000000000000000000000-----------');
      console.log(personInfo);
      if (!_self.personid || _self.personid === '') {
        _self.personid = personInfo.personbasicdetails.personid;
        _self.loadWorkDetails(personInfo.personbasicdetails.personid);
        _self.loadNarrativeDetails(personInfo.personbasicdetails.personid);
        const dob = personInfo.personbasicdetails.dob;
        _self.age = this._personInfoService.calculateAge(dob);

      }
    });
  }
  loadWorkDetails(personid) {
    // this.workInfo = [];
    this.getworkDetails(personid).subscribe(workdetails => {
      this.workInfo = workdetails;
    });
  }
  loadNarrativeDetails(personid) {
    this.getNarrativeDetails(personid).subscribe(workdetails => {

      this.narrativeInfo = workdetails;
      const narrativeuniqueInfo = [];
      this.narrativeuniqueInfo = [];
      for ( let i = 0; i < this.narrativeInfo.length; i++) {
         const promotedemploymentprogramname = this.narrativeInfo[i].promotedemploymentprogramname;
          if (promotedemploymentprogramname) {
            narrativeuniqueInfo[promotedemploymentprogramname] = {};
            narrativeuniqueInfo[promotedemploymentprogramname] = this.narrativeInfo[i];
          }
        }

      for (const k in narrativeuniqueInfo) {
        this.narrativeuniqueInfo.push(narrativeuniqueInfo[k]);
}
    });
  }
  getNarrativeDetails(personid: string) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personid }
        }),
        'People/getpersonworknarrative?filter'
      );
  }
  getworkDetails(personid: string) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personid }
        }),
        'People/getpersonwork?filter'
      );
  }
}

