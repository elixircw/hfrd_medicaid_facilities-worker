import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormMaterialModule } from '../../../../@core/form-material.module';

import { EmploymentProfileRoutingModule } from './employment-profile-routing.module';
import { EmploymentProfileComponent } from './employment-profile.component';
import { AddEmploymentComponent } from './add-employment/add-employment.component';
import { ListEmploymentComponent } from './list-employment/list-employment.component';
import { EmploymentProfileService } from './employment-profile.service';
import { ShareFeaturesModule } from '../share-features/share-features.module';

@NgModule({
  imports: [
    CommonModule,
    EmploymentProfileRoutingModule,
    FormMaterialModule,
    HttpClientModule,
    ShareFeaturesModule
  ],
  declarations: [EmploymentProfileComponent, AddEmploymentComponent, ListEmploymentComponent],
  providers: [EmploymentProfileService]
})
export class EmploymentProfileModule { }
