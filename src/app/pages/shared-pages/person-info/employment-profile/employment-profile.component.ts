import { Component, OnInit } from '@angular/core';
import { EmploymentProfileService } from './employment-profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonInfoService } from '../person-info.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { PaginationRequest } from '../../../../@core/entities/common.entities';

@Component({
  selector: 'employment-profile',
  templateUrl: './employment-profile.component.html',
  styleUrls: ['./employment-profile.component.scss']
})
export class EmploymentProfileComponent implements OnInit {
  personid= '';
  isShowAddEmployment: boolean = false;
  // workInfo: any;
  constructor(
    private _employmentService: EmploymentProfileService,
    private route: ActivatedRoute,
    private _commonHttpService: CommonHttpService,
    private _personInfoService: PersonInfoService
  ) {
    // if(this._personInfoService.personInfo && this._personInfoService.personInfo.personbasicdetails) {
    //   this.personid = this._personInfoService.personInfo.personbasicdetails.personid;
    //   this.loadWorkDetails(this._personInfoService.personInfo.personbasicdetails.personid);
    // }
    this.workUpdateListener();
  }
  workUpdateListener() {
    const _self = this;
    this._personInfoService.workInfoListener$.subscribe(workInfo => {
      if (workInfo && workInfo.personid) {
        _self.isShowAddEmployment = true;
      }
    });
  }
  // loadWorkDetails(personid) {
  //   this.getworkDetails(personid).subscribe(workdetails => {
  //     this.workInfo =workdetails;
  //     if(this.workInfo && this.workInfo.personid) {
  //       this.isShowAddEmployment = true;
  //     }
  //   });
  // }
  // getworkDetails(personid: string) {
  //   return this._commonHttpService
  //     .getPagedArrayList(
  //       new PaginationRequest({
  //         method: 'get',
  //         where: { personid: personid }
  //       }),
  //       'People/getpersonwork?filter'
  //     );
  // }
  ngOnInit() {
    this.listenToEnableAddEmployment();
  }

  enableAddEmployment() {
    // this.workInfo = {};
    this._personInfoService.setWorkInfo({});
    this.isShowAddEmployment = true;
    this._personInfoService.empActionText = 'ADD';
    // return this.isShowAddEmployment = !this.isShowAddEmployment;
  }

  listenToEnableAddEmployment() {
    this._employmentService.isShowAddEmployment$.subscribe((data) => {
      this.isShowAddEmployment = (data) ? true : false;
    });
  }

}
