import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class EmploymentProfileService {
  public isShowAddEmployment$ = new Subject<any>();
  constructor() { }

  isShowAddEmploymentEnabled(value) {
    this.isShowAddEmployment$.next(value);
  }  
}
