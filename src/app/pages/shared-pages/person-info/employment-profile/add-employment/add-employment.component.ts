import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { NewUrlConfig } from '../../../../newintake/newintake-url.config';
import { Validators, Form, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DataStoreService, SessionStorageService, CommonDropdownsService, AlertService } from '../../../../../@core/services';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ControlUtils } from '../../../../../@core/common/control-utils';
import { EmploymentProfileService } from '../employment-profile.service';
import { PersonInfoService } from '../../person-info.service';
import { InvolvedPersonsConstants } from '../../../../case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
import { Employer, Work } from '../../../../case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';


@Component({
  selector: 'add-employment',
  templateUrl: './add-employment.component.html',
  styleUrls: ['./add-employment.component.scss']
})

export class AddEmploymentComponent implements OnInit {
  suggestedAddress$: Observable<any[]>;
  personEmployeeFormGroup: FormGroup;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  addresstypeDropdownItems$: Observable<DropdownModel[]>;
  prefixDropdownItems$: Observable<DropdownModel[]>;
  suffixDropdownItems$: Observable<DropdownModel[]>;
  payperiodDropDownItem$: Observable<DropdownModel[]>;
  personId: any;
  reportMode: string;
  personAddressId: any;
  modalInt: number;
  editMode: boolean;
  employer: any;
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Work;
  work: Work = {};
  phoneNumberList = [];
  emailList = [];
  employmentStartDate: any;
  employeeButton = 'ADD';

  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _dropdownService: CommonDropdownsService,
    private _alertSevice: AlertService,
    private _employmentService: EmploymentProfileService,
    private _personService: PersonInfoService,
    private _dataStoreService: DataStoreService,
    private _personInfoService: PersonInfoService,
    private _commonDropdownService: CommonDropdownsService,

    ) {
    }

  ngOnInit() {
    const personInfo = this._personInfoService.getPersonInfo();
    if (personInfo && personInfo.personbasicdetails) {
      this.employmentStartDate = personInfo.personbasicdetails.dob ? personInfo.personbasicdetails.dob : null;
      this.employeeButton = this._personInfoService.empActionText;
    }
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.initiateFormGroup();
    this.loadDropDown();
    this.phoneNumberList = this.getPhoneNumberList();
    this.emailList = this.getEmailList();
    this.personId = this._personService.getPersonId();
    if (this._personInfoService.workInfo && this._personInfoService.workInfo.personid) {
      this.updateFormView(this._personInfoService.workInfo);
    }
    this._personInfoService.workInfoListener$.subscribe( workInfo => {
        if (workInfo) {
          if (workInfo && workInfo.personid) {
            this.updateFormView(workInfo);
          }
        }
    });
    // this.loadCounty();
  }
  updateFormView(workInfo) {
    console.log(workInfo);
    this.emailList = this.getEmailList();
    this.phoneNumberList = this.getPhoneNumberList();

    // for(let i=0;i<this.emailList.length;i++) {
    //   for(let j=0;j<workInfo.email.length;j++) {
    //     if(this.emailList[i].type == workInfo.email[j].type) {
    //       this.emailList[i].email = workInfo.email[j].email;
    //       break;
    //     }
    //   }
    // }
    if (workInfo.email && workInfo.email.length > 0) {
      this.emailList =  workInfo.email;
    }
    if (workInfo.workphone && workInfo.workphone.length > 0) {
      this.phoneNumberList =  workInfo.workphone;
    }
    this.personEmployeeFormGroup.patchValue(workInfo);
  }
  private initiateFormGroup() {
    this.personEmployeeFormGroup = this._formBuilder.group({
      employername: '',
     // currentemployer: false,
      personworkcarrergoalid: '',
      personemploymentid: '',
      personemployerdetailid: '',
      noofhours: new FormControl('', [Validators.pattern('^[0-9]*$')]),
      duties: new FormControl('', Validators.pattern('[a-zA-Z ]*')),
      startdate: [null],
      enddate: [null],
      reasonforleaving: '',
      addresstype: '',
      address1: '',
      address2: '',
      cityname: '',
      statetypekey: '',
      zip5no: new FormControl('', [Validators.pattern('^[0-9]*$')]),
      countytypekey: '',
      EmailID: '',
      EmailType: '',
      fax: '',
      extension: '',
      contactnumber: '',
      contacttype: '',
      clienttitle: new FormControl('', Validators.pattern('[a-zA-Z ]*')),
      emplymenttypekey: '',
      workschedule: '',
      income: new FormControl('', [Validators.pattern('^[0-9]*$')]) ,
      wagefreqtypekey: '',
      supervisorfirstname: '',
      supervisormiddlename: '',
      supervisorlastname: '',
      supervisorsuffixtypekey: '',
      supervisorprefixtypekey: '',
      careergoals: ''
    });
    if (this._personInfoService.getClosed()) {
      this.personEmployeeFormGroup.disable();
    }
  }

  editAddress(model) {
    this.loadCounty(model.state);
    setTimeout(() => {
      this.personEmployeeFormGroup.patchValue(model);
      this.personEmployeeFormGroup.patchValue({
        addresstype: model.personaddresstypekey,
        address2: model.address2,
        knownDangerAddress: [model.danger ? 'yes' : 'no'],
        knownDangerAddressReason: model.dangerreason
      });
    }, 100);
    this.personAddressId = model.personaddressid;
    this.reportMode = 'edit';
  }
  addPersonEmployment() {
    const data = this.personEmployeeFormGroup.getRawValue();
    data.phoneNumberList = this.phoneNumberList;
    data.emailList = this.emailList;
    this._personInfoService.savePersonworkDetails(data).subscribe(response => {
      this._alertSevice.success('Employment details added successfully!');
      this._employmentService.isShowAddEmploymentEnabled(false);
      this._personInfoService.reloadworkDetails(1);
    },
      error => {
        console.log(error);
      }
    );
    this.employeeButton = 'ADD';
  }
  // loadCounty(statekey?) {
  //   const state = statekey ? statekey : this.personEmployeeFormGroup.get('state').value;
  //   console.log('state...', state);
  //   const source = this._commonHttpService.getArrayList(
  //     {
  //       where: { state: state },
  //       order: 'countyname',
  //       method: 'get',
  //       nolimit: true
  //     },
  //     CommonUrlConfig.EndPoint.Listing.CountyListUrl + '?filter'
  //   ).map((result) => {
  //     return {
  //       counties: result.map(
  //         (res) =>
  //           new DropdownModel({
  //             text: res.countyname,
  //             value: res.countyname
  //           })
  //       )
  //     };
  //   }).share();

  //   this.countyDropDownItems$ = source.pluck('counties');
  // }

  getSuggestedAddress() {
    if (this.personEmployeeFormGroup.value.address1 &&
        this.personEmployeeFormGroup.value.address1.length >= 3 ) {
         this.suggestAddress();
     }
 }
 suggestAddress() {
     this._commonHttpService
         .getArrayListWithNullCheck(
             {
                 method: 'post',
                 where: {
                     prefix: this.personEmployeeFormGroup.value.address1,
                     cityFilter: '',
                     stateFilter: '',
                     geolocate: '',
                     geolocate_precision: '',
                     prefer_ratio: 0.66,
                     suggestions: 25,
                     prefer: 'MD'
                 }
             },
             NewUrlConfig.EndPoint.Intake.SuggestAddressUrl
             ).subscribe(
              (result: any) => {
                if (result.length > 0) {
                this.suggestedAddress$ = result;
              }
              }
            );
         }
     selectedAddress(model) {
      this.personEmployeeFormGroup.patchValue({
        address1: model.streetLine ? model.streetLine : '',
        cityname: model.city ? model.city : '',
        statetypekey: model.state ? model.state : ''
     });
      const addressInput = {
        street: model.streetLine ? model.streetLine : '',
        street2: '',
        city: model.city ? model.city : '',
        state: model.state ? model.state : '',
        zipcode: '',
        match: 'invalid'
      };
      this._commonHttpService
        .getSingle(
            {
                method: 'post',
                where: addressInput
            },
            NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
        )
        .subscribe(
            (result) => {
                if (result[0].analysis) {
                    this.personEmployeeFormGroup.patchValue({
                        zip5no: result[0].components.zipcode ? result[0].components.zipcode : ''
                    });
                    if (result[0].metadata.countyName) {
                      this._commonHttpService.getArrayList(
                        {
                          nolimit: true,
                          where: { referencetypeid: 306,  mdmcode: this.personEmployeeFormGroup.value.statetypekey, description: result[0].metadata.countyName}, method: 'get'
                        },
                        'referencevalues?filter'
                    ) .subscribe(
                      (resultresp) => {
                        this.personEmployeeFormGroup.patchValue({
                          countytypekey: resultresp[0].ref_key
                      });
                      }
                    );
                  }
                 }
                } ,
                (error) => {
                    console.log(error);
                }
        );

 }
 private loadDropDown() {
  // const source = forkJoin([
  //   this._commonHttpService.getArrayList(
  //     {
  //       method: 'get',
  //       nolimit: true
  //     },
  //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
  //       .StateListUrl + '?filter'
  //   ),
  //   this._commonHttpService.getArrayList(
  //     {
  //       method: 'get',
  //       nolimit: true,
  //       order: 'countyname asc'
  //     },
  //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
  //   ),
  //   this._commonHttpService.getArrayList(
  //     {
  //         method: 'get',
  //         nolimit: true,
  //         activeflag: 1,
  //         order: 'typedescription'
  //     },
  //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
  //         .PersonAddressUrl + '?filter'
  // ),
  // ])
  //   .map(result => {
  //     return {
  //       states: result[0].map(
  //         res =>
  //           new DropdownModel({
  //             text: res.statename,
  //             value: res.stateabbr
  //           })
  //       ),
  //       counties: result[1].map(
  //         (res) =>
  //           new DropdownModel({
  //             text: res.countyname,
  //             value: res.countyname
  //           })
  //       ),
  //       addresstype: result[2].map(
  //         res =>
  //             new DropdownModel({
  //                 text: res.typedescription,
  //                 value: res.personaddresstypekey
  //             })
  //     )
  //     };
  //   })
  //   .share();
  // this.stateDropdownItems$ = source.pluck('states');
  // this.countyDropDownItems$ = source.pluck('counties');
  // this.addresstypeDropdownItems$ = source.pluck('addresstype');
  this.stateDropdownItems$ = this._commonDropdownService.getPickListByName('state');
  this.countyDropDownItems$ = this._commonDropdownService.getPickListByName('county');
  this.payperiodDropDownItem$ = this._commonDropdownService.getPickListByName('wagefrequency');
  this.prefixDropdownItems$ = this._commonDropdownService.getPickListByName('prefix');
  this.suffixDropdownItems$ = this._commonDropdownService.getPickListByName('suffix');
  this.addresstypeDropdownItems$ =  this._commonDropdownService.getAddressType();

}
loadCounty(state) {
  this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
    this.countyDropDownItems$ = Observable.of(countyList);
  });
}
private add() {
  this.employer.push(this.personEmployeeFormGroup.getRawValue());
  this.updateData();
  this._alertSevice.success('Added Successfully');
  this.resetForm();
}

private resetForm() {

  this.modalInt = -1;
  this.editMode = false;
  this.reportMode = 'add';
  this.personEmployeeFormGroup.reset();
  this.personEmployeeFormGroup.enable();
  this.personEmployeeFormGroup.get('enddate').enable();
  this.personEmployeeFormGroup.get('enddate').setValidators([Validators.required]);
  this.personEmployeeFormGroup.get('enddate').updateValueAndValidity();
  // this.personEmployeeFormGroup.get('reasonforleaving').patchValue({});
  this.personEmployeeFormGroup.get('reasonforleaving').enable();
  this.personEmployeeFormGroup.get('reasonforleaving').setValidators([Validators.required]);
  this.personEmployeeFormGroup.get('reasonforleaving').updateValueAndValidity();
}

private update() {
  if (this.modalInt !== -1) {
    this.employer[this.modalInt] = this.personEmployeeFormGroup.getRawValue();
  }
  this.updateData();
  this.resetForm();
  this._alertSevice.success('Updated Successfully');
}

private updateData() {
  this.work = this._dataStoreService.getData(this.constants.Work);
  this.work.employer = this.employer;
  // this._dataStoreService.setData(this.constants.Work, this.work);
  this._dataStoreService.setData(this.constants.Work, this.work);
}

//   addPersonAddress(addAddress) {
//     const personAddressForm = this.personAddressFormGroup.getRawValue();
//     if (personAddressForm) {
//       // addAddress = this.personAddressForm.getRawValue();
//       addAddress.danger = personAddressForm.knownDangerAddress;
//       addAddress.personaddresstypekey = personAddressForm.addresstype;
//       addAddress.personid = this.personId;
//       addAddress.dangerreason = personAddressForm.knownDangerAddressReason;
//       addAddress.address = personAddressForm.address;
//       addAddress.address2 = personAddressForm.address2;
//       addAddress.zipcode = personAddressForm.zipcode;
//       addAddress.state = personAddressForm.state;
//       addAddress.city = personAddressForm.city;
//       addAddress.county = personAddressForm.county;
//       addAddress.personaddressid = this.personAddressId;
//       if (this.personAddressFormGroup.value.effectivedate) {
//         addAddress.effectivedate = personAddressForm.effectivedate;
//       } else {
//         addAddress.effectivedate = null;
//       }
//       if (this.personAddressFormGroup.value.expirationdate) {
//         addAddress.expirationdate = personAddressForm.expirationdate;
//       } else {
//         addAddress.expirationdate = null;
//       }

//       this._commonHttpService.create(addAddress, CommonUrlConfig.EndPoint.PERSON.ADDRESS.AddUpdateUrl).subscribe(
//         result => {
//           this._alertSevice.success('Address details Added/Updated successfully!');
//           this.reportMode = 'add';
//         },
//         error => {
//           this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
//         }
//       );
//       this.personAddressFormGroup.reset();
//       this.personAddressFormGroup.enable();
//     } else {
//       this._alertSevice.warn('Please fill mandatory fields for Phone');
//       ControlUtils.validateAllFormFields(this.personAddressFormGroup);
//       // ControlUtils.setFocusOnInvalidFields();
//     }
//   }

  cancelForm() {
    this._employmentService.isShowAddEmploymentEnabled(false);
  }

  private patchForm(modal: Employer) {
    this.personEmployeeFormGroup.patchValue(modal);
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.personEmployeeFormGroup.enable();
    this.isCurrentEmployerSaved(modal.currentemployer);
  }

  isCurrentEmployerSaved(control) {
    if (control) {

      this.personEmployeeFormGroup.get('enddate').disable();
      this.personEmployeeFormGroup.get('enddate').clearValidators();
      this.personEmployeeFormGroup.get('enddate').updateValueAndValidity();
      // this.personEmployeeFormGroup.get('reasonforleaving').patchValue({});
      this.personEmployeeFormGroup.get('reasonforleaving').disable();
      this.personEmployeeFormGroup.get('reasonforleaving').clearValidators();
      this.personEmployeeFormGroup.get('reasonforleaving').updateValueAndValidity();
    } else {

      this.personEmployeeFormGroup.get('enddate').enable();
      this.personEmployeeFormGroup.get('enddate').setValidators([Validators.required]);
      this.personEmployeeFormGroup.get('enddate').updateValueAndValidity();
      // this.personEmployeeFormGroup.get('reasonforleaving').patchValue({});
      this.personEmployeeFormGroup.get('reasonforleaving').enable();
      this.personEmployeeFormGroup.get('reasonforleaving').setValidators([Validators.required]);
      this.personEmployeeFormGroup.get('reasonforleaving').updateValueAndValidity();
    }
  }

  private delete(index) {
    this.employer.splice(index, 1);
    this.updateData();
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  getPhoneNumberList() {
    // check for phone number api a list once both associated with address.
    // assign phoneNumberList
      // set the phone list here on edit
    return [{
      contactType: '',
      phoneNumber: null
    }];
  }

  getEmailList() {
     // check for  email api list once both associated with address.
    // assign phoneNumberList, emailList
    // set the email List  on edit
    return [{
      type: 'Primary',
      email: null
    },
    {
      type: 'Secondary',
      email: null
    }];
  }

  setMaxDate(type: string) {
    if (type === 'startdate') {
      const formenddate = this.personEmployeeFormGroup.getRawValue().enddate;
      const enddate = (formenddate) ? new Date(formenddate) : new Date();
      const currdate = new Date();
      const res = (enddate < currdate) ? enddate : currdate;
      return res;
    }
    if (type === 'enddate') {
      return new Date();
    }
  }
}
