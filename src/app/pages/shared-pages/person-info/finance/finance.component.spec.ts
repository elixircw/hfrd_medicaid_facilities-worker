import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonFinanceComponent } from './finance.component';

describe('PersonFinanceComponent', () => {
  let component: PersonFinanceComponent;
  let fixture: ComponentFixture<PersonFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
