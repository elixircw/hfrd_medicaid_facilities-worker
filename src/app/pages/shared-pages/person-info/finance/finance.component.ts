import { Component, OnInit } from '@angular/core';
import { PersonInfoService } from '../person-info.service';

@Component({
  selector: 'finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.scss']
})
export class PersonFinanceComponent implements OnInit {


  isShowAddMilitary: boolean = true;


  constructor(    private _personInfoService: PersonInfoService) { }

  ngOnInit() {
    
  }


  enableAddMilitary() {
    this.isShowAddMilitary = !this.isShowAddMilitary;
  }


}


