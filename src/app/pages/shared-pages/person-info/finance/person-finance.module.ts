import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormMaterialModule } from '../../../../@core/form-material.module';

import { PersonFinanceRoutingModule } from './finance-routing.module';
import { PersonFinanceComponent } from './finance.component';
import { AddFinanceComponent } from './add-finance/add-finance.component';

@NgModule({
  imports: [
    CommonModule,
    PersonFinanceRoutingModule,
    FormMaterialModule
  ],
  declarations: [PersonFinanceComponent, AddFinanceComponent]
})
export class PersonFinanceModule { }
