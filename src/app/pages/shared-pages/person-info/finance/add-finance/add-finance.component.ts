import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Validators, Form, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PersonInfoService } from '../../person-info.service';
import { DataStoreService, SessionStorageService, CommonDropdownsService, AlertService, AuthService } from '../../../../../@core/services';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { InvolvedPersonsService } from '../../../involved-persons/involved-persons.service';
import { NewUrlConfig } from '../../../../newintake/newintake-url.config';
import { AppUser } from '../../../../../@core/entities/authDataModel';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'add-finance',
  templateUrl: './add-finance.component.html',
  styleUrls: ['./add-finance.component.scss']
})
export class AddFinanceComponent implements OnInit {
  personid= '';
  assetInfo;
  incomeInfo;
  supportInfo;
  csesInfo= {supportorder: {}};
  showcsesDetails = false;
  secondarySchool= [];
  vocational= [];
  activity= [];
  employer= [];
  personDisabilities= [];
  selectedmainTab= 1;
  suggestedAddress$: Observable<any[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  typeofAssetDropDownItem$: Observable<DropdownModel[]>;
  AssetVerificationDropDownItem$: Observable<DropdownModel[]>;
  AssestDisregardDropDownItem$: Observable<DropdownModel[]>;
  IncomeVerificationDropDownItem$: Observable<DropdownModel[]>;
  IncomeFrequencyDropDownItem$: Observable<DropdownModel[]>;
  IncomeSourceDropDownItem$: Observable<DropdownModel[]>;
  SupportOrderStatusDownItem$: Observable<DropdownModel[]>;
  SupportPaymentFrequencyDropDownItem$: Observable<DropdownModel[]>;
  SupportOrderStateDropDownItem$: Observable<DropdownModel[]>;
  incomeDetailsFormGroup: FormGroup;
  deemedparentFormGroup: FormGroup;
  childcareexpensesFormGroup: FormGroup;
  supportorderFormGroup: FormGroup;
  assetFormGroup: FormGroup;
  selectedTab = '';
  caseId= '';
  schedule_h_col_iii_no= '';
  income_result;
  asset_result;
  support_order_result;
  cses_result;
  addIncomeDetails= false;
  addAssetDetails= false;
  addSupportDetails= false;
  involvedPersonList;
  involvedPerson;
  caseNumber
  dob: any;
  userProfile: AppUser;
  currentDate;
  constructor(
    private _formBuilder: FormBuilder,
    private _personInfoService: PersonInfoService,
    private _alertSevice: AlertService,
    private _commonDropdownService: CommonDropdownsService,
    private _commonHttpService: CommonHttpService,
    private _service: InvolvedPersonsService,
    private _authService: AuthService
  ) {
    console.log('person details--------------');
    this.userProfile = this._authService.getCurrentUser();
    this.caseId = this._personInfoService.getCaseId();
    this.caseNumber= this._personInfoService.getCaseNumber();
    this.currentDate = new Date();
    if (this._personInfoService.personInfo && this._personInfoService.personInfo.personbasicdetails) {
      this.personid = this._personInfoService.personInfo.personbasicdetails.personid;
      this.dob = this._personInfoService.personInfo.personbasicdetails.dob;
      this.loadIncomeDetails(this._personInfoService.personInfo.personbasicdetails.personid);
      this.loadAssetDetails(this._personInfoService.personInfo.personbasicdetails.personid);
      this.loadSupportOrderDetails(this._personInfoService.personInfo.personbasicdetails.personid);
      this.loadInvolvedPersons();
    }
    this.initiateFormGroup();
    const _self = this;
    this._personInfoService.personInfoListener$.subscribe(personInfo => {
      if (!_self.personid || _self.personid === '') {
      this.personid = personInfo.personbasicdetails.personid;
      this.loadIncomeDetails(personInfo.personbasicdetails.personid);
      this.loadAssetDetails(personInfo.personbasicdetails.personid);
      this.loadSupportOrderDetails(personInfo.personbasicdetails.personid);
      this.loadInvolvedPersons();
      }
    });
  }
  changePerson() {
    this.loadCsesDetails(this.involvedPerson);
  }
  loadInvolvedPersons() {
    this.getInvolvedPersons().subscribe(response  => {
      if (response && response.data) {

        this.involvedPersonList = response.data;

      }
    });
  }
  getInvolvedPersons() {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          'where': {'intakeserviceid': this.caseId}
        }),
        'People/getpersondetail?filter'
      );
  }
   // {"count":-1,"page":1,"limit":20,"method":"get","where":{"intakeserviceid":"8f6de8a5-1d28-4002-b63f-f3d577c5d903"}}
  ngOnInit() {
    this.loadDropDown();
    this.getIncomeSource();
  }
  private initiateFormGroup() {
    this.incomeDetailsFormGroup = this._formBuilder.group({
      datasourcetypekey: 'CJAMS',
      startdate: null,
      enddate: null,
      incomesourcetypekey: null,
      incomefrequencytypekey:  null,
      amount:  null,
      verificationtypekey:  null,
      notes:  null,
      monthlyamount:  null,
      incomeid: null

    });
    this.deemedparentFormGroup = this._formBuilder.group({
      'caseid': this.caseId ,
      'assistance_unit_no': null,
      'notin_assistance_unit_no': null,
      'schedule_h_col_iii_no': null,
      'monthly_gross_earnings_no': null,
      'unearned_income_no': null,
      'court_ordered_support_no': null,
      'earning_disregard_no': null,
      'total_deemed_income_no': null,
      'verifiedparentpersonid': null,
      'deemed_income_stepparent_id': null
    });
    this.childcareexpensesFormGroup = this._formBuilder.group({
      'employmenttypecode': null,
      'amountearned': null,
      'childrenunder2': null,
      'childrenover2': null,
      'child_care_expense_id': null
    });
    this.assetFormGroup = this._formBuilder.group({
      'personassetid': null,
      'purchasedate': null,
      'disposaldate': null,
      'assettypekey': null,
      'disregardflag': null,
      'accountno': null,
      'beneficiaryname': null,
      'verificationtypekey': null,
      'notes': null,
      'marketvaluetypekey': null,
      'facevalue': null,
      'amountowed': null,
      'locationname': null,
      'cityname': null,
      'statetypekey': null,
      'zip5no': null,
      'countytypekey': null,
      'addressline1': null,
      'addressline2': null
    });
    this.supportorderFormGroup = this._formBuilder.group({
      'socounty': null,
      'socityname': null,
      'sostate': null,
      'sonumber': null,
      'sodate': null,
      'sostatusdate': null,
      'sostatustypekey': null,
      'sopaymentamount': null,
      'sopaymentfreqtypekey': null,
      'sodatasource': 'CJAMS'
    });
    if (this._personInfoService.getClosed()) {
      this.incomeDetailsFormGroup.disable();
      this.deemedparentFormGroup.disable();
      this.childcareexpensesFormGroup.disable();
      this.assetFormGroup.disable();
      this.supportorderFormGroup.disable();
    }
    this.incomeDetailsFormGroup.controls['incomefrequencytypekey'].valueChanges.subscribe(value => {
      this.prefillIncome(parseFloat(this.incomeDetailsFormGroup.value.amount), value);
    });
    this.incomeDetailsFormGroup.controls['amount'].valueChanges.subscribe(value => {
      this.prefillIncome(parseFloat(value), this.incomeDetailsFormGroup.value.incomefrequencytypekey);
    });
  }
  roundToTwo(num) {
    return Math.round(num * 100) / 100;
  }
  prefillIncome(amount, incometype) {
    const calculationArray = {
      'ANN': function(a) {return a / 12; },
      'DAI': function(a) {return a * 30.5; },
      'ETW': function(a) {return a * 2.167; },
      'MON': function(a) {return a * 1; },
      'QRTY': function(a) {return a / 4; },
      'TM': function(a) {return a * 2; },
      'TY': function(a) {return a / 6; },
      'WKLY': function(a) {return a * 4.33; }
    };
    if (incometype && amount && amount > 0) {
        const value = calculationArray[incometype](amount);
        this.incomeDetailsFormGroup.patchValue({
          monthlyamount: this.roundToTwo(value)
        });
    }
  }
  // incomeListener() {
  //   const _self = this;
  //   this._personInfoService.incomeInfoListener$.subscribe(incomeInfo => {
  //     alert('in edit');
  //     if (incomeInfo && incomeInfo.personid) {
  //         this.incomeDetailsFormGroup.patchValue(incomeInfo);
  //         this.deemedparentFormGroup.patchValue(incomeInfo.deemedparent);
  //         this.childcareexpensesFormGroup.patchValue(incomeInfo.childcareexpenses);
  //     }
  //   });
  // }

  private loadDropDown() {
    this.stateDropdownItems$ = this._commonDropdownService.getPickListByName('state');
    this.countyDropDownItems$ = this._commonDropdownService.getPickListByName('county');
    this.typeofAssetDropDownItem$ = this._commonDropdownService.getPickListByName('assettype');
    this.AssetVerificationDropDownItem$ = this._commonDropdownService.getPickListByName('assetverification');
    this.AssestDisregardDropDownItem$ = this._commonDropdownService.getPickListByName('yesno');
    this. IncomeVerificationDropDownItem$ = this._commonDropdownService.getPickListByName('earnedincomeverification');
    this. IncomeFrequencyDropDownItem$ = this._commonDropdownService.getPickListByName('frequencyofincomereceipt');
    // this. IncomeSourceDropDownItem$ = this._commonDropdownService.getPickListByName('incomesource');
    this. SupportOrderStatusDownItem$ = this._commonDropdownService.getPickListByName('statuscodes');
    this. SupportPaymentFrequencyDropDownItem$ = this._commonDropdownService.getPickListByName('wagefrequency');
    this. SupportOrderStateDropDownItem$ = this._commonDropdownService.getPickListByName('state');
  }
  loadCounty(state) {
    console.log(state);
    this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
      this.countyDropDownItems$ = Observable.of(countyList);
    });
  }
  submitIncome() {
    const incomeDetails = this.incomeDetailsFormGroup.getRawValue();
    const deemedparent = this.deemedparentFormGroup.getRawValue();
    const childcareexpenses = this.childcareexpensesFormGroup.getRawValue();
    deemedparent.caseid = this._personInfoService.getCaseId();
    incomeDetails.deemedparent = deemedparent;
    incomeDetails.childcareexpenses = childcareexpenses;
    const details = {'addupdatefinanceincome': incomeDetails};
    console.log('---------------=-=-=-=-=-=-=');
    console.log(details);
    this._personInfoService.savePersonIncomeDetails(details).subscribe(response => {
      this._alertSevice.success('Income details added successfully!');
      this.loadIncomeDetails(this.personid);
      this.resetForm();
    },
      error => {
        console.log(error);
      }
    );
  }
  submitAsset() {
    const asset = this.assetFormGroup.getRawValue();
    const details = {'where': asset};
    console.log('---------------=-=-=-=-=-=-=');
    console.log(details);
    this._personInfoService.saveAssetDetails(details).subscribe(response => {
      this._alertSevice.success('Asset details added successfully!');
      this.loadAssetDetails(this.personid);
    },
      error => {
        console.log(error);
      }
    );
  }
  submitSupportOrder() {
    const supportorder = this.supportorderFormGroup.getRawValue();
    const details = {'addupdatefinancesupportorder': supportorder};
    this._personInfoService.saveSupportOrderDetails(details).subscribe(response => {
      this._alertSevice.success('Supprt Order details added successfully!');
      this.loadSupportOrderDetails(this.personid);
    },
      error => {
        console.log(error);
      }
    );
  }
  getSuggestedAddress() {
    if (this.assetFormGroup.value.addressline1 &&
        this.assetFormGroup.value.addressline1.length >= 3 ) {
         this.suggestAddress();
     }
 }
 suggestAddress() {
  this._commonHttpService
      .getArrayListWithNullCheck(
          {
              method: 'post',
              where: {
                  prefix: this.assetFormGroup.value.addressline1,
                  cityFilter: '',
                  stateFilter: '',
                  geolocate: '',
                  geolocate_precision: '',
                  prefer_ratio: 0.66,
                  suggestions: 25,
                  prefer: 'MD'
              }
          },
          NewUrlConfig.EndPoint.Intake.SuggestAddressUrl
      ).subscribe(
       (result: any) => {
         if (result.length > 0) {
         this.suggestedAddress$ = result;
       }
       }
     );
  }
  selectedAddress(model) {
    this.assetFormGroup.patchValue({
      addressline1: model.streetLine ? model.streetLine : '',
      cityname: model.city ? model.city : '',
      statetypekey: model.state ? model.state : ''
   });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
          {
              method: 'post',
              where: addressInput
          },
          NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
      )
      .subscribe(
          (result) => {
              if (result[0].analysis) {
                  this.assetFormGroup.patchValue({
                      zip5no: result[0].components.zipcode ? result[0].components.zipcode : ''
                  });
                  if (result[0].metadata.countyName) {
                    this._commonHttpService.getArrayList(
                      {
                        nolimit: true,
                        where: { referencetypeid: 306,  mdmcode: this.assetFormGroup.value.statetypekey, description: result[0].metadata.countyName}, method: 'get'
                      },
                      'referencevalues?filter'
                  ) .subscribe(
                    (resultresp) => {
                      this.assetFormGroup.patchValue({
                        countytypekey: resultresp[0].ref_key
                    });
  
                    }
                  );
                }
               }
              } ,
              (error) => {
                  console.log(error);
              }
      );
   }
  resetForm() {
    this.addIncomeDetails = false;
    this.initiateFormGroup();
    // this.incomeDetailsFormGroup.patchValue({});
    // this.deemedparentFormGroup.patchValue({});
    // this.childcareexpensesFormGroup.patchValue({});
  }
  resetAsset() {
    this.addAssetDetails = false;
    this.assetFormGroup.reset();
    // this.assetFormGroup.patchValue({});
  }
  resetSupportOrder() {
    this.addSupportDetails = false;
    this.supportorderFormGroup.reset();
    // this.supportorderFormGroup.patchValue({});
  }
  activatemainTab(type) {
    this.selectedmainTab = type;
  }
  activateTab(type) {
    if (this.selectedTab === type) {
      this.selectedTab = '';
    } else {
      this.selectedTab = type;
    }
  }
  loadAssetDetails(personid) {
    this.getAssetDetails(personid).subscribe(assetdetails => {
      this.asset_result = assetdetails;
      if (this.asset_result && this.asset_result[0] && this.asset_result[0].getfinanceassets) {
        this.assetInfo = this.asset_result[0].getfinanceassets;
      }
    });
  }
  loadSupportOrderDetails(personid) {
    this.getSupportOrder(personid).subscribe(supportorderdetails  => {
      this.support_order_result = supportorderdetails;
      if (this.support_order_result  && this.support_order_result.getfinancesupportorder) {
        this.supportInfo = this.support_order_result.getfinancesupportorder;
      }
    });
  }
  loadCsesDetails(personid) {
    this.showcsesDetails = false;
    this.getCsesDetails(personid).subscribe(supportorderdetails  => {
      this.showcsesDetails = true;
      this.cses_result = supportorderdetails;
      if (this.cses_result  && this.cses_result.getfinancecses) {
        this.csesInfo = this.cses_result.getfinancecses;
        if (!this.csesInfo['supportorder']) {
          this.csesInfo['supportorder'] = {};
        }
      }
    });
  }
  getCsesDetails(personid: string) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personid }
        }),
        'People/financecseslist?filter'
      );
  }
  getSupportOrder(personid: string) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personid }
        }),
        'People/financesupportorderlist?filter'
      );
  }
   loadIncomeDetails(personid) {
    this.getIncomeDetails(personid).subscribe(incomedetails => {
      this.income_result = incomedetails;
      if (this.income_result && this.income_result.getfinanceincome) {
        this.incomeInfo = this.income_result.getfinanceincome;
      }
    });
  }
  confirmDelete(persondetails) {
    this.deleteIncomedetails(persondetails).subscribe(incomedetails => {
      this.loadIncomeDetails(persondetails.personid);
    });
  }
  deleteIncomedetails(persondetails) {
    return this._commonHttpService
      .getPagedArrayList(
        {
          method: 'post',
          financeincomedelete: {
            personid: persondetails.personid,
            incomeid: persondetails.incomeid
          }
        },
        'People/financeincomedelete?filter'
      );
  }
  confirmAssetDelete(assetdetails) {
    this.deleteAssetdetails(assetdetails).subscribe(incomedetails => {
      this.loadAssetDetails(this.personid);
    });
  }
  deleteAssetdetails(assetdetails) {
    return this._commonHttpService
      .getPagedArrayList(
        {
          method: 'post',
          where: {
            personassetid: assetdetails.personassetid,
          }
        },
        'People/deletefinanceasset?filter'
      );
  }
  confirmSupportDelete(assetdetails) {
    this.deleteSupportdetails(assetdetails).subscribe(incomedetails => {
      this.loadSupportOrderDetails(this.personid);
    });
  }
  deleteSupportdetails(assetdetails) {
    return this._commonHttpService
      .getPagedArrayList(
        {
          method: 'post',
          financeincomedelete: {
            personid: assetdetails.personid,
            csesclientsupportorderid: assetdetails.csesclientsupportorderid
          }
        },
        'People/financesupportorderdelete?filter'
      );
  }
  getIncomeDetails(personid) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personid }
        }),
        'People/financeincomelist?filter'
      );
  }
  getAssetDetails(personid: string) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'post',
          where: { personid: personid }
        }),
        'People/getfinanceasset?filter'
      );
  }
  enableIncomeForm() {
    this.addIncomeDetails = true;

  }
  enableAssetForm() {
    this.addAssetDetails = true;
  }
  enableSupportForm() {
    this.addSupportDetails = true;
  }
  editIncomeDetails(incomeInfo) {
    console.log(incomeInfo);
    if (incomeInfo && incomeInfo.personid) {
        this.addIncomeDetails = true;
        this.incomeDetailsFormGroup.patchValue(incomeInfo);
        this.deemedparentFormGroup.patchValue(incomeInfo.deemedparent);
        this.childcareexpensesFormGroup.patchValue(incomeInfo.childcareexpenses);
    }
    // this._personInfoService.setIncomeInfo(incomeDetails);
  }
  editAssetDetails(assetdetails) {
    if (assetdetails && assetdetails.personassetid) {
      this.addAssetDetails = true;
      this.assetFormGroup.patchValue(assetdetails);
    }
  }
  editSupportOrderDetails(supportdetails) {
    if (supportdetails && supportdetails.csesclientsupportorderid) {
      this.addSupportDetails = true;
      this.supportorderFormGroup.patchValue(supportdetails);
    }
  }

  getIncomeSource() {
    this.IncomeSourceDropDownItem$ = this._commonHttpService.getArrayList({
    }, 'Picklistvalues/incomepicklistvalues').map((result) => {
      if (result && result['UserToken'].length > 0) {
        const incomeSource = result['UserToken'];
        return incomeSource.map(
          (res) =>
              new DropdownModel({
                  text: res.value_tx,
                  value: res.picklist_value_cd
              })
      );
      }
  });
}


}





