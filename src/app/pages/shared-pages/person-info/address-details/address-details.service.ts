import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { PersonInfoService } from '../person-info.service';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { Observable } from 'rxjs/Observable';
import { AddressType,EmailType, PhoneType } from '../../../admin/general/_entities/general.data.models';

@Injectable()
export class AddressDetailsService {
  public isShowAddAddress$ = new Subject<any>();
  public selectedAddress$ = new Subject<any>();
  public addressPersonType$ = new Observable<AddressType[]>();
  public emailPersonType$= new Observable<EmailType[]>();
  public phonePersonType$= new Observable<PhoneType[]>();
  personId: any;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _personService: PersonInfoService,
  ) {
    this.personId = (this._personService.getPersonId()) ? this._personService.getPersonId() : '';
  }

  isShowAddAddressEnabled(value) {
    this.isShowAddAddress$.next(value);
  }

  getAddress(model) {
    this.selectedAddress$.next(model);
  }

  getAddressListPage(page: number) {
    const source = this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest(
          {
            method: 'get',
            where: { personid: this._personService.getPersonId()},
            page: 1, // this.paginationInfo.pageNumber,
            limit: 10// this.paginationInfo.pageSize,
          }),
        CommonUrlConfig.EndPoint.PERSON.ADDRESS.ListAddressUrl + '?filter'
      ).map((result: any) => {
        return {
          data: result,
          count: result.length > 0 ? result[0].totalcount : 0
          // canDisplayPager: result.length > 0 ? result[0].totalcount > this.paginationInfo.pageSize : false
        };
      })

    this.addressPersonType$ = source.pluck('data');
  }



  // getEmailList() {

  //   let payload = {
  //                       "personid": this.personId
  //  }


  //   return this._commonHttpService.getArrayList(payload, CommonUrlConfig.EndPoint.PERSON.EMAIL.ListEmail).
  //   subscribe(res => console.log(res));
  // }


  getEmailList(personId: any) {
    const source = this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest(
          {
            method: 'get',
            where: { personid: personId },
            page: 1, // this.paginationInfo.pageNumber,
            limit: 10// this.paginationInfo.pageSize,
          }),
        CommonUrlConfig.EndPoint.PERSON.EMAIL.ListEmail + '?filter'
      ).map((result: any) => {
        return {
          data: result,
          count: result.length > 0 ? result[0].totalcount : 0
          // canDisplayPager: result.length > 0 ? result[0].totalcount > this.paginationInfo.pageSize : false
        };
      }).share();

    this.emailPersonType$ = source.pluck('data');
  }


  getPhoneList(personId: any) {
    const source = this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest(
          {
            method: 'get',
            where: { personid: personId },
            page: 1, // this.paginationInfo.pageNumber,
            limit: 10// this.paginationInfo.pageSize,
          }),
        CommonUrlConfig.EndPoint.PERSON.PHONE.ListPhoneUrl + '?filter'
      ).map((result: any) => {
        return {
          data: result,
          count: result.length > 0 ? result[0].totalcount : 0
          // canDisplayPager: result.length > 0 ? result[0].totalcount > this.paginationInfo.pageSize : false
        };
      }).share();

    this.phonePersonType$ = source.pluck('data');
  }





  set addressPersonType(addressPersonType: Observable<AddressType[]>) {
    this.addressPersonType$ = addressPersonType;
  }

  get addressPersonType(): Observable<AddressType[]> {
    return this.addressPersonType$;
  }
}
