import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { NewUrlConfig } from '../../../../newintake/newintake-url.config';
import { Validators, Form, FormBuilder, FormGroup } from '@angular/forms';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DataStoreService, SessionStorageService, CommonDropdownsService, AlertService } from '../../../../../@core/services';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ControlUtils } from '../../../../../@core/common/control-utils';
import { AddressDetailsService } from '../address-details.service';
import { PersonInfoService } from '../../person-info.service';
import { Phone } from '../../../../provider-applicant/new-public-applicant/_entities/portalApplicantModel';
import { CASE_STORE_CONSTANTS } from '../../../../case-worker/_entities/caseworker.data.constants';
import { ActivatedRoute } from '@angular/router';
import { NavigationUtils } from '../../../../_utils/navigation-utils.service';


@Component({
  selector: 'add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.scss']
})

export class AddAddressComponent implements OnInit {
  suggestedAddress$: Observable<any[]>;
  personAddressFormGroup: FormGroup;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  addresstypeDropdownItems$: Observable<DropdownModel[]>;
  addressTypes: Observable<any[]>;
  personId: any;
  reportMode: string;
  personAddressId: any;
  phoneNumberList = [];
  emailList = [];
  id: string;
  actionText = 'Add';
  minDate = new Date();
  maxDate = new Date();

  @Output() addflag: EventEmitter<Boolean> = new EventEmitter();
  enableReason = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _commonDropdownService: CommonDropdownsService,
    private _dropdownService: CommonDropdownsService,
    private _alertservices: AlertService,
    private _addressService: AddressDetailsService,
    private _personService: PersonInfoService,
    private _session: SessionStorageService,
    private _dataStoreService: DataStoreService,
    private route: ActivatedRoute,
    private _navigationUtils: NavigationUtils
  ) {
  }

  ngOnInit() {
    this.initiateFormGroup();
    this.loadDropDown();
    this.personId = (this._personService.getPersonId()) ? this._personService.getPersonId() : '';
    // this._addressService.getPhoneList(this.personId);
    // this._addressService.getEmailList(this.personId);
    this.phoneNumberList = this.initializePhoneNumberList();
    this.emailList = this.initializeEmailList();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this._addressService.selectedAddress$.subscribe(data => {
      if (data) {

        this.editAddress(data);


      }
    });
  }

  initializePhoneNumberList() {
    // check for phone number api a list once both associated with address.
    // assign phoneNumberList
    // set the phone list here on edit

    return [{
      personphonetypekey: null,
      phonenumber: null
    }];
  }
  initializeEmailList() {
    // check for  email api list once both associated with address.
    // assign phoneNumberList, emailList
    // set the email List  on edit
    return [{
      type: 'Primary',
      email: null
    },
    {
      type: 'Secondary',
      email: null
    }];
  }

  private initiateFormGroup() {
    this.personAddressFormGroup = this._formBuilder.group({
      personaddressid: [null],
      address: ['', [Validators.required]],
      address2: [''],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      county: [''],
      zipcode: ['', [Validators.required]],
      addresstype: ['', [Validators.required]],
      knownDangerAddress: [null],
      knownDangerAddressReason: [null],
      addressstartdate: [''],
      personadrenddate: [''],
      isHouseholdMember: [null],
      isCurrentAddress: [false],
      durationDay: [null],
      currentlocationflag: [0],
    });
  }

  editAddress(model) {
    this.actionText = 'Save';
    this.loadCounty(model.state);
    let knownDangerAddress = null;
    if (model.danger === true) {
      knownDangerAddress = 1;
    } else if (model.danger === false) {
      knownDangerAddress = 0;
    } else {
      knownDangerAddress = 2;
    }
    setTimeout(() => {
      this.personAddressFormGroup.patchValue(model);
      this.personAddressFormGroup.patchValue({
        addresstype: model.personaddresstypekey,
        address2: model.address2,
        knownDangerAddress: knownDangerAddress,
        knownDangerAddressReason: model.dangerreason
      });
    }, 100);
    // this.getPhoneNumberList();
    // this.getEmailList();

    this.personAddressId = model.personaddressid;
    this.reportMode = 'edit';
  }


  loadCounty(state) {
    this._commonDropdownService.getPickListByMdmcode(state).subscribe(countyList => {
      this.countyDropDownItems$ = Observable.of(countyList);
    });
  }

  // loadCounty(statekey?) {
  //   const state = statekey ? statekey : this.personAddressFormGroup.get('state').value;
  //   console.log('state...', state);
  //   const source = this._commonHttpService.getArrayList(
  //     {
  //       where: { state: state },
  //       order: 'countyname',
  //       method: 'get',
  //       nolimit: true
  //     },
  //     CommonUrlConfig.EndPoint.Listing.CountyListUrl + '?filter'
  //   ).map((result) => {
  //     return {
  //       counties: result.map(
  //         (res) =>
  //           new DropdownModel({
  //             text: res.countyname,
  //             value: res.countyname
  //           })
  //       )
  //     };
  //   }).share();

  //   this.countyDropDownItems$ = source.pluck('counties');
  // }


  getSuggestedAddress() {
    if (this.personAddressFormGroup.value.address &&
      this.personAddressFormGroup.value.address.length >= 3) {
      this.suggestAddress();
    }
  }
  suggestAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.personAddressFormGroup.value.address,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        NewUrlConfig.EndPoint.Intake.SuggestAddressUrl
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
            this.suggestedAddress$ = result;
          }
        }
      );
  }
  selectedAddress(model) {
    this.personAddressFormGroup.patchValue({
      address: model.streetLine ? model.streetLine : '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.personAddressFormGroup.patchValue({
              zipcode: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            this.loadCounty(this.personAddressFormGroup.value.state);
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.personAddressFormGroup.value.state, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  if(resultresp[0]){
                    this.personAddressFormGroup.patchValue({
                      county: resultresp[0].ref_key
                    });
                  }
                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }


  // private loadDropDown() {
  //   const source = forkJoin([
  //     this._commonHttpService.getArrayList(
  //       {
  //         method: 'get',
  //         nolimit: true
  //       },
  //       CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
  //         .StateListUrl + '?filter'
  //     ),
  //     this._commonHttpService.getArrayList(
  //       {
  //         method: 'get',
  //         nolimit: true,
  //         order: 'countyname asc'
  //       },
  //       CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
  //     ),
  //     this._commonHttpService.getArrayList(
  //       {
  //         method: 'get',
  //         nolimit: true,
  //         activeflag: 1,
  //         order: 'typedescription'
  //       },
  //       CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
  //         .PersonAddressUrl + '?filter'
  //     ),
  //   ])
  //     .map(result => {
  //       return {
  //         states: result[0].map(
  //           res =>
  //             new DropdownModel({
  //               text: res.statename,
  //               value: res.stateabbr
  //             })
  //         ),
  //         counties: result[1].map(
  //           (res) =>
  //             new DropdownModel({
  //               text: res.countyname,
  //               value: res.countyname
  //             })
  //         ),
  //         addresstype: result[2].map(
  //           res =>
  //             new DropdownModel({
  //               text: res.typedescription,
  //               value: res.personaddresstypekey
  //             })
  //         )
  //       };
  //     })
  //     .share();
  //   this.stateDropdownItems$ = source.pluck('states');
  //   this.countyDropDownItems$ = source.pluck('counties');
  //   this.addresstypeDropdownItems$ = source.pluck('addresstype');

  // }


  private loadDropDown() {

    //   this.maritalDropdownItems$ = this._commonDropdownService.getPickListByName('maritalstatus');
    this.stateDropdownItems$ = this._commonDropdownService.getPickListByName('state');
    console.log('satte county' + this.stateDropdownItems$);
    //this.countyDropDownItems$ = this._commonDropdownService.getPickListByName('county');
    this.addresstypeDropdownItems$ = this._commonDropdownService.getAddressType();

    //   this.genderDropdownItems$ = this._commonDropdownService.getPickListByName('gender');
    //   this.prefixDropdownItems$ = this._commonDropdownService.getPickListByName('prefix');
    //   this.suffixDropdownItems$ = this._commonDropdownService.getPickListByName('suffix');
    //   this.livingSituationDropDownItems$ = this._commonDropdownService.getPickListByName('livingsituation');
    //   this.languageTypesDropDownItems$ = this._commonDropdownService.getPickListByName('languagetype');
    //   this.primaryCitizenshipDropDownItems$ = this._commonDropdownService.getPickListByName('country');
    //   this.ethinicityDropdownItems$ = this._commonDropdownService.getPickListByName('ethnicity');
    //   this.racetypeDropdownItems$ = this._commonDropdownService.getPickListByName('race');
    //   this.religionDropdownItems$ = this._commonDropdownService.getPickListByName('religion');
    //   this.akaDropdownItems$ = this._commonDropdownService.getPickListByName('akatype');
    //   this.substanceclassDropDownItems$ = this._commonDropdownService.getPickListByName('substancetype');
    //   this.alienStatusDropDownItems$= this._commonDropdownService.getPickListByName('alienstatus');

    //   this.getRoleList();
  }

  addPersonAddress(addAddress) {
    // console.log(addAddress);
    const personAddressForm = this.personAddressFormGroup.getRawValue();
    if (personAddressForm) {
      // addAddress = this.personAddressForm.getRawValue();
      if (personAddressForm.knownDangerAddress === 1) {
        addAddress.danger = true;
        addAddress.dangerreason = personAddressForm.knownDangerAddressReason;
      } else if (personAddressForm.knownDangerAddress === 0) {
        addAddress.danger = false;
        addAddress.dangerreason = null;
      } else {
        addAddress.danger = null;
        addAddress.dangerreason = null;
      }
      addAddress.personaddresstypekey = personAddressForm.addresstype;
      // if (personAddressForm.addresstype === 'CUR') {
      //   personAddressForm.currentlocationflag = 1;
      // }
      addAddress.personid = this._personService.getPersonId();
      addAddress.address = personAddressForm.address;
      addAddress.address2 = personAddressForm.address2;
      addAddress.zipcode = personAddressForm.zipcode;
      addAddress.state = personAddressForm.state;
      addAddress.city = personAddressForm.city;
      addAddress.county = personAddressForm.county;
      addAddress.personaddressid = personAddressForm.personaddressid;

      const objectID = this._navigationUtils.getNavigationInfo().sourceID;
      const objectType = this._navigationUtils.getModuleType();
      addAddress.objectid = objectID;
      addAddress.objecttype = objectType;
      if (this.personAddressFormGroup.value.addressstartdate) {
        addAddress.addressstartdate = personAddressForm.addressstartdate;
      } else {
        addAddress.addressstartdate = null;
      }
      if (this.personAddressFormGroup.value.personadrenddate) {
        addAddress.personadrenddate = personAddressForm.personadrenddate;
      } else {
        addAddress.personadrenddate = null;
      }
      delete addAddress.currentlocationflag;
      this._commonHttpService.create(addAddress, CommonUrlConfig.EndPoint.PERSON.ADDRESS.AddUpdateUrl).subscribe(
        result => {
          this._alertservices.success('Address details Added/Updated successfully!');
          this.reportMode = 'add';
          this.personAddressFormGroup.reset();
          this.addflag.emit(true);
        },
        error => {
          this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
      this.personAddressFormGroup.reset();
      this.actionText = 'Add';
      this.personAddressFormGroup.enable();
    } else {
      this._alertservices.warn('Please fill mandatory fields for Phone');
      ControlUtils.validateAllFormFields(this.personAddressFormGroup);
      // ControlUtils.setFocusOnInvalidFields();
    }


    // const personid = this.personId;
    // let addupdateemail=[];


    // const dataemail = this.setEmailList();

    // dataemail.map(
    //   email => {
    //     let type= " ";
    //       if (email.type === 'Primary') {
    //         type='P';
    //     } else { type = 'S'; }

    //     addupdateemail.push({pesronemailid:null,
    //       personid:this.personId,
    //       personemailtypekey:type,
    //       email:email.email})
    //   }
    // )

    // this._commonHttpService.create({'addupdateemail': addupdateemail}, CommonUrlConfig.EndPoint.PERSON.EMAIL.AddUpdateEmail).subscribe(
    // result => {
    //   // this._alertservices.success('Address details Added/Updated successfully!');
    //    this.reportMode = 'add';

    //   console.log('Email List' + result);
    // },
    // error => {
    //   this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    // }


    // );



    //   let addupdatePhone=[];


    // const dataphone = this.setPhoneNumberList();

    // dataphone.map(
    //   phone => {
    //     addupdatePhone.push({personphonenumberid: null,
    //       personid: this.personId,
    //       personphonetypekey: phone.personphonetypekey,
    //       phonenumber: phone.phonenumber,
    //       phoneextension: null})
    //   }
    // )

    // this._commonHttpService.create({'addupdatephonenumber': addupdatePhone}, CommonUrlConfig.EndPoint.PERSON.PHONE.AddUpdatePhone).subscribe(
    // result => {
    //   // this._alertservices.success('Address details Added/Updated successfully!');
    //    this.reportMode = 'add';

    //   console.log('Phone List' + result);
    // },
    // error => {
    //   this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    // }


    // );


  }

  cancelForm() {
    this._addressService.isShowAddAddressEnabled(false);
  }

  getPhoneNumberList() {
    // check for phone number api a list once both associated with address.
    // assign phoneNumberList
    // set the phone list here on edit

    this._addressService.phonePersonType$.subscribe((data) => {
      if (data.length > 0) {
        this.phoneNumberList = data;
      }
    });

  }


  setPhoneNumberList() {
    return this.phoneNumberList;
  }

  getEmailList() {
    // check for  email api list once both associated with address.
    // assign phoneNumberList, emailList
    // set the email List  on edit
    this._addressService.emailPersonType$.subscribe((data) => {
      if (data) {
        data.forEach((email) => {
          if (email.personemailtypekey === 'P') {
            email.type = 'Primary';
          } else if (email.personemailtypekey === 'S') {
            email.type = 'Secondary';
          }
        });
        this.emailList = data;
      }
    });
  }


  // this._commonHttpService.getAll(CommonUrlConfig.EndPoint.PERSON.PHONE.ListPhoneUrl).subscribe(
  //   result => {
  //     // this._alertservices.success('Address details Added/Updated successfully!');
  //     // this.reportMode = 'add';

  //     console.log('Email List' + result);
  //   },
  //   error => {
  //     this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
  //   }
  // );


  setEmailList() {
    // check for  email api list once both associated with address.
    // assign phoneNumberList, emailList
    // set the email List  on edit
    return this.emailList;
  }



  addPhoneNumberList() {

  }



  updateEmailList(event) {
    console.log(event, this.emailList);
    this.emailList = event;
    console.log(this.emailList);

  }


  updatePhoneList(event) {
    console.log(event, this.phoneNumberList);
    this.phoneNumberList = event;
    console.log(this.phoneNumberList);
  }

  knowDangerChange(value) {
    this.enableReason = ( value === 1) ? true : false;
    if ( this.enableReason ) {
      this.personAddressFormGroup.get('knownDangerAddressReason').setValidators(Validators.required);
      this.personAddressFormGroup.get('knownDangerAddressReason').updateValueAndValidity();
    } else {
      this.personAddressFormGroup.get('knownDangerAddressReason').clearValidators();
      this.personAddressFormGroup.get('knownDangerAddressReason').updateValueAndValidity();
    }
  }
}
