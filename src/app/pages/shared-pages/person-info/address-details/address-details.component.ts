import { Component, OnInit } from '@angular/core';
import { AddressDetailsService } from './address-details.service';
import { PersonInfoService } from '../person-info.service';

@Component({
  selector: 'address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss']
})
export class AddressDetailsComponent implements OnInit {

  Flag = true;
  isShowAddAddress: boolean = false;
  constructor(
    private _addressService: AddressDetailsService,
    private _personService: PersonInfoService
  ) { }

  ngOnInit() {
    this.listenToEnableAddAddress();
  }

  enableAddAddress() {
    return this.isShowAddAddress = !this.isShowAddAddress;
  }

  listenToEnableAddAddress() {
    this._addressService.isShowAddAddress$.subscribe((data) => {
      this.isShowAddAddress = (data) ? true : false;
    });
  }


  addressAdd(flag) {
    console.log(flag);
    if (flag) {
      this.isShowAddAddress = false;
      this.Flag = false;
      var _this = this;
      setTimeout(() => {
        _this.Flag = true;
      }, 1000);
    }

  }
}
