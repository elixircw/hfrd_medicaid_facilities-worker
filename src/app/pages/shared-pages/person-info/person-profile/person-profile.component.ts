import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { Validators, Form, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { DataStoreService, SessionStorageService, CommonDropdownsService, AlertService } from '../../../../@core/services';
import * as moment from 'moment';
import { DISABLED } from '@angular/forms/src/model';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { PersonInfoService } from '../person-info.service';
import { PersonProfileService } from './person-profile.service';
import { AppConstants } from '../../../../@core/common/constants';
import { NavigationUtils } from '../../../_utils/navigation-utils.service';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import { IntakeUtils } from '../../../_utils/intake-utils.service';
import { AppConfig } from '../../../../app.config';
import { config } from '../../../../../environments/config';
import { CASE_STORE_CONSTANTS } from '../../../case-worker/_entities/caseworker.data.constants';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';

@Component({
  selector: 'person-profile',
  templateUrl: './person-profile.component.html',
  styleUrls: ['./person-profile.component.scss']
})
export class PersonProfileComponent implements OnInit, OnDestroy {

  id: string;
  involvedPersonFormGroup: FormGroup;
  suggestedAddress$: Observable<any[]>;
  maritalDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<any[]>;
  genderDropdownItems$: Observable<DropdownModel[]>;
  prefixDropdownItems$: Observable<any[]>;
  suffixDropdownItems$: Observable<any[]>;
  livingSituationDropDownItems$: Observable<any[]>;
  languageTypesDropDownItems$: Observable<any[]>;
  primaryCitizenshipDropDownItems$: Observable<any[]>;
  ethinicityDropdownItems$: Observable<any[]>;
  racetypeDropdownItems$: Observable<any[]>;
  religionDropdownItems$: Observable<any[]>;
  akaDropdownItems$: Observable<any[]>;
  substanceclassDropDownItems$: Observable<any[]>;
  babysubstanceclassDropDownItems$: Observable<any[]>;
  alienStatusDropDownItems$: Observable<any[]>;

  householdroleDropdownItems$: Observable<DropdownModel[]>;
  collateralroleDropdownItems$: Observable<DropdownModel[]>;
  ROLE_ITREATION_COUNT = 0;

  imageChangedEvent: File;
  croppedImage: File;
  isImageHide: boolean;
  errorValidateAddress = false;
  beofreImageCropeHide = false;
  afterImageCropeHide = false;
  isDefaultPhoto = true;
  isImageLoadFailed = false;
  editImage = true;
  suffixTypes;
  prefixTypes;
  personAge = '';
  personAgeCheck: number;
  selectedPersonDetails: any;
  selected: String;
  rolecount: any;
  existingRoleId: any;

  today = new Date();
  // dd = String(this.today.getDate()).padStart(2, '0');
  // mm = String(this.today.getMonth() + 1).padStart(2, '0'); //January is 0!
  // yyyy = this.today.getFullYear();

  dd = this.today.getDate();
  mm = this.today.getMonth();
  yyyy = this.today.getFullYear();

  minDate = new Date(1900, 0, 1);
  maxDate = new Date(this.yyyy, this.mm, this.dd);

  aliasList = [];
  personmaritalstatusList = [];
  personspouseaddressList = [];
  personroleList = [];
  personRole: any;
  rolesList = [];
  isSsnHidden = true;
  ssnEye = 'fa-eye';
  showSsnMask = true;
  profileUpdateSubscription: Subscription;
  intakeData: any;
  iscitizen = 0;
  store: any;
  SSNDuplicateFound = false;
  babySubstanceOther = false;
  errorMessage: string;
  errorList: any[] = [];
  isClosed = false;
  dsdsactionsummary: any;
  isCPS = false;
  isServiceCase = false;
  isAdoptionCase = false;
  navigationInfo:any;
  currentStatus: string;
  requiredPalceHolders: any = {
    'Lastname': 'Lastname',
    'Firstname': 'Firstname',
    'gendertypekey': 'Gender',
    'Dob': 'Date of Birth',
    'preadptdate': 'Previously Adoption Date',
    'dangerousself': 'Danger to self ?',
    'Dangerousworker': 'Danger to worker ?',
    'ismentalimpair' : 'Appearance of mentally impaired ?',
    'ismentalillness': 'Signs of mental illness?',
    'roletype': 'In Household/Other/Collateral',
    'roles': 'Roles',
    'ismentalillnessReason' : 'Signs of mental illness reason',
    'dangerousselfreason' : 'Danger to self reason',
    'DangerousWorkerReason': 'Danger to worker reason',
    'ismentalimpairReason' : 'Appearance of mentally impaired reason',
    'arnumber' : 'Alien Registration Number',
    'astatus': 'Alien Status',
    'nationality': 'Nationality',
    'primarycitizenship' : 'Primary Citizenship',
    'drugexposedtypekey' : 'Substance Class',
    'biologicalmothermarriedsw' : 'Mother (Biological) Married at time of child\'s birth ?'

  };
  isGlasses = [{
    id: true,
    value: 'Yes'
  },
  {
    id: false,
    value: 'No'
  }];
  racelist: any;
  setraceunkown: string;
  astatusChange: any;
  hairColor$: Observable<any[]>;
  hairTexture$: Observable<any[]>;
  eyeColor$: Observable<any[]>;
  physicalBuild$: Observable<any[]>;
  skinTone$: Observable<any[]>;
  isGlass$: Observable<any[]>;
  showOtherColor: boolean;
  showOtherTexture: boolean;
  isNotSexOffender = false;
  houseHold: any = [];
  isheadofhouseholdflag: boolean = false;
  userphoto: any;
  isRaceUnknownFlag: boolean = false;
  isChildRole: boolean = false;
  constructor(private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _service: PersonProfileService,
    private _personInfoService: PersonInfoService,
    private _commonDropdownService: CommonDropdownsService,
    private _navigationUtils: NavigationUtils,
    private _alertService: AlertService,
    private renderer: Renderer2,
    private _dataStoreService: DataStoreService,
    private _intakeUtils: IntakeUtils,
    private _session: SessionStorageService) {
    this.store = this._dataStoreService.getCurrentStore();
  }

  ngOnInit() {
    /* if (this._personInfoService.personInfo !== '{}') {

    } */
    const da_status = this._session.getItem('da_status');
    this.currentStatus = this._dataStoreService.getData(IntakeStoreConstants.INTAKE_STATUS);
        if (da_status || this.currentStatus) {
          if (da_status === 'Closed' || da_status === 'Completed' || this.currentStatus === 'Closed' || this.currentStatus === 'Completed') {
              this.isClosed = true;
          } else {
              this.isClosed = false;
          }
        }
    this._personInfoService.aliasList = [];
    this._personInfoService.getIntakeNumberByServiceCaseID().subscribe(data => {
      if (data && data.length) {
        this.intakeData = data[0];
      }
    });
    this.initiateFormGroup();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    const dataStore = this._dataStoreService.getData('dsdsActionsSummary');
    this.dsdsactionsummary = dataStore;
    const intakeserviceid = dataStore ? dataStore.intakeserviceid : '';
    if (this.id === undefined) {
        this.id = intakeserviceid;
    }
    
    this.imageChangedEvent = Object.assign({});
    this.loadDropDown();
    this.suffixTypes = ['Jr', 'Sr'];
    this.prefixTypes = ['Mr', 'Mrs', 'Ms', 'Dr', 'Prof', 'Sister', 'Atty'];
    this.dobChangeListener();
    this.profileUpdateListener();
    this.setupChangeSubscribers();
    this.involvedPersonFormGroup.get('drugexposednewbornflag').valueChanges.subscribe(
      changeFlag => {
    if (changeFlag) {
      this.involvedPersonFormGroup.controls['drugexposedtypekey'].setValidators([Validators.required]);
      this.involvedPersonFormGroup.controls['drugexposedtypekey'].updateValueAndValidity();
    } else {
      this.involvedPersonFormGroup.controls['drugexposedtypekey'].clearValidators();
      this.involvedPersonFormGroup.controls['drugexposedtypekey'].updateValueAndValidity();
    }
    });
    this.involvedPersonFormGroup.get('citizenalenageflag').valueChanges.subscribe(
      changeFlag => {
        console.log('changeFlag changed:' + changeFlag);
        this.iscitizen = changeFlag;
        if (this.iscitizen === 1) {
          this.involvedPersonFormGroup.patchValue({
            primarycitizenship: 'USA',
            nationality: 'USA'
          });
          if (this.selectedPersonDetails && this.selectedPersonDetails.personbasicdetails.secondarycitizenship) {
            if (this.selectedPersonDetails.personbasicdetails.citizenalenageflag === 1) {
              this.involvedPersonFormGroup.patchValue({
                secondarycitizenship : this.selectedPersonDetails.personbasicdetails.secondarycitizenship ? this.selectedPersonDetails.personbasicdetails.secondarycitizenship : null
               });
            } else {
              this.involvedPersonFormGroup.patchValue({
                secondarycitizenship : null
              });
            }
          } else {
            this.involvedPersonFormGroup.patchValue({
              secondarycitizenship : null
            });
          }
          this.involvedPersonFormGroup.controls['primarycitizenship'].clearValidators();
          this.involvedPersonFormGroup.controls['primarycitizenship'].updateValueAndValidity();
          this.involvedPersonFormGroup.controls['nationality'].clearValidators();
          this.involvedPersonFormGroup.controls['nationality'].updateValueAndValidity();
          this.involvedPersonFormGroup.controls['astatus'].clearValidators();
          this.involvedPersonFormGroup.controls['astatus'].updateValueAndValidity();
          this.involvedPersonFormGroup.controls['arnumber'].clearValidators();
          this.involvedPersonFormGroup.controls['arnumber'].updateValueAndValidity();
          this.involvedPersonFormGroup.get('primarycitizenship').disable();
        } else if (this.iscitizen === 0) {
          this.involvedPersonFormGroup.get('primarycitizenship').enable();
          this.involvedPersonFormGroup.controls['primarycitizenship'].setValidators(Validators.required);
          this.involvedPersonFormGroup.controls['primarycitizenship'].updateValueAndValidity();
          this.involvedPersonFormGroup.controls['nationality'].setValidators(Validators.required);
          this.involvedPersonFormGroup.controls['nationality'].updateValueAndValidity();
          this.involvedPersonFormGroup.controls['astatus'].setValidators(Validators.required);
          this.involvedPersonFormGroup.controls['astatus'].updateValueAndValidity();
          if (this.selectedPersonDetails && this.selectedPersonDetails.personbasicdetails) {
            if (this.selectedPersonDetails.personbasicdetails.citizenalenageflag === 0) {
              this.involvedPersonFormGroup.patchValue({
                primarycitizenship: this.selectedPersonDetails.personbasicdetails.primarycitizenship ? this.selectedPersonDetails.personbasicdetails.primarycitizenship : null,
                nationality: this.selectedPersonDetails.personbasicdetails.nationalitytypekey ? this.selectedPersonDetails.personbasicdetails.nationalitytypekey : null,
                secondarycitizenship : this.selectedPersonDetails.personbasicdetails.secondarycitizenship ? this.selectedPersonDetails.personbasicdetails.secondarycitizenship : null,
                astatus: this.selectedPersonDetails.personbasicdetails.alienstatustypekey ? this.selectedPersonDetails.personbasicdetails.alienstatustypekey : null,
                arnumber: this.selectedPersonDetails.personbasicdetails.alienregistrationtext ? this.selectedPersonDetails.personbasicdetails.alienregistrationtext : null,
              });
            } else {
              this.involvedPersonFormGroup.controls['primarycitizenship'].clearValidators();
              this.involvedPersonFormGroup.controls['primarycitizenship'].updateValueAndValidity();
              this.involvedPersonFormGroup.controls['nationality'].clearValidators();
              this.involvedPersonFormGroup.controls['nationality'].updateValueAndValidity();
              this.involvedPersonFormGroup.controls['astatus'].clearValidators();
              this.involvedPersonFormGroup.controls['astatus'].updateValueAndValidity();
              this.involvedPersonFormGroup.controls['arnumber'].clearValidators();
              this.involvedPersonFormGroup.controls['arnumber'].updateValueAndValidity();
              this.involvedPersonFormGroup.patchValue({
                primarycitizenship: null,
                nationality: null,
                secondarycitizenship : null,
                astatus: null,
                arnumber: null,
              });
            }
          }
        }
      }
    );
    this.involvedPersonFormGroup.get('astatus').valueChanges.subscribe(
      astatusChange => {
        console.log('astatusChange changed:' + astatusChange);
        this.astatusChange = astatusChange;
        if (this.astatusChange && this.astatusChange === 'ILLAEN') {
          this.involvedPersonFormGroup.get('arnumber').reset();
          this.involvedPersonFormGroup.get('arnumber').disable();
          this.involvedPersonFormGroup.controls['arnumber'].clearValidators();
          this.involvedPersonFormGroup.controls['arnumber'].updateValueAndValidity();
        } else if ( this.astatusChange && this.astatusChange !== 'ILLAEN') {
          this.involvedPersonFormGroup.get('arnumber').enable();
          this.involvedPersonFormGroup.controls['arnumber'].setValidators(Validators.required);
          this.involvedPersonFormGroup.controls['arnumber'].updateValueAndValidity();
        } else {
          this.involvedPersonFormGroup.controls['arnumber'].clearValidators();
          this.involvedPersonFormGroup.controls['arnumber'].updateValueAndValidity();
        }
      }
    );

  }

  dobchange() {
    this.involvedPersonFormGroup.patchValue({
      drugexposednewbornflag: null,
      drugexposedtypekey: '',
      safehavenbabyflag: null,
      maritalstatustypekey: null
      });
  }

  toggleSsn = () => {
    this.isSsnHidden = !this.isSsnHidden;
    if (this.isSsnHidden) {
      this.ssnEye = 'fa-eye';
      this.showSsnMask = true;
    } else {
      this.ssnEye = 'fa-eye-slash';
      this.showSsnMask = false;
    }
  }

  get f() {
    return this.involvedPersonFormGroup.controls;
  }

  getPlacHolderName(key) {
    if (this.requiredPalceHolders.hasOwnProperty(key)) {
      return this.requiredPalceHolders[key];
    } else {
      return key;
    }
  }

  private initiateFormGroup() {
    this.involvedPersonFormGroup = this._formBuilder.group({
      Lastname: ['', Validators.required],
      Firstname: ['', Validators.required],
      Middlename: [''],
      prefix: [''],
      nameSuffix: [''],
      primarylanguage: ['ENG'],
      secondarylanguage: [null],
      Dob: [Validators.required],
      dateofdeath: [null],
      isapproxdod: [null],
      isapproxdob: [null],
      isdobunknown: [null],
      safehavenbabyflag: [null],
      everbeenadoptedflag: [null],
      age: [null],
      gendertypekey: ['', Validators.required],
      religiontypekey: null,
      // maritalstatustypekeyold: [null],
      maritalstatustypekey: [null],
      // race: [null],
      SSN: [''],
      ssnverified: [null],
      mdm_id: [null],
      actorid: [null],
      intakeservicerequestid: [null],
      ethnicgrouptypekey: [null],
      occupation: [null],
      stateid: [null],
      // aliasname: [null],
      source: [null],
      potentialSOR: [''],
      eDLHistory: [''],
      dMH: [''],
      Race: [[]],
      Address: [''],
      address1: [''],
      Zip: [''],
      City: [''],
      State: [null],
      County: [null],

      DangerousAddressReason: [''],
      tribalassociation: [null],
      height: [''],
      heightft: [''],
      heightin: [''],
      weight: [''],
      weightpnd: [''],
      weightound: [''],
      tattoo: [''],
      haircolortypekey: [''],
      hairtexturetypekey: [''],
      eyecolortypekey: [''],
      physicalbuildtypekey: [''],
      skintonetypekey: [''],
      hairtextureotherdesc: [''],
      haircolorotherdesc: [''],
      isglasses: [null],
      PhyMark: [''],
      dangerousselfreason: [''],
      ismentalimpairReason: [''],
      DangerousWorkerReason: [''],
      ismentalillnessReason: [''],
      dangerousself: [null],
      Dangerousworker: [null],
      ismentalimpair: [null],
      ismentalillness: [null],
      // mentalimpairdetail: [''],
      // mentalillnessdetail: [''],
      personid: [null],
      //  iscollateralcontact: [null],
      //  ishousehold: [null],
      roletype: [null, [Validators.required]],
      drugexposednewbornflag: [null],
      // fetalalcoholspctrmdisordflag: [false],
      sexoffenderregisteredflag: [null],
      probationsearchconductedflag: [null],
      otherdrugs: [''],
      drugexposedtypekey: [''],
      needs: [null],
      strengths: [null],
      livingsituationkey: [null],
      licensedfacilitykey: [null],
      otherlicensedfacility: [null],
      livingsituationdesc: [null],
      otherreligion: [null],
      alienregistrationtext: [null],
      alienstatustypekey: [null],
      citizenalenageflag: [null],
      isqualifiedalien: [null],
      verificationremarks: [null],
      // issafehaven: [false]
      primarycitizenship: [null],
      secondarycitizenship: [null],
      nationality: [null],
      astatus: [null],
      arnumber: [null],
      householdflag: [null],
      // role: '',
      roles: [null],
      spouseaddress1: [null],
      spouseAddress2: [null],
      spousecity: [null],
      spousestate: [null],
      spousezipcode: [null],
      spousecounty: [null],
      spousehomenumber: [null],
      spouseofficenumber: [null],
      spouseofficeextension: [null],
      spouseprefix: [null],
      spousefirstname: [null],
      spouselastname: [null],
      spousemiddlename: [null],
      spousesuffix: [null],
      numberofchildren: [null],
      maritalcomments: [null],
      maritalstartdate: [null],
      maritalenddate: [null],
      marriageplace: [null],
      divorceplace: [null],
      aname: false,
      preadptdate: [],
      userphoto: [''],
      // primaryrole: [null]
      // nname: false,
      // cname: false,
      // mname: false
      employername: [''],
      clienttitle: [''],
      isheadofhousehold: false,
      biologicalmothermarriedsw: [null]
    });
    const search_details = this._personInfoService.getSearchData();
    console.log('--------creating new user-------');
    console.log(search_details);
    if (search_details && search_details.exist === 1) {
      this.updateProfileViewfromSearch(search_details);
    }



  }
  fileChangeEvent(file: any) {
    this.beofreImageCropeHide = true;
    this.afterImageCropeHide = true;
    this.imageChangedEvent = file;
    this.isDefaultPhoto = false;
    this.isImageLoadFailed = false;
    this.isImageHide = true;
  }
  private loadDropDown() {
    const babysubstanceList = ['BOTH', 'BPD', 'BPCP', 'BMTD', 'BMJA', 'BHOI', 'BESY', 'BCOC', 'BBS', 'BAS', 'FASD', 'BTN', 'BTNR' ]; // @TM: Baby substances

    this.maritalDropdownItems$ = this._commonDropdownService.getPickListByName('maritalstatus');
    this.stateDropdownItems$ = this._commonDropdownService.getPickListByName('state');
    this.countyDropDownItems$ = this._commonDropdownService.getPickListByName('county');
    this.genderDropdownItems$ = this._commonDropdownService.getPickListByName('gender');
    this.prefixDropdownItems$ = this._commonDropdownService.getPickListByName('prefix');
    this.suffixDropdownItems$ = this._commonDropdownService.getPickListByName('suffix');
    this.livingSituationDropDownItems$ = this._commonDropdownService.getPickListByName('livingsituation');
    this.languageTypesDropDownItems$ = this._commonDropdownService.getPickListByName('languagetype');
    this.primaryCitizenshipDropDownItems$ = this._commonDropdownService.getPickListByName('country');
    this.ethinicityDropdownItems$ = this._commonDropdownService.getPickListByName('ethnicity');
    this.racetypeDropdownItems$ = this._commonDropdownService.getPickListByName('race');
    this.religionDropdownItems$ = this._commonDropdownService.getPickListByName('religion');
    this.akaDropdownItems$ = this._commonDropdownService.getPickListByName('akatype');
    this.substanceclassDropDownItems$ = this._commonDropdownService.getPickListByName('substancetype');
    this.babysubstanceclassDropDownItems$ = this.substanceclassDropDownItems$.map(arr => // @TM: force filter Baby substances
      arr.filter(item => babysubstanceList.includes(item.ref_key))
    );

    let houseHoldType = 'cpsroles';
    this.navigationInfo = this._navigationUtils.getNavigationInfo();
    // console.log("NAV info", navigationInfo);
    // console.log("STOREEEE", this.store);
    // console.log("intake summary", this.dsdsactionsummary);
    if (this.navigationInfo && (this.navigationInfo.source === AppConstants.MODULE_TYPE.PUBLIC_PROVIDER
      || this.navigationInfo.source === AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_REFERRAL
      || this.navigationInfo.source === AppConstants.MODULE_TYPE.PUBLIC_PROVIDER_APPLICATION)) {
        houseHoldType = 'provdroles';
      }
    
    this.getCaseType();
    
    this.alienStatusDropDownItems$ = this._commonDropdownService.getPickListByName('alienstatus');
    this.householdroleDropdownItems$ = this._commonDropdownService.getPickListByName(houseHoldType);
    this.collateralroleDropdownItems$ = this._commonDropdownService.getPickListByName('collateralroles');
    this.hairColor$ = this._commonDropdownService.getPickList('92');
    this.hairTexture$ = this._commonDropdownService.getPickList('93');
    this.eyeColor$ = this._commonDropdownService.getPickList('79');
    this.physicalBuild$ = this._commonDropdownService.getPickList('140');
    this.skinTone$ = this._commonDropdownService.getPickList('199');
    this.isGlass$ = this._commonDropdownService.getPickList('78');
    // this.getRoleList();
  }
    getCaseType(){
        this.isCPS=false;
        this.isServiceCase=false;
        this.isAdoptionCase=false;
        
        //IS CPS?
        if (this.navigationInfo && (this.navigationInfo.source === AppConstants.CASE_TYPE.CPS_CASE)) {
          console.log("this.navigationInfo.source ", this.navigationInfo.source );
          this.isCPS = true;
        }
    
        //IS SERVICE
        if (this.navigationInfo && (this.navigationInfo.source === AppConstants.CASE_TYPE.SERVICE_CASE)) {
          this.isCPS = false;
          console.log("this.navigationInfo.source ", this.navigationInfo.source );
          this.isServiceCase = true;
        }
        if (this.store && (this.store.CASE_TYPE == AppConstants.CASE_TYPE.SERVICE_CASE)) {
          this.isCPS = false;
          console.log("this.store.CASE_TYPE", this.store.CASE_TYPE );
          this.isServiceCase = true;
        }
        if (this._session && (this._session.getItem('ISSERVICECASE'))) {
          this.isCPS = false;
          console.log("(this._session.getItem('ISSERVICECASE')", "SERVICE", (this._session.getItem('ISSERVICECASE') ));
          this.isServiceCase = true;
        }
    
        //IS ADOPTION
        
        // if (this.navigationInfo && (this.navigationInfo.source === AppConstants.CASE_TYPE.ADOPTION_CASE)) {
        //   this.isServiceCase = false;
        //   this.isCPS = false;
        //   console.log("this.navigationInfo.source CHECKING FOR ADOPTION FROM MODULE TYPE", this.navigationInfo.source );
        //   this.isAdoptionCase = true;
        // }
        if(this.dsdsactionsummary && this.dsdsactionsummary.da_type == AppConstants.DA_TYPE_TEXT.ADOPTION_CASE){
          this.isServiceCase = false;
          this.isCPS = false;
          this.isAdoptionCase = true;
          console.log("Casetype this.dsdsactionsummary.da_type",this.dsdsactionsummary.da_type);
        }  
        if(this._session && this._session.getItem('CASE_TYPE') == 'ADOPTION'){
          this.isServiceCase = false;
          this.isCPS = false;
          this.isAdoptionCase = true;
          console.log("Casetype this._session.getItem('CASE_TYPE') ",this._session.getItem('CASE_TYPE') );
        }
      }
  calculatePersonAge() {
    const person = this.involvedPersonFormGroup.getRawValue();
    if (person && person.Dob) {
      const age = { years: 0, months: 0, days: 0, totalMonths: 0, duration: null };
      age.years = (moment().diff(person.Dob, 'years', false)) ? moment().diff(person.Dob, 'years', false) : 0;
      age.totalMonths = (moment().diff(person.Dob, 'months', false)) ? moment().diff(person.Dob, 'months', false) : 0;
      age.months = (age.totalMonths - (age.years * 12)) ? age.totalMonths - (age.years * 12) : 0;
      age.days = (moment().diff(person.Dob, 'days', false)) ? moment().diff(person.Dob, 'days', false) : 0;
      age.duration = moment.duration(moment(Date.now()).diff(moment(person.Dob)));
      // console.log('durationdurationdurationdurationdurationdayssss', duration.days());
      const ddays = (age.duration.days()) ? age.duration.days() : 0;
      // const ddays = new Date(years, months, days).getDay();
      this.personAge = `${age.years} Years ${age.months} month(s) ${ddays} Day(s)`;
      this._dataStoreService.setData('personAge', age.totalMonths);
      this.personAgeCheck = age.days;
      if (age.years < 14) {
        this.involvedPersonFormGroup.patchValue({
          maritalstatustypekey: 'SG'
        });
      }
    }
  }

  dobChangeListener() {
    this.involvedPersonFormGroup.controls['Dob'].valueChanges.subscribe((res) => {
      if (res) {
        this.calculatePersonAge();
      }
    });
  }
  updateProfileViewfromSearch(searchdata) {
    this.involvedPersonFormGroup.patchValue({
      Lastname: searchdata.lastname,
      Firstname: searchdata.firstname,
      gendertypekey: searchdata.gender,
      Dob: searchdata.dob ? moment(searchdata.dob).format('YYYY-MM-DD') : null,
      dateofdeath: searchdata.dateofdeath ? moment(searchdata.dateofdeath) : null,
      occupation: searchdata.occupation,
      SSN: searchdata.ssn ? searchdata.ssn : '',
      stateid: searchdata.stateid,
      Address: searchdata.address2,
      address1: searchdata.address1,
      Zip: searchdata.zip,
      City: searchdata.city,
      State: searchdata.stateid,
      County: searchdata.county,
      aliasname: searchdata.alias,
      hasAlias: searchdata.length ? true : null,
      // mediasrc,
      // mediasrctxt,
      // maidenname,
      // dl,
      // address1,--
      // address2,--
      // zip,--
      // city,--
      // county,--
      // selectedPerson,
      // cjisnumber,
      // complaintnumber,
      // fein,
      // age,
      // email,--
      // phone, --
      // petitionid,
      // alias,
      // oldId
    });
  }
  updateProfileView(personInfo) {
    this.selectedPersonDetails = personInfo;
    const workEnv = config.workEnvironment;
    if (this.selectedPersonDetails.personbasicdetails) {
      if (this.selectedPersonDetails.personbasicdetails.userphoto) {
        this.userphoto = this.selectedPersonDetails.personbasicdetails.userphoto;
        if (workEnv === 'state') {
          this.selectedPersonDetails.personbasicdetails.userphoto = AppConfig.baseUrl + '/attachment/v1' + this.selectedPersonDetails.personbasicdetails.userphoto;
        }
      }
    }
    const personbasicdetails = (this.selectedPersonDetails) ? this.selectedPersonDetails.personbasicdetails : null;
    if (this.selectedPersonDetails) {

      if (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.personrole &&
        this.selectedPersonDetails.personbasicdetails.personrole.Personroletype &&
        this.selectedPersonDetails.personbasicdetails.personrole.Personroletype.length) {
        // const roleArray = this.selectedPersonDetails.personbasicdetails.personrole.Personroletype;
        if (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personroledetails.actor) {
          const roleArray = this.selectedPersonDetails.personroledetails.actor.intakeservicerequestactor;
          const roleArrayFilter = (roleArray && roleArray.length ) ? roleArray.filter(role => !role.spexpungementflag) : [];
        // const roleArraynotFilter =roleArray.filter(role => role.spexpungementflag);
        this.rolesList = ( roleArrayFilter && roleArrayFilter.length) ?  roleArrayFilter.map(data => data.intakeservicerequestpersontypekey) : [];
        // console.log(this.rolesList);
        }
        this.existingRoleId = this.selectedPersonDetails.personbasicdetails.personrole.Personroletype[0].personroleid;
      }

      let roleType = '';
      if (this.selectedPersonDetails.personbasicdetails &&
        this.selectedPersonDetails.personbasicdetails.personrole) {
        if (this.selectedPersonDetails.personbasicdetails.personrole.ishouseholdmember === 1) {
          roleType = 'household';
          this.invokeValidators(1);
        } else if (this.selectedPersonDetails.personbasicdetails.personrole.ishouseholdmember === 2) {
          roleType = 'other';
          this.invokeValidators(1);
        } else if (this.selectedPersonDetails.personbasicdetails.personrole.iscollateralcontact === 1) {
          roleType = 'collateral';
        }
      }
      let personrole;
      if (this.selectedPersonDetails && this.selectedPersonDetails.personbasicdetails
        && this.selectedPersonDetails.personbasicdetails.personrole) {
        personrole = this.selectedPersonDetails.personbasicdetails.personrole;
      }

      if (this.selectedPersonDetails.personbasicdetails.haircolortypekey === '1298') {
        this.showOtherColor = true;
      } else {
        this.showOtherColor = false;
      }

      if (this.selectedPersonDetails.personbasicdetails.hairtexturetypekey === '1305') {
        this.showOtherTexture = true;
      } else {
        this.showOtherTexture = false;
      }

      this.involvedPersonFormGroup.patchValue({
        mdm_id: this.selectedPersonDetails
        .personbasicdetails
        ? this.selectedPersonDetails.personbasicdetails
          .mdm_id
        : null,
        tribalassociation: this.selectedPersonDetails
          .personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .tribalassociation
          : '',
        prefix: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .prefix
          : '',
        Lastname: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails.lastname
          : '',
        Firstname: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .firstname
          : '',
        Middlename: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .middlename
          : '',
        nameSuffix: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .suffix
          : '',
        maritalstatustypekey: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .maritalstatustypekey
          : null,
        Dob: this.selectedPersonDetails.personbasicdetails
          ? moment(this.selectedPersonDetails.personbasicdetails.dob).format('YYYY-MM-DD') : null,
        preadptdate: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails.preadoptiondate : null,
        dateofdeath: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .dateofdeath : null,
        isapproxdod: this.selectedPersonDetails.personbasicdetails ? (this.selectedPersonDetails.personbasicdetails.isapproxdod === 1 ? true : null) : null,
        isapproxdob: this.selectedPersonDetails.personbasicdetails ? (this.selectedPersonDetails.personbasicdetails.isapproxdob === 1 ? true : false) : false,
        gendertypekey: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .gendertypekey
          : '',
        religiontypekey: this.selectedPersonDetails.personbasicdetails ? (this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .religiontypekey
          : null) : null,
        otherreligion: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .otherreligion
          : null,
        Dangerous: '',
        dangerAddress: '',
        SSN: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails.ssnno ? this.selectedPersonDetails.personbasicdetails.ssnno : ''
            : null,
        ssnverified: this.selectedPersonDetails.personbasicdetails ? (this.selectedPersonDetails.personbasicdetails.ssnverified === true ? true : null) : null,
        ethnicgrouptypekey: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .ethnicgrouptypekey
          : null,
        occupation: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .occupation
          : '',
        stateid: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails.stateid
          : null,
        primarylanguage: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.primarylanguageid) ?
          this.selectedPersonDetails.personbasicdetails.primarylanguageid : 'ENG',
        secondarylanguage: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.secondarylanguageid : null,
        otherprimarylanguage: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.otherprimarylanguagetypekey : null,
        hasAlias: (this.selectedPersonDetails.personbasicdetails) ? (this.selectedPersonDetails.personbasicdetails.alias.length ? true : null) : null,
        aliasname: (this.selectedPersonDetails.personbasicdetails
          && this.selectedPersonDetails.personbasicdetails.alias
          && Array.isArray(this.selectedPersonDetails.personbasicdetails.alias))
          && this.selectedPersonDetails.personbasicdetails.alias.length
          // ? this.selectedPersonDetails.personbasicdetails.salutation
          ? this.selectedPersonDetails.personbasicdetails.alias[0].firstname
          : '',
        dangerousself: personrole ? (personrole.dangertoself) : null,
        ismentalimpair: personrole ? (personrole.ismentalimpair) : null,
        Dangerousworker: personrole ? (personrole.isdangertoworker) : null,
        ismentalillness: personrole ? (personrole.ismentalillness) : null,

        otherdrugs: personrole ? (personrole.otherdrugs) : null,
        drugexposedtypekey: personrole ? (personrole.drugexposedtypekey) : null,

        dangerousselfreason: personrole ? (personrole.dangertoselfreason) : null,
        DangerousWorkerReason: personrole ? (personrole.dangertoworkerreason) : null,
        ismentalillnessReason: personrole ? (personrole.mentalillnessdetail) : null,
        ismentalimpairReason: personrole ? (personrole.mentalimpairdetail) : null,
        // DangerousselfReason: this.selectedPersonDetails
        //   .personbasicdetails
        //   ? this.selectedPersonDetails.personbasicdetails
        //     .dangertoselfreason
        //   : '',
        personid: this.selectedPersonDetails.personbasicdetails ?
          (this.selectedPersonDetails.personbasicdetails.personid
            ? this.selectedPersonDetails.personbasicdetails.personid
            : '') : null,
        iscollateralcontact: (this.selectedPersonDetails
          .personroledetails) ? this.selectedPersonDetails
            .personroledetails.actor.iscollateralcontact : null,
        roletype: roleType,

        // fetalalcoholspctrmdisordflag: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.fetalalcoholspctrmdisordflag === 1) ? true : false,
        drugexposednewbornflag: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.personrole
          && this.selectedPersonDetails.personbasicdetails.personrole.drugexposednewbornflag === 1) ? true : false,
        // probationsearchconductedflag: (this.selectedPersonDetails
        //   .personroledetails) ? this.selectedPersonDetails
        //     .personroledetails.actor.probationsearchconductedflag : null,
        // probationsearchconductedflag: (modal.probationsearchconductedflag === 1) ? true : false,

        sexoffenderregisteredflag: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.personrole
          && this.selectedPersonDetails.personbasicdetails.personrole.sexoffenderregisteredflag === 1) ? true : false,
        probationsearchconductedflag: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.personrole
          && this.selectedPersonDetails.personbasicdetails.personrole.probationsearchconductedflag === 1) ? true : false,
        safehavenbabyflag: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.personrole
            && this.selectedPersonDetails.personbasicdetails.personrole.safehavenbabyflag === 1) ? true : false,
        // everbeenadoptedflag: (modal.everbeenadoptedflag === 1) ? true : false,
        everbeenadoptedflag: personbasicdetails ? (personbasicdetails.everbeenadoptedflag === 1) ? true : false : false,
        strengths: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails
            .strengths
          : '',
        needs: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails.needs
          : '',
        citizenalenageflag: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.citizenalenageflag)
          ? ('' + this.selectedPersonDetails.personbasicdetails.citizenalenageflag) : null,
        primarycitizenship: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.primarycitizenship) ?
          this.selectedPersonDetails.personbasicdetails.primarycitizenship : '',
        secondarycitizenship: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.secondarycitizenship) ?
          this.selectedPersonDetails.personbasicdetails.secondarycitizenship : '',
        nationality: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.nationalitytypekey) ?
          this.selectedPersonDetails.personbasicdetails.nationalitytypekey : '',
        isqualifiedalien: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.isqualifiedalien)
          ? ('' + this.selectedPersonDetails.personbasicdetails.isqualifiedalien) : null,
        astatus: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails.alienstatustypekey : '',
        arnumber: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.alienregistrationtext) ?
          this.selectedPersonDetails.personbasicdetails.alienregistrationtext : '',
        verificationremarks: this.selectedPersonDetails.personbasicdetails
          ? this.selectedPersonDetails.personbasicdetails.verificationremarks : '',
        licensedfacilitykey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.licensedfacilitykey : null,
        livingsituationdesc: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.livingsituationdesc : null,
        livingsituationkey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.livingsituationkey : null,
        otherlicensedfacility: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.otherlicensedfacility : null,
        roles: this.rolesList,
        haircolortypekey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.haircolortypekey : null,
        hairtexturetypekey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.hairtexturetypekey : null,
        eyecolortypekey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.eyecolortypekey : null,
        physicalbuildtypekey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.physicalbuildtypekey : null,
        skintonetypekey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.skintonetypekey : null,
        hairtextureotherdesc: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.hairtextureotherdesc : null,
        haircolorotherdesc: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.haircolorotherdesc : null,
        isglasses: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.isglasses : null,
        employername: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.employername : null,
        clienttitle: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.clienttitle : null,
        //@Simar: On editing the user photo gets lost - Patch existing user photo to the form control so that when saving again it retains old value
        userphoto: this.userphoto ? this.userphoto : '',
        isheadofhousehold: (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personroledetails.actor.intakeservicerequestactor[0] && 
                            this.selectedPersonDetails.personroledetails.actor.intakeservicerequestactor[0].isheadofhousehold)
                            ? this.selectedPersonDetails.personroledetails.actor.intakeservicerequestactor[0].isheadofhousehold : false,
        biologicalmothermarriedsw: personbasicdetails ? personbasicdetails.biologicalmothermarriedsw : null
      });

      this.processRoleValidation();
      if (this.selectedPersonDetails.personbasicdetails) {
        if (this.selectedPersonDetails.personbasicdetails.citizenalenageflag === 1) {
          this.involvedPersonFormGroup.patchValue({
            citizenalenageflag: 1
          });
        } else if (this.selectedPersonDetails.personbasicdetails.citizenalenageflag === 0) {
          this.involvedPersonFormGroup.patchValue({
            citizenalenageflag: 0
          });
        }
      }

      // if (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personroledetails.actor.ishousehold) {
      //   this.involvedPersonFormGroup.patchValue({ householdflag: 1 });
      // }
      // if (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personroledetails.actor.iscollateralcontact) {
      //   this.involvedPersonFormGroup.patchValue({ householdflag: 0 });
      // }

      // if (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personroledetails.actor) {
      //   const roles = [this.selectedPersonDetails.personroledetails.actor.actortype];
      //   this.involvedPersonFormGroup.patchValue({ roles: roles });
      // }

      // if (
      //   this.selectedPersonDetails.personroledetails &&
      //   this.selectedPersonDetails.personroledetails.actor
      // ) {
      //   this.involvedPersonFormGroup.patchValue({
      // Dangerousworker:
      //   this.selectedPersonDetails.personroledetails.actor
      //     .dangerlevel === null
      //     ? 'no'
      //     : this.selectedPersonDetails.personroledetails
      //       .actor.dangerlevel,
      // DangerousWorkerReason: this.selectedPersonDetails
      //   .personroledetails.actor.dangerreason,

      // mentalimpairdetail: this.selectedPersonDetails
      //   .personroledetails.actor.mentalimpairdetail,
      // ismentalillness:
      //   this.selectedPersonDetails.personroledetails.actor
      //     .ismentalillness === null
      //     ? 'no'
      //     : this.selectedPersonDetails.personroledetails
      //       .actor.ismentalillness,
      // mentalillnessdetail: this.selectedPersonDetails
      //   .personroledetails.actor.mentalillnessdetail
      //   });
      // }

      // this.resetRoleTabRadioBtn(this.selectedPersonDetails.personroledetails.actor.iscollateralcontact);
      // tslint:disable-next-line:max-line-length
      if (this.selectedPersonDetails.personbasicdetails
        && Array.isArray(this.selectedPersonDetails.personbasicdetails.personracetypemap)
      ) {
        const parsedRace = this.selectedPersonDetails.personbasicdetails.personracetypemap.map(race => race.racetypekey);
        console.log('parsedRace', parsedRace);
        this.involvedPersonFormGroup.patchValue({ Race: parsedRace });
        this.changerace();
      }
      if (this.selectedPersonDetails.personbasicdetails
        && this.selectedPersonDetails.personbasicdetails.alias
        && this.selectedPersonDetails.personbasicdetails.alias.length) {
          this._personInfoService.aliasList = this.selectedPersonDetails.personbasicdetails.alias;
      } else {
        this._personInfoService.aliasList = [];
      }

      if (this.selectedPersonDetails.personbasicdetails
        && this.selectedPersonDetails.personbasicdetails.personmaritalstatus
        && this.selectedPersonDetails.personbasicdetails.personmaritalstatus.length) {
        this.personmaritalstatusList = this.selectedPersonDetails.personbasicdetails.personmaritalstatus[0];
      }

      if (this.selectedPersonDetails.personbasicdetails
        && this.selectedPersonDetails.personbasicdetails.personrole) {
        this.personroleList = this.selectedPersonDetails.personbasicdetails.personrole;
        this.personRole = this.selectedPersonDetails.personbasicdetails.personrole;
        let roleType = null;
        if (this.personRole.ishouseholdmember === 1) {
          roleType = 'household';
        }
        if (this.personRole.ishouseholdmember === 2) {
          roleType = 'other';
        }
        if (this.personRole.ishouseholdmember === 0) {
          roleType = 'collateral';
        }
        this.involvedPersonFormGroup.patchValue({roletype: roleType});
      }

      if (this.selectedPersonDetails.personbasicdetails
        && this.selectedPersonDetails.personbasicdetails.personspouseaddress
        && this.selectedPersonDetails.personbasicdetails.personspouseaddress.length) {
        this.personspouseaddressList = this.selectedPersonDetails.personbasicdetails.personspouseaddress[0];
      }

      if (this.selectedPersonDetails.personbasicdetails
        && this.selectedPersonDetails.personbasicdetails.personphysicalattribute
        && this.selectedPersonDetails.personbasicdetails.personphysicalattribute.length) {
        const higth = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'Ht');
        const weight = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'Wt');
        const tattoos = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'Tattoo');
        const phyMarks = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'PhyMark');
        let heightin =  higth && higth.length && higth[0].attributevalue ? higth[0].attributevalue : '' ;
        heightin = heightin.split('.');
        let weightpnd =  weight && weight.length && weight[0].attributevalue ? weight[0].attributevalue : '' ;
        weightpnd = weightpnd.split('.');
        this.involvedPersonFormGroup.patchValue({
          heightft: heightin && heightin.length ? heightin[0] : '',
          heightin: heightin && heightin.length ? heightin[1] : '',
          weightpnd: weightpnd && weightpnd.length ? weightpnd[0] : '',
          weightound: weightpnd && weightpnd.length ? weightpnd[1] : '',
          tattoo: tattoos && tattoos.length ? tattoos[0].attributevalue : '',
          PhyMark: phyMarks && phyMarks.length ? phyMarks[0].attributevalue : ''
        });

      }

      if (this.selectedPersonDetails.personbasicdetails &&
        this.selectedPersonDetails.personbasicdetails.personmaritalstatus) {
        const maritalInfo = this.selectedPersonDetails.personbasicdetails.personmaritalstatus;
        const maritalAddressInfo = this.selectedPersonDetails.personbasicdetails.personspouseaddress[0];
        this.involvedPersonFormGroup.patchValue({
          marriageplace: maritalInfo.marriageplace ? maritalInfo.marriageplace : '',
          spousehomenumber: maritalInfo.spousehomenumber ? maritalInfo.spousehomenumber : null,
          spouseofficenumber: maritalInfo.spouseofficenumber ? maritalInfo.spouseofficenumber : null,
          spouseofficeextension: maritalInfo.spouseofficeextension ? maritalInfo.spouseofficeextension : null,
          spouseprefix: maritalInfo.spouseprefix ? maritalInfo.spouseprefix : null,
          spousefirstname: maritalInfo.spousefirstname ? maritalInfo.spousefirstname : null,
          spouselastname: maritalInfo.spouselastname ? maritalInfo.spouselastname : null,
          spousemiddlename: maritalInfo.spousemiddlename ? maritalInfo.spousemiddlename : null,
          spousesuffix: maritalInfo.spousesuffix ? maritalInfo.spousesuffix : null,
          numberofchildren: maritalInfo.numberofchildren ? maritalInfo.numberofchildren : null,
          maritalcomments: maritalInfo.maritalcomments ? maritalInfo.maritalcomments : '',
          maritalstartdate: this._commonDropdownService.getValidDate(maritalInfo.maritalstartdate),
          maritalenddate: this._commonDropdownService.getValidDate(maritalInfo.maritalenddate),
          personmaritalstatusid: maritalInfo.personmaritalstatusid ? maritalInfo.personmaritalstatusid : null,
        });
        if (maritalAddressInfo) {
          this.involvedPersonFormGroup.patchValue({
            marriageplace: maritalInfo.marriageplace ? maritalInfo.marriageplace : '',
            spouseaddress1: maritalAddressInfo.spouseaddress1 ? maritalAddressInfo.spouseaddress1 : '',
            spousecity: maritalAddressInfo.spousecity ? maritalAddressInfo.spousecity : null,
            spousestate: maritalAddressInfo.spousestate ? maritalAddressInfo.spousestate : null,
            spousezipcode: maritalAddressInfo.marriageplace ? maritalAddressInfo.spousezipcode : null,
            spousecounty: maritalAddressInfo.spousecounty ? maritalAddressInfo.spousecounty : null
          });

        }
      }
    }



    
  }

  dangerQuestionChange(ques) {
    const ans = this.involvedPersonFormGroup.getRawValue()[ques];
    if (ques === 'dangerousself' && ans !== 1) {
      this.involvedPersonFormGroup.patchValue({
        dangerousselfreason: '',
      });
    } else if (ques === 'Dangerousworker' && ans !== 1) {
      this.involvedPersonFormGroup.patchValue({
        DangerousWorkerReason: '',
      });
    } else if (ques === 'ismentalillness' && ans !== 1) {
      this.involvedPersonFormGroup.patchValue({
        ismentalillnessReason: '',
      });
    } else if (ques === 'ismentalimpair' && ans !== 1) {
      this.involvedPersonFormGroup.patchValue({
        ismentalimpairReason: '',
      });
    }
  }

  hairColorChange(value) {
    if (value === '1298') {
      this.showOtherColor = true;
    } else {
      this.showOtherColor = false;
    }
  }

  hairTextureChange(value) {
    if (value === '1305') {
      this.showOtherTexture = true;
    } else {
      this.showOtherTexture = false;
    }
  }

  profileUpdateListener() {
    // update here....
    if (this._personInfoService.personInfo) {
      this.updateProfileView(this._personInfoService.personInfo);
    }
    this.profileUpdateSubscription = this._personInfoService.personInfoListener$.subscribe(personInfo => {
      setTimeout(() => {
        if (this._personInfoService.personInfo) {
          this.updateProfileView(personInfo);
        }
      }, 2000);

    });
  }

  getSuggestedAddress() {
    if (this.involvedPersonFormGroup.value.spouseaddress1 &&
      this.involvedPersonFormGroup.value.spouseaddress1.length >= 3) {
      this.suggestAddress();
    }
  }
  suggestAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.involvedPersonFormGroup.value.spouseaddress1,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        NewUrlConfig.EndPoint.Intake.SuggestAddressUrl
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
          this.suggestedAddress$ = result;
        }
        }
      );
  }
  selectedAddress(model) {
    this.involvedPersonFormGroup.patchValue({
      spouseaddress1: model.streetLine ? model.streetLine : '',
      spousecity: model.city ? model.city : '',
      spousestate: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
  };
  this._commonHttpService
      .getSingle(
          {
              method: 'post',
              where: addressInput
          },
          NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
      )
      .subscribe(
          (result) => {
              if (result[0].analysis) {
                this.involvedPersonFormGroup.patchValue({
                    spousezipcode: result[0].components.zipcode ? result[0].components.zipcode : ''
                });
                if (result[0].metadata.countyName) {
                  this._commonHttpService.getArrayList(
                    {
                      nolimit: true,
                      where: { referencetypeid: 306,  mdmcode: this.involvedPersonFormGroup.value.spousestate, description: result[0].metadata.countyName}, method: 'get'
                    },
                    'referencevalues?filter'
                ) .subscribe(
                  (resultresp) => {
                    this.involvedPersonFormGroup.patchValue({
                      spousecounty: resultresp[0].ref_key
                  });

                  }
                );
              }
               }
              } ,
              (error) => {
                  console.log(error);
              }
      );
  }

  toggle(event) {
    if (event) {
      // this.involvedPersonFormGroup.controls['aname'].value = true;

    }

  }

  patchimagedata(imagedata) {
    this.involvedPersonFormGroup.patchValue({
      userphoto: imagedata.s3bucketpathname
    });
  }
  changerace() {
   this.racelist = this.involvedPersonFormGroup.getRawValue().Race.filter(data => data !== 'UN');
   if (this.racelist.length > 0) {
    this.setraceunkown = 'UN';
   } else {
    this.setraceunkown = '';
   }
   let unknownracelist = this.involvedPersonFormGroup.getRawValue().Race.filter(data => data === 'UN');
   if (unknownracelist.length > 0){
      this.isRaceUnknownFlag = true;
    } else {
      this.isRaceUnknownFlag = false;
    } 
  }

  addOrUpdatePerson() {

    if (this.involvedPersonFormGroup.invalid) {
      (<any>$('#validation-list')).modal('show');
      const errorFeilds = this._intakeUtils.findInvalidControls(this.involvedPersonFormGroup);
      this.errorList = [];
      errorFeilds.forEach(field => {
        this.errorList.push(this.getPlacHolderName(field));
      });
      return;
    }
    // For now setting flag on Save button click, should be done on success response of save service call
    this.store['SAVEDONEFLAG'] = true;

    this.involvedPersonFormGroup.controls['personid'].setValue = this._personInfoService.getPersonId();
    const data = this.involvedPersonFormGroup.getRawValue();
    if (this.involvedPersonFormGroup.get('heightft').value || this.involvedPersonFormGroup.get('heightin').value) {
      data.height = this.involvedPersonFormGroup.get('heightft').value + '.' + this.involvedPersonFormGroup.get('heightin').value;
    }
    if (this.involvedPersonFormGroup.get('weightpnd').value || this.involvedPersonFormGroup.get('weightound').value) {
      data.weight = this.involvedPersonFormGroup.get('weightpnd').value + '.' + this.involvedPersonFormGroup.get('weightound').value;
    }

    // console.log(this.rolesList);
    // console.log(data.roles);
    if (data.SSN && this.SSNDuplicateFound) {
      this.showProfileErrorMessage(this.getSSNErrorMessage());
      return false;
    }
    const roles = data.roles;
    let personRoleDetails = [];
    if (this.selectedPersonDetails
      && this.selectedPersonDetails.personbasicdetails
      && this.selectedPersonDetails.personbasicdetails.personrole
      && this.selectedPersonDetails.personbasicdetails.personrole.Personroletype
      && this.selectedPersonDetails.personbasicdetails.personrole.Personroletype.length) {
      personRoleDetails = this.selectedPersonDetails.personbasicdetails.personrole.Personroletype;
    }

    let rolesData = [];

    if (roles && roles.length) {
     rolesData = roles.map((roleKey, index) => {
        if (this.rolesList.indexOf(roleKey) !== -1) {
          const savedRole =
          personRoleDetails.find(role => role.roletype === roleKey );
          return {
            'personroletypeid': savedRole.personroletypeid,
            'personroleid': savedRole.personroleid,
            'roletype': savedRole.roletype,
            'isprimary': index === 0 ? 1 : 0
          };
        } else {
          return {
            'personroletypeid': null,
            'personroleid': null,
            'roletype': roleKey,
            'isprimary':  index === 0 ? 1 : 0
          };
        }
      });
    }


    // For Audit Log
    const objectID = this._navigationUtils.getNavigationInfo().sourceID;
    const objectType = this._navigationUtils.getModuleType();
    data.objectid = objectID;
    data.objecttype = objectType;

    // FIX needed for Multiple roles issue
    data.personroleid = this.existingRoleId;

    if (data.roletype === 'household') {
      data.ishousehold = 1;
    } else if (data.roletype === 'other') {
      data.ishousehold = 2;
    } else {
      data.ishousehold = 0;
    }
    data.iscollateralcontact = (data.roletype && data.roletype === 'collateral') ? 1 : 0;
    // data.roles = rolesData;
    // data.personRole = rolesData;
    data.personRole = this.decidePrimaryRole(rolesData);

    data.alias = this._personInfoService.aliasList;

    if (data.Race && Array.isArray(data.Race)) {
      data.Race = data.Race.map(race => {
        return { 'racetypekey': race };
      });
    } else {
      data.Race = null;
    }

    if (data.citizenalenageflag === 1) {
      data.arnumber = null;
      data.astatus = null;
    }

    this._personInfoService.savePersonDetails(data, this.intakeData).subscribe(response => {
      console.log('.......check', response);
      if (response) {
        // Fix for not getting success alert message as this.selectedPersonDetails is undefined and need more clarification on why this code is needed.
        // if (data.citizenalenageflag === 1) {
        //   this.selectedPersonDetails.personbasicdetails.citizenalenageflag = 1;
        //   this.selectedPersonDetails.personbasicdetails.primarycitizenship = data.primarycitizenship ? data.primarycitizenship : null;
        //   this.selectedPersonDetails.personbasicdetails.secondarycitizenship = data.secondarycitizenship ? data.secondarycitizenship : null;
        //   this.selectedPersonDetails.personbasicdetails.nationality = data.nationality ? data.nationality : null;
        // } else if (data.citizenalenageflag === 1) {
        //   this.selectedPersonDetails.personbasicdetails.citizenalenageflag = 0;
        //   this.selectedPersonDetails.personbasicdetails.primarycitizenship = data.primarycitizenship ? data.primarycitizenship : null;
        //   this.selectedPersonDetails.personbasicdetails.secondarycitizenship = data.secondarycitizenship ? data.secondarycitizenship : null;
        //   this.selectedPersonDetails.personbasicdetails.nationality = data.nationality ? data.nationality : null;
        //   this.selectedPersonDetails.personbasicdetails.primarycitizenship = data.arnumber ? data.arnumber : null;
        //   this.selectedPersonDetails.personbasicdetails.primarycitizenship = data.astatus ? data.astatus : null;
        // }
        const savedPersonID = response.Personid;
        this._personInfoService.updatePersonId(savedPersonID);
        if (response.status !== 'Head of Household Person already added') {
          this._alertService.success(response.status);
          this._personInfoService.personActionListener$.next('UPDATED');
          const savedPersonID = response.Personid;
          this._personInfoService.updatePersonId(savedPersonID);
        } else {
          this._alertService.error(response.status);
        }
        this._personInfoService.getPersonDetails().subscribe(response => {
          console.log(response);
          let personInfo = response;
          this._personInfoService.setPersonInfo(response);
          if (personInfo.personbasicdetails && personInfo.personbasicdetails.dob) {
            this.involvedPersonFormGroup.patchValue({
              Dob: personInfo.personbasicdetails.dob
            });
            this.dobChangeListener();
          }
        });
      }

      this._personInfoService.setPersonDob(data.Dob);
    },
      error => {
        console.log(error);
      }
    );
  }

  // getRoleList() {
  //   console.log('case/intake worker Role itration count', this.ROLE_ITREATION_COUNT);
  //   this._personInfoService.getRoleList().subscribe(data => {
  //     if (data && data.length) {
  //       this.setRoleList(data);
  //     } else {
  //       if (this.ROLE_ITREATION_COUNT < AppConstants.ITRETOR.POLING_COUNT) {
  //         this.ROLE_ITREATION_COUNT++;
  //         this.getRoleList();
  //       }
  //     }

  //   });
  // }

  // setRoleList(data) {
  //   this.roleDropdownItems$ = data.map(res => {
  //     return {
  //       text: res.typedescription,
  //       value: res.actortype,
  //       rolegrp: res.rolegroup
  //     };
  //   });
  // }

  languageType() {

  }



  enableAddAlias(event) {
  }

  loadCounty(state) {
    console.log(state);
    this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
      console.log('cl', countyList);
      this.countyDropDownItems$ = Observable.of(countyList);
    });
  }



  getSelected(item, firstflag) {
    // console.log(item, firstflag, this.selected, this.selected ? this.selected.length : '');

    this.rolecount = this.selected ? this.selected.length : '';

    return item.ref_key;


  }

  invokeValidators(isHouseHold) {
    this.involvedPersonFormGroup.controls['isheadofhousehold'].setValue(false);
    if (isHouseHold) {
      this.involvedPersonFormGroup.controls['Dob'].setValidators(Validators.required);
      this.involvedPersonFormGroup.controls['dangerousself'].setValidators(Validators.required);
      this.involvedPersonFormGroup.controls['ismentalimpair'].setValidators(Validators.required);
      this.involvedPersonFormGroup.controls['Dangerousworker'].setValidators(Validators.required);
      this.involvedPersonFormGroup.controls['ismentalillness'].setValidators(Validators.required);
      this.involvedPersonFormGroup.controls['Dob'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['dangerousself'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['ismentalimpair'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['Dangerousworker'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['ismentalillness'].updateValueAndValidity();
    } else {
      this.involvedPersonFormGroup.controls['Dob'].clearValidators();
      this.involvedPersonFormGroup.controls['dangerousself'].clearValidators();
      this.involvedPersonFormGroup.controls['ismentalimpair'].clearValidators();
      this.involvedPersonFormGroup.controls['Dangerousworker'].clearValidators();
      this.involvedPersonFormGroup.controls['ismentalillness'].clearValidators();
      this.involvedPersonFormGroup.controls['Dob'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['dangerousself'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['ismentalimpair'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['Dangerousworker'].updateValueAndValidity();
      this.involvedPersonFormGroup.controls['ismentalillness'].updateValueAndValidity();
      this.isChildRole = false;
    }
  }

  Falg(event) {
    // let substances: any[] = this.involvedPersonFormGroup.controls['drugexposedtypekey'].value;
    // if(substances){
    //   substances.map(item => {
    //     if(item == 'BOTH'){
    //       this.babySubstanceOther = true;
    //     }
    //   });
    // }
    // console.log(this.involvedPersonFormGroup.controls['substanceClass'].value);
    // this.involvedPersonFormGroup.controls['substanceClass'].enable();
    // this.involvedPersonFormGroup.controls['otherSubstance'].enable();
  }

  ngOnDestroy(): void {
    this.profileUpdateSubscription.unsubscribe();
  }

  // Roles stuff
  selectPrimaryRole(event) {
    console.log(event);
  }


  setupChangeSubscribers() {
    this.involvedPersonFormGroup.controls['drugexposedtypekey'].valueChanges
      .subscribe(() => {
        this.babySubstanceOther = false;
        const substances: any[] = this.involvedPersonFormGroup.controls['drugexposedtypekey'].value;
        if (substances) {
          substances.map(item => {
            if (item === 'BOTH') {
              this.babySubstanceOther = true;
            }
          });
        }
      });
  }

  // ngAfterViewInit() {
  //   setTimeout(() => {

  //       var elem = this.renderer.selectRootElement('#roles');

  //       this.renderer.listen(elem, "focus", () => { console.log('focus') });

  //       this.renderer.listen(elem, "blur", () => { console.log('blur') });

  //       elem.focus();

  //   }, 1000);
  // }

  decidePrimaryRole(roles) {
    if (roles && roles.length) {
      const isChildFound = roles.find(role => role.roletype === 'CHILD');
      const isLGFound = roles.find(role => role.roletype === 'LG');
      const isChildAndLG = (isChildFound && isLGFound) ? true : false;

      if (isChildFound || isLGFound) {
        roles.forEach(role => {
          if (role.roletype === 'CHILD') {
            role.isprimary = 1;
          } else if (role.roletype === 'LG' && !isChildAndLG) {
            role.isprimary = 1; // @Simar: If LG role is present along with CHILD, then only CHILD will be made Primary
          } else {
            role.isprimary = 0;
          }
        });
      }
    }
    const orderedRoles = _.orderBy(roles, ['isprimary'], ['desc']);

    console.log('proccessed primary roles', orderedRoles);
    return orderedRoles;
  }

  // primaryRoleSelected() {
  //   const ans = this.involvedPersonFormGroup.getRawValue()['primaryrole'];
  //   console.log(ans);
  //   if (ans === "1") {
  //     this.involvedPersonFormGroup.controls['roles'].setValue('CHILD');
  //   } else if (ans === "2") {
  //     this.involvedPersonFormGroup.controls['roles'].setValue('LG');
  //   } else {

  //   }
  // }

  SSNchanged() {
    const personInfo = this.involvedPersonFormGroup.getRawValue();
    this.involvedPersonFormGroup.patchValue({
      ssnverified: null
    });
    if (personInfo.SSN) {
      this.checkForSSNExist(personInfo.SSN, true);
    }
  }
  checkForSSNExist(ssnValue, throwError = false) {

    if (ssnValue && ssnValue.length === 9) {
      this._personInfoService.searchPresonWithCritera({ 'ssn': ssnValue }).subscribe(result => {
        console.log('data', result.data);
        if (result.count > 0) {
          result.data = result.data.filter(data => data.source === 'Local');
          if (result.data && result.data.length) {
            this.SSNDuplicateFound = true;
            this.showProfileErrorMessage(this.getSSNErrorMessage());
          }
        } else {
          this.SSNDuplicateFound = false;
        }
      });
    } else {
      this.SSNDuplicateFound = false;
    }



  }

  getSSNErrorMessage() {
    // const personInfo = this.involvedPersonFormGroup.getRawValue();
    return `The SSN entered already exists in CJAMS`;
  }

  showProfileErrorMessage(message: string) {
    this.errorMessage = message;
    (<any>$('#profile-error-message')).modal('show');

  }

  closeErrorMessage() {
    this.errorMessage = null;
    (<any>$('#profile-error-message')).modal('hide');
  }

  goBack() {
    this._dataStoreService.setData(IntakeStoreConstants.NAVIGATE_TO_PERSON, true);
    this._personInfoService.goBack();
  }

  processRoleValidation() {
    const selectedRoles = this.involvedPersonFormGroup.getRawValue().roles;
    console.log('selectedRoles', selectedRoles);
    if (selectedRoles.indexOf('CHILD') !== -1) {
      this.isChildRole = true;
      // D-18392 Remove required
      // this.involvedPersonFormGroup.controls['biologicalmothermarriedsw'].setValidators(Validators.required);
      this.involvedPersonFormGroup.controls['biologicalmothermarriedsw'].updateValueAndValidity();
    } else {
      this.isChildRole = false;
      this.involvedPersonFormGroup.controls['biologicalmothermarriedsw'].clearValidators();
      this.involvedPersonFormGroup.controls['biologicalmothermarriedsw'].updateValueAndValidity();
    }
    if (selectedRoles.indexOf('CHILD') === -1 && selectedRoles.length > 0) {
      this.isheadofhouseholdflag = true;
    } else {
      this.isheadofhouseholdflag = false;
      this.involvedPersonFormGroup.controls['isheadofhousehold'].setValue(false);
    }
    this.checkForSexOffender(selectedRoles);
  }

  /**
   * Checks whether or not to show the role in the 'roles' dropdown
   * returns false for role we don't want to show based on specific conditions
   * @param role the role type key
   */
  checkRoles(role) {

    //Rule 1. If selected roles has Alleged victim then cannot be Alleged maltreator, and vice versa
    let selectedRoles = this.involvedPersonFormGroup.getRawValue().roles;
    selectedRoles = selectedRoles && selectedRoles.length ? selectedRoles : [];
    const isAM = selectedRoles.filter( roleKey => roleKey === 'AM');
    const isAV = selectedRoles.filter( roleKey => roleKey === 'AV');
    if (role === 'AV' && isAM.length && !isAV.length ) {
      return false;
    } else if ( role === 'AM' && isAV.length && !isAM.length) {
      return false;
    } 
    
    //Rule 2. Maltreator role should ONLY be visible for CPS cases
    if (role === 'MALTREATOR' && !this.isCPS) {
      return false;
    }
    
    //Rule 3. Alleged Maltreater should NOT be visible for Service cases
    if(role == 'AM' && this.isServiceCase){
      return false;
    }
    
    //Default rule just show on dropdown, so returning true if none of the above conditions satisfied
    return true;
  }

  checkForSexOffender(roles: any[]) {
    const notSexOffenderRoles = ['AV', 'CHILD', 'OTHERCHILD'];
    if (roles && roles.length) {
     const resultRoles = roles.filter(value => -1 !== notSexOffenderRoles.indexOf(value))
     if (resultRoles && resultRoles.length > 0) {
       this.isNotSexOffender = true;
     } else {
      this.isNotSexOffender = false;
     }
    }
  }
  _keyUp(event: any) {
    const pattern = /^[a-zA-Z0-9]*$/;   
      //let inputChar = String.fromCharCode(event.charCode)
      if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^a-zA-Z0-9]/g, "");
        // invalid character, prevent input
      }
  }
  _onKeyUp(event: any){
    const pattern = /^[a-zA-Z]*$/;
    if(!pattern.test(event.target.value)){
      event.target.value = event.target.value.replace(/[^a-zA-Z]/g, "");
    }
  }
}