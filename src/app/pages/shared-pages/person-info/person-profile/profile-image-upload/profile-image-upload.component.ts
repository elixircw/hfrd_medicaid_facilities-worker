import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { FileError, NgxfUploaderService, UploadStatus } from 'ngxf-uploader';
import { AlertService, AuthService, CommonHttpService, GenericService } from '../../../../../@core/services';
import { AppConfig } from '../../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { config } from '../../../../../../environments/config';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { ActivatedRoute, Router } from '@angular/router';
import { read } from 'fs';

@Component({
  selector: 'profile-image-upload',
  templateUrl: './profile-image-upload.component.html',
  styleUrls: ['./profile-image-upload.component.scss']
})
export class ProfileImageUploadComponent implements OnInit {

  url: string;
  uploadedFile = [];
  isAttachType = '';
  isUploading = false;
  isCate = 'CW-Identity (ID)';
  issubCate= 'CW-Client Photo';
  token: AppUser;
  daNumber: string;
  personid= '';
  attachmenttype= 'case';
  imagefile;
  public imagePath;
  @Output() uploadimagedata: EventEmitter<any> = new EventEmitter<any>();
  @Input() imgURL: any;

  constructor(private _uploadService: NgxfUploaderService, private _alertService: AlertService,
    private _authService: AuthService,
    private route: ActivatedRoute) {
    this.token = this._authService.getCurrentUser();
    this.daNumber = route.snapshot.parent.parent.parent.parent.parent.params['daNumber'];
    if (route.snapshot.params) {
      this.attachmenttype = route.snapshot.params['attachmenttype'] || 'case';
      this.personid = route.snapshot.params['personid'] || '';
      console.log(this.personid);
  }
  }

  ngOnInit() {
    
  }

  uploadFile(file: File | FileError): void {
    this.preview(file);
    console.log(file);

                  const fileExt = file['name'].toLowerCase()
                      .split('.')
                      .pop();
                  if (fileExt === 'jpeg' ||
                  fileExt === 'jpg' ||
                  fileExt === 'png') {
                    this.uploadedFile.push(file);
                        this.uploadedFile[0].attachmenttypekey = 'Document';
                    this.isAttachType = this.uploadedFile[0].attachmenttypekey;
                } else {
                    // tslint:disable-next-line:quotemark
                    this._alertService.error(fileExt + " format can't be uploaded");
                    return;
                }


    const workEnv = config.workEnvironment;
    let uploadUrl = '';
    const dynam  = this.isAttachType + '|' + this.isCate + '|' + this.issubCate ;
    if (workEnv === 'state') {
        uploadUrl =  AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
        + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber + '&' + 'docsInfo=' + dynam + '&attachmenttype=' + this.attachmenttype + '&personid=' + this.personid;
        console.log('state', uploadUrl);
    } else {
        uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
        '&' + 'srno=' + this.daNumber + '&attachmenttype=' + this.attachmenttype + '&personid=' + this.personid;
        console.log('local', uploadUrl);
    }


    this.isUploading = true;
    if (!(file instanceof File)) {
     // this.alertError(file);
      this.isUploading = false;
      return;
    }

    this._uploadService.upload({
      url: uploadUrl,
      headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
      filesKey: ['file'],
                files: this.uploadedFile[0],
                process: true
    }).subscribe(
      (response) => {
        if (response.status) {
            this.uploadedFile[0].percentage = response.percent;
        }
        if ( response.status === UploadStatus.Completed) {
          this.uploadimagedata.emit(response.data);

        }
      },
      (err) => {
        console.log(err);
      },
      () => {
        this.isUploading = false;
        console.log('complete');
      });


  }



  preview(files) {
    console.log(files);
    // if (files.length === 0) { return; }
    let mimeType = files.type;
    if (mimeType.match(/image\/*/) == null) {
      // this.message = "Only images are supported.";
      return;
    }
    let reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files);
    reader.onload = (_event) => {
      console.log(reader.result);
      this.imgURL = reader.result;
    }
  }

  
}
