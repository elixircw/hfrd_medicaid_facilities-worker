import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonProfileRoutingModule } from './person-profile-routing.module';
import { PersonProfileComponent } from './person-profile.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ProfileImageUploadComponent } from './profile-image-upload/profile-image-upload.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { PersonProfileService } from './person-profile.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PersonAlsoKnownAsComponent } from './person-also-known-as/person-also-known-as.component';
// import { MaskPipe } from '../../../../@core/pipes/ssnMask.pipe';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    PersonProfileRoutingModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    HttpClientModule,
    MatTooltipModule,
    SharedPipesModule
  ],
  declarations: [PersonProfileComponent, ProfileImageUploadComponent, PersonAlsoKnownAsComponent],
  providers: [PersonProfileService]
})
export class PersonProfileModule { }
