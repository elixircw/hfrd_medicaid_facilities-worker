import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config'; // need to change the path
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Injectable()
export class PersonProfileService {

  
  constructor(private _commonHttpService: CommonHttpService) { }

  getMaritalStatusList() {
    return this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        order: 'typedescription'
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
        .MartialStatusTypeUrl + '?filter'
    );
  }

  getStateList() {
    return this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
        .StateListUrl + '?filter'
    );
  }
  getCountyList() {
    return this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        order: 'countyname asc'
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
    );
  }

  getGenderList() {
    return this._commonHttpService.getArrayList(
      {
        where: { activeflag: 1 },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
        .GenderTypeUrl + '?filter'
    );
  }

  

  

}
