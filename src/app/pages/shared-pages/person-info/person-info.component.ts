import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonInfoService } from './person-info.service';
import { PersonTabs } from './person-tab-config';
import * as moment from 'moment';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService } from '../../../@core/services';
import { IntakeStoreConstants } from '../../newintake/my-newintake/my-newintake.constants';


@Component({
  selector: 'person-info',
  templateUrl: './person-info.component.html',
  styleUrls: ['./person-info.component.scss']
})
export class PersonInfoComponent implements OnInit, OnDestroy {


  tabs = PersonTabs;
  title: string;
  personInfo: any;
  personAge: number;

  selectedurl: string;
  saveDoneFlag: boolean;
  store: any;

  constructor(private _service: PersonInfoService,
    private route: ActivatedRoute,
    private _router: Router,
    private location: Location,
    private _dataStoreService: DataStoreService) {
      this.store = this._dataStoreService.getCurrentStore();
  }

  ngOnInit() {
    console.log('person infor');
    if(this.location.path() && this.location.path().length > 2) {
      this.selectedurl = this.location.path().split('/')[3] ? this.location.path().split('/')[3] : this.selectedurl;
    }
    this.saveDoneFlag = false;
    this.store['SAVEDONEFLAG'] = this.saveDoneFlag;
    // this._dataStoreService.setData('SAVEDATAFLAG', this.saveDoneFlag);

    this.title = this._service.getTitle();
    if (!this._service.isNew()) {
      this._service.getPersonDetails().subscribe(response => {
        console.log(response);
        this.personInfo = response;
        this._service.setPersonInfo(response);
        if (this.personInfo.personbasicdetails && this.personInfo.personbasicdetails.dob) {
          this.personAge = this.calculateAge(this.personInfo.personbasicdetails.dob ? this.personInfo.personbasicdetails.dob : null);
        }

        this.enableProfileTab();

        // if (this.personAge >= 18 && this.personAge <= 21) {
        //   for ( let i = 0; i < this.tabs.length; i++) {
        //     if (this.tabs[i].name === '18-21') {
        //       this.tabs[i].enable = true;
        //     }
        //   }
        // }
      });
    }
    this._service.personInfoListener$.subscribe(action => {
      this.title = this._service.getTitle();
    });
    this._service.personDobListener$.subscribe(dob => {
      this.personAge = this.calculateAge(dob ? dob : null);
      this.enableProfileTab();
      // if (this.personAge >= 18 && this.personAge <= 21) {
      //     for ( let i = 0; i < this.tabs.length; i++) {
      //       if (this.tabs[i].name === '18-21') {
      //         this.tabs[i].enable = true;
      //       }
      //     }
      //   } else {
      //     for ( let i = 0; i < this.tabs.length; i++) {
      //       if (this.tabs[i].name === '18-21') {
      //         this.tabs[i].enable = false;
      //       }
      //     }
      //   }
    });
  }
  calculateAge(dob) {
    let age = 0;
    if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
      const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
      age = moment().diff(rCDob, 'years');
    }
    return age;
  }

  enableProfileTab() {
    this.tabs.map(data => {
      if (data.name === '18-21') {
        if (this.personAge && this.personAge >= 18 && this.personAge <= 21) {
          data.enable = true;
        } else {
          data.enable = false;
        }
      }

      return {
        active: data.active,
        path: data.path,
        name: data.name,
        enable: data.enable
      };
    });
  }
  goBack() {
    this._dataStoreService.setData(IntakeStoreConstants.NAVIGATE_TO_PERSON, true);
    this._service.goBack();
  }

  ngOnDestroy(): void {
    this._service.personInfo = null;
  }

  tabChanged(url) {
    if (this._service.isNew()) {
      (<any>$('#route-confirm-addnew')).modal('show');
    } else {
      this.selectedurl = url;
      this.saveDoneFlag = this.store['SAVEDONEFLAG'];
      // if save done = true => navigate without popup
      // if save done = false => show confirmation popup
      if (this.saveDoneFlag === false) {
        (<any>$('#route-confirm')).modal('show');
      } else {
        this.routeConfirm();
      }
    }

  }

  routeConfirm() {
    this._router.navigate(['pages/person-info-cw/' + this.selectedurl]);
    // navigated to new page, reset the save done flag
    this.saveDoneFlag = false;
    this.store['SAVEDONEFLAG'] = this.saveDoneFlag;
  }
}
