import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataStoreService } from '../../../../@core/services';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import * as jsPDF from 'jspdf';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'payment-schedule-document',
  templateUrl: './payment-schedule-document.component.html',
  styleUrls: ['./payment-schedule-document.component.scss']
})
export class PaymentScheduleDocumentComponent implements OnInit {


  restiutionDocumentInfo: any;
  downloadInProgress: boolean;
  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
  constructor(private router: Router,
    private route: ActivatedRoute,
    private _datastoreService: DataStoreService) { }

  ngOnInit() {
    (<any>$('#payment-schedule-document')).modal('show');
    this.restiutionDocumentInfo = this._datastoreService.getData(IntakeStoreConstants.restiutionDocumentInfo);
  }

  goBack(): void {
    (<any>$('#payment-schedule-document')).modal('hide');
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  collectivePdfCreator() {
    this.downloadCasePdf('restitiutionPaymentSchedule');
  }

  async downloadCasePdf(element: string) {
    const source = document.getElementById(element);
    const pages = source.getElementsByClassName('pdf-page');
    let pageImages = [];
    for (let i = 0; i < pages.length; i++) {
        // console.log(pages.item(i).getAttribute('data-page-name'));
        const pageName = pages.item(i).getAttribute('data-page-name');
        const isPageEnd = pages.item(i).getAttribute('data-page-end');
        await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
            const img = canvas.toDataURL('image/png');
            pageImages.push(img);
            if (isPageEnd === 'true') {
                this.pdfFiles.push({ fileName: pageName, images: pageImages });
                pageImages = [];
            }
        });
    }
    this.convertImageToPdf();
}

convertImageToPdf() {
    this.pdfFiles.forEach((pdfFile) => {
        const doc = new jsPDF();
        pdfFile.images.forEach((image, index) => {
            doc.addImage(image, 'JPEG', 0, 0);
            if (pdfFile.images.length > index + 1) {
                doc.addPage();
            }
        });
        doc.save(pdfFile.fileName);
    });
    this.goBack();
    this.pdfFiles = [];
    this.downloadInProgress = false;
}

}
