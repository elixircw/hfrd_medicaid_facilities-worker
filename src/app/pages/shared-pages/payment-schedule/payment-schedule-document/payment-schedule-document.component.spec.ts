import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentScheduleDocumentComponent } from './payment-schedule-document.component';

describe('PaymentScheduleDocumentComponent', () => {
  let component: PaymentScheduleDocumentComponent;
  let fixture: ComponentFixture<PaymentScheduleDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentScheduleDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentScheduleDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
