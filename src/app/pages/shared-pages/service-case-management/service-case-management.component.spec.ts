import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceCaseManagementComponent } from './service-case-management.component';

describe('ServiceCaseManagementComponent', () => {
  let component: ServiceCaseManagementComponent;
  let fixture: ComponentFixture<ServiceCaseManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceCaseManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCaseManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
