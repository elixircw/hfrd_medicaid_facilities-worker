import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceCaseManagementComponent } from './service-case-management.component';
import { MatGridListModule, MatCardModule } from '@angular/material';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { FilterPipe } from '../../../@core/pipes/filter.pipe';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    MatCardModule,
    MatGridListModule,
    SharedPipesModule
  ],
  declarations: [ServiceCaseManagementComponent],
  exports:[ServiceCaseManagementComponent]
})
export class ServiceCaseManagementModule { }
