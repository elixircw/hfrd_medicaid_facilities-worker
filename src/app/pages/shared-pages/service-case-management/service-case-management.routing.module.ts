import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceCaseManagementComponent } from './service-case-management.component';
import { CommonModule } from '@angular/common';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MatCardModule, MatGridListModule } from '@angular/material';


const routes: Routes = [{
  path: '',
  component: ServiceCaseManagementComponent
}];

@NgModule({
  imports: [
    CommonModule,
    ServiceCaseManagementRoutingModule,
    FormMaterialModule,
    MatCardModule,
    MatGridListModule
  ],
  exports: [ServiceCaseManagementComponent],
  declarations: [ServiceCaseManagementComponent],
  providers: []
})
export class ServiceCaseManagementRoutingModule { }
