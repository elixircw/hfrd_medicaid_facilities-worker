import { AppConstants } from '../../../@core/common/constants';

export const VendorTabs = [
    {
        id: 'profile',
        title: 'Profile',
        name: 'Vendor Information',
        role: [AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.CASE_WORKER],
        route: 'profile',
        active: false,
        securityKey: 'profile-screen'
    },
    {
        id: 'document',
        title: 'Documents',
        name: 'Documents',
        role: [AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.CASE_WORKER],
        route: 'documents',
        active: false,
        securityKey: 'Document-screen'
    },
    {
        id: 'payment',
        title: 'Payment Information',
        name: 'Payment Information',
        role: [AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.CASE_WORKER],
        route: 'payment',
        active: false,
        securityKey: 'Payment-screen'
    },    
    {
        id: 'Review',
        title: 'Review',
        name: 'Review',
        role: [AppConstants.ROLES.SUPERVISOR],
        route: 'review',
        active: false,
        securityKey: 'Review-screen'
    } 
];


