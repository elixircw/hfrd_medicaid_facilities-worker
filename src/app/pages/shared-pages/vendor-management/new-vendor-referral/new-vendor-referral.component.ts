import { Component, OnInit } from '@angular/core';
import { VendorTabs } from '../vendor-management-tab.config';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService, AuthService, AlertService, CommonHttpService } from '../../../../@core/services';
import { NewUrlConfig } from '../vendor-referral-url.config';
@Component({
  selector: 'new-vendor-referral',
  templateUrl: './new-vendor-referral.component.html',
  styleUrls: ['./new-vendor-referral.component.scss']
})
export class NewVendorReferralComponent implements OnInit {
  tabs = VendorTabs;
  referralId: any;
  user: any;
  isVendorExist: any;
  vendorHead: String = 'New Vendor Request';
  referralDate: Date;
  vendorStatus: String = '';
  vendorId: any = null;
  vendorSubmittedTo: String = '';
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _alertService: AlertService
  ) { }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    this.vendorId = this.route.snapshot.params['id'];
    this.isVendorExist = this._dataStoreService.getData('isVendorExist');
    this.referralId = this._dataStoreService.getData('vendorReferralId');
    const providerId = this._dataStoreService.getData('providerid');
    this.referralDate = this._dataStoreService.getData('referralDate');
    this.vendorStatus = this._dataStoreService.getData('vendorStatus');
    this.vendorSubmittedTo = this._dataStoreService.getData('vendorSubmittedfor');
    if (!this.referralId) {
      this.getVendorInfo();
    }
    if (providerId) {
      this.vendorHead = 'Vendor :#' + providerId;
    }
    this.refreshScrollingTabs();
  }
  getVendorInfo() {
    this._commonHttpService.getArrayList(
      {
          where: { vendorid: this.vendorId },
          nolimit: true,
          method: 'get'
      },
      NewUrlConfig.EndPoint.Vendor.GetVendorInfoUrl
    ).subscribe((response) => {
      console.log('response : ', response );
      if (response && response.length) {
        this.referralId = response[0].vendorid;
        this.referralDate = response[0].create_ts;
        this.vendorStatus = response[0].status;
        this.vendorSubmittedTo = response[0].submittedfor;
      }
    },
    (error) => {
      this._alertService.error('Unable to get vendor info');
    });
  }
  refreshScrollingTabs() {
    const __this = this;
    if (this.user && this.user.role && this.user.role.name === 'field') {
      this.tabs = this.tabs.filter(tab => tab.role.indexOf('field') !== -1);
    }
    let tab = this.getCurrentTabDetails();
    if (!tab) {
      tab = this.tabs[0];
    }
    const tabsLiContent = this.tabs.map(function (tab) {
      return '<li role="presentation" title="' + tab.title + '" data-trigger="hover" class="custom-li" routerLinkActive="active"></li>';
    });
    // this.tabs.forEach(tab => tab.active = false);
    const tabsPostProcessors = this.tabs.map(function (tab) {
      return function ($li, $a) {
        $a.attr('href', '');
        if (tab.active) {
          $a.attr('class', 'active');
        }
        $a.click(function () {
          __this.isVendorExist = __this._dataStoreService.getData('isVendorExist');
          __this.tabs.forEach(filteredTab => {
            filteredTab.active = false;
          });
          tab.active = true;
          if (!__this.isVendorExist && (tab.route === 'payment' || tab.route === 'documents')) {
            __this._alertService.warn('Please complete profile info');
            return false;
          }

          __this._router.navigate([tab.route], { relativeTo: __this.route });

        });
      };
    });
    (<any>$('#vendor-tabs')).html('');
    (<any>$('#vendor-tabs')).scrollingTabs({
      tabs: __this.tabs,
      propPaneId: 'path', // optional - pass in default value for demo purposes
      propTitle: 'name',
      disableScrollArrowsOnFullyScrolled: true,
      tabsLiContent: tabsLiContent,
      tabsPostProcessors: tabsPostProcessors,
      scrollToActiveTab: true,
      tabClickHandler: function (e) {
        $(__this).addClass('active');
        $(__this).parent().addClass('active');
      }
    });
  }

  private getCurrentTabDetails() {
    this.tabs.forEach(tab => tab.active = false);
    const urlSegments = this._router.url.split('/');
    let tab: any;
    this.tabs.forEach(tabObj => {
      if (urlSegments.includes(tabObj.route)) {
        tab = tabObj;
        tab.active = true;
        return;
      }
    });
    return tab;
  }
}

