import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DataStoreService, CommonHttpService, AlertService, AuthService, CommonDropdownsService } from '../../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  reviewFormGroup: FormGroup;
  vendorId: any = null;
  vendorApplicantId: any = null;
  user: any;
  statusDropdownItems$: any;
  providerId: any;
  isManageVendor: Boolean = false;
  isView: Boolean = false;
  isApprove: Boolean = false;
  acknowledgeMessage: String = '';

  constructor(
    private _formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _commonDropdownService: CommonDropdownsService,
    ) { }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    const isVendorExist = this._dataStoreService.getData('isVendorExist');
    this.vendorId = this._route.parent.snapshot.params['id'];
    this.vendorApplicantId = this._dataStoreService.getData('vendorApplicantId');
    this.isManageVendor = this._dataStoreService.getData('isManageVendor');
    this.isView = this._dataStoreService.getData('isView');
    if (!isVendorExist || this.user.role.name !== 'FS') {
      this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.vendorId + '/profile']);
    }

    this.reviewFormGroup = this._formBuilder.group({
      vendorapplicantid: null,
      vendorid: null,
      ref_key: ['', Validators.required],
      approvalcomments: [''],
      narrative: ['']
    });

    this.patchReview();

    if (this.isView === true) {
      this.reviewFormGroup.disable();
    }
    this.statusDropdownItems$ = this._commonDropdownService.getPickListByName('vendordecision');
    this.statusDropdownItems$ = this.statusDropdownItems$.map((data) => {
      data.sort((a, b) => {
        return a.ref_key < b.ref_key ? -1 : 1;
      });
      return data;
    });
  }

  patchReview() {
   const decision = this._dataStoreService.getData('decision');
   const comments = this._dataStoreService.getData('reviewComments');
   const narrative = this._dataStoreService.getData('reviewNarrative');
    this.reviewFormGroup.patchValue({
      ref_key: (decision) ? decision : '',
      approvalcomments: (comments) ? comments  : '',
      narrative: (narrative) ? narrative : ''
    });
  }

  submitReview() {
    let url = '';
    if (this.isManageVendor) {
      url = 'tb_vendor_applicant/managevendor';
    } else {
      url = 'tb_vendor_applicant/supervisordecision';
    }

    const data = this.reviewFormGroup.value;
    data.vendorapplicantid = this.vendorApplicantId;
    data.vendorid = this.vendorId;
    if (data.ref_key === 'APR') {
      this.isApprove = true;
      this.acknowledgeMessage = 'This Vendor has been created successfully.';
    } else {
      this.isApprove = false;
      this.acknowledgeMessage = 'This Vendor has been rejected successfully.';
    }
    this._commonHttpService.create(
      data,
      url
    ).subscribe(
      (response) => {
        if (response && response.length) {
          if (this.isManageVendor && data.ref_key === 'APR') {
            this.acknowledgeMessage = 'This Vendor has been updated successfully.';
            this.providerId = (response[0].managevendorapplicant) ? response[0].managevendorapplicant : '';
          } else if (!this.isManageVendor && data.ref_key === 'APR'){
            this.acknowledgeMessage = 'This Vendor has been created successfully.';
            this.providerId = (response[0].updatesupervisordecision) ? response[0].updatesupervisordecision : '';
          }
          // this._alertService.success('Review Submitted successfully!');
          (<any>$('#approve-vendor-ackmt')).modal('show');        }
      },
      (error) => {
        this._alertService.error('Unable to submit review');
    });
  }

  approveVendorAcknowledged() {
    (<any>$('#approve-vendor-ackmt')).modal('hide');
    this._router.navigate(['/pages/vendor-management/existing-vendor-referral']);
  }
}

