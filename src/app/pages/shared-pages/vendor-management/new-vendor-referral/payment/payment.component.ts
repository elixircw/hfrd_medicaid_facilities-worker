import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DataStoreService, CommonHttpService, AlertService, AuthService, CommonDropdownsService } from '../../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { NewUrlConfig } from '../../vendor-referral-url.config';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
@Component({
  selector: 'payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  paymentFormGroup: FormGroup;
  taskIdType$: Observable<any[]>;
  countyList$: Observable<any[]>;
  stateList$: Observable<any[]>;
  vendorId: any = null;
  vendorApplicantId: any = null;
  user: any;
  addressList: any[];
  addressFormGroup: FormGroup;
  suggestedAddress$: Observable<any[]>;
  vendorCurrentAddress: Boolean = false;
  vendorPaymentAddress: Boolean = false;
  currentAddress: any = {};
  paymentAddress: any = {};
  isCurrentAddress: Boolean = false;
  isMedicalProvider: Boolean = false;
  isManageVendor: Boolean = false;
  isView: Boolean = false;
  recognizing = false;
  action: String = '';
  orgName: String = '';
  jurisdiction: String = '';
  jurisdictionDesc: String = '';
  taxid: String = '';
  notification: string;
  dupTaxId: any[] = [];
  payAddCountyName: String = '';
  supervisorDetail: any[] = [];
  hasTaxid: Boolean = false;
  selectedPerson: any;
  constructor(
    private _formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _commonHttpService: CommonHttpService,
    private _commonDropdownService: CommonDropdownsService,
    private _alertService: AlertService,
    private _authService: AuthService,
    ) { }

    ngOnInit() {
      this.user = this._authService.getCurrentUser();
      const isVendorExist = this._dataStoreService.getData('isVendorExist');
      this.isManageVendor = this._dataStoreService.getData('isManageVendor');
      this.isView = this._dataStoreService.getData('isView');
    this.vendorId = this._route.parent.snapshot.params['id'];
    this.vendorApplicantId = this._dataStoreService.getData('vendorApplicantId');
    if (!isVendorExist) {
      this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.vendorId + '/profile']);
    }

    this.paymentFormGroup = this._formBuilder.group({
      vendorapplicantid: null,
      taxidtype: ['', Validators.required],
      taxid: ['', Validators.required],
      is1099indicator: [''],
      medlicense: [''],
      ismedicalaidprov: false,
      issame: ['', Validators.required],
      speciality: [''],
      narrative: [''],
      adr_1: [''],
      adr_2: [''],
      adr_city_nm: [''],
      adr_state_cd: [''],
      adr_county_cd: [''],
      adr_start_dt: [null],
      adr_end_dt: [null],
      adr_zip_no: [''],
      jurisdiction: [null]
    });
    this.loadDropDowns();
    this.initiateAddressFormGroup();
    if (this.isView === true) {
      this.paymentFormGroup.disable();
      this.addressFormGroup.disable();
    }
    this.getPaymentInfo();
    this.getAddressList();
  }

  private initiateAddressFormGroup() {
    this.addressFormGroup = this._formBuilder.group({
      vendoraddressid: [null],
      address: ['', [Validators.required]],
      address2: [''],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      county: ['', [Validators.required]],
      zipcode: ['', [Validators.required]],
      addresstype: ['3356', [Validators.required]],
      addressstartdate: [''],
      addressenddate: ['']
    });
  } 

  private loadDropDowns() {
    this.taskIdType$ = this._commonDropdownService.getPickList('216');
    this.stateList$ = this._commonDropdownService.getPickListByName('state');
    this.countyList$ = this._commonDropdownService.getPickList('328');
  }

  addPayment(type?: string) {
    const data = this.paymentFormGroup.value;
    data.vendorapplicantid = this.vendorApplicantId;
    data.jursidiction = this.jurisdiction ? this.jurisdiction : null;
    if (data.taxid && data.taxid.length !== 9 && type !== 'draft') {
      this._alertService.error('Tax ID format(00-0000000) is wrong');
      return;
    } else {
      this._commonHttpService.create(
        data,
        NewUrlConfig.EndPoint.Vendor.AddPaymentInfoUrl
      ).subscribe(
        (response) => {
          if (response) {
            if (type !== 'draft') {
              this._alertService.success('Payment info added/updated successfully!', true);
              this.getPaymentInfo();
              this._router.navigate(['/pages/vendor-management/existing-vendor-referral']);
            }
          }
        },
        (error) => {
          this._alertService.error('Unable to save payment');
        });
      }
  }

  getAddress(value) {
    if (!this.paymentFormGroup.value.taxid) {
      this._alertService.error('Tax Id Required');
    } else {
      if (!value) {
        this.openAddAddressModal();
        this.vendorCurrentAddress = false;
      } else {
        if (this.isCurrentAddress === true ) {
          this.vendorPaymentAddress = false;
          this.openViewAddress();
        } else {
          this._alertService.error('Business Address Not Added Yet');
        }
        (<any>$('#vendor-address')).modal('hide');
      }
    }
  }

  getAddressList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: {
          vendorapplicantid: this.vendorApplicantId
        },
        page: 1,
        limit: 20,
      }, NewUrlConfig.EndPoint.Vendor.GetVendorAddressListUrl+ '?filter'
    ).subscribe(
      (response) => {
        this.addressList = [];
        this.currentAddress = {};
        this.isCurrentAddress = false;
        if (response.length) {
          this.addressList = response;
          this.addressList.map(address => {
            if ((address.adr_end_dt === '' || address.adr_end_dt === null) && (address.adr_1 !== '' && address.adr_1 !== null)) {
              this.isCurrentAddress = true;
              this.currentAddress['adr_1'] = address.adr_1;
              this.currentAddress['adr_2'] = address.adr_2;
              this.currentAddress['adr_city_nm'] = address.adr_city_nm;
              this.currentAddress['adr_county_cd'] = address.adr_county_cd;
              this.currentAddress['adr_county_nm'] = address.adr_county_nm;
              this.currentAddress['adr_state_cd'] = address.adr_state_cd;
              this.currentAddress['adr_zip_no'] = address.adr_zip_no;
              this.currentAddress['adr_start_dt'] = address.adr_start_dt;
            }
          });
        }
    });
  }

  addVendorAddress() {
    const data = this.addressFormGroup.getRawValue();
    this.paymentAddress = {};
    this.paymentFormGroup.patchValue({
      adr_1: data.address,
      adr_2: data.address2,
      adr_city_nm: data.city,
      adr_state_cd: data.state,
      adr_county_cd: data.county,
      adr_zip_no  : data.zipcode,
      adr_start_dt: data.addressstartdate,
      adr_end_dt: data.addressenddate
    });
    this.paymentAddress['adr_1'] = data.address;
    this.paymentAddress['adr_2'] = data.address2;
    this.paymentAddress['adr_city_nm'] = data.city;
    this.paymentAddress['adr_county_cd'] = data.county;
    this.paymentAddress['adr_county_nm'] = this.payAddCountyName;
    this.paymentAddress['adr_state_cd'] = data.state;
    this.paymentAddress['adr_zip_no'] = data.zipcode;
    this.paymentAddress['adr_start_dt'] = data.addressstartdate;
    this.paymentAddress['adr_end_dt'] = data.addressenddate;
    this.vendorPaymentAddress = true;
    (<any>$('#vendor-address')).modal('hide');
  }

  openAddAddressModal() {
    this.resetForm();
    (<any>$('#vendor-address')).modal('show');
  }
  openViewAddress() {
    this.vendorCurrentAddress = true;
  }
  resetForm() {
    this.addressFormGroup.reset();
    this.addressFormGroup.patchValue({
      addresstype: '3356'
    });
  }
  getSuggestedAddress() {
      this.suggestAddress();
  }
  suggestAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.addressFormGroup.value.address,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        NewUrlConfig.EndPoint.Vendor.SuggestAddressUrl
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
            this.suggestedAddress$ = result;
          }
        }
      );
  }
  selectedAddress(model) {
    this.addressFormGroup.patchValue({
      address: model.streetLine ? model.streetLine : '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        NewUrlConfig.EndPoint.Vendor.ValidateAddressUrl
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.addressFormGroup.patchValue({
              zipcode: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.addressFormGroup.value.state, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  this.addressFormGroup.patchValue({
                    county: resultresp[0].ref_key
                  });

                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  getPaymentInfo() {
    this._commonHttpService.getArrayList(
      {
          where: { vendorapplicantid: this.vendorApplicantId },
          nolimit: true,
          method: 'get'
      },
      NewUrlConfig.EndPoint.Vendor.GetPaymentInfoUrl
    ).subscribe((response) => {
      if (response && response.length) {
        const paymentData = response[0];
        this.paymentFormGroup.patchValue(paymentData);
        if (paymentData.ismedicalaidprov) {
          this.isMedicalProvider = true;
        }
        if (paymentData.org_nm) {
          this.orgName = paymentData.org_nm;
        }
        this.jurisdiction = (paymentData.jurisdiction) ? paymentData.jurisdiction : null;
        this.jurisdictionDesc = (paymentData.jurisdiction_desc) ? paymentData.jurisdiction_desc : null;

        if (paymentData.address && paymentData.address.length) {
          if (paymentData.address[0].adr_1 !== '' && paymentData.address[0].adr_1 !== null) {
            if (paymentData.issame === true) {
              this.currentAddress = {};
              this.currentAddress['adr_1'] = paymentData.address[0].adr_1;
              this.currentAddress['adr_2'] = paymentData.address[0].adr_2;
              this.currentAddress['adr_city_nm'] = paymentData.address[0].adr_city_nm;
              this.currentAddress['adr_county_cd'] = paymentData.address[0].adr_county_cd;
              this.currentAddress['adr_county_nm'] = paymentData.address[0].adr_county_nm;
              this.currentAddress['adr_state_cd'] = paymentData.address[0].adr_state_cd;
              this.currentAddress['adr_zip_no'] = paymentData.address[0].adr_zip_no;
              this.currentAddress['adr_start_dt'] = paymentData.address[0].adr_start_dt;
              this.currentAddress['adr_end_dt'] = paymentData.address[0].adr_end_dt;
              this.vendorCurrentAddress = true;
            } else if (paymentData.issame === false) {
              this.paymentAddress = {};
              this.paymentAddress['adr_1'] = paymentData.address[0].adr_1;
              this.paymentAddress['adr_2'] = paymentData.address[0].adr_2;
              this.paymentAddress['adr_city_nm'] = paymentData.address[0].adr_city_nm;
              this.paymentAddress['adr_county_cd'] = paymentData.address[0].adr_county_cd;
              this.paymentAddress['adr_county_nm'] = paymentData.address[0].adr_county_nm;
              this.paymentAddress['adr_state_cd'] = paymentData.address[0].adr_state_cd;
              this.paymentAddress['adr_zip_no'] = paymentData.address[0].adr_zip_no;
              this.paymentAddress['adr_start_dt'] = paymentData.address[0].adr_start_dt;
              this.paymentAddress['adr_end_dt'] = paymentData.address[0].adr_end_dt;
              this.vendorPaymentAddress = true;
            } else {
              this.currentAddress = {};
              this.paymentAddress = {};
              this.vendorCurrentAddress = false;
              this.vendorPaymentAddress = false;
            }
          }
        }
      }
    },
    (error) => {
      this._alertService.error('Unable to get profile info');
    });
  }

  submitForApproval() {
    if ( this.paymentFormGroup.invalid) {
      this._alertService.error('Please fill the mandatory fields before approval');
      return false;
    }
   const data = this.paymentFormGroup.value;
    data.vendorapplicantid = this.vendorApplicantId;
    data.vendorid = this.vendorId;
    data.jurisdiction = (this.jurisdiction) ? this.jurisdiction : null;
    data.tosecurityuserid = (this.selectedPerson && this.selectedPerson.securityuserid) ? this.selectedPerson.securityuserid : null;
    if (data.taxid && data.taxid.length !== 9) {
      this._alertService.error('Tax ID format(00-0000000) is wrong');
      return;
    }
    this._commonHttpService.create(
      data,
      NewUrlConfig.EndPoint.Vendor.SubmitVendorApprovalUrl
    ).subscribe(
      (response) => {
        if (response && response.length) {
          this.supervisorDetail = response;
          //this.supervisorDetail = ''; // approval[0].displayname + ' (' + approval[0].title + ' - ' + approval[0].teamtypekey + ' UNIT)';
          // if (response.length > 1) {
          //   approval.forEach(element => {
          //     this.supervisorDetail += element.displayname + ' (' + element.title + ' - ' + element.teamtypekey + ' UNIT), ';
          //   });
          // }
          //(<any>$('#approval-submit-popup')).modal('show');
          this._alertService.success('Approval sent successfully!', true);
          this._router.navigate(['/pages/vendor-management/existing-vendor-referral']);
        }
      },
      (error) => {
        this._alertService.error('Unable to save submit approval');
      });
  }

  navigateToDashboard() {
    (<any>$('#approval-submit-popup')).modal('hide');
    this._router.navigate(['/pages/vendor-management/existing-vendor-referral']);
  }

  getMedicalProvider(checked) {
    this.isMedicalProvider = (checked) ? true : false;
  }

  taxIdCheck(action) {
    this.action = '';
    this.taxid = '';
    this.dupTaxId = [];
    this.taxid = this.paymentFormGroup.getRawValue().taxid;
    this.action = action;
    if (this.taxid.length === 9) {
      this._commonHttpService.getArrayList(
        {
            where: { taxid: this.taxid, vendorid: this.vendorId },
            method: 'get'
        },
        NewUrlConfig.EndPoint.Vendor.CheckTaxIdUrl
      ).subscribe((response) => {
        if (response.length > 0) {
          this.dupTaxId = response;
          // console.log(this.dupTaxId);
          (<any>$('#taxid-dup-popup')).modal('show');
        } else {
          if (this.action === 'save') {
            this.addPayment();
          } else {
            this.getRoutingUser();
            // this.submitForApproval();
          }
        }
      },
      (error) => {
        this._alertService.error('Unable to check');
      });
    } else {
      this._alertService.error('Tax ID format(00-0000000) is wrong');
    }
  }
  submitPayment() {
    (<any>$('#taxid-dup-popup')).modal('hide');
    if (this.action === 'save') {
      this.addPayment();
    } else {
      this.submitForApproval();
    }
    this.action = '';
    this.taxid = '';
    this.dupTaxId = [];
  }
  ngOnDestroy() {
    this.addPayment('draft');
  }
  getCountyName(event, item) {
    if (item) {
      this.payAddCountyName = item.description_tx;
    }
  }
  resetPaymentAddress() {
    this.paymentFormGroup.controls['issame'].reset();
  }

  selectPerson(row) {
    console.log(row);
    this.selectedPerson = row;
  }

  getRoutingUser() {
    (<any>$('#taxid-dup-popup')).modal('hide');
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: {
          vendorapplicantid: this.vendorApplicantId,
          vendorid: this.vendorId
        },
        page: 1,
        limit: 20,
      }, NewUrlConfig.EndPoint.Vendor.GetVendorApprovalList + '?filter'
      ).subscribe(result => {
        if (result && result.length) {
          (<any>$('#approval-submit-popup')).modal('show');
          this.supervisorDetail = result;
          this.supervisorDetail = this.supervisorDetail.filter(
            users => users.userid !== this._authService.getCurrentUser().user.securityusersid
          );
        }
      });

    // if ((this.user.role.name === 'LDSS_SUPERVISOR' ||  this.canproviderapproval === true ) && assigntype === 'SUBMIT-REVIEW') {
    //   this.eventCode = 'PRWS';
    //   this.assignNewUser();
    // }
  }

}
