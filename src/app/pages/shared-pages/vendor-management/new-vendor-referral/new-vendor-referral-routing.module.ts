import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { PaymentComponent } from './payment/payment.component';
import { AddressComponent } from './address/address.component';
import { ContactComponent } from './contact/contact.component';
import { NewVendorReferralComponent } from './new-vendor-referral.component';
import { ReviewComponent } from './review/review.component';
import { VendorReferralDocumentsComponent } from './vendor-referral-documents/vendor-referral-documents.component';

const routes: Routes = [
  {
    path: '',
    component: NewVendorReferralComponent,
    children: [
    ]
  },
  {
    path: ':id',
    component: NewVendorReferralComponent,
    children : [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full',
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'address',
        component: AddressComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'payment',
        component: PaymentComponent
      },
      {
        path: 'documents',
        component: VendorReferralDocumentsComponent
      },
      {
        path: 'review',
        component: ReviewComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewVendorReferralRoutingModule { }

