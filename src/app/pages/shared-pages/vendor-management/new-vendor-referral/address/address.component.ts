import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DataStoreService, AlertService, CommonDropdownsService, CommonHttpService } from '../../../../../@core/services';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AddressDetailsService } from '../../../person-info/address-details/address-details.service';
import { PersonInfoService } from '../../../person-info/person-info.service';
import { NewUrlConfig } from '../../vendor-referral-url.config';

@Component({
  selector: 'address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})

export class AddressComponent implements OnInit {
  suggestedAddress$: Observable<any[]>;
  addressFormGroup: FormGroup;
  vendorApplicantId: any;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  addresstypeDropdownItems$: Observable<DropdownModel[]>;
  addressTypes: Observable<any[]>;
  minDate = new Date();
  maxDate = new Date();
  addeditAddress = 'Add New';
  deleteId = null;
  addressMode: String;
  addressList = [];
  vendorAddressId: any;
  taskIdType$: Observable<any[]>;
  countyList$: Observable<any[]>;
  stateList$: Observable<any[]>;
  addBtn = true;
  @Output() addflag: EventEmitter<Boolean> = new EventEmitter();
  enableReason = false;
  isView: Boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _commonDropdownService: CommonDropdownsService,
    private _alertservices: AlertService,
    private _dataStoreService: DataStoreService
  ) {
  }

  ngOnInit() {
    this.addressMode = 'add';
    this.isView = this._dataStoreService.getData('isView');
    this.vendorApplicantId =  this._dataStoreService.getData('vendorApplicantId') ?
                              this._dataStoreService.getData('vendorApplicantId') : '';
    console.log(this.vendorApplicantId);
    this.loadDropDowns();
    this.initiateFormGroup();
    this.getAddressList();
  }
  getAddressList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: {
          vendorapplicantid: this.vendorApplicantId
        },
        page: 1,
        limit: 20,
      }, NewUrlConfig.EndPoint.Vendor.GetVendorAddressListUrl+ '?filter'
    ).subscribe(
      (response) => {
        this.addressList = [];
        this.addBtn = true;
        if (response.length) {
          this.addressList = response;
          this.addressList.map(address => {
            if (address.adr_end_dt === '' || address.adr_end_dt === null) {
              this.addBtn = false;
            }
          });
        }
    });
  }
  resetAddressForm() {
    this.addeditAddress = 'Add New';
    this.addressMode = 'add';
    (<any>$('#vendor-address')).modal('show');
    this.addressFormGroup.reset();
    this.addressFormGroup.patchValue({
      adr_type_key: '35'
    });
  }

  editAddress(data) {
    this.addeditAddress = 'Edit';
    this.addressMode = 'edit';
    this.addressFormGroup.patchValue(data);
    this.vendorAddressId = data.vendoraddressid;
  }

  private initiateFormGroup() {
    this.addressFormGroup = this._formBuilder.group({
      vendoraddressid: [null],
      adr_1: ['', [Validators.required]],
      adr_2: [''],
      adr_city_nm: ['', [Validators.required]],
      adr_county_cd: ['', [Validators.required]],
      adr_state_cd: ['', [Validators.required]],
      adr_type_key: ['35', [Validators.required]],
      adr_zip_no: ['', [Validators.required]],
      adr_start_dt: [null],
      adr_end_dt: [null]
    });
  }
  private loadDropDowns() {
    this.taskIdType$ = this._commonDropdownService.getPickList('216');
    this.stateList$ = this._commonDropdownService.getPickListByName('state');
    this.countyList$ = this._commonDropdownService.getPickList('328');
  }

  addVendorAddress() {
    const vendorAddressId = (this.addressMode === 'add') ? null : this.vendorAddressId;
    const vendorAddressForm = this.addressFormGroup.getRawValue();
    const addupdateAddress = {
      vendoraddressid: vendorAddressId,
        vendorapplicantid: this.vendorApplicantId,
        adr_1: vendorAddressForm.adr_1,
        adr_2: vendorAddressForm.adr_2,
        adr_city_nm : vendorAddressForm.adr_city_nm,
        adr_county_cd  : vendorAddressForm.adr_county_cd,
        adr_state_cd  : vendorAddressForm.adr_state_cd,
        adr_type_key  : vendorAddressForm.adr_type_key,
        adr_zip_no  : vendorAddressForm.adr_zip_no,
        adr_start_dt  : vendorAddressForm.adr_start_dt,
        adr_end_dt  : vendorAddressForm.adr_end_dt ? vendorAddressForm.adr_end_dt : null,
        ispaymentaddress : false
    };

    this._commonHttpService.create(
      addupdateAddress,
      NewUrlConfig.EndPoint.Vendor.AddAddressUrl
    ).subscribe(
      result => {
        this._alertservices.success('Address added/updated successfully');
        (<any>$('#vendor-address')).modal('hide');
        this.addressMode = 'add';
        this.getAddressList();
      },
      error => {
        this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      });
  }
  confirmDelete(id) {
    this.deleteId = id;
    console.log(this.deleteId );
    (<any>$('#delete-popup')).modal('show');
  }

  declineDelete() {
    this.deleteId = null;
    (<any>$('#delete-popup')).modal('hide');
  }

  deleteVendorAddress() {
    if (this.deleteId) {
      this._commonHttpService.endpointUrl = 'Tb_vendor_addresses';
      this._commonHttpService.patch(this.deleteId, {delete_sw: 'Y'})
        .subscribe((response) => {
          (<any>$('#delete-popup')).modal('hide');
          this._alertservices.success('Address deleted successfully..');
          this.getAddressList();
        },
        error => {
          this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    } else {
      (<any>$('#delete-popup')).modal('hide');
    }
  }


  loadCounty(state) {
    console.log(state);
    this._commonDropdownService.getPickListByMdmcode(state.ref_key).subscribe(countyList => {
      this.countyDropDownItems$ = Observable.of(countyList);
    });
  }

  getSuggestedAddress() {
    this.suggestAddress();
}
suggestAddress() {
  this._commonHttpService
    .getArrayListWithNullCheck(
      {
        method: 'post',
        where: {
          prefix: this.addressFormGroup.value.adr_1,
          cityFilter: '',
          stateFilter: '',
          geolocate: '',
          geolocate_precision: '',
          prefer_ratio: 0.66,
          suggestions: 25,
          prefer: 'MD'
        }
      },
      NewUrlConfig.EndPoint.Vendor.SuggestAddressUrl
    ).subscribe(
      (result: any) => {
        if (result.length > 0) {
          this.suggestedAddress$ = result;
        }
      }
    );
  }
  selectedAddress(model) {
    this.addressFormGroup.patchValue({
      adr_1: model.streetLine ? model.streetLine : '',
      adr_city_nm: model.city ? model.city : '',
      adr_state_cd: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        NewUrlConfig.EndPoint.Vendor.ValidateAddressUrl
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.addressFormGroup.patchValue({
              adr_zip_no: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.addressFormGroup.value.adr_state_cd, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  this.addressFormGroup.patchValue({
                    adr_county_cd: resultresp[0].ref_key
                  });

                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

}
