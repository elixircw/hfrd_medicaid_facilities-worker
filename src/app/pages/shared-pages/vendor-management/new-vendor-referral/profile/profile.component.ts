import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { CommonHttpService, AlertService, CommonDropdownsService, DataStoreService } from '../../../../../@core/services';
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../vendor-referral-url.config';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileFormGroup: FormGroup;
  maxDate = new Date();
  services: FormArray;
  prefixDropdownItems$: Observable<any[]>;
  suffixDropdownItems$: Observable<any[]>;
  vendorServices$: Observable<DropdownModel[]>;
  countyList$: Observable<DropdownModel[]>;
  vendorApplicantId: any = null;
  vendorId: any = null;
  isManageVendor: boolean = false;
  isView: Boolean = false;
  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _commonDropdownService: CommonDropdownsService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _dataStoreService: DataStoreService
  ) { }

  ngOnInit() {
    this.isManageVendor = this._dataStoreService.getData('isManageVendor');
    this.vendorId = this._route.parent.snapshot.params['id'];
    this.isView = this._dataStoreService.getData('isView');
    this.initiateFormGroup();
    if (this.isView === true) {
      this.profileFormGroup.disable();
    }
    this.loadDropDown();
    this.getProfileInfoById();
   // this.listenProfile();
  }

  // listenProfile() {
  //   this.profileFormGroup.valueChanges.subscribe((res) => {
  //     if (this.profileFormGroup.valid && this.vendorApplicantId == null) {
  //       this.addProfile(true);
  //     }
  //   });
  // }

  private loadDropDown() {
    this.prefixDropdownItems$ = this._commonDropdownService.getPickListByName('prefix');
    this.suffixDropdownItems$ = this._commonDropdownService.getPickListByName('suffix');
    this.loadJurisdictioDropDown();
    const source = this._commonHttpService.getArrayList(
        {
            method: 'get',
            nolimit: true
        },
        NewUrlConfig.EndPoint.Vendor.VendorServiceUrl
      ).map((result) => {

          return {  servicetypeid: result['UserToken'].map(
          (res) =>
              new DropdownModel({
                  text: res.service_nm,
                  value: res.service_id
              })
        ),  };
      }).share();
    this.vendorServices$ = source.pluck('servicetypeid');
  }

  
  private initiateFormGroup() {
    this.profileFormGroup = this._formBuilder.group({
      vendorapplicantid: [null],
      vendorid: [null],
      org_nm: [null, Validators.required],
      jurisdiction: [null, Validators.required],
      primary_last_nm: [null],
      primary_first_nm: [null],
      primary_middle_nm: [null],
      primary_prefix_cd: [null],
      primary_suffix_cd: [null],
      isprimaryadmin: [null],
      admin_last_nm: [null],
      admin_first_nm: [null],
      admin_middle_nm: [null],
      admin_prefix_cd: [null],
      admin_suffix_cd: [null],
      regularfrom: [null],
      regularto: [null],
      status: [null],
    });
    this.profileFormGroup.addControl('services', this._formBuilder.array([]));
  }

  createService(): FormGroup {
    return this._formBuilder.group({
      service_id: [null, Validators.required],
      startdate: [null, Validators.required],
      enddate: [null],
      delete_sw: ['N'],
      vendorapplicantid: this.vendorApplicantId,
      vendorapplicantservicesid: [null]
    });
  }

  get serviceArray() {
    return this.profileFormGroup.get('services') as FormArray;
  }

  addServices(): void {
    this.serviceArray.push(this.createService());
  }
  addProfile(isDraft? : boolean) {
    const data = this.profileFormGroup.value;
    data.vendorid = this.vendorId;
    data.vendorapplicantid = (this.vendorApplicantId) ? this.vendorApplicantId : null;
    data.status = 'Draft';
    this._commonHttpService.create(
      data,
      NewUrlConfig.EndPoint.Vendor.AddProfileUrl
    ).subscribe(
      (response) => {
        if (!isDraft) {
          this._alertService.success('Profile info added successfully!');
        }
        this._commonHttpService.getArrayList(
          {
              where: { vendorid: this.vendorId },
              nolimit: true,
              method: 'get'
          },
          NewUrlConfig.EndPoint.Vendor.GetProfileInfoUrl
        ).subscribe((response) => {
          if (response && response.length) {
            const profile = response[0];
            this.vendorApplicantId = profile.vendorapplicantid;
            const isManageVendor = (profile.ismanagevendor) ? true : false;
            this._dataStoreService.setData('isManageVendor', isManageVendor);
            this._dataStoreService.setData('vendorApplicantId', this.vendorApplicantId);
            this._dataStoreService.setData('isVendorExist', true);
            if (!isDraft) {
              this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.vendorId + '/address']);
            }
          }
        },
        (error) => {
          this._alertService.error('Unable to get profile info');
        });
      },
      (error) => {
        this._alertService.error('Unable to save ownership');
      });
  }

  getProfileInfoById() {
    this._commonHttpService.getArrayList(
      {
          where: { vendorid: this.vendorId },
          nolimit: true,
          method: 'get'
      },
      NewUrlConfig.EndPoint.Vendor.GetProfileInfoUrl
    ).subscribe((response) => {
      if (response && Array.isArray(response) && response.length) {
        const profileData = response[0];
        this.vendorApplicantId = profileData.vendorapplicantid;
        const isManageVendor = (profileData.ismanagevendor) ? true : false;
        this._dataStoreService.setData('isManageVendor', isManageVendor);
        this.patchProfileData(profileData);
        this._dataStoreService.setData('vendorApplicantId', this.vendorApplicantId);
        this._dataStoreService.setData('isVendorExist', true);
        this._dataStoreService.setData('decision', profileData.ref_key);
        this._dataStoreService.setData('reviewComments', profileData.approvalcomments);
        this._dataStoreService.setData('reviewNarrative', profileData.narrative);
      } else {
        this.addServices();
      }
    },
    (error) => {
      this._alertService.error('Unable to get profile info');
    });
  }

  patchProfileData(profileData) {
    this.profileFormGroup.patchValue({
      vendorapplicantid: profileData.vendorapplicantid,
      vendorid: profileData.vendorid,
      org_nm: profileData.org_nm,
      jurisdiction: profileData.jurisdiction,
      primary_last_nm: profileData.primary_last_nm,
      primary_first_nm: profileData.primary_first_nm,
      primary_middle_nm: profileData.primary_middle_nm,
      primary_prefix_cd: profileData.primary_prefix_cd,
      primary_suffix_cd: profileData.primary_suffix_cd,
      isprimaryadmin: profileData.isprimaryadmin,
      admin_last_nm: profileData.admin_last_nm,
      admin_first_nm: profileData.admin_first_nm,
      admin_middle_nm: profileData.admin_middle_nm,
      admin_prefix_cd: profileData.admin_prefix_cd,
      admin_suffix_cd: profileData.admin_suffix_cd,
      regularfrom: profileData.regularfrom,
      regularto: profileData.regularto,
      status: profileData.status,
    });
    if (profileData.services && profileData.services.length) {
      const services = profileData.services;
      services.forEach((x, index) => {
        this.serviceArray.push(this.buildServiceForm(x, index));
      });
    }
  }

  clearFormArray = (formArray: FormArray) => {
      formArray = this._formBuilder.array([]);
  }

  private buildServiceForm(x, index): FormGroup {
    return this._formBuilder.group({
      service_id: (x.service_id) ? x.service_id : null,
      startdate: (x.startdate) ?  x.startdate : null,
      enddate: (x.enddate) ? x.enddate : null,
      delete_sw:x.delete_sw ? x.delete_sw : 'N',
      vendorapplicantid: (x.vendorapplicantid) ? x.vendorapplicantid : null,
      vendorapplicantservicesid: (x.vendorapplicantservicesid) ? x.vendorapplicantservicesid : null
    });
  }

  deleteService(index: number) {
    console.log(index);
    this.serviceArray.removeAt(index);
  }

  isPrimaryContact(isChecked) {
    if (isChecked) {
      this.profileFormGroup.patchValue({
        admin_last_nm: this.profileFormGroup.getRawValue().primary_last_nm,
        admin_first_nm: this.profileFormGroup.getRawValue().primary_first_nm,
        admin_middle_nm: this.profileFormGroup.getRawValue().primary_middle_nm,
        admin_prefix_cd: this.profileFormGroup.getRawValue().primary_prefix_cd,
        admin_suffix_cd: this.profileFormGroup.getRawValue().primary_suffix_cd,
      });
    } else {
      this.profileFormGroup.getRawValue().primary_last_nm = '';
      this.profileFormGroup.getRawValue().primary_first_nm = '';
      this.profileFormGroup.getRawValue().primary_middle_nm = '';
      this.profileFormGroup.getRawValue().primary_prefix_cd = '';
      this.profileFormGroup.getRawValue().primary_suffix_cd = '';
    }
  }

  ngOnDestroy() {
    if (this.profileFormGroup.valid && this.vendorApplicantId == null) {
      this.addProfile(true);
    }
  }
  loadJurisdictioDropDown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Vendor.MDCountryListUrl + '?filter'
      )
    ]).map(res => {
      return {
        countyList: res[0].map(
          item =>
            new DropdownModel({
              text: item.countyname,
              value: item.countyid
            })
        )
      };
    });
    this.countyList$ = source.pluck('countyList');
  }

 }
