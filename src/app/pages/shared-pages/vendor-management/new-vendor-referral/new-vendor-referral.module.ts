import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { NewVendorReferralRoutingModule } from './new-vendor-referral-routing.module';
import { NewVendorReferralComponent } from './new-vendor-referral.component';
import { ProfileComponent } from './profile/profile.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaymentComponent } from './payment/payment.component';
import { AddressComponent } from './address/address.component';
import { ContactComponent } from './contact/contact.component';
import { AddressDetailsService } from '../../person-info/address-details/address-details.service';
import { VendorInfoNavComponent } from './vendor-info-nav/vendor-info-nav.component';
import { ReviewComponent } from './review/review.component';
import { QuillModule } from 'ngx-quill';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { VendorReferralDocumentsComponent } from './vendor-referral-documents/vendor-referral-documents.component';
import { AttachmentDetailComponent } from './vendor-referral-documents/attachment-detail/attachment-detail.component';
import { AttachmentUploadComponent } from './vendor-referral-documents/attachment-upload/attachment-upload.component';
import { EditAttachmentComponent } from './vendor-referral-documents/edit-attachment/edit-attachment.component';
@NgModule({
  imports: [
    CommonModule,
    NewVendorReferralRoutingModule,
    FormMaterialModule,
    NgSelectModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    QuillModule,
    ControlMessagesModule,
  ],
  declarations: [
    NewVendorReferralComponent,
    ProfileComponent,
    PaymentComponent,
    AddressComponent,
    ContactComponent,
    VendorInfoNavComponent,
    VendorReferralDocumentsComponent,
    AttachmentDetailComponent,
    AttachmentUploadComponent,
    EditAttachmentComponent,
    ReviewComponent
  ],
  providers: [AddressDetailsService, NgxfUploaderService]
})
export class NewVendorReferralModule { }