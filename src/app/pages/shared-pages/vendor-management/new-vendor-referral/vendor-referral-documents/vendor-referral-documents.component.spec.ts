import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorReferralDocumentsComponent } from './vendor-referral-documents.component';

describe('VendorReferralDocumentsComponent', () => {
  let component: VendorReferralDocumentsComponent;
  let fixture: ComponentFixture<VendorReferralDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorReferralDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorReferralDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
