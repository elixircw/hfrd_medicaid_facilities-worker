import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, CommonHttpService, GenericService } from '../../../../../@core/services';
import { AuthService } from '../../../../../@core/services/auth.service';
import { NewUrlConfig } from '../../vendor-referral-url.config';
import { EditAttachmentComponent } from './edit-attachment/edit-attachment.component';
import { Attachment } from './_entities/attachmnt.model';
import { GeneratedDocuments } from './_entities/generate.model';
import { DataStoreService } from '../../../../../@core/services/data-store.service';



declare var $: any;
import * as ALL_DOCUMENTS from './_configurtions/documents.json';
import { InvolvedPerson } from '../../../../provider-referral/new-referral/_entities/newintakeModel';

@Component({
  selector: 'vendor-referral-documents',
  templateUrl: './vendor-referral-documents.component.html',
  styleUrls: ['./vendor-referral-documents.component.scss']
})
export class VendorReferralDocumentsComponent implements OnInit {

  documentPropertiesId: any;
  @Input() addAttachementSubject$ = new Subject<Attachment[]>();
  @Input() downloadGenereatePDFSubject$ = new Subject<Attachment[]>();
  @Input() reviewstatus: string;
  @Input() generatedDocuments$ = new Subject<string[]>();
  @Input() generateDocumentOutput$ = new Subject<GeneratedDocuments[]>();
  @Input() generateDocumentInput$ = new Subject<GeneratedDocuments[]>();
  // tslint:disable-next-line:no-input-rename
  @Input('addedPersonsChange') addedPersonsChanges$ = new Subject<InvolvedPerson[]>();
  filteredAttachmentGrid: Attachment[] = [];
  intakeNumber: string;
  token: AppUser;
  attachmentTypeDropdown$: Observable<DropdownModel[]>;
  daNumber: string;
  reviewStatus: string;
  id: string;
  attachmentType: FormGroup;
  showScreens = { uploadDocument: false, uploadedDocument: false, createDocument: true };
  searchText = '';
  private allAttachmentGrid: Attachment[];
  @ViewChild(EditAttachmentComponent) editAttach: EditAttachmentComponent;
  generatedDocuments: GeneratedDocuments[] = [];
  allDocuments: GeneratedDocuments[] = <any>ALL_DOCUMENTS;
  config = {isGenerateUploadTabNeeded:false};
  vendorId: string;
  isView: Boolean = false;
  constructor( private formBuilder: FormBuilder,
    private _dropDownService: CommonHttpService,
    private _service: GenericService<Attachment>,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _authService: AuthService,
    private _dataStoreService: DataStoreService) {
      this.vendorId = route.parent.snapshot.params['id'];
    }

  ngOnInit() {
    this.isView = this._dataStoreService.getData('isView');
    this.chooseTab(1);
      this.token = this._authService.getCurrentUser();
      this.attachmentType = this.formBuilder.group({
          selectedAttachment: ['All']
      });
    //   this.generateDocumentInput$.subscribe(documentList => {
    //       if (documentList) {
    //               this.generatedDocuments = [];
    //       }
    //       this.loadGeneratedDocumentList(this.token);
    //   });
      this.loadGeneratedDocumentList(this.token);
      this.prepareConfig();
      this.attachment('All');

  }
  chooseTab(tabId) {
      if (tabId === 1) {
          this.showScreens = { uploadDocument: true, uploadedDocument: false, createDocument: false };
      } else if (tabId === 2) {
          this.showScreens = { uploadDocument: false, uploadedDocument: true, createDocument: false };
      } else if (tabId === 3) {
          this.showScreens = { uploadDocument: false, uploadedDocument: false, createDocument: true };
      }
  }
  getIntakeNumber(intakeNumber) {
      this.vendorId = intakeNumber;
      this.attachment('All');
  }
  filterAttachment(attachType) {
      if (attachType.selectedAttachment === 'All') {
          this.filteredAttachmentGrid = this.allAttachmentGrid;
      } else if (attachType.selectedAttachment === 'Exhibit') {
          this.filteredAttachmentGrid = this.allAttachmentGrid.filter(
              item => item.documenttypekey === attachType.selectedAttachment
          );
      } else {
          this.filteredAttachmentGrid = this.allAttachmentGrid.filter(
              item =>
                  item.documentattachment &&
                  item.documentattachment.attachmenttypekey ===
                  attachType.selectedAttachment
          );
      }
  }
  checkFileType(file: string, accept: string): boolean {
      if (accept) {
          const acceptedFilesArray = accept.split(',');
          return acceptedFilesArray.some(type => {
              const validType = type.trim();
              if (validType.charAt(0) === '.') {
                  return file.toLowerCase().endsWith(validType.toLowerCase());
              }
              return false;
          });
      }
      return true;
  }
   editAttachment(modal) {
       this.editAttach.editForm(modal);
       (<any>$('#edit-attachment')).modal('show');
   }
  confirmDelete(modal) {
      this.documentPropertiesId = modal.documentpropertiesid;
      (<any>$('#delete-attachment')).modal('show');
  }
  deleteAttachment() {
      this._service.endpointUrl =
          NewUrlConfig.EndPoint.Vendor.DeleteAttachmentUrl;
      this._service.remove(this.documentPropertiesId).subscribe(
          result => {
              (<any>$('#delete-attachment')).modal('hide');
              this.attachment('All');
              this._alertService.success('Attachment Deleted successfully!');
          },
          err => {
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          }
      );
  }
  attachment(attachType) {
      this._dropDownService
          .getArrayList(
              new PaginationRequest({
                  nolimit: true,
                  order: 'originalfilename',
                  method: 'get'
              }),
              NewUrlConfig.EndPoint.Vendor.AttachmentGridUrl +
              '/' +
              this.vendorId +
              '?data'
          )
          .subscribe(result => {
              this.allAttachmentGrid = result;
              // this.filteredAttachmentGrid = this.allAttachmentGrid;
              result.map(item => {
                  item.numberofbytes = this.humanizeBytes(item.numberofbytes);
              });
              this.filteredAttachmentGrid = result;
              this.addAttachementSubject$.next(result);
          });
  }

  private loadDropdown() {
      this.attachmentTypeDropdown$ = this._dropDownService
          .getArrayList(
              {},
              NewUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentTypeUrl
          )
          .map(result => {
              return result.map(
                  res =>
                      new DropdownModel({
                          text: res.typedescription,
                          value: res.attachmenttypekey
                      })
              );
          });
  }
  private humanizeBytes(bytes: number): string {
      if (bytes === 0) {
          return '0 Byte';
      }
      if (!bytes) {
        return '';
     }
      const k = 1024;
      const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
      const i: number = Math.floor(Math.log(bytes) / Math.log(k));
      return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
  }

  private loadGeneratedDocumentList(user: AppUser) {
     this.allDocuments.forEach(document => {
           if (document.access.indexOf(user.role.name) !== -1) {
             const isExist = this.generatedDocuments.find(gDocument => document.id === gDocument.id);
               if (!isExist) {
             this.generatedDocuments.push(document);
               }
           }

       });

  }

  downloadDocument(document) {
       this.generatedDocuments$.next([document.key]);
      // this._dataStoreService.setData('documentsToDownload', [document.key]);

  }

  downloadSelectedDocuments() {
      const selectedDocuments = this.getSelectedDocuments();
       if (selectedDocuments) {
           this.generatedDocuments$.next(selectedDocuments.map(document => document.key));
      }

  }

  getSelectedDocuments() {
      return this.generatedDocuments.filter(document => document.isSelected);
  }

  isFileSelected() {
      const selectedDocuments = this.getSelectedDocuments();
      if (selectedDocuments) {
          return selectedDocuments.length > 0;
      }

      return false;
  }

  generateNewDocument(document) {
      document.isInProgress = true;
      setTimeout(() => {
          document.isGenerated = true;
      document.generatedBy = this.token.user.userprofile.displayname;
      document.generatedDateTime = new Date();
      document.isInProgress = false;
      this.generateDocumentOutput$.next(this.generatedDocuments);
      }, 1000);

  }

  generateSelectedDocuments() {
      this.generatedDocuments.forEach(document => {
          if (document.isSelected) {
              this.generateNewDocument(document);
          }

      });
  }

  // isCourtLiaisonWorker() {

  //     if (this.token) {
  //         return this.token.role.name === CLW;
  //     }
  //     return false;

  // }

  // isCompleteVisible() {
  //    return this.isCourtLiaisonWorker();
  // }

  prepareConfig() {
      const isDjs = this._authService.isDJS();

      if (isDjs) {
          this.config.isGenerateUploadTabNeeded = true;
      } else {
          this.config.isGenerateUploadTabNeeded = false;
      }
  }

  }

