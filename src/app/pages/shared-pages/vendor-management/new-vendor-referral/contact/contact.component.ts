import { Component, OnInit } from '@angular/core';
import { CommonDropdownsService, CommonHttpService, AlertService, GenericService, ValidationService, DataStoreService } from '../../../../../@core/services';
import { Validators, Form, FormBuilder, FormGroup } from '@angular/forms';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AddressType } from '../../../../admin/general/_entities/general.data.models';
import { NewUrlConfig } from '../../vendor-referral-url.config';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactTypes: any[];
  vendorPhoneFormGroup: FormGroup;
  vendorEmailFormGroup: FormGroup;
  phoneList = [];
  emailList = [];
  vendorApplicantId: any;
  reportPhoneMode: string;
  vendorPhoneId: any;
  reportEmailMode: string;
  vendorEmailId: any;
  deleteType = null;
  deleteId = null;
  addedit = 'Add New';
  primaryemailList: any[];
  isprimaryemailadded= false;
  isprimaryadded: boolean;
  isView: Boolean = false;
  constructor(
    private _commonDropDownService: CommonDropdownsService,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertservices: AlertService,
    private _service_address: GenericService<AddressType>,
    private _dataStoreService: DataStoreService
  ) { }

  ngOnInit() {
    this.reportPhoneMode = 'add';
    this.reportEmailMode = 'add';
    this.isView = this._dataStoreService.getData('isView');
    this.vendorApplicantId =  this._dataStoreService.getData('vendorApplicantId') ?
                              this._dataStoreService.getData('vendorApplicantId') : '';

    this._commonDropDownService.getPickListByName('phonetype').subscribe(contactTypes => {
      if (contactTypes && Array.isArray(contactTypes)) {
        this.contactTypes = contactTypes;
      }
    });
    this.initializePhoneForm();
    this.initializeEmailForm();
    this.getPhoneNumberList();
    this.getEmailList();
  }

  initializePhoneForm() {
    this.vendorPhoneFormGroup = this._formBuilder.group({
      vendorphoneid: [null],
      vendorapplicantid: [null],
      phonenumber: ['', [Validators.required]],
      phonetypekey: ['', [Validators.required]]
    });
  }

  initializeEmailForm() {
    this.vendorEmailFormGroup = this._formBuilder.group({
      vendorphoneid: [null],
      vendorapplicantid: [null],
      email: ['', [ValidationService.mailFormat]],
      emailtypekey: ['', [Validators.required]]
    });
  }

  resetPhone() {
    this.addedit = 'Add New';
    this.reportPhoneMode = 'add';
    this.vendorPhoneFormGroup.reset();
  }

  resetEmail() {
    this.reportEmailMode = 'add';
    this.addedit = 'Add New';
    this.vendorEmailFormGroup.reset();
    if (this.emailList && this.emailList.length > 0) {
      this.primaryemailList = this.emailList.filter(email => email.emailtypekey === 'P');
      if (this.primaryemailList.length > 0) {
        this.isprimaryadded = true;
      } else {
        this.isprimaryadded = false;
      }
    } else {
        this.isprimaryadded = false;
    }
  }

  addPhone() {
    const vendorPhoneId = (this.reportPhoneMode === 'add') ? null : this.vendorPhoneId;
    const vendorPhoneForm = this.vendorPhoneFormGroup.getRawValue();
    const addupdatePhone = {
        vendorphoneid: vendorPhoneId,
        vendorapplicantid: this.vendorApplicantId,
        phonetypekey: vendorPhoneForm.phonetypekey,
        phonenumber: vendorPhoneForm.phonenumber,
        phoneextension: null
    };

    this._commonHttpService.create(
      addupdatePhone,
      NewUrlConfig.EndPoint.Vendor.AddPhoneUrl
    ).subscribe(
      result => {
        this._alertservices.success('Phone details Added/Updated successfully!');
        (<any>$('#addPhone')).modal('hide');
        this.reportPhoneMode = 'add';
        this.getPhoneNumberList();
      },
      error => {
        this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      });
  }

  addEmail() {
    const vendorEmailForm = this.vendorEmailFormGroup.getRawValue();
    if (vendorEmailForm.emailtypekey === 'S') {
     this.primaryemailList = this.emailList.filter(email => email.emailtypekey === 'P');
     if (this.primaryemailList.length > 0) {
      this.isprimaryemailadded = true;
      } else {
      this._alertservices.error('Please enter primary email address!');
         }
    } else {
      this.isprimaryemailadded = true;
    }
    if (this.isprimaryemailadded === true) {
    const vendorEmailId = (this.reportEmailMode === 'add') ? null : this.vendorEmailId;
    const addupdateemail = {
        vendoremailid: vendorEmailId,
        vendorapplicantid: this.vendorApplicantId,
        emailtypekey: vendorEmailForm.emailtypekey,
        email: vendorEmailForm.email
    };

   this._commonHttpService.create(addupdateemail, NewUrlConfig.EndPoint.Vendor.AddEmailUrl).subscribe(
      result => {
        this._alertservices.success('Email details Added/Updated successfully!');
        (<any>$('#addEmail')).modal('hide');
        this.reportEmailMode = 'add';
        this.getEmailList();
      },
      error => {
        this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
    }
  }

  getPhoneNumberList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: {
          vendorapplicantid: this.vendorApplicantId,
          delete_sw: 'N'
        }
      },  NewUrlConfig.EndPoint.Vendor.GetPhoneListUrl
    ).subscribe(
      (response) => {
        this.phoneList = [];
        if (response.length) {
          this.phoneList = response;
        }
    });
  }

  getEmailList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: {
          vendorapplicantid: this.vendorApplicantId,
          delete_sw: 'N'
        }
      }, NewUrlConfig.EndPoint.Vendor.GetEmailListUrl
    ).subscribe(
      (response) => {
        this.emailList = [];
        if (response.length) {
          this.emailList = response;
        }
    });
  }

  getPhoneType(type) {
    if (this.contactTypes && type) {
      const typeObj = this.contactTypes.find(data => data.ref_key === type);
      return typeObj.description;
    }
    return '';
  }

  getEmailType(type) {
    if (type) {
      let emailType = '';
      if (type === 'P') {
        emailType = 'Primary';
      } else if (type === 'S') {
        emailType = 'Secondary';
      }
      return emailType;
    }
    return '';
  }

  editPhone(data) {
    this.addedit = 'Edit';
    this.reportPhoneMode = 'edit';
    this.vendorPhoneFormGroup.patchValue(data);
    this.vendorPhoneId = data.vendorphoneid;
  }

  editEmail(data) {
    if (data.emailtypekey === 'P') {
      this.isprimaryadded = false;
    } else {
      if (this.emailList && this.emailList.length > 0) {
        this.primaryemailList = this.emailList.filter(email => email.emailtypekey === 'P');
        if (this.primaryemailList.length > 0) {
          this.isprimaryadded = true;
        } else {
          this.isprimaryadded = false;
        }
      } else {
          this.isprimaryadded = false;
      }
    }
    this.addedit = 'Edit';
    this.reportEmailMode = 'edit';
    this.vendorEmailFormGroup.patchValue(data);
    this.vendorEmailId = data.vendoremailid;
  }

  confirmDelete(id, type, typekey) {
    this.deleteType = type;
    this.deleteId = id;
    console.log(this.deleteId + '||' + this.deleteType);
    if ((type === 'email') && (typekey === 'P')) {
    this.primaryemailList = this.emailList.filter(email => email.emailtypekey === 'P');
     if (this.primaryemailList.length < 2) {
      this._alertservices.error('Please make sure to have atleast one primary email address!');
     }
    }
    (<any>$('#delete-popup')).modal('show');
  }

  declineDelete() {
    this.deleteType = null;
    this.deleteId = null;
    (<any>$('#delete-popup')).modal('hide');
  }

  deletePhoneEmail(i: number) {
    if (this.deleteId) {
      if (this.deleteType === 'email') {
         this._commonHttpService.endpointUrl = 'Tb_vendor_email';
      } else {
        this._commonHttpService.endpointUrl = 'Tb_vendor_phone';
      }
      this._commonHttpService.patch(this.deleteId, {delete_sw: 'Y'})
        .subscribe((response) => {
          (<any>$('#delete-popup')).modal('hide');
          const deleteType = (this.deleteType === 'email') ? 'Email' : 'Phone';
          this._alertservices.success(deleteType + ' deleted successfully..');

          if (this.deleteType === 'email') {
            this.getEmailList();
          } else {
            this.getPhoneNumberList();
          }
        },
        error => {
          this._alertservices.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    } else {
      (<any>$('#delete-popup')).modal('hide');
    }
  }

  formatPhoneNumber(phoneNumber: string) {
    return this._commonDropDownService.formatPhoneNumber(phoneNumber);
  }
}

