import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVendorReferralComponent } from './new-vendor-referral.component';

describe('NewVendorReferralComponent', () => {
  let component: NewVendorReferralComponent;
  let fixture: ComponentFixture<NewVendorReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVendorReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVendorReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
