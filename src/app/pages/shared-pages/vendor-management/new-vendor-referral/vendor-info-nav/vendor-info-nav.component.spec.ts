import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorInfoNavComponent } from './vendor-info-nav.component';

describe('VendorInfoNavComponent', () => {
  let component: VendorInfoNavComponent;
  let fixture: ComponentFixture<VendorInfoNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorInfoNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorInfoNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
