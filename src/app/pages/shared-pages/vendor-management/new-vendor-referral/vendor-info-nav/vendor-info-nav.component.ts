import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService, AlertService } from '../../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'vendor-info-nav',
  templateUrl: './vendor-info-nav.component.html',
  styleUrls: ['./vendor-info-nav.component.scss']
})
export class VendorInfoNavComponent implements OnInit {
  @Input()
  page: any;
  profileLink: string;
  addressLink: string;
  contactLink: string;
  isVendorExist: any;
  referralId: any;
  menuActive: any = {};
  constructor( 
    private _dataStoreService: DataStoreService,
    private _alertService: AlertService,
    private _router: Router,
    private _route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.menuActive.page = this.page;
    this.isVendorExist = this._dataStoreService.getData('isVendorExist');
    this.referralId = this._route.parent.snapshot.params['id'];
  }

  redirectPage(page) {
    this.isVendorExist = this._dataStoreService.getData('isVendorExist');
    this.menuActive.page = '';
    if (page === 'profile') {
      this.menuActive.page = 'address';
      this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.referralId + '/profile']);
      this.menuActive.page = 'profile';
    } else if (page === 'address') {
      if (this.isVendorExist) {
        this.menuActive.page = 'address';
        this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.referralId + '/address']);
      } else {
        this.menuActive.page = 'profile';
        this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.referralId + '/profile']);
        this._alertService.warn('Please complete profile info');
      }
    } else if (page === 'contact') {
      if (this.isVendorExist) {
        this.menuActive.page = 'contact';
        this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.referralId + '/contact']);
      } else {
        this.menuActive.page = 'profile';
        this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + this.referralId + '/profile']);
        this._alertService.warn('Please complete profile info');
      }
    }
  }

}
