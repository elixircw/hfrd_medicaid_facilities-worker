import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap';
import { VendorManagementRoutingModule } from './vendor-management-routing.module';
import { VendorManagementComponent } from './vendor-management.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { AddressDetailsService } from '../person-info/address-details/address-details.service';
import { ExistingVendorReferralComponent } from './existing-vendor-referral/existing-vendor-referral.component';
import { MatSortModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    VendorManagementRoutingModule,
    FormMaterialModule,
    PaginationModule,
    MatSortModule
  ],
  declarations: [VendorManagementComponent, ExistingVendorReferralComponent],
  providers: [AddressDetailsService]
})
export class VendorManagementModule { }
