import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingVendorReferralComponent } from './existing-vendor-referral.component';

describe('ExistingVendorReferralComponent', () => {
  let component: ExistingVendorReferralComponent;
  let fixture: ComponentFixture<ExistingVendorReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingVendorReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingVendorReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
