import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { NewUrlConfig } from '../vendor-referral-url.config';
import { MatTableDataSource } from '@angular/material';

export interface VendorList {
  vendorid: string;
  vendorapplicantid: string;
  status: any;
  referraldate: string;
  org_nm: any;
  jurisdiction: string;
  approvaldate: string;
  address: string;
  narrative: string;
}
@Component({
  selector: 'existing-vendor-referral',
  templateUrl: './existing-vendor-referral.component.html',
  styleUrls: ['./existing-vendor-referral.component.scss']
})
export class ExistingVendorReferralComponent implements OnInit {
  vendorList: any[] = [];
  manageVendorList: any[] = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  pageInfo: PaginationInfo = new PaginationInfo();
  private pageSubject$ = new Subject<number>();
  totalRecords: number;
  user: any;
  tabTitle: any = '';
  dateLabel: any = '';
  closeVendorData: any;
  deleteId = null;
  isCloseForSupervisor: Boolean = true;
  rejectId: any = '';
  closeTitle: any;
  closeText: string;
  commentInfo: any;
  vendorid: any;
  org_nm: any;
  address: any;
  referraldate: any;
  approvaldate: any;
  status: any;
  action: any;
  dataSource = new MatTableDataSource<VendorList>();
  displayedColumns: string[] = ['vendorid', 'org_nm', 'address', 'referraldate', 'approvaldate',  'status', 'narrative', 'action'];
  manageDisplayedColumns: string[] = ['providerid', 'org_nm', 'address', 'referraldate', 'approvaldate',  'status', 'narrative', 'action'];
  sortData: any;
  isLoading: Boolean = false;
  inputRequest: {};
  isOpen: Boolean = false;
  isManage: Boolean = false;
  manageDataSource = new MatTableDataSource<VendorList>();
  manageTotalRecords: number;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private route: ActivatedRoute,
    private _authService: AuthService
  ) { }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    if (this.user && this.user.role) {
      this.tabTitle  = (this.user.role.name === 'FS') ? 'Manage Vendors' : 'Open Requests';
      // this.tabTitle = 'MANAGE VENDORS';
      this.dateLabel = (this.user.role.name === 'FS') ? 'CREATE DATE' : 'REFERRAL DATE';
    }
    this.pageSubject$.subscribe(pageNumber => {
      this.paginationInfo.pageNumber = pageNumber;
      this.getOpenRequest(this.paginationInfo.pageNumber);
    });
    this.sortData = {
      // active: 'org_nm',
      // direction: 'asc'
      active:'create_ts',
      direction:'desc'
  };
    // this.getExistingVendorList(1);
    this.isOpen = true;
    this.getOpenRequest(1);
  }

  getNewVendor() {
    this._dataStoreService.setData('isManageVendor', false);
    this._dataStoreService.setData('isView', false);
    this._dataStoreService.setData('providerid', null);
    this._commonHttpService.getArrayList(
      {}, NewUrlConfig.EndPoint.Vendor.NextnumbersUrl
      ).subscribe((result) => {
        const referralID =  result['nextNumber'];
        this._dataStoreService.setData('vendorApplicantId', null);
        this._dataStoreService.setData('vendorReferralId', referralID);
        this._dataStoreService.setData('referralDate', new Date());
        this._dataStoreService.setData('vendorStatus', 'Open');
        this._dataStoreService.setData('isVendorExist', false);
        this._dataStoreService.setData('vendorSubmittedfor', '');
        this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + referralID + '/profile']);
    });
  }

  getOpenRequest(page) {
    this.isOpen = true;
    this.isManage = false;
    this.inputRequest = { filter: '{Draft, Submitted for Approval, Submitted for Closure}', sort :  this.sortData };
    setTimeout(() => {
      this.isLoading = true;
    });
    let url = '';
    if (this.user.role.name === 'FS') {
      url = NewUrlConfig.EndPoint.Vendor.GetVendorSupervisorListUrl;
    } else {
      url = NewUrlConfig.EndPoint.Vendor.GetVendorDashboardListUrl;
    }
    this.paginationInfo.pageNumber = page;
    this._commonHttpService.getArrayList(
      {
        page: this.paginationInfo.pageNumber,
        limit: 20,
        method: 'get',
        where: this.inputRequest,
      }, url + '?filter'
    ).subscribe(
      (response) => {
        if (response.length) {
          this.vendorList = response;
          this.dataSource.data = this.vendorList;
          if (page === 1) {
            this.totalRecords = response[0].totalcount;
          }
          this.isLoading = false;
        }
    });
  }

  getManageRequest(page) {
    this.isManage = true;
    this.isOpen = false;
    let url = '';
    this.inputRequest = { filter: '{Active,Re-Opened,Closed,Rejected}', sort :  this.sortData };
    if (this.user.role.name === 'FS') {
      url = NewUrlConfig.EndPoint.Vendor.GetVendorSupervisorListUrl;
    } else {
      url = NewUrlConfig.EndPoint.Vendor.GetVendorDashboardListUrl;
    }
    this.pageInfo.pageNumber = page;
    this._commonHttpService.getArrayList(
      {
        page: this.pageInfo.pageNumber,
        limit: 20,
        method: 'get',
        where: this.inputRequest
      }, url + '?filter'
    ).subscribe(
      (response) => {
        if (response.length) {
          this.manageVendorList = response;
          this.manageDataSource.data = this.manageVendorList;
          if (page === 1) {
            this.manageTotalRecords = response[0].totalcount;
          }
        }
    });
  }

  // getExistingVendorList(page: number) {
  //   setTimeout(() => {
  //     this.isLoading = true;
  //   });
  //   let url = '';
  //   this.inputRequest = { sort :  this.sortData };
  //   if (this.user.role.name === 'FS') {
  //     url = NewUrlConfig.EndPoint.Vendor.GetVendorSupervisorListUrl;
  //   } else {
  //     url = NewUrlConfig.EndPoint.Vendor.GetVendorDashboardListUrl;
  //   }
  //   this.paginationInfo.pageNumber = page;
  //   this._commonHttpService.getArrayList(
  //     {
  //       where : this.inputRequest,
  //       page: this.paginationInfo.pageNumber,
  //       limit: 20,
  //       method: 'get',
  //     }, url + '?filter'
  //   ).subscribe(
  //     (response) => {
  //       if (response.length) {
  //         this.vendorList = response;
  //         this.dataSource.data = this.vendorList;
  //         if (page === 1) {
  //           this.totalRecords = response[0].totalcount;
  //         }
  //         this.isLoading = false;
  //       }
  //   });
  // }

  viewVendor(item) {
    this._dataStoreService.setData('isView', true);
    this._dataStoreService.setData('isManageVendor', false);
    this._dataStoreService.setData('vendorReferralId', item.vendorid);
    this._dataStoreService.setData('vendorApplicantId', item.vendorapplicantid);
    this._dataStoreService.setData('isVendorExist', true);
    this._dataStoreService.setData('providerid', item.providerid);
    this._dataStoreService.setData('referralDate', item.referraldate);
    this._dataStoreService.setData('vendorStatus', item.status);
    this._dataStoreService.setData('vendorSubmittedfor', item.submittedfor);
    this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + item.vendorid + '/profile']);
  }

  editVendor(item) {
    this._dataStoreService.setData('isManageVendor', false);
    this._dataStoreService.setData('isView', false);
    this._dataStoreService.setData('vendorReferralId', item.vendorid);
    this._dataStoreService.setData('vendorApplicantId', item.vendorapplicantid);
    this._dataStoreService.setData('isVendorExist', true);
    this._dataStoreService.setData('providerid', item.providerid);
    this._dataStoreService.setData('referralDate', item.referraldate);
    this._dataStoreService.setData('vendorStatus', item.status);
    this._dataStoreService.setData('vendorSubmittedfor', item.submittedfor);
    this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + item.vendorid + '/profile']);
  }

  pageChanged(event: any) {
    this.paginationInfo.pageNumber = event.page;
    this.getOpenRequest(this.paginationInfo.pageNumber);
  }
  pageNumberChanged(event: any) {
    this.pageInfo.pageNumber = event.page;
    this.getManageRequest(this.pageInfo.pageNumber);
  }

  confirmDeleteVendor(id) {
    this.deleteId = id;
    (<any>$('#delete-vendor-popup')).modal('show');
  }

  declineDeleteVendor() {
    this.deleteId = null;
    (<any>$('#delete-vendor-popup')).modal('hide');
  }

  deleteVendor() {
    if (this.deleteId) {
      this._commonHttpService.endpointUrl = 'tb_vendor_applicant';
      this._commonHttpService.patch(this.deleteId, {delete_sw: 'Y'})
        .subscribe((response) => {
          (<any>$('#delete-vendor-popup')).modal('hide');
          this._alertService.success('Vendor deleted successfully..');
          this.getOpenRequest(1);
        },
        error => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    } else {
      (<any>$('#delete-vendor-popup')).modal('hide');
    }
  }

  manageVendor(data) {
    const vendorData = data;
    this._dataStoreService.setData('isManageVendor', true);
    this._dataStoreService.setData('isView', false);
    this._dataStoreService.setData('vendorApplicantId', null);
    this._dataStoreService.setData('providerid', data.providerid);
    this._dataStoreService.setData('referralDate', data.referraldate);
    this._dataStoreService.setData('vendorStatus', data.status);
    this._dataStoreService.setData('vendorSubmittedfor', data.submittedfor);
    if (vendorData.status === 'Closed') {
      this.processClosedVendor(vendorData);
    } else if (vendorData.status === 'Active') {
      this.processActiveVendor(vendorData);
    }
  }

  processClosedVendor(data) {
    this._commonHttpService.getArrayList(
      {}, NewUrlConfig.EndPoint.Vendor.NextnumbersUrl
      ).subscribe((result) => {
        const referralID =  result['nextNumber'];
        this._dataStoreService.setData('vendorReferralId', referralID);
        this._dataStoreService.setData('isVendorExist', true);

       const managePostData = {
          vendorid: data.vendorid,
          vendorapplicantid: data.vendorapplicantid,
          newvendorid: referralID
        };

        this._commonHttpService.create(
          managePostData,
          NewUrlConfig.EndPoint.Vendor.ManageVendorDetailsUrl
        ).subscribe((response) => {
            if (response) {
              this._router.navigate(['/pages/vendor-management/new-vendor-referral/' + referralID + '/profile']);
            }
          },
          (error) => {
            this._alertService.error('Unable to close vendor');
        });
    }, (error) => {
      this._alertService.error('Unable to get vendor referral id');
    });
  }

  processActiveVendor(data) {
    // Need to remove patch api for active manage vendors
    // this._commonHttpService.endpointUrl = 'Tb_vendor_applicant';
    // const patchRequest = {
    //   vendorapplicantid: data.vendorapplicantid,
    //   status: 'Submitted for Approval'
    // };
    // this._commonHttpService.patch(data.vendorapplicantid, patchRequest)
    //   .subscribe((response) => {
    //     if (response) {
          this.editVendor(data);
    //     }
    //   },
    //   error => {
    //     this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    // });
  }

  rejectVendor(data) {
    this.rejectId = data.vendorapplicantid;
    (<any>$('#reject-vendor-popup')).modal('show');
  }

  rejectIdentified(status) {
    if (status === 1 && this.rejectId) {
      this._commonHttpService.endpointUrl = 'tb_vendor_applicant';
      const rejectData = {
        vendorapplicantid: this.rejectId,
        status: 'Active'
      };
      this._commonHttpService.patch(this.rejectId, rejectData)
        .subscribe((response) => {
          (<any>$('#delete-vendor-popup')).modal('hide');
          this._alertService.success('Vendor deleted successfully..');
          // this.getManageRequest(1);
          this.getOpenRequest(1);
        },
        error => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    }
    (<any>$('#reject-vendor-popup')).modal('hide');
  }

  closeVendor(isApproval: Boolean, data) {
    this.closeVendorData = data;
    (<any>$('#close-vendor-popup')).modal('show');
    if (this.user.role.name === 'FS') {
      this.isCloseForSupervisor = true;
      this.closeTitle = (isApproval) ? 'Approval for Closure' : 'Close Vendor';
      this.closeText = (isApproval) ? 'Are you sure you want to approve the closure?' : 'Are you sure you want to close this vendor?';
    } else {
      this.isCloseForSupervisor = false;
      this.closeTitle = 'Close Vendor';
      this.closeText = 'Are you sure you want to close this vendor?';
    }
  }

  approveCloseVendor(status) {
    if (status === 1) {
      const data = {
        vendorid : this.closeVendorData.vendorid,
        vendorapplicantid  : this.closeVendorData.vendorapplicantid,
      };
      this._commonHttpService.create(
        data,
        NewUrlConfig.EndPoint.Vendor.CloseVendorUrl
      ).subscribe((response) => {
          if (response) {
            this._alertService.success('Vendor info closed successfully!');
            // this.getManageRequest(1);
            this.getOpenRequest(1);
          }
        },
        (error) => {
          this._alertService.error('Unable to close vendor info');
      });
    }
    (<any>$('#close-vendor-popup')).modal('hide');
  }

  closeIdentified(status: number) {
    if (this.isCloseForSupervisor) {
      this.approveCloseVendor(status);
    } else {
      this.submitClosure(status);
    }
  }

  submitClosure(status) {
    if (status === 1) {
      const data = {
        vendorid : this.closeVendorData.vendorid,
        vendorapplicantid  : this.closeVendorData.vendorapplicantid,
        jurisdiction: this.closeVendorData.jurisdiction
      };
      this._commonHttpService.create(
        data,
        NewUrlConfig.EndPoint.Vendor.SubmitClosureUrl
      ).subscribe((response) => {
          if (response) {
            this._alertService.success('Submit for closure sent successfully!');
            this.getManageRequest(1);
          }
        },
        (error) => {
          this._alertService.error('Unable to send submit for clcosure');
      });
    }
    (<any>$('#close-vendor-popup')).modal('hide');
  }

  showComment(item: any) {
    this.commentInfo = item.narrative ? item.narrative : 'No comments found!';
  }

  resetComment() {
    this.commentInfo = null;
  }

  customSort(event, isManageVendor) {
    console.log('sort data custome', event);
    this.sortData = event;
    if (isManageVendor) {
      this.getManageRequest(1);
      this.pageInfo.pageNumber = 1;
    } else {
      this.getOpenRequest(1);
      this.paginationInfo.pageNumber = 1;
    }
  }
}
