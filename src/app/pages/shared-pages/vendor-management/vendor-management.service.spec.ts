import { TestBed, inject } from '@angular/core/testing';

import { VendorManagementService } from './vendor-management.service';

describe('VendorManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VendorManagementService]
    });
  });

  it('should be created', inject([VendorManagementService], (service: VendorManagementService) => {
    expect(service).toBeTruthy();
  }));
});
