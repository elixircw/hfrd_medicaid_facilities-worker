import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorManagementComponent } from './vendor-management.component';
import { ExistingVendorReferralComponent } from './existing-vendor-referral/existing-vendor-referral.component';
//import { ExistingVendorComponent } from './existing-vendor/existing-vendor.component';
const routes: Routes = [
  {
      path: '',
      component: VendorManagementComponent,
      // canActivate: [RoleGuard],
      children: [
          {
              path: 'new-vendor-referral',
              loadChildren: './new-vendor-referral/new-vendor-referral.module#NewVendorReferralModule'
          }
      ]
  },
  {
      path: 'existing-vendor-referral',
      component: ExistingVendorReferralComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorManagementRoutingModule { }
