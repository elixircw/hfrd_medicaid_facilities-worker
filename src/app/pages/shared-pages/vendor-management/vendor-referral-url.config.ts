// To maintain intake functionality
export class NewUrlConfig {
    public static EndPoint = {
        Vendor: {
            GetVendorSupervisorListUrl: 'tb_vendor_applicant/getvendorsupervisorlist',
            GetVendorDashboardListUrl: 'tb_vendor_applicant/getdashboardlist',
            CloseVendorUrl: 'tb_vendor_applicant/closevendor',
            SubmitClosureUrl: 'tb_vendor_applicant/submitclosevendorapproval',
            NextnumbersUrl: 'Nextnumbers/getNextNumber?apptype=vendorreferral',
            ManageVendorDetailsUrl: 'Tb_vendor_addresses/updatemanagedetails',
            GetVendorAddressListUrl: 'Tb_vendor_addresses/getaddresslist',
            GetVendorApprovalList: 'tb_vendor_applicant/getvendorapprovallist',
            AddAddressUrl: 'Tb_vendor_addresses/addupdate',
            SuggestAddressUrl: 'People/suggestaddress',
            ValidateAddressUrl: 'People/validateaddress',
            AddPhoneUrl: 'Tb_vendor_phone/addupdate',
            AddEmailUrl: 'Tb_vendor_email/addupdate',
            GetPhoneListUrl: 'Tb_vendor_phone?filter',
            GetEmailListUrl: 'Tb_vendor_email?filter',
            AddPaymentInfoUrl: 'tb_vendor_applicant/addupdatepaymentinfo',
            GetPaymentInfoUrl: 'tb_vendor_applicant/getvendorpaymentinfo?filter',
            SubmitVendorApprovalUrl: 'tb_vendor_applicant/submitvendorapproval',
            CheckTaxIdUrl: 'Tb_vendor_addresses/checktaxiddup?filter',
            VendorServiceUrl: 'TbServices/vendorservices?filter',
            AddProfileUrl: 'tb_vendor_applicant/addupdate',
            GetProfileInfoUrl: 'tb_vendor_applicant/getprofileinfo?filter',
            MDCountryListUrl: 'admin/county',
            AttachmentGridUrl: 'Documentproperties/getintakeattachments',
            SaveAttachmentUrl: 'Documentproperties/addintakeattchment',
            DeleteAttachmentUrl: 'Documentproperties/delete',
            GetVendorInfoUrl: 'tb_vendor_applicant/getvendorprofiledetails?filter'
        },
        DSDSAction: {
            Attachment: {
                UploadAttachmentUrl: 'attachments/uploadsFile',
                AttachmentGridUrl: 'Documentproperties/getattachments',
                AttachmentUploadUrl: 'attachments/uploadsFile',
                AttachmentTypeUrl: 'Intake/attachmenttype',
                AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype',
                SaveAttachmentUrl: 'Documentproperties/addattchment'
            },
            Disposition: {
                StatusUrl: 'Intakeservicerequests/statuslist',
                DispositionUrl: 'Intakeservicerequests/dispositionlist',
                DispositionListUrl: 'Intakeservicerequestdispositioncodes/GetDispositon',
                DispositionAddUrl: 'Intakeservicerequestdispositioncodes/Add'
            }
        }
    };
}
