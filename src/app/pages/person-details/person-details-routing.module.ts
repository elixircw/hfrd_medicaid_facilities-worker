import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonDetailsComponent } from './person-details.component';
import { RoleGuard } from '../../@core/guard';
import { PersonEducationDetailsComponent } from './person-education-details/person-education-details.component';
import { PersonWorksDetailsComponent } from './person-works-details/person-works-details.component';
import { PersonAddressDetailsComponent } from './person-address-details/person-address-details.component';
import { PersonDetailsService } from './person-details.service';
import { PersonDetailsResolverService } from './person-details-resolver.service';
import { PersonBasicDetailsService } from './person-basic-details/person-basic-details.service';
import { PersonEducationDetailsService } from './person-education-details/person-education-details.service';
import { PersonWorksDetailsService } from './person-works-details/person-works-details.service';

const routes: Routes = [
  {
    path: ':action/:personid',
    component: PersonDetailsComponent,
    children: [
      // { path: '', loadChildren: './person-basic-details/person-basic-details.module#PersonBasicDetailsModule' },
      { path: 'contact-detail', loadChildren: './contact-details/contact-details.module#ContactDetailsModule' },
      { path: 'basic', loadChildren: './person-basic-details/person-basic-details.module#PersonBasicDetailsModule' },
      { path: 'alerts', loadChildren: './person-alerts/person-alerts.module#PersonAlertsModule' },
      { path: 'health', loadChildren: './person-health/person-health.module#PersonHealthModule' },
      { path: 'education', component: PersonEducationDetailsComponent },
      { path: 'work', component: PersonWorksDetailsComponent },
      { path: 'relationship', loadChildren: './person-relationship/person-relationship.module#PersonRelationshipModule' },
      { path: 'address', component: PersonAddressDetailsComponent },
      { path: 'legal-actions', loadChildren: './person-history-details/person-history-details.module#PersonHistoryDetailsModule' },
      { path: 'client-event-history', loadChildren: './client-event-history/client-event-history.module#ClientEventHistoryModule' },
      { path: 'restitution', loadChildren: './person-restitution/person-restitution.module#PersonRestitutionModule' },
      { path: 'client-family-info', loadChildren: './person-client-family-info/person-client-family-info.module#PersonClientFamilyInfoModule' },
      { path: 'client-appointments', loadChildren: './person-client-appointments/person-client-appointments.module#PersonClientAppointmentsModule' },
      { path: 'client-summaries', loadChildren: './person-client-summaries/person-client-summaries.module#PersonClientSummariesModule' },
      { path: 'transport', loadChildren: './person-transport/person-transport.module#PersonTransportModule' },
      { path: 'other-agencies', loadChildren: './person-other-agencies/person-other-agencies.module#PersonOtherAgenciesModule' },
      { path: 'audit-trail', loadChildren: './person-audit-trail/person-audit-trail.module#PersonAuditTrailModule' },
      { path: 'assesments', loadChildren: './person-assesments/person-assesments.module#PersonAssesmentsModule' },
      { path: 'documents', loadChildren: './person-documents/person-documents.module#PersonDocumentsModule' },
      { path: 'investigation', loadChildren: './person-investigation/person-investigation.module#PersonInvestigationModule' },
      { path: 'physcial-attributes', loadChildren: './physical-attributes/physical-attributes.module#PhysicalAttributesModule' },
      { path: 'youth-status', loadChildren: './person-youthstatus/person-youthstatus.module#PersonYouthstatusModule' },
      { path: 'status-summary', loadChildren: './person-status-summary/person-status-summary.module#PersonStatusSummaryModule' },
      // { path: 'probation', loadChildren: './person-probation/person-probation.module#PersonProbationModule' },
      { path: '**', redirectTo: 'basic' }
    ],
    canActivate: [RoleGuard],
    data: {
      screen: { modules: ['pages', 'menus'], skip: false }
    },
    resolve: {
      personDetails: PersonDetailsResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [PersonDetailsService, PersonDetailsResolverService, PersonBasicDetailsService, PersonEducationDetailsService, PersonWorksDetailsService]
})
export class PersonDetailsRoutingModule { }
