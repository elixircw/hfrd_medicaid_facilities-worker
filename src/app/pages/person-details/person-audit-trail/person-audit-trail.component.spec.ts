import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAuditTrailComponent } from './person-audit-trail.component';

describe('PersonAuditTrailComponent', () => {
  let component: PersonAuditTrailComponent;
  let fixture: ComponentFixture<PersonAuditTrailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonAuditTrailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAuditTrailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
