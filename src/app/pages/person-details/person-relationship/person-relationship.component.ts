import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataStoreService } from '../../../@core/services';
import { RelationshipPerson } from '../../../@core/common/models/involvedperson.data.model';
import { PersonDetailsService } from '../person-details.service';
import { RelationshipService } from './relationship.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'person-relationship',
  templateUrl: './person-relationship.component.html',
  styleUrls: ['./person-relationship.component.scss']
})
export class PersonRelationshipComponent implements OnInit {
  constructor(private router: Router,
    private personService: PersonDetailsService,
    private relationShipService: RelationshipService,
    private route: ActivatedRoute) {
    this.route.data.subscribe(res => {
      this.relationShipService.relativePersons = res.relations;
    });
  }

  ngOnInit() {
  }

}
