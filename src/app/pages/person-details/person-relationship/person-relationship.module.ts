import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';

import { PaginationModule } from 'ngx-bootstrap';

import { PersonRelationshipRoutingModule } from './person-relationship-routing.module';
import { PersonRelationshipComponent } from './person-relationship.component';
import { SearchRelationshipComponent } from './search-relationship/search-relationship.component';
import { SearchRelationshipResultComponent } from './search-relationship-result/search-relationship-result.component';
import { RelativePersonGridComponent } from './relative-person-grid/relative-person-grid.component';
import { FormMaterialModule } from '../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    PersonRelationshipRoutingModule,
    ImageCropperModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    FormMaterialModule,
    PaginationModule,
  ],
  declarations: [PersonRelationshipComponent, SearchRelationshipComponent, SearchRelationshipResultComponent, RelativePersonGridComponent]
})
export class PersonRelationshipModule { }
