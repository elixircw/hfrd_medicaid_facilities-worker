import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchRelationshipComponent } from './search-relationship.component';

describe('SearchRelationshipComponent', () => {
  let component: SearchRelationshipComponent;
  let fixture: ComponentFixture<SearchRelationshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchRelationshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchRelationshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
