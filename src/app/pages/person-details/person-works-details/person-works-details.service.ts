import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Employer } from '../../../@core/common/models/involvedperson.data.model';
import { CommonHttpService } from '../../../@core/services';
import { HttpService } from '../../../@core/services/http.service';
import { CommonUrlConfig } from '../../../@core/common/URLs/common-url.config';
import { PaginationRequest } from '../../../@core/entities/common.entities';

@Injectable()
export class PersonWorksDetailsService {

  employer$  = new Observable<Employer[]>();

  constructor(private _commonHttpService: CommonHttpService,
    private _httpService: HttpService) { }

    addUpdateEmployer(employer) {
      return this._commonHttpService.create(employer, CommonUrlConfig.EndPoint.PERSON.WORK.AddUpdateEmployer);
    }

    getEmployerList(paginationRequest: PaginationRequest, pageinfosize) {
      const source = this._commonHttpService.getPagedArrayList(paginationRequest,
        CommonUrlConfig.EndPoint.PERSON.WORK.ListEmployer + '?filter'
      ).map((result: any) => {
        return {
          data: result,
          count: result.length > 0 ? result[0].totalcount : 0,
          canDisplayPager: result.length > 0 ? result[0].totalcount > pageinfosize : false
        };
      }).share();
      this.employer$ = source.pluck('data');
      return source;
    }

    getCarrerList(paginationRequest: PaginationRequest, pageinfosize) {
      const source = this._commonHttpService.getPagedArrayList(paginationRequest,
        CommonUrlConfig.EndPoint.PERSON.WORK.ListCarrer + '?filter'
      ).map((result: any) => {
        return {
          data: result,
          count: result.length > 0 ? result[0].totalcount : 0,
          canDisplayPager: result.length > 0 ? result[0].totalcount > pageinfosize : false
        };
      }).share();
      return source;
    }

    deleteEmployer(employerid) {
      this._commonHttpService.endpointUrl = CommonUrlConfig.EndPoint.PERSON.WORK.DeleteEmployer;
      return this._commonHttpService.remove(employerid);
    }

    addUpdateCarrer(carrer) {
      return this._commonHttpService.create(carrer, CommonUrlConfig.EndPoint.PERSON.WORK.AddUpdateCarrer);
    }

}
