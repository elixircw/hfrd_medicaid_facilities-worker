import { Injectable } from '@angular/core';
import { ClientAppointmentsService } from './client-appointments.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PersonDetailsService } from '../person-details.service';

@Injectable()
export class ClientAppointmentsResolverService {

  constructor(private _service: ClientAppointmentsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const personid =  route.parent.parent.paramMap.get('personid');
    return this._service.getappointmentList(personid);
  }

}
