import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientAppointmentsService } from './client-appointments.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'person-client-appointments',
  templateUrl: './person-client-appointments.component.html',
  styleUrls: ['./person-client-appointments.component.scss']
})
export class PersonClientAppointmentsComponent implements OnInit {

  appointments = [];
  completionNotes = null;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private _service: ClientAppointmentsService) {
    this.route.data.subscribe(response => {
      this.appointments = response.appointmentList;
    });
  }

  ngOnInit() {
  }

  openAppointmentComments(appointment) {
    this.completionNotes = appointment.notes;
  }
  routeToCase(item) {
    const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/appointment';
    this.router.navigate([currentUrl]);
  }

}
