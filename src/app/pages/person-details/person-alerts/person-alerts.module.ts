import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonAlertsRoutingModule } from './person-alerts-routing.module';
import { PersonAlertsComponent } from './person-alerts.component';
import { PersonAlertListComponent } from './person-alert-list/person-alert-list.component';
import { PersonAlertDetailsComponent } from './person-alert-details/person-alert-details.component';
import { QuillModule } from 'ngx-quill';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
  imports: [
    CommonModule,
    PersonAlertsRoutingModule,
    QuillModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    PaginationModule
  ],
  declarations: [PersonAlertsComponent, PersonAlertListComponent, PersonAlertDetailsComponent]
})
export class PersonAlertsModule { }
