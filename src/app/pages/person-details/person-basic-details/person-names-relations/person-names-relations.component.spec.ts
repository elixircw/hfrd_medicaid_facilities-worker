import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonNamesRelationsComponent } from './person-names-relations.component';

describe('PersonNamesRelationsComponent', () => {
  let component: PersonNamesRelationsComponent;
  let fixture: ComponentFixture<PersonNamesRelationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonNamesRelationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonNamesRelationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
