import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonBasicDetailsComponent } from './person-basic-details.component';

describe('PersonBasicDetailsComponent', () => {
  let component: PersonBasicDetailsComponent;
  let fixture: ComponentFixture<PersonBasicDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonBasicDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonBasicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
