import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonBasicDetailsRoutingModule } from './person-basic-details-routing.module';
import { PersonNamesRelationsComponent } from './person-names-relations/person-names-relations.component';
import { PersonDemographicsComponent } from './person-demographics/person-demographics.component';
import { PersonBasicDetailsService } from './person-basic-details.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PersonBasicDetailsComponent } from './person-basic-details.component';
import {
  MatDatepickerModule, MatNativeDateModule, MatFormFieldModule,
  MatInputModule, MatSelectModule, MatButtonModule,
  MatRadioModule, MatTabsModule, MatCheckboxModule,
  MatListModule, MatCardModule, MatTableModule, MatExpansionModule,
  MatAutocompleteModule
} from '@angular/material';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    PersonBasicDetailsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    ImageCropperModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [PersonBasicDetailsComponent, PersonNamesRelationsComponent, PersonDemographicsComponent],
  providers: [PersonBasicDetailsService]
})
export class PersonBasicDetailsModule { }

