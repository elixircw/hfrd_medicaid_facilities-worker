import { ChangeDetectorRef, Component, OnInit, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PersonDetailsService } from '../person-details.service';

import { InvolvedPerson, UserProfileImage } from '../../../@core/common/models/involvedperson.data.model';
import { AlertService, AuthService, CommonHttpService, DataStoreService, GenericService, CommonDropdownsService } from '../../../@core/services';
import { PersonBasicDetailsService } from './person-basic-details.service';
import { NgxfUploaderService } from 'ngxf-uploader';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'person-basic-details',
  templateUrl: './person-basic-details.component.html',
  styleUrls: ['./person-basic-details.component.scss']
})
export class PersonBasicDetailsComponent implements OnInit {
  basicTabs = [];
  personid: string;

  constructor(private _service: GenericService<InvolvedPerson>,
    private route: ActivatedRoute,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _commonHttpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private personService: PersonDetailsService,
    private dropdownService: CommonDropdownsService,
    private _basicDetailsService: PersonBasicDetailsService,
    private _uploadService: NgxfUploaderService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.loadBasicTabs();
    this.personid = this.route.snapshot.parent.parent.paramMap.get('personid');
    this.loadPersonInfo();
  }

  loadBasicTabs() {
    this.basicTabs = [{
      path: 'demographics',
      name: 'Demographics'
    },
    {
      path: 'names-relations',
      name: 'Other Names / Relations'
    }];
  }

  private loadPersonInfo() {
    this.personService.getPersonDetails(this.personid).subscribe(response => {
      this.personService.person = response;
    });
  }
}
