import { Injectable } from '@angular/core';
import { CommonHttpService, GenericService } from '../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { CommonUrlConfig } from '../../../@core/common/URLs/common-url.config';

@Injectable()
export class PersonBasicDetailsService {
  constructor(private _service: GenericService<any>) { }

  addNickName(nickname): Observable<any> {
    return this._service.create(nickname, CommonUrlConfig.EndPoint.PERSON.AddNickname);
  }

  getNickNames(personid): Observable<any> {
    return this._service.getArrayList({
      method: 'get',
      where: {
        personid: personid
      }
    }, CommonUrlConfig.EndPoint.PERSON.NickNameList);
  }

  updateNickName(nickname): Observable<any> {
    return this._service.patchWithoutid(nickname, CommonUrlConfig.EndPoint.PERSON.UpdateNickName);
  }

  deleteNickName(nickname): Observable<any> {
    return this._service.patchWithoutid(nickname, CommonUrlConfig.EndPoint.PERSON.DeleteNickName);
  }

  addAliasName(aliasName): Observable<any> {
    return this._service.create(aliasName, CommonUrlConfig.EndPoint.PERSON.AddAliasName);
  }

  getAliasNames(personid): Observable<any> {
    return this._service.getArrayList({
      method: 'get',
      where: {
        personid: personid
      }
    }, CommonUrlConfig.EndPoint.PERSON.AliasNameList);
  }

  updateAliasName(aliasname): Observable<any> {
    return this._service.patchWithoutid(aliasname, CommonUrlConfig.EndPoint.PERSON.UpdateAliasName);
  }

  deleteAliasName(aliasname): Observable<any> {
    return this._service.patchWithoutid(aliasname, CommonUrlConfig.EndPoint.PERSON.DeleteAliasName);
  }

  updateBasicDetails(basicInfo): Observable<any> {
    return this._service.patchWithoutid(basicInfo, CommonUrlConfig.EndPoint.PERSON.UpdateBasicInfo);
  }

}
