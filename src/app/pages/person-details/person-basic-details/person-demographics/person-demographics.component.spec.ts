import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonDemographicsComponent } from './person-demographics.component';

describe('PersonDemographicsComponent', () => {
  let component: PersonDemographicsComponent;
  let fixture: ComponentFixture<PersonDemographicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonDemographicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDemographicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
