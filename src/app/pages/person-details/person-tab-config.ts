

export const PersonTabConfig = [{
    active: false, path: 'basic',
    name: 'Profile',
    agency: ['DJS', 'CW']
},
// DJS-015, DJS-016 Move the tabs in this order when creating a person -1. Profile 2. Person Role 3. Contact 4. Address
{
    active: false, path: 'contact-detail',
    name: 'Contacts',
    agency: ['DJS', 'CW']
},
{
    active: false, path: 'address',
    name: 'Address',
    agency: ['DJS', 'CW']
},
{
    active: false, path: 'alerts',
    name: 'Alerts',
    agency: ['DJS']
},
{
    active: false, path: 'health',
    name: 'Medical',
    agency: ['DJS', 'CW']
},
{
    active: false, path: 'education',
    name: 'Education',
    agency: ['DJS', 'CW']
},
{
    active: false, path: 'work',
    name: 'Work',
    agency: ['DJS', 'CW']
},
{
    active: false, path: 'relationship',
    name: 'Relations',
    agency: ['DJS', 'CW']
},
{
    active: false, path: 'legal-actions',
    name: 'Legal Actions History',
    agency: ['DJS']
}, {
    active: false, path: 'client-event-history',
    name: 'Client Event History',
    agency: ['DJS']
},
{
    active: false, path: 'restitution',
    name: 'Restitution',
    agency: ['DJS']
},
{
    active: false, path: 'client-family-info',
    name: 'Client Family Info',
    agency: ['DJS']
},
{
    active: false, path: 'client-appointments',
    name: 'Client Appointments',
    agency: ['DJS']
},
{
    active: false, path: 'client-summaries',
    name: 'Client Summary',
    agency: ['DJS']
},
{
    active: false, path: 'transport',
    name: 'Transport',
    agency: ['DJS']
},
{
    active: false, path: 'other-agencies',
    name: 'Other Agencies',
    agency: ['DJS']
},
{
    active: false, path: 'audit-trail',
    name: 'Audit Trail',
    agency: ['DJS']
},
{
    active: false, path: 'youth-status',
    name: 'Update Status',
    agency: ['DJS']
},
{
    active: false, path: 'status-summary',
    name: 'Status Summary',
    agency: ['DJS']
}
];
export const PersonStatusTabConfig = [{
    active: false, path: 'basic',
    name: 'Profile',
    agency: ['DJS']
},
{
    active: false, path: 'alerts',
    name: 'Alerts',
    agency: ['DJS']
},

{
    active: false, path: 'legal-actions',
    name: 'Legal Actions History',
    agency: ['DJS']
}
    , {
    active: false, path: 'assesments',
    name: 'Assessments',
    agency: ['DJS']
}, {
    active: false, path: 'client-summaries/placement',
    name: 'Placements',
    agency: ['DJS']
}, {
    active: false, path: 'documents',
    name: 'Documents',
    agency: ['DJS']
}, {
    active: false, path: 'client-summaries/review',
    name: 'Review',
    agency: ['DJS']
}, {
    active: false, path: 'investigation',
    name: 'Investigations',
    agency: ['DJS']
}, {
    active: false, path: 'client-summaries/contact-notes',
    name: 'Contact Summary',
    agency: ['DJS']
}, {
    active: false, path: 'physcial-attributes',
    name: 'Physical Attributes/Photo',
    agency: ['DJS']
}, {
    active: false, path: 'status-summary',
    name: 'Status Summary',
    agency: ['DJS']
}
];


