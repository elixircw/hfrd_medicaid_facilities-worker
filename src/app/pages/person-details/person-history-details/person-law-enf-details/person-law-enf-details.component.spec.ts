import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonLawEnfDetailsComponent } from './person-law-enf-details.component';

describe('PersonLawEnfDetailsComponent', () => {
  let component: PersonLawEnfDetailsComponent;
  let fixture: ComponentFixture<PersonLawEnfDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonLawEnfDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonLawEnfDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
