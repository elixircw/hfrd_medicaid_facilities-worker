import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonHistoryDetailsComponent } from './person-history-details.component';

describe('PersonHistoryDetailsComponent', () => {
  let component: PersonHistoryDetailsComponent;
  let fixture: ComponentFixture<PersonHistoryDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonHistoryDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonHistoryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
