import { Component, OnInit } from '@angular/core';
import { CompliantDetailsService } from '../compliant-details.service';

@Component({
  selector: 'petition-details',
  templateUrl: './petition-details.component.html',
  styleUrls: ['./petition-details.component.scss']
})
export class PetitionDetailsComponent implements OnInit {

  petitions = [];
  constructor(private complaintDetailsService: CompliantDetailsService) {
    if (this.complaintDetailsService.complaintDetails && this.complaintDetailsService.complaintDetails.petitions) {
      this.petitions = this.complaintDetailsService.complaintDetails.petitions;
    }
  }

  ngOnInit() {
  }

  toggleTable(id) {
    (<any>$('#' + id)).collapse('toggle');
  }


}
