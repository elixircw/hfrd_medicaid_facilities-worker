import { Component, OnInit } from '@angular/core';
import { CompliantDetailsService } from '../compliant-details.service';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'folder-details',
  templateUrl: './folder-details.component.html',
  styleUrls: ['./folder-details.component.scss']
})
export class FolderDetailsComponent implements OnInit {

  folders = [];
  constructor(private complaintDetailsService: CompliantDetailsService,
    private _router: Router) {
    if (this.complaintDetailsService.complaintDetails && this.complaintDetailsService.complaintDetails.folders) {
      this.folders = this.complaintDetailsService.complaintDetails.folders;
    }
  }

  ngOnInit() {
  }

  openFoder(item) {
    switch (item.foldertype) {
      case 'Intake':
      case 'LegalAction':
        this.routeToIntake(item.intakenumber);
        break;
      default:
        this.routeToCase(item.intakenumber);
    }
  }
  routeToIntake(intakenumber) {
    const currentUrl = '/pages/newintake/my-newintake/' + intakenumber;
    this._router.navigate([currentUrl]);
  }

  routeToCase(caseNumber) {
    // need case uuid to route
  }

}
