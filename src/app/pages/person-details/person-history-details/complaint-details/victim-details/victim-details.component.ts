import { Component, OnInit } from '@angular/core';
import { CompliantDetailsService } from '../compliant-details.service';

@Component({
  selector: 'victim-details',
  templateUrl: './victim-details.component.html',
  styleUrls: ['./victim-details.component.scss']
})
export class VictimDetailsComponent implements OnInit {

  victims = [];
  constructor(private complaintDetailsService: CompliantDetailsService) {
    if (this.complaintDetailsService.complaintDetails && this.complaintDetailsService.complaintDetails.victim) {
      this.victims = this.complaintDetailsService.complaintDetails.victim;
    }
  }

  ngOnInit() {
  }

}
