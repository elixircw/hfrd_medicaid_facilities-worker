import { Component, OnInit, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { PaginationInfo, DropdownModel, PaginationRequest } from '../../../@core/entities/common.entities';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonAssesmentsService } from './person-assesments.service';
import { AuthService, GenericService, CommonHttpService, SessionStorageService, AlertService, DataStoreService } from '../../../@core/services';
import {
  Assessments, GetintakAssessment,
  IntakeAssessmentRequestIds, IntakeDATypeDetail,
  AssessmentSummary, AllegationItem, InvolvedPerson,
  AssessmentScores
} from '../../newintake/my-newintake/_entities/newintakeModel';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { HttpService } from '../../../@core/services/http.service';
import { IntakeStoreConstants } from '../../newintake/my-newintake/my-newintake.constants';
import { NewUrlConfig } from '../../newintake/newintake-url.config';
import { AppConfig } from '../../../app.config';
import { environment } from '../../../../environments/environment';
import { AppUser } from '../../../@core/entities/authDataModel';
import { Observable } from 'rxjs/Observable';
import { EvaluationFields } from '../../newintake/my-newintake/_entities/newintakeSaveModel';
import { Subject } from 'rxjs/Subject';
declare var $: any;
declare var Formio: any;
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { AssessmentPreFill } from '../../newintake/my-newintake/intake-assessment/assessment-prefill';
import { AssessmentMode } from '../../newintake/my-newintake/intake-assessment/AssessmentMode';
import { PersonDetailsService } from '../person-details.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'person-assesments',
  templateUrl: './person-assesments.component.html',
  styleUrls: ['./person-assesments.component.scss']
})
export class PersonAssesmentsComponent implements OnInit {
  totalRecords: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  items = [];
  personid: string;
  id: string;
  daNumber: string;
  assessmmentName: string;
  private intakeDATypeDetail: IntakeDATypeDetail;
  role: AppUser;
  private assessmentRequestDetail: IntakeAssessmentRequestIds;
  startAssessment$: Observable<Assessments[]>;
  assessmentSummary$: Observable<AssessmentSummary[]>;
  totalRecords$: Observable<number>;
  daTypeDropDownItems$: Observable<DropdownModel[]>;
  daSubTypeDropDownItems$: Observable<DropdownModel[]>;
  filteredAllegationItems: AllegationItem[] = [];
  formBuilderUrl: string;
  safeUrl: SafeResourceUrl;
  submissionId: string;
  assessmentTemplateId: string;
  showAssesment = -1;
  getAsseesmentHistory: GetintakAssessment[] = [];
  private token: AppUser;
  formioOptions: {
    formio: {
      ignoreLayout: true;
      emptyValue: '-';
    };
  };
  templateComponentData: any;
  templateSubmissionData: any;
  getScoreOnClose: boolean;
  selectedPurpose: string;
  currentTemplateId: string;
  isReadOnlyForm = false;
  private assessmentMode: AssessmentMode;
  refreshForm: any;
  safeCKeys: string[];
  selectedSafeCDangerInfluence: any[] = [];
  intakeFormData: any;
  addedPersons: InvolvedPerson[] = [];
  evalFields: EvaluationFields;
  assmntScores: AssessmentScores = new AssessmentScores();
  store: any;
  agency = '';
  showAssmnt: boolean;
  intakeFormDRAIData = null;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private _personService: PersonAssesmentsService,
    private _personDetailsService: PersonDetailsService,
    private _authService: AuthService,
    private _service: GenericService<Assessments>,
    private _commonService: CommonHttpService,
    private storage: SessionStorageService,
    private _alertService: AlertService,
    public sanitizer: DomSanitizer,
    private _http: HttpService,
    private _cd: ChangeDetectorRef,
    private _dataStore: DataStoreService) {
    this.route.data.subscribe(response => {
      this.items = response.items.data;
      this.totalRecords = response.items.count;
    });
    this.personid = route.snapshot.parent.parent.paramMap.get('personid');
    this.store = this._dataStore.getCurrentStore();
  }

  ngOnInit() {
    this.agency = this._authService.getAgencyName();
    this.refreshForm = new EventEmitter();
    this.token = this._authService.getCurrentUser();

    // this.populateIntake();
    this.loadDropdownItems();
    this.role = this._authService.getCurrentUser();

    this.id = this.store[IntakeStoreConstants.intakenumber];
    // this.addedPersonsChanges$.subscribe(data =>
    {
      this.addedPersons = this.store[IntakeStoreConstants.addedPersons];
    }// );
    // this.evalFieldsOutputSubject$.subscribe(data =>
    {
      this.evalFields = this.store[IntakeStoreConstants.evalFields];
    }// );
    // this.getIntakeAssessmentDetails();
    // this.pageChanged({ itemsPerPage: 10, page: 1 });
    if (this._authService.isDJS()) {
      this.getIntakeDRAIAssessmentDetails();
    }
  }

  private loadDropdownItems() {
    this.daTypeDropDownItems$ = this._commonService
      .getArrayList(
        {
          nolimit: true,
          where: { activeflag: 1 },
          method: 'get'
        },
        NewUrlConfig.EndPoint.Intake.DATypeUrl + '?filter'
      )
      .map(result => {
        return result.map(res => new DropdownModel({ text: res.description, value: res.intakeservreqtypeid }));
      });
  }

  getPage(paginationInfo: PaginationInfo) {
    this.paginationInfo.where = {
      personid: this.personid, personstatus: this._personDetailsService.personStatus, agencycode: 'DJS',
      intakeservicerequesttypeid: '', intakeservicerequestsubtypeid: '',
      target: 'Intake'
    };
    this._personService.getAssesmentList(this.paginationInfo).subscribe((assessment: any) => {
      this.items = assessment.data;
      this.totalRecords = assessment.count;
    });
  }

  startAssessment(assessment) {
    this.showAssmnt = true;
    this.assessmmentName = assessment.titleheadertext;
    this.currentTemplateId = assessment.external_templateid;
    const _self = this;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`).then(function (form) {
      form.components = form.components.map((item) => {
        if (item.key === 'Complete' && item.type === 'button') {
          item.action = 'submit';
        }
        return item;
      });
      form.submission = {
        data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
      };
      form.on('submit', (submission) => {
        if (_self.assessmmentName === 'SAFE-C') {
          submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
        }
        _self._http
          .post('admin/assessment/Add', {
            externaltemplateid: _self.currentTemplateId,
            assessmentstatustypekey1: 'Submitted',
            objectid: _self.id,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0
          })
          .subscribe((response) => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            // _self.getPage(1);
            _self.redirectToAssessment();
            // (<any>$('#iframe-popup')).modal('hide');
          });
      });
      form.on('change', (formData) => {
        _self.safeCProcess(formData);
      });
      form.on('render', (formData) => {
        (<any>$('#iframe-popup')).modal('show');
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });

      form.on('error', (error) => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
        _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
      });
    });
  }
  redirectToAssessment() {
    this.showAssmnt = false;
    this.pageChanged({ itemsPerPage: 10, page: 1 });
  }

  updateAssessment(assessment: GetintakAssessment) {
    this.showAssmnt = true;
    this.assessmmentName = assessment.titleheadertext;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    // this.assessmmentName = assessment.description;
    // this.currentTemplateId = assessment.external_templateid;
    const _self = this;
    // Formio.setToken(this.storage.getObj('fbToken'));
    // Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
      readOnly: false
    }).then(function (form) {
      form.components = form.components.map(item => {
        if (item.key === 'Complete' && item.type === 'button') {
          item.action = 'submit';
        }
        return item;
      });
      form.submission = {
        data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
      };
      (<any>$('#iframe-popup')).modal('show');
      form.on('render', formData => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });
      form.on('submit', submission => {
        if (_self.assessmmentName === 'SAFE-C') {
          submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
        }
        let status = null;
        let comments = '';
        if (_self.token.role.name === 'apcs') {
          status = submission.data.assessmentstatus;
          comments = submission.data.supervisorcomments2;
        } else if (_self.token.role.name === 'field') {
          status = submission.data.assessmentreviewed;
          comments = submission.data.caseworkercomments;
        }
        _self._http
          .post('admin/assessment/Add', {
            externaltemplateid: _self.currentTemplateId,
            objectid: _self.id,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0,
            assessmentstatustypekey1: 'Submitted',
            // comments: comments
          })
          .subscribe(response => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            _self.redirectToAssessment();
            // _self.getPage(1);
            // _self.showAssessment(_self.showAssesment, _self.getAsseesmentHistory);
            // (<any>$('#iframe-popup')).modal('hide');
          });
      });
      form.on('change', formData => {
        _self.safeCProcess(formData);
      });
      // form.on('render', (formData) => {
      //     (<any>$('#iframe-popup')).modal('show');
      //     setTimeout(function () {
      //         $('#assessment-popup').scrollTop(0);
      //     }, 200);
      // });

      form.on('error', error => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
        _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
      });
    });
  }

  editAssessment(assessment: GetintakAssessment) {
    this.getScoreOnClose = false;
    this.assessmmentName = assessment.description;
    this.formBuilderUrl = environment.formBuilderHost + `/#/views/completeform?sid=${assessment.submissionid}&fid=${assessment.external_templateid}&da=`
      + this.id + `&ro=false&t=` + this.token.id;
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.formBuilderUrl);
  }

  submittedAssessment(assessment: GetintakAssessment) {
    this.assessmmentName = assessment.titleheadertext;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
      readOnly: true
    }).then(function (submission) {
      (<any>$('#iframe-popup')).modal('show');
      submission.on('render', (formData) => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });
    });
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.getPage(this.paginationInfo);
  }

  assessmentPrintView(assessment: GetintakAssessment) {
    const _self = this;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
      readOnly: true
    }).then(function (submission) {
      const options = {
        ignoreLayout: true
      };
      _self.viewHtml(submission._form, submission._submission, options);
    });
  }

  private safeCProcess($event) {
    if (this.assessmmentName === 'SAFE-C') {
      if ($event.changed) {
        const dangerInfluenceKey = $event.changed.component.key;
        if (dangerInfluenceKey && this.safeCKeys.indexOf(dangerInfluenceKey) > -1) {
          const dangerInflunceItem = this.selectedSafeCDangerInfluence.find((item) => item.value === dangerInfluenceKey);
          if (dangerInflunceItem) {
            if ($event.changed.value === 'no' || $event.data[$event.changed.component.key] === 'no') {
              const itemIndex = this.selectedSafeCDangerInfluence.indexOf(dangerInflunceItem);
              this.selectedSafeCDangerInfluence.splice(itemIndex, 1);
            }
            // console.log(this.selectedSafeCDangerInfluence);
          } else {
            if ($event.changed.value === 'yes' || $event.data[$event.changed.component.key] === 'yes') {
              this.selectedSafeCDangerInfluence.push({
                text: $event.changed.component.label,
                value: dangerInfluenceKey
              });
              // console.log(this.selectedSafeCDangerInfluence);
            }
          }
        }
      }
    } else {
      this.selectedSafeCDangerInfluence = [];
    }
  }

  onCustomEvent($event) {
    // console.log($event);
  }

  viewHtml(componentData, submissionData, formioOptions) {
    delete submissionData._id;
    delete submissionData.owner;
    delete submissionData.modified;
    const exporter = new FormioExport(componentData, submissionData, formioOptions);
    const appDiv = document.getElementById('divPrintView');
    exporter.toHtml().then((html) => {
      html.style.margin = 'auto';
      const iframe = this.createIframe(appDiv);
      const doc = iframe.contentDocument || iframe.contentWindow.document;
      doc.body.appendChild(html);
      window.frames['ifAssessmentView'].focus();
      window.frames['ifAssessmentView'].print();
    });
  }

  private createIframe(el) {
    _.forEach(el.getElementsByTagName('iframe'), (_iframe) => {
      el.removeChild(_iframe);
    });
    const iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'ifAssessmentView');
    iframe.setAttribute('name', 'ifAssessmentView');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('webkitallowfullscreen', '');
    iframe.setAttribute('mozallowfullscreen', '');
    iframe.setAttribute('allowfullscreen', '');
    iframe.setAttribute('style', 'width: -webkit-fill-available;height: -webkit-fill-available;');
    el.appendChild(iframe);
    return iframe;
  }

  showAssessment(id: number, row) {
    this.getAsseesmentHistory = row;
    if (this.showAssesment !== id) {
      this.showAssesment = id;
    } else {
      this.showAssesment = -1;
    }
  }

  private getFormPrePopulation(formName: string, submissionData: any) {
    const prefillUtil = new AssessmentPreFill(this.addedPersons, this.evalFields, this._authService);
    switch (formName.toUpperCase()) {
      case 'MARYLAND FAMILY INITIAL  RISK ASSESSMENT':
        // submissionData = prefillUtil.fillMFRA(submissionData, this.daNumber);
        break;

      case 'MARYLAND FAMILY RISK REASSESSMENT':
        // submissionData = prefillUtil.fillMFRR(submissionData, this.daNumber);

        break;

      case 'SAFE-C':
        // submissionData = prefillUtil.fillSafeC(submissionData, this.daNumber);
        break;

      case 'CANS-F':
        // submissionData = prefillUtil.fillCansF(submissionData, this.daNumber);
        break;
      case 'HOME HEALTH REPORT':
        // submissionData = prefillUtil.fillHomeHealthReport(submissionData, this.daNumber);
        break;
      case 'SAFE-C OHP':
        // submissionData = prefillUtil.fillSafeCOHP(submissionData, this.daNumber);
        break;
      case 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT':
        // submissionData = prefillUtil.fillTransportationPlan(submissionData, this.daNumber);
        break;
      case 'BEST INTEREST DETERMINATION FORM':
        // submissionData = prefillUtil.fillBestInterestDetermination(submissionData, this.daNumber, this.token.user.userprofile.displayname);
        break;
      case 'INTAKE DETENTION RISK ASSESSMENT INSTRUMENT':
        submissionData = prefillUtil.fillIntakeDetentnRiskAssmntInstrument(submissionData, this.intakeFormDRAIData);
        break;
      case 'DOMESTIC VIOLENCE LETHALITY ASSESSMENT':
        submissionData = prefillUtil.fillDomsticViolncLethalityAssmnt(submissionData);
        break;
      case 'DMST SCREENING TOOL':
        submissionData = prefillUtil.fillDMSTScreeningTool(submissionData);
        break;
      case 'MCASP RISK ASSESSMENT':
        submissionData = prefillUtil.fillMCASPriskAssment(submissionData);
    }
    return submissionData;
  }

  getIntakeDRAIAssessmentDetails() {
    if (this.addedPersons && this.addedPersons.length) {
      const personid = this.addedPersons.filter(data => data.Role === 'Youth');
      this._commonService
        .getArrayList(
          {
            method: 'get',
            where: {
              personid: personid && personid.length ? personid[0].Pid : ''
            }
          },
          'Intakeservicerequests/prepopasmtdrai?filter'
        )
        .subscribe((response) => {
          this.intakeFormDRAIData = response;
        });
    }
  }

  private populateIntake() {
    this._commonService
      .getArrayList(
        {
          method: 'get',
          where: {
            servicerequestid: this.id
          }
        },
        'Intakedastagings/getintakesnapshot?filter'
      )
      .subscribe((response) => {
        // console.log(JSON.stringify(response));
        this.intakeFormData = response[0];
      });
  }

  private getSubmittedAssessmentForm(external_templateid: string, submissionid: string) {
    const templateUrl = environment.formBuilderHost + `/form/${external_templateid}`;
    const submissionUrl = environment.formBuilderHost + `/form/${external_templateid}/submission/${submissionid}`;
    const fbToken = this.storage.getObj('fbToken');
    this._http.setHeader('x-jwt-token', `${fbToken}`);
    this._http.overrideUrl = true;
    return this._http.get(templateUrl).flatMap((data) => {
      this.templateComponentData = data;
      return this._http.get(submissionUrl);
    });
  }

  private getAssessmentForm(external_templateid: string) {
    const templateUrl = environment.formBuilderHost + `/form/${external_templateid}`;
    const fbToken = this.storage.getObj('fbToken');
    this._http.setHeader('x-jwt-token', `${fbToken}`);
    this._http.overrideUrl = true;
    return this._http.get(templateUrl);
  }

  onFormSubmit($event) {
    const submissionUrl = environment.formBuilderHost + `/form/${this.currentTemplateId}/submission?live=1`;
    const fbToken = this.storage.getObj('fbToken');
    this._http.setHeader('x-jwt-token', `${fbToken}`);
    this._http.overrideUrl = true;
    let submittedForm = {};
    if (this.assessmmentName === 'SAFE-C') {
      $event.data['safeCDangerInfluence'] = this.selectedSafeCDangerInfluence;
    }
    // console.log($event);
    return this._http
      .post(submissionUrl, $event)
      .flatMap(
        (data) => {
          console.log(data);
          submittedForm = {
            externaltemplateid: this.currentTemplateId,
            assessmentstatustypekey1: 'Submitted',
            objectid: this.id,
            submissionid: data._id,
            submissiondata: $event.data ? $event.data : null,
            form: data.form ? data.form : null,
            score: $event.data.score ? $event.data.score : 0
          };
          console.log(submittedForm);
          this._http.overrideUrl = false;
          this._http.baseUrl = AppConfig.baseUrl;
          return this._http.post(NewUrlConfig.EndPoint.Intake.SubmitAssessment, submittedForm);
        },
        (error) => {
          console.log(error);
        }
      )
      .subscribe((response) => {
        console.log(response);
        this._alertService.success(this.assessmmentName + ' saved successfully.');
        (<any>$('#iframe-popup')).modal('hide');
        this.closeAssessment();
      });
  }
  onFormRendered($event) {
    console.log($event);
    $('#iframe-popup').removeClass(' intake radio-inline');
  }
  onFormInvalid($event) {
    console.log($event);
  }
  onFormChange($event) {
    if (this.assessmmentName === 'SAFE-C') {
      if ($event.changed) {
        const dangerInfluenceKey = $event.changed.component.key;
        if (dangerInfluenceKey && this.safeCKeys.indexOf(dangerInfluenceKey) > -1) {
          const dangerInflunceItem = this.selectedSafeCDangerInfluence.find((item) => item.value === dangerInfluenceKey);
          if (dangerInflunceItem) {
            if ($event.changed.value === 'no' || $event.data[$event.changed.component.key] === 'no') {
              const itemIndex = this.selectedSafeCDangerInfluence.indexOf(dangerInflunceItem);
              this.selectedSafeCDangerInfluence.splice(itemIndex, 1);
            }
            console.log(this.selectedSafeCDangerInfluence);
          } else {
            if ($event.changed.value === 'yes' || $event.data[$event.changed.component.key] === 'yes') {
              this.selectedSafeCDangerInfluence.push({
                text: $event.changed.component.label,
                value: dangerInfluenceKey
              });
              console.log(this.selectedSafeCDangerInfluence);
            }
          }
        }
      }
    } else {
      this.selectedSafeCDangerInfluence = [];
    }
  }


  closeAssessment() {
    // this.getPage(1);
    this.pageChanged({ itemsPerPage: 10, page: 1 });
  }



  navigationToSdm() {
    (<any>$('#sdm-tab')).click();
  }

  printView() {
    window.frames['ifAssessmentView'].focus();
    window.frames['ifAssessmentView'].print();
  }

  viewPdf() {
    const exporter = new FormioExport(this.templateComponentData, this.templateSubmissionData, this.formioOptions);
    const appDiv = document.getElementById('divPrintView');
    const formioPdfConfig = {
      download: false,
      filename: this.assessmmentName + '.pdf',
      html2canvas: {
        logging: true,
        onclone: doc => {
          // You can modify the html before converting it to canvas (add additional page breaks, etc)
          console.log('html cloned!', doc);
        },
        onrendered: canvas => {
          // You can access the canvas before converting it to PDF
          console.log('html rendered!', canvas);
        }
      }
    };

    exporter.toPdf(formioPdfConfig).then(pdf => {
      console.log('pdf ready', pdf);
      const iframe = this.createIframe(appDiv);
      iframe.src = pdf.output('datauristring');
    });
  }

  private getIntakeAssessmentDetails() {
    this.assessmentRequestDetail = Object.assign({});
    this.token = this._authService.getCurrentUser();
    const assessmentRequest = new IntakeAssessmentRequestIds();
    assessmentRequest.intakeservicerequesttypeid = '';
    assessmentRequest.intakeservicerequestsubtypeid = '';
    assessmentRequest.agencycode = this.agency;
    assessmentRequest.intakenumber = this.id;
    assessmentRequest.target = 'Intake';
    this.assessmentRequestDetail = assessmentRequest;
    this.pageChanged({ itemsPerPage: 10, page: 1 });
  }



  // getSavedIntakeAssessmentDetails(daDetails: IntakeDATypeDetail[], intakeNo: string) {
  //     this.token = this._authService.getCurrentUser();
  //     // this.intakeNumber = intakeNo;
  //     daDetails.map(item => {
  //         const savedAssessmentRequest = new IntakeAssessmentRequestIds();
  //         savedAssessmentRequest.intakeservicerequesttypeid = '';
  //         savedAssessmentRequest.intakeservicerequestsubtypeid = '';
  //         savedAssessmentRequest.agencycode = item.agencycode;
  //         savedAssessmentRequest.intakenumber = this.id;
  //         savedAssessmentRequest.target = 'Intake';
  //         this.assessmentRequestDetail = savedAssessmentRequest;
  //     });
  //     this.getPage(1);
  // }
  onDASubTypeChange(option: any) {
    if (option.value) {
      this.intakeDATypeDetail.DasubtypeKey = option.value;
      this.intakeDATypeDetail.DasubtypeText = option.label;
    }
  }

  onDATypeChange(option: any) {
    this.intakeDATypeDetail = new IntakeDATypeDetail();
    this.intakeDATypeDetail.DaTypeKey = option.value;
    this.intakeDATypeDetail.DaTypeText = option.label;
    const url = NewUrlConfig.EndPoint.Intake.DATypeUrl + '?filter';
    this.daSubTypeDropDownItems$ = this._commonService
      .getArrayList(
        {
          include: 'servicerequestsubtype',
          where: { intakeservreqtypeid: this.intakeDATypeDetail.DaTypeKey },
          method: 'get',
          nolimit: true
        },
        url
      )
      .map(data => {
        return data[0].servicerequestsubtype.map(res => new DropdownModel({ text: res.description, value: res.servicerequestsubtypeid }));
      });
  }

  getDRAIScore(result) {
    if (result && result.data) {
      result.data.forEach(element => {
        if (element.titleheadertext === 'Intake Detention Risk Assessment Instrument') {
          if (element.intakassessment && element.intakassessment.length > 0) {
            const latestDrai = element.intakassessment[0].submissiondata;
            const scores = {
              score: latestDrai.score,
              value: latestDrai.value,
              AD: latestDrai.AD,
              SD: latestDrai.SD,
              SD2: latestDrai.SD2
            };
            this.assmntScores.DRAI = scores;
            // this.scoresSubject$.next(this.assmntScores);
            this._dataStore.setData(IntakeStoreConstants.assessmentScore, this.assmntScores);
          }
        }
      });
    }
  }
  getMCAPScore(result) {
    if (result && result.data) {
      result.data.forEach(element => {
        if (element.titleheadertext === 'MCASP Risk Assessment') {
          if (element.intakassessment && element.intakassessment.length > 0) {
            const latestMcasp = element.intakassessment[0].submissiondata;
            const scores = {
              dhs: latestMcasp.dhs1,
              shs: latestMcasp.shs2,
              risklevel: latestMcasp.risklevel
            };
            this.assmntScores.MCASP = scores;
            // this.scoresSubject$.next(this.assmntScores);
            this._dataStore.setData(IntakeStoreConstants.assessmentScore, this.assmntScores);
          }
        }
      });
    }
  }

  isAssmentCompelete(submissionData: any) {
    if (submissionData) {
      if (submissionData.Complete === true) {
        return 1;
      }
      if (submissionData.submit === true) {
        return 2;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }
}
