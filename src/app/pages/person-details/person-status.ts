export interface PersonStatus {
    status: string;
    personStatusChanged(e);
}
