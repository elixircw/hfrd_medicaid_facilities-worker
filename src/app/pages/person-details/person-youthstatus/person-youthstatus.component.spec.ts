import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonYouthstatusComponent } from './person-youthstatus.component';

describe('PersonYouthstatusComponent', () => {
  let component: PersonYouthstatusComponent;
  let fixture: ComponentFixture<PersonYouthstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonYouthstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonYouthstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
