import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonEducationDetailsComponent } from './person-education-details.component';

describe('PersonEducationDetailsComponent', () => {
  let component: PersonEducationDetailsComponent;
  let fixture: ComponentFixture<PersonEducationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonEducationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonEducationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
