import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationRequest } from '../../../@core/entities/common.entities';
import { AppUser } from '../../../@core/entities/authDataModel';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { CommonHttpService, AlertService, AuthService, DataStoreService, CommonDropdownsService } from '../../../@core/services';
import { Education, School, Vocation, Testing, Accomplishment } from '../../../@core/common/models/involvedperson.data.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonUrlConfig } from '../../../@core/common/URLs/common-url.config';
import { AppConfig } from '../../../app.config';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { NewUrlConfig } from '../../newintake/newintake-url.config';
import { PersonEducationDetailsService } from './person-education-details.service';
import { PersonDetailsService } from '../person-details.service';
const TEST_LEVEL_BASIC = 'Basic';
const TEST_LEVEL_ADVANCED = 'Advanced';
import { config } from '../../../../environments/config';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'person-education-details',
  templateUrl: './person-education-details.component.html',
  styleUrls: ['./person-education-details.component.scss']
})
export class PersonEducationDetailsComponent implements OnInit {
  // @Input()
  // addEducationSubject$ = new Subject<Education>();
  // @Input()
  // addEducationOutputSubject$ = new Subject<Education>();
  // @Input()
  // educationFormReset$ = new Subject<boolean>();
  addEducation: Education;
  schoolForm: FormGroup;
  vocationForm: FormGroup;
  testingForm: FormGroup;
  accomplishmentForm: FormGroup;
  vocationButton: boolean;
  school: School[] = [];
  vocation: Vocation[] = [];
  testing: Testing[] = [];
  accomplishment: Accomplishment[] = [];
  vocationEditInd = 'ADD';
  schoolEditInd = 'ADD';
  testingEditInd = 'ADD';
  accomplishEditInd = 'ADD';
  isSpecialEducation = false;
  schoolTypeDropdownItems$: Observable<DropdownModel[]>;
  specialEducationDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  lastGradeDropdownItems$: Observable<DropdownModel[]>;
  currentGradeDropdownItems$: Observable<DropdownModel[]>;
  testingTypeDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems: DropdownModel[] = [];
  testingDescription: string;
  gradeDescription: string;
  schoolDescription: string;
  maxDate = new Date();
  uploadedFile: File;
  private token: AppUser;
  private personId: string;
  schoolSearchList$ = new Observable<any>();
  schoolList$ = new Observable<any>();
  accomplishmentList$ = new Observable<any>();
  vocationList$ = new Observable<any>();
  basicTestingList$ = new Observable<any>();
  advancedTestingList$ = new Observable<any>();
  baseUrl = '';
  schoolResourceID: string;
  schoolreportMode: string;
  schoolxpandStatus = false;
  accompResourceID: string;
  accompreportMode: string;
  accompxpandStatus = false;
  testingResourceID: string;
  testingreportMode: string;
  testingxpandStatus = false;
  vocationResourceID: string;
  vocationreportMode: string;
  vocationxpandStatus = false;
  schoolid: string;
  isBasicMode = true;
  testLevels = [TEST_LEVEL_BASIC, TEST_LEVEL_ADVANCED];
  constructor(
    private formbulider: FormBuilder,
    private _uploadService: NgxfUploaderService,
    private _commonHttpService: CommonHttpService,
    private _alertSevice: AlertService,
    private _authService: AuthService,
    private http: HttpClient,
    private _dataStore: DataStoreService,
    private _commonDropdownsService: CommonDropdownsService,
    private _educationService: PersonEducationDetailsService,
    private _personDetailService: PersonDetailsService
  ) {
    this.baseUrl = AppConfig.baseUrl;
    this.token = this._authService.getCurrentUser();
  }

  ngOnInit() {
    this.personId = this._personDetailService.person.personid;
    this.schoolSearchList$ = Observable.empty();
    this.schoolList$ = Observable.empty();
    this.accomplishmentList$ = Observable.empty();
    this.basicTestingList$ = Observable.empty();
    this.advancedTestingList$ = Observable.empty();
    this.vocationList$ = Observable.empty();
    this.schoolreportMode = 'add';
    this.accompreportMode = 'add';
    this.testingreportMode = 'add';
    this.vocationreportMode = 'add';
    this.schoolForm = this.formbulider.group({
      personeducationid: [''],
      personid: [''],
      educationname: ['', Validators.required],
      educationtypekey: [null, Validators.required],
      countyid: ['', Validators.required],
      statecode: ['', Validators.required],
      startdate: [null, Validators.required],
      enddate: [null],
      address: [''],
      zip: [''],
      lastgradetypekey: [null, Validators.required],
      currentgradetypekey: [null, Validators.required],
      isspecialeducation: [false],
      specialeducation: [''],
      specialeducationtypekey: [null],
      absentdate: [null],
      isreceived: [false],
      isverified: [false],
      isexcuesed: [false],
      reciveVeriExc: [''],
      extracurricular: ['']
    });

    this.testingForm = this.formbulider.group({
      personeducationtestingid: [''],
      personid: [''],
      testingtypekey: [null],
      testinginfotype: [TEST_LEVEL_BASIC],
      readinglevel: [null],
      readingtestdate: [null],
      mathlevel: [null],
      mathtestdate: [null],
      nameoftester: [null],
      pretestdate: [null],
      pretestscore: [null],
      posttestdate: [null],
      posttestscore: [null],
      testingprovider: ['']
    });

    this.accomplishmentForm = this.formbulider.group({
      personaccomplishmentid: [''],
      personid: [''],
      highestgradetypekey: [null],
      accomplishmentdate: [null],
      isrecordreceiveds: ['', [Validators.required]],
      receiveddate: [null]
    });

    this.vocationForm = this.formbulider.group({
      personeducationvocationid: [''],
      personid: [''],
      isvocationaltests: [''],
      vocationinterest: [''],
      vocationaptitude: [''],
      certificatename: [''],
      certificatepath: [''],
      uploadFile: ['']
    });
    this.loadDropDown();
    this.getSchoolList();
    this.getAccomplishmentList();
    this.getBasicTestingList();
    this.getAdvancedTestingList();
    this.getVocationList();
    this.addEducation = {
      school: [],
      testing: [],
      accomplishment: [],
      vocation: [],
      personId: this.personId
    };
    // this.educationFormReset$.subscribe((res) => {
    //     if (res === true) {
    //         this.schoolForm.reset();
    //         this.testingForm.reset();
    //         this.accomplishmentForm.reset();
    //         this.vocationForm.reset();
    //     }
    // });
    // this.addEducationOutputSubject$.subscribe((education) => {
    //     this.personId = education.personId;
    //     this.school = education.school ? education.school : [];
    //     this.accomplishment = education.accomplishment ? education.accomplishment : [];
    //     this.testing = education.testing ? education.testing : [];
    //     this.vocation = education.vocation ? education.vocation : [];
    // });
    // this.addEducationSubject$.next(this.addEducation);
  }

  checkTestMode() {
    const testForm = this.testingForm.getRawValue();
    if (testForm.testinginfotype === TEST_LEVEL_BASIC) {
      this.isBasicMode = true;
    } else {
      this.isBasicMode = false;
    }

  }

  sourceSelected(school) {
    this.loadCounty(school.statekey, school.countyid);
    setTimeout(() => {
      this.schoolid = school.schoolid;
      this.schoolForm.patchValue({
        educationtypekey: school.schooltypekey,
        statecode: school.statekey,
        address: school.address,
        zip: school.zipcode
      });
    }, 1000);
  }

  private loadCounty(countystate, countyid) {
    this._commonDropdownsService.getCountyList(countystate).subscribe(result => {
      this.countyDropDownItems = result;
      this.schoolForm.patchValue({
        countyid: countyid
      });
    });
  }

  schoolTypeDescription(model) {
    this.schoolDescription = '';
    this.schoolDescription = model.text;
  }

  addSchool(model) {
    model.personeducationid = this.schoolResourceID;
    if (this.schoolDescription && this.schoolDescription !== '') {
      model.schoolTypeDescription = this.schoolDescription;
    }
    model.isspecialeducation = this.isSpecialEducation;
    model.personid = this.personId;
    model.schoolid = this.schoolid;
    this._educationService.addUpdateSchool(model).subscribe(result => {
      this.getSchoolList();
      this.schoolEditInd = 'ADD';
      this.schoolResourceID = null;
      this.schoolid = null;
      this.schoolForm.reset();
      this._alertSevice.success('School details saved successfully!');
    }, error => {
      this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  getSchoolList() {
    const source = this._educationService.getSchoolList(new PaginationRequest(
      {
        method: 'get',
        where: { personid: this.personId },
        page: 1,
        limit: 10,
      }), 10);

    this.schoolList$ = source.pluck('data');
  }

  editSchool(model, index) {
    this.schoolForm.patchValue(model);
    setTimeout(() => {
      this.schoolForm.patchValue({
        specialeducation: [model.isspecialeducation],
        address: model.school.address,
        zip: model.school.zipcode
      });
    }, 1000);
    this.schoolResourceID = model.personeducationid;
    this.schoolid = model.schoolid;
    this.schoolEditInd = 'UPDATE';
    this.specialEducation(model.isspecialeducation);
    this.schoolTypeDescription({ text: model.schoolTypeDescription, value: model.currentgradetypekey });
    this.schoolForm.enable();
    this.schoolreportMode = 'edit';
    this.loadCounty(model.statecode, model.countyid);
    if (!this.schoolxpandStatus) {
      this.schoolxpandStatus = true;
      this.accompxpandStatus = false;
      this.vocationxpandStatus = false;
      this.testingxpandStatus = false;
    }
  }
  viewSchool(model) {
    this.schoolForm.patchValue(model);
    setTimeout(() => {
      this.schoolForm.patchValue({
        specialeducation: [model.isspecialeducation],
        address: model.school.address,
        zip: model.school.zipcode
      });
    }, 1000);
    this.schoolResourceID = model.personeducationid;
    this.specialEducation(model.isspecialeducation);
    this.schoolreportMode = 'view';
    this.loadCounty(model.statecode, model.countyid);
    if (!this.schoolxpandStatus) {
      this.schoolxpandStatus = true;
      this.accompxpandStatus = false;
      this.vocationxpandStatus = false;
      this.testingxpandStatus = false;
    }
    this.schoolForm.disable();
  }
  showDeletePopSchool(resourceid) {
    this.schoolResourceID = resourceid;
    (<any>$('#delete-schoolInfo-popup')).modal('show');
  }
  deleteSchool() {
    this._educationService.deleteSchool(this.schoolResourceID).subscribe(
      response => {
        this.getSchoolList();
        this.schoolResourceID = null;
        this.schoolid = null;
        this.schoolEditInd = 'ADD';
        this.schoolForm.reset();
        this._alertSevice.success('School deleted successfully');
        (<any>$('#delete-schoolInfo-popup')).modal('hide');
      },
      error => {
        this.schoolResourceID = null;
        this.schoolid = null;
        this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-schoolInfo-popup')).modal('hide');
      }
    );
  }

  addUpdateAccomplishment(model) {
    model.personaccomplishmentid = this.accompResourceID;
    model.personid = this.personId;
    if (this.gradeDescription && this.gradeDescription !== '') {
      model.gradedescription = this.gradeDescription;
    }
    model.isrecordreceived = model.isrecordreceiveds;
    this._educationService.addAccomplishmentSchool(model).subscribe(result => {
      this.getAccomplishmentList();
      this.accomplishEditInd = 'ADD';
      this.accompResourceID = null;
      this.accomplishmentForm.reset();
      this._alertSevice.success('Accomplishments details saved successfully!');
    }, error => {
      this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  getAccomplishmentList() {
    const source = this._educationService.getAccomplishmentList(new PaginationRequest(
      {
        method: 'get',
        where: { personid: this.personId },
        page: 1,
        limit: 10,
      }), 10);

    this.accomplishmentList$ = source.pluck('data');
  }

  editAccomplishment(model, index) {
    this.accomplishmentForm.patchValue(model);
    setTimeout(() => {
      this.accomplishmentForm.patchValue({
        isrecordreceiveds: [model.isrecordreceived]
      });
    }, 100);
    this.highetGradeDescription(model.highestgradetypekey);
    if (model.isrecordreceived) {
      this.setManditory(!model.isrecordreceived, 'receiveddate', 'isManditory');
    } else {
      this.setManditory(!model.isrecordreceived, 'receiveddate', 'notManditory');
    }
    this.accompResourceID = model.personaccomplishmentid;
    this.accomplishEditInd = 'UPDATE';
    this.accomplishmentForm.enable();
    this.accompreportMode = 'edit';
    if (!this.accompxpandStatus) {
      this.accompxpandStatus = true;
      this.schoolxpandStatus = false;
      this.vocationxpandStatus = false;
      this.testingxpandStatus = false;
    }
  }
  viewAccomplishment(model) {
    this.accomplishmentForm.patchValue(model);
    setTimeout(() => {
      this.accomplishmentForm.patchValue({
        isrecordreceiveds: [model.isrecordreceived]
      });
    }, 100);
    this.highetGradeDescription(model.highestgradetypekey);
    if (model.isrecordreceived) {
      this.setManditory(!model.isrecordreceived, 'receiveddate', 'isManditory');
    } else {
      this.setManditory(!model.isrecordreceived, 'receiveddate', 'notManditory');
    }
    this.accompResourceID = model.personaccomplishmentid;
    this.accompreportMode = 'view';
    if (!this.accompxpandStatus) {
      this.accompxpandStatus = true;
      this.schoolxpandStatus = false;
      this.vocationxpandStatus = false;
      this.testingxpandStatus = false;
    }
    this.accomplishmentForm.disable();
  }
  showDeletePopAccomplishments(resourceid) {
    this.accompResourceID = resourceid;
    (<any>$('#delete-accomplishmentInfo-popup')).modal('show');
  }
  deleteAccomplishment() {
    this._educationService.deleteAccomplishment(this.accompResourceID).subscribe(
      response => {
        this.getAccomplishmentList();
        this.accompResourceID = null;
        this.accomplishEditInd = 'ADD';
        this.accomplishmentForm.reset();
        this._alertSevice.success('Accomplishment deleted successfully');
        (<any>$('#delete-accomplishmentInfo-popup')).modal('hide');
      },
      error => {
        this.accompResourceID = null;
        this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-accomplishmentInfo-popup')).modal('hide');
      }
    );
  }

  addUpdateTesting(model) {
    model.personeducationtestingid = this.testingResourceID;
    model.personid = this.personId;
    if (this.testingDescription && this.testingDescription !== '') {
      model.testdescription = this.testingDescription;
    }
    this._educationService.addTesting(model).subscribe(result => {
      if (model.testinginfotype === TEST_LEVEL_BASIC) {
        this.getBasicTestingList();
      } else {
        this.getAdvancedTestingList();
      }
      this.testingEditInd = 'ADD';
      this.testingResourceID = null;
      this.testingForm.reset();
      this._alertSevice.success('Testing Info details saved successfully!');
    }, error => {
      this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  getAdvancedTestingList() {
    this.advancedTestingList$ = this.getTestingList(TEST_LEVEL_ADVANCED);
  }

  getBasicTestingList() {
    this.basicTestingList$ = this.getTestingList(TEST_LEVEL_BASIC);
  }
  getTestingList(testLevel) {
    const source = this._educationService.getTestingList(new PaginationRequest(
      {
        method: 'get',
        where: { personid: this.personId, testinginfotype: testLevel },
        page: 1,
        limit: 10,
      }), 10);

    return source.pluck('data');
  }

  editTesting(model, index) {
    setTimeout(() => {
      this.testingForm.patchValue(model);
      this.checkTestMode();
    }, 100);
    this.selectTestingDescription({ text: model.testdescription, value: model.testingtypekey });
    this.testingResourceID = model.personeducationtestingid;
    this.testingEditInd = 'UPDATE';
    this.testingForm.enable();
    this.testingreportMode = 'edit';
    if (!this.testingxpandStatus) {
      this.testingxpandStatus = true;
      this.schoolxpandStatus = false;
      this.vocationxpandStatus = false;
      this.accompxpandStatus = false;
    }
  }
  viewTesting(model) {
    setTimeout(() => {
      this.testingForm.patchValue(model);
    }, 100);
    this.selectTestingDescription({ text: model.testdescription, value: model.testingtypekey });
    this.testingResourceID = model.personeducationtestingid;
    this.testingreportMode = 'view';
    if (!this.testingxpandStatus) {
      this.testingxpandStatus = true;
      this.schoolxpandStatus = false;
      this.vocationxpandStatus = false;
      this.accompxpandStatus = false;
    }
    this.testingForm.disable();
  }
  showDeletePopTesting(resourceid) {
    this.testingResourceID = resourceid;
    (<any>$('#delete-testingInfo-popup')).modal('show');
  }
  deleteTesting() {
    this._educationService.deleteTesting(this.testingResourceID).subscribe(
      response => {
        // todo list based upon level delete
        this.getAdvancedTestingList();
        this.getBasicTestingList();
        this.testingResourceID = null;
        this.testingEditInd = 'ADD';
        this.testingForm.reset();
        this._alertSevice.success('Testing Info deleted successfully');
        (<any>$('#delete-testingInfo-popup')).modal('hide');
      },
      error => {
        this.testingResourceID = null;
        this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-testingInfo-popup')).modal('hide');
      }
    );
  }

  addUpdateVocation(model) {
    model.personeducationvocationid = this.vocationResourceID;
    model.personid = this.personId;
    model.isvocationaltest = model.isvocationaltests;
    console.log(model);
    this._educationService.addVocation(model).subscribe(result => {
      this.getVocationList();
      this.vocationEditInd = 'ADD';
      this.vocationResourceID = null;
      this.vocationForm.reset();
      this._alertSevice.success('Vocation details saved successfully!');
    }, error => {
      this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  getVocationList() {
    const source = this._educationService.getVocationList(new PaginationRequest(
      {
        method: 'get',
        where: { personid: this.personId },
        page: 1,
        limit: 10,
      }), 10);

    this.vocationList$ = source.pluck('data');
  }

  editVocation(model, index) {
    setTimeout(() => {
      this.vocationForm.patchValue(model);
      this.vocationForm.patchValue({
        isvocationaltests: [model.isvocationaltest]
      });
    }, 100);
    this.vocationResourceID = model.personeducationvocationid;
    this.vocationEditInd = 'UPDATE';
    this.vocationForm.enable();
    this.vocationreportMode = 'edit';
    if (!this.vocationxpandStatus) {
      this.vocationxpandStatus = true;
      this.schoolxpandStatus = false;
      this.testingxpandStatus = false;
      this.accompxpandStatus = false;
    }
  }
  viewVocation(model) {
    setTimeout(() => {
      this.vocationForm.patchValue(model);
      this.vocationForm.patchValue({
        isvocationaltests: [model.isvocationaltest]
      });
    }, 100);
    this.vocationResourceID = model.personeducationvocationid;
    this.vocationreportMode = 'view';
    if (!this.vocationxpandStatus) {
      this.vocationxpandStatus = true;
      this.schoolxpandStatus = false;
      this.testingxpandStatus = false;
      this.accompxpandStatus = false;
    }
    this.vocationForm.disable();
  }
  showDeletePopVocation(resourceid) {
    this.vocationResourceID = resourceid;
    (<any>$('#delete-vocation-popup')).modal('show');
  }
  deleteVocation() {
    this._educationService.deleteVocation(this.vocationResourceID).subscribe(
      response => {
        this.getVocationList();
        this.vocationResourceID = null;
        this.vocationEditInd = 'ADD';
        this.vocationForm.reset();
        this._alertSevice.success('Vocation Info deleted successfully');
        (<any>$('#delete-vocation-popup')).modal('hide');
      },
      error => {
        this.vocationResourceID = null;
        this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-vocation-popup')).modal('hide');
      }
    );
  }

  selectTestingDescription(model) {
    this.testingDescription = '';
    this.testingDescription = model.text;
  }
  highetGradeDescription(model) {
    this.gradeDescription = '';
    this.gradeDescription = model.text;
  }
  setManditory(state: boolean, inputfield: string, manditory: string) {
    if (state === false && manditory === 'isManditory') {
      if (inputfield === 'receiveddate') {
        this.accomplishmentForm.get('isrecordreceiveds').valueChanges.subscribe((recordRecived: any) => {
          if (recordRecived === 'Yes') {
            this.accomplishmentForm.get('receiveddate').setValidators([Validators.required]);
            this.accomplishmentForm.get('receiveddate').updateValueAndValidity();
          } else {
            this.accomplishmentForm.get('receiveddate').clearValidators();
            this.accomplishmentForm.get('receiveddate').updateValueAndValidity();
          }
        });
      }
    }
  }
  addVocation(model: Vocation) {
    // upload attachment
    this._uploadService
      .upload({
        url: AppConfig.baseUrl + '/' + CommonUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
        headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
        filesKey: ['file'],
        files: this.uploadedFile,
        process: true
      })
      .subscribe(
        (response) => {
          if (response.status) {
            // this.progress.percentage = response.percent;
          }
          if (response.status === 1 && response.data) {
            this.vocation.push(model);
            this.vocation[this.vocation.length - 1].certificatename = response.data.originalfilename;
            this.vocation[this.vocation.length - 1].certificatepath = response.data.s3bucketpathname;
            this.vocationForm.reset();
            this.addEducation.vocation = this.vocation;
            // this.addEducationSubject$.next(this.addEducation);
            this._alertSevice.success('File Uploaded Succesfully!');
          }
        },
        (err) => {
          console.log(err);
          this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        },
        () => {
          // console.log('complete');
        }
      );
    // end of attachment upload
  }

  uploadFile(file: File | FileError): void {
    if (!(file instanceof File)) {
      // this.alertError(file);
      return;
    }
    this.uploadedFile = file;
    this.vocationForm.patchValue({ certificatename: file.name });
  }
  private loadDropDown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { isspecialeducation: false },
          order: 'displayorder ASC'
        },
        CommonUrlConfig.EndPoint.Intake.EducationTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { isspecialeducation: true },
          order: 'displayorder ASC'
        },
        CommonUrlConfig.EndPoint.Intake.EducationTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { ishighergrade: false },
          order: 'displayorder ASC'
        },
        CommonUrlConfig.EndPoint.Intake.GradeTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { ishighergrade: true },
          order: 'displayorder ASC'
        },
        CommonUrlConfig.EndPoint.Intake.GradeTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.TestingTypeUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          schoolType: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.educationtypekey
              })
          ),
          specialEducation: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.educationtypekey
              })
          ),
          states: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),
          lastGrade: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.gradetypekey
              })
          ),
          currentGrade: result[4].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.gradetypekey
              })
          ),
          testingType: result[5].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.testingtypekey
              })
          )
        };
      })
      .share();
    this.schoolTypeDropdownItems$ = source.pluck('schoolType');
    this.specialEducationDropdownItems$ = source.pluck('specialEducation');
    this.stateDropdownItems$ = source.pluck('states');
    this.lastGradeDropdownItems$ = source.pluck('lastGrade');
    this.currentGradeDropdownItems$ = source.pluck('currentGrade');
    this.testingTypeDropdownItems$ = source.pluck('testingType');
    // this.countyDropDownItems$ = source.pluck('counties');
  }

  loadSchoolDropdown() {
    const searchkey = this.schoolForm.get('educationname').value;
    const headers = new HttpHeaders().set('no-loader', 'true');
    const workEnv = config.workEnvironment;
    let schoolUrl = '';
    if (workEnv === 'state') {
        schoolUrl = this.baseUrl + '/' + 'School/v1/' + CommonUrlConfig.EndPoint.PERSON.EDUCATION.SCHOOL.GetSchoolUrl;
    } else {
        schoolUrl =  this.baseUrl + '/' + CommonUrlConfig.EndPoint.PERSON.EDUCATION.SCHOOL.GetSchoolUrl;
    }
    this.schoolSearchList$ = this.http.get(schoolUrl + '?filter=' + JSON.stringify({ where: {
        searchkey: searchkey
      },
      nolimit: true, method: 'get'
    }), { headers: headers }).map((result) => {
      return result;
    });
  }


  specialEducation(specialEdu) {
    if (specialEdu === 'true' || specialEdu === true) {
      this.isSpecialEducation = true;
    } else {
      this.isSpecialEducation = false;
    }
  }

}
