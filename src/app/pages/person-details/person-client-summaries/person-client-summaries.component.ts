import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataStoreService } from '../../../@core/services';
import { PersonDetailConstants } from '../person-details-constants';
import { PersonDetailsService } from '../person-details.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'person-client-summaries',
  templateUrl: './person-client-summaries.component.html',
  styleUrls: ['./person-client-summaries.component.scss']
})
export class PersonClientSummariesComponent implements OnInit, OnDestroy {


  summaryTabs = [];
  IS_HEADER_TAB_NEEDED = true;
  constructor(private personService: PersonDetailsService) { }

  ngOnInit() {
    this.loadSummaryTabs();
    this.listenPersonStatus();

  }

  private listenPersonStatus() {
    /* this._dataStoreService.currentStore.subscribe(store => {
      if (store && store[PersonDetailConstants.PERSON_STATUS]) {
        if (this.PERSON_STATUS !== store[PersonDetailConstants.PERSON_STATUS]) {
          this.PERSON_STATUS = store[PersonDetailConstants.PERSON_STATUS];
          console.log('person status');
          this.isHeaderTabNeeded();

        }
      }

    }); */
    this.personService.personStatus$.subscribe(status => {
      console.log('person status client-summary', status);
      this.isHeaderTabNeeded();
    });
  }

  isHeaderTabNeeded() {
    if (this.personService.personStatus !== PersonDetailConstants.ALL) {
      this.IS_HEADER_TAB_NEEDED = false;
    }
  }

  loadSummaryTabs() {
    this.summaryTabs = [{
      path: 'offense',
      name: 'Offense Summary'
    },
    {
      path: 'contact-notes',
      name: 'Contact Notes Summary'
    },
    {
      path: 'placement',
      name: 'Placement Summary'
    },
    {
      path: 'review',
      name: 'Review Summary'
    }];
  }

  ngOnDestroy() {
   // this.personService.personStatus$.unsubscribe();

  }

}
