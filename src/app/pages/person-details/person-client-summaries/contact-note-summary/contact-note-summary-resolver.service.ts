import { Injectable } from '@angular/core';
import { ContactNoteSummaryService } from './contact-note-summary.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { DataStoreService } from '../../../../@core/services';
import { PersonDetailConstants } from '../../person-details-constants';
import { PersonDetailsService } from '../../person-details.service';

@Injectable()
export class ContactNoteSummaryResolverService {

  constructor(private _service: ContactNoteSummaryService, private personService: PersonDetailsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const personid = route.parent.parent.parent.paramMap.get('personid');
    const personstatus = this.personService.personStatus;
    const paginationInfo = new PaginationInfo();
    paginationInfo.where = { personid: personid, personstatus: personstatus };
    return this._service.getContactNoteSummaryList(paginationInfo);
  }

}
