import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule,
  MatAutocompleteModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { PersonClientFamilyInfoRoutingModule } from './person-client-family-info-routing.module';
import { PersonClientFamilyInfoComponent } from './person-client-family-info.component';
import { PersonClientFamilyInfoService } from './person-client-family-info.service';

@NgModule({
  imports: [
    CommonModule,
    PersonClientFamilyInfoRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule
  ],
  declarations: [PersonClientFamilyInfoComponent],
  providers: [PersonClientFamilyInfoService]
})
export class PersonClientFamilyInfoModule { }
