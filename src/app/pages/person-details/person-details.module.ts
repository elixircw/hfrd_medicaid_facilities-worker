import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxfUploaderModule, NgxfUploaderService } from 'ngxf-uploader';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule,
  MatAutocompleteModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { PersonDetailsRoutingModule } from './person-details-routing.module';
import { PersonDetailsComponent } from './person-details.component';
import { PersonEducationDetailsComponent } from './person-education-details/person-education-details.component';
import { PersonWorksDetailsComponent } from './person-works-details/person-works-details.component';
import { PersonAddressDetailsComponent } from './person-address-details/person-address-details.component';
import { QuillModule } from 'ngx-quill';
import { IntakeUtils } from '../_utils/intake-utils.service';
import { SharedDirectivesModule } from '../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    ImageCropperModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    QuillModule,
    PersonDetailsRoutingModule,
    SharedDirectivesModule
  ],
  declarations: [PersonDetailsComponent, PersonEducationDetailsComponent, PersonWorksDetailsComponent, PersonAddressDetailsComponent],
  providers: [NgxfUploaderService, IntakeUtils]
})
export class PersonDetailsModule { }
