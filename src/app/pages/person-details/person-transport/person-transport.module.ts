import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule,
  MatAutocompleteModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PersonTransportRoutingModule } from './person-transport-routing.module';
import { PersonTransportComponent } from './person-transport.component';

@NgModule({
  imports: [
    CommonModule,
    PersonTransportRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule
  ],
  declarations: [PersonTransportComponent]
})
export class PersonTransportModule { }
