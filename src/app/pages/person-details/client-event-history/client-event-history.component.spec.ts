import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientEventHistoryComponent } from './client-event-history.component';

describe('ClientEventHistoryComponent', () => {
  let component: ClientEventHistoryComponent;
  let fixture: ComponentFixture<ClientEventHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientEventHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientEventHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
