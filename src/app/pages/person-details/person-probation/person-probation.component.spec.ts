import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonProbationComponent } from './person-probation.component';

describe('PersonProbationComponent', () => {
  let component: PersonProbationComponent;
  let fixture: ComponentFixture<PersonProbationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonProbationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonProbationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
