import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GenericService, CommonHttpService } from '../../../../@core/services';
import { Medication } from '../../../../@core/common/models/involvedperson.data.model';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { PaginationRequest } from '../../../../@core/entities/common.entities';

@Injectable()
export class MedicationIncludingPsychotropicService {

  medicationInfo$ = new Observable<any[]>();

  constructor(private _service: GenericService<Medication>,
    private _commonHttpService: CommonHttpService) { }

  addUpdatemedition(medication) {
    return this._commonHttpService.create(medication, CommonUrlConfig.EndPoint.PERSON.MEDICAL.medicationInfoAdd);
  }

  getmedication(paginationRequest: PaginationRequest, pageinfosize) {
    const source = this._commonHttpService.getPagedArrayList(paginationRequest,
      CommonUrlConfig.EndPoint.PERSON.MEDICAL.medicationInfoList + '?filter'
    ).map((result: any) => {
      return {
        data: result,
        count: result.length > 0 ? result[0].totalcount : 0,
        canDisplayPager: result.length > 0 ? result[0].totalcount > pageinfosize : false
      };
    }).share();
    this.medicationInfo$ = source.pluck('data');
    return source;
  }

  delete(medicationid) {
    this._service.endpointUrl = CommonUrlConfig.EndPoint.PERSON.MEDICAL.medicationInfoDelete;
    return this._service.remove(medicationid);
  }

  set medication(medication: Observable<any[]>) {
    this.medicationInfo$ = medication;
  }

  get medication(): Observable<any[]> {
    return this.medicationInfo$;
  }


}
