import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Medication, Health } from '../../../../../@core/common/models/involvedperson.data.model';
import { MyNewintakeConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { DataStoreService, CommonHttpService, AlertService } from '../../../../../@core/services';
import { MedicationIncludingPsychotropicService } from '../medication-including-psychotropic.service';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Subject, Observable } from 'rxjs';
import { PersonDetailsService } from '../../../person-details.service';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'medication-including-psychotropic-list',
  templateUrl: './medication-including-psychotropic-list.component.html',
  styleUrls: ['./medication-including-psychotropic-list.component.scss']
})

export class MedicationIncludingPsychotropicListComponent implements OnInit {
  id: string;
  paginationInfo: PaginationInfo = new PaginationInfo();
  private pageStream$ = new Subject<number>();
  medication$ = new Observable<Medication[]>();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  resourceID: string;

  constructor(private _alertSevice: AlertService,
    private _personDetailService: PersonDetailsService,
    private _medicationService: MedicationIncludingPsychotropicService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.parent.parent.parent.parent.parent.params['id'];
    this.pageStream$.subscribe((pageNumber) => {
      this.paginationInfo.pageNumber = pageNumber;
      this.getPage(this.paginationInfo.pageNumber);
    });
    this.getPage(1);
  }

  getPage(page: number) {
    const source = this._medicationService.getmedication(new PaginationRequest(
      {
        method: 'get',
        where: { personid: this._personDetailService.person.personid },
        page: this.paginationInfo.pageNumber,
        limit: this.paginationInfo.pageSize,
      }), this.paginationInfo.pageSize);

    this.medication$ = source.pluck('data');
    if (page === 1) {
      this.totalRecords$ = source.pluck('count');
      this.canDisplayPager$ = source.pluck('canDisplayPager');
    }
  }

  view(id: string) {
    this.router.navigate(['../create-edit', { id: id, reportMode: 'view' }], { relativeTo: this.route });
  }

  edit(id: string) {
    this.router.navigate(['../create-edit', { id: id, reportMode: 'edit' }], { relativeTo: this.route });
  }

  showDeletePop(resourceid) {
    this.resourceID = resourceid;
    (<any>$('#delete-medation-popup')).modal('show');
  }

  delete() {
    this._medicationService.delete(this.resourceID).subscribe(
      response => {
        this.getPage(1);
        this.resourceID = null;
        this._alertSevice.success('Medications deleted successfully');
        (<any>$('#delete-medation-popup')).modal('hide');
      },
      error => {
        this.resourceID = null;
        this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-medation-popup')).modal('hide');
      }
    );
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.pageStream$.next(this.paginationInfo.pageNumber);
  }
}
