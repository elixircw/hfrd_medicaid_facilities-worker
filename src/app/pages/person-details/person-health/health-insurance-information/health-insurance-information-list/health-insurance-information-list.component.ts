import { Component, OnInit } from '@angular/core';
import { HealthInsuranceInformation, Health, Physician } from '../../../../../@core/common/models/involvedperson.data.model';
import { AlertService } from '../../../../../@core/services';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { HealthInsuranceInformationService } from '../health-insurance-information.service';
import { PersonDetailsService } from '../../../person-details.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { Subject } from 'rxjs/Subject';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'health-insurance-information-list',
  templateUrl: './health-insurance-information-list.component.html',
  styleUrls: ['./health-insurance-information-list.component.scss']
})
export class HealthInsuranceInformationListComponent implements OnInit {

  id: string;
  healthinsurance: HealthInsuranceInformation[] = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  private pageStream$ = new Subject<number>();
  healthinsurance$ = new Observable<HealthInsuranceInformation[]>();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  resourceID: string;


  constructor(private _alertSevice: AlertService,
    private _personDetailService: PersonDetailsService,
    private _healthInsInfoService: HealthInsuranceInformationService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.parent.parent.parent.parent.parent.params['id'];
    this.pageStream$.subscribe((pageNumber) => {
      this.paginationInfo.pageNumber = pageNumber;
      this.getPage(this.paginationInfo.pageNumber);
    });
    this.getPage(1);
  }

  getPage(page: number) {
    const source = this._healthInsInfoService.getHealthInsInfo(new PaginationRequest(
      {
        method: 'get',
        where: { personid: this._personDetailService.person.personid },
        page: this.paginationInfo.pageNumber,
        limit: this.paginationInfo.pageSize,
      }), this.paginationInfo.pageSize);

    this.healthinsurance$ = source.pluck('data');
    if (page === 1) {
      this.totalRecords$ = source.pluck('count');
      this.canDisplayPager$ = source.pluck('canDisplayPager');
    }
  }

  view(id: string) {
    this.router.navigate([ '../create-edit', { id: id, reportMode: 'view' } ], { relativeTo: this.route });
  }

  edit(id: string) {
    this.router.navigate([ '../create-edit', { id: id, reportMode: 'edit' } ], { relativeTo: this.route });
  }

  showDeletePop(resourceid) {
    this.resourceID = resourceid;
    (<any>$('#delete-healthInsInfo-popup')).modal('show');
  }

  delete() {
    this._healthInsInfoService.delete(this.resourceID).subscribe(
      response => {
        this.getPage(1);
        this.resourceID = null;
        this._alertSevice.success('Health Insurance Information deleted successfully');
        (<any>$('#delete-healthInsInfo-popup')).modal('hide');
      },
      error => {
        this.resourceID = null;
        this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-healthInsInfo-popup')).modal('hide');
      }
    );
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.pageStream$.next(this.paginationInfo.pageNumber);
  }


}
