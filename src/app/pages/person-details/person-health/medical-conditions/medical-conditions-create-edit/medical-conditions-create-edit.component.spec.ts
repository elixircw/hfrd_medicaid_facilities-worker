import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalConditionsCreateEditComponent } from './medical-conditions-create-edit.component';

describe('MedicalConditionsCreateEditComponent', () => {
  let component: MedicalConditionsCreateEditComponent;
  let fixture: ComponentFixture<MedicalConditionsCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalConditionsCreateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalConditionsCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
