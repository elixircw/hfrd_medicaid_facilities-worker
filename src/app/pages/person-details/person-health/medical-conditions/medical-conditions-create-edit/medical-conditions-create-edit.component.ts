import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MedicalConditionType } from '../../../../newintake/my-newintake/_entities/newintakeModel';
import { MedicalConditions, Health } from '../../../../../@core/common/models/involvedperson.data.model';
import { MyNewintakeConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { MedicalConditionsService } from '../medical-conditions.service';
import { PersonDetailsService } from '../../../person-details.service';
import { ActivatedRoute } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'medical-conditions-create-edit',
  templateUrl: './medical-conditions-create-edit.component.html',
  styleUrls: ['./medical-conditions-create-edit.component.scss']
})
export class MedicalConditionsCreateEditComponent implements OnInit {
  medicalconditionForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  minDate = new Date();
  maxDate = new Date();
  medicalCondtionType: MedicalConditionType[] = [];
  medicalConditionDescription: string[] = [];
  resourceId: string;
  medicalConditionType$: Observable<DropdownModel[]>;
  _medicalcondtion: string[] = [];

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _commonHttpService: CommonHttpService,
    private personService: PersonDetailsService,
    private route: ActivatedRoute,
    private _medicalConditionService: MedicalConditionsService) { }

  ngOnInit() {
    this.loadDropDowns();
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.medicalconditionForm = this.formbulider.group({
      medicalconditiontypekey: ['', Validators.required],
      begindate: [null, Validators.required],
      enddate: [null, Validators.required],
      recordedby: ['', Validators.required],
      medicalconditiondesc: null
    });

    this.route.params.subscribe(params => {
      this._medicalConditionService.medicalConditionInfo$.subscribe(data => {
        data.forEach(element => {
          if (element.personmedicalconditionid === params.id) {
            if (params.reportMode === 'edit') {
              this.reportMode = 'edit';
              this.editMode = true;
              this.resourceId = params.id;
            } else if (params.reportMode === 'view') {
              this.medicalconditionForm.disable();
              this.reportMode = 'view';
              this.editMode = false;
              this.resourceId = params.id;
            }
            setTimeout(() => {
              element.medicalcondition.forEach(_element => {
                this._medicalcondtion.push(_element.medicalconditiontypekey);
                this.medicalConditionDescription.push(_element.medicalconditiontypekey);
              });
              this.medicalconditionForm.controls['medicalconditiontypekey'].setValue(this._medicalcondtion);
              this.patchForm(element);
            }, 1000);
          }
        });
      });
    });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.medicalconditiontype + '?filter'
      )
    ])
      .map((result) => {
        return {
          medicalconditiontype: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.medicalconditiontypekey
              })
          )
        };
      })
      .share();
    this.medicalConditionType$ = source.pluck('medicalconditiontype');
  }

  private addUpdate(medicalconditionForm) {
    medicalconditionForm.personmedicalconditionid = this.resourceId;
    medicalconditionForm.personid = this.personService.person.personid;
    medicalconditionForm.medicalcondition = this.medicalCondtionType;
    if (medicalconditionForm.begindate) {
      if (!(medicalconditionForm.begindate instanceof Date)) {
        medicalconditionForm.begindate = new Date(medicalconditionForm.begindate);
      }
    }
    if (medicalconditionForm.enddate) {
      if (!(medicalconditionForm.enddate instanceof Date)) {
        medicalconditionForm.enddate = new Date(medicalconditionForm.enddate);
      }
    }
    console.log(medicalconditionForm);
    this._medicalConditionService.addUpdatemedicalCondition(medicalconditionForm).subscribe(result => {
      this.resetForm();
      const element: HTMLElement = document.getElementById('backbutton') as HTMLElement;
      element.click();
      this._alertSevice.success('Medical Conditions details saved successfully!');
    }, error => {
      const element: HTMLElement = document.getElementById('backbutton') as HTMLElement;
      element.click();
      this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  private resetForm() {
    this.medicalconditionForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.medicalconditionForm.enable();
  }

  private getMedicalCondition(modal) {
    const obj = JSON.parse(JSON.stringify(modal));
    obj.medicalconditiontype = obj.medicalconditiontypekey;
    obj.medicalconditiontypekey = obj.medicalconditiontypekey.map(item => item.medicalconditiontypekey);

    return obj;
  }

  selectMedicalConditionType(event) {
    if (event) {
      const medicalConditionType = event.map(res => {
        return { medicalconditiontypekey: res };
      });
      this.medicalCondtionType = medicalConditionType;
      this.medicalConditionType$.subscribe(items => {
        if (items) {
          const getConditiontems = items.filter(item => {
            if (event.includes(item.value)) {
              return item;
            }
          });
          this.medicalConditionDescription = getConditiontems.map(res => res.text);
        }
      });
    }
  }
  startDateChanged() {
    this.medicalconditionForm.patchValue({ enddate: '' });
    const empForm = this.medicalconditionForm.getRawValue();
    this.maxDate = new Date(empForm.begindate);
  }
  endDateChanged() {
    this.medicalconditionForm.patchValue({ begindate: '' });
    const empForm = this.medicalconditionForm.getRawValue();
    this.minDate = new Date(empForm.enddate);
  }
  private patchForm(modal: MedicalConditions) {
    this.medicalconditionForm.patchValue(modal);
  }

}
