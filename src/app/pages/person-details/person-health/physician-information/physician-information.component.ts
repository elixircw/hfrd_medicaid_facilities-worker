import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'physician-information',
  templateUrl: './physician-information.component.html',
  styleUrls: ['./physician-information.component.scss']
})
export class PhysicianInformationComponent implements OnInit {

  constructor(private route: ActivatedRoute) {  }

  ngOnInit() {  }

}
