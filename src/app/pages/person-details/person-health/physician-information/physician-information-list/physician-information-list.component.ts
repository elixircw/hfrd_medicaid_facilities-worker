import { Component, OnInit } from '@angular/core';
import { Physician } from '../../../../../@core/common/models/involvedperson.data.model';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';

import { FormBuilder } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../../@core/services';
import { PersonDetailsService } from '../../../person-details.service';
import { PhysicianInformationService } from '../physician-information.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'physician-information-list',
  templateUrl: './physician-information-list.component.html',
  styleUrls: ['./physician-information-list.component.scss']
})
export class PhysicianInformationListComponent implements OnInit {

  id: string;
  physician: Physician[] = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  private pageStream$ = new Subject<number>();
  physician$ = new Observable<Physician[]>();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  resourceID: string;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _personDetailService: PersonDetailsService,
    private _physicalInfoService: PhysicianInformationService,
    private router: Router,
    private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.id = this.route.snapshot.parent.parent.parent.parent.parent.params['id'];
    this.pageStream$.subscribe((pageNumber) => {
      this.paginationInfo.pageNumber = pageNumber;
      this.getPage(this.paginationInfo.pageNumber);
    });
    this.getPage(1);
  }

  getPage(page: number) {
    const source = this._physicalInfoService.getPhysicianInfo(new PaginationRequest(
      {
        method: 'get',
        where: { personid: this._personDetailService.person.personid },
        page: this.paginationInfo.pageNumber,
        limit: this.paginationInfo.pageSize,
      }), this.paginationInfo.pageSize);

    this.physician$ = source.pluck('data');
    if (page === 1) {
      this.totalRecords$ = source.pluck('count');
      this.canDisplayPager$ = source.pluck('canDisplayPager');
    }
  }

  view(id) {
    this.router.navigate(['../create-edit', { id: id, reportMode: 'view' }], { relativeTo: this.route });
  }

  edit(id) {
    this.router.navigate(['../create-edit', { id: id, reportMode: 'edit' }], { relativeTo: this.route });
  }

  showDeletePop(resourceid) {
    this.resourceID = resourceid;
    (<any>$('#delete-physicalinfo-popup')).modal('show');
  }

  delete() {
    this._physicalInfoService.delete(this.resourceID).subscribe(
      response => {
        this.getPage(1);
        this.resourceID = null;
        this._alertSevice.success('Physician Information deleted successfully');
        (<any>$('#delete-physicalinfo-popup')).modal('hide');
      },
      error => {
        this.resourceID = null;
        this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-physicalinfo-popup')).modal('hide');
      }
    );
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.pageStream$.next(this.paginationInfo.pageNumber);
  }


}
