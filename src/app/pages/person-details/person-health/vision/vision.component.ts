import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { PersonDentalInfo } from '../../../../@core/common/models/involvedperson.data.model';
import { AlertService, CommonHttpService, DataStoreService, ValidationService } from '../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'vision',
  templateUrl: './vision.component.html',
  styleUrls: ['./vision.component.scss']
})
export class VisionComponent implements OnInit {


  minDate = new Date();
  maxDate = new Date();
  visionForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  persondentalinfo: PersonDentalInfo[] = [];
  specialty$: Observable<DropdownModel[]>;
  specialty: any = [];

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {

  }

  ngOnInit() {
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.visionForm = this.formbulider.group({
      'isdentalinfo': [false, Validators.required],
      'dentistname': ['', Validators.required],
      'dentalspecialtytypekey': ['', Validators.required],
      'phone': ['', Validators.required],
      'email': ['', ValidationService.mailFormat],
      'address1': '',
      'address2': '',
      'city': '',
      'state': '',
      'county': ['', Validators.required],
      'zip': '',
      'startdate': [null, Validators.required],
      'enddate': null,
    });

    this.loadDropDowns();
  }




  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'description'
        },
        CommonUrlConfig.EndPoint.Intake.dentalspecialtytype + '?filter'
      ),
    ])
      .map((result) => {
        result[3].forEach(type => {
          this.specialty[type.dentalspecialtytypekey] = type.description;
        });
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          ),
          counties: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.countyname,
                value: res.countyname
              })
          ),
          specialty: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.dentalspecialtytypekey
              })
          ),
        };
      })
      .share();
    this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
    this.specialty$ = source.pluck('specialty');
  }

  private add() {

    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.visionForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.visionForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.persondentalinfo[this.modalInt] = this.visionForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.visionForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.visionForm.enable();
  }

  private delete(index) {
    this.persondentalinfo.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  startDateChanged() {
    this.visionForm.patchValue({ enddate: '' });
    const empForm = this.visionForm.getRawValue();
    this.maxDate = new Date(empForm.startdate);
  }
  endDateChanged() {
    this.visionForm.patchValue({ startdate: '' });
    const empForm = this.visionForm.getRawValue();
    this.minDate = new Date(empForm.enddate);
  }
  private patchForm(modal: PersonDentalInfo) {
    this.visionForm.patchValue(modal);
  }

}
