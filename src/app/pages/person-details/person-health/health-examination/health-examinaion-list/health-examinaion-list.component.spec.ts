import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthExaminaionListComponent } from './health-examinaion-list.component';

describe('HealthExaminaionListComponent', () => {
  let component: HealthExaminaionListComponent;
  let fixture: ComponentFixture<HealthExaminaionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthExaminaionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthExaminaionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
