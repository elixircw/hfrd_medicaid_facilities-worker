import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { BehaviouralHealthInfo, Health } from '../../../../../@core/common/models/involvedperson.data.model';
import { MyNewintakeConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AlertService, DataStoreService, CommonHttpService, AuthService, CommonDropdownsService } from '../../../../../@core/services';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { AppConfig } from '../../../../../app.config';
import { HttpHeaders } from '@angular/common/http';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { BehavioralHealthInfoService } from '../behavioral-health-info.service';
import { ActivatedRoute } from '@angular/router';
import { PersonDetailsService } from '../../../person-details.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'behavioral-health-info-create-edit',
  templateUrl: './behavioral-health-info-create-edit.component.html',
  styleUrls: ['./behavioral-health-info-create-edit.component.scss']
})
export class BehavioralHealthInfoCreateEditComponent implements OnInit {

  behaviouralhealthinfoForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  private token: AppUser;
  uploadedFile: File;
  resourceId: string;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _commonHttpService: CommonHttpService,
    private personService: PersonDetailsService,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService,
    private route: ActivatedRoute,
    private _commonDropdownsService: CommonDropdownsService,
    private _behaviorHealthService: BehavioralHealthInfoService) {
    this.token = this._authService.getCurrentUser();
  }

  ngOnInit() {
    this.loadDropDowns();
    this.editMode = false;
    this.reportMode = 'add';
    this.resourceId = null;
    this.modalInt = -1;
    this.behaviouralhealthinfoForm = this.formbulider.group({
      clinicianname: [null, Validators.required],
      address1: [null, Validators.required],
      address2: null,
      city: null,
      state: null,
      county: null,
      zip: null,
      phone: null,
      currentdiagnoses: null,
      reportname: [null],
      reportpath: null
    });

    this.route.params.subscribe(params => {
      this._behaviorHealthService.behaviorHealth$.subscribe(data => {
        data.forEach(element => {
          if (element.personbehavioralhealthid === params.id) {
            setTimeout(() => {
              this.patchForm(element);
              this.behaviouralhealthinfoForm.patchValue({
                county: element.countyid
              });
            }, 1000);
            this.loadCounty(element.state);
            if (params.reportMode === 'edit') {
              this.reportMode = 'edit';
              this.editMode = true;
              this.resourceId = params.id;
            } else if (params.reportMode === 'view') {
              this.behaviouralhealthinfoForm.disable();
              this.reportMode = 'view';
              this.editMode = false;
              this.resourceId = params.id;
            }
          }
        });
      });
    });
  }


  uploadFile(file: File | FileError): void {
    if (!(file instanceof File)) {
      // this.alertError(file);
      return;
    }

    this.uploadedFile = file;
    this.behaviouralhealthinfoForm.patchValue({ reportname: file.name });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CommonUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();
    // this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
  }

  private loadCounty(countystate?) {
    const state = countystate ? countystate : this.behaviouralhealthinfoForm.get('state').value;
    this.countyDropDownItems$ = this._commonDropdownsService.getCountyList(state);
  }


  private addUpdate(behaviorHealthForm) {
    behaviorHealthForm.personbehavioralhealthid = this.resourceId;
    behaviorHealthForm.personid = this.personService.person.personid;
    console.log(behaviorHealthForm);
    this._behaviorHealthService.addUpdatebehaviorHealthInfo(behaviorHealthForm).subscribe(result => {
      this.resetForm();
      const element: HTMLElement = document.getElementById('backbutton') as HTMLElement;
      element.click();
      this._alertSevice.success('Behavior Health Information details saved successfully!');
    }, error => {
      const element: HTMLElement = document.getElementById('backbutton') as HTMLElement;
      element.click();
      this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  private oldadd() {
    this._uploadService
      .upload({
        url: AppConfig.baseUrl + '/' + CommonUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
        headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
        filesKey: ['file'],
        files: this.uploadedFile,
        process: true
      })
      .subscribe(
        (response) => {
          if (response.status) {
            // this.progress.percentage = response.percent;
          }
          if (response.status === 1 && response.data) {
            // this.behaviouralhealthinfo.push(this.behaviouralhealthinfoForm.getRawValue());
            // this.behaviouralhealthinfo[this.behaviouralhealthinfo.length - 1].reportname = response.data.originalfilename;
            // this.behaviouralhealthinfo[this.behaviouralhealthinfo.length - 1].reportpath = response.data.s3bucketpathname;
            // this.health = this._dataStoreService.getData(this.constants.Health);
            // this.health.behaviouralhealthinfo = this.behaviouralhealthinfo;
            // this._dataStoreService.setData(this.constants.Health, this.health);
            this._alertSevice.success('Added Successfully');
            this.resetForm();
            this._alertSevice.success('File Uploaded Succesfully!');
          }
        },
        (err) => {
          console.log(err);
          this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        },
        () => {
          // console.log('complete');
        }
      );
  }

  private resetForm() {
    this.behaviouralhealthinfoForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.behaviouralhealthinfoForm.enable();
  }

  private patchForm(modal: BehaviouralHealthInfo) {
    this.behaviouralhealthinfoForm.patchValue(modal);
  }

}
