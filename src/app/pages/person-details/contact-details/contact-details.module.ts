import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactDetailsRoutingModule } from './contact-details-routing.module';
import { ContactDetailsComponent } from './contact-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatFormFieldModule, MatRadioModule, MatInputModule } from '@angular/material';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    ContactDetailsRoutingModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRadioModule,
    MatInputModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [ContactDetailsComponent]
})
export class ContactDetailsModule { }
