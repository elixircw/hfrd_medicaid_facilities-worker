import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'add-edit-non-contracting-providers',
  templateUrl: './add-edit-non-contracting-providers.component.html',
  styleUrls: ['./add-edit-non-contracting-providers.component.scss']
})
export class AddEditNonContractingProvidersComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    this.showAddUpdate();
  }
  showAddUpdate() {
    (<any>$('#myModal-add-edit-non-contract')).modal('show');
  }
  clearItem() {
    this.router.navigate(['/pages/manage/non-contracting-providers']);
    (<any>$('#myModal-add-edit-non-contract')).modal('hide');
  }

}
