import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleGuard } from '../../@core/guard';
import { ManageComponent } from './manage.component';

const routes: Routes = [
    {
        path: '',
        component: ManageComponent,
        canActivate: [RoleGuard],
        children: [
            { path: 'team-manage', loadChildren: './team-manage/team-manage.module#TeamManageModule' },
            { path: 'significant-events', loadChildren: './significant-events/significant-events.module#SignificantEventsModule' },
            { path: 'regulation-library', loadChildren: './regulation-library/regulation-library.module#RegulationLibraryModule' },
            { path: 'reference-links', loadChildren: './reference-links/reference-links.module#ReferenceLinksModule' },
            { path: 'person-merge', loadChildren: './person-merge/person-merge.module#PersonMergeModule' },
            { path: 'non-contracting-providers', loadChildren: './non-contracting-providers/non-contracting-providers.module#NonContractingProvidersModule' },
            {
                path: 'news',
                loadChildren: './news/news.module#NewsModule',
                data: {
                    title: ['News'],
                    desc: 'Maryland department of human services',
                    screen: { key: 'manage.news', modules: [], skip: false }
                }
            },
            { path: 'manage-da-group', loadChildren: './manage-da-group/manage-da-group.module#ManageDaGroupModule' }
        ],
        data: {
            title: ['MDTHINK manage application'],
            desc: 'Maryland department of human services',
            screen: { current: 'manage', modules: [], skip: false }
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManageRoutingModule {}
