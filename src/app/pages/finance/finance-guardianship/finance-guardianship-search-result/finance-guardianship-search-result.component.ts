import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { FinanceGuardianshipSearchEntry} from '../../_entities/finance-entity.module';



@Component({
  selector: 'app-finance-guardianship-search-result',
  templateUrl: './finance-guardianship-search-result.component.html',
  styleUrls: ['./finance-guardianship-search-result.component.scss']
})
export class FinanceGuardianshipSearchResultComponent implements OnInit {

  @Input() showTable: boolean;
  @Input() pageNumberResult$: Subject<number>;
  @Input() searchResultData$: Observable<FinanceGuardianshipSearchEntry[]>;
  @Input() totalResulRecords$: Observable<number>;

  selectedRecord: FinanceGuardianshipSearchEntry;
  paginationInfo: PaginationInfo = new PaginationInfo();
  private pageSubject$ = new Subject<number>();


  constructor() {
    this.showTable = false;
    this.selectedRecord = new FinanceGuardianshipSearchEntry();
  }

  ngOnInit() {
    this.pageSubject$.subscribe(pageNumber => {
      this.paginationInfo.pageNumber = pageNumber;
    });

  }

  pageChanged(page: number) {
     // this.paginationInfo.pageNumber = page;
      this.pageNumberResult$.next(page);
  }

  selectGuardianship(data: FinanceGuardianshipSearchEntry) {
      this.selectedRecord = data;
  }
}
