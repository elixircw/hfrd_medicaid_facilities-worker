import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { FinanceGuardianshipSearchEntry} from '../../_entities/finance-entity.module';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'guardianship-search-view',
    templateUrl: './finance-guardianship-search-view.component.html',
    styleUrls: ['./finance-guardianship-search-view.component.scss']
})
export class FinanceGuardianshipSearchViewComponent implements OnInit {
    @Input() selectedGuardianship: FinanceGuardianshipSearchEntry;
    constructor() {
    }
    ngOnInit() {
    }

}
