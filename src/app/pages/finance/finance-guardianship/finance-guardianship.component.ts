import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { FinanceGuardianshipSearchEntry} from '../_entities/finance-entity.module';

@Component({
    selector: 'app-finance-guardianship',
    templateUrl: './finance-guardianship.component.html',
    styleUrls: ['./finance-guardianship.component.scss']
})
export class FinanceGuardianshipComponent implements OnInit {
    searchData$: Subject<Observable<FinanceGuardianshipSearchEntry[]>>;
    totalSearchRecord$: Subject<Observable<number>>;
    pageNumberResult$: Subject<number>;
    searchResultData$: Observable<FinanceGuardianshipSearchEntry[]>;
    totalResulRecords$: Observable<number>;
    showTable = false;

    constructor() {
        this.searchData$ = new Subject();
        this.totalSearchRecord$ = new Subject();
        this.pageNumberResult$ = new Subject();
    }

    ngOnInit() {
        this.searchData$.subscribe(data => {
            this.searchResultData$ = data;
            this.showTable = true;
        });
        this.totalSearchRecord$.subscribe(data => {
            this.totalResulRecords$ = data;
        });

    }
}
