import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceProviderContractRateRoutingModule } from './finance-provider-contract-rate-routing.module';
import { FinanceProviderContractRateComponent } from './finance-provider-contract-rate.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { PaginationModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FinanceProviderContractRateRoutingModule,
    FormMaterialModule,
    SharedPipesModule,
    PaginationModule
  ],
  declarations: [FinanceProviderContractRateComponent]
})
export class FinanceProviderContractRateModule { }
