import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, AlertService, DataStoreService } from '../../@core/services';
import { PaginationRequest } from '../../@core/entities/common.entities';
import { FinanceUrlConfig } from './finance.url.config';

@Injectable()
export class FinanceService {
  providerInfoDetails: any;
  paymentAddress: any;
  businessAddress: any;
  providerId:any;
  isPaymentWithhold: boolean;
  electronicfundtransfer: boolean;
  placement: boolean;
  vendor: boolean;
  community: boolean;
  _page: string;
  changeLogList: any;
  totalcount: any;
  changehistory = [];
  adjustment: any[];
  overPayments: any;
  overpaymentvalue: any[];

  constructor(
    private _commonHttpService: CommonHttpService, private _router: Router,
    private _authService: AuthService, private _alertService: AlertService,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService
  ) { }

  getProviderDetails(providerID) {
    this.providerId = providerID;
    this._commonHttpService.getArrayList(new PaginationRequest({
    where: {
      provider_id: providerID
    },
    nolimit: true,
    method: 'get'
    }), 'providerinfo/financeproviderreport?filter').subscribe(result => {
      if (result && result.length && result[0].financeproviderreport && result[0].financeproviderreport.length ) {
        this.providerInfoDetails = result[0].financeproviderreport[0];
        this.paymentAddress = this.providerInfoDetails.paymentaddress ? this.providerInfoDetails.paymentaddress[0].paymentaddress : null;
        this.businessAddress = this.providerInfoDetails.businessaddress ? this.providerInfoDetails.businessaddress[0].businessaddress : null;
        this.isPaymentWithhold = this.providerInfoDetails.withhold_payment_sw === 'Y' ? true : false;
        this.electronicfundtransfer = this.providerInfoDetails.eft_sw === 'Y' ? true : false;
        this.placement = false;
        this.vendor = false;
        this.community = false;
        this.providerInfoDetails.provider_category_cd.forEach(code =>{
          if (code.picklist_value_cd === '1783') {
            this.placement = true;
          } else if (code.picklist_value_cd === '3304') {
            this.vendor = true;
          } else if (code.picklist_value_cd === '3049') {
            this.community = true;
          }
        })
        this.getChangeLogList(this.providerInfoDetails.provider_id, {pageSize: 10, pageNumber: 1});
      }
    });
  }

  getChangeLogList(providerid, paginationInfo) {
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: paginationInfo.pageSize,
        page: paginationInfo.pageNumber,
        method: 'get',
        where: {provider_id : providerid}
        }), 'withhold_eft_config/getwithholdchangelog?filter'
    ).subscribe((result: any) => {
      if (result) {
        this.changeLogList = result;
        this.totalcount = (this.changeLogList && this.changeLogList.length > 0) ? this.changeLogList[0].totalcount : 0;
      }
    });
  }

  public get page(): string {
    return this._page;
  }

  public set page(value: string) {
    this._page = value;
  }

  getFiscalAudit(clientID, page, overpayments?) {
    this.changehistory = [];
    this.adjustment = [];
    if (overpayments) {
      this.overpaymentvalue = overpayments;
    } else {
      this.overpaymentvalue = [];
    }
    this._commonHttpService.getPagedArrayList({
      where: {
        receivable_detail_id: null,
        clientid: clientID
      },
      page: page,
      limit: 10,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.audit.auditList).subscribe((result: any) => {
        if (result && result.data && result.data.length > 0) {
          this.changehistory = result.data;
          for (let i = 0;  i < result.data.length; i++) {
            if (result.data[i].adjstment) {
              this.adjustment = result.data[i].adjstment;
            }
          }
        }
    });
  } 
}
