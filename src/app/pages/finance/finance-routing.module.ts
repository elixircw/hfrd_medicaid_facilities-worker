import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinanceComponent } from './finance.component';
import { RoleGuard } from '../../@core/guard';

const routes: Routes = [
    {
        path: '',
        component: FinanceComponent,
        canActivate: [RoleGuard],
        children: [
            { path: 'finance-dashboard', loadChildren: './finance-dashboard/finance-dashboard.module#FinanceDashboardModule' },
            { path: 'finance-accountsPayable', loadChildren: './finance-accountsPayable/finance-accountsPayable.module#FinanceAccountsPayableModule' },
            { path: 'finance-accountsReceivable', loadChildren: './finance-accountsReceivable/finance-accountsReceivable.module#FinanceAccountsReceivableModule' },
            { path: 'finance-adjustment', loadChildren: './finance-adjustment/finance-adjustment.module#FinanceAdjustmentModule' },
            { path: 'finance-placement', loadChildren: './finance-placement/finance-placement.module#FinancePlacementModule' },
            { path: 'finance-guardianship', loadChildren: './finance-guardianship/finance-guardianship.module#FinanceGuardianshipModule' },
            { path: 'finance-adoption', loadChildren: './finance-adoption/finance-adoption.module#FinanceAdoptionModule' },
            { path: 'child-accounts', loadChildren: './child-accounts/child-accounts.module#ChildAccountsModule' },
            { path: 'commingled-accounts', loadChildren: './commingled-accounts/commingled-accounts.module#CommingledAccountsModule' },
            { path: 'receipt-fast-entry', loadChildren: './receipt-fast-entry/receipt-fast-entry.module#ReceiptFastEntryModule' },
            { path: 'ssi-ssa-tracking', loadChildren: './ssi-ssa-tracking/ssi-ssa-tracking.module#SsiSsaTrackingModule' },
            { path: 'finance-foster-care-rate', loadChildren: './finance-foster-care-rate/finance-foster-care-rate.module#FinanceFosterCareRateModule' },
            { path: 'funding-allocation', loadChildren: './finance-accountsPayable/history/funding-source-allocation/funding-source-allocation.module#FundingSourceAllocationModule' },
            { path: 'reference', loadChildren: './reference/reference.module#ReferenceModule' },
            { path: 'provider-checklist', loadChildren: './reference/provider-checklist/provider-checklist.module#ProviderChecklistModule' },
            { path: 'vendor-pay-file-calendar', loadChildren: './vendor-pay-file-calendar/vendor-pay-file-calendar.module#VendorPayFileCalendarModule'},
            { path: 'finance-provider-contract-rate', loadChildren: './finance-provider-contract-rate/finance-provider-contract-rate.module#FinanceProviderContractRateModule' },
            { path: '**', redirectTo: 'finance-dashboard' }],
        data: { roles: ['admin', 'intakeuser', 'caseworker', 'reviewer'] }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FinanceRoutingModule { }