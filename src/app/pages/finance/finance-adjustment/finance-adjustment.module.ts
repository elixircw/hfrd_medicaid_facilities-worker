import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceAdjustmentRoutingModule } from './finance-adjustment-routing.module';
import { FinanceAdjustmentComponent } from './finance-adjustment.component';
import { PlacementAdjustmentComponent } from './placement-adjustment/placement-adjustment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMaskModule } from 'ngx-mask';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatButtonModule, MatRadioModule, MatTooltipModule } from '@angular/material';
import { AdjustmentSearchComponent } from './adjustment-search/adjustment-search.component';
import { AdjustmentResultComponent } from './adjustment-result/adjustment-result.component';
import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    FinanceAdjustmentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    A2Edatetimepicker,
    NgSelectModule,
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatButtonModule,
    MatRadioModule,
    MatTooltipModule,
    SharedDirectivesModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [FinanceAdjustmentComponent, PlacementAdjustmentComponent, AdjustmentSearchComponent, AdjustmentResultComponent]
})
export class FinanceAdjustmentModule { }
