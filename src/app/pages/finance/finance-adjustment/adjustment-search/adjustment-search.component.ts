import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataStoreService } from '../../../../@core/services';
import { FinanceStoreConstants, FinanceAdjustment } from '../../finance.constants';

@Component({
  selector: 'adjustment-search',
  templateUrl: './adjustment-search.component.html',
  styleUrls: ['./adjustment-search.component.scss']
})
export class AdjustmentSearchComponent implements OnInit {

  ancillaryAdjustmentFormGroup: FormGroup;
  @Output() searchEmitter = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder, private _datatoreService: DataStoreService) { }

  ngOnInit() {
      this.ancillaryAdjustmentFormGroup = this.formBuilder.group({
        providerid: [null],
        paymentid: [null],
        providername: [null],
        clientid: [null],
        taxid: [null],
        clientname: [null],
        daterangeto: [null],
        daterangefrom: [null],
       // ayear: ['Y'],
        dobdaterangefrom: [null],
        dobdaterangeto: [null]
      });
      this.ancillaryAdjustmentFormGroup.get('daterangeto').disable();
      this.ancillaryAdjustmentFormGroup.get('dobdaterangeto').disable();
      this._datatoreService.clearStore();
      setTimeout(() => {
          const searchParams = this._datatoreService.getData(FinanceAdjustment.AncillaryAdjustmentSearchParams);
          if (searchParams) {
              this.ancillaryAdjustmentFormGroup.patchValue(searchParams);
          }
      }, 100);
  }

  onChangeDate(form, field) {
    if ( field === 'startdate' && form.get('daterangefrom').value ) {
      form.get('daterangeto').enable();
    } else if ( field === 'startdobdate' && form.get('dobdaterangefrom').value) {
      form.get('dobdaterangeto').enable();
    }
  }

  searchProviders() {
      const searchParams = this.ancillaryAdjustmentFormGroup.getRawValue();
      this._datatoreService.setData(FinanceAdjustment.AncillaryAdjustmentSearchParams, searchParams);
      this.searchEmitter.emit();
  }

  clearSearch() {
      this.ancillaryAdjustmentFormGroup.reset();
      this._datatoreService.setData(FinanceAdjustment.AncillaryAdjustmentSearchParams, null);
      this.searchEmitter.emit();
  }

}
