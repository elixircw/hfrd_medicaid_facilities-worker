import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { FinanceAccountsReceivableSearchEntry } from '../../_entities/finance-entity.module';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'accountsReceivable-search-view',
    templateUrl: './finance-accountsReceivable-search-view.component.html',
    styleUrls: ['./finance-accountsReceivable-search-view.component.scss']
})
export class FinanceAccountsReceivableSearchViewComponent implements OnInit {
    @Input() selectedAccountsReceivable: FinanceAccountsReceivableSearchEntry;
    constructor() {
    }
    ngOnInit() {
    }

}
