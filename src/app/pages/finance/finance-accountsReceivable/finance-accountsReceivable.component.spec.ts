// import { HttpClientModule } from '@angular/common/http';
// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RouterTestingModule } from '@angular/router/testing';
// import { NgSelectModule } from '@ng-select/ng-select';
// import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
// import { PaginationModule } from 'ngx-bootstrap';

// import { CoreModule } from '../../../@core/core.module';
// import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
// import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
// import { FinanceAccountsReceivableSearchFiltersComponent } from './finance-accountsReceivable-search-filters/finance-accountsReceivable-search-filters.component';
// import { FinanceAccountsReceivableSearchResultComponent } from './finance-accountsReceivable-search-result/finance-accountsReceivable-search-result.component';
// import { FinanceAccountsReceivableComponent } from './finance-accountsReceivable.component';

// describe('FinanceAccountsPayableComponent', () => {
//     let component: FinanceAccountsReceivableComponent;
//     let fixture: ComponentFixture<FinanceAccountsReceivableComponent>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             imports: [
//                 RouterTestingModule,
//                 CoreModule.forRoot(),
//                 HttpClientModule,
//                 FormsModule,
//                 ReactiveFormsModule,
//                 ControlMessagesModule,
//                 PaginationModule,
//                 NgSelectModule,
//                 A2Edatetimepicker,
//                 SharedPipesModule
//             ],
//             declarations: [FinanceAccountsReceivableComponent, FinanceAccountsReceivableSearchFiltersComponent, FinanceAccountsReceivableSearchResultComponent]
//         }).compileComponents();
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FinanceAccountsReceivableComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });
// });
