import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import {
  MatDatepickerModule, MatInputModule, MatCheckboxModule,
  MatSelectModule, MatFormFieldModule, MatRippleModule,
  MatButtonModule,
  MatRadioModule
} from '@angular/material';
import { FinanceAccountsReceivableRoutingModule } from './finance-accountsReceivable-routing.module';
import { FinanceAccountsReceivableSearchFiltersComponent } from './finance-accountsReceivable-search-filters/finance-accountsReceivable-search-filters.component';
import { FinanceAccountsReceivableSearchResultComponent } from './finance-accountsReceivable-search-result/finance-accountsReceivable-search-result.component';
import { FinanceAccountsReceivableSearchViewComponent } from './finance-accountsReceivable-search-view/finance-accountsReceivable-search-view.component';
import { FinanceAccountsReceivableComponent } from './finance-accountsReceivable.component';


@NgModule({
  imports: [
    CommonModule,
    FinanceAccountsReceivableRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    A2Edatetimepicker,
    NgSelectModule,
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatButtonModule,
    MatRadioModule,
    SharedPipesModule
  ],
  declarations: [FinanceAccountsReceivableComponent, FinanceAccountsReceivableSearchFiltersComponent, FinanceAccountsReceivableSearchResultComponent, FinanceAccountsReceivableSearchViewComponent]
})
export class FinanceAccountsReceivableModule { }
