import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService, CommonHttpService } from '../../../../@core/services';
import { FinanceStoreConstants } from '../../finance.constants';
import { FinanceUrlConfig } from '../../finance.url.config';
import { ObjectUtils } from '../../../../@core/common/initializer';

@Component({
  selector: 'app-finance-accountsReceivable-search-result',
  templateUrl: './finance-accountsReceivable-search-result.component.html',
  styleUrls: ['./finance-accountsReceivable-search-result.component.scss']
})
export class FinanceAccountsReceivableSearchResultComponent implements OnInit, OnDestroy {
  searchParams: any;
  providerList = [];
  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _datastoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {
  }

  getARSearchData(page) {
    this.paginationInfo.pageNumber = page;
    const searchParams = this._datastoreService.getData(FinanceStoreConstants.AccountsReceivableSearchParams);
    if (!searchParams) {
        this.searchParams = null;
        this.providerList = [];
        this.totalcount = 0;
    } else if (searchParams) {
      this.searchParams = searchParams;
      ObjectUtils.removeEmptyProperties(this.searchParams);
      this.paginationInfo.pageNumber = 1;
     this.getFilteredProviders();
    }
  }


  ngOnInit() {
    // const searchParams = this._datastoreService.getData(FinanceStoreConstants.AccountsReceivableSearchParams);
    // if (searchParams && JSON.stringify(this.searchParams) !== JSON.stringify(searchParams)) {
    //   this.searchParams = searchParams;
    // }
    // this.getFilteredProviders();
  }

  private getFilteredProviders() {
    this._commonHttpService.getPagedArrayList({
      limit: this.paginationInfo.pageSize,
      page: this.paginationInfo.pageNumber,
      where: this.searchParams,
      method: 'post'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.search.list)
      .subscribe((res: any) => {
        if (res) {
          this.providerList = res;
          this.totalcount = (this.providerList && this.providerList.length > 0) ? this.providerList[0].totalcount : 0;
        }
      });
  }

  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getFilteredProviders();
  }

  openProvider(provider) {
    this._datastoreService.setData(FinanceStoreConstants.SelectedProvider, provider);
    this._router.navigate(['../provider/' + provider.provider_id], { relativeTo: this._route });
  }

  ngOnDestroy(): void {
    this._datastoreService.setData(FinanceStoreConstants.AccountsReceivableSearchParams, null);
  }
}
