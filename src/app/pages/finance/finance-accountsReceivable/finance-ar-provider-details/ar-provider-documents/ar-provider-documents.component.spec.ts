import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArProviderDocumentsComponent } from './ar-provider-documents.component';

describe('ArProviderComponent', () => {
  let component: ArProviderDocumentsComponent;
  let fixture: ComponentFixture<ArProviderDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArProviderDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArProviderDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
