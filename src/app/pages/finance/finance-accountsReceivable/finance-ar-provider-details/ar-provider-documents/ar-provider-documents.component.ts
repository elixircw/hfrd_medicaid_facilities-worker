import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../../@core/services';
import { FinanceArProviderDetailsService } from '../finance-ar-provider-details.service';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { config } from '../../../../../../environments/config';
import { AppConfig } from '../../../../../app.config';

@Component({
  selector: 'documents',
  templateUrl: './ar-provider-documents.component.html',
  styleUrls: ['./ar-provider-documents.component.scss']
})
export class ArProviderDocumentsComponent implements OnInit {
  receivableDocumentList = [];
  doucmentFlag: boolean;
  
  constructor(
    private _commonHttpService: CommonHttpService,
    private _providerService: FinanceArProviderDetailsService
  ) { }

  ngOnInit() {
    this.receivableDocuments();
  }

  receivableDocuments() {
     this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
      where: {
        providerid: this._providerService.providerid
      },
      method: 'get',
    }), 'Accountreceivabledocuments/getaccountreceivabledocuments?filter'
    ).subscribe((result: any) => {
      if (result && result[0] && result[0].getaccountreceivabledocuments) {
        this.receivableDocumentList = result[0].getaccountreceivabledocuments;
      } else {
        this.doucmentFlag = true;
      }
    });
  }

  downloadFile(s3bucketpathname) {
    const workEnv = config.workEnvironment;
    let downldSrcURL;
    if (workEnv === 'state') {
        downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
    } else {
        downldSrcURL = s3bucketpathname;
    }
    console.log('this.downloadSrc', downldSrcURL);
    window.open(downldSrcURL, '_blank');
}

}
