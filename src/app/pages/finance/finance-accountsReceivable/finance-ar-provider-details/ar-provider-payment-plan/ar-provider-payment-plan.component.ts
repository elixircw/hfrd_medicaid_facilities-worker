import { Component, OnInit } from '@angular/core';
import { FinanceArProviderDetailsService } from '../finance-ar-provider-details.service';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { PaginationInfo } from '../../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../../finance.url.config';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ManualPayment } from '../../../_entities/finance-entity.module';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import * as moment from 'moment';

@Component({
  selector: 'ar-provider-payment-plan',
  templateUrl: './ar-provider-payment-plan.component.html',
  styleUrls: ['./ar-provider-payment-plan.component.scss']
})
export class ArProviderPaymentPlanComponent implements OnInit {
  paymentPlans = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  selectedPaymentPlan: any;
  paymentPlanForm: FormGroup;
  totalcount: number;
  totalAmount: number;
  receivedAmount: number;
  percentage: number;
  percentageOffset: number;
  noOfMonth: number;
  placementexists: boolean;
  manualPaymentStore: ManualPayment;
  payment_option_sw: string;
  percentage_check: any;
  amount_check: boolean;
  month_check: boolean;
  isEdit: boolean;
  isValid: boolean = true;
  isButtonClicked = false;
  constructor(
    private _providerService: FinanceArProviderDetailsService,
    private _commonHttpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private _alertService: AlertService
  ) { }

  ngOnInit() {
    this.getPaymentPlans();
    this.initPaymentPlanForm();
  }

  initPaymentPlanForm() {
    this.paymentPlanForm = this._formBuilder.group({
      entered_by: [null],
      start_dt: [null],
      amount_no: [null],
      end_dt: [null],
      current_receivable_amount: [null],
      percentage_no: [null],
      monthly_amount_active: [null],
      no_month_active: [null],
      months_no: [null],
      months_amount: [null],
      payment_option_sw: [null],
      offset_percentage: [null],
      month_check: [null],
      amount_check: [null],
      percentage_check: [null],
      reasons_tx: ['']
    });
    this.paymentPlanForm.get('months_no').valueChanges.subscribe(data => {
      this.onChangeStartDate();
    });
  }

  getPaymentPlans() {
    this._commonHttpService.getPagedArrayList({
      method: 'get',
      page: this.paginationInfo.pageNumber,
      limit: this.paginationInfo.pageSize,
      where: {
        providerid: this._providerService.providerid
      }
    },
    FinanceUrlConfig.EndPoint.accountsReceivable.paymentplan.list).subscribe((res: any) => {
      this.paymentPlans = res;
      this.totalcount = (this.paymentPlans && this.paymentPlans.length) ? this.paymentPlans[0].totalcount : 0;
    });
  }

  viewPaymentPlan(paymentPlan: any) {
    this.isValid = true;
    this.isEdit = false;
    this.selectedPaymentPlan = paymentPlan;
    this.paymentPlanForm.reset();
    this.paymentPlanForm.patchValue(this.selectedPaymentPlan);
    this.paymentPlanForm.patchValue({
      months_amount : paymentPlan.amount_no ? paymentPlan.amount_no : '0.00'
    });
    this.paymentPlanForm.disable();
    this.paymentPlanForm.disable();
    if (paymentPlan.placementexists === true) {
      this.placementexists = true;
       this.paymentPlanForm.disable();
       this.percentage = paymentPlan.percentage_no;
       this.receivedAmount = this.paymentPlanForm.get('current_receivable_amount').value;
        this.totalAmount = +((this.receivedAmount * (this.percentage / 100))).toFixed(2);
        this.noOfMonth =  Math.round(this.receivedAmount / this.totalAmount);
        setTimeout(() => {
          this.paymentPlanForm.patchValue ({
            monthly_amount_active: this.totalAmount,
            no_month_active: this.noOfMonth
          });
          const startDate = moment(this.paymentPlanForm.get('start_dt').value);
          const endDate = moment(startDate);
          endDate.add(this.paymentPlanForm.get('no_month_active').value, 'months');
          this.paymentPlanForm.patchValue({
            end_dt: endDate
          });
        }, 100);
       this.paymentPlanForm.get('offset_percentage').setValue(paymentPlan.percentage_no);
       this.paymentPlanForm.get('months_amount').setValue(0);
       this.paymentPlanForm.get('percentage_no').setValue(0);
    }
    (<any>$('#paymentplan-details')).modal('show');
  }
  editPaymentPlan(paymentPlan: any) {
    this.isValid = true;
    this.isEdit = true;
    this.selectedPaymentPlan = paymentPlan;
    this.paymentPlanForm.reset();
    this.paymentPlanForm.patchValue(this.selectedPaymentPlan);
    this.paymentPlanForm.patchValue({
      months_amount : paymentPlan.amount_no ? paymentPlan.amount_no : '0.00'
    });
    this.paymentPlanForm.disable();
    if (!this.paymentPlanForm.get('payment_option_sw').value && !paymentPlan.payment_option_sw) {
      this.paymentPlanForm.patchValue({
        payment_option_sw: 'P'
      });
      paymentPlan.payment_option_sw = 'P';
    }
    if (paymentPlan.placementexists === true) {
      this.placementexists = true;
        this.percentage = +event;
       this.paymentPlanForm.disable();
       this.paymentPlanForm.get('offset_percentage').setValue(paymentPlan.percentage_no);
       this.paymentPlanForm.get('months_amount').setValue(0);
       this.paymentPlanForm.get('percentage_no').setValue(0);
    } else if (paymentPlan.approvalstatus == '3047') {
      this.placementexists = false;
      if (paymentPlan.payment_option_sw === 'P') {
        this.percentage_check = true;
        this.paymentPlanForm.get('percentage_no').enable();
        this.paymentPlanForm.get('months_amount').disable();
        this.paymentPlanForm.get('months_no').disable();
      } else if (paymentPlan.payment_option_sw === 'A') {
          this.amount_check = true;
          this.paymentPlanForm.get('months_amount').enable();
          this.paymentPlanForm.get('percentage_no').disable();
          this.paymentPlanForm.get('months_no').disable();
      } else if (paymentPlan.payment_option_sw === 'M') {
        this.month_check = true;
        this.paymentPlanForm.get('months_no').enable();
        this.paymentPlanForm.get('months_amount').disable();
        this.paymentPlanForm.get('percentage_no').disable();
      }
      this.paymentPlanForm.get('payment_option_sw').enable();
      this.paymentPlanForm.get('reasons_tx').enable();
    } else {
      this.paymentPlanForm.disable();
      this.placementexists = true;
    }
    if (this.paymentPlanForm.get('payment_option_sw').value) {
      if (this.paymentPlanForm.get('payment_option_sw').value === 'P') {
        this.calPercentage(this.paymentPlanForm.get('percentage_no').value);
      } else if (this.paymentPlanForm.get('payment_option_sw').value === 'A') {
        this.calMonthlyAmount(this.paymentPlanForm.get('months_amount').value);
      } else if (this.paymentPlanForm.get('payment_option_sw').value === 'M') {
        this.calNumberOfMonths(this.paymentPlanForm.get('months_no').value);
      }
    }
    this.paymentPlanForm.get('start_dt').enable();
    (<any>$('#paymentplan-details')).modal('show');
  }

  onChangeStartDate() {
    const startdate = this.paymentPlanForm.get('start_dt').value;
    const month = this.paymentPlanForm.get('months_no').value;
    if (startdate) {
      const enddate = moment(startdate).add(month, 'M');
      this.paymentPlanForm.patchValue({
        end_dt: enddate
      });
    }
  }

  pageChanged(page: any) {
    this.paginationInfo.pageNumber = page;
    this.getPaymentPlans();
  }

  onSelectNumberOfMonths() {
    // this.paymentPlanForm.get('percentage_no').reset();
    // this.paymentPlanForm.get('months_no').reset();
    // this.paymentPlanForm.get('months_amount').reset();
    this.paymentPlanForm.get('percentage_no').disable();
    this.paymentPlanForm.get('months_no').enable();
    this.paymentPlanForm.get('months_amount').disable();
  }

  calNumberOfMonths(event) {
    this.noOfMonth = +event;
    this.receivedAmount = this.paymentPlanForm.get('current_receivable_amount').value;
    this.totalAmount = +(this.receivedAmount / this.noOfMonth).toFixed(2);
    this.percentage = +((this.totalAmount / this.receivedAmount) * 100).toFixed(2);
    this.payment_option_sw = 'M';
    setTimeout(() => {
      this.paymentPlanForm.patchValue ({
        amount_no: this.totalAmount,
        months_amount: this.totalAmount,
        percentage_no: this.percentage,
        months_no: this.noOfMonth
      });
    }, 100);
    this.validate();
  }

  calOffsetPercentage(event) {
    this.percentageOffset = +event;

    this.validate();
  }

  onSelectPercentage() {
    // this.paymentPlanForm.get('percentage_no').reset();
    // this.paymentPlanForm.get('months_no').reset();
    // this.paymentPlanForm.get('months_amount').reset();
    this.paymentPlanForm.get('percentage_no').enable();
    this.paymentPlanForm.get('months_no').disable();
    this.paymentPlanForm.get('months_amount').disable();
  }

  calPercentage(event) {
    this.percentage = +event;
    this.receivedAmount = this.paymentPlanForm.get('current_receivable_amount').value;
    this.totalAmount = +((this.receivedAmount * (this.percentage / 100))).toFixed(2);
    this.noOfMonth =  Math.round(this.receivedAmount / this.totalAmount);
    this.payment_option_sw = 'P';
    setTimeout(() => {
      this.paymentPlanForm.patchValue ({
        amount_no: this.totalAmount,
        months_amount: this.totalAmount,
        percentage_no: this.percentage,
        months_no: this.noOfMonth
      });
    }, 100);
    this.validate();
  }

  private validate() {
    if (this.percentage < 2.78) {
      this.isValid = false;
      return this._alertService.warn('Minimum Inactive / Active status Percentage should be greater than 2.78%');
    } else if (this.percentage > 100) {
      this.isValid = false;
      return this._alertService.warn('Maximum Inactive / Active status Percentage should be lesser than 100%');
    } else if (this.noOfMonth > 36) {
      this.isValid = false;
      return this._alertService.warn('Maximum number months should be less than 36 months.');
    } else if (this.percentageOffset < 25) {
      return this._alertService.warn('Minimum Active status Percentage should be greater than 25%');
    } else if (this.percentageOffset > 100) {
      return this._alertService.warn('Maximum Active status Percentage should be lesser than 25%');
    }
    this.isValid = true;
  }

  onMonthlyAmount() {
    // this.paymentPlanForm.get('percentage_no').reset();
    // this.paymentPlanForm.get('months_no').reset();
    // this.paymentPlanForm.get('months_amount').reset();
    this.paymentPlanForm.get('percentage_no').disable();
    this.paymentPlanForm.get('months_no').disable();
    this.paymentPlanForm.get('months_amount').enable();
  }

  calMonthlyAmount(event) {
    this.totalAmount = +event;
    this.receivedAmount = this.paymentPlanForm.get('current_receivable_amount').value;
    this.noOfMonth = +Math.round(this.receivedAmount / this.totalAmount);
    this.percentage = +((this.totalAmount / this.receivedAmount) * 100).toFixed(2);
    this.payment_option_sw = 'A';
    setTimeout(() => {
      this.paymentPlanForm.patchValue ({
        amount_no: this.totalAmount,
        months_amount: this.totalAmount,
        percentage_no: this.percentage,
        months_no: this.noOfMonth
      });
    }, 100);
  }

  savePayment() {
    this.isButtonClicked = true;
    this.validate();
    if (this.isValid) {
      setTimeout(() => {
        this._commonHttpService.endpointUrl = 'tb_payment_plan/paymentplanedit';
        this.manualPaymentStore = this.paymentPlanForm.getRawValue();
        this.manualPaymentStore.payment_option_sw = this.payment_option_sw;
        this.manualPaymentStore.placementexists = this.placementexists;
        this.manualPaymentStore.payment_plan_id = this.selectedPaymentPlan.payment_plan_id;
        this.manualPaymentStore.plan_dt = this.selectedPaymentPlan.plan_dt;
        this.manualPaymentStore.receivable_id = this.selectedPaymentPlan.receivable_id;
        this.manualPaymentStore.amount_no = this.totalAmount ? this.totalAmount : this.selectedPaymentPlan.amount_no;
        this.manualPaymentStore.offset_sw = this.selectedPaymentPlan.offset_sw;
        this.manualPaymentStore.offset_option_sw = this.selectedPaymentPlan.offset_option_sw;
        this.manualPaymentStore.securityusersid = this.selectedPaymentPlan.securityusersid;
        // this.manualPaymentStore.reasons_tx = this.selectedPaymentPlan.reasons_tx;
        this._commonHttpService.create(this.manualPaymentStore).subscribe(
            (response) => {
              this._alertService.success('Payment Saved Successfully');
              (<any>$('#paymentplan-details')).modal('hide');
              this.getPaymentPlans();
              this.isButtonClicked = false;
            },
            (error) => {
              this.isButtonClicked = false;
              (<any>$('#paymentplan-details')).modal('hide');
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
          );
      }, 100);
    }
  }

}
