import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FinanceArProviderDetailsService } from './finance-ar-provider-details.service';
import { DataStoreService } from '../../../../@core/services';
import { FinanceStoreConstants } from '../../finance.constants';

@Component({
    selector: 'finance-ar-provider-details',
    templateUrl: './finance-ar-provider-details.component.html',
    styleUrls: ['./finance-ar-provider-details.component.scss']
})
export class FinanceArProviderDetailsComponent implements OnInit {
    tabDetails = [];

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _financeDetailsService: FinanceArProviderDetailsService,
        private _datastoreService: DataStoreService
    ) {
        _financeDetailsService.providerid = _route.snapshot.params['providerid'];
    }

    ngOnInit() {
        const selectedProvider = this._datastoreService.getData(FinanceStoreConstants.SelectedProvider);
        if (!selectedProvider) {
            this._financeDetailsService.loadProviderDetails();
        } else {
            this._financeDetailsService.provider = selectedProvider;
        }
        this.initTabs();
    }

    backToSearch() {
        this._datastoreService.setData(FinanceStoreConstants.AccountsReceivableSearchParams, null);
        this._router.navigate(['/pages/finance/finance-accountsReceivable/search-result']);
    }

    initTabs() {
        this.tabDetails = [
            {
                id: 'details',
                title: 'Summary',
                name: 'Summary',
                route: 'details'
            },
            {
                id: 'overpayments',
                title: 'Provider Overpayments',
                name: 'Provider Overpayments',
                route: 'overpayments'
            },
            {
                id: 'history',
                title: 'Overpayments History',
                name: 'Overpayments History',
                route: 'history'
            },
            {
                id: 'payment-plan',
                title: 'Payment Plan',
                name: 'Payment Plan',
                route: 'payment-plan'
            },
            {
                id: 'documents',
                title: 'Documents',
                name: 'Documents',
                route: 'documents'
            }
        ];
    }

}
