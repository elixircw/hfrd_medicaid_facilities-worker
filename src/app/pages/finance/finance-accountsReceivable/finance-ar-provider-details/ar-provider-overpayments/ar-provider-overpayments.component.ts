import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, CommonDropdownsService, AuthService, SessionStorageService } from '../../../../../@core/services';
import { FinanceArProviderDetailsService } from '../finance-ar-provider-details.service';
import { PaginationInfo, PaginationRequest, DropdownModel } from '../../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../../finance.url.config';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import * as _ from 'lodash';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { AppConfig } from '../../../../../app.config';
import { HttpHeaders } from '@angular/common/http';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import * as FileSaver from 'file-saver';
import { HttpService } from '../../../../../@core/services/http.service';

@Component({
  selector: 'ar-provider-overpayments',
  templateUrl: './ar-provider-overpayments.component.html',
  styleUrls: ['./ar-provider-overpayments.component.scss']
})
export class ArProviderOverpaymentsComponent implements OnInit {
  overpayments = [];
  overpaymentvalue = [];
  changehistory = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalcount: number;
  collectionStatus = [];
  arStatus = [];
  selectedOverPayment: any = {};
  overpaymentsForm: FormGroup;
  isEdit: boolean;
  buttonText: string;
  totalAmount: string;
  totalReceivableAmount: string;
  isFW: boolean;
  isSupervisor: boolean;
  isApproved: boolean;
  isPendingWithSupervisor: boolean;
  getUsersList: any[];
  originalUserList: any[];
  selectedPerson: any;
  assignedTo: any;
  isWriteOff: boolean;
  initialBalance: number;
  overpaymentId: any;
  adjustment: any[];
  receivableid: any;
  receivable_detail_id: any;
  arstatus: any;
  prevWriteOffAmount: any;
  manualpaymentsForm: FormGroup;
  selectedPayment: any[];
  totalpagecount: number;
  pageInfo: PaginationInfo = new PaginationInfo();
  paymentMethod$: Observable<DropdownModel[]>;
  selectedOriginalPayment: any;
  overPayment: boolean;
  intakeserviceid: any;
  paymentDetailID: any;
  receivableType: any;
  buttonVisible: boolean;
  isAncillary: boolean;
  alertTxt: string;
  manualOverPayment: any;
  approveStatus: number;
  arStatusOriginal: any[];
  maxDate = new Date();
  startDate = new Date();
  overpaymentFlag: boolean;
  receivableHistoryList: any[];
  isButtonClicked = false;
  underPayment: any;
  approveTxt: string;
  is_resend: boolean;
  disablePaymentSave: boolean;
  receivableColletionStatusList: any[];
  restitutionupload: File;
  restitutionuploadedFile: any;
  private token: AppUser;
  clientID: any;
  activeModule: any;
  userProfile: AppUser;
  assignedUser: boolean;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _providerService: FinanceArProviderDetailsService,
    private _formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _commonDropdownService: CommonDropdownsService,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService,
    private _sessionStorage: SessionStorageService,
    private http: HttpService) {
      this.token = this._authService.getCurrentUser(); }

  ngOnInit() {
    this.getOverPayments();
    this.loadDropdowns();
    this.initFormGroup();
    // this.fiscalAudit();
    this.userProfile = this._authService.getCurrentUser();
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    this.isFW = this._providerService.isFW();
    this.isSupervisor = this._providerService.isSupervisor();
    if (this.activeModule === 'Finance') {
      this.isSupervisor = false;
      this.isFW = true;
      this.buttonText = 'Update Overpayments';
    } else if (this.activeModule === 'Finance Approval') {
      this.isSupervisor = true;
      this.isFW = false;
    }
    this.overpaymentsForm.get('receivable_balance_no').disable();
    this.overpaymentsForm.get('written_off_request_amount_no').disable();
  }

  initFormGroup() {
    this.overpaymentsForm = this._formBuilder.group({
      write_off_approval_status: [null],
      receivable_status_cd: [null, Validators.required],
      write_off_action_date: [null],
      comments_tx: [null, Validators.required],
      notes_tx: [null],
      collection_status_cd: [{ value: null, disabled: false }],
      start_dt: [{ value: null, disabled: true }],
      end_dt: [{ value: null, disabled: true }],
      amount_no: [{ value: null, disabled: true }],
      receivable_balance_no: [{ value: null, disabled: true }],
      intakeserviceid: [null],
      writeOffAmount: [null],
      written_off_request_amount_no: [null]
    });
    this.manualpaymentsForm = this._formBuilder.group({
      receivable_status_cd: [null, Validators.required],
      comments_tx: [null, Validators.required],
      notes_tx: [null],
      collection_status_cd: [{ value: null, disabled: false }],
      // start_dt: [null, Validators.required],
      // end_dt: [null, Validators.required],
      start_dt: [null],
      end_dt: [null],
      amount_no: [null, Validators.required],
      receivable_balance_no: [{ value: null, disabled: true }],
      payment_id: [null],
      payment_method_cd: '',
      payment_date: [null],
      payment_amount: [null]
      });
  }

  loadDropdowns() {
    this.arStatus = [];
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '7'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
     // this.arStatus = res;
     this.arStatusOriginal = res;
     res.forEach(element => {
        if (this.activeModule === 'Finance Approval') {
          if (element.description_tx !== 'Write-Off Request' ) {
            this.arStatus.push(element);
          }
        } else {
          if (element.description_tx !== 'Write-Off') {
            this.arStatus.push(element);
          }
        }
      });
    });
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '52'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.collectionStatus = res;
    });
  }

  private getOverPayments() {
    this._commonHttpService.getPagedArrayList({
      where: {
        providerid: this._providerService.providerid,
        client_id: null
      },
      page: this.paginationInfo.pageNumber,
      limit: this.paginationInfo.pageSize,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.list).subscribe((res: any) => {
      if (res && res.data && res.data.length) {
        this.overpayments = res.data;
        this.overpayments.map(op => {
          op.ageing = op.receivable_balance_no === '0.00' ? 'N/A' : moment().diff(moment(op.receivable_ts).format('MM/DD/YYYY'), 'days');
        });
        this.receivableid = res.data.receivable_detail_id;
        // for (let i = 0;  i < res.data.length; i++) {
        //   if (res.data.receivable_detail_id === this.overpaymentId) {
        //     this.overpaymentvalue = res.data;
        //   }
        // }
        this.totalAmount = res.totalamount;
        this.totalReceivableAmount = res.totalreceivable_balance_no;
        this.underPayment = res.under_payment;
        if (res.data[0].isancillary === true) {
          this.isAncillary = true;
        } else {
          this.isAncillary = false;
        }
        this.totalcount = (this.overpayments && this.overpayments.length > 0) ? this.overpayments[0].totalcount : 0;
      } else {
        this.isAncillary = true;
        this.overpaymentFlag = true;
      }
    });
  }

  fiscalAudit() {
    this.changehistory = [];
    this.adjustment = [];
    for (let i = 0;  i < this.overpayments.length; i++) {
      if (this.overpayments[i].receivable_detail_id === this.overpaymentId) {
        this.overpaymentvalue = this.overpayments[i];
      }
    }
    this._commonHttpService.getPagedArrayList({
      where: {
        receivable_detail_id: this.overpaymentId,
        clientid: this.clientID
      },
      page: this.paginationInfo.pageNumber,
      limit: this.paginationInfo.pageSize,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.audit.auditList).subscribe((result: any) => {
        if (result && result.data && result.data.length > 0) {
          this.changehistory = result.data;
          for (let i = 0;  i < result.data.length; i++) {
            if (result.data[i].adjstment) {
              this.adjustment = result.data[i].adjstment;
            }
          }
        }
    });
  }

  onARStatusChange(statusCode: string) {
    this.isWriteOff = ['21', '22'].includes(statusCode);
    /* if (this.isWriteOff) {
      this.overpaymentsForm.get('receivable_balance_no').disable();
      this.overpaymentsForm.get('writeOffAmount').setValidators(Validators.required);
    } else {
      this.overpaymentsForm.get('receivable_balance_no').enable();
      this.overpaymentsForm.get('writeOffAmount').clearValidators();
    }*/
    this.overpaymentsForm.get('writeOffAmount').updateValueAndValidity();
    if (this.activeModule === 'Finance') {
      this.buttonText = (statusCode === '22') ? 'Send For Approval' : 'Update Overpayments';
    } else if (this.activeModule === 'Finance Approval') {
      this.buttonText = (this.approveStatus === 30 && (statusCode === '21' || statusCode === '22')) ? 'Approve' : 'Update Overpayments';
    }
  }

  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getOverPayments();
  }

  viewOverPayment(overpayment: any) {
    this.selectedOverPayment = overpayment;
    this.overpaymentId = overpayment.receivable_detail_id;
    this.clientID = overpayment.client_id;
    this.getReceivableHistory(this.overpaymentId);
    this.overpaymentsForm.patchValue(overpayment);
    this.initialBalance = parseFloat(this.overpaymentsForm.get('receivable_balance_no').value ? this.overpaymentsForm.get('receivable_balance_no').value : 0);
    if (this.activeModule === 'Finance Approval') {
      if (overpayment.receivable_status_cd === '22') {
        this.overpaymentsForm.patchValue({
          receivable_status_cd: (this.initialBalance) ? '21' : '20',
        });
      } else {
        this.overpaymentsForm.patchValue({
          receivable_status_cd: (this.initialBalance) ? overpayment.receivable_status_cd : '20',
        });
      }
    } else {
        this.overpaymentsForm.patchValue({
          receivable_status_cd: (this.initialBalance) ?  overpayment.receivable_status_cd : '20',
        });
    }
    setTimeout(() => {
    this.overpaymentsForm.patchValue({
     // start_dt: serviceStartDate,
     // end_dt: serviceEndDate,
     written_off_request_amount_no: overpayment.written_off_request_amount_no ? overpayment.written_off_request_amount_no : '0.00',
     writeOffAmount : overpayment.written_off_amount_no ? overpayment.written_off_amount_no : '0.00'
    });
  }, 100);
    this.overpaymentsForm.disable();
    this.isEdit = false;
    this.isApproved = (overpayment.write_off_approval_status === '3047');
    this.overpaymentsForm.get('writeOffAmount').setValue(overpayment.written_off_amount_no);
    this.approveStatus = overpayment.status_cd;
    this.onARStatusChange(overpayment.receivable_status_cd);
    this.writeOffChanged(); (<any>$('#overpayment-details')).modal('show');
  }

  isEditAllowed(overpayment: any) {
    return !(this.activeModule === 'Finance' && (overpayment.receivable_status === 'Pending' || overpayment.receivable_status === 'Approved')) &&
      !(this.activeModule === 'Finance Approval' && (overpayment.receivable_status === 'Approved'));
  }

  editOverPayment(overpayment: any) {
    this.disablePaymentSave = true;
    this.overpaymentId = overpayment.receivable_detail_id;
    this.clientID = overpayment.client_id;
    this.paymentDetailID = overpayment.payment_detail_id;
    this.getReceivableHistory(this.overpaymentId);
    this.receivable_detail_id = overpayment.receivable_detail_id;
    this.intakeserviceid = overpayment.intakeserviceid;
    if (overpayment.routingstatustypeid === 73) {
      this.buttonVisible = true;
    } else {
      this.buttonVisible = false;
      this.getPaymentMethod();
    }
    if (this.userProfile.user.securityusersid === overpayment.writeoffassigneduser) {
      this.assignedUser = true;
    } else {
      if (overpayment.receivable_status === 'Pending') {
        this.overpaymentsForm.disable();
      }
      this.assignedUser = false;
    }
    this.selectedOverPayment = overpayment;
    this.initFormGroup();
    this.overpaymentsForm.patchValue(overpayment);
    this.isApproved = (overpayment.write_off_approval_status === '3047');
    this.isPendingWithSupervisor = (this.activeModule === 'Finance Approval' && (overpayment.write_off_approval_status === '3045'));

    if (this.activeModule === 'Finance' && ['3045', '3047'].includes(overpayment.write_off_approval_status)) {
      this.overpaymentsForm.get('receivable_status_cd').disable();
    }
    this.isEdit = true;
    this.initialBalance = parseFloat(this.overpaymentsForm.get('receivable_balance_no').value ? this.overpaymentsForm.get('receivable_balance_no').value : 0);
    if (this.activeModule === 'Finance Approval') {
      if (overpayment.receivable_status_cd === '22') {
        this.arstatus = 'Write-Off';
        this.overpaymentsForm.patchValue({
          receivable_status_cd: (this.initialBalance) ? '21' : '20',
        });
      }
    } else {
        this.overpaymentsForm.patchValue({
          receivable_status_cd: (this.initialBalance) ?  overpayment.receivable_status_cd : '20',
        });
        this.arstatus = overpayment.arstatus;
    }
    const arStatusBal = ['19, 21, 22'];
    const arStatusPif = ['20'];
    if (this.initialBalance) {
      this.arStatus =  this.arStatus.filter(status => status.picklist_value_cd !== '20');
    } else {
      this.arStatus =  this.arStatus.filter(status => {
        return ( arStatusBal.indexOf(status.picklist_value_cd) > -1);
      });
    }
    this.prevWriteOffAmount = overpayment.written_off_amount_no;
    this.overpaymentsForm.get('writeOffAmount').setValue(overpayment.written_off_amount_no);
    this.approveStatus = overpayment.status_cd;
    this.onARStatusChange(overpayment.receivable_status_cd);
    this.writeOffChanged();
    if (this.activeModule === 'Finance' && (overpayment.receivable_status === 'Approved' || overpayment.receivable_status === 'Denied') && overpayment.receivable_status_cd == '21') {
      if (this.arStatus && !this.arStatus.find(ele => ele.picklist_value_cd == '22')) {
        this.arStatus.push(this.arStatusOriginal.find(ele => ele.picklist_value_cd == '22'));
      }
      this.overpaymentsForm.patchValue({
        receivable_status_cd: '22'
      });
      this.onARStatusChange('22');
    }
    setTimeout(() => {
      this.overpaymentsForm.patchValue({
        written_off_request_amount_no: overpayment.written_off_request_amount_no ? overpayment.written_off_request_amount_no : '0.00',
        writeOffAmount: overpayment.written_off_amount_no ? overpayment.written_off_amount_no : '0.00'
      });
      this.overpaymentsForm.get('writeOffAmount').setValidators(Validators.required);
      this.overpaymentsForm.get('receivable_status_cd').enable();
      this.overpaymentsForm.get('collection_status_cd').enable();
      this.overpaymentsForm.get('written_off_request_amount_no').disable();
      if (this.initialBalance === 0) {
        this.overpaymentsForm.get('receivable_status_cd').disable();
      }
      if (this.activeModule === 'Finance Approval') {
        this.overpaymentsForm.get('writeOffAmount').disable();
      }
    }, 100);
    if (this.isFW && overpayment.routingstatustypeid === 75) {
      this.is_resend = true;
      this.approveTxt = 'Resend for approval';
      this.manualpaymentsForm.patchValue(overpayment);
      this.manualpaymentsForm.disable();
      this.manualpaymentsForm.get('amount_no').enable();
      this.manualpaymentsForm.get('comments_tx').enable();
      this.manualpaymentsForm.get('notes_tx').enable();
      (<any>$('#manual-payment-details')).modal('show');
    } else {
      (<any>$('#overpayment-details')).modal('show');
    }
  }

  addManual() {
    this.disablePaymentSave = false;
    this.is_resend = false;
    this.approveTxt = 'send for approval';
  }

  updateOverPayments() {
    this.isButtonClicked = true;
    const overpayment = this.overpaymentsForm.getRawValue();
    if (this.activeModule === 'Finance Approval' && overpayment.receivable_status_cd === '21') {
      overpayment.eventcode = 'FNSWO';
      overpayment.write_off_approval_status = '3047';
      overpayment.write_off_action_date = moment().format('YYYY-MM-DD');
    }

    overpayment.payment_id = this.selectedOverPayment.payment_id;
    overpayment.receivable_balance_amount = parseFloat(this.isWriteOff ? overpayment.writeOffAmount : overpayment.receivable_balance_no);
    overpayment.provider_id = this._providerService.providerid;
    if (overpayment.receivable_balance_amount <= parseFloat(this.selectedOverPayment.receivable_balance_no)) {
      delete overpayment.receivable_balance_no;
      this._commonHttpService.patch(this.selectedOverPayment.receivable_detail_id, overpayment, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.update)
        .subscribe(res => {
          let message;
          if (this.activeModule === 'Finance') {
            message = (this.buttonText === 'Update Overpayments')
            ? 'Overpayment updated successfully' : 'Write off send for approval';
          } else if (this.activeModule === 'Finance Approval') {
            message = (this.buttonText === 'Update Overpayments')
            ? 'Overpayment updated successfully' : 'Write off approved Successfully';
          }
          this._alertService.success(message);
          this.isButtonClicked = false;
          (<any>$('#overpayment-details')).modal('hide');
          this.getOverPayments();
        });
    } else {
      this.isButtonClicked = false;
      this._alertService.warn('Balance cannot be greater than overpayment');
    }
  }

  writeOffChanged() {
    if (this.overpaymentsForm.get('write_off_approval_status').value !== '3047') {
      const writeOff = parseFloat(this.overpaymentsForm.get('writeOffAmount').value ? this.overpaymentsForm.get('writeOffAmount').value : 0);
      const updatedBalance = this.initialBalance - writeOff;
      // this.overpaymentsForm.get('receivable_balance_no').setValue(updatedBalance);
    }
  }

  private assignUser() {

    const overpayment = this.overpaymentsForm.getRawValue();
    if (this.activeModule === 'Finance' && overpayment.receivable_status_cd === '22') {
      overpayment.eventcode = 'FNSWO';
      overpayment.write_off_approval_status = '3045';
      overpayment.write_off_request_date = moment().format('YYYY-MM-DD');
      overpayment.assignedtoid = this.assignedTo;
    }
    overpayment.payment_id = this.selectedOverPayment.payment_id;
    overpayment.receivable_balance_amount = parseFloat(this.isWriteOff ? overpayment.writeOffAmount : overpayment.receivable_balance_no);
    overpayment.provider_id = this._providerService.providerid;
    if (overpayment.receivable_balance_amount <= parseFloat(this.selectedOverPayment.receivable_balance_no)) {
      delete overpayment.receivable_balance_no;
      this._commonHttpService.patch(this.selectedOverPayment.receivable_detail_id, overpayment, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.update)
        .subscribe(res => {
          const message = (this.buttonText === 'Update Overpayments')
            ? 'Overpayment updated successfully' : 'Write off send for approval';
          this._alertService.success(message);
          this.isButtonClicked = false;
          (<any>$('#payment-approval')).modal('hide');
          this.getOverPayments();
        });
    } else {
      this.isButtonClicked = false;
      this._alertService.warn('Balance cannot be greater than overpayment');
    }

  }

  selectPerson(row) {
    if (row) {
      this.selectedPerson = row;
      this.assignedTo = row.userid;
    }
  }

  getRoutingUser(type) {
    this.isButtonClicked = true;
    if (type === 'overPayment') {
      const overpayment = this.overpaymentsForm.getRawValue();
      this.overPayment = true;
      if (overpayment.receivable_balance_no < parseFloat(overpayment.writeOffAmount)) {
        this._alertService.warn('Balance cannot be greater than overpayment');
        this.isButtonClicked = false;
        return false;
      }

      (<any>$('#overpayment-details')).modal('hide');
      (<any>$('#payment-approval')).modal('show');
    } else {
      const manualpaymentsForm = this.manualpaymentsForm.getRawValue();
      if (!manualpaymentsForm.amount_no || manualpaymentsForm.amount_no == '0.00') {
        this._alertService.warn('Please enter Overpayment Amount');
        this.isButtonClicked = false;
        return false;
      }
      this.overPayment = false;
      (<any>$('#manual-payment-details')).modal('hide');
      (<any>$('#payment-approval')).modal('show');
    }
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'FNSWO' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe((result) => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  listUser(assigned: string) {
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter((res) => {
        if (res.userrole === 'LDSS Fiscal Supervisor' && res.userid !== this.token.user.securityusersid) {
          return res;
        }
      });
    }
  }

  checkDec(el) {
    if (el.target.value !== '') {
      return el.target.value = isNaN(el.target.value) ? '' : el.target.value.replace(/[^0-9.]{0,2}/g, '');
    }
    return '';
  }

  selectPaymentDetails() {
    (<any>$('#manual-payment-details')).modal('hide');
    this.getPaymentMethod();
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        where: {providerid: this._providerService.providerid},
        limit: this.pageInfo.pageSize,
        page: this.pageInfo.pageNumber,
        method: 'get'
    }), 'tb_payment_header/getpaymentdetail' + '?filter'
    ).subscribe(response => {
      this.selectedPayment = response.data;
      for (let i = 0; i < this.selectedPayment.length; i++) {
        this.selectedPayment[i].payment_date = this._commonDropdownService.getValidDate(this.selectedPayment[i].payment_date);
        this.selectedPayment[i].final_service_start_dt = this._commonDropdownService.getValidDate(this.selectedPayment[i].final_service_start_dt);
        this.selectedPayment[i].final_service_end_dt = this._commonDropdownService.getValidDate(this.selectedPayment[i].final_service_end_dt);

      }
      this.totalpagecount = (this.selectedPayment && this.selectedPayment.length > 0) ? this.selectedPayment[0].totalcount : 0;
    });
  }

  pageNumberChanged(page) {
    this.pageInfo.pageNumber = page;
    this.selectPaymentDetails();
  }

  selectOriginalPayment(payment) {
    console.log('payment', payment);
    this.intakeserviceid = payment.intakeserviceid;
    this.paymentDetailID = payment.payment_detail_id;
    this.receivableType = payment.receivable_type;
    this.selectedOriginalPayment = payment;
    this.manualpaymentsForm.patchValue(payment);
    this.manualpaymentsForm.patchValue({
      receivable_status_cd: payment.arstatus,
      collection_status_cd: payment.colletion_status,
      receivable_balance_no: '0.00'
    });
    this.manualpaymentsForm.disable();
    this.manualpaymentsForm.get('start_dt').enable();
    this.manualpaymentsForm.get('end_dt').enable();
    this.manualpaymentsForm.get('amount_no').enable();
    this.manualpaymentsForm.get('comments_tx').enable();
    this.manualpaymentsForm.get('notes_tx').enable();

  }

  getPaymentMethod() {
    this.paymentMethod$ = this._commonHttpService.getArrayList({
      where: {picklist_type_id : '1'},
      nolimit: true,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsPayable.PaymentPickList + '?filter'
    ) .map((result) => {
      return result.map(
          (res) =>
              new DropdownModel({
                  text: res.description_tx,
                  value: res.picklist_value_cd
              })
      );
  });
  }

  getRoutingForManualUser() {
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'FNSWO' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe((result) => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  paymentAmount(amount) {
    if (parseFloat(amount) > this.manualpaymentsForm.get('payment_amount').value) {
      this._alertService.warn('Not allowed to send approve more than the payment amount');
      this.manualpaymentsForm.get('amount_no').reset();
      return false;
    } else {
      this.manualpaymentsForm.patchValue({
        amount_no: _.toNumber(amount).toFixed(2),
        receivable_balance_no: _.toNumber(amount).toFixed(2)
      });
    }
  }

  manualPaymentApproval(type) {
    this.isButtonClicked = true;
    if (type === 'approve') {
      if (this.activeModule === 'Finance Approval') {
        this.manualOverPayment = this.overpaymentsForm.getRawValue();
        this._commonHttpService.endpointUrl = FinanceUrlConfig.EndPoint.accountsReceivable.manualOverPayment.approve;
        this.manualOverPayment.status = 74;
        this.manualOverPayment.receivable_detail_id = this.receivable_detail_id;
        this.alertTxt = 'Manual AR request Approved successfully!';
      } else {
        this.manualOverPayment = this.manualpaymentsForm.getRawValue();
        this._commonHttpService.endpointUrl = FinanceUrlConfig.EndPoint.accountsReceivable.manualOverPayment.request;
        this.manualOverPayment.status = 73;
        this.alertTxt = 'Manual AR Approval Request sent successfully!';
      }
    } else {
      if (this.activeModule === 'Finance Approval') {
        // Reject - Manual Payment
        this.manualOverPayment = this.overpaymentsForm.getRawValue();
        this._commonHttpService.endpointUrl = FinanceUrlConfig.EndPoint.accountsReceivable.manualOverPayment.approve;
        this.manualOverPayment.status = 75;
        this.manualOverPayment.receivable_detail_id = this.receivable_detail_id;
        this.alertTxt = 'Manual Payment request Denied successfully!';
      }
    }
    if (this.is_resend) {
      this.manualOverPayment.receivable_detail_id = this.receivable_detail_id;
    }
    this.manualOverPayment.is_resend = this.is_resend;
    this.manualOverPayment.balance_no = this.manualOverPayment.amount_no;
    this.manualOverPayment.receivable_balance_no = this.manualOverPayment.amount_no;
    this.manualOverPayment.eventcode = 'MANREC';
    this.manualOverPayment.intakeserviceid = this.intakeserviceid;
    this.manualOverPayment.assignedtoid = this.assignedTo;
    this.manualOverPayment.provider_id = this._providerService.providerid;
    this.manualOverPayment.payment_detail_id = this.paymentDetailID;
    this.manualOverPayment.receivable_type = this.receivableType;
    this._commonHttpService.create(this.manualOverPayment).subscribe(
      (response) => {
        this.isButtonClicked = false;
        (<any>$('#payment-approval')).modal('hide');
        (<any>$('#overpayment-details')).modal('hide');
        this.getOverPayments();
        this._alertService.success(this.alertTxt);
        this.clearManualForm();
      },
      (error) => {
        this.isButtonClicked = false;
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  clearManualForm() {
    this.manualpaymentsForm.reset();
  }

  getManualStatus(statuskey) {
    if (statuskey) {
      if (statuskey === 73) {
        return 'Pending';
      } else if (statuskey === 74) {
        return 'Approved';
      } else if (statuskey === 75) {
        return 'Denied';
      } else {
        return null;
      }
    } else {
      return null;
    }

  }

  getManualStatus1(statuskey) {
      console.log('overpayment.approval_status_cd--', statuskey);
    if (statuskey) {
      if (statuskey === '3045' ) {
        return 'Pending';
      } else if (statuskey === '3047') {
        return 'Approved';
      } else if (statuskey === '3281' ) {
        return 'Denied';
      } else if (statuskey === '6550' ) {
        return 'Closed';
     } else if (statuskey === '3046' ) {
        return 'Un-Requested';
    } else {
      return null;
    }
  }
}

  // Validation of Manual payment Start date and end date
  addManualPaymentDays(){
    if (this.manualpaymentsForm.getRawValue().start_dt !== null &&
        this.manualpaymentsForm.getRawValue().end_dt !== null &&
        (this.manualpaymentsForm.getRawValue().end_dt < this.manualpaymentsForm.getRawValue().start_dt)) {
            this._alertService.error('From date should not  be greater than To date');
            this.manualpaymentsForm.controls['end_dt'].setValue('');
    }
  }

  rejectOverPayments() {
    this.isButtonClicked = true;
    const overpayment = this.overpaymentsForm.getRawValue();
    overpayment.eventcode = 'FNSWO';
    overpayment.write_off_approval_status = '3281';
    overpayment.write_off_action_date = moment().format('YYYY-MM-DD');
    overpayment.write_off_request_date = moment().format('YYYY-MM-DD');
    overpayment.payment_id = this.selectedOverPayment.payment_id;
    overpayment.receivable_balance_amount = parseFloat(this.isWriteOff ? overpayment.writeOffAmount : overpayment.receivable_balance_no);
    overpayment.provider_id = this._providerService.providerid;
    delete overpayment.receivable_balance_no;
    this._commonHttpService.patch(this.selectedOverPayment.receivable_detail_id, overpayment, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.update)
        .subscribe(res => {
          this._alertService.success('Write off Denied Successfully');
          this.isButtonClicked = false;
          (<any>$('#overpayment-details')).modal('hide');
          this.getOverPayments();
       });
  }

  getReceivableHistory(receivableDetailid) {
    this.receivableHistoryList =[];
    this._commonHttpService.getPagedArrayList({
      where: {
        receivable_detail_id: receivableDetailid
      },
      page: 1,
      limit: 10,
      method: 'get'
    }, 'tb_receivable_detail/list?filter').subscribe((res: any) => {
      if (res && res.data && res.data.length) {
        this.receivableHistoryList = res.data;

      }
    });
  }

  nofticationDetails() {
    this.receivableColletionStatusList =[];
    this._commonHttpService.getPagedArrayList({
      where: {
        receivable_detail_id: this.overpaymentId
      },
      method: 'get'
    }, 'tb_receivable_detail/receivableColletionStatusList?filter').subscribe((res: any) => {
      if (res && res.data && res.data.length) {
        this.receivableColletionStatusList = res.data;

      }
    });
  }

  uploadFile(file: File | FileError, overpayment): void {
    if (!(file instanceof File)) {
      this._alertService.error('Invalid File!');
      return;
    } else {
      this.restitutionupload = file;
      this.upload(overpayment);
    }
  }

  private upload(overpayment) {
    this._uploadService
      .upload({
        url: AppConfig.baseUrl + '/' + CommonUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
        headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
        filesKey: ['file'],
        files: this.restitutionupload,
        process: true
      })
      .subscribe(
        (response) => {
          if (response.status === 1 && response.data) {
            this.restitutionuploadedFile = response.data;
            this.uploadDocumnet(overpayment, this.restitutionuploadedFile);
          }
        },
        (err) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        },
        () => {
          // console.log('complete');
        }
      );
  }

  private uploadDocumnet(overpayment, uploadfile) {
    var data ={
      "providerid": this._providerService.providerid,
      "receivableid": overpayment.receivable_id,
      "receivabledetailid": overpayment.receivable_detail_id,
      "receivablebalance": overpayment.receivable_balance_no,
      "collectionstatus": overpayment.collection_status,
      "uploadpath":uploadfile.s3bucketpathname,
      "filename":uploadfile.originalfilename
    }
    this._commonHttpService.create(data, 'Accountreceivabledocuments/addupdate').subscribe((res: any) => {
      if (res) {
        this._alertService.success('File Uploaded Succesfully');

      }
    });
  }

  downloadStautsDocumentPDF (key) {
    let notice_type;
    if (key === 'Notice 1 Sent') {
      notice_type = 1;
    } else if (key === 'Notice 2 Sent') {
      notice_type = 2;
    }  else if (key ==='Notice 3 Sent') {
      notice_type = 3;
    } else {
      notice_type = key;
    }
    const modal = {
      "count": -1,
      "where": {
          "documenttemplatekey": [
              "overpaymentnotice"
          ],
          "provider_id": this._providerService.providerid,
          "receivable_detail_id": this.overpaymentId,
          "notice_type": notice_type,
          "format": 'PDF'
      },
      "method": "post"
    };
    this._commonHttpService.download('evaluationdocument/generateintakedocument', modal)
    .subscribe((result) => {
      const blob = new Blob([new Uint8Array(result)]);
      FileSaver.saveAs(blob,  key+'.pdf');
    });
  }

  downloadStautsDocument (key) {
    let notice_type;
    if (key === 'Notice 1 Sent') {
      notice_type = 1;
    } else if (key === 'Notice 2 Sent') {
      notice_type = 2;
    }  else if (key ==='Notice 3 Sent') {
      notice_type = 3;
    } else {
      notice_type = key;
    }
    const modal = {
      "count": -1,
      "where": {
          "documenttemplatekey": [
              "overpaymentnotice"
          ],
          "provider_id": this._providerService.providerid,
          "receivable_detail_id": this.overpaymentId,
          "notice_type": notice_type,
          "format": 'DOCX'
      },
      "method": "post"
    };
    this.http.download('evaluationdocument/generateintakedocument', modal, {}, 'buffer')
    .subscribe((result) => {
      const blob = new Blob([new Uint8Array(JSON.parse(result).data[0].data)]);
      FileSaver.saveAs(blob,  key+'.docx');
    });
  }
}
