import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceArProviderDetailsRoutingModule } from './finance-ar-provider-details-routing.module';
import { FinanceArProviderDetailsComponent } from './finance-ar-provider-details.component';
import { ArProviderComponent } from './ar-provider/ar-provider.component';
import { ArProviderOverpaymentsComponent } from './ar-provider-overpayments/ar-provider-overpayments.component';
import { ArProviderPaymentPlanComponent } from './ar-provider-payment-plan/ar-provider-payment-plan.component';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import { NgxfUploaderService, NgxfUploaderModule } from 'ngxf-uploader';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { FinanceArProviderDetailsService } from './finance-ar-provider-details.service';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { CommonControlsModule } from '../../../../shared/modules/common-controls/common-controls.module';
import { CurrencyInputComponent } from '../../../../shared/modules/common-controls/currency-input/currency-input.component';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { ArProviderDocumentsComponent } from './ar-provider-documents/ar-provider-documents.component';

@NgModule({
  imports: [
    CommonModule,
    FinanceArProviderDetailsRoutingModule,
    CommonControlsModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    A2Edatetimepicker,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    SharedPipesModule,
    SharedDirectivesModule,
    NgxfUploaderModule.forRoot()
  ],
  declarations: [FinanceArProviderDetailsComponent, ArProviderComponent, ArProviderOverpaymentsComponent, ArProviderPaymentPlanComponent, ArProviderDocumentsComponent],
  providers: [NgxfUploaderService, FinanceArProviderDetailsService]
})
export class FinanceArProviderDetailsModule { }
