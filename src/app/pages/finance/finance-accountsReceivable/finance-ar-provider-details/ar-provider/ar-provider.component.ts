import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { FinanceArProviderDetailsService } from '../finance-ar-provider-details.service';
import { FormBuilder } from '@angular/forms';
import { FinanceUrlConfig } from '../../../finance.url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ProviderSearchDetails } from '../../../_entities/finance-entity.module';
import { PaginationRequest, DropdownModel, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'ar-provider',
  templateUrl: './ar-provider.component.html',
  styleUrls: ['./ar-provider.component.scss']
})
export class ArProviderComponent implements OnInit {
  providerSearchDetails: ProviderSearchDetails;
  providerAddress: any;
  street: string;
  city: string;
  county: string;
  state: string;
  zipcode: any;
  paymentHistoryResult = [];
  paymentDetail: any;
  providerCategory: any[];
  placement: boolean;
  vendor: boolean;
  community: boolean;
  paymentDetailList: any[];
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalcount: any;
  paymentDetailView: boolean;
  streetNo: string;

  constructor(
    private _commonHttpService: CommonHttpService,
    private _providerService: FinanceArProviderDetailsService,
    private _formBuilder: FormBuilder,
    private _alertService: AlertService
  ) { }

  ngOnInit() {
    this.getProviderSearchDetails();
  }

  getProviderSearchDetails() {
    this._commonHttpService.endpointUrl = FinanceUrlConfig.EndPoint.accountsReceivable.provider.providerSearch;
    const modal = {
      where: {
        providerid: this._providerService.providerid
      }
    };
    this._commonHttpService.create(modal).subscribe(
      (response) => {
        if (response) {
          const providerSearchDetails = response.length ? response[0] : {};
          this.providerSearchDetails = providerSearchDetails;
          this.streetNo = this.providerSearchDetails.adr_street_tx ? this.providerSearchDetails.adr_street_tx + ', ' : '';
          this.street = this.providerSearchDetails.adr_street_nm ? this.providerSearchDetails.adr_street_nm + ', ' : '';
          this.city = this.providerSearchDetails.adr_city_nm ? this.providerSearchDetails.adr_city_nm + ', ' : '',
          this.county = this.providerSearchDetails.adr_county_nm ? this.providerSearchDetails.adr_county_nm + ', ' : '';
          this.state = this.providerSearchDetails.adr_state_cd ? this.providerSearchDetails.adr_state_cd + ', ' : '';
          this.zipcode = this.providerSearchDetails.adr_zip5_no ? this.providerSearchDetails.adr_zip5_no : '';
          // this.collectionresp = this.providerSearchDetails.collectionresp ? this.providerSearchDetails.collectionresp : '';
          setTimeout(() => {
            // this.providerSearchDetails.address = this.streetNo + this.street + this.city + this.county + this.state + this.zipcode;
          }, 100);
          if (this.providerSearchDetails.provider_category_cd === '1782' || this.providerSearchDetails.provider_category_cd === '1783' ||
          this.providerSearchDetails.provider_category_cd === '3049' ||
          this.providerSearchDetails.provider_category_cd === '3274' ||
          this.providerSearchDetails.provider_category_cd === '3302') {
            this.placement = true;
          } else if (this.providerSearchDetails.provider_category_cd === '3304') {
            this.vendor = true;
          } else if (this.providerSearchDetails.provider_category_cd === '3305') {
            this.community = true;
          }
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);

      });
  }

  paymentHistory() {
     this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
      where: {
        provider_id: +this._providerService.providerid
      },
      method: 'get',
      limit: 5,
      page: this.paginationInfo.pageNumber
    }), FinanceUrlConfig.EndPoint.accountsReceivable.provider.paymentHistory + '?filter'
    ).subscribe((result: any) => {
      if (result) {
        this.paymentHistoryResult = result.data;
        this.totalcount = (this.paymentHistoryResult && this.paymentHistoryResult.length > 0) ? this.paymentHistoryResult[0].totalcount : 0;
      }
    });
  }

  viewPayment(payment) {
    this.paymentDetailView = true;
    this.paymentDetail = payment;
  }

  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.paymentDetailView = false;
    this.paymentHistory();
  }


}
