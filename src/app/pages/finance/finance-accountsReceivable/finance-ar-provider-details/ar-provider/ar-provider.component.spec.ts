import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArProviderComponent } from './ar-provider.component';

describe('ArProviderComponent', () => {
  let component: ArProviderComponent;
  let fixture: ComponentFixture<ArProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
