import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AuthService, AlertService, DataStoreService } from '../../../../../../@core/services';
import { PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../../../finance.url.config';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { ArProviderHistoryService } from '../ar-provider-history.service';
import { FinanceArProviderDetailsService } from '../../finance-ar-provider-details.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'offset-receipt',
  templateUrl: './offset-receipt.component.html',
  styleUrls: ['./offset-receipt.component.scss']
})
export class OffsetReceiptComponent implements OnInit {
  paginationInfo: PaginationInfo = new PaginationInfo();
  receipts: any[] = [];
  offsetReceiptFormFroup: FormGroup;
  paymenttypeList: any[] = [];
  payeeList: any[] = [];
  receipttypeList: any[] = [];
  reasonforreversalList: any[] = [];
  buttontext = 'Add';
  receipt_id = null;
  totalCount: number;
  receivableDetailID: string;
  loggedInUser: string;
  balanceamount: any;
  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _authService: AuthService,
    private _alertService: AlertService, private route: ActivatedRoute, private _storeService: DataStoreService,
    private _history: ArProviderHistoryService, private _providerService: FinanceArProviderDetailsService,
    private _router: Router
  ) {
    this.receivableDetailID = route.snapshot.params['receivabledetailid'];
  }

  ngOnInit() {
    if (!this._history.receipt) {
      this._router.navigate(['../../overpayments'], {relativeTo: this.route});
    }
    this.loggedInUser = this._authService.getCurrentUser().user.username;
    this.initiateForm();
    this.loadDropdowns();
    this.paginationInfo.pageNumber = 1;
    this.loadReceipts();
    this.addReceipt();
    this.prefillReciept();
  }

  prefillReciept() {
    // collected_amount_no: 124
    // offset_amount_no: 124
    // offset_dt: "2018-02-01"
    // offset_id: 10000004
    // receipttype: "Cash"
    // receivable_detail_id: 10000018
    // receivable_id: 10000015
    // referencenumber: null
    // type: "Recovery"

    this.offsetReceiptFormFroup.patchValue({
      payment_method_cd: this._history.receipt.payment_method_cd,
      payment_no_tx: this._history.receipt.referencenumber,
      payment_amount_no: this._history.receipt.collected_amount_no,
    });
    this.changeReceiptType(this._history.receipt.payment_method_cd);
    this.buttontext = 'Update';
  }

  loadDropdowns() {
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '10042'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.paymenttypeList = res;
    });
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '5'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.receipttypeList = res;
    });
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '104'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.payeeList = res;
    });
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '6'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.reasonforreversalList = res;
    });
  }

  initiateForm() {
    this.offsetReceiptFormFroup = this.formBuilder.group({
      receipt_dt: '',
      payment_type_cd: '',
      payment_method_cd: '',
      payment_no_tx: '',
      payment_amount_no: '',
      payee_cd: '',
      reason_tx: '',
      notes_tx: '',
      enteredby: '',
      enteredat: '',
      approveat: '',
      approvedby: ''
    });
  }

  addReceipt() {
    this.resetForm();
    this.offsetReceiptFormFroup.enable();
    this.offsetReceiptFormFroup.get('enteredat').setValue(new Date());
    this.offsetReceiptFormFroup.get('enteredby').setValue(this.loggedInUser);
    this.offsetReceiptFormFroup.get('enteredat').disable();
    this.offsetReceiptFormFroup.get('enteredby').disable();
    this.offsetReceiptFormFroup.get('payee_cd').setValue(this._providerService.provider.county_cd);
    this.offsetReceiptFormFroup.get('payee_cd').disable();
    this.offsetReceiptFormFroup.get('payment_amount_no').setValue(this.balanceamount);
    (<any>$('#receipt-details')).modal('show');
  }

  resetForm() {
    this.offsetReceiptFormFroup.reset();
    this.buttontext = 'Add';
    this.receipt_id = null;
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadReceipts();
  }

  loadReceipts(receiptid = null) {
    this._commonHttpService.getPagedArrayList({
      where: {
        receivabledetailid: this.receivableDetailID
      },
      limit: this.paginationInfo.pageSize,
      page: this.paginationInfo.pageNumber,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.list).subscribe(res => {
      this.receipts = (res && res.data) ? res.data : [];
      this.totalCount = res.data.length ? res.data[0].totalcount : 0;
      this.balanceamount = res['balanceamount'];
    });
  }

  getReceipt(model, editindex) {
    this.resetForm();
    this.receipt_id = model.receipt_id;
    setTimeout(() => {
      this.patchform(model);
      if (editindex === 1) {
        this.buttontext = 'View';
        this.offsetReceiptFormFroup.disable();
      } else {
        this.buttontext = 'Update';
        this.offsetReceiptFormFroup.enable();
      }
      (<any>$('#receipt-details')).modal('show');
    }, 100);
  }

  SaveUpdateReceipts() {
    const model = this.offsetReceiptFormFroup.getRawValue();
    model.receivable_detail_id = this.receivableDetailID;
    model.receivable_id = this._history.receipt.receivable_id;
    model.provider_id = this._providerService.providerid;
    model.offset_dt = new Date();
    if (this.buttontext === 'Update') {
      model.receipt_id = this._history.receipt.offset_id;
    }
    // model.offset_id = this._history.receipt.offset_id;
    this._commonHttpService.create(model, FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.addUpdate).subscribe(
      (result) => {
        (<any>$('#receipt-details')).modal('hide');
        this.resetForm();
        this._alertService.success('Receipt details Saved successfully!');
        this.loadReceipts();
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  deleteReceipt() {
    this._commonHttpService.create({ receipt_id: this.receipt_id }, FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.deleteReceipt).subscribe(
      response => {
        this._alertService.success('Receipt id ' + this.receipt_id + ' deleted successfully');
        this.receipt_id = null;
        (<any>$('#delete-receipt-popup')).modal('hide');
        this.loadReceipts();
      },
      error => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#delete-receipt-popup')).modal('hide');
      }
    );
  }

  changeReceiptType(type: string) {
    if (type !== '16') {
      this.offsetReceiptFormFroup.get('reason_tx').setValue(null);
      this.offsetReceiptFormFroup.get('reason_tx').disable();
    } else {
      this.offsetReceiptFormFroup.get('reason_tx').enable();
    }
  }

  showDeletePop(model) {
    this.receipt_id = model.receipt_id;
    (<any>$('#delete-receipt-popup')).modal('show');
  }

  patchform(model) {
    this.offsetReceiptFormFroup.patchValue(model);
    this.changeReceiptType(model.payment_method_cd);
  }

  getValues(value, type) {
    switch (type) {
      case 'receipttype':
        const receiptdesc = this.receipttypeList.find(item => item.picklist_value_cd === value);
        if (receiptdesc) {
          return receiptdesc.description_tx;
        } else {
          return '';
        }
      default: return '';
    }
  }

}
