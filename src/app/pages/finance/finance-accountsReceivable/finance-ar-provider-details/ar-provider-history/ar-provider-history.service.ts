import { Injectable } from '@angular/core';
import { PaginationInfo } from '../../../../../@core/entities/common.entities';

@Injectable()
export class ArProviderHistoryService {
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalcount: number;
  historyPaginationInfo: PaginationInfo = new PaginationInfo();
  historyTotalcount: number;
  overPaymentHistory = [];
  historyDetails = [];
  selectedPaymentid: number;
  index: number;
  receiptDetails: any;
  historyIndex: number;
  receiptIndex: number;
  receipt: any;
  receivable_detail_id: number;
  receivable_id: number;
  receivable_balance_no: any;
  collection_status_cd: any;
  payment_type_cd: any;
  county_cd: any;
  receivable_status_cd: any;
  approval_status_cd: string;
  manual_sw: string;
  constructor() { }
}
