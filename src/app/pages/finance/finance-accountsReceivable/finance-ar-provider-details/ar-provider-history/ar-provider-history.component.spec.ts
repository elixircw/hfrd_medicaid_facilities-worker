import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArProviderHistoryComponent } from './ar-provider-history.component';

describe('ArProviderHistoryComponent', () => {
  let component: ArProviderHistoryComponent;
  let fixture: ComponentFixture<ArProviderHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArProviderHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArProviderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
