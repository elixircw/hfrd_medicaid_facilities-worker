import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArProviderHistoryRoutingModule } from './ar-provider-history-routing.module';
import { ArProviderHistoryComponent } from './ar-provider-history.component';
import { HistoryOverpaymentsComponent } from './history-overpayments/history-overpayments.component';
import { OffsetReceiptComponent } from './offset-receipt/offset-receipt.component';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FinanceArProviderDetailsService } from '../finance-ar-provider-details.service';
import { ArProviderHistoryService } from './ar-provider-history.service';
import { CommonControlsModule } from '../../../../../shared/modules/common-controls/common-controls.module';
import { SharedPipesModule } from '../../../../../@core/pipes/shared-pipes.module';
@NgModule({
  imports: [
    CommonModule,
    ArProviderHistoryRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    CommonControlsModule,
    SharedPipesModule
  ],
  declarations: [ArProviderHistoryComponent, HistoryOverpaymentsComponent, OffsetReceiptComponent],
  providers: [FinanceArProviderDetailsService, ArProviderHistoryService]
})
export class ArProviderHistoryModule { }
