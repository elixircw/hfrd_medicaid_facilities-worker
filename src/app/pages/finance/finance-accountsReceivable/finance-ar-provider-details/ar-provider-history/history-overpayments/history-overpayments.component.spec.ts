import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryOverpaymentsComponent } from './history-overpayments.component';

describe('HistoryOverpaymentsComponent', () => {
  let component: HistoryOverpaymentsComponent;
  let fixture: ComponentFixture<HistoryOverpaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryOverpaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryOverpaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
