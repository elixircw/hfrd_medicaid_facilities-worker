import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FinanceArProviderDetailsService } from '../../finance-ar-provider-details.service';
import { CommonHttpService, DataStoreService, AlertService, AuthService } from '../../../../../../@core/services';
import { PaginationInfo, PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../../../finance.url.config';
import { ActivatedRoute, Router } from '@angular/router';
import { ArProviderHistoryService } from '../ar-provider-history.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
  selector: 'history-overpayments',
  templateUrl: './history-overpayments.component.html',
  styleUrls: ['./history-overpayments.component.scss']
})
export class HistoryOverpaymentsComponent implements OnInit, AfterViewInit {

  paginationInfo: PaginationInfo = new PaginationInfo();
  offsetReceiptFormFroup: FormGroup;
  receivableDetailID: string;
  loggedInUser: string;
  buttontext = 'Add';
  receipt_id = null;
  balanceamount: any;
  receipts: any[] = [];
  totalCount: number;
  paymenttypeList: any[] = [];
  payeeList: any[] = [];
  receipttypeList: any[] = [];
  reasonforreversalList: any[] = [];
  isEnableReceipt = false;
  payment_amount_error: string;
  id: any;
  bulkReceipt: any[];
  enableBulk: boolean;
  dupResponse: any[];
  selectReceiptBulk: boolean;
  enableSave: boolean;
  bulkSave: boolean;
  isBulkView: boolean;
  overpaymentCheck: boolean;
  isreversalReceipt: boolean;
  receipttypeListOriginal: any[];
  getUsersList: any[];
  selectedPerson: any;
  assignedTo: any;
  isreversal: boolean;

  isAddAllowed: boolean = true;

  currentDate = new Date();
  isButtonClicked = false;
  paymenttypeListOriginal: any[];
  isaddreceipt: boolean;
  collectionStatus775: boolean;
  accepted: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private _providerService: FinanceArProviderDetailsService,
    private _commonHttpService: CommonHttpService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _history: ArProviderHistoryService,
    private _dataStore: DataStoreService,
    private _alertService: AlertService,
    private _authService: AuthService,
  ) {
    this.receivableDetailID = _route.snapshot.params['receivabledetailid'];
  }

  ngOnInit() {
   const data = this._dataStore.getData('PERMISSION');
   if (data && data.length) {
     const res = data.filter((item) => item.resourceid === 'manage_receipt_add');
     this.isAddAllowed = res && res.length ? res[0].isallowed : true;
   }
    // if (!this._history.receipt) {
    //   this._router.navigate(['../../overpayments'], {relativeTo: this._route});
    // }
    this.loggedInUser = this._authService.getCurrentUser().user.username;
    if (!this._history.index && this._history.index !== 0) {
      this.loadOverPaymentHistory();
    } else {
      setTimeout(() => {
        (<any>$('.overpayment-history tr')).removeClass('selected-bg');
        (<any>$(`#overpayment-${this._history.index}`)).addClass('selected-bg');
        (<any>$('.history-details tr')).removeClass('selected-bg');
        (<any>$(`#overpayment-details-${this._history.historyIndex}`)).addClass('selected-bg');
        (<any>$('.receipt-details tr')).removeClass('selected-bg');
        (<any>$(`#receipt-details-${this._history.receiptIndex}`)).addClass('selected-bg');
      }, 100);
    }
    this.initiateForm();
    this.loadDropdowns();
  }

  ngAfterViewInit() {
    // const reversal = this._dataStore.getData('ReversalReceiptFromDashboard');
    // if (reversal && reversal.receivable_detail_id) {
    //   (<any>$('#overpaymentid' + reversal.receivable_detail_id)).click();
    // }
  }

  loadOverPaymentHistory() {
    this.loadOverpaymentList(false).subscribe((res: any) => {
        this._history.overPaymentHistory = res.data;
        const balance = this._history.overPaymentHistory.filter(rec => rec.receivable_balance_no !== '0.00');
        if (balance.length > 0) {
          this.overpaymentCheck = true;
        } else {
          this.overpaymentCheck = false;
        }
        this._history.totalcount = res.count;
        this._history.historyDetails = [];
        this._history.receiptDetails = [];
        this.dupResponse = [...res.data];
        if (this._history.index && this._history.index >= 0) {
          this.getHistoryDetails(this._history.selectedPaymentid, this._history.index, this._history, this.id);
        }
        const reversal = this._dataStore.getData('ReversalReceiptFromDashboard');
        if (reversal && reversal.client_id && this._history.overPaymentHistory && this._history.overPaymentHistory.length > 0) {
          let historydetails = null;
          this._history.overPaymentHistory.forEach((data, index) => {
            this._history.index = index;
            if (data.receivable_detail_id ==  reversal.receivable_detail_id) {
              historydetails = data;
              this._history.selectedPaymentid = data ? data.payment_detail_id : null;
            }
        });
        if (!this._history.selectedPaymentid || !historydetails) {
          this.loadOverpaymentList(true).subscribe(ele => {
            if (ele && ele.data) {
              historydetails = ele.data.find(e => e.receivable_detail_id ==  reversal.receivable_detail_id);
              this._history.index = ele.data.findIndex(e => e.receivable_detail_id ==  reversal.receivable_detail_id);
              if (historydetails) {
                this.getHistoryDetails(reversal.payment_detail_id, this._history.index, historydetails, 'accordion-tables' + this._history.index);
              }
            }
          });
        } else {
          this.getHistoryDetails(reversal.payment_detail_id, this._history.index, historydetails, 'accordion-tables' + this._history.index);
        }
        }
      });
  }

  loadOverpaymentList(isnolimit) {
   return this._commonHttpService.getPagedArrayList({
      method: 'get',
      page: this._history.paginationInfo.pageNumber,
      limit: isnolimit ? 1000 : this._history.paginationInfo.pageSize,
      nolimit: isnolimit,
      where: {
        providerid: this._providerService.providerid
      }
    },
      FinanceUrlConfig.EndPoint.accountsReceivable.history.overpayments.list);
  }

  pageChanged(page) {
    this._history.paginationInfo.pageNumber = page;
    // this.getHistoryDetails(this._history.selectedPaymentid, this._history.index, this._history, this.id);
    this.loadOverPaymentHistory();

  }

  getHistoryDetails(paymentdetailid: number, index: number, history, id) {
    this.id = id;
    this.isEnableReceipt = false;
    this._history.selectedPaymentid = paymentdetailid;
    this._history.index = index;
    this._history.receivable_detail_id = history.receivable_detail_id;
    this._history.receivable_balance_no = history.receivable_balance_no;
    this._history.collection_status_cd = history.collection_status_cd;
    this._history.receivable_id = history.receivable_id;
    this._history.county_cd = history.county_id;
    this._history.manual_sw = history.manual_sw;
    this._history.approval_status_cd = history.approval_status_cd;
    this.accepted = false;
    if (history.collection_status_cd === '775') {
      this.collectionStatus775 = true;
    } else {
      this.collectionStatus775 = false;
    }
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle');
    (<any>$('.overpayment-history tr')).removeClass('selected-bg');
    (<any>$(`#overpayment-${this._history.index}`)).addClass('selected-bg');
    this._commonHttpService.getPagedArrayList({
      method: 'get',
      page: this._history.historyPaginationInfo.pageNumber,
      limit: this._history.historyPaginationInfo.pageSize,
      where: {
        paymentdetailid: paymentdetailid,
        providerid: this._providerService.providerid
      }
    },
      FinanceUrlConfig.EndPoint.accountsReceivable.history.overpayments.subList)
      .subscribe((res: any) => {
        this._history.historyDetails = res.data;
        this._history.historyTotalcount = res.count;
        if (res.data && res.data.length > 0) {
          this._history.receiptDetails = res.data[0].offsetreceiptdetails;
        }
        if (this._history.historyIndex && this._history.historyIndex >= 0) {
          this.getOffsetDetails(this._history.receiptDetails, this._history.historyIndex, '');
        }
        const reversal = this._dataStore.getData('ReversalReceiptFromDashboard');
        if (reversal && reversal.client_id && this._history.overPaymentHistory && this._history.overPaymentHistory.length > 0) {
          let historydetails = null;
          this._history.historyDetails.forEach((data, index) => {
            this._history.index = index;
            if (data.receivable_detail_id ==  reversal.receivable_detail_id) {
              this._history.receiptDetails = data;
            }
        });
        this.getOffsetDetails(this._history.receiptDetails, this._history.historyIndex, '');
        }
      });
  }

  historyPageChanged(page) {
    this._history.historyPaginationInfo.pageNumber = page;
    this.getHistoryDetails(this._history.selectedPaymentid, this._history.index, this._history, this.id);
  }

  getOffsetDetails(receiptdetails: any, index: number, receivable_status_cd: any, collection_status_cd?: any, history?: any) {
    // this.totalCount = receiptdetails.length ? res.data[0].totalcount : 0;
    if (history) {
      this._history.receivable_balance_no = history.receivable_balance_no;
      this._history.collection_status_cd = history.collection_status_cd;
      this._history.approval_status_cd = history.approval_status_cd;
    }
    if (collection_status_cd && collection_status_cd === '775') {
      this.collectionStatus775 = true;
    } else {
      this.collectionStatus775 = false;
    }
    this.enableBulk = false;
    this.selectReceiptBulk = false;
    (<any>$('.trans')).prop('checked', false);
    this._history.receiptDetails = receiptdetails;
    this._history.historyIndex = index;
    this._history.payment_type_cd = receivable_status_cd;
    (<any>$('.history-details tr')).removeClass('selected-bg');
    (<any>$(`#overpayment-details-${this._history.historyIndex}`)).addClass('selected-bg');
    this._history.receiptDetails = receiptdetails ? receiptdetails : [];
    this.isEnableReceipt = true;
    this.isBulkView = false;
    this.loadReceipts();
    const reversal = this._dataStore.getData('ReversalReceiptFromDashboard');
    if (reversal && reversal.receipt_id && this._history.receiptDetails && this._history.receiptDetails.length > 0) {
      const receipt = this._history.receiptDetails.find(data => data.receipt_id === reversal.receipt_id);
      this.getReceipt(receipt, 3);
    }
  }

  getReceipt(model, editindex) {
    this.resetForm();
    this.receipt_id = model.receipt_id;
    this._history.receivable_detail_id = model.receivable_detail_id;
    setTimeout(() => {
      this.patchform(model);
      this.offsetReceiptFormFroup.patchValue({
        enteredat: model.enteredat,
        receipt_dt: model.receipt_dt,
        payment_balance_amount_no: model.collected_amount_no ? model.collected_amount_no : '0.00',
        payment_amount_no: model.payment_amount_no ? _.toNumber(model.payment_amount_no).toFixed(2) : '0.00' ,
        reason_tx: model.reversal_reason_tx ? model.reversal_reason_tx : null
      });
      this.isreversalReceipt = false;
      this.receipttypeList = this.receipttypeListOriginal;
      if (editindex === 1) {
        this.buttontext = 'View';
        this.offsetReceiptFormFroup.disable();
      } else if (editindex === 2) {
        // Reversal of receipt code
        this.isreversalReceipt = true;
        this.changeReceiptType('16');
        this.receipttypeList = this.receipttypeList.filter(data => data.picklist_value_cd == '16');
        this.buttontext = 'Send For Approval';
        this.isreversal = model.isreversal;
        this.offsetReceiptFormFroup.disable();
        this.offsetReceiptFormFroup.get('payment_method_cd').enable();
        this.offsetReceiptFormFroup.get('payment_amount_no').enable();
        this.offsetReceiptFormFroup.get('payment_amount_no').reset();
        this.offsetReceiptFormFroup.get('reason_tx').enable();
        this.offsetReceiptFormFroup.get('notes_tx').enable();
      } else if (editindex === 3) {
        // Reversal of receipt code For approval
        this.isreversalReceipt = true;
        this.changeReceiptType('16');
        this.buttontext = 'Send For Approval';
        this.isreversal = model.isreversal;
        this.offsetReceiptFormFroup.patchValue({
          payment_method_cd: model.payment_method_cd,
          reason_tx: model.reversal_reason_tx,
          payment_amount_no: model.reversal_amount_no ? _.toNumber(model.reversal_amount_no).toFixed(2) : '0.00'
        });
        this.offsetReceiptFormFroup.disable();
        this.offsetReceiptFormFroup.get('notes_tx').enable();
      }  else {
        this.buttontext = 'Update';
        this.offsetReceiptFormFroup.enable();
      }
      (<any>$('#receipt-details')).modal('show');
    }, 100);
  }

  patchform(model) {
    const receiptFormData = {
    receipt_dt: model.offset_dt,
    payment_type_cd: model.payment_type_cd,
    notes_tx: model.notes_tx,
    enteredat: moment(model.enteredat).format('YYYY-MM-DD'),
    enteredby: model.enteredby,
    payment_no_tx: model.referencenumber,
    payment_method_cd: model.payment_method_cd,
    payment_amount_no: _.toNumber(model.collected_amount_no).toFixed(2),
    payee_cd: model.payee_cd };

    this.offsetReceiptFormFroup.patchValue(receiptFormData);
    this.offsetReceiptFormFroup.get('payment_balance_amount_no').setValue(this._history.receivable_balance_no);
    // this.offsetReceiptFormFroup.get('payment_balance_amount_no').setValue(this._history.county_cd);
    this.changeReceiptType(model.payment_method_cd);
  }

  // routeToReceipts(receipt: any, index: number) {
  //   this._history.receiptIndex = index;
  //   (<any>$('.receipt-details tr')).removeClass('selected-bg');
  //   (<any>$(`#receipt-details-${index}`)).addClass('selected-bg');
  //   if (receipt.type === 'Recovery') {
  //     this._history.receipt = receipt;
  //     this._dataStore.setData('HistoryReceipt', receipt);
  //     this._router.navigate([`../receipt/${receipt.receivable_detail_id}`], { relativeTo: this._route });
  //   }
  // }

  // Receipt Details

  initiateForm() {
    this.offsetReceiptFormFroup = this.formBuilder.group({
      receipt_dt: '',
      payment_type_cd: '',
      payment_method_cd: '',
      payment_no_tx: '',
      payment_amount_no: new FormControl(null, Validators.compose([
        Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')
      ])),
      payment_balance_amount_no: '',
      payee_cd: '',
      reason_tx: '',
      notes_tx: '',
      enteredby: '',
      enteredat: '',
      approveat: '',
      approvedby: ''
    });
  }

  SaveUpdateReceipts() {
    this.isButtonClicked = true;
    this.bulkReceipt = [];
    const balance = this.offsetReceiptFormFroup.getRawValue().payment_balance_amount_no;
    const amount_no = this.offsetReceiptFormFroup.getRawValue().payment_amount_no;
    if (!this.collectionStatus775) {
      if (+amount_no > +balance) {
        this._alertService.error('Payment amount should not exceed the balance');
        this.isButtonClicked = false;
        return false;
      }
    }
    //  else if (amount_no <= 0) {
    //   this._alertService.error('Payment amount should be more than $0.00');
    //   this.isButtonClicked = false;
    //   return false;
    // }
    if (this.isreversalReceipt) {
      (<any>$('#receipt-details')).modal('hide');
      this.getRoutinguser();
      (<any>$('#adjustment-approval')).modal('show');
      return false;
    }
    const model = this.offsetReceiptFormFroup.getRawValue();
    model.receivable_detail_id = this._history.receivable_detail_id;
    model.receivable_id =  this._history.receivable_id;
    model.provider_id = this._providerService.providerid;
    model.offset_dt = new Date();
    this.bulkReceipt.push({
      payment_amount_no: amount_no,
      receivable_detail_id: this._history.receivable_detail_id,
      receivable_id: this._history.receivable_id
    });
    model.receivable_details = this.bulkReceipt;
    // if (this.buttontext === 'Update') {
    //   model.receipt_id = (this._history.receipt.offset_id) ? this._history.receipt.offset_id: null;
    // }
    // model.offset_id = this._history.receipt.offset_id;
    this._commonHttpService.create(model, FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.addUpdate).subscribe(
      (result) => {
        (<any>$('#receipt-details')).modal('hide');
        this.resetForm();
        this._alertService.success('Receipt details Saved successfully!');
        this.bulkReceipt = [];
        // this.getHistoryDetails(this._history.selectedPaymentid, this._history.index, this._history);

        this._commonHttpService.getPagedArrayList({
          method: 'get',
          page: this._history.paginationInfo.pageNumber,
          limit: this._history.paginationInfo.pageSize,
          where: {
            providerid: this._providerService.providerid
          }
        },
          FinanceUrlConfig.EndPoint.accountsReceivable.history.overpayments.list)
          .subscribe((res: any) => {
            this._history.overPaymentHistory = res.data;
            this._history.totalcount = res.count;
            const paymentHistory = this._history.overPaymentHistory.find(payment => payment.receivable_detail_id === this._history.receivable_detail_id );
            this._history.receivable_balance_no = paymentHistory.receivable_balance_no;
            const balanceCheck = this._history.overPaymentHistory.filter(rec => rec.receivable_balance_no !== '0.00');
            if (balanceCheck.length > 0) {
              this.overpaymentCheck = true;
            } else {
              this.overpaymentCheck = false;
            }
            setTimeout(() => {
              (<any>$('.overpayment-history tr')).removeClass('selected-bg');
              (<any>$(`#overpayment-${this._history.index}`)).addClass('selected-bg');
            }, 100);


            this._commonHttpService.getPagedArrayList({
              method: 'get',
              page: this._history.historyPaginationInfo.pageNumber,
              limit: this._history.historyPaginationInfo.pageSize,
              where: {
                paymentdetailid: this._history.selectedPaymentid,
                providerid: this._providerService.providerid
              }
            },
              FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.list)
              .subscribe((res: any) => {
                this._history.historyDetails = res.data;
                this._history.historyTotalcount = res.count;
                this._history.receiptDetails = [];
               setTimeout( () => {
                (<any>$('.history-details tr')).removeClass('selected-bg');
                (<any>$(`#overpayment-details-${this._history.historyIndex}`)).addClass('selected-bg');
               }, 200);
               this._history.receiptDetails = this._history.historyDetails;
              //  this._history.receiptDetails = (this._history.historyDetails[this._history.historyIndex]['offsetreceiptdetails']) ?
              //                                 this._history.historyDetails[this._history.historyIndex]['offsetreceiptdetails'] : [];
               this.isEnableReceipt = true;
               this.isBulkView = false;
               this.loadOverPaymentHistory();
              });
          });
      },
      (error) => {
        this.isButtonClicked = false;
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  addReceipt() {
    this.bulkSave = false;
    this.isaddreceipt = true;
    this.resetForm();
    /* if ( this._history.manual_sw && this._history.manual_sw === 'Y') {
      this.paymenttypeList = this.paymenttypeListOriginal.filter(data => data.picklist_value_cd == '3');
    } */
    this.receipttypeList = this.receipttypeList.filter(data => data.picklist_value_cd !== '16');
    this.offsetReceiptFormFroup.enable();
    this.offsetReceiptFormFroup.get('enteredat').setValue(new Date());
    this.offsetReceiptFormFroup.get('enteredby').setValue(this.loggedInUser);
    this.offsetReceiptFormFroup.get('enteredat').disable();
    this.offsetReceiptFormFroup.get('enteredby').disable();
    this.offsetReceiptFormFroup.get('payee_cd').setValue(this._history.county_cd);
    this.offsetReceiptFormFroup.get('payee_cd').enable();
    // this.offsetReceiptFormFroup.get('payment_type_cd').setValue(this._history.payment_type_cd);
    // const payment_type = (this._history.payment_type_cd === 'Foster Care') ? '1' : '2';
    // this.offsetReceiptFormFroup.patchValue({
    //   payment_type_cd: this._history.payment_type_cd
    // });
    // this.offsetReceiptFormFroup.get('payment_type_cd').disable();
    this.offsetReceiptFormFroup.get('payment_balance_amount_no').setValue(this._history.receivable_balance_no);
    this.offsetReceiptFormFroup.get('payment_balance_amount_no').disable();
    // this.offsetReceiptFormFroup.get('payment_amount_no').setValue(this.balanceamount);
    this.offsetReceiptFormFroup.get('payment_amount_no').setValue('0.00');
    (<any>$('#receipt-details')).modal('show');
  }

  resetForm() {
    this.offsetReceiptFormFroup.reset();
    this.buttontext = 'Add';
    this.isaddreceipt = false;
    this.receipt_id = null;
    this.isreversal = false;
    this.isreversalReceipt = false;
    this.bulkSave = false;
    this.receipttypeList = this.receipttypeListOriginal;
    this.isButtonClicked = false;
    this._dataStore.setData('ReversalReceiptFromDashboard', null);
  }

  loadReceipts(receiptid = null) {
    this.isEnableReceipt = true;
    this.isBulkView = false;
    this._commonHttpService.getPagedArrayList({
      where: {
        receivabledetailid: this._history.receivable_detail_id
      },
      limit: this.paginationInfo.pageSize,
      // nolimit: 'true',
      page: this.paginationInfo.pageNumber,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.list).subscribe(res => {
      this.receipts = (res && res.data) ? res.data : [];
      this.totalCount = res.data.length ? res.data[0].totalcount : 0;
      this.balanceamount = res['balanceamount'];
      window.scrollTo(0, document.body.scrollHeight);
    });
  }

  loadDropdowns() {
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '10042'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.paymenttypeList = res;
      this.paymenttypeListOriginal = res;
      /* if (this._history.manual_sw === 'Y') {
        this.paymenttypeList = this.paymenttypeList.filter(response => response.picklist_value_cd === '3');
          this.offsetReceiptFormFroup.get('payment_type_cd').setValue((this.paymenttypeList && this.paymenttypeList.length > 0) ? this.paymenttypeList[0].picklist_value_cd : null);
          this.offsetReceiptFormFroup.get('payment_type_cd').disable();
      } else {
        this.paymenttypeList = this.paymenttypeList.filter(response => response.picklist_value_cd !== '3');
      } */
    });
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '5'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.receipttypeList = res;
      this.receipttypeListOriginal = res;
    });
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '104'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.payeeList = res;
    });
    this._commonHttpService.getArrayList({
      where: {
        picklist_type_id: '6'
      },
      nolimit: 'true',
      method: 'get'
    }, FinanceUrlConfig.EndPoint.general.dropdownURL).subscribe(res => {
      this.reasonforreversalList = res;
    });
  }

  changeReceiptType(type: string) {
    if (type !== '16') {
      this.offsetReceiptFormFroup.get('reason_tx').setValue(null);
      this.offsetReceiptFormFroup.get('reason_tx').disable();
    } else {
      this.offsetReceiptFormFroup.get('reason_tx').enable();
    }
  }

  calculatePerDiem(paymentAmount) {
    const amount = +paymentAmount.target.value;
    const tempPaymentAmount = +amount.toFixed(2);
    this.offsetReceiptFormFroup.patchValue({
      payment_amount_no: tempPaymentAmount ? _.toNumber(tempPaymentAmount).toFixed(2) : 0.00
    });
  }

  bulkCheck() {
    this.resetForm();
    this.bulkSave = true;
    this.offsetReceiptFormFroup.enable();
    this.offsetReceiptFormFroup.get('enteredat').setValue(new Date());
    this.offsetReceiptFormFroup.get('enteredby').setValue(this.loggedInUser);
    this.offsetReceiptFormFroup.get('enteredat').disable();
    this.offsetReceiptFormFroup.get('enteredby').disable();
    this.offsetReceiptFormFroup.get('payee_cd').setValue(this._history.county_cd);
    this.offsetReceiptFormFroup.get('payee_cd').enable();
    this.offsetReceiptFormFroup.patchValue({
      payment_type_cd: this._history.payment_type_cd
    });
    this.offsetReceiptFormFroup.get('payment_balance_amount_no').setValue(this.balanceamount);
    this.offsetReceiptFormFroup.get('payment_balance_amount_no').disable();
   // this.offsetReceiptFormFroup.get('payment_amount_no').setValue(this.balanceamount);
   this.offsetReceiptFormFroup.get('payment_amount_no').setValue('0.00');
   // (<any>$('#receipt-details')).modal('show');
   /* if (this.bulkReceipt && this.bulkReceipt.length > 0 && this.bulkReceipt[0].manual_sw === 'Y') {
      this.paymenttypeList = this.paymenttypeListOriginal.filter(data => data.picklist_value_cd == '3');
   } */
   this.receipttypeList = this.receipttypeList.filter(data => data.picklist_value_cd !== '16');
  }

  viewBulk() {
    this.isBulkView = true;
    (<any>$('.history-details tr')).removeClass('selected-bg');
    (<any>$('.overpayment tr')).removeClass('selected-bg');
    (<any>$('.collapse.in')).collapse('hide');
    this._commonHttpService.getPagedArrayList({
      method: 'get',
      page: this._history.historyPaginationInfo.pageNumber,
      limit: this._history.historyPaginationInfo.pageSize,
      where: {
        paymentdetailid: null,
        providerid: this._providerService.providerid
      }
    },
      FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.list)
      .subscribe((res: any) => {
        this._history.receiptDetails = (res && res.data) ? res.data : [];
        this._history.historyTotalcount = res.data.length ? res.data[0].totalcount : 0;
        // this._history.receiptDetails = [];
        // for (let i = 0; i < this._history.historyDetails.length; i++) {
        //   this._history.receiptDetails.push(this._history.historyDetails[i]['offsetreceiptdetails']);
        // }
       this.isEnableReceipt = true;
       window.scrollTo(0, document.body.scrollHeight);
    });
  }

  getBulk(event) {
    this.isEnableReceipt = false;
    (<any>$('.history-details tr')).removeClass('selected-bg');
    (<any>$('.collapse.in')).collapse('hide');
    this.bulkReceipt = [];
   if (event) {
      this.balanceamount = 0;
      if (this.dupResponse && this.dupResponse.length) {
        for (let i = 0; i < this.dupResponse.length; i++) {
          if (this.dupResponse[i].collection_status_cd !== '779' && this.dupResponse[i].receivable_balance_no !== '0.00'  && this.dupResponse[i].approval_status_cd === '3047') {
            this.dupResponse[i].update_ts = new Date();
            (<any>$('#trans-id-' + i)).prop('checked', true);
            this.selectReceiptBulk = true;
            this.bulkReceipt.push({
              payment_amount_no: this.dupResponse[i].receivable_balance_no,
              receivable_detail_id: this.dupResponse[i].receivable_detail_id,
              receivable_id: this.dupResponse[i].receivable_id,
              manual_sw: this.dupResponse[i].manual_sw
            });
            this.balanceamount = +this.dupResponse[i].receivable_balance_no + this.balanceamount;
          }
        }
      }
      this.enableBulk = !!this.bulkReceipt.length;
     } else {
      this.enableBulk = false;
      this.selectReceiptBulk = false;
        (<any>$('.trans')).prop('checked', false);
        this.balanceamount = 0;

    }
  }

  transChanged(checked, receiptDetails) {
    this.isEnableReceipt = false;
    this.collectionStatus775 = false;
    (<any>$('.history-details tr')).removeClass('selected-bg');
    (<any>$('.collapse.in')).collapse('hide');
    const receipt = JSON.parse(JSON.stringify(receiptDetails));
    const selectedReceipt = (this.bulkReceipt && this.bulkReceipt.length > 0) ? this.bulkReceipt.find(rec => rec.receivable_detail_id === receipt.receivable_detail_id) : null;
    // receipt.validation_status_cd = placement.validation_status_cd ?  1750 : 1749,
      receipt.update_ts = new Date();
    if (checked) {
      if (!selectedReceipt) {
        if (!this.balanceamount) {
          this.balanceamount = 0;
        }
        if (!this.bulkReceipt) {
          this.bulkReceipt = [];
        }
        if (receiptDetails.collection_status_cd !== '779') {
          this.balanceamount = +receiptDetails.receivable_balance_no + this.balanceamount ;
          this.bulkReceipt.push({
            payment_amount_no: receiptDetails.receivable_balance_no,
            receivable_detail_id: receiptDetails.receivable_detail_id,
            receivable_id: receiptDetails.receivable_id,
            manual_sw: receiptDetails.manual_sw ? receiptDetails.manual_sw : null
          });
        }
        if (receiptDetails.collection_status_cd === '775') {
          this.collectionStatus775 = true;
        }
      }
    } else {
      if (selectedReceipt) {
        this.selectReceiptBulk = false;
        if (this.balanceamount && this.balanceamount > +selectedReceipt.payment_amount_no) {
          this.balanceamount = this.balanceamount - selectedReceipt.payment_amount_no;
        } else {
          this.balanceamount = selectedReceipt.payment_amount_no - this.balanceamount;
        }
        this.bulkReceipt = this.bulkReceipt.filter(rec => rec.receivable_detail_id !== receipt.receivable_detail_id);
      }
      this.collectionStatus775 = false;
    }
    this.enableBulk = !!this.bulkReceipt.length;
    const receptlist = this._history.overPaymentHistory;
    if (this.bulkReceipt.length !== 0) {
      const bulkcheckboxselect = (receptlist.length === this.bulkReceipt.length);
      if (bulkcheckboxselect) {
        this.selectReceiptBulk = true;
      } else {
        this.selectReceiptBulk = false;
      }
    } else {
      this.selectReceiptBulk = false;
    }
}

  saveBulkValidation() {
    this.isButtonClicked = true;
    if (!this.collectionStatus775) {
      if (+this.offsetReceiptFormFroup.get('payment_balance_amount_no').value < +this.offsetReceiptFormFroup.get('payment_amount_no').value) {
        this._alertService.error('Receipt Balance should not be exceeded to Balance Amount');
        this.isButtonClicked = false;
        return false;
      }
    }
      // else  if (+this.offsetReceiptFormFroup.get('payment_amount_no').value <= 0) {
      //   this._alertService.error('Payment amount should be more than $0.00');
      //   this.isButtonClicked = false;
      //   return false;
      // }
      this._commonHttpService.endpointUrl = FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.addUpdate;
     // const data = this.offsetReceiptFormFroup.getRawValue();
      const model = this.offsetReceiptFormFroup.getRawValue();
      model.receivable_detail_id = this._history.receivable_detail_id;
      model.receivable_id =  this._history.receivable_id;
      model.provider_id = this._providerService.providerid;
      model.offset_dt = new Date();
      model.receivable_details = this.bulkReceipt;
    this._commonHttpService.create(model).subscribe(
      (response) => {
        (<any>$('#receipt-details')).modal('hide');
        this._alertService.success('Selected Receipts Added Successfully');
        this.loadOverPaymentHistory();
        this.selectReceiptBulk = false;
        this.enableBulk = false;
        this.isEnableReceipt = true;
        this.bulkReceipt = [];
        this.resetForm();
      },
      (error) => {
        this.isButtonClicked = false;
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        this.resetForm();
      }
    );
  }

  validateCheck(checked) {
    if (checked === true) {
      this.enableSave = true;
    } else {
      this.enableSave = false;
    }
  }

  getRoutinguser() {
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'RVRSL' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe((result) => {
        this.getUsersList = result.data;
      });
  }

  selectPerson(row) {
    if (row) {
      this.selectedPerson = row;
      this.assignedTo = row.userid;
    }
  }

  saveReversalReceipt() {
    if (this.isreversalReceipt && this.assignedTo) {
      this._commonHttpService.endpointUrl = FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.ReversalAdd;
      const model = this.offsetReceiptFormFroup.getRawValue();
      model.receivable_detail_id = this._history.receivable_detail_id;
      model.toassingedsecurityuserid = this.assignedTo;
      this._commonHttpService.create({
        receivable_detail_id: model.receivable_detail_id,
        toassingedsecurityuserid: this.assignedTo,
        payment_amount_no: model.payment_amount_no,
        reason_tx: model.reason_tx,
        receipt_id: this.receipt_id
      }).subscribe(
        (response) => {
          (<any>$('#adjustment-approval')).modal('hide');
          this._alertService.success('Reversal Receipts Assigned Successfully');
          // this.loadOverPaymentHistory();
          // this.selectReceiptBulk = false;
          // this.enableBulk = false;
          // this.isEnableReceipt = true;
          // this.bulkReceipt = [];
          this.isreversalReceipt = false;
          this.resetForm();
        },
        (error) => {
          this.isButtonClicked = false;
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    } else {
      this.isButtonClicked = false;
      this._alertService.warn('Please Select User!');
    }
  }

  ApproveReversalReceipt(index) {
    this.isButtonClicked = true;
    this._commonHttpService.endpointUrl = FinanceUrlConfig.EndPoint.accountsReceivable.history.receipt.ReversalApproval;
    this._commonHttpService.create({
      receipt_id: this.receipt_id,
      status: index == 1 ? 'A' : 'R'
    }).subscribe(
      (response) => {
        (<any>$('#receipt-details')).modal('hide');
        this._alertService.success('Reversal Receipts Approved Successfully');
        this.loadOverPaymentHistory();
        this.isreversalReceipt = false;
        this.offsetReceiptFormFroup.reset();
        this._dataStore.setData('ReversalReceiptFromDashboard', null);
        this.resetForm();
      },
      (error) => {
        this.isButtonClicked = false;
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

 checkDec(el) {
    if (el.target.value !== '') {
      return el.target.value = isNaN(el.target.value) ? '' : el.target.value.replace(/[^0-9.]{0,2}/g, '');
    }
    return '';
  }
}
