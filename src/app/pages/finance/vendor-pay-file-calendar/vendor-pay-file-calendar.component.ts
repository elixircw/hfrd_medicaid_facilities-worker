import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import * as moment from 'moment';
import { CommonHttpService, AuthService, AlertService, CommonDropdownsService } from '../../../@core/services';
import { AppUser } from '../../../@core/entities/authDataModel';

@Component({
  selector: 'vendor-pay-file-calendar',
  templateUrl: './vendor-pay-file-calendar.component.html',
  styleUrls: ['./vendor-pay-file-calendar.component.scss']
})
export class VendorPayFileCalendarComponent implements OnInit {
  payFileCalendar= [];
  calendarYear: any[] = [];
  calendarPayFileFormGroup: FormGroup;
  minYear: number;
  calendarYearList: any;
  listCalendar: any[];
  token: AppUser;
  currentDate: Date;
  previousDate: any;
  constructor(
    private _commonService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _alertService: AlertService,
    private __dropdownService: CommonDropdownsService
  ) { }

  ngOnInit() {
    this.getYear();
    this.token = this._authService.getCurrentUser();
    this.minYear = new Date().getFullYear();
    const maxYear = this.minYear + 20;
    for (let i = this.minYear; i <= maxYear; i++) {
      this.calendarYear.push(i);
    }
    this.initCalendarForm();
    this.getCalendarList();
    this.listVendorPayCalendar(this.minYear);
  }

  getYear() {
    const url = 'Fmis_payment_vendor_date/Fmis_payment_vendor_date_year_list';
    const data = {
      method: 'post'
    };
    this._commonService.create(data, url).subscribe( response => {
      console.log('this.calendarYearList...', this.calendarYearList);
      this.calendarYearList = response.map(ele => {
        return {
          value: ele.year_no,
          text: ele.year_no
        };
      });
    });
  }

  saveCalendar()  {
    const url = 'Fmis_payment_vendor_date/Fmis_payment_vendor_date_update';
    const data = this.calendarPayFileFormGroup.getRawValue();
    let yearsObj = [];
    if (data) {
      if (data.calendar && data.calendar.length > 0) {
        // yearsObj = data.calendar.filter()
        yearsObj = data.calendar.map(ele => {
          // if (ele.ischanged) {
            ele.month = ele.month_no;
            ele.securityuserid = this.token.user.securityusersid;
            return ele;
          // }
        });
      }
    }
    this._commonService.create({yearsObj: yearsObj}, url).subscribe(response => {
      if (response) {
        this._alertService.success('Saved Successfully');
        //this.calendarPayFileFormGroup.reset();
        this.listVendorPayCalendar(this.minYear);
      }
    });
  }

  generateNextYear() {
    const nextYear = this.minYear + 1;
    const url = 'Fmis_payment_vendor_date/Fmis_payment_vendor_date_nxtyr_update';
    this._commonService.getArrayList({
      year: nextYear,
      method: 'post'
    }, url).subscribe( res => {
      this._alertService.success('Generated Successfully');
      this.calendarPayFileFormGroup.patchValue({
      year_no: nextYear
      });
      this.listVendorPayCalendar(nextYear);
    });
  }

  listVendorPayCalendar(year) {
    this.calendarPayFileFormGroup.setControl('calendar', this._formBuilder.array([]));
    this._commonService.getArrayList({
      method: 'get',
      nolimit: true,
      where: {
        year_no: year,
        delete_sw: 'N'
      },
      order: 'month_no asc'
    }, 'Fmis_payment_vendor_date?filter'
    ).subscribe(response => {
      if (response) {
        this.listCalendar = response;
        this.payFileCalendar = response;
        this.getCalendarList();
        console.log('this.listCalendar...', this.listCalendar);
      }
    });
  }

  initCalendarForm() {
    this.calendarPayFileFormGroup = this._formBuilder.group({
      year_no: [0]
    });
    this.calendarPayFileFormGroup.setControl('calendar', this._formBuilder.array([]));
    this.calendarPayFileFormGroup.patchValue({
      year_no: this.minYear
    });
  }
  createCalendarForm() {
    return this._formBuilder.group({
      month_no: ['', Validators.required],
      vendor_file_1_dt: ['', Validators.required],
      pay_file_1_dt: ['', Validators.required],
      vendor_file_2_dt: ['', Validators.required],
      pay_file_2_dt: ['', Validators.required],
      comments_tx: ['', Validators.required],
      min_date: [''],
      max_date: [''],
      vendor_1_default_date: [''],
      pay_1_default_date: [''],
      vendor_2_default_date: [''],
      pay_2_default_date: [''],
      updated_on: ['', Validators.required],
      updated_by: ['', Validators.required]
    });
  }

  private buildCalendarForm(x): FormGroup {
    return this._formBuilder.group({
      monthName: x.monthName,
      month_no: x.month_no,
      year: x.year_no,
      vendor_file_1_dt: this.__dropdownService.getValidDate(x.vendor_file_1_dt),
      pay_file_1_dt: this.__dropdownService.getValidDate(x.pay_file_1_dt),
      vendor_file_2_dt: this.__dropdownService.getValidDate(x.vendor_file_2_dt),
      pay_file_2_dt: this.__dropdownService.getValidDate(x.pay_file_2_dt),
      comments_tx: x.comments_tx,
      min_date: this.getMinDate(x.month_no, x.year_no),
      max_date: this.getMaxDate(x.month_no),
      vendor_1_default_date: this.checkDefaultDate('vendor_1', x.vendor_file_1_dt),
      pay_1_default_date: this.checkDefaultDate('pay_1', x.pay_file_1_dt),
      vendor_2_default_date: this.checkDefaultDate('vendor_2', x.vendor_file_2_dt),
      pay_2_default_date: this.checkDefaultDate('pay_2', x.pay_file_2_dt),
      past_month: this.checkIfPastMonth(x.month_no, x.year_no),
      updated_on: x.update_ts,
      updated_by: x.update_user_id,
      ischanged: false
    });
  }

  getCalendarList() {
    this.calendarPayFileFormGroup.setControl('calendar', this._formBuilder.array([]));
    const control = <FormArray>this.calendarPayFileFormGroup.controls['calendar'];
    this.payFileCalendar = this.payFileCalendar.map((x) => {
      x.monthName = this.getMonthName(x.month_no);
      return x;
    });
    this.payFileCalendar.forEach((x) => {
      control.push(this.buildCalendarForm(x));
    });
  }
  check(value) {
    this.previousDate = value;
  }

  appendList(i, date, value) {
     if (value) {
      const currentDate = new Date();
      const selectedDate = new Date(value);
      if (date === '1') {
        if (currentDate >= selectedDate) {
          if (currentDate.getHours() >= 17) {
            this._alertService.error('Vendor and Pay file dates cannot be in the past. If the date is set to "today" it has to be done before 5:00 PM');
            (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
              vendor_file_1_dt: new Date(this.previousDate)
            });
            return false;
          }
        }
        if (selectedDate.getDate() > 7) {
          this._alertService.error('Vendor file date cannot precede more 7 days prior and after to the default date');
          (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
            vendor_file_1_dt: new Date(this.previousDate)
          });
          return false;
        }
      }  else if (date === '4') {
        if (currentDate >= selectedDate) {
          if (currentDate.getHours() >= 17) {
            this._alertService.error('Vendor and Pay file dates cannot be in the past. If the date is set to "today" it has to be done before 5:00 PM');
            (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
              pay_file_1_dt: new Date(this.previousDate)
            });
            return false;
          }
        }
        if (selectedDate.getDate() > 7) {
          this._alertService.error('Pay file date cannot precede more than 4 days prior and after to the default date');
          (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
            pay_file_1_dt: new Date(this.previousDate)
          });
          return false;
        }
      } else if (date === '13') {
        if (currentDate >= selectedDate) {
          if (currentDate.getHours() >= 17) {
            this._alertService.error('Vendor and Pay file dates cannot be in the past. If the date is set to "today" it has to be done before 5:00 PM');
            (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
              vendor_file_2_dt: new Date(this.previousDate)
            });
            return false;
          }
        }
        if (selectedDate.getDate() > 19 || selectedDate.getDate() < 7) {
          this._alertService.error('Vendor file date cannot precede more 7 days prior and after to the default date');
          (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
            vendor_file_2_dt: new Date(this.previousDate)
          });
          return false;
        }
      }  else if (date === '16') {
        if (currentDate >= selectedDate) {
          if (currentDate.getHours() > 17) {
            this._alertService.error('Vendor and Pay file dates cannot be in the past. If the date is set to "today" it has to be done before 5:00 PM');
            (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
              pay_file_2_dt: new Date(this.previousDate)
            });
            return false;
          }
        }
        if (selectedDate.getDate() > 19 || selectedDate.getDate() < 13) {
          this._alertService.error('Pay file date cannot precede more than 4 days prior and after to the default date');
          (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
            pay_file_2_dt: new Date(this.previousDate)
          });
          return false;
        }
      }
     }
    (<FormArray>this.calendarPayFileFormGroup.get('calendar')).controls[i].patchValue({
      ischanged: true
    });
  }

  getMonthName(id) {
   switch (id) {
     case 1: return 'January';
     case 2: return 'February';
     case 3: return 'March';
     case 4: return 'April';
     case 5: return 'May';
     case 6: return 'June';
     case 7: return 'July';
     case 8: return 'August';
     case 9: return 'September';
     case 10: return 'October';
     case 11: return 'November';
     case 12: return 'December';
   }
  }

  checkIfPastMonth(month, year) {
    if (month && year) {
      const currentMonth = moment().get('month') + 1;
      const curryear = new Date().getFullYear();
      if (year > curryear) {
        return false;
      } else if (month >= currentMonth) {
        return false;
      }
      return true;
    }
    return true;
  }

  getMinDate(month, year) {
    let minDate;
    if (month <= new Date().getMonth() && year <= new Date().getFullYear()) {
      minDate = moment(this.calendarPayFileFormGroup.getRawValue().year_no + '/' + new Date().getMonth() + '/' + moment().get('date')).format('YYYY-MM-DD');
    } else {
      if (month === (new Date().getMonth() + 1)) {
        minDate = moment(this.calendarPayFileFormGroup.getRawValue().year_no + '/' + month + '/' + moment().get('date')).format('YYYY-MM-DD');
      } else {
        minDate = moment(this.calendarPayFileFormGroup.getRawValue().year_no + '/' + month + '/01').format('YYYY-MM-DD');
      }
    }
    return this.__dropdownService.getValidDate(minDate);
  }
  getMaxDate(month) {
    const maxDate = moment(this.calendarPayFileFormGroup.getRawValue().year_no + '/' + month + '/01').endOf('month').format('YYYY-MM-DD');
    return maxDate;
  }

  checkDefaultDate(type, dateDefault) {
    const date = this.__dropdownService.getValidDate(dateDefault);
    if (type === 'vendor_1') {
      return (moment(date).format('D') == '1') ? false : true;
    } else if (type === 'pay_1') {
      return (moment(date).format('D') == '4') ? false : true;
    } else if (type === 'vendor_2') {
      return (moment(date).format('D') == '13') ? false : true;
    } else if (type === 'pay_2') {
      return (moment(date).format('D') == '16') ? false : true;
    } else {
      return true;
    }
  }

  saveVendorPayFile() {
    console.log(this.calendarPayFileFormGroup.value);
  }

}
