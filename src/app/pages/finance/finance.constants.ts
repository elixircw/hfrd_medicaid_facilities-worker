export enum FinanceStoreConstants {
    AccountsReceivableSearchParams = 'AccountsReceivableSearchParams',
    SelectedProvider = 'SelectedProvider'
}

export enum FinanceAdjustment {
  AncillaryAdjustmentSearchParams = 'AncillaryAdjustmentSearchParams',
  Selectedpayment = 'Selectedpayment',
  SelectedpaymentFromDashboard = 'SelectedpaymentFromDashboard'
}

export enum FinanceFundingSource {
  FundingSourceSearchParams = 'FundingSourceSearchParams',
  SelectedFundingSource = 'SelectedFundingSource'
}

export enum ClientPayment {
  ClientPaymentSearchParams = 'ClientPaymentSearchParams',
  SelectedClientSource = 'SelectedClientSource'
}

export enum  ProviderChecklist {
  ProviderChecklistSearchParams = 'ProviderChecklistSearchParams',
  SelectedProvider = 'SelectedProvider'
}

export enum PurchaseAuthorizationParams {
  DirectorParms = 'DirectorParms',
  FundingParms = 'FundingParms',
  PaymentParms = 'PaymentParms'
}

export enum AccountDisbursement {
  FundingParms = 'FundingParms',
  PaymentParms = 'PaymentParms'
}

export enum CommingledAccount {
  ErrorCorrection = 'ErrorCorrection'
}

export enum ChildAccount {
  ErrorCorrection = 'ErrorCorrection'
}

export enum UserCounty {
  CountyData = 'CountyData'
}
