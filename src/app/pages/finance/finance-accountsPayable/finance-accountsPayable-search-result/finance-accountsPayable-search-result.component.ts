import { Component, OnInit, Input, AfterViewInit, ViewChild } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { FinanceAccountsPayableSearchEntry, PayableSearchParams } from '../../_entities/finance-entity.module';
import { ActivatedRoute, Router } from '@angular/router';
import { clone } from 'lodash';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { Sort } from '@angular/material/sort';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';
import { FinanceUrlConfig } from '../../finance.url.config';
import { Observable } from 'rxjs/Observable';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { ProviderInfoPopupComponent } from '../../provider-info-popup/provider-info-popup.component';
import { FinanceService } from '../../finance.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-finance-accountsPayable-search-result',
  templateUrl: './finance-accountsPayable-search-result.component.html',
  styleUrls: ['./finance-accountsPayable-search-result.component.scss']
})
export class FinanceAccountsPayableSearchResultComponent implements OnInit {
  [x: string]: any;
  providerList = [];
  accountpayableList = [];
  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  selectedRecord: any;
  paymentDetails = [];
  searchParams: PayableSearchParams;
  amount1: number;
  amount2: number;
  pageInfo: PaginationInfo = new PaginationInfo();
  id: any;
  paymentid: any;
  totalPage: number;
  provider_id: any;
  selectPaymentid: any;
  selectedPaymentid: any;
  // sortedData: any;
  provider_index: any;

  constructor(private _commonService: CommonHttpService,
    private route: ActivatedRoute,

    private _dataStoreService: DataStoreService,
    private _route: ActivatedRoute,
    private _router: Router, private _providerpopup: FinanceService) {
      // this.sortedData = this.providerList.slice();
     }

  ngOnInit() {
    this.searchParams = null;
    this.paginationInfo.sortColumn = 'providerid';
    this.paginationInfo.sortBy = 'desc';
    this.paginationInfo.pageNumber = 1;
    this.pageInfo.pageNumber = 1;
    this._dataStoreService.currentStore.subscribe(store => {
      const searchParams = store['AccountPayableSerchFilter'];
      if (searchParams && (JSON.stringify(searchParams) !== JSON.stringify(this.searchParams))) {
        if (searchParams === 'clear') {
          this.providerList = [];
        } else {
          this.searchParams = searchParams;
          ObjectUtils.removeEmptyProperties(this.searchParams);
          this.loadList(true);
        }
      }
    });
  }

  loadList(isInit = false) {
    this.accountpayableList = [];
     this.searchParams.providerid = this.searchParams.providerid ? this.searchParams.providerid : null;
    this.searchParams.paymentid = this.searchParams.paymentid ? this.searchParams.paymentid : null;
    this.paymentid = this.searchParams.paymentid;
    this.searchParams.clientid = this.searchParams.clientid ? this.searchParams.clientid : null;
    this.searchParams.taxid = this.searchParams.taxid ? this.searchParams.taxid : null;
    this.searchParams['sortorder'] = this.paginationInfo.sortBy ? this.paginationInfo.sortBy : null;
    this.searchParams['sortcolumn'] = this.paginationInfo.sortColumn ? this.paginationInfo.sortColumn : null;
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: isInit ? this.paginationInfo.pageNumber = 1 : this.paginationInfo.pageNumber,
        method: 'post',
        where: this.searchParams
        }), 'tb_provider/getproviderMaintainancesearch'
    ).subscribe((result: any) => {
      if (result) {
        this.providerList = result;
        // this.sortedData = this.providerList.slice();
        this.totalcount = (this.providerList && this.providerList.length > 0) ? this.providerList[0].totalcount : 0;
      }
    });
  }

  pageChanged(page: any) {
    this.paginationInfo.pageNumber = page;
    this.loadList();
  }

  toggleTable(id, providerid, index, mode) {
    const searchParams = clone(this.searchParams);
    this.id = id;
    this.provider_index = index;
    this.provider_id = providerid;
    if (!this.paymentid) {
      searchParams.paymentid = null;
    }
    searchParams.providerid = providerid;
    if (mode === 'Add') {
      this.pageInfo.pageNumber = 1;
      (<any>$('.collapse.in')).collapse('hide');
      (<any>$('#' + id)).collapse('toggle');
    }

    (<any>$('.provider-details tr')).removeClass('selected-bg');
    (<any>$(`#provider-details-${index}`)).addClass('selected-bg');

    this._commonService.getPagedArrayList(new PaginationRequest({
      where: searchParams,
      // sortcolumn: this.paginationInfo.sortColumn,
      // sortorder: this.paginationInfo.sortBy,
      // limit : this.pageInfo.pageSize,
      //   page: this.pageInfo.pageNumber,
// Reverting back to original by Narayana
      limit : this.pageInfo.pageSize,
        page: this.pageInfo.pageNumber,
      method: 'post'

    }), FinanceUrlConfig.EndPoint.accountsPayable.getPaymentList).subscribe(res => {
      this.accountpayableList = res.data;
      this.totalPage = (this.accountpayableList && this.accountpayableList.length > 0) ? this.accountpayableList[0].totalcount : 0;
    });
  }



  pageNumberChanged(page) {
    this.pageInfo.pageNumber = page;
    this.toggleTable(this.id, this.provider_id, this.provider_index, 'Edit');
  }

  getNetAmount(gross_amount_no, offset_amount_no) {
    this.amount1 = (gross_amount_no) ? parseFloat(gross_amount_no) : 0;
    this.amount2 = (offset_amount_no) ? parseFloat(offset_amount_no) : 0;
    return (this.amount1 - this.amount2).toFixed(2);
    // (item.gross_amount_no || item.offset_amount_no) ? (parseFloat(item.gross_amount_no-(item.offset_amount_no ? item.offset_amount_no: 0 )).toFixed(2)) : '0.00'
  }

  selectAccountsPayable(paymentid, paymentStatus) {
    const searchParams = clone(this.searchParams);
    this.selectedPaymentid = paymentid;
    searchParams.paymentid = paymentid;
    searchParams.payment_status_cd = paymentStatus;
    this._dataStoreService.setData('paymentid', searchParams);
   // this._dataStoreService.setData('AccountPayableSerchFilter', this.searchParams);
    // this._router.navigate(['../finance-accountsPayable-search-view'], { relativeTo: this._route });
  }

  onMaintenancePayments($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.providerList;
}

onAccountsHeaders($event: ColumnSortedEvent) {
  this.paginationInfo.sortBy = $event.sortDirection;
  this.paginationInfo.sortColumn = $event.sortColumn;
  this.accountpayableList;
}

showProviderPopup(providerID) {
  this._dataStoreService.setData('paymentid', null);
  (<any>$('#modal-view-finace-accountsPayable')).modal('hide');
  this._dataStoreService.setData('ProviderInfoID', providerID);
  (<any>$('#provider-info-popup')).modal('show');
  this._providerpopup.getProviderDetails(providerID);
  this._providerpopup.page = 'Maintenance';
}


}
  // sortData(sort: Sort) {
  //   const data = this.providerList.slice();
  //   if (!sort.active || sort.direction === '') {
  //     this.sortedData = data;
  //     return;
  //   }

  //   this.sortedData = data.sort((a, b) => {
  //     const isAsc = sort.direction === 'asc';
  //     switch (sort.active) {
  //       case 'providerid': return compare(a.provider_id, b.provider_id, isAsc);
  //       case 'providername': return compare(a.provider_nm, b.provider_nm, isAsc);
  //       case 'providertype': return compare(a.providertype, b.providertype, isAsc);
  //       case 'mailcodetx': return compare(a.mail_code_tx, b.mail_code_tx, isAsc);
  //       case 'taxidno': return compare(a.tax_id_no, b.tax_id_no, isAsc);


  //       default: return 0;
  //     }
  //   });
  // }



// function compare(a: number | string, b: number | string, isAsc: boolean) {
//   return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
// }
