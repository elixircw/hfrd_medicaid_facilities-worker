import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { FinanceAccountsPayableSearchEntry } from '../_entities/finance-entity.module';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'app-finance-accountsPayable',
    templateUrl: './finance-accountsPayable.component.html',
    styleUrls: ['./finance-accountsPayable.component.scss']
})
export class FinanceAccountsPayableComponent implements OnInit {
    searchData$: Subject<Observable<FinanceAccountsPayableSearchEntry[]>>;
    totalSearchRecord$: Subject<Observable<number>>;
    pageNumberResult$: Subject<number>;
    searchResultData$: Observable<FinanceAccountsPayableSearchEntry[]>;
    totalResulRecords$: Observable<number>;
    showTable = false;

    constructor() {
        this.searchData$ = new Subject();
        this.totalSearchRecord$ = new Subject();
        this.pageNumberResult$ = new Subject();
    }

    ngOnInit() {
        this.searchData$.subscribe(data => {
            this.searchResultData$ = data;
            this.showTable = true;
        });
        this.totalSearchRecord$.subscribe(data => {
            this.totalResulRecords$ = data;
        });

    }
}
