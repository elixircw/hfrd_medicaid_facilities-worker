import { Component, OnInit, AfterContentInit } from '@angular/core';
import { CommonHttpService, AlertService, AuthService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { FinanceUrlConfig } from '../../../finance.url.config';
import { PaginationRequest, DropdownModel, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import * as moment from 'moment';
import { PurchaseAuthorization } from '../../../_entities/finance-entity.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PurchaseAuthorizationParams } from '../../../finance.constants';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
const FISCAL_CODE_DISABLE = ['2126', '2127', '7126', '7127'];
@Component({
  selector: 'director-approval',
  templateUrl: './director-approval.component.html',
  styleUrls: ['./director-approval.component.scss']
})
export class DirectorApprovalComponent implements OnInit, AfterContentInit {
  stateandZip: string;
  payableApproval: any[];
  authorization_id: any;
  fiscal_category_cd: any;
  cost_no: any;
  provider_id: any;
  intakeserviceid: any;
  startDate: any;
  endDate: any;
  payment_method_cd: any;
  payableApprovalHistory: any[];
  assignedTo: any;
  selectedPerson: any;
  getUsersList: any[];
  originalUserList: any[];
  isapproved: any;
  child_account_no: any;
  client_id: any;
  dob: Date;
  gender: string;
  justification_tx: string;
  description_tx: string;
  service_log_id: number;
  service_start_dt: number;
  provider_nm: string;
  service_nm: number;
  service_end_dt: number;
  client_name: string;
  client_account_id: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalcount: number;
  totalPage: number;
  pageInfo: PaginationInfo = new PaginationInfo();
  approval: any;
  purchaseAuthData: PurchaseAuthorization;
  purchaseServiceForm: FormGroup;
  remarks: any;
  fiscalCode$: Observable<DropdownModel[]>;
  reason_tx: any;
  childAccountReject = true;
  purchaseRequestForm: FormGroup;
  reDirect = true;
  paymentType: any[];
  paymentType$: Observable<DropdownModel[]>;
  paymentMethod$: Observable<DropdownModel[]>;
  enablePurchase: boolean;
  timestamp: Date;
  initSearchParams: any = null;
  statusDropDownList = [
    {
      'text': 'All',
      'value': 'All'
    },
    {
      'text': 'Approved',
      'value': 'A'
    },
    {
    'text': 'Pending',
    'value': 'P'
    },
    {
      'text': 'Denied',
      'value': 'R'
    },
  ];
  directorStatus: any;
  status: any;
  reasonDesc: boolean;
  otherCase: boolean;
  roleBased: boolean;
  eventCode: string;
  roletypekey: any;
  role: string;
  userRole: string;
  rolename: string;
  fiscalCodes: any[];
  statuskey: any;
  age: any;
  isapprovedFlag: boolean;
  streetNo: any;
  street: string;
  city: string;
  county: string;
  state: string;
  zipCode: any;
  paymentAddress: string;
  disableupdateButton: boolean;
  clientEligibility: any;
  clientEligibilityStatus: any;
  service_id: any;
  childAccountsList: any[];
  streetDir: string;
  suit: string;
  adrUnitType: string;
  adrUnitNo: string;
  isButtonClicked: boolean;
  caseID: any;
  activeModule: any;
  roletype: string;


  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _route: ActivatedRoute,
    private _authService: AuthService,
    private _router: Router,
    private _datastoreService: DataStoreService,
    private _formBuilder: FormBuilder,
    private _sessionStorage: SessionStorageService) { }

  ngOnInit() {
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    if (this.activeModule === 'Finance') {
      this.roletype = 'FNSFW';
    } else if (this.activeModule === 'Finance Approval') {
      this.roletype = 'FNSFS';
    }
    this.directorStatus = 'P';
    this.timestamp = new Date();
    this.getPayableApproval();
    this.buildForm();
    // this.getFiscalCategory();
    this.getPaymentMethod();
    this.getPaymentType();
    this._datastoreService.currentStore.subscribe(store => {
      this.initSearchParams = store[PurchaseAuthorizationParams.DirectorParms];
    });
  }

  ngAfterContentInit(): void {
      if (this.initSearchParams) {
        this.onAuthID(this.initSearchParams);
        this._datastoreService.setData(PurchaseAuthorizationParams.DirectorParms, null);
      }
  }

  buildForm() {
    this.purchaseRequestForm = this._formBuilder.group({
      payment_start_dt: [null],
      payment_end_dt: [null],
      payment_method_cd: [null, Validators.required],
      type_1099_cd: [null, Validators.required],
      amount_no: [null],
      store_receipt_id: [null],
      actualAmount: [null],
      payment_id: [null],
      report_1099_sw: ['']
    });
    this.purchaseServiceForm = this._formBuilder.group(
      {
          fiscalCode: ['', Validators.required],
          voucherRequested: ['', Validators.required],
          costnottoexceed: '',
          justificationCode: '',
          startDt: [null, Validators.required],
          endDt: [null, Validators.required],
          fundingstatus: '',
          authorization_id: '',
          paymentstatus: '',
          final_amount_no: '',
          actualAmount: '',
          client_account_no: '',
          reason_tx: ''
      });

  }

  statusDirectorDropDown(status) {
    this.directorStatus = status;
    this.getPayableApproval();
  }

  getPayableApproval() {
    if (this.directorStatus === 'All') {
      this.status = null;
    } else {
      this.status = this.directorStatus;
    }
    this.payableApproval = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {approveltype: 'direcort', status: this.status, roletypekey: 'FNSDF'},
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter'
    ).subscribe((result: any) => {
      this.payableApproval = result;
      this.totalcount = (this.payableApproval && this.payableApproval.length > 0) ? this.payableApproval[0].totalcount : 0;
    });

  }
  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getPayableApproval();
  }

  getClientEligibility(client_id, caseNumber, programkey) {
    this._commonService.getArrayList({
        where: {
            client_id: client_id,
            case_id: caseNumber ? caseNumber : null,
        },
        method: 'get'
    }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientEligibility).subscribe((response) => {
        this.clientEligibility = response['data'];
        this.clientEligibilityStatus = this.clientEligibility[0].eligibility_status_cd;
      this.getFiscalCategoryCode(programkey);
    });
}

  onAuthID(authid) {
    this.caseID = authid.case_id ? authid.case_id : null;
    this.service_id = authid.service_id;
    this.fiscal_category_cd = authid.fiscal_category_cd;
    this.getClientEligibility(authid.client_id, authid.case_id, authid.programkey);
    (<any>$('#approval-history')).modal('show');
    if (authid && (authid.fiscal_category_cd === '7502' ||
    authid.fiscal_category_cd === '7503')) {
      this.childAccountReject = true;
    } else {
      this.childAccountReject = false;
    }
    this.otherCase = authid.bmanualrouting;
    this.authorization_id = authid.authorization_id;
    if (authid.routingstatustypeid === 62) {
      this.statuskey = false;
    } else {
      this.statuskey = true;
    }
    this.cost_no = authid.cost_no ? authid.cost_no : '0.00';
    this.provider_id = authid.provider_id;
    this.intakeserviceid = authid.intakeserviceid;
    this.startDate = authid.payment_start_dt;
    this.endDate = authid.payment_end_dt;
    this.payment_method_cd = authid.payment_method_cd;
    this.isapproved = authid.isapproved;
    this.client_account_id = authid.client_account_id;
    this.client_id = authid.client_id;
    this.client_name = authid.client_name;
    this.dob = authid.dob;
    this.gender = authid.gender;
    this.age = authid.age;
    this.service_nm = authid.service_nm;
    this.justification_tx = authid.justification_tx;
    this.description_tx = authid.description_tx ? authid.description_tx : '';
    this.service_log_id = authid.service_log_id;
    this.service_start_dt = authid.service_start_dt;
    this.provider_nm = authid.provider_nm;
    this.service_end_dt = authid.service_end_dt;
    this.streetNo = authid.adr_street_no ? authid.adr_street_no + ' ' : '';
    this.streetDir = authid.adr_pre_dir_cd ? authid.adr_pre_dir_cd + ' ' : '';
    this.street = authid.adr_street_nm ? authid.adr_street_nm + '  ' : '';
    this.suit = authid.adr_street_suffix_cd ? authid.adr_street_suffix_cd + ' ' : '';
    this.adrUnitType = authid.adr_unit_type_cd ? authid.adr_unit_type_cd + ' ' : '';
    this.adrUnitNo = authid.adr_unit_no_tx ? authid.adr_unit_no_tx + ' ' : '';
    this.city = authid.adr_city_nm ? authid.adr_city_nm + ',' : '';
    // this.county = authid.county_cd ? authid.county_cd + ', ' : '';
    this.state = authid.adr_state_cd ? authid.adr_state_cd + ' ' : '';
    this.zipCode =  authid.adr_zip5_no ? authid.adr_zip5_no  : '';
    this.paymentAddress = this.streetNo + this.streetDir + this.street + this.suit + this.adrUnitType + this.adrUnitNo + this.city;
    this.stateandZip = this.state + this.zipCode;
    this.remarks = authid.remarks;
    this.getApproveHistory(authid.authorization_id);
    this.viewPurchaseAuthorization(authid);
    setTimeout(() => {
      this.purchaseRequestForm.patchValue(authid);
      if (!authid.type_1099_cd) {
          this.purchaseRequestForm.patchValue({type_1099_cd : '3158'});
      }
      if (authid.report_1099_sw === 'Y') {
        this.purchaseRequestForm.patchValue({ report_1099_sw : 'Y'});
      } else {
        this.purchaseRequestForm.patchValue({ report_1099_sw : false});
      }
      if (authid.remarks === 'Denied') {
        this.reasonDesc = true;
      } else {
        this.reasonDesc = false;
      }
      this.purchaseRequestForm.disable();
      this.initSearchParams = null;
    }, 10);
  }

  getApproveHistory(authid) {
    this.payableApprovalHistory = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {authorization_id: authid},
        limit : this.pageInfo.pageSize,
        page: this.pageInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApprovalHistory + '?filter'
    ).subscribe((result: any) => {
      this.payableApprovalHistory = result;
      this.totalPage = (this.payableApprovalHistory && this.payableApprovalHistory.length > 0) ? this.payableApprovalHistory[0].totalPage : 0;
    });
  }

  viewPurchaseAuthorization(purchaseDetail) {
    // this.authorizationID = purchaseDetail.authorization_id;
  //  this.viewPurchaseDetails = purchaseDetail;

    this._commonService.getArrayList(
      new PaginationRequest({
          page: 1,
          limit: 20,
          where: { service_log_id: purchaseDetail.service_log_id },
          method: 'get'
      }),
      FinanceUrlConfig.EndPoint.accountsPayable.approval.purchaseAuthorizationGet + '?filter'
      ).subscribe(result => {
        if (result['data'] && result['data'].length > 0) {
            const paDetails = result['data'];
            this.purchaseAuthData = paDetails.length > 0 ? paDetails[0] : {};
            // this.actualAmount = this.purchaseAuthData.final_amount_no;
            const model = {
              fiscalCode: this.purchaseAuthData.fiscal_category_cd,
              voucherRequested: this.purchaseAuthData.voucher_requested,
              costnottoexceed: this.purchaseAuthData.cost_no ? this.purchaseAuthData.cost_no : '0.00',
              justificationCode: this.purchaseAuthData.justification_text,
              fundingstatus: this.purchaseAuthData.fundingstatus,
              authorization_id: this.purchaseAuthData.authorization_id ? this.purchaseAuthData.authorization_id : '',
              startDt: this.purchaseAuthData.startdt,
              endDt: this.purchaseAuthData.enddt,
              actualAmount: this.purchaseAuthData.final_amount_no,
              paymentstatus: this.purchaseAuthData.paymentstatus,
              final_amount_no: this.purchaseAuthData.final_amount_no ? this.purchaseAuthData.final_amount_no : '0.00',
              client_account_no: this.purchaseAuthData.client_account_no ? this.purchaseAuthData.client_account_no : '',
              reason_tx: this.purchaseAuthData.reason_tx ? this.purchaseAuthData.reason_tx : ''
          };
          if (this.purchaseAuthData.paymentstatus === 'Approved') {
            this.enablePurchase = true;
          } else {
            this.enablePurchase = false;
          }
          this.purchaseServiceForm.setValue(model, { emitEvent: true, onlySelf: false });
          this.purchaseServiceForm.disable();
          if (this.remarks === 'Pending') {
            this.purchaseServiceForm.get('justificationCode').disable();
            if (!FISCAL_CODE_DISABLE.includes(this.purchaseAuthData.fiscal_category_cd)) {
              this.purchaseServiceForm.get('fiscalCode').disable();
              this.disableupdateButton = false;
            } else {
              this.disableupdateButton = true;
            }
          }
      }
      });
    }

    getFiscalCategoryCode(vendorprogramid) {
      this._commonService.getArrayList({
              where: {agencyprogramareaid: vendorprogramid},
              method: 'get'
            }, FinanceUrlConfig.EndPoint.accountsPayable.approval.fiscalCodes + '?filter',
          ).subscribe( result => {
            this.fiscalCodes = result;
            // THIS RULE APPLICABLE FOR IV-E - Should not block service-log, Filter needs to be revisited
            // this.fiscalCodes = this.fiscalCodes.filter(item => item.eligibility_cd === this.clientEligibilityStatus);
            if (this.service_id) {
                if (this.clientEligibilityStatus === '2913') {
                   // this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                } else {
                    this.fiscalCodes = this.fiscalCodes.filter(item => this.startsWith(item));
                }
                if (this.service_id === 11333) {
                   // if (this.clientEligibilityStatus === '3951') {
                    if (this.clientEligibilityStatus === '2913') {
                        this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                    } else {
                        this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7126');
                    }
               } else if (this.service_id === 11334) {
                    // if (this.clientEligibilityStatus === '3951') {
                        if (this.clientEligibilityStatus === '2913') {
                        this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2127');
                    } else {
                        this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7127');
                    }
               } else {
                   if (this.clientEligibilityStatus === '3951') {
                    this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                }
               }
            }


/*
              this.fiscalCodes = result;
              this.fiscalCodes = this.fiscalCodes.filter(item => item.eligibility_cd === this.clientEligibilityStatus);
              if (this.service_id) {
                  if (this.service_id === 11333) {
                      if (this.clientEligibilityStatus === '3951') {
                          this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                      } else {
                          this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7126');
                      }
                 } else if (this.service_id === 11334) {
                      if (this.clientEligibilityStatus === '3951') {
                          this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2127');
                      } else {
                          this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7127');
                      }
                 }
              }
 */              this.purchaseServiceForm.patchValue( {fiscalCode: this.fiscal_category_cd} );
          });
  }

    updateAuthoriz() {
      let age = 0;
    const purchaseDate = moment(this.purchaseServiceForm.get('endDt').value);
    if (this.dob && moment(new Date(this.dob), 'MM/DD/YYYY', true).isValid()) {
        const pDob = moment(new Date(this.dob), 'MM/DD/YYYY').toDate();
        age = purchaseDate.diff(pDob, 'years');


      if (this.purchaseServiceForm.get('fiscalCode').value === '5113') {
          if (age < 14 || age > 18) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5114' ||
                 this.purchaseServiceForm.get('fiscalCode').value === '5115')  {
            if (age < 18 || age > 21) {
              this._alertService.error('Invalid Fiscal Category Code due to Client age');
              this.purchaseServiceForm.patchValue({
                fiscalCode: this.purchaseAuthData.fiscal_category_cd
              });
              return false;
            }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5116') {
        if (age < 18) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5117') {
        if (age < 18 || age > 21) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5118') {
          if (age < 16 || age > 21) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '7110' ||
                 this.purchaseServiceForm.get('fiscalCode').value === '2110') {
        if (+this.cost_no > 2000) {
          this._alertService.error('Selected Fiscal Category will be not allowed if the amount is more than $2000.00');
          this.purchaseServiceForm.patchValue({
            fiscalCode: this.purchaseAuthData.fiscal_category_cd
          });
          return false;
        }
      }
    }
      this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.history.updatePurchaseAuthorizationGet;
      const modal = {
        authorization_id: this.authorization_id,
        fiscal_category_cd: this.purchaseServiceForm.get('fiscalCode').value,
        justification_tx: this.purchaseServiceForm.get('justificationCode').value,
        'costno' : this.cost_no ? this.cost_no : '0.00',
        'client_id': this.client_id
      };
      this._commonService.create(modal).subscribe(
        (response) => {
          if (response && response.isexceed) {
            if (response.isexceed === 3) {
              this.fiscal_category_cd = this.purchaseServiceForm.get('fiscalCode').value;
              this._alertService.success('Fiscal category has been Updated');
              this.getPayableApproval();
            } else if (response.isexceed === 2) {
              this.purchaseServiceForm.patchValue({
                fiscalCode: this.fiscal_category_cd
              });
              this._alertService.error(`Insufficient Client Account balance, please raise a request at lower cost`);
            } else if (response.isexceed === 1) {
              this.purchaseServiceForm.patchValue({
                fiscalCode: this.fiscal_category_cd
              });
              this._alertService.error(`Account is not available, kindly please add account for the selected client`);
            }
          }
        },
        (error) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    }

  pageNumberChanged(page) {
    this.pageInfo.pageNumber = page;
    this.getApproveHistory(this.authorization_id);
  }

  private assignUser() {
    this.isButtonClicked = true;
    console.log('this.authorization_id...', this.authorization_id);
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.ServiceLogApproval;
        const modal = {
           'case_id' : this.caseID,
           'fiscalcategorycd' : this.fiscal_category_cd,
           'costno' : this.cost_no ? this.cost_no : '0.00',
           'authorization_id' : this.authorization_id,
           'provider_id' : this.provider_id,
           'intakeserviceid' : this.intakeserviceid,
           'assignedtoid' : this.assignedTo ? this.assignedTo : null,
            'eventcode' : this.eventCode ? this.eventCode : 'PCAUTH',
           'status' : 44,
           'startDt': this.startDate,
           'endDt': this.endDate,
           'payment_method_cd': this.payment_method_cd,
           'client_id': this.client_id,
           'client_account_id': this.client_account_id,
           bmanualrouting : this.otherCase,
           'roletypekey': this.roletypekey ? this.roletypekey : null
        };
    this._commonService.create(modal).subscribe(
        (response) => {
            this._alertService.success('Approval sent successfully!');
            (<any>$('#payment-approval')).modal('hide');
            this.getPayableApproval();
            this.isButtonClicked = false;
        },
        (error) => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            this.isButtonClicked = false;
        }
    );

  }

  selectPerson(row) {
    if (row) {
      this.selectedPerson = row;
      this.assignedTo = row.userid;
    }
  }

  getRoutingUser() {
    (<any>$('#approval-history')).modal('hide');
    this._commonService
        .getPagedArrayList(
            new PaginationRequest({
                where: { appevent: 'PCAUTH' },
                method: 'post'
            }),
            'Intakedastagings/getroutingusers'
        )
        .subscribe((result) => {
            this.getUsersList = result.data;
            this.originalUserList = this.getUsersList;
            this.listUser('TOBEASSIGNED');
        });
 }

 enableUserRole(type) {
  if (type === 'user') {
     this.roleBased = false;
     this.eventCode = 'PCAUTH';
     this.roletypekey = null;
  } else {
     this.roleBased = true;
     this.assignedTo = null;
     this.eventCode = 'PCAUTHR';
     this.roletypekey = 'FNS' + this.role;
  }
}

listUser(assigned: string) {
this.userRole = 'role';
this.selectedPerson = '';
this.getUsersList = [];
this.getUsersList = this.originalUserList;
 if (assigned === 'TOBEASSIGNED') {
   this.getUsersList = this.getUsersList.filter((res) => {
         if (res.rolecode === 'FS') {
             this.role = res.rolecode;
             this.rolename = 'Fiscal Supervisor';
             return res;
         }
 });
 } else {
     this.getUsersList = this.getUsersList.filter((res) => {
             if (res.rolecode === 'FW') {
                 this.role = res.rolecode;
                 this.rolename = 'Fiscal Worker';
                 return res;
             }
     });
 }
 this.enableUserRole('role');
}
  confirmReject() {
    (<any>$('#approval-history')).modal('hide');
    (<any>$('#payment-approval')).modal('hide');
    (<any>$('#reject-approval')).modal('show');
    this.reason_tx = '';
  }

  rejectApproval() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = 'tb_account_transaction/deleteClientTransactionbyAuthId' + '/' + this.authorization_id;
    const modal = {
      client_account_id: this.client_account_id,
      fiscal_category_cd: this.fiscal_category_cd,
      'case_id': this.caseID,
      cost_no: this.cost_no
    };
    this._commonService.create(modal).subscribe(
    (response) => {
      this._alertService.success('Approval has been Denied!');
      (<any>$('#reject-approval')).modal('hide');
      this.getPayableApproval();
      this.isButtonClicked = false;
    },
    (error) => {
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      this.isButtonClicked = false;
    });

  }

  rejectOtherApproval() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.approval.PurchaseAuthorizationReject + this.authorization_id;
    const modal = {
      reason_tx: this.reason_tx
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        this._alertService.success('Purchase Authorization has been Denied!');
        (<any>$('#reject-approval')).modal('hide');
        this.getPayableApproval();
        this.isButtonClicked = false;
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        this.isButtonClicked = false;
      }
    );
  }

  getPaymentType() {
    this.paymentType = [];
    this.paymentType$ = this._commonService.getArrayList({
      where: {picklist_type_id : '308'},
      nolimit: true,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsPayable.PaymentPickList + '?filter'
    ) .map((result) => {
      return result.map(
          (res) =>
              new DropdownModel({
                  text: res.value_tx,
                  value: res.picklist_value_cd
              })
      );
  });
  this.paymentType$.subscribe((response) => {
    for (let i = 0; i < response.length; i++) {
      if (response[i].value !== '3158') {
        this.paymentType.push(response[i]);
      }
    }
  });
  }

  getPaymentMethod() {
    this.paymentMethod$ = this._commonService.getArrayList({
      where: {picklist_type_id : '1'},
      nolimit: true,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsPayable.PaymentPickList + '?filter'
    ) .map((result) => {
      return result.map(
          (res) =>
              new DropdownModel({
                  text: res.description_tx,
                  value: res.picklist_value_cd
              })
      );
  });
  }

  documentGenerate() {
    if (this.remarks === 'Approved') {
      this.isapprovedFlag = true;
    } else {
        this.isapprovedFlag = false;
    }
    this._commonService.endpointUrl = 'evaluationdocument/generateintakedocument';
    const modal = {
      count: -1,
      where: {
          documenttemplatekey: ['purchaseauthorization'],
          authorizationid: this.authorization_id,
          isapproved: this.isapprovedFlag
      },
      method: 'post'
    };
    this._commonService.download('evaluationdocument/generateintakedocument', modal)
    .subscribe(res => {
      const blob = new Blob([new Uint8Array(res)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      const timestamp = moment(this.timestamp).format('MM/DD/YYYY HH:mm:ss');
      link.download = 'Purchase-authorization-Form-' + this.authorization_id + '.' + timestamp + '.pdf';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  }

  viewChildAccount(clientid, accno) {
    (<any>$('#approval-history')).modal('hide');
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: {client_id: clientid },
      nolimit: true,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.childAccounts.getchildaccountslistUrl + '?filter').subscribe(result => {
      this.childAccountsList = result.data;
      if (accno && this.childAccountsList && this.childAccountsList.length > 0) {
        this.childAccountsList = this.childAccountsList.filter(data => data.account_no_tx == accno);
      }
      (<any>$('#view-child-account')).modal('show');
    });
  }

  showPrevious(id) {
    (<any>$(`#${id}`)).modal('show');
  }

  startsWith = function (item) {
    if (item.fiscalcateforycd.indexOf('21') !== 0) {
        return item;
    }
  }

}
