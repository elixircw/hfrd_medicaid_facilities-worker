import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, AuthService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { FinanceUrlConfig } from '../../../finance.url.config';
import { PaginationRequest, DropdownModel, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
import { ActivatedRoute } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { PurchaseAuthorization } from '../../../_entities/finance-entity.module';
import { date } from 'ng4-validators/src/app/date/validator';
import { PurchaseAuthorizationParams } from '../../../finance.constants';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';

const FISCAL_CODE_DISABLE = ['2126', '2127', '7126', '7127'];
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'financial-approval',
  templateUrl: './financial-approval.component.html',
  styleUrls: ['./financial-approval.component.scss']
})
export class FinancialApprovalComponent implements OnInit {
  stateandZip: string;
  payableApproval = [];
  payableApprovalHistory: any[];
  authId: any;
  authorization_id: any;
  fiscal_category_cd: any;
  cost_no: any;
  getUsersList: any[];
  provider_id: any;
  intakeserviceid: any;
  originalUserList: any[];
  selectedPerson: string;
  mergeUsersList: any[];
  userProfile: AppUser;
  assignedTo: any;
  startDate: Date;
  endDate: Date;
  payment_method_cd: any;
  calculate: boolean;
  groupCategoryCodeData: any[];
  fiscodeArr = [];
  isapproved: any;
  client_id: any;
  street: string;
  city: string;
  county: string;
  state: string;
  zipCode: string;
  paymentAddress: string;
  client_account_id: number;
  purchaseAuthData: PurchaseAuthorization;
  purchaseServiceForm: FormGroup;
  fiscalCode$: Observable<DropdownModel[]>;
  actualAmount: number;
  viewPurchaseDetails = PurchaseAuthorization;
  authorizationID: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalcount: number;
  totalPage: number;
  pageInfo: PaginationInfo = new PaginationInfo();
  remarks: any;
  reason_tx: string;
  childAccountReject: boolean;
  isRejected: boolean;
  timestamp: Date;
  statusDropDownList = [
    {
      'text': 'All',
      'value': 'All'
    },
    {
      'text': 'Approved',
      'value': 'A'
    },
    {
    'text': 'Pending',
    'value': 'P'
    },
    {
      'text': 'Denied',
      'value': 'R'
    },
  ];
  fundingStatus: any;
  status: any;
  reasonDesc: boolean;
  otherCase: any;
  calculateDisable = true;
  roleBased: boolean;
  eventCode: string;
  roletypekey: any;
  role: string;
  rolename: string;
  userRole: string;
  fiscalCodes: any[];
  statuskey: any;
  persondob: Date;
  isapprovedFlag: any;
  fundingPayee: any;
  fundingPayeeName: any;
  streetNo: string;
  disableupdateButton: boolean;
  clientEligibility: any;
  clientEligibilityStatus: any;
  service_id: any;
  childAccountsList: any[];
  streetDir: string;
  suit: string;
  adrUnitType: string;
  adrUnitNo: string;
  isButtonClicked: boolean;
  caseID: any;
  payableApprovalID: any;
  updateAuth: boolean;
  activeModule: any;
  roletype: any;

  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _route: ActivatedRoute,
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _datastoreService: DataStoreService,
    private _sessionStorage: SessionStorageService
    ) { }

  ngOnInit() {
    this.fundingStatus = 'P';
    this.fundingPayeeName = null;
    this.paginationInfo.sortBy = null;
    this.paginationInfo.sortColumn = null;
    this.timestamp = new Date();
    this.paginationInfo.pageNumber = 1;
    this.userProfile = this._authService.getCurrentUser();
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    if (this.activeModule === 'Finance') {
      this.roletype = 'FNSFW';
    } else if (this.activeModule === 'Finance Approval') {
      this.roletype = 'FNSFS';
    }
    this.getPayableApproval();
    this.buildForm();
   // this.getFiscalCategory();
    this._datastoreService.currentStore.subscribe(store => {
      const searchParams = store[PurchaseAuthorizationParams.FundingParms];
      if (searchParams) {
        this.onAuthID(searchParams);
      }
    });
  }

  buildForm() {
    this.purchaseServiceForm = this._formBuilder.group(
      {
          fiscalCode: ['', Validators.required],
          voucherRequested: ['', Validators.required],
          costnottoexceed: '',
          justificationCode: '',
          startDt: [null, Validators.required],
          endDt: [null, Validators.required],
          fundingstatus: '',
          authorization_id: '',
          paymentstatus: '',
          final_amount_no: '',
          actualAmount: '',
          client_account_no: '',
          reason_tx: ''
      });

  }

  statusFundingDropDown(status) {
    this.fundingStatus = status;
    this.getPayableApproval();
  }

  viewPurchaseAuthorization(purchaseDetail) {
    this.authorizationID = purchaseDetail.authorization_id;
    this.viewPurchaseDetails = purchaseDetail;
    this.persondob = purchaseDetail.dob;

    this._commonService.getArrayList(
      new PaginationRequest({
          page: 1,
          limit: 20,
          where: { service_log_id: purchaseDetail.service_log_id },
          method: 'get'
      }),
      FinanceUrlConfig.EndPoint.accountsPayable.approval.purchaseAuthorizationGet + '?filter'
      ).subscribe(result => {
        if (result['data'] && result['data'].length > 0) {
            const paDetails = result['data'];
            this.purchaseAuthData = paDetails.length > 0 ? paDetails[0] : {};
            // this.actualAmount = this.purchaseAuthData.final_amount_no;
            const model = {
              fiscalCode: this.purchaseAuthData.fiscal_category_cd,
              voucherRequested: this.purchaseAuthData.voucher_requested,
              costnottoexceed: this.purchaseAuthData.cost_no ? this.purchaseAuthData.cost_no : '0.00',
              justificationCode: this.purchaseAuthData.justification_text,
              fundingstatus: this.purchaseAuthData.fundingstatus,
              authorization_id: this.purchaseAuthData.authorization_id ? this.purchaseAuthData.authorization_id : '',
              startDt: this.purchaseAuthData.startdt,
              endDt: this.purchaseAuthData.enddt,
              actualAmount: this.purchaseAuthData.final_amount_no,
              paymentstatus: this.purchaseAuthData.paymentstatus,
              final_amount_no: this.purchaseAuthData.final_amount_no ? this.purchaseAuthData.final_amount_no : '0.00',
              client_account_no: this.purchaseAuthData.client_account_no ? this.purchaseAuthData.client_account_no : '',
              reason_tx: this.purchaseAuthData.reason_tx ? this.purchaseAuthData.reason_tx : ''
          };
          this.purchaseServiceForm.setValue(model, { emitEvent: true, onlySelf: false });
          this.purchaseServiceForm.disable();
          if (this.remarks === 'Pending') {
            this.purchaseServiceForm.get('justificationCode').enable();
            if (!FISCAL_CODE_DISABLE.includes(this.purchaseAuthData.fiscal_category_cd)) {
              this.purchaseServiceForm.get('fiscalCode').enable();
              this.disableupdateButton = false;
            } else {
              this.disableupdateButton = true;
            }
            this.reasonDesc = false;
          } else if (this.remarks === 'Denied') {
            this.reasonDesc = true;
          } else {
            this.reasonDesc = false;
          }
      }
      });
      }

      getFiscalCategoryCode(vendorprogramid) {
        this._commonService.getArrayList({
                where: {agencyprogramareaid: vendorprogramid},
                method: 'get'
              }, FinanceUrlConfig.EndPoint.accountsPayable.approval.fiscalCodes + '?filter',
            ).subscribe( result => {

                this.fiscalCodes = result;
                if (!this.disableupdateButton) {
                  this.fiscalCodes = this.fiscalCodes.filter(item => !(FISCAL_CODE_DISABLE.includes(item.fiscalcateforycd)));
                }
                // THIS RULE APPLICABLE FOR IV-E - Should not block service-log, Filter needs to be revisited
                // this.fiscalCodes = this.fiscalCodes.filter(item => item.eligibility_cd === this.clientEligibilityStatus);
                if (this.service_id) {
                    if (this.clientEligibilityStatus === '2913') {
                       // this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                    } else {
                        this.fiscalCodes = this.fiscalCodes.filter(item => this.startsWith(item));
                    }
                    if (this.service_id === 11333) {
                       // if (this.clientEligibilityStatus === '3951') {
                        if (this.clientEligibilityStatus === '2913') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7126');
                        }
                   } else if (this.service_id === 11334) {
                        // if (this.clientEligibilityStatus === '3951') {
                            if (this.clientEligibilityStatus === '2913') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2127');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7127');
                        }
                   } else {
                       if (this.clientEligibilityStatus === '3951') {
                        this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                    }
                   }
                }

              /*   this.fiscalCodes = result;
                this.fiscalCodes = this.fiscalCodes.filter(item => item.eligibility_cd === this.clientEligibilityStatus);
                if (this.service_id) {
                    if (this.service_id === 11333) {
                        if (this.clientEligibilityStatus === '3951') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7126');
                        }
                   } else if (this.service_id === 11334) {
                        if (this.clientEligibilityStatus === '3951') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2127');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7127');
                        }
                   }
                } */
                 this.purchaseServiceForm.patchValue( {fiscalCode: this.fiscal_category_cd} );
            });
    }

    getClientEligibility(client_id, caseNumber, programkey) {
      this._commonService.getArrayList({
          where: {
              client_id: client_id,
              case_id: caseNumber ? caseNumber : null,
          },
          method: 'get'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientEligibility).subscribe((response) => {
          this.clientEligibility = response['data'];
          this.clientEligibilityStatus = this.clientEligibility[0].eligibility_status_cd;
        this.getFiscalCategoryCode(programkey);
      });
  }

  documentGenerate() {
    if (this.remarks === 'Approved') {
      this.isapprovedFlag = true;
    } else {
        this.isapprovedFlag = false;
    }
    const modal = {
      count: -1,
      where: {
          documenttemplatekey: ['purchaseauthorization'],
          authorizationid: this.authorizationID,
          isapproved: this.isapprovedFlag
      },
      method: 'post'
    };
    this._commonService.download('evaluationdocument/generateintakedocument', modal)
    .subscribe(res => {
      const blob = new Blob([new Uint8Array(res)]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      // link.download = 'Purchase_authorization-' + this.authorizationID + + '-' + moment(this.timestamp).format('MM/DD/YYYY HH:mm') + '.pdf';
      const timestamp = moment(this.timestamp).format('MM/DD/YYYY HH:mm:ss');
      link.download = 'Purchase-authorization-Form-' + this.authorizationID + '.' + timestamp + '.pdf';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  }

  onSearchPayee() {
    if (this.fundingPayee) {
      this.fundingPayeeName = this.fundingPayee.replace(/'/g, `''`);
    } else {
      this.fundingPayeeName = null;
    }
    this.getPayableApproval();
  }
  onSortedFunding($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.getPayableApproval();
  }
  getPayableApproval() {
    if (this.fundingStatus === 'All') {
      this.status = null;
    } else {
      this.status = this.fundingStatus;
    }
    this.payableApproval = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {approveltype: 'funding', status: this.status,
        sortcolumn: this.paginationInfo.sortColumn,
        sortorder: this.paginationInfo.sortBy,
        payee_nm: this.fundingPayeeName ? this.fundingPayeeName : null,
        roletypekey: this.roletype
        },
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter'
    ).subscribe((result: any) => {
      this.payableApproval = result;
      if (this.payableApproval && this.updateAuth) {
        this.payableApprovalID = this.payableApproval.find(item => item.authorization_id === this.authorizationID);
        this.onAuthID(this.payableApprovalID);
      }
      this.totalcount = (this.payableApproval && this.payableApproval.length > 0) ? this.payableApproval[0].totalcount : 0;
    });

  }

  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getPayableApproval();
  }
  onAuthID(authid) {
    this.updateAuth = false;
    this.caseID = authid.case_id ? authid.case_id : null;
    this.service_id = authid.service_id;
    this.fiscal_category_cd = authid.fiscal_category_cd;
    this.getClientEligibility(authid.client_id, authid.case_id, authid.programkey);
    (<any>$('#approval-history')).modal('show');
    if (authid && (authid.fiscal_category_cd === '7502' ||
    authid.fiscal_category_cd === '7503')) {
      this.childAccountReject = true;
    } else {
      this.childAccountReject = false;
    }
    if (authid && (authid.remarks === 'Denied')) {
      this.isRejected = true;
      this.purchaseServiceForm.disable();
    } else {
      this.isRejected = false;
    }
    this.otherCase = authid.bmanualrouting;
    this.authorization_id = authid.authorization_id;
    this.fiscal_category_cd = authid.fiscal_category_cd;
    this.cost_no = authid.cost_no ? authid.cost_no : '0.00';
    this.provider_id = authid.provider_id;
    this.intakeserviceid = authid.intakeserviceid;
    this.startDate = authid.payment_start_dt;
    this.endDate = authid.payment_end_dt;
    this.payment_method_cd = authid.payment_method_cd;
    this.isapproved = authid.isapproved;
    if (authid.routingstatustypeid === 62) {
      this.statuskey = false;
    } else {
      this.statuskey = true;
    }
    this.remarks = authid.remarks;
    this.client_id = authid.client_id;
    this.client_account_id = authid.client_account_id;
    this.streetNo = authid.adr_street_no ? authid.adr_street_no + ' ' : '';
    this.streetDir = authid.adr_pre_dir_cd ? authid.adr_pre_dir_cd + ' ' : '';
    this.street = authid.adr_street_nm ? authid.adr_street_nm + '  ' : '';
    this.suit = authid.adr_street_suffix_cd ? authid.adr_street_suffix_cd + ' ' : '';
    this.adrUnitType = authid.adr_unit_type_cd ? authid.adr_unit_type_cd + ' ' : '';
    this.adrUnitNo = authid.adr_unit_no_tx ? authid.adr_unit_no_tx + ' ' : '';
    this.city = authid.adr_city_nm ? authid.adr_city_nm + ',' : '';
    // this.county = authid.county_cd ? authid.county_cd + ', ' : '';
    this.state = authid.adr_state_cd ? authid.adr_state_cd + ' ' : '';
    this.zipCode =  authid.adr_zip5_no ? authid.adr_zip5_no  : '';
    this.paymentAddress = this.streetNo + this.streetDir + this.street + this.suit + this.adrUnitType + this.adrUnitNo + this.city;
    this.stateandZip = this.state + this.zipCode;
    this.getApproveHistory(authid.authorization_id);
    this.viewPurchaseAuthorization(authid);
    setTimeout(() => {
      this._datastoreService.clearStore();
    }, 100);
    // if (authid.remarks === 'Pending') {
    //   this.purchaseServiceForm.get('justificationCode').enable();
    // }
  }

  getApproveHistory(authid) {
    this.payableApprovalHistory = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {authorization_id: authid},
        limit : this.pageInfo.pageSize,
        page: this.pageInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApprovalHistory + '?filter'
    ).subscribe((result: any) => {
      this.payableApprovalHistory = result;
      this.totalPage = (this.payableApprovalHistory && this.payableApprovalHistory.length > 0) ? this.payableApprovalHistory[0].totalPage : 0;
    });
  }

  pageNumberChanged(page) {
    this.pageInfo.pageNumber = page;
    this.getApproveHistory(this.authorization_id);
  }

  private assignUser() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.ServiceLogApproval;
        const modal = {
           'case_id' : this.caseID,
           'fiscalcategorycd' : this.fiscal_category_cd,
           'costno' : this.cost_no ? this.cost_no : '0.00',
           'authorization_id' : this.authorization_id,
           'provider_id' : this.provider_id,
           'intakeserviceid' : this.intakeserviceid,
           'assignedtoid' : this.assignedTo ? this.assignedTo : null,
           'eventcode' : this.eventCode ? this.eventCode : 'PCAUTH',
           'status' : 41,
           'startDt': this.startDate,
           'endDt': this.endDate,
           'payment_method_cd': this.payment_method_cd,
           'client_id': this.client_id,
           'client_account_id': this.client_account_id,
            bmanualrouting: this.otherCase,
           'roletypekey': this.roletypekey ? this.roletypekey : null
        };
    this._commonService.create(modal).subscribe(
        (response) => {
            this._alertService.success('Approval sent successfully!');
            (<any>$('#payment-approval')).modal('hide');
            this.getPayableApproval();
            this.isButtonClicked = false;
        },
        (error) => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            this.isButtonClicked = false;
        }
    );

  }

  selectPerson(row) {
    if (row) {
      this.selectedPerson = row;
      this.assignedTo = row.userid;
    }
  }

  getRoutingUser(mode) {
    if (mode === 'category') {
      this._commonService
          .getPagedArrayList(
              new PaginationRequest({
                  where: { appevent: 'PCAUTH' },
                  method: 'post'
              }),
              'Intakedastagings/getroutingusers'
          )
          .subscribe((result) => {
              this.getUsersList = result.data;
              this.originalUserList = this.getUsersList;
              this.listUser('TOBEASSIGNED');
          });
    } else {
      if (this.purchaseServiceForm.valid) {
        (<any>$('#approval-history')).modal('hide');
        (<any>$('#category-calculation')).modal('hide');
        this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'PCAUTH' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                this.originalUserList = this.getUsersList;
                this.listUser('TOBEASSIGNED');
            });
      } else {
        this._alertService.error('Please fill the mandatory fields');
      }
    }
 }

 enableUserRole(type) {
  if (type === 'user') {
     this.roleBased = false;
     this.eventCode = 'PCAUTH';
     this.roletypekey = null;
  } else {
     this.roleBased = true;
     this.assignedTo = null;
     this.eventCode = 'PCAUTHR';
     this.roletypekey = 'FNS' + this.role;
  }
}

 listUser(assigned: string) {
  this.userRole = 'role';
  this.selectedPerson = '';
  this.getUsersList = [];
  this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter((res) => {
        if (res.rolecode === 'FS' && res.userid !== this.userProfile.user.securityusersid) {
          this.role = res.rolecode;
          this.rolename = 'Fiscal Supervisor';
            return res;
        }
    });
    }
    this.enableUserRole('role');
  }

  enableCategoryCalculate(value) {
    if (value === 'category') {
      this.calculate = true;
    } else {
      this.calculate = false;
    }
  }
  changeFCC(value) {
    if (!(value === '7502' || value === '7503')) {
      this.purchaseServiceForm.patchValue({
        client_account_no: null
      });
    }
    (<any>$('#approval-history')).modal('hide');
    (<any>$('#update-approval')).modal('show');
  }
  revertUpdate() {
    this.purchaseServiceForm.patchValue({
      fiscalCode: this.fiscal_category_cd
    });
  }

  updateAuthoriz(mode?: any) {

    let age = 0;
    const purchaseDate = moment(this.purchaseServiceForm.get('endDt').value);
    if (this.persondob && moment(new Date(this.persondob), 'MM/DD/YYYY', true).isValid()) {
        const pDob = moment(new Date(this.persondob), 'MM/DD/YYYY').toDate();
        age = purchaseDate.diff(pDob, 'years');


      if (this.purchaseServiceForm.get('fiscalCode').value === '5113') {
          if (age < 14 || age > 18) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5114' ||
                 this.purchaseServiceForm.get('fiscalCode').value === '5115')  {
            if (age < 18 || age > 21) {
              this._alertService.error('Invalid Fiscal Category Code due to Client age');
              this.purchaseServiceForm.patchValue({
                fiscalCode: this.purchaseAuthData.fiscal_category_cd
              });
              return false;
            }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5116') {
        if (age < 18) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5117') {
        if (age < 18 || age > 21) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5118') {
          if (age < 16 || age > 21) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '7110' ||
                 this.purchaseServiceForm.get('fiscalCode').value === '2110') {
        if (+this.cost_no > 2000) {
          this._alertService.error('Selected Fiscal Category will be not allowed if the amount is more than $2000.00');
          this.purchaseServiceForm.patchValue({
            fiscalCode: this.purchaseAuthData.fiscal_category_cd
          });
          return false;
        }
      }
    }
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.history.updatePurchaseAuthorizationGet;
    const modal = {
      roletypekey: this.roletype,
      authorization_id: this.authorization_id,
      fiscal_category_cd: this.purchaseServiceForm.get('fiscalCode').value,
      justification_tx: this.purchaseServiceForm.get('justificationCode').value,
      'costno' : this.cost_no ? this.cost_no : '0.00',
      'client_id': this.client_id
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        if (response && response.isexceed) {
          if (mode === 'update' && !this.updateAuth) {
          //  (<any>$('#approval-history')).modal('show');
            (<any>$('#update-approval')).modal('hide');
          }
          if (response.isexceed === 3) {
            if (this.purchaseServiceForm.get('fiscalCode').value === '7502' ||
            this.purchaseServiceForm.get('fiscalCode').value === '7503') {
              this.updateAuth = true;
            } else {
              (<any>$('#approval-history')).modal('show');
              this.updateAuth = false;
            }
            this.fiscal_category_cd = this.purchaseServiceForm.get('fiscalCode').value;
            this._alertService.success('Fiscal category has been Updated');
            this.getPayableApproval();
          } else if (response.isexceed === 2) {
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.fiscal_category_cd
            });
            this._alertService.error(`Insufficient Client Account balance, please raise a request at lower cost`);
          } else if (response.isexceed === 1) {
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.fiscal_category_cd
            });
            this._alertService.error(`Account is not available, kindly please add account for the selected client`);
          }
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  groupFundingApproval() {
     this._commonService.getArrayList({
       method: 'get',
       where: {
        roletypekey: this.roletype
       }
     },
     FinanceUrlConfig.EndPoint.accountsPayable.approval.groupCategory + '?filter'
     ).subscribe(result => {
       this.groupCategoryCodeData = result;
       if (this.groupCategoryCodeData && this.groupCategoryCodeData.length > 0) {
        this.calculateDisable = false;
       } else {
        this.calculateDisable = true;
       }
     });
  }
  categoryCheck(event, fiscalCode) {
    if (event) {
      this.fiscodeArr.push(fiscalCode);
      this.fiscodeArr.toString();
    } else {
      this.fiscodeArr = this.fiscodeArr.filter(data => data !== fiscalCode);
      this.fiscodeArr.toString();
    }
  }
  categoryApproval() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.approval.categoryApproval;
    const modal = {
      'fiscalcategorycd' : '{' + this.fiscodeArr + '}',
      'costno' : this.cost_no ? this.cost_no : '0.00',
      'authorization_id' : this.authorization_id ? this.authorization_id : null,
      'provider_id' : this.provider_id ? this.provider_id : null,
      'intakeserviceid' : this.intakeserviceid ? this.intakeserviceid : null,
      'assignedtoid' : this.assignedTo ? this.assignedTo : null,
      'eventcode' : this.eventCode ? this.eventCode : 'PCAUTH',
      'status' : 41,
      'startDt': this.startDate ? this.startDate : null,
      'endDt': this.endDate ? this.endDate : null,
      'payment_method_cd': this.payment_method_cd ? this.payment_method_cd : null,
      'client_id': this.client_id,
      'client_account_id': this.client_account_id,
      'roletypekey': this.roletypekey ? this.roletypekey : null
   };

   this._commonService.create(modal).subscribe(
      (response) => {
          this._alertService.success('Approval sent successfully!');
          (<any>$('#payment-approval')).modal('hide');
          (<any>$('#approval-history')).modal('hide');
          this.fiscodeArr = [];
          this.getPayableApproval();
          this.isButtonClicked = false;
      },
      (error) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          this.isButtonClicked = false;
      }
    );
  }
  confirmReject() {
    (<any>$('#approval-history')).modal('hide');
    (<any>$('#payment-approval')).modal('hide');
    (<any>$('#reject-approval')).modal('show');
    this.reason_tx = '';
  }

  rejectApproval() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = 'tb_account_transaction/deleteClientTransactionbyAuthId' + '/' + this.authorization_id;
    const modal = {
      client_account_id: this.client_account_id,
      fiscal_category_cd: this.fiscal_category_cd,
      cost_no: this.cost_no,
      'case_id': this.caseID
    };
    this._commonService.create(modal).subscribe(
    (response) => {
      this._alertService.success('Approval has been Denied!');
      (<any>$('#reject-approval')).modal('hide');
      this.getPayableApproval();
      this.isButtonClicked = false;
    },
    (error) => {
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      this.isButtonClicked = false;
    });

  }

  rejectOtherApproval() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.approval.PurchaseAuthorizationReject + this.authorization_id;
    const modal = {
      reason_tx: this.reason_tx
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        this._alertService.success('Purchase Authorization has been Denied!');
        (<any>$('#reject-approval')).modal('hide');
        this.getPayableApproval();
        this.isButtonClicked = false;
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        this.isButtonClicked = false;
      }
    );
  }


  viewChildAccount(clientid, accno) {
    (<any>$('#approval-history')).modal('hide');
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: {client_id: clientid },
      nolimit: true,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.childAccounts.getchildaccountslistUrl + '?filter').subscribe(result => {
      this.childAccountsList = result.data;
      if (accno && this.childAccountsList && this.childAccountsList.length > 0) {
        this.childAccountsList = this.childAccountsList.filter(data => data.account_no_tx == accno);
      }
      (<any>$('#view-child-account')).modal('show');
    });
  }

  showPrevious(id) {
    (<any>$(`#${id}`)).modal('show');
  }

  startsWith = function (item) {
    if (item.fiscalcateforycd.indexOf('21') !== 0) {
        return item;
    }
  }
}
