import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { PaginationRequest, DropdownModel, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../../finance.url.config';
import { Observable } from 'rxjs/Observable';
import { FindUrlConfig } from '../../../../find/find.url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { PurchaseAuthorization } from '../../../_entities/finance-entity.module';
import { PurchaseAuthorizationParams } from '../../../finance.constants';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker/case-worker-url.config';
const FISCAL_CODE_DISABLE = ['2126', '2127', '7126', '7127'];
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'payment-approval',
  templateUrl: './payment-approval.component.html',
  styleUrls: ['./payment-approval.component.scss']
})
export class PaymentApprovalComponent implements OnInit {
  payableApproval = [];
  payableApprovalHistory: any[];
  authorization_id: any;
  fiscal_category_cd: any;
  cost_no: any;
  provider_id: any;
  intakeserviceid: any;
  startDate: any;
  endDate: any;
  payment_method_cd: any;
  paymentType$: Observable<any[]>;
  paymentType: any[];
  purchaseRequestForm: FormGroup;
  reportCheck: string;
  paymentMethod$: Observable<DropdownModel[]>;
  isapproved: any;
  client_account_id: number;
  purchaseServiceForm: FormGroup;
  purchaseAuthData: PurchaseAuthorization;
  fiscalCode$: Observable<DropdownModel[]>;
  actualAmount: number;
  viewPurchaseDetails = PurchaseAuthorization;
  authorizationID: any;
  actualCost: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalcount: number;
  totalPage: number;
  pageInfo: PaginationInfo = new PaginationInfo();
  remarks: any;
  street: string;
  city: string;
  county: string;
  state: string;
  zipCode: string;
  paymentAddress: string;
  reason_tx: any;
  childAccountReject: boolean;
  isRejected: boolean;
  disableApprove: boolean;
  timestamp: Date;
  statusDropDownList = [
    {
      'text': 'All',
      'value': 'All'
    },
    {
      'text': 'Approved',
      'value': 'A'
    },
    {
    'text': 'Pending',
    'value': 'P'
    },
    {
      'text': 'Denied',
      'value': 'R'
    },
  ];
  paymentStatus: any;
  status: any;
  reasonDesc: boolean;
  otherCase: boolean;
  fiscalCodes: any[];
  persondob: Date;
  isapprovedFlag: boolean;
  reportableCheck: boolean;
  paymentPayee: any;
  paymentPayeeName: any;
  streetNo: string;
  disableupdateButton: boolean;
  service_id: any;
  clientEligibility: any;
  clientEligibilityStatus: any;
  childAccountsList: any[];
  client_id: any;
  streetDir: string;
  suit: string;
  adrUnitType: string;
  adrUnitNo: string;
  stateandZip: string;
  isButtonClicked: boolean;
  caseID: any;
  updateAuth: boolean;
  payableApprovalID: any;
  activeModule: any;
  roletype: any;

  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _formBuilder: FormBuilder,
    private _datastoreService: DataStoreService,
    private _sessionStorage: SessionStorageService
    ) { }

  ngOnInit() {
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    if (this.activeModule === 'Finance') {
      this.roletype = 'FNSFW';
    } else if (this.activeModule === 'Finance Approval') {
      this.roletype = 'FNSFS';
    }
    this.paymentStatus = 'P';
    this.timestamp = new Date();
    this.paginationInfo.pageNumber = 1;
    this.paymentPayeeName = null;
    this.pageInfo.sortBy = null;
      this.pageInfo.sortColumn = null;
    this.getPayableApproval();
    this.buildForm();
    this.getPaymentMethod();
    this.purchaseRequestForm.get('payment_id').disable();
    this.purchaseRequestForm.get('payment_start_dt').disable();
    this.purchaseRequestForm.get('payment_end_dt').disable();
    // this.getFiscalCategory();
    this._datastoreService.currentStore.subscribe(store => {
      const searchParams = store[PurchaseAuthorizationParams.PaymentParms];
      if (searchParams) {
        this.onAuthId(searchParams);
      }
    });
  }

  statusPaymentDropDown(status) {
    this.paymentStatus = status;
    this.getPayableApproval();
  }

  buildForm() {
    this.purchaseRequestForm = this._formBuilder.group({
      payment_start_dt: [null],
      payment_end_dt: [null],
      payment_method_cd: [null, Validators.required],
      type_1099_cd: [null, Validators.required],
      amount_no: [null],
      store_receipt_id: [null],
      actualAmount: [null],
      payment_id: [null],
      report_1099_sw: ['']
    });

    this.purchaseServiceForm = this._formBuilder.group(
      {
          fiscalCode: ['', Validators.required],
          voucherRequested: ['', Validators.required],
          costnottoexceed: '',
          justificationCode: '',
          startDt: [null, Validators.required],
          endDt: [null, Validators.required],
          fundingstatus: '',
          authorization_id: '',
          paymentstatus: '',
          final_amount_no: '',
          actualAmount: '',
          client_account_no: '',
          reason_tx: ''
      });

  }

  viewPurchaseAuthorization(purchaseDetail) {
    this.disableApprove = true;
    this.authorizationID = purchaseDetail.authorization_id;
    this.persondob = purchaseDetail.dob;
    this.viewPurchaseDetails = purchaseDetail;
    this._commonService.getArrayList(
      new PaginationRequest({
          page: 1,
          limit: 20,
          where: { service_log_id: purchaseDetail.service_log_id },
          method: 'get'
      }),
      FinanceUrlConfig.EndPoint.accountsPayable.approval.purchaseAuthorizationGet + '?filter'
      ).subscribe(result => {
        if (result['data'] && result['data'].length > 0) {
            const paDetails = result['data'];
            this.purchaseAuthData = (paDetails.length > 0) ? paDetails.find(item => item.authorization_id === this.authorizationID) : {};
            if (this.purchaseAuthData && this.purchaseAuthData.status) {
              if (this.purchaseAuthData.status === 41 || purchaseDetail.routingstatustypeid === 41) {
                this.disableApprove = true;
                this.purchaseServiceForm.disable();
              } else {
                  this.disableApprove = false;
                 // this.purchaseServiceForm.disable();
              }
          } else {
            this.disableApprove = true;
            this.purchaseServiceForm.disable();
          }
            // this.actualAmount = this.purchaseAuthData.final_amount_no;
            const model = {
              fiscalCode: this.purchaseAuthData.fiscal_category_cd,
              voucherRequested: this.purchaseAuthData.voucher_requested,
              costnottoexceed: this.purchaseAuthData.cost_no ? this.purchaseAuthData.cost_no : '0.00',
              justificationCode: this.purchaseAuthData.justification_text,
              fundingstatus: this.purchaseAuthData.fundingstatus,
              authorization_id: this.purchaseAuthData.authorization_id ? this.purchaseAuthData.authorization_id : '',
              startDt: this.purchaseAuthData.startdt,
              endDt: this.purchaseAuthData.enddt,
              actualAmount: this.purchaseAuthData.final_amount_no,
              paymentstatus: this.purchaseAuthData.paymentstatus,
              final_amount_no: this.purchaseAuthData.final_amount_no ? this.purchaseAuthData.final_amount_no : '0.00',
              client_account_no: this.purchaseAuthData.client_account_no ? this.purchaseAuthData.client_account_no : '',
              reason_tx: this.purchaseAuthData.reason_tx ? this.purchaseAuthData.reason_tx : ''
          };
          this.purchaseServiceForm.setValue(model, { emitEvent: true, onlySelf: false });
          this.purchaseServiceForm.disable();
          if (this.remarks === 'Pending') {
            this.purchaseServiceForm.get('justificationCode').enable();
            if (!FISCAL_CODE_DISABLE.includes(this.purchaseAuthData.fiscal_category_cd)) {
              this.purchaseServiceForm.get('fiscalCode').enable();
              this.disableupdateButton = false;
            } else {
              this.disableupdateButton = true;
            }
            this.purchaseRequestForm.get('amount_no').enable();
            this.purchaseRequestForm.get('payment_method_cd').enable();
            this.purchaseRequestForm.get('store_receipt_id').enable();
            this.purchaseRequestForm.patchValue({
              amount_no: this.purchaseAuthData.cost_no ? this.purchaseAuthData.cost_no : '0.00'
            });
          } else if ( this.remarks === 'Denied' ) {
            this.purchaseServiceForm.get('justificationCode').disable();
            this.purchaseServiceForm.get('fiscalCode').disable();
            this.purchaseRequestForm.get('amount_no').disable();
            this.purchaseRequestForm.get('payment_method_cd').disable();
            this.purchaseRequestForm.get('store_receipt_id').disable();
            this.purchaseServiceForm.get('reason_tx').disable();
            this.purchaseRequestForm.patchValue({
              amount_no: this.purchaseAuthData.cost_no ? this.purchaseAuthData.cost_no : '0.00'
            });
          } else if ( this.remarks === 'Approved' ) {
            this.purchaseRequestForm.disable();
          }
      }
      });
      }

      actualCostChange(amount) {
        let actual_cost = this.actualCost;
        const percentage = actual_cost * 0.15;
        actual_cost = percentage + (+actual_cost);
        if (parseFloat(amount) > actual_cost) {
          this._alertService.warn('Not allowed to approve more than 15% from the actual cost');
          this.purchaseRequestForm.patchValue({
            cost_no : this.actualCost
          });
        }
      }

      checkDec(el) {
        // const ex = /^\d+(\.\d{1,2})?$/;
        // if (el.target.value !== '' && !ex.test(el.target.value)) {
        //   return el.target.value =  parseFloat(el.target.value.replace(/[^0-9.]{0,2}/g, '')).toFixed(2);
        // } else {
        //   return el.target.value;
        // }

        if (el.target.value !== '') {
          return el.target.value = isNaN(el.target.value) ? '' : el.target.value.replace(/[^0-9.]{0,2}/g, '');
        }
        return '';
      }

      getFiscalCategoryCode(vendorprogramid) {
        this._commonService.getArrayList({
                where: {agencyprogramareaid: vendorprogramid},
                method: 'get'
              }, FinanceUrlConfig.EndPoint.accountsPayable.approval.fiscalCodes + '?filter',
            ).subscribe( result => {

                this.fiscalCodes = result;
                if (!this.disableupdateButton) {
                  this.fiscalCodes = this.fiscalCodes.filter(item => !(FISCAL_CODE_DISABLE.includes(item.fiscalcateforycd)));
                }
                // THIS RULE APPLICABLE FOR IV-E - Should not block service-log, Filter needs to be revisited
                // this.fiscalCodes = this.fiscalCodes.filter(item => item.eligibility_cd === this.clientEligibilityStatus);
                if (this.service_id) {
                    if (this.clientEligibilityStatus === '2913') {
                       // this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                    } else {
                        this.fiscalCodes = this.fiscalCodes.filter(item => this.startsWith(item));
                    }
                    if (this.service_id === 11333) {
                       // if (this.clientEligibilityStatus === '3951') {
                        if (this.clientEligibilityStatus === '2913') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7126');
                        }
                   } else if (this.service_id === 11334) {
                        // if (this.clientEligibilityStatus === '3951') {
                            if (this.clientEligibilityStatus === '2913') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2127');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7127');
                        }
                   } else {
                       if (this.clientEligibilityStatus === '3951') {
                        this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                    }
                   }
                }


               /*  this.fiscalCodes = result;
                this.fiscalCodes = this.fiscalCodes.filter(item => item.eligibility_cd === this.clientEligibilityStatus);
                if (this.service_id) {
                    if (this.service_id === 11333) {
                        if (this.clientEligibilityStatus === '3951') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7126');
                       }
                   } else if (this.service_id === 11334) {
                        if (this.clientEligibilityStatus === '3951') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2127');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7127');
                        }
                   }
                } */
                this.purchaseServiceForm.patchValue( {fiscalCode: this.fiscal_category_cd} );
            });
    }


      documentGenerate() {
        if (this.remarks === 'Approved') {
          this.isapprovedFlag = true;
        } else {
            this.isapprovedFlag = false;
        }
        this._commonService.endpointUrl = 'evaluationdocument/generateintakedocument';
        const modal = {
          count: -1,
          where: {
              documenttemplatekey: ['purchaseauthorization'],
              authorizationid: this.authorizationID,
              isapproved: this.isapprovedFlag
          },
          method: 'post'
        };
        this._commonService.download('evaluationdocument/generateintakedocument', modal)
        .subscribe(res => {
          const blob = new Blob([new Uint8Array(res)]);
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
         // link.download = 'Purchase_authorization-' + this.authorizationID + moment(this.timestamp).format('MM/DD/YYYY HH:mm') + '.pdf';
          const timestamp = moment(this.timestamp).format('MM/DD/YYYY HH:mm:ss');
          link.download = 'Purchase-authorization-Form-' + this.authorizationID + '.' + timestamp + '.pdf';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        });
      }

    onSearchPayee() {
      if (this.paymentPayee) {
        this.paymentPayeeName = this.paymentPayee.replace(/'/g, `''`);
      } else {
        this.paymentPayeeName = null;
      }
      this.getPayableApproval();
    }
    onSortedPayment($event: ColumnSortedEvent) {
      this.pageInfo.sortBy = $event.sortDirection;
      this.pageInfo.sortColumn = $event.sortColumn;
      this.getPayableApproval();
    }
  getPayableApproval() {
    if (this.paymentStatus === 'All') {
      this.status = null;
    } else {
      this.status = this.paymentStatus;
    }
    this.payableApproval = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {approveltype: 'payment', status: this.status,
        payee_nm: this.paymentPayeeName ? this.paymentPayeeName : null,
        sortcolumn: this.pageInfo.sortColumn,
        sortorder: this.pageInfo.sortBy,
        roletypekey: this.roletype},
        limit : this.pageInfo.pageSize,
        page: this.pageInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter'
    ).subscribe((result: any) => {
      this.payableApproval = result;
      if (this.payableApproval && this.updateAuth) {
         this.payableApprovalID = this.payableApproval.find(item => item.authorization_id === this.authorizationID);
         this.onAuthId(this.payableApprovalID);
      }
      this.totalPage = (this.payableApproval && this.payableApproval.length > 0) ? this.payableApproval[0].totalcount : 0;
    });

  }

  pageNumberChanged(page) {
    this.pageInfo.pageNumber = page;
    this.getPayableApproval();
  }

  getClientEligibility(client_id, caseNumber, programkey) {
    this._commonService.getArrayList({
        where: {
            client_id: client_id,
            case_id: caseNumber ? caseNumber : null,
        },
        method: 'get'
    }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientEligibility).subscribe((response) => {
        this.clientEligibility = response['data'];
        this.clientEligibilityStatus = this.clientEligibility[0].eligibility_status_cd;
      this.getFiscalCategoryCode(programkey);
    });
}

  onAuthId(authid) {
    this.caseID = authid.case_id ? authid.case_id : null;
    this.service_id = authid.service_id;
    this.fiscal_category_cd = authid.fiscal_category_cd;
    this.getClientEligibility(authid.client_id, authid.case_id, authid.programkey);
    this.client_id = authid.client_id;
    (<any>$('#approval-history')).modal('show');
    if (authid && (authid.fiscal_category_cd === '7502' ||
    authid.fiscal_category_cd === '7503')) {
      this.childAccountReject = true;
    } else {
      this.childAccountReject = false;
    }
    if (authid && (authid.remarks === 'Denied')) {
      this.isRejected = true;
      this.purchaseServiceForm.disable();
    } else {
      this.isRejected = false;
    }
    this.purchaseRequestForm.patchValue(authid);
    if (authid.remarks === 'Denied') {
      this.reasonDesc = true;
    } else {
      this.reasonDesc = false;
    }
    this.service_id = authid.service_id;
    this.isapproved = authid.isapproved;
    this.otherCase = authid.bmanualrouting;
    this.authorization_id = authid.authorization_id;
    this.fiscal_category_cd = authid.fiscal_category_cd;
    this.cost_no = authid.cost_no ? authid.cost_no : '0.00';
    this.provider_id = authid.provider_id;
    this.intakeserviceid = authid.intakeserviceid;
    this.startDate = authid.payment_start_dt;
    this.endDate = authid.payment_end_dt;
    this.remarks = authid.remarks;
    this.payment_method_cd = authid.payment_method_cd;
    this.client_account_id = authid.client_account_id;
    this.actualCost = authid.cost_no;
    this.streetNo = authid.adr_street_no ? authid.adr_street_no + ' ' : '';
    this.streetDir = authid.adr_pre_dir_cd ? authid.adr_pre_dir_cd + ' ' : '';
    this.street = authid.adr_street_nm ? authid.adr_street_nm + '  ' : '';
    this.suit = authid.adr_street_suffix_cd ? authid.adr_street_suffix_cd + ' ' : '';
    this.adrUnitType = authid.adr_unit_type_cd ? authid.adr_unit_type_cd + ' ' : '';
    this.adrUnitNo = authid.adr_unit_no_tx ? authid.adr_unit_no_tx + ' ' : '';
    this.city = authid.adr_city_nm ? authid.adr_city_nm + ',' : '';
    // this.county = authid.county_cd ? authid.county_cd + ', ' : '';
    this.state = authid.adr_state_cd ? authid.adr_state_cd + ' ' : '';
    this.zipCode =  authid.adr_zip5_no ? authid.adr_zip5_no  : '';
    this.paymentAddress = this.streetNo + this.streetDir + this.street + this.suit + this.adrUnitType + this.adrUnitNo + this.city;
    this.stateandZip = this.state + this.zipCode;
    this.purchaseRequestForm.patchValue({
      amount_no: authid.amount_no ? authid.amount_no : '0.00',
      payment_start_dt: authid.payment_start_dt,
      payment_end_dt: authid.payment_end_dt,
      payment_id: authid.payment_id ? authid.payment_id : ''
    });
    this.getApproveHistory(authid.authorization_id);
    this.getPaymentType();
    if (authid.remarks !== 'Approved' && authid.remarks !== 'Denied') {
      this.purchaseRequestForm.patchValue({ type_1099_cd : '3158'});
      this.purchaseRequestForm.get('type_1099_cd').disable();
    } else {
      if (authid.report_1099_sw === 'Y') {
        this.purchaseRequestForm.patchValue({ report_1099_sw : 'Y'});
      } else {
        this.purchaseRequestForm.patchValue({ report_1099_sw : false});
      }
     // this.purchaseRequestForm.disable();
    }
    this.viewPurchaseAuthorization(authid);
    setTimeout(() => {
      this._datastoreService.clearStore();
    }, 100);
  }

  changeFCC(value) {
    if (!(value === '7502' || value === '7503')) {
      this.purchaseServiceForm.patchValue({
        client_account_no: null
      });
    }
    (<any>$('#approval-history')).modal('hide');
    (<any>$('#update-approval')).modal('show');
  }
  revertUpdate() {
    this.purchaseServiceForm.patchValue({
      fiscalCode: this.fiscal_category_cd
    });
  }

  updateAuthoriz(mode?: any) {
    let age = 0;
    const purchaseDate = moment(this.purchaseServiceForm.get('endDt').value);
    if (this.persondob && moment(new Date(this.persondob), 'MM/DD/YYYY', true).isValid()) {
        const pDob = moment(new Date(this.persondob), 'MM/DD/YYYY').toDate();
        age = purchaseDate.diff(pDob, 'years');


      if (this.purchaseServiceForm.get('fiscalCode').value === '5113') {
          if (age < 14 || age > 18) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5114' ||
                 this.purchaseServiceForm.get('fiscalCode').value === '5115')  {
            if (age < 18 || age > 21) {
              this._alertService.error('Invalid Fiscal Category Code due to Client age');
              this.purchaseServiceForm.patchValue({
                fiscalCode: this.purchaseAuthData.fiscal_category_cd
              });
              return false;
            }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5116') {
        if (age < 18) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5117') {
        if (age < 18 || age > 21) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5118') {
          if (age < 16 || age > 21) {
            this._alertService.error('Invalid Fiscal Category Code due to Client age');
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.purchaseAuthData.fiscal_category_cd
            });
            return false;
          }
      } else if (this.purchaseServiceForm.get('fiscalCode').value === '7110' ||
                 this.purchaseServiceForm.get('fiscalCode').value === '2110') {
        if (+this.cost_no > 2000) {
          this._alertService.error('Selected Fiscal Category will be not allowed if the amount is more than $2000.00');
          this.purchaseServiceForm.patchValue({
            fiscalCode: this.purchaseAuthData.fiscal_category_cd
          });
          return false;
        }
      }
    }
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.history.updatePurchaseAuthorizationGet;
    const modal = {
      authorization_id: this.authorization_id,
      fiscal_category_cd: this.purchaseServiceForm.get('fiscalCode').value,
      justification_tx: this.purchaseServiceForm.get('justificationCode').value,
      'costno' : this.cost_no ? this.cost_no : '0.00',
      'client_id': this.client_id
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        if (response && response.isexceed) {
         if (mode === 'update') {
           // (<any>$('#approval-history')).modal('show');
            (<any>$('#update-approval')).modal('hide');
          }
          if (response.isexceed === 3) {
            if (this.purchaseServiceForm.get('fiscalCode').value === '7502' ||
            this.purchaseServiceForm.get('fiscalCode').value === '7503') {
              this.updateAuth = true;
            } else {
              (<any>$('#approval-history')).modal('show');
              this.updateAuth = false;
            }
            this.fiscal_category_cd = this.purchaseServiceForm.get('fiscalCode').value;
            this._alertService.success('Fiscal category has been Updated');
            this.getPayableApproval();
          } else if (response.isexceed === 2) {
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.fiscal_category_cd
            });
            this._alertService.error(`Insufficient Client Account balance, please raise a request at lower cost`);
          } else if (response.isexceed === 1) {
            this.purchaseServiceForm.patchValue({
              fiscalCode: this.fiscal_category_cd
            });
            this._alertService.error(`Account is not available, kindly please add account for the selected client`);
          }
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  getPaymentType() {
    this.paymentType = [];
    this.paymentType$ = this._commonService.getArrayList({
      where: {picklist_type_id : '308'},
      nolimit: true,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsPayable.PaymentPickList + '?filter'
    ) .map((result) => {
      return result.map(
          (res) =>
              new DropdownModel({
                  text: res.value_tx,
                  value: res.picklist_value_cd
              })
      );
  });
  this.paymentType$.subscribe((response) => {
    for (let i = 0; i < response.length; i++) {
      if (response[i].value !== '3158') {
        this.paymentType.push(response[i]);
      }
    }
  });
  }

  getPaymentMethod() {
    this.paymentMethod$ = this._commonService.getArrayList({
      where: {picklist_type_id : '1'},
      nolimit: true,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsPayable.PaymentPickList + '?filter'
    ) .map((result) => {
      return result.map(
          (res) =>
              new DropdownModel({
                  text: res.description_tx,
                  value: res.picklist_value_cd
              })
      );
  });
  }

  getApproveHistory(authid) {
    this.payableApprovalHistory = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {authorization_id: authid},
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApprovalHistory + '?filter'
    ).subscribe((result: any) => {
      this.payableApprovalHistory = result;
      this.totalcount = (this.payableApprovalHistory && this.payableApprovalHistory.length > 0) ? this.payableApprovalHistory[0].totalcount : 0;
    });
  }

  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getApproveHistory(this.authorization_id);
  }

  repotCheck(event) {
    if (event) {
      this.reportCheck = 'Y';
      this.purchaseRequestForm.get('type_1099_cd').enable();
      this.paymentType$ = Observable.of(this.paymentType);
    } else {
      this.getPaymentType();
      this.reportCheck = 'N';
      this.purchaseRequestForm.get('type_1099_cd').reset();
     setTimeout(() => {
      this.purchaseRequestForm.patchValue({type_1099_cd : '3158'});
      this.purchaseRequestForm.get('type_1099_cd').disable();
     }, 50);
    }
  }

  finalApproval() {
    this.isButtonClicked = true;
    if (this.purchaseRequestForm.invalid) {
      this._alertService.error('Please fill the mandatory details.');
      this.isButtonClicked = false;
      return false;
    } else {

      let actual_cost = this.actualCost;
      const percentage = actual_cost * 0.15;
      actual_cost = percentage + (+actual_cost);
      if (parseFloat(this.purchaseRequestForm.get('amount_no').value) > actual_cost) {
        this._alertService.warn('Not allowed to approve more than 15% from the actual cost');
        this.purchaseRequestForm.patchValue({
          amount_no : this.actualCost
        });
        return false;
      }

      this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.ServiceLogApproval;
        const modal = {
           'case_id' : this.caseID,
           'fiscalcategorycd' : this.fiscal_category_cd,
           'costno' : this.purchaseRequestForm.get('amount_no').value ? this.purchaseRequestForm.get('amount_no').value : '0.00',
           'authorization_id' : this.authorization_id,
           'provider_id' : this.provider_id,
           'intakeserviceid' : this.intakeserviceid,
           'eventcode' : 'PCAUTH',
           'status' : 43,
           'startDt': this.startDate,
           'endDt': this.endDate,
           'payment_method_cd': this.purchaseRequestForm.get('payment_method_cd').value,
           'type_1099_cd': this.purchaseRequestForm.get('type_1099_cd').value,
           'store_receipt_id': this.purchaseRequestForm.get('store_receipt_id').value,
           'report_1099_sw': this.reportCheck,
           'client_account_id': this.client_account_id,
           'bmanualrouting': this.otherCase
        };
      this._commonService.create(modal).subscribe(
          (response) => {
              this._alertService.success('Approval sent successfully!');
              (<any>$('#approval-history')).modal('hide');
              this.getPayableApproval();
              this.isButtonClicked = false;
          },
          (error) => {
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
              this.isButtonClicked = false;
          }
      );
    }
  }

  confirmReject() {
    (<any>$('#approval-history')).modal('hide');
    (<any>$('#payment-approval')).modal('hide');
    (<any>$('#reject-approval')).modal('show');
    this.reason_tx = '';
  }

  rejectApproval() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = 'tb_account_transaction/deleteClientTransactionbyAuthId' + '/' + this.authorization_id;
    const modal = {
      client_account_id: this.client_account_id,
      fiscal_category_cd: this.fiscal_category_cd,
      'case_id': this.caseID,
      cost_no: this.cost_no
    };
    this._commonService.create(modal).subscribe(
    (response) => {
      this._alertService.success('Approval has been Denied!');
      (<any>$('#reject-approval')).modal('hide');
      this.getPayableApproval();
      this.isButtonClicked = false;
    },
    (error) => {
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      this.isButtonClicked = false;
    });

  }

  rejectOtherApproval() {
    this.isButtonClicked = true;
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.approval.PurchaseAuthorizationReject + this.authorization_id;
    const modal = {
      reason_tx: this.reason_tx
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        this._alertService.success('Purchase Authorization has been Denied!');
        (<any>$('#reject-approval')).modal('hide');
        this.getPayableApproval();
        this.isButtonClicked = false;
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        this.isButtonClicked = false;
      }
    );
  }

  viewChildAccount(clientid, accno) {
    (<any>$('#approval-history')).modal('hide');
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: {client_id: clientid },
      nolimit: true,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.childAccounts.getchildaccountslistUrl + '?filter').subscribe(result => {
      this.childAccountsList = result.data;
      if (accno && this.childAccountsList && this.childAccountsList.length > 0) {
        this.childAccountsList = this.childAccountsList.filter(data => data.account_no_tx == accno);
      }
      (<any>$('#view-child-account')).modal('show');
    });
  }

  showPrevious(id) {
    (<any>$(`#${id}`)).modal('show');
  }

  startsWith = function (item) {
    if (item.fiscalcateforycd.indexOf('21') !== 0) {
        return item;
    }
  }
}
