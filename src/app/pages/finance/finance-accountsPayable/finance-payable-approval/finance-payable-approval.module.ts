import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinancePayableApprovalRoutingModule } from './finance-payable-approval-routing.module';
import { FinancePayableApprovalComponent } from './finance-payable-approval.component';
import { FinancialApprovalComponent } from './financial-approval/financial-approval.component';
import { PaymentApprovalComponent } from './payment-approval/payment-approval.component';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatButtonModule, MatRadioModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectorApprovalComponent } from './director-approval/director-approval.component';
import { SortTableModule } from '../../../../shared/modules/sortable-table/sortable-table.module';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    FinancePayableApprovalRoutingModule,
    PaginationModule,
    A2Edatetimepicker,
    NgSelectModule,
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    SortTableModule,
    FormMaterialModule
  ],
  declarations: [FinancePayableApprovalComponent, FinancialApprovalComponent, PaymentApprovalComponent, DirectorApprovalComponent]
})
export class FinancePayableApprovalModule { }
