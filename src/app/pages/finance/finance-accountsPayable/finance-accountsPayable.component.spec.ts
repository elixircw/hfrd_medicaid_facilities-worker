import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';

import { CoreModule } from '../../../@core/core.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { FinanceAccountsPayableSearchFiltersComponent } from './finance-accountsPayable-search-filters/finance-accountsPayable-search-filters.component';
import { FinanceAccountsPayableSearchResultComponent } from './finance-accountsPayable-search-result/finance-accountsPayable-search-result.component';
import { FinanceAccountsPayableComponent } from './finance-accountsPayable.component';

describe('FinanceAccountsPayableComponent', () => {
    let component: FinanceAccountsPayableComponent;
    let fixture: ComponentFixture<FinanceAccountsPayableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                A2Edatetimepicker,
                SharedPipesModule
            ],
            declarations: [FinanceAccountsPayableComponent, FinanceAccountsPayableSearchFiltersComponent, FinanceAccountsPayableSearchResultComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FinanceAccountsPayableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
