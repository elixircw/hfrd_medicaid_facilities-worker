import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayableAncillaryRoutingModule } from './payable-ancillary-routing.module';
import { PayableAncillaryComponent } from './payable-ancillary.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatButtonModule, MatRadioModule } from '@angular/material';
import { ProviderInfoPopupModule } from '../../provider-info-popup/provider-info-popup.module';

@NgModule({
  imports: [
    CommonModule,
    PayableAncillaryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    A2Edatetimepicker,
    NgSelectModule,
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatButtonModule,
    MatRadioModule,
    ProviderInfoPopupModule
  ],
  declarations: [PayableAncillaryComponent]
})
export class PayableAncillaryModule { }
