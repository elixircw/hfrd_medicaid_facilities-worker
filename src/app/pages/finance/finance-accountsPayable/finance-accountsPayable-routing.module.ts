import { RoleGuard } from '../../../@core/guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinanceAccountsPayableComponent } from './finance-accountsPayable.component';
import { FinanceAccountsPayableSearchViewComponent } from './finance-accountsPayable-search-view/finance-accountsPayable-search-view.component';

const routes: Routes = [
  {
    path: '',
    component: FinanceAccountsPayableComponent,
    canActivate: [RoleGuard],
    children: [
      { path: 'showFinanceAccountsPayableDetails/:id', component: FinanceAccountsPayableSearchViewComponent }
    ],
    data: { roles: ['admin', 'intakeuser', 'caseworker', 'reviewer'] }
  },
  {
    path: 'ancillary',
    loadChildren: './payable-ancillary/payable-ancillary.module#PayableAncillaryModule'
  },
  {
    path: 'history',
    loadChildren: './history/history.module#HistoryModule'
  },
  {
    path: 'approval',
    loadChildren: './finance-payable-approval/finance-payable-approval.module#FinancePayableApprovalModule'
    // './finance-payable-approval/finance-payable-approval.module#FinancePayableApprovalModule'
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceAccountsPayableRoutingModule { }
