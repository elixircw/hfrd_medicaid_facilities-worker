import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../@core/services';
import { ClientPayment } from '../../../finance.constants';


@Component({
  selector: 'client-payment',
  templateUrl: './client-payment.component.html',
  styleUrls: ['./client-payment.component.scss']
})
export class ClientPaymentComponent implements OnInit {

  constructor( private _dataStoreService: DataStoreService) {
   }

  ngOnInit() {
    this._dataStoreService.setData(ClientPayment.ClientPaymentSearchParams, null);
  }

}
