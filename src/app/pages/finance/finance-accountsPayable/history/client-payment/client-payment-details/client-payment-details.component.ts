import { Component, OnInit, AfterContentInit } from '@angular/core';
import { FinanceUrlConfig } from '../../../../finance.url.config';
import { CommonHttpService, DataStoreService, AlertService } from '../../../../../../@core/services';
import { PaginationRequest, PaginationInfo, DropdownModel } from '../../../../../../@core/entities/common.entities';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientPayment } from '../../../../finance.constants';
import { ColumnSortedEvent } from '../../../../../../shared/modules/sortable-table/sort.service';
import { Observable } from 'rxjs/Observable';
import { take } from 'rxjs/operator/take';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'client-payment-details',
  templateUrl: './client-payment-details.component.html',
  styleUrls: ['./client-payment-details.component.scss']
})
export class ClientPaymentDetailsComponent implements OnInit, AfterContentInit {
  paymentHistoryResult = [];
  _providerService: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  pageInfo: PaginationInfo = new PaginationInfo();
  totalcount: any;
  paymentDetailView: boolean;
  paymentDetail: any[];
  paymentDetailList: any[];
  clientData: any;
  caseWorker: any;
  client_id: any;
  payment_id: any;
  totalRecords: any;
  paymentTypeList: any[];
  paymenttype:any;
  paymentInterface: any[];
  paymentId: any;
  paymentinterfaceStatuses: any;
  payment: any;
  checkPaymentStatus: any;
  statuscheck: any;
  notes: any;
  checkStatus$: Observable<DropdownModel[]>;
  paymentdaterangefrom: any;
  paymentdaterangeto: any;
  date_sw: any;
  date_from: any;
  date_to: any;
  selectedClient: any;
  oldcheckPaymentStatus: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _alertService: AlertService
  ) { }

  ngOnInit() {
    this.clientData = this._dataStoreService.getCurrentStore();
    if (this.clientData && this.clientData.SelectedClientSource) {
      this.selectedClient = this.clientData.SelectedClientSource;
    }
    this.paginationInfo.sortColumn = 'paymentDate';
    this.paginationInfo.sortBy = 'desc';
    this.populatePaymentType();
    this.date_sw = 'D';
    const searchParams = this._dataStoreService.getData(ClientPayment.ClientPaymentSearchParams);
    if (searchParams) {
      this.paymentdaterangefrom = searchParams.daterangefrom;
      this.paymentdaterangeto = searchParams.daterangeto;
      if (this.paymentdaterangefrom) {
        this.date_sw = 'D';
      }
      this.date_from = this.paymentdaterangefrom ? new Date(this.paymentdaterangefrom) : null;
      this.date_to = this.paymentdaterangeto ? new Date(this.paymentdaterangeto) : null;
    }
  }

  ngAfterContentInit() {
    if (this.clientData && this.clientData.SelectedClientSource) {
        this.client_id = this.clientData.SelectedClientSource.client_id;
      this.paymentHistory(null);
      this._dataStoreService.setData(ClientPayment.SelectedClientSource, null);
    }
  }

  populatePaymentType(){
    this.paymentTypeList = [];
    this._commonService.getArrayList(new PaginationRequest({
        where: {
          'picklist_type_id': '2'
        },
        nolimit: true,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.childAccounts.pickListUrl).subscribe((result) => {
        return result.filter((res) => {
        //   if (res.picklist_value_cd === '4') {
        //    // return res;
        // } else {
          this.paymentTypeList.push(res);
          //this.populatePaymentTypeforDisbursement();

        //}
    });
    });
  }


  populatePaymentTypeforDisbursement(){
    this._commonService.getArrayList(new PaginationRequest({
        where: {
          'picklist_type_id': '10044'
        },
        nolimit: true,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.childAccounts.pickListUrl).subscribe((result) => {
        return result.filter((res) => {
          if (res.picklist_value_cd === '4' || res.picklist_value_cd === '5989') {
           // return res;
        } else {
          this.paymentTypeList.push(res);
        }
    });
    });
  }
  paymentHistory(paymenttype) {
   // this.client_id = client_id;
   this.paymenttype = paymenttype;
   const searchParams = this._dataStoreService.getData(ClientPayment.ClientPaymentSearchParams);
     this._commonService.getPagedArrayList(
      new PaginationRequest({
      where: {
        client_id: this.client_id,
        sortcolumn: this.paginationInfo.sortColumn,
        sortorder: this.paginationInfo.sortBy,
        paymenttype: paymenttype,
        daterangefrom : searchParams.daterangefrom,
        daterangeto :  searchParams.daterangeto
      },
      method: 'get',
      limit: 10,
      page: this.paginationInfo.pageNumber
    }), 'tb_receivable_detail/getpaymenthistorybyclientid?filter'
    ).subscribe((result: any) => {
      if (result && result.data) {
        this.paymentHistoryResult = result.data;
        if(result.data && result.data[0] && result.data[0].caseworker && result.data[0].caseworker[0]){
            this.caseWorker = result.data[0].caseworker[0];
        }

        this.totalcount = (this.paymentHistoryResult && this.paymentHistoryResult.length > 0) ? this.paymentHistoryResult[0].totalcount : 0;
      }
    });
  }

  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.paymentHistory(this.paymenttype);
  }

  viewPayment(mode, payment, id, index) {
    if (mode === 'add') {
      this.pageInfo.pageNumber = 1;
    }
   // this.paymentDetailView = true;
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle'); //
    (<any>$('.client-details-view tr')).removeClass('selected-bg');
    (<any>$(`#client-details-view-${index}`)).addClass('selected-bg');
    this.paymentDetailHistory(payment.payment_id);
  }

  paymentDetailHistory(payment_id) {
    this.payment_id = payment_id;
     this._commonService.getPagedArrayList(
      new PaginationRequest({
      where: {
        paymentid: payment_id,
        clientid: this.client_id,
        sortorder: 'asc',
        sortcolumn: 'detailedId'
      },
      method: 'get',
      limit: 10,
      page: this.pageInfo.pageNumber
    }), 'tb_receivable_detail/getpaymentdetails?filter'
    ).subscribe((result: any) => {
      if (result && result.data && result.data[0]['getpaymentdetails'].length) {
        this.paymentDetail = result.data[0]['getpaymentdetails'];
        this.totalRecords = (this.paymentDetail && this.paymentDetail.length > 0) ? this.paymentDetail[0].totalcount : 0;
      } else {
        this.paymentDetail = [];
      }
    });
  }

  pageNumberChanged(page) {
    this.paymentDetailHistory(this.payment_id);
    this.pageInfo.pageNumber = page;
  }

  backToSearch() {
    this._router.navigate(['../client-payment-result'], { relativeTo: this._route });
  }

  onHistorySorted($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.paymentHistory(this.paymenttype);
}

paymentInterfaceStatus(paymentID, paymentinterfaceStatus) {
  this._commonService.getArrayList({
      where: {
          payment_id: paymentID
      },
      method: 'get',
      limit: 10,
      page: 1,
      fmslimit: 10,
      fmspage: 1
  }, 'tb_payment_status/paymentafsfmis?filter'
  ).subscribe(response => {
      this.paymentInterface = response;
  });
  this.paymentId = paymentID;
  this.paymentinterfaceStatuses = paymentinterfaceStatus;
  this.loadcheckStatus();
}

editPayment(paymentDetails) {
  this.payment = paymentDetails;
  this.checkPaymentStatus = paymentDetails.check_status_cd ? paymentDetails.check_status_cd : null;
  this.statuscheck = paymentDetails.check_status_cd ? paymentDetails.check_status_cd : null;
  this.notes = paymentDetails.notes_tx ? paymentDetails.notes_tx : '';
  this.oldcheckPaymentStatus = this.checkPaymentStatus;
  (<any>$('#payment-interface')).modal('hide');
  (<any>$('#payment-details-edit')).modal('show');
}

updateCheckStatus() {
  if (this.notes && this.checkPaymentStatus) {
    if (this.checkPaymentStatus === this.statuscheck) {
      this._alertService.warn('Check Status is already updated, Please update new one');
      return false;
    } else {
        this._commonService.endpointUrl = 'tb_payment_header/updatePaymentCheckStatus/' + this.paymentId;
        const model = {
          notes: this.notes,
          check_status: this.checkPaymentStatus
        };
        this._commonService.create(model).subscribe(res => {
          if (res) {
            this._alertService.success('Check Status updated Successfully');
            (<any>$('#payment-details-edit')).modal('hide');
            this.paymentInterfaceStatus(this.payment.payment_id, this.paymentinterfaceStatuses);
            (<any>$('#payment-interface')).modal('show');
          }
        });
      }
  } else {
    this._alertService.error('Please fill the mandatory fields');
  }
}

loadcheckStatus() {
  this.checkStatus$ = this._commonService.getArrayList({
    where: {picklist_type_id : '37'},
    nolimit: true,
    method: 'get'
  }, FinanceUrlConfig.EndPoint.accountsPayable.PaymentPickList + '?filter'
  ) .map((result) => {
    return result.map(
        (res) =>
            new DropdownModel({
                text: res.value_tx,
                value: res.picklist_value_cd
            })
    );
  });
}


documentGenerate(type) {
  const modal = {
    count: -1,
    limit: null,
    page: 1,
    where: {
      documenttemplatekey: ['child111report'],
      clientid: this.client_id,
      date_sw: this.date_sw,
      date_from: this.date_from,
      date_to: this.date_to,
      paymenttype: this.paymenttype,
      format: type
    },
    method: 'post'
  };
  // if (type === 'pdf') {
    this._commonService.download('evaluationdocument/generateintakedocument', modal)
      .subscribe(res => {
        const blob = new Blob([new Uint8Array(res)]);
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        if (type === 'pdf') {
          link.download = 'DHS_111_Client_Expenditure_Report.pdf';
        } else if (type === 'excel') {
          link.download = 'DHS_111_Client_Expenditure_Report.xlsx';
        }

        document.body.appendChild(link);

        link.click();

        document.body.removeChild(link);
        (<any>$('#downloadReport')).modal('hide');
        this.resetDownload();
        this.date_sw = 'D';
        this.date_from = null;
        this.date_to = null;
      });
}

resetDownload() {
  // if (this.paymentdaterangefrom) {
  //   this.date_sw = 'D';
  // } else {
  //   this.date_sw = null;
  // }
  this.date_sw = 'D';
  this.date_from = this.paymentdaterangefrom ? new Date(this.paymentdaterangefrom) : null;
  this.date_to = this.paymentdaterangeto ? new Date(this.paymentdaterangeto) : null;
}
checkStatusChange(value) {
  if (value == '4849') {
    (<any>$('#payment-details-edit')).modal('hide');
    (<any>$('#confirm-checkstatus')).modal('show');
  } else {
    this.oldcheckPaymentStatus = this.checkPaymentStatus;
  }
}
ConfirmPopup(index) {
  (<any>$('#confirm-checkstatus')).modal('hide');
  (<any>$('#payment-details-edit')).modal('show');
  if (index === 1) {
    this.checkPaymentStatus = this.oldcheckPaymentStatus;
  }
}
}
