import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientPaymentRoutingModule } from './client-payment-routing.module';
import { ClientPaymentComponent } from './client-payment.component';
import { ClientPaymentDetailsComponent } from './client-payment-details/client-payment-details.component';
import { ClientPaymentResultComponent } from './client-payment-result/client-payment-result.component';
import { ClientPaymentSearchComponent } from './client-payment-search/client-payment-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatButtonModule, MatRadioModule } from '@angular/material';
import { SortTableModule } from '../../../../../shared/modules/sortable-table/sortable-table.module';

@NgModule({
  imports: [
    CommonModule,
    ClientPaymentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    A2Edatetimepicker,
    NgSelectModule, 
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatButtonModule,
    MatRadioModule,
    SortTableModule
  ],
  declarations: [ClientPaymentComponent, ClientPaymentDetailsComponent, ClientPaymentResultComponent, ClientPaymentSearchComponent]
})
export class ClientPaymentModule { }
