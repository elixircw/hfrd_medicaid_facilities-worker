import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoryRoutingModule } from './history-routing.module';
import { HistoryComponent } from './history.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatButtonModule, MatRadioModule, MatAutocompleteModule } from '@angular/material';
import { PaymentHistoryReportComponent } from './payment-history-report/payment-history-report.component';
import { CommonControlsModule } from '../../../../shared/modules/common-controls/common-controls.module';

@NgModule({
  imports: [
    CommonModule,
    HistoryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    A2Edatetimepicker,
    NgSelectModule, 
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatButtonModule,
    MatRadioModule,
    MatAutocompleteModule,
    CommonControlsModule
  ],
  declarations: [HistoryComponent, PaymentHistoryReportComponent]
})
export class HistoryModule { }
