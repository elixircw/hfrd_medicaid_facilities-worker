import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService, CommonHttpService } from '../../../../../../@core/services';
import { PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { FinanceStoreConstants, FinanceFundingSource } from '../../../../finance.constants';
import { FinanceUrlConfig } from '../../../../finance.url.config';

@Component({
  selector: 'funding-allocation-result',
  templateUrl: './funding-allocation-result.component.html',
  styleUrls: ['./funding-allocation-result.component.scss']
})
export class FundingAllocationResultComponent implements OnInit {
  searchParams: any;

  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  fundingSearch: any;
  totalRecords: number;
  isSsnHidden = true;
  ssnEye = 'fa-eye';
  showSsnMask = true;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _datastoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {
    }

  ngOnInit() {
    this.paginationInfo.pageNumber = 1;
  }



  getFundingSearchData(page) {
    this.paginationInfo.pageNumber = page;
        const searchParams = this._datastoreService.getData(FinanceFundingSource.FundingSourceSearchParams);
        if (!searchParams){
          this.searchParams = null;
          this.fundingSearch = [];
          this.totalRecords = 0;
        } else if (searchParams) {
          this.searchParams = searchParams;
         // this.totalRecords = 0;
        //  this.getFundingSearchData();
        this.searchParams.client_id = this.searchParams.client_id ? this.searchParams.client_id : null;
        this.searchParams.client_name = this.searchParams.client_name ? this.searchParams.client_name : null;
        this.searchParams.ssn = this.searchParams.ssn ? this.searchParams.ssn : null;
        this.searchParams.dobdaterangefrom = this.searchParams.dobdaterangefrom ? this.searchParams.dobdaterangefrom : null;
        this.searchParams.dobdaterangeto = this.searchParams.dobdaterangeto ? this.searchParams.dobdaterangeto : null;
        this.searchParams.local_dept = this.searchParams.local_dept ? this.searchParams.local_dept : null;
        this.searchParams.caseworker_name = this.searchParams.caseworker_name ? this.searchParams.caseworker_name : null;
        this._commonHttpService.getPagedArrayList({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        where: this.searchParams,
        method: 'post'
        }, FinanceUrlConfig.EndPoint.accountsPayable.history.searchFundingAllocation).subscribe((res: any) => {
            if (res) {
                this.fundingSearch = res.data;
                this.totalRecords = (this.fundingSearch && this.fundingSearch.length > 0) ? this.fundingSearch[0].totalcount : 0;
            }
        });
        }
  }

  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getFundingSearchData(page);
  }

  fundingDetails(fund) {
    this._datastoreService.setData(FinanceFundingSource.SelectedFundingSource, fund);
    this._router.navigate(['../details'], { relativeTo: this._route });
  }

  toggleSsn = () => {
    this.isSsnHidden = !this.isSsnHidden;
    if (this.isSsnHidden) {
      this.ssnEye = 'fa-eye';
      this.showSsnMask = true;
    } else {
      this.ssnEye = 'fa-eye-slash';
      this.showSsnMask = false;
    }
  }
}
