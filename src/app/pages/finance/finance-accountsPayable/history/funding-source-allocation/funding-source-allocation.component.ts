import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../@core/services';
import { FinanceFundingSource } from '../../../finance.constants';

@Component({
  selector: 'funding-source-allocation',
  templateUrl: './funding-source-allocation.component.html',
  styleUrls: ['./funding-source-allocation.component.scss']
})
export class FundingSourceAllocationComponent implements OnInit {

    constructor( private _dataStoreService: DataStoreService) {
    }

   ngOnInit() {
     this._dataStoreService.setData(FinanceFundingSource.FundingSourceSearchParams, null);
   }

}
