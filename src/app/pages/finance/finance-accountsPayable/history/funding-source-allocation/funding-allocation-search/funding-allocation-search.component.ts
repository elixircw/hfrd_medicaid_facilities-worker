import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonHttpService,DataStoreService } from '../../../../../../@core/services';
import { FinanceStoreConstants, FinanceFundingSource } from '../../../../finance.constants';
import { PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../../../finance.url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'funding-allocation-search',
  templateUrl: './funding-allocation-search.component.html',
  styleUrls: ['./funding-allocation-search.component.scss']
})
export class FundingAllocationSearchComponent implements OnInit {
  fundingSearchForm: FormGroup;
  @Output() searchFundingEmitter = new EventEmitter<any>();
  localDepartmentList:any[];
  constructor(
    private _dataStoreService: DataStoreService,
    private formBuilder: FormBuilder,
    private _commonService: CommonHttpService
  ) { }

  ngOnInit() {
    this._dataStoreService.setData(FinanceFundingSource.FundingSourceSearchParams, null);
    this.fundingSearchForm = this.formBuilder.group({
      client_id: [null],
      client_name: [null],
      dobdaterangefrom: [null],
      dobdaterangeto: [null],
      ssn: [null],
      caseworker_name: [null],
      local_dept: [null]
     // ayear: ['Y']
    });
    setTimeout(() => {
      const searchParams = this._dataStoreService.getData(FinanceFundingSource.FundingSourceSearchParams);
      if (searchParams) {
          this.fundingSearchForm.patchValue(searchParams);
      }
  }, 100);
  this.populateLocalDropDownType();
  }

  searchClient() {
    const searchParams = this.fundingSearchForm.getRawValue();
    this._dataStoreService.setData(FinanceFundingSource.FundingSourceSearchParams, searchParams);
    this.searchFundingEmitter.emit();
  }


  clearSearch() {
    this.fundingSearchForm.reset();
    this._dataStoreService.setData(FinanceFundingSource.FundingSourceSearchParams, null);
    this.searchFundingEmitter.emit();
  }

  populateLocalDropDownType(){
    this.localDepartmentList = [];
    this._commonService.getArrayList(new PaginationRequest({
        where: {
          'picklist_type_id': '104'
        },
        nolimit: true,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.childAccounts.pickListUrl).subscribe((result) => {
        return result.filter((res) => {
          this.localDepartmentList.push(res);
    });
    });
  }

}
