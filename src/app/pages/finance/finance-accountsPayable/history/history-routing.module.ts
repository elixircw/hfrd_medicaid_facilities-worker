import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryComponent } from './history.component';
import { PaymentHistoryReportComponent } from './payment-history-report/payment-history-report.component';

const routes: Routes = [
  {
    path: '',
    component: HistoryComponent,
    children: [
      {
        path: 'funding-source-allocation',
        loadChildren: './funding-source-allocation/funding-source-allocation.module#FundingSourceAllocationModule'
      },
      {
        path: 'client-payment',
        loadChildren: './client-payment/client-payment.module#ClientPaymentModule'
      },
      {
        path: 'payment-report',
        component: PaymentHistoryReportComponent
      },
      {
        path: '**',
        redirectTo: 'search'
      }
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
