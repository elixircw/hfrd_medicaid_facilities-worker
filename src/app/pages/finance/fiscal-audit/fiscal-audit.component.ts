import { Component, OnInit } from '@angular/core';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'fiscal-audit',
  templateUrl: './fiscal-audit.component.html',
  styleUrls: ['./fiscal-audit.component.scss']
})
export class FiscalAuditComponent implements OnInit {
  changehistory: any[];
  adjustment: any[];

  constructor(
    private service: FinanceService,
    ) { }

  ngOnInit() {
    this.changehistory = this.service.changehistory;
    this.adjustment = this.service.adjustment;
  }

}
