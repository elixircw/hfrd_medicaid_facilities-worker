import { Component, OnInit, AfterContentInit } from '@angular/core';
import { CommonHttpService, AlertService, DataStoreService } from '../../../@core/services';
import { PaginationRequest } from '../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../finance.url.config';
import { CommingledAccount, UserCounty } from '../finance.constants';

@Component({
  selector: 'commingled-accounts',
  templateUrl: './commingled-accounts.component.html',
  styleUrls: ['./commingled-accounts.component.scss'],
})
export class CommingledAccountsComponent implements OnInit {
  selectedDept: any;
  localDept: any[];
  selectedAccount: any;
  deptSelected: any;
  disableCounty: boolean;
  constructor(
    private _commonService: CommonHttpService,
    private _datastoreService: DataStoreService
  ) { }

  ngOnInit() {
    this.getDropdown();
  }

  getDropdown() {
    this._commonService.getArrayList({ method: 'get', where : {}}, FinanceUrlConfig.EndPoint.general.userCounty + '?filter').subscribe((result) => {
      if (result !== null) {
          this.localDept = result;
          this.selectedDept = result[0].statecountycode;
          this.disableCounty = true;
          this.getchilddetailslist();
      }
    });
  }

  getchilddetailslist() {
    if (this.selectedDept) {
      this._datastoreService.setData('selectedFinanceDept', this.selectedDept);
    }
  }

}
