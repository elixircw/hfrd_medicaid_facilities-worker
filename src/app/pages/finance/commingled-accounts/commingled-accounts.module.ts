import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import {
  MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule,
  MatSelectModule, MatButtonModule, MatRadioModule, MatTabsModule, MatTooltipModule,
  MatCheckboxModule, MatListModule, MatCardModule, MatTableModule, MatExpansionModule,
  MatChipsModule, MatIconModule, MatAutocompleteModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule, TimepickerModule } from 'ngx-bootstrap';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SortTableModule } from '../../../shared/modules/sortable-table/sortable-table.module'; 
import { CommingledAccountsComponent } from './commingled-accounts.component';
import { CommingledAccountsRoutingModule } from './commingled-accounts-routing.module';
import { InterestTransactionsComponent } from './interest-transactions/interest-transactions.component';
import { CommingledAccountsTabComponent } from './commingled-accounts/commingled-accounts.component';

@NgModule({
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatChipsModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TimepickerModule,
    ControlMessagesModule,
    SharedDirectivesModule,
    SharedPipesModule,
    NgSelectModule,
    ImageCropperModule,
    SortTableModule,
    MatAutocompleteModule,
    CommingledAccountsRoutingModule
  ],
  declarations: [CommingledAccountsComponent, InterestTransactionsComponent, CommingledAccountsTabComponent]
})
export class CommingledAccountsModule { }
