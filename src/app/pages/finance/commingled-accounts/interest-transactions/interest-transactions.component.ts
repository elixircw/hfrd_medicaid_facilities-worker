import { Component, OnInit, OnDestroy, AfterContentInit } from '@angular/core';
import { FinanceUrlConfig } from '../../finance.url.config';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, GenericService, DataStoreService, AuthService, CommonDropdownsService, SessionStorageService } from '../../../../@core/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { AddForm } from '../../_entities/finance-entity.module';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { CommingledAccount } from '../../finance.constants';
import * as _ from 'lodash';

@Component({
  selector: 'interest-transactions',
  templateUrl: './interest-transactions.component.html',
  styleUrls: ['./interest-transactions.component.scss'],
  providers: [DatePipe]
})
export class InterestTransactionsComponent implements OnInit, OnDestroy, AfterContentInit {

  id: string;
  commingledAccountLists: any[];
  selectedCommingledAccount: any;
  getPagedArrayList$: Observable<any[]>;
  childtransactionslist: any[] = [];
  // transactionIdArray: any[] = [];
  childdetails: any;
  selectedTransaction: any;
  formAdd: AddForm;
  addForm: FormGroup;
  editTransactionForm: FormGroup;
  childId = '';
  transactionExistOptions = [
    { text: 'Yes', value: 'Y' },
    { text: 'No', value: 'N' },
  ];
  transactionType: any[];
  transactionStatus: any[];
  localDept: any[];
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalRecords: number;
  selectedDept: any;
  loggedInUser: string;
  currentDate: Date = new Date();
  isView: boolean;
  isEdit: boolean;
  datastoreSubscription: any;
  selectedPerson: any;
  assignedTo: any;
  getUsersList: any[];
  originalUserList: any[];
  user: AppUser;
  isFinanceSupervisor: boolean;
  userId: string;
  requestUser: any;
  disableTrans: boolean;
  account_status_nm: string;
  minDate: Date;
  reason_tx: string;
  commAccId: any;
  selectedAccount: any;
  errorCorrection: any;
  accountNo: any;
  errorData: any[];
  accountDetails: any;
  maxDate: Date;
  transactionID: any;
  commingledTrans: any[];
  isApprove: boolean;
  transaction: any;
  activeModule: any;
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _ativityService: GenericService<AddForm>,
    private formBuilder: FormBuilder,
    private _datastoreService: DataStoreService,
    private datePipe: DatePipe,
    private _authService: AuthService,
    private _commonDropDownService: CommonDropdownsService,
    private _sessionStorage: SessionStorageService) { }

  ngOnInit() {
    this.loggedInUser = this._authService.getCurrentUser().user.userprofile.displayname;
    this.user = this._authService.getCurrentUser();
    this.userId = this._authService.getCurrentUser().user.securityusersid;
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    if (this.activeModule === 'Finance') {
      this.isFinanceSupervisor = false;
    } else if (this.activeModule === 'Finance Approval') {
      this.isFinanceSupervisor = true;
    }
    this.selectedCommingledAccount = null;
    this.initTransactionForm();
    this.datastoreSubscription = this._datastoreService.currentStore.subscribe(store => {
      const selectedDept = store['selectedFinanceDept'];
      this.selectedAccount = store['CommingledErrorCorrection'];
      if (selectedDept && selectedDept !== this.selectedDept) {
        if (this.selectedAccount) {
          this.selectedCommingledAccount = this.selectedAccount;
        } else {
          this.selectedCommingledAccount = null;
        }
        this.selectedDept = selectedDept;
        this.errorCorrection = store[CommingledAccount.ErrorCorrection];
        this.getCommingledDropdown();
      }
    });
  }

  ngAfterContentInit() {
    if (this.selectedAccount) {
      this.getTransactions(this.selectedAccount);
      if (this.isFinanceSupervisor) {
        this.editTransaction(this.selectedAccount);
      } else {
        this.viewTransaction(this.selectedAccount);
      }
      this._datastoreService.setData('CommingledErrorCorrection', null);
    } else if (this.errorCorrection) {
      this.accountDetails = this.errorCorrection;
      this.getCommingledDropdown();
      this._datastoreService.setData(CommingledAccount.ErrorCorrection, null);
    } else {
      this.getTransactions();
    }
  }

  ngOnDestroy() {
    this.datastoreSubscription.unsubscribe();
  }

  addChildTransaction(ativity: AddForm) {
    this.formAdd = Object.assign(ativity);
    this.formAdd.comm_account_id = this.selectedCommingledAccount.comm_account_id;
    this.formAdd.interest_start_dt = this.datePipe.transform(this.formAdd.interest_start_dt, 'yyyy-MM-dd');
    this.formAdd.interest_end_dt = this.datePipe.transform(this.formAdd.interest_end_dt, 'yyyy-MM-dd');

    if (this.isEdit) {
      /* const balance = +this.formAdd.interest_amount_no;
      const amount_no = +this.formAdd.mod_interest_amount_no;
       if (amount_no > balance) {
        this._alertService.error('Request amount should not exceed the interest amount');
        return false;
      } */

      if (!this.isFinanceSupervisor) {
        this.selectedTransaction.interest_amount_no = ativity.interest_amount_no;
        this.selectedTransaction.mod_interest_amount_no = ativity.mod_interest_amount_no;
        this.getRoutingUser();
      }
      // const request = {
      //   assignedtoid: '',
      //   intakeserviceid: '',
      //   interest_amount_no: ''
      // };
      // this._ativityService.getSingle(this.formAdd, FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.update + this.selectedTransaction.comm_acct_trans_id)
      //   .subscribe(res => {
      //     this.closePopupAndLoadTransactions();
      //   });
    } else {
      this._ativityService.endpointUrl = FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.add;
      this._ativityService.create(this.formAdd).subscribe(
        (res: any) => {
          if (res) {
            if (!res.isexists) {
              this.closePopupAndLoadTransactions();
            } else {
              this._alertService.error('Already added for the selected month');
            }
          }
        },
        (error) => console.log(error)
      );
    }
  }

  deleteInterestTransaction() {
    this._ativityService.getSingle({
      method: 'post'
    }, FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.delete + this.selectedTransaction.comm_acct_trans_id)
      .subscribe(res => {
        this._alertService.success('Interest Transaction deleted successfuly.');
        this.getTransactions();
        this.getCommingledDropdown();
        (<any>$('#edit-transaction')).modal('hide');
      });
  }

  private closePopupAndLoadTransactions() {
    (<any>$('#addNewTransaction')).modal('hide');
    this.getTransactions();
    this.getCommingledDropdown();
    this._alertService.success(`Transaction ${this.isEdit ? 'updated' : 'added'} successfully`);
  }

  getTransactions(account: any = null) {
    this.selectedCommingledAccount = account ? account : this.selectedCommingledAccount;
    console.log(this.selectedCommingledAccount);
    if (this.selectedCommingledAccount) {
        if (this.selectedCommingledAccount.close_dt) {
        this.disableTrans = true;
      } else {
        this.disableTrans = false;
      }
      if (this.selectedCommingledAccount.close_dt) {
        this.account_status_nm = 'Closed';
      } else {
        this.account_status_nm = 'Active';
      }
    } else {
      this.disableTrans = true;
    }
    console.log(this.disableTrans);
    if (this.selectedCommingledAccount) {
      this._ativityService.getPagedArrayList({
        where: {
          comm_account_id: this.selectedCommingledAccount.comm_account_id
        },
        method: 'get'
      }, FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.list
      ).subscribe((res: any) => {
        if (res && res.data) {
          this.childtransactionslist = res.data;
          for (let i = 0; i < this.childtransactionslist.length; i++) {
            this.childtransactionslist[i].interest_start_dt = this._commonDropDownService.getValidDate(this.childtransactionslist[i].interest_start_dt);
            this.childtransactionslist[i].interest_end_dt = this._commonDropDownService.getValidDate(this.childtransactionslist[i].interest_end_dt);
          }
          if (this.transactionID) {
            this.commingledTrans = this.childtransactionslist.filter( response => {
              return (response.comm_acct_trans_id === this.transactionID);
            });
            this.accountDetails = [];
            if (this.isFinanceSupervisor) {
              this.approveModal(this.commingledTrans[0]);
            } else {
              this.viewTransaction(this.commingledTrans[0]);
            }
            this.transactionID = null;
          }
          this.totalRecords = (this.childtransactionslist.length > 0) ? this.childtransactionslist[0].totalcount : 0;
        }
      });
    }
  }

  initTransactionForm() {
    this.addForm = this.formBuilder.group({
      interest_start_dt: [{ value: '' }, Validators.required],
      interest_end_dt: [{ value: '' }, Validators.required],
      enteredby: [{ value: '', disabled: true }],
      interest_amount_no: [{ value: '' }, Validators.required],
      notes_tx: [{ value: '' }, Validators.required],
      mod_interest_amount_no: ['']
    });
  }

  onChangeDate(form) {
    this.minDate = new Date(form.value.interest_start_dt);
    form.get('interest_end_dt').reset();
  }

  viewTransaction(transaction) {
    this.selectedTransaction = transaction;
    this.addForm.patchValue(this.selectedTransaction);
    this.addForm.disable();
    this.isView = true;
    this.isEdit = false;
    this.isApprove = false;
    (<any>$('#addNewTransaction')).modal('show');
  }


  editTransaction(transaction) {
    this.minDate = new Date(transaction.interest_start_dt);
    this.selectedTransaction = transaction;
    this.addForm.patchValue(this.selectedTransaction);
    if (!this.isFinanceSupervisor) {
      this.addForm.patchValue({
        mod_interest_amount_no: ''
      });
    }
    this.addForm.disable();
    // this.addForm.get('interest_start_dt').disable();
    // this.addForm.get('enteredby').disable();
    this.addForm.get('mod_interest_amount_no').enable();
    this.isView = false;
    this.isEdit = true;
    this.isApprove = false;
    (<any>$('#addNewTransaction')).modal('show');
  }

  approveModal(transaction: any) {
    this.transaction = transaction;
    this.addForm.patchValue(this.transaction);
    this.addForm.disable();
    this.isView = false;
    this.isEdit = false;
    if (this.transaction.statuskey === 45) {
      this.isApprove = true;
    } else {
      this.isApprove = false;
      this.isView = true;
    }
    (<any>$('#addNewTransaction')).modal('show');
  }

  approveEditTransaction() {
    const transaction = this.transaction;
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.approveUpdate + transaction.comm_acct_trans_id;
    const modal = {
      'fromsecurityusersid': transaction.fromsecurityusersid,
      'intakeserviceid': transaction.intakeserviceid
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        (<any>$('#addNewTransaction')).modal('hide');
        this._alertService.success('Requested Transaction Approved successfully!');
        this.getTransactions();
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  rejectEditTransaction(transaction) {
    this.commAccId = transaction.comm_acct_trans_id;
    this.reason_tx = '';
    (<any>$('#reject-approval')).modal('show');
  }

  rejectTransaction() {
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.rejectUpdate + this.commAccId;
    const modal = {
      reason_tx: this.reason_tx
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        if (response) {
          this._alertService.success('Commingled Updated Transaction request has been Denied!');
          (<any>$('#reject-approval')).modal('hide');
          this.getTransactions();
        } else {
          this._alertService.success('Commingled Updated Transaction request has been Denied!');
          (<any>$('#reject-approval')).modal('hide');
          this.getTransactions();
        }
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  deleteTransaction(transaction) {
    this.selectedTransaction = transaction;
    (<any>$('#edit-transaction')).modal('show');
  }

  openAddModal() {
    this.addForm.reset();
    this.addForm.patchValue({
      interest_start_dt: this.currentDate,
      enteredby: this.loggedInUser
    });
    this.addForm.enable();
    // this.addForm.get('interest_start_dt').disable();
    this.addForm.get('enteredby').disable();
    this.isView = false;
    this.isEdit = false;
    this.isApprove = false;
    this.minDate = new Date(this.addForm.value.interest_start_dt);
    const today = new Date();
    this.maxDate = new Date();
    // this.maxDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    (<any>$('#addNewTransaction')).modal('show');
  }

  clearForm() {
    this.addForm.get('mod_interest_amount_no').reset();
  }

  getCommingledDropdown(page = 1) {
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: { localdepartment: this.selectedDept },
      page: page,
      limit: null,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.commingledAccounts.getCommingledAccountUrl + '?filter').subscribe(result => {
      if (!this.selectedCommingledAccount) {
        this.commingledAccountLists = result['data'];
        if (this.accountDetails) {
          this.transactionID = this.accountDetails.comm_acct_trans_id;
          for (let i = 0; i < this.commingledAccountLists.length; i++) {
            if (this.commingledAccountLists[i].comm_account_id === this.accountDetails.comm_account_id) {
              this.accountNo = this.commingledAccountLists[i];
              this.getTransactions(this.commingledAccountLists[i]);
            //  this.approveEditTransaction(this.commingledAccountLists[i]);
              // this.accountDetails = [];
            }
          }
        }
      } else if (this.selectedCommingledAccount) {
        if (this.accountDetails) {
          this.transactionID = this.accountDetails.comm_acct_trans_id;
            if (this.selectedCommingledAccount.comm_account_id === this.accountDetails.comm_account_id) {
              this.accountNo = this.selectedCommingledAccount;
              this.getTransactions(this.selectedCommingledAccount);
              // this.accountDetails = [];
            }
        } else {
          this.selectedCommingledAccount = (result['data'] && result['data'].length) ? result['data'][0] : null;
        }
      }
    });
  }
  /* getCommingledDropdown() {
    const where = this.selectedCommingledAccount ? {
      account_no: this.selectedCommingledAccount.account_no
    } : {
        county_cd: this.selectedDept
      };
    this._commonService.getArrayList(new PaginationRequest({
      where: where,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.commingledDropdownList).subscribe(result => {
      if (!this.selectedCommingledAccount) {
        this.commingledAccountLists = result;
        if (this.accountDetails) {
          this.transactionID = this.accountDetails.comm_acct_trans_id;
          for (let i = 0; i < this.commingledAccountLists.length; i++) {
            if (this.commingledAccountLists[i].comm_account_id === this.accountDetails.comm_account_id) {
              this.accountNo = this.commingledAccountLists[i];
              this.getTransactions(this.commingledAccountLists[i]);
            //  this.approveEditTransaction(this.commingledAccountLists[i]);
              // this.accountDetails = [];
            }
          }
        }
      } else if (this.selectedCommingledAccount) {
        if (this.accountDetails) {
          this.transactionID = this.accountDetails.comm_acct_trans_id;
            if (this.selectedCommingledAccount.comm_account_id === this.accountDetails.comm_account_id) {
              this.accountNo = this.selectedCommingledAccount;
              this.getTransactions(this.selectedCommingledAccount);
              // this.accountDetails = [];
            }
        } else {
          this.selectedCommingledAccount = (result && result.length) ? result[0] : null;
        }
      }
    });
  } */

  selectPerson(row) {
    if (row) {
      this.selectedPerson = row;
      this.assignedTo = row.userid;
    }
  }

  getRoutingUser() {
    this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PCAUTH' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe((result) => {
        (<any>$('#edit-transaction')).modal('show');
        (<any>$('#addNewTransaction')).modal('hide');
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
  }

  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter((res) => {
        if (res.userrole === 'LDSS Fiscal Supervisor') {
          return res;
        }
      });
    }
  }

  private assignUser() {

    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.commingledAccounts.interestTransactions.update + this.selectedTransaction.comm_acct_trans_id;
    const modal = {
      'interest_amount_no': this.selectedTransaction.interest_amount_no,
      'mod_interest_amount_no': this.selectedTransaction.mod_interest_amount_no,
      'assignedtoid': this.assignedTo,
      'intakeserviceid': this.selectedTransaction.intakeserviceid
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        this._alertService.success('Edit Approval sent successfully!');
        (<any>$('#edit-transaction')).modal('hide');
        this.getTransactions();
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );

  }

  private deleteSupervisorApproval(transaction) {

    this.requestUser = transaction.fromsecurityusersid;
    this._commonService.endpointUrl = 'Tb_comm_acct_transactions/deleteCommAccountTransaction' + '/' + transaction.comm_acct_trans_id;
    const modal = {
      // 'comm_acct_trans_id' : this.selectedTransaction.comm_acct_trans_id,
      'fromsecurityusersid': this.requestUser,
      method: 'post'
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        this._alertService.success('Approved successfully!');
        this.getTransactions();
        this.getCommingledDropdown();
        // (<any>$('#edit-transaction')).modal('hide');
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  calculatePerDiem(paymentAmount) {
    // const amount = +paymentAmount.target.value;
    // const tempPaymentAmount = +amount.toFixed(2);

    // this.addForm.patchValue({
    //   payment_amount_no: tempPaymentAmount ? tempPaymentAmount : 0.00
    // });
    if (Number.isInteger(+paymentAmount.target.value)) {
      this.addForm.patchValue ({
        mod_interest_amount_no : paymentAmount.target.value + '.00'
      });
    }
  }

  checkDec(el) {
    if (el.target.value !== '') {
      return el.target.value = isNaN(el.target.value) ? '' : el.target.value.replace(/[^0-9.]{0,2}/g, '');
    }
    return '';
  }

  interestAmountChange(amount) {
    if (amount) {
      this.addForm.patchValue({
        interest_amount_no: _.toNumber(amount).toFixed(2)
    });
    }
  }
}
