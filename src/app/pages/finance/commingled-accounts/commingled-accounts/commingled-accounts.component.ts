import { Component, OnInit, OnDestroy } from '@angular/core';
import { FinanceUrlConfig } from '../../finance.url.config';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, DataStoreService, CommonDropdownsService } from '../../../../@core/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import * as moment from 'moment';


@Component({
  selector: 'commingled-accounts-tab',
  templateUrl: './commingled-accounts.component.html',
  styleUrls: ['./commingled-accounts.component.scss'],
  providers: [DatePipe]
})
export class CommingledAccountsTabComponent implements OnInit, OnDestroy {
  getPagedArrayList$: Observable<any[]>;
  commingledAccountslist: any[] = [];
  childdetails: any;
  selectedAccount: any;
  addCommingledForm: FormGroup;
  editAccountForm: FormGroup;
  selectedDept: any;
  accountExistOptions = [
    { text: 'Yes', value: 'Y' },
    { text: 'No', value: 'N' },
  ];
  accountType: any[];
  accountStatus: any[];
  pageNo: any;
  localDept: any[];
  county_nm: any[];
  approval_status_cd : any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalRecords: any;
  datastoreSubscription: any;
  childReportForm: FormGroup;
  clientAccountId: any;
  minDate: Date;
  maxOpenDate: Date;
  showBusy: boolean;
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private _datastoreService: DataStoreService,
    private _commonDDservice: CommonDropdownsService) { }

  ngOnInit() {
    this.maxOpenDate = new Date();
    this.initAccountForm();
    this.initEditAccountForm();
    this.getAccountStatus();
    this.getDropdown();
    this.childReport();
    // this.getchilddetailslist();
    this.datastoreSubscription = this._datastoreService.currentStore.subscribe(store => {
      const selectedDept = store['selectedFinanceDept'];
      if (selectedDept && selectedDept !== this.selectedDept) {
        this.selectedDept = selectedDept;
        this.getcommingledAccountslist(1);
      }
    });
  }

  ngOnDestroy() {
    this.datastoreSubscription.unsubscribe();
  }

  initAccountForm() {
    this.addCommingledForm = this.formBuilder.group({
      open_dt: [{ value: ''}],
      close_dt: [{ value: '', disabled: true}],
      bank_nm: ['', Validators.required],
      // status_cd: ['', Validators.required],
      status_cd: [{disabled: true}, Validators.required],
      local_dept: [{ value: '', disabled: true }, Validators.required],
      bank_acc_num: ['', Validators.required],
      // total_balance: ['', Validators.required],
      entered_by: [''],
      interest_amount_no: [''],
      total_balance_no: ['']
    });
  }

  initEditAccountForm() {
    this.editAccountForm = this.formBuilder.group({
      open_dt: [{ value: '', disabled: true}],
      close_dt: [''],
      bank_nm: ['', Validators.required],
      approval_status_cd: ['', Validators.required],
      county_cd: [{ value: '', disabled: true }, Validators.required],
      account_no: ['', Validators.required],
      // total_balance_no:['', Validators.required]
    });
  }

  childReport() {
    this.childReportForm = this.formBuilder.group({
      date_sw: ['Y'],
      date_from: [null],
      date_to: [null]
    });
  }

  onChangeDate(form) {
    this.minDate = new Date(form.value.open_dt);
    form.get('close_dt').reset();
  }

  addCommingledAccount() {
    if (this.addCommingledForm.valid) {
      const data = this.addCommingledForm.getRawValue();

      if ( this.addCommingledForm.getRawValue().open_dt && this.addCommingledForm.getRawValue().close_dt ) {
        const openDate = moment(new Date(this.addCommingledForm.getRawValue().open_dt)).format('YYYY-MM-DD');
        const closeDate = moment(new Date(this.addCommingledForm.getRawValue().close_dt)).format('YYYY-MM-DD');
        if ( new Date(openDate).getTime() > new Date(closeDate).getTime()) {
          this._alertService.error('Close Date should be greater than or equal to Open Date');
          return false;
        }
      }

      this._commonService.create({

        'openDt': (data.open_dt) ? data.open_dt : null,
        'closeDt': (this.addCommingledForm.value.close_dt) ? this.datePipe.transform(this.addCommingledForm.value.close_dt, 'yyyy-MM-dd') : null,
        'bankNm': data.bank_nm,
        'accountNo': data.bank_acc_num,
        'approvalStatusCd': data.status_cd,
        'countyCd': data.local_dept,
        'totalBalanceNo': data.total_balance,
        'deleteSw': 'N'
      }
        , FinanceUrlConfig.EndPoint.commingledAccounts.addCommingledAccountUrl).subscribe(result => {
          if (result) {
            if (result['UserToken'].isaccountexists) {
              this._alertService.error('Commingled Account Bank details already exist');
            } else {
              this._alertService.success('Commingled Account added successfully');
              this.getcommingledAccountslist(this.pageChanged);
            }
            (<any>$('#addNewAccount')).modal('hide');
          } else {
            this._alertService.error('Commingled Account not added successfully');
          }
        });
    } else {
      this._alertService.warn('Please fill the mandatory details.');
    }
  }

  UpdateCase() {
    if (this.editAccountForm.valid) {
      const data = this.editAccountForm.getRawValue();

      if ( this.editAccountForm.getRawValue().open_dt && this.editAccountForm.getRawValue().close_dt) {
        const openDate = moment(new Date(this.editAccountForm.getRawValue().open_dt)).format('YYYY-MM-DD');
        const closeDate = moment(new Date(this.editAccountForm.getRawValue().close_dt)).format('YYYY-MM-DD');
        if ( new Date(openDate).getTime() > new Date(closeDate).getTime()) {
          this._alertService.error('Close Date should be greater than or equal to Open Date');
          return false;
        }
      }

      this._commonService.update('',
        {
          'commAccountId': this.selectedAccount.comm_account_id,
          'account_type_cd': data.account_type_cd,
          'bankNm': data.bank_nm,
          'accountNo': data.account_no,
          'totalBalanceNo': data.total_balance_no,
          'deleteSw': 'N',
          'openDt': (data.open_dt) ? moment(new Date(data.open_dt)).format('MM/DD/YYYY') : null,
          'closeDt': (data.close_dt) ?  moment(new Date(data.close_dt)).format('MM/DD/YYYY') : null,
          'approvalStatusCd': data.approval_status_cd,
          'countyCd': data.county_cd
        }
        , FinanceUrlConfig.EndPoint.commingledAccounts.updateAccountUrl).subscribe(result => {
          if (result) {
            if (result['UserToken'].isaccountexists) {
              this._alertService.error('Commingled Account Bank details already exist');
            } else {
              if (result['UserToken'].ischildexist === false) {
                this._alertService.success('Commingled Account updated successfully');
                this.getcommingledAccountslist(this.pageNo);
                (<any>$('#editAccount')).modal('hide');
              } else {
                this._alertService.error('A commingled account cannot be closed if any associated child account is active or if the total balance is greater than zero.');
              }
            }
          }
        });
    } else {
      this._alertService.warn('Please fill the mandatory details.');
    }
  }

  getcommingledAccountslist(page) {
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: { localdepartment: this.selectedDept },
      page: page,
      limit: 10,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.commingledAccounts.getCommingledAccountUrl + '?filter').subscribe(result => {
      this.commingledAccountslist = result['data'];
      this.totalRecords = result['count'];
    });

  }

  pageChanged(page) {
      this.pageNo = page;
    this.getcommingledAccountslist(page);
  }

  // getchilddetailslist() {
  //   if (this.selectedDept)
  //     this.getcommingledAccountslist();
  // }

  getAccountType() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '40'
      },
      nolimit: true,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.commingledAccounts.pickListUrl).subscribe(result => {
      this.accountType = result;
    });
  }

  getAccountStatus() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '365'
      },
      nolimit: true,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.commingledAccounts.pickListUrl).subscribe(result => {
      this.accountStatus = result;
    });
  }


  getDropdown() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '104'
      },
      nolimit: true,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.commingledAccounts.pickListUrl).subscribe(result => {
      this.localDept = result;
    });
  }

  viewAccount(account) {
    this.selectedAccount = account;
  }

  editAccount(account) {
    this.getAccountStatus();
    this.minDate = new Date(account.open_dt);
    this.selectedAccount = account;
    this.editAccountForm.patchValue(account);
    if (account.close_dt) {
        this.editAccountForm.patchValue({
          approval_status_cd: '3565'
        });
        this.approval_status_cd = 'Closed';
        account.approval_status_cd = '3565';
    } else {
        this.editAccountForm.patchValue({
          approval_status_cd: '3564'
        });
        this.approval_status_cd =  'Active';
        account.approval_status_cd = '3564';
    }

    // this.editAccountForm.get('approval_status_cd').enable();
    // this.editAccountForm.get('close_dt').enable();
    // if ( account.approval_status_cd === '593' && account.close_dt && account.open_dt) {
    //   this.editAccountForm.get('approval_status_cd').disable();
    //   this.editAccountForm.get('close_dt').disable();
    // } else if ( account.approval_status_cd === '592' ) {
    //   this.editAccountForm.get('close_dt').setValue(null);
    //   this.editAccountForm.get('close_dt').disable();
    // }
    this.editAccountForm.disable();
    this.editAccountForm.get('approval_status_cd').enable();
    this.editAccountForm.get('county_cd').disable();
    if (account.close_dt) {
      this.editAccountForm.get('close_dt').disable();
    }
  }

  statusChange(id, mode) {
    const formNm = (mode === 'add') ? this.addCommingledForm : this.editAccountForm;
    formNm.get('close_dt').enable();
    if (id === '592') {
      formNm.get('close_dt').setValue(null);
      formNm.get('close_dt').disable();
    }
  }

  deleteAccount(account) {
    this.selectedAccount = account;
  }


  deleteItem() {
    this._commonService.create(
      {
        'commAccountId': this.selectedAccount.comm_account_id
      },
      FinanceUrlConfig.EndPoint.commingledAccounts.deleteCommingledAccount).subscribe(
        res => {
          this._alertService.success('Account deleted successfully');
          (<any>$('#deleteAccount')).modal('hide');
          this.getcommingledAccountslist(1);
        },
        err => { }
      );
  }


  openAddModal() {
    if (this.selectedDept) {
      this.getAccountStatus();
      this.addCommingledForm.reset();
      this.addCommingledForm.patchValue({
        open_dt: new Date(),
        local_dept: this.selectedDept.trim(),
        status_cd: '3564'
      });
      // this.addCommingledForm.get('status_cd').patchValue ({
      //   picklist_value_cd: '592'
      // });
      // console.log('test dropdown...', picklist_value_cd);
      this.addCommingledForm.get('status_cd').disable();
      (<any>$('#addNewAccount')).modal('show');
    } else {
      this._alertService.warn('Please select the Department.');
    }
  }

  accountTypeSelected(event) {
    if (event.value === '591') {
      this.addCommingledForm.get('bank_nm').setValidators([Validators.required]);
      this.addCommingledForm.get('bank_acc_num').setValidators([Validators.required]);
      this.addCommingledForm.get('bank_nm').updateValueAndValidity();
      this.addCommingledForm.get('bank_acc_num').updateValueAndValidity();

      // this.accountForm.get('bank_nm').enable();
      // this.accountForm.get('bank_acc_num').enable();
    } else if (event.value === '590') {
      this.addCommingledForm.get('bank_nm').clearValidators();
      this.addCommingledForm.get('bank_acc_num').clearValidators();
      this.addCommingledForm.get('bank_nm').updateValueAndValidity();
      this.addCommingledForm.get('bank_acc_num').updateValueAndValidity();
      // this.accountForm.get('bank_nm').disable();
      // this.accountForm.get('bank_acc_num').disable();
    }
  }

  documentPopup(comm_account_id) {
    this.showBusy = false;
    this.clientAccountId = comm_account_id;
  }

  clearchildForm() {
    this.childReportForm.reset();
    this.childReportForm.patchValue({
      date_sw: 'M'
  });
  }

  documentGenerate(childReport, type) {
    this.showBusy = true;
    const modal = {
      count: -1,
      where: {
        documenttemplatekey: ['child117report'],
        commaccountid: this.clientAccountId,
        date_sw: childReport.date_sw,
        date_from: childReport.date_from,
        date_to: childReport.date_to,
        format: type
      },
      method: 'post'
    };
      this._commonService.download('evaluationdocument/generateintakedocument', modal)
        .subscribe(res => {
          this.showBusy = false;
          const blob = new Blob([new Uint8Array(res)]);
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          if (type === 'pdf') {
            link.download = 'DHS_117_Commingled_Account_History_Report.pdf';
          } else if (type === 'excel') {
            link.download = 'DHS_117_Commingled_Account_History_Report.xlsx';
          }

          document.body.appendChild(link);

          link.click();

          document.body.removeChild(link);
          this.clearchildForm();
          (<any>$('#downloadReport')).modal('hide');
        });
  }
}
