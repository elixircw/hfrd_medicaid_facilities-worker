import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptFastEntryComponent } from './receipt-fast-entry.component';

describe('ReceiptFastEntryComponent', () => {
  let component: ReceiptFastEntryComponent;
  let fixture: ComponentFixture<ReceiptFastEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptFastEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptFastEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
