import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiptFastEntryRoutingModule } from './receipt-fast-entry-routing.module';
import { ReceiptFastEntryComponent } from './receipt-fast-entry.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatButtonModule, MatRadioModule } from '@angular/material';
import { CommonControlsModule } from '../../../shared/modules/common-controls/common-controls.module';

@NgModule({
  imports: [
    CommonModule,
    CommonControlsModule,
    ReceiptFastEntryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    A2Edatetimepicker,
    NgSelectModule,
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatButtonModule,
    MatRadioModule
  ],
  declarations: [ReceiptFastEntryComponent]
})
export class ReceiptFastEntryModule { }
