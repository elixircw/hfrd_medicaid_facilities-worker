import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProviderInfoPopupComponent } from './provider-info-popup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatButtonModule, MatRadioModule, MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatButtonModule,
    MatRadioModule,
    MatTooltipModule
  ],
  declarations: [ProviderInfoPopupComponent],
  exports: [ProviderInfoPopupComponent]
})
export class ProviderInfoPopupModule { }
