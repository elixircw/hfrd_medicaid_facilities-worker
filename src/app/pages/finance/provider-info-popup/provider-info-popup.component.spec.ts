import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderInfoPopupComponent } from './provider-info-popup.component';

describe('ProviderInfoPopupComponent', () => {
  let component: ProviderInfoPopupComponent;
  let fixture: ComponentFixture<ProviderInfoPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderInfoPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderInfoPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
