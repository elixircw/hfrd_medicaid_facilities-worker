import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../@core/services/common-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, AlertService, DataStoreService } from '../../../@core/services';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'provider-info-popup',
  templateUrl: './provider-info-popup.component.html',
  styleUrls: ['./provider-info-popup.component.scss']
})
export class ProviderInfoPopupComponent implements OnInit {
  alertTxt: string;
  headerTxt: string;
  currentvalue: any;
  userInfo: any;
  agency: any;
  checkAlertTxt: string;
  reasonwithhold: any;

  constructor(
    private _commonHttpService: CommonHttpService, private _router: Router,
    private _authService: AuthService, private _alertService: AlertService,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private service: FinanceService
  ) { }

  ngOnInit() {
    // this.getProviderDetails(this.providerID);
    this.userInfo = this._authService.getCurrentUser();
    this.agency = this.userInfo.user.userprofile.teamtypekey;
  }

  closePopup() {
    (<any>$('#confirm-withhold-payment-change')).modal('hide');
    (<any>$('#provider-info-popup')).modal('show');
  }
  changePaymentWithholding() {
    let payload = {};
    payload['provider_id'] = this.service.providerInfoDetails ? this.service.providerInfoDetails.provider_id : null;
    if (this.currentvalue === true) {
      // Reverse to the desired value
      payload['withhold_payment_sw'] = 'N';
    } else {
      payload['withhold_payment_sw'] = 'Y';
    }
    payload['withhold_reason'] = this.reasonwithhold;
    this._commonHttpService.create(
      payload,
      'tb_provider/updatepaymentwithold'
    ).subscribe(
      (response) => {
        this.triggerReleasePaymentSP();
        this._alertService.success('Payment withhold changed');
        this.reasonwithhold = null;
        (<any>$('#confirm-withhold-payment-change')).modal('hide');
        (<any>$('#provider-info-popup')).modal('show');
        this.service.getChangeLogList(this.service.providerInfoDetails ? this.service.providerInfoDetails.provider_id : null, { pageSize: 10, pageNumber: 1 });
      },
      (error) => {
        this.service.isPaymentWithhold = this.currentvalue;
        this.reasonwithhold = null;
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#confirm-withhold-payment-change')).modal('hide');
        (<any>$('#provider-info-popup')).modal('show');
      }
    );
  }
  triggerReleasePaymentSP() {
    this._commonHttpService.getArrayList(
      {
        where: { provider_id: this.service.providerInfoDetails.provider_id },
        method: 'get',
        nolimit: true
      },
      'tb_provider/updateReleasePayment?filter'
    ).subscribe();
  }

  changeEFT() {
    const modal = {
      provider_id: this.service.providerInfoDetails ? this.service.providerInfoDetails.provider_id : null,
      eft_sw: this.currentvalue ? 'N' : 'Y',
    };
    this._commonHttpService.create(
      modal,
      'tb_provider/updatepaymentwithold'
    ).subscribe(
      (response) => {
        this._alertService.success(this.checkAlertTxt);
        (<any>$('#confirm-withhold-payment-change')).modal('hide');
        (<any>$('#provider-info-popup')).modal('show');
        this.service.getChangeLogList(this.service.providerInfoDetails ? this.service.providerInfoDetails.provider_id : null, { pageSize: 10, pageNumber: 1 });
      },
      (error) => {
        this.service.electronicfundtransfer = this.currentvalue;
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#confirm-withhold-payment-change')).modal('hide');
        (<any>$('#provider-info-popup')).modal('show');
      }
    );
  }

  cancelConfirm() {
    if (this.headerTxt === 'Withhold Payment (Returned Check)') {
      this.service.isPaymentWithhold = this.currentvalue;
    } else if (this.headerTxt === 'Electronic Funds Transfer (EFT)') {
      this.service.electronicfundtransfer = this.currentvalue;
    } else if (this.headerTxt = 'Electronic Funds Transfer (EFT) Change Alert') {
      this.service.electronicfundtransfer = this.currentvalue;
      (<any>$('#confirm-withhold-payment-change')).modal('hide');
    }
    (<any>$('#provider-info-popup')).modal('show');
  }

  showConfirmPopup(index, value) {
    this.currentvalue = value;
    if (index === 1) {
      this.headerTxt = 'Withhold Payment (Returned Check)';
      this.checkAlertTxt = 'Payment Withhold updated';
      if (value === true) {
        this.alertTxt = 'Are you sure want to Release the payment? Please confirm.';
      } else {
        this.alertTxt = 'Are you sure want to Stop the payment? Please confirm.';
      }
      if (this.agency === 'FNS' && !this.service.electronicfundtransfer) {
        (<any>$('#provider-info-popup')).modal('hide');
        (<any>$('#confirm-withhold-payment-change')).modal('show');
      } else {
        this.service.isPaymentWithhold = this.currentvalue;
      }
    }
    if (index === 2) {
      this.headerTxt = 'Electronic Funds Transfer (EFT)';
      this.checkAlertTxt = 'Electronic Funds Transfer (EFT) updated';
      if (value === true) {
        this.alertTxt = 'Are you sure want to remove EFT check? Please confirm.';
      } else {
        this.alertTxt = 'Are you sure want to do EFT Check? Please confirm.';
      }
      if (this.agency === 'FNS') {
        if (this.service.isPaymentWithhold && !this.currentvalue) {
          this.headerTxt = 'Electronic Funds Transfer (EFT) Change Alert';
          this.alertTxt = 'Please release Withhold Payment (Returned Check) before changing Electronic Funds Transfer (EFT)';
          (<any>$('#provider-info-popup')).modal('hide');
          (<any>$('#confirm-withhold-payment-change')).modal('show');
        } else {
          (<any>$('#provider-info-popup')).modal('hide');
          (<any>$('#confirm-withhold-payment-change')).modal('show');
        }
      }
    }
  }

  saveConfirmPopup() {
    if (this.headerTxt === 'Withhold Payment (Returned Check)') {
      if (this.currentvalue) {
        this.changePaymentWithholding();
      } else {
        if (this.reasonwithhold) {
          this.changePaymentWithholding();
        } else {
          this._alertService.warn('Please Add reason!');
          return false;
        }
      }
    } else if (this.headerTxt === 'Electronic Funds Transfer (EFT)') {
      this.changeEFT();
    }
  }

  navigateMaintenance(index) {
    (<any>$('#provider-info-popup')).modal('hide');
    if (index === 1) {
      this._router.navigate(['/pages/finance/finance-accountsPayable']);
    } else if (index === 2) {
      this._router.navigate(['/pages/finance/finance-accountsPayable/ancillary']);
    } else if (index === 3) {
      this._router.navigate(['/pages/finance/finance-accountsReceivable']);
    }
    this._dataStoreService.setData('FinanceProviderSearch', { providerId: this.service.providerInfoDetails ? this.service.providerInfoDetails.provider_id : null, page: this.service.page });
  }

  clearCache() {
    this.headerTxt = '';
    this.alertTxt = '';
    this.currentvalue = null;
    this.service.changeLogList = [];
    this.service.providerInfoDetails = '';
  }

}
