import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceFosterCareRateRoutingModule } from './finance-foster-care-rate-routing.module';
import { FinanceFosterCareRateComponent } from './finance-foster-care-rate.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule, MatListModule, MatNativeDateModule, MatRadioModule, MatSelectModule, MatTableModule, MatTabsModule } from '@angular/material';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxMaskModule } from 'ngx-mask';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { SortTableModule } from '../../../shared/modules/sortable-table/sortable-table.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FinanceFosterCareRateRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    NgxMaskModule.forRoot(),
    NgxfUploaderModule.forRoot(),
    SortTableModule,
    SharedPipesModule
  ],
  declarations: [FinanceFosterCareRateComponent]
})
export class FinanceFosterCareRateModule { }
