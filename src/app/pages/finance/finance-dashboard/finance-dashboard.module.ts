import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceDashboardRoutingModule } from './finance-dashboard-routing.module';
import { WriteoffApprovalComponent } from './writeoff-approval/writeoff-approval.component';
import { FinanceDashboardComponent } from './finance-dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ConservedAcBalanceAlertComponent } from './conserved-ac-balance-alert/conserved-ac-balance-alert.component';
import { DirectorApprovalComponent } from './director-approval/director-approval.component';
import { SupervisorApprovalComponent } from './supervisor-approval/supervisor-approval.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AncillaryPaymentAdjustmentComponent } from './ancillary-payment-adjustment/ancillary-payment-adjustment.component';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { MatDatepickerModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatFormFieldModule, MatRippleModule, MatRadioModule, MatButtonModule } from '@angular/material';
import { CommingledTransactionApprovalComponent } from './commingled-transaction-approval/commingled-transaction-approval.component';
import { DynamicModule } from 'ng-dynamic-component';
import { PurchaseAuthorizationComponent } from './purchase-authorization/purchase-authorization.component';
import { GridsterModule } from 'angular-gridster2';
import { SupervisorApprovalModule } from './supervisor-approval/supervisor-approval.module';
import { PurchaseAuthorizationModule } from './purchase-authorization/purchase-authorization.module';
import { ChildTransactionApprovalComponent } from './child-transaction-approval/child-transaction-approval.component';
import { ManualReceivableComponent } from './manual-receivable/manual-receivable.component';
import { ArReversalReceiptComponent } from './ar-reversal-receipt/ar-reversal-receipt.component';
import { FiscalUnitTicklersComponent } from './fiscal-unit-ticklers/fiscal-unit-ticklers.component';

@NgModule({
  imports: [
    CommonModule,
    FinanceDashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    MatTooltipModule,
    A2Edatetimepicker,
    NgSelectModule,
    SharedPipesModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatRippleModule,
    MatRadioModule,
    MatButtonModule,
    SupervisorApprovalModule,
    PurchaseAuthorizationModule,
    SharedPipesModule,
    DynamicModule.withComponents([
      FinanceDashboardComponent,
      ConservedAcBalanceAlertComponent,
      WriteoffApprovalComponent,
      AncillaryPaymentAdjustmentComponent,
      SupervisorApprovalComponent,
      CommingledTransactionApprovalComponent,
      PurchaseAuthorizationComponent,
      DirectorApprovalComponent,
      ChildTransactionApprovalComponent,
      ManualReceivableComponent,
      ArReversalReceiptComponent,
      FiscalUnitTicklersComponent
  ]),
  GridsterModule,
  ],
  declarations: [WriteoffApprovalComponent,
    FinanceDashboardComponent,
    ConservedAcBalanceAlertComponent,
    DirectorApprovalComponent,
    AncillaryPaymentAdjustmentComponent, CommingledTransactionApprovalComponent, ChildTransactionApprovalComponent, ManualReceivableComponent, ArReversalReceiptComponent, FiscalUnitTicklersComponent]
})
export class FinanceDashboardModule { }
