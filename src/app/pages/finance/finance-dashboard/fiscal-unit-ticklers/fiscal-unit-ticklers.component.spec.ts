import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiscalUnitTicklersComponent } from './fiscal-unit-ticklers.component';

describe('FiscalUnitTicklersComponent', () => {
  let component: FiscalUnitTicklersComponent;
  let fixture: ComponentFixture<FiscalUnitTicklersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiscalUnitTicklersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalUnitTicklersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
