import { Component, OnInit } from '@angular/core';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AuthService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  selector: 'fiscal-unit-ticklers',
  templateUrl: './fiscal-unit-ticklers.component.html',
  styleUrls: ['./fiscal-unit-ticklers.component.scss']
})
export class FiscalUnitTicklersComponent implements OnInit {

  totalcount: number;
  overduecount: number;
  currentduecount: number;
  upcomingcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  fiscalunitticklersList: any[] = [];
  statusDropDownList = [
    {
      'text': 'All',
      'value': 'All'
    },
    {
      'text': 'Unread',
      'value': 'Unread'
    }
  ];
  typeDropDown = [
    {
      'text': 'All',
      'value': 'All'
    },
    /* {
      'text': 'Ancillary Adjustment',
      'value': 'AA'
    },
    {
      'text': 'Ancillary payment',
      'value': 'AP'
    }, */
    {
      'text': 'Account receivable',
      'value': 'AR'
    },
    {
      'text': 'Conserved balance',
      'value': 'CB'
    },
    {
      'text': 'Child leaves from care',
      'value': 'PEV'
    }
  ];
  fiscalStatus: any;
  userProfile: AppUser;
  isLastPage: boolean = false;
  ticklerType: any;
  duevalue: any;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService
  ) { }

  ngOnInit() {
    this.userProfile = this._authService.getCurrentUser();
    this.paginationInfo.pageNumber = 1;
    this.fiscalStatus = 'All';
    this.ticklerType = 'All';
    this.loadList();
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo;
    this.loadList();
  }

  loadList() {
    this.userProfile = this._authService.getCurrentUser();
    this.paginationInfo.pageNumber = 1;
    this.fiscalStatus = 'All';
    this.ticklerType = 'All';
    this._commonHttpService.endpointUrl = 'tb_receivable_detail/listFiscalunitTicklersDashboard';
    const modal = {
      page : this.paginationInfo.pageNumber,
      limit : this.paginationInfo.pageSize,
      statusval: this.fiscalStatus,
      typeval: this.ticklerType,
      dueval : this.duevalue
    };
    this._commonHttpService.create(modal).subscribe(
      (response) => {
        if (response && response.data && response.data.length > 0) {
          this.fiscalunitticklersList = response.data;
          this.totalcount = (this.fiscalunitticklersList && this.fiscalunitticklersList.length > 0) ? this.fiscalunitticklersList[0].totalcount : 0;
          this.overduecount = (this.fiscalunitticklersList && this.fiscalunitticklersList.length > 0) ? this.fiscalunitticklersList[0].overduecount : 0;
          this.currentduecount = (this.fiscalunitticklersList && this.fiscalunitticklersList.length > 0) ? this.fiscalunitticklersList[0].currentduecount : 0;
          this.upcomingcount = (this.fiscalunitticklersList && this.fiscalunitticklersList.length > 0) ? this.fiscalunitticklersList[0].upcomingcount : 0;
        } else {
          if (this.isLastPage) {
              this.paginationInfo.pageNumber =  this.paginationInfo.pageNumber -1;
              this.loadList();
          } else {
            this.fiscalunitticklersList = response.data;
          }
        }
      });
  }

  statusManualDropDown(status) {
    this.isLastPage = false;
    this.fiscalStatus = status;
    this.paginationInfo.pageNumber = 1;
    this.loadList();
  }

  typeManualDropDown(type) {
    this.isLastPage = false;
    this.ticklerType = type;
    this.paginationInfo.pageNumber = 1;
    this.loadList();
  }

  updatefiscalticklers(selectedtickler: any, value: string) {
    if (value === 'markasread') {
      selectedtickler.data_valid_sw = 'Y';
    } else if (value === 'markasdone') {
      selectedtickler.action_sw = 'Y';
      selectedtickler.data_valid_sw = 'Y';
    } else {
      selectedtickler.delete_sw = 'Y';
    }
    this.isLastPage = true;

    this._commonHttpService.create(selectedtickler, 'tb_receivable_detail/fiscalunitticklersupdate')
    .subscribe(
      (response) => {
        if (response) {
          this.loadList();
        }
      });    
  }

  dueChange (value : String) {
    this.duevalue = value;
    this.paginationInfo.pageNumber = 1;
    this.loadList();
  }

  resetFilter() {
    this.paginationInfo.pageNumber = 1;
    this.fiscalStatus = 'All';
    this.ticklerType = 'All';
    this.duevalue = '';
    this.loadList();
}

}
