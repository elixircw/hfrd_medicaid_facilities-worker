import { Component, OnInit } from '@angular/core';

import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AuthService } from '../../../../@core/services';
import { FinanceUrlConfig } from '../../finance.url.config';
import { AppConstants } from '../../../../@core/common/constants';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'writeoff-approval',
  templateUrl: './writeoff-approval.component.html',
  styleUrls: ['./writeoff-approval.component.scss']
})
export class WriteoffApprovalComponent implements OnInit {

  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  writeOffList: any[] = [];
  statusDropDownList = [
    {
      'text': 'All',
      'value': 'All'
    },
    {
      'text': 'Approved',
      'value': 'A'
    },
    {
    'text': 'Pending',
    'value': 'P'
    },
    {
    'text': 'Denied',
    'value': 'R'
    }
  ];
  writeOffStatus: any;
  statusId: string;
  activeModule: any;
  _sessionStorage: any;

  constructor(
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadDashboard();
    this._authService.dashboardConfig$.subscribe(_ => {
      this.loadDashboard();
    });
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo;
    this.loadList();
  }

  loadDashboard() {
    // this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    this.paginationInfo.pageNumber = 1;
    this.writeOffStatus = 'P';
    this.statusId = `{3045}`;
    this.loadList();
  }

  loadList() {
    // const status = this._authService.getCurrentUser().role.name === AppConstants.ROLES.LDSS_FISCAL_SUPERVISOR ? `{3045}` : `{3045,3047}`;
    this._commonHttpService.getPagedArrayList({
      limit: this.paginationInfo.pageSize,
      page: this.paginationInfo.pageNumber,
      where: {
        writeoffstatus: this.statusId,
        status: this.writeOffStatus
      },
      method: 'post'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.search.dashboardList)
      .subscribe((res: any) => {
        if (res) {
          this.writeOffList = res;
          this.totalcount = (this.writeOffList && this.writeOffList.length > 0) ? this.writeOffList[0].totalcount : 0;
        }
      });
  }

  openProvider(provider) {
    this._router.navigate([`/pages/finance/finance-accountsReceivable/provider/${provider.provider_id}/overpayments`]);
  }

  statusWriteOffDropDown(status) {
    if (status === 'All') {
      this.statusId = `{3045, 3047}`;
    } else if (status === 'P') {
      this.statusId = `{3045}`;
    } else if (status === 'A') {
      this.statusId = `{3047}`;
    } else if (status === 'R') {
      this.statusId = `{3281}`;
    }
    this.writeOffStatus = status;
    this.loadList();
  }

}
