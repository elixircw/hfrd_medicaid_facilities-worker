import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteoffApprovalComponent } from './writeoff-approval.component';

describe('WriteoffApprovalComponent', () => {
  let component: WriteoffApprovalComponent;
  let fixture: ComponentFixture<WriteoffApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WriteoffApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WriteoffApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
