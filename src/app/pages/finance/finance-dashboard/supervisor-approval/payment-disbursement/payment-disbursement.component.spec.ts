import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentDisbursementComponent } from './payment-disbursement.component';

describe('PaymentDisbursementComponent', () => {
  let component: PaymentDisbursementComponent;
  let fixture: ComponentFixture<PaymentDisbursementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentDisbursementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
