import { Component, OnInit } from '@angular/core';
import { PaginationInfo } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { Router } from '@angular/router';

@Component({
  selector: 'payment-disbursement',
  templateUrl: './payment-disbursement.component.html',
  styleUrls: ['./payment-disbursement.component.scss']
})
export class PaymentDisbursementComponent implements OnInit {

  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  paymentData = [];
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.getPaymentDisbursementApprove();

  }
  getPaymentDisbursementApprove() {
    this._commonService.getPagedArrayList({
      limit: this.paginationInfo.pageSize,
      page: this.paginationInfo.pageNumber,
      where: {status: 57},
      method: 'get'
    }, 'tb_child_account_disbursement/listFinalDisbursement?filter')
      .subscribe((res: any) => {
        if (res) {
          this.paymentData = res.data;
          this.totalcount = (this.paymentData && this.paymentData.length > 0) ? this.paymentData[0].totalcount : 0;
        }
      });
  }
  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getPaymentDisbursementApprove();
  }

  reDirectToDisbursement(paymentData) {
    this._router.navigate(['/pages/finance/child-accounts/child-accounts/' + paymentData.client_id]);
  }

}
