import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { PaginationRequest, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { Router } from '@angular/router';

@Component({
  selector: 'funding-disbursement',
  templateUrl: './funding-disbursement.component.html',
  styleUrls: ['./funding-disbursement.component.scss']
})
export class FundingDisbursementComponent implements OnInit {
  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  fundingData = [];
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.getFundingDisbursementApprove();

  }
  getFundingDisbursementApprove() {
    this._commonService.getPagedArrayList({
      limit: this.paginationInfo.pageSize,
      page: this.paginationInfo.pageNumber,
      where: {status: 56},
      method: 'get'
    }, 'tb_child_account_disbursement/listFinalDisbursement?filter')
      .subscribe((res: any) => {
        if (res) {
          this.fundingData = res.data;
          this.totalcount = (this.fundingData && this.fundingData.length > 0) ? this.fundingData[0].totalcount : 0;
        }
      });
  }
  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getFundingDisbursementApprove();
  }

  reDirectToDisbursement(fundingData) {
    this._router.navigate(['/pages/finance/child-accounts/child-accounts/' + fundingData.client_id]);
  }

}
