import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FundingDisbursementComponent } from './funding-disbursement.component';

describe('FundingDisbursementComponent', () => {
  let component: FundingDisbursementComponent;
  let fixture: ComponentFixture<FundingDisbursementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundingDisbursementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FundingDisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
