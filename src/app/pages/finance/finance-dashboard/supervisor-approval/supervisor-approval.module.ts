import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupervisorApprovalRoutingModule } from './supervisor-approval-routing.module';
// import { FundingDisbursementComponent } from './funding-disbursement/funding-disbursement.component';
// import { PaymentDisbursementComponent } from './payment-disbursement/payment-disbursement.component';
import { SupervisorApprovalComponent } from './supervisor-approval.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { MatTooltipModule, MatSelect, MatSelectModule } from '@angular/material';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    SupervisorApprovalRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    MatTooltipModule,
    MatSelectModule,
    SharedPipesModule
  ],
  declarations: [SupervisorApprovalComponent] // , FundingDisbursementComponent, PaymentDisbursementComponent
})
export class SupervisorApprovalModule { }
