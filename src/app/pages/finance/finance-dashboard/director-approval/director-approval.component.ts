import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, AuthService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { PaginationRequest, PaginationInfo, DropdownModel } from '../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../finance.url.config';
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { PurchaseAuthorization } from '../../_entities/finance-entity.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PurchaseAuthorizationParams } from '../../finance.constants';

@Component({
  selector: 'director-approval',
  templateUrl: './director-approval.component.html',
  styleUrls: ['./director-approval.component.scss']
})
export class DirectorApprovalComponent implements OnInit {
  payableApproval: any[];
  authorization_id: any;
  fiscal_category_cd: any;
  cost_no: any;
  provider_id: any;
  intakeserviceid: any;
  startDate: any;
  endDate: any;
  payment_method_cd: any;
  payableApprovalHistory: any[];
  assignedTo: any;
  selectedPerson: any;
  getUsersList: any[];
  originalUserList: any[];
  isapproved: any;
  child_account_no: any;
  client_id: any;
  dob: number;
  gender: string;
  justification_tx: string;
  service_log_id: number;
  service_start_dt: number;
  provider_nm: string;
  service_nm: number;
  service_end_dt: number;
  adr_street_nm: string;
  adr_city_nm: string;
  adr_state_cd: string;
  adr_zip5_no: string;
  client_name: string;
  client_account_id: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalcount: number;
  totalPage: number;
  pageInfo: PaginationInfo = new PaginationInfo();
  approval: any;
  purchaseAuthData: PurchaseAuthorization;
  purchaseServiceForm: FormGroup;
  remarks: any;
  fiscalCode$: Observable<DropdownModel[]>;
  reason_tx: any;
  childAccountReject = true;
  purchaseRequestForm: FormGroup;
  statusDropDownList = [
    {
      'text': 'All',
      'value': 'All'
    },
    {
      'text': 'Approved',
      'value': 'A'
    },
    {
    'text': 'Pending',
    'value': 'P'
    },
    {
      'text': 'Denied',
      'value': 'R'
    },
  ];
  directorStatus: any;
  status: any;
  fiscalCodes: any[];
  activeModule: any;

  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _route: ActivatedRoute,
    private _authService: AuthService,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private _sessionStorage: SessionStorageService) { }

  ngOnInit() {
    this.loadInfo();
    this._authService.dashboardConfig$.subscribe(_ => {
      this.loadInfo();
    });
  }

  loadInfo() {
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    this.directorStatus = 'P';
    this.getPayableApproval();
    this.buildForm();
  }

  buildForm() {
    this.purchaseRequestForm = this._formBuilder.group({
      payment_start_dt: [null],
      payment_end_dt: [null],
      payment_method_cd: [null, Validators.required],
      type_1099_cd: [null, Validators.required],
      amount_no: [null],
      store_receipt_id: [null],
      actualAmount: [null],
      payment_id: [null]
    });
    this.purchaseServiceForm = this._formBuilder.group(
      {
          fiscalCode: ['', Validators.required],
          voucherRequested: ['', Validators.required],
          costnottoexceed: '',
          justificationCode: '',
          startDt: [null, Validators.required],
          endDt: [null, Validators.required],
          fundingstatus: '',
          authorization_id: '',
          paymentstatus: '',
          final_amount_no: '',
          actualAmount: '',
          client_account_no: ''
      });

  }

  statusDirectorDropDown(status) {
    this.directorStatus = status;
    this.getPayableApproval();
  }

  getPayableApproval() {
    if (this.directorStatus === 'All') {
      this.status = null;
    } else {
      this.status = this.directorStatus;
    }
    this.payableApproval = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {approveltype: 'direcort', status: this.status,
        roletypekey: 'FNSDF'},
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter'
    ).subscribe((result: any) => {
      this.payableApproval = result;
      this.totalcount = (this.payableApproval && this.payableApproval.length > 0) ? this.payableApproval[0].totalcount : 0;
    });

  }
  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getPayableApproval();
  }
  onAuthID(authid) {
    this.getFiscalCategoryCode(authid.programkey);
    if (authid && (authid.fiscal_category_cd === '7502' ||
    authid.fiscal_category_cd === '7503')) {
      this.childAccountReject = true;
    } else {
      this.childAccountReject = false;
    }
    this.purchaseRequestForm.patchValue(authid);
    this.authorization_id = authid.authorization_id;
    this.fiscal_category_cd = authid.fiscal_category_cd;
    this.cost_no = authid.cost_no ? authid.cost_no : '0.00';
    this.provider_id = authid.provider_id;
    this.intakeserviceid = authid.intakeserviceid;
    this.startDate = authid.payment_start_dt;
    this.endDate = authid.payment_end_dt;
    this.payment_method_cd = authid.payment_method_cd;
    this.isapproved = authid.isapproved;
    this.client_account_id = authid.client_account_id;
    this.client_id = authid.client_id;
    this.client_name = authid.client_name;
    this.dob = authid.dob;
    this.gender = authid.gender;
    this.service_nm = authid.service_nm;
    this.justification_tx = authid.justification_tx;
    this.service_log_id = authid.service_log_id;
    this.service_start_dt = authid.service_start_dt;
    this.provider_nm = authid.provider_nm;
    this.service_end_dt = authid.service_end_dt;
    this.adr_street_nm = authid.adr_street_nm;
    this.adr_city_nm = authid.adr_city_nm;
    this.adr_state_cd = authid.adr_state_cd;
    this.adr_zip5_no = authid.adr_zip5_no;
    this.remarks = authid.remarks;
    this.getApproveHistory(authid.authorization_id);
    this.viewPurchaseAuthorization(authid);
    // this.approvalHistory(authid);
  }

  getApproveHistory(authid) {
    this.payableApprovalHistory = [];
    const source = this._commonService.getPagedArrayList(
      new PaginationRequest({
        where: {authorization_id: authid},
        limit : this.pageInfo.pageSize,
        page: this.pageInfo.pageNumber,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApprovalHistory + '?filter'
    ).subscribe((result: any) => {
      this.payableApprovalHistory = result;
      this.totalPage = (this.payableApprovalHistory && this.payableApprovalHistory.length > 0) ? this.payableApprovalHistory[0].totalPage : 0;
    });
  }

  viewPurchaseAuthorization(purchaseDetail) {
    // this.authorizationID = purchaseDetail.authorization_id;
  //  this.viewPurchaseDetails = purchaseDetail;

    this._commonService.getArrayList(
      new PaginationRequest({
          page: 1,
          limit: 20,
          where: { service_log_id: purchaseDetail.service_log_id },
          method: 'get'
      }),
      FinanceUrlConfig.EndPoint.accountsPayable.approval.purchaseAuthorizationGet + '?filter'
      ).subscribe(result => {
        if (result['data'] && result['data'].length > 0) {
            const paDetails = result['data'];
            this.purchaseAuthData = paDetails.length > 0 ? paDetails[0] : {};
            // this.actualAmount = this.purchaseAuthData.final_amount_no;
            const model = {
              fiscalCode: this.purchaseAuthData.fiscal_category_cd,
              voucherRequested: this.purchaseAuthData.voucher_requested,
              costnottoexceed: this.purchaseAuthData.cost_no ? this.purchaseAuthData.cost_no : '0.00',
              justificationCode: this.purchaseAuthData.justification_text,
              fundingstatus: this.purchaseAuthData.fundingstatus,
              authorization_id: this.purchaseAuthData.authorization_id ? this.purchaseAuthData.authorization_id : '',
              startDt: this.purchaseAuthData.startdt,
              endDt: this.purchaseAuthData.enddt,
              actualAmount: this.purchaseAuthData.final_amount_no,
              paymentstatus: this.purchaseAuthData.paymentstatus,
              final_amount_no: this.purchaseAuthData.final_amount_no ? this.purchaseAuthData.final_amount_no : '0.00',
              client_account_no: this.purchaseAuthData.client_account_no ? this.purchaseAuthData.client_account_no : ''
          };
          this.purchaseServiceForm.setValue(model, { emitEvent: true, onlySelf: false });
          this.purchaseServiceForm.disable();
          if (this.remarks === 'Pending') {
            this.purchaseServiceForm.get('justificationCode').enable();
            this.purchaseServiceForm.get('fiscalCode').enable();
          }
      }
      });
    }

    getFiscalCategoryCode(vendorprogramid) {
      this._commonService.getArrayList({
              where: {agencyprogramareaid: vendorprogramid},
              method: 'get'
            }, FinanceUrlConfig.EndPoint.accountsPayable.approval.fiscalCodes + '?filter',
          ).subscribe( result => {
              this.fiscalCodes = result;
          });
  }

    updateAuthoriz() {
      this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.history.updatePurchaseAuthorizationGet;
      const modal = {
        authorization_id: this.authorization_id,
        fiscal_category_cd: this.purchaseServiceForm.get('fiscalCode').value,
        justification_tx: this.purchaseServiceForm.get('justificationCode').value
      };
      this._commonService.create(modal).subscribe(
        (response) => {
          this._alertService.success('Fiscal category has been Updated');
          this.getPayableApproval();
        },
        (error) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    }

  pageNumberChanged(page) {
    this.pageInfo.pageNumber = page;
    this.getApproveHistory(this.authorization_id);
  }

  private assignUser() {
    console.log('this.authorization_id...', this.authorization_id);
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.ServiceLogApproval;
        const modal = {
           'fiscalcategorycd' : this.fiscal_category_cd,
           'costno' : this.cost_no ? this.cost_no : '0.00',
           'authorization_id' : this.authorization_id,
           'provider_id' : this.provider_id,
           'intakeserviceid' : this.intakeserviceid,
           'assignedtoid' : this.assignedTo,
           'eventcode' : 'PCAUTH',
           'status' : 44,
           'startDt': this.startDate,
           'endDt': this.endDate,
           'payment_method_cd': this.payment_method_cd,
           'client_id': this.client_id,
           'client_account_id': this.client_account_id
        };
    this._commonService.create(modal).subscribe(
        (response) => {
            this._alertService.success('Approval sent successfully!');
            (<any>$('#payment-approval')).modal('hide');
            this.getPayableApproval();
        },
        (error) => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
    );

  }

  selectPerson(row) {
    if (row) {
      this.selectedPerson = row;
      this.assignedTo = row.userid;
    }
  }

  getRoutingUser() {
    (<any>$('#approval-history')).modal('hide');
    this._commonService
        .getPagedArrayList(
            new PaginationRequest({
                where: { appevent: 'PCAUTH' },
                method: 'post'
            }),
            'Intakedastagings/getroutingusers'
        )
        .subscribe((result) => {
            this.getUsersList = result.data;
            this.originalUserList = this.getUsersList;
            this.listUser('TOBEASSIGNED');
        });
 }

 listUser(assigned: string) {
  this.selectedPerson = '';
  this.getUsersList = [];
  this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter((res) => {
        if (res.userrole === 'LDSS Fiscal Supervisor') {
            return res;
        }
    });
    }
  }
  confirmReject() {
    (<any>$('#approval-history')).modal('hide');
    (<any>$('#payment-approval')).modal('hide');
    (<any>$('#reject-approval')).modal('show');
    this.reason_tx = '';
  }

  rejectApproval() {
    this._commonService.endpointUrl = 'tb_account_transaction/deleteClientTransactionbyAuthId' + '/' + this.authorization_id;
    const modal = {
      client_account_id: this.client_account_id,
      fiscal_category_cd: this.fiscal_category_cd,
      cost_no: this.cost_no
    };
    this._commonService.create(modal).subscribe(
    (response) => {
      this._alertService.success('Approval has been Rejected!');
      (<any>$('#reject-approval')).modal('hide');
      this.getPayableApproval();
    },
    (error) => {
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });

  }

  rejectOtherApproval() {
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.accountsPayable.approval.PurchaseAuthorizationReject + this.authorization_id;
    const modal = {
      reason_tx: this.reason_tx
    };
    this._commonService.create(modal).subscribe(
      (response) => {
        this._alertService.success('Purchase Authorization has been rejected!');
        (<any>$('#reject-approval')).modal('hide');
        this.getPayableApproval();
      },
      (error) => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  reDirectToDirector(fundingData) {
    this._router.navigate(['/pages/finance/finance-accountsPayable/approval/director']);
    this._dataStoreService.setData(PurchaseAuthorizationParams.DirectorParms, fundingData);
  }

}
