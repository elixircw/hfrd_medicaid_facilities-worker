import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FundingPurchaseAuthorizationComponent } from './funding-purchase-authorization.component';

describe('FundingPurchaseAuthorizationComponent', () => {
  let component: FundingPurchaseAuthorizationComponent;
  let fixture: ComponentFixture<FundingPurchaseAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundingPurchaseAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FundingPurchaseAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
