import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { Router } from '@angular/router';
import { FinanceUrlConfig } from '../../../finance.url.config';

@Component({
  selector: 'funding-purchase-authorization',
  templateUrl: './funding-purchase-authorization.component.html',
  styleUrls: ['./funding-purchase-authorization.component.scss']
})
export class FundingPurchaseAuthorizationComponent implements OnInit {

  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  fundingData = [];
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.getFundingDisbursementApprove();

  }
  getFundingDisbursementApprove() {
    this._commonService.getPagedArrayList(
      new PaginationRequest({
      limit: this.paginationInfo.pageSize,
      page: this.paginationInfo.pageNumber,
      where: {approveltype: 'funding'},
      method: 'get'
    }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter')
      .subscribe((res: any) => {
        if (res) {
          this.fundingData = res;
          this.totalcount = (this.fundingData && this.fundingData.length > 0) ? this.fundingData[0].totalcount : 0;
        }
      });
  }
  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getFundingDisbursementApprove();
  }

  reDirectToFunding(fundingData) {
    this._router.navigate(['/pages/finance/finance-accountsPayable/approval/financial']);
  }


}
