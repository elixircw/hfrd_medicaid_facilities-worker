import { Component, OnInit } from '@angular/core';
import { PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { FinanceUrlConfig } from '../../finance.url.config';
import { CommonHttpService, AlertService, DataStoreService, AuthService, SessionStorageService } from '../../../../@core/services';
import { Router } from '@angular/router';
import { PurchaseAuthorizationParams } from '../../finance.constants';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';

@Component({
  selector: 'purchase-authorization',
  templateUrl: './purchase-authorization.component.html',
  styleUrls: ['./purchase-authorization.component.scss']
})
export class PurchaseAuthorizationComponent implements OnInit {

  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  paymentData = [];
  payableApproval: any;
  fundingData = [];
  statusDropDownList = [
    {
      'text': 'All',
      'value': 'All'
    },
    {
      'text': 'Approved',
      'value': 'A'
    },
    {
    'text': 'Pending',
    'value': 'P'
    },
    {
      'text': 'Denied',
      'value': 'R'
    },
  ];
  fundingStatus: any;
  paymentStatus: any;
  pageInfo: PaginationInfo = new PaginationInfo;
  totalpagecount: number;
  status: any;
  user: AppUser;
  financeSupervisor: boolean;
  paymentPayee: any;
  fundingPayee: any;
  paymentPayeeName: any;
  fundingPayeeName: any;
  activeModule: string;
  roletype: string;
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _sessionStorage: SessionStorageService
  ) { }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    this.pageInfo.sortBy = null;
    this.pageInfo.sortColumn = null;
    this.paginationInfo.sortBy = null;
    this.paginationInfo.sortColumn = null;
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    this.loadInfo();
    this._authService.dashboardConfig$.subscribe(_ => {
      this.loadInfo();
    });
  }
  loadInfo() {
    this.user = this._authService.getCurrentUser();
    this.pageInfo.sortBy = null;
    this.pageInfo.sortColumn = null;
    this.paginationInfo.sortBy = null;
    this.paginationInfo.sortColumn = null;
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    if (this.activeModule === 'Finance Approval') {
      this.roletype = 'FNSFS';
      this.financeSupervisor = true;
    } else if (this.activeModule === 'Finance') {
      this.roletype = 'FNSFW';
      this.financeSupervisor = false;
    } else {
      this.roletype = 'FNSDF';
      this.financeSupervisor = false;
    }
    this.fundingStatus = 'P';
    this.paymentStatus = 'P';
    if (this.financeSupervisor) {
      this.getPaymentApprove();
    }
    this.getFundingApprove();
  }
  statusFundingDropDown(status) {
    this.fundingStatus = status;
    this.pageInfo.sortBy = null;
    this.pageInfo.sortColumn = null;
    this.getFundingApprove();
  }
  statusPaymentDropDown(status) {
    this.paymentStatus = status;
    this.paginationInfo.sortBy = null;
    this.paginationInfo.sortColumn = null;
    this.getPaymentApprove();
  }
  getFundingApprove() {
    this.paymentPayee = null;
    this.paymentPayeeName = null;
    if (this.fundingStatus === 'All') {
      this.status = null;
    } else {
      this.status = this.fundingStatus;
    }
    this._commonService.getPagedArrayList(
      new PaginationRequest({
      limit: this.pageInfo.pageSize,
      page: this.pageInfo.pageNumber,
      where: {approveltype: 'funding', status: this.fundingStatus,
      sortcolumn: this.pageInfo.sortColumn,
      sortorder: this.pageInfo.sortBy,
      payee_nm: this.fundingPayeeName ? this.fundingPayeeName : null,
      roletypekey: this.roletype
     },
      method: 'get'
    }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter')
      .subscribe((res: any) => {
        if (res) {
          this.fundingData = res;
          this.totalpagecount = (this.fundingData && this.fundingData.length > 0) ? this.fundingData[0].totalcount : 0;
          this.pageInfo.sortBy = null;
          this.pageInfo.sortColumn = null;
        }
      });
  }
  getPaymentApprove() {
    this.fundingPayee = null;
    this.fundingPayeeName = null;
    if (this.paymentStatus === 'All') {
      this.status = null;
    } else {
      this.status = this.paymentStatus;
    }
      this.payableApproval = [];
      const source = this._commonService.getPagedArrayList(
        new PaginationRequest({
          where: {
          approveltype: 'payment',
          status: this.paymentStatus,
          sortcolumn: this.paginationInfo.sortColumn,
          sortorder: this.paginationInfo.sortBy,
          payee_nm: this.paymentPayeeName ? this.paymentPayeeName : null,
          roletypekey: this.roletype
          },
          limit : this.paginationInfo.pageSize,
          page: this.paginationInfo.pageNumber,
          method: 'get'
        }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter'
      ).subscribe((result: any) => {
        this.payableApproval = result;
        this.totalcount = (this.payableApproval && this.payableApproval.length > 0) ? this.payableApproval[0].totalcount : 0;
        this.paginationInfo.sortBy = null;
        this.paginationInfo.sortColumn = null;
      });
  }
  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getPaymentApprove();
  }

  onSearchPayee(mode) {
    if (mode === 'funding') {
      if (this.fundingPayee) {
        this.fundingPayeeName = this.fundingPayee.replace(/'/g, `''`);
      } else {
        this.fundingPayeeName = null;
      }
      this.getFundingApprove();
    } else {
      if (this.paymentPayee) {
        this.paymentPayeeName = this.paymentPayee.replace(/'/g, `''`);
      } else {
        this.paymentPayeeName = null;
      }
      this.getPaymentApprove();
    }
  }

  pageNumberChanged(page) {
    this.pageInfo.pageNumber = page;
    this.getFundingApprove();
  }

  reDirectToPayment(paymentData) {
    this._router.navigate(['/pages/finance/finance-accountsPayable/approval/payment']);
    this._dataStoreService.setData(PurchaseAuthorizationParams.PaymentParms, paymentData);
  }

  reDirectToFunding(fundingData) {
    this._router.navigate(['/pages/finance/finance-accountsPayable/approval/financial']);
    this._dataStoreService.setData(PurchaseAuthorizationParams.FundingParms, fundingData);
  }

  onSortedFunding($event: ColumnSortedEvent) {
    this.pageInfo.sortBy = $event.sortDirection;
    this.pageInfo.sortColumn = $event.sortColumn;
    this.getFundingApprove();
  }

  onSortedPayment($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.getPaymentApprove();
  }


}
