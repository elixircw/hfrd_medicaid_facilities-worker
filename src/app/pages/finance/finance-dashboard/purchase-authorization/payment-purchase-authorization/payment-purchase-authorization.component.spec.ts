import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentPurchaseAuthorizationComponent } from './payment-purchase-authorization.component';

describe('PaymentPurchaseAuthorizationComponent', () => {
  let component: PaymentPurchaseAuthorizationComponent;
  let fixture: ComponentFixture<PaymentPurchaseAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentPurchaseAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentPurchaseAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
