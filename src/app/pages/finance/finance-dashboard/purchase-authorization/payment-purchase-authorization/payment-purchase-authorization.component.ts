import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService } from '../../../../../@core/services';
import { Router } from '@angular/router';
import { FinanceUrlConfig } from '../../../finance.url.config';

@Component({
  selector: 'payment-purchase-authorization',
  templateUrl: './payment-purchase-authorization.component.html',
  styleUrls: ['./payment-purchase-authorization.component.scss']
})
export class PaymentPurchaseAuthorizationComponent implements OnInit {

  totalcount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  paymentData = [];
  payableApproval: any;
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.getPaymentApprove();

  }
  getPaymentApprove() {
      this.payableApproval = [];
      const source = this._commonService.getPagedArrayList(
        new PaginationRequest({
          where: {approveltype: 'payment'},
          limit : this.paginationInfo.pageSize,
          page: this.paginationInfo.pageNumber,
          method: 'get'
        }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApproval + '?filter'
      ).subscribe((result: any) => {
        this.payableApproval = result;
        this.totalcount = (this.payableApproval && this.payableApproval.length > 0) ? this.payableApproval[0].totalcount : 0;
      });
  }
  pageChanged(page) {
    this.paginationInfo.pageNumber = page;
    this.getPaymentApprove();
  }

  reDirectToPayment(paymentData) {
    this._router.navigate(['/pages/finance/finance-accountsPayable/approval/payment']);
  }

}
