import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseAuthorizationRoutingModule } from './purchase-authorization-routing.module';
import { PurchaseAuthorizationComponent } from './purchase-authorization.component';
import { FundingPurchaseAuthorizationComponent } from './funding-purchase-authorization/funding-purchase-authorization.component';
import { PaymentPurchaseAuthorizationComponent } from './payment-purchase-authorization/payment-purchase-authorization.component';
import { MatTooltipModule, MatSelectModule, MatFormFieldModule, MatIconModule, MatButtonToggleModule } from '@angular/material';
import { PaginationModule } from 'ngx-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SortTableModule } from '../../../../shared/modules/sortable-table/sortable-table.module';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    PurchaseAuthorizationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonToggleModule,
    MatSelectModule,
    SortTableModule,
    FormMaterialModule,
    SharedPipesModule
  ],
  declarations: [PurchaseAuthorizationComponent] // FundingPurchaseAuthorizationComponent, PaymentPurchaseAuthorizationComponent
})
export class PurchaseAuthorizationModule { }
