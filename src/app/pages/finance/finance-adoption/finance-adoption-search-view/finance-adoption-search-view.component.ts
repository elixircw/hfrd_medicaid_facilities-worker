import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { FinanceAdoptionSearchEntry} from '../../_entities/finance-entity.module';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'adoption-search-view',
    templateUrl: './finance-adoption-search-view.component.html',
    styleUrls: ['./finance-adoption-search-view.component.scss']
})
export class FinanceAdoptionSearchViewComponent implements OnInit {
    @Input() selectedAdoption: FinanceAdoptionSearchEntry;
    constructor() {
    }
    ngOnInit() {
    }

}
