import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable, Subject } from 'rxjs/Rx';

import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { GenericService } from '../../../../@core/services/generic.service';
import { FinancePlacementSearchEntry} from '../../_entities/finance-entity.module';
import { FinanceUrlConfig } from '../../finance.url.config';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'app-finance-placement-search-filters',
    templateUrl: './finance-placement-search-filters.component.html',
    styleUrls: ['./finance-placement-search-filters.component.scss']
})
export class FinancePlacementSearchFiltersComponent implements OnInit {

    @Input() searchData$: Subject<Observable<FinancePlacementSearchEntry[]>>;
    @Input() totalSearchRecord$: Subject<Observable<number>>;
    @Input() pageNumberSearch$: Subject<number>;

    placementSearch: FinancePlacementSearchEntry;
    placementFormGroup: FormGroup;

    // datepicker
    minDate = new Date();
    colorTheme = 'theme-blue';

    constructor(private formBuilder: FormBuilder, private _service: GenericService<FinancePlacementSearchEntry>) {
        this._service.endpointUrl = FinanceUrlConfig.EndPoint.placement.getPlacementInfo; 
        //financePlacement/getPlacementInfo;

        
        this.placementSearch = new FinancePlacementSearchEntry();
        this.placementSearch.sortdir = 'asc';
        this.placementSearch.sortcol = 'placementId';
        this.placementSearch.activeflag = '1';

        this.placementFormGroup = this.formBuilder.group({
            clientid: [''],
            clientname: [''],
            providerid: [''],
            placementid: [''],
            providername: [''],
            placementstartdatefrom: [''],
            placementstartdateto: [''],
            placementenddatefrom: [''],
            placementenddateto: [''],
            address: [''],
            zip: [''],
            state: [''],
            city: [''],
            county: [''],
            region: [''],
            phonenumber: ['']
        });
    }


    ngOnInit() {
        this.pageNumberSearch$.subscribe(data => {
           // if (data !== 1) {
                this.searchPlacement(data, this.placementSearch);
           // }
        });

    }


    searchPlacement(pageNo: number, modal: FinancePlacementSearchEntry) {
        this.placementSearch = modal;
        modal = Object.assign(new FinancePlacementSearchEntry(), modal);
        ObjectUtils.removeEmptyProperties(modal);
        const source = this._service.getPagedArrayList(new PaginationRequest({
            where: modal,
            page: pageNo,
            limit: 10,
            method: 'post'
        })).share();

        this.searchData$.next(source.pluck('data'));
        if (pageNo === 1) {
            this.totalSearchRecord$.next(source.pluck('count'));
        }
    }
    
    clearSearch() {
        this.placementFormGroup.reset();
        this.placementFormGroup.patchValue({ county: '', region: '', gender: '', race: '' });
    }
}
