import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { FinancePlacementSearchEntry} from '../../_entities/finance-entity.module';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'placement-search-view',
    templateUrl: './finance-placement-search-view.component.html',
    styleUrls: ['./finance-placement-search-view.component.scss']
})
export class FinancePlacementSearchViewComponent implements OnInit {
    @Input() selectedPlacement: FinancePlacementSearchEntry;
    constructor() {
    }
    ngOnInit() {
    }

}
