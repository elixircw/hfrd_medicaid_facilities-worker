import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { FinancePlacementSearchEntry} from '../../_entities/finance-entity.module';



@Component({
  selector: 'app-finance-placement-search-result',
  templateUrl: './finance-placement-search-result.component.html',
  styleUrls: ['./finance-placement-search-result.component.scss']
})
export class FinancePlacementSearchResultComponent implements OnInit {

  @Input() showTable: boolean;
  @Input() pageNumberResult$: Subject<number>;
  @Input() searchResultData$: Observable<FinancePlacementSearchEntry[]>;
  @Input() totalResulRecords$: Observable<number>;

  selectedRecord: FinancePlacementSearchEntry;
  paginationInfo: PaginationInfo = new PaginationInfo();


  constructor() {
    this.showTable = false;
    this.selectedRecord = new FinancePlacementSearchEntry();
  }

  ngOnInit() {
  }

  pageChanged(page: number) {
      this.paginationInfo.pageNumber = page;
      this.pageNumberResult$.next(page);
  }

  selectPlacement(data: FinancePlacementSearchEntry) {
      this.selectedRecord = data;
  }
}
