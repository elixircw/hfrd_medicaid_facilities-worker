import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChildAccountsRoutingModule } from './child-accounts-routing.module';
import {
  MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule,
  MatSelectModule, MatButtonModule, MatRadioModule, MatTabsModule, MatTooltipModule,
  MatCheckboxModule, MatListModule, MatCardModule, MatTableModule, MatExpansionModule,
  MatChipsModule, MatIconModule, MatAutocompleteModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule, TimepickerModule } from 'ngx-bootstrap';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SortTableModule } from '../../../shared/modules/sortable-table/sortable-table.module';
import { ChildAccountsComponent } from './child-accounts.component';
import {MatSortModule} from '@angular/material/sort';
import { ChildAccountsTabComponent } from './child-accounts/child-accounts.component';
import { InterestTransactionsComponent } from './interest-transactions/interest-transactions.component';
import {CostReimbursementComponent } from './cost-reimbursement/cost-reimbursement.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MaskPipe } from '../../../@core/pipes/ssnMask.pipe';
import { NgxMaskModule } from 'ngx-mask';
import { CommonControlsModule } from '../../../shared/modules/common-controls/common-controls.module';

@NgModule({
  imports: [
    CommonModule,
    CommonControlsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatChipsModule,
    MatIconModule,
    FormsModule,
    MatSortModule,
    ReactiveFormsModule,
    PaginationModule,
    TimepickerModule,
    ControlMessagesModule,
    SharedDirectivesModule,
    NgSelectModule,
    ImageCropperModule,
    SortTableModule,
    MatAutocompleteModule,
    ChildAccountsRoutingModule,
    NgxMaskModule.forRoot(),
    FormMaterialModule,
    SharedPipesModule,
    
  ],
  declarations: [ChildAccountsComponent, ChildAccountsTabComponent, InterestTransactionsComponent, CostReimbursementComponent]

})
export class ChildAccountsModule { }
