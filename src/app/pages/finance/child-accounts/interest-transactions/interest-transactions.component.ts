import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
import { FinanceUrlConfig } from '../../finance.url.config';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, AuthService, DataStoreService, CommonDropdownsService } from '../../../../@core/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import { SessionStorageService } from '../../../../@core/services/storage.service';
import { Sort } from '@angular/material/sort';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';
import * as moment from 'moment';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { ChildAccount } from '../../finance.constants';

@Component({
  selector: 'interest-transactions',
  templateUrl: './interest-transactions.component.html',
  styleUrls: ['./interest-transactions.component.scss'],
  providers: [DatePipe]
})

export class InterestTransactionsComponent implements OnInit, AfterContentInit {
  childaccountslist: any[];
  getPagedArrayList$: Observable<any[]>;
  paginationInfo: PaginationInfo = new PaginationInfo();
  childtransactionslist: any[] = [];
  childdetails: any;
  selectedTransaction: any;
  addForm: FormGroup;
  editTransactionForm: FormGroup;
  clientAccount: any;
  transactionExistOptions = [
    { text: 'Yes', value: 'Y' },
    { text: 'No', value: 'N' },
  ];
  transactionType$: Observable<any[]>;
  transactionTypeChangeList: any[];
  transactionSourceChangeList: any[];
  transactionSource: any[];
  localDept: any[];
  totalRecords: any;
  disableTrans: boolean;
  typeTransaction: string;
  client_account_type: any;
  minDate: Date;
  transSource: boolean;
  monthStart: Date;
  approvalRequest: boolean;
  selectedPerson: any;
  getUsersList: any[];
  originalUserList: any[];
  assignedTo: any;
  userProfile: AppUser;
  addTransctionForm: any;
  isFW: boolean;
  isFS: boolean;
  status: number;
  comments: string;
  alertTxt: string;
  url: string;
  reason_tx: any;
  buttonTxt: string;
  datastoreSubscription: any;
  errorCorrection: any;
  accountDetails: any;
  sortedData: any;
  benefitEndDt: boolean;
  benefitStartDt: boolean;
  actualCost: any;
  editTransctionForm: any;
  editTrans: boolean;
  isAdjust: boolean;
  obligated_amount: any;
  late_entry_sw: string;
  dateMin: Date;
  endMaxDate: Date;
  transOtherSource: boolean;
  receiptsType: boolean;
  dateMinCheck: boolean;
  activeModule: any;
  allowedErrorCorrection: any;
  minTransDate: Date;
  dateMinBk: Date;
  constructor(
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private sessionStora: SessionStorageService,
    private _authService: AuthService,
    private _datastoreService: DataStoreService,

    private _commonDropDownService: CommonDropdownsService,
    private _sessionStorage: SessionStorageService) {
    this.sortedData = this.childtransactionslist.slice();

  }

  ngOnInit() {
    this.userProfile = this._authService.getCurrentUser();
    this.paginationInfo.sortColumn = 'transactionid';
    this.paginationInfo.sortBy = 'desc';
    this.activeModule = this._sessionStorage.getItem('activeModuleNav');
    if (this.activeModule === 'Finance') {
      this.isFW = true;
      this.status = 81;
    } else if (this.activeModule === 'Finance Approval') {
      this.isFS = true;
      this.status = 82;
    }
    this.datastoreSubscription = this._datastoreService.currentStore.subscribe(store => {
      this.errorCorrection = store[ChildAccount.ErrorCorrection];
    });
    this.initTransactionForm();
    this.initEditTransactionForm();
    this.getchilddetailslist();
  }

  ngAfterContentInit() {
    if (this.errorCorrection) {
      this.accountDetails = this.errorCorrection;
      this.getchilddetailslist();
      this._datastoreService.setData(ChildAccount.ErrorCorrection, null);
    }

  }

  getchilddetailslist() {
    // const client_Id = this.sessionStora.getObj('clientId');
    // const client_Name = this.sessionStora.getObj('clientName');
    let selectedClientId;
    if (this.accountDetails) {
      selectedClientId = this.accountDetails.client_id;
    } else {
      selectedClientId = this.sessionStora.getObj('selectedClientId');
    }

    if (selectedClientId) {
      this._commonService.getPagedArrayList(new PaginationRequest({
        where: { client_id: selectedClientId, sortcolumn:this.paginationInfo.sortColumn,sortorder:this.paginationInfo.sortBy  },
        page: 1,
        limit: 20,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.childAccounts.getchilddetailslistUrl + '?filter').subscribe(result => {
        if (result && result.data && result.data.length > 0) {
          this.childdetails = result.data[0];
          this.getchildaccountslist();
        } else {
          this.childdetails = null;
        }
      });
    }
  }

  pageChanged(page) {
    this.getTransactionslist(page);
  }

  getTransactionslist(page = 1) {
    if (this.clientAccount) {
      this.dateMin = this.clientAccount.open_dt ? new Date(new Date(this.clientAccount.open_dt).setHours(0, 0, 0, 0)) : null;
      this.dateMinBk = this.dateMin;
      this.obligated_amount = this.clientAccount.obligated_for_anc ? this.clientAccount.obligated_for_anc : '0.00';
      this.client_account_type = this.clientAccount.account_type_cd;
      if (this.clientAccount.status_cd === '592' && this.clientAccount.service_id !== 101) {
        this.disableTrans = false;
      } else {
        this.disableTrans = true;
      }
    }
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: { client_account_id: this.clientAccount.client_account_id,sortcolumn:this.paginationInfo.sortColumn,sortorder:this.paginationInfo.sortBy },
      page: page,
      limit: 10,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.interestTransactions.getTransactionsUrl + '?filter').subscribe(result => {
      this.childtransactionslist = result.data;
      for (let i = 0; i < this.childtransactionslist.length; i++) {
        this.childtransactionslist[i].benefit_start_dt = this._commonDropDownService.getValidDate(this.childtransactionslist[i].benefit_start_dt);
        this.childtransactionslist[i].benefit_end_dt = this._commonDropDownService.getValidDate(this.childtransactionslist[i].benefit_end_dt);
      }
      this.sortedData = this.childtransactionslist.slice();
      this.totalRecords = result.count;
      this.getDropdowns(this.clientAccount.client_account_id);
      if (this.accountDetails) {
        const transaction = this.childtransactionslist.filter(res => res.transaction_id === this.accountDetails.transaction_id);
        if (this.isFS && transaction[0].status_type === 81) {
          this.editTransaction(transaction[0], 'fs');
        } else if (this.isFW && transaction[0].status_type === 83) {
          this.editTransaction(transaction[0], 'fw');
        } else {
          this.viewTransaction(transaction[0]);
        }
        this.accountDetails = null;
      }
    });
  }

  initTransactionForm() {
    this.addForm = this.formBuilder.group({
      transaction_dt: [{ value: '', disabled: true }],
      entered_by: [''],
      transaction_type_cd: ['', Validators.required],
      recurring_transaction: ['', Validators.required],
      transaction_source_cd: ['', Validators.required],
      reference_transaction_id: ['', Validators.required],
      benefit_start_dt: ['', Validators.required],
      benefit_end_dt: ['', Validators.required],
      transaction_amount_no: ['', Validators.required],
      credit_debit_sw: [{ value: '', disabled: true }],
      notes_tx: [''],
      delete_sw: [''],
      transaction_type: [''],
      transaction_source: [''],
      transaction_id: [''],


      client_account_id: [''],
      create_ts: [''],
      update_ts: [''],
      adjustment_approval_status_cd: [''],
      manual_db_approval_status_cd: [''],
      post_sw: [''],
      payment_detail_id: [''],
      authorization_id: [''],
      late_entry_sw: [''],
      frequency_cd: ['']
    });
    this.addForm.get('reference_transaction_id').disable();
    this.addForm.get('benefit_start_dt').disable();
    this.addForm.get('benefit_end_dt').disable();
    this.benefitEndDt = true;
    this.benefitStartDt = true;
  }

  initEditTransactionForm() {
    this.editTransactionForm = this.formBuilder.group({
      transaction_dt: [{ value: '', disabled: true }],
      transaction_type_cd: ['', Validators.required],
      transaction_source_cd: ['', Validators.required],
      frequency_cd: ['', Validators.required],
      // reference_transaction_id: ['', Validators.required],
      benefit_start_dt: [{ value: '', disabled: true }, Validators.required],
      benefit_end_dt: ['', Validators.required],
      transaction_amount_no: ['', Validators.required],
      credit_debit_sw: [{ value: '', disabled: true }],
      notes_tx: [''],

      client_account_id: [''],
      create_ts: [''],
      update_ts: [''],
      adjustment_approval_status_cd: [''],
      manual_db_approval_status_cd: [''],
      post_sw: [''],
      payment_detail_id: [''],
      authorization_id: [''],
      late_entry_sw: [''],
      delete_sw: ['']
    });
  }

  onChangeDate(form, mode) {
    if (form.value.benefit_start_dt) {
        const beginDate = new Date(form.value.benefit_start_dt);
        const day = new Date(form.value.benefit_start_dt).getDate();
        const startdate = new Date(beginDate.getFullYear(), beginDate.getMonth(), 1);
        const start = new Date(startdate).getDate();
      if (mode === 'check') {
        if (day === start) {
          this.minDate = new Date(form.value.benefit_start_dt);
          form.get('benefit_end_dt').reset();
        } else {
          if (!this.dateMinCheck) {
            this._alertService.error('Please Select the Beginning date of the month');
            form.get('benefit_start_dt').reset();
            form.get('benefit_end_dt').reset();
          }
        }
      } else {
        this.minDate = new Date(form.value.benefit_start_dt);
        this.endMaxDate = new Date(this.minDate.getFullYear(), this.minDate.getMonth() + 1, 0);
        form.get('benefit_end_dt').reset();
      }
    }
  }

  onChangeEndDate(form) {
    if (form.value.benefit_end_dt) {
      const endDt = new Date(form.value.benefit_end_dt);
      const day = new Date(form.value.benefit_end_dt).getDate();
      const endDate = new Date(endDt.getFullYear(), endDt.getMonth() + 1, 0);
      const end = new Date(endDate).getDate();
      if (day !== end) {
        this._alertService.error('Please Select the End date of the month');
        form.get('benefit_end_dt').reset();
      }
    }
  }

  addNewTransaction() {
    if (this.addForm.valid) {
      const data = this.addForm.getRawValue();

      if (this.addForm.getRawValue().benefit_start_dt && this.addForm.getRawValue().benefit_end_dt) {
        const startDate = moment(new Date(this.addForm.getRawValue().benefit_start_dt)).format('YYYY-MM-DD');
        const endDate = moment(new Date(this.addForm.getRawValue().benefit_end_dt)).format('YYYY-MM-DD');
        const prestartDate = new Date().getMonth() - 1;
        if (this.transSource) {
          if ((new Date(startDate).getMonth() === new Date().getMonth() || new Date(startDate).getMonth() === prestartDate)) {
            this.late_entry_sw = null;
          } else {
            this.late_entry_sw = 'Y';
          }
        } else {
          this.late_entry_sw = null;
        }
        if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
          this._alertService.error('Benefit End Date should be greater than or equal to Benefit Start Date');
          return false;
        }
      }
      if (data.transaction_amount_no <= 0) {
        this._alertService.error('Amount should be greater then $0.00');
        return false;
      }

      if (data.transaction_type_cd === '588') {
        if (+data.transaction_amount_no > +this.clientAccount.total_balance_no) {
          this._alertService.error('Adjustment Amount should be within the total balance');
          return false;
        }
      }

      this._commonService.create({

        // 'transaction_dt': this.datePipe.transform(this.addForm.value.transaction_dt, 'yyyy-MM-dd'),
        'transaction_dt': new Date(),
        'transaction_type_cd': data.transaction_type_cd,
        'frequency_cd': data.recurring_transaction,
        'transaction_source_cd': data.transaction_source_cd,
        'reference_transaction_id': data.reference_transaction_id,
        'benefit_start_dt': this.datePipe.transform(this.addForm.value.benefit_start_dt, 'yyyy-MM-dd'),
        'benefit_end_dt': this.datePipe.transform(this.addForm.value.benefit_end_dt, 'yyyy-MM-dd'),
        'transaction_amount_no': data.transaction_amount_no,
        'credit_debit_sw': data.credit_debit_sw,
        'notes_tx': data.notes_tx,
        'delete_sw': 'N',

        'client_account_id': this.clientAccount.client_account_id,
        'create_ts': new Date(),
        // 'update_ts': '2019-07-26',
        // 'adjustment_approval_status_cd': '500',
        // 'manual_db_approval_status_cd': '500',
        'post_sw': null,
        // 'payment_detail_id': 1232131,
        // 'authorization_id': 4543,
        'late_entry_sw': this.late_entry_sw

      }
        , FinanceUrlConfig.EndPoint.interestTransactions.addNewTransactionUrl).subscribe(result => {
          if (result) {
            if (!result.isexists) {
              this._alertService.success('Transaction added successfully');
              this.getTransactionslist();
              this.getchildaccountslist();
              (<any>$('#addNewTransaction')).modal('hide');
              this.benefitEndDt = true;
              this.benefitStartDt = true;
            } else {
              this._alertService.error('Transaction is already added for the selected month');
            }
          } else {
           // this._alertService.error(' Transaction not added successfully');
            this._alertService.success('Transaction added successfully');
            this.getTransactionslist();
            this.getchildaccountslist();
            (<any>$('#addNewTransaction')).modal('hide');
            this.benefitEndDt = true;
            this.benefitStartDt = true;
          }
        });
    } else {
      this._alertService.warn('Please fill the mandatory details.');
    }
  }

  UpdateCase() {
    if (this.editTransactionForm.valid) {
      const data = this.editTransactionForm.getRawValue();

      if (this.editTransactionForm.getRawValue().benefit_start_dt && this.editTransactionForm.getRawValue().benefit_end_dt) {
        const startDate = moment(new Date(this.editTransactionForm.getRawValue().benefit_start_dt)).format('YYYY-MM-DD');
        const endDate = moment(new Date(this.editTransactionForm.getRawValue().benefit_end_dt)).format('YYYY-MM-DD');
        if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
          this._alertService.error('Benefit End Date should be greater than or equal to Benefit Start Date');
          return false;
        }
      }

      this._commonService.update('',
        {
          'transaction_dt': data.transaction_dt,
          'transaction_type_cd': data.transaction_type_cd,
          'frequency_cd': data.frequency_cd,
          'transaction_source_cd': data.transaction_source_cd,
          // 'reference_transaction_id': data.reference_transaction_id,
          'benefit_start_dt': data.benefit_start_dt,
          'benefit_end_dt': data.benefit_end_dt,
          'transaction_amount_no': data.transaction_amount_no,
          'credit_debit_sw': data.credit_debit_sw,
          'notes_tx': data.notes_tx,
          'client_account_id': this.clientAccount.client_account_id,
          'create_ts': null,
          'update_ts': null,
          'adjustment_approval_status_cd': 500,
          'manual_db_approval_status_cd': 500,
          'post_sw': null,
          'payment_detail_id': 1232131,
          'authorization_id': 4543,
          'late_entry_sw': null,
          'delete_sw': 'N'
        }
        , FinanceUrlConfig.EndPoint.interestTransactions.EditTransactionUrl + this.selectedTransaction.transaction_id).subscribe(result => {
          this._alertService.success('Child Transaction updated successfully');
          this.getTransactionslist();
          this.getchildaccountslist();
          (<any>$('#editTransaction')).modal('hide');
        });
    } else {
      this._alertService.warn('Please fill the mandatory details.');
    }
  }


  viewTransaction(transaction) {
    this.selectedTransaction = transaction;
    (<any>$('#viewTransaction')).modal('show');
    if (transaction.credit_debit_sw === 'C') {
      this.typeTransaction = 'Credit';
    } else {
      this.typeTransaction = 'Debit';
    }
  }

  /* editTransaction(transaction) {
   // console.log(transaction);
    this.minDate = new Date(transaction.benefit_start_dt);

    this.transactionType$.subscribe(response => {
      if (response) {
        this.transactionTypeChangeList = response;
       // if (this.client_account_type === '590' || this.client_account_type === '592' ) {
          this.transactionTypeChangeList =  response.filter(transactionSource => {
            return (transactionSource.picklist_value_cd === '588' || transactionSource.picklist_value_cd === '589');
          });
      //  }
      }
    });

    const transactionType = transaction.transaction_type_cd;
   // this.transactionSource$.subscribe(response => {
      if (this.transactionSource) {
        this.transactionSourceChangeList = this.transactionSource;
        const conservedReceipt = ['583', '587', '586', '5479', '584', '585', '5471', '5481', '5482'];
        const adjustment = ['5472', '5473'];
        const fosterReceipt = ['5479', '584', '5481', '5479', '5482', '5471', '585', '588', '589' ];
        const dedicatedReceipt = ['587', '586', '585', '5471'];

        if (this.client_account_type === '590' && transactionType === '589' ) {
            this.transactionSourceChangeList =  this.transactionSource.filter(transactionData => {
              return ( conservedReceipt.indexOf(transactionData.picklist_value_cd) > -1);
            });
        } else if (this.client_account_type === '592' && transactionType === '589' ) {
          this.transactionSourceChangeList =  this.transactionSource.filter(transactionData => {
            return ( fosterReceipt.indexOf(transactionData.picklist_value_cd) > -1);
          });
        } else if (this.client_account_type === '591' && transactionType === '589' ) {
          this.transactionSourceChangeList =  this.transactionSource.filter(transactionData => {
            return ( dedicatedReceipt.indexOf(transactionData.picklist_value_cd) > -1);
          });
        } else if ((this.client_account_type === '590' || this.client_account_type === '591' || this.client_account_type === '592')
                    && transactionType === '588' ) {
          this.transactionSourceChangeList =  this.transactionSource.filter(transactionData => {
            return ( adjustment.indexOf(transactionData.picklist_value_cd) > -1);
          });
        }
      }
    // });

  //  setTimeout( () => {
      this.selectedTransaction = transaction;
      this.editTransactionForm.patchValue(transaction);
      // this.editTransactionForm.get('reference_transaction_id').setValue(transaction.transaction_id);
      // this.editTransactionForm.get('reference_transaction_id').disable();
      this.editTransactionForm.get('transaction_type_cd').enable();
      this.editTransactionForm.get('transaction_source_cd').enable();
      this.editTransactionForm.get('frequency_cd').enable();
      // this.editTransactionForm.get('benefit_start_dt').enable();
      this.editTransactionForm.get('benefit_end_dt').enable();
      this.editTransactionForm.get('transaction_amount_no').enable();
      this.editTransactionForm.get('notes_tx').enable();

      if (this.clientAccount.status_cd === '593') {
        this.editTransactionForm.disable();
      }
   // }, 200);
  } */

  deleteTransaction(transaction) {
    this.selectedTransaction = transaction;
  }

  deleteItem() {
    this._commonService.create(
      {
        'commTransactionId': this.selectedTransaction.comm_transaction_id
      },
      FinanceUrlConfig.EndPoint.interestTransactions.deleteTransaction).subscribe(
        res => {
          this._alertService.success('Transaction deleted successfully');
          (<any>$('#deleteTransaction')).modal('hide');
          this.getTransactionslist();
        },
        err => { }
      );
  }

  openAddModal() {
    this.transactionType$.subscribe(response => {
      if (response) {
        this.transactionTypeChangeList = response;
        // if (this.client_account_type === '590' || this.client_account_type === '592' ) {
        this.transactionTypeChangeList = response.filter(transactionSource => {
          return (transactionSource.picklist_value_cd === '588' || transactionSource.picklist_value_cd === '589');
        });
        //  }
      }
    });

    if (this.clientAccount) {
      this.addForm.reset();
      this.addForm.patchValue({
        transaction_dt: new Date(),
        credit_debit_sw: 'C',
        recurring_transaction: 'N'
      });
      (<any>$('#addNewTransaction')).modal('show');
    } else {
      this._alertService.warn('Please Select the child account number');
    }
  }

  private getDropdowns(client_id) {
    const source = forkJoin([
      this._commonService.getArrayList(new PaginationRequest({
        where: {
          'picklist_type_id': '39'
        },
        nolimit: true,
        method: 'get'
      }), FinanceUrlConfig.EndPoint.childAccounts.pickListUrl)
    ])
      .map((result) => {

        return {
          transactionType: result[0]
        };
      })
      .share();
    this.transactionType$ = source.pluck('transactionType');
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'client_account_id': client_id ? client_id : null
      },
      nolimit: true,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.childAccounts.transactionSource + '?filter')
      .subscribe(response => {
        if (response) {
          this.transactionSource = response['data'];
        }
      });
  }

  getchildaccountslist(page = 1) {
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: { client_id: this.childdetails.client_id,
        istransaction: true },
      page: page,
      limit: 50,
      method: 'get'
    }), FinanceUrlConfig.EndPoint.childAccounts.getchildaccountslistUrl + '?filter').subscribe(result => {
      if (result) {
        this.childaccountslist = result['data'];
        if (this.clientAccount) {
          this.clientAccount = this.childaccountslist.find(data => data.account_no_tx === this.clientAccount.account_no_tx);
        }
        if (this.accountDetails) {
          this.clientAccount = this.childaccountslist.find(data => data.client_account_id === this.accountDetails.client_account_id);
          this.getTransactionslist();
        }
        console.log('this.clientAccount', this.clientAccount);
      }
    });
  }

  transactionModeChange(id) {
    this.addForm.get('credit_debit_sw').reset();
    const transactionType = id;
    if (transactionType === '588') {
      this.receiptsType = false;
      this.addForm.get('credit_debit_sw').setValue('D');
      this.addForm.get('recurring_transaction').reset();
      this.addForm.get('recurring_transaction').setValue('N');
      this.addForm.get('recurring_transaction').disable();
    } else if (transactionType === '589') {
      this.receiptsType = true;
      // this.addForm.get('recurring_transaction').reset();
      this.addForm.get('recurring_transaction').enable();
      this.addForm.get('credit_debit_sw').setValue('C');
    }
    this.addForm.get('credit_debit_sw').disable();

    // if (this.client_account_type === '590' && transaction_type === '589') {

    // }

    // this.transactionSource.subscribe(response => {
    if (this.transactionSource) {
      this.transactionSourceChangeList = this.transactionSource;
      const conservedReceipt = ['583', '587', '586', '5479', '584', '585', '5471', '5482', '5487'];
      const adjustment = ['5472'];
      const fosterReceipt = ['5479', '584', '5481', '5479', '5482', '5486', '588', '589'];
      const dedicatedReceipt = ['5488', '5489', '5490', '5486'];



      if (this.client_account_type === '590' && transactionType === '589') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (conservedReceipt.indexOf(transaction.picklist_value_cd) > -1);
        });
      } else if (this.client_account_type === '592' && transactionType === '589') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (fosterReceipt.indexOf(transaction.picklist_value_cd) > -1);
        });
      } else if (this.client_account_type === '591' && transactionType === '589') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (dedicatedReceipt.indexOf(transaction.picklist_value_cd) > -1);
        });
      } else if ((this.client_account_type === '590' || this.client_account_type === '591' || this.client_account_type === '592')
        && transactionType === '588') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (adjustment.indexOf(transaction.picklist_value_cd) > -1);
        });
      }
    }
    // });

  }

  transactionSourceChange(source, mode) {
    if (mode === 'add') {
      if (source === '5473') {
        this.approvalRequest = true;
      } else {
        this.approvalRequest = false;
      }
      if (source === '585' || source === '586' || source === '587') {
        this.transSource = true;
        const currDate = new Date();
        const year = currDate.getFullYear();
        const month = currDate.getMonth();
        const day = currDate.getDate();
        this.minTransDate = new Date(year - 2, month, day);
        if (this.dateMin < this.minTransDate) {
          this.dateMin = this.minTransDate;
        }
        this.transOtherSource = false;
        this.addForm.get('benefit_start_dt').reset();
        this.addForm.get('benefit_end_dt').reset();
        this.benefitEndDt = false;
        this.benefitStartDt = false;
        this.addForm.get('benefit_start_dt').enable();
        this.addForm.get('benefit_end_dt').enable();
      } else if (this.receiptsType) {
        this.dateMin = this.dateMinBk;
        this.transSource = false;
        this.transOtherSource = true;
        this.addForm.get('benefit_start_dt').reset();
        this.addForm.get('benefit_end_dt').reset();
        this.benefitEndDt = false;
        this.benefitStartDt = false;
        this.addForm.get('benefit_start_dt').enable();
        this.addForm.get('benefit_end_dt').enable();
      } else {
        this.dateMin = this.dateMinBk;
        this.transOtherSource = false;
        this.addForm.get('benefit_start_dt').reset();
        this.addForm.get('benefit_end_dt').reset();
        this.benefitEndDt = false;
        this.benefitStartDt = false;
        this.addForm.get('benefit_start_dt').enable();
        this.addForm.get('benefit_end_dt').enable();
        this.transSource = false;
      }
    } else if (mode === 'edit') {
      if (source === '585' || source === '586' || source === '587') {
        this.transSource = true;
        this.editTransactionForm.get('benefit_start_dt').reset();
        this.editTransactionForm.get('benefit_end_dt').reset();
      } else {
        this.editTransactionForm.get('benefit_start_dt').reset();
        this.editTransactionForm.get('benefit_end_dt').reset();
        this.transSource = false;
      }
    }
  }

  confirmApproval(mode) {
    if (mode === 'edit') {
      if (this.editTransactionForm.valid) {
        this.addTransctionForm = this.editTransactionForm.getRawValue();
        this.addTransctionForm.reference_transaction_id = this.editTransctionForm.transaction_id;
        this.addTransctionForm.recurring_transaction = this.editTransactionForm.get('frequency_cd').value;
        if ( this.editTransactionForm.getRawValue().benefit_start_dt && this.editTransactionForm.getRawValue().benefit_end_dt) {
          const startDate = moment(new Date(this.editTransactionForm.getRawValue().benefit_start_dt)).format('YYYY-MM-DD');
          const endDate = moment(new Date(this.editTransactionForm.getRawValue().benefit_end_dt)).format('YYYY-MM-DD');
          if ( new Date(startDate).getTime() > new Date(endDate).getTime()) {
            this._alertService.error('Benefit End Date should be greater than or equal to Benefit Start Date');
            return false;
          }
        }
        if (this.addTransctionForm.transaction_amount_no <= 0) {
          this._alertService.error('Amount should be greater then $0.00');
          return false;
        } else if (this.addTransctionForm.credit_debit_sw === 'D' && +this.addTransctionForm.transaction_amount_no > this.allowedErrorCorrection) {
          this._alertService.error('Amount should be less then $' + this.allowedErrorCorrection);
          return false;
        }
        if (this.addTransctionForm.transaction_type_cd === '588') {
          if (+this.addTransctionForm.transaction_amount_no > +this.clientAccount.total_balance_no) {
            this._alertService.error('Adjustment Amount should be within the total balance');
            return false;
          }
        }
      } else {
        this._alertService.warn('Please fill the mandatory details.');
        return false;
      }
    } else if (mode === 'send') {
      if (this.addForm.valid) {
        this.addTransctionForm = this.addForm.getRawValue();
        if (this.addForm.getRawValue().benefit_start_dt && this.addForm.getRawValue().benefit_end_dt) {
          const startDate = moment(new Date(this.addForm.getRawValue().benefit_start_dt)).format('YYYY-MM-DD');
          const endDate = moment(new Date(this.addForm.getRawValue().benefit_end_dt)).format('YYYY-MM-DD');
          if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
            this._alertService.error('Benefit End Date should be greater than or equal to Benefit Start Date');
            return false;
          }
        }
        if (this.addTransctionForm.transaction_amount_no <= 0) {
          this._alertService.error('Amount should be greater then $0.00');
          return false;
        }
        if (this.addTransctionForm.transaction_type_cd === '588') {
          if (+this.addTransctionForm.transaction_amount_no > +this.clientAccount.total_balance_no) {
            this._alertService.error('Adjustment Amount should be within the total balance');
            return false;
          }
        }
      } else {
        this._alertService.warn('Please fill the mandatory details.');
        return false;
      }
    } else {
      if (this.editTransactionForm.valid) {
        if (this.addTransctionForm.transaction_amount_no <= 0) {
          this._alertService.error('Amount should be greater then $0.00');
          return false;
        }
        this.addTransctionForm.transaction_amount_no = this.editTransactionForm.get('transaction_amount_no').value;
      } else {
        this._alertService.warn('Please fill the mandatory details.');
        return false;
      }
    }
    this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'PCAUTH' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe((result) => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser('TOBEASSIGNED');
      });
    (<any>$('#addNewTransaction')).modal('hide');
    (<any>$('#editTransaction')).modal('hide');
  }


  selectPerson(row) {
    if (row) {
      this.selectedPerson = row;
      this.assignedTo = row.userid;
    }
  }

  listUser(assigned: string) {
    this.selectedPerson = '';
    this.getUsersList = [];
    this.getUsersList = this.originalUserList;
    if (assigned === 'TOBEASSIGNED') {
      this.getUsersList = this.getUsersList.filter((res) => {
        if (res.userrole === 'LDSS Fiscal Supervisor' && res.userid !== this.userProfile.user.securityusersid) {
          (<any>$('#adjustment-approval')).modal('show');
          return res;
        }
      });
    }
  }

  editTransaction(transaction, mode) {
    this.isAdjust = false;
    this.addTransctionForm = transaction;
    this.editTransactionForm.patchValue(transaction);
    if (mode === 'fs') {
      (<any>$('#editTransaction')).modal('show');
      this.editTransactionForm.disable();
      this.buttonTxt = 'Approve';
    } else {
      if (transaction.status_type === 83) {
        this.editTransactionForm.disable();
        this.editTransactionForm.get('transaction_amount_no').enable();
        this.buttonTxt = 'Resend For Approval';
      } else {
        this.editTransactionForm.enable();
        this.buttonTxt = 'Send For Approval';
      }
    }
  }
  editTransactionAdjustment(transaction) {
    this.allowedErrorCorrection = (transaction && transaction.error_correction_amt) ? transaction.error_correction_amt : '0.00';
    this.buttonTxt = 'Send For Approval';
    this.isAdjust = true;
    this.transSource = false;
    this.transOtherSource = false;
    this.editTransctionForm = transaction;
    this.actualCost = transaction.transaction_amount_no;
    this.editTransactionForm.patchValue(transaction);
    this.editTransactionForm.patchValue({
      frequency_cd: 'N',
      transaction_type_cd: '588',
      transaction_source_cd: '5473',
      transaction_amount_no: this.allowedErrorCorrection
    });
    this.editTransactionForm.get('frequency_cd').disable();
    this.editTransactionForm.get('transaction_type_cd').disable();
    this.editTransactionForm.get('transaction_source_cd').disable();
    this.editTransactionForm.get('transaction_dt').disable();
    this.editTransactionForm.get('credit_debit_sw').enable();
    this.editTransactionForm.get('benefit_start_dt').enable();
    this.benefitEndDt = false;
    this.benefitStartDt = false;
  }

  transModeSwitch(value) {
    if (value === 'D') {
      const actual_cost = this.actualCost;
        if (this.editTransactionForm.get('transaction_amount_no').value) {
          if (parseFloat(this.editTransactionForm.get('transaction_amount_no').value) > actual_cost) {
            this._alertService.warn('Not allowed to add more than the actual cost');
            this.editTransactionForm.patchValue({
              transaction_amount_no : this.actualCost
            });
          }
        }
      }
  }
  confirmReject() {
    this.reason_tx = '';
    (<any>$('#editTransaction')).modal('hide');
    (<any>$('#reject-approval')).modal('show');
  }

  rejectAdjustment() {
    this._commonService.endpointUrl = FinanceUrlConfig.EndPoint.interestTransactions.adjustmentReject;
    const modal = {
      reason_tx: this.reason_tx,
      transaction_id: this.addTransctionForm.transaction_id,
      account_no_tx: this.clientAccount.account_no_tx
    };

    this._commonService.create(modal).subscribe(
      (response) => {
        (<any>$('#rejectApproval')).modal('hide');
        this._alertService.success('Adjustment Error Correction Rejected Successfully');
        this.getTransactionslist();
        this.getchildaccountslist();
      },
      (error) => {
        this._alertService.success(GLOBAL_MESSAGES.ERROR_MESSAGE);
      });
  }

  supervisorApproval() {
    if (this.isFW) {
      this.comments = 'Approval request for Error Correction for the Client Account ID(' + this.clientAccount.client_account_id + ')';
      this.alertTxt = 'Approval Request sent successfully';
      this.url = FinanceUrlConfig.EndPoint.interestTransactions.adjustmentRequest;
    } else if (this.isFS) {
      this.comments = 'Approved Error Correction for the Client Account ID(' + this.clientAccount.client_account_id + ')';
      this.alertTxt = 'Approved Adjustment - Error Correction successfully';
      this.url = FinanceUrlConfig.EndPoint.interestTransactions.adjustmentApproval;
    }
      this._commonService.create({
        'transaction_dt': new Date(),
        'transaction_type_cd': this.addTransctionForm.transaction_type_cd,
        'frequency_cd': this.addTransctionForm.recurring_transaction,
        'transaction_source_cd': this.addTransctionForm.transaction_source_cd,
        'reference_transaction_id': this.isFW ? this.addTransctionForm.reference_transaction_id : null,
        'benefit_start_dt': this.datePipe.transform(this.addTransctionForm.benefit_start_dt, 'yyyy-MM-dd'),
        'benefit_end_dt': this.datePipe.transform(this.addTransctionForm.benefit_end_dt, 'yyyy-MM-dd'),
        'transaction_amount_no': this.addTransctionForm.transaction_amount_no,
        'credit_debit_sw': this.addTransctionForm.credit_debit_sw,
        'notes_tx': this.addTransctionForm.notes_tx,
        'delete_sw': 'N',
        'client_account_id': this.clientAccount.client_account_id,
        'late_entry_sw': null,
        'assignedtoid': this.assignedTo,
        'intakeserviceid': this.clientAccount.intakeserviceid,
        'eventcode': 'CACCTRANS',
        'status': this.status,
        'comments': this.comments,
        notifymsg : this.comments,
        routeddescription: this.comments,
        transaction_id: this.addTransctionForm.transaction_id ? this.addTransctionForm.transaction_id : null
      }, this.url).subscribe(result => {
          if (result) {
            this._alertService.success(this.alertTxt);
            this.getchildaccountslist();
            this.getTransactionslist();
            (<any>$('#adjustment-approval')).modal('hide');
            (<any>$('#editTransaction')).modal('hide');
          } else {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          }
        });
  }

  beneficiaryStartDate = (d: Date): boolean => {
    const month = this.dateMin.getMonth().toString();
    const year = this.dateMin.getFullYear().toString();
    const minday = this.dateMin.getDate();
    const day = new Date(d).getDate();
    const startdate = new Date(d.getFullYear(), d.getMonth(), 1);
    this.monthStart = new Date(d.getFullYear(), d.getMonth() + 1, 0);
    const start = new Date(startdate).getDate();
    if (+year === +d.getFullYear().toString()) {
      if (+month === +d.getMonth().toString()) {
        this.dateMinCheck = true;
        return (day === minday);
      } else {
        this.dateMinCheck = false;
        return (day === start);
      }
    } else {
      this.dateMinCheck = false;
      return (day === start);
    }
  }

  beneficiaryEndDate = (d: Date): boolean => {
    const day = new Date(d).getDate();
    const endDate = this.monthStart;
    const end = new Date(endDate).getDate();
    return (day === end);
  }

  editTransactionModeChange(id) {
    // this.editTransactionForm.get('credit_debit_sw').reset();
    const transactionType = id;
    if (transactionType === '588') {
      this.editTransactionForm.get('credit_debit_sw').setValue('D');
    } else if (transactionType === '589') {
      this.editTransactionForm.get('credit_debit_sw').setValue('C');
    }
    this.editTransactionForm.get('credit_debit_sw').disable();

    // if (this.client_account_type === '590' && transaction_type === '589') {

    // }

    // this.transactionSource$.subscribe(response => {
    if (this.transactionSource) {
      this.transactionSourceChangeList = this.transactionSource;
      const conservedReceipt = ['583', '587', '586', '5479', '584', '585', '5471', '5482', '5487'];
      const adjustment = ['5472', '5473'];
      const fosterReceipt = ['5479', '584', '5481', '5479', '5482', '5486', '588', '589'];
      const dedicatedReceipt = ['5488', '5489', '5490', '5486'];



      if (this.client_account_type === '590' && transactionType === '589') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (conservedReceipt.indexOf(transaction.picklist_value_cd) > -1);
        });
      } else if (this.client_account_type === '592' && transactionType === '589') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (fosterReceipt.indexOf(transaction.picklist_value_cd) > -1);
        });
      } else if (this.client_account_type === '591' && transactionType === '589') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (dedicatedReceipt.indexOf(transaction.picklist_value_cd) > -1);
        });
      } else if ((this.client_account_type === '590' || this.client_account_type === '591' || this.client_account_type === '592')
        && transactionType === '588') {
        this.transactionSourceChangeList = this.transactionSource.filter(transaction => {
          return (adjustment.indexOf(transaction.picklist_value_cd) > -1);
        });
      }
    }
    // });

  }

  onSorted($event: ColumnSortedEvent){

  }

  onHistorySorted($event: ColumnSortedEvent) {
    this.paginationInfo.sortBy = $event.sortDirection;
    let sortOrder:any = $event.sortDirection;
    this.paginationInfo.sortColumn = $event.sortColumn;
    this.childtransactionslist = _.orderBy(this.childtransactionslist, [ $event.sortColumn],[sortOrder]);
  //this.getTransactionslist();
    //this.getchildaccountslist();
}

  checkDec(el) {
    if (el.target.value !== '') {
      return el.target.value = isNaN(el.target.value) ? '' : el.target.value.replace(/[^0-9.]{0,2}/g, '');
    }
    return '';
  }

  // sortData(sort: Sort) {
  //   const data = this.childtransactionslist.slice();
  //   if (!sort.active || sort.direction === '') {
  //     this.sortedData = data;
  //     return;
  //   }

  //   this.sortedData = data.sort((a, b) => {
  //     const isAsc = sort.direction === 'asc';
  //     switch (sort.active) {
  //       case 'transactionid': return compare(a.transaction_id, b.transaction_id, isAsc);
  //       case 'entrydate': return compare(a.entrydate, b.entrydate, isAsc);
  //       case 'benefitancillarymo/yrstartdate': return compare(a.benefit_start_dt, b.benefit_start_dt, isAsc);
  //       case 'transactiontype': return compare(a.transactiontype, b.transactiontype, isAsc);
  //       case 'transactionstatus': return compare(a.transactionstatus, b.transactionstatus, isAsc);
  //       case 'source': return compare(a.source, b.source, isAsc);
  //       case 'amount': return compare(a.amount, b.amount, isAsc);
  //       case 'status': return compare(a.status, b.status, isAsc);
  //       case 'action': return compare(a.action, b.action, isAsc);

  //       default: return 0;
  //     }
  //   });
  // }
}

// function compare(a: number | string, b: number | string, isAsc: boolean) {
//   return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
// }
