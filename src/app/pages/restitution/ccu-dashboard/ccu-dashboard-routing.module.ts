import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CcuDashboardComponent } from './ccu-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: CcuDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CcuDashboardRoutingModule { }
