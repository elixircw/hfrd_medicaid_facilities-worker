import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CcuDashboardRoutingModule } from './ccu-dashboard-routing.module';
import { CcuDashboardComponent } from './ccu-dashboard.component';
import { PaginationModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { MatTooltipModule } from '@angular/material';
import { GenerateReportsService } from '../../generate-reports/generate-reports.service';

@NgModule({
  imports: [
    CommonModule,
    CcuDashboardRoutingModule,
    PaginationModule,
    FormMaterialModule,
    MatTooltipModule,
  ],
  declarations: [CcuDashboardComponent],
  providers: [GenerateReportsService]
})
export class CcuDashboardModule { }
