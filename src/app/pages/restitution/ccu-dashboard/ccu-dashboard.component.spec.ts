import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcuDashboardComponent } from './ccu-dashboard.component';

describe('CcuDashboardComponent', () => {
  let component: CcuDashboardComponent;
  let fixture: ComponentFixture<CcuDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcuDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcuDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
