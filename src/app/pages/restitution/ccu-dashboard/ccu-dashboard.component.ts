import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../@core/entities/common.entities';
import { CommonUrlConfig } from '../../../@core/common/URLs/common-url.config';
import { CommonHttpService, DataStoreService, AuthService, AlertService } from '../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { AppUser } from '../../../@core/entities/authDataModel';
import { GLOBAL_MESSAGES } from '../../../@core/entities/constants';
import { IntakeStoreConstants } from '../../newintake/my-newintake/my-newintake.constants';
import { GenerateReportsService } from '../../generate-reports/generate-reports.service';

@Component({
  selector: 'ccu-dashboard',
  templateUrl: './ccu-dashboard.component.html',
  styleUrls: ['./ccu-dashboard.component.scss']
})
export class CcuDashboardComponent implements OnInit {

  cculist: any = [];
  totalCount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  status: string;
  userRole: AppUser;
  isDjs: boolean;
  constructor(private _commonService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute,
    private _store: DataStoreService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _generateReport: GenerateReportsService) { }

  ngOnInit() {
    this.userRole = this._authService.getCurrentUser();
    this.isDjs = this._authService.isDJS();
    if (this.userRole.role.name === 'FU') {
      this.status = 'approved';
    } else {
      this.status = 'pending';
    }
    this.paginationInfo.pageNumber = 1;
    this.loadCculist(this.status);
  }

  loadCculist(status: string) {
    this.status = status;
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.CCU.DASHBOARD_LIST + '?data'
    ).subscribe((result: any) => {
      if (result) {
        this.cculist = result.data;
        this.totalCount = result.data.length ? result.count : 0;
      }
    });
  }

  ApproveReject(model: any, action: number) {
    const requestObj = {
      intakeserreqrestitutionid: model.intakeserreqrestitutionid,
      restitutionno: model.restitutionno,
      approvestatus: status === 'Approve' ? 'RSTAPR' : 'RSTRJT'
    };
    // this._commonService.create(requestObj,
    //   CommonUrlConfig.EndPoint.RESTITUTION.CHANGE_FORM.APPROVE_REJECT).subscribe(success => {
    //     this._alertService.success('Change Form Approved successfully.');
    //     setTimeout(() => {
    //       this.loadCculist(this.status);
    //     }, 100);
    //   }, error => {
    //     this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    //   });
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadCculist(this.status);
  }

  viewDetails(data: any, status) {
    this._store.setData(IntakeStoreConstants.intakenumber, data.intakenumber);
    this.router.navigate([`/pages/restitution/${data.intakeserreqrestitutionid}/${data.intakeserviceid}`],
      { relativeTo: this.route, queryParams: { status: status, IsCCUenable: data.isccusent, intakeFrom: data.restitutiontype, pageFrom: 'CCU' } });
  }

  generateReport(filetype) {
    this._generateReport.generateDocument('ccuList',
      new PaginationRequest({
        nolimit: true,
        method: 'get',
        where: {
          outputfilename: 'CCU_List_Report',
          type: filetype ? filetype : 'pdf'
        }
      }));
  }

}
