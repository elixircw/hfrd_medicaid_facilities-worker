import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // {
  //   path: 'reports',
  //   loadChildren: './restitution-reports/restitution-reports.module#RestitutionReportsModule'
  // },
  {
    path: 'djs-finance-dashboard',
    loadChildren: './djs-finance-dashboard/djs-finance-dashboard.module#DjsFinanceDashboardModule'
  },
  {
    path: 'lockboxpayments',
    loadChildren: './lockboxpayments/lockboxpayments.module#LockboxpaymentsModule'
  },
  {
    path: ':paymentid/payment',
    loadChildren: './payment-details/payment-details.module#PaymentDetailsModule'
  },
  {
    path: ':restituionid/:id',
    loadChildren: './restitution-details/restitution-details.module#RestitutionDetailsModule'
  },
  {
    path: 'ccu-dashboard',
    loadChildren: './ccu-dashboard/ccu-dashboard.module#CcuDashboardModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestitutionRoutingModule { }
