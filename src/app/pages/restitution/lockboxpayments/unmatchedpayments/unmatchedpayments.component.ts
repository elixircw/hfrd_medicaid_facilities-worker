import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'unmatchedpayments',
  templateUrl: './unmatchedpayments.component.html',
  styleUrls: ['./unmatchedpayments.component.scss']
})
export class UnmatchedpaymentsComponent implements OnInit {
  checkReglist = [];
  totalcheckList: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  areaSearch = '';

  constructor(private _commonService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.paginationInfo.pageNumber = 1;
    this.loadList({ matched: false });
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    if (this.areaSearch) {
      this.loadList({ vakaccountnumber: this.areaSearch, matched: false });
    } else {
      this.loadList({ matched: false });
    }
  }

  loadList(whereReq) {
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get',
        where: whereReq
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.LOCKBOX.MATCHEDPAYMENTLIST + '?data'
    ).subscribe((result: any) => {
      if (result) {
        this.checkReglist = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalcheckList = result.count;
        }
      }

    });
  }

  onSearch(mode, value) {
    if (mode === 'AreaCode') {
      if (value === '' || value.length > 4) {
        this.paginationInfo.pageNumber = 1;
        this.loadList({ vakaccountnumber: value, matched: false });
      }
    }
  }

  resetList() {
    this.checkReglist = [];
    this.totalcheckList = 0;
  }

  openPaymentDetails(payment: any) {
    this.router.navigate([`/pages/restitution/${payment.restitutionpaymentflatfilecontentid}/payment`], {
      relativeTo: this.route
    });
  }
}
