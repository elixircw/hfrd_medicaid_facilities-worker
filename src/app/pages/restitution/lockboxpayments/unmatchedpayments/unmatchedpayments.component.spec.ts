import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnmatchedpaymentsComponent } from './unmatchedpayments.component';

describe('UnmatchedpaymentsComponent', () => {
  let component: UnmatchedpaymentsComponent;
  let fixture: ComponentFixture<UnmatchedpaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnmatchedpaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnmatchedpaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
