import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatchedpaymentsComponent } from './matchedpayments/matchedpayments.component';
import { UnmatchedpaymentsComponent } from './unmatchedpayments/unmatchedpayments.component';
import { CheckregisterComponent } from './checkregister/checkregister.component';
import { LockboxpaymentsComponent } from './lockboxpayments.component';
import { PaymentaccountsComponent } from './paymentaccounts/paymentaccounts.component';

const routes: Routes = [
  {
    path: '',
    component: LockboxpaymentsComponent,
    children: [
      {
        path: 'checkregister',
        component: CheckregisterComponent
      },
      {
        path: 'matchedpayments',
        component: MatchedpaymentsComponent
      },
      {
        path: 'unmatchedpayments',
        component: UnmatchedpaymentsComponent
      },
      {
        path: 'allocated-payments',
        loadChildren: '../djs-finance-dashboard/djs-finance-dashboard.module#DjsFinanceDashboardModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LockboxpaymentsRoutingModule { }
