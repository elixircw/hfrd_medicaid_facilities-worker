import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'matchedpayments',
  templateUrl: './matchedpayments.component.html',
  styleUrls: ['./matchedpayments.component.scss']
})
export class MatchedpaymentsComponent implements OnInit {
  checkReglist = [];
  totalcheckList: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  areaSearch = '';

  constructor(private _commonService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.paginationInfo.pageNumber = 1;
    this.loadList({ matched: true });
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    if (this.areaSearch) {
      this.loadList({ vakaccountnumber: this.areaSearch, matched: true });
    } else {
      this.loadList({ matched: true });
    }
  }

  loadList(whereReq) {
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get',
        where: whereReq
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.LOCKBOX.MATCHEDPAYMENTLIST + '?data'
    ).subscribe((result: any) => {
      if (result) {
        this.checkReglist = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalcheckList = result.count;
        }
      }

    });
  }

  resetList() {
    this.checkReglist = [];
    this.totalcheckList = 0;
    this.areaSearch = '';
  }

  onSearch(mode, value) {
    if (mode === 'AreaCode') {
      if (value === '' || value.length > 4) {
        this.areaSearch = value;
        this.paginationInfo.pageNumber = 1;
        this.loadList({ vakaccountnumber: value, matched: true });
      }
    }
  }

  openPaymentDetails(payment: any) {
    this.router.navigate([`/pages/restitution/${payment.restitutionpaymentflatfilecontentid}/payment`], {
      relativeTo: this.route
    });
  }
}
