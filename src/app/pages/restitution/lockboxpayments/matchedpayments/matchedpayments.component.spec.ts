import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchedpaymentsComponent } from './matchedpayments.component';

describe('MatchedpaymentsComponent', () => {
  let component: MatchedpaymentsComponent;
  let fixture: ComponentFixture<MatchedpaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchedpaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchedpaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
