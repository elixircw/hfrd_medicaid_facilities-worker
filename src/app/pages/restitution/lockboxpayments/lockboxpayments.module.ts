import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LockboxpaymentsRoutingModule } from './lockboxpayments-routing.module';
import { LockboxpaymentsComponent } from './lockboxpayments.component';
import { MatchedpaymentsComponent } from './matchedpayments/matchedpayments.component';
import { UnmatchedpaymentsComponent } from './unmatchedpayments/unmatchedpayments.component';
import { CheckregisterComponent } from './checkregister/checkregister.component';
import { PaginationModule } from 'ngx-bootstrap/pagination/pagination.module';
import { FormsModule } from '@angular/forms';
import { PaymentaccountsComponent } from './paymentaccounts/paymentaccounts.component';

@NgModule({
  imports: [
    CommonModule,
    LockboxpaymentsRoutingModule,
    PaginationModule,
    FormsModule
  ],
  declarations: [LockboxpaymentsComponent, MatchedpaymentsComponent, UnmatchedpaymentsComponent, CheckregisterComponent, PaymentaccountsComponent]
})
export class LockboxpaymentsModule { }
