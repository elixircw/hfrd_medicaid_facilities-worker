import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'checkregister',
  templateUrl: './checkregister.component.html',
  styleUrls: ['./checkregister.component.scss']
})
export class CheckregisterComponent implements OnInit {
  checkReglist = [];
  totalcheckList: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  areaSearch = '';
  constructor(private _commonService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.paginationInfo.pageNumber = 1;
    this.loadList();
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadList({ searchfile: this.areaSearch });
  }

  loadList(whereReq?) {
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get',
        where: whereReq
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.LOCKBOX.CHECKREGLIST + '?data'
    ).subscribe((result: any) => {
      if (result) {
        this.checkReglist = result.data;
        this.totalcheckList = result.data.length ? result.data[0].totalcount : 0;
      }

    });
  }

  toggleTable(id) {
    (<any>$('#' + id)).collapse('toggle');
  }

  resetList() {
    this.checkReglist = [];
    this.totalcheckList = 0;
  }

  onSearch(mode, value) {
    if (mode === 'AreaCode') {
      if (value === '' || value.length > 4) {
        this.areaSearch = value;
        this.paginationInfo.pageNumber = 1;
        this.loadList({ searchfile: value });
      }
    }
  }

}
