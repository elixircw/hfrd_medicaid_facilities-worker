import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LockboxpaymentsComponent } from './lockboxpayments.component';

describe('LockboxpaymentsComponent', () => {
  let component: LockboxpaymentsComponent;
  let fixture: ComponentFixture<LockboxpaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LockboxpaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockboxpaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
