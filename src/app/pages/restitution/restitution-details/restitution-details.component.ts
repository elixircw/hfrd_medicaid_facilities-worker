import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'restitution-details',
  templateUrl: './restitution-details.component.html',
  styleUrls: ['./restitution-details.component.scss']
})
export class RestitutionDetailsComponent implements OnInit {
  tabs = [
    {
      id: 'person',
      title: 'Persons Involved',
      name: 'Persons Involved',
      route: 'person'
    },
    // {
    //   id: 'transfer',
    //   title: 'Transfer Of Jurisdiction',
    //   name: 'Transfer Of Jurisdiction',
    //   route: 'transfer'
    // },
    {
      id: 'changeform',
      title: 'Change Form',
      name: 'Change Form',
      route: 'changeform'
    },
    {
      id: 'courtaction',
      title: 'Court Action',
      name: 'Court Action',
      route: 'courtaction'
    },
    {
      id: 'document',
      title: 'Documents',
      name: 'Documents',
      route: 'document'
    },
    {
      id: 'decision',
      title: 'Close Restitution',
      name: 'Close Restitution',
      route: 'decision'
    }
  ];
  tabStatus: any;
  RESTITUION_STATUS = {
    INACTIVE: 'Inactive',
    ACTIVE: 'Active',
    CLOSED: 'Closed'
  };
  intakeisfrom = '';
  pageSource: any;

  constructor(private _route: ActivatedRoute) {
    _route.queryParams.subscribe(result => {
      this.tabStatus = result['status'];
      this.intakeisfrom = result['intakeFrom'];
      this.pageSource = result['pageFrom'];
    });
  }

  ngOnInit() {
    if (this.pageSource === 'CCU') {
      this.tabs = this.tabs.filter(data => (data.id !== 'changeform' && data.id !== 'decision'));
    } else {
      if (this.tabStatus === this.RESTITUION_STATUS.CLOSED) {
        this.tabs = this.tabs.filter(data => (data.id !== 'changeform' && data.id !== 'decision'));
      }
      if (this.intakeisfrom !== 'court') {
        this.tabs = this.tabs.filter(data => (data.id !== 'courtaction'));
      }
    }
  }

}
