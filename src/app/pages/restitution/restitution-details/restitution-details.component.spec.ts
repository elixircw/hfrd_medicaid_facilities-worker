import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestitutionDetailsComponent } from './restitution-details.component';

describe('RestitutionDetailsComponent', () => {
  let component: RestitutionDetailsComponent;
  let fixture: ComponentFixture<RestitutionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestitutionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestitutionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
