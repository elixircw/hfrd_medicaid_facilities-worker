import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestitutionDetailsRoutingModule } from './restitution-details-routing.module';
import { RestitutionDetailsComponent } from './restitution-details.component';
import { PaymentScheduleModule } from '../../shared-pages/payment-schedule/payment-schedule.module';
import { InvolvedPersonsModule } from '../../case-worker/dsds-action/involved-persons/involved-persons.module';
import { DsdsService } from '../../case-worker/dsds-action/_services/dsds.service';
import { IntakeConfigService } from '../../newintake/my-newintake/intake-config.service';
import { RestitutionDecisionComponent } from './restitution-decision/restitution-decision.component';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RestitutionDetailsRoutingModule,
    PaymentScheduleModule,
    InvolvedPersonsModule,
    QuillModule,
    PaginationModule,
    FormMaterialModule,
    ReactiveFormsModule
  ],
  declarations: [RestitutionDetailsComponent, RestitutionDecisionComponent],
  providers: [DsdsService, IntakeConfigService]
})
export class RestitutionDetailsModule { }
