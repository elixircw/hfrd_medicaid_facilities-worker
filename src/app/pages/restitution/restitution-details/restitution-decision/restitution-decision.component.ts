import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, CommonDropdownsService, DataStoreService, AuthService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AppConstants } from '../../../../@core/common/constants';

@Component({
  selector: 'restitution-decision',
  templateUrl: './restitution-decision.component.html',
  styleUrls: ['./restitution-decision.component.scss']
})
export class RestitutionDecisionComponent implements OnInit {
  decisionForm: FormGroup;
  reasonypeList = [];
  id: any;
  restituionid: any;
  userInfo: AppUser;

  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService,
    private _alertService: AlertService, private _dropDownService: CommonDropdownsService,
    private _dataStoreService: DataStoreService, private route: ActivatedRoute,
    private _authService: AuthService) {
    this.id = this.route.snapshot.parent.parent.params['id'];
    this.restituionid = this.route.snapshot.parent.parent.params['restituionid'];
  }

  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    if (this.userInfo.role.name !== AppConstants.ROLES.RESTITUTION_COORDINATOR) {
      (<any>$(':button')).prop('disabled', true);
      (<any>$('.ccuapprovebutton')).prop('disabled', false);
    }
    this.loadDropdowns();
    this.initFormGroup();
  }

  loadDropdowns() {
    this._dropDownService.getDropownsByTable('RestitutionCloseReasonType').subscribe(data => {
      this.reasonypeList = data;
    });
  }

  initFormGroup() {
    this.decisionForm = this.formBuilder.group({
      comments: [null],
      reasontype: [null]
    });
  }

  saveDecision(modal) {
    const restitutionPayment = this._dataStoreService.getData(IntakeStoreConstants.restitution);
    if (modal.reasontype === 'RCPF' && restitutionPayment) {
      if (restitutionPayment.balance && Number(restitutionPayment.balance) !== 0) {
        this._alertService.error('Restitution Balance Amount is Pending.');
        return true;
      }
    }
    const requestData = {
      restitutionid: this.restituionid,
      restitutionclosereasontypekey: modal.reasontype,
      closenotes: modal.comments
    };
    this._commonHttpService.create(requestData, CommonUrlConfig.EndPoint.RESTITUTION.DECISION.CLOSE_RESTITUTION).subscribe(success => {
      this._alertService.success('Restitution Closed successfully.');
    }, error => {
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

}
