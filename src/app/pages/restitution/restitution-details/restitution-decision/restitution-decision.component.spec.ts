import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestitutionDecisionComponent } from './restitution-decision.component';

describe('RestitutionDecisionComponent', () => {
  let component: RestitutionDecisionComponent;
  let fixture: ComponentFixture<RestitutionDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestitutionDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestitutionDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
