import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, DataStoreService, AlertService } from '../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';

@Component({
  selector: 'restitution-transfer',
  templateUrl: './restitution-transfer.component.html',
  styleUrls: ['./restitution-transfer.component.scss']
})
export class RestitutionTransferComponent implements OnInit {
  countyList = [];
  transferForm: FormGroup;
  intakeserreqrestitutionid: string;
  restitution: any;
  constructor(private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private _alertService: AlertService,
    private _router: Router) {
    this.intakeserreqrestitutionid = this._route.snapshot.parent.parent.params['restituionid'];
  }

  ngOnInit() {
    this.getCountyList();
    this.initFormGroup();
    this.restitution = this._dataStoreService.getData(IntakeStoreConstants.restitution);
  }

  initFormGroup() {
    this.transferForm = this._formBuilder.group({
      county: [null, Validators.required],
      transferreason: [null, Validators.required]
    });
  }

  private getCountyList() {
    this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      order: 'countyname asc'
    }, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter').subscribe(res => {
      this.countyList = res;
    });
  }

  transfer() {
    const transferObj = this.transferForm.getRawValue();
    const reqObj = {
      county: {
        countyid: transferObj.county,
        transfernotes: transferObj.transferreason
      },
      restitutionid: this.restitution.restitutionnumber
    };
    this._commonHttpService.patch(this.intakeserreqrestitutionid, reqObj, CommonUrlConfig.EndPoint.RESTITUTION.TRANSFER)
      .subscribe(res => {
        this._alertService.success('Restitution transfered successfully.');
        setTimeout(() => {
          this._router.navigate(['/pages/cjams-dashboard/restitution/review']);
        }, 1000);
      });
  }
}
