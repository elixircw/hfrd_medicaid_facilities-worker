import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestitutionTransferRoutingModule } from './restitution-transfer-routing.module';
import { RestitutionTransferComponent } from './restitution-transfer.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RestitutionTransferRoutingModule,
    FormMaterialModule,
    PaginationModule
  ],
  declarations: [RestitutionTransferComponent]
})
export class RestitutionTransferModule { }
