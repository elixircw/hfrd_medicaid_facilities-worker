import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestitutionTransferComponent } from './restitution-transfer.component';

describe('RestitutionTransferComponent', () => {
  let component: RestitutionTransferComponent;
  let fixture: ComponentFixture<RestitutionTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestitutionTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestitutionTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
