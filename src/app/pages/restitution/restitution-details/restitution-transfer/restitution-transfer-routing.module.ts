import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestitutionTransferComponent } from './restitution-transfer.component';

const routes: Routes = [
  {
    path: '',
    component: RestitutionTransferComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestitutionTransferRoutingModule { }
