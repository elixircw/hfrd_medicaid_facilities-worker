import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { CommonHttpService, AlertService, CommonDropdownsService, DataStoreService, AuthService } from '../../../../@core/services';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import { ActivatedRoute } from '@angular/router';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AppConstants } from '../../../../@core/common/constants';

@Component({
  selector: 'changeform',
  templateUrl: './changeform.component.html',
  styleUrls: ['./changeform.component.scss']
})
export class ChangeformComponent implements OnInit {
  changeForm: FormGroup;
  changetypeList = [];
  relations = [];
  changeformlist = [];
  id: string;
  isshowAmt = 0;
  paymentSchedule;
  restitutionno;
  currentDate = new Date();
  userInfo: AppUser;
  liableperson: any;
  totalCount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  viewmodal: any;
  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService,
    private _alertService: AlertService, private _dropDownService: CommonDropdownsService,
    private _dataStoreService: DataStoreService, private route: ActivatedRoute,
    private _authService: AuthService) {
    this.id = this.route.snapshot.parent.parent.params['id'];
  }

  ngOnInit() {
    this.loadDropdowns();
    this.initFormGroup();
    const paymentschedule = this._dataStoreService.getData(IntakeStoreConstants.restitution);
    if (paymentschedule) {
      this.paymentSchedule = paymentschedule;
      this.restitutionno = paymentschedule.restitutionnumber;
    }
    this.userInfo = this._authService.getCurrentUser();
    if (this.userInfo.role.name !== AppConstants.ROLES.RESTITUTION_COORDINATOR) {
      (<any>$(':button')).prop('disabled', true);
      (<any>$('.ccuapprovebutton')).prop('disabled', false);
    }
    this.paginationInfo.pageNumber = 1;
    this.loadList();
  }

  loadBasicDetails() {
    if (this.relations && this.relations.length > 0) {
      const liableperson = this.relations.find(relation => relation.personid === (this.paymentSchedule.liableperson ? this.paymentSchedule.liableperson.pid : ''));
      setTimeout(() => {
        this.changeForm.patchValue({
          liableperson: liableperson,
          restitutionAmt: this.paymentSchedule.amount,
          currentamt: this.paymentSchedule.balance
        });
      }, 100);
    }
    this.changeForm.get('restitutionAmt').disable();
    this.changeForm.get('currentamt').disable();
    this.changeForm.get('waivedamount').disable();
    this.changeForm.get('additionalamount').disable();
  }

  loadList() {
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.CHANGE_FORM.HISTORY_LIST + '/' + this.restitutionno + '?filter'
    ).subscribe((result: any) => {
      if (result) {
        this.changeformlist = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalCount = result.count;
        }
      }

    });
  }

  addChangeForm() {
    const status = this.changeformlist.find(data => data.status === 'Restitution Approval Pending');
    if (status) {
      this._alertService.warn('Please Approve Pending Change Form');
      return true;
    }
    this.loadBasicDetails();
    this.resetForm();
    (<any>$('#add-change-form')).modal('show');
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadList();
  }

  loadDropdowns() {
    this._dropDownService.getDropownsByTable('Change_form_reason').subscribe(data => {
      this.changetypeList = data;
    });
    this._commonHttpService.getPagedArrayList(new PaginationRequest({
      page: 1,
      limit: 200,
      method: 'get',
      where: { intakeserviceid: this.id }
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
      .PersonList + '?filter').subscribe(result => {
        this.relations = (result.data && result.data.length > 0) ? result.data.filter((person) => (person.rolename !== 'Youth' && person.rolename !== 'Victim')) : [];
      });
  }

  initFormGroup() {
    this.changeForm = this.formBuilder.group({
      changeformreasontype: [null],
      restitutionAmt: [null],
      currentamt: [null],
      liableperson: [null],
      balanceamt: [null],
      waivedamount: [null],
      additionalamount: [null],
      changeformreason: [null]
    });
  }

  saveChangeForm() {
    const changeFormModel = this.changeForm.getRawValue();
    const requestObj = {
      restitutionno: this.restitutionno,
      liablepersonid: changeFormModel.liableperson.personid,
      liablepersonrelationtypekey: changeFormModel.liableperson.relationship,
      waivedamount: changeFormModel.waivedamount,
      additionalamount: changeFormModel.additionalamount,
      changeformreason: changeFormModel.changeformreason,
      changeformreasontype: changeFormModel.changeformreasontype
    };

    this._commonHttpService.create(requestObj,
      CommonUrlConfig.EndPoint.RESTITUTION.CHANGE_FORM.SUBMIT).subscribe(success => {
        console.log(requestObj);
        this.resetForm();
        (<any>$('#add-change-form')).modal('hide');
        this._alertService.success('Change Form Submitted successfully.');
        this.loadList();
      }, error => {
        (<any>$('#add-change-form')).modal('hide');
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      });
  }

  onBalanceChanges() {
    const balanceamt = this.changeForm.get('balanceamt').value;
    const currentamt = this.changeForm.get('currentamt').value;
    const newbalanceamt = (balanceamt ? balanceamt : 0) - (currentamt ? currentamt : 0);
    if (newbalanceamt < 0) {
      this.isshowAmt = -1;
      this.changeForm.patchValue({
        waivedamount: Math.abs(newbalanceamt),
        additionalamount: 0
      });
    } else {
      this.isshowAmt = 1;
      this.changeForm.patchValue({
        additionalamount: newbalanceamt,
        waivedamount: 0
      });
    }
  }

  viewChangeForm(modal) {
    this.viewmodal = modal;
    if (Number(modal.waivedamount) !== 0) {
      this.isshowAmt = -1;
    }
    if (Number(modal.additionalamount) !== 0) {
      this.isshowAmt = 1;
    }
  }

  resetForm() {
    this.isshowAmt = 0;
    this.viewmodal = null;
    this.changeForm.reset();
  }

}
