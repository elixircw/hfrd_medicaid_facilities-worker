import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangeformComponent } from './changeform.component';

const routes: Routes = [{
  path: '',
  component: ChangeformComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangeformRoutingModule { }
