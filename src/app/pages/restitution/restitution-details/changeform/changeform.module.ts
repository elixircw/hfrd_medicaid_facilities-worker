import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangeformRoutingModule } from './changeform-routing.module';
import { ChangeformComponent } from './changeform.component';
import { PaginationModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ChangeformRoutingModule,
    PaginationModule,
    FormMaterialModule,
    ReactiveFormsModule
  ],
  declarations: [ChangeformComponent]
})
export class ChangeformModule { }
