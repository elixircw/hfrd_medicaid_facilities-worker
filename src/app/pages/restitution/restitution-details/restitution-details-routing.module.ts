import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestitutionDetailsComponent } from './restitution-details.component';
import { AppConstants } from '../../../@core/common/constants';
import { RestitutionDecisionComponent } from './restitution-decision/restitution-decision.component';

const routes: Routes = [
  {
    path: '',
    component: RestitutionDetailsComponent,
    data: {
      pageSource: AppConstants.PAGES.RESTITUTION_COORDINATOR
    },
    children: [
      {
        path: 'person',
        loadChildren: 'app/pages/case-worker/dsds-action/involved-persons/involved-persons.module#InvolvedPersonsModule',
        data: {
          pageSource: AppConstants.PAGES.RESTITUTION_COORDINATOR
        }
      },
      {
        path: 'transfer',
        loadChildren: 'app/pages/restitution/restitution-details/restitution-transfer/restitution-transfer.module#RestitutionTransferModule'
      },
      {
        path: 'changeform',
        loadChildren: 'app/pages/restitution/restitution-details/changeform/changeform.module#ChangeformModule'
      },
      {
        path: 'courtaction',
        loadChildren: 'app/pages/newintake/my-newintake/intake-sao-hearing-details/intake-sao-hearing-details.module#IntakeSaoHearingDetailsModule'
      },
      {
        path: 'document',
        loadChildren: 'app/pages/newintake/my-newintake/intake-attachments/intake-attachments.module#IntakeAttachmentsModule',
        data: {
          pageSource: AppConstants.PAGES.RESTITUTION_COORDINATOR
        }
      },
      {
        path: 'decision',
        component: RestitutionDecisionComponent
      },
      {
        path: '**',
        redirectTo: 'person',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestitutionDetailsRoutingModule { }
