import { Component, OnInit } from '@angular/core';
import { PaymentDetailsService } from '../payment-details.service';

@Component({
  selector: 'payment-header',
  templateUrl: './payment-header.component.html',
  styleUrls: ['./payment-header.component.scss']
})
export class PaymentHeaderComponent implements OnInit {
  payment: any;
  constructor(
    private _paymentService: PaymentDetailsService
  ) { }

  ngOnInit() {
    this.loadPaymentDetails();
  }

  loadPaymentDetails() {
    this._paymentService.loadPaymentDetails().subscribe(res => {
      const payment = res ? res.data : null;
      this.payment = (payment && payment.length > 0) ? payment[0] : [];
    });
  }

}
