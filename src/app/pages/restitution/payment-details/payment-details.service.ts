import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../@core/services';
import { CommonUrlConfig } from '../../../@core/common/URLs/common-url.config';
import { PaginationInfo } from '../../../@core/entities/common.entities';

@Injectable()
export class PaymentDetailsService {
  paymentid: string;
  constructor(
    private _commonHttpService: CommonHttpService
  ) { }

  submitPaymentDetails(restitutionpaymentdetails) {
    return this._commonHttpService.create(restitutionpaymentdetails, CommonUrlConfig.EndPoint.RESTITUTION.LOCKBOX.ADDPAYMENTDETAILS);
  }

  approvePaymentDetails(restitutionpaymentdetails) {
    return this._commonHttpService.create(restitutionpaymentdetails, CommonUrlConfig.EndPoint.RESTITUTION.LOCKBOX.APPROVEPAYMENTDETAILS);
  }

  loadPaymentDetails() {
    return this._commonHttpService.getSingle(
      {
        limit: 1,
        page: 1,
        method: 'get',
        where: {
          restitutionpaymentflatfilecontentid: this.paymentid
        }
      },
      CommonUrlConfig.EndPoint.RESTITUTION.LOCKBOX.MATCHEDPAYMENTLIST + '?data'
    );
  }

  getYouthList(paginationInfo: PaginationInfo, where: any) {
    return this._commonHttpService.getPagedArrayList({
      method: 'post',
      limit: paginationInfo.pageSize,
      page: paginationInfo.pageNumber,
      where: where
    }, CommonUrlConfig.EndPoint.RESTITUTION.PAYMENT_DETAILS.YOUTH_LIST);
  }

  loadMatchedYouthAndRestitutions() {
    return this._commonHttpService.getPagedArrayList({
      method: 'get',
      limit: 1,
      page: 1,
      where: {
        contentid: this.paymentid
      }
    }, CommonUrlConfig.EndPoint.RESTITUTION.PAYMENT_DETAILS.PAYMENT_DETAILS);
  }

  getpaymentApproveDetails() {
    return this._commonHttpService.getPagedArrayList({
      method: 'get',
      limit: 1,
      page: 1,
      where: {
        contentid: this.paymentid
      }
    }, CommonUrlConfig.EndPoint.RESTITUTION.PAYMENT_DETAILS.PAYMENT_APPROVE_DETAILS);
  }

}
