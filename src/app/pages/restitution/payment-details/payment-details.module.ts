import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentDetailsRoutingModule } from './payment-details-routing.module';
import { PaymentDetailsComponent } from './payment-details.component';
import { PaymentHeaderComponent } from './payment-header/payment-header.component';
import { PaymentSplitComponent } from './payment-split/payment-split.component';
import { PaymentDetailsService } from './payment-details.service';
import { PaginationModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PaymentDetailsRoutingModule,
    PaginationModule,
    FormMaterialModule,
    ReactiveFormsModule
  ],
  declarations: [PaymentDetailsComponent, PaymentHeaderComponent, PaymentSplitComponent],
  providers: [PaymentDetailsService]
})
export class PaymentDetailsModule { }
