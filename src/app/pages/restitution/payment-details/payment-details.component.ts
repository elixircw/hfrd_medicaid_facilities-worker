import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentDetailsService } from './payment-details.service';

@Component({
  selector: 'payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.scss']
})
export class PaymentDetailsComponent implements OnInit {

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _paymentService: PaymentDetailsService
  ) {
    _paymentService.paymentid = _route.parent.snapshot.params['paymentid'];
   }

  ngOnInit() {
  }

}
