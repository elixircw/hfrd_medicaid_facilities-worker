import { Component, OnInit } from '@angular/core';
import { CommonHttpService, AlertService, AuthService, CommonDropdownsService } from '../../../../@core/services';
import { PaymentDetailsService } from '../payment-details.service';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { FormGroup, FormBuilder } from '@angular/forms';
import { isNumber } from 'util';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { ActivatedRoute, Router } from '@angular/router';
const DJS_FINANCEWORKER = 'JSFU';
@Component({
  selector: 'payment-split',
  templateUrl: './payment-split.component.html',
  styleUrls: ['./payment-split.component.scss']
})
export class PaymentSplitComponent implements OnInit {
  matchedYouth: any;
  youth: any;
  selectedYouth: any;
  youthList = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  totalCount: number;
  searchYouthForm: FormGroup;
  matchedRestitution = [];
  matchedRestitutionInitial = [];
  approvedRestitution = [];
  isMatched: boolean;
  approvedRestitutionNoList = [];
  isEdit: boolean;
  allocatedamount: number;
  previousAllocatedAmount: number;
  paidAmount: number;
  balance: number;
  balanceMode: number;
  initialBalance: number;
  allocatedAmountTotal: number;
  allocatedAmountTotalMode: number;
  isOverpayed: boolean;
  isOverpayedLessThanOne: boolean;
  customerNumber: number;
  restitutionData = null;
  accountType: string;
  roleId;
  disablefields = false;
  mode = '';
  overpaymentsAccountList = [];
  constructor(
    private _paymentService: PaymentDetailsService,
    private _formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _router: Router,
    private _authService: AuthService,
    private _dropDownService: CommonDropdownsService,
    private route: ActivatedRoute
  ) {
    this.mode = this.route.snapshot.paramMap.get('operation');
  }

  ngOnInit() {
    this.roleId = this._authService.getCurrentUser().user.userprofile.teammemberassignment.teammember.roletypekey;
    if (this.roleId === DJS_FINANCEWORKER) {
      this.disablefields = true;
    }
    if (this.mode === 'approved' || this.mode === 'view' || this.mode === 'refund') {
      this.disablefields = true;
    }
    this.allocatedAmountTotal = 0;
    this.initFormGroup();
    if (!this.disablefields) {
      this.loadMatchedYouthAndRestitutions();
    } else {
      this.getloadPaymentApproveRestitutions();
    }
    this.isEdit = false;
    this.checkOverPayment();
    this._dropDownService.getDropownsByTable('Overpaymentacc').subscribe(data => {
      this.overpaymentsAccountList = data;
    });
  }

  initFormGroup() {
    this.searchYouthForm = this._formBuilder.group({
      firstname: [null],
      lastname: [null],
      cjamspid: [null]
    });
    if (this.disablefields) {
      this.searchYouthForm.disable();
    }
  }

  submitPaymentAllocation(getConfirmation: boolean, mode: string) {
    // if (((this.balance >= 1) && this.matchedRestitution.length &&
    //   (this.matchedRestitution.filter(restitution => restitution.allocatedamount === 0)).length === this.matchedRestitution.length)) {
    //   this._alertService.warn('Please Allocate balance in any of the restitution account');
    //   return false;
    // } else
    if ((this.balance >= 1) && this.matchedRestitution.filter(restitution => (restitution.finalbalanceamount !== 0)).length) {
      this._alertService.warn('Please Allocate Remaining balance in any of the restitution account');
      return false;
    }
    if (this.balance !== 0 && mode === 'initial') {
      (<any>$('#confirm-submission')).modal('show');
      return;
    }
    const requestObj = {
      appevent: 'RTFUP',
      balanceamount: this.balance,
      restitutionpaymentdetails: [],
      restitutionpaymentflatfilecontentid: this._paymentService.paymentid,
      isallocated: this.isOverpayed,
      overpaymentacctype: this.accountType
    };
    requestObj.restitutionpaymentdetails = this.matchedRestitution.filter(restitution => restitution.allocatedamount)
      .map(restitution => {
        return {
          restitutionno: restitution.restitutionno,
          intakeserreqrestitutionid: restitution.intakeserreqrestitutionid,
          paymentamount: restitution.allocatedamount,
          allocatedamount: restitution.allocatedamount,
          paymentnumber: this.restitutionData.contentflatfilepaymentno,
          paymentdate: this.restitutionData.asofdate,
          paymenttype: null,
          checknumber: this.restitutionData.contentflatfilecheckno,
          payerfirstname: this.restitutionData.contentflatfilefirstname,
          payerlastname: this.restitutionData.contentflatfilelastname,
          payeraddress: this.restitutionData.contentflatfileaddressline1,
          payercity: this.restitutionData.contentflatfilecity,
          payerstatekey: this.restitutionData.contentflatfilestate,
          payerzipcode: this.restitutionData.contentflatfilezip,
          depositnumber: this.restitutionData.lockboxno,
          accountarea: this.restitutionData.vakaccountnumber,
          memofirstname: this.restitutionData.contentflatfilecustomernumber,
          memolastname: this.restitutionData.contentflatfilecheckno,
          youthpersonid: restitution.youthpersondetails.youthpersonid,
          victimpersonid: restitution.victimpersondetails.victimpersonid
        };
      });
    this._paymentService.submitPaymentDetails(requestObj).subscribe(success => {
      console.log(requestObj);
      (<any>$('#confirm-submission')).modal('hide');
      this._alertService.success('Payment allocated successfully.');
      setTimeout(() => {
        this._router.navigate(['/pages/restitution/lockboxpayments/checkregister']);
      }, 1000);
    }, error => {
      (<any>$('#confirm-submission')).modal('hide');
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  searchYouth(isInitialSearch: boolean) {
    const where = isInitialSearch ? {} : this.searchYouthForm.getRawValue();
    this._paymentService.getYouthList(this.paginationInfo, where).subscribe(res => {
      this.youthList = res ? res.data : [];
      this.totalCount = (res && res.count) ? res.count : 0;
      (<any>$('#search-youth')).modal('show');
      this.selectedYouth = null;
    });
  }

  pageChanged(page: number) {
    this.paginationInfo.pageNumber = page;
    this.searchYouth(false);
  }

  loadMatchedYouthAndRestitutions() {
    this._paymentService.loadMatchedYouthAndRestitutions().subscribe(res => {
      const data = (res.data && res.data.length) ? res.data[0] : null;
      if (data) {
        this.restitutionData = data;
        this.customerNumber = data.contentflatfilecustomernumber;
        if (data.matchedpersondetails && data.matchedpersondetails.length > 0) {
          this.matchedYouth = (data.matchedpersondetails && data.matchedpersondetails.length) ? data.matchedpersondetails[0] : null;
          this.matchedRestitution = data.matchedrestitutionacc ? data.matchedrestitutionacc : [];
        }
        this.youth = (data.approvedpersondetails && data.approvedpersondetails.length) ? data.approvedpersondetails[0] : null;
        this.balance = data.balance ? data.balance : 0;
        this.updateFinalBalance();
        this.matchedRestitutionInitial = [...this.matchedRestitution];
        this.approvedRestitution = data.approvedrestitutionacc ? data.approvedrestitutionacc : [];
        this.getApprovedRestitutionNumbers();
        this.isMatched = data.ismatched;
        this.paidAmount = data.paidamount ? data.paidamount : 0;
        this.initialBalance = this.balance;
      }
    });
  }

  getloadPaymentApproveRestitutions() {
    this._paymentService.getpaymentApproveDetails().subscribe(res => {
      const data = (res.data && res.data.length) ? res.data[0] : null;
      if (data) {
        this.restitutionData = data;
        this.customerNumber = data.contentflatfilecustomernumber;
        this.balance = data.restitutionbalanceamount ? data.restitutionbalanceamount : 0;
        if (data.approvedrestitutionacc && data.approvedrestitutionacc.length > 0) {
          this.matchedRestitution = data.approvedrestitutionacc ? data.approvedrestitutionacc : [];
          const matchedYouth = this.matchedRestitution[0].youthpersondetails;
          this.matchedYouth = {
            cjamspid: matchedYouth.youthcjamspid,
            dob: matchedYouth.youthdob,
            firstname: matchedYouth.youthfirstname,
            gendertypekey: matchedYouth.youthgendertypekey,
            lastname: matchedYouth.youthlastname,
            personid: matchedYouth.youthpersonid
          };
          this.matchedRestitution.map(restitution => {
            const respayment = restitution.restitutionpayment ? restitution.restitutionpayment : 0;
            const paidamnt = restitution.paidamount ? restitution.paidamount : 0;
            const restitutionbalance = restitution.restitutionpaymentbalance ? restitution.restitutionpaymentbalance : 0;
            restitution.finalbalanceamount = restitutionbalance ? restitutionbalance : (respayment - paidamnt);
            restitution.finalbalanceamount -= restitution.allocatedamount;
            return restitution;
          });
        }
        this.allocatedAmountTotal = data.restitutionallocatedamount;
        this.matchedRestitutionInitial = [...this.matchedRestitution];
        this.approvedRestitution = data.approvedrestitutionacc ? data.approvedrestitutionacc : [];
        this.getApprovedRestitutionNumbers();
        this.isMatched = data.ismatched;
        this.initialBalance = this.balance;
        this.allocatedAmountTotalMode = data.restitutionallocatedamount ? data.restitutionallocatedamount : 0;
        this.balanceMode = data.restitutionbalanceamount ? data.restitutionbalanceamount : 0;
      }
    });
  }

  private updateFinalBalance() {
    if (this.matchedRestitution.length === 1) {
      const restitution = this.matchedRestitution[0];
      restitution.finalbalanceamount = restitution.balance ? restitution.balance : 0;
      if (!this.disablefields) {
        restitution.allocatedamount = (this.balance >= restitution.balance) ? restitution.balance : this.balance;
      }
      restitution.finalbalanceamount -= restitution.allocatedamount;
      restitution.isMatched = (restitution.restitutionno === this.customerNumber);
      this.balance -= restitution.allocatedamount;
      this.allocatedAmountTotal += Number(restitution.allocatedamount);
      this.checkOverPayment();
      console.log(restitution);
    } else {
      this.matchedRestitution.forEach(restitution => {
        restitution.allocatedamount = 0;
        restitution.finalbalanceamount = restitution.balance ? restitution.balance : 0;
        restitution.isMatched = (restitution.restitutionno === Number(this.customerNumber));
        console.log(restitution.isMatched, restitution.restitutionno, this.customerNumber);
      });
    }
  }

  private getApprovedRestitutionNumbers() {
    this.approvedRestitutionNoList = this.approvedRestitution ? this.approvedRestitution.map(restitution => restitution.restitutionno) : [];
  }

  choosenYouth(youth: any) {
    this.selectedYouth = youth;
  }

  addYouth() {
    this.youth = this.selectedYouth;
    this.matchedRestitution = this.youth.restitutionacc;
    this.updateFinalBalance();
    (<any>$('#search-youth')).modal('hide');
    this.initFormGroup();
  }

  removeYouthPopup() {
    (<any>$('#remove-youth')).modal('toggle');
  }

  removeYouth() {
    this.youth = null;
    this.balance = this.initialBalance;
    this.allocatedAmountTotal = 0;
    if (this.isMatched) {
      this.matchedRestitution = this.matchedRestitutionInitial;
      this.updateFinalBalance();
    } else {
      this.matchedRestitution = [];
    }
    this.approvedRestitution = [];
    this.removeYouthPopup();
  }

  approveYouth() {
    this.youth = this.matchedYouth;
  }

  rejectYouth() {
    this.searchYouth(true);
  }

  editRestition(restitution: any) {
    this.toggleRestitutionEdit(restitution);
    this.allocatedamount = restitution.allocatedamount ? restitution.allocatedamount : 0;
    this.previousAllocatedAmount = this.allocatedamount;
  }

  private toggleRestitutionEdit(restitution: any) {
    restitution.isEdit = !restitution.isEdit;
    this.isEdit = !this.isEdit;
  }

  approveRestition(restitution: any) {
    const amountAdded = this.allocatedamount - this.previousAllocatedAmount;
    if (this.allocatedamount < 0) {
      this._alertService.warn('Allocated amount should be greater than 0.');
    } else if (amountAdded > this.balance) {
      this._alertService.warn('Allocated amount for the restitution should be less than the Balance Amount of the Payment.');
    } else if (amountAdded > restitution.finalbalanceamount && this.balance && !(this.balance > 0 && this.balance < 1)) {
      this._alertService.warn('Allocated amount for the restitution should be less than the Balance Amount of the Restitution.');
    } else {
      this.toggleRestitutionEdit(restitution);
      restitution.allocatedamount = this.allocatedamount;
      // restitution.paidamount += amountAdded;
      restitution.finalbalanceamount -= amountAdded;
      this.allocatedAmountTotal += amountAdded;
      this.balance -= Number(amountAdded);
      this.checkOverPayment();
    }
  }

  private checkOverPayment() {
    this.isOverpayed = ((this.balance >= 1) && this.matchedRestitution.length &&
      (this.matchedRestitution.filter(restitution => restitution.finalbalanceamount === 0)).length === this.matchedRestitution.length);
    this.isOverpayedLessThanOne = ((this.balance < 1) && this.matchedRestitution.length &&
      (this.matchedRestitution.filter(restitution => restitution.finalbalanceamount === 0)).length === this.matchedRestitution.length);
  }

  revertRestition(restitution: any) {
    this.toggleRestitutionEdit(restitution);
    restitution.allocatedamount = this.previousAllocatedAmount;
  }

  changeAllocatedAmount(allocatedamount: number) {
    console.log(allocatedamount);
  }

  resetModal() {
    this.accountType = '';
  }

  getAccountType(data) {
    if (this.overpaymentsAccountList && this.overpaymentsAccountList.length > 0) {
      return this.overpaymentsAccountList.find(ele => ele.ref_key === data).description;
    }
  }

  approvePaymentAllocation() {
    const requestObj = {
      restitutionpaymentflatfilecontentid: this._paymentService.paymentid,
      restitutionpaymentdetails: [],
      status: 'approved'
    };
    requestObj.restitutionpaymentdetails = this.matchedRestitution.filter(restitution => restitution.allocatedamount)
      .map(restitution => {
        return {
          restitutionno: restitution.restitutionno,
          intakeserreqrestitutionpaymentid: restitution.intakeserreqrestitutionid,
          paymentamount: restitution.allocatedamount,
          allocatedamount: restitution.allocatedamount,
          memofirstname: this.restitutionData.contentflatfilecustomernumber,
          intakeserreqrestitutionid: restitution.intakeserreqrestitutionid
        };
      });
    this._paymentService.approvePaymentDetails(requestObj).subscribe(success => {
      console.log(requestObj);
      (<any>$('#confirm-submission')).modal('hide');
      this._alertService.success('Payment allocation Approved successfully.');
      setTimeout(() => {
        this._router.navigate(['/pages/restitution/djs-finance-dashboard/accounts']);
      }, 1000);
    }, error => {
      (<any>$('#confirm-submission')).modal('hide');
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  approveRefundPaymentAllocation() {
    const requestObj = {
      restitutionpaymentflatfilecontentid: this._paymentService.paymentid,
      restitutionpaymentdetails: [],
      balanceamount: this.balance,
      status: 'refund approved'
    };
    requestObj.restitutionpaymentdetails = this.matchedRestitution.filter(restitution => restitution.allocatedamount)
      .map(restitution => {
        return {
          restitutionno: restitution.restitutionno,
          intakeserreqrestitutionpaymentid: restitution.intakeserreqrestitutionid,
          paymentamount: restitution.allocatedamount,
          allocatedamount: restitution.allocatedamount,
          memofirstname: this.restitutionData.contentflatfilecustomernumber,
          intakeserreqrestitutionid: restitution.intakeserreqrestitutionid
        };
      });
    this._paymentService.approvePaymentDetails(requestObj).subscribe(success => {
      console.log(requestObj);
      (<any>$('#confirm-submission')).modal('hide');
      this._alertService.success('Payment refund Approved successfully.');
      setTimeout(() => {
        this._router.navigate(['/pages/restitution/djs-finance-dashboard/accounts']);
        (<any>$('#refund')).click();
      }, 1000);
    }, error => {
      (<any>$('#confirm-submission')).modal('hide');
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

}
