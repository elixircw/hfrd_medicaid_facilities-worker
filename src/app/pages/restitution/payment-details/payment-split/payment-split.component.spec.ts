import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentSplitComponent } from './payment-split.component';

describe('PaymentSplitComponent', () => {
  let component: PaymentSplitComponent;
  let fixture: ComponentFixture<PaymentSplitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentSplitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentSplitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
