import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestitutionRoutingModule } from './restitution-routing.module';
import { RestitutionComponent } from './restitution.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    RestitutionRoutingModule
  ],
  declarations: [RestitutionComponent]
})
export class RestitutionModule { }
