import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DjsFinanceDashboardComponent } from './djs-finance-dashboard.component';
import { PaymentsDashboardComponent } from './payments-dashboard/payments-dashboard.component';
import { ChangeformDashboardComponent } from './changeform-dashboard/changeform-dashboard.component';

const routes: Routes = [{
  path: '',
  component: DjsFinanceDashboardComponent,
  children: [
    {
      path: 'accounts',
      component: PaymentsDashboardComponent
    },
    {
      path: 'changeform',
      component: ChangeformDashboardComponent
    },
    {
      path: '',
      redirectTo: 'accounts'
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DjsFinanceDashboardRoutingModule { }
