import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../@core/services';

@Component({
  selector: 'djs-finance-dashboard',
  templateUrl: './djs-finance-dashboard.component.html',
  styleUrls: ['./djs-finance-dashboard.component.scss']
})
export class DjsFinanceDashboardComponent implements OnInit {
  roleId: string;
  constructor(private _authservice: AuthService) { }

  ngOnInit() {
    this.roleId = this._authservice.getCurrentUser().user.userprofile.teammemberassignment.teammember.roletypekey;
  }

}
