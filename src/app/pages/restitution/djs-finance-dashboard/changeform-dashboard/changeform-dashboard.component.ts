import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, AuthService } from '../../../../@core/services';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'changeform-dashboard',
  templateUrl: './changeform-dashboard.component.html',
  styleUrls: ['./changeform-dashboard.component.scss']
})
export class ChangeformDashboardComponent implements OnInit {

  changeformlist = [];
  totalCount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  isDjs: boolean;
  constructor(private _alertService: AlertService, private _commonHttpService: CommonHttpService,private _authService: AuthService) { }

  ngOnInit() {
    this.paginationInfo.pageNumber = 1;
    this.loadList();
    this.isDjs = this._authService.isDJS();
  }

  loadList() {
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get'
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.CHANGE_FORM.DASHBOARD_LIST + '?data'
    ).subscribe((result: any) => {
      if (result) {
        this.changeformlist = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalCount = result.count;
        }
      }

    });
  }

  approveReject(model, status) {
    const requestObj = {
      intakeserreqrestitutionid: model.intakeserreqrestitutionid,
      restitutionno: model.restitutionno,
      approvestatus: status === 'Approve' ? 'RSTAPR' : 'RSTRJT'
    };
    this._commonHttpService.create(requestObj,
      CommonUrlConfig.EndPoint.RESTITUTION.CHANGE_FORM.APPROVE_REJECT).subscribe(success => {
        this._alertService.success('Change Form Approved successfully.');
        setTimeout(() => {
          this.loadList();
        }, 100);
      }, error => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      });
  }

}
