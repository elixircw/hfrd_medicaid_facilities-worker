import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeformDashboardComponent } from './changeform-dashboard.component';

describe('ChangeformDashboardComponent', () => {
  let component: ChangeformDashboardComponent;
  let fixture: ComponentFixture<ChangeformDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeformDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeformDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
