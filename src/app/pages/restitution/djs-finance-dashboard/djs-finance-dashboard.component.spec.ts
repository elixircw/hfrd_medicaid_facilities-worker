import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsFinanceDashboardComponent } from './djs-finance-dashboard.component';

describe('DjsFinanceDashboardComponent', () => {
  let component: DjsFinanceDashboardComponent;
  let fixture: ComponentFixture<DjsFinanceDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsFinanceDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsFinanceDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
