import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { Router, ActivatedRoute } from '@angular/router';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';

@Component({
  selector: 'payments-dashboard',
  templateUrl: './payments-dashboard.component.html',
  styleUrls: ['./payments-dashboard.component.scss']
})
export class PaymentsDashboardComponent implements OnInit {
  paymentslist = [];
  paymentslistRefund = [];
  totalCount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  pageType = 'Payment';
  roleId;
  disablefields = true;
  isDjs: boolean;

  constructor(private _commonService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _store: DataStoreService) { }

  ngOnInit() {
    this.roleId = this._authService.getCurrentUser().user.userprofile.teammemberassignment.teammember.roletypekey;
    if (this.roleId !== 'JSFU') {
      this.disablefields = false;
    }
    this.paginationInfo.pageNumber = 1;
    this.loadPaymentList();
    this.isDjs = this._authService.isDJS();
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.loadPaymentList();
  }

  loadPaymentList() {
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'get',
        where: {
          pagetype: this.pageType
        }
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.DJSFINANCEDASHBOARD.PAYMENTLIST + '?filter'
    ).subscribe((result: any) => {
      if (result) {
        this.paymentslist = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalCount = result.count;
        }
      }

    });
  }

  toggleTable(id) {
    (<any>$('#' + id)).collapse('toggle');
  }

  resetList() {
    this.paymentslist = [];
    this.totalCount = 0;
  }

  view(data) {
    this.router.navigate([`/pages/restitution/${data.restitutionpaymentflatfilecontentid}/payment/view`], {
      relativeTo: this.route
    });
  }

  approve(data) {
    this.router.navigate([`/pages/restitution/${data.restitutionpaymentflatfilecontentid}/payment/approve`], {
      relativeTo: this.route
    });
  }

  refund(data) {
    this.router.navigate([`/pages/restitution/${data.restitutionpaymentflatfilecontentid}/payment/refund`], {
      relativeTo: this.route
    });
  }

  generateDocument(data) {
    const requestData = {
      count: -1,
      where: {

        documenttemplatekey: ['paymentauthorization'],
        paymentid: data.restitutionpaymentflatfilecontentid

      },
      method: 'post'
    };
    this._commonService.create(requestData, CommonUrlConfig.EndPoint.RESTITUTION.DJSFINANCEDASHBOARD.GENERATEDOCUMENT).subscribe(success => {
      this._alertService.success('Document Generated successfully.');
      setTimeout(() => {
        this.loadPaymentList();
      }, 100);
    }, error => {
      this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    });
  }

  downloadDocument(data) {
    this._store.setData(IntakeStoreConstants.generatedDocumentDownloadKey, [data.documentpath]);
    this._store.setData('loadhtml', true);
  }

  getPayments(type) {
    this.pageType = type;
    this.pageChanged({ page: 1 });
  }


}
