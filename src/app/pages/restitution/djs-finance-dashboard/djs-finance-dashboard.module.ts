import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DjsFinanceDashboardRoutingModule } from './djs-finance-dashboard-routing.module';
import { PaymentsDashboardComponent } from './payments-dashboard/payments-dashboard.component';
import { ChangeformDashboardComponent } from './changeform-dashboard/changeform-dashboard.component';
import { DjsFinanceDashboardComponent } from './djs-finance-dashboard.component';
import { PaginationModule } from 'ngx-bootstrap/pagination/pagination.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    DjsFinanceDashboardRoutingModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule
  ],
  declarations: [PaymentsDashboardComponent, ChangeformDashboardComponent, DjsFinanceDashboardComponent]
})
export class DjsFinanceDashboardModule { }
