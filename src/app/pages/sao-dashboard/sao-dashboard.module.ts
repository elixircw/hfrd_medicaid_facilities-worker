import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatTableModule, MatRadioModule, MatTabsModule, MatCheckbox } from '@angular/material';

import { PaginationModule } from 'ngx-bootstrap/pagination/pagination.module';

import { SortTableModule } from '../../shared/modules/sortable-table/sortable-table.module';
import { SaoDashboardComponent } from './sao-dashboard.component';
import { SaoDashboardRoutingModule } from './sao-dashboard-routing.module';
import { IntakeUtils } from '../_utils/intake-utils.service';


@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        CommonModule,
        MatTabsModule,
        MatSelectModule,
        MatTableModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        SaoDashboardRoutingModule,
        SortTableModule
    ],
    declarations: [SaoDashboardComponent],
    providers: [IntakeUtils]
})
export class SaoDashboardModule {}
