export class SaoDashboardUrlConfig {
    public static EndPoint = {
        Intake: {
            dashboard: 'servicerequestsearches/usersservicerequestclw',
            addpetition: 'intakeservicerequestpetition/add',
            addcourtdetails: 'intakeservicerequestcourtaction/add'
        }
    };
}
