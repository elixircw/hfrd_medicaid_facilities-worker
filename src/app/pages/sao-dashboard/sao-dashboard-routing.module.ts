import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from '../../@core/guard';
import { SaoDashboardComponent } from './sao-dashboard.component';


const routes: Routes = [
    {
        path: '',
        component: SaoDashboardComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SaoDashboardRoutingModule {}
