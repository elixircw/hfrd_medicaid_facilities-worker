import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { PaginationInfo, DynamicObject, PaginationRequest } from '../../@core/entities/common.entities';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, DataStoreService, SessionStorageService, AuthService } from '../../@core/services';
import { IntakeSummary } from '../cjams-dashboard/_entities/dashBoard-datamodel';
import { SaoDashboardUrlConfig } from './sao-dashboard-url.config';
import { Router } from '@angular/router';
import { ColumnSortedEvent } from '../../shared/modules/sortable-table/sort.service';
import { AppConstants } from '../../@core/common/constants';
import { IntakeUtils } from '../_utils/intake-utils.service';

const DEFAULT_PAGE_NUMBER = 1;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sao-dashboard',
  templateUrl: './sao-dashboard.component.html',
  styleUrls: ['./sao-dashboard.component.scss']
})

export class SaoDashboardComponent implements OnInit {
  maxSize = 10;
  currentStatus: string;
  paginationPendingSAODocumentsInfo: PaginationInfo = new PaginationInfo();
  paginationPendingSAOResponseInfo: PaginationInfo = new PaginationInfo();
  paginationWaitingSAODocumentsInfo: PaginationInfo = new PaginationInfo();
  paginationScheduleHearingInfo: PaginationInfo = new PaginationInfo();
  paginationCourtActionsInfo: PaginationInfo = new PaginationInfo();
  paginationCourtHearingInfo: PaginationInfo = new PaginationInfo();
  paginationMyActionsInfo: PaginationInfo = new PaginationInfo();
  paginationClosedIntakesInfo: PaginationInfo = new PaginationInfo();
  paginationWithCaseWorkerInfo: PaginationInfo = new PaginationInfo();
  paginationFromCaseWorkerInfo: PaginationInfo = new PaginationInfo();
  dynamicObjectIntakeSummary: DynamicObject = {};
  intakeSummaryForm: FormGroup;
  intakesSummary: IntakeSummary[];
  totalRecords: number;
  searchCriteria: any;
  tabIndex: string;
  saoTabIndex$ = new Subject<string>();
  INTAKE_CONSTANTS = {};
  isDjs = false;

  private searchTermStreamIntake$ = new Subject<DynamicObject>();
  private pageStreamIntake$ = new Subject<number>();

  constructor(private _commonService: CommonHttpService,
    private formBuilder: FormBuilder,
    private _router: Router,
    private _intakeUtils: IntakeUtils,
    private _authService: AuthService) { }

  ngOnInit() {
    this.paginationPendingSAODocumentsInfo.sortColumn = 'intakenumber';
    this.isDjs = this._authService.isDJS();
    this.paginationPendingSAODocumentsInfo.sortBy = 'desc';

    this.formIntakeSummaryInitilize();
    this.getIntakesForPendingSAODocuments(this.paginationPendingSAODocumentsInfo);
    this.INTAKE_CONSTANTS = AppConstants.INTAKE_CONSTANTS;
  }

  getIntakesForPendingSAODocuments(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationPendingSAODocumentsInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationPendingSAODocumentsInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationPendingSAODocumentsInfo.sortBy = pageInfo.sortDirection;
      this.paginationPendingSAODocumentsInfo.sortColumn = pageInfo.sortColumn;
    }
    this.getIntakeSummary(this.paginationPendingSAODocumentsInfo, AppConstants.INTAKE_CONSTANTS.ROUTED_CLW);
    (<any>$('#pending-sao')).tab('show');
  }
  getIntakesForPendingSAOResponse(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationPendingSAOResponseInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationPendingSAOResponseInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationPendingSAOResponseInfo.sortBy = pageInfo.sortDirection;
      this.paginationPendingSAOResponseInfo.sortColumn = pageInfo.sortColumn;
    }
    this.getIntakeSummary(this.paginationPendingSAOResponseInfo, AppConstants.INTAKE_CONSTANTS.SAO_DOUCUMENT_GENERATED);
  }

  getIntakesForWaitingSAODocuments(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationWaitingSAODocumentsInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationWaitingSAODocumentsInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationWaitingSAODocumentsInfo.sortBy = pageInfo.sortDirection;
      this.paginationWaitingSAODocumentsInfo.sortColumn = pageInfo.sortColumn;
    }
    this.getIntakeSummary(this.paginationWaitingSAODocumentsInfo, AppConstants.INTAKE_CONSTANTS.SAO_RESPONSED);
  }

  getIntakesForScheduleHearing(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationScheduleHearingInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationScheduleHearingInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationScheduleHearingInfo.sortBy = pageInfo.sortDirection;
      this.paginationScheduleHearingInfo.sortColumn = pageInfo.sortColumn;
    }
    this.getIntakeSummary(this.paginationScheduleHearingInfo, AppConstants.INTAKE_CONSTANTS.PETTIION_SUBMITED);
    (<any>$('#scheduled-hearing-tab')).tab('show');
  }

  getIntakesForHearingDetails(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationCourtActionsInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationCourtActionsInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationCourtActionsInfo.sortBy = pageInfo.sortDirection;
      this.paginationCourtActionsInfo.sortColumn = pageInfo.sortColumn;
    }
    this.getIntakeSummary(this.paginationCourtActionsInfo, AppConstants.INTAKE_CONSTANTS.HEARING_SCHEDULED);
  }

  getIntakesForCourtHearing(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationCourtHearingInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationCourtHearingInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationCourtHearingInfo.sortBy = pageInfo.sortDirection;
      this.paginationCourtHearingInfo.sortColumn = pageInfo.sortColumn;
    }
    this.getIntakeSummary(this.paginationCourtHearingInfo, AppConstants.INTAKE_CONSTANTS.WAITNG_FOR_COURT_HEARING);
  }

  getIntakesForMyActions(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationMyActionsInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationMyActionsInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationMyActionsInfo.sortBy = pageInfo.sortDirection;
      this.paginationMyActionsInfo.sortColumn = pageInfo.sortColumn;
    }
    this.getIntakeSummary(this.paginationMyActionsInfo, AppConstants.INTAKE_CONSTANTS.COURT_ACTIONS_TO_BE_UPDATED);
    (<any>$('#my-actions-tab')).tab('show');
  }

  getSAOClosedIntakes(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationClosedIntakesInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationCourtActionsInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationClosedIntakesInfo.sortBy = pageInfo.sortDirection;
      this.paginationClosedIntakesInfo.sortColumn = pageInfo.sortColumn;
    }
    // need to get normal close also
    this.getIntakeSummary(this.paginationClosedIntakesInfo, AppConstants.INTAKE_CONSTANTS.SAO_CLOSED);
  }

  getIntakesWithCaseWorker(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationWithCaseWorkerInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationWithCaseWorkerInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationWithCaseWorkerInfo.sortBy = pageInfo.sortDirection;
      this.paginationWithCaseWorkerInfo.sortColumn = pageInfo.sortColumn;
    }
    // need to get normal close also
    this.getIntakeSummary(this.paginationWithCaseWorkerInfo, AppConstants.INTAKE_CONSTANTS.SAO_CLOSED);
  }

  getIntakesFromCaseWorker(pageInfo) {
    if (pageInfo && pageInfo.page) {
      this.paginationFromCaseWorkerInfo.pageNumber = pageInfo.page;
    } else {
      this.paginationFromCaseWorkerInfo.pageNumber = 1;
    }
    if (pageInfo && pageInfo.sortDirection && pageInfo.sortColumn) {
      this.paginationFromCaseWorkerInfo.sortBy = pageInfo.sortDirection;
      this.paginationFromCaseWorkerInfo.sortColumn = pageInfo.sortColumn;
    }
    // need to get normal close also
    this.getIntakeSummary(this.paginationFromCaseWorkerInfo, AppConstants.INTAKE_CONSTANTS.SAO_CLOSED);
  }

  pageChanged(pageInfo: any, actionType) {
    this.paginationCourtActionsInfo.pageNumber = pageInfo.page;
    this.getIntakeSummary(this.paginationCourtActionsInfo, actionType);
  }

  getIntakeSummary(paginationInfo: PaginationInfo, actiontype: string) {
    // this.saoTabIndex$.next(actiontype);
    this.tabIndex = actiontype;
    this.currentStatus = status;
    const pageSource = this.pageStreamIntake$.map(pageNumber => {
      paginationInfo.pageNumber = pageNumber;
      return {
        search: this.dynamicObjectIntakeSummary,
        page: pageNumber
      };
    });

    const searchSource = this.searchTermStreamIntake$
      .debounceTime(1000)
      .map(searchTerm => {
        this.dynamicObjectIntakeSummary = searchTerm;
        return { search: searchTerm, page: 1 };
      });

    const source = pageSource
      .merge(searchSource)
      .startWith({
        search: this.dynamicObjectIntakeSummary,
        page: paginationInfo.pageNumber
      })
      .flatMap((params: { search: DynamicObject; page: number }) => {
        if (this.intakeSummaryForm.value.intakenumber === '') {
          this.searchCriteria = {
            intakenumber: '',
            sortcolumn: paginationInfo.sortColumn,
            sortorder: paginationInfo.sortBy,
            actiontype: actiontype
          };
        } else {
          this.searchCriteria = {
            intakenumber: params.search.intakenumber,
            sortcolumn: paginationInfo.sortColumn,
            sortorder: paginationInfo.sortBy,
            actiontype: actiontype
          };
        }

        return this._commonService.getPagedArrayList(
          new PaginationRequest({
            limit: paginationInfo.pageSize,
            page: paginationInfo.pageNumber,
            method: 'get',
            where: this.searchCriteria
          }),
          SaoDashboardUrlConfig.EndPoint.Intake.dashboard + '?data'
        );
      })
      .subscribe(result => {
        this.intakesSummary = result.data;
        if (paginationInfo.pageNumber === 1) {
          this.totalRecords = result.count;
        }
      });
  }
  formIntakeSummaryInitilize() {
    this.intakeSummaryForm = this.formBuilder.group({
      intakenumber: ['']
    });
  }

  routToIntake(intakeId: string) {
    this._intakeUtils.redirectIntake(intakeId);
  }

  onSearch(field: string, value: string) {
    this.dynamicObjectIntakeSummary[field] = {
      like: '%25' + value + '%25'
    };
    if (!value) {
      delete this.dynamicObjectIntakeSummary[field];
    }
    this.searchTermStreamIntake$.next(this.dynamicObjectIntakeSummary);
  }

  // onSorted($event: ColumnSortedEvent) {
  //   this.paginationInfo.sortBy = $event.sortDirection;
  //   this.paginationInfo.sortColumn = $event.sortColumn;
  //   this.getIntakeSummary(
  //     this.paginationInfo, this.tabIndex
  //   );
  // }
}
