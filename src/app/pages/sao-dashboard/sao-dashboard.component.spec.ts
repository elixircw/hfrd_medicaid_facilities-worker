import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaoDashboardComponent } from './sao-dashboard.component';

describe('SaoDashboardComponent', () => {
  let component: SaoDashboardComponent;
  let fixture: ComponentFixture<SaoDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaoDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaoDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
