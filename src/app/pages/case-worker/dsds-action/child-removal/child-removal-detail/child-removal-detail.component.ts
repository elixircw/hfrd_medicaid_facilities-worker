import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../../@core/services';
import { AppConstants } from '../../../../../@core/common/constants';

@Component({
  selector: 'child-removal-detail',
  templateUrl: './child-removal-detail.component.html',
  styleUrls: ['./child-removal-detail.component.scss']
})
export class ChildRemovalDetailComponent implements OnInit {

  isCaseworker = false;

  constructor(private _authService: AuthService) { }

  ngOnInit() {
    this.isCaseworker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);

  }

}
