import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildRemovalDetailComponent } from './child-removal-detail.component';

describe('ChildRemovalDetailComponent', () => {
  let component: ChildRemovalDetailComponent;
  let fixture: ComponentFixture<ChildRemovalDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildRemovalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildRemovalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
