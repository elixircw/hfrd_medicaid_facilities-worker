import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ChildRemovalService } from './child-removal.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChildRemovalResolverService implements Resolve<any> {

  constructor(private _service: ChildRemovalService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

    return this._service.getPersonsAndChildRemovalInfo();
  }
}
