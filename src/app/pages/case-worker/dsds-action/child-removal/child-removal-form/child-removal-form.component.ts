import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChildRemovalService } from '../child-removal.service';
import { Subscription } from 'rxjs/Subscription';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { SpeechRecognitionService } from '../../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, AuthService, DataStoreService, SessionStorageService, GenericService } from '../../../../../@core/services';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';
import { ChildRemovalMapperService } from '../child-removal-mapper.service';
import { AppConstants } from '../../../../../@core/common/constants';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { HttpService } from '../../../../../@core/services/http.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonDisabilityService } from '../../../../shared-pages/person-disability/person-disability.service';
import { Assessments, GetintakAssessment, InvolvedPerson, RoutingInfo } from '../../../_entities/caseworker.data.model';
import { minDate } from 'ng4-validators/src/app/min-date/validator';
import { PaginationRequest, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { AppConfig } from '../../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { ChildRoles } from '../../child-removal/_entities/childremoval.model';
import { PersonInfoService } from '../../../../shared-pages/person-info/person-info.service';
import { PAGES_STORE_CONSTANTS } from '../../../../pages.constants';

const DEFAULT_FOSTER_CARE = 'FC';
const DEFAULT_REMOVAL_TYPE = 'JD'; // Judicial Determination
const CHILD_DISABILITY_VOLUNTARY_PLACEMENT = 'CDVP';
const TIME_LIMITED_VOLUNTARY = 'TLV';
const INDEPENDENT_LIVING_ENH_AFTERCARE = 'EHA';

const NO_EFFORTS_MADE = 'NREM';

const NEW_SERVICE_CASE = 1;
const OLD_SERVICE_CASE = 0;
@Component({
  selector: 'child-removal-form',
  templateUrl: './child-removal-form.component.html',
  styleUrls: ['./child-removal-form.component.scss']
})
export class ChildRemovalFormComponent implements OnInit, OnDestroy {

  removedChildren = [];
  removalSubscription: Subscription;
  speechRecogninitionOn: boolean;
  speechData: string;
  personAddress = {};
  minDate = new Date();
  selectedPersonid: any;
  currentLanguage: string;
  notification: string;
  recognizing = false;
  removalReason = []; sendTo = []; familyStructure = []; childRemovalType = [];
  exitCaseReasons = []; reasonableEfforts = []; reasonableEffortsNotMade = [];
  disabilityTypes = []; disabilityConditions = []; removalEndReasons = [];

  careGiverPersons = [];
  maxDate = new Date();
  yesterday = new Date();
  isVoluntaryAgreementType = false;
  childRemovalFormGroup: FormGroup;

  isCaseWorker = false;
  isSupervisor = false;

  serviceCaseNumber: string;
  approvalQueueIndex = 0;
  approvalQueueLength = 0;

  approveRejectQueueIndex = 0;
  approveRejectQueueLength = 0;

  message: string;

  sendApprovalDisabled = false;
  showparent1signature = true;
  showparent2signature = true;
  showparent2signmisreason = false;
  showYouthSignedDate: boolean;
  showbeginDate: boolean;
  showParentsSignature: boolean;
  showEndDate: boolean;
  showNotes: boolean;
  validList = [];
  isValidChildRemoval: boolean;
  isApproved: boolean;

  disabilityChild: any;
  personDisability: any;

  showNotMakingReasonableEfforts = false;
  notAddedDisabilityPersons = [];

  serviceCaseFormGroup: FormGroup;
  exitingServiceCaseList: any;
  exitingServiceCaseId: any;
  newservicecasenumber: any;

  CHILD_REMOVAL_SUBMIT = 0;

  id: string;
  daNumber: string;
  startAssessment$: any;
  getAsseesmentHistory: GetintakAssessment[] = [];
  showAssesment = -1;
  involvedPersons: any;
  routingInfo: RoutingInfo[];
  isServiceCase: string;
  inputRequest: Object;
  assessmentTemplateId: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  childDetail: ChildRoles[] = [];
  shelterChkBox = 0;
  ischildhomeaddress: boolean;
  isuploadedmanually: number;
  personDisabilities = [];
  hasDisability: boolean;
  disabilityConditionsType = [{
    'value_text': 'Yes',
    'description': 'Yes'
  },
  {
    'value_text': 'No',
    'description': 'No'
  },
  {
    'value_text': 'Unknown',
    'description': 'Unknown'
  }];
  disabilityTypesCheck: any[];
  enableEndRemoval = false;
  viewOnly = false;

  removalMinDate = null;
  isClosed = false;
  formDisabled: boolean;
  constructor(private _childRemovalService: ChildRemovalService,
    private _mapperService: ChildRemovalMapperService,
    private _formBuilder: FormBuilder,
    private speechRecognizer: SpeechRecognizerService,
    private _speechRecognitionService: SpeechRecognitionService,
    private _alertService: AlertService,
    private _intakeUtils: IntakeUtils,
    private _authService: AuthService,
    private _router: Router,
    private route: ActivatedRoute,
    private _disabilityService: PersonDisabilityService,
    private _dataStoreService: DataStoreService,
    private _commonService: CommonHttpService,
    private _http: HttpService,
    private _service: GenericService<Assessments>,
    private _servicePerson: PersonDisabilityService,
    private storage: SessionStorageService,
    private _personInfoService: PersonInfoService
  ) {
    this.speechRecogninitionOn = false;
    this.speechData = '';
    this.yesterday = new Date(this.minDate);
    this.yesterday.setDate(this.minDate.getDate() - 1);

    this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);

    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
  }


  ngOnInit() {
    this.initFormGroup();
    this.setDefaultValues();
    this.currentLanguage = 'en-US';
    this.notification = null;
    this.speechRecognizer.initialize(this.currentLanguage);
    this.processRemovalType();
    this.listenForChildRemoval();
    this.listenForChildRemovalInfo();
    this.listenForPersonDisability();
    if (this.isSupervisor) {
      this._childRemovalService.getChildList();
      this.loadRemovalChildrenInfo(this._childRemovalService.getReviewChildrenForSupervisor());
     // this.shelterChkBox = 1;
    }
    this._childRemovalService.removalConfig$.subscribe(data => {
      this.loadDropDownList();
      if (!this.isSupervisor) {
      //  this.shelterChkBox = 0;
        this.isuploadedmanually = null;
      }
    });

    this.inputRequest = {
      servicerequestid: this.id,
    };
    this.getPage(1);
    this.getAssessmentPrefillData();
    this.loadPersonDisabilityList();
    this.childDetails();
    if (this.isSupervisor) {
      this.shelterChkBox = 1;
    }

    this.childRemovalFormGroup
    .get('primarycaregiveradd')
    .valueChanges.subscribe(result => {
      if (!result) {
        this.childRemovalFormGroup.patchValue({
          isverifiedcaregiver1add: null
        });
      }
    });
    this.childRemovalFormGroup
    .get('seccaregiveradd')
    .valueChanges.subscribe(result => {
      if (!result) {
        this.childRemovalFormGroup.patchValue({
          isverifiedcaregiver2add: null
        });
      }
    });
    this.childRemovalFormGroup
    .get('childphysicalremovaladdress')
    .valueChanges.subscribe(result => {
      if (!result) {
        this.childRemovalFormGroup.patchValue({
          ischildphysicalremovaladdressverified: null
        });
      }
    });
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

  childDetails() {
    this._servicePerson.getDisabilityTypes().subscribe(response => {
      if (response && Array.isArray(response)) {
        this.disabilityTypesCheck = response;
        this.setDisabiltyFields();
      }
    });
  }

  loadPersonDisabilityList() {
    this._servicePerson.getDisabilityList(this._personInfoService.getPersonId()).subscribe(response => {
      if (response && Array.isArray(response) && response.length) {
        this.personDisabilities = response;
        this.setDisabiltyFields();
        if (this.personDisabilities.length > 0) {
          this.hasDisability = true;
        }
      } else {
        this.personDisabilities = [];
        this.hasDisability = false;
        this.setDisabiltyFields();
      }
    });
  }

  setDisabiltyFields() {
    if (this.disabilityTypesCheck && this.disabilityTypesCheck.length) {
      const personDisabilities = this.personDisabilities;
      this.disabilityTypesCheck.forEach(element => {
        element.conditionType = null;
        if (personDisabilities && personDisabilities.length) {
          const isDisabilityAvailable = personDisabilities.find(child => child.disabilitytypekey === element.ref_key);
          if (isDisabilityAvailable) {
            if (isDisabilityAvailable.comments === 'No_Disability' || isDisabilityAvailable.comments === 'Unknown_Disability') {
              element.conditionType = (isDisabilityAvailable.comments === 'No_Disability') ? 'No' : 'Unknown';
            } else {
              element.conditionType = isDisabilityAvailable.disabilityconditiontypekey;
            }
            element.disabled = true;
          } else {
            element.disabled = false;
          }
        } else {
          element.conditionType = null;
          element.disabled = false;
        }
      });
    }
  }

  getPage(page: number) {

    this._http.overrideUrl = false;
    this._http.baseUrl = AppConfig.baseUrl;
    this._childRemovalService.getAssessments().subscribe((result) => {
      this.startAssessment$ = result.data;
    });
  }

  private getAssessmentPrefillData() {
    this._childRemovalService.getAssessmentsInvolvedPersons().subscribe(result => {
      if (result && result['data']) {
        this.involvedPersons = result['data'];
      }
    });
    this._childRemovalService.getAssessmentsRoutingInfo().subscribe(result => {
      if (result && result['data']) {
        this.routingInfo = result['data'];
      }
    });
    // const source = forkJoin(
    //   this._service.getPagedArrayList(
    //     new PaginationRequest({
    //       page: 1,
    //       limit: 20,
    //       method: 'get',
    //       where: this.inputRequest
    //     }),
    //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
    //   ),
    //   this._service.getArrayList(
    //     new PaginationRequest({
    //       page: 1,
    //       limit: 50,
    //       method: 'get',
    //       where: { 'servicecaseid': this.id }
    //     }),
    //     CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.RoutingInfoList
    //   ),
    // ).subscribe(result => {
    //   if (result && result[0]['data']) {
    //     this.involvedPersons = result[0]['data'];
    //   }
    //   if (result && result[1].length > 0) {
    //     this.routingInfo = result[1][0]['routinginfo'];
    //   }
    // });
  }

  startAssessment(assessment: GetintakAssessment, mode) {
    assessment.mode = mode;
    const storeData = this._dataStoreService.getCurrentStore();

    storeData['CASEWORKER_INVOLVED_PERSON'] = this.involvedPersons;
    storeData['CASEWORKER_ROUTING_INFO'] = this.routingInfo;
    // storeData['CASEWORKER_PLACEMENT'] = this.placement;
    storeData['CASEWORKER_SELECTED_ASSESSMENT'] = assessment;
    storeData['CASEWORKER_CHILD_DETAIL'] = this.childDetail;
    this._dataStoreService.setObject(storeData, true, 'CASEWORKER_ASSESSMENT_LOAD');
    this._router.navigate(['../view'], { relativeTo: this.route });
    // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment/case-worker-view-assessment']);
  }
  initFormGroup() {
    this.childRemovalFormGroup = this._formBuilder.group({
      familystructuretypekey: [null],
      agencytypekey: [null, Validators.required],
      isdisability: [null],
      removaltypekey: [null, Validators.required],
      removaldate: [null, Validators.required],
      exitdate: [null],
      exittime: [null],
      removaltime: [null, Validators.required],
      rmvdfrmpersonname: [null],
      primarycaregiveractorid: [null, Validators.required],
      seccaregiveractorid: [null],
      removaladd1: [null],
      vpabegindate: [null],
      vpaenddate: [null],
      vpaparentssigneddate: [null],
      parent2signeddate: [null],
      vpaguardiansigneddate: [null],
      parent1id: [null],
      parent2id: [null],
      guardianid: [null],
      vpa2parentssigneddate: [null],
      parent2sigmissreason: [null],
      primarycaregiveradd: [null],
      ischildaddressasprimaryaddress: [null, Validators.required],
      childhomeaddress: [null],
      childphysicalremovaladdress: [null],
      ischildphysicalremovaladdressverified: [null],
      seccaregiveradd: [null],
      isverifiedcaregiver1add: [false, Validators.required],
      isverifiedcaregiver2add: [false],
      vpayouthsigneddate: [null],
      isbothparentssigned: [null],
      volrelinquishment: [null],
      agencysigneddate: [null],
      removalreason: [null, Validators.required],
      removalexitreason: [null],
      caregiverreason: [null],
      reasonableefforts: [null],
      notmakingefforts: [null],
      specifiedrelativename: [null],
      returndate: [null],
      returntime: [null],
      specifiedrelativedatechildlastlivedwith: [null],
      exitreason: [null],
      parent2comments: [''],
      comments: ['', Validators.required],
      reasonableeffortsmade: [null],
      isverifiedreporteradd: [null],
      placement: [''],
      familyhistory: [''],
      childdesc: [null],
      // primarycaregiverid: [null],
      //  Validators.required removed in on init please do validation on @showTypeofRemoval()
    });



    this.serviceCaseFormGroup = this._formBuilder.group({
      intakeserviceid: [''],
      servicecaseid: [null],
      isnewcase: ['']
    });
  }

  setDefaultValues() {
    this.childRemovalFormGroup.patchValue({ 'removaltypekey': DEFAULT_REMOVAL_TYPE });
    this.childRemovalFormGroup.patchValue({ 'agencytypekey': DEFAULT_FOSTER_CARE });
    // this.dispositionFormGroup.patchValue({ intakeserreqstatustypekey: 'Closed'
  }

  listenForChildRemoval() {
    this.removalSubscription = this._childRemovalService.childRemoval$.subscribe(child => {
      console.log('removed child', child);
      this.removedChildren = this._childRemovalService.getRemovedChildren();
      if (this.removedChildren.length) {
        this.sendApprovalDisabled = false;
      }
      if (this.removedChildren.length === 1) {
        this.childRemovalFormGroup.reset();
        this.setDefaultValues();
        this.processRemovalType();
      }
      this.loadDropDownList();
    });

  }

  listenForChildRemovalInfo() {
    this.removalSubscription = this._childRemovalService.childRemovalInfo$.subscribe(children => {
      console.log('removed children', children);
      this.loadRemovalChildrenInfo(children);
      if (children && children.length) {
        this.removalMinDate = new Date(children[0].dob);
        console.log('Min Removal Data', this.removalMinDate);
      }

    });

  }

  listenForPersonDisability() {
    this.removalSubscription = this._dataStoreService.currentStore.subscribe(store => {
      if (store.SUBSCRIPTION_TARGET === 'DISABILITY') {
        this._childRemovalService.loadPersonDisabilities();
      }
    });
  }
  loadRemovalChildrenInfo(children) {
    this.removedChildren = children;
    if (this.removedChildren && this.removedChildren.length > 0) {
      setTimeout( () => {
       // $('#child-removal').scrollTop(0);
       const el = document.getElementById('new-child-removel');
       el.scrollIntoView();
       window.scrollBy(0, 94);
      }, 100);
    }
    this.loadDropDownList();
    // const lastIndex = this._childRemovalService.childRemovalInfo.length - 1;
    if (Array.isArray(children) && children.length > 0) {
      const unMappedData = this._mapperService.unMapChildRemovalInfo(children[0].removalInfo);
      console.log('unMappedData', unMappedData);
      // unMappedData.removaltime = moment(unMappedData.removaltime).format('hh:mm A');
      this.isApproved = (unMappedData && unMappedData.approvalstatus === 'Approved') ? true : false;
      this.shelterChkBox = unMappedData.isshelterauthcompleted;
      this.isuploadedmanually = unMappedData.isuploadedmanually ? unMappedData.isuploadedmanually : 0;
      if (unMappedData.removaladd1 && unMappedData.ischildaddressasprimaryaddress === 1) {
        unMappedData.isverifiedreporteradd = 1;
      }
      this.childRemovalFormGroup.patchValue(unMappedData);
      this.processRemovalType();
      this.processActionButtons(children);
    }

  }

  loadDropDownList() {
    this.removalReason = this._childRemovalService.removalReason;
    this.sendTo = this._childRemovalService.sendTo;
    this.familyStructure = this._childRemovalService.familyStructure;
    this.childRemovalType = this._childRemovalService.childRemoval;
    this.exitCaseReasons = this._childRemovalService.exitCaseReasons;
    this.reasonableEfforts = this._childRemovalService.reasonableEfforts;
    this.reasonableEffortsNotMade = this._childRemovalService.reasonableEffortsNotMade;
    this.disabilityTypes = this._childRemovalService.disabilityTypes;
    this.disabilityConditions = this._childRemovalService.disabilityConditions;
    this.removalEndReasons = this._childRemovalService.removalEndReasons;

    this.careGiverPersons = this._childRemovalService.getCareGiverList();
    console.log('cgp', this.careGiverPersons);
  }

  filterCareGiver( { roles }) {
    return (_.find(roles, {intakeservicerequestpersontypekey: 'LG'}) || _.find(roles, {intakeservicerequestpersontypekey: 'PARENT'}));
  }

  castToInt(str) {
    return _.parseInt(str, 10) || '';
  }

  activateSpeechToText(type): void {
    this.recognizing = type;
    this.speechRecogninitionOn = !this.speechRecogninitionOn;
    if (this.speechRecogninitionOn) {
      this._speechRecognitionService.record().subscribe(
        // listener
        (value) => {
          this.speechData = value;
          switch (type) {
            case 'comments':
              let comments = this.childRemovalFormGroup.getRawValue().comments;
              if (comments) {
                comments = comments + ' ' + this.speechData;
              } else {
                comments = this.speechData;
              }
              this.childRemovalFormGroup.patchValue({ comments: comments });
              break;
            case 'parent_comments':
              let parent_comments = this.childRemovalFormGroup.getRawValue().parent2comments;
              if (parent_comments) {
                parent_comments = parent_comments + ' ' + this.speechData;
              } else {
                parent_comments = this.speechData;
              }
              this.childRemovalFormGroup.patchValue({ parent2comments: parent_comments + ' ' + this.speechData });
              break;
            case 'placement':
              const placement = this.childRemovalFormGroup.getRawValue().placement;
              this.childRemovalFormGroup.patchValue({ placement: placement + ' ' + this.speechData });
              break;
            case 'familyhistory':
              const familyhistory = this.childRemovalFormGroup.getRawValue().familyhistory;
              this.childRemovalFormGroup.patchValue({ familyhistory: familyhistory + ' ' + this.speechData });
              break;
            case 'childdesc':
              const childdesc = this.childRemovalFormGroup.getRawValue().childdesc;
              this.childRemovalFormGroup.patchValue({ childdesc: childdesc + ' ' + this.speechData });
              break;
            default: break;
          }
        },
        // errror
        (err) => {
          console.log(err);
          this.recognizing = false;
          if (err.error === 'no-speech') {
            this.notification = `No speech has been detected. Please try again.`;
            this._alertService.warn(this.notification);
            this.activateSpeechToText(type);
          } else if (err.error === 'not-allowed') {
            this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
            this._alertService.warn(this.notification);
            // this.activateSpeechToText(type);
          } else if (err.error === 'not-microphone') {
            this.notification = `Microphone is not available. Please verify the connection of your microphone and try again.`;
            this._alertService.warn(this.notification);
            // this.activateSpeechToText(type);
          }
        },
        // completion
        () => {
          this.speechRecogninitionOn = true;
          console.log('--complete--');
          this.activateSpeechToText(type);
        }
      );
    } else {
      this.recognizing = false;
      this.deActivateSpeechRecognition();
    }
  }

  deActivateSpeechRecognition() {
    this.speechRecogninitionOn = false;
    this._speechRecognitionService.destroySpeechObject();
  }

  ngOnDestroy(): void {
    this._speechRecognitionService.destroySpeechObject();
    this.removalSubscription.unsubscribe();
  }
  selectPerson(cjamsPid) {
    if (this.selectedPersonid !== cjamsPid) {
      this.selectedPersonid = cjamsPid;
    } else {
      this.selectedPersonid = null;
    }
   }
  processRemovalType() {
    if (this.removedChildren && this.removedChildren.length) {
      // this.removalMinDate = new Date(this.removedChildren[0].dob);
       const dob = new Date(this.removedChildren[0].dob);
       dob.setUTCHours(0);
       dob.setHours(0, 0, 0, 0);
       this.removalMinDate  = dob;
      //  var year = dob.getFullYear();
      //  var month = dob.getMonth();
      //  var day = dob.getDate()
      //  var birthdate = new Date(Date.UTC(year, month, day, 0, 0, 0))
      //  this.removalMinDate = new Date(dob.getTime() - (1 * 24 * 60 * 60 * 1000));
    }
    const removalForm = this.childRemovalFormGroup.getRawValue();
    if ([CHILD_DISABILITY_VOLUNTARY_PLACEMENT, TIME_LIMITED_VOLUNTARY].includes(removalForm.removaltypekey)) {
      this.isVoluntaryAgreementType = true;
      this.showYouthSignedDate = false;
      this.showNotes = true;
      this.showEndDate = true;
      this.showParentsSignature = true;
      this.showbeginDate = true;
      this.setVoluntaryPlacementValidators();
    } else if (removalForm.removaltypekey === INDEPENDENT_LIVING_ENH_AFTERCARE) {
      this.isVoluntaryAgreementType = true;
      this.showYouthSignedDate = true;
      this.showNotes = false;
      this.showEndDate = false;
      this.showParentsSignature = false;
      this.showbeginDate = false;
      this.clearVoluntaryPlacementValidators();
    } else {
      this.isVoluntaryAgreementType = false;
      this.showYouthSignedDate = false;
      this.clearVoluntaryPlacementValidators();
    }
    this.parentsignedAggmntChange();
    this.reasonableEffortsChanged();
  }

  getPersonAddress(personid, controlName) {
    if (personid) {
      this._childRemovalService.getAddressPerson(personid).subscribe(
        (response: any) => {
          if (response && response.length) {
            let fullAddress = '';
            const currentAddress = response.filter( address => address.currentlocationflag );
            if (currentAddress && currentAddress.length) {
              fullAddress = (currentAddress[0].address ? currentAddress[0].address : '') +
              (currentAddress[0].address2 ? ',' + currentAddress[0].address2 : '') +
              (currentAddress[0].city ? ', ' + currentAddress[0].city : '') +
              (currentAddress[0].state ? ', ' + currentAddress[0].state : '');
            }
            // (response[0].county ? ',' + response[0].county : '') +
            // (response[0].country ? ',' + response[0].country : '');
            const obj = {};
            obj[controlName] = fullAddress;
            this.childRemovalFormGroup.patchValue(obj);
          }

        }
      );
    }
  }

  saveAsDraft() {
    this.approvalQueueIndex = 0;
    this.approvalQueueLength = this.removedChildren.length;
    this.CHILD_REMOVAL_SUBMIT = 0; // save as draft
    this.submitForApprovalQueue();
  }

  submitChildRemoval() {
    console.log(this._dataStoreService.getData(PAGES_STORE_CONSTANTS.PERSON_DISABILITY_PRINSTINE));
    const disabilityChangesMade = this._dataStoreService.getData(PAGES_STORE_CONSTANTS.PERSON_DISABILITY_PRINSTINE);
    if (disabilityChangesMade) {
      // this._alertService.warn('Changes to child disability has been made. please save to proceed.');
      (<any>$('#disability-check-popup')).modal('show');
      return;
    }
    const removalInfo = this.childRemovalFormGroup.getRawValue();

    if (this.childRemovalFormGroup.invalid || (this.shelterChkBox === 1 && this.isuploadedmanually == null) || this.shelterChkBox === null) {
      console.log(this._intakeUtils.findInvalidControls(this.childRemovalFormGroup));
      this._alertService.error('Please fill required fields');
      return;
    }

    if ((removalInfo.removaladd1 && !removalInfo.isverifiedreporteradd) ||
    (removalInfo.childphysicalremovaladdress && !removalInfo.ischildphysicalremovaladdressverified) ||
    (removalInfo.seccaregiveradd && !removalInfo.isverifiedcaregiver2add)) {
      this._alertService.error('Please fill required fields');
      return;
    }
    const removaldate = this.childRemovalFormGroup.getRawValue().removaldate;
    if ((removaldate && moment(new Date(removaldate)))) {
      const removalDate = new Date(removaldate);
      const toDate = new Date();
      if (removalDate.getTime() > toDate.getTime()) {
        this._alertService.error('Child Removal date can not be a future date');
        return;
      }
    }

    const disabilityNotChosen = this.removedChildren.filter(child => child.hasDisability === null);

    if (!disabilityNotChosen || disabilityNotChosen.length > 0) {
      this._alertService.error('Please fill required fields');
      return;
    }

    if ([CHILD_DISABILITY_VOLUNTARY_PLACEMENT, TIME_LIMITED_VOLUNTARY].includes(removalInfo.removaltypekey)) {
      if (!removalInfo.vpaparentssigneddate && !removalInfo.vpaguardiansigneddate) {
        (<any>$('#error-info')).modal('show');
        return;
      }
    }

    const disabilityEnabledChildren = this.removedChildren.filter(child => child.hasDisability === 1);
    if (disabilityEnabledChildren && disabilityEnabledChildren.length > 0) {
      let notAddedDisabilityCount = 0;
      this.notAddedDisabilityPersons = [];
      disabilityEnabledChildren.forEach(child => {
        if (child.personDisabilities.length === 0) {
          notAddedDisabilityCount++;
          this.notAddedDisabilityPersons.push(child);
        }
      });
      if (notAddedDisabilityCount > 0) {
        this._alertService.error('Please add a disability');
        return;
      }
    }

    if (this.shelterChkBox === 0) {
      (<any>$('#confirm-shelter-popup')).modal('show');
      return;
    }




    this.validatechildRemoval();


  }

  getLegalCustody(childActorId) {
    this._commonService.getArrayList({
      method: 'get',
      where: {
        personid: childActorId
      }
    }, 'legalcustody/getlegalcustody?filter').subscribe( res => {
      let hascustody = false;
      if (res && res.length && res[0].getlegalcustody) {
        const legalCustodyDetails = res[0].getlegalcustody;
        hascustody = (Array.isArray(legalCustodyDetails) && legalCustodyDetails.length) ? true : false;
      }

      this.validList.forEach(item => {
        item.hasLG = hascustody;
        if (!(item['validMDM'] && item['hasLG'] && item['isadult'] && item['isvalidremovalDate']
          && item['primCitizenReq'])) {
          this.isValidChildRemoval = false;
        }
      });
    });
  }

  triggerChildRemoval() {
    (<any>$('#removal-verification')).modal('hide');
    if (this.isValidChildRemoval) {
      this.serviceCaseNumber = null;
      const childRemovalData = this.childRemovalFormGroup.getRawValue();
      childRemovalData.isshelterauthcompleted = (this.shelterChkBox != null) ? this.shelterChkBox : 0;
      childRemovalData.isuploadedmanually = (this.isuploadedmanually != null) ? this.isuploadedmanually : 0;
      const formatedData = this._mapperService.mapChildRemovalFormData(childRemovalData);
      console.log('Child Removal data', formatedData);
      this.approvalQueueIndex = 0;
      this.approvalQueueLength = this.removedChildren.length;
      this.CHILD_REMOVAL_SUBMIT = 1;
      this.submitForApprovalQueue();
    } else {
      this._alertService.error('Please complete all checklist');
    }
  }

  serviceCaseCreation() {
    if (this.isValidChildRemoval) {
      this._childRemovalService.serviceCaseCreateOrCheckIsExist().subscribe(scResponse => {
        console.log('service case validate Response', scResponse);
        if (scResponse && scResponse.data && scResponse.data.length) {
          if (scResponse.isavailable === 1) {
            this.exitingServiceCaseList = scResponse.data;
            (<any>$('#ServiceCaseValidation')).modal('show');
          } else {
            this.serviceCaseNumber = scResponse.data[0].servicecasenumber;
            console.log('Child serviceCaseNumber', this.serviceCaseNumber);
            (<any>$('#removal-ackmt')).modal('show');
            // this._alertService.success('Service case created successfully. Servicecase #' + result.data[0].servicecaseno);
          }

        }

        console.log('Child removed', scResponse);

      });
    } else {
      (<any>$('#removal-verification')).modal('hide');
    }
  }

  findServiceCaseId() {
    const removalIfo = this._childRemovalService.childRemovalInfo;
    let serviceCaseId = null;
    if (removalIfo && Array.isArray(removalIfo) && removalIfo.length) {
      const ri = removalIfo.find(item => item.servicecaseid !== null);
      serviceCaseId = (ri) ? ri.servicecaseid : null;
    }

    return serviceCaseId;
  }

  validatechildRemoval() {
    const lgList = this._childRemovalService.getLegalGuardianList();
    const remvalChildList = this._childRemovalService.getRemovedChildren();
    this.isValidChildRemoval = true;
    this.validList = [];
    this.validList.length = 0;
    this.validList = remvalChildList.map(child => {
      const obj = {};
      obj['validMDM'] = true; // (child.mdm === 'Y') ? true : false; un comment this line once mdm related proc solved
      // obj['hasLG'] = (lgList && lgList.length) ? true : false;
      obj['isadult'] = (this.calculateAge(child.dob) > 18) ? false : true;
      obj['isvalidremovalDate'] = this.isremovaldateafterchilddob(child);
      obj['primCitizenReq'] = (child.citizenalenageflag === 1 || child.citizenalenageflag === 0) ? true : false;
      if (!(obj['validMDM'] && obj['isadult'] && obj['isvalidremovalDate']
        && obj['primCitizenReq'])) {
        this.isValidChildRemoval = false;
      }
      obj['cjamspid'] = child.cjamspid;
      obj['firstname'] = child.firstname;
      obj['lastname'] = child.lastname;
      return obj;
    });
    if (this.validList.length) {
      this.getLegalCustody(remvalChildList[0].intakeservicerequestactorid);
    }
    (<any>$('#removal-verification')).modal('show');
  }

  submitForApprovalQueue() {
    const actionSummary = this._dataStoreService.getData('dsdsActionsSummary');
    if (actionSummary.da_subtype === 'IHS') {
      this.approvalSend();
    } else {
      if (this.approvalQueueIndex < this.removedChildren.length) {
        this.approvalSend();
      } else {
        if (this.CHILD_REMOVAL_SUBMIT === 1) {

          if (this._childRemovalService.isServiceCase()) {
            (<any>$('#service-case-removal-ackmt')).modal('show');
          } else {
            const serviceCaseId = this.findServiceCaseId();
            if (serviceCaseId) {
              this._childRemovalService.serviceCaseCreatOrMerge(serviceCaseId, OLD_SERVICE_CASE).subscribe(response => {
                console.log('service case create', response);
                (<any>$('#ServiceCaseValidation')).modal('hide');
                this._alertService.success('Service case merged successfully with Service case #' + response[0].servicecaseno);
                this.serviceCaseNumber = response[0].servicecaseno;
                console.log('Child serviceCaseNumber', this.serviceCaseNumber);
                (<any>$('#removal-ackmt')).modal('show');
              });
            } else {
              this._childRemovalService.serviceCaseCreatOrMerge(null, NEW_SERVICE_CASE).subscribe(response => {
                if (response && Array.isArray(response)) {
                  this._alertService.success('Service case created successfully with Service case #' + response[0].servicecaseno);
                  this.serviceCaseNumber = response[0].servicecaseno;
                  console.log('Child serviceCaseNumber', this.serviceCaseNumber);
                  (<any>$('#removal-ackmt')).modal('show');
                }
              });
            }
          }

          // this.serviceCaseCreation();
          this.sendApprovalDisabled = true;
        } else {
          this._alertService.success('saved successfully');
          this._childRemovalService.getPersonsAndChildRemovalInfo().subscribe(response => {
            console.log('crp', response);
          });
        }

      }
    }
  }

  approvalSend() {
    const childRemovalData = this.childRemovalFormGroup.getRawValue();
    childRemovalData.isshelterauthcompleted = (this.shelterChkBox != null) ? this.shelterChkBox : 0;
    childRemovalData.isuploadedmanually = (this.isuploadedmanually != null) ? this.isuploadedmanually : 0;
    const formatedData = this._mapperService.mapChildRemovalFormData(childRemovalData);
    const removedChild = this.removedChildren[this.approvalQueueIndex];
    formatedData.intakeservicerequestactorid = removedChild.intakeservicerequestactorid;
    formatedData.isreviewsubmit = this.CHILD_REMOVAL_SUBMIT;
    if (removedChild.hasOwnProperty('intakeservreqchildremovalid')) {
      formatedData.intakeservreqchildremovalid = removedChild.intakeservreqchildremovalid;
    }

    console.log('saving', formatedData);
    // need to send as disability array as service dependency sending as object
    this._childRemovalService.sendForApproval(formatedData).subscribe(item => {
      if (this.approvalQueueIndex < this.approvalQueueLength) {
        this.approvalQueueIndex++;
        this.submitForApprovalQueue();
      }

    });
  }

  removalAcknowledged() {
    (<any>$('#removal-ackmt')).modal('hide');
  }

  reject() {
    this.approveRejectQueueIndex = 0;
    const childRemovalId = this.removedChildren[this.approveRejectQueueIndex].intakeservreqchildremovalid;
    this.rejectionQueue(childRemovalId);
  }

  approve() {
    this.approveRejectQueueIndex = 0;
    const childRemovalId = this.removedChildren[this.approveRejectQueueIndex].intakeservreqchildremovalid;
    this.approveQueue(childRemovalId);
  }

  rejectionQueue(remvoalID) {
    this._childRemovalService.rejectChildRemoval(remvoalID).subscribe(removal => {
      this.approveRejectQueueIndex++;
      if (this.approveRejectQueueIndex < this.removedChildren.length) {
        const childRemovalId = this.removedChildren[this.approveRejectQueueIndex].intakeservreqchildremovalid;
        this.rejectionQueue(childRemovalId);
      } else {
        this.rejectionCompleted(removal);
      }
    });
  }

  rejectionCompleted(data) {
    console.log('rejected', data);
    // (<any>$('#approve-reject-ackmt')).modal('show');
    this.message = 'Child removal rejected successfully';
    this.isApproved = true;
    this._alertService.success(this.message, true);
    this._router.navigate(['/pages/cjams-dashboard/cw-approval']);
  }

  approveCompleted(data) {
    console.log('approved', data);
    this.message = 'Child removal approved successfully.';
    this.isApproved = true;
    this._alertService.success(this.message, true);
    // this.newservicecasenumber = result[0].servicecasenumber;
    if (this._childRemovalService.childRemovalInfo && this._childRemovalService.childRemovalInfo.length) {
      const ri = this._childRemovalService.childRemovalInfo.find(removal => removal.servicecasenumber !== null);
      this.newservicecasenumber = (ri) ? ri.servicecasenumber : null;
      if (this._childRemovalService.isServiceCase()) {
        (<any>$('#show-approval-success')).modal('show');
      } else {
        (<any>$('#show-new-service-case-number')).modal('show');
      }

    }

    // this.getnewservicecaselist();
    // (<any>$('#approve-reject-ackmt')).modal('show');
  }


  getnewservicecaselist() {
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        limit: 1,
        page: 1,
        method: 'get',
        where: { 'status': 'OPEN' }

      }),
      'Intakedastagings/servicecaseassignlist?filter').subscribe((result) => {
        if (result && result[0]) {
          this.newservicecasenumber = result[0].servicecasenumber;
          (<any>$('#show-new-service-case-number')).modal('show');
        }
      });
  }

  navigatetohome() {
    if (this.isServiceCase) {
      (<any>$('#show-approval-success')).modal('hide');
      (<any>$('#show-new-service-case-number')).modal('hide');
      this._router.navigate(['/pages/cjams-dashboard/cw-approval']);
    } else {
      (<any>$('#show-new-service-case-number')).modal('hide');
      (<any>$('#show-approval-success')).modal('hide');
      this._router.navigate(['/pages/cjams-dashboard/cw-assign-service-case']);
    }

  }

  closeApproveReject() {
    (<any>$('#approve-reject-ackmt')).modal('hide');
  }

  approveQueue(remvoalID) {
    this._childRemovalService.approveChildRemoval(remvoalID).subscribe(removal => {
      this.approveRejectQueueIndex++;
      if (this.approveRejectQueueIndex < this.removedChildren.length) {
        const childRemovalId = this.removedChildren[this.approveRejectQueueIndex].intakeservreqchildremovalid;
        this.approveQueue(childRemovalId);
      } else {
        this.approveCompleted(removal);
      }
    });

  }
  parentsignedAggmntChange() {
    console.log(event);
    const value = this.childRemovalFormGroup.getRawValue().isbothparentssigned;
    if (value === 1) {
      this.showparent1signature = true;
      this.showparent2signature = true;
      this.showparent2signmisreason = false;
    } else if (value === 2) {
      this.showparent1signature = true;
      this.showparent2signature = false;
      this.showparent2signmisreason = true;
    }
  }

  resonableeffortsmadeChange(event) {

  }
  calculateAge(dob) {
    // const dob = this.selectedPerson.dob;
    let age = 0;
    if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
      const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
      age = moment().diff(rCDob, 'years');
    }
    return age;
  }

  isremovaldateafterchilddob(child) {
    const removaldate = this.childRemovalFormGroup.getRawValue().removaldate;
    const dob = child.dob;
    if ((removaldate && moment(new Date(removaldate))) && (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid())) {
      const removalDate = moment(new Date(removaldate));
      const dobDate = moment(new Date(dob));
      if (removalDate.isSameOrAfter(dobDate, 'day')) {
        return true;
      }
    }
    return false;
  }

  openDisabilityForm(child) {
    this.disabilityChild = child;
    this._router.navigate(['disability/' + child.personid + '/create'], { relativeTo: this.route });
  }

  openDisabilityList(child) {
    this.disabilityChild = child;
    this._router.navigate(['disability/' + child.personid + '/list'], { relativeTo: this.route });
  }


  reasonableEffortsChanged() {
    const efforts = this.childRemovalFormGroup.getRawValue().reasonableefforts;
    if (efforts && Array.isArray(efforts) && efforts.length > 0) {
      if (efforts.indexOf(NO_EFFORTS_MADE) !== -1) {
        this.showNotMakingReasonableEfforts = true;
        // this.childRemovalFormGroup.get('notmakingefforts').enable();
      } else {
        this.showNotMakingReasonableEfforts = false;
        // this.childRemovalFormGroup.get('notmakingefforts').disable();
      }
      console.log(efforts);
    }
  }

  deleteDisability() {
    this._disabilityService.deleteDisability(this.personDisability.persondisabilityid).subscribe(response => {
      this._alertService.success('Disability deleted successfully');
      this._childRemovalService.loadPersonDisabilities();
    });
  }

  confirmDeleteDisability(personDisability) {
    this.personDisability = personDisability;
  }

  checkForNoDisabilityAdded(child) {
    if (child.personDisabilities.length > 0) {
      child.hasDisability = 1;
      this.removedChildren.forEach(removedChild => {
        if (removedChild.personid === child.personid) {
          removedChild.hasDisability = 1;
        }
      });
      this._childRemovalService.loadPersonDisabilities();
      (<any>$('#alert-message')).modal('show');
    }
  }

  selectCase(selectedCase) {
    this.exitingServiceCaseId = selectedCase.servicecaseid;
  }

  createServiceCase(isnewcase) {
    if (isnewcase === 1) {
      this._childRemovalService.serviceCaseCreatOrMerge(null, isnewcase).subscribe(response => {
        console.log('service case create', response);
        (<any>$('#ServiceCaseValidation')).modal('hide');
        if (response && Array.isArray(response)) {
          this._alertService.success('Service case created successfully. Servicecase #' + response[0].servicecaseno);
          this.serviceCaseNumber = response[0].servicecaseno;
          console.log('Child serviceCaseNumber', this.serviceCaseNumber);
          (<any>$('#removal-ackmt')).modal('show');
        }

      });
    } else {
      if (!this.exitingServiceCaseId) {
        this._alertService.error('Please Select Case');
        return false;
      }
      this._childRemovalService.serviceCaseCreatOrMerge(this.exitingServiceCaseId, isnewcase).subscribe(response => {
        console.log('service case create', response);
        (<any>$('#ServiceCaseValidation')).modal('hide');
        this._alertService.success('Service case merged successfully. Servicecase #' + response[0].servicecaseno);
        this.serviceCaseNumber = response[0].servicecaseno;
        console.log('Child serviceCaseNumber', this.serviceCaseNumber);
        (<any>$('#removal-ackmt')).modal('show');

      });
    }
  }
  childAddressAsPrimary() {
    const value = this.childRemovalFormGroup.getRawValue().ischildaddressasprimaryaddress;
    if (value === 2) {
      this.childRemovalFormGroup.get('removaladd1').setValidators([Validators.required]);
      this.childRemovalFormGroup.get('removaladd1').updateValueAndValidity();
      this.ischildhomeaddress = true;
    } else {
      this.childRemovalFormGroup.get('removaladd1').clearValidators();
      this.childRemovalFormGroup.get('removaladd1').updateValueAndValidity();
      this.ischildhomeaddress = false;
    }
  }

  onChangechildAddress(value) {
    const primaryAddress = this.childRemovalFormGroup.getRawValue().primarycaregiveradd;
    if (value === 1) {
      this.childRemovalFormGroup.patchValue({ removaladd1: primaryAddress });
      this.childRemovalFormGroup.patchValue({ isverifiedreporteradd: 1 });
    } else {
      this.childRemovalFormGroup.patchValue({ removaladd1: null });
      this.childRemovalFormGroup.patchValue({ isverifiedreporteradd: null });
    }
  }

  showAssessment(id: number, row) {
    /* for (let j = 0; j < row.length; j++) {
         if (row[j].score === -1) {
             this.notApplicableScore = 'N/A';
         }
     } */
    this.getAsseesmentHistory = row;
    if (this.showAssesment !== id) {
      this.showAssesment = id;
    } else {
      this.showAssesment = -1;
    }
  }
  isIconDisabled(modal) {
    if (this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR)) {
      if (modal.assessmentstatustypekey === 'InProcess' || modal.assessmentstatustypekey === 'Rejected' || modal.assessmentstatustypekey === 'Accepted') {
        return 'icon-disabled';
      }
    } else {
      return;
    }
  }

  actionIconDisplay(modal, status: string): boolean {
    if (status === 'View') {
      return modal.assessmentstatustypekey !== 'Open' && modal !== null;
    } else if (status === 'Edit') {
      const cwstatusList = ['Accepted']; // Rejected is removed based on UAT team request on 03072019
      const supStatusList = ['Open', 'Accepted'];
      return (!cwstatusList.includes(modal.assessmentstatustypekey) && modal !== null)
        || (this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR)
          && !supStatusList.includes(modal.assessmentstatustypekey));
    } else if (status === 'Print') {
      return modal.assessmentstatustypekey !== 'Open' && modal !== null;
    } else if (status === 'InProcess') {
      return modal.assessmentstatustypekey === 'InProcess' && modal !== null;
    }
    return false;
  }
  confirmDelete(assessmentid, index) {
    // a.index = index;
    console.log('assessmentid....index', assessmentid, index);
    this.assessmentTemplateId = assessmentid;
    (<any>$('#delete-assessment-popup')).modal('show');
  }
  deleteAssessment() {
    this._commonService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.DeleteAssessment;
    this._commonService.create({
      assessmentid: this.assessmentTemplateId,
      // method: 'post'
    })
      .subscribe(
        response => {
          if (response) {
            this._alertService.success(
              'Assessment deleted successfully'
            );
            (<any>$('#delete-assessment-popup')).modal('hide');
            // this.getPage(1);
            this.getAsseesmentHistory = null;
            this.getPage(1);
            this.showAssesment = -1;
          }
        }
      );
  }

  close() {
    this.removedChildren = [];
    this.shelterChkBox = 0;
    this.isuploadedmanually = null;
    this.enableEndRemoval = false;
    this.sendApprovalDisabled = false;
    this.viewOnly = false;
    this._childRemovalService.getPersonsAndChildRemovalInfo().subscribe(response => {
      console.log('crp', response);
    });

  }

  processActionButtons(children) {
    this.sendApprovalDisabled = false;
    const draftChildRemoval = children.filter(child => child.removalStatus === 'Draft');
    if (draftChildRemoval.length === children.length) {
      this.sendApprovalDisabled = false;
    }
    const reviewChildRemoval = children.filter(child => child.removalStatus === 'Review');
    if (reviewChildRemoval.length === children.length && this.isCaseWorker) {
      this.sendApprovalDisabled = true;
    }
    // Enable the Send for approval if Child removal active again after reunification or placement end date in any permanency plan closure
    const enableEndRemoval = children.filter(child => child.enableEndRemoval === true);
    if (enableEndRemoval && enableEndRemoval.length) {
      this.enableEndRemoval = true;
      this.formDisabled = true;
      this.childRemovalFormGroup.disable();
      this.childRemovalFormGroup.controls['exitdate'].enable();
      this.childRemovalFormGroup.controls['exittime'].enable();
      this.childRemovalFormGroup.controls['removalexitreason'].enable();
    } else {
      this.enableEndRemoval = false;
      this.formDisabled = false;
      this.childRemovalFormGroup.enable();
    }

    const viewOnlyRemoval = children.filter(child => child.viewOnly === true);
    if (viewOnlyRemoval && viewOnlyRemoval.length) {
      this.viewOnly = true;
    } else {
      this.viewOnly = false;
    }


  }

  setVoluntaryPlacementValidators() {
    this.setRequriedValidtor('vpabegindate');
    this.setRequriedValidtor('vpaenddate');
    this.setRequriedValidtor('agencysigneddate');
    // this.setRequriedValidtor('vpaparentssigneddate');
  }

  clearVoluntaryPlacementValidators() {
    this.clearValidators('vpabegindate');
    this.clearValidators('vpaenddate');
    this.clearValidators('agencysigneddate');
    // this.clearValidators('vpaparentssigneddate');
  }

  setRequriedValidtor(formControlName: string) {
    this.childRemovalFormGroup.get(formControlName).setValidators([Validators.required]);
    this.childRemovalFormGroup.get(formControlName).updateValueAndValidity();
  }
  clearValidators(formControlName: string) {
    this.childRemovalFormGroup.get(formControlName).clearValidators();
    this.childRemovalFormGroup.get(formControlName).updateValueAndValidity();
  }


}
