import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildRemovalComponent } from './child-removal.component';

describe('ChildRemovalComponent', () => {
  let component: ChildRemovalComponent;
  let fixture: ComponentFixture<ChildRemovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildRemovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildRemovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
