import { Component, OnInit } from '@angular/core';
import { ChildRemovalService } from '../child-removal.service';
import { PersonInfoService } from '../../../../shared-pages/person-info/person-info.service';
import { DataStoreService } from '../../../../../@core/services';

@Component({
  selector: 'child-card-list',
  templateUrl: './child-card-list.component.html',
  styleUrls: ['./child-card-list.component.scss']
})
export class ChildCardListComponent implements OnInit {

  childList = [];
  selectedChild: any;
  isClosed: Boolean = false;
  constructor(
    private _childRemovalService: ChildRemovalService,
    private _personInfoService: PersonInfoService,
    private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    const statusobj = this._dataStoreService.getData('object');
    this.childList = this._childRemovalService.getChildList();
    this.childList.forEach(item => {
      if(item.removalHistory && item.removalHistory.length>0){
        item.removalHistory = item.removalHistory.sort((remove1, remove2)=> {return new Date(remove2.removaldate).getTime() - new Date(remove1.removaldate).getTime()});
      }
    });
    if (statusobj) {
      if(statusobj.da_status === 'Closed' || statusobj.da_status === 'Completed') {
          this.isClosed = true;
      } else {
          this.isClosed = false;
      }
    }
  }

  onChildChecked(event, child) {
    console.log(event, child);
    child.isRemoved = event.checked;
    this._personInfoService.setPersonId(child.personid);
    this._childRemovalService.removeChild(child, event.checked);

  }

  showChildRemovalDetails(child) {
    this._personInfoService.setPersonId(child.personid);
    child.isRemoved = true;
    this._childRemovalService.showChildRemovalInformation(child.intakeservreqchildremovalid);
  }

  showRemovalHistory(child) {
    console.log('remoavl history', child);
    this.selectedChild = child;
  }

  viewRemovalInfo(removalInfo) {
    console.log('view removal info', removalInfo);
    (<any>$('#ChildHistory')).modal('hide');
    this._childRemovalService.showChildRemovalHistoryInfo(this.selectedChild,removalInfo);
  }

}
