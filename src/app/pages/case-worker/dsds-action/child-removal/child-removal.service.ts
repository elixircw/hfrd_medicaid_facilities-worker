import { Injectable } from '@angular/core';
import { CommonHttpService, DataStoreService, AuthService, SessionStorageService, CommonDropdownsService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';
import { PersonDisabilityService } from '../../../shared-pages/person-disability/person-disability.service';
const UNSAFE = 0;
import * as _ from 'lodash';

@Injectable()
export class ChildRemovalService {

  intakeserviceid: string;
  daNumber: string;
  personList = [];
  childList = [];
  teamTypeKey: string;
  CHILD_CATEGORIES = ['CHILD', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC', 'AV'];
  removedChildren = [];
  public childRemoval$ = new Subject<any>();
  public childRemovalInfo$ = new Subject<any>();
  public removalConfig$ = new Subject<any>();
  childRemovalInfo = [];
  placementList = [];


  // dropdown list
  removalReason = []; sendTo = []; familyStructure = []; childRemoval = [];
  exitCaseReasons = []; reasonableEfforts = []; reasonableEffortsNotMade = [];
  disabilityTypes = []; disabilityConditions = []; removalEndReasons = [];
  constructor(private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _personDisabilityService: PersonDisabilityService,
    private _session: SessionStorageService,
    private _commonDDService: CommonDropdownsService) {
    this.teamTypeKey = this._authService.getAgencyName();
  }

  getPersonsList() {

    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: this.getRequestParam()
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );
  }

  getAssessments() {
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          where: this.getRequestParamAssessment(),
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.ListAssessment + '?filter'
      );
  }
  getAssessmentsInvolvedPersons() {
    return this._commonService
      .getArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          where: this.getRequestParam(),
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
      );
  }
  getAssessmentsRoutingInfo() {
    return this._commonService
      .getArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          where: this.getRequestParam(),
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.RoutingInfoList
      );
  }

  loadPersonList(persons) {
    this.personList = persons;
    this.loadPersonDisabilities();
  }

  loadPersonDisabilities() {
    this.personList.forEach(person => {
      person.hasDisability = null;
      this._personDisabilityService.getDisabilityList(person.personid).subscribe(response => {
        if (response && Array.isArray(response) && response.length) {
          person.personDisabilities = response;
          person.hasDisability = true;
        } else {
          person.personDisabilities = [];
          person.hasDisability = false;
        }

      });
    });
  }

  loadChildRemoval(info) {
    this.childRemovalInfo = info;
  }

  getCareGiverList() {
    // return this.personList;
    return this.personList.filter(person => {
      const houseHold = (person.ishousehold);
      let isChild = false;
      if (person.roles && person.roles.length) {
        person.roles.forEach(role => {
          const childCategory = this.CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
          if (childCategory) {
            isChild = true;
            return;
          }
        });
      }
      // return childFound;
      return houseHold && !isChild;
    });
  }

  getChildList() {
    console.log('process person list', this.personList);
    this.childList = this.personList.filter(child => {
      let childFound = false;
      if (child.roles && child.roles.length) {
        child.roles.forEach(role => {
          const childCategory = this.CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
          if (childCategory) {
            childFound = true;
            return;
          }
        });
      }
      // return childFound; // removing unsafe child validation for 11/3 demo
      // return childFound && (child.issafe === UNSAFE || this.isServiceCase()); //  D-11926
      return childFound ;
    });

    return this.childList;
  }

  getLegalGuardianList() {
    const lgList = this.personList.filter(item => {
      const roles = (item.roles && item.roles.length) ? item.roles : [];
      const lg = roles.some(ele => ele.intakeservicerequestpersontypekey === 'LG');
      if (lg) {
        return true;
      } else {
        return false;
      }
    });
    return lgList;
  }

  loadDropDownList() {
    const source = forkJoin([
      this._commonService.getArrayList(
        {
          method: 'get',
          order: 'description asc'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .GetRemovalReason + '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { agencycategorykey: 'CHDC', order: 'description asc' },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetSentTo +
        '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
            method: 'get',
            nolimit: true,
            where: { 'active_sw': 'Y', 'picklist_type_id': '28', 'delete_sw': 'N' }
        },
        'tb_picklist_values/getpicklist' + '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { referencetypeid: '53', teamtypekey: null, order: 'description asc' },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
        '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { referencetypeid: '68', teamtypekey: this.teamTypeKey, order: 'description asc' },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
        '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { referencetypeid: '69', teamtypekey: this.teamTypeKey, order: 'description asc', nolimit: true },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
        '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { referencetypeid: '70', teamtypekey: this.teamTypeKey },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
        '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { referencetypeid: '97', teamtypekey: this.teamTypeKey },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
        '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { referencetypeid: '70', teamtypekey: this.teamTypeKey },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
        '?filter'
      ),
      this._commonService.getArrayList(
        {
          where: { referencetypeid: '343', teamtypekey: this.teamTypeKey },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
        '?filter'
      )
    ])
      .subscribe(([removalReason, sendTo, familyStructure, childRemoval,
        exitCaseReasons, reasonableEfforts, reasonableEffortsNotMade, disabilityTypes, disabilityConditions, removalEndReasons]) => {
        this.removalReason = removalReason;
        // this.states = states;
        this.sendTo = sendTo;
        this.familyStructure = familyStructure;
        this.childRemoval = childRemoval;
        this.exitCaseReasons = exitCaseReasons;
        this.reasonableEfforts = reasonableEfforts;
        this.reasonableEffortsNotMade = reasonableEffortsNotMade;
        if (disabilityTypes && Array.isArray(disabilityTypes)) {
          this.disabilityTypes = disabilityTypes;
        }
        this.disabilityConditions = disabilityConditions;
        this.removalEndReasons = removalEndReasons;
        this.removalConfig$.next('loaded');


      });
  }

  getRemovedChildren() {
    return this.childList.filter(child => child.isRemoved);
  }

  getReviewChildrenForSupervisor() {
    const reviewInfo = this.childList.find(person => person.removalStatus === 'Review');
    return reviewInfo ? [reviewInfo] : [];
  }

  removeChild(removedChild, isRemoved) {
    this.childList.find(child => {
      if (child.personid === removedChild.personid) {
        child.isRemoved = isRemoved;
        return true;
      } else {
        return false;
      }
    });
    this.childRemoval$.next(removedChild);
  }

  getAddressPerson(personid: number) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest(
          {
            method: 'get',
            where: { personid: personid },
            page: 1, // this.paginationInfo.pageNumber,
            limit: 10// this.paginationInfo.pageSize,
          }),
        CommonUrlConfig.EndPoint.PERSON.ADDRESS.ListAddressUrl + '?filter'
      );
  }

  sendForApproval(childRemovalFormData) {
    return this._commonService
      .create(childRemovalFormData, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.SubmitChildRemoval);
  }
  approveChildRemoval(childRemovalId) {

    let data;
    if (this.isServiceCase()) {
      data = {
        'objectid': childRemovalId,
        'eventcode': 'CHRR',
        'status': 'Approved',
        'comments': 'Child Removal Approved',
        'notifymsg': 'Child Removal Approved',
        'routeddescription': 'Child Removal Approved',
         'servicecaseid': this.intakeserviceid
      };
    } else {
      data = {
        'objectid': childRemovalId,
        'eventcode': 'CHRR',
        'status': 'Approved',
        'comments': 'Child Removal Approved',
        'notifymsg': 'Child Removal Approved',
        'routeddescription': 'Child Removal Approved',
        intakeserviceid: this.intakeserviceid
      };
    }

    return this._commonService
      .create(data, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.approveOrReject);
  }

  rejectChildRemoval(childRemovalId) {
    const data = {
      'objectid': childRemovalId,
      'eventcode': 'CHRR',
      'status': 'Rejected',
      'comments': 'Child Removal Rejected',
      'notifymsg': 'Child Removal Rejected',
      'routeddescription': 'Child Removal Rejected'
    };
    return this._commonService
      .create(data, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.approveOrReject);
  }


  getChildRemoval(groupByPerson: number = 0) {
    const requestData = { ...this.getRequestParam(), ...{ isgroup: groupByPerson } };
    return this._commonService
      .getSingle(
        {
          where: requestData,
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .GetChildRemovalList + '?filter'
      );
  }

  getPlacementInfoList() {
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: { servicecaseid: this._commonDDService.getStoredCaseUuid() },
        }),
        'placement/getplacementbyservicecase?filter'
      );
  }

  getPersonsAndChildRemovalInfo() {
    return forkJoin([this.getPersonsList(),
    this.getChildRemoval(),
    this.getPlacementInfoList()])
      .map(([persons, childRemoval, placmentList]) => {
        let personList = [];
        let childRemovalInfo = [];
        if (persons && persons.data) {
          personList = persons.data;
        }
        if (childRemoval) {
          childRemovalInfo = childRemoval;
        }

        if (placmentList && placmentList.data && Array.isArray(placmentList.data)) {
          this.placementList = placmentList.data;
        }
        this.loadPersonList(personList);
        this.loadChildRemoval(childRemovalInfo);
        this.updatePersonListWithChildRemoval();



        return { persons: personList, childRemovalInfo: childRemovalInfo };
      });
  }

  updatePersonListWithChildRemoval() {
    if (this.childRemovalInfo && this.childRemovalInfo.length) {
      this.childRemovalInfo.forEach(removalInfo => {
        this.personList.forEach(person => {
          if (person.personid === removalInfo.personid) {
            // person.isRemoved = true;
            // if (person.servicecaseid) {
            //   person.removalStatus = 'Approved';
            // } else {
            //   person.removalStatus = 'Review';
            // }
            person.removalStatus = removalInfo.approvalstatus ? removalInfo.approvalstatus : 'Draft';
            // if record has end date then eligble for new removal

            person.intakeservreqchildremovalid = removalInfo.intakeservreqchildremovalid;
            if (removalInfo.exitdate && person.removalStatus === 'Approved') {
              person.removalStatus = null;
              person.intakeservreqchildremovalid = null;
              person.enableNewRemoval = true;
            }

            if (this.isServiceCase()) {
              if (!removalInfo.exitdate && person.removalStatus === 'Approved') {
                person.enableEndRemoval = true;
              } else if (removalInfo.exitdate && person.removalStatus === 'Review') {
                person.enableEndRemoval = true;
              } else {
                person.enableEndRemoval = false;
              }
            } else {
              person.enableEndRemoval = false;
              if (!removalInfo.exitdate && person.removalStatus === 'Approved') {
                person.viewOnly = true;
              }
            }
            person.removalInfo = removalInfo;
            person.placementList = this.placementList ? this.placementList.filter(placement => placement.personid === person.personid) : null;


            person.removalHistory = JSON.parse(JSON.stringify(this.childRemovalInfo.filter(removalHistory => removalHistory.personid === person.personid)));
            person.removalHistory = _.orderBy(person.removalHistory, ['removalid'], ['desc']);
            person.hasHistory = person.removalHistory.length ? true : false;
          }
        });
      });

    }
  }

  showChildRemovalInformation(childRemovalId) {
    this.childList.forEach(child => {
      if (child.intakeservreqchildremovalid === childRemovalId && ['Draft', 'Approved', 'Review', 'Rejected'].includes(child.removalStatus)) {
        child.isRemoved = true;
      } else {
        child.isRemoved = false;
      }
    });
    const removedChildren = this.childList.filter(child => child.intakeservreqchildremovalid === childRemovalId);
    this.childRemovalInfo$.next(removedChildren);
  }

  showChildRemovalHistoryInfo(selectedChild, removalInfo) {
    const child = selectedChild;
    child.removalInfo = removalInfo;
    if (removalInfo.exitdate) {
      child.enableEndRemoval = true;
    } else {
      child.enableEndRemoval = false;
    }
    child.viewOnly = true;
    this.childRemovalInfo$.next([JSON.parse(JSON.stringify(child))]);
  }

  serviceCaseCreatOrMerge(serviceCaseId, isNewCase) {
    return this._commonService
      .create(
        {
          servicecaseid: serviceCaseId,
          intakeserviceid: this.intakeserviceid,
          isnewcase: isNewCase
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .CreateCaseUrl
      );
  }

  serviceCaseCreateOrCheckIsExist() {
    return this._commonService
      .getSingle(
        {
          where: {
            intakeserviceid: this.intakeserviceid
          },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .ServiceCaseValidate + '?filter'
      );
    /*
      .subscribe(result => {
          if (result.isavailable === 1) {
              this.serviceCase = result.data;
              (<any>$('#ServiceCaseValidation')).modal('show');
              console.log(result);
          } else {
              this._alertService.success('Service case created successfully. Servicecase #' + result.data[0].servicecaseno);
          }
      }); */
  }

  getRequestParam() {

    this.intakeserviceid = this._commonDDService.getStoredCaseUuid();
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    let requestData;
    if (this.isServiceCase()) {
      requestData = {
        objectid: this.intakeserviceid,
        objecttypekey: 'servicecase'
      };

    } else {
      requestData = { intakeserviceid: this.intakeserviceid };
    }
    return requestData;
  }

  getRequestParamAssessment() {

    this.intakeserviceid = this._commonDDService.getStoredCaseUuid();
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    let requestData;
    if (this.isServiceCase()) {
      requestData = {
        objectid: this.intakeserviceid,
        objecttypekey: 'servicecase'
      };

    } else {
      requestData = { servicerequestid: this.intakeserviceid };
    }
    return requestData;
  }

  isServiceCase() {
    const isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    if (isServiceCase) {
      return true;
    } else {
      return false;
    }
  }



}
