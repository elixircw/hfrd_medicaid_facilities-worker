import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadChildAttachmentComponent } from './upload-child-attachment.component';

describe('UploadChildAttachmentComponent', () => {
  let component: UploadChildAttachmentComponent;
  let fixture: ComponentFixture<UploadChildAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadChildAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadChildAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
