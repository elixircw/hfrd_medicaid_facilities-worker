import { Injectable } from '@angular/core';
import { ChildRemovalService } from './child-removal.service';
import * as moment from 'moment';

@Injectable()
export class ChildRemovalMapperService {

  constructor(private _childRemovalService: ChildRemovalService) { }
  mapChildRemovalFormData(childRemovalData) {
    const formatedData = childRemovalData;
    formatedData.intakeserviceid = this._childRemovalService.isServiceCase() ? null : this._childRemovalService.intakeserviceid;
    formatedData.servicecaseid = this._childRemovalService.isServiceCase() ? this._childRemovalService.intakeserviceid : null;
    const time = moment(formatedData.removaltime).format('YYYY-MM-DD HH:mm:ss');
    formatedData.removaltime = moment(time).isValid() ? time : null;
    if (formatedData.exittime && formatedData.exittime !== 'Invalid date') {
      const exittime = moment(formatedData.exittime).isValid() ? moment(formatedData.exittime).format('HH:mm:ss') :  formatedData.exittime;
      console.log('et', exittime);
      if (formatedData.exitdate) {
        const exitDateOnly = formatedData.exitdate.substring(0, 10);
        const exitdattime = exitDateOnly + 'T' + exittime;
        console.log('edt', exitdattime);
        formatedData.exitdate = exitdattime;

      }
     

    }
    // formatedData.intakeservicerequestactorid = this._childRemovalService.getRemovedChildren()[0].intakeservicerequestactorid;
    // formatedData.intakeservicerequestactorid = this.removedChildren[0].roles[0].intakeservicerequestactorid;
    formatedData.rmvdfrmisractorid = null;
    formatedData.mothername = null;
    formatedData.removalzip = '65559999';
    if (formatedData.removalreason) {
      formatedData.removalreason = formatedData.removalreason.map(reason => {
        return {
          removalreasontypeid: reason,
          removalreasontypekey: reason
        };
      });
    }

    if (formatedData.caregiverreason) {
      formatedData.caregiverreason = formatedData.caregiverreason.map(reason => {
        return {
          reasontypekey: reason
        };
      });
    }

    if (formatedData.reasonableefforts) {
      formatedData.reasonableefforts = formatedData.reasonableefforts.map(reason => {
        return {
          reasontypekey: reason
        };
      });
    }

    if (formatedData.exitreason) {
      formatedData.exitreason = formatedData.exitreason.map(reason => {
        return {
          reasontypekey: reason
        };
      });
    }
    if (formatedData.notmakingefforts) {
      formatedData.notmakingefforts = formatedData.notmakingefforts.map(reason => {
        return {
          reasontypekey: reason
        };
      });
    }
    formatedData.isverifiedcaregiver1add = formatedData.isverifiedcaregiver1add ? 1 : 0;
    formatedData.isverifiedcaregiver2add = formatedData.isverifiedcaregiver2add ? 1 : 0;
    console.log('formated data', formatedData);

    return formatedData;

  }

  unMapChildRemovalInfo(childRemovalInfo) {
    const formatedData = childRemovalInfo;
    formatedData.isverifiedcaregiver1add = formatedData.isverifiedcaregiver1add ? true : false;
    formatedData.isverifiedcaregiver2add = formatedData.isverifiedcaregiver2add ? true : false;
    if (formatedData.removaltime && formatedData.removaltime !== 'Invalid date') {
      formatedData.removaltime = moment(formatedData.removaltime).isValid() ? moment.utc(formatedData.removaltime) :  formatedData.removaltime;
    }
    if (formatedData.exitdate && formatedData.exitdate !== 'Invalid date') {
      formatedData.exittime = moment(formatedData.exitdate).isValid() ? moment.utc(formatedData.exitdate).format('h:mm a') :  formatedData.exitdate;
    }
    if (formatedData.removalreason) {
      formatedData.removalreason = formatedData.removalreason.map(reason => {
        return reason.hasOwnProperty('removalreasontypekey') ? reason.removalreasontypekey : reason;
      });
    }
    if (formatedData.caregiverreason) {
      formatedData.caregiverreason = formatedData.caregiverreason.map(reason => {
        return reason.hasOwnProperty('removalreasontypekey') ? reason.removalreasontypekey : reason;
      });
    }

    if (formatedData.reasonableefforts) {
      formatedData.reasonableefforts = formatedData.reasonableefforts.map(reason => {
        return reason.hasOwnProperty('removalreasontypekey') ? reason.removalreasontypekey : reason;
      });
    }
    if (formatedData.notmakingefforts) {
      formatedData.notmakingefforts = formatedData.notmakingefforts.map(reason => {
        return reason.hasOwnProperty('removalreasontypekey') ? reason.removalreasontypekey : reason;
      });
    }
    if (formatedData.exitreason) {
      formatedData.exitreason = formatedData.exitreason.map(reason => {
        return reason.hasOwnProperty('removalreasontypekey') ? reason.removalreasontypekey : reason;
      });
    }
    console.log('unmapped data', formatedData);
    return formatedData;
  }

}
