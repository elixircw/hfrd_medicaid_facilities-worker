import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChildAttachmentComponent } from './edit-child-attachment.component';

describe('EditChildAttachmentComponent', () => {
  let component: EditChildAttachmentComponent;
  let fixture: ComponentFixture<EditChildAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChildAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChildAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
