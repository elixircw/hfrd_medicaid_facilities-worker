import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChildRemovalRoutingModule } from './child-removal-routing.module';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ChildRemovalWrapperComponent } from './child-removal-wrapper/child-removal-wrapper.component';
import { ChildRemovalComponent } from './child-removal.component';
import { NgxMaskModule } from 'ngx-mask';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { QuillModule } from 'ngx-quill';
import { ChildRemovalFormComponent } from './child-removal-form/child-removal-form.component';
import { ChildCardListComponent } from './child-card-list/child-card-list.component';
import { ChildRemovalService } from './child-removal.service';
import { ChildRemovalDetailComponent } from './child-removal-detail/child-removal-detail.component';
import { ChildRemovalResolverService } from './child-removal-resolver.service';
import { ChildRemovalMapperService } from './child-removal-mapper.service';
import { PersonDisabilityModule } from '../../../shared-pages/person-disability/person-disability.module';
import { PersonDisabilityService } from '../../../shared-pages/person-disability/person-disability.service';
import { DsdsActionModule } from '../dsds-action.module';
import { PersonInfoService } from '../../../shared-pages/person-info/person-info.service';
import { NavigationUtils } from '../../../_utils/navigation-utils.service';
import { PersonHealthInfoModule } from '../../../shared-pages/person-info/person-health/person-health-info.module';

@NgModule({
  imports: [
    CommonModule,
    ChildRemovalRoutingModule,
    FormMaterialModule,
    A2Edatetimepicker,
    NgxMaskModule.forRoot(),
    QuillModule,
    DsdsActionModule,
    PersonHealthInfoModule

  ],
  declarations: [ChildRemovalWrapperComponent, ChildRemovalComponent, ChildRemovalFormComponent, ChildCardListComponent, ChildRemovalDetailComponent],
  providers: [ChildRemovalService, ChildRemovalResolverService, ChildRemovalMapperService, PersonDisabilityService, PersonInfoService, NavigationUtils]
})
export class ChildRemovalModule { }
