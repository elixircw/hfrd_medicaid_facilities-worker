import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChildRemovalService } from '../child-removal.service';

@Component({
  selector: 'child-removal-wrapper',
  templateUrl: './child-removal-wrapper.component.html',
  styleUrls: ['./child-removal-wrapper.component.scss']
})
export class ChildRemovalWrapperComponent implements OnInit {

  childRemovalShow = false;
  constructor(private route: ActivatedRoute, private _service: ChildRemovalService) {
    this.route.data.subscribe(res => {
      console.log('response', res);
      if (res && res.config) {
        this._service.loadDropDownList();
      }

    });
  }

  ngOnInit() {
  }

}
