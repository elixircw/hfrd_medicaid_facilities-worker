import { ReportSummary } from './../../../providers/_entities/provider.data.model';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportSummaryComponent } from './report-summary.component';
import { ReportSummaryRoutingModule } from './report-summary-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
@NgModule({
  imports: [
    CommonModule,
    ReportSummaryRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
  ],
  declarations: [
    ReportSummaryComponent
  ],
  providers: []
})
export class ReportSummaryModule { }
