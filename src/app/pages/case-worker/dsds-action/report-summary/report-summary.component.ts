import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ControlUtils } from '../../../../@core/common/control-utils';
import { CheckboxModel, DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, GenericService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { CaseWorkReportSummary, Illegalactivity, ReportSummary, PersonAddress, DSDSActionSummary } from '../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { SpeechSynthesizerService } from '../../../../shared/modules/web-speech/shared/services/speech-synthesizer.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import * as moment from 'moment';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { filter } from 'rxjs/operators';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'report-summary',
    templateUrl: './report-summary.component.html',
    styleUrls: ['./report-summary.component.scss']
})
export class ReportSummaryComponent implements OnInit, OnDestroy {
    disableNo: boolean;
    disableAddress: boolean;
    dangerousself: boolean;
    dangerousaddress: boolean;
    id: string;
    isCW: boolean;
    daNumber: string;
    caseclosuresummaryid: string;
    possibleIllegalActivityDropdown: FormArray;
    reportSummaryForm: FormGroup;
    ARCaseSummaryForm: FormGroup;
    closureSubTypeItems: DropdownModel[] = [];
    illegalActivityDd = false;
    countyList = [];
    significantEventDd = false;
    notPlacedChildList: any;
    significantEventDropdownItems$: Observable<DropdownModel[]>;
    reasonDropDown: any[];
    statusDropDown: any[];
    statusTempDropDown: any[];
    serviceDropDown: any[];
    sourceDropdownItems$: Observable<DropdownModel[]>;
    possibleCheckboxItems: CheckboxModel[] = [];
    reportSummary: ReportSummary;
    LegalGuardian: string;
    reportSummaryDangerAddress: PersonAddress;
    private selectedIllegalActivities: string[] = [];
    private speaking = false;
    private paused = false;
    private voiceNotStarted = true;
    missinglegalRole: any[];
    involvedPersons$: Observable<any[]>;
    countyid: string;
    reporterFirstName: string;
    reporterLastName: string;
    reporterPhone: string;
    reporterPhoneExt: string;
    reporteEmail: string;
    reporterUnknown: string;
    isLGPresent: boolean;
    clientrefrdservicesdisabled: boolean;
    statuslistdisabled: boolean;
    involevedUnkPerson: any[] = [];
    involvedChildren: any[] = [];
    involvedOthers: any[] = [];
    involvedPersons: any[] = [];
    selectedParticipant: any[];
    personsInvolved: any[] = [];
    selectedChild: any[];
    isUnkPresent: boolean;
    userRole: AppUser;
    isAs: boolean;
    isRequestService: boolean;
    speechRecogninitionOn: boolean;
    speechData: string;
    currentLanguage: string;
    currentSpeechRecInput: string;
    notification: string;
    recognizing = false;
    ARAssessmentClosureDate: string;
    AssessmentParticipant: any[];
    savedParticipants: any[];
    dsdsActionsSummary = new DSDSActionSummary();
    maltreatmentInformation: any[];
    isInfantnotReported: boolean;
    needToSafeInfants = [];
    infants: any[];
    communicationList: any;
    isServiceCase: any;
    hohPersonName: string;

    narrativePresent = false;
    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private _reportSummaryService: GenericService<ReportSummary>,
        private _dataStoreService: DataStoreService,
        private _speechSynthesizer: SpeechSynthesizerService,
        private speechRecognizer: SpeechRecognizerService,
        private _speechRecognitionService: SpeechRecognitionService,
        private storage: SessionStorageService,
    ) {

        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.speechRecogninitionOn = false;
        this.speechData = '';
    }
    ngOnInit() {

        this._speechSynthesizer.initSynthesis();
        this.userRole = this._authService.getCurrentUser();
        this.speechRecognizer.initialize(this.currentLanguage);
        this.selectedParticipant = [];
        this.selectedChild = [];
        this.initARCaseSummaryForm();
        this.loadReasonDropDown();
        this.loadStatusDropDown();
        this.loadServiceDropDown();
        this.getInvolvedPerson();
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');

        if(this.isServiceCase){
            this.getChildRemoval();
        }
        // this.getCommunicationList();
        if (this.userRole.user.userprofile.teamtypekey === 'AS') {
            this.isAs = true;
        }
        this.possibleIllegalActivityDropdown = this.formBuilder.array([this.formBuilder.control(false)]);
        if (this.id !== '0') {
            this.listReportSummary(this.id);
        }
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.PossibleCheckList + '?filter'
            ),
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.Source + '?filter'
            )
        ])
            .map(result => {
                return {
                    intakeServiceRequestIllegalActivityTypes: result[0].map(
                        res =>
                            new CheckboxModel({
                                text: res.typedescription,
                                value: res.intakeservicerequestillegalactivitytypekey,
                                isSelected: false
                            })
                    ),
                    intakeServiceRequestInputTypes: result[1].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeservreqinputtypeid
                            })
                    )
                };
            })
            .share();
        this.sourceDropdownItems$ = source.pluck('intakeServiceRequestInputTypes');
        // this._dataStoreService.currentStore.subscribe(store => {
        const store = this._dataStoreService.getCurrentStore();
        if (store['da_status'] === 'Closed') {
            ControlUtils.disableElements($('#Involved-Persons').children());
        }
        if (store['countyid']) {
            this.countyid = store['countyid'];
            console.log(this.countyid);
        }
        if (store['dsdsActionsSummary']) {
            this.dsdsActionsSummary = store['dsdsActionsSummary'];

        }
        

        
        this.getCountyList();
        this.getARSummaryCase();
//        console.log("REPORT SUMMARY",this.reportSummary);
    }


    getFindingList() {
        // this.initFindingsForm();
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    where: {
                        investigationid: this.dsdsActionsSummary.da_investigationid
                    },
                    method: 'get'
                }),
                'Investigationallegations/getmaltreatmentfinding?filter'
            )
            .subscribe((iFindings) => {
                if (iFindings && Array.isArray(iFindings) && iFindings.length > 0) {
                    this.checkForInfantAndToddler(iFindings);
                }
            });
    }

    private checkForInfantAndToddler(iFindings) {

        const indicatedFindings = iFindings.filter(item => {
            if (item.findings && item.findings.length) {
                const indicatedData = item.findings.find(data => data.investigationfindingtypekey === 'ID');
                if (indicatedData) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        });
        if (indicatedFindings && Array.isArray(indicatedFindings) && indicatedFindings.length > 0) {
            if (this.infants.length > 0) {
                this.infants.forEach((infant, index) => {
                    const malTreatedPerson = indicatedFindings.find(ifData => ifData.personid === infant.personid);
                    if (!malTreatedPerson) {
                        this.needToSafeInfants.push(infant);
                    }

                });
                if (this.needToSafeInfants.length > 0) {
                    this.isInfantnotReported = true;
                    (<any>$('#legal-guardian-role')).modal('show');
                } else {
                    this.isInfantnotReported = true;
                }
            } else {
                this.isInfantnotReported = false;
            }
        }
    }

    private getAge(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }

    initARCaseSummaryForm() {
        this.ARCaseSummaryForm = this.formBuilder.group({
            reason: [null],
            caseclosuresummaryid: [null],
            intakeserviceid: [null],
            referralreason: [null],
            riskissues: [null],
            recommendation: [null],
            interventionissues: [null],
            statusList: this.formBuilder.array([]),
            clientrefrdservices: [null],
            closuretypekey: [null],
            closuresubtypekey: [null],
            notes: [null],
            participants: [null]
        });

    }
    getCountyList() {
        this._commonHttpService.getArrayList({
            where: {
                activeflag: '1',
                state: 'MD'
            },
            order: 'countyname asc',
            method: 'get',
            nolimit: true
        }, 'admin/county?filter').subscribe((item) => {
            if (item && item.length) {
                this.countyList = item.filter((res) => res.countyid === this.countyid);
            }
        });
    }

    onChangeIllegalActivityMain($event) {
        this.illegalActivityDd = $event.target.checked;
        if (this.illegalActivityDd) {
            this.buildCheckBox();
        } else {
            this.selectedIllegalActivities = [];
        }
    }
    changeSignificantEvent($event) {
        this.significantEventDd = $event.target.checked;
        if (this.significantEventDd) {
            this.reportSummaryForm.patchValue({
                significantkey: this.reportSummary.servicerequestincidenttypekey ? this.reportSummary.servicerequestincidenttypekey : ''
            });
        } else {
            this.reportSummaryForm.value.significantkey = null;
        }
    }
    onChangeIllegalActivity($event) {
        if ($event.target.checked) {
            this.selectedIllegalActivities.push($event.target.value);
        } else {
            this.selectedIllegalActivities.splice($event.target.value, 1);
        }
    }
    saveReportSummary() {
        const userId = this._authService.getCurrentUser().userId;
        const caseWorkReportSummary = new CaseWorkReportSummary();
        caseWorkReportSummary.servicerequestincidenttypekey = this.reportSummaryForm.value.significantkey ? this.reportSummaryForm.value.significantkey : null;
        const addedItems = this.selectedIllegalActivities.map(id => {
            const illegalactivity = this.reportSummary.intakeservicerequestillegalactivity.filter((item, index, array) => {
                return item.intakeservicerequestillegalactivitytypekey === id;
            });
            if (!illegalactivity.length) {
                return new Illegalactivity({
                    activeflag: 1,
                    effectivedate: new Date(),
                    insertedby: userId,
                    intakeservicerequestillegalactivitytypekey: id,
                    intakeservicerequestid: this.reportSummary.intakeserviceid,
                    updatedby: userId
                });
            } else {
                return new Illegalactivity({
                    activeflag: 1,
                    effectivedate: new Date(),
                    insertedby: userId,
                    intakeservicerequestillegalactivitytypekey: id,
                    intakeservicerequestid: this.reportSummary.intakeserviceid,
                    intakeservicerequestillegalactivityid: this.reportSummary.intakeservicerequestillegalactivity[0].intakeservicerequestillegalactivityid,
                    updatedby: userId
                });
            }
        });
        caseWorkReportSummary.intakeservicerequestillegalactivity = [];
        caseWorkReportSummary.intakeservicerequestillegalactivity.push(...addedItems);
        caseWorkReportSummary.suspiciousdeath = this.reportSummaryForm.value.suspiciousdeath;
        caseWorkReportSummary.missingpersons = this.reportSummaryForm.value.missingpersons;
        caseWorkReportSummary.effectivedate = new Date();
        this._commonHttpService.update(this.reportSummary.intakeserviceid, caseWorkReportSummary, CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.UpdateReportSummary).subscribe(
            responce => {
                this._alertService.success('Report summary updated successfully');
                this.listReportSummary(this.id);
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
        
    }

    readNarrative() {
        if (this.voiceNotStarted) {
            const narrativeText = $('#reportSummaryNarrative').text();
            this._speechSynthesizer.speak(narrativeText, 'en-US');
            this.voiceNotStarted = false;
            this.paused = false;
            this.speaking = true;
        } else if (this.speaking) {
            this._speechSynthesizer.pause();
            this.paused = true;
            this.speaking = false;
        } else if (this.paused) {
            this._speechSynthesizer.resume();
            this.paused = false;
            this.speaking = true;
        }
    }

    private formInitilizer(reportSummary: ReportSummary) {
        this.reportSummaryForm = this.formBuilder.group({
            suspiciousdeath: [reportSummary.suspiciousdeath ? reportSummary.suspiciousdeath : false],
            missingpersons: [reportSummary.missingpersons ? reportSummary.missingpersons : false],
            significantkey: [reportSummary.servicerequestincidenttypekey ? reportSummary.servicerequestincidenttypekey : '']
        });
    }

    // private getCommunicationList() {
    //     this.communicationList = {};
    //     this._commonHttpService
    //     .getArrayList(
    //         {},
    //         NewUrlConfig.EndPoint.Intake.IntakeServiceRequestInputTypeUrl
    //     )
    //     .subscribe(data => {
    //         if (data && data.length) {
    //             data.forEach(res => {
    //                 this.communicationList[res.intakeservreqinputtypeid] = res.description;
    //             });
    //         }
    //         // new DropdownModel({
    //         //     text: res.description,
    //         //     value: res.intakeservreqinputtypeid
    //         // })communicationList
    //     });
    // }

//     getCommunicationType(Summary) {

//         console.log(this.communicationList);
// // tslint:disable-next-line: max-line-length
//         const key = (Summary && Summary.intakeservicerequestinputtype && Summary.intakeservicerequestinputtype.intakeservreqinputtypekey )
// ? Summary.intakeservicerequestinputtype.intakeservreqinputtypekey : '';
//       if (key &&  this.communicationList && this.communicationList[key]) {
//         return this.communicationList[key];
//       } else {
//           return key;
//       }
//     }

    isPersonAddressDangerous(person) {
        const address = (person &&  person.actor && person.actor.Person && person.actor.Person.personaddress) ? person.actor.Person.personaddress : null ;
        if (address && address.length) {
            return (
                address.filter(res => {
                    return res.danger === true;
                }).length !== 0
            );
        } else {
            return false;
        }
    }
    
    fixNarrativeHistoryClearanceText(){
        let n: string = this.reportSummary.narrative;
        if(n){
            n = n.replace(/(\\n)/g, '<br>');
            n = n.replace(/(\\r)/g, '');
            this.reportSummary.narrative = n;
        }
    }
    private listReportSummary(id: string) {
        let caseid = '';
        const isServicecase = this._dataStoreService.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        const cpscaseid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CPS_CASE_ID);
        caseid = (isServicecase) ? cpscaseid : this.id;
        this._reportSummaryService.getSingle(new PaginationRequest({}), CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.ReportSummary + caseid)
        .subscribe(result => {
            this.reportSummary = result;
            this._dataStoreService.setData('reportSummaryData',this.reportSummary);
            // console.log("REPORT SUMMARY",this.reportSummary);
            // console.log("MYSTUFF", this.reportSummary.intakeservicerequesttype.description);
            this.isRequestService = this.reportSummary.intakeservicerequesttype.description === 'Request for services' ? true : false;
            
            this._dataStoreService.setData('CurrentSupervisor',
                (result && result.userprofile) ?
                    (result.userprofile.securityusersid ? result.userprofile.securityusersid : '') : '');
            if (this.reportSummary && this.reportSummary.narrative && this.reportSummary.narrative) {
                this.reportSummary.narrative = this.reportSummary.narrative.replace('<a', '<a target="_blank"').replace(/'+'/g, '\'');
                this.narrativePresent = true;
                this.fixNarrativeHistoryClearanceText();
                
            }
            console.log('Involved Actors', this.reportSummary.intakeservicerequestactor);
            const reportSummaryDanger = this.reportSummary.intakeservicerequestactor.filter(item => {
                console.log('Involved Person', item.actor);
                if (item.actor) {
                    if (item.actor.Person) {
                        return item.actor.Person.dangerlevel === 1;
                    }
                }
            });
            if (reportSummaryDanger.length !== 0) {
                this.disableNo = true;
            } else {
                this.disableNo = false;
            }
            const reportSummaryDangerAddress = this.reportSummary.intakeservicerequestactor.filter(item => {
                if (item.actor) {
                    return (
                        item.actor.Person.personaddress.filter(res => {
                            return res.danger === true;
                        }).length !== 0
                    );
                }
            });
            if (reportSummaryDangerAddress.length !== 0) {
                this.disableAddress = true;
            } else {
                this.disableAddress = false;
            }
            if (result.servicerequestincidenttypekey) {
                this.significantEventDd = true;
            }
            if (result.intakeservicerequestillegalactivity.length) {
                this.illegalActivityDd = true;
            }
            // this.formInitilizer(result);
            this.possibleIllegalActivityDropdown = this.buildCheckBox();
         
            const reportSummaryHOH = this.reportSummary.intakeservicerequestactor.filter(item => {
            return item.isheadofhousehold === true;
        });
        if (reportSummaryHOH.length !== 0) {
            this.hohPersonName = (reportSummaryHOH[0].actor && reportSummaryHOH[0].actor.Person.firstname ? reportSummaryHOH[0].actor.Person.firstname: '' )
            + ' ' + (reportSummaryHOH[0].actor && reportSummaryHOH[0].actor.Person.middlename ? reportSummaryHOH[0].actor.Person.middlename : '')
            + ' ' + (reportSummaryHOH[0].actor && reportSummaryHOH[0].actor.Person.lastname ? reportSummaryHOH[0].actor.Person.lastname : '')
            + ' ' + (reportSummaryHOH[0].actor && reportSummaryHOH[0].actor.Person.suffix ? reportSummaryHOH[0].actor.Person.suffix : '');
        } else {
            this.hohPersonName = '';
        }
        });
        if (this._authService.isCW()) {
            this.isCW = true;
            this.legalGuardianCheck();
            this.getInvolvedUnkPerson();
        }
    }

    getChildRemoval() {
        this._commonHttpService
          .getSingle(
            {
              where: { objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID), 'objecttypekey': 'servicecase' , isgroup: 1},
              method: 'get'
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
              .GetChildRemovalList + '?filter'
          ).subscribe(data => {
            if (data && data.length) {
                const approvedRemoval = data.filter(item => {
                    if (item.childremoval && item.childremoval.length) {
                      const removal = item.childremoval[0];
                      return (removal.approvalstatus === 'Approved') ? true : false;
                    } else {
                      return false;
                    }
                  });
            this.placementCheck(approvedRemoval);
            }
          });
    }

    placementCheck(removedChild) {
        forkJoin([this.getPlacementInfoList(1,10), this.getPermanencyPlanList()]).subscribe(([placementData, plans]) => {
            console.log('placements', placementData );
            console.log('plans',plans );
            const placmentArray = [];
            removedChild.forEach(child => {
  
              let cjamsPid = child.cjamspid;
              let personName = child.personname;
              let isActivePlacement = null;
  
              if(child.childremoval && child.childremoval.length) {
                  child.childremoval.forEach(rem => {
                      if(rem.exitdate) {
                          isActivePlacement = true;
                      } else {
                          isActivePlacement = null;
                          if(placementData && placementData.data && placementData.data.length) {
                              const placementList = placementData.data;
                              placementList.forEach(placement => {
                                  const placements = placement.placements;
                                  placements.forEach(plmnt => {
                                      if( ( (child.cjamspid).toString() ===  placement.cjamspid )
                                          && (plmnt.routingstatus === "Approved" || plmnt.routingstatus === "Review")
                                          && !plmnt.enddate) {
                                          isActivePlacement = true;
                                      } else {
                                         // const plan = plans.forEach(item => item.personid === child.personid);
                                          console.log('child plan', plans);
                                          const permPlans = JSON.parse(JSON.stringify(plans));
                                          if (Array.isArray(permPlans)) {
                                            const childPlan = permPlans.find(item => item.personid === child.personid);
                                            if (childPlan && Array.isArray(childPlan.permanencyplans)) {
                                                const activePP = childPlan.permanencyplans.find(item => item.status === 'Approved');
                                                if (activePP) {
                                                    isActivePlacement = true;
                                                }
                                            }

                                          }
                                          

                                      }
                                  });
                              });
                          }
                      }
                  });
              }
              let childPlacement = { 'cjamsPid': cjamsPid, 'personName': personName, 'isActivePlacement': isActivePlacement};
               placmentArray.push(childPlacement);
  
            });
            this._dataStoreService.setData('placment_check', placmentArray);
            this.notPlacedChildList = placmentArray.filter(placment => !placment.isActivePlacement);
            if(this.notPlacedChildList && this.notPlacedChildList.length) {
              (<any>$('#legal-guardian-role')).modal('show');
            }
        
        });
    
    }

    getPlacementInfoList(pageNumber, limit) {
        return this._commonHttpService
          .getPagedArrayList(
            new PaginationRequest({
              page: pageNumber,
              limit: limit,
              method: 'get',
              where: { servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
            }),
            'placement/getplacementbyservicecase?filter'
            // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          );
    }

    private getInvolvedPerson() {
        // this._involvedPersonService.getInvolvedPerson(1,10).subscribe(response => {
        //   console.log('clientList',response);
        // });
        let inputRequest;
        if (this._dataStoreService.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE)) {
            inputRequest = {
              objectid: this.id,
              objecttypekey: 'servicecase'
            };
          } else {
            inputRequest = {
              intakeserviceid: this.id
            };
          }
        Observable.forkJoin([
          this._commonHttpService
            .getPagedArrayList(
              {
                where: inputRequest,
                page: 1,
                limit: 20,
                method: 'get'
              },
              CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListCWUrl + '?filter'
            )]).subscribe((result) => {
            if (result[0].data) {
               
                this.personsInvolved = result[0].data;
                console.log(this.personsInvolved);
                const dangerAddress  = this.personsInvolved.filter( person => person.dangeraddress === true || person.dangeraddress === 1 );
                const dangerSelf  = this.personsInvolved.filter( person => person.dangerous && person.dangerous.length &&  ( person.dangerous[0].isdangertoworker === 1) );
                this.dangerousself = dangerSelf && dangerSelf.length ? true : false;
                this.dangerousaddress = dangerAddress && dangerAddress.length ? true : false;
                
            }
          });
      }

    legalGuardianCheck() {
        this.isLGPresent = false;
        this.missinglegalRole = [];
        this.involvedPersons$ = this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: this.getRequestParam()
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            )
            .share()
            .pluck('data');
        this.involvedPersons$.subscribe((items) => {
            if (items) {
                this.getInvolvedChildrenAndOthers(items);
                const person = items.filter((item) => {
                    // const roles = (Array.isArray(item.roles)) ? item.roles : [];
                    // roles.forEach(element => {
                    //     if (element.intakeservicerequestpersontypekey === 'LG') {
                    //         this.missinglegalRole.push(item);
                    //     }
                    // });

                    if (item.isheadofhousehold === true) {
                        this.missinglegalRole.push(item);
                    }

                });
            }
            const store = this._dataStoreService.getCurrentStore();
            if (this.missinglegalRole && this.missinglegalRole.length > 0) {
            } else if(store['da_status'] !== 'Closed' && store['da_status'] !== 'Completed') {
                this.isLGPresent = true;
                (<any>$('#legal-guardian-role')).modal('show');
            }
            this.infants = items.filter(person => {
                const childAge = this.getAge(person.dob);
                if (childAge <= 3) {
                    return true;
                } else {
                    return false;
                }
            });
            // this.getFindingList();

        });
    }
    ifChildExist() {
        if(this.involvedPersons && this.involvedPersons.length) {
            let childCount = 0;
            this.involvedPersons.forEach(person => {
              const isChildExist = this.checkIfChild(person);
              if(isChildExist) {
                childCount = childCount + 1;
              }
           });
            return childCount ? true : false;
        } else {
            return true;
        }
    }
    checkIfChild(child) {
        if(child && child.roles && child.roles.length) {
          const isChild = child.roles.find( role => role.intakeservicerequestpersontypekey === 'AV' || role.intakeservicerequestpersontypekey === 'CHILD');
          return isChild ? true : false;
        } else {
            return false;
        }
    }

    getRoleDescription(roles) {
         if(roles && roles.length) {
            const roleArray = roles.filter(  role => role.intakeservicerequestpersontypekey === 'AV' || role.intakeservicerequestpersontypekey === 'CHILD');
            let roleDesc = '';
            if(roleArray && roleArray.length) {
                roleArray.forEach((role, index) => {
                    roleDesc = roleDesc + role.typedescription + ( (index !== (roleArray.length-1) )? ', ' : '' );
                });
            }
            return roleDesc  ? roleDesc : null;

         } else {
             return null;
         }
    }

    getInvolvedUnkPerson() {
        this.isUnkPresent = false;
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeserviceid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .UnkPersonList + '?filter'
            )
            .subscribe(data => {
                this.involevedUnkPerson = data;
                if (this.involevedUnkPerson) {
                    const unkPersonList = this.involevedUnkPerson.filter(item => item.isNew);
                    if (unkPersonList && unkPersonList.length > 0) {
                        this.isUnkPresent = true;
                        (<any>$('#legal-guardian-role')).modal('show');
                    }
                }
            });
    }

    getRequestParam() {
        let inputRequest: Object;
            if (this._dataStoreService.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE)) {
              inputRequest = {
                objectid: this.id,
                objecttypekey: 'servicecase'
              };
            } else {
              inputRequest = {
                intakeservreqid: this.id
              };
            }
        return inputRequest;
      }


    private buildCheckBox(): FormArray {
        let selectedIllActs: string[] = [];
        if (this.reportSummary.intakeservicerequestillegalactivity && this.reportSummary.intakeservicerequestillegalactivity.length) {
            selectedIllActs = this.reportSummary.intakeservicerequestillegalactivity.map(item => {
                return item.intakeservicerequestillegalactivitytypekey;
            });
        }
        this.selectedIllegalActivities = selectedIllActs;
        return ControlUtils.buildCheckBoxGroup(this.formBuilder, this.possibleCheckboxItems, selectedIllActs);
    }


    getInvolvedChildrenAndOthers(invlovedPersonsList) {
        invlovedPersonsList.map((item, $index) => {
            if (
                item.rolename !== 'AV' &&
                item.rolename !== 'AM' &&
                item.rolename !== 'CHILD' &&
                item.rolename !== 'RC' &&
                item.rolename !== 'BIOCHILD' &&
                item.rolename !== 'NVC' &&
                item.rolename !== 'OTHERCHILD' &&
                item.rolename !== 'PAC'
                // (item.roles.length &&
                //     item.roles.filter(
                //         itm =>
                //             itm.rolename !== 'AV' &&
                //             itm.rolename !== 'AM' &&
                //             itm.rolename !== 'CHILD' &&
                //             itm.rolename !== 'RC' &&
                //             itm.rolename !== 'BIOCHILD' &&
                //             itm.rolename !== 'NVC' &&
                //             itm.rolename !== 'OTHERCHILD' &&
                //             itm.rolename !== 'PAC'
                //     ))
            ) {
                this.involvedOthers.push(item);
                this.involvedPersons.push(item);
            } else {
                this.involvedChildren.push(item);
                this.involvedPersons.push(item);
            }

            if (item.rolename === 'LG') {
                this.LegalGuardian = (this.LegalGuardian ? this.LegalGuardian : '') + ($index !== 0 ? ', ' : '') + item.firstname + ' ' + item.lastname;
            }
            // });
        });
    }

    loadReasonDropDown() {

        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 101, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            ).subscribe((data) => {
                this.reasonDropDown = data;
                console.log(data);
            });
    }

    loadStatusDropDown() {

        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 102, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            ).subscribe((data) => {

                this.closureSubTypeItems = data.map(
                    res =>
                        new DropdownModel({
                            text: res.value_text,
                            value: res.ref_key
                        })
                );

                const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
                this.closureSubTypeItems.map(c => {
                    status.push(new FormControl(false));
                });
                this.statusDropDown = data;
            });
    }

    unchecksubtypes() {
        const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
        this.closureSubTypeItems.map(function (c, index) {
            status.removeAt(0);
        });
        this.closureSubTypeItems.map(function (c, index) {
            status.push(new FormControl(false));
        });
    }

    loadServiceDropDown() {

        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 103, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            ).subscribe((data) => {
                this.serviceDropDown = data;
                console.log(data);
            });

    }

    getRiskDropDown(array) {
        const riskDropDown = [];
        if (array && array.length) {
            array.forEach(data => {
                const obj = data;
                riskDropDown.push(obj);
            });
        }
        return riskDropDown ? riskDropDown : [];
    }


    setRiskDropDown(array) {
        const riskDropDown = [];
        if (array && array.length) {
            const arryriskdropdown = array.split(',');
            arryriskdropdown.forEach(data => {
                riskDropDown.push(data);
            });
        }
        return riskDropDown ? riskDropDown : [];
    }

    chooseAssessmentParticipant(event, child, isChild) {
        const participantObj = {
            intakeservicerequestactorid: child.intakeservicerequestactorid,
            ischild: isChild
        };
        if (this.AssessmentParticipant && this.AssessmentParticipant.length) {
            // AssessmentParticipant Array Null Check
        } else {
            this.AssessmentParticipant = [];
        }
        if (event.checked) {
            this.AssessmentParticipant.push(participantObj);
        } else {
            const index = this.AssessmentParticipant.findIndex(p => p.intakeservicerequestactorid === child.intakeservicerequestactorid);
            this.selectedChild.splice(index, 1);
        }
    }
    chooseChildren(event, child) {
        if (event.checked) {
            this.selectedChild.push(child.intakeservicerequestactorid);
        } else {
            const selectedItem = child.intakeservicerequestactorid;
            const index = this.selectedParticipant.indexOf(selectedItem);
            this.selectedChild.splice(index, 1);
        }
    }
    chooseParticipant(event, participant) {
        if (event.checked) {
            this.selectedParticipant.push(participant.intakeservicerequestactorid);
        } else {
            const selectedItem = participant.intakeservicerequestactorid;
            const index = this.selectedParticipant.indexOf(selectedItem);
            this.selectedParticipant.splice(index, 1);
        }
    }

    getChildName(childList) {
        let childNameList = '';
        if(childList && childList.length) {
            childList.forEach((child, index )=> {
                childNameList = childNameList + ' "'+child.personName+'"' + ((index !== childList.length-1) ? ', ': '');
            });
        }
        return childNameList;
    }

    saveARCAseSummary(caseCreatedDate?) {
        this.ARCaseSummaryForm.patchValue({
            participants: this.AssessmentParticipant ? this.AssessmentParticipant : [],
            caseclosuresummaryid: this.caseclosuresummaryid
        });
        const ARCaseSummaryData = this.ARCaseSummaryForm.getRawValue();
        this.statusTempDropDown = [];
        ARCaseSummaryData.statusList.forEach((item, index) => {
            if (item) {
                this.statusTempDropDown.push(this.closureSubTypeItems[index].value);
            }
        });
        ARCaseSummaryData.closuresubtypekey = this.statusTempDropDown;
        ARCaseSummaryData.intakeserviceid = this.id;
        ARCaseSummaryData.interventionissues = this.getRiskDropDown(ARCaseSummaryData.interventionissues);
        console.log('ARCaseSummaryData' + JSON.stringify(ARCaseSummaryData));
        if (ARCaseSummaryData) {
            this._commonHttpService
                .create(ARCaseSummaryData, 'caseclosuresummary/addupdate')
                .subscribe(response => {
                    this._alertService.success('AR case  summary submitted successfully');
                });
        }
    }

    activateSpeechToText(type): void {
        this.currentSpeechRecInput = type;
        this.recognizing = type;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
            this._speechRecognitionService.record().subscribe(
                // listener
                (value) => {
                    this.speechData = value;
                    switch (type) {
                        case 'q1':
                            const comments = this.ARCaseSummaryForm.getRawValue().serviceProvidedAddress;
                            this.ARCaseSummaryForm.patchValue({ serviceProvidedAddress: comments + ' ' + this.speechData });
                            break;
                        case 'q2':
                            const comments1 = this.ARCaseSummaryForm.getRawValue().issuesRequiring;
                            this.ARCaseSummaryForm.patchValue({ issuesRequiring: comments1 + ' ' + this.speechData });
                            break;
                        case 'q3':
                            const comments2 = this.ARCaseSummaryForm.getRawValue().recommendationforFamily;
                            this.ARCaseSummaryForm.patchValue({ recommendationforFamily: comments2 + ' ' + this.speechData });
                            break;
                        default: break;
                    }
                },
                // errror
                (err) => {
                    console.log(err);
                    this.recognizing = false;
                    if (err.error === 'no-speech') {
                        this.notification = `No speech has been detected. Please try again.`;
                        this._alertService.warn(this.notification);
                        this.activateSpeechToText(type);
                    } else if (err.error === 'not-allowed') {
                        this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText(type);
                    } else if (err.error === 'not-microphone') {
                        this.notification = `Microphone is not available. Please verify the connection of your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText(type);
                    }
                },
                // completion
                () => {
                    this.speechRecogninitionOn = true;
                    console.log('--complete--');
                    this.activateSpeechToText(type);
                }
            );
        } else {
            this.recognizing = false;
            this.deActivateSpeechRecognition();
        }
    }

    deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
    }

    
    ngAfterViewChecked() {

        // if(this.cpsHistPresent)
        // {
        //     var ch = document.getElementById('cps-history-div').children;
        //     // if(ch.length > 0){
        //     var i=0;
        //     for(i=0;i<ch.length;i++){
        //         if(ch[i].tagName == 'pre' || ch[i].tagName == 'PRE'){
        //             ch[i].setAttribute('style','white-space: pre-line;');
        //         }
        //     }
        // }
        if(this.narrativePresent){
            var cn = document.getElementById('reportSummaryNarrative').children;
            
            var i=0;
            for(i=0;i<cn.length;i++){
                if(cn[i].tagName == 'pre' || cn[i].tagName == 'PRE'){
                    cn[i].setAttribute('style','white-space: pre-line;');
                }
            }
        }
    }

    ngOnDestroy(): void {
        this._speechRecognitionService.destroySpeechObject();
    }
    isChildParticipantSelected(childActorId) {
        const form = this.ARCaseSummaryForm.getRawValue();
        if (this.savedParticipants && this.savedParticipants.length) {
            this.savedParticipants.forEach((data, $index) => {
                if (data.intakeservicerequestactorid === childActorId) {
                    return true;
                }
            });
            //   const selected = this.savedParticipants.indexOf(p => p.intakeservicerequestactorid === childActorId)
            //   const Index =  selected ? selected : -1;
            //   return (Index !== -1) ? true : false; }
        }
        return false;
    }

    onChangeClosureStatus(value) {
        if (value === 'ONGS') {
            this.clientrefrdservicesdisabled = true;
            this.ARCaseSummaryForm.patchValue({ clientrefrdservices: '' });
            this.statuslistdisabled = true;
            this.unchecksubtypes();
        } else if (value === 'CONS') {
            this.clientrefrdservicesdisabled = false;
            this.statuslistdisabled = true;
            this.unchecksubtypes();
        } else if (value === 'LDSS') {
            this.clientrefrdservicesdisabled = true;
            this.ARCaseSummaryForm.patchValue({ clientrefrdservices: '' });
            this.statuslistdisabled = false;
        } else if (value === 'RRMS') {
            this.clientrefrdservicesdisabled = true;
            this.ARCaseSummaryForm.patchValue({ clientrefrdservices: '' });
            this.statuslistdisabled = true;
            this.unchecksubtypes();
        }
    }



    getARSummaryCase() {
        console.log('Get data');

        this._commonHttpService.getArrayList({
            where: {
                intakeserviceid: this.id
            },
            method: 'get',
            page: 1,
            limit: 10
        }, 'caseclosuresummary?filter').subscribe((item) => {
            console.log('Get data1' + JSON.stringify(item[0]));
            if (item && item.length) {
                // this.ARCaseSummaryForm.patchValue(item[0]);


                this.ARCaseSummaryForm = this.formBuilder.group({
                    reason: item[0].reason,
                    caseclosuresummaryid: item[0].caseclosuresummaryid,
                    intakeserviceid: item[0].intakeserviceid,
                    referralreason: item[0].referralreason,
                    riskissues: item[0].riskissues,
                    recommendation: item[0].recommendation,
                    statusList: this.formBuilder.array([]),
                    clientrefrdservices: null,
                    closuretypekey: item[0].closuretypekey,
                    // closuresubtypekey: item[0].closuresubtypekey,
                    interventionissues: null,
                    notes: item[0].notes,
                    participants: item[0].participants ? item[0].participants : null
                });
                this.caseclosuresummaryid = item[0].caseclosuresummaryid;
                this.savedParticipants = item[0].participants;
                if (item[0].interventionissues) {
                    const riskData = this.setRiskDropDown(item[0].interventionissues);
                    this.ARCaseSummaryForm.patchValue({ interventionissues: riskData });
                }

                if (item[0].clientrefrdservices) {
                    const clientrefrdservicesdata = this.setRiskDropDown(item[0].clientrefrdservices);
                    this.ARCaseSummaryForm.patchValue({ clientrefrdservices: clientrefrdservicesdata });
                }

                //  const closuresubstypearry = item[0].closuresubtypekey.split(",");
                //  const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
                //  this.closureSubTypeItems.map(function(closuresubtype){
                //      if(item[0].closuresubtypekey.indexOf(closuresubtype.value)){
                //         status.push(new FormControl(false));
                //      }else{
                //         status.push(new FormControl(false));
                //      }
                //  });



                //  this.closureSubTypeItems.map(closuresubtype => {

                //     if(item[0].closuresubtypekey.indexOf(closuresubtype.value)){
                //        status.push(new FormControl(false));
                //     }else{
                //        status.push(new FormControl(false));
                //     }
                // });

                const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
                this.closureSubTypeItems.map(function (closuresubtype, index) {
                    let resp = item[0].closuresubtypekey;
                    if (resp === null || resp === undefined) {
                        resp = '';
                    }
                    const temp = resp.split(',');
                    temp.map(function (sub, subindex) {
                        if (closuresubtype.value === sub) {
                            status.push(new FormControl(true));
                        }
                    });
                    if (status.length === index) {
                        status.push(new FormControl(false));
                    }
                });

                if (item[0].closuretypekey) {
                    this.onChangeClosureStatus(item[0].closuretypekey);
                }
                //  const ARCaseForData = this.ARCaseSummaryForm.getRawValue();
                //  this.ARAssessmentClosureDate = ARCaseForData.ARAssessmentClosureDate;
                //  this.selectedChild = ARCaseForData.selectedChildren;
                //  this.selectedParticipant = ARCaseForData.selectedParticipants;
            }
        });

    }
    getPermanencyPlanList() {
        return this._commonHttpService
          .getPagedArrayList(
            new PaginationRequest({
              page: 1,
              limit: 20,
              method: 'get',
              where: { objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
            }),
            'permanencyplan/list?filter'
            // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          ).map(data => data );
    }
}
