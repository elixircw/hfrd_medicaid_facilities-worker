import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CommonHttpService, DataStoreService, AuthService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { AppConstants } from '../../../../../@core/common/constants';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';


@Component({
  selector: 'case-plan-two',
  templateUrl: './case-plan-two.component.html',
  styleUrls: ['./case-plan-two.component.scss']
})
export class CasePlanTwoComponent implements OnInit {
  report = null;
  caseTwoObj = null;
  livingplacement: any[] = [];
  assessmentsummary: any[] = [];
  legalinfo_tb: any[] = [];
  examinations: any[] = [];
  initialexam: any[] = [];
  annualexam: any[] = [];
  followup: any[] = [];
  reason_followup: any[] = [];
  medications: any[] = [];
  disable: any[] = [];
  expandAll = true;
  xxxxxxxxx: any;

  currentDataStore: any;
  selectedChildData: any;
  periodList: any[] = [];
  selectedPeriodKey: any;

  permanencyPlanData: any;
  personHealthInfo: any;
  permanencyPlanQuestionData: any;
  isSupervisor: boolean;
  snapshotVersions = [];

  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService
  ) {
    this.currentDataStore = this._dataStoreService.getCurrentStore();
  }

  toggleAll() {
    this.expandAll = true;
    console.log('setAllPanelOpen');
    (<any>$(' .panel-collapse:not(".in")')).collapse('show');
  }

  toggleAll1() {
    this.expandAll = false;
    console.log('setAllPanelclose');
    (<any>$(' .panel-collapse.in')).collapse('hide');
  }

  ngOnInit() {
    this.initPeriodList();

    this.selectedChildData = this.currentDataStore['SELECTED_CHILD_DATA'];
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this.getEducationData();


    // Make these methods to patch data
    this.caseTwoObj = {
      childname: 'kane',
      caseworker: 'jack mathew',
      dob: '05/03/2010',
      jurisdiction: 'Tim',
      clientId: '54678',
      csiId: '55-09',
      caseId: '345678765',
      report: 'Five Month',

      servicenplan: {
        q1: 'Treatment Foster Care (Private)',

        // Permanency Plan Question Data
        q2a: 'No',
        q2b: 'Baltimore City',
        q3: 'The current continued to reside in Baltimore city during her commitment to foster care. She currently resides in Baltimore county since she was moved on July 17, 2009.',
        q4a: 'Yes',
        q4b: 'Yes',
        q4c: 'Yes',
        q5: 'Copy of Record (progress report)? Generated',
        q6: 'has been in the same Placement since July 17, 2009. There are no anticipated changes in placement at this time. The Placement may change if the care giver doesn’t obtain employment. She also does not have the back up. Her mother was her back up. She past away. The other problem is that if the caregiver doesn’t ascertain employment, Anther adoptive resource will be sought.',
        q7: 'Folder Generated',
        q8: 'Case worker is trying to promote permanency for this child by getting her adopted - 10/10/2005',
        q9: 'Case worker is trying to pursue adoption with the current Caregiver.',
        q10: 'Lorem ipsum dolor sit amet',
        q11: 'Lorem ipsum dolor sit amet',
        q12: '10/10/2005',
        q13: 'Generated from service plan',
        q14: '10/05/2005',
        q15: '10/05/2005',
        q16: 'Lorem ipsum dolor sit amet.',
        q17: 'Lorem ipsum dolor sit amet.',
        q18: 'Pulled from contacts',
        q19: '10/05/2005',
        q20: 'Will Max',
        q21: 'Lorem ipsum dolor',
        q22: 'Lorem ipsum dolor',
        q23: 'Yes',
        q24: 'Lorem ipsum dolor',
        q25: 'Lorem ipsum dolor',
        q26: 'TPR',
        q27: 'Service',
        q28: '10/05/2005',
        q29: '10/05/2005',
        q30: 'TPR',
        q31: 'TPR',
        q32: 'Yes',
        q33: 'Medical and Cloths',
        q34: '643.00',
        q35: 'Yes',
        q5a: '',
      },
      servicetoprovider: {
        q1: { passport: true },
        q2: { expenses: true },
        q3: '4/3/2019',
        q4: { guide: false },
        q5: { other: true },
        q6: 'Lorem ipsum dolor sit amet',
        q7: { day_care: true },
        q8: { transport: true },
        q9: { finance_support: false },
        q10: { training: true },
        q11: '4/3/2019',
        q12: { repite_care: false },
        q13: 'No',
        q14: 'Lorem ipsum dolor sit amet',
        q15: 'Lorem ipsum dolor sit amet',
        q16: 'No',
        q17: 'This is a treatment Foster home.',
      },
      legalinfo: {
        docket: '',
        voluntary_placement: '',
        voluntary_agg: '',
        shelter_hearing: '',
        adj_hearing: '',
        dispos_hearing: '',
        guardian: '',
        attorney: '',
        doaddresscket: '',
        attorney_telephone: '',
        casa: '',
        address: '',
        telephone: '',
        committed: 'No',
        youth_exper: 'No',
        youth_exper_explain: 'Testing',
        youth_involved: 'Yes',
        name: 'Jennifer E Mclaughlin',
        ohp_status_yes: 'Yes placed 15 of Last 22 Months',
        ohp_status_no: '',
        tpr_volunt_mother: '',
        tpr_volunt_fater: '',
        tpr_grant_date: '',
        tpr_denied_date: '',
        tpr_father_filed: '',
        tpr_mom_filed: '',
        tpr_decision_father: '',
        tpr_decision_mom: '',
      },
      waiver_service: {
        q1: { life_thread_father: true },
        q1a: { life_thread_mom: false },
        q2: { abuse_father: true },
        q2a: { abuse_mom: false },
        q3: { torture_father: false },
        q3a: { torture_mom: true },
        q4: { sex_abuse_father: true },
        q4a: { sex_abuse_mom: false },
        q5: { crime_father: false },
        q5a: { crime_mom: true },
        req_date: '4/3/2019',
        outcome: 'Granted',
        lost_sibling: '',
      },
      court_lang: {
        q1: { reunify_effort: true }
      },
      child_ed: {
        schoolname: '',
        adrcityname: '',
        adresscounty: '',
        school_address: '',
        adrworkphone: '',
        currentgradelevel: '',
        startdate: '',
        enddate: '',
        schoolexitcomments: '',
        educationname: '',
        schoolsettingtypekey: '',
        classtypetypekey: '',
        transportmodetypedetail: '',
        lastgradelevel: '',
        tel_num: '',
        grade_current: '',
        start_dt: '',
        end_dt: '',
        exit_reason: '',
        edu_program: '',
        setting: '',
        type_class: '',
        mode_trans: '',
        grade_last: '',
        school_enroll: '',
        prox_placement_nm: '',
        prox_type__class: '',
        prox_address: '',
        prox_telephone: '',
        prox_setting: '',
        prox_grade: '',
        prox_last_dt: '',
        prox_exit_dt: '',
        school_change: '',
        interest_change: '',
        appro_current_ed: '',
        ed_service_receive: '',
        enroll_special_prg: '',
        report_copy: '',
        iep_record: '',
        iep_dated: '',
        ed_parent_name: '',
        ed_parent_address: '',
        ed_parent_req: '',
        age_level: '',
        behavioral_prb: '',
        prb_peers: '',
        child_strength: '',
        child_weak: '',
        childbehave_schoolnm: '',
        childbehave_entrydt: '',
        childbehave_exitdt: '',
        behave_prb: '',
        behave_peers: '',
        quarterly_grade1: '',
        quarterly_grade2: '',
        school_schedule: '',
        school_comment: 'Child initially started her first few years as being special education. She no longer require special educational services. She is at her grade level academically.',
        fungrade_level: 'Grade 3',
        special_ed_code: '',
        child_extra_active: '',
        ed_prg_goal: '',
        child_comment: '',
        child_status: 'No',
        q1: { behave_prb: true },
        q2: { behave_peers: false },
        describe: 'Child needs to learn how to deal with her anger and not act out so behaviourally.',

      },
      child_employment: {
        employed: 'Yes',
        cur_employer: 'fost care',
        cur_employment: '10 Months',
        earnings: '100'
      },
      child_health: {
        child_ma: 'Good'
      },
      mental_health: {
        evaluation: 'No',
        reason: 'NA',
        eval_dt: '08/27/2007',
        eval_by: 'James',
      },
      healthcare: {
        conf: 'NA',
        conf_dt: 'NA',
        note: 'NA'
      },
      info_sheet: {
        fname: '',
        father_client: '',
        f_aka: '',
        f_bod: '',
        f_address: '',
        f_security_no: '',
        f_telephone: '',
      },
      child_list: {
        Name: '',
        bod: '',
        caregiver: '',
        partner: '',
      },
      fin_resource: {
        income_dt: '',
        income_type: '',
        income_amount: '',
        asset_dt: '',
        asset_type: '',
        asset_amount: '',
      }
      ,
      disable: {
        permanent: '',
        type: '',
      }
    };

    this.livingplacement = [{
      placement: 'Foster care',
      name: 'Kane',
      start_date: '23/09/2018',
      end_date: '08/10/2018',
      type: 'Care',
      program_nm: 'Foster care'
    }];

    this.assessmentsummary = [];

    this.legalinfo_tb = [{
      date: '23/09/2018',
      type: 'CANS-F',
      status: 'Completed'
    }];

    // this.initialexam=[{
    //   entry:'Entry',
    //   type:'NA',
    //   provider:'NA',
    //   date:'NA',
    // }]

    // this.annualexam=[{
    //   type:'NA',
    //   provider_nm:'NA',
    //   date:'NA',
    // }]

    // this.followup=[{
    //   type:'NA',
    //   appt_date:'NA',
    //   provider_details:'NA'
    // }]

    // this.reason_followup=[{
    //   reason:'NA',
    //   appt_date:'NA',
    // }]

    // this.medications=[{
    //   medication_nm:'NA',
    //   dose_frq: '12',
    //   date_start:'09/02/2019',
    //   date_stop:'03/04/2019',
    //   order_physician:'NA',
    //   pharmacy_nm:'NA',
    //   reason_medic:'NA'
    // }]
    this.getHist();
  }


  getEducationData() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PED')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          console.log('education--------------------');
          console.log(res);
          const total_data = res.length - 1;
          if (total_data >= 0 && res[total_data] && res[total_data].metadata && res[total_data].metadata.education.personeducation) {
            const educationDetails = res[total_data].metadata.education.personeducation[0];
            this.caseTwoObj.child_ed.schoolname = educationDetails.summerschoolname;
            this.caseTwoObj.child_ed.adrcityname = educationDetails.adrcityname;
            this.caseTwoObj.child_ed.adresscounty = educationDetails.adresscounty;
            this.caseTwoObj.child_ed.statecode = educationDetails.statecode;
            this.caseTwoObj.child_ed.adrworkphone = educationDetails.adrworkphone;
            this.caseTwoObj.child_ed.currentgradelevel = educationDetails.currentgradelevel;
            this.caseTwoObj.child_ed.startdate = educationDetails.startdate;
            this.caseTwoObj.child_ed.enddate = educationDetails.enddate;
            this.caseTwoObj.child_ed.schoolexitcomments = educationDetails.schoolexitcomments;
            this.caseTwoObj.child_ed.educationname = educationDetails.educationname;
            this.caseTwoObj.child_ed.schoolsettingtypekey = educationDetails.schoolsettingtypekey;
            this.caseTwoObj.child_ed.classtypetypekey = educationDetails.classtypetypekey;
            this.caseTwoObj.child_ed.transportmodetypedetail = educationDetails.transportmodetypedetail;
            this.caseTwoObj.child_ed.lastgradelevel = educationDetails.lastgradelevel;
            this.caseTwoObj.child_ed.prox_telephone = educationDetails.adrworkphone;
            this.caseTwoObj.child_ed.prox_setting = educationDetails.schoolsettingtypekey;
            this.caseTwoObj.child_ed.prox_grade = educationDetails.currentgradelevel;
            this.caseTwoObj.child_ed.prox_exit_dt = educationDetails.enddate;
            this.caseTwoObj.child_ed.prox_last_dt = educationDetails.lastattendeddate;
            this.caseTwoObj.child_ed.prox_type__class = educationDetails.classtypetypekey;
            this.caseTwoObj.child_ed.prox_type__class = educationDetails.classtypetypekey;
            this.caseTwoObj.child_ed.prox_type__class = educationDetails.classtypetypekey;
            this.caseTwoObj.child_ed.prox_address = educationDetails.adrcityname + educationDetails.adresscounty + educationDetails.educationDetails.statecode;
            console.log(this.caseTwoObj.child_ed);

          }
        }
      );
  }
    // Person Health info
    getHealthData() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PMED')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;

          res.forEach(element => {
            obj = {
              medication_nm: element.metadata[0].medicinename ? element.metadata[0].medicinename : '',
              dose_frq: element.metadata[0].frequency ? element.metadata[0].frequency : '',
              date_start: element.metadata[0].startdate ? element.metadata[0].startdate : '',
              date_stop: element.metadata[0].enddate ? element.metadata[0].enddate : '',
              order_physician: element.metadata[0].prescribingdoctor ? element.metadata[0].prescribingdoctor : '',
              pharmacy_nm: '',
              reason_medic: element.metadata[0].prescribedreason ? element.metadata[0].prescribedreason : ''
            };
            this.medications.push(obj);
          });
          console.log('Tanmay', this.medications);
        }
      );
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PEXAM')
      .subscribe(
        (response) => {
          this.examinations = response;
          let res, obj, obj1;

          res = response;

          res.forEach( exam => {
            if (exam.metadata[0].appointment[0].natureofexamkey === 'Initial Health Screening Examination') {
              obj = {
                entry: exam.metadata[0].appointment[0].natureofexamkey ? exam.metadata[0].appointment[0].natureofexamkey : '',
                type: exam.metadata[0].appointment[0].specialityexamkey ? exam.metadata[0].appointment[0].specialityexamkey : '',
                provider: '',               // @TM: field not available
                date: exam.metadata[0].appointment[0].apptDate ? exam.metadata[0].appointment[0].apptDate : ''
              };
              this.initialexam.push(obj);
            }

            if (exam.metadata[0].appointment[0].natureofexamkey === 'Problem Follow-up Health Examination') {
              obj = {
                type: exam.metadata[0].appointment[0].specialityexamkey ? exam.metadata[0].appointment[0].specialityexamkey : '',
                appt_date: exam.metadata[0].appointment[0].apptDate ? exam.metadata[0].appointment[0].apptDate : '',
                provider_details: ''        // @TM: field not available
              };
              this.followup.push(obj);

              obj1 = {
                reason: exam.metadata[0].appointment[0].nextappointmentreason ? exam.metadata[0].appointment[0].nextappointmentreason : '',
                appt_date: exam.metadata[0].appointment[0].nextApptDate ? exam.metadata[0].appointment[0].nextApptDate : ''
              };
              this.reason_followup.push(obj1);
            }

            if (exam.metadata[0].appointment[0].natureofexamkey === 'Comprehensive Health Examination') {
              obj = {
                type: exam.metadata[0].appointment[0].specialityexamkey ? exam.metadata[0].appointment[0].specialityexamkey : '',
                provider_nm: '',             // @TM: field not available
                date: exam.metadata[0].appointment[0].apptDate ? exam.metadata[0].appointment[0].apptDate : ''
              };
              this.annualexam.push(obj);
            }
          });

          console.log('Tanmay', this.medications);
        }
      );

    // this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PDISADDUP')
    // .subscribe(
    //   (response) => {
    //     var res, obj;
    //     res = response;
    //     console.log("Karthik", res);
    //     res.forEach(element => {
    //       obj = {
    //         permanent: element.metadata[0].disabilityflag ? element.metadata[0].disabilityflag : '',
    //         type: element.metadata[0].disabilitytypekey ? element.metadata[0].disabilitytypekey : '',
    //         start_dt: element.metadata[0].startdate ? element.metadata[0].startdate : '',
    //         end_dt: element.metadata[0].enddate ? element.metadata[0].enddate : ''
    //       }
    //       this.disable.push(obj);
    //     });
    //     console.log("Karthik", this.disable);
    //   }
    // );

    this.getProviderInfo();
    this.getBirthHealthInfo();
    this.getSexualInfo();
    this.getHospitalizationInfo();
    this.getImmunizationInfo();
    this.getFamilyHistoryInfo();
    this.getInsuranceInfo();
    this.getDisabilitiesInfo();
    this.getBehaviouaralInfo();
    this.getSubstanceAbuseInfo();
     this.getEducationData();
     this.getAssessment();
     this.getChildInfo();

  }

  getChildInfo() {
    this.caseTwoObj.childname = this.selectedChildData.firstname + ' '+  this.selectedChildData.lastname;
    this.caseTwoObj.caseworker = this.selectedChildData.caseheadname;
    this.caseTwoObj.dob = this.selectedChildData.dob;
    this.caseTwoObj.jurisdiction = this.selectedChildData.county;
    this.caseTwoObj.clientId =  this.selectedChildData.cjamspid;
    this.caseTwoObj.report = this.report;
    this.caseTwoObj.caseId =  this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
   }

  //  Veera
  getProviderInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PPHYI')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;

          // if object

          // this.child_employment ={
          //   employed: res.employed,
          //   cur_employer: res.employed,
          //   cur_employment: res.employed,
          //   earnings: res.employed,
          // };
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              console.log(element);

              // const child_employment_obj = {
              //   employed: element.employed,
              //   cur_employer: element.employed,
              //   cur_employment: element.employed,
              //   earnings: element.employed,
              // };
              // this.child_employment.push(child_employment_obj);

            });
          }
        });
  }

  getBirthHealthInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'CU5YII')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getSexualInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PSEXINFO')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getHospitalizationInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PHOS')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getImmunizationInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PIMM')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getFamilyHistoryInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PFI')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getInsuranceInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PHLTHINS')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getDisabilitiesInfo() {

    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PDISADDUP')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;


          // res.forEach(element => {
          //   obj = {
          //     permanent: element.metadata[0].disabilityconditiontypekey ? element.metadata[0].disabilityconditiontypekey : '',
          //     type: element.metadata[0].disabilitytypekey ? element.metadata[0].disabilitytypekey : '',
          //     start_dt: element.metadata[0].startdate ? element.metadata[0].startdate : '',
          //     end_dt: element.metadata[0].enddate ? element.metadata[0].enddate : ''
          //   }
          //   this.disable.push(obj);
          // });

          res.forEach(element => {
            obj = {
              permanent: element.metadata.disabilityflag ? element.metadata.disabilityflag : '',
              type: element.metadata.disabilitytypekey ? element.metadata.disabilitytypekey : '',
              start_dt: element.metadata.startdate ? element.metadata.startdate : '',
              end_dt: element.metadata.enddate ? element.metadata.enddate : ''
            };
            this.disable.push(obj);
          });
          // if(res && Array.isArray(res)) {
          // res.forEach(element => {
          //   obj = {element};
          //  });
          // }
        });
  }

  getBehaviouaralInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PBHVHLTH')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getSubstanceAbuseInfo() {
    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PABUSE')
      .subscribe(
        (response) => {
          let res, obj;
          res = response;
          if (res && Array.isArray(res)) {
            res.forEach(element => {
              obj = { element };
            });
          }
        });
  }

  getEducationInfo() {

    this.getCasePlanDataByLogType(this.selectedChildData.personid, 'PED')
      .subscribe(
        (response) => {
          let res, obj, personEducationElement, element;
          res = response[0];
          element = res;
              personEducationElement = element.metadata.education.personeducation[0];
              this.caseTwoObj.child_ed = {
                school_nm: personEducationElement.educationname ? personEducationElement.educationname : '',
                school_address: '',
                tel_num: personEducationElement.adrworkphone ? personEducationElement.adrworkphone : '',
                grade_current: personEducationElement.currentgradetypekey ? personEducationElement.currentgradetypekey : '',
                start_dt: personEducationElement.startdate ? personEducationElement.startdate : '',
                end_dt: personEducationElement.enddate ? personEducationElement.enddate : '',
                exit_reason: personEducationElement.schoolexitcomments ? personEducationElement.schoolexitcomments : '',
                edu_program: 'Special',
                setting: personEducationElement.schoolsettingtypekey ? personEducationElement.schoolsettingtypekey : '',
                type_class: personEducationElement.classtypetypekey ? personEducationElement.classtypetypekey : '',
                mode_trans: personEducationElement.transportmodetypekey ? personEducationElement.transportmodetypekey : '',
                grade_last: personEducationElement.lastgradetypekey ? personEducationElement.lastgradetypekey : '',
                school_enroll: '',
                prox_placement_nm: '',
                prox_type__class: '',
                prox_address: '',
                prox_telephone: personEducationElement.adrworkphone ? personEducationElement.adrworkphone : '',
                prox_setting: '',
                prox_grade: '',
                prox_last_dt: '',
                prox_exit_dt: '',
                school_change: 'NA',
                interest_change: 'Yes',
                appro_current_ed: '',
                ed_service_receive: '',
                enroll_special_prg: '',
                report_copy: '',
                iep_record: '',
                iep_dated: personEducationElement.lastiepdate ? personEducationElement.lastiepdate : '',
                ed_parent_name: '',
                ed_parent_address: '',
                ed_parent_req: '',
                age_level: '',
                behavioral_prb: '',
                prb_peers: '',
                child_strength: '',
                child_weak: '',
                childbehave_schoolnm: '',
                childbehave_entrydt: '',
                childbehave_exitdt: '',
                behave_prb: '',
                behave_peers: '',
                quarterly_grade1: personEducationElement.firstqtrperformancetypekey ? personEducationElement.firstqtrperformancetypekey : '',
                quarterly_grade2: personEducationElement.secondqtrperformancetypekey ? personEducationElement.secondqtrperformancetypekey : '',
                school_schedule: personEducationElement.schoolschedule ? personEducationElement.schoolschedule : '',
                school_comment: 'Child initially started her first few years as being special education. She no longer require special educational services. She is at her grade level academically.',
                fungrade_level: personEducationElement.functioninggradelevel ? personEducationElement.functioninggradelevel : '',
                special_ed_code: '',
                child_extra_active: '',
                ed_prg_goal: '',
                child_comment: '',
                child_status: 'No',
                q1: { behave_prb: true },
                q2: { behave_peers: false },
                describe: 'Child needs to learn how to deal with her anger and not act out so behaviourally.',
              };

        });
  }

  getPermanencyPlanData() {
    // Get all the different bunch of data as required
    // Each can be made on specific reference id as needed

    // Permanency plan data
    this.getCasePlanDataByLogType(this.selectedChildData.intakeservicerequestactorid, 'PPLAN')
      .subscribe(
        (response) => {
          if (response.length > 0) {
            this.permanencyPlanData = response[0].metadata;

            // Selecting the most recent data from the returned result set as it is sorted by insertedon
            if (this.permanencyPlanData && this.permanencyPlanData.permplanquestdata) {
              this.permanencyPlanQuestionData = this.permanencyPlanData.permplanquestdata;
            }

            console.log('PPLAN Data => ', this.permanencyPlanData);
            console.log('PPLAN Question Data =>', this.permanencyPlanQuestionData);

             this.patchPermanencyPlanData();
          }
        }
      );
  }

  patchPermanencyPlanData() {
    console.log('Permanency Plan Data ------', this.permanencyPlanQuestionData);
    this.caseTwoObj.servicenplan = {
    q2a : this.permanencyPlanQuestionData.isInClosedProximity,
    q2b : this.permanencyPlanQuestionData.isInClosedProximityExpln,
    q3 : this.permanencyPlanQuestionData.meetingSafetyNeedsExpln,
    // q4 ?
    // q5 ?
    q6 : this.permanencyPlanQuestionData.sixMonthsPlacementExpln,
    // q7 ? court order
    // q8 ? court order
    q9 : this.permanencyPlanQuestionData.courtOrdersExpln,

    // q9a: pp primary, concurret, establihed date

    // q10: service plan
    q11: this.permanencyPlanQuestionData.permToPermExpln,
    // q12 : adoption screen

    // q13 : contacts se -- face - to - face

    q14: this.permanencyPlanQuestionData.safeAndCareExpln,

    // q15 : service plan se

    q16: this.permanencyPlanQuestionData.assessmentPeriodExpln,
    q17: this.permanencyPlanQuestionData.lifebookExpln,

    // q18: child removal se
    // q19: service plan, visitation plan
    // q20: MERGE with 18

    // q21: Pull all the action iteams and start & end date where focus area is Renification
    // q22: NEED MORE CLARIFICATION

    q23: this.permanencyPlanQuestionData.serviceAgreementExpln,

    // q24: Service Log
    // q25: deleted

    // this.permanencyPlanQuestionData.permToPermExpln;
    // this.permanencyPlanQuestionData.safeAndCareExpln;
    // this.permanencyPlanQuestionData.assessmentPeriodExpln;
    // this.permanencyPlanQuestionData.lifebookExpln;
    // this.permanencyPlanQuestionData.serviceAgreementExpln;

    // this.permanencyPlanQuestionData.isProviderAgree;
    // this.permanencyPlanQuestionData.isProviderAgreeExpln;
    // this.permanencyPlanQuestionData.serviceAgreementForOtherExpln;
    q2_2ans : this.permanencyPlanQuestionData.isProviderAgree,
    q2_2expln : this.permanencyPlanQuestionData.isProviderAgreeExpln,
    q2_3 : this.permanencyPlanQuestionData.serviceAgreementForOtherExpln
    };
  }

  selectPeriod(item) {
    this.selectedPeriodKey = item.key;
    this.report = item.value;
    this.getPermanencyPlanData();
    this.getHealthData();
    this.getDisabilitiesInfo();
  }

  initPeriod(key, value, numDays) {
    const item = {
      'fromDate': new Date(),
      'toDate': new Date(),
      'key': key,
      'value': value,
      'numDays': numDays,
    };
    return item;
  }

  initPeriodList() {
    this.periodList = [
      this.initPeriod(1, '0 - 60', 60),
      this.initPeriod(2, '61 - 90', 30),
      this.initPeriod(3, '91 - 180', 90),
      this.initPeriod(4, '180 +', 100)
    ];
  }

  getAssessment() {
    this.getAssessmentByActorID(this.selectedChildData.intakeservicerequestactorid)
    .subscribe(
      (response) => {
        if (response && response.length > 0) {
          this.assessmentsummary = response;
          console.log('assessmentsummary => ', this.assessmentsummary);
        } else {
          this.assessmentsummary  = [];
        }
      }
    );
  }

  getAssessmentByActorID(actorIDData){
    return this._commonHttpService.getArrayList(
      {
        where: {
          activeflag: '1',
          actorID: actorIDData,
         removalDate: this.selectedChildData && this.selectedChildData.removalInfo && this.selectedChildData.removalInfo.removaldate ?this.selectedChildData.removalInfo.removaldate:new Date(), // this.selectedChildData.removalInfo.removaldate,
          rangeType: this.selectedPeriodKey
        },
        method: 'get',
        nolimit: true
      },
      'admin/assessment/getassesmentbyactorid?filter');
  }

  getCasePlanDataByLogType(referenceId, logType) {
    return this._commonHttpService.getArrayList(
      {
        where: {
          activeflag: '1',
          referenceid: referenceId,
          logType: logType,
          removalDate:  this.selectedChildData && this.selectedChildData.removalInfo && this.selectedChildData.removalInfo.removaldate ?this.selectedChildData.removalInfo.removaldate:new Date(), // this.selectedChildData.removalInfo.removaldate,
          rangeType: this.selectedPeriodKey
        },
        method: 'get',
        nolimit: true
      },
      'auditlog/list?filter');
  }

  generatecaseplansnapshot() {
    this._commonHttpService.getArrayList(
      {
        personid: this.selectedChildData.personid,
        servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID),
        intakeservicerequestactorid: this.selectedChildData.intakeservicerequestactorid,
        periodstartdate:'2019-07-1',
        periodenddate:'2019-07-30',
        method: 'post',
        nolimit: true
      },
      'caseplan/generatecaseplansnapshot').subscribe(data => {
        this.getHist();
      });
  }

  getHist() {
    this._commonHttpService
    .getArrayList(
      new PaginationRequest({
        page: 1,
        limit: 20,
        method: 'get',
        where: {
         objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID),
         objecttype: 'CPLAN2',
         personid: this.selectedChildData.personid
         }
      }),
      'snapshothist' + '?filter'
    ).subscribe(
      response => {
        this.snapshotVersions = response;
     });
  }


  casePlan2Print(item, index) {
    const inputRequest = {
      'caseplan2data': (Array.isArray(item.snapshotdata) && item.snapshotdata.length) ? item.snapshotdata[0] : null,
      'isheaderrequired': false,
  };
  const payload = {
    method: 'post',
    count: -1,
    page: 1,
    limit: 20,
    where: inputRequest,
    documntkey: [
      'oohome'
  ],
  };
  this._commonHttpService.create(payload, 'caseplan/getReportCaseplan').subscribe(
    response => {
      console.log(response, 'pdf response');
      setTimeout(() => window.open(response.data.documentpath), 2000);
   });
    // this.backupService = this.selectedService ? this.selectedService : null;
    // this.selectedService = item;
    // (<any>$('#servicePlanOohPrint')).modal('show');
  }
}
