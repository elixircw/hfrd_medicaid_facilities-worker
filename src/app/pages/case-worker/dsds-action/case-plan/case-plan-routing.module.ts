import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CasePlanComponent } from './case-plan.component';
import { CasePlanResolverService } from './case-plan-resolver.service';
import { CasePlanOneComponent } from './case-plan-one/case-plan-one.component';
import { CasePlanFourComponent } from './case-plan-four/case-plan-four.component';
import { CasePlanThreeComponent } from './case-plan-three/case-plan-three.component';
import { CasePlanTwoComponent } from './case-plan-two/case-plan-two.component';
import { CaseReviewComponent } from './case-review/case-review.component';
const routes: Routes = [
  {
  path: '',
  component: CasePlanComponent,
  resolve: {
    config: CasePlanResolverService
  },
  children: [
    {
      path: 'case-plan-one',
      component: CasePlanOneComponent
    },
    {
      path: 'case-plan-two',
      component: CasePlanTwoComponent
    },
    {
      path: 'case-plan-three',
      component: CasePlanThreeComponent
    },
    {
      path: 'case-plan-four',
      component: CasePlanFourComponent
    },
    {
      path: 'case-review',
      component: CaseReviewComponent
    }
  ]
 }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CasePlanRoutingModule { }
