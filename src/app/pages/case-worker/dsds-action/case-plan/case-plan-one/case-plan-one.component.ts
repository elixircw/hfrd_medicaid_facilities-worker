import { Component, OnInit } from '@angular/core';
import { DataStoreService, CommonHttpService } from '../../../../../@core/services';
import { CasePlanService } from '../case-plan.service';
import * as jsPDF from 'jspdf';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import * as moment from 'moment';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
@Component({
  selector: 'case-plan-one',
  templateUrl: './case-plan-one.component.html',
  styleUrls: ['./case-plan-one.component.scss']
})
export class CasePlanOneComponent implements OnInit {
  store: any;
  cp1Data: any;
  selectedChild: any;
  childList = [];
  parent1: any;
  parent2: any;
  raceDropDown = [];
  riskAssessment: any;
  safeCAssessment: any;
  searchResult: any;
  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
  intakeserviceid: any;
  mfiradate: any;
  safecdate: any;
  removaladdress: any;
  constructor(
    private _dataStoreService: DataStoreService,
    private _casePlanService: CasePlanService,
    private _commonHttpService: CommonHttpService
  ) {
    this.store = this._dataStoreService.getCurrentStore();
    this.cp1Data = this.store['CP1DATA'];
    this._casePlanService.loadRaceDropDown().subscribe(data => {
      this.raceDropDown = data;
      this.viewChildInfo(this.cp1Data);
    });
   }

  ngOnInit() {
    const parentList = this._casePlanService.getParentList();
    // this.parent1 = (Array.isArray(parentList) && parentList.length >= 1) ? parentList[0] : null;
    // this.parent2 = (Array.isArray(parentList) && parentList.length >= 2) ? parentList[1] : null;
    if (this.cp1Data) {
      this.viewChildInfo(this.cp1Data);
    }
  }

  viewChildInfo(child) {
    this.selectedChild = child;
    this.selectedChild.parent1name = ( this.parent1) ? ( this.parent1.firstname + ' ' +  this.parent1.lastname ) : null;
    this.selectedChild.parent2name = ( this.parent2) ? ( this.parent2.firstname + ' ' +  this.parent2.lastname ) : null;
    this.selectedChild.riskAssessment = (this.riskAssessment && this.riskAssessment.length) ? this.riskAssessment[0] : null;
    this.selectedChild.safeCAssessment = (this.safeCAssessment && this.safeCAssessment.length) ? this.safeCAssessment[0] : null;
    this.viewSocialHistoryInfo(child);
    const childremovalinfo = this._casePlanService.childRemovalInfo;
    const childremoval = childremovalinfo.find(item => item.personid === child.personid);
    this.removaladdress = childremoval.removaladd1;
    this._casePlanService.getassessment().subscribe(response => {
      const list = response.data;
      const mfiraass = list.find(item => item.description.toUpperCase() === 'MARYLAND FAMILY RISK REASSESSMENT');
      const safecass = list.find(item => item.description.toUpperCase() === 'SAFE-C');
      this.mfiradate = (Array.isArray(mfiraass.intakassessment) && mfiraass.intakassessment.length) ? mfiraass.intakassessment[0].updateddate : null;
      this.safecdate = (Array.isArray(safecass.intakassessment) && safecass.intakassessment.length) ? safecass.intakassessment[0].updateddate : null;
    });
    if (this.selectedChild) {
      this.setParentInformation(this.selectedChild.personid);
    }

  }

 

  viewSocialHistoryInfo(child) {
    if ( child &&  child.intakeservicerequestactorid) {
      const selectedChild = this._casePlanService.getSocialHistory(child.intakeservicerequestactorid, child.removaldate);
      console.log( this.selectedChild);
      selectedChild.subscribe(data => {
        console.log(data);
        if (data && data && data.length) {
         this.selectedChild.placement = data[0].metadata.placement;
         this.selectedChild.familyhistory = data[0].metadata.familyhistory;
         this.selectedChild.childdesc = data[0].metadata.childdesc;
        }
      });
    }  else  {
      this.selectedChild = [];
    }
  }

  getRaceDesc(race) {
    race = Array.isArray(race) ? race : [];
    if (this.raceDropDown) {
      const raceList = '';
      race = race.map(item => {
        const a = this.raceDropDown.find(ele => ele.racetypekey === item.racetypekey);
        return a ? a.typedescription : '';
      });
      return race.toString();
      // return this.raceDropDown[key];
    } else {
      return null;
    }
  }

  // async downloadCasePdf(val) {
  //   const source = document.getElementById(val);
  //   const pages = document.getElementsByClassName('pdf-page');
  //   let pageImages = [];
  //   for (let i = 0; i < pages.length; i++) {
  //     const pageName = pages.item(i).getAttribute('data-page-name');
  //     const isPageEnd = pages.item(i).getAttribute('data-page-end');
  //     await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
  //           const img = canvas.toDataURL('image/png');
  //           pageImages.push(img);
  //           if (isPageEnd === 'true') {
  //               this.pdfFiles.push({ fileName: pageName, images: pageImages });
  //               pageImages = [];
  //           }
  //         });
  //   }
  //    this.convertImageToPdf();
  // }

  // convertImageToPdf() {
  //     this.pdfFiles.forEach((pdfFile) => {
  //         // const doc = new jsPDF('landscape');
  //         let doc = null;
  //         if (pdfFile.fileName === 'In Home Service Plan') {
  //             doc = new jsPDF('landscape');
  //         } else {
  //             doc = new jsPDF();
  //         }

  //         const width = doc.internal.pageSize.getWidth() - 10;
  //         const heigth = doc.internal.pageSize.getHeight() - 10;
  //         pdfFile.images.forEach((image, index) => {

  //             doc.addImage(image, 'JPEG', 3, 5, width, heigth);
  //             if (pdfFile.images.length > index + 1) {
  //                 doc.addPage();
  //             }
  //         });console.log(pdfFile.fileName);
  //         doc.save(pdfFile.fileName);
  //     });
  //     (<any>$('#docu-View')).modal('hide');
  //     this.pdfFiles = [];
  //     // this.downloadInProgress = false;
  // }

  printReport() {
    this.intakeserviceid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this._commonHttpService.getSingle({
        count: -1,
        where: {
            documenttemplatekey: ['CasePlanSocialHistory'],
            intakeserviceid: this.intakeserviceid,
            clientId: (this.selectedChild) ? this.selectedChild.cjamspid : null,
            name: (this.selectedChild) ? this.selectedChild.firstname + ' ' + this.selectedChild.lastname : null,
            cjamspid: (this.selectedChild) ? this.selectedChild.cjamspid : null,
            removaldate: (this.selectedChild) ? moment(this.selectedChild.removaldate).format('MM/DD/YYYY') : null,
            racetypekey: (this.selectedChild && this.selectedChild.race) ? this.getRaceDesc(this.selectedChild.race) : null,
            gender: (this.selectedChild) ? this.selectedChild.gender : null,
            dob: (this.selectedChild) ?  moment(this.selectedChild.dob).format('MM/DD/YYYY') : null,
           // birthplace: null,
            ssn: (this.selectedChild) ? this.selectedChild.ssn : null,
            religion: (this.selectedChild) ? this.selectedChild.religion : null,
            parent1name: (this.selectedChild) ? this.selectedChild.parent1name : null,
            parent2name: (this.selectedChild) ? this.selectedChild.parent2name : null,
            removaladd1: (this.removaladdress) ? this.removaladdress : null,
            riskassessupdatedon: (this.mfiradate) ? moment(this.mfiradate).format('MM/DD/YYYY') : null,
            safeassessupdatedon: (this.safecdate) ? moment(this.safecdate).format('MM/DD/YYYY') : null,
            removalinfo: (this.selectedChild && this.selectedChild.removalInfo) ? this.selectedChild.removalInfo.comments : null,
            placement: (this.selectedChild.placement) ? this.selectedChild.placement : null,
            familyhistory: (this.selectedChild) ? this.selectedChild.familyhistory : null,
            childdesc: (this.selectedChild) ? this.selectedChild.childdesc : null,
        },
        method: 'post'
    }, 'evaluationdocument/generateintakedocument').subscribe((data) => {
        if (data && data.data[0]) {
            window.open(data.data[0].documentpath, '_blank');
        }
    });
  }

  setParentInformation(personid) {
    this._casePlanService.getRelationshipOfPerson(personid).subscribe(response => {
      if (Array.isArray(response) && response.length) {
        const bioFather = response.find(item => item.relationshiptypekey === 'BGFTHR');
        const bioMother = response.find(item => item.relationshiptypekey === 'BGMTHR');
        if (bioFather && Array.isArray(this._casePlanService.personList)) {
            this.parent1 = this._casePlanService.personList.find(item => item.personid === bioFather.person2id);
        } else {
          this.parent1 = null;
        }
        if (bioMother && Array.isArray(this._casePlanService.personList)) {
          this.parent2 = this._casePlanService.personList.find(item => item.personid === bioMother.person2id);
      } else {
        this.parent2 = null;
      }
      } else {
        this.parent1 = null;
        this.parent2 = null;
      }
      this.selectedChild.parent1name = ( this.parent1) ? ( this.parent1.firstname + ' ' +  this.parent1.lastname ) : null;
      this.selectedChild.parent2name = ( this.parent2) ? ( this.parent2.firstname + ' ' +  this.parent2.lastname ) : null;

    });
  }
}
