/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CasePlanOneComponent } from './case-plan-one.component';

describe('CasePlanOneComponent', () => {
  let component: CasePlanOneComponent;
  let fixture: ComponentFixture<CasePlanOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasePlanOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasePlanOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
