export const CP_TABS = [
    {
        tabName: 'CP1',
        title: 'Social History',
        path: 'case-plan-one'
    },
    {
        tabName: 'CP2',
        title: 'Permanency',
        path: 'case-plan-two'
    },
    {
        tabName: 'CP3',
        title: 'Service Plan',
        path: 'case-plan-three'
    },
    {
        tabName: 'CP4',
        title: 'Youth Transition Plan',
        path: 'case-plan-four'
    },
    {
        tabName: 'CP5',
        title: 'Case Review',
        path: 'case-review'
    }
];
