import { Injectable } from '@angular/core';
import { CommonHttpService, DataStoreService, AuthService, SessionStorageService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';
// import { PersonDisabilityService } from '../../../shared-pages/person-disability/person-disability.service';
const UNSAFE = 0;
@Injectable()
export class CasePlanService {
  intakeserviceid: string;
  daNumber: string;
  personList = [];
  parent = [];
  childList = [];
  teamTypeKey: string;
  CHILD_CATEGORIES = ['CHILD', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC', 'AV'];
  PARENT1_CATEGORIES = ['BGMTHR'];
  PARENT2_CATEGORIES = ['BGFTHR'];
  removedChildren = [];
  public childRemoval$ = new Subject<any>();
  public childRemovalInfo$ = new Subject<any>();
  public removalConfig$ = new Subject<any>();
  childRemovalInfo = [];

  constructor(private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    // private _personDisabilityService: PersonDisabilityService,
    private _session: SessionStorageService) {
    this.teamTypeKey = this._authService.getAgencyName();
  }

  getPersonsAndChildRemovalInfo() {
    return forkJoin([this.getPersonsList(), this.getChildRemoval()])
      .map(([persons, childRemoval]) => {
        let personList = [];
        let childRemovalInfo = [];
        if (persons && persons.data) {
          personList = persons.data;
        }
        if (childRemoval) {
          childRemovalInfo = childRemoval;
        }
        this.loadPersonList(personList);
        this.loadChildRemoval(childRemovalInfo);
        this.updatePersonListWithChildRemoval();



        return { persons: personList, childRemovalInfo: childRemovalInfo };
      });
  }

  loadPersonList(persons) {
    this.personList = persons;
    // this.loadPersonDisabilities();
  }

  loadRaceDropDown() {

    return this._commonService
    .getSingle(
      {

        where: { activeflag: '1' },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.RaceTypeUrl + '?filter'
    );

  }

  getParent(ParentRole) {
    console.log('process person list', this.personList);
    this.parent = this.personList.filter(parent =>
  parent.relationship === ParentRole
  ) ;
      // parent.roles.forEach(role => {
      //   const parentCategory = ParentRole.find(category => category === role.intakeservicerequestpersontypekey);
      //   if (parentCategory) {
      //     parentFound = true;
      //     return;
      //   }
      // });
      // return childFound; // removing unsafe child validation for 11/3 demo
    //   return parentFound; //  comment if unsafe child only needs to remove
    // });

    return this.parent;
  }

  getParentList() {
    console.log('process person list', this.personList);
    this.parent = this.personList.filter(parent => {
      const roleList = Array.isArray(parent.roles) ? parent.roles : [];
      return roleList.some(item => item.intakeservicerequestpersontypekey === 'PARENT');
    });
    return this.parent;
  }

  getChildList() {
    console.log('process person list', this.personList);
    this.childList = this.personList.filter(child => {
      let childFound = false;
      if (child.roles) {
        child.roles.forEach(role => {
          const childCategory = this.CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
          if (childCategory) {
            childFound = true;
            return;
          }
        });
      }
      // return childFound; // removing unsafe child validation for 11/3 demo
      return childFound && (child.issafe === UNSAFE || this.isServiceCase()); //  comment if unsafe child only needs to remove
    });

    return this.childList;
  }

  loadChildRemoval(info) {
    this.childRemovalInfo = info;
  }

  getPersonsList() {

    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: this.getRequestParam()
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );
  }

  getChildRemoval(groupByPerson: number = 0) {
    const requestData = { ...this.getRequestParam(), ...{ isgroup: groupByPerson } };
    return this._commonService
      .getSingle(
        {
          where: requestData,
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .GetChildRemovalList + '?filter'
      );
  }

  isServiceCase() {
    const isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    if (isServiceCase) {
      return true;
    } else {
      return false;
    }
  }

  getRequestParam() {

    this.intakeserviceid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    let requestData;
    if (this.isServiceCase()) {
      requestData = {
        objectid: this.intakeserviceid,
        objecttypekey: 'servicecase'
      };

    } else {
      requestData = { intakeserviceid: this.intakeserviceid };
    }
    return requestData;
  }

  getAssessMents(tempId) {
   {
     // e.g tempId : 'SAFE-C'
    return this._commonService
    .getSingle(
      {

        limit: 10,
        order: 'desc',
        page: 1,
        count: -1,
        where: { 'objectid' : this.intakeserviceid,
        // this.intakeserviceid,
         templatename : tempId } ,
        method: 'get'
      },
      'serviceplanchild/getAssessment?filter'
    );
  }
 }

  getassessment() {
    let inputRequest: Object;
        if (this.isServiceCase) {
            inputRequest = {
                objecttypekey: 'servicecase',
                objectid: this.intakeserviceid
                // objectid: '1ab13583-6d94-4929-92e0-09723df839f3'
            };
        } else {
            inputRequest = {
                servicerequestid: this.intakeserviceid,
                categoryid: null,
                subcategoryid: null,
                targetid: null,
                assessmentstatus: null
            };
        }
        return this._commonService
        .getPagedArrayList(
            new PaginationRequest({
                page: 1,
                limit: 25,
                where: inputRequest,
                method: 'get'
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.ListAssessment + '?filter'
        );
  }

  updatePersonListWithChildRemoval() {
    if (this.childRemovalInfo && this.childRemovalInfo.length) {
      this.childRemovalInfo.forEach(removalInfo => {
        this.personList.forEach(person => {
          if (person.personid === removalInfo.personid) {
            // person.isRemoved = true;
            // if (person.servicecaseid) {
            //   person.removalStatus = 'Approved';
            // } else {
            //   person.removalStatus = 'Review';
            // }
            person.removalStatus = removalInfo.approvalstatus ? removalInfo.approvalstatus : 'Draft';

            person.intakeservreqchildremovalid = removalInfo.intakeservreqchildremovalid;
            person.removalInfo = removalInfo;
          }
        });
      });

    }
  }

  getSocialHistory(intakeservicerequestactorid, removalDate) {
    return this._commonHttpService.getArrayList(
      {
        where: {
          activeflag: '1',
          referenceid : intakeservicerequestactorid,
          logType: 'SHIST',
          removalDate: removalDate, // new Date('04/01/2019'),
          rangeType: '1'
        },
        method: 'get',
        nolimit: true
    },
    'auditlog/list?filter');
  }

  searchCase(caseId, caseType, url) {
    return this._commonService
    .getSingle(
      {

        limit: 10,
        order: 'desc',
        page: 1,
        count: -1,
        where: { servicerequestnumber : caseId, actiontype : caseType } ,
        method: 'get'
      },
      url
    );
  }

  getCaseReview(caseid) {
    return this._commonHttpService.getArrayList(
      {
        where: {
          caseid: caseid
        },
        method: 'get',
        nolimit: true
    },
    'casereview/fetchCasereviewbycaseid?filter');
  }

  getRecommendation(casereviewid) {
    return this._commonHttpService.getArrayList(
      {
        where: {
          casereviewid: casereviewid
        },
        method: 'get',
        nolimit: true
    },
    'Reviewrecommendations/fetchrecommendationsbycasereviewid?filter');
  }

  getReviewType() {
    return this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true
    },
    'Reviewrecommendations/getReviewtypeList?filter');
  }

  saveCaseReview() {
    return this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true
    },
    'http://localhost:3000/api/casereview/addupdate?filter');
  }

  getRelationshipOfPerson(personid: string) {
    const caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 30,
          method: 'get',
          where:  {objectid: caseID, objecttypekey: 'servicecase', personid : personid}
        }),
        'People/getallpersonrelationbyprovidedpersonid?filter'
      );
  }

}
