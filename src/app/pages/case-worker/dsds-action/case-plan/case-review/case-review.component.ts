import { Component, OnInit } from '@angular/core';
import { CasePlanService } from '../case-plan.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DataStoreService, AlertService, SessionStorageService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
  selector: 'case-review',
  templateUrl: './case-review.component.html',
  styleUrls: ['./case-review.component.scss']
})
export class CaseReviewComponent implements OnInit {
  casePlanFormGroup:  FormGroup;
  recommendationFormGroup:  FormGroup;
  reviewTypeDropDown: any;
  isReviewData: boolean;
  selectedChild: any;
  caseID: any;
  recommendationData: any;
  actionType: any;
  isClosed = false;
  constructor(
    private _casePlanService: CasePlanService,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _alert: AlertService,
    private storage: SessionStorageService
  ) { }

  ngOnInit() {
    this.isReviewData = false;
    this.selectedChild = this._dataStoreService.getData('SELECTED_CHILD_DATA');
    this.caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.initForm();
    this.getReviewTypeDD();
    this.getCaseReviewData();

    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

  initForm() {
    console.log('Selected Child Info' , this.selectedChild);
    console.log('Case Id', this.caseID);
    this.casePlanFormGroup = this._formBuilder.group({
      personid: [null],
      caseid: [null],
      casereviewid: [null],
      nameofChild: [null],
      otherpanelmembers: [null],
      reviewtypekey: [null],
      reviewdate: [null],
      reviewtime: [null],
      nextreviewdate: [null],
    });
    this.casePlanFormGroup.patchValue({
      personid : this.selectedChild && this.selectedChild.personid ? this.selectedChild.personid : null,
      nameofChild : this.selectedChild && this.selectedChild.fullname ? this.selectedChild.fullname : null,
      caseid: this.caseID ? this.caseID : null
    });
    this.recommendationFormGroup = this._formBuilder.group({
      reviewrecommendationid: [null],
      casereviewid: [null],
      recommendation: [null],
      caseid: [null],
      goalcompletedate: [null],
      workerresponse: [null],
      followup: [null],
      followupdate: [null],
      recommendationstatustypekey: [null],
    });
    this.recommendationFormGroup.patchValue({
       caseid: this.caseID ? this.caseID : null
    });
  }



  getCaseReviewData() {
    this._casePlanService.getCaseReview(this.caseID).subscribe( data => {
      console.log(data);
      if(data && data.length) {
        this.getRecommendationData(data[0].casereviewid);
        this.isReviewData = true;
        this.casePlanFormGroup.patchValue(data[0]);
      }

    });
  }

  getRecommendationData(reviewId) {
    this._casePlanService.getRecommendation(reviewId).subscribe( data => {
      console.log(data);
      if(data && data.length) {
      this.recommendationData = data;
      }
      //  this.recommendationFormGroup.patchValue(data);
    });
  }

  getReviewTypeDD() {
    this._casePlanService.getReviewType().subscribe( data => {
      this.reviewTypeDropDown = data;
    });
  }



  saveorUpdateCaseReview() {
    const caseReviewData  = this.casePlanFormGroup.getRawValue();
    console.log('Case review :', caseReviewData);
    this._commonHttpService.create(caseReviewData, 'casereview/addupdate').subscribe( data => {
        this._alert.success('Case Review saved Successfully');
        this.getCaseReviewData();
    });
  }

  closeModal() {
    (<any>$('#add-recommend')).modal('hide');
    (<any>$('#add-staff')).modal('hide');
    this.resetRecommendForm();
  }

  saveorUpdateRecommendation() {
    const recommendationData  = this.recommendationFormGroup.getRawValue();
    recommendationData.casereviewid = this.casePlanFormGroup.getRawValue().casereviewid;
    console.log('Case recommend  review :', recommendationData);
    this._commonHttpService.create(recommendationData, 'Reviewrecommendations/addupdate').subscribe( data => {
        (<any>$('#add-recommend')).modal('hide');
        this._alert.success('Recommendation saved Successfully');
        this.resetRecommendForm();
        this.getCaseReviewData();
    });
  }
  resetRecommendForm() {
    this.recommendationFormGroup.enable();
    this.recommendationFormGroup.reset();
    this.actionType = null;
  }
  openRecommendation(recommend, action) {
    this.recommendationFormGroup.enable();
    this.recommendationFormGroup.reset();
    this.recommendationFormGroup.patchValue(recommend);
    this.actionType = action;
    if(action === 'view') {
      this.recommendationFormGroup.disable();
    }
  }

}
