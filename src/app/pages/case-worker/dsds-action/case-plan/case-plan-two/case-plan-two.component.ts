import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonHttpService, DataStoreService, AuthService, AlertService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { AppConstants } from '../../../../../@core/common/constants';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { MatRadioChange, MatHorizontalStepper } from '@angular/material';
import { AppUser } from '../../../../../@core/entities/authDataModel';


@Component({
  selector: 'case-plan-two',
  templateUrl: './case-plan-two.component.html',
  styleUrls: ['./case-plan-two.component.scss']
})
export class CasePlanTwoComponent implements OnInit {
  @ViewChild('stepper') stepper: MatHorizontalStepper;
  currentDataStore: any;
  selectedChildData: any;
  intakeservicerequestactorids = [];

  isSupervisor: boolean;
  snapshotVersions = [];

  selectedChildRemovalDate: Date;

  //Periods for review
  periodList: any[] = [];

  //Filtering
  selectedFilterType: string;
  selectedPeriod: any;

  snapshotFilterFormGroup: FormGroup;
  selectedSnapshotVersion: any;
  casePlanQuestionsFormGroup: FormGroup;
  casePlanQuestionsFormGroupII: FormGroup;
  casePlanQuestionsFormGroupIII: FormGroup;
  
  //Routing and approval
  selectedPerson: any;
  getUsersList = [];
  approvalProcess: string;
  user: AppUser;
  reviewComments: any;
  reviewFormGroup: FormGroup;

  id = '';
  daNumber = '';

  //Editable case plan
  selectedVersion: any;
  expandAll = true;

  caseplan2data = {};


  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _alertservice: AlertService
  ) {
    this.currentDataStore = this._dataStoreService.getCurrentStore();
  }
  
  ngOnInit() {
    this.selectedChildData = this.currentDataStore['SELECTED_CHILD_DATA'];
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this.user = this._authService.getCurrentUser();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);

    this.initSnapshotFilterFormGroup();
    this.cPlanQuestionsFormGroup();
    this.initReviewFormGroup();
    this.getActorIds();
    this.getChildRemovalDate();
    this.getHist();
  }

  getActorIds() {
    if (this.selectedChildData.roles && Array.isArray(this.selectedChildData.roles)) {
      this.selectedChildData.roles.forEach(role => {
        this.intakeservicerequestactorids.push(role.intakeservicerequestactorid)
      });
    }
  }

  ngAfterViewInit() {
      this.stepper._getIndicatorType = () => 'number';
  }
   
  getChildRemovalDate() {
    this.selectedChildRemovalDate = new Date(this.selectedChildData.removalInfo.removaldate);
  }

  generatecaseplansnapshot() {
    this._commonHttpService.getArrayList(
      {
        personid: this.selectedChildData.personid,
        servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID),
        intakeservicerequestactorid: this.selectedChildData.intakeservicerequestactorid,
        intakeservicerequestactorids: this.intakeservicerequestactorids,
        periodstartdate: this.getVersionFilterStartDate(),
        periodenddate: this.getVersionFilterEndDate(),
        cplanquestions: this.casePlanQuestionsFormGroup.getRawValue(),
        method: 'post',
        nolimit: true
      },
      'caseplan/generatecaseplansnapshot').subscribe(data => {
        this.getHist();
      });
  }

  getHist() {
    this._commonHttpService
    .getArrayList(
      new PaginationRequest({
        page: 1,
        limit: 20,
        method: 'get',
        order: 'insertedon desc',
        where: {
         objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID),
         objecttype: 'CPLAN2',
         personid: this.selectedChildData.personid
         }
      }),
      'snapshothist' + '?filter'
    ).subscribe(
      response => {
        this.snapshotVersions = response;
     });
  }


  casePlan2Print(item, index) {
    const inputRequest = {
      'caseplan2data': (Array.isArray(item.snapshotdata) && item.snapshotdata.length) ? item.snapshotdata[0] : item.snapshotdata,
      'isheaderrequired': false,
  };
  const payload = {
    method: 'post',
    count: -1,
    page: 1,
    limit: 20,
    where: inputRequest,
    documntkey: [
      'oohome'
  ],
  };
  this._commonHttpService.create(payload, 'caseplan/getReportCaseplan').subscribe(
    response => {
      console.log(response, 'pdf response');
      setTimeout(() => window.open(response.data.documentpath), 2000);
   });
    // this.backupService = this.selectedService ? this.selectedService : null;
    // this.selectedService = item;
    // (<any>$('#servicePlanOohPrint')).modal('show');
  }
  
  // Case Plan Question 
  cPlanQuestionsFormGroup() {
    this.casePlanQuestionsFormGroup = this._formBuilder.group({
      // safeAppropriateCare: [null],
      discussVisitationPlan: [null],
      currentRelationship: [null],
      parentChildInteraction: [null],
      servicesForStability: [null],
      refMadeDate: [null],
      physicalOrMentaldiagnosis: [null],
      episodesOfHomelessness: [null],
      lengthOfHomeless: [null],
      outOfHomeService: [null],
      facilitatePermanencySelfSufficiency: [null],
      specifyTheNeedsAndServices: [null],
      utilizationOrNonUtilization: [null],
      flexibleFundsBeenExpended_purpose: [null],
      flexibleFundsBeenExpended_Amount: [null],
      healthPassport: [null],
      reimbursement: [null],
      discussionOfVisitation: [null],
      emotionalSupportGuidance: [null],
      otherReasons: [null],
      dayCare: [null],
      transportation: [null],
      financialSupport: [null],
      specialTraining: [null],
      discussionOfChildsNeeds: [null],
      respiteCare: [null],
      writeOtherReson: [null],
      diagnosedAxisOne:false,
      diagnosedAxisTwo:false,
      diagnosedAxisThree:false,
      diagnosedAxisFour:false,
      diagnosedAxisFive:false,
      diagnosedAxisOneComment:'',
      diagnosedAxisTwoComment:'',
      diagnosedAxisThreeComment:'',
      diagnosedAxisFourComment:'',
      diagnosedAxisFiveComment:'',
      //curentplacetolive:null,
      //isInClosedProximity:[null],
      //isInClosedProximityExpln:[null],
      //meetingSafetyNeedsExpln:[null],
      //isTreatmentFosterCare:[null],
      //treatmentteameetingdate:[null],
      //lasttreatmentteameetingdate:[null],
      lastprogressreportdate:[null],
      sixMonthsPlacementExpln:[null],
      servicecourtorderdetails:[null],
      visitationcourtorderdetails:[null],
      //courtOrdersExpln:[null],
      //primarypermanencytype:[null],
      //concurrentpermanencytype:[null],
      //establisheddate:[null],
      facilitatePermanencySelfSufficiencyStartDate:[null],
      facilitatePermanencySelfSufficiencyEndDate:[null],
      //permToPermExpln:[null],
      faceToFaceVisit:[null],
      isChildCoComitted:[null],
      episodeExperience:[null],
      episodeExperienceExplain:[null],
      juvenileCourtInvolvement:[null],
      discussServices:[null],
      faceToFaceVisitContactDate:[null],
      faceToFaceVisitAuthoredDate:[null],
      //safeAndCareExpln:[null],
      addressChildNeeds:[null],
      //assessmentPeriodExpln:[null],
      //lifebookExpln:[null],
      //serviceAgreementExpln:[null],
      //parentsInvolvementExpln:[null],
      reasonTPRPetition:[null],
      parentName:[null],
      childInAndOutOfHome:[null],
      tprPetitionDate:[null],
      voluntarySignatureDateMother:[null],
      voluntarySignatureDateFather:[null],
      tprGrantedDate:[null],
      tprDeniedDate:[null],
      tprAppealDateMother:[null],
      tprAppealDateFather:[null],
      tprAppealDecisionDateFather:[null],
      tprAppealDecisionDateMother:[null],
      supportiveServicesRequired:[null],
      passportRecordConfirmation:[null],
      conditionsAndAllergies:[null],
      passportRecordConfirmedDate:[null],
      childEnrolledInFiveDays:[null],
      schoolProximityToPrior:[null],
      placementChangedSchool:[null],
      explainChangeOfSchool:[null],
      discussChildCurrentEducationalSetting:[null],
      educationalServicesReceiving:[null],
      isChildEnrolledInSpecialProgram:[null],
      childCurrentReportCardCopy:[null],
      describeStrengthsAndNeeds:[null],
      eighteenToTwentyOneChildStatus:[null],individualEducationProgram:[null],
      iepDated:[null],
      educationalParentSurrogateName:[null],
      educationalParentSurrogateAddress:[null],
      educationalParentSurrogateRequested:[null],
      atOrNearAgeLevel:[null],
      behavioralProblemsBlockD:[null],
      problemsWithPeersBlockD:[null],
      educationalParentSurrogateStrengths:[null],
      educationalParentSurrogateWeakness:[null],
      behavioralProblemsBlockE:[null],
      problemsWithPeersBlockE:[null],
      schoolSchedule:[null],
      schoolAdjustmentComments:[null],
      extraCurricularActivities:[null],
      educationProgramGoal:[null],
      blockEComments:[null],
    });
    this.casePlanQuestionsFormGroupII = this._formBuilder.group({
    });
  }
  
  //Snopshot version work
  initSnapshotFilterFormGroup() {
    this.snapshotFilterFormGroup = this._formBuilder.group({
      personlist: [null],
      startdate: [null],
      enddate: [null]
    });
  }

  initReviewFormGroup() {
    this.reviewFormGroup = this._formBuilder.group({
      comments: [null]
    });
  }


  setDefaultSnapshotFilterValues() {
    this.selectedFilterType = 'PERIOD_RANGE';
    this.snapshotFilterFormGroup.patchValue({
      // personlist: [this.cp1Data.personid],
      // startdate: this.selectedServicePlan.effectivedate,
      // enddate: this.selectedServicePlan.targetenddate
      startdate: new Date(),
      enddate: new Date()
    });
  }

  setSelectedSnapshotVersion(item) {
    this.selectedSnapshotVersion = item;
  }

  initPeriod(key, periodstart, periodend, timeframe, numdays) {
    const item = {
      'fromdate': this.getPeriodStartDate(periodstart),
      'todate': this.getPeriodEndDate(periodend),
      'key': key,
      'periodstart': periodstart,
      'periodend': periodend,
      'numdays': numdays,
      'timeframe': timeframe,
      'isperiodcompleted': this.isPeriodVersionApproved(periodstart, periodend)
    };
    return item;
  }

  initPeriodList() {
    this.periodList = [
      this.initPeriod(1, 0, 60, '0-2', 60),
      this.initPeriod(2, 60, 180, '2-6', 120),
      this.initPeriod(3, 180, 360, '6-12', 180),
      this.initPeriod(4, 360, 540, '12-18', 180),
      this.initPeriod(5, 540, 720, '18-24', 180),
      this.initPeriod(6, 720, 900, '24-30', 180)
    ];
    console.log("SIMAR =>", this.periodList)
  }

  getPeriodStartDate(periodstart) {
    let date = new Date(this.selectedChildRemovalDate);
    date.setDate(this.selectedChildRemovalDate.getDate() + periodstart);
    return date;
  }

  getPeriodEndDate(periodend) {
    let date = new Date(this.selectedChildRemovalDate);
    date.setDate(this.selectedChildRemovalDate.getDate() + periodend);
    return date;
  }

  isPeriodVersionApproved(periodstart, periodend) {
    let iscompleted = false;
    this.snapshotVersions.forEach( item => {
      if (new Date(item.fromdate).getDate() === this.getPeriodStartDate(periodstart).getDate() &&
          new Date(item.todate).getDate() === this.getPeriodEndDate(periodend).getDate()) {
            console.log("Period done", periodstart, periodend, iscompleted)
        iscompleted = true;
      }
    })
    console.log("Period NOT done", periodstart, periodend, iscompleted)
    return iscompleted;
  }


  /**
   * Snapshot work 
   * */
  getVersionFilterStartDate() {
    if (this.selectedFilterType === 'DATE_RANGE') {
      return this.snapshotFilterFormGroup.get('startdate').value;
    } else 
    if (this.selectedFilterType === 'PERIOD_RANGE') {
      console.log(this.selectedPeriod['fromdate'])
      return this.selectedPeriod['fromdate'];
    }
  }
  
  getVersionFilterEndDate() {
    if (this.selectedFilterType === 'DATE_RANGE') {
      return this.snapshotFilterFormGroup.get('enddate').value;
    } else 
    if (this.selectedFilterType === 'PERIOD_RANGE') {
      console.log(this.selectedPeriod['todate'])
      return this.selectedPeriod['todate'];
    }
  }

  //Filtering
  resetSelectedFilterType() {
    this.selectedFilterType = null;
  }

  changeSelectedFilterType(event: MatRadioChange) {
    console.log("DO I NEED THIS ?");
  }

  selectPeriod(item) {
    this.selectedPeriod = item;
  }

   //Snapshot version approval and routing

   getRoutingUser(appovalValue) {
    this.approvalProcess = appovalValue;

    this.getUsersList = [];
    if (appovalValue === 'Pending') {
      this._commonHttpService
      .getPagedArrayList(
          new PaginationRequest({
              where: { appevent:  'SPLAN' },
              method: 'post'
          }),
          'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
          this.getUsersList = result.data;
          this.getUsersList = this.getUsersList.filter(
              users => users.userid !== this._authService.getCurrentUser().user.securityusersid
          );
      });
    }  else {
      // this.assignNewUser();
      (<any>$('#confirm-decision')).modal('show');
    }
  }
  
  selectPerson(row) {
    this.selectedPerson = row;
  }

  assignNewUser() {
      
    // Instead of the service plan, the snapshot version is sent for approval 
    // selectedService.approvalstatustypekey = this.approvalProcess;
    
    this.selectedSnapshotVersion.approvalstatus = this.approvalProcess;

    const payload = {
      eventcode: 'CPLAN2',
      tosecurityusersid: this.selectedPerson ? this.selectedPerson.userid : '',
      objectid: this.id,
      serviceNumber: this.daNumber,
      approvalstatustypekey: this.approvalProcess
    };
    this._commonHttpService.create(
        payload,
      'caseplan/caseplanrouting'
    ).subscribe(
      (response) => {
        this._alertservice.success('Decision submitted successful!');
        // if (this.approvalProcess && this.approvalProcess.toLowerCase() === 'approved') {
        //       this.saveSnapshothist();
        // }
        this.updateSnapshotVersionStatus();
        (<any>$('#intake-caseassignnewX')).modal('hide');
      },
      (error) => {
        this._alertservice.error('Unable to submit decision!');
      });
  }

  updateSnapshotVersionStatus() {
    const payload = {};
    payload['id'] = this.selectedSnapshotVersion.id;
    payload['approvalstatus'] = this.approvalProcess;
    payload['comments'] = this.reviewFormGroup.get('comments').value

    this._commonHttpService.patch(
      this.selectedSnapshotVersion.id,
      payload,
      'snapshothist'
    ).subscribe(
      response => {
        this.getHist();
        this._alertservice.success('Decision submitted successfully!');
      },
      error => {
        this._alertservice.error('Error in submitting decision!');
      }
    );
  }

  confirmDecision() {
    this.assignNewUser();
    (<any>$('#confirm-decision')).modal('hide');
  }

  cancelDecision() {
    this.reviewFormGroup.reset();
    (<any>$('#confirm-decision')).modal('hide');
  }

  showReviewComments(item: any) {
    this.reviewComments = item.comments ? item.comments : 'No comments found!';
  }

  /**
   * Editable case plan
   */

  selectVersion(item) {
    this.selectedVersion = item;
    if ((Array.isArray(item.snapshotdata) && item.snapshotdata.length)) {
      this.caseplan2data = item.snapshotdata[0];
      //Patch the formgroup with the cplanquestion data from the response data already answered
      this.casePlanQuestionsFormGroup.patchValue(item.snapshotdata[0].cplanquestions);
    }
    // else if (item.snapshotdata) {
    //   this.caseplan2data = item.snapshotdata;
    //   //Patch the formgroup with the cplanquestion data from the response data already answered
    //   this.casePlanQuestionsFormGroup.patchValue(item.snapshotdata.cplanquestions);
    // }
  }

  toggleAll() {
    this.expandAll = true;
    console.log('setAllPanelOpen');
    (<any>$(' .panel-collapse:not(".in")')).collapse('show');
  }

  toggleAll1() {
    this.expandAll = false;
    console.log('setAllPanelclose');
    (<any>$(' .panel-collapse.in')).collapse('hide');
  }

  getPatchDataPayload() {
    let data = []
    this.caseplan2data['cplanquestions'] = this.casePlanQuestionsFormGroup.getRawValue();
    data[0] = this.caseplan2data;
    return data;
  }

   // Save as draft and update once the assessment record is created
   patchSnapshotData(item) {
    let payload = {};
    payload['id'] = this.selectedVersion.id;
    payload['snapshotdata'] = this.getPatchDataPayload();
    this._commonHttpService.patch(
      this.selectedVersion.id,
      payload,
      'snapshothist'
    ).subscribe(
      response => {
        this.getHist();
        this._alertservice.success('Data saved successfully');
      },
      error => {
        this._alertservice.error('Error in saving data');
      }
    );
  }

  /**
   * Regenerate case plan snapshot
   * we will recapture all the application data, 
   * basically re-hydrate the existing snapshot with latest data
   * but preserve the editable questions that have been answered
   * -- All editable data goes in cplanquestion
   */
  regeneratecaseplansnapshot() {
    this._commonHttpService.getArrayList(
      {
        personid: this.selectedChildData.personid,
        servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID),
        intakeservicerequestactorid: this.selectedChildData.intakeservicerequestactorid,
        intakeservicerequestactorids: this.intakeservicerequestactorids,
        periodstartdate: this.getVersionFilterStartDate(),
        periodenddate: this.getVersionFilterEndDate(),
        // Regenerate relevant stuff
        cplanquestions: this.casePlanQuestionsFormGroup.getRawValue(),
        regeneratecaseplan: true,
        id: this.selectedVersion.id,
        method: 'post',
        nolimit: true
      },
      'caseplan/generatecaseplansnapshot').subscribe(data => {
        this.getHist();
      });
  }

}