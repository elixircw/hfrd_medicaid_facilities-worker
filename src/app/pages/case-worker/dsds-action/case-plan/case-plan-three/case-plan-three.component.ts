import { Component, OnInit } from '@angular/core';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { CommonHttpService, DataStoreService, AuthService, AlertService, CommonDropdownsService, SessionStorageService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ServicePlanService, ServicePlanFocus } from '../../../_entities/caseworker.data.model';
import * as jsPDF from 'jspdf';
import { MatRadioChange } from '@angular/material';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AppConstants } from '../../../../../@core/common/constants';
import * as _ from 'lodash';

@Component({
  selector: 'case-plan-three',
  templateUrl: './case-plan-three.component.html',
  styleUrls: ['./case-plan-three.component.scss']
})
export class CasePlanThreeComponent implements OnInit {
  store: any;
  cp1Data: any;
  personList: any[];
  personListName: any;
  personDataList: any[] = [];
  visitationplans: any[] = [];
  id = '';
  daNumber = '';
  isSupervisor = false;

  //Routing and approval
  selectedPerson: any;
  getUsersList = [];


  approvalProcess: string;
  user: AppUser;
  reviewComments: any;
  reviewFormGroup: FormGroup;


  periodList: any[] = [];
  serviceplanslist: ServicePlanService[] = [];
  selectedServicePlan: ServicePlanService;

  //Snapshot versions for the selected service plan
  snapshotVersions = [];
  snapshotFilterFormGroup: FormGroup;
  selectedSnapshotVersion: any;

  //Filtering
  selectedFilterType: string;
  selectedPeriod: any;

  count: any;
  countobj: number;

  //Child data
  selectedChildData: any;
  selectedChildRemovalDate: Date;
  isClosed = false;

  //visitation plan reference values
  childandvisitortransportation = [];
  frequencyofplannedvisits = [];
  lengthofplannedvisit = [];

  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];

  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _alertservice: AlertService,
    private _commonDropdownService: CommonDropdownsService,
    private storage: SessionStorageService
  ) { 
    this.store = this._dataStoreService.getCurrentStore();
    this.cp1Data = this.store['CP1DATA'];
    this.selectedChildData = this.store['SELECTED_CHILD_DATA'];
  }

  ngOnInit() {
    this.user = this._authService.getCurrentUser();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);

    this.initSnapshotFilterFormGroup();
    this.initReviewFormGroup();

    this.getInvolvedPerson();
    this.getServicePlans();
    this.getReferenceDropdowns();
    this.getChildRemovalDate();

    const da_status = this.storage.getItem('da_status');
           if (da_status) {
            if (da_status === 'Closed' || da_status === 'Completed') {
                this.isClosed = true;
            } else {
                this.isClosed = false;
            }
        }
  }

  getChildRemovalDate() {
    this.selectedChildRemovalDate = new Date(this.selectedChildData.removalInfo.removaldate);
  }

  getPersonName(id) {
    if (this.personList && Array.isArray(this.personList)) {
      const person = this.personList.find(item => item.personid === id);
      const name = (person) ? (person.firstname + ' ' + person.lastname) : '';
      return name;
    }
    return '';
  }


  // getServicePlans() {
  //   const payload = {
  //     method: 'get',
  //     where: {
  //       caseid : this.id,
  //       // approvalstatuskey: 'approved'
  //     }
  //   };
  //   this._commonHttpService.getArrayList(payload, 'serviceplan/listbyallrelation?filter').subscribe(
  //     response => {
  //       this.list = response.filter((item) => 
  //         item.approvalstatustypekey === 'approved'
  //       );

  //       if (this.selectedService) {
  //         this.selectedService = response.find(item => item.serviceplanid === this.selectedService.serviceplanid);
  //         if (this.selectedFocus) {
  //           this.selectedFocus = this.selectedService.serviceplanfocus.find(item => item.serviceplanfocusid === this.selectedFocus.serviceplanfocusid);
  //         }
  //       }
  //     });
  //     this.list = this.list.filter((sp) => {
  //       sp.serviceplanfocus.forEach((focus) => {
  //         focus.serviceplanaction.forEach((action) => {
  //           action.serviceplanpersoninvolved.forEach((person) =>
  //             person.personinvolved == this.cp1Data.personid
  //           );
  //         });
  //       })
  //     }
  //   );
  // }

  getServicePlans() {
    const payload = {
      method: 'get',
      where: {
        caseid : this.id
      }
    };
    this._commonHttpService.getArrayList(payload, 'serviceplan/listbyallrelation?filter').subscribe(
      response => {
        if (this.user.role.name !== 'field') {
          this.serviceplanslist = response.filter((item) =>  item.approvalstatustypekey !== null && item.approvalstatustypekey !== '');
        } else {
          this.serviceplanslist = response;
        }

        // @Simar - Filter the service plans retrieved to only the ones with the person involved
      //   this.list = this.list.filter((sp) => {
      //     sp.serviceplanfocus.forEach((focus) => {
      //       focus.serviceplanaction.forEach((action) => {
      //         action.serviceplanpersoninvolved.forEach((person) =>
      //           person.personinvolved == this.cp1Data.personid
      //         );
      //       });
      //     })
      //   }
      // );


    });
  }

  selectServicePlan(item) {
    this.selectedServicePlan = item;
    this.getVisitationPlans();
    
    //Get the corresponding snapshot versions of the selected service plan
    this.getSnapshotHist();
  }

  initReviewFormGroup() {
    this.reviewFormGroup = this._formBuilder.group({
      comments: [null]
    });
  }



  //Snopshot version work
  initSnapshotFilterFormGroup() {
    this.snapshotFilterFormGroup = this._formBuilder.group({
      personlist: [null],
      startdate: [null],
      enddate: [null]
    });
  }

  setDefaultSnapshotFilterValues() {
    this.selectedFilterType = 'PERIOD_RANGE';
    this.snapshotFilterFormGroup.patchValue({
      // personlist: [this.cp1Data.personid],
      startdate: this.selectedServicePlan.effectivedate,
      enddate: this.selectedServicePlan.targetenddate
    });
  }

  setSelectedSnapshotVersion(item) {
    this.selectedSnapshotVersion = item;
  }



  getPeriodStartDate(periodstart) {
    let date = new Date(this.selectedChildRemovalDate);
    date.setDate(this.selectedChildRemovalDate.getDate() + periodstart);
    return date;
  }

  getPeriodEndDate(periodend) {
    let date = new Date(this.selectedChildRemovalDate);
    date.setDate(this.selectedChildRemovalDate.getDate() + periodend);
    return date;
  }

  isPeriodVersionApproved(periodstart, periodend) {
    let iscompleted = false;
    this.snapshotVersions.forEach( item => {
      if (new Date(item.fromdate).getDate() === this.getPeriodStartDate(periodstart).getDate() &&
          new Date(item.todate).getDate() === this.getPeriodEndDate(periodend).getDate()) {
            console.log("Period done", periodstart, periodend, iscompleted)
        iscompleted = true;
      }
    })
    console.log("Period NOT done", periodstart, periodend, iscompleted)
    return iscompleted;
  }

  initPeriod(key, periodstart, periodend, timeframe, numdays) {
    const item = {
      'fromdate': this.getPeriodStartDate(periodstart),
      'todate': this.getPeriodEndDate(periodend),
      'key': key,
      'periodstart': periodstart,
      'periodend': periodend,
      'numdays': numdays,
      'timeframe': timeframe,
      'isperiodcompleted': this.isPeriodVersionApproved(periodstart, periodend)
    };
    return item;
  }

  initPeriodList() {
    this.periodList = [
      this.initPeriod(1, 0, 60, '0-2', 60),
      this.initPeriod(2, 60, 180, '2-6', 120),
      this.initPeriod(3, 180, 360, '6-12', 180),
      this.initPeriod(4, 360, 540, '12-18', 180),
      this.initPeriod(5, 540, 720, '18-24', 180),
      this.initPeriod(6, 720, 900, '24-30', 180)
    ];
  }

  getSnapshotHist() {
    // this.selectedService = item;
    this.getHist();
    // (<any>$('#splan-snapshot-version')).modal('show');
  }

  getHist() {
    this._commonHttpService
    .getArrayList(
      new PaginationRequest({
        page: 1,
        limit: 20,
        method: 'get',
        where: {
         objectid: this.selectedServicePlan.serviceplanid,
         objecttype: 'CPLAN3',
         personid: this.selectedChildData.personid
         }
      }),
      'snapshothist' + '?filter'
    ).subscribe(
      response => {
        this.snapshotVersions = response;
     });
  }


  //Snapshot version approval and routing

  getRoutingUser(appovalValue) {
    this.approvalProcess = appovalValue;

    this.getUsersList = [];
    if (appovalValue === 'Pending') {
      this._commonHttpService
      .getPagedArrayList(
          new PaginationRequest({
              where: { appevent:  'SPLAN' },
              method: 'post'
          }),
          'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
          this.getUsersList = result.data;
          this.getUsersList = this.getUsersList.filter(
              users => users.userid !== this._authService.getCurrentUser().user.securityusersid
          );
      });
    }  else {
      // this.assignNewUser();
      (<any>$('#confirm-decision')).modal('show');
    }
  }
  
  selectPerson(row) {
    this.selectedPerson = row;
  }

  assignNewUser() {
      
    // Instead of the service plan, the snapshot version is sent for approval 
    // selectedService.approvalstatustypekey = this.approvalProcess;
    
    this.selectedSnapshotVersion.approvalstatus = this.approvalProcess;

    const payload = {
      eventcode: 'CPLAN3',
      tosecurityusersid: this.selectedPerson ? this.selectedPerson.userid : '',
      objectid: this.id,
      serviceNumber: this.daNumber,
      approvalstatustypekey: this.approvalProcess,
      serviceplanid: this.selectedServicePlan.serviceplanid
    };
    this._commonHttpService.create(
        payload,
      'serviceplan/serviceplanrouting'
    ).subscribe(
      (response) => {
        this._alertservice.success('Decision submitted successful!');
        // if (this.approvalProcess && this.approvalProcess.toLowerCase() === 'approved') {
        //       this.saveSnapshothist();
        // }
        this.updateSnapshotVersionStatus();
        (<any>$('#intake-caseassignnewX')).modal('hide');
      },
      (error) => {
        this._alertservice.error('Unable to submit decision!');
      });
  }

  updateSnapshotVersionStatus() {
    const payload = {};
    payload['id'] = this.selectedSnapshotVersion.id;
    payload['approvalstatus'] = this.approvalProcess;
    payload['comments'] = this.reviewFormGroup.get('comments').value

    this._commonHttpService.patch(
      this.selectedSnapshotVersion.id,
      payload,
      'snapshothist'
    ).subscribe(
      response => {
        this.getSnapshotHist();
        this._alertservice.success('Service plan version decision submitted successfully!');
      },
      error => {
        this._alertservice.error('Error in submitting service plan version decision!');
      }
    );
  }

  confirmDecision() {
    this.assignNewUser();
    (<any>$('#confirm-decision')).modal('hide');
  }

  cancelDecision() {
    this.reviewFormGroup.reset();
    (<any>$('#confirm-decision')).modal('hide');
  }

  showReviewComments(item: any) {
    this.reviewComments = item.comments ? item.comments : 'No comments found!';
  }

  //Get list of persons involved -- don't think we need this here
  getInvolvedPerson() {
    let inputRequest = {};
    const isServiceCase = this._dataStoreService.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    if (isServiceCase) {
      inputRequest = {
        objectid: this.id,
        objecttypekey: 'servicecase'
      };
    } else {
      inputRequest = {
        intakeserviceid: this.id
      };
    }
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest
    };
    this._commonHttpService.getPagedArrayList(payload, 'People/getpersondetail?filter').subscribe(
      response => {
        console.log(response);
        this.personList = response.data;

        if (response.data && response.data.length) {
          response.data.forEach(person => {
            this.personDataList.push({'name': person.firstname + ' ' + person.lastname});
            // person.roles.forEach(role => {
            //   if (role.intakeservicerequestpersontypekey === 'CHILD') {
            //     if (this.childrenHeader !== '') {
            //       this.childrenHeader += ',';
            //     }
            //     this.childrenHeader = person.firstname + ' ' + person.lastname;
            //   }
            //   if (role.intakeservicerequestpersontypekey === 'LG') {
            //     this.legalGuardian = person.firstname + ' ' + person.lastname;
            //   }
            // });
          });
          this.personDataList.push({'name': this.user.user.userprofile.displayname});
          if (this.personDataList && this.personDataList.length) {
          this.personListName = this.personDataList[0].name;
          }
        }
      });
  }

  //Filtering
  resetSelectedFilterType() {
    this.selectedFilterType = null;
  }

  changeSelectedFilterType(event: MatRadioChange) {
    console.log("DO I NEED THIS ?");
  }

  selectPeriod(item) {
    this.selectedPeriod = item;
  }


  /**
   * Snapshot work 
   * */
  getVersionFilterStartDate() {
    if (this.selectedFilterType === 'DATE_RANGE') {
      return this.snapshotFilterFormGroup.get('startdate').value;
    } else 
    if (this.selectedFilterType === 'PERIOD_RANGE') {
      console.log(this.selectedPeriod['fromdate'])
      return this.selectedPeriod['fromdate'];
    }
  }
  
  getVersionFilterEndDate() {
    if (this.selectedFilterType === 'DATE_RANGE') {
      return this.snapshotFilterFormGroup.get('enddate').value;
    } else 
    if (this.selectedFilterType === 'PERIOD_RANGE') {
      console.log(this.selectedPeriod['todate'])
      return this.selectedPeriod['todate'];
    }
  }

  //Get Visitaion plans
  getVisitationPlans() {
    const payload = {
      method: 'get',
      where: {
        serviceplanid : this.selectedServicePlan.serviceplanid
      }
    };
    this._commonHttpService.getArrayList(payload, 'serviceplanvisitationplanmapping/listallvisitation?filter').subscribe(
      response => {
        this.visitationplans = response.map(item => item.visitationplan[0]);
        this.selectedServicePlan.visitationplans = this.visitationplans;
      }
    );
  }

  createSnapshot() {
    
    let versionfilterstartdate = this.getVersionFilterStartDate();
    let versionfilterenddate = this.getVersionFilterEndDate();
    let splanversionpersoninvolved = [this.selectedChildData.personid]; //this.snapshotFilterFormGroup.get('personlist').value
    this.selectedServicePlan.childrenHeader = this.selectedChildData.fullname;
    let versionsnapshot = _.cloneDeep(this.selectedServicePlan);
    const versiongoal = [];
    const versionvisitation = [];
    const goals = Array.isArray(versionsnapshot.splangoal) ? versionsnapshot.splangoal : [];
    const visitations = Array.isArray(versionsnapshot.visitationplans) ? versionsnapshot.visitationplans : [];
    goals.forEach(goal => {
      const objectives = Array.isArray(goal.splanobjective) ? goal.splanobjective : [];
      const versionobjective = [];
      this.countobj = 0;
      objectives.forEach(objective => {
        const actions = Array.isArray(objective.serviceplanaction) ? objective.serviceplanaction : [];
        const versionactions = [];
        actions.forEach(action => {
          this.count = 0;
          action.serviceplanpersoninvolved.forEach(serviceplanperinvolved => {
            if (splanversionpersoninvolved.indexOf(serviceplanperinvolved.personinvolved) > -1) {
              this.count++;
            }
          });
          // *** Filter based on the end date of the actions being within the filter start to end date ***
          // *** We dont care when the action started, but want to show the actions that are ending in the selected period range ***
          if (this.count > 0 && new Date(versionfilterstartdate) <= new Date(action.enddate) && new Date(versionfilterenddate) >= new Date(action.enddate)) {
            versionactions.push(action);
          }
        });
        if (versionactions.length > 0) {
          objective.serviceplanaction = versionactions;
          versionobjective.push(objective);
        } else {
          objective = {};
          this.countobj++;
        }
      });
      if (this.countobj === objectives.length) {
        goal = {};
      } else {
        goal.splanobjective = versionobjective;
        versiongoal.push(goal);
      }
    });
    versionsnapshot.splangoal = versiongoal;
    visitations.forEach(visitation => {
      if (splanversionpersoninvolved.indexOf(visitation.personid) > -1) {
        let temp = this.frequencyofplannedvisits.find(x => x.ref_key === visitation.visitfrequencytypekey);
        visitation.visitfrequencytypekey = (temp) ? (temp.value_text) : '';
        temp = this.lengthofplannedvisit.find(x => x.ref_key === visitation.visitdurationtypekey);
        visitation.visitdurationtypekey = (temp) ? (temp.value_text) : '';
        temp = this.childandvisitortransportation.find(x => x.ref_key === visitation.childtransportationtypekey);
        visitation.childtransportationtypekey = (temp) ? (temp.value_text) : '';
        temp = this.childandvisitortransportation.find(x => x.ref_key === visitation.visitortransportationtypekey);
        visitation.visitortransportationtypekey = (temp) ? (temp.value_text) : '';
        versionvisitation.push(visitation);
      }
    });
    versionsnapshot.visitationplans = versionvisitation;
    this.saveSnapshothist(versionsnapshot);
  }
  
  getReferenceDropdowns() {
    this._commonDropdownService.getReferenveValuesByTypeId('50042')
      .subscribe( (response) => { this.childandvisitortransportation = response });
    this._commonDropdownService.getReferenveValuesByTypeId('50084')
      .subscribe( (response) => { this.frequencyofplannedvisits = response });    
    this._commonDropdownService.getReferenveValuesByTypeId('500108')
      .subscribe( (response) => { this.lengthofplannedvisit = response });
  }

  saveSnapshothist(versionsnapshot) {
    const payload = {
      'objectid': this.selectedServicePlan.serviceplanid,
      'objecttype': 'CPLAN3',
      'snapshotdata': versionsnapshot,
      'fromdate': this.getVersionFilterStartDate(),
      'todate': this.getVersionFilterEndDate(),
      'personid': this.selectedChildData.personid
    };
    this._commonHttpService.create(payload, 'snapshothist').subscribe(
      response => {
          this.getSnapshotHist();
      });
  }

  //Get pdfs
  servicePlanIhmPrint(item,index) {
    const inputRequest = {
         'intakeserviceid': this.id,
         'id': this.selectedServicePlan.serviceplanid,
         'type': 'IHSFP',
         'snapshotid': item,
         'documenttemplatekey': [
             'inhome'
         ],
         'isheaderrequired': false,
        //  personinvolved: this.personlistforsnapshot[index]
     };
     const payload = {
       method: 'post',
       count: -1,
       page: 1,
       limit: 20,
       where: inputRequest,
       documntkey: [
         'inhome'
     ],
     };
     this._commonHttpService.create(payload, 'serviceplan/getreportserviceplan').subscribe(
       response => {
         console.log(response, 'pdf response');
         setTimeout(() => window.open(response.data.documentpath), 2000);
      });
     // this.backupService = this.selectedService ? this.selectedService : null;
     // this.selectedService = item;
     // (<any>$('#servicePlanIhmPrint')).modal('show');
   }

   servicePlanOohPrint(item,index) {
    const inputRequest = {
      'intakeserviceid': this.id,
      'id': this.selectedServicePlan.serviceplanid,
      'type': 'OOH',
      'snapshotid': item,
      'documenttemplatekey': [
          'inhome'
      ],
      'isheaderrequired': false,
      // personinvolved: this.personlistforsnapshot[index]
  };
  const payload = {
    method: 'post',
    count: -1,
    page: 1,
    limit: 20,
    where: inputRequest,
    documntkey: [
      'oohome'
  ],
  };
  this._commonHttpService.create(payload, 'serviceplan/getreportserviceplan').subscribe(
    response => {
      console.log(response, 'pdf response');
      setTimeout(() => window.open(response.data.documentpath), 2000);
   });
    // this.backupService = this.selectedService ? this.selectedService : null;
    // this.selectedService = item;
    // (<any>$('#servicePlanOohPrint')).modal('show');
  }

}
