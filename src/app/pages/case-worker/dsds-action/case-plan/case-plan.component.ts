import { Component, OnInit } from '@angular/core';
import { CasePlanService } from './case-plan.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CP_TABS } from './case-plan-config';
import { DataStoreService } from '../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'case-plan',
  templateUrl: './case-plan.component.html',
  styleUrls: ['./case-plan.component.scss']
})
export class CasePlanComponent implements OnInit {
  childList = [];
  selectedChild: any;
  parent1 = [];
  parent2 = [];
  raceDropDown = {};
  childCardFormGroup:  FormGroup;
  riskAssessment: any;
  safeCAssessment: any;
  searchResult: any;

  tabs = CP_TABS;
  CASE_PLAN_ONE_PATH = 'case-plan-one';

  constructor(private _casePlanService: CasePlanService,
    private _dataStoreService: DataStoreService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _router: Router
    ) { }

  ngOnInit(

  ) {
    const childList = this._casePlanService.getChildList();
    this.childList = childList.filter(data => data.removalStatus === 'Approved');
    this.initForm();
    this.parent1 = this._casePlanService.getParent('Biological Mother');
    this.parent2 = this._casePlanService.getParent('Biological Father');

    this.involvedPersonDropdown();
    this.getAssessmentsForm();
  }

  initForm() {
    this.childCardFormGroup = this._formBuilder.group({
      child: [null],
    });
  }

  viewChildInfo(child) {
    console.log('Child Status', child);
    this._dataStoreService.setData('CP1DATA', child);
    this.selectedChild = child;
    this.selectedChild.parent1name = ( this.parent1 && this.parent1.length ) ? ( this.parent1[0].firstname + ' ' +  this.parent1[0].lastname ) : null;
    this.selectedChild.parent2name = ( this.parent2 && this.parent2.length ) ? ( this.parent2[0].firstname + ' ' +  this.parent2[0].lastname ) : null;
    this.selectedChild.riskAssessment = (this.riskAssessment && this.riskAssessment.length) ? this.riskAssessment[0] : null;
    this.selectedChild.safeCAssessment = (this.safeCAssessment && this.safeCAssessment.length) ? this.safeCAssessment[0] : null;
    this.viewSocialHistoryInfo(child);
  }

  selectChild(child) {
    this._dataStoreService.setData('SELECTED_CHILD_DATA', child);
    this._router.navigate([this.CASE_PLAN_ONE_PATH], { relativeTo: this.route });
    console.log('SETTING CHILD DATA', child);
  }



  getAssessmentsForm() {
    this._casePlanService.getAssessMents('MARYLAND FAMILY INITIAL RISK ASSESSMENT').subscribe(data => {
       this.riskAssessment = data.data[0];
    });
    this._casePlanService.getAssessMents('SAFE-C').subscribe(data => {
      this.safeCAssessment = data.data[0];
   });
  }

  getRaceDesc(key) {
    if (this.raceDropDown  && this.raceDropDown[key]) {
        return this.raceDropDown[key];
    } else {
      return null;
    }
  }
  private involvedPersonDropdown() {
    this.raceDropDown = {};
   this._casePlanService.loadRaceDropDown().
   subscribe( res => {
    res.forEach(data => {
      this.raceDropDown[data.racetypekey] = data.typedescription;
    });
    console.log(this.raceDropDown);
    // text: res.typedescription,
    // value: res.racetypekey,
    });


}

  resetSelectedChild() {
    this.selectedChild = null;
    this.childCardFormGroup.reset();
  }

  viewSocialHistoryInfo(child) {
    if ( child &&  child.intakeservicerequestactorid) {
      const selectedChild = this._casePlanService.getSocialHistory(child.intakeservicerequestactorid, child.removaldate);
      console.log( this.selectedChild);
      selectedChild.subscribe(data => {
        console.log(data);
        if (data && data.length) {
         this.selectedChild.placement = data[0].metadata.placement;
         this.selectedChild.familyhistory = data[0].metadata.familyhistory;
         this.selectedChild.childdesc = data[0].metadata.childdesc;
        }
      });
    }  else  {
      this.selectedChild = [];
    }
  }

  onTabClick(tabItem, contentElement: HTMLElement) {
    // contentElement.scrollIntoView();
    // this.tabs = this._ytpService.processTabState(this.tabs, this._dataStoreService.getData('YTPDATA'));
  }

}
