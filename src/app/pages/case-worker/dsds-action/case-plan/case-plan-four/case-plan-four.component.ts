import { Component, OnInit } from '@angular/core';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DataStoreService, CommonHttpService } from '../../../../../@core/services';
import * as jsPDF from 'jspdf';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
  selector: 'case-plan-four',
  templateUrl: './case-plan-four.component.html',
  styleUrls: ['./case-plan-four.component.scss']
})
export class CasePlanFourComponent implements OnInit {

  store: any;
  cp1Data: any;
  ytpPlanGoalList: any[] = [];
  selectedYTPPlan: any;
  ytpData: any;
  intakeserviceid = '';
  selectedClientId: string;
  approvalProcess: string;
  selectedItem: string;
  selectedPlan: any;
  selectedPlanId: string;
  getUsersList = [];
  selectedPerson: any;
  selectService: any;
  id: any;
  daNumber: any;
  user: AppUser;
  ytpList: any;
  
  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
  selYouth: any;
  ytpSummary: any = {};

  constructor(
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService,
  ) {
    this.store = this._dataStoreService.getCurrentStore();
    this.cp1Data = this.store['CP1DATA'];
   }

  ngOnInit() {
    if(this.cp1Data) {
      this.getYTPPlanList(this.cp1Data.personid);
    }
  }

  selectPlan(item) {
    this.selectedYTPPlan = item;
    this.youthTransPlanPrint(item);
  }

  getYTPPlanList(clientID) {
    this.intakeserviceid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    return this._commonHttpService.getArrayList(
      {
        page: 1,
        limit: 20,
        method: 'get',
        where: {
          clientid: clientID,
          intakeserviceid: this.intakeserviceid,
          approvalstatuskey: 'Approved'
        },
        order: 'insertedon desc'
      },'youthtransitionplan' + '?filter'
    ).subscribe(
      (response) => {
        this.ytpList = response;
        this.selYouth = null;
    });
  }

  closeView() {
    this.selectedYTPPlan = null;
    this.getYTPPlanList(this.cp1Data.personid);
  }

  youthTransPlanPrint(modal) {
    this.selYouth = modal;

    if (this.selYouth && this.selYouth.documentation_json 
      && this.selYouth.documentation_json.youthSign) {
      setTimeout(() => {
      this.createImage(this.selYouth.documentation_json.youthSign)
      }, 1000);
    }
  }

  createImage(data) {
    var image = new Image();
    var blob = new Blob([data], { type: 'image/png' });
    image.src = URL.createObjectURL(blob);
    document.body.appendChild(image);
  }

  async downloadYTPPdf() {
    const pages = document.getElementsByClassName('pdf-page');
    let pageImages = [];
    for (let i = 0; i < pages.length; i++) {
      const pageName = pages.item(i).getAttribute('data-page-name');
      if (pageName === 'Youth Transitional Plan') {
        await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
          const img = canvas.toDataURL('image/png');
          pageImages.push(img);
        });
      }
    }

    this.pdfFiles.push({ fileName: 'Youth Transitional Plan', images: pageImages });
    pageImages = [];
    this.convertImageToPdf();
  }

  convertImageToPdf() {
    this.pdfFiles.forEach((pdfFile) => {
      var doc = null;
      doc = new jsPDF();
      var width = doc.internal.pageSize.getWidth() - 10;
      var heigth = doc.internal.pageSize.getHeight() - 10;

      pdfFile.images.forEach((image, index) => {

        doc.addImage(image, 'PNG', 3, 5, width, heigth);
        if (pdfFile.images.length > index + 1) {
          doc.addPage();
        }
      });
      doc.save(pdfFile.fileName);
    });
    (<any>$('#youthTransPlan')).modal('hide');
    this.pdfFiles = [];
  }
}
