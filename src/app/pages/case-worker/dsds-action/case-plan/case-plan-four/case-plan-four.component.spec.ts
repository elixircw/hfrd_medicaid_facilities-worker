/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CasePlanFourComponent } from './case-plan-four.component';

describe('CasePlanFourComponent', () => {
  let component: CasePlanFourComponent;
  let fixture: ComponentFixture<CasePlanFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasePlanFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasePlanFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
