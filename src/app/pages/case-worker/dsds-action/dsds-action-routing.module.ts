import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DsdsActionComponent } from './dsds-action.component';
import { AppConstants } from '../../../@core/common/constants';
import { RoleGuard } from '../../../@core/guard';
const routes: Routes = [
    {
        path: '',
        component: DsdsActionComponent,
        canActivate: [RoleGuard],
        children: [
             { path: '', loadChildren: './report-summary/report-summary.module#ReportSummaryModule'  },
            // { path: 'allegation', component: AllegationComponent },
            // {path : 'alternative-response-summary', component: AlternativeResponseSummaryComponent },
               { path: 'assessment',  loadChildren: './assessment/assessment.module#AssessmentModule'  },
               { path: 'cw-assignments', loadChildren: './cw-assignments/cw-assignments.module#CwAssignmentsModule'  },
              { path: 'kinship', loadChildren: './kinship/kinship.module#KinshipModule'},
             { path: 'adult-assessment', loadChildren: './adult-assessment/adult-assessment.module#AdultAssessmentModule'},
             { path: 'adult-assessment/adult-caseworker-assessment',  loadChildren: './adult-assessment/adult-caseworker-assessment/adult-caseworker-assessment.module#AdultCaseworkerAssessmentModule'  },
              { path: 'assessment/case-worker-view-assessment', loadChildren: './assessment/case-worker-view-assessment/case-worker-view-assessment.module#CaseWorkerViewAssessmentModule'  },
              { path: 'assessment/view-assessment', loadChildren: './assessment/view-assessment/view-assessment.module#ViewAssessmentModule'  },
              {
                 path: 'attachment', loadChildren: './attachment/attachment.module#AttachmentModule'
              },
              {
                 path: 'attachment/:type',
                 loadChildren: './attachment/attachment.module#AttachmentModule'
             },
             { path: 'cross-reference', loadChildren: './cross-reference/cross-reference.module#CrossReferenceModule' },
             { path: 'disposition', loadChildren: './disposition/disposition.module#DispositionModule' },
             // { path: 'case-plan', component: CasePlanComponent },
             { path: 'case-plan', loadChildren: './case-plan/case-plan.module#CasePlanModule' },
             { path: 'social-history', loadChildren: './social-history/social-history.module#SocialHistoryModule' },
            // { path: 'history',  loadChildren: './history/history.module#HistoryModule' },
                {
                    path: 'investigation-plan',
                    loadChildren: './investigation-plan/investigation-plan.module#InvestigationPlanModule'
                },
             // { path: 'involved-entity', component: InvolvedEntityComponent },
                {
                    path: 'involved-person',
                    loadChildren: './involved-persons/involved-persons.module#InvolvedPersonsModule'
                    // './involved-person/involved-person.module#InvolvedPersonModule'
                },
                {
                    path: 'person-cw',
                    loadChildren: '../../shared-pages/involved-persons/involved-persons.module#InvolvedPersonsModule',
                    data: { source: AppConstants.MODULE_TYPE.CASE }
                },
                // {
                //     path: 'person',
                //     loadChildren: 'app/pages/person-djs/person-djs.module#PersonDjsModule'
                // },
                { 
                    path: 'adoption-persons', 
                    loadChildren: '../../shared-pages/involved-persons/involved-persons.module#InvolvedPersonsModule',
                    // data: { source: AppConstants.MODULE_TYPE.ADOPTION_CASE }
                    data: { source: AppConstants.MODULE_TYPE.CASE }
                },
             { path: 'recording', loadChildren: './recording/recording.module#RecordingModule' },
             { path: 'as-recording', loadChildren: './as-recording/as-recording.module#AsRecordingModule' },
             { path: 'referral', loadChildren: './referral/referral.module#ReferralModule' },
             { path: 'as-referral', loadChildren: './adult-referral/adult-referral.module#AdultReferralModule' },
              // { path: 'report-summary',  loadChildren: './report-summary/report-summary.module#ReportSummaryModule'  },
             { path: 'narrative', loadChildren: './narrative/narrative.module#NarrativeModule' },
             { path: 'report-summary-djs', loadChildren: './report-summary-djs/report-summary-djs.module#ReportSummaryDjsModule' },
             { path: 'time-line-view',  loadChildren: './time-line-view/time-line-view.module#TimeLineViewModule' },
             { path: 'time-line-view-djs',  loadChildren: './time-line-view-djs/time-line-view-djs.module#TimeLineViewDjsModule' },
             { path: 'plan', loadChildren: './plan/plan.module#PlanModule' },
             { path: 'maltreatment-information', loadChildren: './maltreatment-information/maltreatment-information.module#MaltreatmentInformationModule' },
             { path: 'as-maltreatment-information', loadChildren: './as-maltreatment-information/as-maltreatment-information.module#AsMaltreatmentInformationModule' },
             { path: 'investigation-findings', loadChildren: './investigation-findings/investigation-findings.module#InvestigationFindingsModule' },
             { path: 'alternative-response-summary', loadChildren: './alternative-response-summary/alternative-response-summary.module#AlternativeResponseSummaryModule' },
             { path: 'adult-placement', loadChildren: './adult-placement/adult-placement.module#AdultPlacementModule' },
             { path: 'djs-placement', loadChildren: './djs-placement/djs-placement.module#DjsPlacementModule' },
             { path: 'sdm', loadChildren: './sdm/sdm.module#SdmModule' },
             { path: 'case-adult-screen-tool', loadChildren: './case-adult-screen-tool/case-adult-screen-tool.module#CaseAdultScreenToolModule' },
             { path: 'child-removal', loadChildren: './child-removal/child-removal.module#ChildRemovalModule' },
             { path: 'court', loadChildren: './court/court.module#CourtModule' },
             { path: 'service-plan', loadChildren: './service-plan/service-plan.module#ServicePlanModule' },
             { path: 'as-service-plan', loadChildren: './as-service-plan/as-service-plan.module#AsServicePlanModule' },
             { path: 'appointment', loadChildren: './appointment/appointment.module#AppointmentModule' },
             { path: 'legal-action-history',loadChildren: './legal-action-history/legal-action-history.module#LegalActionHistoryModule' },
            // { path: 'entities', loadChildren: './entities/entities.module#EntitiesModule' },
             { path: 'ar-case-closure', loadChildren: './ar-case-closure/ar-case-closure.module#ArCaseClosureModule' },
             { path: 'investigation-findings-as', loadChildren: './investigation-findings-as/investigation-findings-as.module#InvestigationFindingsAsModule' },
             { path: 'transport', loadChildren: './transport/transport.module#TransportModule' },
             { path: 'as-oas', loadChildren: './as-oas/as-oas.module#AsOasModule' },
             { path: 'as-invstgn-plan', loadChildren: './as-investigation-plan/as-investigation-plan.module#AsInvestigationPlanModule' },
             { path: 'oh-placement', loadChildren: './out-of-home-placement/out-of-home-placement.module#OutOfHomePlacementModule' },
             { path: 'foster-care', loadChildren: './foster-care/foster-care.module#FosterCareModule' },
             { path: 'monthly-report', loadChildren: './as-monthly-report/as-monthly-report.module#AsMonthlyReportModule' },
             { path: 'kinship-placement', loadChildren: './placement/kinship/kinship.module#KinshipModule' },
             // { path: 'visitor', loadChildren: './visitor/visitor.module#VisitorModule' },
             { path: 'court-action', loadChildren: './court-actions/court-actions.module#CourtActionsModule' },
             { path: 'djs-restitution', loadChildren: './djs-restitution/djs-restitution.module#DjsRestitutionModule' },
             { path: 'placement-menu', loadChildren: './placement-menu/placement-menu.module#PlacementMenuModule' },
             { path: 'service-child-removal', loadChildren: './child-removal-list/child-removal-list.module#ChildRemovalListModule' },
             { path: 'service-foster-care', loadChildren: './service-foster-care/service-foster-care.module#ServiceFosterCareModule' },
             { path: 'placement', loadChildren: './placement/placement.module#PlacementModule' },
             { path: 'relationship', loadChildren: '../../shared-pages/relationship-new/relationship-new.module#RelationshipNewModule' },
             { path: 'collateral', loadChildren: '../../shared-pages/collateral-new/collateral-new.module#CollateralNewModule' },
             { path: 'participation', loadChildren: './participations/participations.module#ParticipationsModule' },
             { path: 'payments',  loadChildren: './payments/payments.module#PaymentsModule'},
             { path: 'sc-placements', loadChildren: './service-case-placements/service-case-placements.module#ServiceCasePlacementsModule'},
             { path: 'sc-permanency-plan', loadChildren: './service-case-permanency-plan/service-case-permanency-plan.module#ServiceCasePermanencyPlanModule'},
             { path: 'in-home-service', loadChildren: './in-home-service/in-home-service.module#InHomeServiceModule'},
             { path: 'payment-history', loadChildren: './payment-history/payment-history.module#PaymentHistoryModule'}
        ],
        data: {
            title: ['MDTHINK - Case Worker'],
            desc: 'Maryland department of human services',
            screen: { current: 'case-worker', key: 'caseworker-module', includeMenus: true, modules: [], skip: false }
           }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DsdsActionRoutingModule { }
