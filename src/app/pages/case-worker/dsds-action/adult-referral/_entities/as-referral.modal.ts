export class ApproveIntake {
    intake: Intake;
    review: ReviewStatus;
}

export class Intake {
    CrossReferences: any[];
    createdCases: CreatedCase[];
    attachement: Attachement[];
    intakeDATypeDetails: IntakeDATypeDetail[];
    disposition: Disposition[];
    persondetails: Person[];
    General: General;
    DAType: DAType;
    reviewstatus: ReviewStatus;
    assessment: any[];
    intakeserviceid: string;
    asignsecurityuserid: string;
}

export class ReviewStatus {
    appevent: string;
    status: string;
    commenttext: string;
    youthstatuseventcode: string;
    ispreintakeapproved: boolean;
}

export class DAType {
    DATypeDetail: Disposition[];
}

export class General {
    Time: Date;
    IntakeNumber: string;
    Source: string;
    InputSource: string;
    RecivedDate: Date;
    CreatedDate: Date;
    Author: string;
    Agency: string;
    Purpose: string;
    islocalreferal: number;
    queAdditionDate: string;
    AgencyCode: string;
    Narrative: string;
    IsAnonymousReporter: boolean;
    IsUnknownReporter: boolean;
    RefuseToShareZip: boolean;
    Firstname: string;
    Lastname: string;
}

export class Disposition {
    ServiceRequestNumber: string;
    DaTypeKey: string;
    subSeriviceTypeValue: string;
    DasubtypeKey: string;
    DAStatus: string;
    DADisposition: string;
    Summary: string;
    dispositioncode: string;
    intakeserreqstatustypekey: string;
    comments: string;
    ReasonforDelay: string;
    supStatus: string;
    supDisposition: string;
}

export class IntakeDATypeDetail {
    ServiceRequestNumber: string;
    DaTypeKey: string;
    subSeriviceTypeValue: string;
    DasubtypeKey: string;
    DAStatus: string;
    DADisposition: string;
    Summary: string;
    dispositioncode: string;
    intakeserreqstatustypekey: string;
    comments: string;
    supStatus: string;
    supDisposition: string;
}

export class CreatedCase {
    caseID: string;
    serviceTypeID: string;
    serviceTypeValue: string;
    subServiceTypeID: string;
    subSeriviceTypeValue: string;
}
export class Attachement {
    documentpropertiesid: string;
    documenttypekey: string;
    documentdate: string;
    filename: string;
    originalfilename: string;
    title: string;
    description: string;
    mime: string;
    numberofbytes: string;
    updatedby: string;
    updatedon: string;
    insertedby: string;
    insertedon: string;
    s3bucketpathname: string;
    documentattachment: DocumentAttachment;
}
export class DocumentAttachment {
    documentpropertiesid: string;
    attachmenttypekey: string;
    attachmentclassificationtypekey: string;
    assessmenttemplateid: string;
}
export class Person {
    Pid: string;
    personid?: string;
    Lastname: string;
    Firstname: string;
    Gender: string;
    Dob: string;
    Role: string;
    rolekeyDesc: string;
    Dangerousself: string;
    Dangerousworker: string;
    Mentealimpair: string;
    Mentealillness: string;
    drugexposednewbornflag: number;
    fetalalcoholspctrmdisordflag: number;
    sexoffenderregisteredflag: number;
    probationsearchconductedflag: number;
    personRole: PersonRole[];
    personAddressInput: any[];
    cjamspid: string;
    substances: any[];
    phoneNumber: any[];
    emailID: any[];
    fullName: string;
}

export class PersonRole {
    rolekey: string;
    description: string;
    isprimary: string;
    relationshiptorakey?: any;
    hidden: boolean;
}
export class Supervisor {
    securityusersid: string;
    firstname: string;
    lastname: string;
    fullname: string;
    countyname: string;
    email: string;
}
