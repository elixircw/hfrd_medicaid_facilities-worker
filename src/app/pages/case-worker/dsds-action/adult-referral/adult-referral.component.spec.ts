import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdultReferralComponent } from './adult-referral.component';

describe('AdultReferralComponent', () => {
  let component: AdultReferralComponent;
  let fixture: ComponentFixture<AdultReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdultReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
