import { Component, OnInit } from '@angular/core';
import {  CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { PaginationRequest, DropdownModel } from '../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { ReferralSupervisourList, DSDSActionSummary, CrossReference } from '../../_entities/caseworker.data.model';
import { Intake,  CreatedCase, IntakeDATypeDetail, Disposition, ReviewStatus, ApproveIntake, Supervisor } from './_entities/as-referral.modal';
import { Person } from './_entities/as-referral.modal';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'adult-referral',
    templateUrl: './adult-referral.component.html',
    styleUrls: ['./adult-referral.component.scss']
})
export class AdultReferralComponent implements OnInit {
    id: string;
    referralForm: FormGroup;
    involevedPerson$: Observable<Person[]>;
    persons = [];
    selectPersonList: Person[] = [];
    supervisorDetails: Supervisor = new Supervisor();
    serviceTypeList$: Observable<DropdownModel[]>;
    subServiceTypes$: Observable<DropdownModel[]>;
    countyList$: Observable<DropdownModel[]>;
    attachmentList$: Observable<DropdownModel[]>;
    assessmentList$: Observable<DropdownModel[]>;
    supervisorsList: ReferralSupervisourList[] = [];
    approveIntakeRequest: Intake = new Intake();
    reviewStatus: ReviewStatus = new ReviewStatus();
    dsdsActionDetails: DSDSActionSummary = new DSDSActionSummary();
    crossReference$: Observable<CrossReference[]>;
    crossReference: CrossReference[] = [];
    subTypeName: string[] = [];
    serviceType: string[] = [];
    constructor(
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute,
        private _formBulider: FormBuilder,
        private _alertService: AlertService,
        private _dataSoreService: DataStoreService
    ) {}

    ngOnInit() {
        this.id = this._dataSoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.formInitializer();
        this.loadDropdown();
        this.getCrossreferance();
        this._dataSoreService.currentStore.subscribe(item => {
            if (item['dsdsActionsSummary']) {
                this.dsdsActionDetails = item['dsdsActionsSummary'];
            }
        });
    }
    formInitializer() {
        this.referralForm = this._formBulider.group({
            countyid: ['', Validators.required],
            attachement: [''],
            assessment: [''],
            servicetype: ['', Validators.required],
            servicesubtype: ['', Validators.required],
            narrative: ['', Validators.required]
        });
    }
    loadDropdown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    nolimit: true,
                    where: { teamtypekey: 'AS' },
                    method: 'get',
                    order: 'description'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.AdultReferral.ServiceTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    nolimit: true,
                    method: 'get',
                    order: 'originalfilename'
                },
               CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/' + this.id + '?data'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.AdultReferral.AssessmentUrl + '?filter'
            ),
            this._commonHttpService.getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: {
                        intakeserviceid: this.id
                    }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PersonList + '?filter'
            )
        ]).map(res => {
            return {
                servicetype: res[0].map(
                    item =>
                        new DropdownModel({
                            text: item.description,
                            value: item.intakeservreqtypeid
                        })
                ),
                countyList: res[1].map(
                    item =>
                        new DropdownModel({
                            text: item.countyname,
                            value: item.countyid
                        })
                ),
                attachment: res[2].map(item => {
                    return item;
                }),
                assessment: res[3].map(item => {
                    return item;
                }),
                person: res[4]['data'].map(item => {
                    return item;
                }),
                name: res[0].map(item => {
                    this.serviceType[item.intakeservreqtypeid] =
                        item.description;
                })
            };
        });
        this.serviceTypeList$ = source.pluck('servicetype');
        this.countyList$ = source.pluck('countyList');
        this.attachmentList$ = source.pluck('attachment');
        this.assessmentList$ = source.pluck('assessment');
        this.involevedPerson$ = source.pluck('person');
        this.involevedPerson$.subscribe(item => {
            this.persons = item;
        });
    }
    getCrossreferance() {
        this._commonHttpService.getPagedArrayList(
            {
                method: 'get',
                where: {
                    intakerequestid: this.id
                }
            },  CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.CrossReferenceUrl + '?filter'
        ).subscribe((res) => {
            if (res) {
                this.crossReference = res['data'];
            }
        });
    }
    listServiceSubtype(intakeservreqtypeid) {
        const source = this._commonHttpService
            .getArrayList(
                {
                    include: 'servicerequestsubtype',
                    nolimit: true,
                    where: { intakeservreqtypeid: intakeservreqtypeid },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.AdultReferral
                    .ServiceSubTypeUrl + '?filter'
            )
            .map(result => {
                if (
                    result &&
                    result.length &&
                    result[0].servicerequestsubtype
                ) {
                    return {
                        subtype: result[0].servicerequestsubtype.map(
                            res =>
                                new DropdownModel({
                                    text: res.description,
                                    value: res.servicerequestsubtypeid
                                })
                        ),
                        name: result[0].servicerequestsubtype.map(res => {
                            this.subTypeName[res.servicerequestsubtypeid] = res.description;
                        })
                    };
                }
            });
        this.subServiceTypes$ = source.pluck('subtype');
    }
    submitReview() {
        if (this.selectPersonList && this.selectPersonList.length) {
            this._commonHttpService
                .getArrayList(
                    {
                        where: {
                            countyid: this.referralForm.value.countyid
                        },
                        method: 'get'
                    },
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.AdultReferral.supervisorsListUrl + '?filter'
                )
                .subscribe(
                    item => {
                        this.supervisorsList = item;
                        (<any>$('#list-supervisor')).modal('show');
                    },
                    () => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
        } else {
            this._alertService.warn('Please select involved persons');
        }
    }
    onChangeSupervisor(modal) {
        this.supervisorDetails = modal;
    }
    selectPerson(modal, person) {
        if (modal) {
            this.persons.map(item => {
                if (item.personid === person.personid) {
                    delete item.emergency;
                    const personDetails = Object.assign({
                        Dob: item.dob,
                        Pid: item.personid,
                        Role: item.rolename,
                        Gender: item.gender,
                        Lastname: item.lastname,
                        Firstname: item.firstname,
                        cjamspid: item.cjamspid,
                        personRoles: item.roles,
                        phoneNumber: item.phonenumber,
                    }, item);
                    this.selectPersonList.push(personDetails);
                }
            });
        } else {
            this.selectPersonList.map((item, index) => {
                if (item.personid === person.personid) {
                    this.selectPersonList.splice(index, 1);
                }
            });
        }
    }
    closeSupervisor() {
        this.referralForm.reset();
    }
    approveIntake() {
        if (this.supervisorDetails.securityusersid) {
            const createdCases: CreatedCase[] = [];
            const intakeDATypeDetails: IntakeDATypeDetail[] = [];
            const disposition: Disposition[] = [];
            createdCases.push(
                Object.assign({
                    caseID: this.dsdsActionDetails.da_number,
                    serviceTypeID: this.referralForm.value.servicetype,
                    serviceTypeValue: this.serviceType[this.referralForm.value.servicetype],
                subServiceTypeID: this.referralForm.value.servicesubtype,
                subSeriviceTypeValue: this.subTypeName[this.referralForm.value.servicesubtype]
                })
            );
            intakeDATypeDetails.push(
                Object.assign({
                    ServiceRequestNumber: this.dsdsActionDetails.da_number,
                    DaTypeKey: this.referralForm.value.servicetype,
                    subSeriviceTypeValue: this.subTypeName[this.referralForm.value.servicesubtype],
                    DasubtypeKey: this.referralForm.value.servicesubtype,
                    DAStatus: 'Approved',
                    DADisposition: 'Scrnin',
                    Summary: '',
                    dispositioncode: 'screenin',
                    intakeserreqstatustypekey: 'Review',
                    comments: '',
                    supStatus: 'Approved',
                    supDisposition: 'Scrnin'
                })
            );
            disposition.push(
                Object.assign({
                    ServiceRequestNumber: this.dsdsActionDetails.da_number,
                    DaTypeKey: this.referralForm.value.servicetype,
                    subSeriviceTypeValue: this.subTypeName[this.referralForm.value.servicesubtype],
                    DasubtypeKey: this.referralForm.value.servicesubtype,
                    DAStatus: 'Approved',
                    DADisposition: 'Scrnin',
                    Summary: '',
                    dispositioncode: 'ScreenOUT',
                    intakeserreqstatustypekey: 'Review',
                    comments: '',
                    ReasonforDelay: null,
                    supStatus: 'Approved',
                    supDisposition: 'Scrnin'
                })
            );
            this.approveIntakeRequest.General = {
                Time: new Date(),
                IntakeNumber: this.dsdsActionDetails.intakenumber,
                Source: this.dsdsActionDetails.da_communicationid,
                InputSource: this.dsdsActionDetails.da_communicationid,
                RecivedDate: this.dsdsActionDetails.da_receiveddate,
                CreatedDate: this.dsdsActionDetails.da_insertedon,
                Author: '',
                Agency: 'AS',
                Purpose: this.dsdsActionDetails.da_typeid,
                islocalreferal: 0,
                queAdditionDate: '2019-02-20T09:35:19.994Z',
                AgencyCode: 'AS',
                Narrative: this.referralForm.value.narrative,
                IsAnonymousReporter: true,
                IsUnknownReporter: false,
                RefuseToShareZip: false,
                Firstname: 'test',
                Lastname: 'test'
            };
            this.approveIntakeRequest.intakeserviceid = this.id;
            this.approveIntakeRequest.asignsecurityuserid = this.supervisorDetails.securityusersid;
            this.approveIntakeRequest.persondetails = Object.assign({Person: this.selectPersonList});
            this.approveIntakeRequest.CrossReferences = this.crossReference;
            if (this.referralForm.value.attachement &&  this.referralForm.value.attachement.length) {
                this.approveIntakeRequest.attachement = this.referralForm.value.attachement.map((item) => {
                    delete item.documentpropertiesid;
                    delete item.documentattachment.documentpropertiesid;
                    delete item.userprofile;
                    delete item.updateduserprofile;
                    return item;
                });
            } else {
                this.approveIntakeRequest.attachement = [];
            }
            this.approveIntakeRequest.assessment = this.referralForm.value.assessment;
            this.approveIntakeRequest.createdCases = createdCases;
            this.approveIntakeRequest.intakeDATypeDetails = intakeDATypeDetails;
            this.approveIntakeRequest.disposition = disposition;
            this.approveIntakeRequest.DAType = Object.assign({DATypeDetail: disposition});
            this.reviewStatus = {
                appevent: 'INTR',
                status: 'Approved',
                commenttext: '',
                youthstatuseventcode: '',
                ispreintakeapproved: false
            };
            const approveIntake: ApproveIntake = Object.assign({
                intake: this.approveIntakeRequest,
                review: this.reviewStatus
            });
            this._commonHttpService
                .create(
                    approveIntake,
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.AdultReferral
                        .approveIntakeUrl
                )
                .subscribe(
                    item => {
                        (<any>$('#list-supervisor')).modal('hide');
                        this.closeSupervisor();
                        this._alertService.success(
                            'successfully created the case'
                        );
                    },
                    () => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                        this.closeSupervisor();
                        (<any>$('#list-supervisor')).modal('hide');
                    }
                );
        } else {
            this._alertService.warn('Please select supervisor');
        }
    }
}
