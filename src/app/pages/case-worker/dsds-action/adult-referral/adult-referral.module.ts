import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdultReferralComponent } from './adult-referral.component';
import { AdultReferralRoutingModule } from './adult-referral-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    AdultReferralRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    PaginationModule
  ],
  declarations: [
    AdultReferralComponent
  ],
  providers: []
})
export class AdultReferralModule { }
