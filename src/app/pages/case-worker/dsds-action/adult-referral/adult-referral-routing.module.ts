import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdultReferralComponent } from './adult-referral.component';
const routes: Routes = [
    {
    path: '',
    component: AdultReferralComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdultReferralRoutingModule { }
