import { Component, OnInit } from '@angular/core';
import { ServiceCasePermanencyPlanService } from '../service-case-permanency-plan.service';
import { DataStoreService, CommonHttpService, AuthService, SessionStorageService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { Router, ActivatedRoute } from '@angular/router';
import { PlacementConstants } from '../../service-case-placements/constants';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AppConstants } from '../../../../../@core/common/constants';

@Component({
  selector: 'child-wrapper',
  templateUrl: './child-wrapper.component.html',
  styleUrls: ['./child-wrapper.component.scss']
})
export class ChildWrapperComponent implements OnInit {
  childList = [];
  permanencyPlanList= [];
  accountpayableList = [];
  placementList = [];
  selectedChildren: any[];
  selectedChild: any;
  id: string;
  permanencyType: any;
  primaryPlanSubType: any;
  isAppla: boolean;
  formAction: string;
  isSuperVisor: boolean;
  personsInvolved: any;
  placementchk: any;
  isClosed = false;
  paginationInfo: PaginationInfo  = new PaginationInfo();
  constructor(private _serviceCasePermanencyPlanService: ServiceCasePermanencyPlanService,
    private _dataStoreService: DataStoreService,
    private commonHttpService: CommonHttpService,
    private router: Router,
    private storage: SessionStorageService,
    private _authService: AuthService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this._serviceCasePermanencyPlanService.getChildRemovalInfoAndPlacements();
    this.isSuperVisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this.childList = this.setApprovedFlag(this._serviceCasePermanencyPlanService.placementList);
    this._serviceCasePermanencyPlanService.refresh$.subscribe(_ => {
      this.childList = this.setApprovedFlag(this._serviceCasePermanencyPlanService.placementList);
      this.permanencyPlanList = this._serviceCasePermanencyPlanService.permanencyPlanList;
    });
    this.childList.map(res => {
      return res.fullname = res.prefx + ' ' + res.firstname + ' ' + res.middlename + ' ' + res.lastname + ' ' + res.suffix;
    });
    this.formAction = this._serviceCasePermanencyPlanService.formAction;
    this.selectedChildren = this._serviceCasePermanencyPlanService.selectedChildren;
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

  setApprovedFlag(list) {
    /* list = list.filter(item => {
      const placement = Array.isArray(item.placements) ? item.placements : [];
      const isApproved = placement.some(ele => (ele.routingstatus === 'Approved' && ele.isvoided !== 1 && ele.enddate === null && ele.placementtypekey === 'PRPL'));
      item.placementapproved = isApproved;
      item.placementtypekey = 'PRPL';
      return true;
     });
    const isActivePlacement = list && list.length ?  list.filter(ele => ele.placementtypekey === 'PRPL') : [];
    return isActivePlacement && isActivePlacement.length ? list : []; */
    list.forEach(child => {
      child.hasActivePlacement = false;
      if (Array.isArray(child.placements)) {
        child.hasActivePlacement = this._serviceCasePermanencyPlanService.checkForChildHasActivePlacements(child.placements);
        const providerPlacements =  child.placements.filter(placement => placement.placementtypekey === 'PRPL');
        child.hasOnlyLA = providerPlacements && providerPlacements.length ? false : true;
      }
    });
    return list.filter(item => !item.hasOnlyLA);
  }
  onChildChecked(event, child) {
    this._serviceCasePermanencyPlanService.selectChild(child, event.checked);
  }

  showPlacementDetails(placement) {
    this._serviceCasePermanencyPlanService.placementDetails = this._serviceCasePermanencyPlanService.placementList;
    this.router.navigate(['details/' + PlacementConstants.ACTIONS.REVIEW], { relativeTo: this.route });
  }

  isEditForm() {
    return ( this._serviceCasePermanencyPlanService.formAction === 'Edit' ) ;
  }


}
