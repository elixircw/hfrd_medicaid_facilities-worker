import { Component, OnInit } from '@angular/core';
import {
    CommonHttpService,
    AlertService,
    DataStoreService,
    AuthService
} from '../../../../../@core/services';
import { ServiceCasePermanencyPlanService } from '../service-case-permanency-plan.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { PaginationInfo } from '../../../../../@core/entities/common.entities';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { PermanencyListComponent } from '../permanency-list/permanency-list.component';
import * as moment from 'moment';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';

@Component({
    selector: 'permanency-plan-form',
    templateUrl: './permanency-plan-form.component.html',
    styleUrls: ['./permanency-plan-form.component.scss']
})
export class PermanencyPlanFormComponent implements OnInit {
    childList = [];
    permanencyPlanList: any;
    accountpayableList = [];
    placementList = [];
    selectedChildren: any[];
    selectedChild: any;
    involvedPersons: any;
    id: string;
    permanencyType: any;
    userInfo: AppUser;
    primaryPlanSubType: any;
    isAppla: boolean;
    permanencyPlanForm: FormGroup;
    personsInvolved: any;
    formAction: string;
    placementchk: any;
    paginationInfo: PaginationInfo = new PaginationInfo();
    errorMessage: string;
    isViewMode: boolean;
    endMaxDate: any;
    concurrentPlanRequired = false;
    minDate: any;
    isInView = false;
    planEstablishedMinDate: any;
    disableEndPlan = true;
    constructor(
        private _serviceCasePermanencyPlanService: ServiceCasePermanencyPlanService,
        private commonHttpService: CommonHttpService,
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _alert: AlertService,
        private _dataStoreService: DataStoreService,
        private _authService: AuthService,
        private _intakeUtils: IntakeUtils
    ) { }

    ngOnInit() {
        this.forminit();
        
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.loadPermanencyTypeDropDown();
        this.getInvolvedPerson();
        this.formAction = 'Add';
      //  this.endMaxDate = new Date();
        this.userInfo = this._authService.getCurrentUser();
        this.isViewMode = this._serviceCasePermanencyPlanService.isViewMode;
        // this._serviceCasePermanencyPlanService.updatePermanencyList$.subscribe(data=>{
        //   this.formAction = 'Edit';
        //   this.LoadPermanencyForm(data);
        // });
        this.formAction = this._serviceCasePermanencyPlanService.formAction;
        this.toggleFormMode();
        if (this.userInfo && this.userInfo.user.userprofile.displayname) {
            this.permanencyPlanForm.patchValue({
                caseworkername: this.userInfo.user.userprofile.displayname
            });
        }
        const allPlan = (Array.isArray(this._serviceCasePermanencyPlanService.permanencyPlanList)) ? this._serviceCasePermanencyPlanService.permanencyPlanList : [];
        // const person = (allPlan.length > 0) ? allPlan[0] : null;
        const selectedChild = this._serviceCasePermanencyPlanService.selectedChildren[0];
        if(selectedChild && selectedChild.placements && selectedChild.placements.length) {
            this.planEstablishedMinDate = selectedChild.placements[0].startdate;
        }
        const multiSelectedChild = this._serviceCasePermanencyPlanService.selectedChildren;
        if (multiSelectedChild && multiSelectedChild.length > 0) {
            for (let i = 0; i < multiSelectedChild.length; i++) {
                if (this.minDate) {
                    if (this.minDate < multiSelectedChild[i].dob) {
                        this.minDate = multiSelectedChild[i].dob;
                    }
                } else {
                    this.minDate = multiSelectedChild[i].dob;
                }
            }
        }
        if (selectedChild) {
            const person = allPlan.find(child => child.personid === selectedChild.personid);
            this.permanencyPlanList = (person) ? person.permanencyplans : [];
        }
        // this.permanencyPlanList = this._serviceCasePermanencyPlanService.permanencyPlanList;
        this._serviceCasePermanencyPlanService.placementApprovalQueue$.subscribe(
            data => {
                this._alert.success(data);
                setTimeout(() => {
                    this.goBack();
                }, 2000);
            }
        );
    }

    forminit() {
        this.permanencyPlanForm = this.formBuilder.group({
            permanencyplanid: [null],
            primaryplandate: [null],
            concurrentplandate: [null],
            caseworkername: [null],
            primarypermanencytype: [null],
            concurrentpermanencytype: [null],
            primaryarrangetype: [null],
            concurrentarrangetype: [null],
            remarks: [null],
            concurrentcomments: [null],
            isreviewsubmit: [0], // 0 - save  1-submit,
            isInClosedProximity: [null],
            isInClosedProximityExpln: [null],
            meetingSafetyNeedsExpln: [null],
            sixMonthsPlacementExpln: [null],
            courtOrdersExpln: [null],
            permToPermExpln: [null],
            parentname: [null],
            safeAndCareExpln: [null],
            assessmentPeriodExpln: [null],
            lifebookExpln: [null],
            serviceAgreementExpln: [null],
            isProviderAgree: [null],
            isProviderAgreeExpln: [null],
            serviceAgreementForOtherExpln: [null],
            reason: [null],
            enddate: [null],
            achieveddate: [null]
            // endcomments: [null]
        });

    }

    toggleFormMode() {
        this.isInView = false;
        switch (this.formAction) {
            case 'Add':
                this.forminit();
                break;
            case 'Edit':
                const formData = this._serviceCasePermanencyPlanService
                    .selectedPermanencyPlan;
                this.LoadPermanencyForm(formData);
                break;
            case 'View':
                this.permanencyPlanForm.disable();
                this.isInView = true;
                break;
            default:
                break;
        }
        console.log('form action', this.isInView);
    }

    LoadPermanencyForm(formData) {
        const data = {};
        data['permanencyplanid'] = formData.permanencyplanid
            ? formData.permanencyplanid
            : null;
        
        data['concurrentplandate'] = formData.establisheddate
            ? formData.establisheddate
            : null;
        data['primaryplandate'] = formData.establisheddate
            ? formData.establisheddate
            : null;
        // data['caseworkername']  = formData.caseworkername ?  formData.caseworkername  : null;
        data['primarypermanencytype'] =
            formData.primarypermanency &&
                formData.primarypermanency[0].permanencyplantypekey
                ? formData.primarypermanency[0].permanencyplantypekey
                : null;
        data['concurrentpermanencytype'] =
            formData.concurrentpermanency &&
                formData.concurrentpermanency[0].concurrentplantypekey
                ? formData.concurrentpermanency[0].concurrentplantypekey
                : null;
        data['primaryarrangetype'] = formData.primaryarrangetype
            ? formData.primaryarrangetype
            : null;
        data['concurrentarrangetype'] = formData.concurrentarrangetype
            ? formData.concurrentarrangetype
            : null;
        data['remarks'] = formData.primarycomments
            ? formData.primarycomments
            : null;
        data['concurrentcomments'] = formData.concurrentcomments
            ? formData.concurrentcomments
            : null;
        data['enddate'] = formData.enddate
            ? formData.enddate
            : null;
        data['reason'] = formData.reason
            ? formData.reason
            : null;
        data['parentname'] = formData.parentname
        ? formData.parentname
        : null;        
        data['achieveddate'] = formData.achieveddate ? new Date(formData.achieveddate) : null;
        

        this.permanencyPlanForm.patchValue(data);
        this.endMaxDate = new Date(data['primaryplandate']);
        const permplanquestdata = formData && formData['permplanquestdata'] ? formData['permplanquestdata'] : null;
        if (permplanquestdata) {
            this.permanencyPlanForm.patchValue({
                'isInClosedProximity': permplanquestdata.isInClosedProximity,
                'isInClosedProximityExpln': permplanquestdata.isInClosedProximityExpln,
                'meetingSafetyNeedsExpln': permplanquestdata.meetingSafetyNeedsExpln,
                'sixMonthsPlacementExpln': permplanquestdata.sixMonthsPlacementExpln,
                'courtOrdersExpln': permplanquestdata.courtOrdersExpln,
                'permToPermExpln': permplanquestdata.permToPermExpln,
                'safeAndCareExpln': permplanquestdata.safeAndCareExpln,
                'assessmentPeriodExpln': permplanquestdata.assessmentPeriodExpln,
                'lifebookExpln': permplanquestdata.lifebookExpln,
                'serviceAgreementExpln': permplanquestdata.serviceAgreementExpln,
                'isProviderAgree': permplanquestdata.isProviderAgree,
                'isProviderAgreeExpln': permplanquestdata.isProviderAgreeExpln,
                'serviceAgreementForOtherExpln': permplanquestdata.serviceAgreementForOtherExpln,
            });
        }
        const primaryFormData = this.permanencyPlanForm.getRawValue();
        if (primaryFormData.primarypermanencytype === 'Reunification') {
            this.permanencyPlanForm.controls['primarypermanencytype'].disable();
        } else {
            this.permanencyPlanForm.controls['primarypermanencytype'].enable();
        }
        if (this.isViewMode) {
            this.permanencyPlanForm.disable();
            this.isInView = true;
            this.disableEndPlan = true;
        } else {
            this.isInView = false;
        }
        if (formData.status === 'Approved') {
            this.permanencyPlanForm.disable();
            this.disableEndPlan = false;
            this.isInView = true;
            this.permanencyPlanForm.controls['enddate'].enable();
            this.permanencyPlanForm.controls['reason'].enable();
        }
    }
    loadPermanencyTypeDropDown() {
        this.commonHttpService
            .getArrayList(
                {},
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PermanencyPlan
                    .PermanancyPlanTypeSubType + '?filter={}'
            )
            .subscribe(type => {
                this.permanencyType = type;
                // Removing Auto Population of Primary permanency plan as Reunification
                // if (!this._serviceCasePermanencyPlanService.isAnyPlansExist()) {
                //     this.permanencyPlanForm.patchValue({
                //         primarypermanencytype: 'Reunification'
                //     });
                // }
            });
    }

    private getInvolvedPerson() {
        this.commonHttpService
            .getArrayList(
                {
                    page: 1,
                    method: 'get',
                    where: {objectid: this.id , objecttypekey : 'servicecase'}
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListCWUrl + '?filter'
            )
            .subscribe(res => {
                if (res['data'] && res['data'].length) {
                    this.involvedPersons = [];
                    // const avoidRoles = ['AV', 'AM', 'CHILD', 'RC', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC'];
                    res['data'].map(item => {
                        const roles = (Array.isArray(item.roles)) ? item.roles : [];
                        const parent = roles.some(role => ['PARENT', 'LG'].includes(role.intakeservicerequestpersontypekey));
                        if(parent) {
                            this.involvedPersons.push(item);
                        }
                        //  if ( !item.rolename || item.rolename === '') {

                        //     if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestpersontypekey) {
                        //         item.rolename  = item.roles[0].intakeservicerequestpersontypekey; }
                        // }
                        // if (
                        //     item.rolename === 'PARENT' || item.rolename === 'LG'
                         
                        // ) {
                        //     if (!item.intakeservicerequestactorid) {
                        //        if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestactorid) {
                        //         item.intakeservicerequestactorid  = item.roles[0].intakeservicerequestactorid; }
                        //     }
                        //     this.involvedPersons.push(item);
                        // }
                        // });
                    });
                    if(this.involvedPersons && this.involvedPersons.length) {
                    this.involvedPersons.sort((a,b)=>a.fullname.localeCompare(b.fullname));
                    }
                    }
            });
    }

    onChangePrimaryPlan(selected) {
        const formData = this.permanencyPlanForm.getRawValue();
        const concurrentPlan = formData.concurrentpermanencytype;
        if (concurrentPlan && concurrentPlan === selected) {
            this._alert.error(
                'Primary and concurrent permanency plan cant be the same.'
            );
            this.permanencyPlanForm.patchValue({ primarypermanencytype: null });
        }
        if (concurrentPlan && concurrentPlan === 'APPLA' && selected === 'CRLTC') {
            this._alert.error(
                'Primary and concurrent permanency plan cant be the same.'
            );
            this.permanencyPlanForm.patchValue({
                primarypermanencytype: null
            });
        }
        if (concurrentPlan && concurrentPlan === 'CRLTC' && selected === 'APPLA') {
            this._alert.error(
                'Primary and concurrent permanency plan cant be the same.'
            );
            this.permanencyPlanForm.patchValue({
                primarypermanencytype: null
            });
        }
        // if (selected === 'APPLA' || selected === 'CRLTC') {
        //     this.permanencyPlanForm
        //         .get('concurrentpermanencytype')
        //         .setValidators([Validators.required]);
        //         this.concurrentPlanRequired = true;
        // } else {
        //     this.permanencyPlanForm
        //         .get('concurrentpermanencytype')
        //         .clearValidators();
        //         this.concurrentPlanRequired = false;
        // }

        // concurrency plan is now mandatory for all primary concurrency plan
        this.permanencyPlanForm
            .get('concurrentpermanencytype')
            .setValidators([Validators.required]);
        this.concurrentPlanRequired = true;
    }

    onChangeConcurrentPlan(selected) {
        const formData = this.permanencyPlanForm.getRawValue();
        const permanencyPlan = formData.primarypermanencytype;
        this.onChangePrimaryDate();
        if (permanencyPlan && permanencyPlan === selected) {
            this._alert.error(
                'Primary and concurrent permanency plan cant be the same.'
            );
            this.permanencyPlanForm.patchValue({
                concurrentpermanencytype: null
            });
        }

        if (permanencyPlan && permanencyPlan === 'APPLA' && selected === 'CRLTC') {
            this._alert.error(
                'Primary and concurrent permanency plan cant be the same.'
            );
            this.permanencyPlanForm.patchValue({
                concurrentpermanencytype: null
            });
        }
        if (permanencyPlan && permanencyPlan === 'CRLTC' && selected === 'APPLA') {
            this._alert.error(
                'Primary and concurrent permanency plan cant be the same.'
            );
            this.permanencyPlanForm.patchValue({
                concurrentpermanencytype: null
            });
        }
    }

    onChangePrimaryDate() {
        const formData = this.permanencyPlanForm.getRawValue();
        const permanencyPlanDate = formData.primaryplandate;
        this.endMaxDate = new Date(permanencyPlanDate);
        if (permanencyPlanDate) {
            this.permanencyPlanForm.patchValue({
                concurrentplandate: new Date(permanencyPlanDate)
            });
        }
    }

    goBack() {
        this._serviceCasePermanencyPlanService
            .getChildRemovalInfoAndPlacements()
            .subscribe(response => {
                this._serviceCasePermanencyPlanService.broadCastPageRefresh();
                this.router.navigate(['../'], { relativeTo: this.route });
            });
    }

    submitPermanencyPlan(isreviewsubmit) {
        if (this.permanencyPlanForm.invalid) {
            console.log('invalid', this._intakeUtils.findInvalidControls(this.permanencyPlanForm));
            this._alert.error('Please fill required fields');
            return false;
        }
        const planData = this.permanencyPlanForm.getRawValue();
        const isExist = this.permanencyPlanList.some(plan => {
            const list = (Array.isArray(plan.primarypermanency)) ? plan.primarypermanency : [];
            const isPresent = list.some(item => {
                return ((((item.permanencyplantypekey === planData.primarypermanencytype) && !plan.enddate)
                    || ((item.permanencyplantypekey === planData.primarypermanencytype) && plan.status !== 'Approved')) && plan.status !== 'Rejected');
            });
            return isPresent;
        });
        const isAllowed = (this.formAction === 'Edit') ? true : (isExist) ? false : true;
        if (isAllowed) {
            const permplanquestdata = {
                'isInClosedProximity': planData.isInClosedProximity,
                'isInClosedProximityExpln': planData.isInClosedProximityExpln,
                'meetingSafetyNeedsExpln': planData.meetingSafetyNeedsExpln,
                'sixMonthsPlacementExpln': planData.sixMonthsPlacementExpln,
                'courtOrdersExpln': planData.courtOrdersExpln,
                'permToPermExpln': planData.permToPermExpln,
                'safeAndCareExpln': planData.safeAndCareExpln,
                'assessmentPeriodExpln': planData.assessmentPeriodExpln,
                'lifebookExpln': planData.lifebookExpln,
                'serviceAgreementExpln': planData.serviceAgreementExpln,
                'isProviderAgree': planData.isProviderAgree,
                'isProviderAgreeExpln': planData.isProviderAgreeExpln,
                'serviceAgreementForOtherExpln': planData.serviceAgreementForOtherExpln,
            };
            planData.permplanquestdata = permplanquestdata;
            this.selectedChildren = this._serviceCasePermanencyPlanService.selectedChildren;
            if (
                (!this.selectedChildren || !this.selectedChildren.length) &&
                !planData.permanencyplanid
            ) {
                this._alert.error('Please Select Child');
                return;
            }

            if (!this.permanencyPlanForm.valid) {
                this._alert.error('Please fill all required fields');
                return;
            }

            if (
                planData.primarypermanencytype === 'APPLA' &&
                !planData.concurrentpermanencytype
            ) {
                this.errorMessage = 'Concurrent Permanency is must for APPLA';
                (<any>$('#validation-error')).modal('show');
                return;
            }

            // if (planData.concurrentpermanencytype && planData.primaryplandate !== planData.concurrentplandate) {
            //   this._alert.error('Primary plandate and concurrent date should be same');
            //   return;
            // }


            if (
                !this._serviceCasePermanencyPlanService.isAnyPlansExist() &&
                planData.primarypermanencytype !== 'Reunification'
            ) {
                // Removing Reunification must be the primary permanency plan Validation

                // this.errorMessage = 'Reunification must be the primary permanency plan for the first entry';
                // (<any>$('#validation-error')).modal('show');
                // return;
            }
            if (planData.primarypermanencytype === 'APPLA' || planData.primarypermanencytype === 'CRLTC') {
                // Todo make it false below for validation
                let isValidAgeForAllChildCount = 0;
                this.selectedChildren.forEach(child => {
                    console.log(child.dob);
                    const age = moment().diff(child.dob, 'years', true);
                    console.log('age', age);
                    if (age >= 16) {
                        isValidAgeForAllChildCount++;
                    }

                });

                if (isValidAgeForAllChildCount !== this.selectedChildren.length) {
                    this.errorMessage = 'APPLA should not be used as a permanency plan for child/children under the age of 16 years';
                    (<any>$('#validation-error')).modal('show');
                    return;
                }

            }

            planData['isreviewsubmit'] = isreviewsubmit;
            this._serviceCasePermanencyPlanService.sendApprovalInQueue(planData);
        } else {
            this._alert.warn('Active Permanency plan already exist.');
        }
    }
}
