import { Component, OnInit } from '@angular/core';
import { ServiceCasePermanencyPlanService } from '../service-case-permanency-plan.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { AuthService, AlertService, DataStoreService, CommonHttpService, SessionStorageService } from '../../../../../@core/services';
import { AppConstants } from '../../../../../@core/common/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PaginationInfo } from '../../../../../@core/entities/common.entities';
import { ColumnSortedEvent } from '../../../../../shared/modules/sortable-table/sort.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'permanency-list',
  templateUrl: './permanency-list.component.html',
  styleUrls: ['./permanency-list.component.scss']
})
export class PermanencyListComponent implements OnInit {
  placementList = [];
  selectedPlan: any;
  selectedPlanid: string;
  isCaseWorker = false;
  isViewQues = false;
  isSuperVisor = false;
  selectedPlanforReview: any;
  permanencyPlanQuestionnaireForm: FormGroup;
  isInClosedProximity: any;
  isInClosedProximityExpln: any;
  meetingSafetyNeedsExpln: any;
  sixMonthsPlacementExpln: any;
  courtOrdersExpln: any;
  permToPermExpln: any;
  safeAndCareExpln: any;
  assessmentPeriodExpln: any;
  lifebookExpln: any;
  serviceAgreementExpln: any;
  isProviderAgree: any;
  isProviderAgreeExpln: string;
  serviceAgreementForOtherExpln: string;
  isReviewPlan: boolean;
  permanencyPlanList: any[];
  permanencyPlan: any;
  public updatePermanency$ = new Subject<any>();
  planHistory: any;
  historyTotalRecords: any;
  historyPaginationInfo: PaginationInfo = new PaginationInfo();
  historyRequest: any;
  fromDate: any;
  toDate: any;
  worker: any;
  caseworkerList: any;
  updatedby: any;
  permanancyPlanDueList: any;

  maxDate: Date = new Date();
  toMinDate: Date = null;
  isClosed = false;
  constructor(
    private _serviceCasePermanencyPlanService: ServiceCasePermanencyPlanService,
    private router: Router,
    private route: ActivatedRoute,
    private _authService: AuthService,
    private _alert: AlertService,
    private _router: Router,
    private formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private storage: SessionStorageService
  ) { }

  ngOnInit() {
    this.selectedPlanid = null;
    this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
    this.isSuperVisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this._serviceCasePermanencyPlanService.getChildRemovalInfoAndPlacements();
    this.permanencyPlanList = this._serviceCasePermanencyPlanService.permanencyPlanList;
    this.permanancyPlanDueList = [];
    if (this.permanencyPlanList && this.permanencyPlanList.length) {
      this.permanencyPlanList.forEach(child => {
        if (child.permanencyplans && child.permanencyplans.length) {
          let notReviewed = false;
          child.permanencyplans.forEach(plan => {
            let days = 0;
            if (plan.establisheddate) {
            days = this.diffbetweenDays(new Date(plan.establisheddate), new Date());
            }
            if (plan.status === 'Review' && days > 180) {
              notReviewed = true;
            }
          });
          if (notReviewed) {
             this.permanancyPlanDueList.push(child.clientname);
          }
        }

      });
      if (this.permanancyPlanDueList && this.permanancyPlanDueList.length && !this.isSuperVisor) {
        (<any>$('#permanancy-plan-alert')).modal('show');
      }
    }

    if (this.permanencyPlanList) {
      this.permanencyPlan = this.permanencyPlanList[0];
    }

    this.forminit();
    this._serviceCasePermanencyPlanService.refresh$.subscribe(_ => {
      this.permanencyPlanList = this._serviceCasePermanencyPlanService.permanencyPlanList;
      //   if(this.isSuperVisor) {
      //   const reviewPlans = this.permanencyPlanList.map(data=>{
      //   return this.isReviewPlanExists(data);
      //   });
      //   console.log("Review", reviewPlans);
      // if(reviewPlans && reviewPlans.length) {
      //   this.isReviewPlan = true;
      // }
      // }
    });
    this.permanencyPlanList.map(res => {
      return res.fullname =  res.firstname + ' ' + res.middlename + ' ' + res.lastname + ' ' ;
    });
    this._serviceCasePermanencyPlanService.placementApprovalQueue$.subscribe(data => {
      this._alert.success(data);
      this.goBack();
    });
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

   diffbetweenDays(date1, date2) {

      const diffc = date2.getTime() - date1.getTime();
      const days = Math.round(Math.abs(diffc / (1000 * 60 * 60 * 24)));
      return days;

   }
  viewQuesData(plan) {
    if (plan && plan.permplanquestdata && plan.permanencyplanid) {
      this.isViewQues = true;
      this.selectedPlanid = plan.permanencyplanid;
      this.permanencyPlanQuestionnaireForm.patchValue(plan.permplanquestdata);
      this.permanencyPlanQuestionnaireForm.disable();
    }
  }


  resetQuestForm() {
    this.selectedPlanid = null;
    this.isViewQues = false;
    this.permanencyPlanQuestionnaireForm.reset();
    this.permanencyPlanQuestionnaireForm.enable();
  }

  forminit() {
    this.permanencyPlanQuestionnaireForm = this.formBuilder.group({
      isInClosedProximity: [null],
      isInClosedProximityExpln: [null],
      meetingSafetyNeedsExpln: [null],
      sixMonthsPlacementExpln: [null],
      courtOrdersExpln: [null],
      permToPermExpln: [null],
      safeAndCareExpln: [null],
      assessmentPeriodExpln: [null],
      lifebookExpln: [null],
      serviceAgreementExpln: [null],
      isProviderAgree: [null],
      isProviderAgreeExpln: [null],
      serviceAgreementForOtherExpln: [null],
    });
  }


  addPermanency() {
    this._serviceCasePermanencyPlanService.setViewMode(false);
    if (this._serviceCasePermanencyPlanService.selectedChildren && this._serviceCasePermanencyPlanService.selectedChildren.length) {

      const childList = this._serviceCasePermanencyPlanService.selectedChildren.map(child => child.personid);

      const plan = this.permanencyPlanList.filter(item => childList.includes(item.personid));
      const hasActiveCase = plan.some(item => {
        const permanencyPlan = (Array.isArray(item.permanencyplans)) ? item.permanencyplans : [];
        const activeCase = permanencyPlan.some(pp => {
          const casedetails = Array.isArray(pp.casedetails) ? pp.casedetails : [];
          const res = casedetails.some(ele => ele.adoptioncase_status === 1);
          return res;
          // (pp.casedetails && pp.casedetails.adoptioncase_status === 1)
        });
        return activeCase;
      });
      const hasActivePlan = plan.some(item => {
        const permanencyPlan = (Array.isArray(item.permanencyplans)) ? item.permanencyplans : [];
        const activeCase = permanencyPlan.some(pp => {
          return pp.enddate === null; // if there is not enddate, consider this as active.
        });
        return activeCase;
      });
      if (hasActiveCase) {
        (<any>$('#active-case-exist')).modal('show');
        // this._alert.warn('Active Adoption case exits, cannot add new permanency plan');
      } else if (hasActivePlan) {
        (<any>$('#active-permenency-plan-exist')).modal('show');
      } else {
        this._serviceCasePermanencyPlanService.toggleFormAction('Add');
        this.router.navigate(['form'], { relativeTo: this.route });
      }
    } else {
      this._alert.warn('Please Select Child');
    }
  }

  editPermanency(plan, child, isView) {
    this._serviceCasePermanencyPlanService.toggleFormAction('Edit', plan);
    child.permanencyplans = [];
    child.permanencyplans.push(plan);
    this._serviceCasePermanencyPlanService.setViewMode(isView);
    this._serviceCasePermanencyPlanService.setSelectedChild(child);
    setTimeout(() => {
      this.router.navigate(['form'], { relativeTo: this.route });
    }, 200);

  }

  savePermanencyPlanQuestionnaire() {
    if (this.permanencyPlanList) {
      if (this.permanencyPlanList[0].permanencyplans) {
        this.permanencyPlan = this.permanencyPlanList[0].permanencyplans[0];
        console.log('permanencyplan', this.permanencyPlan);
        const permanencyPlanData = this.permanencyPlanQuestionnaireForm.getRawValue();
        this.permanencyPlan.permplanquestdata = JSON.stringify(permanencyPlanData);
        this._serviceCasePermanencyPlanService.saveOrUpdatePermanencyPlan(this.permanencyPlan);
      }
    }
  }

  viewPlan(plan) {
    this.selectedPlan = plan;
    (<any>$('#viewPlan')).modal('show');
  }

  isPlanSubmitted(planData) {
    if (planData && planData.permanencyplans && planData.permanencyplans.length) {
      const plans = planData.permanencyplans.filter((item) => {
        if (item.status === 'Review' || item.status === 'Approved') {
          return item;
        }
      });
      return (plans && plans.length) ? plans.length : 0;
    }
    return false;
  }

  isPlanReview(planData) {

    if (planData && planData.permanencyplans && planData.permanencyplans.length) {
      const plans = planData.permanencyplans.filter((item) => {
        if (item.status === 'Review') {
          return item;
        }
      });
      return (plans && plans.length) ? plans.length : 0;
    }
    return false;
  }

  addChild() {
    if (this.isSuperVisor) {
      this._serviceCasePermanencyPlanService.resetValues();
    }
    this.permanencyPlanList.map(data => {
      this.isReviewPlanExists(data);
    });
    if (this._serviceCasePermanencyPlanService.selectedChildren && this._serviceCasePermanencyPlanService.selectedChildren.length) {
      (<any>$('#review-plan')).modal('show');
    }
  }

  reviewPermanency(plan, child) {
    this.selectedPlanforReview = [];
    this.selectedPlanforReview.push(child);
    this.selectedPlanforReview[0].permanencyplans = [];
    this.selectedPlanforReview[0].permanencyplans.push(plan);
    (<any>$('#review-plan')).modal('show');
  }

  approveorRejectPlacement(status) {
    let permanencyPlanId = null;
    if (this.selectedPlanforReview[0] && this.selectedPlanforReview[0].permanencyplans && this.selectedPlanforReview[0].permanencyplans.length) {
      permanencyPlanId = this.selectedPlanforReview[0].permanencyplans[0].permanencyplanid;
      this._serviceCasePermanencyPlanService.approveorRejectPlacement(permanencyPlanId, status);
    }
  }

  checkIfReviewPlanExist() {
    let ifPlanExist = false;
    this.permanencyPlanList.map(data => {
      if (data && data.permanencyPlanList && data.permanencyPlanList.length) {
        data.permanencyplans.filter((item) => {
          if (item.status === 'Review') {
            ifPlanExist = true;
          }
        });
      }
    });
    return ifPlanExist;

  }

  isReviewPlanExists(planData) {
    if (planData && planData.permanencyplans && planData.permanencyplans.length) {
      const plans = planData.permanencyplans.filter((item) => {
        if (item.status === 'Review') {
          return item;
        }
      });
      console.log(plans);
      if (plans && plans.length && this.isSuperVisor) {
        this._serviceCasePermanencyPlanService.selectChild(planData, true);
      }
      return (plans && plans.length) ? plans : null;
    }
    return null;
  }



  goBack() {
    this.resetQuestForm();
    this._serviceCasePermanencyPlanService.getChildRemovalInfoAndPlacements().subscribe(response => {
      this._serviceCasePermanencyPlanService.broadCastPageRefresh();
      (<any>$('#review-plan')).modal('hide');
      this.router.navigate(['../'], { relativeTo: this.route });
    });
  }

  approveorReject(selectedPlan, action) {
    if (selectedPlan && selectedPlan.permanencyplanid) {
      this._serviceCasePermanencyPlanService.approveOrRejectPlacement(selectedPlan.permanencyplanid, action);
    }
  }

  approve() {
    this.resetQuestForm();
    this._serviceCasePermanencyPlanService.startPlacementApprovalInQueue();
  }

  reject() {
    this.resetQuestForm();
    this._serviceCasePermanencyPlanService.startPlacementRejectionInQueue();
  }

  routeToPlan(plan, child) {
    this._dataStoreService.setData('placed_child', child);
    this.storage.setObj('placed_child', child);
    this._dataStoreService.setData('adoptionAgreement', null);
    this._dataStoreService.setData('adoptionEffort', null);
    this._dataStoreService.setData('TPR_LIST', null);
    this._dataStoreService.setData('TPR_RECOMENDATION_LIST', null);
    this._dataStoreService.setData(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID, null);
    
    
    
    
    if (child.permanencyplans.length > 0) {
      this._dataStoreService.setData('placement_child', plan);
      this.storage.setObj('placement_child', plan);
    }

    const selectedPlacedChild = this._serviceCasePermanencyPlanService.selectedPlacedChild(child.cjamspid);
    if (selectedPlacedChild && selectedPlacedChild.length && selectedPlacedChild[0]['placements'] && selectedPlacedChild[0]['placements'].length) {
      const placedChild = selectedPlacedChild[0]['placements'];

      placedChild.forEach(placement => {
        if (placement.placementtypekey && placement.placementtypekey !== 'LA' && !placement.enddate) {
          placement.permanencyplanid = plan.permanencyplanid;
          // selectedPlacedChild[0]['placements'][0].permanencyplanid = plan.permanencyplanid;
          this._dataStoreService.setData(CASE_STORE_CONSTANTS.PLACEMENT_CHILD, placement);
          this.storage.setObj('placement_child', placement);
        }
      });


    }
    this._dataStoreService.setData('childforGAP', child.cjamspid);
    if (plan.primarypermanency && plan.primarypermanency.length > 0) {
      const primary = plan.primarypermanency[0];
      let url = '';
      if (primary.permanencyplantypekey === 'Guardianship' || primary.permanencyplantypekey === 'GUARDR') { // Garudian non relative or Garudian by relative
        // this._dataStoreService.setData('permanencyPlan', plan, true);
        // this._dataStoreService.setData('placement_child', placements);
        this._dataStoreService.setData(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID, plan.permanencyplanid);
        url = 'placement/placement-gap/application';
      } else if (primary.permanencyplantypekey === 'ADOPTR' || primary.permanencyplantypekey === 'ADOPTNR' || primary.permanencyplantypekey === 'Adoption') {
        // this._dataStoreService.setData('permanencyPlan', plan, true);
        // this._dataStoreService.setData('placement_child', placements);
        this._dataStoreService.setData(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID, plan.permanencyplanid);
        url = 'placement/adoption/tpr-recom';
      } else if (primary.permanencyplantypekey === 'CRLTC' || primary.permanencyplantypekey === 'APPLA') {
        url = 'placement/appla/list';
      }
      if (url !== '') {
        this._router.navigate([url], { relativeTo: this.route });
      }
    }
  }

  showHistory(child) {
    this.getCaseWorkerList();
    (<any>$('#history-list')).modal('show');
    this.historyRequest = {
      'intakeservicerequestactorid': child.intakeservicerequestactorid,
      'personid': child.personid,
      'sortcol': this.historyPaginationInfo.sortColumn,
      'sortby': this.historyPaginationInfo.sortBy,
      'updatedfrom': this.fromDate,
      'updatedto': this.toDate,
      'updatedby': this.worker
    };

    this.loadHistory(1);
  }



  loadHistory(pageNumber) {
    this.historyRequest.updatedfrom = this.fromDate;
    this.historyRequest.updatedto = this.toDate;
    this.historyRequest.updatedby = this.updatedby;
    this.historyRequest.sortby = this.historyPaginationInfo.sortBy;
    this.historyRequest.sortcol = this.historyPaginationInfo.sortColumn;

    this._serviceCasePermanencyPlanService.getPermanencyPlanHistory(pageNumber, this.historyRequest).subscribe(result => {
      if (result.data && result.data.length) {
        this.historyTotalRecords = result.count;
        this.planHistory = result.data;
      } else {
        this.historyTotalRecords = 0;
        this.planHistory = [];
      }
    });
  }

  clearSearch() {
    this.fromDate = null;
    this.toDate = null;
    this.updatedby = null;
    this.toMinDate = null;
    this.maxDate = new Date();
    this.loadHistory(1);
  }

  getCaseWorkerList() {
    this._commonHttpService
      .getPagedArrayList(
        {
          where: { appevent: 'ALL' },
          method: 'post'
        },
        'Intakedastagings/getroutingusers'
      ).subscribe(result => {
        this.caseworkerList = result.data;
      });
  }

  historyPageChanged(event: any) {
    this.historyPaginationInfo.pageNumber = event.page;
    this.loadHistory(this.historyPaginationInfo.pageNumber);
  }

  onHistorySorted($event: ColumnSortedEvent) {
    this.historyPaginationInfo.pageNumber = 1;
    this.historyPaginationInfo.sortBy = $event.sortDirection;
    this.historyPaginationInfo.sortColumn = $event.sortColumn;
    this.loadHistory(this.historyPaginationInfo.pageNumber);
  }

  fromDateChanged(fromDate: string) {
    console.log('from date', fromDate);
    this.toMinDate = new Date(fromDate);
  }
  toDateChanged(toDate: string) {
    console.log('from date', toDate);
    this.maxDate = new Date(toDate);
  }

}
