import { Injectable } from '@angular/core';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { AuthService, DataStoreService, CommonHttpService } from '../../../../@core/services';
import { Subject } from 'rxjs/Subject';
import { ErrorInfo } from '../../../../@core/common/errorDisplay';
import { AppConstants } from '../../../../@core/common/constants';
import { PlacementConstants } from '../service-case-placements/constants';
const LIVING_ARRANGEMENT = 'LA';
const VOID_PLACEMENT = 1;
const RESPONSE_ACCEPTED_YES = '4612';
const RESPONSE_REJECTED = 'Rejected';
const RESPONSE_APPROVED = 'Approved';

@Injectable()
export class ServiceCasePermanencyPlanService {

  removedChildList = [];
  placementList = [];
  permanencyPlanList: any;
  selectedChildren = [];
  placementDetails = [];
  formAction = 'Add';
  // For Edit Load
  selectedPermanencyPlan: any;
  isCaseWorker = false;
  isSuperVisor = false;
  error: ErrorInfo = new ErrorInfo();
  isViewMode: boolean;
  queueIndex = 0;
  public placementApprovalQueue$ = new Subject<any>();
  public refresh$ = new Subject<any>();
  public childSelection$ = new Subject<any>();
  public updatePermanencyList$ = new Subject<any>();


  constructor(private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService) {
    this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
    this.isSuperVisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this.isViewMode = false;
  }

  toggleFormAction(action, data?) {
    this.formAction = action;
    if (data) {
      this.selectedPermanencyPlan = data;
    }
  }

  setSelectedChild(child) {
    this.selectedChildren = [];
    this.selectedChildren.push(child);
  }

  setViewMode(isViewMode) {
    this.isViewMode = isViewMode;
  }
  getChildRemoval() {
    return this._commonService
      .getSingle(
        {
          where: { objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID), 'objecttypekey': 'servicecase' },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .GetChildRemovalList + '?filter'
      );
  }

  resetValues() {
    this.selectedChildren = [];
  }

  getPlacementInfoList(pageNumber, limit) {
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: limit,
          method: 'get',
          where: { servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
        }),
        'placement/getplacementbyservicecase?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );
  }

  getPermanencyPlanList(pageNumber, limit) {
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: limit,
          method: 'get',
          where: { objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
        }),
        'permanencyplan/list?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );


  }

  getPermanencyPlanHistory(pageNumber: number, requestParam: any) {
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: 10,
          method: 'get',
          where: requestParam,
        }),
        'permanencyplanhistory/list?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );


  }

  getChildRemovalInfoAndPlacements() {
    this.resetValues();
    return forkJoin([this.getPlacementInfoList(1, 10), this.getChildRemoval(), this.getPermanencyPlanList(1, 10)])
      .map(([children, childRemoval, permanencyPlanList]) => {
        if (children && children.data) {
          children.data.forEach(child => {
            child.hasAlert = this.checkForChildHasAlert(child.placements);
            child.hasActivePlacement = this.checkForChildHasActivePlacements(child.placements);
            child.placements.map(placement => {
              return this.processForPlacementActions(placement);
            });
          });
          // Display all child card 
          // const validPlacement = children.data && children.data.length ? children.data.filter(placement => placement.hasActivePlacement === true) : [];
          // this.placementList =validPlacement;
          this.placementList =  children.data;
        }
        if (childRemoval) {
          this.removedChildList = childRemoval;          
          if (this.placementList && this.placementList.length > 0) {
            this.placementList.forEach(data => {
              const isavailableatremoval = (this.removedChildList && this.removedChildList.length > 0) ? this.removedChildList.find(ele => ele.personid === data.personid) : null;
              data.isAvailinRemoval = isavailableatremoval ? true : false;
            });
            this.placementList = this.placementList.filter(ele => ele.isAvailinRemoval)
          }
        } else {
          this.placementList = [];
        }
        if (permanencyPlanList) {
          permanencyPlanList.forEach(item => {
            const childPlacment = this.placementList.find(placment => item.personid === placment.personid);
            item.placements = childPlacment ? childPlacment.placements : [];
            const removalList = this.removedChildList.filter(child => child.personid === item.personid);
            item.removalList = removalList;
          });
         
          this.permanencyPlanList = permanencyPlanList;
        }
        // this.processRemovedChildListStatus();
        return { placements: this.placementList, removedChildList: this.removedChildList };
      });
  }

  checkForChildHasAlert(placements) {
    let hasAlert = false;
    const runAwayPlacement = placements.find(placement => placement.livingarrangementtypekey === PlacementConstants.RUN_AWAY);
    if (runAwayPlacement) {
      hasAlert = true;
    }
    return hasAlert;
  }
  checkForChildHasActivePlacements(placements) {
    let hasActivePlacement = false;
    if (placements && placements.length) {
      const providerPlacement = placements.filter(placement => placement.placementtypekey !== LIVING_ARRANGEMENT 
        && (placement.enddate === null || (placement.revisionupdate && placement.revisionupdate.enddate))
        && (placement.isvoided !== VOID_PLACEMENT || (placement.revisionupdate && placement.revisionupdate.isvoided))
        && placement.responseacceptedkey === RESPONSE_ACCEPTED_YES
        && (placement.routingstatus === RESPONSE_APPROVED
          || (placement.revisionupdate &&  ( placement.revisionupdate.enddate || placement.revisionupdate.isvoided)) ));
      if (providerPlacement && providerPlacement.length) {
        hasActivePlacement = true;
      }
    }
   // return false; // for dev check
    return hasActivePlacement;
  }

  selectedPlacedChild(childId) {
    return this.placementList.filter(placement => placement.cjamspid === childId);
  }

  broadCastPageRefresh() {
    this.refresh$.next('refresh');
  }

  updatePermanency(list) {
    this.updatePermanencyList$.next(list);
  }

  processRemovedChildListStatus() {
    this.resetValues();
    if (this.placementList && this.placementList.length) {
      this.removedChildList.map(removedChild => {

        const childPlaced = this.placementList.find(placedChild => placedChild.personid === removedChild.personid);

        if (childPlaced && childPlaced.placements) {

          childPlaced.placements.map(placement => {
            placement.placementTypeDesc = this.getPlacementTypeDescription(placement.placementtypekey);
          });
          removedChild.hasActivePlacement = this.checkForChildHasActivePlacements(childPlaced.placements);
          const reviewPlacement = childPlaced.placements.find(placement => placement.routingstatus === 'Review');
          if (reviewPlacement) {
            reviewPlacement.fullAddress = this.processAddress(reviewPlacement);
            removedChild.placementStatus = reviewPlacement.routingstatus;
            removedChild.placementType = reviewPlacement.placementtypekey;
            removedChild.placement = reviewPlacement;
            // removedChild.isPlaced = true;
            removedChild.isSelected = true;
            if(removedChild.hasActivePlacement) {
            this.selectedChildren.push(removedChild);
            }
          }


        }
      });
    }
  }

  processAddress(placement) {
    const fullAddress = `${placement.address1} , ${placement.address2} , ${placement.cityname}, ${placement.statetypekey}, ${placement.zipcode}`;
    return fullAddress;
  }

  processForPlacementActions(placement) {
    if (placement.routingstatus === 'Approved' && this.isCaseWorker) {
      placement.isEditable = true;
    } else {
      placement.isEditable = false;
    }
    if (placement.enddate || placement.voidreasontypekey) {
      placement.isEditable = false;
    }
    return placement;
  }

  getPlacementTypeDescription(type) {
    switch (type) {
      case 'LA':
        return 'Living Arrangement';
      default:
        return 'Provider Placement';
    }
  }

  selectChild(selectedChild, selection) {
    selectedChild.isSelected = selection;
    if (selection) {
      this.selectedChildren.push(selectedChild);
    } else {
      this.selectedChildren = this.selectedChildren.filter(child => child.personid !== selectedChild.personid);
    }
    this.childSelection$.next('SELECTED');
    console.log(selection, 'selected', this.selectedChildren);

  }

  getSelectedChildren() {
    return this.selectedChildren ? this.selectedChildren : [];
  }

  sendApprovalInQueue(data) {
    this.queueIndex = 0;
    this.sendForApproval(data);
  }

  getPropertyFromChildRemoval(propertyName) {
    if (this.selectedChildren[this.queueIndex]) {
      if (this.formAction === 'Add' && this.selectedChildren[this.queueIndex].placements && this.selectedChildren[this.queueIndex].placements.length) {
        // const childRemoval = this.selectedChildren[this.queueIndex].placements[0];
        const placedChild = this.selectedChildren[this.queueIndex].placements;
        let propertyValue = null;
        placedChild.forEach(placement => {
          if (placement.placementtypekey && placement.placementtypekey !== "LA" && !placement.enddate) {
            const childRemoval = placement;
            propertyValue = childRemoval[propertyName];
          }
        });

        return propertyValue;

      } else if (this.selectedChildren[this.queueIndex].permanencyplans && this.selectedChildren[this.queueIndex].permanencyplans.length) {
        const childRemoval = this.selectedChildren[this.queueIndex].permanencyplans[0];
        return childRemoval[propertyName];
        // const placedChild = this.selectedChildren[this.queueIndex].permanencyplans;
        // let propertyValue = null;
        // placedChild.forEach(placement => {
        //   if(placement.placementtypekey && placement.placementtypekey !== "LA" && !placement.enddate) {
        //     const childRemoval =  placement;
        //     propertyValue =  childRemoval[propertyName];
        //   }
        // });
        // return propertyValue;
      } else {
        return null;
      }
    } else {
      return null;
    }

  }

  sendForApproval(formData) {
    if (this.isQueueAvailable()) {
      formData.servicecaseid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
      // formData.intakeserviceid = this.getPropertyFromChildRemoval('intakeserviceid');
      formData.intakeserviceid = null;
      formData.intakeservicerequestactorid = this.getPropertyFromChildRemoval('intakeservicerequestactorid');
      formData.placementid = this.getPropertyFromChildRemoval('placementid');
      console.log('send for approval', formData);

      this._commonService
        .create(formData, 'permanencyplan/add')
        .subscribe(response => {
          console.log('la inserted', response);
          this.queueIndex++;
          if (this.isQueueAvailable()) {
            this.sendForApproval(formData);
          } else {
            console.log('queue completed after success');
            const msg = formData.isreviewsubmit ? ' Permanency plan submitted for supervisor approval successfully!' : ' Permanency plan saved successfully!';
            this.placementApprovalQueue$.next(msg);
            this.getChildRemovalInfoAndPlacements();
            this.resetValues();
          }
        });


    } else {
      console.log('queue completed outside');
    }

  }

  saveOrUpdatePermanencyPlan(formData) {
    this._commonService
      .create(formData, 'permanencyplan/add')
      .subscribe(response => {
        console.log('la inserted', response);
      });
  }

  isQueueAvailable() {
    return this.queueIndex < this.selectedChildren.length;
  }

  isApproveRejectionQueueAvailable() {
    return this.queueIndex < this.selectedChildren.length;
  }

  extractPlacementId(queueIndex) {
    const permanencyplans = (this.selectedChildren && this.selectedChildren[queueIndex].permanencyplans) ? this.selectedChildren[queueIndex].permanencyplans : null;
    if (permanencyplans) {
      return permanencyplans.map(plan => {
        if (plan.status === 'Review') {
          return plan.permanencyplanid;
        }
      });
    } else {
      return null;
    }
  }

  approvePlacements(placementId) {
    return this.approveOrRejectPlacement(placementId, 'Approved');
  }

  rejectPlacements(placementId) {
    return this.approveOrRejectPlacement(placementId, 'Rejected');
  }

  approveOrRejectPlacement(placementId, status) {
    const data = {
      'objectid': placementId,
      'eventcode': 'PPLR',
      'status': status,
      'comments': 'Permanency Plan ' + status,
      'notifymsg': 'Permanency Plan ' + status,
      'routeddescription': 'Permanency Plan ' + status
    };

    return this._commonService
      .create(data, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.approveOrReject);
    //this.resetValues();
    // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.approveOrReject
  }



  startPlacementApprovalInQueue() {
    this.queueIndex = 0;
    this.startPlacementApproval();
  }

  startPlacementApproval() {
    if (this.isApproveRejectionQueueAvailable()) {
      const availablePlans = this.extractPlacementId(this.queueIndex);
      if (availablePlans && availablePlans.length) {
        availablePlans.map(placementId => {
          if (placementId) {
            this.approvePlacements(placementId).subscribe(response => {
              this.queueIndex++;
              if (this.isApproveRejectionQueueAvailable()) {
                this.startPlacementApproval();
              } else {
                this.placementApprovalQueue$.next('Permanency Plan Approved Successfully');
              }
            });
          }
        });
      }
    }

  }

  startPlacementRejectionInQueue() {
    this.queueIndex = 0;
    this.startPlacementRejection();
  }

  approveorRejectPlacement(permanencyPlanId, status) {
    if (status === 'Approved') {
      this.approvePlacements(permanencyPlanId).subscribe(response => {
        this.placementApprovalQueue$.next('Permanency Plan Approved Successfully');
      });
    } else if (status === 'Rejected') {
      this.rejectPlacements(permanencyPlanId).subscribe(response => {
        this.placementApprovalQueue$.next('Permanency Plan Rejected Successfully');
      });
    }
  }

  startPlacementRejection() {
    if (this.isApproveRejectionQueueAvailable()) {
      this.extractPlacementId(this.queueIndex).map(placementId => {
        this.rejectPlacements(placementId).subscribe(response => {
          this.queueIndex++;
          if (this.isApproveRejectionQueueAvailable()) {
            this.startPlacementRejection();
          } else {
            this.placementApprovalQueue$.next('Placement Rejected Successfully');
          }
        });
      });
    }
  }

  childHasAnyPermanencyPlans(child) {
    if (this.permanencyPlanList.length === 0) {
      return false;
    }

    const childPlans = this.permanencyPlanList.find(plan => plan.cjamspid === child.cjamspid);

    if (!childPlans) {
      return false;
    } else {
      return true;
    }

  }

  isAnyPlansExist() {
    let isAnyPlanExist = false;
    this.selectedChildren.forEach(child => {
      isAnyPlanExist = this.childHasAnyPermanencyPlans(child);
    });
    return isAnyPlanExist;
  }



}
