import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceCasePermanencyPlanRoutingModule } from './service-case-permanency-plan-routing.module';
import { ServiceCasePermanencyPlanComponent } from './service-case-permanency-plan.component';
import { ChildWrapperComponent } from './child-wrapper/child-wrapper.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { MatMenuModule, MatRadioModule, MatFormFieldModule } from '@angular/material';

import { ServiceCasePermanencyPlanService } from './service-case-permanency-plan.service';
import { ServiceCasePermanencyPlanResolverService } from './service-case-permanency-plan-resolver.service';
import { PermanencyPlanFormComponent } from './permanency-plan-form/permanency-plan-form.component';
import { PermanencyListComponent } from './permanency-list/permanency-list.component';
import { PaginationModule } from 'ngx-bootstrap';
import { SortTableModule } from '../../../../shared/modules/sortable-table/sortable-table.module';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  imports: [
    CommonModule,
    ServiceCasePermanencyPlanRoutingModule,
    FormMaterialModule,
    MatMenuModule,
    MatRadioModule,
    MatFormFieldModule,
    PaginationModule,
    SortTableModule,
    NgSelectModule
  ],
  declarations: [ServiceCasePermanencyPlanComponent, ChildWrapperComponent, PermanencyPlanFormComponent, PermanencyListComponent],
  providers: [ServiceCasePermanencyPlanResolverService, ServiceCasePermanencyPlanService

    ]
})
export class ServiceCasePermanencyPlanModule { }
