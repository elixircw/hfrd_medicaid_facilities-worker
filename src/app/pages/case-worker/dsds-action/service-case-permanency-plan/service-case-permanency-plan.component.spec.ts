import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceCasePermanencyPlanComponent } from './service-case-permanency-plan.component';

describe('ServiceCasePermanencyPlanComponent', () => {
  let component: ServiceCasePermanencyPlanComponent;
  let fixture: ComponentFixture<ServiceCasePermanencyPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceCasePermanencyPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCasePermanencyPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
