import { Component, OnInit } from '@angular/core';
import { PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { ServiceCasePermanencyPlanService } from './service-case-permanency-plan.service';
import { PlacementConstants } from '../service-case-placements/constants';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { DsdsService } from '../_services/dsds.service';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
  selector: 'service-case-permanency-plan',
  templateUrl: './service-case-permanency-plan.component.html',
  styleUrls: ['./service-case-permanency-plan.component.scss']
})
export class ServiceCasePermanencyPlanComponent implements OnInit {

  childList = [];
  permanencyPlanList = [];
  accountpayableList = [];
  placementList = [];
  selectedChildren: any[];
  selectedChild: any;
  id: string;
  permanencyType: any;
  primaryPlanSubType: any;
  isAppla: boolean;
  permanencyPlanForm: FormGroup;
  personsInvolved: any;
  placementchk: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  constructor(private _serviceCasePermanencyPlanService: ServiceCasePermanencyPlanService,
    private commonHttpService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _alert: AlertService,
    private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);

    this.childList = this._serviceCasePermanencyPlanService.placementList;
    this._serviceCasePermanencyPlanService.refresh$.subscribe(_ => {
      this.childList = this._serviceCasePermanencyPlanService.placementList;
      this.permanencyPlanList = this._serviceCasePermanencyPlanService.permanencyPlanList;
    });
    this.selectedChildren = this._serviceCasePermanencyPlanService.selectedChildren;

    this.loadInvolvedPersons();
  }


  private loadInvolvedPersons() {

    this.commonHttpService
        .getPagedArrayList(
            {
                method: 'get',
                where: { intakeservreqid: this.id }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
        )
        .subscribe(res => {
            this.personsInvolved = res.data;
        });
}




}
