import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule, MatDatepickerModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatRadioModule, MatSelectModule } from '@angular/material';

import { ScPlacementGapRoutingModule } from './sc-placement-gap-routing.module';
import { ScPlacementGapComponent } from './sc-placement-gap.component';
import { ScDisclosureChecklistComponent } from './sc-disclosure-checklist/sc-disclosure-checklist.component';
import { ScAgreementComponent } from './sc-agreement/sc-agreement.component';

@NgModule({
  imports: [
    CommonModule,
    ScPlacementGapRoutingModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ScPlacementGapComponent, ScDisclosureChecklistComponent, ScAgreementComponent]
})
export class ScPlacementGapModule { }
