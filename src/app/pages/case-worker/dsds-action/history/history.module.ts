//import { ReportSummary } from './../../../providers/_history/provider.data.model';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryComponent } from './history.component';
import { HistoryRoutingModule } from './history-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    HistoryRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    QuillModule,
    PaginationModule
  ],
  declarations: [
    HistoryComponent
  ],
  providers: []
})
export class HistoryModule { }
