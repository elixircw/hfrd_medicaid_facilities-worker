import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Router } from '@angular/router';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { AlertService, DataStoreService, GenericService, SessionStorageService, AuthService } from '../../../../../@core/services';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DSDSActionSummary } from '../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { InvolvedPerson } from '../../involved-person/_entities/involvedperson.data.model';
import { FamilyList, Fim } from '../_entities/recording.data.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { AppConstants } from '../../../../../@core/common/constants';

declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'family-involvement-meeting',
    templateUrl: './family-involvement-meeting.component.html',
    styleUrls: ['./family-involvement-meeting.component.scss']
})
export class FamilyInvolvementMeetingComponent implements OnInit {
    id: string;
    daNumber: string;
    isFim = false;
    meetingTypesDropdown$: Observable<DropdownModel[]>;
    familyMeetingTypesDropdown$: Observable<DropdownModel[]>;
    familyMeetingTypesDropdown: any[] = [];
    informalSupportDropdown$: Observable<DropdownModel[]>;
    ldssStaffDropdown$: Observable<DropdownModel[]>;
    schoolSystemDropdown$: Observable<DropdownModel[]>;
    involevedPerson$: Observable<InvolvedPerson[]>;
    familyIM: Fim;
    fimList$: Observable<FamilyList[]>;
    fimView: FamilyList;
    fimForm: FormGroup;
    lastParticipantsCall: any[];
    selectedFimType = [];
    fimSubType = [];
    minDate = new Date();
    dsdsActionsSummary = new DSDSActionSummary();
    showFollowUp = false;
    meetingDecisionAccepted = false;
    meetingDecisionRejected = false;
    involvedDrop: boolean;
    otherPerticipants: boolean;
    selected = 'involvedPerson';
    isServiceCase: any;
    uploadedDocuments = [];
    uploadType= 'meeting';
    isClosed = false;
    userRole: any;
    caseNumber: string;
    source: string;
    caseType: string;
    isIntakeWorker: boolean;
    isUploadClicked: boolean;
    involevedPersons: InvolvedPerson[];
    constructor(
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _servicePlan: GenericService<Fim>,
        private _dataStoreService: DataStoreService,
        private _session: SessionStorageService
    ) {
        this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    }

    ngOnInit() {
        console.log(this.isServiceCase);
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.getPageList();
        this.getFimDropDown();
        this.getInvolvedPerson();
        this.isUploadClicked = false;
        this.userRole = this._authService.getCurrentUser();
        this.isIntakeWorker =
            this.userRole.role.name === AppConstants.ROLES.INTAKE_WORKER;
        this.fimForm = this._formBuilder.group({
            meetingdate: [new Date()],
            meetingrecordingid: [null],
            meetingtypekey: [null],
            persontype: [''],
            meetingstatus: [null],
            personname: [''],
            meetingdescription: [''],
            meetingcomments: [''],
            meetingdecision: [null],
            meetingdecisioncomments: [''],
            isfollowupmeeting: [null],
            followmeetingrecordingactor: [''],
            followmeetingdate: [null],
            followupdate: [null],
            parentmeetingid: [null],
            iscompleted: [null],
            participants: [''],
            meetingrecordingactor: [''],
            fimtype: [''],
            intakeserviceid: ['']
        });
        this.fimForm.setControl('meetingparticipants', this._formBuilder.array([]));
        this._dataStoreService.currentStore.subscribe((store) => {
            if (store['dsdsActionsSummary']) {
                this.dsdsActionsSummary = store['dsdsActionsSummary'];
            }
        });
        const da_status = this._session.getItem('da_status');
        if (da_status) {
         if (da_status === 'Closed' || da_status === 'Completed') {
             this.isClosed = true;
         } else {
             this.isClosed = false;
         }
        }
    }
    addServicePlan(fimModel) {
        fimModel = fimModel.getRawValue();
        // if (fimModel.meetingparticipants.length > 0) {
        fimModel.meetingrecordingactor = fimModel.meetingrecordingactor.map((personid) => {
            const person = this.involevedPersons.find(item => item.personid === personid);
            return {
                intakeservicerequestactorid: person.intakeservicerequestactorid,
                personid: personid
            };
        });
        if (fimModel.followmeetingrecordingactor && fimModel.followmeetingrecordingactor.length) {
            fimModel.followmeetingrecordingactor = fimModel.followmeetingrecordingactor.map((res) => {
                return {
                    intakeservicerequestactorid: res.intakeservicerequestactorid,
                    personid: res.personid
                };
            });
        }
        fimModel.fimtype = this.selectedFimType;
        if (fimModel.meetingparticipants.length > 0) {
            fimModel.meetingparticipants = fimModel.meetingparticipants.map((res) => {
                return {
                    participanttype: res.Persondropdown,
                    participantkey: res.participantkey,
                    participantroledesc: res.participantroledesc,
                    firstname: res.firstname,
                    lastname: res.lastname,
                    emailid: res.emailid,
                    personid: res.personid,
                    isinvited: res.isinvited ? 1 : 0,
                    isattended: res.isattended ? 1 : 0,
                    isaccpted: res.isaccpted === 1 ? 1 : 0,
                    providerId: res.providerId
                };
            });
            fimModel.uploadedfile = this.uploadedDocuments;
            this.familyIM = Object.assign(fimModel);
            this.familyIM.intakeserviceid = this.id;
            this.familyIM.uploadedfile = this.uploadedDocuments;
            this.familyIM.iscompleted = fimModel.meetingstatus;
            this.familyIM.ismeetingdecision = fimModel.meetingdecision;
            if (this.isServiceCase) {
                this.familyIM.servicecaseid = this.id;
                this.familyIM.intakeserviceid = null;
            }
            this._servicePlan.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Fim.AddParticipantUrl;
            this._servicePlan.create(this.familyIM).subscribe(
                (res) => {
                    this.getPageList();
                    (<any>$('#add-new-meeting')).modal('hide');
                    if (this.isUploadClicked) {
                        this.isUploadClicked = false;
                        this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment/attachment-upload']);
                    }
                    this.clearFim();
                    this._alertService.success('Participants Added successfully');
                },
                (error) => console.log(error)
            );
        } else {
            this._alertService.error('Please Add atleast One Participants');
        }
        // }
        // } else {
        //     this._alertService.error('Please Add atleast One Participants');
        // }
    }

    navigatetoattachements() {

                // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment/attachment-upload']);
                // (<any>$('#add-new-meeting')).modal('hide');
    }


    getFimType(fimType) {
        if (fimType === 'FIM') {
            this.isFim = true;
        } else {
            this.isFim = false;
        }
    }
    viewFim(family: FamilyList) {
        this.fimView = Object.assign(family);
        if (family.meetingtypekey === 'FIM') {
            this.isFim = true;
        } else {
            this.isFim = false;
        }
    }
    redirectToUpload() {
        this.isUploadClicked = true;
        this.addServicePlan(this.fimForm.value);
    }
    editFim(family: FamilyList) {
        // this.fimForm = Object.assign(family);

        // (<any>$('#add-new-meeting')).modal('hide');
        if (family.meetingtypekey === 'FIM') {
            this.isFim = true;
        } else {
            this.isFim = false;
        }
        if(family.fimtype) {
            this.fimSubType = family.fimtype;
        }
        this.uploadedDocuments = [];
       // this.uploadedDocuments = family.uploadedfile && family.uploadedfile.data ? family.uploadedfile.data : [];
        console.log('firForm all value---->', this.fimForm);
        console.log('Family list console---->', family);
        console.log('involved person list---->', this.involevedPerson$);
            this.uploadedDocuments = [];
            this.uploadedDocuments = family.uploadedfile && family.uploadedfile.data ? family.uploadedfile.data : [];
            const meetingparticipants = family.participants.map(item => {
                return { Persondropdown: item.participanttype,
                firstname: item.firstname,
                lastname: item.lastname,
                participantroledesc : item.participantroledesc,
                isinvited : item.isinvited,
                isattended : item.isattended,
                isaccpted : item.isaccpted,
            };

            });
        this.fimForm.patchValue(
                {
                    meetingdate: family.meetingdate,
                    meetingrecordingactor: family.recordingactor ? family.recordingactor.map(item => item.personid) : null,
                    persontype: family.persontype,
                    personname: family.personname,
                    meetingstatus: family.iscompleted,
                    meetingtypekey: family.meetingtypekey,
                    fimtype: family.fimdetails ? family.fimdetails.map(item => item.familymeetingtypekey) : null,
                    participants: family.participants ? family.participants.map(item => item.personid) : null,
                    meetingdescription: family.meetingdescription,
                    meetingcomments: family.meetingcomments,
                    isfollowupmeeting: family.isfollowupmeeting,
                    followmeetingdate: family.followmeetingdate,
                    followupdate: family.followupdate,
                    followmeetingrecordingactor: family.followmeetingrecordingactor,
                    meetingdecision: family.ismeetingdecision,
                    meetingdecisioncomments: family.meetingdecisioncomments,
                    meetingrecordingid: family.meetingrecordingid
                    // meetingparticipants: meetingparticipants
                }
        );
        this.fimForm.setControl('meetingparticipants', this._formBuilder.array([]));
        const control = <FormArray>this.fimForm.controls['meetingparticipants'];
        meetingparticipants.forEach(element => {
            control.push(this._formBuilder.group(element));
        });
        (<any>$('#add-new-meeting')).modal('show');

    }

    clearFim() {
        this.fimForm.reset();
        this.selectedFimType = [];
        this.fimSubType = [];
        this.isFim = false;
        this.informalSupportDropdown$ = Observable.empty();
        this.ldssStaffDropdown$ = Observable.empty();
        this.schoolSystemDropdown$ = Observable.empty();
        this.getFimDropDown();
        this.fimForm.setControl('meetingparticipants', this._formBuilder.array([]));
    }
    changeParticipants(event) {
        this.involvedDrop = true;
        this.otherPerticipants = false;
        // console.log(event.source.value, event.source.selected);
        if (event.source.selected && event.source.value !== this.lastParticipantsCall) {
            this.addMeetingParticipants(true, event.source.value);
            this.lastParticipantsCall = event.source.value;
        } else if (!event.source.selected) {
            const control = <FormArray>this.fimForm.controls['meetingparticipants'];
            control.controls.forEach((cont) => {
                const participantType = event.source.value.participanttype ? event.source.value.participanttype : 'IP';
                if (event.source.value.rolename === cont.value.participantkey && participantType === cont.value.participanttype) {
                    const removeIndex = control.controls.indexOf(cont);
                    this.deleteMeetingParticipants(removeIndex, event);
                }
            });
        } else {
            this.lastParticipantsCall = [];
        }
    }
    newParticipants(modal) {
        console.log(modal);
        const role = [];
        role.push({ typedescription: modal.source.value.split('~')[2] });
        if (modal.checked) {
            const psrticip = Object.assign({
                rolename: modal.source.value.split('~')[1],
                parentrolename: modal.source.value.split('~')[0],
                roles: role
            });
            this.addMeetingParticipants(false, psrticip);
        } else {
            const control = <FormArray>this.fimForm.controls['meetingparticipants'];
            control.controls.forEach((cont) => {
                if (modal.source.value.split('~')[1] === cont.value.participantkey && modal.source.value.split('~')[0] === cont.value.participanttype) {
                    const removeIndex = control.controls.indexOf(cont);
                    this.deleteMeetingParticipants(removeIndex, modal);
                }
            });
        }
    }
    otherParticipants() {
        this.otherPerticipants = true;
        this.involvedDrop = false;
        const psrticip = Object.assign({
            rolename: '',
            parentrolename: '',
            roles: ''
        });
        this.addMeetingParticipants(false, psrticip);
    }
    deleteOthersMeetingParticipants(index: number) {
        const control = <FormArray>this.fimForm.controls['meetingparticipants'];
        control.removeAt(index);
    }

    // displayCaseworker(event) {
    //     if (event === 'Case worker' && this.dsdsActionsSummary.da_assignedto !== undefined ) {
    //         const caseworkerName = this.dsdsActionsSummary.da_assignedto;
    //         this.fimForm.patchValue({ personname: caseworkerName});
    //         // this.fimForm.get('personname').disable();
    //     } else {
    //         this.fimForm.get('personname').reset();
    //         this.fimForm.get('personname').enable();
    //     }
    // }
    addMeetingParticipants(isInvovled, personinfo) {
        let involvedpersontype = '';
        if (this.involvedDrop) {
            involvedpersontype = 'involvedperson';
        }
        const control = <FormArray>this.fimForm.controls['meetingparticipants'];
        if (isInvovled) {
            const person = this.involevedPersons.find(item => item.personid === personinfo);
            control.push(this.createFormGroup(person, involvedpersontype));
        } else {
            control.push(this.createFormGroup(personinfo, involvedpersontype));
        }

    }
    deleteMeetingParticipants(index: number, e) {
        const control = <FormArray>this.fimForm.controls['meetingparticipants'];
        control.removeAt(index);
    }

    onFimTypeChange(event) {
        this.fimSubType = event.value;
        this.selectedFimType = [];
        event.value.map((subType) => {
            const familymeetingsubtype = this.familyMeetingTypesDropdown.find(item => item.familymeetingtypekey === subType);
            if (!familymeetingsubtype.length) {
                this.selectedFimType.push({
                    familymeetingtypekey: subType,
                    familymeetingsubtypekey: 'null'
                });
            }
        });
        if (this.selectedFimType.length !== event.value.length) {
            this.fimForm.markAsPristine();
        } else {
            this.fimForm.markAsDirty();
        }
        if (event.value.length) {
            if (event.value.filter((fim) => fim === 'Removal FIM').length) {
                const fimType = this.fimForm.value.fimtype;
                if (fimType && fimType.length) {
                    fimType.forEach((fim, index) => {
                        if (fim !== 'Removal FIM') {
                            fimType.splice(index, 1);
                        }
                    });
                    this.fimForm.patchValue({ primaryPlan: fimType });
                }
                this.familyMeetingTypesDropdown.forEach((item) => {
                    if (item.familymeetingtypekey !== 'Removal FIM') {
                        item.additionalProperty = true;
                    } else {
                        item.additionalProperty = false;
                    }
                });
            } else if (event.value.filter((fim) => fim === 'Voluntary Placement Agreement (VPA) FIM').length) {
                const fimType = this.fimForm.value.fimtype;
                if (fimType && fimType.length) {
                    fimType.forEach((fim, index) => {
                        if (fim !== 'Voluntary Placement Agreement (VPA) FIM') {
                            fimType.splice(index, 1);
                        }
                    });
                    this.fimForm.patchValue({ primaryPlan: fimType });
                }
                this.familyMeetingTypesDropdown.forEach((item) => {
                    if (item.familymeetingtypekey !== 'Voluntary Placement Agreement (VPA) FIM') {
                        item.additionalProperty = true;
                    } else {
                        item.additionalProperty = false;
                    }
                });
            } else {
                const fimType = this.fimForm.value.fimtype;
                if (fimType && fimType.length) {
                    fimType.forEach((fim, index) => {
                        if (fim === 'Removal FIM' || fim === 'Voluntary Placement Agreement (VPA) FIM') {
                            fimType.splice(index, 1);
                        }
                    });
                    this.fimForm.patchValue({ primaryPlan: fimType });
                }
                this.familyMeetingTypesDropdown.forEach((item) => {
                    if (item.familymeetingtypekey === 'Removal FIM' || item.familymeetingtypekey === 'Voluntary Placement Agreement (VPA) FIM') {
                        item.additionalProperty = true;
                    } else {
                        item.additionalProperty = false;
                    }
                });
            }
        } else {
            this.familyMeetingTypesDropdown.forEach((item) => {
                item.additionalProperty = false;
            });
        }
    }
    onFimSubTypeChange(event) {
        const subType = this.familyMeetingTypesDropdown.find(item => item.familymeetingtypekey === event.value);
        this.selectedFimType.push({
            familymeetingtypekey: subType.familymeetingtypekey,
            familymeetingsubtypekey: subType.familymeetingsubtypekey
        });
        this.fimForm.markAsDirty();
    }
    private createFormGroup(getROle, involvedpersontype) {
        console.log(getROle);
        return this._formBuilder.group({
            participanttype: [getROle.parentrolename ? getROle.parentrolename : 'IP'],
            participantkey: [getROle.rolename ? getROle.rolename : null],
            participantDesc: [getROle.roles ? getROle.roles[0].typedescription : null],
            participantroledesc: [getROle.relationship ? getROle.relationship : null],
            firstname: [getROle.firstname ? getROle.firstname : null, Validators.required],
            lastname: [getROle.lastname ? getROle.lastname : null, Validators.required],
            emailid: [getROle.email ? getROle.email : null],
            personid: [getROle.personid],
            isinvited: [''],
            isattended: [''],
            isaccpted: [''],
            providerId: [null],
            Persondropdown: [involvedpersontype]
        });
    }

    // enableMeetingstat(event) {
    //     this.showMeetingStatusSet = event === 'Scheduled' ? true : false;
    //     this.showMeetingStatusSet = event === 'Re-Scheduled' ? true : false;
    // }

    enableFollowUp(event) {
        this.showFollowUp = event === '1' ? true : false;
    }

    // enableMeetingAccepted(event) {
    //     this.meetingDecisionAccepted = event === 'Accepted' ? true : false;
    // }

    enableMeetingRejected(event) {

        this.meetingDecisionRejected = event === 'Rejected' ? true : false;
        if (!this.meetingDecisionRejected) {
        this._alertService.success('Assessments Should be completed within next 5 days');

    }

    }


    private getInvolvedPerson() {
        // const inputRequest = this.getRequestParam();
        let params: any =  this.getRequestParam();
        if (this.isServiceCase) {
            params = { objectid: this.id, objecttypekey: 'servicecase' };
        }

        this.involevedPerson$ = this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: params
                },
                'People/getpersondetailcw' + '?filter'
            )
            .map((res) => {
                return res['data'];
            });
            this.involevedPerson$.subscribe(persons => {
                this.involevedPersons = persons;
            });
    }

    private getFimDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Fim.MeetingTypesUrl),
            this._commonHttpService.getArrayList({ method: 'get' }, CaseWorkerUrlConfig.EndPoint.DSDSAction.Fim.FamilyMeetingTypesUrl + '?filter'),
            this._commonHttpService.getArrayList(
                {
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Fim.ParticipantTypesUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    meetingType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.meetingtypekey
                            })
                    ),
                    familyMeeting: result[1],
                    InformalSupport: result[2].filter((final) => final.participanttypekey === 'IS')[0].participantsubtype.map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.participanttypekey + '~' + res.participantsubtypekey + '~' + res.typedescription
                            })
                    ),
                    LdssStaff: result[2].filter((final) => final.participanttypekey === 'LDSS')[0].participantsubtype.map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.participanttypekey + '~' + res.participantsubtypekey + '~' + res.typedescription
                            })
                    ),
                    SchoolSystem: result[2].filter((final) => final.participanttypekey === 'SS')[0].participantsubtype.map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.participanttypekey + '~' + res.participantsubtypekey + '~' + res.typedescription
                            })
                    )
                };
            })
            .share();
        this.meetingTypesDropdown$ = source.pluck('meetingType');
        this.familyMeetingTypesDropdown$ = source.pluck('familyMeeting');
        this.informalSupportDropdown$ = source.pluck('InformalSupport');
        this.ldssStaffDropdown$ = source.pluck('LdssStaff');
        this.schoolSystemDropdown$ = source.pluck('SchoolSystem');
        this.familyMeetingTypesDropdown$.subscribe((res) => {
            res.forEach((item) => {
                item.additionalProperty = false;
            });
            this.familyMeetingTypesDropdown = res;
        });
    }

    private getPageList() {
        const obj = {};
        if (this.isServiceCase) {
            obj['objectid'] = this.id;
            obj['objecttype'] = 'servicecase';
        } else {
            obj['intakeserviceid'] = this.id;
        }
        this.fimList$ = this._commonHttpService
            .getArrayList(
                {
                    where: obj,
                    method: 'get',
                    count: -1,
                    page: 1,
                    limit: 20
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Fim.FimListUrl + '?filter'
            )
            .map((res) => {
                return res;
            });
    }

    getRequestParam() {
        let inputRequest;
        const caseID = this.getCaseUuid();
        console.log('store', this._dataStoreService.getCurrentStore());
        this.source = this.getSource();
        if (this.isServiceCaseData()) {
          inputRequest = {
            objectid: caseID,
            objecttypekey : 'servicecase'
          };
        } else if (this.isIntakeMode()) {
          inputRequest = {
            intakenumber: this.getIntakeNumber()
          };

        } else {
          inputRequest = {
            intakeserviceid: caseID
          };
        }

        return inputRequest;
      }

      getCaseUuid() {
        const caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
        let caseUUID = null;
        if (caseInfo) {
          caseUUID = caseInfo.intakeserviceid;
          this.caseNumber = caseInfo.da_number;
          if (this.isServiceCaseData()) {
            this.caseType = 'Service Case';
          } else {
            this.caseType = caseInfo.da_subtype ;
          }
        }
        if (caseID) {
          return caseID;
        }
        return caseUUID;
      }

      isServiceCaseData() {
        return this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
      }

      isIntakeMode() {
        return this.getIntakeNumber() ? true : false;
      }

      getIntakeNumber() {
        const intakeStore = this._session.getObj('intake');
        if (intakeStore && intakeStore.number) {
          return intakeStore.number;
        } else {
          return null;
        }
      }
      getSource() {

        if (this.isServiceCaseData()) {
         return AppConstants.CASE_TYPE.SERVICE_CASE;
        } else if (this.isIntakeMode()) {
          return AppConstants.CASE_TYPE.INTAKE;
        } else {
          return AppConstants.CASE_TYPE.CPS_CASE;
        }
        // need to add condition for adoption case
      }
}
