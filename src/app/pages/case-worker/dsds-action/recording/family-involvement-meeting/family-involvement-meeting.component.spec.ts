import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyInvolvementMeetingComponent } from './family-involvement-meeting.component';

describe('FamilyInvolvementMeetingComponent', () => {
  let component: FamilyInvolvementMeetingComponent;
  let fixture: ComponentFixture<FamilyInvolvementMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyInvolvementMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyInvolvementMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
