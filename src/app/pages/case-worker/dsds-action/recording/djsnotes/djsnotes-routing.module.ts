import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DjsnotesComponent } from './djsnotes.component';

const routes: Routes = [{
  path: '',
  component: DjsnotesComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DjsnotesRoutingModule { }
