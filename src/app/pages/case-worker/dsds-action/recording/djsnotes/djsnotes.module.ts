import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DjsnotesRoutingModule } from './djsnotes-routing.module';
import { DjsnotesComponent } from './djsnotes.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    DjsnotesRoutingModule,
    PaginationModule,
    FormMaterialModule,
    MatTooltipModule
  ],
  declarations: [DjsnotesComponent]
})
export class DjsnotesModule { }
