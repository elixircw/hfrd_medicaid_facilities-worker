import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ContactType, PurposeType, TypeofLocation, PersonsInvolvedType } from '../../../../newintake/my-newintake/_entities/newintakeModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvolvedPerson } from '../../as-service-plan/_entities/as-service-plan.model';
import { DynamicObject, PaginationRequest, DropdownModel, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, AuthService, AlertService, DataStoreService } from '../../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../../../newintake/newintake-url.config';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { RecordingNotes } from '../_entities/recording.data.model';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'djsnotes',
  templateUrl: './djsnotes.component.html',
  styleUrls: ['./djsnotes.component.scss']
})
export class DjsnotesComponent implements OnInit, AfterViewInit {
  id: string;
  daNumber: string;
  contactType$: Observable<ContactType[]>;
  purposeType$: Observable<PurposeType[]>;
  typeofLocation$: Observable<TypeofLocation[]>;
  personsInvolvedType$: Observable<PersonsInvolvedType[]>;
  notesForm: FormGroup;
  purposeType: PurposeType[] = [];
  contactType: ContactType[] = [];
  typeofLocation: TypeofLocation[] = [];
  personsInvolvedType: PersonsInvolvedType[] = [];
  addNotes: RecordingNotes = new RecordingNotes();
  recordingNotes$: Observable<RecordingNotes[]>;
  recordingNotesHistory$: Observable<RecordingNotes[]>;
  addedPersons = ([] = []);
  youthList: any[];
  intakeId: string;
  notesId: string;
  editMode: boolean;
  userInfo: any;
  currentDate: Date = new Date();
  private store: DynamicObject;
  staffTypeList: DropdownModel[];
  notes: any;
  notefilterForm: FormGroup;
  notesHistory = [];
  totalCount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  notesid: any;
  notesIdfromdate: any;
  constructor(private formBuilder: FormBuilder, private _commonHttpService: CommonHttpService, private _authService: AuthService,
    private _alertService: AlertService, private route: ActivatedRoute, private _storeService: DataStoreService) {
    this.store = this._storeService.getCurrentStore();
  }


  ngOnInit() {
    this.id = this._storeService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._storeService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
    // this.getInvolvedPerson();
    this.loadDropdowns();
    // this.loadStaffType();
    this.paginationInfo.pageNumber = 1;
    this.userInfo = this._authService.getCurrentUser();
    this.notesId = null;
    this.editMode = false;
    this.notesForm = this.formBuilder.group({
      detail: '',
      progressnoteid: '',
      progressnotetypeid: '',
      progressnotesubtypeid: '',
      date: '',
      location: '',
      contactname: '',
      contactroletypekey: [''],
      contactphone: '',
      contactemail: '',
      attemptindicator: false,
      description: '',
      starttime: '',
      staff: [''],
      endtime: '',
      entitytypeid: 'a426ee09-ed7f-4b37-aead-60925f13425b',
      entitytype: 'intakeservicerequest',
      savemode: 1,
      stafftype: '',
      notes: '',
      instantresults: 1,
      contactstatus: true,
      drugscreen: true,
      progressnotepurposetypekey: '',
      progressnoteroletype: [''],
      progressnoterole: [''],
      contactparticipant: ['']
    });
    this.notefilterForm = this.formBuilder.group({
      stafftype: '',
      startdate: '',
      enddate: '',
      workername: '',
      keyword: ''
    });
    this.notesForm.patchValue({ 'staff': this.userInfo ? this.userInfo.user.userprofile.displayname : '' });
    this.notesForm.get('staff').disable();
    this.notesForm.get('staff').updateValueAndValidity();
    this.addedPersons = this.store['da_intakeservicerequestactor'];
    if (this.addedPersons && this.addedPersons.length > 0) {
      this.youthList = this.addedPersons.filter(item => item.intakeservicerequestpersontypekey === 'Youth');
    }
    this.getNotesList();
  }

  ngAfterViewInit() {
    const intakeCaseStore = this._storeService.getData('IntakeCaseStore');
    if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
      (<any>$(':button')).prop('disabled', true);
      (<any>$('span')).css({
        'pointer-events': 'none',
        'cursor': 'default',
        'opacity': '0.5',
        'text-decoration': 'none'
      });
      (<any>$('i')).css({
        'pointer-events': 'none',
        'cursor': 'default',
        'opacity': '0.5',
        'text-decoration': 'none'
      });
      (<any>$('th a')).css({
        'pointer-events': 'none',
        'cursor': 'default',
        'opacity': '0.5',
        'text-decoration': 'none'
      });
    }
  }
  private loadDropdowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.contactType),
      this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.purposeType),
      this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.personsInvolvedType),
      this._commonHttpService
        .getArrayList(
          new PaginationRequest({
            nolimit: true,
            method: 'get',
            where: { 'tablename': 'ProgressNoteStaffType', 'teamtypekey': 'DJS' }
          }),
          'referencetype/gettypes?filter'
        )
    ])
      .map(([contactType, purposeType, personsInvolvedType, stafftypeli]) => {
        return {
          contactType,
          purposeType,
          personsInvolvedType,
          stafftypelist: stafftypeli.map((result) => {
            return new DropdownModel({
              text: result.value_text,
              value: result.ref_key
            });
          })
        };
      })
      .share();

    this.contactType$ = source.pluck('contactType');
    this.purposeType$ = source.pluck('purposeType');
    this.personsInvolvedType$ = source.pluck('personsInvolvedType');
    source.pluck('stafftypelist').subscribe((data: DropdownModel[]) => { this.staffTypeList = data; });
    this.contactType$.subscribe((data) => { this.contactType = data; });
    this.purposeType$.subscribe((data) => { this.purposeType = data; });
    this.personsInvolvedType$.subscribe((data) => { this.personsInvolvedType = data; });

  }
  private loadLocationTypeDropdown(progressnotetypeid: string) {
    this.notesForm.patchValue({
      progressnotetypeid: progressnotetypeid,
    });
    const locationType = this._commonHttpService
      .getArrayList(
        new PaginationRequest({
          'nolimit': true,
          'order': 'description'
        }),
        NewUrlConfig.EndPoint.Intake.typeofLocation + '?prognotetypeid=' + progressnotetypeid
      )
      .map((typeofLocation) => {
        return {
          typeofLocation
        };
      })
      .share();

    this.typeofLocation$ = locationType.pluck('typeofLocation');
    this.typeofLocation$.subscribe((data) => { this.typeofLocation = data; });
  }

  private getNotesList(notesid?: number) {
    const model = this.notefilterForm.getRawValue();
    const request = {
      servicerequestid: this.id,
      method: 'get',
      pagesize: this.paginationInfo.pageSize,
      pagenumber: this.paginationInfo.pageNumber,
      notesid: notesid ? notesid : null,
      stafftype: null,
      datefrom: null,
      dateto: null,
      workername: null,
      keyword: null
    };
    if (model.stafftype) {
      request.stafftype = model.stafftype;
    } else {
      delete request.stafftype;
    }
    if (model.startdate) {
      request.datefrom = new Date(model.startdate);
    } else {
      delete request.datefrom;
    }
    if (model.enddate) {
      request.dateto = new Date(model.enddate);
    } else {
      delete request.dateto;
    }
    if (model.workername) {
      request.workername = model.workername;
    } else {
      delete request.workername;
    }
    if (model.keyword) {
      request.keyword = model.keyword;
    } else {
      delete request.keyword;
    }
    const source = this._commonHttpService
      .getPagedArrayList(request,
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.ContactNotesList + '?data'
      )
      .map((result) => {
        this.totalCount = result.data.length ? result.count : 0;
        return {
          data: result.data
        };
      })
      .share();
    if (notesid) {
      this.recordingNotesHistory$ = source.pluck('data');
    } else {
      this.recordingNotes$ = source.pluck('data');
    }
  }

  private saveNote() {
    if (this.notesForm.valid) {
      this.notesForm.patchValue({ progressnoteid: this.notesId ? this.notesId : '' });
      const data = this.notesForm.getRawValue();
      data.description = data.notes;
      if (data.progressnoterole && data.progressnoterole.length > 0) {
        data.progressnoteroletype = [];
        data.progressnoterole.forEach(role => {
          data.progressnoteroletype.push({
            contactroletypekey: role
          });
        });
      }
      data.detail = data.staff;
      data.contactname = data.staff;
      data.contactdate = data.date;
      data.savemode = 1;
      data.entitytype = 'intakeservicerequest';
      data.entitytypeid = this.id;
      data.notesid = this.notesIdfromdate;
      if (!this.notesId) {
        data.notesid = this.generateNotesID();
        data.histories = [];
      }
      this.addNotes = Object.assign(data);
      this._commonHttpService.create(this.addNotes, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.AddUpdateNotes).subscribe(
        (result) => {
          (<any>$('#add-djs-notes')).modal('hide'); this.resetForm();
          this._alertService.success('Contact details saved successfully!');
          this.getNotesList();
        },
        (error) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    } else {
      this._alertService.error('Please fill the required fields');
    }
  }

  generateNotesID() {
    return new Date().getTime();
  }

  resetForm() {
    this.notesForm.reset();
    this.notesId = null;
    this.notesForm.enable();
    this.notesForm.patchValue({ 'staff': this.userInfo ? this.userInfo.user.userprofile.displayname : '' });
    this.notesForm.get('staff').disable();
  }

  private getNotes(note: RecordingNotes, editFlag) {
    this.resetForm();
    this.loadLocationTypeDropdown(note.progressnotetypeid);
    this.notesHistory = note.histories;
    this.notesId = note.progressnoteid;
    this.notesIdfromdate = note.notesid;
    this.editMode = editFlag;
    if (editFlag === 0) {
      this.notesForm.disable();
    } else {
      this.notesForm.enable();
    }

    this.patchForm(note);
  }

  private getSavedNote(id) {
    const NotesList = this._commonHttpService
      .getArrayList(
        new PaginationRequest({
          count: -1,
          method: 'get'
        }),
        NewUrlConfig.EndPoint.Intake.notesEdit + '/' + id
      )
      .map((notes) => {
        return {
          notes
        };
      })
      .share();

    return NotesList.pluck('notes');
  }

  notefilter(value) {
    // should bind the request to API .
    // console.log ('notesfilterform',value);
  }
  clearFilter() {
    this.notefilterForm.reset();
    this.getNotesList();
  }

  private getValueForHistory(value, name) {

    switch (name) {
      case 'contype':

        const cindex = this.contactType.findIndex((p) => p.progressnotetypeid === value);
        if (cindex !== -1) {
          return this.contactType[cindex].description;
        } else {
          return '';
        }
      case 'stafftype':
      const staffdata = (this.staffTypeList && this.staffTypeList.length > 0) ? this.staffTypeList.findIndex(data => data.value === value) : -1;
        if (staffdata !== -1) {
          return this.staffTypeList[staffdata].text;
        } else {
          return '';
        }
      case 'constatus': if (value === true) {
        return 'Yes';
      } else if (value === false) {
        return 'No';
      } else {
        return '';
      }
      case 'location':
        if (this.typeofLocation$ && this.typeofLocation$ !== null) {

          const i = this.typeofLocation.findIndex((p) => p.progressnotetypeid === value);
          if (i !== -1) {
            return this.typeofLocation[i].description;
          } else {
            return '';
          }

        } else {
          return '';
        }
      case 'drugscrn': if (value === true) {
        return 'Yes';
      } else if (value === false) {
        return 'No';
      } else {
        return '';
      }
      case 'instresult': if (value === 1) {
        return 'Positive';
      } else if (value === 2) {
        return 'Negative';
      } else {
        return '';
      }

      case 'purpose':
        const index = this.purposeType.findIndex((p) => p.description.trim() === (value ? value.trim() : ''));
        if (index !== -1) {
          return this.purposeType[index].description;
        } else {
          return '';
        }

      case 'involvedperson':
        if (value && value.length > 0) {
          return value.map(item => item.contactroletypekey);
        } else {
          return '';
        }

      default: return '';
    }
  }

  private getValue(value, name) {

    switch (name) {
      case 'contype':

        const cindex = this.contactType.findIndex((p) => p.progressnotetypeid === value);
        if (cindex !== -1) {
          return this.contactType[cindex].description;
        } else {
          return '';
        }
      case 'stafftype':
      const staffdata = (this.staffTypeList && this.staffTypeList.length > 0) ? this.staffTypeList.findIndex(data => data.value === value) : -1;
      if (staffdata !== -1) {
        return this.staffTypeList[staffdata].text;
      } else {
        return '';
      }
      case 'constatus': if (value === true) {
        return 'Yes';
      } else if (value === false) {
        return 'No';
      } else {
        return '';
      }
      case 'location':
        if (this.typeofLocation$ && this.typeofLocation$ !== null) {

          const i = this.typeofLocation.findIndex((p) => p.progressnotetypeid === value);
          if (i !== -1) {
            return this.typeofLocation[i].description;
          } else {
            return '';
          }

        } else {
          return '';
        }
      case 'drugscrn': if (value === true) {
        return 'Yes';
      } else if (value === false) {
        return 'No';
      } else {
        return '';
      }
      case 'instresult': if (value === 1) {
        return 'Positive';
      } else if (value === 2) {
        return 'Negative';
      } else {
        return '';
      }

      case 'purpose':
        const index = this.purposeType.findIndex((p) => p.description.trim() === (value ? value.trim() : ''));
        if (index !== -1) {
          return this.purposeType[index].description;
        } else {
          return '';
        }

      case 'involvedperson':
        if (value && value.length > 0) {
          return value.map(item => item.contactroletypekey);
        } else {
          return '';
        }

      default: return '';
    }
  }

  private setLocation(desc) {
    this.notesForm.patchValue({ 'location': desc });
  }

  private patchForm(data) {
    if (data) {
      if (data.progressroletype && data.progressroletype.length > 0) {
        data.progressnoterole = data.progressroletype.map(item => item.contactroletypekey);
      }
      this.notesForm.patchValue(data);
      if (this.youthList && this.youthList.length > 0) {
        this.notesForm.patchValue({
          contactroletypekey: this.youthList[0].actor.Person.firstname
        });
      }
      this.notesForm.patchValue({
        progressnotepurposetypekey: data.progressnotepurposetype,
        progressnoterole: data.progressnoterole,
        notes: data.description,
        staff: data.contactname,
        date: data.contactdate
      });
      this.notesForm.get('staff').disable();
      this.notesForm.get('staff').updateValueAndValidity();
    }
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.getNotesList();
  }

  onChangeDrugScreen(value: boolean) {
    if (value) {
      this.notesForm.get('instantresults').setValidators([Validators.required]);
      this.notesForm.updateValueAndValidity();
    } else {
      this.notesForm.get('instantresults').clearValidators();
      this.notesForm.updateValueAndValidity();
    }
  }

}
