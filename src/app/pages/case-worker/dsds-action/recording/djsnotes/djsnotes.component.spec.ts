import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsnotesComponent } from './djsnotes.component';

describe('DjsnotesComponent', () => {
  let component: DjsnotesComponent;
  let fixture: ComponentFixture<DjsnotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsnotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsnotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
