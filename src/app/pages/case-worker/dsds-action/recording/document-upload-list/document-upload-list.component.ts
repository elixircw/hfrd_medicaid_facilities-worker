import { Component, OnInit, Input } from '@angular/core';
import { AppConfig } from '../../../../../app.config';
import { config } from '../../../../../../environments/config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { HttpHeaders } from '@angular/common/http';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { Attachment } from '../../attachment/_entities/attachment.data.models';
import { AlertService, AuthService, DataStoreService } from '../../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { GenericService, SessionStorageService } from '../../../../../@core/services';
import { DsdsService } from '../../_services/dsds.service';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';

@Component({
  selector: 'document-upload-list',
  templateUrl: './document-upload-list.component.html',
  styleUrls: ['./document-upload-list.component.scss']
})
export class DocumentUploadListComponent implements OnInit {

  @Input() uploadedDocuments = [];
  @Input() uploadType;
  deleteAttachmentIndex: number;
  token: AppUser;
  caseNumber: string;
  enableUpload = false;
  attachment_type = '1';
  involvedPerson=[];
  id: string;
  personid='';
  isServiceCase=false;
  constructor(private _alertService: AlertService,
    private _dropDownService: CommonHttpService,
    private route: ActivatedRoute,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService,
    private _service: GenericService<Attachment>,
    private _dsdsService: DsdsService,
    private _dataStoreService: DataStoreService) {
        this.uploadedDocuments = [];

    }

  ngOnInit() {
    this.token = this._authService.getCurrentUser();
    this.caseNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
    this.isServiceCase = this._dsdsService.isServiceCase();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.getInvolvedPerson();
  }
  continueUpload() {
    if(this.attachment_type == '1')  {
        this.personid = '';
        this._dsdsService.setField('attachmenttype','case');
        this._dsdsService.setField('personid','');
        (<any>$('#upload-attachment')).modal('show');
        if(this.uploadType=='note') {
            (<any>$('#myModal-recordings')).modal('hide');
        } else if(this.uploadType=='meeting') {
            (<any>$('#add-new-meeting')).modal('hide');
        }
        (<any>$('#confirm-attachment-popup')).modal('hide');
    } else {
        if(this.personid=='' || !this.personid) {
            this._alertService.error('Please choose a person');
            return;
        } else {
            this._dsdsService.setField('attachmenttype','person');
            this._dsdsService.setField('personid',this.personid);
            (<any>$('#upload-attachment')).modal('show');
            if(this.uploadType=='note') {
                (<any>$('#myModal-recordings')).modal('hide');
            } else if(this.uploadType=='meeting') {
                (<any>$('#add-new-meeting')).modal('hide');
            }
            (<any>$('#confirm-attachment-popup')).modal('hide');
        }
    }
  }
  getInvolvedPerson() {

    let inputRequest: Object;
    if (this.isServiceCase) {
        inputRequest = {
            objectid: this.id,
            objecttypekey: 'servicecase'
        };
    } else {
        inputRequest = {
            intakeserviceid: this.id
        };
    }
    this._dropDownService
        .getSingle(
            new PaginationRequest({
                page: 1,
                limit: 20,
                method: 'get',
                where: inputRequest
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
        )
        .subscribe(data => {
            if (data && data.data.length > 0) {
                this.involvedPerson = data.data;
            }
        });
}
  uploadFile(file: File | FileError): void {
    // this.enableUpload = false;
    // this.enableUpload = true;
    // (<any>$('#upload-attachment')).modal('show');
    // (<any>$('#myModal-recordings')).modal('hide');
    (<any>$('#confirm-attachment-popup')).modal('show');

    // if (!(file instanceof Array)) {
    //     return;
    // }
    // file.map((item, index) => {
    //     const fileExt = item.name
    //         .toLowerCase()
    //         .split('.')
    //         .pop();
    //     if (
    //         fileExt === 'mp3' ||
    //         fileExt === 'ogg' ||
    //         fileExt === 'wav' ||
    //         fileExt === 'acc' ||
    //         fileExt === 'flac' ||
    //         fileExt === 'aiff' ||
    //         fileExt === 'mp4' ||
    //         fileExt === 'mov' ||
    //         fileExt === 'avi' ||
    //         fileExt === '3gp' ||
    //         fileExt === 'wmv' ||
    //         fileExt === 'mpeg-4' ||
    //         fileExt === 'pdf' ||
    //         fileExt === 'txt' ||
    //         fileExt === 'docx' ||
    //         fileExt === 'doc' ||
    //         fileExt === 'xls' ||
    //         fileExt === 'xlsx' ||
    //         fileExt === 'jpeg' ||
    //         fileExt === 'jpg' ||
    //         fileExt === 'png' ||
    //         fileExt === 'ppt' ||
    //         fileExt === 'pptx' ||
    //         fileExt === 'gif'
    //     ) {
    //         this.uploadedDocuments.push(item);
    //         const uindex = this.uploadedDocuments.length - 1;
    //         if (!this.uploadedDocuments[uindex].hasOwnProperty('percentage')) {
    //             this.uploadedDocuments[uindex].percentage = 1;
    //         }

    //         this.uploadAttachment(uindex);
    //         const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
    //         const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
    //         if (audio_ext.indexOf(fileExt) >= 0) {
    //             this.uploadedDocuments[uindex].attachmenttypekey = 'Audio';
    //         } else if (video_ext.indexOf(fileExt) >= 0) {
    //             this.uploadedDocuments[uindex].attachmenttypekey = 'Video';
    //         } else {
    //             this.uploadedDocuments[uindex].attachmenttypekey = 'Document';
    //         }
    //     } else {
    //         // tslint:disable-next-line:quotemark
    //         this._alertService.error(fileExt + " format can't be uploaded");
    //         return;
    //     }
    // });
}
// uploadAttachment(index) {
//     const workEnv = config.workEnvironment;
//     let uploadUrl = '';
//     if (workEnv === 'state') {
//         uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
//             + '?access_token=' + this.token.id + '&' + 'srno=' + this.caseNumber + '&' + 'docsInfo='; // Need to discuss about the docsInfo
//         console.log('state', uploadUrl);
//     } else {
//         uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
//             '&' + 'srno=' + this.caseNumber;
//         console.log('local', uploadUrl);
//     }

//     this._uploadService
//         .upload({
//             url: uploadUrl,
//             headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
//             filesKey: ['file'],
//             files: this.uploadedDocuments[index],
//             process: true,
//         })
//         .subscribe(
//             (response) => {
//                 if (response.status) {
//                     this.uploadedDocuments[index].percentage = response.percent;
//                 }
//                 if (response.status === 1 && response.data) {
//                     const doucumentInfo = response.data;
//                     doucumentInfo.documentdate = doucumentInfo.date;
//                     doucumentInfo.title = doucumentInfo.originalfilename;
//                     doucumentInfo.objecttypekey = 'YTP';
//                     doucumentInfo.rootobjecttypekey = 'YTP';
//                     doucumentInfo.activeflag = 1;
//                     doucumentInfo.servicerequestid = null;
//                     this.uploadedDocuments[index] = { ...this.uploadedDocuments[index], ...doucumentInfo };
//                     console.log(index, this.uploadedDocuments[index]);
//                 }

//             }, (err) => {
//                 console.log(err);
//                 this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
//                 this.uploadedDocuments.splice(index, 1);
//             }
//         );
// }
// deleteAttachment() {
//     this.uploadedDocuments.splice(this.deleteAttachmentIndex, 1);
//     (<any>$('#delete-attachment-popup')).modal('hide');
// }
deleteAttachment() {
    const workEnv = config.workEnvironment;
    let documentPropertiesId = this.uploadedDocuments[this.deleteAttachmentIndex].documentpropertiesid;
    let documentId = this.uploadedDocuments[this.deleteAttachmentIndex].filename;
    if(!documentPropertiesId || documentPropertiesId == undefined) {
        this.uploadedDocuments.splice(this.deleteAttachmentIndex, 1);
        (<any>$('#delete-attachment-popup')).modal('hide');
        return;
    }
    if (workEnv === 'state') {
        this._service.endpointUrl =
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
        const id = documentPropertiesId + '&' + documentId;
        this._service.remove(id).subscribe(
            result => {
                this._alertService.success('Attachment Deleted successfully!');
                this.uploadedDocuments.splice(this.deleteAttachmentIndex, 1);
                (<any>$('#delete-attachment-popup')).modal('hide');
            },
            err => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            });
    } else {
        this._service.endpointUrl =
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.DeleteAttachmentUrl;
        this._service.remove(documentPropertiesId).subscribe(
            result => {
                this._alertService.success('Attachment Deleted successfully!');
                this.uploadedDocuments.splice(this.deleteAttachmentIndex, 1);
                (<any>$('#delete-attachment-popup')).modal('hide');
            },
            err => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
}
closeAttachment(){
    (<any>$('#delete-attachment-popup')).modal('hide');
}
closeattachmentConfirm(){
    (<any>$('#confirm-attachment-popup')).modal('hide');
}
downloadFile(s3bucketpathname) {
    const workEnv = config.workEnvironment;
    let downldSrcURL;
    if (workEnv === 'state') {
        downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
    } else {
        // 4200
        downldSrcURL = s3bucketpathname;
    }
    console.log('this.downloadSrc', downldSrcURL);
    window.open(downldSrcURL, '_blank');
}

confirmDeleteAttachment(index: number) {
    (<any>$('#delete-attachment-popup')).modal('show');
    this.deleteAttachmentIndex = index;
}

}
