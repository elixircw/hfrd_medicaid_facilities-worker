import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../@core/services';
import { AppConstants } from '../../../../@core/common/constants';

// tslint:disable-next-line:max-line-length

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'recording',
    templateUrl: './recording.component.html',
    styleUrls: ['./recording.component.scss']
})
export class RecordingComponent implements OnInit {
    userInfo: string;
    userRole: any;
    isIntakeWorker : boolean;
    constructor(private _authService: AuthService) {}

    ngOnInit() {
        this.userInfo = this._authService.getCurrentUser().user.userprofile.teamtypekey;
        this.userRole = this._authService.getCurrentUser();
        this.isIntakeWorker =
            this.userRole.role.name === AppConstants.ROLES.INTAKE_WORKER;
        console.log('auth', this.userInfo);
    }
}
