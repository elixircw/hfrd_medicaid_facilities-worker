import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { RecordingRoutingModule } from './recording-routing.module';
import { NotesComponent } from './notes/notes.component';
import { FamilyInvolvementMeetingComponent } from './family-involvement-meeting/family-involvement-meeting.component';
import { MatSelectModule, MatCheckboxModule, MatDatepickerModule, MatRadioModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatCardModule, MatListModule, MatExpansionModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { RecordingComponent } from './recording.component';
import { ResourceConsultComponent } from './resource-consult/resource-consult.component';
import { QuillModule } from 'ngx-quill';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { DocumentUploadListComponent } from './document-upload-list/document-upload-list.component';
import { AttachmentUploadComponent } from './attachment-upload/attachment-upload.component';


@NgModule({
  imports: [
    CommonModule,
    RecordingRoutingModule,
    CommonModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    ControlMessagesModule,
    NgSelectModule,
    PaginationModule,
    A2Edatetimepicker,
    SharedPipesModule,
    QuillModule,
    PaginationModule,
    NgxMaskModule,
    SharedDirectivesModule,
    NgxfUploaderModule.forRoot(),
    MatListModule,
    MatExpansionModule
  ],
  declarations: [RecordingComponent, NotesComponent, FamilyInvolvementMeetingComponent, ResourceConsultComponent,DocumentUploadListComponent,AttachmentUploadComponent],
  providers: [DatePipe]
})
export class RecordingModule { }
