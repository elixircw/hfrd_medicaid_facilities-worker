import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService, CommonHttpService, AlertService, GenericService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// tslint:disable-next-line:import-blacklist
import { Observable, Subject } from 'rxjs';
import { ResourceConsult, ResourceList } from '../_entities/recording.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { AppConstants } from '../../../../../@core/common/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'resource-consult',
    templateUrl: './resource-consult.component.html',
    styleUrls: ['./resource-consult.component.scss']
})
export class ResourceConsultComponent implements OnInit, AfterViewInit {
    recsourceConsultForm: FormGroup;
    id: string;
    popUpText = 'Add';
    btnName = 'Add';
    intakeservicerequestconsultreviewid: string;
    resourceID: string;
    isView: boolean;
    canDisplayPager$: Observable<boolean>;
    totalRecords$: Observable<number>;
    resourceCOnsult$: Observable<ResourceConsult[]>;
    resourceCOnsult: ResourceConsult;
    paginationInfo: PaginationInfo = new PaginationInfo();
    private pageStream$ = new Subject<number>();
    selectType$ = new Observable<DropdownModel[]>();
    resourceList$ = new Observable<ResourceList[]>();
    recsourceConsultUserForm: FormGroup;
    recsourceConsultUser$ = new Observable<Array<any>>();
    recsourceConsultUser = [];
    isClosed = false;
    source: string;
    caseNumber: any;
    caseType: string;
    constructor(
        private _formBuilder: FormBuilder,
        private _service: GenericService<ResourceConsult>,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private route: ActivatedRoute,
        private _storeService: DataStoreService,
        private _authService: AuthService,
        private _dataStoreService: DataStoreService,
        private _session: SessionStorageService
    ) { }

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.loadForm();
        this.loadresourceConsultUserForm();
        this.listDropdown();
        this.pageStream$.subscribe((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            this.getPage(this.paginationInfo.pageNumber);
        });
        this.getPage(1);
        const da_status = this._session.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }
    }

    ngAfterViewInit() {
      const intakeCaseStore = this._storeService.getData('IntakeCaseStore');
      if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
          (<any>$(':button')).prop('disabled', true);
          (<any>$('span')).css({'pointer-events': 'none',
                      'cursor': 'default',
                      'opacity': '0.5',
                      'text-decoration': 'none'});
          (<any>$('i')).css({'pointer-events': 'none',
                                  'cursor': 'default',
                                  'opacity': '0.5',
                                  'text-decoration': 'none'});
          (<any>$('th a')).css({'pointer-events': 'none',
                                  'cursor': 'default',
                                  'opacity': '0.5',
                                  'text-decoration': 'none'});
      }
  }

    loadForm() {
        this.recsourceConsultForm = this._formBuilder.group({
            resourceconsultuser: [''],
            notes: ['', Validators.required],
            date: ['', Validators.required],
            time: ['', Validators.required]
        });
    }

    loadresourceConsultUserForm() {
        this.recsourceConsultUser$ = Observable.empty();
        this.recsourceConsultUser = [];
        this.recsourceConsultUserForm = this._formBuilder.group({
            name: ['', Validators.required],
            consultreviewusertype: [''],
        });
    }

    addrecsourceConsultUser() {
        const nameModel = this.recsourceConsultUserForm.get('name').value;
        const consultreviewusertypeModel = this.recsourceConsultUserForm.get('consultreviewusertype').value;
        if (nameModel && consultreviewusertypeModel) {
            this.recsourceConsultUser.push({
                name: nameModel,
                consultreviewusertypekey: consultreviewusertypeModel.value,
                consultreviewusertype: consultreviewusertypeModel.text,
                onDelete: false,
                intakeservicerequestconsultreviewconfigid: null
            });
            this.recsourceConsultUser$ = Observable.of(this.recsourceConsultUser);
            this.recsourceConsultUserForm.reset();
        }
    }

    deleteUser(configid, i: number) {
        if (this.recsourceConsultUser.length <= 1 && configid) {
            this._alertService.error('Minimum one user required!');
        } else {
            if (configid) {
                this._service.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.DeleteResourceUserURL;
                this._service.remove(configid).subscribe(
                    response => {
                        this.recsourceConsultUser.splice(i, 1);
                        this.recsourceConsultUser$ = Observable.of(this.recsourceConsultUser);
                        this._alertService.success('Resource consult review user deleted successfully');
                    },
                    error => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                this.recsourceConsultUser.splice(i, 1);
                this.recsourceConsultUser$ = Observable.of(this.recsourceConsultUser);
                this._alertService.success('Resource consult review user deleted successfully');
            }
        }
    }

    deleteUsercheck(i: number) {
        this.recsourceConsultUser[i].onDelete = true;
    }

    cancelDelete(i: number) {
        this.recsourceConsultUser[i].onDelete = false;
    }

    saveResourceConsult(resourceCOnsult) {
        if (this.recsourceConsultUser.length < 1) {
            this._alertService.error('Minimum one user required!');
        } else {
            if (this.recsourceConsultUser) {
                this.recsourceConsultUser.forEach(element => {
                    delete element.consultreviewusertype;
                    delete element.onDelete;
                });
                resourceCOnsult.resourceConsultUser = Object.assign(this.recsourceConsultUser);
            }
            if (resourceCOnsult.date) {
                const timeSplit = resourceCOnsult.time.split(':');
                if (!(resourceCOnsult.date instanceof Date)) {
                    resourceCOnsult.date = new Date(resourceCOnsult.date);
                }
                resourceCOnsult.date.setHours(timeSplit[0]);
                resourceCOnsult.date.setMinutes(timeSplit[1]);
            }
            // resourceCOnsult.intakeserviceid = this.id;
            const requestParam = this.getRequestParam();
            resourceCOnsult.objectid = requestParam.objectid;
            resourceCOnsult.objecttypekey = requestParam.objecttypekey;
            resourceCOnsult.intakeservicerequestconsultreviewid = this.intakeservicerequestconsultreviewid;
            this._commonHttpService.create(resourceCOnsult, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.AddResourceConsultURL).subscribe(
                result => {
                    this._alertService.success('Resource consult details saved successfully!');
                    this.getPage(1);
                    (<any>$('#cls-add-resource')).click();
                    this.clearItem();
                },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }

    clearItem() {
        this.recsourceConsultForm.reset();
        this.recsourceConsultForm.enable();
        this.recsourceConsultUserForm.reset();
        this.recsourceConsultUserForm.enable();
        this.recsourceConsultUser = [];
        this.recsourceConsultUser$ = Observable.empty();
        this.popUpText = 'Add';
        this.btnName = 'Add';
        this.resourceID = null;
        this.intakeservicerequestconsultreviewid = null;
        this.isView = false;
    }
    showDeletePop(resourceid) {
        this.resourceID = resourceid.intakeservicerequestconsultreviewid;
        (<any>$('#delete-resource-popup')).modal('show');
    }

    showEditPopup(resource) {
        this.resourceID = resource.objectid;
        if (this.resourceID) {
            this.popUpText = 'Edit';
            this.btnName = 'Update';
            this.intakeservicerequestconsultreviewid = resource.intakeservicerequestconsultreviewid;
        } else {
            this.intakeservicerequestconsultreviewid = null;
        }

        resource.time = moment(resource.reviewdate).format('HH:mm');
        resource.date = resource.reviewdate;
        this.recsourceConsultUser = [...resource.resourceconsultuser];
        this.recsourceConsultUser.forEach(data => {
            data.onDelete = false;
        });
        this.recsourceConsultUser$ = Observable.of(this.recsourceConsultUser);
        this.recsourceConsultForm.patchValue(resource);
        this.recsourceConsultForm.enable();
        this.isView = false;
        (<any>$('#add')).modal('show');
    }

    showViewPopup(resource) {
        this.popUpText = 'View';
        resource.time = moment(resource.date).format('HH:mm');
        resource.date = resource.reviewdate;
        this.recsourceConsultUser = resource.resourceconsultuser;
        this.recsourceConsultUser$ = Observable.of(this.recsourceConsultUser);
        this.recsourceConsultForm.patchValue(resource);
        this.recsourceConsultForm.disable();
        this.isView = true;
        (<any>$('#add')).modal('show');
    }

    deleteResource() {
        this._service.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.DeleteResourceURL;
        this._service.remove(this.resourceID).subscribe(
            response => {
                this.getPage(1);
                this._alertService.success('Resource consult review deleted successfully');
                this.resourceID = null;
                (<any>$('#delete-resource-popup')).modal('hide');
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                (<any>$('#delete-resource-popup')).modal('hide');
            }
        );
    }
    private mergeDateTime(dateobj, timeobj) {
        const Dateobj: any = new Date(dateobj);
        const timeSplit = timeobj.split(':');
        const TimeHour = timeSplit[0];
        const TimeMin = timeSplit[1];

        Dateobj.setHours(TimeHour);
        Dateobj.setMinutes(TimeMin);

        return Dateobj;
    }

    listDropdown() {
        this.selectType$ = this._commonHttpService.getArrayList(
            {
                method: 'get',
                where: {},
                order: 'description ASC' // cw-006 : Ascending order list
            },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetSelectTypeURL + '?filter').map(items => {
            return items.map(
                list =>
                    new DropdownModel({
                        text: list.description,
                        value: list.consultreviewusertypekey
                    })
            );
        });
    }

    getPage(page: number) {
        const source = this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest(
                    {
                        method: 'get',
                        where: this.getRequestParam(),
                        page: this.paginationInfo.pageNumber,
                        limit: this.paginationInfo.pageSize,
                    }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetResourceConsultURL + '?filter'
            ).map((result: any) => {
                return {
                    data: result,
                    count: result.length > 0 ? result[0].totalcount : 0,
                    canDisplayPager: result.length > 0 ? result[0].totalcount > this.paginationInfo.pageSize : false
                };
            }).share();
        this.resourceList$ = source.pluck('data');
        if (page === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }


    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }

    getRequestParam() {
        let inputRequest;
        const caseID = this.getCaseUuid();
        console.log('store', this._dataStoreService.getCurrentStore());
        this.source = this.getSource();
        if (this.isServiceCaseData()) {
            inputRequest = {
                objectid: caseID,
                objecttypekey: 'servicecase'
            };
        } else if (this.isIntakeMode()) {
            inputRequest = {
                objectid: this.getIntakeNumber(),
                objecttypekey: 'intake'
            };

        } else {
            inputRequest = {
                objectid: caseID,
                objecttypekey: 'servicerequest'
            };
        }

        return inputRequest;
    }

    getCaseUuid() {
        const caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
        let caseUUID = null;
        if (caseInfo) {
            caseUUID = caseInfo.intakeserviceid;
            this.caseNumber = caseInfo.da_number;
            if (this.isServiceCaseData()) {
                this.caseType = 'Service Case';
            } else {
                this.caseType = caseInfo.da_subtype;
            }
        }
        if (caseID) {
            return caseID;
        }
        return caseUUID;
    }

    isServiceCaseData() {
        return this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    }

    isIntakeMode() {
        return this.getIntakeNumber() ? true : false;
    }

    getIntakeNumber() {
        const intakeStore = this._session.getObj('intake');
        if (intakeStore && intakeStore.number) {
            return intakeStore.number;
        } else {
            return null;
        }
    }
    getSource() {

        if (this.isServiceCaseData()) {
            return AppConstants.CASE_TYPE.SERVICE_CASE;
        } else if (this.isIntakeMode()) {
            return AppConstants.CASE_TYPE.INTAKE;
        } else {
            return AppConstants.CASE_TYPE.CPS_CASE;
        }
        // need to add condition for adoption case
    }


}
