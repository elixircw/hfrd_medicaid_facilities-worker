import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecordingComponent } from './recording.component';
import { NotesComponent } from './notes/notes.component';
import { FamilyInvolvementMeetingComponent } from './family-involvement-meeting/family-involvement-meeting.component';
import { ResourceConsultComponent } from './resource-consult/resource-consult.component';
import { MatTooltipModule } from '@angular/material';

const routes: Routes = [{
  path: '',
  component: RecordingComponent,
  children: [
    {
      path: 'notes',
      component: NotesComponent
    },
    {
      path: 'family-involvement-meeting',
      component: FamilyInvolvementMeetingComponent
    },
    {
      path: 'resource-consult',
      component: ResourceConsultComponent
    },
    {
      path: 'djsnotes',
      loadChildren: './djsnotes/djsnotes.module#DjsnotesModule'
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule,
    MatTooltipModule,
  ]
})
export class RecordingRoutingModule { }
