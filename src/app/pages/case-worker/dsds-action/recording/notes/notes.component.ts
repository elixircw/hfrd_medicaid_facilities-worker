import { DatePipe } from '@angular/common';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { Subject } from 'rxjs/Rx';
import { ObjectUtils } from '../../../../../@core/common/initializer';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, ValidationService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { CaseWorkerContactRoles, CaseWorkerRecording, CaseWorkerRecordingEdit, CaseWorkerRecordingType, ProgressNoteRoleType, SearchRecording } from '../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { Attachment } from '../../attachment/_entities/attachment.data.models';
import * as category from '../_configurations/category.json';
import * as sortByData from '../_configurations/sortBy.json';
import * as status from '../_configurations/status.json';
import * as type from '../_configurations/type.json';
import { ParticipantType, ReasonForContact, RecordingNotes } from '../_entities/recording.data.model';
import { SpeechRecognitionService } from '../../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { DsdsService } from '../../_services/dsds.service';
import * as jsPDF from 'jspdf';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { NavigationUtils } from '../../../../_utils/navigation-utils.service';
import { AppConstants } from '../../../../../@core/common/constants';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import { IntakeStoreConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { MatRadioChange } from '@angular/material';
import { AppConfig } from '../../../../../app.config';
import { config } from '../../../../../../environments/config';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';
import { json } from 'ng4-validators/src/app/json/validator';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    encapsulation: ViewEncapsulation.None,
    selector: 'notes',
    templateUrl: './notes.component.html',
    styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit, OnDestroy {
    searchparams: any;
    recordingSubTypeDropDown: any;
    startEndTimeValidator: boolean;
    searchParamArr: any[];
    searchParameter: any;
    attachmentGrid$: Observable<Attachment[]>;
    dateValidation = true;
    viewEdit: string;
    currentDate: Date = new Date();
    categoryForm: FormGroup;
    updateAppend: FormGroup;
    commentForm: FormGroup;
    courtForm: FormGroup;
    source: string;
    sourceEdited: string;
    // isOthers: boolean;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    id: string;
    daNumber: string;
    multipleRoles: string;
    recordingForm: FormGroup;
    emailForm: FormGroup;
    searchCategoryForm: FormGroup;
    // actionSelectionForm: FormGroup;
    saveButton: boolean;
    isCW: boolean;
    isInvolvePerson: boolean;
    isotherPerson: boolean;
    isUploadClicked: boolean;
    recordingCategory = false;
    contactpurpose = false;
    notetext = false;
    searchWorkerName = false;
    searchContactType = false;
    searchLocation = false;
    sortBy = false;
    recordDate = false;
    contactDate = false;
    paginationInfo: PaginationInfo = new PaginationInfo();
    recordingedit: CaseWorkerRecordingEdit = new CaseWorkerRecordingEdit();
    editRecord: RecordingNotes = new RecordingNotes();
    viewRecord: RecordingNotes = new RecordingNotes();
    appendNoteControl: AbstractControl;
    typeDropdown: DropdownModel[];
    statusDropdown: DropdownModel[];
    categoryDropdown: DropdownModel[];
    sortDropDown: DropdownModel[];
    userInfo: AppUser;
    addNotes: RecordingNotes = new RecordingNotes();
    recordingDetail: CaseWorkerRecording;
    recording: RecordingNotes[];
    totalRecords$: Observable<number>;
    totalRecords: number;
    canDisplayPager$: Observable<boolean>;
    recordingType$: Observable<CaseWorkerRecordingType[]>;
    recordType: any;
    contactRoles$: Observable<CaseWorkerContactRoles[]>;
    participantType$: Observable<ParticipantType[]>;
    //recordingSubTypeDropDown$: Observable<CaseWorkerRecordingType[]>;
    progressNoteRoleType: ProgressNoteRoleType[] = [];
    personNameDescription: string[] = [];
    duration: string;
    isCourtDetails = false;
    progressNoteActor = ([] = []);
    private pageSubject$ = new Subject<number>();
    private recordSearch = new SearchRecording();
    reasonForContact$: Observable<ReasonForContact[]>;
    reasonForContact: ReasonForContact[] = [];
    personRoles = ([] = []);
    collateralPersons = [];
    involvedPersons = [];
    edittimeduration = false;
    isSerachResultFound: boolean;
    minDate = new Date();
    maxDate = new Date();
    actors = [];
    Others = { intakeservicerequestactorid: 'Others' };
    initiateText: boolean;
    attempText: boolean;
    initialFace = false;
    stateValuesDropdownItems$: Observable<DropdownModel[]>;
    CountyValuesDropdownItems$: Observable<DropdownModel[]>;
    recognizing = false;
    speechRecogninitionOn: boolean;
    //showparticipanttypekey: boolean;
    speechData: string;
    notification: string;
    agency: string;
    currentLanguage: string;
    enableAppend = false;
    isServiceCase = false;
    personinvolved = [];
    selectedNotes: string;
    pageNumber: number;
    histData = [];
    userRole: any;
    isIntakeWorker: boolean;
    uploadedDocuments = [];
    personid='';
    attachmenttype='case';
    uploadType='note';
    isDateCheck = false;
    isDate24Check = false;
    // Variables when viewing contact note in read-only mode
    calculatedContactDuration: string;
    calculatedTravelDuration: string;

    // Least and max date for a contact note to be added
    maxContactDate = new Date(); // Max date will always be todays date
    minContactDate = new Date(); // Min date will vary depending on Intake or Case received date

    // Filter, Search or Sort
    selectedActionType: string;
    downldSrcURL: string;
    baseUrl: string;
    personContacted = false;
    insertedByDetails: any[];
    // caseNumber: string;
    caseType: string;
    entityType: string;
    selectedRecord: any;
    isClosed = false;
    qualityForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _commonHttpService: CommonHttpService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _route: Router,
        private datePipe: DatePipe,
        private _speechRecognitionService: SpeechRecognitionService,
        private speechRecognizer: SpeechRecognizerService,
        private _dsdsService: DsdsService,
        private _dataStoreService: DataStoreService,
        private _navigationUtils: NavigationUtils,
        private _session: SessionStorageService,
        private _intakeUtils: IntakeUtils
    ) {

        this.baseUrl = AppConfig.baseUrl;
    }

    ngOnInit() {
        this.viewEdit = 'Add';
        this.startEndTimeValidator = false;
        this.getContactLocationsDropDown();
        this.id = this.getCaseUuid();
        this.caseType = this.getCurrentCaseType();
        this.entityType = this.getEntityType();
        this.daNumber = this.getCaseNumber();
        this.source = this.getCurrentCaseType();
        this.userRole = this._authService.getCurrentUser();
        console.log('this.userRole -- '+JSON.stringify(this.userRole));
        this.isIntakeWorker =
            this.userRole.role.name === AppConstants.ROLES.INTAKE_WORKER;

        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.isServiceCase = this._dsdsService.isServiceCase();
        this.agency = this._authService.getAgencyName();
        this.currentLanguage = 'en-US';
        this.isUploadClicked = false;
        this.speechRecognizer.initialize(this.currentLanguage);
        this.statusDropdown = <any>status;
        this.typeDropdown = <any>type;
        this.categoryDropdown = <any>category;
        this.sortDropDown = <any>sortByData;
        this.isCW = this._authService.isCW();
        this.getStateDropdown();
        this.getCountyDropdown();
        this.formInitilize();
        //this.showparticipanttypekey = false;
        this.recordingForm.controls['attemptindicator'].clearValidators();
        this.recordingForm.controls['attemptindicator'].updateValueAndValidity();

        this.pageSubject$.subscribe((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            this.getPage(this.paginationInfo.pageNumber);
        });
        this.getPage(1);
        this.recordingDropDown();
        this.attachmentDropdown();
        this.getInvolvedPerson();
        this.getcollateral();
        this.getReasonForContact();
        this.appendNoteControl = this.recordingForm.get('appendtitle');
        this.userInfo = this._authService.getCurrentUser();
        // this.recordingForm.get('starttime').valueChanges.subscribe(
        //     startTime => {
        //       console.log('startTime changed:' + startTime);
        //       this.timeDuration();
        //     }
        // );
        // this.recordingForm.get('endtime').valueChanges.subscribe(
        //     endtime => {
        //       console.log('endtime changed:' + endtime);
        //       this.timeDuration();
        //     }
        //   );

        this.setMinContactDate();
        // this.setupTimeChangeSubscribers();

        const da_status = this._session.getItem('da_status');
        if (da_status) {
            if (da_status === 'Closed' || da_status === 'Completed') {
                this.isClosed = true;
            } else {
                this.isClosed = false;
            }
        }

        const currentStatus = this._dataStoreService.getData(IntakeStoreConstants.INTAKE_STATUS);
        if (currentStatus) {
            if (currentStatus === 'Closed' || currentStatus === 'Completed') {
                this.isClosed = true;
            } else {
                this.isClosed = false;
            }
        }

        this.formInvolvedPersonsDropDown();
        $('body').on('shown.bs.modal', '#myModal-recordings', function () {
            $('#myModal-recordings .modal-body').scrollTop(0);
        });

    }

    setMinContactDate() {
        const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
        const intakeReceivedDate = this._dataStoreService.getData(IntakeStoreConstants.receivedDate);

        if (this.isIntakeMode() && intakeReceivedDate) {
            // For intake min contact date will be the Intake/Referral received date
            this.minContactDate = intakeReceivedDate;
        } else {
            // For case min contact date will be the Case received date
            if (caseInfo) {
                this.minContactDate = caseInfo.da_receiveddate;
            }
        }
    }

    setNotes(record) {
        this.selectedRecord = record;
        //console.log("RECORD in setnotes", record);
        if (record && record.progressnotepurposetypekey) {
            this.selectedNotes = record.progressnotepurposetypekey;
        } else {
            this.selectedNotes = '';
        }
    }

    private formInitilize() {
        this.recordingForm = this.formBuilder.group(
            {
                progressnotetypeid: ['', Validators.required],
                progressnotesubtypeid: [null],
                contactdate: ['', Validators.required],
                locationname: [''],
                documentpropertiesid: [''],
                firstname: [''],
                lastname: [''],
                address1: [''],
                address2: '',
                city: [''],
                state: '',
                county: '',
                zipcode: [''],
                email: ['', ValidationService.mailFormat],
                phonenumber: [''],
                initiationindicator: ['', Validators.required],
                attemptindicator: [''],
                description: ['', Validators.required],
                appendtitle: [''],
                starttime: [{ value: null, disabled: true }],
                endtime: [{ value: null, disabled: true }],
                //participanttypekey: [null],
                intakeservicerequestactorid: [''],
                progressnotereasontypekey: ['', Validators.required],
                travelhours: ['', Validators.maxLength(3)],
                travelminutes: ['', Validators.maxLength(2)],
                durationhours: [{ value: null, disabled: true }, Validators.maxLength(3)],
                durationminutes: [{ value: null, disabled: true }, Validators.maxLength(2)],
                progressnotepurposetypekey: [null],
                progressnoteroletype: [null],
                progressnoteid: [null],
                others: null
            },
            { validator: this.checkTimeValidation }
        );

        this.courtForm = this.formBuilder.group({
            issuedesc: [''],
            safetydesc: [''],
            services_childdesc: [''],
            services_parentdesc: [''],
            permanencystepdesc: [''],
            placementdesc: [''],
            educationdesc: [''],
            healthdesc: [''],
            socialareadesc: [''],
            financialliteracydesc: [''],
            familyplanningdesc: [''],
            skillissuedesc: [''],
            transitionplandesc: ['']
        });

        this.updateAppend = this.formBuilder.group({
            appendtitle: ['', Validators.required]
        });

        this.searchCategoryForm = this.formBuilder.group(
            {
                draft: [''],
                type: [''],
                datefrom: [new Date(), Validators.required],
                dateto: [new Date(), Validators.required],
                contactdatefrom: [new Date(), Validators.required],
                contactdateto: [new Date(), Validators.required],
                progressnotereasontypekey: [''],
                note: [''],
                workerName: [''],
                insertedby: [''],
                sortBy: ['contactdate'],
                sortDir: ['desc'],
                recordingsubtype: [null],
                recordingtype: [null],
                intakeservicerequestactorids: [null]
            },
            {
                validator: Validators.compose([ValidationService.checkDateRange('starttime', 'endtime')])
            }
        );

        this.categoryForm = this.formBuilder.group({
            category: ['']
        });

        // this.actionSelectionForm = this.formBuilder.group({
        //     selectedaction: ['']
        // });

        this.emailForm = this.formBuilder.group({
            email: ['', [ValidationService.mailFormat, Validators.required]]
        });

        this.commentForm = this.formBuilder.group({
            description: ['', [Validators.required]]
        });

        this.qualityForm = this.formBuilder.group({
            qualityofcaretochildtext: [''],
            adjustmentfostercaretext: [''],
            screeningfortheservicetext: [''],
            ischildgotoshool: ['']
        });

    }

    openAddendum() {
        (<any>$('#myModal-recordings')).modal('hide');
        (<any>$('#CommentDialog')).modal('show');
        (<any>$('#upload-attachment')).modal('hide');
        this.commentForm.patchValue({ description: '' });
    }

    saveAddendum() {
        let username;
        this._authService.currentUser.subscribe((userInfo) => {
            username = userInfo.user.userprofile.displayname

        });
        const comment = {
            progressnoteid: this.editRecord.progressnoteid,
            description: this.commentForm.getRawValue().description,
            insertedon: new Date(),
            isaddendum: 1,
            displayname: username
        };
        this._commonHttpService.create(comment, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.AddComments).subscribe(
            (result) => {
                (<any>$('#CommentDialog')).modal('hide');
                if (this.editRecord.notedetails && this.editRecord.notedetails.length) {
                    this.editRecord.notedetails.push(comment);
                } else {
                    this.editRecord.notedetails = [];
                    this.editRecord.notedetails.push(comment);
                }
                this.editRecording(this.editRecord, 'Edit', 'draftEdit');
                (<any>$('#myModal-recordings')).modal('show');
            },
            (error) => {
                console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    saveQualityCare() {
        let username;
        this._authService.currentUser.subscribe((userInfo) => {
            username = userInfo.user.userprofile.displayname

        });
        const qualityCare = {
            progressnoteid: this.viewRecord.progressnoteid,
            qualityofcaretochildtext: this.qualityForm.getRawValue().qualityofcaretochildtext,
            screeningfortheservicetext: this.qualityForm.getRawValue().screeningfortheservicetext,
            adjustmentfostercaretext: this.qualityForm.getRawValue().adjustmentfostercaretext,
            ischildgotoshool: this.qualityForm.getRawValue().ischildgotoshool
        };
        this._commonHttpService.create(qualityCare, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.AddQualityCare).subscribe(
            (result) => {
                this._alertService.success('Quality care data saved successfully!');
                (<any>$('#iframe-Quality-care')).modal('hide');
                this.getPage(1);
            },
            (error) => {
                console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    addOtherPerson() {
        console.log('ADDING OTHERS');
        this.recordingForm.patchValue({
            others: ''
        });
        this.recordingForm.controls['intakeservicerequestactorid'].clearValidators();
        this.recordingForm.controls['intakeservicerequestactorid'].updateValueAndValidity();
        this.isotherPerson = true;
    }

    removeOtherPerson() {
        console.log('REMOVING OTHERS');
        this.recordingForm.patchValue({
            others: null
        });
        this.recordingForm.controls['intakeservicerequestactorid'].setValidators([Validators.required]);
        this.recordingForm.controls['intakeservicerequestactorid'].updateValueAndValidity();
        this.isotherPerson = false;
    }

    // TODO : We should have delcare a composit object but
    private setInitialFaceToFace(recording) {
        if (recording.recordingtype === 'Initialfacetoface') {
            //this.showparticipanttypekey = true;
            this.initialFace = true;
            this.recordingForm.controls['attemptindicator'].setValidators([Validators.required]);
            this.recordingForm.controls['attemptindicator'].updateValueAndValidity();
        } else {
            //this.showparticipanttypekey = false;
            this.initialFace = false;
            this.recordingForm.controls['attemptindicator'].clearValidators();
            this.recordingForm.controls['attemptindicator'].updateValueAndValidity();
        }
        if (recording && recording.contactparticipant && recording.contactparticipant.length) {
            this.recordingForm.patchValue({
                firstname: recording.contactparticipant[0].firstname,
                lastname: recording.contactparticipant[0].lastname,
                address1: recording.contactparticipant[0].address1,
                address2: recording.contactparticipant[0].address2,
                city: recording.contactparticipant[0].city,
                state: recording.contactparticipant[0].state,
                county: recording.contactparticipant[0].county,
                zipcode: recording.contactparticipant[0].zipcode,
                email: recording.contactparticipant[0].email,
                phonenumber: recording.contactparticipant[0].phonenumber,
                //participanttypekey: recording.contactparticipant[0].participanttypekey,
            });
        }
        this.checkChangeDateValue(this.recordingForm.controls['contactdate'].value);
    }

    private getInvolvedPerson() {
        if(this.personRoles && this.personRoles.length == 0){
            const inputRequest = this.getRequestParam();
        this._commonHttpService
            .getPagedArrayList(
                {
                    where: inputRequest,
                    page: 1,
                    limit: 30,
                    method: 'get'
                },
                'People/getpersondetailcw' + '?filter'
            )
            .subscribe((itm) => {
                if (itm.data) {
                    this.personRoles = [];
                    itm.data.map((list) => {
                        return this.personRoles.push({
                            intakeservicerequestactorid: list.roles ? list.roles[0].intakeservicerequestactorid : null,
                            displayname: list.firstname + ' ' + list.lastname
                                + '(' + ((list.roles && list.roles.length && list.roles[0].typedescription) ? list.roles[0].typedescription : '') + ')',
                            personname: list.firstname + ' ' + list.lastname,
                            role: list.roles,

                        });
                    });
                }
            });
        }
    }

    getcollateral() {
        if(this.collateralPersons && this.collateralPersons.length == 0){
            const request = {
                objectid: this.getCaseUuid(),
                objecttype: 'case'
              };
              this._commonHttpService.getArrayList(
                {
                  where: request,
                  method: 'get',
                  nolimit: true
                },
                'collateral/list?filter'
              ).subscribe(res => {
                if (res && res.length && res[0].getcollateraldetails && res[0].getcollateraldetails.length) {
                  this.collateralPersons = res[0].getcollateraldetails;        
                }
              });
        }
      }

    formInvolvedPersonsDropDown() {
        this.involvedPersons = [];
        this.personRoles.forEach(personRole => {
            this.involvedPersons.push({
                involvedPersonId: personRole.intakeservicerequestactorid,
                displayName: personRole.displayname,
                isCollateral:false
            })
        });

        this.collateralPersons.forEach(collateralPerson => {
            this.involvedPersons.push({
                involvedPersonId: collateralPerson.collateralid,
                displayName: collateralPerson.fullname + '(' + (collateralPerson.collateralroleconfig ? (collateralPerson.collateralroleconfig.length > 0 ? collateralPerson.collateralroleconfig[0].description : '') : '') + ')',
                isCollateral:true
            })
        });
        return true;
    }
    
    getStateDropdown() {
        this.stateValuesDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                'States?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.statename,
                            value: res.stateid
                        })
                );
            });
    }
    getCountyDropdown() {
        this.CountyValuesDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                'admin/county?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }

    getReasonForContact() {
        this.reasonForContact$ = this._commonHttpService.getSingle({}, 'Progressnotereasontypes?filter={"nolimit":true}').map((itm) => {
            return itm;
        });
        this.reasonForContact$.subscribe(data => {
            this.reasonForContact = data;
        });
    }
    getPage(page: number) {
        this.recordSearch.progressnotereasontypekey =
            this.searchCategoryForm.getRawValue().progressnotereasontypekey ?
                this.searchCategoryForm.getRawValue().progressnotereasontypekey.join() : '';
        ObjectUtils.removeEmptyProperties(this.recordSearch);

        const source = this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: this.paginationInfo.pageNumber,
                    limit: this.paginationInfo.pageSize50,
                    where: this.recordSearch,
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetAllDaRecordingUrl + '/' + this.id + '?data'
            )
            .subscribe((result) => {
                this.recording = result.data;
                if(this.recording){
                    this.recording.forEach((record) => {
                        record.duration = this.calculateContactDurationForViewRecord(record);
                    });
                }
                this.getInsertedUser(this.id);
                this.pageNumber = page;
                if (page === 1) {
                    this.totalRecords = result.data.length ? result.data[0].totalcount : 0;
                }
            });
    }
    getInsertedUser(id) {
        this._commonHttpService.getArrayList({
            where: {
                entitytypeid: id
            },
            method: 'get'
        }, 'admin/progressnote/getCaseWorkerList?filter').subscribe(response => {
            if (response && response.length > 0) {
                this.insertedByDetails = response;
            }
        });
    }
    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageSubject$.next(this.paginationInfo.pageNumber);
    }

    changeSelectedActionType(event: MatRadioChange) {
        this.resetSearchForm();
        this.resetAllSearchFlags();

        if (event.value === 'SORT') {
            this.sortBy = true;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.recordDate = false;
            this.contactDate = false;
            this.notetext = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;
        } else if (event.value === 'SEARCH') {
            this.sortBy = false;
            this.searchWorkerName = false;
            this.notetext = true;
            this.contactpurpose = false;
            this.recordDate = false;
            this.contactDate = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;
        } else {
            // Filter by default
        }
    }

    resetSelectedActionType() {
        this.selectedActionType = null;
        this.categoryForm.controls['category'].reset();

        // Get all the default results
        this.resetSearchForm();
        this.resetAllSearchFlags();
        this.getPage(1);
    }

    resetAllSearchFlags() {
        this.sortBy = false;
        this.notetext = false;
        this.searchWorkerName = false;
        this.contactpurpose = false;
        this.recordDate = false;
        this.recordingCategory = false;
        this.contactDate = false;
        this.searchContactType = false;
        this.searchLocation = false;
        this.personContacted = false;
    }

    resetSearchForm() {
        this.isSerachResultFound = true;
        this.searchCategoryForm.reset();
        this.recordSearch = Object.assign({});
        this.searchCategoryForm.patchValue({ draft: '', type: '' });
        this.displaySearch();
    }

    getData() {
        return {
            draft: 'Draft',
            type: 'Type',
            datefrom: 'Data From',
            dateto: ' Data To',
            contactdatefrom: 'Contact Data From',
            contactdateto: 'Contact Data To',
            progressnotereasontypekey: 'Contact Purpose',
            note: 'Note',
            workerName: 'WorkerName',
            insertedby: 'Worker Name',
            recordingsubtype: 'Contact Location',
            recordingtype: 'Type Of Contact',
            intakeservicerequestactorids: ' Person Contacted'
        }
    }

    displaySearch() {
        this.searchParameter = this.searchCategoryForm.getRawValue();
        var x
        var dataMap = this.getData();
        this.searchParamArr = [];
        for (x in this.searchParameter) {
            var data = dataMap[x] + ":" + this.setTransFromData(x, this.searchParameter[x]);
            if (this.searchParameter[x]) {
                this.searchParamArr.push(data);
            }

        }
        this.searchparams = this.searchParamArr.join(',');
    }
    setTransFromData(x, value) {
        if (x === 'contactdatefrom') {
            return value;
        }
        if (x === 'contactdateto') {
            return value;
        }
        if (x === 'progressnotereasontypekey' && value && value.length) {
            var values = '';
            for (var key in value) {
                var arr = this.reasonForContact.filter((item) => item.progressnotereasontypekey === value[key]);
                if (arr && arr.length) {
                    values = values + ',' + arr[0].typedescription;
                }
            }
            return values;
        }
        if (x === 'insertedby') {
            var values = '';

            const progressnote = this.insertedByDetails.filter((item) => item.userid === value);
            if (progressnote && progressnote.length) {
                values = progressnote[0].fullname;
            }


            return values;
        }
        if (x === 'recordingsubtype') {
            var values = '';

            const progressnote = this.recordingSubTypeDropDown.filter((item) => item.progressnotetypeid === value);
            if (progressnote && progressnote.length) {
                values = progressnote[0].description;
            }


            return values;
        }
        if (x === 'recordingtype') {
            var values = '';

            const progressnote = this.recordType.filter((item) => item.progressnotetypeid === value);
            if (progressnote && progressnote.length) {
                values = progressnote[0].description;
            }

            return values;
        }
        if (x === 'intakeservicerequestactorids' && value && value.length) {
            var values = '';
            for (var key in value) {
                if (this.personRoles) {
                    var data = this.personRoles.filter((item) => item.intakeservicerequestactorid === value[key]);
                    if (data && data.length) {
                        values = values + ',' + data[0].displayname;
                    }
                }
               
            }
            return values;
        }

        return value;
    }
    getKey(value) {
        var x;
        for (x in this.getData()) {
            if (this.getData()[x] === value) {
                return x;
            }
        }
    }
    clearSearchParam(param) {
        var key = param.split(':')[0];
        key = this.getKey(key);
        this.searchCategoryForm.controls[key].setValue('');
        // this.searchCategoryForm.patchValue({key : });
        this.searchRecording(this.searchCategoryForm.value);
    }

    changeCategory(event: any) {
        // this.isSerachResultFound = true;
        // this.searchCategoryForm.reset();
        // this.recordSearch = Object.assign({});
        //this.resetSearchForm(); // refactored all these above to a method

        // @Simar - This is scary code, refactor this whole logic if possible -- eventually! hopefully!
        // Just moving the Sort & Search into changeSelectedActionType based on radio button selection
        // All the remaining drop down values in category will work for filering as before
        if (event.value === 'Sortby') { // Removed this value from category dropdown
            // this.sortBy = true;
            // this.searchWorkerName = false;
            // this.contactpurpose = false;
            // this.recordDate = false;
            // this.contactDate = false;
            // this.notetext = false;
        } else if (event.value === 'CaseWorkerName') {
            this.sortBy = false;
            this.searchWorkerName = true;
            this.contactpurpose = false;
            this.recordDate = false;
            this.contactDate = false;
            this.notetext = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;

        } else if (event.value === 'Note') { // Removed this value from category dropdown
            // this.sortBy = false;
            // this.searchWorkerName = false;
            // this.notetext = true;
            // this.contactpurpose = false;
            // this.recordDate = false;
            // this.contactDate = false;
        } else if (event.value === 'Contactpurpose') {
            this.sortBy = false;
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = true;
            this.recordDate = false;
            this.contactDate = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;
        } else if (event.value === 'RecordingCategory') {
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.recordingCategory = true;
            this.recordDate = false;
            this.contactDate = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;
        } else if (event.value === 'RecordingDate') {
            this.sortBy = false;
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.recordDate = true;
            this.contactDate = false;
            this.recordingCategory = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;
        } else if (event.value === 'ContactDate') {
            this.sortBy = false;
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.contactDate = true;
            this.recordDate = false;
            this.recordingCategory = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;
        } else if (event.value === 'ContactLocation') {
            this.sortBy = false;
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.contactDate = false;
            this.recordDate = false;
            this.recordingCategory = false;
            this.searchContactType = false;
            this.searchLocation = true;
            this.personContacted = false;
        } else if (event.value === 'TypeofContact') {
            this.sortBy = false;
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.contactDate = false;
            this.recordDate = false;
            this.recordingCategory = false;
            this.searchContactType = true;
            this.searchLocation = false;
            this.personContacted = false;
        } else if (event.value === 'PersonContacted') {
            this.sortBy = false;
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.contactDate = false;
            this.recordDate = false;
            this.recordingCategory = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = true;
            this.formInvolvedPersonsDropDown();
        } else {
            this.sortBy = false;
            this.notetext = false;
            this.searchWorkerName = false;
            this.contactpurpose = false;
            this.recordDate = false;
            this.recordingCategory = false;
            this.contactDate = false;
            this.searchContactType = false;
            this.searchLocation = false;
            this.personContacted = false;
            this.getPage(1);
        }
        // this.searchCategoryForm.patchValue({ draft: '', type: '' });
    }
    recordingSubType(progressnotetypeid: string, recordingtype?: string) {
        const progressNoteTypeKey = progressnotetypeid.split('~');

        if (progressNoteTypeKey[1] === 'Initialfacetoface') {
            //this.showparticipanttypekey = true;
            this.recordingForm.controls['attemptindicator'].setValidators([Validators.required]);
            this.recordingForm.controls['attemptindicator'].updateValueAndValidity();
        } else {
            //this.showparticipanttypekey = false;
            this.recordingForm.controls['attemptindicator'].clearValidators();
            this.recordingForm.controls['attemptindicator'].updateValueAndValidity();
        }

        if (progressNoteTypeKey[1] === 'Court approved Trial Home Visit') {
            this.isCourtDetails = true;
        } else {
            this.isCourtDetails = false;
        }
        if (progressNoteTypeKey[1] === 'Email') {
            this.recordingForm.get('progressnotesubtypeid').reset();
            this.recordingForm.get('progressnotesubtypeid').disable();
        } else {
            this.recordingForm.get('progressnotesubtypeid').enable();
        }

        if (progressNoteTypeKey[1] === 'Initialfacetoface' || progressNoteTypeKey[1] === 'Face To Face'
            || progressNoteTypeKey[1] === 'Weekly Visits' || progressNoteTypeKey[1] === 'Walk-in' ||
            recordingtype === 'Initialfacetoface' || recordingtype === 'Face To Face'
            || recordingtype === 'Weekly Visits' || recordingtype === 'Walk-in') {
            this.recordingForm.get('starttime').setValidators([Validators.required]);
            this.recordingForm.get('starttime').updateValueAndValidity({ onlySelf: true,emitEvent: false });
            this.recordingForm.get('endtime').setValidators([Validators.required]);
            this.recordingForm.get('endtime').updateValueAndValidity({ onlySelf: true,emitEvent: false });
            this.startEndTimeValidator = true;
        } else {
            this.startEndTimeValidator = false;
            this.recordingForm.get('starttime').clearValidators();
            this.recordingForm.get('starttime').updateValueAndValidity({ onlySelf: true,emitEvent: false });
            this.recordingForm.controls['endtime'].clearValidators();
            this.recordingForm.controls['endtime'].updateValueAndValidity({ emitEvent: false });
        }

        // @Simar
        // This does not make sense to make an API call for Contact locations every single time Contact Type changes
        // So making this call just once at the init to polulate this dropdown
        // the dropdown can be disabled as needed, no need to make the get call again

        // if (progressNoteTypeKey[0]) {
        //     this.recordingSubTypeDropDown$ = this._commonHttpService
        //         .getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.ListProgressSubTypeUrl + '?prognotetypeid=' + progressNoteTypeKey[0])
        //         .map((result) => {
        //             return result;
        //         });
        // }

        if (progressNoteTypeKey[1] !== 'Initialfacetoface') {
            // this.checkInitialFaceToFace();
            this.initialFace = false;
        } else {
            this.initialFace = true;
        }
    }
    searchRecording(modal: SearchRecording) {
        this.displaySearch();
        if (modal.contactdateto) {
            modal.contactdateto = moment(modal.contactdateto)
            .utc()
            .format();
        }
        if (modal.contactdatefrom) {
            modal.contactdatefrom = moment(modal.contactdatefrom)
                .utc()
                .format();
        }
        if (modal.dateto) {
            modal.dateto = moment(modal.dateto)
                .utc()
                .format();
        }
        if (modal.datefrom) {
            modal.datefrom = moment(modal.datefrom)
                .utc()
                .format();
        }
        this.recordSearch = modal;
        this.getPage(1);
    }
    personRoleTypeChange(model) {
        if (model === 'IP') {
            this.recordingForm.controls['intakeservicerequestactorid'].setValidators([Validators.required]);
            this.recordingForm.controls['intakeservicerequestactorid'].updateValueAndValidity();
        } else {
            // this.recordingForm.controls['intakeservicerequestactorid'].reset();
            this.recordingForm.controls['intakeservicerequestactorid'].clearValidators();
            this.recordingForm.controls['intakeservicerequestactorid'].updateValueAndValidity();
        }
    }

    changePersonInvolved(item) {
        this.personinvolved = item.map((res) => {
            return {
                subtancekey: res,
                others: ''
            };
        });
    }

    personRoleList(model) {
        if(this.involvedPersons && this.involvedPersons.length===0){
            this.getInvolvedPerson();
            this.getcollateral();
            this.formInvolvedPersonsDropDown();
        }
        if (model) {
            const removalReasonItems = this.involvedPersons.filter((item) => {
                if (model.includes(item.involvedPersonId)) {
                    return item;
                }
            });
            this.changePersonInvolved(model);
            this.personNameDescription = removalReasonItems.map((res) => {
                return res.displayName;
            });
            // if (this.recordingForm.controls['participanttypekey'].value === 'IP') {
            if (model) {
                this.multipleRoles = '';
                const progressNoteRoleType = model.map((res) => {
                    if (res && res.intakeservicerequestactorid === 'Others') {
                        // (<any>$('#myModal-recordings')).modal('hide');
                        // this._speechRecognitionService.destroySpeechObject();
                        // (<any>$('#personsTab')).click();
                        // this._route.routeReuseStrategy.shouldReuseRoute = function() {
                        //     return false;
                        // };
                        // const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/involved-person';
                        // this._route.navigateByUrl(currentUrl).then(() => {
                        //     this._route.navigated = false;
                        //     this._route.navigate([currentUrl]);
                        // });
                        // this._route.navigate([currentUrl], { relativeTo: this.route });
                    }
                    if (this.multipleRoles === '') {
                        this.multipleRoles = this.multipleRoles + '' + res;
                    } else {
                        this.multipleRoles = this.multipleRoles + ', ' + res;
                    }
                    const selectedPerson = this.involvedPersons ? this.involvedPersons.find(item => item.involvedPersonId == res) : null;

                    return { intakeservicerequestactorid: res, participanttypekey : selectedPerson?(selectedPerson.isCollateral? 'COLLATERAL':'IP'):null//, participanttypekey: 'IP' 
                };
                });
                this.progressNoteRoleType = progressNoteRoleType;
            }
            // }
        }
    }
    // timeDuration() {
    //     // const start_date = moment(this.recordingForm.value.starttime, 'HH:mm a');
    //     // const end_date = moment(this.recordingForm.value.endtime, ' HH:mm a');
    //     const start_date = moment(moment(this.recordingForm.getRawValue().contactdate).format('MM/DD/YYYY') + ' ' + moment(this.recordingForm.getRawValue().starttime).format('HH:mm'));
    //     const end_date = moment(moment(this.recordingForm.getRawValue().contactdate).format('MM/DD/YYYY') + ' ' + moment(this.recordingForm.getRawValue().endtime).format('HH:mm'));

    //     if (start_date.isAfter(end_date)) {
    //          this._alertService.error('Start time must be before end time!');
    //     } else {
    //         const duration = moment.duration(end_date.diff(start_date));
    //         if (duration['_data'] && this.recordingForm.value.starttime && this.recordingForm.value.endtime) {
    //             this.recordingForm.get('durationhours').reset();
    //             this.recordingForm.get('durationminutes').reset();
    //             this.recordingForm.controls['durationhours'].patchValue((duration['_data'].hours));
    //             this.recordingForm.controls['durationminutes'].patchValue((duration['_data'].minutes));
    //             // this.duration = duration['_data'].hours + ' Hr :' + duration['_data'].minutes + ' Min';
    //         }
    //     }
    // }

    checkTimeValidation(group: FormGroup) {
        // if (!group.controls.endtime.value || group.controls.endtime.value !== '') {
        //     if (group.controls.endtime.value < group.controls.starttime.value) {
        //         return { notValid: true };
        //     }
        return null;
        // }
    }

    // 'recording' variable is basically just the raw value of recordingForm
    saveRecording(recording, saveType: string) {

        if (!this.recordingForm.valid) {
            this._alertService.error('Please fill the required fields');
            return;
        }
        if (this.recordingForm.valid && this.recordingForm.get('others').value === '' && this.recordingForm.get('attemptindicator').value === 'no') {
            this._alertService.error('Other person cannot be empty');
            return;
        }



        if (this.checkParticipantsRole(saveType, recording)) {

            // @Simar: This is not making any sense, we should not be changing time like this before saving
            // if (recording.contactdate) {
            //     recording.contactdate = moment(recording.contactdate)
            //         .utc()
            //         .format();
            // }

            // The matinput time is text so need to turn it to timestamp
            recording.starttime = this.convertMatinputTimeToTimestamp(recording.contactdate, recording.starttime);
            recording.endtime = this.convertMatinputTimeToTimestamp(recording.contactdate, recording.endtime);

            const progressNoteTypeKey = this.recordingForm.get('progressnotetypeid').value.split('~');
            recording.progressnotetypeid = progressNoteTypeKey[0];
            if (recording.attemptindicator !== null) {
                recording.attemptindicator = recording.attemptindicator === 'yes' ? true : false;
            }
            if (recording.initiationindicator !== null) {
                recording.initiationindicator = recording.initiationindicator === 'yes' ? true : false;
            }
            /*
            if (this.recordingForm.controls['participanttypekey'].value) {
                const data = Object.assign({
                    intakeservicerequestactorid: null,
                    participanttypekey: this.recordingForm.value.participanttypekey ? this.recordingForm.value.participanttypekey : '',
                    firstname: this.recordingForm.value.firstname ? this.recordingForm.value.firstname : '',
                    lastname: this.recordingForm.value.lastname ? this.recordingForm.value.lastname : '',
                    address1: this.recordingForm.value.address1 ? this.recordingForm.value.address1 : '',
                    address2: this.recordingForm.value.address2 ? this.recordingForm.value.address2 : '',
                    city: this.recordingForm.value.city ? this.recordingForm.value.city : '',
                    state: this.recordingForm.value.state ? this.recordingForm.value.state : '',
                    zipcode: this.recordingForm.value.zipcode ? this.recordingForm.value.zipcode : '',
                    email: this.recordingForm.value.email ? this.recordingForm.value.email : '',
                    phonenumber: this.recordingForm.value.phonenumber ? this.recordingForm.value.phonenumber : ''
                });
                // Below line was commented to avoid multiple other roletypes added to the Edit Notes
                // this.progressNoteRoleType.push(data);
            } */
            this.addNotes = Object.assign(
                {
                    contactparticipant: this.progressNoteRoleType,
                    entitytypeid: this.id,
                    savemode: saveType === 'SAVE' ? 1 : 0,
                    contacttrialvisit: this.courtForm.value,
                    entitytype: this.entityType,
                    stafftype: 1,
                    instantresults: 1,
                    contactstatus: false,
                    drugscreen: false,
                    description: recording.detail,
                    traveltime: recording.travelhours ? recording.travelhours + ':' + recording.travelminutes : '',
                    totaltime: recording.durationhours ? recording.durationhours + ':' + recording.durationminutes : null,
                    uploadedfile: this.uploadedDocuments
                },
                recording
            );

            if (!this.recordingForm.value.documentpropertiesid) {
                delete this.addNotes.documentpropertiesid;
            }
            if (this.editRecord) {
                this.addNotes.notedetails = this.editRecord.notedetails;
            }
            this._commonHttpService.create(this.addNotes, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.AddRecordingUrl).subscribe(
                (result) => {
                    this.cancelRecording();
                    this.getPage(1);
                    if (this.isUploadClicked) {
                        this.isUploadClicked = false;
                        this._route.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment/attachment-upload']);
                    }
                    this._alertService.success('Contact details saved successfully!');
                    this._intakeUtils.notesUpdated$.next('UPDATED');
                },
                (error) => {
                    console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }

    viewRecording(recording) {
        this.viewRecord = recording;
        this.updateAppend.disable();
        this._commonHttpService.getPagedArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetRecordingDetailUrl + '/' + recording.progressnoteid)
            .map((result) => {
                result.data[0]['progressnoteroletypeview'] = result.data[0]['progressnoteroletype'].map((item) => item.contactroletypekey);
                this.recordingDetail =  result.data[0];
            });
        //
        this.calculatedContactDuration = this.calculateContactDurationForViewRecord(recording);
        this.calculatedTravelDuration = this.calculateTravelDurationForViewRecord(recording);
    }

    qualityRecording(recording) {
        this.viewRecord = recording;
        this.qualityForm.patchValue(this.viewRecord);
    }

    downloadFile(s3bucketpathname) {
        const workEnv = config.workEnvironment;
        if (workEnv === 'state') {
            this.downldSrcURL = this.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            // 4200
            this.downldSrcURL = s3bucketpathname;
        }
        console.log('this.downloadSrc', this.downldSrcURL);
        window.open(this.downldSrcURL, '_blank');
    }

    downloadContactsLogRreport(){
        console.log('id'+this.id);
        var req = {
            username: this.userRole.user.userprofile.fullname,
            usertitle: this.userRole.role.description,
            currentdate: this.currentDate,
            localdepartment: '',
            unit: '',
            name: '',
            danumber: this.daNumber,
            casetype: this.caseType,
            intakeserviceid: this.id,
            recording: this.recording
          };

        const payload = {
            method: 'post',
            count: -1,
            page: 1,
            limit: 20,
            where: req,
            documntkey: [
                    'contactslogreport'
                ]
          };

        this._commonHttpService.create(payload, 'admin/progressnote/getcontactslogreport').subscribe(
            response => {
              setTimeout(() => window.open(response.data.documentpath), 2000);
        });
    }

    editRecording(recording, viewEdit: string, typeOfEdit: string) {
        this.startEndTimeValidator = false;
        this.uploadedDocuments = [];
        this.uploadedDocuments = recording.uploadedfile && recording.uploadedfile.data ? recording.uploadedfile.data : [];
        this._dataStoreService.setData(CASE_STORE_CONSTANTS.PRO_NOTE_ID, recording.prognotetypeid);

        // @Simar - this variable is only used for VIEWING the record in readonly mode.
        // It is referred in the modal --> myModal-recordings-view-readonly
        // Not sure why it's called editRecord, but moving the view logic to viewRecording()
        this.editRecord = recording;
        this.sourceEdited = this.getSourceData(this.editRecord);
        this.isDate7DaysOld(recording.recordingdate);
        this.isDate24hoursOld(recording.recordingdate);

        // this.recordingDetail$ = this._commonHttpService
        //     .getPagedArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetRecordingDetailUrl + '/' + recording.progressnoteid)
        //     .map((result) => {
        //         result.data[0]['progressnoteroletypeview'] = result.data[0]['progressnoteroletype'].map((item) => item.contactroletypekey);
        //         return result.data[0];
        //     });

        this.viewEdit = viewEdit;
        // if (viewEdit === 'View') {
        //     this.updateAppend.disable();
        // } else {
        //     this.updateAppend.enable();
        // }
        this.updateAppend.enable();


        if (typeOfEdit === 'draftEdit') {
            const address = recording.contactparticipant && recording.contactparticipant.map((res) => {
                if (res.participanttypekey === 'Oth') {
                    return {
                        firstname: res.firstname,
                        lastname: res.lastname,
                        address1: res.address1,
                        address2: res.address2,
                        city: res.city,
                        state: res.state,
                        zipcode: res.zipcode,
                        email: res.email,
                        phonenumber: res.phonenumber
                    };
                }
            });
            const reasontypeValue = [];
            const param = Object.assign(
                {
                    //participanttypekey: recording.contactparticipant && recording.contactparticipant.length > 0 && recording.contactparticipant[0].participanttypekey,
                    // progressnotereasontypekey: [recording.progressnotereasontypekey],
                    progressnotereasontypekey: [],
                    description: recording.detail,
                    travelhours: recording.description ? recording.description.split(':')[0] : '',
                    travelminutes: recording.description ? recording.description.split(':')[1] : '',
                    // durationhours: recording.totaltime ? recording.totaltime.split(':')[0] : '',
                    // durationminutes: recording.totaltime ? recording.totaltime.split(':')[1] : ''
                    durationhours: recording.traveltime ? recording.traveltime.split(':')[0] : '',
                    durationminutes: recording.traveltime ? recording.traveltime.split(':')[1] : ''
                },
                recording,
                address && address.length > 0 && address[0] ? address[0] : ''
            );
            param.progressnotereasontypekey = recording.progressnotereasontypekey.split(',');
            this.recordingForm.patchValue(param);
            if (recording.progressnotetypeid) {
                this.recordingForm.patchValue({
                    progressnotetypeid: recording.progressnotetypeid + '~' + recording.recordingtype,
                    description: recording.progressnotepurposetypekey
                });
            }
            if (recording.progressnotecontacttrialvisit && recording.progressnotecontacttrialvisit.length) {
                this.courtForm.patchValue(recording.progressnotecontacttrialvisit[0]);
            }
            const serviceReqId = [];
            const list = recording.contactparticipant && recording.contactparticipant.map((res) => {
                serviceReqId.push(res.intakeservicerequestactorid);
                this.personRoleList(serviceReqId);
            });
            this.recordingForm.controls['intakeservicerequestactorid'].patchValue(serviceReqId);
            if (recording.progressnotesubtypeid) {
                this.recordingSubType(recording.progressnotesubtypeid);
            }

            if (recording.recordingtype === 'Court approved Trial Home Visit') {
                this.isCourtDetails = true;
            } else {
                this.isCourtDetails = false;
            }
        } else {
            this.recordingForm.reset();
        }
        // const start_date = this.datePipe.transform(recording.starttime, 'hh:mm');
        // const end_date = this.datePipe.transform(recording.endtime, 'hh:mm');
        // this.recordingForm.controls['starttime'].patchValue(start_date);
        // this.recordingForm.controls['endtime'].patchValue(end_date);

        // Patch the start and end time
        /**
         * Removing 'Z' at end of the Date Object to produce Start and End time as per the response
         */
        if (recording.starttime || recording.endtime) {
            //Note:- DO NOT CHANGE THIS START TIME /END TIME CODE WITHOUT PROPER APPROVAL
            //D-16611(DM)/D-16612(APP)-Start Time and End Time issue

             //1) Timezone has been defaulted to 'EST' at server level
             //2) Changing start/end time here , will force the browser to default to UTC (4 hrs ahead of EST)
            this.recordingForm.patchValue({
                starttime: moment(recording.starttime).format('HH:mm:ss'),
                endtime: moment(recording.endtime).format('HH:mm:ss'),
            },
                { emitEvent: false }
            );
        }

        this.computeContactDuration();
        // this.timeDuration();

        if (recording.attemptind !== null) {
            this.recordingForm.patchValue({
                attemptindicator: recording.attemptind === true ? 'yes' : 'no'
            });
            this.attempText = false;
        } else {
            this.attempText = true;
        }
        if (recording.initiateind !== null) {
            this.recordingForm.patchValue({
                initiationindicator: recording.initiationindicator === true ? 'yes' : 'no'
            });
            this.initiateText = false;
        } else {
            this.initiateText = true;
        }
        this.setInitialFaceToFace(recording);
    }
    changeMinit(model, name) {
        const minit = Number(model);
        if (minit && minit > 59) {
            this._alertService.warn('Please enter valid time');
            if (name === 'duration') {
                this.recordingForm.controls['durationminutes'].reset();
                this.setEndData();
            } else if (name === 'travel') {
                this.recordingForm.controls['travelminutes'].reset();
            }
        } else {
            if (name === 'duration') {
                this.setEndData();
            }
        }
    }
    getStartTime() {
        //console.log('---> '+this.recordingForm.getRawValue().endtime);
        const val = this.recordingForm.getRawValue().starttime;
        if (val === '' || val === null) {
            this.recordingForm.controls['durationhours'].reset();
            this.recordingForm.controls['durationminutes'].reset();
        }
        const progressNoteTypeKey = this.recordingForm.get('progressnotetypeid').value.split('~');
        if( !(progressNoteTypeKey[1] === 'Initialfacetoface' || progressNoteTypeKey[1] === 'Face To Face'
            || progressNoteTypeKey[1] === 'Weekly Visits' || progressNoteTypeKey[1] === 'Walk-in') )
            {
                this.recordingForm.controls['endtime'].setValue(val);
            }
        if (this.validateTimeFields()) {
            this.computeContactDuration();
        }
    }
    getEndTime() {
        //console.log('---> '+this.recordingForm.getRawValue().starttime);
        const val = this.recordingForm.getRawValue().endtime;
        if (val === '' || val === null) {
            this.recordingForm.controls['durationhours'].reset();
            this.recordingForm.controls['durationminutes'].reset();
        }
        if (this.validateTimeFields()) {
            this.computeContactDuration();
        }
    }
    setEndData() {
        const minit = this.recordingForm.controls['durationminutes'].value;
        const hour = this.recordingForm.controls['durationhours'].value;
        const calminits = (hour * 60) + Number(minit);
        const start_date = moment(this.recordingForm.value.starttime, 'HH:mm:ss');
        if (minit && hour && start_date) {
            const enddate = start_date.add(moment.duration(calminits, 'minutes'));
            this.recordingForm.controls['endtime'].setValue(enddate);
        }
    }

    // @Simar: This code is not used anymore, as the saveRecording is for both adding new / editing existing contact notes
    // updateRecording(recording, updateType: string) {
    //     if (recording.attemptindicator !== null) {
    //         recording.attemptindicator = recording.attemptindicator === 'yes' ? true : false;
    //     }
    //     if (recording.initiationindicator !== null) {
    //         recording.initiationindicator = recording.initiationindicator === 'yes' ? true : false;
    //     }
    //     if (updateType === 'darft') {
    //         const progressNoteTypeKey = this.recordingForm.get('progressnotetypeid').value.split('~');
    //         recording.progressnotetypeid = progressNoteTypeKey[0];
    //     } else {
    //         this.recordingForm.get('progressnotetypeid').reset();
    //     }
    //     if (recording.contactdate) {
    //         recording.contactdate = moment(recording.contactdate)
    //             .utc()
    //             .format();
    //     }
    //     const data = Object.assign({
    //         intakeservicerequestactorid: null,
    //         participanttypekey: this.recordingForm.value.participanttypekey,
    //         firstname: this.recordingForm.value.firstname ? this.recordingForm.value.firstname : '',
    //         lastname: this.recordingForm.value.lastname ? this.recordingForm.value.lastname : '',
    //         address1: this.recordingForm.value.address1 ? this.recordingForm.value.address1 : '',
    //         address2: this.recordingForm.value.address2 ? this.recordingForm.value.address2 : '',
    //         city: this.recordingForm.value.city ? this.recordingForm.value.city : '',
    //         state: this.recordingForm.value.state ? this.recordingForm.value.state : '',
    //         zipcode: this.recordingForm.value.zipcode ? this.recordingForm.value.zipcode : '',
    //         email: this.recordingForm.value.email ? this.recordingForm.value.email : '',
    //         phonenumber: this.recordingForm.value.phonenumber ? this.recordingForm.value.phonenumber : ''
    //     });
    //     const otherPerson = [];
    //     otherPerson.push(data);
    //     this.recordingedit = Object.assign(
    //         {
    //             contactparticipant: this.recordingForm.value.participanttypekey === 'IP' ? this.progressNoteRoleType : otherPerson,
    //             entitytypeid: this.id,
    //             savemode: this.editRecord.draft ? 1 : 0,
    //             contacttrialvisit: this.courtForm.value,
    //             entitytype: 'intakeservicerequest',
    //             stafftype: 1,
    //             instantresults: 1,
    //             contactstatus: false,
    //             drugscreen: false,
    //             description: recording.appendtitle,
    //             traveltime: recording.travelhours ? recording.travelhours + ':' + recording.travelminutes : '',
    //             totaltime: recording.durationhours ? recording.durationhours + ':' + recording.durationminutes : null
    //         },
    //         recording
    //     );
    //     this._commonHttpService.patch(this.editRecord.progressnoteid, this.recordingedit, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.UpdateRecordingUrl).subscribe(
    //         (result) => {
    //             this.cancelRecording();
    //             this.getPage(1);
    //             this._alertService.success('Contact details updated successfully!');
    //             (<any>$('#myModal-recordings-edit')).modal('hide');
    //             this._speechRecognitionService.destroySpeechObject();
    //         },
    //         (error) => {
    //             console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
    //         }
    //     );
    // }

    cancelRecording() {
        this.editRecord = null;
        this.sourceEdited = null;
        this.isDate24Check = true;
        this.uploadedDocuments = [];
        this.formInvolvedPersonsDropDown();
        (<any>$('#myModal-recordings')).modal('hide');
        (<any>$('#myModal-recordings-edit')).modal('hide');
        (<any>$('#upload-attachment')).modal('hide');
        this._speechRecognitionService.destroySpeechObject();
        this.recordingForm.reset({}, { emitEvent: false });
        this.recordingForm.controls['starttime'].disable({ emitEvent: false });
        this.recordingForm.controls['endtime'].disable({ emitEvent: false });
        this.courtForm.reset();
        this.progressNoteActor = [];
        this.updateAppend.reset();
        this.duration = '';
        this.isSerachResultFound = false;
        this.isCourtDetails = false;
        this.viewEdit = 'Add';
        //this.showparticipanttypekey = false;
        this.recordingForm.controls['attemptindicator'].clearValidators();
        this.recordingForm.controls['attemptindicator'].updateValueAndValidity();
    }

    private getContactLocationsDropDown(){
        // The API expects an id that is not used for filtering at all (use Face-to-face 786495b2-c779-4cc4-b812-6a8439bfa96e if needed)
        // So passng an empty value for prognotetypeid to get locations as in reality lacation values always seem the same list
        this._commonHttpService.getArrayList({},
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.ListProgressSubTypeUrl + '?prognotetypeid=' + ''
        ).subscribe(response => {
            if (response && Array.isArray(response) && response.length) {
                this.recordingSubTypeDropDown = response;
            }
        });
    }

    private recordingDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.ListProgressTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                }),
                'Contactroletypes' + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { objecttypekey: 'CPT' },
                    order: 'displayorder',
                    nolimit: true,
                    method: 'get'
                }, 'Participanttypes' + '?filter'
            )/*,
                // The API expects an id that is not used for filtering at all (use Face-to-face 786495b2-c779-4cc4-b812-6a8439bfa96e if needed)
                // So passng an empty value for prognotetypeid to get locations as in reality lacation values always seem the same list
                this._commonHttpService.getArrayList(
                    {},
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.ListProgressSubTypeUrl + '?prognotetypeid=' + ''
                )*/
        ])
            .map((result) => {
                //console.log("RESULT for recordingdropdown",result);
                return {
                    recordingType: result[0],
                    contactRoles: result[1],
                    participantType: result[2] //,
                    //contactLocations: result[3]
                };
            })
            .share();
        this.recordingType$ = source.pluck('recordingType');
        this.contactRoles$ = source.pluck('contactRoles');
        this.participantType$ = source.pluck('participantType');
        // Not sure why this was named recordingsubtype, when its actually just contact locations list
        //this.recordingSubTypeDropDown$ = source.pluck('contactLocations');

        this.recordingType$.subscribe((res) => {
            this.recordType = res;
        });
        //this.recordingSubTypeDropDown$.subscribe((res) => {            this.recordingSubTypeDropDown = res;        });

    }
    private attachmentDropdown() {
        const inputreq = {
            objectid: this.isServiceCase ? null : this.id,
            servicecaseid: this.isServiceCase ? this.id : null,
            objecttypekey: this.isServiceCase ? 'Servicecase' : 'ServiceRequest',
            page: 1,
            limit: 10
        };
        this.attachmentGrid$ = this._commonHttpService.getArrayList(
            new PaginationRequest({
                nolimit: true,
                method: 'get',
                where: inputreq,
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '?filter'
        );
    }
    checkInitialFaceToFace() {
        let isInitialFTF: RecordingNotes[] = [];
        {
            const result = this.recording;
            isInitialFTF = result.filter((res) => res.recordingtype === 'Initialfacetoface');
            this.recordingType$.subscribe((res) => {
                const progressnote = res.filter((item) => item.progressnotetypekey === 'Initialfacetoface');
                if (!isInitialFTF.length && progressnote.length && this.userInfo.user.userprofile.teamtypekey === 'CW') {
                    this.initialFace = true;
                    this.recordingForm.get('progressnotesubtypeid').enable();
                    this.isCourtDetails = false;
                    this.recordingForm.patchValue({
                        progressnotetypeid: progressnote[0].progressnotetypeid + '~' + progressnote[0].progressnotetypekey
                    });
                    this._alertService.error('Initial Face to Face is Mandatory');
                }
            });
        }
    }
    checkParticipantsRole(saveType, recording) {
        const progressNoteTypeKey = this.recordingForm.get('progressnotetypeid').value.split('~');

        if (progressNoteTypeKey[1] === 'Initialfacetoface' && saveType === 'SAVE' && recording.attemptindicator && recording.attemptindicator === 'no') {
            const avPersonRoles = [];
            let intakeSRActorId: any = [];
            const formAVPersonRoles = [];
            this.personRoles.map((result) => {
                // Removed RC (Reported child) in the validation.
                if (result.role) {
                    const avrole = result.role.filter((res) => res.intakeservicerequestpersontypekey === 'AV' || res.intakeservicerequestpersontypekey === 'CHILD');
                    if (avrole.length) {
                        avPersonRoles.push(avrole[0]);
                    }
                }
               
            });
            intakeSRActorId = this.recordingForm.get('intakeservicerequestactorid').value;
            if (intakeSRActorId && intakeSRActorId.length) {
                intakeSRActorId.map((person) => {
                    avPersonRoles.map((avperson) => {
                        if (avperson.intakeservicerequestactorid === person) {
                            formAVPersonRoles.push(person);
                        }
                    });
                });
            }
            if (formAVPersonRoles.length) {
                return true;
            } else {
                this._alertService.error('Please select atleast one alleged victim or child.');
                return false;
            }
        } else {
            return true;
        }
    }

    ngOnDestroy() {
        this._speechRecognitionService.destroySpeechObject();
    }

    activateSpeechToText(): void {
        this.recognizing = true;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
            this._speechRecognitionService.record().subscribe(
                // listener
                (value) => {
                    this.speechData = value;
                    if (this.viewEdit === 'Edit') {
                        this.enableAppend = true;
                        this.updateAppend.patchValue({ appendtitle: this.speechData });
                    } else {
                        this.recordingForm.patchValue({ description: this.speechData });
                    }
                },
                // errror
                (err) => {
                    console.log(err);
                    this.recognizing = false;
                    if (err.error === 'no-speech') {
                        this.notification = `No speech has been detected. Please try again.`;
                        this._alertService.warn(this.notification);
                        this.activateSpeechToText();
                    } else if (err.error === 'not-allowed') {
                        this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    } else if (err.error === 'not-microphone') {
                        this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    }
                },
                // completion
                () => {
                    this.speechRecogninitionOn = true;
                    console.log('--complete--');
                    this.activateSpeechToText();
                }
            );
        } else {
            this.recognizing = false;
            this.deActivateSpeechRecognition();
        }
    }
    deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
    }

    async downloadPdf() {
        
        // const pages = document.getElementsByClassName('page-pdf-down');
        // let pageImages = [];
        // for (let i = 0; i < pages.length; i++) {
        //     const pageName = pages.item(i).getAttribute('data-page-name');
        //     const isPageEnd = pages.item(i).getAttribute('data-page-end');
        //     await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
        //         const img = canvas.toDataURL('image/png');
        //         pageImages.push(img);
        //         if (isPageEnd === 'true') {
        //             this.pdfFiles.push({ fileName: pageName, images: pageImages });
        //             pageImages = [];
        //         }
        //     });
        // }
        //this.convertImageToPdf();
    }

    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            // const doc = new jsPDF('landscape');
            let doc = null;
            doc = new jsPDF();


            const width = doc.internal.pageSize.getWidth() - 10;
            const heigth = doc.internal.pageSize.getHeight() - 10;
            pdfFile.images.forEach((image, index) => {

                doc.addImage(image, 'JPEG', 3, 5, width, heigth);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        this.pdfFiles = [];

    }


    startDateChanged(investigationForm) {
        const empForm = investigationForm.getRawValue();
        this.minDate = new Date(empForm.contactdatefrom);
    }

    endDateChanged(investigationForm) {
        const empForm = investigationForm.getRawValue();
        this.maxDate = new Date(empForm.contactdateto);
    }

    redirectToUpload() {
        this.isUploadClicked = true;
        this.saveRecording(this.recordingForm.value, 'SAVE');
    }
    getContactPurpose(purpose) {
        let result = '';
        if (Array.isArray(purpose)) {
            purpose.forEach((element, index) => {
                const reason = this.reasonForContact.find(item => element === item.progressnotereasontypekey);
                if (reason) {
                    result = (index < (purpose.length) && index > 0) ? (result + ',' + reason.typedescription) : (result + reason.typedescription);
                }
            });
            return result;
        } else if (purpose && purpose.indexOf(',') !== -1) {
            let list = purpose.indexOf(',') !== -1 ? purpose.split(',') : purpose;
            list = (Array.isArray(list)) ? list : [];
            list.forEach((element, index) => {
                const reason = this.reasonForContact.find(item => element === item.progressnotereasontypekey);
                if (reason) {
                    result = (index < (list.length) && index > 0) ? (result + ',' + reason.typedescription) : (result + reason.typedescription);
                }
            });
            return result;
        } else {
            const reason = this.reasonForContact.find(item => purpose === item.progressnotereasontypekey);
            if (reason) {
                result = result + reason.typedescription;
            }
            return result;
        }
    }
    sendEmail() {
        const caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        if (this.emailForm.valid) {
            const request = {
                email: this.emailForm.getRawValue().email,
                caseNumber: caseID,
                objectid: this.id,
                objecttype: this.getEntityType(),
                body: document.getElementById('contactEmailData').innerHTML
            };
            this._commonHttpService.create(request, 'admin/progressnote/sendemailcontact').subscribe(
                (result) => {
                    this._alertService.success('Email Sent successfully!');
                    this.emailForm.reset();
                    (<any>$('#iframe-Send-Email')).modal('hide');

                },
                (error) => {
                    console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._alertService.error('Please enter valid email address!');
        }
    }
    getCasePlanDataByLogType(referenceId, logType) {
        return this._commonHttpService.getArrayList(
            {
                where: {
                    activeflag: '1',
                    referenceid: referenceId,
                    logType: logType
                },
                method: 'get',
                nolimit: true
            },
            'auditlog/list?filter');
    }

    getHist(prognotetypeid) {
        this.histData = [];
        this.getCasePlanDataByLogType(prognotetypeid, 'PNOTE')
            .subscribe(
                (response) => {
                    //console.log("HISTORY", response);
                    if (response.length > 0) {
                        for (let i = 0; i < response.length; i++) {
                            response[i].metadata.displayname = response[i].displayname;
                            response[i].metadata.insertedon = response[i].insertedon;
                            response[i].metadata.contactType = this.getContactType(response[i].metadata.progressnotetypeid);
                            response[i].metadata.purposeString = this.getContactPurpose(response[i].metadata.progressnotereasontypekey);
                            this.histData.push(response[i].metadata);
                        }
                    }
                }
            );
    }

    getContactType(progressnotetypeid) {
        let result;
        if (this.recordType) {
            const progressnote = this.recordType.filter((item) => item.progressnotetypeid === progressnotetypeid);
            if (progressnote && progressnote.length) {
                result = progressnote[0].description;
            }
        }
        return result;
    }
    getLocationType(locationname) {
        let result;
        /*
        if (this.recordingSubTypeDropDown$) {
            this.recordingSubTypeDropDown$.subscribe((res) => {
                const progressnote = res.filter((item) => item.progressnotesubtypeid === locationname);
                if (progressnote && progressnote.length) {
                    result = progressnote[0].description;
                }
            }); */
        if (this.recordingSubTypeDropDown) {
                const progressnote = this.recordingSubTypeDropDown.filter((item) => item.progressnotesubtypeid === locationname);
                if (progressnote && progressnote.length) {
                    result = progressnote[0].description;
                }
        }
        return result;
    }
    getPersonName(intakeservicerequestactorid: string) {
        const person = this.personRoles.find(p => p.intakeservicerequestactorid === intakeservicerequestactorid);
        if (person) {
            return person.personname;
        }
    }

    getDataDiffdays(recordingdate, days) {

        const now = moment(new Date()); // todays date
        const end = this.addWeekdays(recordingdate, days); // adding 5 days to recording date
        if (moment(recordingdate).isSameOrBefore(now) && moment(end).isAfter(now, 'hour')) {
            return true;
        } else {
            return false;
        }

    }

    get24HoursDiff(recordingdate) {
        return this.getDataDiffdays(recordingdate, 1);
    }

    getDataDiff5days(recordingdate) {
        return this.getDataDiffdays(recordingdate, 7);
    }


    isDate24hoursOld(recordingdate) {
        setTimeout(() => {
            this.isDate24Check = this.get24HoursDiff(recordingdate);
        });
    }



    isDate7DaysOld(recordingdate) {
        setTimeout(() => {
            this.isDateCheck = this.getDataDiff5days(recordingdate);
        });
    }

    addWeekdays(date, days) {
        date = moment(date); // clone
        while (days > 0) {
            date = date.add(1, 'days');
            // decrease "days" only if it's a weekday.
            if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
                days -= 1;
            }
        }
        return date;
    }

    getRequestParam() {
        let inputRequest;
        const caseID = this.getCaseUuid();
        console.log('store', this._dataStoreService.getCurrentStore());
        this.source = this.getCurrentCaseType();
        this.caseType = this.getCurrentCaseType();
        if (this.isServiceCaseData()) {
            inputRequest = {
                objectid: caseID,
                objecttypekey: 'servicecase'
            };
        } else if (this.isIntakeMode()) {
            inputRequest = {
                intakenumber: this.getIntakeNumber()
            };

        } else if (this.isAdoptionCase()){
            inputRequest = {
                intakeserviceid: caseID
            };
        }
         else {
            inputRequest = {
                intakeserviceid: caseID
            };
        }

        return inputRequest;
    }


    getCaseNumber(){
        const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');

        return caseInfo.da_number;

        

    }
    getCaseUuid() {
        const caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
        if(this.isIntakeMode())
            return this.getIntakeNumber
        let caseUUID = null;
        if (caseInfo) {
            caseUUID = caseInfo.intakeserviceid;
            this.daNumber = caseInfo.da_number;
        }
        if (caseID) {
            return caseID;
        }
        return caseUUID;
    }
    isAdoptionCase() {
        // console.log("CHECKING ADOPTION")
        if( this._session.getItem(CASE_STORE_CONSTANTS.CASE_TYPE) == AppConstants.CASE_TYPE.ADOPTION_CASE)
            return true;
        const dsds = this._dataStoreService.getData('dsdsActionsSummary');
        if(dsds && dsds.adoptioncasenumber!= null)
            return true;
        // console.log("NOT ADOPTION");
        return false;
    }
    isServiceCaseData() {
        return this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    }

    isIntakeMode() {
        return this.getIntakeNumber() ? true : false;
    }

    getIntakeNumber() {
        const intakeStore = this._session.getObj('intake');
        if (intakeStore && intakeStore.number) {
            return intakeStore.number;
        } else {
            return null;
        }
    }
    getCurrentCaseType() {
        if (this.isServiceCaseData()) {
            return "Service Case";
        } else if (this.isIntakeMode()) {
            return "Intake";
        } else if (this.isAdoptionCase()){
            return "Adoption Case";
        } else {
            return "CPS";
        }
    }

    getEntityType(){
        let entitytype = '';
        if (this.isIntakeMode()) {
            entitytype = 'intake';
        } else if (this.isServiceCaseData()) {
            entitytype = 'servicecase';
        } else if (this.isAdoptionCase()) {
            entitytype = 'adoption';
        } else {
            entitytype = 'intakeservicerequest';
        }
        return entitytype;
    }

    /**
     * Contact date time related stuff
     * @Simar: Redoing entirely how contact data and time works
     */
    validateTimeFields(): boolean {
        const recordingForm = this.recordingForm.getRawValue();
        if (!recordingForm.contactdate) {
            this.timeValidationAlert('Please enter contact date.');
            return false;
        } else if (this.startEndTimeValidator && !recordingForm.starttime) {
            this.timeValidationAlert('Please enter start time.');
            return false;
        } else if (this.startEndTimeValidator && !recordingForm.endtime &&
            this.recordingForm.controls['endtime'].dirty) {
            // need this dirty check on end date so that don't trigger alert messages even before user has had chance to enter value
            this.timeValidationAlert('Please enter end time.');
            return false;
        }
        return true;
    }

    timeValidationAlert(message: string) {
        this._alertService.error(message);
    }

    setupTimeChangeSubscribers() {
        this.recordingForm.controls['endtime'].valueChanges
            .subscribe(form => {
                if (this.validateTimeFields()) {
                    this.computeContactDuration();
                }
            });
        this.recordingForm.controls['starttime'].valueChanges
            .subscribe(form => {
                if (this.validateTimeFields()) {
                    this.computeContactDuration();
                }
            });
    }

    computeContactDuration() {
        const startTime = moment(moment(this.recordingForm.getRawValue().contactdate).format('MM/DD/YYYY') + ' ' + this.recordingForm.getRawValue().starttime);
        const endTime = moment(moment(this.recordingForm.getRawValue().contactdate).format('MM/DD/YYYY') + ' ' + this.recordingForm.getRawValue().endtime);
        const now = new Date();
        if (startTime.isAfter(now)) {
            this.timeValidationAlert('Future time is not allowed!');
            this.dateValidation = true;
            return;
        }
        if (endTime.isAfter(now)) {
            this.timeValidationAlert('Future time is not allowed!');
            this.dateValidation = true;
            return;

        }
        if (startTime.isAfter(endTime)) {
            this.timeValidationAlert('Start time must be before end time!');
            this.dateValidation = true;
            return;
        } else {
            const computedDuration = moment.duration(endTime.diff(startTime));

            if (computedDuration['_data'] && this.recordingForm.getRawValue().starttime && this.recordingForm.getRawValue().endtime) {
                this.recordingForm.controls['durationhours'].patchValue((computedDuration['_data'].hours));
                this.recordingForm.controls['durationminutes'].patchValue((computedDuration['_data'].minutes));
            }
            this.dateValidation = false;
        }
        this.dateValidation = false;
    }

    calculateContactDurationForViewRecord(recording) {
        const _startTime = moment(moment(recording.contactdate).format('MM/DD/YYYY') + ' ' + moment(recording.starttime).format('HH:mm:ss'));
        const _endTime = moment(moment(recording.contactdate).format('MM/DD/YYYY') + ' ' + moment(recording.endtime).format('HH:mm:ss'));

        const computedDuration = moment.duration(_endTime.diff(_startTime));
        if (computedDuration['_data']) {
            return computedDuration['_data'].hours + ' Hr(s) ' + computedDuration['_data'].minutes + ' Min(s)';
        }
    }

    calculateTravelDurationForViewRecord(recording) {
        // @Simar: I honestly don't understand why the Travel Time is returned in field called 'descripton'
        // But for now extracting the hours and mins from the string description field
        const travelhours = recording.description ? recording.description.split(':')[0] : '0';
        const travelminutes = recording.description ? recording.description.split(':')[1] : '0';
        return travelhours + ' Hr(s) ' + travelminutes + ' Mins(s)';
    }

    convertMatinputTimeToTimestamp(date, time) {
        return moment(moment(date).format('MM/DD/YYYY') + ' ' + time).format();
    }

    setupTimePayload(recording) {
        // const trainingDetails = this.trainingForm.getRawValue();
        const startTime = moment(recording.contactdate).format('MM/DD/YYYY') + ' ' + recording.starttime;
        const endTime = moment(recording.contactdate).format('MM/DD/YYYY') + ' ' + recording.starttime;
        recording.starttime = moment(startTime).format();
        recording.endtime = moment(endTime).format();
        return recording;
    }

    changeContactDate(value) {
        console.log(value)
        this.checkChangeDateValue(value);
    }

    checkChangeDateValue(value) {
        if (value) {
            this.recordingForm.controls['starttime'].enable();
            this.recordingForm.controls['endtime'].enable();
        } else {
            this.recordingForm.controls['starttime'].setValue('');
            this.recordingForm.controls['endtime'].setValue('');
            this.recordingForm.controls['starttime'].disable();
            this.recordingForm.controls['endtime'].disable();
        }
    }


    
    getSourceData(editRecord) {
        if(editRecord && editRecord.entitytype){
          var data = editRecord.entitytypeid.substring(2,editRecord.entitytypeid.length);
          if(editRecord.entitytype.toLowerCase()==='intake' ){
              return "Intake"
          } else if(editRecord.entitytype.toLowerCase()==='intakeservicerequest' ){
            return "CPS"
          } else if(editRecord.entitytype.toLowerCase()==='servicecase' ){
            return "Service Case"
          } else if(editRecord.entitytype.toLowerCase()==='adoption' ){
            return "Adoption"
          }
        }
    }

    getLocation(record) {
        if (record) {
            const recordData = Object.assign({}, record);
            if (recordData.recordingsubtype === 'Other') {
                return recordData.locationname;
            } else {
                return recordData.recordingsubtype;
            }
        }
        return '';
    }

    downloadContactPDF() {
    const modal = {
      count: -1,
      where: {
        documenttemplatekey: ["contactpdf"],
        entitytype: this.entityType,
        caseNumber: this.daNumber,
        entitytypeid: this.id
      },
      method: 'post'
    };
    console.log('download', modal);
    this._commonHttpService.getSingle(modal, 'evaluationdocument/generateintakedocument').subscribe((data) => {
        if (data) {
            window.open(data.data[0].documentpath, '_blank');
        }
    });
    // if (type === 'pdf') {
     /*  this._commonHttpService.download('evaluationdocument/generateintakedocument', modal)
        .subscribe(res => {
            if (res) {
                window.open(res.data[0].documentpath, '_blank');
            }
        }); */
    }
}
