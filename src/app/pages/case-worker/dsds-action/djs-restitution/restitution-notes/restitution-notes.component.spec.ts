import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestitutionNotesComponent } from './restitution-notes.component';

describe('RestitutionNotesComponent', () => {
  let component: RestitutionNotesComponent;
  let fixture: ComponentFixture<RestitutionNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestitutionNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestitutionNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
