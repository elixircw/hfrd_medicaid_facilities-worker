import { Component, OnInit } from '@angular/core';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../@core/services';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ActivatedRoute } from '@angular/router';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
  selector: 'restitution-notes',
  templateUrl: './restitution-notes.component.html',
  styleUrls: ['./restitution-notes.component.scss']
})
export class RestitutionNotesComponent implements OnInit {

  noteslist = [];
  restitutionlist = [];
  totalCount: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  restitutionno: string;
  notes: string;
  id: any;
  constructor(private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _route: ActivatedRoute,
    private _dataStoreService: DataStoreService) {
      this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
  }

  ngOnInit() {
    this.paginationInfo.pageNumber = 1;
    this.loadDropdown();
    this.loadList();
  }

  loadDropdown() {
    this._commonHttpService.getPagedArrayList({
      count: -1,
      page: 1,
      limit: 20,
      nolimit: true,
      where: {
        intakeserviceid: this.id,
        intakeserreqrestitutionid: null
      },
      method: 'get'
    }, CommonUrlConfig.EndPoint.RESTITUTION.LIST_RESTITUTION).subscribe((res: any) => {
      this.restitutionlist = res;
    });
  }

  loadList() {
    this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        where: { intakeserviceid : this.id },
        method: 'get'
      }),
      CommonUrlConfig.EndPoint.RESTITUTION.CCU.NOTES_LIST + '?data'
    ).subscribe((result: any) => {
      if (result && result.data) {
        this.noteslist = result.data;
        if (this.paginationInfo.pageNumber === 1) {
          this.totalCount = result.count;
        }
      }

    });
  }

  createNotes() {
    (<any>$('#add-notes')).modal('show');
  }

  savenotes() {
    const requestObj = {
      restitutionno: this.restitutionno,
      ccunotes: this.notes
    };
    this._commonHttpService.create(requestObj,
      CommonUrlConfig.EndPoint.RESTITUTION.CCU.CW_NOTES_ADD).subscribe(success => {
        this.restitutionno = null;
        this.notes = null;
        (<any>$('#add-notes')).modal('hide');
        this._alertService.success('Notes Submitted successfully.');
        this.loadList();
      }, error => {
        (<any>$('#add-notes')).modal('hide');
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      });
  }

}
