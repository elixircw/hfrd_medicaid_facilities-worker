import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DjsRestitutionComponent } from './djs-restitution.component';
import { AppConstants } from '../../../../@core/common/constants';
import { RestitutionNotesComponent } from './restitution-notes/restitution-notes.component';

const routes: Routes = [
  {
    path: '',
    component: DjsRestitutionComponent,
    children: [
      {
        path: 'payment-schedule',
        loadChildren: 'app/pages/shared-pages/payment-schedule/payment-schedule.module#PaymentScheduleModule',
        data: {
          pageSource: AppConstants.PAGES.CASE_PAGE
        }
      },
      {
        path: 'notes',
        component: RestitutionNotesComponent
      }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DjsRestitutionRoutingModule { }
