import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'djs-restitution',
  templateUrl: './djs-restitution.component.html',
  styleUrls: ['./djs-restitution.component.scss']
})
export class DjsRestitutionComponent implements OnInit {
  tabs = [
    {
      id: 'payment-schedule',
      title: 'Payment Schedule',
      name: 'payment-schedule',
      route: 'payment-schedule'
    } ,
    {
      id: 'notes',
      title: 'Notes',
      name: 'notes',
      route: 'notes'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
