import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DjsRestitutionRoutingModule } from './djs-restitution-routing.module';
import { DjsRestitutionComponent } from './djs-restitution.component';
import { RestitutionNotesComponent } from './restitution-notes/restitution-notes.component';
import { MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    DjsRestitutionRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    QuillModule,
    PaginationModule,
    FormMaterialModule
  ],
  declarations: [DjsRestitutionComponent, RestitutionNotesComponent]
})
export class DjsRestitutionModule { }
