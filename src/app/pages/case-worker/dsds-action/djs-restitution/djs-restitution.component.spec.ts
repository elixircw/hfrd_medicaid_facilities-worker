import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsRestitutionComponent } from './djs-restitution.component';

describe('DjsRestitutionComponent', () => {
  let component: DjsRestitutionComponent;
  let fixture: ComponentFixture<DjsRestitutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsRestitutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsRestitutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
