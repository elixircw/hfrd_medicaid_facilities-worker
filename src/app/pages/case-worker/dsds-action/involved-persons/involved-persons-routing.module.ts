import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InvolvedPersonsComponent } from './involved-persons.component';

const routes: Routes = [
    {
        path: '',
        component: InvolvedPersonsComponent,
        children: [
            {
                path: 'disability',
                loadChildren: '../../../shared-pages/person-disability/person-disability.module#PersonDisabilityModule'
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InvolvedPersonsRoutingModule { }
