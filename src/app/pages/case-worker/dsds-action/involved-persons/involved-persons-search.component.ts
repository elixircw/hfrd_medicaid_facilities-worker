import { AfterViewChecked, ChangeDetectorRef, Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { Observable, Subject } from '../../../../../../node_modules/rxjs';
import { ControlUtils } from '../../../../@core/common/control-utils';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { ValidationService } from '../../../../@core/services/validation.service';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { PersonRole, Persons, Health, ContactPerson, PersonBasicDetail, PersonSupport, PersonPayType } from './_entities/involvedperson.data.model';
import { AddPerson, InvolvedPerson, InvolvedPersonSearchResponse, People, PersonData, PersonSearch } from './_entities/involvedperson.data.model';
import { AuthService } from '../../../../@core/services/auth.service';
import { DataStoreService } from '../../../../@core/services';
import { DSDSConstants } from '../dsds-action.constants';
declare var $: any;
import * as moment from 'moment';
import { AppConstants } from '../../../../@core/common/constants';
import { SessionStorageService } from '../../../../@core/services/storage.service';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'involved-persons-search',
    templateUrl: './involved-persons-search.component.html',
    styleUrls: ['./involved-persons-search.component.scss']
})
export class InvolvedPersonsSearchComponent implements OnInit, AfterViewChecked {
    @Output()
    newPersonAdd = new EventEmitter();
    @Output()
    getInvolvedPerson = new EventEmitter();
    @Output()
    close = new EventEmitter();
    @Input()
    healthFormReset$ = new Subject<boolean>();
    @Input() serachPersonProfileInfo$ = new Subject<boolean>();
    @Input() isEmergencyContact: boolean;
    @Input() emergencyContactPersonInfo$ = new Subject<ContactPerson>();
    @Input() showGuardianship$ = new Subject<boolean>();
    @Input() guardianSearchStoreId: string;
    guardianPerson: PersonBasicDetail[] = [];
    @Input() guardianPersonBtn: boolean;
    @Input() involvedPersonList: InvolvedPerson[];
    @Input() representativePerson: boolean;
    @Input() representativeDetails$ = new Subject<PersonPayType>();
    profileTabActive: boolean;
    serachResultTabActive: boolean;
    personRoleTabActive: boolean;
    showAddPerson: boolean;
    showSearchGrid: boolean;
    isAS: boolean;
    selectedPersonDetails: PersonData = new PersonData();
    id: string;
    involvedPersonSearchForm: FormGroup;
    personSearchAddForm: FormGroup;
    showPersonDetail = -1;
    selectedPersonRow: People = new People();
    addedPerson: People[] = [];
    selectedPerson: InvolvedPersonSearchResponse;
    paginationInfo: PaginationInfo = new PaginationInfo();
    involvedPerson: Persons = new Persons();
    personRole: PersonRole[] = [];
    maxDate = new Date();
    canDisplayPager$: Observable<boolean>;
    totalRecords$: Observable<number>;
    personSearchResult$: Observable<PersonSearch[]>;
    genderDropdownItems$: Observable<DropdownModel[]>;
    roleDropdownItems$: DropdownModel[];
    relationShipToRADropdownItems$: DropdownModel[];
    relationShipToRADropdownItems: DropdownModel[];
    stateDropdownItems$: Observable<DropdownModel[]>;
    isDjs = false;
    isDjsYouth: boolean;
    isSDR = false;
    isduplicate: boolean;
    private involvedPersonSearch: InvolvedPerson;
    selectedPersonsage: number;
    enableSafeHaven: boolean;
    da_typeid: any;
    da_type: any;
    isRCPage: boolean;
    dangerSelf: boolean;
    dangerWorker: boolean;
    dangerAppearance: boolean;
    dangerIll: boolean;
    validation_messages: any;
    countyDropdownItems$: Observable<DropdownModel[]>;
    agency: string;
    isServiceCase: string;
    formalSupportForm: FormGroup;
    isCW: boolean;
    isCheckedCollateral = true;
    constructor(
        private _formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute,
        private _getPersonDetail: CommonHttpService,
        private _involvedPersonSeachService: GenericService<InvolvedPersonSearchResponse>,
        private cdRef: ChangeDetectorRef,
        private _authService: AuthService,
        private storage: SessionStorageService,
        private _dataStoreService: DataStoreService
    ) {
        this.route.data.subscribe(response => {
            const pageSource = response.pageSource;
            console.log('persons-search ' + pageSource);
            this.isRCPage = pageSource === AppConstants.PAGES.RESTITUTION_COORDINATOR;
        });
    }

    ngOnInit() {
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        this.agency = this._authService.getAgencyName();
        this.isAS = this._authService.isAS();
        if (this.isRCPage) {
            this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        } else {
            this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        }
        this.da_typeid = this._dataStoreService.getData('da_typeid');
        this.da_type = this._dataStoreService.getData('da_type');
        this.loadDropDown();
        this.getCommonDropdowns();
        this.personSearchAddForm = this._formBuilder.group({
            Pid: [''],
            Lastname: [''],
            Firstname: [''],
            middlename: [''],
            Gender: [''],
            Dob: [''],
            ssn: [''],
            Role: [''],
            suffix: [''],
            aliasname: [''],
            address1: [''],
            Address2: [''],
            zipcode: [''],
            state: [''],
            city: [''],
            county: [''],
            RelationshiptoRA: [''],
            Dangerousself: [null, [Validators.required]],
            DangerousselfReason: [''],
            Dangerousworker: [null, [Validators.required]],
            DangerousWorkerReason: [''],
            ismentalimpair: [null, [Validators.required]],
            mentalimpairdetail: [''],
            ismentalillness: [null, [Validators.required]],
            mentalillnessdetail: [''],
            iscollateralcontact: [false],
            ishousehold: ['', [!this._authService.isDJS() ? Validators.required : Validators.pattern('')]],
            drugexposednewbornflag: [false],
            fetalalcoholspctrmdisordflag: [false],
            sexoffenderregisteredflag: [false],
            probationsearchconductedflag: [false],
            source: [''],
            safehavenbabyflag: [false]
            // issafehaven: [false]
        });
        this.personSearchAddForm.addControl('personRole', this._formBuilder.array([this.createFormGroup(true)]));
        this.validation_messages = {
            'lastname': [
                { type: 'pattern', message: 'Enter a valid Last Name' }
            ],
            'firstname': [
                { type: 'pattern', message: 'Enter a valid First Name' }
            ]
        };
        this.involvedPersonSearchForm = this._formBuilder.group({
            lastname: new FormControl('', Validators.compose([
                Validators.pattern('[a-zA-Z ]*$') // [a-zA-Z]+(\s+[a-zA-Z]+)*
            ])),
            firstname: new FormControl('', Validators.compose([
                Validators.pattern('^[a-zA-Z]*$')
            ])),
            maidenname: [''],
            gender: [''],
            dob: [''],
            dateofdeath: [''],
            ssn: [''],
            mediasrc: [''],
            mediasrctxt: [''],
            occupation: [''],
            dl: [''],
            stateid: [''],
            address1: [''],
            address2: [''],
            zip: [''],
            city: [''],
            county: [''],
            selectedPerson: [''],
            cjisnumber: [''],
            complaintnumber: [''],
            fein: [''],
            age: [''],
            email: ['', [ValidationService.mailFormat]],
            phone: [''],
            petitionid: [''],
            alias: [''],
            oldId: ['']
        });
        this.formalSupportForm = this._formBuilder.group({
            personsupporttypekey: [''],
            description: ['']
        });
        this.isDjs = this._authService.isDJS();
        this.isCW = this._authService.isCW();
        if (this.isDjs) {
            this.isDjsYouth = false;
        } else {
            this.isDjsYouth = true;
        }
    }
    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }
    searchPersonDetails(id: number) {
        if (this.showPersonDetail !== id) {
            this.showPersonDetail = id;
        } else {
            this.showPersonDetail = -1;
        }
    }
    selectPerson(row) {
        this.selectedPersonRow = row;
        this.enableSafeHaven = false;
        this.selectedPersonsage = this.calculateAge(this.selectedPersonRow.dob);
        if (this.selectedPersonRow.source === 'SDR') {
            this.isSDR = true;
        } else {
            this.isSDR = false;
        }
        this.serachResultTabActive = true;
        this._getPersonDetail
            .getSingle(
                new PaginationRequest({
                    method: 'get',
                    where: {
                        intakeserviceid: this.id,
                        personid: this.selectedPersonRow.personid
                    }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
            .subscribe((result) => {
                this.selectedPersonDetails = result;
            });
    }
    formalSupprotChecked(formal) {
        if (formal) {
            this.formalSupportForm.controls['description'].setValidators([Validators.required]);
            this.formalSupportForm.controls['description'].updateValueAndValidity();
        } else {
            this.formalSupportForm.controls['description'].clearValidators();
            this.formalSupportForm.controls['description'].updateValueAndValidity();
            this.formalSupportForm.controls['description'].reset();
        }
    }
    addContactPerson() {
        if (this.selectedPersonDetails.personbasicdetails.personid) {
            if (this.guardianPersonBtn) {
                this.guardianPerson.push(this.selectedPersonDetails.personbasicdetails);
                this._dataStoreService.setData(this.guardianSearchStoreId, this.guardianPerson);
                this.showGuardianship$.next(true);
            } else {
                const person: ContactPerson = Object.assign({
                    contactpersonid: this.selectedPersonDetails.personbasicdetails.personid,
                    firstname: this.selectedPersonDetails.personbasicdetails.firstname,
                    lastname: this.selectedPersonDetails.personbasicdetails.lastname
                });
                this.emergencyContactPersonInfo$.next(person);
            }
            this.involvedPersonSearchForm.reset();
            this.tabNavigation('profileinfo');
            this.serachResultTabActive = false;
        } else {
            this._alertService.error('Please select person');
        }
    }
    addRepresentativePerson() {
        if (this.selectedPersonDetails.personbasicdetails) {
            const person = Object.assign({
                personid: this.selectedPersonDetails.personbasicdetails.personid,
                firstname: this.selectedPersonDetails.personbasicdetails.firstname,
                lastname: this.selectedPersonDetails.personbasicdetails.lastname
            });
            this.representativeDetails$.next(person);
            this.tabNavigation('searchresult');
            this.serachResultTabActive = false;
        } else {
            this._alertService.error('Please select person');
        }
    }
    searchInvolvedPersons(model: People) {
        this.showPersonDetail = -1;
        this.selectedPerson = Object.assign({}, new InvolvedPersonSearchResponse());
        // if ($('#dobirth').val().length > 4) {
        //     if (model.dob) {
        //         const event = new Date(model.dob);
        //         model.dob = event.getMonth() + 1 + '/' + event.getDate() + '/' + event.getFullYear();
        //     }
        // } else {
        //     model.dob = $('#dobirth').val();
        // }
        this.involvedPersonSearch = Object.assign(new InvolvedPerson(), model);
        if (this.involvedPersonSearchForm.value.address1) {
            this.involvedPersonSearch.address = this.involvedPersonSearchForm.value.address1 + '' + this.involvedPersonSearchForm.value.address2;
        }
        this.involvedPersonSearch.stateid = this.involvedPersonSearchForm.value.dl;
        this._dataStoreService.setData('SearchDataInvolvedPerson', model);
        this.getPage(1);
        this.profileTabActive = true;
        this.personRoleTabActive = false;
        this.tabNavigation('searchresult');
    }
    addSearchPerson() {
        this.isduplicate = false;
        this.verifyDuplicatePerson();
        if (this.isduplicate) {
            this._alertService.error('Duplicate Person');
        } else {
            if (this.selectedPersonRow && this.selectedPersonRow.lastname) {
                this.tabNavigation('reporterrole');
            } else {
                this._alertService.warn('Please select a person');
            }
        }
    }
    createFormGroup(isHideRA) {
        return this._formBuilder.group({
            rolekey: ['', Validators.required],
            isprimary: ['', Validators.required],
            relationshiptorakey: [''],
            hidden: [isHideRA]
        });
    }
    disabledInput(state: boolean, inputfield: string, manditory: string) {
        if (state === false && manditory === 'isManditory') {
            this.personSearchAddForm.get(inputfield).enable();
            if (inputfield === 'DangerousselfReason') {
                this.personSearchAddForm.get('Dangerousself').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === '1') {
                        this.dangerSelf = true;
                        this.personSearchAddForm.get('DangerousselfReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('DangerousselfReason').updateValueAndValidity();
                    } else {
                        this.dangerSelf = false;
                        this.personSearchAddForm.get('DangerousselfReason').clearValidators();
                        this.personSearchAddForm.get('DangerousselfReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'DangerousWorkerReason') {
                this.personSearchAddForm.get('Dangerousworker').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === '1') {
                        this.dangerWorker = true;
                        this.personSearchAddForm.get('DangerousWorkerReason').setValidators([Validators.required]);
                        this.personSearchAddForm.get('DangerousWorkerReason').updateValueAndValidity();
                    } else {
                        this.dangerWorker = false;
                        this.personSearchAddForm.get('DangerousWorkerReason').clearValidators();
                        this.personSearchAddForm.get('DangerousWorkerReason').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'mentalimpairdetail') {
                this.personSearchAddForm.get('ismentalimpair').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === '1') {
                        this.dangerAppearance = true;
                        this.personSearchAddForm.get('mentalimpairdetail').setValidators([Validators.required]);
                        this.personSearchAddForm.get('mentalimpairdetail').updateValueAndValidity();
                    } else {
                        this.dangerAppearance = true;
                        this.personSearchAddForm.get('mentalimpairdetail').clearValidators();
                        this.personSearchAddForm.get('mentalimpairdetail').updateValueAndValidity();
                    }
                });
            }
            if (inputfield === 'mentalillnessdetail') {
                this.personSearchAddForm.get('ismentalillness').valueChanges.subscribe((Dangerousself: any) => {
                    if (Dangerousself === '1') {
                        this.dangerIll = true;
                        this.personSearchAddForm.get('mentalillnessdetail').setValidators([Validators.required]);
                        this.personSearchAddForm.get('mentalillnessdetail').updateValueAndValidity();
                    } else {
                        this.dangerIll = false;
                        this.personSearchAddForm.get('mentalillnessdetail').clearValidators();
                        this.personSearchAddForm.get('mentalillnessdetail').updateValueAndValidity();
                    }
                });
            }
        }
        if (state) {
            this.personSearchAddForm.get(inputfield).disable();
        } else if (!state && manditory === 'notManditory') {
            this.personSearchAddForm.get(inputfield).enable();
        }
    }
    tabNavigation(id) {
        (<any>$('#' + id)).click();
        if (id === 'reporterrole') {
            this.personRoleTabActive = true;
        }
        if (id === 'profileinfo') {
            this.paginationInfo.pageNumber = 1;
        }
    }
    addPerson(involvedPerson) {
        if (this.personSearchAddForm.valid && this.personSearchAddForm.dirty) {
            if (this.formalSupportForm.valid) {
                this.involvedPerson = Object.assign({
                    activeflag: 1,
                    Dangerousself: involvedPerson.Dangerousself === 'no' ? null : involvedPerson.Dangerousself,
                    DangerousselfReason: involvedPerson.DangerousselfReason,
                    fetalalcoholspctrmdisordflag: involvedPerson.fetalalcoholspctrmdisordflag ? 1 : 0,
                    drugexposednewbornflag: involvedPerson.drugexposednewbornflag ? 1 : 0,
                    probationsearchconductedflag: involvedPerson.probationsearchconductedflag ? 1 : 0,
                    sexoffenderregisteredflag: involvedPerson.sexoffenderregisteredflag ? 1 : 0,
                    Dangerousworker: involvedPerson.Dangerousworker === 'no' ? null : involvedPerson.Dangerousworker,
                    DangerousWorkerReason: involvedPerson.DangerousWorkerReason,
                    ismentalimpair: involvedPerson.ismentalimpair === 'no' ? null : involvedPerson.ismentalimpair,
                    ismentalillness: involvedPerson.ismentalillness === 'no' ? null : involvedPerson.ismentalillness,
                    iscollateralcontact: involvedPerson.iscollateralcontact,
                    ishousehold: involvedPerson.ishousehold,
                    issafehaven: involvedPerson.issafehaven,
                    mentalillnessdetail: involvedPerson.mentalillnessdetail,
                    mentalimpairdetail: involvedPerson.mentalimpairdetail,
                    Dob: this.selectedPersonDetails.personbasicdetails.dob ? this.selectedPersonDetails.personbasicdetails.dob : null,
                    dateofdeath: this.selectedPersonDetails.personbasicdetails.dateofdeath,
                    isapproxdod: this.selectedPersonDetails.personbasicdetails.isapproxdod,
                    effectivedate: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.effectivedate : null,
                    ethnicgrouptypekey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.ethnicgrouptypekey : null,
                    Firstname: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.firstname : null,
                    firstnamesoundex: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.firstnamesoundex : null,
                    Gender: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.gendertypekey : null,
                    incometypekey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.incometypekey : null,
                    interpreterrequired: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.interpreterrequired : null,
                    Lastname: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.lastname : null,
                    safehavenbabyflag: this.selectedPersonDetails.personbasicdetails ? (this.selectedPersonDetails.personbasicdetails.safehavenbabyflag ? 1 : 0) : null,
                    lastnamesoundex: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.lastnamesoundex : null,
                    maritalstatus: null,
                    Middlename: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.middlename : null,
                    nameSuffix: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.nameSuffix : null,
                    primarylanguageid: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.primarylanguageid : null,
                    race: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.racetypekey : null,
                    secondarylanguageid: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.secondarylanguageid : null,
                    insertedby: '',
                    refusessn: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.refusessn : null,
                    refusedob: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.refusedob : null,
                    Pid: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personid : null,
                    ssn: this.selectedPersonDetails.personbasicdetails.personidentifier.length ? this.selectedPersonDetails.personbasicdetails.personidentifier[0].personidentifiervalue : null,
                    aliasid: this.selectedPersonDetails.personbasicdetails.alias ? this.selectedPersonDetails.personbasicdetails.aliasid : null,
                    aliasname: this.selectedPersonDetails.personbasicdetails.alias ? this.selectedPersonDetails.personbasicdetails.alias.firstname : '',
                    dcn: this.selectedPersonDetails.personbasicdetails.personidentifier.length ? this.selectedPersonDetails.personbasicdetails.personidentifier[0].personidentifiervalue : null,
                    role: this.selectedPersonDetails.personbasicdetails.actor.length ? this.selectedPersonDetails.personbasicdetails.actor[0].actortype : null,
                    actorid: this.selectedPersonDetails.personbasicdetails.actor.length ? this.selectedPersonDetails.personbasicdetails.actor[0].actorid : null
                });
                this.involvedPerson.personAddressInput = this.selectedPersonDetails.personbasicdetails.personaddress;
                this.involvedPerson.phoneNumber = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personphonenumber : null;
                this.involvedPerson.school = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personeducation : null;
                this.involvedPerson.accomplishment = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personaccomplishment : null;
                this.involvedPerson.testing = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personeducationtesting : null;
                this.involvedPerson.vocation = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personeducationvocation : null;
                this.involvedPerson.physicianinfo = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personphycisianinfo : [];
                this.involvedPerson.healthinsurance = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personhealthinsurance : [];
                this.involvedPerson.personmedicationphyscotropic = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personmedicationphyscotropic : [];
                this.involvedPerson.personhealthexam = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personhealthexam : [];
                this.involvedPerson.personmedicalcondition = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personmedicalcondition : [];
                this.involvedPerson.personbehavioralhealth = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personbehavioralhealth : [];
                this.involvedPerson.personabusehistory = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personabusehistory : {};
                this.involvedPerson.personabusesubstance = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.personabusesubstance : {};
                this.involvedPerson.persondentalinfo = this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.persondentalinfo : [];
                this.involvedPerson.personRole = [];
                this.involvedPerson.personRole = involvedPerson.personRole;
                if (this.formalSupportForm.controls['personsupporttypekey'].value) {
                    this.formalSupportForm.value.personsupporttypekey = this.formalSupportForm.value.personsupporttypekey ? 'formal' : null;
                    const formalSupport: PersonSupport[] = [];
                    formalSupport.push(Object.assign({
                        personid: this.selectedPersonDetails.personbasicdetails.personid,
                        supportername: null,
                    }, this.formalSupportForm.value));
                    this.involvedPerson.personsupport = formalSupport;
                }
                if (involvedPerson && involvedPerson.personRole) {
                    involvedPerson.personRole.map((item, index) => {
                        if (this.isCW) {
                           item.isprimary = (index === 0) ? 'true' : 'false';
                            if (index === 0) {
                                this.involvedPerson.Role = item.rolekey;
                                this.involvedPerson.RelationshiptoRA = item.relationshiptorakey ? item.relationshiptorakey : null;
                            }
                        } else {
                            if (item.isprimary === 'true') {
                                this.involvedPerson.Role = item.rolekey;
                                this.involvedPerson.RelationshiptoRA = item.relationshiptorakey ? item.relationshiptorakey : null;
                            }
                        }
                    });
                }
                if (this.selectedPersonRow.personid) {
                    this.addedPerson = [];
                    let inputRequest;
                    if (this.isServiceCase) {
                        inputRequest = {
                            objecttypekey: 'servicecase',
                            objectid: this.id,
                            intakeserviceid: null,
                            Person: this.addedPerson,
                        };
                    } else {
                        inputRequest = {
                            Person: this.addedPerson,
                            intakeserviceid: this.id
                        };
                    }
                    this.addedPerson.push(Object.assign(this.involvedPerson));
                    this._commonHttpService.create(inputRequest, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.UpdatePerson).subscribe(
                        (result) => {
                            this.getInvolvedPerson.emit();
                            this._alertService.success('Person Added successfully');
                            this.clearSearchDetails();
                            (<any>$('#intake-findperson')).modal('hide');
                            this._dataStoreService.setData(DSDSConstants.DSDS.PersonsInvolved.Health.Health, new Health());
                            this.healthFormReset$.next(true);
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
                }
            } else {
                this._alertService.warn('Please fill the formal support description');
            }
        } else {
            ControlUtils.validateAllFormFields(this.personSearchAddForm);
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill mandatory fields');
        }
    }
    clearPersonSearch() {
        this.involvedPersonSearchForm.reset();
        this.involvedPersonSearchForm.patchValue({
            occupation: '',
            gender: '',
            mediasrctxt: '',
            stateid: ''
        });
        this.personSearchAddForm.reset();
        this.formalSupportForm.reset();
        this.personSearchAddForm.get('iscollateralcontact').reset();
        this.personSearchAddForm.patchValue({
            Role: '',
            RelationshiptoRA: ''
        });
    }
    addNewPerson() {
        this.newPersonAdd.emit(null);
        this.clearSearchDetails();
        (<any>$('#intake-findperson')).modal('hide');
    }
    addPersonFromSDR() {
        this.isduplicate = false;
        this.verifyDuplicatePerson();
        if (this.isduplicate) {
            this._alertService.error('Duplicate Person');
        } else {
            this.newPersonAdd.emit(this.selectedPersonRow);
            this.clearSearchDetails();
            (<any>$('#intake-findperson')).modal('hide');
        }

    }

    verifyDuplicatePerson() {
      const selectedPersonId = this.selectedPersonRow.personid;
      this.involvedPersonList.forEach(p => {
        if (p.personid === selectedPersonId) {
            this.isduplicate = true;
        }
      });
    }

    onRelationShipToROChange(event: any, i: number) {
        if (this.agency === 'CW') {
            const rolegrp = <FormArray>this.personSearchAddForm.get('personRole');
            rolegrp.controls.forEach(element => {
                element.patchValue({
                    relationshiptorakey: event.value
                });
            });
        }
    }

    relationShipToRO(event: any, i: number) {
        if (this.isDjs) {
            this.personsRoleSelected(event, i);
        } else {
            const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
            const _personRole = personRoleArray.controls[i];
            _personRole.patchValue({ description: event.source.triggerValue });
            const roleCount = personRoleArray.controls.filter((res) => res.value.rolekey === event.value);
            if (['BIOCHILD', 'CHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC'].includes(event.value)) {
                this.filterrelationshipByRole('child');
            } else {
                this.filterrelationshipByRole('LG');
            }
            if (roleCount.length > 1) {
                this.deleteRole(i);
                this._alertService.error('Role is already Added');
            } else {
                if (event.value !== 'RA' && event.value !== 'PA' && event.value !== 'RC' && event.value !== 'CLI') {
                    const raCount = personRoleArray.controls.filter((res) => res.value.hidden === false);
                    if (raCount.length === 0) {
                        _personRole.patchValue({ hidden: false });
                        _personRole.get('relationshiptorakey').setValidators([Validators.required]);
                        _personRole.get('relationshiptorakey').updateValueAndValidity();
                    } else {
                        _personRole.patchValue({ hidden: true });
                        _personRole.get('relationshiptorakey').clearValidators();
                        _personRole.get('relationshiptorakey').updateValueAndValidity();
                    }
                } else {
                    _personRole.patchValue({ hidden: true });
                    _personRole.get('relationshiptorakey').clearValidators();
                    _personRole.get('relationshiptorakey').updateValueAndValidity();
                }
            }

            // D-07962 Start
            if (event.value === 'CHILD') {
                _personRole.get('relationshiptorakey').setValue('SELF');
            }
            if (this.isCW) {
                _personRole.patchValue({ isprimary: 'true' });
            }
            // D-07962 End
        }
    }

    personsRoleSelected(event: any, i: number) {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls[i];
        _personRole.patchValue({ isprimary: 'true' });
        if (event.value !== 'RA' && event.value !== 'PA' && event.value !== 'RC' && event.value !== 'CLI' && event.value !== 'Youth') {
            _personRole.patchValue({ hidden: false });
            _personRole.get('relationshiptorakey').setValidators([Validators.required]);
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        } else {
            _personRole.patchValue({ hidden: true });
            _personRole.get('relationshiptorakey').clearValidators();
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        }

        // D-07265 Start
        // if (event.value !== 'Youth' || event.value === 'Youth') {
            // D-07265 End
            this.isDjsYouth = false;
            this.personSearchAddForm.get('Dangerousself').clearValidators();
            this.personSearchAddForm.get('Dangerousself').updateValueAndValidity();
            this.personSearchAddForm.get('Dangerousworker').clearValidators();
            this.personSearchAddForm.get('Dangerousworker').updateValueAndValidity();
            this.personSearchAddForm.get('ismentalimpair').clearValidators();
            this.personSearchAddForm.get('ismentalimpair').updateValueAndValidity();
            this.personSearchAddForm.get('ismentalillness').clearValidators();
            this.personSearchAddForm.get('ismentalillness').updateValueAndValidity();
        // } else {
        //     this.isDjsYouth = true;
        // }
    }
    checkPrimaryRole(i) {
        const personRoleArray = <FormArray>this.personSearchAddForm.controls.personRole;
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach((x) => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        if (primaryCount > 1) {
            this._alertService.error('Primary role for this person is already selected');
            _personRole[i].patchValue({ isprimary: 'false' });
        }
    }
    addNewRole() {
        const control = <FormArray>this.personSearchAddForm.controls['personRole'];
        control.push(this.createFormGroup(true));
    }
    deleteRole(index: number) {
        const control = <FormArray>this.personSearchAddForm.controls['personRole'];
        control.removeAt(index);
    }
    clearSearchDetails() {
        // this.personSearchAddForm.setControl('personRole', this._formBuilder.array([]));
        this.personSearchResult$ = Observable.empty();
        this.tabNavigation('profileinfo');
        (<any>$('#intake-findperson')).modal('hide');
        this.clearPersonSearch();
        this.personRoleTabActive = false;
        this.serachResultTabActive = false;
        this.profileTabActive = false;
        // this.representativePerson = false;
        if (this.isEmergencyContact) {
            (<any>$('#intake-findperson')).modal('hide');
            this.serachPersonProfileInfo$.next(true);
        }
        this.close.emit();
    }
    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.getPage(this.paginationInfo.pageNumber);
    }
    private loadDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.GenderTypeUrl + '?filter'
            ),
            // this._commonHttpService.getArrayList(
            //     {
            //         where: { activeflag: 1,
            //             datypeid: (this._authService.isCW() && this.da_type) ? this.da_typeid : null  },
            //         method: 'get',
            //         nolimit: true
            //     },
            //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.UserActorTypeUrl + '?filter'
            // ),
            // this._commonHttpService.getArrayList(
            //     {
            //         where: { activeflag: 1, teamtypekey: this._authService.getAgencyName()},
            //         method: 'get',
            //         nolimit: true
            //     },
            //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.RelationshipTypesUrl  + '?filter'
            // ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'countyname asc'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
            )
        ])
            .map((result) => {
                return {
                    genders: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gendertypekey
                            })
                    ),
                    // roles: result[1].map(
                    //     (res) => { return {
                    //             text: res.typedescription,
                    //             value: res.actortype,
                    //             rolegrp : res.rolegroup
                    //         }; }
                    // ),
                    // relationShipToRAs: result[2].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.description,
                    //             value: res.relationshiptypekey
                    //         })
                    // ),
                    states: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    counties: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyname
                            })
                    )
                };
            })
            .share();
        this.genderDropdownItems$ = source.pluck('genders');
        // this.roleDropdownItems$ = source.pluck('roles');
        // this.relationShipToRADropdownItems$ = source.pluck('relationShipToRAs');
        this.stateDropdownItems$ = source.pluck('states');
        this.countyDropdownItems$ = source.pluck('counties');
    }

    getCommonDropdowns() {
        this.roleDropdownItems$ = this._dataStoreService.getData('role');
        this.relationShipToRADropdownItems$ = this._dataStoreService.getData('relation');
        this.relationShipToRADropdownItems = this._dataStoreService.getData('relation');
    }

    private getPage(pageNumber: number) {
        ObjectUtils.removeEmptyProperties(this.involvedPersonSearch);
        const source = this._involvedPersonSeachService
            .getPagedArrayList(
                {
                    limit: this.paginationInfo.pageSize,
                    order: this.paginationInfo.sortBy,
                    page: pageNumber,
                    count: this.paginationInfo.total,
                    where: this.involvedPersonSearch,
                    method: 'post'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonSearchUrl
            )
            .map((result) => {
                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.personSearchResult$ = source.pluck('data');
        if (pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    calculateAge(dob) {
        // const dob = this.selectedPerson.dob;
        let age = -1;
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            age = moment().diff(rCDob, 'years');
            const days = moment().diff(rCDob, 'days');
            this.enableSafeHaven = (days <= 10);
        }
        return age;
    }

    filterRoles(item): boolean {
        if (this.selectedPersonsage === -1) {
            return false;
        } else if (this.selectedPersonsage < 22) {
            if (item.rolegrp === 'C' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        } else if (this.selectedPersonsage >= 22) {
            if (item.rolegrp === 'A' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        }
    }

    filterrelationshipByRole(role) {
        const childrelationList = ['BIOBR', 'BGSISTR', 'BFRND', 'BFRNDX', 'DACRCHLD', 'fosterchild', 'FRND', 'GFRND',
            'GFRNDX', 'HLFBR', 'HLFSISTR', 'LEGLBR', 'LGLSISTR', 'MTNLCN', 'MATNLNPW',
            'MATNLNC', 'NBHR', 'NORLTN', 'OTHER', 'PRNTLCN', 'PRNTLNPW', ' PRNTLNC', 'PUTCHLD', 'RELOTHR', 'RESDNT', 'SELF', 'SNFOPAR', 'STPBR', 'STPSISTR',
            'STUDNT', 'UNKNWN'];
        if (this.agency === 'CW') {
            switch (role) {
                case 'child':
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems.filter(item => childrelationList.includes(item.value));
                    break;

                default:
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems.filter(item => true);
                    break;
            }
        }
    }

    getPrimaryRelationname(listdata) {
        const finddata = listdata.find(data => data.relationcategory === 'Primary');
        if (finddata) {
            return (finddata.relationtype + ': ' + finddata.personname);
        }
        return '';
    }
    // reset collateral chkbox
    resetCollateralChkbox() {
        if (this.isCW) {
        this.personSearchAddForm.get('iscollateralcontact').reset();
        this.resetRoleTabRadioBtn(null);
        }
    }
    // reset roletab radio btn
    resetRoleTabRadioBtn(paramValue = null) {
        if (this.isCW) {
        let value = false;
        if (paramValue !== null) {
            value = paramValue;
        } else {
            value = this.personSearchAddForm.get('iscollateralcontact').value;
        }
        const radioBtnAry = ['ishousehold', 'ismentalillness', 'ismentalimpair', 'Dangerousworker', 'Dangerousself'];
        if (value) {
            radioBtnAry.forEach(ctrlName => {
                this.personSearchAddForm.get(ctrlName).clearValidators();
                this.personSearchAddForm.get(ctrlName).updateValueAndValidity();
            });
            this.isCheckedCollateral = false;
            this.personSearchAddForm.get('ishousehold').reset();
        } else {
            // radioBtnAry.forEach(ctrlName => {
            //     this.personSearchAddForm.get(ctrlName).setValidators([Validators.required]);
            //     this.personSearchAddForm.get(ctrlName).updateValueAndValidity();
            // });
            this.personSearchAddForm.get('ishousehold').setValidators([Validators.required]);
            this.personSearchAddForm.get('ishousehold').updateValueAndValidity();

            this.personSearchAddForm.get('ismentalillness').setValidators([Validators.required]);
            this.personSearchAddForm.get('ismentalillness').updateValueAndValidity();

            this.personSearchAddForm.get('ismentalimpair').setValidators([Validators.required]);
            this.personSearchAddForm.get('ismentalimpair').updateValueAndValidity();

            this.personSearchAddForm.get('Dangerousworker').setValidators([Validators.required]);
            this.personSearchAddForm.get('Dangerousworker').updateValueAndValidity();

            this.personSearchAddForm.get('Dangerousself').setValidators([Validators.required]);
            this.personSearchAddForm.get('Dangerousself').updateValueAndValidity();
            this.isCheckedCollateral = true;
        }
    }
    }
}
