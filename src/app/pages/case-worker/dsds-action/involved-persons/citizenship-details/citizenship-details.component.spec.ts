import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitizenshipDetailsComponent } from './citizenship-details.component';

describe('CitizenshipDetailsComponent', () => {
  let component: CitizenshipDetailsComponent;
  let fixture: ComponentFixture<CitizenshipDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitizenshipDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitizenshipDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
