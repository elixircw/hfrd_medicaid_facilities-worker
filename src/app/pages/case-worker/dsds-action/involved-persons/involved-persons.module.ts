import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvolvedPersonsRoutingModule } from './involved-persons-routing.module';
import { InvolvedPersonsComponent } from './involved-persons.component';
import { InvolvedPersonsSearchComponent } from './involved-persons-search.component';
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatTooltipModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { EducationalProfileComponent } from '../../../../person-info/educational-profile/educational-profile.component';
import { PersonHealthComponent } from '../../../../person-info/person-health/person-health.component';
import { PersonHealthSectionLogComponent } from './person-health/person-health-section-log/person-health-section-log.component';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule } from 'ngx-mask';
import { CitizenshipDetailsComponent } from './citizenship-details/citizenship-details.component';
import { HealthInsuranceInformationComponent } from './person-health/person-health-sections/health-insurance-information/health-insurance-information.component';
import { MedicationIncludingPsychotropicComponent } from './person-health/person-health-sections/medication-including-psychotropic/medication-including-psychotropic.component';
import { MedicationIncludingPsychotropicCwComponent } from './person-health/person-health-sections/medication-including-psychotropic-cw/medication-including-psychotropic-cw.component';
import { HealthExaminationComponent } from './person-health/person-health-sections/health-examination/health-examination.component';
import { MedicalConditionsComponent } from './person-health/person-health-sections/medical-conditions/medical-conditions.component';
import { MedicalConditionsCwComponent } from '../../../../person-info/person-health/person-health-sections/medical-conditions-cw/medical-conditions-cw.component';
import { BehavioralHealthInfoComponent } from '../../../../person-info/person-health/person-health-sections/behavioral-health-info/behavioral-health-info.component';

import { HistoryOfAbuseComponent } from './person-health/person-health-sections/history-of-abuse/history-of-abuse.component';
import { SubstanceAbuseComponent } from '../../../../person-info/person-health/person-health-sections/substance-abuse/substance-abuse.component';
import { ProviderInformationCwComponent } from '../../../../person-info/person-health/person-health-sections/provider-information-cw/provider-information-cw.component';
import { DentalInformationCwComponent } from './person-health/person-health-sections/dental-information-cw/dental-information-cw.component';
import { PhysicianInformationComponent } from './person-health/person-health-sections/physician-information/physician-information.component';
import { CommonControlsModule } from '../../../../shared/modules/common-controls/common-controls.module';
import { GuardianshipComponent } from './guardianship/guardianship.component';
import { AttorneyComponent } from './guardianship/attorney/attorney.component';
import { GuardianPersonComponent } from './guardianship/guardian-person/guardian-person.component';
import { GuardianPropertyComponent } from './guardianship/guardian-property/guardian-property.component';
import { WorkerComponent } from './guardianship/worker/worker.component';
import { HssCodeStatusComponent } from './guardianship/hss-code-status/hss-code-status.component';
import { FuneralComponent } from './guardianship/funeral/funeral.component';
import { PersonHealthDisabilityComponent } from '../../../../person-info/person-health/person-health-sections/person-health-disability/person-health-disability.component';
import { PersonDisabilityService } from '../../../shared-pages/person-disability/person-disability.service';
import { ExaminationCwComponent } from '../../../../person-info/person-health/person-health-sections/examination-cw/examination-cw.component';
import { Under5yearsCwComponent } from '../../../../person-info/person-health/person-health-sections/under-5years-cw/under-5years-cw.component';
import { MedicationCwComponent } from './person-health/person-health-sections/medication-cw/medication-cw.component';
import { SexualInformationCwComponent } from '../../../../person-info/person-health/person-health-sections/sexual-information-cw/sexual-information-cw.component';
import { HospitalizationCwComponent } from '../../../../person-info/person-health/person-health-sections/hospitalization-cw/hospitalization-cw.component';
import { CommonDropdownsService } from '../../../../@core/services';
import { WorkProfileComponent } from '../../../../person-info/work-profile/work-profile.component';
/* Need to use its own component */
// import { WorkProfileComponent } from '../../../provider-referral/new-referral/intake-persons-involved/work-profile/work-profile.component';

import { Eighteen21Component } from './eighteen21/eighteen21.component';
import { PersonHealthModule } from '../../../../person-info/person-health/person-health.module';

@NgModule({
    imports: [
        CommonModule,
        InvolvedPersonsRoutingModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatRadioModule,
        MatTabsModule,
        MatCheckboxModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        TimepickerModule,
        ControlMessagesModule,
        SharedDirectivesModule,
        SharedPipesModule,
        MatAutocompleteModule,
        ImageCropperModule,
        CommonControlsModule,
        MatTooltipModule,
        NgxfUploaderModule.forRoot(),
        NgxMaskModule.forRoot(),
        PersonHealthModule

    ],
    declarations: [InvolvedPersonsComponent, InvolvedPersonsSearchComponent, CitizenshipDetailsComponent,
        EducationalProfileComponent, PersonHealthComponent, PersonHealthSectionLogComponent, PersonHealthSectionLogComponent,
        HealthInsuranceInformationComponent, MedicationIncludingPsychotropicComponent, MedicationIncludingPsychotropicCwComponent,
        HealthExaminationComponent, MedicalConditionsComponent, MedicalConditionsCwComponent, BehavioralHealthInfoComponent,
        BehavioralHealthInfoComponent,
        HistoryOfAbuseComponent, SubstanceAbuseComponent, ProviderInformationCwComponent, DentalInformationCwComponent,
         PhysicianInformationComponent, GuardianshipComponent, AttorneyComponent,
        GuardianPersonComponent, GuardianPropertyComponent, WorkerComponent, HssCodeStatusComponent, FuneralComponent,
        PersonHealthDisabilityComponent, ExaminationCwComponent, Under5yearsCwComponent,
        MedicationCwComponent,
        SexualInformationCwComponent, HospitalizationCwComponent, WorkProfileComponent, Eighteen21Component,
    ],
    providers: [
        PersonDisabilityService, CommonDropdownsService
    ]
})
export class InvolvedPersonsModule {}
