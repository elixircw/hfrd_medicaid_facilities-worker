import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvolvedPersonsSearchComponent } from './involved-persons-search.component';

describe('InvolvedPersonsSearchComponent', () => {
  let component: InvolvedPersonsSearchComponent;
  let fixture: ComponentFixture<InvolvedPersonsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvolvedPersonsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvolvedPersonsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
