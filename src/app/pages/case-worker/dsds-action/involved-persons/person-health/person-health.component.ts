import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AlertService, AuthService } from '../../../../../@core/services';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { Physician } from '../_entities/involvedperson.data.model';
import { Subject } from 'rxjs/Subject';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'person-health',
    templateUrl: './person-health.component.html',
    styleUrls: ['./person-health.component.scss']
})
export class PersonHealthComponent implements OnInit {
    addHealth: any;
    physicianForm: FormGroup;
    physician: any[] = [];
    token: AppUser;
    healthSections: any[];
    djshealthSections = [
        { id: 'physician-information', name: 'Physician Information', isActive: true, securityKey: '' },
        { id: 'health-insurance-information', name: 'Health Insurance Information', isActive: false, securityKey: '' },
        { id: 'medication-including-psychotropic', name: 'Medication Including Psychotropic', isActive: false, securityKey: '' },
        { id: 'health-examination', name: 'Health Examination/Evaluations', isActive: false, securityKey: '' },
        { id: 'medical-conditions', name: 'Medical Conditions', isActive: false, securityKey: '' },
        { id: 'behavioral-health-info', name: 'Behavioral Health Info', isActive: false, securityKey: '' },
        { id: 'history-of-abuse', name: 'History of Abuse', isActive: false, securityKey: '' },
        { id: 'substance-abuse', name: 'Substance Abuse', isActive: false, securityKey: '' },
        // { id: '', name: 'Substance Use' },
        // { id: '', name: 'Substance Abuse Assessments' },
        // { id: '', name: 'Dental' },
        // { id: '', name: 'Vision' },
        // { id: '', name: 'Immunizations' },
        // { id: '', name: 'Behavioral/Mental Health Situation' },
        { id: 'log', name: 'Log', isActive: false }
    ];
    cwhealthSections = [
        { id: 'examination-cw', name: 'Examination', isActive: true },
        { id: 'under-5years-cw', name: 'Under 5 Years', isActive: true },
        { id: 'birth-information-cw', name: 'Birth Information', isActive: true },
        { id: 'sexual-information-cw', name: 'Sexual Information', isActive: true },
        { id: 'hospitalization-cw', name: 'Hospitalization', isActive: true },
        { id: 'immunization-cw', name: 'Immunization', isActive: true },
        { id: 'chronic-cw', name: 'Chronic', isActive: true },
        { id: 'allergies-cw', name: 'Allergies', isActive: true },
        // { id: 'medication-cw', name: 'Medication', isActive: true },
        { id: 'family-history-cw', name: 'Family History', isActive: true },
        { id: 'provider-dental-information-cw', name: 'Provider Information', isActive: true, securityKey: 'caseworker-persons-edit-health-provider-info' },
        { id: 'insurance-information-cw', name: 'Insurance Information', isActive: false, securityKey: 'caseworker-persons-edit-health-insurance-info'  },
        { id: 'medication-including-psychotropic-cw', name: 'Medication Including Psychotropic', isActive: false, securityKey: 'caseworker-persons-edit-health-medical-including'  },
        { id: 'medical-conditions-cw', name: 'Medical Conditions', isActive: false, securityKey: 'caseworker-persons-edit-health-medical-conditions' },
        { id: 'person-disability-cw', name: 'Disabilities', isActive: false },
        { id: 'behavioral-health-info-cw', name: 'Behavioral Health Info', isActive: false, securityKey: 'caseworker-persons-edit-health-behavioral-health-info' },
        { id: 'substance-abuse', name: 'Substance Abuse', isActive: false, securityKey: 'caseworker-persons-edit-health-substance-abuse' },
    ];


    selectedHealthSection: any;
    physicianEditInd = -1;
    @Input()
    healthFormReset$ = new Subject<boolean>();

    constructor(private formbulider: FormBuilder,
        private _alertSevice: AlertService,
        private _authService: AuthService) {
    }

    ngOnInit() {
        this.token = this._authService.getCurrentUser();
        const teamTypeKey = this.token.user.userprofile.teamtypekey;
        if (teamTypeKey === 'DJS') {
            this.healthSections = this.djshealthSections;
        } else if (teamTypeKey === 'CW') {
            this.healthSections = this.cwhealthSections;
        } else if (teamTypeKey === 'AS') {
            this.healthSections = this.djshealthSections;
        }
        this.selectedHealthSection = this.healthSections[0];
        this.healthFormReset$.subscribe((res) => {
            if (res === true) {
                // this.resetForm();
                this.showHealthSection(this.healthSections[0]);
            }
        });
    }
    addPhysician(model: Physician) {

        this.physician.push(model);
        this.addHealth.physician = this.physician;
        // this.addHealthSubject$.next(this.addHealth);
    }
    viewPhysician(model) {
        this.physicianForm.patchValue(model);
    }

    showHealthSection(healthSection) {
        this.healthSections.forEach(section => section.isActive = false);
        healthSection.isActive = true;
        this.selectedHealthSection = healthSection;
    }

}
