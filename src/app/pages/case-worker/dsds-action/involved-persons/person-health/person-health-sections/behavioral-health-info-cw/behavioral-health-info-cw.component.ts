import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../../../../@core/services';
import { BehaviouralHealthInfo, Health } from '../../../_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { GLOBAL_MESSAGES } from '../../../../../../../@core/entities/constants';
import { HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../../../../../../../app.config';
import { AppUser } from '../../../../../../../@core/entities/authDataModel';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'behavioral-health-info-cw',
  templateUrl: './behavioral-health-info-cw.component.html',
  styleUrls: ['./behavioral-health-info-cw.component.scss']
})
export class BehavioralHealthInfoCwComponent implements OnInit {
  behaviouralHealthInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  behaviouralcw: BehaviouralHealthInfo[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  uploadedFile: File;
  private token: AppUser;
  serviceDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService) {
    this.token = this._authService.getCurrentUser();
  }

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
  }

  initForm() {
    this.behaviouralHealthInfoForm = this._formBuilder.group({
      behaviouralhealthdiagnosis: '',
      typeofservice: '',
      clinicianname: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      county: '',
      zip: '',
      phone: '',
      currentdiagnoses: '',
      reportname: '',
      reportpath: ''
    });
  }

  private resetForm() {
    this.behaviouralHealthInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.behaviouralHealthInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.behaviouralHealthInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.behaviouralHealthInfoForm.enable();
  }

  private delete(index) {
    this.behaviouralcw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: BehaviouralHealthInfo) {
    this.behaviouralHealthInfoForm.patchValue(modal);
  }

  private add() {
    this.behaviouralcw.push(this.behaviouralHealthInfoForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.personBehaviour = this.behaviouralcw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
    // this._uploadService
    //   .upload({
    //     url: AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
    //     headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
    //     filesKey: ['file'],
    //     files: this.uploadedFile,
    //     process: true
    //   })
    //   .subscribe(
    //     (response) => {
    //       if (response.status) {
    //         // this.progress.percentage = response.percent;
    //       }
    //       if (response.status === 1 && response.data) {
    //         this.behaviouralcw.push(this.behaviouralHealthInfoForm.getRawValue());
    //         this.behaviouralcw[this.behaviouralcw.length - 1].reportname = response.data.originalfilename;
    //         this.behaviouralcw[this.behaviouralcw.length - 1].reportpath = response.data.s3bucketpathname;
    //         this.health = this._dataStoreService.getData(this.constants.Health);
    //         this.health.personBehaviour = this.behaviouralcw;
    //         this._dataStoreService.setData(this.constants.Health, this.health);
    //         this._alertSevice.success('Added Successfully');
    //         this.resetForm();
    //         this._alertSevice.success('File Uploaded Succesfully!');
    //       }
    //     },
    //     (err) => {
    //       console.log(err);
    //       this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
    //     },
    //     () => {
    //       // console.log('complete');
    //     }
    //   );
  }

  uploadFile(file: File | FileError): void {
    if (!(file instanceof File)) {
      // this.alertError(file);
      return;
    }

    this.uploadedFile = file;
    this.behaviouralHealthInfoForm.patchValue({ reportname: file.name });
  }

  private update() {
    if (this.modalInt !== -1) {
      this.behaviouralcw[this.modalInt] = this.behaviouralHealthInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { activeflag: 1 },
          order: 'description asc'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.personservicetype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'countyname asc'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
      )
    ]).map((result) => {
      return {
        typeofservice: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.personservicetypekey
            })
        ),
        states: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.statename,
              value: res.stateabbr
            })
        ),
        counties: result[2].map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyname
            })
        )
      };
    })
      .share();
    this.serviceDropdownItems$ = source.pluck('typeofservice');
    this.stateDropdownItems$ = source.pluck('states');
    this.countyDropDownItems$ = source.pluck('counties');
  }
}
