import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../../@core/services';
import { PersonAllergies, Health } from '../../../_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'allergies-cw',
  templateUrl: './allergies-cw.component.html',
  styleUrls: ['./allergies-cw.component.scss']
})
export class AllergiesCwComponent implements OnInit {
  allergiesInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  allergiescw: PersonAllergies[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  allergiesTypeDropdownItems$: Observable<DropdownModel[]>;
  specialneedsTypeDropdownItems$: Observable<DropdownModel[]>;
  hygieneTypeDropdownItems$: Observable<DropdownModel[]>;
  fearsTypeDropdownItems$: Observable<DropdownModel[]>;

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
  }

  initForm() {
    this.allergiesInfoForm = this._formBuilder.group({
      allergieskey: '',
      allergiessymptoms: '',
      specialkey: '',
      specialcomments: '',
      phobiakey: '',
      phobiacomments: '',
      hygienekey: '',
      hygienecomments: '',
      medicationcomments: '',
      comments: ''
    });
  }
  private resetForm() {
    this.allergiesInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.allergiesInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.allergiesInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.allergiesInfoForm.enable();
  }

  private delete(index) {
    this.allergiescw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: PersonAllergies) {
    this.allergiesInfoForm.patchValue(modal);
  }

  private add() {
    this.allergiescw.push(this.allergiesInfoForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.personAllergies = this.allergiescw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.allergiescw[this.modalInt] = this.allergiesInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '15', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
          {
              method: 'get',
              nolimit: true,
              where: { 'active_sw': 'Y', 'picklist_type_id': '203', 'delete_sw': 'N' }
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
          {
              method: 'get',
              nolimit: true,
              where: { 'active_sw': 'Y', 'picklist_type_id': '137', 'delete_sw': 'N' }
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
          {
              method: 'get',
              nolimit: true,
              where: { 'active_sw': 'Y', 'picklist_type_id': '80', 'delete_sw': 'N' }
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        allergiesTypeList: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        ),
        specialneedsTypeList: result[1].map(
            (res) =>
                new DropdownModel({
                  text: res.description_tx,
                  value: res.value_tx
                })
        ),
        hygieneTypeList: result[2].map(
            (res) =>
                new DropdownModel({
                  text: res.description_tx,
                  value: res.value_tx
                })
        ),
        fearsTypeList: result[3].map(
            (res) =>
                new DropdownModel({
                  text: res.description_tx,
                  value: res.value_tx
                })
        )
      };
    })
      .share();
    this.allergiesTypeDropdownItems$ = source.pluck('allergiesTypeList');
    this.specialneedsTypeDropdownItems$ = source.pluck('specialneedsTypeList');
    this.hygieneTypeDropdownItems$ = source.pluck('hygieneTypeList');
    this.fearsTypeDropdownItems$ = source.pluck('fearsTypeList');
  }
}
