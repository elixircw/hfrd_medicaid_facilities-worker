import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../../@core/services';
import { PersonAllergies, Health } from '../../../_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'immunization-cw',
  templateUrl: './immunization-cw.component.html',
  styleUrls: ['./immunization-cw.component.scss']
})
export class ImmunizationCwComponent implements OnInit {
  immunizationInfoForm: FormGroup;
  isImmunizedforPerson: boolean;
  immunizationTypeDropdownItems$: Observable<DropdownModel[]>;

  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.isImmunizedforPerson = false;
    this.loadDropDowns();
    this.initForm();
  }

  initForm() {
    this.immunizationInfoForm = this._formBuilder.group({
      isimmunized: [''],
      immunizationkey: '',
      date: '',
      duedate: '',
      comments: ''
    });
  }

  isImmunizedExists(opt) {

    this.enableorDisableField('immunizationkey', opt);
    this.enableorDisableField('date', opt);
    this.enableorDisableField('duedate', opt);
    this.enableorDisableField('comments', opt);
    this.isImmunizedforPerson = opt;
    this.immunizationInfoForm.reset();
    this.immunizationInfoForm.patchValue({ 'isinsuranceavailable': opt });
  }
  private enableorDisableField(field, opt) {
    if (opt) {
      this.immunizationInfoForm.get(field).enable();
    }
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '231', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        immunizationTypeList: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.picklist_value_cd
            })
        )
      };
    })
      .share();
    this.immunizationTypeDropdownItems$ = source.pluck('immunizationTypeList');
  }
}
