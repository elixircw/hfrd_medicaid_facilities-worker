import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../../../../@core/services';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { MedicalConditions, Health } from '../../../_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'medical-conditions-cw',
  templateUrl: './medical-conditions-cw.component.html',
  styleUrls: ['./medical-conditions-cw.component.scss']
})
export class MedicalConditionsCwComponent implements OnInit {
  medicalconditionForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  minDate = new Date();
  maxDate = new Date();
  isCustomMedicalCondition: boolean;
  medicalcondition: MedicalConditions[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  medicalConditionType$: Observable<DropdownModel[]>;
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.loadDropDowns();
    this.editMode = false;
    this.isCustomMedicalCondition = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.medicalconditionForm = this.formbulider.group({
      medicalconditiontypekey: '',
      medicalconditionsnotes: null,
      custommedicalcondition: null,
      begindate: '',
      enddate: '',
      recordedby: ''
    });

    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.medicalcondition) {
      this.medicalcondition = this.health.medicalcondition;
    }

    this.medicalconditionForm.valueChanges.subscribe(physician => {
     // this.setMedicalCondition('opt');
     // this._dataStoreService.setData('medical_condition', this.medicalconditionForm.getRawValue());
    });

  }

  setMedicalCondition(option) {
    const medicalcondition = this.medicalconditionForm.getRawValue();
    const conditions = medicalcondition.medicalconditiontypekey;
    const Index  =   conditions.findIndex(c => c === 'Other ');
    if (Index !== -1) {
      this.isCustomMedicalCondition = true;
      this.medicalconditionForm.get('custommedicalcondition').setValidators([Validators.required]);
      this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
    } else {
      this.isCustomMedicalCondition = false;
      this.medicalconditionForm.get('custommedicalcondition').clearValidators();
      this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
    }
  }

  setmedicalconditionflag(opt) {
    if (opt) {
      this.isCustomMedicalCondition = true;
      this.medicalconditionForm.get('custommedicalcondition').setValidators([Validators.required]);
      this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
    } else {
      this.isCustomMedicalCondition = false;
      this.medicalconditionForm.get('custommedicalcondition').clearValidators();
      this.medicalconditionForm.get('custommedicalcondition').updateValueAndValidity();
    }
  }
  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.medicalconditiontype + '?filter'
      )
    ])
      .map((result) => {
        return {
          medicalconditiontype: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.description
              })
          )
        };
      })
      .share();
    this.medicalConditionType$ = source.pluck('medicalconditiontype');
  }
  private add() {
    this.medicalcondition.push(this.medicalconditionForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.medicalcondition = this.medicalcondition;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.medicalconditionForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.medicalconditionForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.medicalcondition[this.modalInt] = this.medicalconditionForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    const flag =  modal.custommedicalcondition ? true : false;
    this.setmedicalconditionflag(flag);
    this.patchForm(modal);
    this.editMode = false;
    this.medicalconditionForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    const flag =  modal.custommedicalcondition ? true : false;
    this.setmedicalconditionflag(flag);
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.medicalconditionForm.enable();
  }

  private delete(index) {
    this.medicalcondition.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  startDateChanged() {
    this.medicalconditionForm.patchValue({enddate : ''});
    const empForm = this.medicalconditionForm.getRawValue();
    this.maxDate = new Date(empForm.begindate);
  }
  endDateChanged() {
    this.medicalconditionForm.patchValue({begindate : ''});
    const empForm = this.medicalconditionForm.getRawValue();
    this.minDate = new Date(empForm.enddate);
  }
  private patchForm(modal: MedicalConditions) {
    this.medicalconditionForm.patchValue(modal);
  }
}
