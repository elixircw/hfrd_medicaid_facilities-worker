import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../../@core/services';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { BirthInfocw, Health } from '../../../_entities/involvedperson.data.model';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'birth-information-cw',
  templateUrl: './birth-information-cw.component.html',
  styleUrls: ['./birth-information-cw.component.scss']
})
export class BirthInformationCwComponent implements OnInit {

  birthInfoForm: FormGroup;
  examTypeList: any[];
  specialityExamTypeList: any[];
  labTestList: any[];
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  birthInfocw: BirthInfocw[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  pregnatMothersUseDropdownItems$: Observable<DropdownModel[]>;
  medicalConditionsDropdownItems$: Observable<DropdownModel[]>;
  diseasesDropdownItems$: Observable<DropdownModel[]>;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {

  }

  ngOnInit() {
    this.reportMode = 'add';
     this.loadDropDown();
    this.birthInfoForm = this.formbulider.group({
      mothersuse: '',
      motherspciality: '',
      medicalcondition: '',
      medicalspciality: '',
      diseases: '',
      diseasespciality: '',
      birthdefects: '',
      comments: '',
    });
  }

  private resetForm() {

    this.birthInfoForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.birthInfoForm.enable();

  }


  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.birthInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.birthInfoForm.enable();
  }

  private delete(index) {
    this.birthInfocw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private add() {
    this.birthInfocw.push(this.birthInfoForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.birthInfo = this.birthInfocw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.birthInfocw[this.modalInt] = this.birthInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }
  private patchForm(modal: BirthInfocw) {
    this.birthInfoForm.patchValue(modal);

    // if (modal.startdate) {
    //   this.birthInfoForm.patchValue({startdate: new Date(modal.startdate)});
    // }
    // if (modal.enddate) {
    //   this.birthInfoForm.patchValue({enddate: new Date(modal.enddate)});
    // }
  }

  private loadDropDown() {
    const source = forkJoin([
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { activeflag: 1, 'picklist_type_id': '125' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { activeflag: 1, 'picklist_type_id': '48' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { activeflag: 1, 'picklist_type_id': '71' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        )
    ])
        .map((result) => {
            return {
              pregnatMothersUseList: result[0].map(
                    (res) =>
                        new DropdownModel({
                            text: res.description_tx,
                            value: res.value_tx
                        })
                ),
                medicalConditionsList: result[1].map(
                    (res) =>
                        new DropdownModel({
                          text: res.description_tx,
                          value: res.value_tx
                        })
                ),
                diseasesList: result[2].map(
                    (res) =>
                        new DropdownModel({
                          text: res.description_tx,
                          value: res.value_tx
                        })
                )
            };
        })
        .share();
    this.pregnatMothersUseDropdownItems$ = source.pluck('pregnatMothersUseList');
    this.medicalConditionsDropdownItems$ = source.pluck('medicalConditionsList');
    this.diseasesDropdownItems$ = source.pluck('diseasesList');

}

}
