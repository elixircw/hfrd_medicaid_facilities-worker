import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChronicCwComponent } from './chronic-cw.component';

describe('ChronicCwComponent', () => {
  let component: ChronicCwComponent;
  let fixture: ComponentFixture<ChronicCwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChronicCwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChronicCwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
