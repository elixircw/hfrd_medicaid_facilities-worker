import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicationCwComponent } from './medication-cw.component';

describe('MedicationCwComponent', () => {
  let component: MedicationCwComponent;
  let fixture: ComponentFixture<MedicationCwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationCwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationCwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
