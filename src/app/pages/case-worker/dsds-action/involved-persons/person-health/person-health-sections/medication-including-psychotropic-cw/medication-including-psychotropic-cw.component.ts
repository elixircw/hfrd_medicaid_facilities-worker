import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { Medication } from '../../../_entities/involvedperson.data.model';
import { Health } from '../../../../../../newintake/my-newintake/_entities/newintakeModel';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'medication-including-psychotropic-cw',
  templateUrl: './medication-including-psychotropic-cw.component.html',
  styleUrls: ['./medication-including-psychotropic-cw.component.scss']
})
export class MedicationIncludingPsychotropicCwComponent implements OnInit {
  medicationpsychotropicForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  medicationpsychotropic: Medication[] = [];
  health: Health = {};
  medicationType$: Observable<DropdownModel[]>;
  medicationType: any = [];
  isMedicationIncludes:  boolean;
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  prescriptionReasonType$: Observable<DropdownModel[]>;
  informationSourceType$: Observable<DropdownModel[]>;
  maxDate = new Date();
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.loadDropDowns();
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.isMedicationIncludes = false;
    this.medicationpsychotropicForm = this.formbulider.group({
      medicationname: '',
      medicationtype: '',
      dosage: '',
      frequency: '',
      complaint: '',
      informationsourcetypekey: '',
      medicationexpirationdate: '',
      startdate: '',
      enddate: '',
      prescribingdoctor: '',
      prescriptionreasontypekey: '',
      comments: '',
      isMedicationIncludes: [''],
      reportedby: ['']
    });

    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.medication) {
      this.medicationpsychotropic = this.health.medication;
    }

    // console.log('medicationpsychotropic', this.health);

    // this.medicationpsychotropicForm.valueChanges.subscribe(physician => {
    //   this._dataStoreService.setData('medication_including_psychopatric', this.medicationpsychotropicForm.getRawValue());
    // });
  }

  ismedication(opt) {
    this.isMedicationIncludes = opt;
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.prescriptionreasontype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.informationsourcetype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          order: 'description'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.medicationtype + '?filter'
      ),
    ])
      .map((result) => {
        result[2].forEach(type => {
          this.medicationType[type.medicationtypekey] = type.description;
        });
        return {
          prescriptionreasontype: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.description
              })
          ),
          informationsourcetype: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.description
              })
          ),
          medicationtype: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.medicationtypekey
              })
          )
        };
      })
      .share();
    this.prescriptionReasonType$ = source.pluck('prescriptionreasontype');
    this.informationSourceType$ = source.pluck('informationsourcetype');
    this.medicationType$ = source.pluck('medicationtype');
  }

  private add() {
    this.medicationpsychotropic.push(this.medicationpsychotropicForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.medication = this.medicationpsychotropic;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.medicationpsychotropicForm.reset();
    this.modalInt = -1;
    this.isMedicationIncludes = false;
    this.editMode = false;
    this.reportMode = 'add';
    this.medicationpsychotropicForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.medicationpsychotropic[this.modalInt] = this.medicationpsychotropicForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.ismedication(modal.isMedicationIncludes);
    this.editMode = false;
    this.medicationpsychotropicForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.ismedication(modal.isMedicationIncludes);
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.medicationpsychotropicForm.enable();
  }

  private delete(index) {
    this.medicationpsychotropic.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: Medication) {
    this.medicationpsychotropicForm.patchValue(modal);
  }

}
