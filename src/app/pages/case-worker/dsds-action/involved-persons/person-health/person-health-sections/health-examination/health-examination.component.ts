import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { HealthExamination, Health } from '../../../_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'health-examination',
  templateUrl: './health-examination.component.html',
  styleUrls: ['./health-examination.component.scss']
})
export class HealthExaminationComponent implements OnInit {
  healthexamination: HealthExamination[] = [];
  healthexaminationForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  healthExaminationForm: FormGroup;
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  healthProfessionType$: Observable<DropdownModel[]>;
  healthDomainType$: Observable<DropdownModel[]>;
  healthDomainTypes: any = [];
  healthAssessmentTypes: any = [];
  domainTypeKey: string;
  healthAssessmentType$: Observable<DropdownModel[]>;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.loadDropDowns();
    this.editMode = false;
    this.modalInt = -1;
    this.domainTypeKey = '';
    this.reportMode = 'add';
    this.healthexaminationForm = this.formbulider.group({
      healthexamname: '',
      healthassessmenttypekey: '',
      healthdomaintypekey: '',
      assessmentdate: '',
      healthprofessiontypekey: '',
      practitionername: '',
      outcomeresults: '',
      notes: ''
    });

    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.healthExamination) {
      this.healthexamination = this.health.healthExamination;
    }

    // this.healthExaminationForm.valueChanges.subscribe( physician => {
    //   this._dataStoreService.setData('health_examination', this.healthExaminationForm.getRawValue());
    // });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.healthprofessiontype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.healthdomaintype + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.healthassessmenttype + '?filter'
      )
    ])
      .map((result) => {
        result[1].forEach(type => {
          this.healthDomainTypes[type.healthdomaintypeid] = type.description;
        });
        result[2].forEach(type => {
          this.healthAssessmentTypes.push(type);
        });

        return {
          healthprofessiontype: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.description
              })
          ),
          healthdomaintype: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.healthdomaintypeid
              })
          ),
          healthassessmenttype: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description,
                value: res.description
              })
          )
        };
      })
      .share();
    this.healthProfessionType$ = source.pluck('healthprofessiontype');
    this.healthDomainType$ = source.pluck('healthdomaintype');
    this.healthAssessmentType$ = source.pluck('healthassessmenttype');
  }
  setDomainTypeKey(key) {
    this.domainTypeKey = key;
    // this.healthAssessmentTypes = this.healthAssessmentTypes.filter(p => p.healthdomaintypekey === key);
  }

  getHealthAssessments() {
    return this.healthAssessmentTypes.filter(p => p.healthdomaintypekey === this.domainTypeKey);
  }

  private add() {
    this.healthexamination.push(this.healthexaminationForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.healthExamination = this.healthexamination;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.healthexaminationForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.setDomainTypeKey('');
    this.healthexaminationForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.healthexamination[this.modalInt] = this.healthexaminationForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.setDomainTypeKey(modal.domain);
    this.patchForm(modal);
    this.editMode = false;
    this.healthexaminationForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.setDomainTypeKey(modal.domain);
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.healthexaminationForm.enable();
  }

  private delete(index) {
    this.healthexamination.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: HealthExamination) {
    this.healthexaminationForm.patchValue(modal);
  }


}
