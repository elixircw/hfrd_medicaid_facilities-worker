import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../../../@core/services';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { Under5Info, Health } from '../../../_entities/involvedperson.data.model';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'under-5years-cw',
  templateUrl: './under-5years-cw.component.html',
  styleUrls: ['./under-5years-cw.component.scss']
})
export class Under5yearsCwComponent implements OnInit {

  under5yearsInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  under5Infocw: Under5Info[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  prenatalCareDropdownItems$: Observable<DropdownModel[]>;
  gestationDropdownItems$: Observable<DropdownModel[]>;
  deliveryTypeDropdownItems$: Observable<DropdownModel[]>;
  complicationTypeDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {

  }


  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDown();
    this.under5yearsInfoForm = this.formbulider.group({
        parentcare: '',
        gestation: '',
        deliveryType: '',
        comments: '',
        hospitalcomments: '',
        complications: '',
        childbornin: '',
        speciality: '',
        address1: '',
        address2: '',
        city: '',
        state: '',
        county: '',
        zip: '',
        phone: '',
        email: ''
    });
  }

  private resetForm() {

    this.under5yearsInfoForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.under5yearsInfoForm.enable();

  }


  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.under5yearsInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.under5yearsInfoForm.enable();
  }

  private delete(index) {
    this.under5Infocw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: Under5Info) {
    this.under5yearsInfoForm.patchValue(modal);

    // if (modal.startdate) {
    //   this.examinationInfoForm.patchValue({startdate: new Date(modal.startdate)});
    // }
    // if (modal.enddate) {
    //   this.examinationInfoForm.patchValue({enddate: new Date(modal.enddate)});
    // }
  }

  private add() {
    this.under5Infocw.push(this.under5yearsInfoForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.personUnder5Info = this.under5Infocw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.under5Infocw[this.modalInt] = this.under5yearsInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDown() {
    const source = forkJoin([
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { activeflag: 1, 'picklist_type_id': '320' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { activeflag: 1, 'picklist_type_id': '90' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { activeflag: 1, 'picklist_type_id': '319' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { activeflag: 1, 'picklist_type_id': '53' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
          {
            method: 'get',
            nolimit: true
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
          {
            method: 'get',
            nolimit: true,
            order: 'countyname asc'
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
        )
    ])
        .map((result) => {
            return {
              prenatalCareList: result[0].map(
                    (res) =>
                        new DropdownModel({
                            text: res.description_tx,
                            value: res.picklist_value_cd
                        })
                ),
                gestationList: result[1].map(
                    (res) =>
                        new DropdownModel({
                          text: res.description_tx,
                          value: res.picklist_value_cd
                        })
                ),
                deliveryTypeList: result[2].map(
                    (res) =>
                        new DropdownModel({
                          text: res.description_tx,
                          value: res.picklist_value_cd
                        })
                ),
                complicationTypeList: result[3].map(
                    (res) =>
                        new DropdownModel({
                          text: res.description_tx,
                          value: res.picklist_value_cd
                        })
                ),
                states: result[4].map(
                  (res) =>
                    new DropdownModel({
                      text: res.statename,
                      value: res.stateabbr
                    })
                ),
                counties: result[5].map(
                  (res) =>
                    new DropdownModel({
                      text: res.countyname,
                      value: res.countyname
                    })
                )
            };
        })
        .share();
    this.prenatalCareDropdownItems$ = source.pluck('prenatalCareList');
    this.gestationDropdownItems$ = source.pluck('gestationList');
    this.deliveryTypeDropdownItems$ = source.pluck('deliveryTypeList');
    this.complicationTypeDropdownItems$ = source.pluck('complicationTypeList');
    this.stateDropdownItems$ = source.pluck('states');
    this.countyDropDownItems$ = source.pluck('counties');
}
}
