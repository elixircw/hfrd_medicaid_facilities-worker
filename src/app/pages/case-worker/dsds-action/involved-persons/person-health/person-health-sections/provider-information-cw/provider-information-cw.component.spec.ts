import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderInformationCwComponent } from './provider-information-cw.component';

describe('ProviderInformationCwComponent', () => {
  let component: ProviderInformationCwComponent;
  let fixture: ComponentFixture<ProviderInformationCwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderInformationCwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderInformationCwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
