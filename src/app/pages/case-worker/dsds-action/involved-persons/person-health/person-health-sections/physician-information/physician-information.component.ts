import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService, ValidationService } from '../../../../../../../@core/services';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { Physician } from '../../../_entities/involvedperson.data.model';
import { Health } from '../../../../../../newintake/my-newintake/_entities/newintakeModel';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'physician-information',
  templateUrl: './physician-information.component.html',
  styleUrls: ['./physician-information.component.scss']
})
export class PhysicianInformationComponent implements OnInit {
  @Input()
  healthFormReset$ = new Subject<boolean>();
  minDate = new Date();
  maxDate = new Date();
  physicianForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  physician: Physician[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {

  }

  ngOnInit() {
    this.editMode = false;
    this.modalInt = -1;
    this.reportMode = 'add';
    this.physicianForm = this.formbulider.group({
      'isprimaryphycisian': '',
      'name': '',
      'facility': '',
      'phone': '',
      'email': ['', ValidationService.mailFormat],
      'address1': '',
      'address2': '',
      'city': '',
      'state': '',
      'county': '',
      'zip': '',
      'startdate': '',
      'enddate': '',
    });
    this.healthFormReset$.subscribe((res) => {
      if (res === true) {
        this.initializePhysician();
      }
    });

    this.initializePhysician();

    this.loadDropDowns();
  }

  initializePhysician() {
    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.physician) {
      this.physician = this.health.physician;
    }

    this.resetForm();
  }


  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();
    // this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
  }

  loadCounty() {
    const state = this.physicianForm.get('state').value;
    const source = this._commonHttpService.getArrayList(
      {
        where: { state: state },
        order: 'countyname asc',
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
    ).map((result) => {
      return {
        counties: result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyname
            })
        )
      };
    }).share();

    this.countyDropDownItems$ = source.pluck('counties');
  }

  private add() {
    this.physician.push(this.physicianForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.physician = this.physician;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private resetForm() {
    this.physicianForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.physicianForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.physician[this.modalInt] = this.physicianForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.physicianForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.physicianForm.enable();
  }

  private delete(index) {
    this.physician.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  startDateChanged() {
    this.physicianForm.patchValue({enddate : ''});
    const empForm = this.physicianForm.getRawValue();
    this.maxDate = new Date(empForm.startdate);
  }
  endDateChanged() {
    this.physicianForm.patchValue({startdate : ''});
    const empForm = this.physicianForm.getRawValue();
    this.minDate = new Date(empForm.enddate);
  }
  private patchForm(modal: Physician) {
    this.physicianForm.patchValue(modal);

    if(modal.startdate){
      this.physicianForm.patchValue({startdate: new Date(modal.startdate)});
    }
    if(modal.enddate){
      this.physicianForm.patchValue({enddate: new Date(modal.enddate)});
    }
  }

}
