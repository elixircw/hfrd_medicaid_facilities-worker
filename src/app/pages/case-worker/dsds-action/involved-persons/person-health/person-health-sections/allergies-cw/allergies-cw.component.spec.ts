import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllergiesCwComponent } from './allergies-cw.component';

describe('AllergiesCwComponent', () => {
  let component: AllergiesCwComponent;
  let fixture: ComponentFixture<AllergiesCwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllergiesCwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllergiesCwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
