import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../../@core/services';
import { FamilyHistory, Health, InvolvedPerson } from '../../../_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../_entities/involvedPersons.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { DropdownModel } from '../../../../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { CASE_STORE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'family-history-cw',
  templateUrl: './family-history-cw.component.html',
  styleUrls: ['./family-history-cw.component.scss']
})
export class FamilyHistoryCwComponent implements OnInit {
  familyHistoryForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  familyHistorycw: FamilyHistory[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  relationshipTypeDropdownItems$: Observable<DropdownModel[]>;
  deathCauseTypeDropdownItems$: Observable<DropdownModel[]>;
  infoclienttypekeyDropdownItems$: Observable<DropdownModel[]>;
  id: string;
  personRoles = ([] = []);

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute) {
      this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
  }

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDowns();
    this.getInvolvedPerson();
    this.initForm();
  }

  initForm() {
    this.familyHistoryForm = this._formBuilder.group({
      infoclienttypekey: '',
      relationshiptypekey: '',
      majorhealthproblems: '',
      deathcausetypekey: '',
      comments: ''
    });
  }

  private resetForm() {
    this.familyHistoryForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.familyHistoryForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.familyHistoryForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.familyHistoryForm.enable();
  }

  private delete(index) {
    this.familyHistorycw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: FamilyHistory) {
    this.familyHistoryForm.patchValue(modal);
  }

  private add() {
    this.familyHistorycw.push(this.familyHistoryForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.familyHistory = this.familyHistorycw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.familyHistorycw[this.modalInt] = this.familyHistoryForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { activeflag: 1 }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.RelationshipTypesUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '34', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        relationshipTypeList: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.relationshiptypekey
            })
        ),
        deathCauseTypeList: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        )
      };
    })
      .share();
    this.relationshipTypeDropdownItems$ = source.pluck('relationshipTypeList');
    this.deathCauseTypeDropdownItems$ = source.pluck('deathCauseTypeList');
  }

  private getInvolvedPerson() {
    Observable.forkJoin([
      this._commonHttpService
        .getPagedArrayList(
          {
            where: { intakeserviceid: this.id },
            page: 1,
            limit: 20,
            method: 'get'
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.InvolvedPersonUrl + '?filter'
        )]).subscribe((result) => {
        if (result[0].data) {
          this.personRoles = [];
          result[0].data.map((list: InvolvedPerson) => {
            return this.personRoles.push({
              intakeservicerequestactorid: list.intakeservicerequestactorid,
              displayname: list.firstname + ' ' + list.lastname,
              personname: list.firstname + ' ' + list.lastname,
              role: list.roles
            });
          });
        }

      });
  }
}
