import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'person-health-section-log',
  templateUrl: './person-health-section-log.component.html',
  styleUrls: ['./person-health-section-log.component.scss']
})
export class PersonHealthSectionLogComponent implements OnInit {

    log = null;

    constructor() {
    }

    ngOnInit() {
        this.log = {
            createdDate: '',
            createdWorkerID: '',
            createdWorkerName: '',
            updatedDate: '',
            updatedWorkerID: '',
            updatedWorkerName: '',
            completedDate: '',
            completedWorkerID : '',
            completedWorkerName : ''
        }
    }

}
