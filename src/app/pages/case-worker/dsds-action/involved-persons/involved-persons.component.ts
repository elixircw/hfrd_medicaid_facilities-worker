import { HttpHeaders } from '@angular/common/http';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { NgxfUploaderService } from 'ngxf-uploader';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Rx';

import { environment } from '../../../../../environments/environment';
import { ControlUtils } from '../../../../@core/common/control-utils';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { CheckboxModel, DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { DataStoreService, SessionStorageService, CommonDropdownsService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { HttpService } from '../../../../@core/services/http.service';
import { ValidationService } from '../../../../@core/services/validation.service';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker/case-worker-url.config';
import { Assessments } from '../../_entities/caseworker.data.model';
import { DSDSConstants } from '../dsds-action.constants';
import {
    AssessmentBlob,
    Health,
    IntakeServiceRequestActor,
    InvolvedPerson,
    PersonBasicDetail,
    PersonData,
    Persons,
    UserProfileImage,
    ContactPerson,
    People,
    Guardian,
    PersonSupport,
    PersonPayee,
    PersonPayType,
} from './_entities/involvedperson.data.model';
import { AppConstants } from '../../../../@core/common/constants';
import { IntakeUtils } from '../../../_utils/intake-utils.service';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { DsdsService } from '../_services/dsds.service';
import { GUARDIANSHIP } from './guardianship/_entities/guardianship.const';
import { GuardianshipSearch } from './guardianship/_entities/guardianship.modal';
import * as _ from 'lodash';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { LessThanEqualValidator } from 'ng4-validators/src/app/less-than-equal/directive';
import { UniqueSelectionDispatcher } from '@angular/cdk/collections';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';


declare var $: any;
declare var Formio: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'involved-persons',
    templateUrl: './involved-persons.component.html',
    styleUrls: ['./involved-persons.component.scss']
})
export class InvolvedPersonsComponent
    implements OnInit, AfterViewInit, AfterViewChecked {
    @Input()
    healthFormReset$ = new Subject<boolean>();
    @Input()
    workFormReset$ = new Subject<boolean>();
    startAssessment$: Observable<Assessments[]>;
    totalRecords$: Observable<number>;
    guardianSearch$ = new Subject<GuardianshipSearch>();
    id: string;
    daNumber: string;
    investigationId: string;
    addEditLabel: string;
    finalImage: File;
    userProfile: string;
    editImagesShow: string;
    addresstypeLabel: string;
    guardianSearchStoreId: string;
    reviewStatus: string;
    isPersonNew = true;
    maxDate = new Date();
    imageChangedEvent: File;
    croppedImage: File;
    isImageHide: boolean;
    errorValidateAddress = false;
    beofreImageCropeHide = false;
    afterImageCropeHide = false;
    isDefaultPhoto = true;
    isImageLoadFailed = false;
    editImage = true;
    isFormalSupportDes: boolean;
    involvedPersonFormGroup: FormGroup;
    addAliasForm: FormGroup;
    personAddressForm: FormGroup;
    personPhoneForm: FormGroup;
    personEmailForm: FormGroup;
    formalSupportForm: FormGroup;
    informalForm: FormGroup;
    representativegroup: FormGroup;
    addedPersons: Persons[] = [];
    inFormalSuport: PersonSupport[] = [];
    involvedPreson: Persons = new Persons();
    representative: PersonPayType[] = [];
    involevedPerson: InvolvedPerson = new InvolvedPerson();
    addEducation: PersonBasicDetail;
    addHealth: any = {};
    token: AppUser;
    userProfilePicture: UserProfileImage;
    personAddressInput = [];
    copypersonAddressInput = [];
    phoneNumber = ([] = []);
    emailID = ([] = []);
    addressAnalysis = [];
    personRole: IntakeServiceRequestActor[] = [];
    progress: { percentage: number } = { percentage: 0 };
    selectedPersonDetails: PersonData;
    selectedPerson: InvolvedPerson;
    genderDropdownItems$: Observable<DropdownModel[]>;
    stateDropdownItems$: Observable<DropdownModel[]>;
    roleDropdownItems$: DropdownModel[];
    ethinicityDropdownItems$: Observable<DropdownModel[]>;
    livingArrangementDropdownItems$: Observable<DropdownModel[]>;
    primaryLanguageDropdownItems$: Observable<DropdownModel[]>;
    relationShipToRADropdownItems$: DropdownModel[];
    relationShipToRADropdownItems: DropdownModel[] = [];
    maritalDropdownItems$: Observable<DropdownModel[]>;
    religionDropdownItems$: Observable<DropdownModel[]>;
    racetypeDropdownItems$: Observable<DropdownModel[]>;
    addresstypeDropdownItems$: Observable<DropdownModel[]>;
    phoneTypeDropdownItems$: Observable<DropdownModel[]>;
    emailTypeDropdownItems$: Observable<DropdownModel[]>;
    phoneNumber$: Observable<Array<any>>;
    emailID$: Observable<Array<any>>;
    involevedPerson$: Observable<InvolvedPerson[]>;
    involvedUnkPerson: any[];
    addEducationSubject$ = new Subject<PersonBasicDetail>();
    addHealthSubject$ = new Subject<PersonBasicDetail>();
    addEducationOutputSubject$ = new Subject<PersonBasicDetail>();
    educationFormReset$ = new Subject<boolean>();
    isDjs = false;
    currentdate = new Date();
    assessmentDetail: AssessmentBlob;
    isSurvey = false;
    phoneTypeDescription: string[] = [];
    cjamsUserName: string[] = [];
    livingSituationTypes$: Observable<DropdownModel[]>;
    licensedfacilityType$: Observable<DropdownModel[]>;
    languageTypes$: Observable<DropdownModel[]>;
    contactPersonBasicDetails: PersonBasicDetail = new PersonBasicDetail();
    isAS: boolean;
    isCW: boolean;
    isContactSerach: boolean;
    isHouseholdActive = true;
    guardianships: Guardian = new Guardian();
    contactPersonInfo: ContactPerson[] = [];
    personPayeeDetails: PersonPayee[] = [];
    asLanguageTypes$: Observable<DropdownModel[]>;
    countyDropDownItems$: Observable<DropdownModel[]>;
    alienStatusDropDownItems$: Observable<DropdownModel[]>;
    emergencyContactPersonInfo$ = new Subject<ContactPerson>();
    serachPersonProfileInfo$ = new Subject<boolean>();
    isEmergencyContact: boolean;
    isChild: boolean;
    agency: string;
    da_typeid: string;
    da_type: string;
    selectedPersonsage: number;
    isRCPage: boolean;
    requiredTab: boolean;
    dangerSelf: boolean;
    dangerWorker: boolean;
    dangerApperance: boolean;
    dangerIll: boolean;
    dangerAddress: boolean;
    mandatoryCountyRequired: boolean;
    selectedUnkPerson: any;
    isDjsYouth: boolean;
    selectedDob: Date;
    dodPresent: Date;
    youthAge: string;
    isalleged: boolean;
    showPersonSearch: boolean;
    ROLE_ITREATION_COUNT = 0;
    RELATION_TO_RA_COUNT = 0;
    dobRequired = true;
    showDobRequired = true;
    isServiceCase: string;
    guardianshipTabReset$ = new Subject<boolean>();
    isrole: string[] = [];
    guardianPersonBtn: boolean;
    inFormalIndex: number;
    representativeindex: number;
    showGuardianship$ = new Subject<boolean>();
    // D-07962 Start
    changeRelationShipOnChild: boolean;
    // D-07962 End
    // D-07178 Start
    teamtypekey = '';
    // D-07178 End
    enableSafeHaven: boolean;
    enableSEN: boolean;
    substanceClass$: Observable<DropdownModel[]>;
    babySubstanceClass$: Observable<DropdownModel[]>;
    substances = [];
    isSubstance: boolean;
    selectedState: 'MD';
    isAdoptionCase = false;
    involvedPersonList: InvolvedPerson[];
    representativePayeeType$: Observable<DropdownModel[]>;
    representativePerson: boolean;
    representativeDetails$ = new Subject<PersonPayType>();
    representativeName: string[] = [];
    isReportAdult: boolean;
    repPayContactPersonTitle: string;
    involvedrepresentative: PersonPayType[] = [];
    isSaveBtnDisabled: boolean;
    showQualifiedAlienQuestionValue = false;
    repPayeeType$: Observable<DropdownModel[]>;
    county: string;
    cjamsUserList$: Observable<DropdownModel[]>;
    entityList$: Observable<DropdownModel[]>;
    providerName: string[] = [];
    repPayTyes: string[] = [];
    // involvedrepresentative: ContactPerson[] = [];
    // edit person address start
    isAddressUpdate: boolean;
    selectedPersonAddressIndex: number;
    endDateReason = false;
    isViewCase = false;
    personNameSuffix$: any;
    storeMaritalStatus = '';
    isAddressReq = false;
    isCheckedCollateral = true;
    isnoCopyAddress = true;
    changeView = 'card'; // for DJS
    activepersondetails;
    /* Service case - Program Assignment */

    addAssignmentForm: FormGroup;
    programArea: any;
    programSubArea: any;
    reasonForEndList: any;
    personid: any;
    programAsssignList: any[];
    minDate: Date;
    personprogramid: any;
    isAddAssignment: boolean;
    programPersonDetails = {};
    isNotPersonYouth = false;
    outhomestatus: boolean = false;
    outhomemessage: string = '';
    currentaddressDropdownItems$: Observable<DropdownModel[]>;
    constructor(
        private _service: GenericService<InvolvedPerson>,
        private route: ActivatedRoute,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private _formBuilder: FormBuilder,
        private _uploadService: NgxfUploaderService,
        private cdRef: ChangeDetectorRef,
        private _dataStoreService: DataStoreService,
        private _http: HttpService,
        private storage: SessionStorageService,
        private _intakeUtils: IntakeUtils,
        private _dsdsService: DsdsService,
        private _dropdownService: CommonDropdownsService
    ) {
        this.route.data.subscribe(response => {
            const pageSource = response.pageSource;
            this.isRCPage = pageSource === AppConstants.PAGES.RESTITUTION_COORDINATOR;
        });
    }

    ngOnInit() {
        this.inFormalIndex = -1;
        this.representativeindex = -1;
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        this.da_typeid = this._dataStoreService.getData('da_typeid');
        this.da_type = this._dataStoreService.getData('da_type');
        this.token = this._authService.getCurrentUser();
        // D-07178 Start
        this.teamtypekey = this.token.user.userprofile.teamtypekey;
        // D-07178 End
        this.selectedState = 'MD';
        this.mandatoryCountyRequired = true;
        this.agency = this._authService.getAgencyName();
        if (!this.isRCPage) {
            this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
            this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        } else {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('.ccuapprovebutton')).prop('disabled', false);
            this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        }
        this.isDjs = this._authService.isDJS();
        this.isCW = this._authService.isCW();
        const caseType = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
        if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
            this.isAdoptionCase = true;
            this.getPersonInadoptionCase();
        } else if (CASE_TYPE_CONSTANTS.SERVICE_CASE) {
            this.getInvolvedPerson();
        } else {
            this.getInvolvedPerson();
            this.getInvolvedUnkPerson();
        }
        this.loadDropDown();
        this.getProviderList();
        this.loadSubstance();
        this.getCommonDropdowns();
        if (this.token.user.userprofile.teamtypekey === 'AS') {
            this.isAS = true;
        }
        this.initiateFormGroup();
        this.dodChange();
        this.getAssessmentDetail();
        this.conditionValidation();
        this.initAddAssignmentForm(); /* Service case - Program Assignment */
        this.addEducationSubject$.subscribe(education => {
            this.addEducation = education;
        });
        this.addHealthSubject$.subscribe(health => {
            this.addHealth = health;
        });
        this._dataStoreService.currentStore.subscribe(store => {
            // const health = DSDSConstants.DSDS.PersonsInvolved.Health;
            if (store[GUARDIANSHIP.Attorney]) {
                this.guardianships.ATTY = store[GUARDIANSHIP.Attorney];
            }
            if (store[GUARDIANSHIP.GuardianPerson]) {
                this.guardianships.GOP = store[GUARDIANSHIP.GuardianPerson];
            }
            if (store[GUARDIANSHIP.GuardianProperty]) {
                this.guardianships.GOPT = store[GUARDIANSHIP.GuardianProperty];
            }
            if (store[GUARDIANSHIP.Worker]) {
                this.guardianships.WRK = store[GUARDIANSHIP.Worker];
            }
            if (store[GUARDIANSHIP.HSSClinetID]) {
                this.guardianships.hhsdclient = store[GUARDIANSHIP.HSSClinetID];
            }
            if (store[GUARDIANSHIP.CodeStatus]) {
                this.guardianships.codestatus = store[GUARDIANSHIP.CodeStatus];
            }
            if (store[GUARDIANSHIP.Funeral]) {
                this.guardianships.funeral = store[GUARDIANSHIP.Funeral];
            }
        });
        this.serachPersonProfileInfo$.subscribe((item) => {
            if (item) {
                this.addEditLabel = 'Edit';
                (<any>$('#intake-addperson')).modal('show');
                this.isEmergencyContact = false;
            }
        });
        this.emergencyContactPersonInfo$.subscribe((item) => {
            (<any>$('#intake-findperson')).modal('hide');
            (<any>$('#intake-addperson')).modal('show');
            this.contactPersonInfo.push(item);
            this.isEmergencyContact = false;
            this.showPersonSearch = false;
        });
        this.guardianSearch$.subscribe((item) => {
            if (item.personsearch) {
                (<any>$('#intake-addperson')).modal('hide');
                this.isEmergencyContact = true;
                this.showPersonSearch = true;
                this.guardianPersonBtn = true;
                this.guardianSearchStoreId = item.storeid;
                (<any>$('#intake-findperson')).modal('show');
            }
        });
        this.showGuardianship$.subscribe((item) => {
            if (item) {
                (<any>$('#intake-addperson')).modal('show');
                this.isEmergencyContact = false;
                this.showPersonSearch = false;
                this.guardianPersonBtn = false;
                (<any>$('#intake-findperson')).modal('hide');
            }
        });
        this.representativeDetails$.subscribe((item) => {
            (<any>$('#intake-findperson')).modal('hide');
            this.representative.push(item);
            this.representative.map((res) => {
                this.representativeName[res.personid] = res.firstname + ' ' + res.lastname;
            });
            this.representativegroup.controls['personrepresentativepersonid'].patchValue(item.personid);
            this.representativegroup.controls['personrepresentativepersonid'].disable();
            this.isEmergencyContact = false;
            this.representativePerson = false;
            this.showPersonSearch = false;
            (<any>$('#intake-addperson')).modal('show');
        });
        if (this.isDjs) {
            this.personNameSuffix$ = this._dropdownService.getListByTableID('134');
            this.isDjsYouth = false;
            this.changeView = this._authService.getCurrentUser().user.userprofile.viewpreference ? this._authService.getCurrentUser().user.userprofile.viewpreference : 'card';
        } else {
            this.isDjsYouth = true;
        }
        this.dobChange();
        this.dodChange();
        // edit person address start
        this.isAddressUpdate = false;
        this.selectedPersonAddressIndex = 0;
        this.mandatoryCountyRequired = true;
        // edit person address end
        this.involvedPersonFormGroup.get('primarylanguage').valueChanges.subscribe((data) => {
            if (data === 'Other' && this.isAS) {
                this.involvedPersonFormGroup.get('otherprimarylanguage').setValidators([Validators.required]);
                this.involvedPersonFormGroup.get('otherprimarylanguage').updateValueAndValidity();
            } else {
                this.involvedPersonFormGroup.get('otherprimarylanguage').setValidators([]);
                this.involvedPersonFormGroup.get('otherprimarylanguage').updateValueAndValidity();
            }
        });

        this.involvedPersonFormGroup.get('religiontypekey').valueChanges.subscribe((data) => {
            if (data === 'OTH' && this.isAS) {
                this.involvedPersonFormGroup.get('otherreligion').setValidators([Validators.required]);
                this.involvedPersonFormGroup.get('otherreligion').updateValueAndValidity();
            } else {
                this.involvedPersonFormGroup.get('otherreligion').setValidators([]);
                this.involvedPersonFormGroup.get('otherreligion').updateValueAndValidity();
            }
        });
        this.involvedPersonFormGroup.controls['citizenalenageflag'].valueChanges.subscribe(data => {
            console.log('Value is ' + data);
            if (data === '0') {
                if (this.showQualifiedAlienQuestion()) {
                    this.showQualifiedAlienQuestionValue = true;
                } else {
                    this.showQualifiedAlienQuestionValue = false;
                }
            } else {
                this.showQualifiedAlienQuestionValue = false;
            }
        });
        this.getCjamsUserList();
    }

    /* Service case - Program Assignment */

    loadProgramAssignForm() {
        this.isAddAssignment = true;
        this.addAssignmentForm.reset();
        this.loadProgramAreaDropdowns(null);
        this.loadEndReasonDropDown();
    }

    initAddAssignmentForm() {
        this.addAssignmentForm = this._formBuilder.group(
            {
                programkey: ['', Validators.required],
                subprogramkey: [''],
                ifpsatriskflag: [''],
                startdate: ['', Validators.required],
                enddate: [''],
                endreasonkey: ['']
            }
        );
    }

    loadEndReasonDropDown() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { referencetypeid: 138, teamtypekey: 'CW' },
                    method: 'get',
                    nolimit: true
                }),
                'referencetype/gettypes?filter'
            )
            .subscribe((result) => {
                console.log(result);
                if (result && Array.isArray(result) && result.length) {
                    this.reasonForEndList = result;
                }
            });
    }

    viewChanged() {
        this._commonHttpService.create({
            viewpreference: this.changeView
        }, NewUrlConfig.EndPoint.Intake.ViewPreference).subscribe(res => console.log(this._authService.getCurrentUser().user.userprofile.viewpreference));
    }

    getPersonData(person) {
        this.activepersondetails = person;
        this.personid = person.personid;
        this.getProgramAssignmentList();
    }

    getProgramAssignmentList() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { objectid: this.id, personid: this.personid },
                    method: 'get',
                    nolimit: true
                }),
                'Personprogramareas/getpersonprogramarea?filter'
            )
            .subscribe((result) => {
                if (result && Array.isArray(result) && result.length) {
                    this.programAsssignList = result[0];
                    this.programPersonDetails = result[0].persondetails;
                }
            });
    }

    editProgramAssigment(person) {
        this.loadProgramAreaDropdowns(null); // person.programkey
        this.loadEndReasonDropDown();
        this.isAddAssignment = false;
        this.minDate = new Date(person.startdate);
        this.personprogramid = person.personprogramid;
        this.addAssignmentForm.patchValue(
            {
                startdate: person.startdate,
                enddate: person.enddate,
                programkey: person.programkey,
                subprogramkey: person.subprogramkey,
                endreasonkey: person.endreasonkey,
                ifpsatriskflag: (person.ifpsatriskflag) ? 'Yes' : 'No'
            }
        );
    }

    saveProgramAssignment() {
        const model = {
            personid: this.personid,
            personprogramid: (this.personprogramid && !this.isAddAssignment) ? this.personprogramid : null,
            startdate: this.addAssignmentForm.value.startdate,
            enddate: this.addAssignmentForm.value.enddate,
            programkey: this.addAssignmentForm.value.programkey,
            subprogramkey: this.addAssignmentForm.value.subprogramkey,
            endreasonkey: this.addAssignmentForm.value.endreasonkey,
            clientmergeid: null,
            ifpsatriskflag: (this.addAssignmentForm.value.ifpsatriskflag === 'Yes') ? 1 : 0,
            objecttypekey: 'Servicecase',
            objectid: this.id
        };

        this._commonHttpService
            .create(model,
                'Personprogramareas/addupdate'
            )
            .subscribe((result) => {
                const status = (this.isAddAssignment) ? 'added' : 'updated';
                (<any>$('#add-program-assignment')).modal('hide');
                this._alertService.success('Program assignment ' + status + ' successfully!');
                this.getProgramAssignmentList();
                (<any>$('#program-assignment-service-case')).modal('show');
            });
    }


    private loadProgramAreaDropdowns(servicerequesttypekey: string) {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { servicerequestsubtypekey: servicerequesttypekey },
                    method: 'get',
                    nolimit: true
                }),
                'agencyprogramarea/list?filter'
            )
            .subscribe((result) => {
                if (result && Array.isArray(result) && result.length) {
                    this.programArea = result[0].programarea;
                    this.programSubArea = result[0].subprogram;
                }
            });
    }

    /* Service case - Program Assignment */

    navigateToPerson(personid: string) {
        this._intakeUtils.redirectToPerson(personid);
    }

    onDob(event) {
        console.log('event...', event);
        this.selectedDob = event.target.value ? new Date(event.target.value) : null;
        this.getAgeInYearsAndMonth(this.currentdate, this.selectedDob);
    }
    // DJS-018 - Victim's Date of birth may not be known by the worker at the point of adding a person
    dobUnknown(event) {
        if (event.checked) {
            this.isDobRequired(null);
        } else {
            this.dobRequired = true;
            this.involvedPersonFormGroup.get('Dob').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('Dob').updateValueAndValidity();
        }
    }
    isDobRequired(role) {
        if (role && role === 'Youth') {
            this.dobRequired = true;
            this.showDobRequired = false;
            this.involvedPersonFormGroup.get('Dob').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('Dob').updateValueAndValidity();
        } else {
            this.dobRequired = false;
            this.showDobRequired = true;
            this.involvedPersonFormGroup.get('Dob').clearValidators();
            this.involvedPersonFormGroup.get('Dob').updateValueAndValidity();
        }
    }
    onDod(event) {
        console.log('event...', event.target.value);
        this.dodPresent = new Date(event.target.value);
        this.selectedDob = new Date(this.involvedPersonFormGroup.get('Dob').value);
        this.getAgeInYearsAndMonth(this.dodPresent, this.selectedDob);
    }


    dobChange() {
        this.involvedPersonFormGroup.controls['Dob'].valueChanges.subscribe((res) => {
            this.selectedPersonsage = this.calculateAge(res ? res : null);
            this.involvedPersonFormGroup.get('safehavenbabyflag').setValue(false);
            if (res) {
                if (this.agency === 'DJS') {
                    this.selectedDob = res ? new Date(res) : null;
                    this.getAgeInYearsAndMonth(this.currentdate, this.selectedDob);
                }
            }
        });

    }
    personTypeChange(modal) {
        if (modal === 'PRSN') {
            this.representativegroup.controls['personrepresentativepersonid'].disable();
        } else {
            this.representativegroup.controls['personrepresentativepersonid'].enable();
        }
        this.representativegroup.controls['personrepresentativepersonid'].reset();
    }

    private getAgeInYearsAndMonth(currentDate, youthDob) {
        // const timeDiff = currentDate - youthDob;
        // const youthAge = new Date(timeDiff); // miliseconds from epoch

        // const currentMonth = currentDate.getMonth();
        // const youthMonth = youthDob.getMonth();

        // const monthDiff = currentMonth - youthMonth;

        // let months = (monthDiff >= 0) ? (monthDiff) : (monthDiff + 12);

        // months = (currentDate.getDate() < youthDob.getDate()) ? ((months - 1)) : months;

        // let years = Math.abs(youthAge.getUTCFullYear() - 1970);
        // if (months < 0) {
        //     months = 11;
        //     years = years - 1;
        // }
        // this.youthAge = years.toString() + ' Years ' + ((months > 0) ? months + ' Months' : '');
        // return years.toString() + ' Years ' + ((months > 0) ? months + ' Months' : '');
        if (youthDob) {
            const years = moment().diff(youthDob, 'years', false);
            const totalMonths = moment().diff(youthDob, 'months', false);
            const months = totalMonths - (years * 12);
            console.log(years, (years * 12), totalMonths, months);
            this.youthAge = `${years} Years ${months} month(s)`;
        }
    }


    getAssessmentDetail() {
        const safeCAssessmentRequest = this._service
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        objectid: this.id,
                        assessmenttemplatename: 'readyBy21exitsurvey'
                    },
                    page: 1,
                    limit: 10
                },
                'admin/assessment/getassessment?filter'
            )
            .subscribe(item => {
                if (item && item.length) {
                    this.assessmentDetail = <AssessmentBlob>item[0];
                    this.isSurvey = true;
                } else {
                    this.isSurvey = false;
                }
            });
    }
    openAssessment() {
        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(
            document.getElementById('assessmentForm'),
            environment.formBuilderHost +
            `/form/${this.assessmentDetail.externaltemplateid}/submission/${
            this.assessmentDetail.submissionid
            }`,
            {
                readOnly: true
            }
        ).then(function (submission) {
            /// (<any>$('#iframe-popup')).modal('show');
            submission.on('render', formData => {
                setTimeout(function () {
                    // $('#assessment-popup').scrollTop(0);
                }, 200);
            });
        });
    }
    conditionValidation() {
        this.involvedPersonFormGroup.controls[
            'dateofdeath'
        ].valueChanges.subscribe(res => {
            if (this.involvedPersonFormGroup.controls['Dob'].value) {
                if (
                    res !== null &&
                    res < this.involvedPersonFormGroup.controls['Dob'].value
                ) {
                    this._alertService.error(
                        'DOD should be greater than Date of birth'
                    );
                    return false;
                } else if (res !== null && res > this.currentdate) {
                    this._alertService.error(
                        'DOB should not be greater than current date'
                    );
                    return false;
                }
                return true;
            }
        });
    }
    changeLiving() {
        this.involvedPersonFormGroup.controls['licensedfacilitykey'].reset();
        this.involvedPersonFormGroup.controls['otherlicensedfacility'].reset();
    }

    getInvolvedPerson() {
        let inputRequest: Object;
        if (this._dsdsService.isServiceCase()) {
            inputRequest = {
                objectid: this.id,
                objecttypekey: 'servicecase'
            };
        } else {
            inputRequest = {
                intakeserviceid: this.id
            };
        }
        this.involevedPerson$ = this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: inputRequest
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PersonList + '?filter'
            )
            .share()
            .pluck('data');
        if (this.involevedPerson$) {
            this.involevedPerson$.subscribe((data) => {
                this.involvedPersonList = data;
                if (this.isServiceCase) {
                    this.filterPerson(1);
                }
                if (data && data.length) {
                    data.map((item) => {
                        if (item.rolename !== 'RA') {
                            const person = Object.assign({
                                firstname: item.firstname,
                                lastname: item.lastname,
                                personid: item.personid
                            } as PersonPayType);
                            this.involvedrepresentative.push(person);
                        }
                    });
                    this.representativePersnNames(this.involvedrepresentative);
                    this.isalleged = data.some(person => person.isalleged === 0);
                    data.map((item) => {
                        if (item.roles && item.roles.length) {
                            return item.roles.map((res) => {
                                // tslint:disable-next-line:max-line-length
                                if (res.intakeservicerequestpersontypekey === 'RC' || res.intakeservicerequestpersontypekey === 'LG' || res.intakeservicerequestpersontypekey === 'CHILD' || res.intakeservicerequestpersontypekey === 'AV' || res.intakeservicerequestpersontypekey === 'BIOCHILD') {
                                    this.isrole[item.personid] = res.intakeservicerequestpersontypekey;
                                } else {
                                    this.isrole[item.personid] = '';
                                }
                            });
                        }
                    });
                }
            });

        }
    }
    getInvolvedUnkPerson() {
        if (this._dsdsService.isServiceCase()) {
            return null;
            // currently inkakejsondata api not available for service case, once we have it, then will implement here accordingly
        } else {
            this._service
                .getArrayList(
                    new PaginationRequest({
                        page: 1,
                        limit: 20,
                        method: 'get',
                        where: { intakeserviceid: this.id }
                    }),
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                        .UnkPersonList + '?filter'
                )
                .subscribe(unkList => {
                    this.involvedUnkPerson = unkList;
                });
        }
    }
    getPersonInadoptionCase() {
        const caseDb = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_DASHBOARD);
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { adoptioncaseid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PersonsInAdoptionCase + '?filter'
            ).subscribe((data) => {
                if (data && data.length) {
                    const response = data[0];
                    const list = (response && response.person) ? response.person : [];
                    this.involevedPerson$ = Observable.of(list);
                    list.map((item) => {
                        const personAddress = (item.address && item.address.length) ? item.address[0] : null;
                        const fullAddress = (personAddress.address) ? personAddress.address : '' +
                            (personAddress.address2) ? personAddress.address2 : '' +
                                (personAddress.city) ? personAddress.city : '' +
                                    (personAddress.state) ? personAddress.state : '';
                        item.fullAddress = fullAddress;
                        item = Object.assign(item, personAddress);
                        if (item.roles && item.roles.length) {
                            return item.roles.map((res) => {
                                // tslint:disable-next-line:max-line-length
                                if (res.intakeservicerequestpersontypekey === 'RC' || res.intakeservicerequestpersontypekey === 'LG' || res.intakeservicerequestpersontypekey === 'CHILD' || res.intakeservicerequestpersontypekey === 'AV' || res.intakeservicerequestpersontypekey === 'BIOCHILD') {
                                    this.isrole[item.personid] = res.intakeservicerequestpersontypekey;
                                } else {
                                    this.isrole[item.personid] = '';
                                }
                            });
                        }
                    });
                }
            });
    }

    editInvolvedUnkPerson(action) {
        this._commonHttpService
            .create(
                {
                    intakeserviceid: this.id,
                    unknownpersonlist: this.involvedUnkPerson
                }
                ,
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .updateUnkPersonList
            )
            .subscribe(unkList => {
                const actions = (action === 'edit') ? 'edited' : 'deleted';
                this._alertService.success('Person' + actions + 'successfully.');
                this.getInvolvedPerson();
                this.getInvolvedUnkPerson();
            });
    }
    // this.personAddressForm.value.endDate
    checkDateRange(personAddressForm) {
        if (personAddressForm.controls.endDate.value) {
            if (personAddressForm.controls.endDate.value < personAddressForm.controls.startDate.value) {
                return { notValid: true };
            }
            return null;
        }
    }

    ngAfterViewInit() {
        // this.personAddressForm
        //     .get('Address2')
        //     .valueChanges.subscribe(result => {
        //         if (result) {
        //             this.personAddressForm.get('address1').clearValidators();
        //             this.personAddressForm
        //                 .get('address1')
        //                 .updateValueAndValidity();
        //         }
        //     });


        this.personAddressForm.get('danger').valueChanges.subscribe(result => {
            if (result === '1') {
                this.dangerAddress = true;
                this.personAddressForm
                    .get('dangerreason')
                    .setValidators([Validators.required]);
                this.personAddressForm
                    .get('dangerreason')
                    .updateValueAndValidity();
            } else {
                this.dangerAddress = false;
                this.personAddressForm.get('dangerreason').clearValidators();
                this.personAddressForm
                    .get('dangerreason')
                    .updateValueAndValidity();
            }
            if (result === '0') {
                this.dangerAddress = false;
                this.personAddressForm.get('dangerreason').disable();
            } else {
                this.personAddressForm.get('dangerreason').enable();
            }
        });
        this.personAddressForm.get('state').valueChanges.subscribe((result) => {
            if (this.isDjs) {
                this.mandatoryCountyRequired = true;
            } else {
                if (result === 'MD') {
                    this.mandatoryCountyRequired = true;
                    this.personAddressForm.get('county').setValidators([Validators.required]);
                    this.personAddressForm.get('county').updateValueAndValidity();
                } else {
                    this.mandatoryCountyRequired = false;
                    this.personAddressForm.get('county').clearValidators();
                    this.personAddressForm.get('county').updateValueAndValidity();
                }
            }
        });
        this.selectedState = 'MD';

        const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
        if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
            this.isViewCase = true;
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({
                'pointer-events': 'none',
                'cursor': 'default',
                'opacity': '0.5',
                'text-decoration': 'none'
            });
            (<any>$('th a')).css({
                'pointer-events': 'none',
                'cursor': 'default',
                'opacity': '0.5',
                'text-decoration': 'none'
            });
            (<any>$('btn-pri')).addClass('.btn-pri.disabled');
            (<any>$('fa')).addClass('disabledView');
        }
        this.mandatoryCountyRequired = true;
    }
    ngAfterViewChecked() {
        this.cdRef.markForCheck();
        this.cdRef.detectChanges();
    }
    fileChangeEvent(file: any) {
        this.beofreImageCropeHide = true;
        this.afterImageCropeHide = true;
        this.imageChangedEvent = file;
        this.isDefaultPhoto = false;
        this.isImageLoadFailed = false;
        this.isImageHide = true;
    }
    imageCropped(file: File) {
        this.progress.percentage = 0;
        this.croppedImage = file;
        const imageBase64 = this.croppedImage;
        const blob = this.dataURItoBlob(imageBase64);
        this.finalImage = new File([blob], 'image.png');
        this.saveImage(this.finalImage);
    }
    imageLoaded() { }
    loadImageFailed() {
        this.isImageLoadFailed = true;
        this._alertService.error('Image failed to upload');
    }
    dataURItoBlob(dataURI) {
        const binary = atob(dataURI.split(',')[1]);
        const array = [];
        for (let i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: 'image/png'
        });
    }
    saveImage(data: any) {
        this._uploadService
            .upload({
                url:
                    AppConfig.baseUrl +
                    '/' +
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment
                        .UploadAttachmentUrl +
                    '?access_token=' +
                    this.token.id +
                    '&srno=userprofile',
                headers: new HttpHeaders()
                    .set('access_token', this.token.id)
                    .set('srno', 'userprofile')
                    .set('ctype', 'file'),
                filesKey: ['file'],
                files: data,
                process: true
            })
            .subscribe(
                response => {
                    if (response.status) {
                        this.progress.percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        this.userProfilePicture = response.data;
                    }
                },
                err => {
                    console.log(err);
                }
            );
    }

    addPhone() {
        const phoneNumber = this.personPhoneForm.get('contactnumber').value;
        const phoneType = this.personPhoneForm.get('contacttype').value;
        // const isMoble = this.personPhoneForm.get('ismobile').value;
        if (phoneNumber && phoneType) {
            this.phoneNumber.push({
                contactnumber: phoneNumber,
                contacttype: phoneType,
                // ismobile: isMoble,
                isactive: 1
            });
            this.phoneNumber$ = Observable.of(this.phoneNumber);
            this.personPhoneForm.reset();
        } else {
            this._alertService.warn('Please fill mandatory fields for Phone');
            ControlUtils.validateAllFormFields(this.personPhoneForm);
        }
    }

    addEmail() {
        const emailID = this.personEmailForm.get('EmailID').value;
        const emailType = this.personEmailForm.get('EmailType').value;
        if (emailID && this.personEmailForm.invalid) {
            this._alertService.warn('Please enter a valid email id');
            return;
        }
        if (emailID && emailType && this.personEmailForm.valid) {
            this.emailID.push({
                mailid: emailID,
                mailtype: emailType,
                isactive: 1
            });
            this.emailID$ = Observable.of(this.emailID);
            this.personEmailForm.reset();
        } else {
            this._alertService.warn('Please fill mandatory fields for Email');
            ControlUtils.validateAllFormFields(this.personEmailForm);
        }
    }
    deletePhone(i: number) {
        this.phoneNumber.splice(i, 1);
        this.phoneNumber$ = Observable.of(this.phoneNumber);
    }

    deleteEmail(i: number) {
        this.emailID.splice(i, 1);
        this.emailID$ = Observable.of(this.emailID);
    }
    changeAddressType(event) {
        this.addresstypeLabel = event.value;
        if (this.isDjs) {
            if (this.addresstypeLabel === 'CUR-Current' || this.addresstypeLabel === 'CUR') {
                this.personAddressForm.get('personaddresssubtypekey').setValidators(Validators.required);
            } else {
                this.personAddressForm.get('personaddresssubtypekey').clearValidators();
            }
            this.personAddressForm.get('personaddresssubtypekey').updateValueAndValidity();
            this.currentaddressDropdownItems$ = this._dropdownService.getListByTableID('152');
        }
    }
    zipcodeChange() {
        if (this.isDjs) {
            const zipcode = this.personAddressForm.get('zipcode').value;
            if (zipcode.length === 5) {
                this._commonHttpService.getArrayList({
                    count: -1,
                    where: {
                        zipcode: zipcode
                    },
                    method: 'get'
                }, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                        .ZIPSearch).subscribe(res => {
                            const zipDetails = res.length ? res[0] : null;
                            if (zipDetails) {
                                this.personAddressForm.get('state').setValue(zipDetails.stateabbr);
                                this.personAddressForm.get('city').setValue(zipDetails.cityname);
                                this.personAddressForm.get('county').setValue(zipDetails.countyname);
                            } else {
                                this.personAddressForm.get('state').reset();
                                this.personAddressForm.get('city').reset();
                                this.personAddressForm.get('county').reset();
                            }
                        });
            } else {
                this.personAddressForm.get('state').reset();
                this.personAddressForm.get('city').reset();
                this.personAddressForm.get('county').reset();
            }
        }
    }
    // { validator: this.checkDateRange }
    addPersonAddress() {
        if (this.personAddressForm.value.addresstype) {
            this.personAddressForm.value.addresstype = this.addresstypeLabel.split('-')[0];
        }
        const address = this.personAddressForm.getRawValue();
        if (this.personAddressForm.value.danger === '' && !this.isDjs) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please check if address is a Known danger or not !');
        } else if (this.personAddressForm.value.danger === 1 && (this.personAddressForm.value.dangerreason == null || this.personAddressForm.value.dangerreason === '')) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill the Reason for address being a Danger!');
        } else if (this.personAddressForm.value.addresstype == null || this.personAddressForm.value.addresstype === '') {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please pick the Address Type field!');
        } else if (this.personAddressForm.value.address1 == null || this.personAddressForm.value.address1 === '') {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter Address Line 1 field!');
        } else if (!address.zipcode) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter Zipcode!');
        } else if (!address.state) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please pick a State!');
        } else if (!address.city) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter City!');
        } else if (this.endDateReason === true && (this.personAddressForm.value.changereason === '' || this.personAddressForm.value.changereason == null)) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill Reason for update field!');
        } else if (this.personAddressForm.value.endDate !== '' && this.personAddressForm.value.endDate < this.personAddressForm.value.startDate) {
            this._alertService.warn('End Date should be greater than Start Date!');
        } else {
            this.personAddressInput.push(this.personAddressForm.getRawValue());
            if (this.personAddressForm.value.startDate !== '' && this.personAddressForm.value.startDate != null) {
                const startDate = this.personAddressForm.value.startDate + '';
                const date1 = moment(new Date(startDate.substr(0, 16)));
                this.personAddressForm.value.startDate = date1.format(
                    'MM/DD/YYYY'
                );
                this.personAddressInput[this.personAddressInput.length - 1].effectivedate = this.personAddressForm.value.startDate;
            } else {
                this.personAddressForm.value.startDate = null;
            }
            if (this.personAddressForm.value.endDate = '' && this.personAddressForm.value.endDate != null) {
                const endDate = this.personAddressForm.value.endDate + '';
                if (!this.isDjs) {
                    const date2 = moment(new Date(endDate.substr(0, 16)));
                    this.personAddressForm.value.endDate = date2.format('MM/DD/YYYY');

                    this.personAddressInput[this.personAddressInput.length - 1].expirationdate = this.personAddressForm.value.endDate;
                }
            } else {
                this.personAddressForm.value.endDate = null;
            }
            this.personAddressInput[this.personAddressInput.length - 1].address = this.personAddressForm.value.address1;
            this.personAddressInput[this.personAddressInput.length - 1].address2 = this.personAddressForm.value.Address2;
            this.personAddressInput[this.personAddressInput.length - 1].addresstypeLabel = this.addresstypeLabel.split('-')[1];
            this.personAddressInput[this.personAddressInput.length - 1].activeflag = 1;
            // this.personAddressInput[this.personAddressInput.length - 1].country = 'MO';
            this.personAddressForm.reset();
            // this.personAddressForm
            //     .get('Address2')
            //     .setValidators(Validators.required);
        }
        // } else {
        //     ControlUtils.validateAllFormFields(this.personAddressForm);
        //     ControlUtils.setFocusOnInvalidFields();
        //     this._alertService.warn('Please fill mandatory fields!');
        // }
    }

    editSelectedPersonAddress() {
        let totalHours;
        let insertedDate;
        let dbEndDate;
        let dbPersonId;
        const address = this.personAddressForm.getRawValue();
        if (this.personAddressInput[this.selectedPersonAddressIndex]['personaddresstypekey']) {
            totalHours = this.calculateHours(this.personAddressInput[this.selectedPersonAddressIndex]['insertedon']);
            insertedDate = this.personAddressInput[this.selectedPersonAddressIndex]['insertedon'];
            dbEndDate = this.personAddressInput[this.selectedPersonAddressIndex]['expirationdate'];
            dbPersonId = this.personAddressInput[this.selectedPersonAddressIndex]['personaddressid'];
        }
        if (this.personAddressForm.value.endDate !== '' && this.personAddressForm.value.startDate !== '') {
            const startDate = this.personAddressForm.value.startDate + '';
            const date1 = moment(new Date(startDate.substr(0, 16)));
            this.personAddressForm.value.startDate = date1.format('MM/DD/YYYY');

            const endDate = this.personAddressForm.value.endDate + '';
            const date2 = moment(new Date(endDate.substr(0, 16)));
            this.personAddressForm.value.endDate = date2.format('MM/DD/YYYY');
        }
        if (this.personAddressForm.value.danger === '' && !this.isDjs) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please check if Address is a Known danger or not!');
            this.isAddressUpdate = true;
        } else if (this.personAddressForm.value.danger === 1 && (this.personAddressForm.value.dangerreason == null || this.personAddressForm.value.dangerreason === '')) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter the Reason for address being a Danger!');
            this.isAddressUpdate = true;
        } else if (this.personAddressForm.value.addresstype == null || this.personAddressForm.value.addresstype === '') {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter Address Line 1 field!');
            this.isAddressUpdate = true;
        } else if (!address.zipcode) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter Zip Code!');
            this.isAddressUpdate = true;
        } else if (!address.state) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please pick a State!');
            this.isAddressUpdate = true;
        } else if (!address.city) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter City!');
            this.isAddressUpdate = true;
        } else if (this.endDateReason === true && (this.personAddressForm.value.changereason === '' || this.personAddressForm.value.changereason == null)) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please enter Reason for Change!');
            this.isAddressUpdate = true;
        } else if (this.personAddressForm.value.endDate !== '' && this.personAddressForm.value.endDate < this.personAddressForm.value.startDate) {
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('End Date should be greater than Start Date!');
            this.isAddressUpdate = true;
        } else {
            this.personAddressInput[this.selectedPersonAddressIndex] = this.personAddressForm.getRawValue();
            if (this.personAddressForm.value.startDate) {
                const startDate = this.personAddressForm.value.startDate + '';
                const date1 = moment(new Date(startDate.substr(0, 16)));
                this.personAddressForm.value.startDate = date1.format('MM/DD/YYYY');
                this.personAddressInput[this.selectedPersonAddressIndex].effectivedate = this.personAddressForm.value.startDate;
            } else {
                this.personAddressForm.value.startDate = null;
            }
            if (this.personAddressForm.value.endDate) {
                const endDate = this.personAddressForm.value.endDate + '';
                const date2 = moment(new Date(endDate.substr(0, 16)));
                this.personAddressForm.value.endDate = date2.format('MM/DD/YYYY');
                this.personAddressInput[this.selectedPersonAddressIndex].expirationdate = this.personAddressForm.value.endDate;

            } else {
                this.personAddressForm.value.endDate = null;
            }

            if (totalHours > 72) {
                this.personAddressInput[this.selectedPersonAddressIndex].disableDeleteBtn = 'yes';
            } else {
                this.personAddressInput[this.selectedPersonAddressIndex].disableDeleteBtn = 'no';
            }
            if (insertedDate !== '') {
                this.personAddressInput[this.selectedPersonAddressIndex].insertedon = insertedDate;
            }
            this.personAddressInput[this.selectedPersonAddressIndex]['expirationdate'] = this.personAddressForm.value.endDate;
            this.personAddressInput[this.selectedPersonAddressIndex]['personaddressid'] = dbPersonId;
            this.personAddressInput[this.selectedPersonAddressIndex].address = this.personAddressForm.value.address1;
            this.personAddressInput[this.selectedPersonAddressIndex].address2 = this.personAddressForm.value.Address2;
            this.personAddressInput[this.selectedPersonAddressIndex].addresstypeLabel = this.addresstypeLabel.split('-')[1];
            this.personAddressInput[this.selectedPersonAddressIndex].addresstype = this.addresstypeLabel.split('-')[0];
            this.personAddressInput[this.selectedPersonAddressIndex].activeflag = 1;
            this.personAddressInput[this.selectedPersonAddressIndex].changereason = this.personAddressForm.value.changereason;
            // this.personAddressInput[this.selectedPersonAddressIndex].country = 'MO';
            // }
            this.personAddressForm.reset();
            // this.personAddressForm.get('Address2').setValidators(Validators.required);
            this.isAddressUpdate = false;
            this.endDateReason = false;
        }
        // } else {
        //     ControlUtils.validateAllFormFields(this.personAddressForm);
        //     ControlUtils.setFocusOnInvalidFields();
        //     this._alertService.warn('Please fill mandatory fields!');
        //     this.isAddressUpdate = true;
        // }
    }

    editPersonAddress(modal, index) {
        if (this.personAddressInput[index]['personaddresstypekey']) {
            this.personAddressInput[index].addresstype = this.personAddressInput[index]['personaddresstypekey'];
            this.personAddressInput[index].Address2 = this.personAddressInput[index]['address2'];
            this.personAddressInput[index].address1 = this.personAddressInput[index]['address'];
            if (this.personAddressInput[index]['effectivedate'] !== null) {
                const date2 = moment(new Date(this.personAddressInput[index]['effectivedate']));
                this.personAddressInput[index].startDate = date2.format('MM/DD/YYYY');
            } else {
                this.personAddressInput[index].startDate = null;
            }
            if (this.personAddressInput[index]['expirationdate'] !== null) {
                const date1 = moment(new Date(this.personAddressInput[index]['expirationdate']));
                this.personAddressInput[index].endDate = date1.format('MM/DD/YYYY');
            } else {
                this.personAddressInput[index].endDate = null;
            }
        }
        let endDate;
        if (this.personAddressInput[index].endDate !== null && this.personAddressInput[index].endDate !== '') {
            endDate = this.personAddressInput[index].endDate.split('/');
        }

        let startDate;
        if (this.personAddressInput[index].startDate !== null && this.personAddressInput[index].startDate !== '') {
            startDate = this.personAddressInput[index].startDate.split('/');
        }
        if (this.personAddressInput[index].changereason !== null && this.personAddressInput[index].changereason !== '') {
            this.endDateReason = true;
        }
        this.addresstypeDropdownItems$
            .subscribe(addresstypes => addresstypes.forEach(addresstype => {
                if (addresstype.value === this.personAddressInput[index].addresstype) {
                    if (this.personAddressInput[index].addresstypeLabel === 'Current' && this.isDjs) {
                        this.currentaddressDropdownItems$ = this._dropdownService.getListByTableID('152');
                    }
                    this.personAddressForm.patchValue({
                        danger: this.personAddressInput[index].danger ? this.personAddressInput[index].danger : '',
                        dangerreason: this.personAddressInput[index].dangerreason ? this.personAddressInput[index].dangerreason : '',
                        addresstype: addresstype.value + '-' + addresstype.text,
                        address1: this.personAddressInput[index].address1 ? this.personAddressInput[index].address1 : '',
                        Address2: this.personAddressInput[index].Address2 ? this.personAddressInput[index].Address2 : '',
                        zipcode: this.personAddressInput[index].zipcode ? this.personAddressInput[index].zipcode : '',
                        state: this.personAddressInput[index].state ? this.personAddressInput[index].state : '',
                        city: this.personAddressInput[index].city ? this.personAddressInput[index].city : '',
                        county: this.personAddressInput[index].county ? this.personAddressInput[index].county : '',
                        startDate: this.personAddressInput[index].startDate ? new Date(startDate[2], startDate[0] - 1, startDate[1]) : '',
                        endDate: this.personAddressInput[index].endDate ? new Date(endDate[2], endDate[0] - 1, endDate[1]) : '',
                        changereason: this.personAddressInput[index].changereason ? this.personAddressInput[index].changereason : '',
                        addressstatus: this.personAddressInput[index].addressstatus ? this.personAddressInput[index].addressstatus : '',
                        personaddresssubtypekey: this.personAddressInput[index].personaddresssubtypekey ? this.personAddressInput[index].personaddresssubtypekey : ''
                    });
                    this.addresstypeLabel = addresstype.value + '-' + addresstype.text;
                }
            }
            ));
        this.isAddressUpdate = true;
        this.selectedPersonAddressIndex = index;

    }

    displayReasonBox(currentValue) {
        if (this.isAddressUpdate) {
            if (currentValue === '') {
                this.endDateReason = false;
                this.personAddressForm.get('changereason').clearValidators();
                this.personAddressForm
                    .get('changereason')
                    .updateValueAndValidity();
                this.personAddressForm.patchValue({ changereason: '' });
            } else {
                this.endDateReason = true;
                this.personAddressForm
                    .get('changereason')
                    .setValidators([Validators.required]);
                this.personAddressForm
                    .get('changereason')
                    .updateValueAndValidity();
            }
        }
    }

    deleteAddressInput(modal, index) {
        if (modal.personaddressid) {
            this.personAddressInput.map(item => {
                if (item.personaddressid === modal.personaddressid) {
                    item.activeflag = 0;
                }
                this.personAddressInput[index].dangerreason = '';
            });
        } else {
            this.personAddressInput.splice(index, 1);
        }
    }
    saveInFormal(modal: PersonSupport) {
        modal.intakenumber = this.id;
        if (this.selectedPersonDetails && this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.personid) {
            modal.personid = this.selectedPersonDetails.personbasicdetails.personid;
        } else {
            modal.personid = null;
        }
        modal.personsupporttypekey = 'informal';
        if (this.inFormalIndex === -1) {
            this.inFormalSuport.push(modal);
        } else {
            this.inFormalSuport[this.inFormalIndex] = modal;
            this.inFormalIndex = -1;
        }
        this.informalForm.reset();
    }
    editInFormal(modal, i) {
        this.informalForm.patchValue(modal);
        this.inFormalIndex = i;
    }
    deleteInFormal(i) {
        this.inFormalSuport.splice(i, 1);
    }
    cancelInFormal() {
        this.informalForm.reset();
        this.inFormalIndex = -1;
    }
    formalSupprotChecked(formal) {
        this.isFormalSupportDes = formal;
        if (formal) {
            this.formalSupportForm.controls['description'].setValidators([Validators.required]);
            this.formalSupportForm.controls['description'].updateValueAndValidity();
        } else {
            this.formalSupportForm.controls['description'].clearValidators();
            this.formalSupportForm.controls['description'].updateValueAndValidity();
            this.formalSupportForm.controls['description'].reset();
        }
    }
    saveRepresentative(modal: PersonPayee) {
        modal.personid = this.selectedPerson.personid ? this.selectedPerson.personid : null;
        if (this.representativeindex === -1) {
            this.personPayeeDetails.push(modal);
        } else {
            this.personPayeeDetails[this.representativeindex] = modal;
        }
        this.representativegroup.reset();
        this.representativeindex = -1;
        this.endDateValidation(this.personPayeeDetails);
    }
    editPayee(modal, index) {
        this.representativeindex = index;
        this.representativegroup.patchValue(modal);
        this.isSaveBtnDisabled = false;
    }
    deletePayee(index) {
        this.personPayeeDetails.splice(index, 1);
        this.endDateValidation(this.personPayeeDetails);
    }
    cancelRepresentative() {
        this.representativegroup.reset();
        this.representativeindex = -1;
        this.endDateValidation(this.personPayeeDetails);
    }
    onRelationShipToROChange(event: any, i: number) {
        // if (this.isDjs) {
        //     if (event.value === 'father' || event.value === 'guardian' || event.value === 'mother') {
        //         this.isAddressReq = true;
        //     } else {
        //         this.isAddressReq = false;
        //     }
        // }
        if (this.agency === 'CW') {
            const rolegrp = <FormArray>this.involvedPersonFormGroup.get('personRole');
            rolegrp.controls.forEach(element => {
                element.patchValue({
                    relationshiptorakey: event.value
                });
            });
        }
    }

    relationShipToRO(event: any, i: number) {
        if (this.isDjs) {
            const personRoleArray = <FormArray>this.involvedPersonFormGroup.controls.personRole;
            const _personRole = personRoleArray.controls[i];
            if (event.value === 'Victim') {
                _personRole.get('relationshiptorakey').setValue('NORLTN');
            }
            this.personsRoleSelected(event, i);
        } else {
            const personRoleArray = <FormArray>(
                this.involvedPersonFormGroup.controls.personRole
            );
            if (['BIOCHILD', 'CHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC'].includes(event.value)) {
                this.filterrelationshipByRole('child');
            } else {
                this.filterrelationshipByRole('LG');
            }
            const _personRole = personRoleArray.controls[i];
            const roleCount = personRoleArray.controls.filter((res) => res.value.rolekey === event.value);
            if (roleCount.length > 1) {
                this.deleteRole(i);
                this._alertService.error('Role is already Added');
            } else {
                if (
                    event.value !== 'RA' &&
                    event.value !== 'PA' &&
                    event.value !== 'RC' &&
                    event.value !== 'CLI'
                ) {
                    const raCount = personRoleArray.controls.filter(
                        res => res.value.hidden === false
                    );
                    if (raCount.length === 0) {
                        _personRole.patchValue({ hidden: false });
                        _personRole
                            .get('relationshiptorakey')
                            .setValidators([Validators.required]);
                        _personRole.get('relationshiptorakey').updateValueAndValidity();
                    } else {
                        _personRole.patchValue({ hidden: true });
                        _personRole.get('relationshiptorakey').clearValidators();
                        _personRole.get('relationshiptorakey').updateValueAndValidity();
                    }
                } else {
                    _personRole.patchValue({ hidden: true });
                    _personRole.get('relationshiptorakey').clearValidators();
                    _personRole.get('relationshiptorakey').updateValueAndValidity();
                }

                // D-07962 Start
                if (event.value === 'CHILD') {
                    if (this.changeRelationShipOnChild) {
                        _personRole.get('relationshiptorakey').setValue('SELF');
                    }
                }
                // D-07962 End
            }
        }
    }

    personsRoleSelected(event: any, i: number) {
        const personRoleArray = <FormArray>this.involvedPersonFormGroup.controls.personRole;
        const _personRole = personRoleArray.controls[i];
        this.isDobRequired(event.value);
        _personRole.patchValue({ isprimary: 'true' });
        if (event.value !== 'RA' && event.value !== 'RC' && event.value !== 'CLI' && event.value !== 'Youth') {
            _personRole.patchValue({ hidden: false });
            _personRole.get('relationshiptorakey').setValidators([Validators.required]);
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        } else {
            _personRole.patchValue({ hidden: true });
            _personRole.get('relationshiptorakey').clearValidators();
            _personRole.get('relationshiptorakey').updateValueAndValidity();
        }

        if (event.value !== 'Youth') {
            this.isDjsYouth = false;
            this.isNotPersonYouth = this.isDjs ? true : false;
            if (this.addEditLabel !== 'Edit') {
                this.isAddressReq = false;
            }
        } else {
            this.isAddressReq = true;
            this.isDjsYouth = false;
            this.isNotPersonYouth = false;
        }


        // D-07265 Start
        // if (event.value !== 'Youth' || event.value === 'Youth') {
        this.involvedPersonFormGroup.get('Dangerousself').clearValidators();
        this.involvedPersonFormGroup.get('Dangerousself').updateValueAndValidity();
        this.involvedPersonFormGroup.get('Dangerousworker').clearValidators();
        this.involvedPersonFormGroup.get('Dangerousworker').updateValueAndValidity();
        this.involvedPersonFormGroup.get('ismentalimpair').clearValidators();
        this.involvedPersonFormGroup.get('ismentalimpair').updateValueAndValidity();
        this.involvedPersonFormGroup.get('ismentalillness').clearValidators();
        this.involvedPersonFormGroup.get('ismentalillness').updateValueAndValidity();
        // }
        // D-07265 End
    }
    checkPrimaryRole(i) {
        const personRoleArray = <FormArray>(
            this.involvedPersonFormGroup.controls.personRole
        );
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach(x => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        if (primaryCount > 1) {
            this._alertService.error(
                'Primary role for this person is already selected'
            );
            _personRole[i].patchValue({ isprimary: 'false' });
        }
    }
    validatePrimaryRole() {
        const personRoleArray = <FormArray>(
            this.involvedPersonFormGroup.controls.personRole
        );
        const _personRole = personRoleArray.controls;
        let primaryCount = 0;
        _personRole.forEach(x => {
            if (x.get('isprimary').value === 'true') {
                primaryCount = primaryCount + 1;
            }
        });
        // if (primaryCount === 1) {
        //     return true;
        // } else {
        //     if (this.isCW) {
        //         return true;
        //     } else {
        //         this._alertService.error('Please Select One Primary Role Type');
        //         return false;
        //     }
        // }
        //quick fix on primary rle check, this will be fixed when person profile redeisnged
        return true;
    }

    addNewRole() {
        const control = <FormArray>(
            this.involvedPersonFormGroup.controls['personRole']
        );
        const controlvalue = control.value;
        const relationship = (controlvalue && controlvalue.length) ? controlvalue[0].relationshiptorakey : '';
        control.push(this.createFormGroup(true, relationship));
    }

    deleteRole(index: number) {
        const control = <FormArray>(
            this.involvedPersonFormGroup.controls['personRole']
        );
        setTimeout(() => {
            control.removeAt(index);
        }, 100);
    }

    createFormGroup(isHideRA, relationship?: string) {
        if (this.isCW) {
            return this._formBuilder.group({
                rolekey: ['', Validators.required],
                description: [''],
                isprimary: ['true', Validators.required],
                relationshiptorakey: [relationship],
                hidden: [isHideRA]
            });
        }
        return this._formBuilder.group({
            rolekey: ['', Validators.required],
            description: [''],
            isprimary: ['', Validators.required],
            relationshiptorakey: [''],
            hidden: [isHideRA]
        });
    }
    setFormValues() {
        this.involvedPersonFormGroup.setControl(
            'personRole',
            this._formBuilder.array([])
        );
        const control = <FormArray>(
            this.involvedPersonFormGroup.controls.personRole
        );
        const actor = this.personRole.find(item => item.hasOwnProperty('actorrelationship') === true);
        const relation = (actor) ? actor.actorrelationship.relationshiptypekey : '';
        this.personRole.forEach((x, index) => {
            control.push(this.buildPersonRoleForm(x, relation));
            if (this.isDjs) {
                this.personsRoleSelected({ value: x.intakeservicerequestpersontypekey }, index);
            }
        });
    }

    disabledInput(state: boolean, inputfield: string, manditory: string) {
        if (state === false && manditory === 'isManditory') {
            this.involvedPersonFormGroup.get(inputfield).enable();
            if (inputfield === 'DangerousselfReason') {
                this.involvedPersonFormGroup
                    .get('Dangerousself')
                    .valueChanges.subscribe((Dangerousself: any) => {
                        if (Dangerousself === '1') {
                            this.dangerSelf = true;
                            this.involvedPersonFormGroup
                                .get('DangerousselfReason')
                                .setValidators([Validators.required]);
                            this.involvedPersonFormGroup
                                .get('DangerousselfReason')
                                .updateValueAndValidity();
                        } else {
                            this.dangerSelf = false;
                            this.involvedPersonFormGroup
                                .get('DangerousselfReason')
                                .clearValidators();
                            this.involvedPersonFormGroup
                                .get('DangerousselfReason')
                                .updateValueAndValidity();
                        }
                    });
            }

            if (inputfield === 'DangerousWorkerReason') {
                this.involvedPersonFormGroup
                    .get('Dangerousworker')
                    .valueChanges.subscribe((Dangerousself: any) => {
                        if (Dangerousself === '1') {
                            this.dangerWorker = true;
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .setValidators([Validators.required]);
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .updateValueAndValidity();
                        } else {
                            this.dangerWorker = false;
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .clearValidators();
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .updateValueAndValidity();
                        }
                    });
            }

            if (inputfield === 'DangerousWorkerReason') {
                this.involvedPersonFormGroup
                    .get('Dangerousworker')
                    .valueChanges.subscribe((Dangerousself: any) => {
                        if (Dangerousself === '1') {
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .setValidators([Validators.required]);
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .updateValueAndValidity();
                        } else {
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .clearValidators();
                            this.involvedPersonFormGroup
                                .get('DangerousWorkerReason')
                                .updateValueAndValidity();
                        }
                    });
            }
            if (inputfield === 'mentalimpairdetail') {
                this.involvedPersonFormGroup
                    .get('ismentalimpair')
                    .valueChanges.subscribe((Dangerousself: any) => {
                        if (Dangerousself === '1') {
                            this.dangerApperance = true;
                            this.involvedPersonFormGroup
                                .get('mentalimpairdetail')
                                .setValidators([Validators.required]);
                            this.involvedPersonFormGroup
                                .get('mentalimpairdetail')
                                .updateValueAndValidity();
                        } else {
                            this.dangerApperance = false;
                            this.involvedPersonFormGroup
                                .get('mentalimpairdetail')
                                .clearValidators();
                            this.involvedPersonFormGroup
                                .get('mentalimpairdetail')
                                .updateValueAndValidity();
                        }
                    });
            }
            if (inputfield === 'mentalillnessdetail') {
                this.involvedPersonFormGroup
                    .get('ismentalillness')
                    .valueChanges.subscribe((Dangerousself: any) => {
                        if (Dangerousself === '1') {
                            this.dangerIll = true;
                            this.involvedPersonFormGroup
                                .get('mentalillnessdetail')
                                .setValidators([Validators.required]);
                            this.involvedPersonFormGroup
                                .get('mentalillnessdetail')
                                .updateValueAndValidity();
                        } else {
                            this.dangerIll = false;
                            this.involvedPersonFormGroup
                                .get('mentalillnessdetail')
                                .clearValidators();
                            this.involvedPersonFormGroup
                                .get('mentalillnessdetail')
                                .updateValueAndValidity();
                        }
                    });
            }
        } else if (state === true) {
            this.involvedPersonFormGroup.get(inputfield).disable();
        } else if (state === false && manditory === 'notManditory') {
            this.involvedPersonFormGroup.get(inputfield).enable();
        }
    }
    languageType() {
        if (this.involvedPersonFormGroup.controls['primarylanguage'].value && this.involvedPersonFormGroup.controls['secondarylanguage'].value) {
            if (this.involvedPersonFormGroup.controls['primarylanguage'].value === this.involvedPersonFormGroup.controls['secondarylanguage'].value) {
                this._alertService.error('Please select two different languages');
                this.involvedPersonFormGroup.controls['primarylanguage'].reset();
                this.involvedPersonFormGroup.controls['secondarylanguage'].reset();
            }
        }
        if (this.isAS) {
            this.involvedPersonFormGroup.controls['otherprimarylanguage'].reset();
        }
    }

    toggleAddPopup(person: People) {
        this.addEditLabel = 'Add New';
        this.isImageHide = true;
        this.beofreImageCropeHide = false;
        this.isPersonNew = true;
        if (person && person.source === 'SDR') {
            this.involvedPersonFormGroup.patchValue({
                Lastname: person.lastname,
                Firstname: person.firstname,
                middlename: person.middlename,
                Gender: person.gendertypekey,
                Dob: new Date(person.dob),
                // dateofdeath: new Date(person.dateofdeath),
                SSN: person.ssn,
                mdm_id: person.mdm_id,
                Pid: null,
                suffix: person.suffix,
                aliasname: person.alias,
                source: person.source
            });
        } else {
            const searchedFormData = this._dataStoreService.getData('SearchDataInvolvedPerson');
            if (searchedFormData) {
                this.patchSearchFormData(searchedFormData);
            }
        }
        // }
        // this.isEditPersonSaveBtn = false;
        this.involvedPersonFormGroup.setControl(
            'personRole',
            this._formBuilder.array([])
        );
        this.addNewRole();
        (<any>$('#intake-addperson')).modal('show');
        if (this.isDjs) {
            (<any>$('#add-Reporter-click')).click();
        } else {
            (<any>$('#profile-click')).click();
        }
        this.userProfile = '../../../../../assets/images/ic_silhouette.png';
        this.contactPersonInfo = [];
        this.isContactSerach = false;
        this.requiredTab = true;
    }
    // DJS-008 Auto Populate the Add New Person screen
    patchSearchFormData(model) {
        console.log('model...', model);
        this.involvedPersonFormGroup.patchValue({
            Lastname: model.lastname,
            Firstname: model.firstname,
            middlename: model.maidenname,
            Gender: model.gender,
            City: model.city,
            Address: model.address,
            Zip: model.zip,
            State: model.state,
            County: model.county,
            SSN: model.ssn,
            stateid: model.dl,
            occupation: model.occupation,
            aliasname: model.alias
        });
        if (model.dob) {
            this.involvedPersonFormGroup.patchValue({
                Dob: model.dob ? new Date(model.dob) : ''
            });
        }
        if (model.dateofdeath) {
            this.involvedPersonFormGroup.patchValue({
                Dob: model.dateofdeath ? new Date(model.dateofdeath) : ''
            });
        }
        this.personAddressForm.patchValue({
            address1: model.address1,
            Address2: model.address2,
            zipcode: model.zip,
            state: model.stateid,
            county: model.county,
            city: model.city
        });
        this.personPhoneForm.patchValue({
            contactnumber: model.phone
        });
        this.personEmailForm.patchValue({
            EmailID: model.email
        });
    }
    showSerachDetailsPage(modal) {
        if (modal === 'emergency') {
            this.isEmergencyContact = true;
            this.representativePerson = false;
        } else {
            this.representativePerson = true;
            this.isEmergencyContact = false;
        }
        (<any>$('#intake-addperson')).modal('hide');
        this.guardianPersonBtn = false;
        this.toggleSearchModal();
        (<any>$('#intake-findperson')).modal('show');
    }
    removePerson(index) {
        this.contactPersonInfo.splice(index, 1);
    }
    contactPersonDetails(personid: string, title: string) {
        this.repPayContactPersonTitle = title;
        (<any>$('#intake-addperson')).modal('hide');
        (<any>$('#contact-person-details')).modal('show');
        this._commonHttpService.getSingle(
            {
                method: 'get',
                where: {
                    intakeserviceid: this.id,
                    personid: personid
                }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                .PersonList + '?filter'
        ).subscribe((res) => {
            this.contactPersonBasicDetails = res['personbasicdetails'];
        });
    }
    closeContactPerson() {
        (<any>$('#contact-person-details')).modal('hide');
        (<any>$('#intake-addperson')).modal('show');
    }
    addSerachInvolvedPerson() {
        if (this.selectedUnkPerson) {
            this.involvedUnkPerson = this.involvedUnkPerson.map(person => {
                if (person.id === this.selectedUnkPerson.id) {
                    person.isNew = false;
                }
                return person;
            });
            this.editInvolvedUnkPerson('edit');
        } else {
            this.getInvolvedPerson();
            this.getInvolvedUnkPerson();
        }
    }

    checkSubstanceAbuse() {
        let valid = true;
        const isDrugExposed = this.involvedPersonFormGroup.get('drugexposednewbornflag').value;
        if (isDrugExposed) {
            const substanceClass = this.involvedPersonFormGroup.get('substanceClass').value;
            valid = ((substanceClass && substanceClass.length > 0) || this.involvedPersonFormGroup.get('otherSubstance').value);
        }

        return valid;
    }

    addPerson(involvedPerson) {
        involvedPerson.maritalstatustypekey = (involvedPerson.maritalstatustypekey) ? involvedPerson.maritalstatustypekey : null;
        const isSubstanceAbuseValid: boolean = this.checkSubstanceAbuse();
        if (this.involvedPersonFormGroup.valid) {
            if (this.formalSupportForm.valid) {
                if (this.formalSupportForm.controls['personsupporttypekey'].value && this.isAS) {
                    this.formalSupportForm.value.personsupporttypekey = 'formal';
                    const formalSupport = Object.assign({
                        supportername: null,
                        intakenumber: this.id,
                        personid: this.selectedPersonDetails.personbasicdetails.personid
                    }, this.formalSupportForm.value);
                    this.inFormalSuport.push(formalSupport);
                }
                if (this.personAddressInput.length > 0) {
                    for (let i = 0; i < this.personAddressInput.length; i++) {
                        const tempDanger = this.personAddressInput[i].danger;
                        if (tempDanger === 1 || tempDanger === 'yes') {
                            this.personAddressInput[i].knowndangeraddress = 'yes';
                            this.personAddressInput[i].danger = 'yes';
                        } else if (tempDanger === 0 || tempDanger === 'no') {
                            this.personAddressInput[i].knowndangeraddress = 'no';
                            this.personAddressInput[i].danger = 'no';
                        } else {
                            this.personAddressInput[i].knowndangeraddress = null;
                            this.personAddressInput[i].danger = null;
                        }
                    }
                }
                // DJS-013 Required Victim Data Elements
                if (this.isDjs) {
                    const roleVictim = involvedPerson.personRole[0].rolekey;
                    const roleRelation = involvedPerson.personRole[0].relationshiptorakey;
                    // D-07756, D-07755 - Address should be mandatory for Youth role and Parent/Guardian role. Address should not be mandatory for Victim role option
                    if (roleVictim === 'Youth' || roleRelation === 'father' || roleRelation === 'guardian' || roleRelation === 'mother') {
                        const addresform = this.personAddressInput[0];
                        if (addresform) {
                            if (addresform.address && addresform.zipcode
                                && addresform.state && addresform.city) {
                            } else {
                                if (roleVictim !== 'Youth' && this.isnoCopyAddress) {
                                    if (this.copypersonAddressInput && this.copypersonAddressInput.length > 0) {
                                        this.personAddressInput = this.copypersonAddressInput.map(data => {
                                            data.personaddressid = '';
                                            data.personid = involvedPerson.Pid;
                                            return data;
                                        });
                                        (<any>$('#intake-addperson')).modal('hide');
                                        (<any>$('#address-add-popup')).modal('show');
                                        return true;
                                    }
                                } else if (roleVictim === 'Youth') {
                                    this._alertService.error('Please add Address!');
                                    return true;
                                }
                            }
                        } else {
                            if (roleVictim !== 'Youth' && this.isnoCopyAddress) {
                                if (this.copypersonAddressInput && this.copypersonAddressInput.length > 0) {
                                    this.personAddressInput = this.copypersonAddressInput.map(data => {
                                        data.personaddressid = '';
                                        data.personid = involvedPerson.Pid;
                                        return data;
                                    });
                                    (<any>$('#intake-addperson')).modal('hide');
                                    (<any>$('#address-add-popup')).modal('show');
                                    return true;
                                }
                            } else if (roleVictim === 'Youth') {
                                this._alertService.error('Please add Address!');
                                return true;
                            }
                        }
                    }
                }
                if (this.formalSupportForm.controls['personsupporttypekey'].value && this.isAS) {
                    this.formalSupportForm.value.personsupporttypekey = 'formal';
                    const formalSupport = Object.assign({
                        supportername: null,
                        intakenumber: this.id,
                        personid: this.selectedPersonDetails.personbasicdetails.personid
                    }, this.formalSupportForm.value);
                    this.inFormalSuport.push(formalSupport);
                }
                if (this.validatePrimaryRole()) {
                    if (
                        !this.involvedPreson.index &&
                        this.involvedPreson.index !== 0
                    ) {
                        this.involvedPreson.school = this.addEducation.personeducation;
                        this.involvedPreson.accomplishment = this.addEducation.personaccomplishment;
                        this.involvedPreson.testing = this.addEducation.personeducationtesting;
                        this.involvedPreson.vocation = this.addEducation.personeducationvocation;
                        this.involvedPreson.contacts = this.phoneNumber;
                        this.involvedPreson.address = this.personAddressInput;
                        this.involvedPreson.emergency = this.contactPersonInfo;
                        this.involvedPreson.guardian = Object.assign(this.guardianships);
                        this.involvedPreson.personpayee = this.personPayeeDetails;
                        this.involvedPreson.personsupport = this.inFormalSuport ? this.inFormalSuport : [];
                        this.involvedPreson.userphoto = this.userProfilePicture
                            ? this.userProfilePicture.s3bucketpathname
                            : null;
                        this.involvedPreson.physicianinfo = this.addHealth.physician
                            ? this.addHealth.physician
                            : [];
                        this.involvedPreson.healthinsurance = this.addHealth
                            .healthInsurance
                            ? this.addHealth.healthInsurance
                            : [];
                        this.involvedPreson.personmedicationphyscotropic = this
                            .addHealth.medication
                            ? this.addHealth.medication
                            : [];
                        this.involvedPreson.personhealthexam = this.addHealth
                            .healthExamination
                            ? this.addHealth.healthExamination
                            : [];
                        this.involvedPreson.personmedicalcondition = this.addHealth
                            .medicalcondition
                            ? this.addHealth.medicalcondition
                            : [];
                        this.involvedPreson.personbehavioralhealth = this.addHealth
                            .behaviouralhealthinfo
                            ? this.addHealth.behaviouralhealthinfo
                            : [];
                        this.involvedPreson.personabusehistory = this.addHealth
                            .history
                            ? this.addHealth.history
                            : {};
                        this.involvedPreson.personabusesubstance = this.addHealth
                            .substanceAbuse
                            ? this.addHealth.substanceAbuse
                            : {};
                        this.involvedPreson.persondentalinfo = this.addHealth
                            .persondentalinfo
                            ? this.addHealth.persondentalinfo
                            : [];
                        this.involvedPreson.contactsmail = this.emailID;
                        involvedPerson.Pid =
                            involvedPerson.Pid === null ? '' : involvedPerson.Pid;
                        involvedPerson.ismentalimpair =
                            involvedPerson.ismentalimpair === 'no'
                                ? null
                                : involvedPerson.ismentalimpair;
                        involvedPerson.ismentalillness =
                            involvedPerson.ismentalillness === 'no'
                                ? null
                                : involvedPerson.ismentalillness;
                        involvedPerson.Dangerousself =
                            involvedPerson.Dangerousself === 'no'
                                ? null
                                : involvedPerson.Dangerousself;
                        involvedPerson.Dangerousworker =
                            involvedPerson.Dangerousworker === 'no'
                                ? null
                                : involvedPerson.Dangerousworker;
                        involvedPerson.isapproxdod = involvedPerson.isapproxdod
                            ? 1
                            : null;
                        involvedPerson.isapproxdob = involvedPerson.isapproxdob
                            ? 1
                            : null;
                        involvedPerson.dateofdeath = involvedPerson.dateofdeath
                            ? moment(involvedPerson.dateofdeath).format(
                                'MM/DD/YYYY'
                            )
                            : null;
                        involvedPerson.Dob = involvedPerson.Dob
                            ? moment(involvedPerson.Dob).format('MM/DD/YYYY')
                            : null;
                        this.involvedPreson.personRole = [];
                        this.involvedPreson.personRole = involvedPerson.personRole;
                        if (involvedPerson && involvedPerson.personRole) {
                            involvedPerson.personRole.map((item, index) => {
                                if (this.isCW) {
                                    item.isprimary = (index === 0) ? 'true' : 'false';
                                    if (index === 0) {
                                        this.involvedPreson.Role = item.rolekey;
                                        this.involvedPreson.RelationshiptoRA = item.relationshiptorakey
                                            ? item.relationshiptorakey
                                            : null;
                                    }
                                } else {
                                    if (item.isprimary === 'true') {
                                        this.involvedPreson.Role = item.rolekey;
                                        this.involvedPreson.RelationshiptoRA = item.relationshiptorakey
                                            ? item.relationshiptorakey
                                            : null;
                                    }
                                }
                            });
                        }
                        const person = Object.assign(this.involvedPreson, involvedPerson);
                        person.fetalalcoholspctrmdisordflag = (involvedPerson.fetalalcoholspctrmdisordflag) ? 1 : 0;
                        person.drugexposednewbornflag = (involvedPerson.drugexposednewbornflag) ? 1 : 0;
                        person.probationsearchconductedflag = (involvedPerson.probationsearchconductedflag) ? 1 : 0;
                        person.sexoffenderregisteredflag = (involvedPerson.sexoffenderregisteredflag) ? 1 : 0;
                        person.safehavenbabyflag = (involvedPerson.safehavenbabyflag) ? 1 : 0;
                        person.everbeenadoptedflag = (involvedPerson.everbeenadoptedflag) ? 1 : 0;
                        person.isqualifiedalien = (involvedPerson.isqualifiedalien === 'null') ? '0' : involvedPerson.isqualifiedalien;
                        person.citizenalenageflag = (involvedPerson.citizenalenageflag === 'null') ? '0' : involvedPerson.citizenalenageflag;
                        if (this.isCW) {
                            person.personRole.map((item, index) => {
                                item.isprimary = (index === 0) ? 'true' : 'false';
                            });
                        }
                        this.addedPersons = [];
                        this.addedPersons.push(
                            // Object.assign(this.involvedPreson, involvedPerson)
                            person
                        );
                        let data: Object;
                        if (this.isServiceCase) {
                            data = {
                                Person: this.addedPersons,
                                objectid: this.id,
                                objecttypekey: 'servicecase'
                            };
                        } else {
                            data = {
                                Person: this.addedPersons,
                                intakeserviceid: this.id
                            };
                        }
                        this._commonHttpService
                            .create(
                                data,
                                CaseWorkerUrlConfig.EndPoint.DSDSAction
                                    .InvolvedPerson.UpdatePerson
                            )
                            .subscribe(
                                result => {
                                    this.getInvolvedPerson();
                                    /* if (this.isPersonNew || !this.isCW) {
                                        (<any>$('#intake-addperson')).modal('hide');
                                        this.clearPerson();
                                     }
                                     this._alertService.success(
                                         'Person updated successfully'
                                     );*/
                                    // if (this.isPersonNew || !this.isCW) {
                                    if (this.addEditLabel === 'Add New') {
                                        (<any>$('#intake-addperson')).modal('hide');
                                        this._alertService.success(
                                            'New Person Added successfully'
                                        );
                                        this.clearPerson();
                                    } else {
                                        this._alertService.success(
                                            'Person updated successfully'
                                        );
                                    }
                                    //  this.clearPerson();
                                    // }
                                },
                                error => {
                                    console.log(error);
                                }
                            );
                    }
                }
            } else {
                this._alertService.warn('Please fill the formal support description');
            }
        } else {
            ControlUtils.validateAllFormFields(this.involvedPersonFormGroup);
            ControlUtils.setFocusOnInvalidFields();
            this._alertService.warn('Please fill mandatory fields!');
        }
        this.isnoCopyAddress = true;
    }

    getYouthAddress() {
        const persondetails = this.involvedPersonList;
        if (persondetails && persondetails.length > 0) {
            const getYouthDetails = persondetails.find(data => data.rolename === 'Youth');
            if (getYouthDetails && getYouthDetails.personid) {
                this._commonHttpService
                    .getArrayList(
                        new PaginationRequest(
                            {
                                method: 'get',
                                where: { personid: getYouthDetails.personid },
                                nolimit: true
                            }),
                        CommonUrlConfig.EndPoint.PERSON.ADDRESS.ListAddressUrl + '?filter'
                    ).subscribe(data => {
                        this.copypersonAddressInput = data.map(ele => {
                            ele.personaddressid = '';
                            return ele;
                        });
                    });
            }
        }
    }

    editPerson(modal, text) {
        let inputRequest: Object;
        this.requiredTab = false;
        this.selectedPerson = modal;
        this._dataStoreService.setData(GUARDIANSHIP.PersonId, modal.personid);
        this._dataStoreService.setData(AppConstants.GLOBAL_KEY.SELECTED_PERSON_ID, modal.personid);
        this.enableSafeHaven = false;
        this.enableSEN = false;
        this.selectedState = 'MD';
        this.mandatoryCountyRequired = true;
        this.selectedPersonsage = this.calculateAge(this.selectedPerson.dob ? this.selectedPerson.dob : null);
        if (this.selectedPerson.rolename === 'RC' && this.assessmentDetail) {
            this.isSurvey = true;
        } else {
            this.isSurvey = false;
        }
        if (modal.rolename === 'RA') {
            this.isReportAdult = true;
        } else {
            this.isReportAdult = false;
        }
        if (['BIOCHILD', 'CHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC'].includes(this.selectedPerson.rolename)) {
            this.isChild = true;
            this.filterrelationshipByRole('child');
        } else {
            this.isChild = false;
            this.filterrelationshipByRole('LG');
        }
        this._dataStoreService.setData(
            DSDSConstants.DSDS.PersonsInvolved.Health.Health,
            new Health()
        );
        this.healthFormReset$.next(true);
        if (this.isServiceCase) {
            inputRequest = {
                servicecaseid: this.id,
                personid: modal.personid
            };
        } else {
            inputRequest = {
                intakeserviceid: this.id,
                personid: modal.personid
            };
        }
        this._commonHttpService
            .getSingle(
                {
                    method: 'get',
                    where: inputRequest
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PersonList + '?filter'
            )
            .subscribe(result => {
                this.selectedPersonDetails = result;
                const personbasicdetails = (this.selectedPersonDetails) ? this.selectedPersonDetails.personbasicdetails : null;
                this.storeMaritalStatus = this.selectedPersonDetails.personbasicdetails
                    ? this.selectedPersonDetails.personbasicdetails
                        .maritalstatustypekey
                    : null,
                    this.involvedPersonFormGroup.patchValue({
                        tribalassociation: this.selectedPersonDetails
                            .personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .tribalassociation
                            : '',
                        Lastname: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails.lastname
                            : '',
                        Firstname: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .firstname
                            : '',
                        Middlename: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .middlename
                            : '',
                        suffix: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .nameSuffix
                            : '',
                        maritalstatustypekey: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .maritalstatustypekey
                            : null,
                        Dob: this.selectedPersonDetails.personbasicdetails.dob,
                        dateofdeath: this.selectedPersonDetails.personbasicdetails
                            .dateofdeath,
                        isapproxdod: modal.isapproxdod === 1 ? true : null,
                        isapproxdob: modal.isapproxdob === 1 ? true : null,
                        Gender: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .gendertypekey
                            : '',
                        religiontypekey: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .religiontypekey
                            : null,
                        otherreligion: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .otherreligion
                            : null,
                        Dangerous: '',
                        dangerAddress: '',
                        Race: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .racetypekey
                            : null,
                        SSN: this.selectedPersonDetails.personbasicdetails
                            .personidentifier.length
                            ? this.selectedPersonDetails.personbasicdetails
                                .personidentifier[0].personidentifiervalue
                            : '',
                        Ethicity: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .ethnicgrouptypekey
                            : null,
                        occupation: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .occupation
                            : '',
                        stateid: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails.stateid
                            : null,
                        primarylanguage: (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.primarylanguageid) ?
                            this.selectedPersonDetails.personbasicdetails.primarylanguageid : 'ENG',
                        secondarylanguage: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.secondarylanguageid : null,
                        otherprimarylanguage: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.otherprimarylanguagetypekey : null,
                        aliasname: (this.selectedPersonDetails.personbasicdetails
                            && this.selectedPersonDetails.personbasicdetails.alias
                            && Array.isArray(this.selectedPersonDetails.personbasicdetails.alias))
                            && this.selectedPersonDetails.personbasicdetails.alias.length
                            // ? this.selectedPersonDetails.personbasicdetails.salutation
                            ? this.selectedPersonDetails.personbasicdetails.alias[0].firstname
                            : '',
                        Dangerousself:
                            this.selectedPersonDetails.personbasicdetails
                                .dangertoself === null
                                ? 'no'
                                : this.selectedPersonDetails.personbasicdetails
                                    .dangertoself,
                        DangerousselfReason: this.selectedPersonDetails
                            .personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .dangertoselfreason
                            : '',
                        Pid: this.selectedPersonDetails.personbasicdetails.personid
                            ? this.selectedPersonDetails.personbasicdetails.personid
                            : '',
                        iscollateralcontact: (this.selectedPersonDetails
                            .personroledetails) ? this.selectedPersonDetails
                                .personroledetails.actor.iscollateralcontact : null,
                        ishousehold: (this.selectedPersonDetails.personroledetails) ?
                            this.selectedPersonDetails.personroledetails.actor.ishousehold : null,
                        // issafehaven: this.selectedPersonDetails.personroledetails
                        //     .actor.issafehaven,
                        fetalalcoholspctrmdisordflag: (modal.fetalalcoholspctrmdisordflag === 1) ? true : false,
                        drugexposednewbornflag: (modal.drugexposednewbornflag === 1) ? true : false,
                        probationsearchconductedflag: (this.selectedPersonDetails
                            .personroledetails) ? this.selectedPersonDetails
                                .personroledetails.actor.probationsearchconductedflag : null,
                        // probationsearchconductedflag: (modal.probationsearchconductedflag === 1) ? true : false,
                        sexoffenderregisteredflag: (modal.sexoffenderregisteredflag === 1) ? true : false,
                        safehavenbabyflag: (modal.safehavenbabyflag === 1) ? true : false,
                        // everbeenadoptedflag: (modal.everbeenadoptedflag === 1) ? true : false,
                        everbeenadoptedflag: personbasicdetails ? (personbasicdetails.everbeenadoptedflag === 1) ? true : false : false,
                        strengths: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails
                                .strengths
                            : '',
                        needs: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails.needs
                            : '',
                        citizenalenageflag: this.selectedPersonDetails.personbasicdetails
                            ? ('' + this.selectedPersonDetails.personbasicdetails.citizenalenageflag) : '',
                        isqualifiedalien: this.selectedPersonDetails.personbasicdetails
                            ? ('' + this.selectedPersonDetails.personbasicdetails.isqualifiedalien) : '',
                        alienstatustypekey: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails.alienstatustypekey : '',
                        alienregistrationtext: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails.alienregistrationtext : '',
                        verificationremarks: this.selectedPersonDetails.personbasicdetails
                            ? this.selectedPersonDetails.personbasicdetails.verificationremarks : '',
                        licensedfacilitykey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.licensedfacilitykey : null,
                        livingsituationdesc: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.livingsituationdesc : null,
                        livingsituationkey: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.livingsituationkey : null,
                        otherlicensedfacility: this.selectedPersonDetails.personbasicdetails ? this.selectedPersonDetails.personbasicdetails.otherlicensedfacility : null
                    });

                if (
                    this.selectedPersonDetails.personroledetails &&
                    this.selectedPersonDetails.personroledetails.actor
                ) {
                    this.involvedPersonFormGroup.patchValue({
                        Dangerousworker:
                            this.selectedPersonDetails.personroledetails.actor
                                .dangerlevel === null
                                ? 'no'
                                : this.selectedPersonDetails.personroledetails
                                    .actor.dangerlevel,
                        DangerousWorkerReason: this.selectedPersonDetails
                            .personroledetails.actor.dangerreason,
                        ismentalimpair:
                            this.selectedPersonDetails.personroledetails.actor
                                .ismentalimpair === null
                                ? 'no'
                                : this.selectedPersonDetails.personroledetails
                                    .actor.ismentalimpair,
                        mentalimpairdetail: this.selectedPersonDetails
                            .personroledetails.actor.mentalimpairdetail,
                        ismentalillness:
                            this.selectedPersonDetails.personroledetails.actor
                                .ismentalillness === null
                                ? 'no'
                                : this.selectedPersonDetails.personroledetails
                                    .actor.ismentalillness,
                        mentalillnessdetail: this.selectedPersonDetails
                            .personroledetails.actor.mentalillnessdetail
                    });
                    this.resetRoleTabRadioBtn(this.selectedPersonDetails.personroledetails.actor.iscollateralcontact);
                }

                // tslint:disable-next-line:max-line-length
                if (this.selectedPersonDetails.personbasicdetails && this.selectedPersonDetails.personbasicdetails.personphysicalattribute && this.selectedPersonDetails.personbasicdetails.personphysicalattribute.length) {
                    const higth = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'Ht');
                    const weight = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'Wt');
                    const tattoos = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'Tattoo');
                    const phyMarks = this.selectedPersonDetails.personbasicdetails.personphysicalattribute.filter((item) => item.physicalattributetypekey === 'PhyMark');
                    this.involvedPersonFormGroup.patchValue({
                        height: higth && higth.length ? higth[0].attributevalue : '',
                        weight: weight && weight.length ? weight[0].attributevalue : '',
                        tattoo: tattoos && tattoos.length ? tattoos[0].attributevalue : '',
                        PhyMark: phyMarks && phyMarks.length ? phyMarks[0].attributevalue : ''
                    });
                }
                if (this.selectedPersonDetails.personbasicdetails.personrepresentativepayee && this.selectedPersonDetails.personbasicdetails.personrepresentativepayee.length) {
                    this.selectedPersonDetails.personbasicdetails.personrepresentativepayee.map((item) => {
                        const data = Object.assign({
                            personid: item.personid,
                            personrepresentativeworkerid: item.personrepresentativeworkerid,
                            personrepresentativepersonid: item.personrepresentativepersonid,
                            entityid: item.entityid,
                            persontypekey: item.persontypekey,
                            representativetypekey: item.referencevalues.ref_key,
                            startdate: item.startdate,
                            enddate: item.enddate
                        } as PersonPayee);
                        this.personPayeeDetails.push(data);
                        if (item && (item.persontypekey === 'INPRSN' || item.persontypekey === 'PRSN')) {
                            const persons = Object.assign({
                                firstname: item.payeecontact.firstname,
                                lastname: item.payeecontact.lastname,
                                personid: item.payeecontact.personid
                            } as PersonPayType);
                            this.representative.push(persons);
                        }
                    });
                    this.representativePersnNames(this.representative);
                    this.endDateValidation(this.personPayeeDetails);
                } else {
                    this.endDateValidation(this.personPayeeDetails);
                }
                if (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personbasicdetails.emergencycontactperson) {
                    this.contactPersonInfo = this.selectedPersonDetails.personbasicdetails.emergencycontactperson.map((item) => {
                        item.contactperson.contactpersonid = item.contactperson.personid;
                        delete item.contactperson.personid;
                        return item.contactperson;
                    });
                }
                if (this.selectedPersonDetails.personbasicdetails) {
                    this.addEducation.personeducation = this.selectedPersonDetails.personbasicdetails.personeducation;
                    this.addEducation.personaccomplishment = this.selectedPersonDetails.personbasicdetails.personaccomplishment;
                    this.addEducation.personeducationtesting = this.selectedPersonDetails.personbasicdetails.personeducationtesting;
                    this.addEducation.personeducationvocation = this.selectedPersonDetails.personbasicdetails.personeducationvocation;
                    this.addHealth.physician = this.selectedPersonDetails.personbasicdetails.personphycisianinfo;
                    if (this.selectedPersonDetails.personbasicdetails.personsupport && this.selectedPersonDetails.personbasicdetails.personsupport.length) {
                        this.inFormalSuport = this.selectedPersonDetails.personbasicdetails.personsupport.filter((item) => item.personsupporttypekey === 'informal');
                        this.selectedPersonDetails.personbasicdetails.personsupport.map((item) => {
                            if (item.personsupporttypekey === 'formal') {
                                this.formalSupportForm.controls['personsupporttypekey'].patchValue(item.personsupporttypekey === 'formal' ? true : false);
                                this.formalSupportForm.controls['description'].patchValue(item.description);
                                this.formalSupprotChecked(item.personsupporttypekey === 'formal' ? true : false);
                            }
                        });
                    }
                    this.addHealth.physician = this.selectedPersonDetails.personbasicdetails.personphycisianinfo;
                    this.addHealth.healthInsurance = this.selectedPersonDetails.personbasicdetails.personhealthinsurance;
                    this.addHealth.medication = this.selectedPersonDetails.personbasicdetails.personmedicationphyscotropic;
                    this.addHealth.healthExamination = this.selectedPersonDetails.personbasicdetails.personhealthexam;
                    this.addHealth.medicalcondition = this.selectedPersonDetails.personbasicdetails.personmedicalcondition;
                    this.addHealth.behaviouralhealthinfo = this.selectedPersonDetails.personbasicdetails.personbehavioralhealth;
                    this.addHealth.history = this.selectedPersonDetails.personbasicdetails.personabusehistory;
                    this.addHealth.substanceAbuse = this.selectedPersonDetails.personbasicdetails.personabusesubstance;
                    // this.addHealth.providerType = this.selectedPersonDetails.personbasicdetails.providercw;
                    this.addHealth.persondentalinfo = this.selectedPersonDetails.personbasicdetails.persondentalinfo;
                    // this.addHealthOutputSubject$.next(this.addHealth);
                    this._dataStoreService.setData(
                        DSDSConstants.DSDS.PersonsInvolved.Health.Health,
                        this.addHealth
                    );
                    if (this.selectedPersonDetails.personbasicdetails.personguardianfuneral) {
                        this._dataStoreService.setData(GUARDIANSHIP.Funeral, this.selectedPersonDetails.personbasicdetails.personguardianfuneral);
                    }
                    if (this.selectedPersonDetails.personbasicdetails.personguardiancode) {
                        this._dataStoreService.setData(GUARDIANSHIP.CodeStatus, this.selectedPersonDetails.personbasicdetails.personguardiancode);
                    }
                    if (this.selectedPersonDetails.personbasicdetails.personguardiandetails && this.selectedPersonDetails.personbasicdetails.personguardiandetails.hhsclientid) {
                        const hhscNotes = {
                            hhsclientid: this.selectedPersonDetails.personbasicdetails.personguardiandetails.hhsclientid,
                            notes: this.selectedPersonDetails.personbasicdetails.personguardiandetails.notes
                        };
                        this._dataStoreService.setData(GUARDIANSHIP.HSSClinetID, hhscNotes);
                    }
                    if (this.selectedPersonDetails.personbasicdetails.personguardian && this.selectedPersonDetails.personbasicdetails.personguardian.length) {
                        const WRK = this.selectedPersonDetails.personbasicdetails.personguardian.filter((item) => item.guardianpersontypekey === 'WRK');
                        const GOP = this.selectedPersonDetails.personbasicdetails.personguardian.filter((item) => item.guardianpersontypekey === 'GOP');
                        const GOPT = this.selectedPersonDetails.personbasicdetails.personguardian.filter((item) => item.guardianpersontypekey === 'GOPT');
                        const ATTY = this.selectedPersonDetails.personbasicdetails.personguardian.filter((item) => item.guardianpersontypekey === 'ATTY');
                        this._dataStoreService.setData(GUARDIANSHIP.Worker, WRK);
                        this._dataStoreService.setData(GUARDIANSHIP.GuardianPerson, GOP);
                        this._dataStoreService.setData(GUARDIANSHIP.GuardianProperty, GOPT);
                        this._dataStoreService.setData(GUARDIANSHIP.Attorney, ATTY);
                    }
                    this.healthFormReset$.next(true);
                    this.personAddressInput = this.selectedPersonDetails.personbasicdetails.personaddress;
                    if (
                        this.selectedPersonDetails.personbasicdetails
                            .personphonenumber &&
                        this.selectedPersonDetails.personbasicdetails
                            .personphonenumber.length
                    ) {
                        const contact = this.selectedPersonDetails.personbasicdetails.personphonenumber.map(
                            res => {
                                return {
                                    contactnumber: res.phonenumber,
                                    contacttype: res.personphonetypekey,
                                    isactive: 1,
                                    contacttypeid: res.personphonenumberid
                                };
                            }
                        );
                        this.phoneNumber$ = Observable.of(contact);
                        this.phoneNumber = contact;
                    }
                    if (
                        this.selectedPersonDetails.personbasicdetails.personemail &&
                        this.selectedPersonDetails.personbasicdetails.personemail
                            .length
                    ) {
                        const email = this.selectedPersonDetails.personbasicdetails.personemail.map(
                            res => {
                                return {
                                    mailid: res.email,
                                    mailtype: res.personemailtypekey,
                                    isactive: 1,
                                    mailtypeid: res.personemailid
                                };
                            }
                        );
                        this.emailID$ = Observable.of(email);
                        this.emailID = email;
                    }

                    this.addEducationOutputSubject$.next(
                        this.selectedPersonDetails.personbasicdetails
                    );
                    // tslint:disable-next-line:max-line-length
                    this.personRole = (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personroledetails
                        .actor.intakeservicerequestactor.length)
                        ? this.selectedPersonDetails.personroledetails.actor
                            .intakeservicerequestactor
                        : [];
                    this.checkChildStatus('drugexposednewbornflag');
                    if (modal.substances && modal.substances.length) {
                        const substanc = modal.substances.map((item) => {
                            return item.subtancekey;
                        });
                        this.involvedPersonFormGroup.controls['substanceClass'].patchValue(substanc);
                    }

                    this.setFormValues();
                    this.changeRelationShipOnChild = false; // D-07962
                    setTimeout(() => {
                        this.personRole.forEach((item, i) => {
                            this.relationShipToRO({ value: item.intakeservicerequestpersontypekey }, i);
                            this.changeRelationShipOnChild = true; // D-07962
                        });
                    }, 100);
                    if (
                        this.selectedPersonDetails &&
                        this.selectedPersonDetails.personbasicdetails.userphoto !==
                        null
                    ) {
                        this.editImagesShow = this.selectedPersonDetails.personbasicdetails.userphoto;
                    } else {
                        this.editImagesShow =
                            '../../../../../assets/images/ic_silhouette.png';
                        this.userProfile =
                            '../../../../../assets/images/ic_silhouette.png';
                    }
                }
                if (this.selectedPersonDetails.personroledetails && this.selectedPersonDetails.personroledetails.actor.intakeservicerequestactor
                    && this.selectedPersonDetails.personroledetails.actor.intakeservicerequestactor.length) {
                    const personRoleRA = this.selectedPersonDetails.personroledetails.actor.intakeservicerequestactor.filter((item) => item.intakeservicerequestpersontypekey === 'RA');
                    if (personRoleRA && personRoleRA.length) {
                        this.isContactSerach = true;
                    } else {
                        this.isContactSerach = false;
                    }
                }
                // edit person address disable delete btn 72 hours
                if (this.selectedPersonDetails.personbasicdetails.personaddress.length > 0) {
                    this.selectedPersonDetails.personbasicdetails.personaddress.forEach((item, i) => {
                        this.selectedPersonDetails.personbasicdetails.personaddress[i]['address1'] = this.selectedPersonDetails.personbasicdetails.personaddress[i]['address'];
                        this.selectedPersonDetails.personbasicdetails.personaddress[i]['Address2'] = this.selectedPersonDetails.personbasicdetails.personaddress[i]['address2'];
                        const totalTime = this.calculateHours(item.insertedon);
                        if (totalTime > 72) {
                            this.selectedPersonDetails.personbasicdetails.personaddress[i]['disableDeleteBtn'] = 'yes';
                        } else {
                            this.selectedPersonDetails.personbasicdetails.personaddress[i]['disableDeleteBtn'] = 'no';
                        }
                    });
                }
            });
        if (this.agency === 'DJS') {
            this.isDobRequired(modal.Role);
            if (modal.Role !== 'Youth') {
                this.isNotPersonYouth = this.isDjs ? true : false;
                this.isAddressReq = false;
                // if (modal.relationship === 'father' || modal.relationship === 'guardian' || modal.relationship === 'mother') {
                //     this.isAddressReq = true;
                // }
                if (!modal.dob) {
                    this.involvedPersonFormGroup.patchValue({ isdobunknown: true });
                } else {
                    this.involvedPersonFormGroup.patchValue({ isdobunknown: false });
                }
            } else {
                this.isAddressReq = true;
                this.isNotPersonYouth = false;
            }
            this.selectedDob = modal.dob ? new Date(modal.dob) : null;
            if (modal.dateofdeath) {
                this.dodPresent = new Date(modal.dateofdeath);
                this.getAgeInYearsAndMonth(this.dodPresent, this.selectedDob);
            } else {
                this.getAgeInYearsAndMonth(this.currentdate, this.selectedDob);
            }
        }
        this.isPersonNew = false;
        this.isImageHide = false;
        this.afterImageCropeHide = false;
        this.addEditLabel = text;
        // this.isEditPersonSaveBtn = modal.reported;
        (<any>$('#intake-addperson')).modal('show');
        if (this.isDjs) {
            (<any>$('#add-Reporter-click')).click();
        } else {
            (<any>$('#profile-click')).click();
        }
        this.getYouthAddress();
    }
    // edit person address calculate hours
    calculateHours(insertDate) {
        const date1 = new Date(insertDate);
        const date2 = new Date();
        const timeDiff = Math.abs(date2.getTime() - date1.getTime());
        const temptotalTime = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const totalTime = temptotalTime * 24;
        return totalTime;
    }
    clearPerson() {
        this.involvedPersonFormGroup.reset();
        this.personAddressForm.reset();
        this.formalSupportForm.reset();
        this.personAddressInput = [];
        this.phoneNumber = [];
        this.phoneNumber$ = Observable.empty();
        this.emailID = [];
        this.emailID$ = Observable.empty();
        this.imageChangedEvent = Object.assign({});
        this.isDefaultPhoto = true;
        this.isImageLoadFailed = false;
        this.isImageHide = false;
        this.isReportAdult = false;
        this.beofreImageCropeHide = false;
        this.afterImageCropeHide = false;
        this.educationFormReset$.next(true);
        this.healthFormReset$.next(true);
        this.workFormReset$.next(true);
        this.addEducation.personeducation = [];
        this.addEducation.personaccomplishment = [];
        this.addEducation.personeducationvocation = [];
        this.addEducation.personeducationtesting = [];
        // this.involvedrepresentative = [];
        this.personPayeeDetails = [];
        this.representative = [];
        this.representativegroup.reset();
        this.addEducationOutputSubject$.next(this.addEducation);
        this.contactPersonInfo = [];
        this.inFormalSuport = [];
        this.dobRequired = true;
        this.showDobRequired = true;
        this.youthAge = '';
        this.storeMaritalStatus = '';
        this._dataStoreService.setData(GUARDIANSHIP.Worker, null);
        this._dataStoreService.setData(GUARDIANSHIP.GuardianPerson, null);
        this._dataStoreService.setData(GUARDIANSHIP.GuardianProperty, null);
        this._dataStoreService.setData(GUARDIANSHIP.Attorney, null);
        this._dataStoreService.setData(GUARDIANSHIP.Funeral, null);
        this._dataStoreService.setData(GUARDIANSHIP.CodeStatus, null);
        this._dataStoreService.setData(GUARDIANSHIP.HSSClinetID, null);
        this.guardianshipTabReset$.next(true);
        this.isAddressUpdate = false;
        this.endDateReason = false;
    }
    navigateNext(modal) {
        if (modal === 'health') {
            (<any>$('#health-click')).click();
        }
        if (modal === 'guardianship') {
            (<any>$('#guardianship-click')).click();
        }
        if (modal === 'add-informal-supports') {
            (<any>$('#add-informal-supports-click')).click(); // D-07945
        }
        if (modal === 'educational') {
            (<any>$('#educational-click')).click();
        }
        if (modal === 'contacts') {
            (<any>$('#add-contacts-click')).click();
        }
        if (modal === 'address') {
            (<any>$('#add-address-click')).click();
        } else if (modal === 'role') {
            (<any>$('#add-Reporter-click')).click();
        } else if (modal === 'survey') {
            (<any>$('#add-Survey-click')).click();
        }
    }
    dodChange() {
        this.involvedPersonFormGroup.controls[
            'dateofdeath'
        ].valueChanges.subscribe(res => {
            if (!res) {
                this.involvedPersonFormGroup.controls['isapproxdod'].reset();
            }
        });
    }
    deletePerson() {
        // if (this.isDjs) {
        this._commonHttpService
            .create(
                { actorid: this.involevedPerson.actorid, objecttype: 'servicecase', intakeserviceid: this.id },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .DeletePerson
            )
            .subscribe(
                response => {
                    if (response) {
                        this._alertService.success(
                            'Person deleted successfully'
                        );
                        this.getInvolvedPerson();
                        (<any>$('#delete-popup')).modal('hide');
                    }
                },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        // }
        // else {
        // this._service
        // .patch(
        //     this.id,
        //     { actorid: this.involevedPerson.actorid, objecttype: 'servicecase' },
        //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
        //         .DeletePerson
        // )
        // .subscribe(
        //     response => {
        //         if (response) {
        //             this._alertService.success(
        //                 'Person deleted successfully'
        //             );
        //             this.getInvolvedPerson();
        //             (<any>$('#delete-popup')).modal('hide');
        //         }
        //     },
        //     error => {
        //         this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        //     }
        // );
        // }
    }

    declineDelete() {
        (<any>$('#delete-popup')).modal('hide');
    }

    confirmDelete(id, model) {
        this.involevedPerson = model;
        (<any>$('#delete-popup')).modal('show');
        this.clearPerson();
    }
    private getProviderList() {
        this.entityList$ = this._commonHttpService.getArrayList(
            {
                where: {
                    servicetype: null,
                    service: null,
                    servicesubtype: null,
                    county: null,
                    zipcode: null,
                    provider: null,
                    providerTaxId: null,
                    distance: null,
                    pagenumber: null,
                    pagesize: null,
                    count: null,
                    childcharacteristic: null
                },
                method: 'post'
            },
            'provider/search'
        ).map((res) => {
            if (res && res['data'] && res['data'].length) {
                res['data'].map((item) => {
                    // console.log(item);
                    this.providerName[item.providerid] = item.providername;
                });
            }
            return res['data'].map((item) =>
                new DropdownModel({
                    text: item.providername,
                    value: item.providerid
                }));
        });
        this.entityList$.subscribe((item) => {
            if (item && item.length) {
                item.map((res) => {
                    this.providerName[res.value] = res.text;
                });
            }
        });
    }
    private getCjamsUserList() {
        this.cjamsUserList$ = this._commonHttpService.getArrayList(
            { where: { pagenumber: null, pagesize: null, county: this._dataStoreService.getData('countyid') }, method: 'post' },
            'People/personrepresentativeworkersearch'
        ).map((res) => {
            if (res && res.length) {
                res.map((item) => {
                    this.cjamsUserName[item.securityusersid] = item.fullname;
                });
            }
            return res.map((item) =>
                new DropdownModel({
                    text: item.fullname,
                    value: item.securityusersid
                }));
        });
        this.cjamsUserList$.subscribe((item) => {
            if (item && item.length) {
                item.map((res) => {
                    this.cjamsUserName[res.value] = res.text;
                });
            }
        });
    }

    private loadDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .EthnicGroupTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .GenderTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .LivingArrangementTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .LanguageTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .RaceTypeUrl + '?filter'
            ),
            // this._commonHttpService.getArrayList(
            //     {
            //         where: { activeflag: 1, teamtypekey: this._authService.getAgencyName() },
            //         method: 'get',
            //         nolimit: true
            //     },
            //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
            //         .RelationshipTypesUrl  + '?filter'
            // ),
            // this._commonHttpService.getArrayList(
            //     {
            //         where: {
            //             activeflag: 1,
            //             datypeid: (this._authService.isCW() && this.da_type) ? this.da_typeid : null
            //         },
            //         method: 'get',
            //         nolimit: true
            //     },
            //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
            //         .UserActorTypeUrl + '?filter'
            // ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .StateListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .MartialStatusTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'typedescription',
                    // D-07178 Start
                    where: {
                        teamtypekey: this.teamtypekey
                    }
                    // D-07178 End
                },
                'admin/religiontype/list?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    activeflag: 1
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .RaceTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    activeflag: 1,
                    order: 'typedescription'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PersonAddressUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    filter: {}
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PhoneTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    filter: {}
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .EmailTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 19,
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 20,
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 27,
                        teamtypekey: null
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 27,
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'countyname asc'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
            ),
            this._commonHttpService.getArrayList({
                nolimit: true,
                where: { referencetypeid: 89 }, method: 'get'
            },
                'referencevalues?filter'),
            this._commonHttpService.getArrayList({
                nolimit: true,
                where: {
                    tablename: 'RepresentativePayeeType',
                    teamtypekey: 'AS'
                }, method: 'get'
            }, 'referencetype/gettypes?filter'
            ),
            this._commonHttpService.getArrayList({
                nolimit: true,
                where: {
                    tablename: 'RepresentativeType',
                    teamtypekey: 'AS'
                }, method: 'get'
            }, 'referencetype/gettypes?filter'
            )
        ])
            .map(result => {
                if (result && result.length) {
                    result[18].map((item) => {
                        this.repPayTyes[item.ref_key] = item.description;
                    });
                    console.log(this.repPayTyes);
                }
                return {
                    ethinicities: result[0].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.ethnicgrouptypekey
                            })
                    ),
                    genders: result[1].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gendertypekey
                            })
                    ),
                    livingArrangements: result[2].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.livingarrangementtypekey
                            })
                    ),
                    primaryLanguages: result[3].map(
                        res =>
                            new DropdownModel({
                                text: res.languagetypename,
                                value: res.languagetypeid
                            })
                    ),
                    races: result[4].map(
                        res =>
                            new CheckboxModel({
                                text: res.typedescription,
                                value: res.racetypekey,
                                isSelected: false
                            })
                    ),
                    // relationShipToRAs: result[5].map(
                    //     res =>
                    //         new DropdownModel({
                    //             text: res.description,
                    //             value: res.relationshiptypekey
                    //         })
                    // ),
                    // roles: result[6].map(
                    //     res => {
                    //         return {
                    //             text: res.typedescription,
                    //             value: res.actortype,
                    //             rolegrp: res.rolegroup
                    //         };
                    //     }
                    // ),
                    states: result[5].map(
                        res =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    maritalstatus: result[6].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.maritalstatustypekey
                            })
                    ),
                    religionkey: result[7].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.religiontypekey
                            })
                    ),
                    racetype: result[8].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.racetypekey
                            })
                    ),
                    addresstype: result[9].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personaddresstypekey
                            })
                    ),
                    phonetype: result[10].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personphonetypekey
                            })
                    ),
                    emailtype: result[11].map(
                        res =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.personemailtypekey
                            })
                    ),
                    livingsituation: result[12].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    licensedfacility: result[13].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    languagetypes: result[14].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    aslanguagetypes: result[15].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    counties: result[16].map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyname
                            })
                    ),
                    alienStatus: result[17].map(
                        (res) => new DropdownModel({
                            text: res.value_text,
                            value: res.ref_key
                        })),
                    representativePayee: result[18].map(
                        (res) => new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                    ),
                    payeeType: result[19].map(
                        (res) => new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                    )
                };
            })
            .share();
        this.ethinicityDropdownItems$ = source.pluck('ethinicities');
        this.genderDropdownItems$ = source.pluck('genders');
        this.livingArrangementDropdownItems$ = source.pluck(
            'livingArrangements'
        );
        this.primaryLanguageDropdownItems$ = source.pluck('primaryLanguages');
        // this.relationShipToRADropdownItems$ = source.pluck('relationShipToRAs');
        // this.roleDropdownItems$ = source.pluck('roles');
        this.stateDropdownItems$ = source.pluck('states');
        this.maritalDropdownItems$ = source.pluck('maritalstatus');
        this.religionDropdownItems$ = source.pluck('religionkey');
        this.racetypeDropdownItems$ = source.pluck('racetype');
        this.addresstypeDropdownItems$ = source.pluck('addresstype');
        this.phoneTypeDropdownItems$ = source.pluck('phonetype');
        this.emailTypeDropdownItems$ = source.pluck('emailtype');
        this.livingSituationTypes$ = source.pluck('livingsituation');
        this.licensedfacilityType$ = source.pluck('licensedfacility');
        this.languageTypes$ = source.pluck('languagetypes');
        this.asLanguageTypes$ = source.pluck('aslanguagetypes');
        this.countyDropDownItems$ = source.pluck('counties');
        this.alienStatusDropDownItems$ = source.pluck('alienStatus');
        this.representativePayeeType$ = source.pluck('representativePayee');
        this.repPayeeType$ = source.pluck('payeeType');
        this.phoneTypeDropdownItems$.subscribe((res) => {
            if (res) {
                res.forEach(type => {
                    this.phoneTypeDescription[type.value] = type.text;
                });
            }
        });
        this.selectedState = 'MD';
    }
    getCommonDropdowns() {
        // const actortypeUrl = this.isDjs ? NewUrlConfig.EndPoint.Intake.ActorTypeListUrl + '?filter' : NewUrlConfig.EndPoint.Intake.UserActorTypeUrl + '?filter';


        this.getRoleList();
        this.getRelationList();
    }
    getRelationList() {
        console.log('case workder Relation itreation count', this.RELATION_TO_RA_COUNT);
        this._commonHttpService.getArrayList(
            {
                where: { activeflag: 1, teamtypekey: this._authService.getAgencyName() },
                method: 'get',
                nolimit: true
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                .RelationshipTypesUrl + '?filter'
        ).subscribe(data => {
            if (data && data.length) {
                this.setRelationList(data);
            } else {
                if (this.RELATION_TO_RA_COUNT < AppConstants.ITRETOR.POLING_COUNT) {
                    this.RELATION_TO_RA_COUNT++;
                    this.getRelationList();
                }
            }
        });
    }

    setRelationList(data) {
        if (data && data.length) {
            this.relationShipToRADropdownItems$ = data.map(res => {
                return new DropdownModel({
                    text: res.description,
                    value: res.relationshiptypekey
                });
            });
            this._dataStoreService.setData('relation', this.relationShipToRADropdownItems$);
            this.relationShipToRADropdownItems = this._dataStoreService.getData('relation');
        }
    }
    getRoleList() {
        console.log('case worker Role itration count', this.ROLE_ITREATION_COUNT);
        this._commonHttpService.getArrayList(
            {
                where: {
                    activeflag: 1,
                    datypeid: (this._authService.isCW() && this.da_type) ? this.da_typeid : null
                },
                method: 'get',
                nolimit: true
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                .UserActorTypeUrl + '?filter'
        ).subscribe(data => {
            if (data && data.length) {
                this.setRoleList(data);
            } else {
                if (this.ROLE_ITREATION_COUNT < AppConstants.ITRETOR.POLING_COUNT) {
                    this.ROLE_ITREATION_COUNT++;
                    this.getRoleList();
                }
            }

        });
    }

    setRoleList(data) {
        this.roleDropdownItems$ = data.map(res => {
            return {
                text: res.typedescription,
                value: res.actortype,
                rolegrp: res.rolegroup
            };
        });
        this._dataStoreService.setData('role', this.roleDropdownItems$);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
    private initiateFormGroup() {
        this.involvedPersonFormGroup = this._formBuilder.group({
            Lastname: ['', Validators.required],
            Firstname: ['', Validators.required],
            Middlename: [''],
            suffix: [''],
            primarylanguage: ['ENG'],
            otherprimarylanguage: [null],
            secondarylanguage: [null],
            Dob: [Validators.required],
            dateofdeath: [''],
            isapproxdod: [''],
            isapproxdob: [''],
            isdobunknown: [''],
            safehavenbabyflag: [''],
            everbeenadoptedflag: [''],
            age: [''],
            Gender: ['', Validators.required],
            religiontypekey: null,
            maritalstatustypekey: [null],
            Dangerous: [''],
            dangerAddress: [''],
            race: [null],
            SSN: [null],
            mdm_id: [null],
            Ethicity: [null],
            occupation: [null],
            stateid: [null],
            aliasname: [null],
            source: [null],
            potentialSOR: [''],
            eDLHistory: [''],
            dMH: [''],
            Race: [null],
            Address: [''],
            address1: [''],
            Zip: [''],
            City: [''],
            State: [null],
            County: [null],
            DangerousWorkerReason: [''],
            DangerousAddressReason: [''],
            tribalassociation: [null],
            height: [''],
            weight: [''],
            tattoo: [''],
            PhyMark: [''],
            DangerousselfReason: [''],
            Dangerousself: [null, [Validators.required]],
            Dangerousworker: [null, [Validators.required]],
            ismentalimpair: [null, [Validators.required]],
            ismentalillness: [null, [Validators.required]],
            mentalimpairdetail: [''],
            mentalillnessdetail: [''],
            Pid: [''],
            iscollateralcontact: [false],
            ishousehold: ['', [!this._authService.isDJS() ? Validators.required : Validators.pattern('')]],
            drugexposednewbornflag: [false],
            fetalalcoholspctrmdisordflag: [false],
            sexoffenderregisteredflag: [false],
            probationsearchconductedflag: [false],
            otherSubstance: [''],
            substanceClass: [''],
            needs: [''],
            strengths: [''],
            livingsituationkey: [null],
            licensedfacilitykey: [null],
            otherlicensedfacility: [null],
            livingsituationdesc: [null],
            otherreligion: [''],
            alienregistrationtext: [null],
            alienstatustypekey: [null],
            citizenalenageflag: [null],
            isqualifiedalien: [null],
            verificationremarks: [null]
            // issafehaven: [false]
        });
        this.involvedPersonFormGroup.addControl(
            'personRole',
            this._formBuilder.array([this.createFormGroup(false)])
        );

        this.addAliasForm = this._formBuilder.group({
            AliasFirstName: [''],
            AliasLastName: ['']
        });
        this.personAddressForm = this._formBuilder.group({
            danger: ['', [Validators.required]],
            dangerreason: ['', [Validators.required]],
            addresstype: ['', Validators.required],
            address1: ['', Validators.required],
            Address2: [''], // DJS-009 : Removed 'Validators.required' - Address line 2 should not be a mandatory field
            zipcode: ['', Validators.required],
            state: ['', Validators.required],
            city: ['', Validators.required],
            county: [''],
            startDate: [''],
            endDate: [''],
            changereason: [''],
            addressstatus: [''],
            personaddresssubtypekey: ['']
        });
        if (this.agency === 'DJS') {
            this.personAddressForm.get('state').disable();
            this.personAddressForm.get('city').disable();
            this.personAddressForm.get('county').disable();
            this.involvedPersonFormGroup.get('Race').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('Race').updateValueAndValidity();
            this.involvedPersonFormGroup.get('Ethicity').setValidators([Validators.required]);
            this.involvedPersonFormGroup.get('Ethicity').updateValueAndValidity();
        }
        this.personPhoneForm = this._formBuilder.group({
            contactnumber: ['', Validators.required],
            // ismobile: ['', Validators.required],
            contacttype: ['', Validators.required]
        });
        this.personEmailForm = this._formBuilder.group({
            // EmailID: ['', [ValidationService.mailFormat, Validators.required]],
            // EmailType: ['', Validators.required]
            EmailID: ['', [ValidationService.mailFormat]],
            EmailType: ['']

        });
        this.informalForm = this._formBuilder.group({
            supportername: ['', Validators.required],
            description: ['']
        });
        this.formalSupportForm = this._formBuilder.group({
            personsupporttypekey: [''],
            description: ['']
        });
        this.representativegroup = this._formBuilder.group({
            personid: [''],
            personrepresentativeworkerid: [null],
            personrepresentativepersonid: [null],
            entityid: [null],
            persontypekey: [''],
            representativetypekey: [''],
            startdate: [null],
            enddate: [null]
        });
    }
    private buildPersonRoleForm(x, relation?: string): FormGroup {
        if (this.isCW) {
            return this._formBuilder.group({
                rolekey: x.intakeservicerequestpersontypekey,
                isprimary: x.isprimary ? 'true' : 'false',
                relationshiptorakey: relation,
                hidden: x.actorrelationship ? false : true,
                description: ['']
            });
        }
        return this._formBuilder.group({
            rolekey: x.intakeservicerequestpersontypekey,
            isprimary: x.isprimary ? 'true' : 'false',
            relationshiptorakey: x.actorrelationship
                ? x.actorrelationship.relationshiptypekey
                : null,
            hidden: x.actorrelationship ? false : true,
            description: ['']
        });
    }
    closeSurvey() {
        (<any>$('#intake-addperson')).modal('hide');
    }

    checkChildStatus(control) {
        const value = this.involvedPersonFormGroup.get(control).value;
        if (value) {
            const dob = this.involvedPersonFormGroup.get('Dob').value;
            let ageDays = 0;
            if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
                const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
                ageDays = moment().diff(rCDob, 'days');
            }
            if (ageDays >= 31) {
                this._alertService.warn('Person must be 30 days or less in age');
                this.involvedPersonFormGroup.get(control).setValue(false);
            } else {
                if (control === 'drugexposednewbornflag') {
                    this.isSubstance = value;
                    this.involvedPersonFormGroup.get('otherSubstance').reset();
                    this.involvedPersonFormGroup.get('substanceClass').reset();
                    // this.isSubstance = !this.isSubstance;
                } else {
                    // this.isSubstance = false;
                }
            }
        }
        this.isSubstance = (control === 'drugexposednewbornflag') ? value : this.isSubstance;
        console.log(value);
    }

    calculateAge(dob) {
        // const dob = this.selectedPerson.dob;
        let age = 0;
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            age = moment().diff(rCDob, 'years');
            if (this.isCW) {
                if (age <= 17) {
                    if (this.storeMaritalStatus === '' || this.storeMaritalStatus === null || this.storeMaritalStatus === undefined) {
                        this.involvedPersonFormGroup.controls['maritalstatustypekey'].setValue('S');
                    }
                } else {
                    if (this.storeMaritalStatus === '' || this.storeMaritalStatus === null || this.storeMaritalStatus === undefined) {
                        this.involvedPersonFormGroup.controls['maritalstatustypekey'].setValue('');
                    }
                }
            } else {
                if (age <= 15) {
                    this.involvedPersonFormGroup.controls['maritalstatustypekey'].setValue('S');
                } else {
                    this.involvedPersonFormGroup.controls['maritalstatustypekey'].setValue('');
                }
            }
            const days = moment().diff(rCDob, 'days');
            this.enableSafeHaven = (days <= 10);
            this.enableSEN = (days <= 30);
        }
        return age;
    }

    private loadSubstance() {
        const babysubstanceList = ['BOTH', 'BPD', 'BPCP', 'BMTD', 'BMJA', 'BHOI', 'BESY', 'BCOC', 'BBS', 'BAS'];
        const source = this._commonHttpService.getArrayList(
            {
                where: { teamtypekey: 'CW', referencetypeid: 55 },
                method: 'get',
                nolimit: true
            },
            CommonUrlConfig.EndPoint.Intake.adultScreenRiskMeasure
        ).map((result) => {
            return {
                response: result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                ),
                babylist: result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                ).filter(
                    (item) => babysubstanceList.includes(item.value)
                )
            };
        }).share();
        this.substanceClass$ = source.pluck('response');
        this.babySubstanceClass$ = source.pluck('babylist');
    }
    changeSubtance(item) {
        this.substances = item.map((res) => {
            return {
                subtancekey: res,
                othersubtance: ''
            };
        });
    }

    filterRoles(item): boolean {
        if (this.selectedPersonsage < 22) {
            if (item.rolegrp === 'C' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        } else if (this.selectedPersonsage >= 22) {
            if (item.rolegrp === 'A' || item.rolegrp === null) {
                return true;
            } else {
                return false;
            }
        }
    }
    getvalidRole(isalleged, role) {
        if (this.isalleged && role.intakeservicerequestpersontypekey === 'AV') {
            return 'Victim';
        } else if (this.isalleged && role.intakeservicerequestpersontypekey === 'AM') {
            return 'Maltreator';
        } else {
            return role.typedescription;
        }
    }
    representativePersnNames(modal) {
        if (modal && modal.length) {
            this.representativegroup.controls['personrepresentativepersonid'].disable();
            modal.map((item) => {
                this.representativeName[item.personid] = item.firstname + ' ' + item.lastname;
            });
        }
    }
    editUnkPerson(person) {
        this.selectedUnkPerson = person;
        (<any>$('#intake-findperson')).modal('show');
    }

    deleteUnkPerson(person) {
        this.involvedUnkPerson = this.involvedUnkPerson.filter(item => item.id !== person.id);
        this.editInvolvedUnkPerson('delete');
    }
    toggleSearchModal() {
        this.showPersonSearch = !this.showPersonSearch;
    }

    filterrelationshipByRole(role) {
        const childrelationList = ['BIOBR', 'BGSISTR', 'BFRND', 'BFRNDX', 'DACRCHLD', 'fosterchild', 'FRND', 'GFRND',
            'GFRNDX', 'HLFBR', 'HLFSISTR', 'LEGLBR', 'LGLSISTR', 'MTNLCN', 'MATNLNPW',
            'MATNLNC', 'NBHR', 'NORLTN', 'OTHER', 'PRNTLCN', 'PRNTLNPW', ' PRNTLNC', 'PUTCHLD', 'RELOTHR', 'RESDNT', 'SELF', 'SNFOPAR', 'STPBR', 'STPSISTR',
            'STUDNT', 'UNKNWN'];
        if (this.agency === 'CW') {
            switch (role) {
                case 'child':
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems.filter(item => childrelationList.includes(item.value));
                    break;

                default:
                    this.relationShipToRADropdownItems$ = this.relationShipToRADropdownItems.filter(item => true);
                    break;
            }
        }
    }
    test(event) {
        console.log(event, this.involvedPersonFormGroup.value);
    }

    copyAddress(involvedPerson: InvolvedPerson, action) {
        if (action === 1) {
            this.personAddressInput = this.copypersonAddressInput;
            this.isnoCopyAddress = true;
            this.addPerson(involvedPerson);
            (<any>$('#address-add-popup')).modal('hide');
        } else {
            this.isnoCopyAddress = false;
            this.personAddressInput = [];
            this.addPerson(involvedPerson);
            (<any>$('#address-add-popup')).modal('hide');
        }
    }


    setcitizenalenageflag(event) {
        console.log(event, this.involvedPersonFormGroup.value);
        this.involvedPersonFormGroup.patchValue({
            isqualifiedalien: (event.value === '1') ? 1 : 0
        });
    }

    showQualifiedAlienQuestion(): boolean {
        let toReturn = false;

        const dobControl = this.involvedPersonFormGroup.controls['Dob'];
        if (dobControl && this.agency === 'CW') {

            if (dobControl.value) {

                if (this.calculateAge(dobControl.value) <= 18) {
                    toReturn = true;
                }
            }
        }
        return toReturn;
    }
    // reset collateral chkbox
    resetCollateralChkbox() {
        if (this.isCW) {
            this.involvedPersonFormGroup.get('iscollateralcontact').reset();
            this.resetRoleTabRadioBtn(null);
        }
    }
    // reset roletab radio btn
    resetRoleTabRadioBtn(paramValue = null) {
        if (this.isCW) {
            let value = false;
            if (paramValue !== null) {
                value = paramValue;
            } else {
                value = this.involvedPersonFormGroup.get('iscollateralcontact').value;
            }
            const radioBtnAry = ['ishousehold', 'ismentalillness', 'ismentalimpair', 'Dangerousworker', 'Dangerousself'];
            if (value) {
                radioBtnAry.forEach(ctrlName => {
                    this.involvedPersonFormGroup.get(ctrlName).clearValidators();
                    this.involvedPersonFormGroup.get(ctrlName).updateValueAndValidity();
                });
                this.isCheckedCollateral = false;
                this.involvedPersonFormGroup.get('ishousehold').reset();
            } else {
                radioBtnAry.forEach(ctrlName => {
                    this.involvedPersonFormGroup.get(ctrlName).setValidators([Validators.required]);
                    this.involvedPersonFormGroup.get(ctrlName).updateValueAndValidity();
                });
                this.isCheckedCollateral = true;
            }
        }
    }

    filterPerson(ishousehold: number) {
        this.isHouseholdActive = ishousehold === 1 ? true : false;
        this.involevedPerson$ = Observable.of(this.involvedPersonList.filter(person => person.ishousehold === ishousehold));
    }

    filterPersonCount(ishousehold: number) {
        if (this.involvedPersonList === undefined) { return 0; }
        return this.involvedPersonList.filter(person => person.ishousehold === ishousehold).length;
    }

    getServiceCaseRole(person: InvolvedPerson) {
        const roleName = person.roles.filter(role => role.intakeservicerequestpersontypekey === 'LG' || role.intakeservicerequestpersontypekey === 'CHILD');
        if (roleName.length > 0) {
            return roleName[0].typedescription;
        } else {
            return undefined;
        }

    }

    sortingNameOnList() {
        this.involvedPersonList = _.sortBy(this.involvedPersonList, item => {
            return item.lastname;
        });
        this.involevedPerson$ = Observable.of(this.involvedPersonList);
    }
    sortByRole() {
        const youthFilter = _.chain(this.involvedPersonList).filter((item) => {
            return item.rolename === 'Youth';
        }).sortBy('fullName').value();
        const victimFilter = _.chain(this.involvedPersonList).filter((item) => {
            return item.rolename === 'Victim';
        }).sortBy('fullName').value();
        const parentFilter = _.chain(this.involvedPersonList).filter((item) => {
            return item.rolename === 'parent-or-guard';
        }).sortBy('fullName').value();
        let others = _.filter(this.involvedPersonList, (item) => {
            return item.rolename !== 'Victim' && item.rolename !== 'Youth' && item.rolename !== 'parent-or-guard';
        });
        others = _.without(others, undefined);
        this.involvedPersonList = [...youthFilter, ...parentFilter, ...victimFilter, ...others];
        this.involevedPerson$ = Observable.of(this.involvedPersonList);
    }
    populateCurrentDate() {
        const radioBtnValue = this.personAddressForm.value.addressstatus;
        if (radioBtnValue === '1') {
            this.personAddressForm.patchValue({
                startDate: new Date()
            });
        }
        if (radioBtnValue === '0') {
            this.personAddressForm.patchValue({
                endDate: new Date()
            });
        }
    }
    patchAddressData(value) {
        this.personAddressForm.patchValue({
            address: '',
            Address2: '',
            state: '',
            city: '',
        });
        if ('FOSHOME' === value || 'GRPHOME' === value || 'ILP' === value
            || 'PRVPLC' === value || 'RETC' === value || 'SJD' === value ||
            'SHELHOM' === value || 'TLP' === value) {
            this._commonHttpService
                .getPagedArrayList(
                    {
                        method: 'get',
                        where: { placementadmissiontype: 'DET', personid: this._dataStoreService.getData('PersonId') },
                    },
                    CommonUrlConfig.EndPoint.PERSON.ADDRESS.Placementaddress + '?filter'
                )
                .subscribe(
                    (result) => {
                        if (result.data.length) {
                            this.personAddressForm.patchValue({
                                address1: result.data[0].addressline1,
                                Address2: result.data[0].addressline2,
                                state: result.data[0].state,
                                city: result.data[0].city,
                            });
                        }
                    });
        }
    }
    checkaccesstoproceed(value) {
        // console.log(value);
        // console.log(this.activepersondetails);
        if (value === 'IHSFP' && this.activepersondetails.relationship === 'Child') {
            this._commonHttpService.getPagedArrayList(new PaginationRequest({
                where: {
                    personid: this.activepersondetails.personid,
                    servicecaseid: this.activepersondetails.servicecaseid
                },
                method: 'get'
            }), 'Caseassignments/validateprogramarea?filter').subscribe((result: any) => {
                if (result.count !== '0') {
                    this.outhomestatus = true;
                    this.outhomemessage = 'Out of home placement is not ended.'
                } else {
                    this.outhomestatus = false;
                    this.outhomemessage = '';
                }
            });


        }
    }
}
