import { Component, OnInit, Input } from '@angular/core';
import { WRK, GuardianshipSearch } from '../_entities/guardianship.modal';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { GUARDIANSHIP } from '../_entities/guardianship.const';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { InvolvedPersonSearchResponse } from '../../_entities/involvedperson.data.model';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'guardian-person',
    templateUrl: './guardian-person.component.html',
    styleUrls: ['./guardian-person.component.scss']
})
export class GuardianPersonComponent implements OnInit {
    @Input() guardianSearch$ = new Subject<GuardianshipSearch>();
    personGuardianForm: FormGroup;
    personGuardianList: WRK[] = [];
    index: number;
    personList$: Observable<DropdownModel[]>;
    id: string;
    personId: string;
    personName: string[] = [];
    isSaveBtnDisabled: boolean;
    searchPerson: InvolvedPersonSearchResponse[] = [];
    constructor(
        private _formBuider: FormBuilder,
        private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute
    ) {}
    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.index = -1;
        this.formIntilizer();
        this.getInvolvedPerson();
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[GUARDIANSHIP.GuardianPerson]) {
                this.personGuardianList = item[GUARDIANSHIP.GuardianPerson];
                if (this.index === -1) {
                    this.endDateValidation(item[GUARDIANSHIP.GuardianPerson]);
                }
            }
            if (item[GUARDIANSHIP.PersonId]) {
                this.personId = item[GUARDIANSHIP.PersonId];
            }
            if (item[GUARDIANSHIP.SelectPersonGuardian]) {
                this.personName = item[GUARDIANSHIP.SelectPersonGuardian];
            }
            if (item[GUARDIANSHIP.PersonSearchDetails] && item[GUARDIANSHIP.PersonSearchDetails].length) {
                this.searchPerson = item[GUARDIANSHIP.PersonSearchDetails].map((res) => {
                    this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                    return res;
                });
                const data = this.searchPerson;
                this.personGuardianForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                if (this.personGuardianForm.controls['typeofperson'].value === 'search') {
                    this.personGuardianForm.controls['guadianpersonid'].disable();
                }
            }
        });
    }
    formIntilizer() {
        this.personGuardianForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    guardianType() {
        this.personGuardianForm.controls['guadianpersonid'].reset();
        this.personGuardianForm.controls['startdate'].reset();
        this.personGuardianForm.controls['enddate'].reset();
        if (this.personGuardianForm.controls['typeofperson'].value === 'search') {
            this.personGuardianForm.controls['guadianpersonid'].disable();
        } else {
            this.personGuardianForm.controls['guadianpersonid'].enable();
        }
    }
    toggleSearch() {
        const searchRequest: GuardianshipSearch = {
            storeid: GUARDIANSHIP.PersonSearchDetails,
            personsearch: true
        };
        this.guardianSearch$.next(searchRequest);
    }
    getInvolvedPerson() {
        const persons = this._commonHttpService
            .getPagedArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    page: 1,
                    limit: 10,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .InvolvedPersonList + '?filter'
            )
            .map(item => {
                return {
                    person: item['data'].map(
                        res =>
                            new DropdownModel({
                                text: res.firstname + ' ' + res.lastname,
                                value: res.personid
                            })
                    ),
                    name: item['data'].map(res => {
                        this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                        this._dataStoreService.setData(GUARDIANSHIP.SelectPersonGuardian, this.personName);
                    })
                };
            }).share();
            this.personList$ = persons.pluck('person');
    }
    savePerson(personGuardian: WRK) {
        personGuardian.guardianpersontypekey = 'GOP';
        personGuardian.personid = this.personId;
        if (this.index === -1) {
            this.personGuardianList.push(personGuardian);
        } else {
            this.personGuardianList[this.index] = personGuardian;
        }
        this.index = -1;
        this._dataStoreService.setData(GUARDIANSHIP.GuardianPerson, this.personGuardianList);
        this.personGuardianForm.controls['guadianpersonid'].enable();
        this.personGuardianForm.controls['typeofperson'].enable();
        this.personGuardianForm.reset();
    }
    cancelPerson() {
        this.index = -1;
        this.endDateValidation(this.personGuardianList);
        this.personGuardianForm.controls['guadianpersonid'].enable();
        this.personGuardianForm.controls['typeofperson'].enable();
        this.personGuardianForm.reset();
    }
    editPerson(modal, i) {
        this.index = i;
        this.personGuardianForm.patchValue(modal);
        this.personGuardianForm.controls['guadianpersonid'].disable();
        this.personGuardianForm.controls['typeofperson'].disable();
        this.isSaveBtnDisabled = false;
    }
    deletePerson(i) {
        this.personGuardianList.splice(i, 1);
        this._dataStoreService.setData(GUARDIANSHIP.GuardianPerson, this.personGuardianList);
        this.endDateValidation(this.personGuardianList);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
