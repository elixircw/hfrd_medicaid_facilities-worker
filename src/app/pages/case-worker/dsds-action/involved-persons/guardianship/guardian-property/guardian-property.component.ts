import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { WRK, GuardianshipSearch } from '../_entities/guardianship.modal';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { GUARDIANSHIP } from '../_entities/guardianship.const';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { InvolvedPersonSearchResponse } from '../../_entities/involvedperson.data.model';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'guardian-property',
    templateUrl: './guardian-property.component.html',
    styleUrls: ['./guardian-property.component.scss']
})
export class GuardianPropertyComponent implements OnInit {
    @Input() guardianSearch$ = new Subject<GuardianshipSearch>();
    addedPersonList = [];
    propertyGuardianForm: FormGroup;
    propertyGuardianList: WRK[] = [];
    index: number;
    searchPerson: InvolvedPersonSearchResponse[] = [];
    personList$: Observable<DropdownModel[]>;
    id: string;
    personId: string;
    personName: string[] = [];
    isSaveBtnDisabled: boolean;
    constructor(
        private _formBuider: FormBuilder,
        private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.index = -1;
        this.formIntilizer();
        this.getInvolvedPerson();
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[GUARDIANSHIP.GuardianProperty]) {
                this.propertyGuardianList = item[GUARDIANSHIP.GuardianProperty];
                if (this.index === -1) {
                    this.endDateValidation(item[GUARDIANSHIP.GuardianProperty]);
                }
            }
            if (item[GUARDIANSHIP.PersonId]) {
                this.personId = item[GUARDIANSHIP.PersonId];
            }
            if (item[GUARDIANSHIP.SelectWorkerGuardian]) {
                this.personName = item[GUARDIANSHIP.SelectWorkerGuardian];
            }
            if (item[GUARDIANSHIP.PropertySearchDetails] && item[GUARDIANSHIP.PropertySearchDetails].length) {
                this.searchPerson = item[GUARDIANSHIP.PropertySearchDetails].map((res) => {
                    this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                    return res;
                });
                const data = this.searchPerson;
                this.propertyGuardianForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                if (this.propertyGuardianForm.controls['typeofperson'].value === 'search') {
                    this.propertyGuardianForm.controls['guadianpersonid'].disable();
                }
            }
        });
    }
    formIntilizer() {
        this.propertyGuardianForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    guardianType() {
        this.propertyGuardianForm.controls['guadianpersonid'].reset();
        this.propertyGuardianForm.controls['startdate'].reset();
        this.propertyGuardianForm.controls['enddate'].reset();
        if (this.propertyGuardianForm.controls['typeofperson'].value === 'search') {
            this.propertyGuardianForm.controls['guadianpersonid'].disable();
        } else {
            this.propertyGuardianForm.controls['guadianpersonid'].enable();
        }
    }
    toggleSearch() {
        const searchRequest: GuardianshipSearch = {
            storeid: GUARDIANSHIP.PropertySearchDetails,
            personsearch: true
        };
        this.guardianSearch$.next(searchRequest);
    }
    getInvolvedPerson() {
        const persons = this._commonHttpService
            .getPagedArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    page: 1,
                    limit: 10,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .InvolvedPersonList + '?filter'
            )
            .map(item => {
                return {
                    person: item['data'].map(
                        res =>
                            new DropdownModel({
                                text: res.firstname + ' ' + res.lastname,
                                value: res.personid
                            })
                    ),
                    name: item['data'].map(res => {
                        this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                        this._dataStoreService.setData(GUARDIANSHIP.SelectWorkerGuardian, this.personName);
                    })
                };
            }).share();
            this.personList$ = persons.pluck('person');
    }
    saveProperty(property: WRK) {
        property.guardianpersontypekey = 'GOPT';
        property.personid = this.personId;
        if (this.index === -1) {
            this.propertyGuardianList.push(property);
        } else {
            this.propertyGuardianList[this.index] = property;
        }
        this.index = -1;
        this._dataStoreService.setData(GUARDIANSHIP.GuardianProperty, this.propertyGuardianList);
        this.propertyGuardianForm.reset();
        this.propertyGuardianForm.controls['guadianpersonid'].enable();
        this.propertyGuardianForm.controls['typeofperson'].enable();
    }
    cancelPerson() {
        this.index = -1;
        this.propertyGuardianForm.controls['guadianpersonid'].enable();
        this.propertyGuardianForm.controls['typeofperson'].enable();
        this.propertyGuardianForm.reset();
        this.endDateValidation(this.propertyGuardianList);
    }
    editProperty(modal, i) {
        this.index = i;
        this.propertyGuardianForm.patchValue(modal);
        this.propertyGuardianForm.controls['guadianpersonid'].disable();
        this.propertyGuardianForm.controls['typeofperson'].disable();
        this.isSaveBtnDisabled = false;
    }
    deleteProperty(i) {
        this.propertyGuardianList.splice(i, 1);
        this._dataStoreService.setData(GUARDIANSHIP.GuardianProperty, this.propertyGuardianList);
        this.endDateValidation(this.propertyGuardianList);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
