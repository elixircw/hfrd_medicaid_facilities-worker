import { Component, OnInit, Input } from '@angular/core';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../../@core/services/auth.service';
import { AppConstants } from '../../../../../@core/common/constants';
import { GuardianshipTabInfo, GuardianshipSearch } from './_entities/guardianship.modal';
import { Subject } from 'rxjs/Subject';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'guardianship',
    templateUrl: './guardianship.component.html',
    styleUrls: ['./guardianship.component.scss']
})
export class GuardianshipComponent implements OnInit {
    token: AppUser;
    @Input() guardianshipTabReset$ = new Subject<boolean>();
    @Input() guardianSearch$ = new Subject<GuardianshipSearch>();
    selectedGuardianshipSection: GuardianshipTabInfo = new GuardianshipTabInfo();
    guardianshipSections: GuardianshipTabInfo[] = [];
    constructor(private _authService: AuthService) {}

    ngOnInit() {
        this.token = this._authService.getCurrentUser();
        const teamTypeKey = this.token.user.userprofile.teamtypekey;

        if (teamTypeKey === AppConstants.AGENCY.AS) {
            this.guardianshipSectionInfo();
            if (this.guardianshipSections && this.guardianshipSections.length) {
                this.selectedGuardianshipSection = this.guardianshipSections[0];
            }
        }
        this.guardianshipTabReset$.subscribe((item) => {
            if (item) {
                if (this.guardianshipSections && this.guardianshipSections.length) {
                    this.showGuardianshipSection(this.guardianshipSections[0]);
                  }
                }
            });
        }
    showGuardianshipSection(guardianshipSection) {
        this.guardianshipSections.forEach(
            section => (section.isActive = false)
        );
        guardianshipSection.isActive = true;
        this.selectedGuardianshipSection = guardianshipSection;
    }
    private guardianshipSectionInfo() {
        this.guardianshipSections = [
            {
                id: 'guardian-person',
                name: 'Guardian of Person',
                isActive: true
            },
            {
                id: 'guardian-property',
                name: 'Guardian of Property',
                isActive: false
            },
            { id: 'attorney', name: 'Attorney', isActive: false },
            { id: 'worker', name: 'Worker', isActive: false },
            { id: 'funeral', name: 'Funeral', isActive: false },
            {id: 'hhs-code-status', name: 'HSS Clinet ID & Code Status', isActive: false}
        ];
    }
}
