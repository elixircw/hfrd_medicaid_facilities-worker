import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { WRK, GuardianshipSearch } from '../_entities/guardianship.modal';
import {
    DataStoreService,
    CommonHttpService
} from '../../../../../../@core/services';
import { GUARDIANSHIP } from '../_entities/guardianship.const';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { InvolvedPersonSearchResponse } from '../../_entities/involvedperson.data.model';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attorney',
    templateUrl: './attorney.component.html',
    styleUrls: ['./attorney.component.scss']
})
export class AttorneyComponent implements OnInit {
    @Input() guardianSearch$ = new Subject<GuardianshipSearch>();
    addedPersonList = [];
    attorneyForm: FormGroup;
    attorneyList: WRK[] = [];
    index: number;
    personList$: Observable<DropdownModel[]>;
    id: string;
    personId: string;
    personName: string[] = [];
    isSaveBtnDisabled: boolean;
    searchPerson: InvolvedPersonSearchResponse[] = [];
    constructor(
        private _formBuider: FormBuilder,
        private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute
    ) {}
    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.index = -1;
        this.formIntilizer();
        this.getInvolvedPerson();
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[GUARDIANSHIP.Attorney]) {
                this.attorneyList = item[GUARDIANSHIP.Attorney];
                if (this.index === -1) {
                    this.endDateValidation(item[GUARDIANSHIP.Attorney]);
                }
            }
            if (item[GUARDIANSHIP.PersonId]) {
                this.personId = item[GUARDIANSHIP.PersonId];
            }
            if (item[GUARDIANSHIP.SelectWorkerGuardian]) {
                this.personName = item[GUARDIANSHIP.SelectWorkerGuardian];
            }
            if (item[GUARDIANSHIP.AttorneySearchDetails] && item[GUARDIANSHIP.AttorneySearchDetails].length) {
                this.searchPerson = item[GUARDIANSHIP.AttorneySearchDetails].map((res) => {
                    this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                    return res;
                });
                const data = this.searchPerson;
                this.attorneyForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                if (this.attorneyForm.controls['typeofperson'].value === 'search') {
                    this.attorneyForm.controls['guadianpersonid'].disable();
                }
            }
        });
    }
    formIntilizer() {
        this.attorneyForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    guardianType() {
        this.attorneyForm.controls['guadianpersonid'].reset();
        this.attorneyForm.controls['startdate'].reset();
        this.attorneyForm.controls['enddate'].reset();
        if (this.attorneyForm.controls['typeofperson'].value === 'search') {
            this.attorneyForm.controls['guadianpersonid'].disable();
        } else {
            this.attorneyForm.controls['guadianpersonid'].enable();
        }
    }
    toggleSearch() {
        const searchRequest: GuardianshipSearch = {
            storeid: GUARDIANSHIP.AttorneySearchDetails,
            personsearch: true
        };
        this.guardianSearch$.next(searchRequest);
    }
    getInvolvedPerson() {
        const persons = this._commonHttpService
            .getPagedArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    page: 1,
                    limit: 10,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .InvolvedPersonList + '?filter'
            )
            .map(item => {
                return {
                    person: item['data'].map(
                        res =>
                            new DropdownModel({
                                text: res.firstname + ' ' + res.lastname,
                                value: res.personid
                            })
                    ),
                    name: item['data'].map(res => {
                        this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                        this._dataStoreService.setData(GUARDIANSHIP.SelectWorkerGuardian, this.personName);
                    })
                };
            }).share();
            this.personList$ = persons.pluck('person');
    }
    saveAttorney(attorney: WRK) {
        attorney.guardianpersontypekey = 'ATTY';
        attorney.personid = this.personId;
        if (this.index === -1) {
            this.attorneyList.push(attorney);
        } else {
            this.attorneyList[this.index] = attorney;
        }
        this.attorneyForm.reset();
        this.attorneyForm.controls['guadianpersonid'].enable();
        this.attorneyForm.controls['typeofperson'].enable();
        this.index = -1;
        this._dataStoreService.setData(GUARDIANSHIP.Attorney, this.attorneyList);
    }
    cancelPerson() {
        this.index = -1;
        this.endDateValidation(this.attorneyList);
        this.attorneyForm.controls['guadianpersonid'].enable();
        this.attorneyForm.controls['typeofperson'].enable();
        this.attorneyForm.reset();
    }
    editAttorney(modal, index) {
        this.index = index;
        this.attorneyForm.patchValue(modal);
        this.attorneyForm.controls['guadianpersonid'].disable();
        this.attorneyForm.controls['typeofperson'].disable();
        this.isSaveBtnDisabled = false;
    }
    deleteAttorney(index) {
        this.attorneyList.splice(index, 1);
        this._dataStoreService.setData(
            GUARDIANSHIP.Attorney,
            this.attorneyList
        );
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
