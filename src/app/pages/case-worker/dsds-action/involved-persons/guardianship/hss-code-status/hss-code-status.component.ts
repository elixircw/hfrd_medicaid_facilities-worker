import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataStoreService } from '../../../../../../@core/services';
import { GUARDIANSHIP } from '../_entities/guardianship.const';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'hss-code-status',
    templateUrl: './hss-code-status.component.html',
    styleUrls: ['./hss-code-status.component.scss']
})
export class HssCodeStatusComponent implements OnInit {
    statusCodeDeatils$: Observable<DropdownModel[]>;
    hssClientForm: FormGroup;
    statusForm: FormGroup;
    notesForm: FormGroup;
    personId: string;
    constructor(
        private _commonHttpService: CommonHttpService,
        private _formBuilder: FormBuilder,
        private _dataStoreService: DataStoreService
    ) {}

    ngOnInit() {
        this.loadDropDown();
        this.formIntilizer();
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[GUARDIANSHIP.HSSClinetID]) {
                this.hssClientForm.patchValue({hhsclientid: item[GUARDIANSHIP.HSSClinetID].hhsclientid});
                this.notesForm.patchValue({notes: item[GUARDIANSHIP.HSSClinetID].notes});
            }
            if (item[GUARDIANSHIP.CodeStatus]) {
                this.statusForm.patchValue(item[GUARDIANSHIP.CodeStatus]);
            }
            if (item[GUARDIANSHIP.CodeStatus] && item[GUARDIANSHIP.CodeStatus].ref_key) {
                this.statusForm.controls['codestatustypekey'].patchValue(item[GUARDIANSHIP.CodeStatus].ref_key);
            }
            if (item[GUARDIANSHIP.PersonId]) {
                this.personId = item[GUARDIANSHIP.PersonId];
            }
        });
    }
    loadDropDown() {
        this.statusCodeDeatils$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        tablename: 'GuardianCodeStatus',
                        teamtypekey: 'AS'
                    },
                    method: 'get'
                },
                'referencetype/gettypes?filter'
            )
            .map(item => {
                return item.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }
    saveHssCode() {
        const statusCode = Object.assign({personid: this.personId}, this.statusForm.value);
        const hssClient = Object.assign({personid: this.personId}, this.hssClientForm.value, this.notesForm.value);
        this._dataStoreService.setData(GUARDIANSHIP.CodeStatus, statusCode);
        this._dataStoreService.setData(GUARDIANSHIP.HSSClinetID, hssClient);
    }
    formIntilizer() {
        this.hssClientForm = this._formBuilder.group({
            hhsclientid: [null]
        });
        this.statusForm = this._formBuilder.group({
            codestatustypekey: [''],
            othercodestatus: ['']
        });
        this.notesForm = this._formBuilder.group({
            notes: ['']
        });
    }
}
