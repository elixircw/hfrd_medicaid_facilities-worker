import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HssCodeStatusComponent } from './hss-code-status.component';

describe('HssCodeStatusComponent', () => {
  let component: HssCodeStatusComponent;
  let fixture: ComponentFixture<HssCodeStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HssCodeStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HssCodeStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
