import { Component, OnInit, Input } from '@angular/core';
import { WRK, GuardianshipSearch } from '../_entities/guardianship.modal';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { GUARDIANSHIP } from '../_entities/guardianship.const';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { InvolvedPersonSearchResponse } from '../../_entities/involvedperson.data.model';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'worker',
    templateUrl: './worker.component.html',
    styleUrls: ['./worker.component.scss']
})
export class WorkerComponent implements OnInit {
    @Input() guardianSearch$ = new Subject<GuardianshipSearch>();
    workerGuardianForm: FormGroup;
    workerGuardianList: WRK[] = [];
    index: number;
    personList$: Observable<DropdownModel[]>;
    searchPerson: InvolvedPersonSearchResponse[] = [];
    id: string;
    personId: string;
    personName: string[] = [];
    isSaveBtnDisabled: boolean;

    constructor(
        private _formBuider: FormBuilder,
        private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute
    ) {}
    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.index = -1;
        this.formIntilizer();
        this.getInvolvedPerson();
        this._dataStoreService.currentStore.subscribe(item => {
            if (item[GUARDIANSHIP.Worker]) {
                this.workerGuardianList = item[GUARDIANSHIP.Worker];
                if (this.index === -1) {
                    this.endDateValidation(item[GUARDIANSHIP.Worker]);
                }
            }
            if (item[GUARDIANSHIP.PersonId]) {
                this.personId = item[GUARDIANSHIP.PersonId];
            }
            if (item[GUARDIANSHIP.SelectWorkerGuardian]) {
                this.personName = item[GUARDIANSHIP.SelectWorkerGuardian];
            }
            if (item[GUARDIANSHIP.WorkerSearchDetails] && item[GUARDIANSHIP.WorkerSearchDetails].length) {
                this.searchPerson = item[GUARDIANSHIP.WorkerSearchDetails].map((res) => {
                    this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                    return res;
                });
                const data = this.searchPerson;
                this.workerGuardianForm.controls['guadianpersonid'].patchValue(data[data.length - 1].personid);
                if (this.workerGuardianForm.controls['typeofperson'].value === 'search') {
                    this.workerGuardianForm.controls['guadianpersonid'].disable();
                }
            }
        });
    }
    formIntilizer() {
        this.workerGuardianForm = this._formBuider.group({
            guardianpersontypekey: [''],
            personid: [''],
            guadianpersonid: [''],
            startdate: [''],
            enddate: [''],
            typeofperson: ['']
        });
    }
    guardianType() {
        this.workerGuardianForm.controls['guadianpersonid'].reset();
        this.workerGuardianForm.controls['startdate'].reset();
        this.workerGuardianForm.controls['enddate'].reset();
        if (this.workerGuardianForm.controls['typeofperson'].value === 'search') {
            this.workerGuardianForm.controls['guadianpersonid'].disable();
        } else {
            this.workerGuardianForm.controls['guadianpersonid'].enable();
        }
    }
    toggleSearch() {
        const searchRequest: GuardianshipSearch = {
            storeid: GUARDIANSHIP.WorkerSearchDetails,
            personsearch: true
        };
        this.guardianSearch$.next(searchRequest);
    }
    getInvolvedPerson() {
        const persons = this._commonHttpService
            .getPagedArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    page: 1,
                    limit: 10,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .InvolvedPersonList + '?filter'
            )
            .map(item => {
                return {
                    person: item['data'].map(
                        res =>
                            new DropdownModel({
                                text: res.firstname + ' ' + res.lastname,
                                value: res.personid
                            })
                    ),
                    name: item['data'].map((res) => {
                        this.personName[res.personid] = res.firstname + ' ' + res.lastname;
                        this._dataStoreService.setData(GUARDIANSHIP.SelectWorkerGuardian, this.personName);
                    })
                };
            }).share();
            this.personList$ = persons.pluck('person');
    }
    workerSave(worker: WRK) {
        worker.guardianpersontypekey = 'WRK';
        worker.personid = this.personId;
        if (this.index === -1) {
            this.workerGuardianList.push(worker);
        } else {
            this.workerGuardianList[this.index] = worker;
        }
        this.workerGuardianForm.reset();
        this.workerGuardianForm.controls['guadianpersonid'].enable();
        this.workerGuardianForm.controls['typeofperson'].enable();
        this.index = -1;
        this._dataStoreService.setData(GUARDIANSHIP.Worker, this.workerGuardianList);
    }
    cancelWorker() {
        this.index = -1;
        this.workerGuardianForm.controls['guadianpersonid'].enable();
        this.workerGuardianForm.controls['typeofperson'].enable();
        this.workerGuardianForm.reset();
        this.endDateValidation(this.workerGuardianList);
    }
    editWorker(modal, i) {
        this.index = i;
        this.workerGuardianForm.patchValue(modal);
        this.workerGuardianForm.controls['guadianpersonid'].disable();
        this.workerGuardianForm.controls['typeofperson'].disable();
        this.isSaveBtnDisabled = false;
    }
    deleteWorker(index) {
        this.workerGuardianList.splice(index, 1);
        this._dataStoreService.setData(GUARDIANSHIP.Worker, this.workerGuardianList);
        this.endDateValidation(this.workerGuardianList);
    }
    endDateValidation(dueDate) {
        if (dueDate && dueDate.length) {
            if (dueDate.length && (dueDate[dueDate.length - 1].enddate)) {
                this.isSaveBtnDisabled = false;
            } else {
                this.isSaveBtnDisabled = true;
            }
        } else {
            this.isSaveBtnDisabled = false;
        }
    }
}
