import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { DataStoreService } from '../../../../../@core/services';
import { InvolvedPersonsConstants } from '../_entities/involvedPersons.constants';
import { Employer, Work, EmployActivity } from '../_entities/involvedperson.data.model';
import { School, Vocation } from '../../../../newintake/my-newintake/_entities/newintakeModel';
import { PersonDisabilityService } from '../../../../shared-pages/person-disability/person-disability.service';


@Component({
  selector: 'eighteen21',
  templateUrl: './eighteen21.component.html',
  styleUrls: ['./eighteen21.component.scss']
})
export class Eighteen21Component implements OnInit {

  @Input()
  addEducationOutputSubject$ = new Subject<any>();
  enableForm: boolean;
  monthlyActivityForm: FormGroup;
  addEditLabel: string;
  educationStore: any;
  employerStore: any;
  secondarySchool: any[] = [];
  vocational: any[] = [];
  workStore: any;
  employer: any[] = [];
  activity: EmployActivity[] = [];
  personDisabilities = [];
  school: School[] = [];
  vocation: Vocation[] = [];
  constructor(
    private _dataStoreService: DataStoreService,
    private formBuilder: FormBuilder,
    private _disablityService: PersonDisabilityService
  ) { }

  ngOnInit() {
   this.loadDisabilityList();

   this.addEducationOutputSubject$.subscribe((education) => {
      this.school = education.personeducation ? education.personeducation : [];
      console.log(this.school);
      this.vocation = education.personeducationvocation ? education.personeducationvocation : [];
      console.log(this.vocation);
  });

    // this._dataStoreService.currentStore.subscribe(store => {
    //   this.educationStore = store[InvolvedPersonsConstants.Intake.PersonsInvolved.Educational];
    //   if (this.educationStore.school && this.educationStore.school.length > 0) {
    //     this.secondarySchool = this.educationStore.school.filter(school =>
    //       (school.schoolTypeDescription === 'Non Public - Secondary' ||
    //         school.schoolTypeDescription === 'Secondary' ||
    //         school.schoolTypeDescription === 'Non Public Elementary + Secondary'));

    //     this.vocational = this.educationStore.school.filter(school =>
    //       (school.schoolTypeDescription === 'Post-Secondary/Vocational' ||
    //         school.schoolTypeDescription === 'Vocational'));

    //   }

    //   this.workStore = store[InvolvedPersonsConstants.Intake.PersonsInvolved.Work.Work];
    //   if (this.workStore) {
    //     if (this.workStore.employer && this.workStore.employer.length > 0) {
    //       this.employer = this.workStore.employer.filter(employ => (employ.noofhours >= '80'));
    //     }

    //     if (this.workStore.employactivity && this.workStore.employactivity.length > 0) {
    //       this.activity = this.workStore.employactivity;
    //     }
    //   }

    // });
  }

  loadDisabilityList() {
    if (this._dataStoreService.getData('PersonId')) { // '67ea6733-d9ba-4b39-a52c-b5b52df0b83e'
      this._disablityService.getDisabilityList(this._dataStoreService.getData('PersonId')).subscribe(response => {
        if (response && Array.isArray(response)) {
          this.personDisabilities = response;
        }
      });
    }
  }
}
