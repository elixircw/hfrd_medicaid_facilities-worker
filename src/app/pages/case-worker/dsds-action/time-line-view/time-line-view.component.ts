import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { TimeLineView } from '../../_entities/caseworker.data.model';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'time-line-view',
    templateUrl: './time-line-view.component.html',
    styleUrls: ['./time-line-view.component.scss']
})
export class TimeLineViewComponent implements OnInit {
    id: string;
    daNumber: string;
    timeLineViewList: TimeLineView[];
    isClosed = false;
    constructor(private route: ActivatedRoute, private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private _dataStoreService: DataStoreService,
        private storage: SessionStorageService) {
        this.id =  this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    }
    ngOnInit() {
        this._commonHttpService.getArrayList({}, 'Intakeservicerequests/getdatimeline/' + this.id).subscribe(
            (response) => {
                this.timeLineViewList = response ? response : [];
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );

        const da_status = this.storage.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }
    }

    openWindow(data: TimeLineView) {
        window.open('#/pages/case-worker/' + data.intakeserviceid + '/' + data.servicerequestnumber + '/dsds-action/report-summary', '_blank');
    }
}
