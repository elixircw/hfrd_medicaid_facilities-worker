import { Component, OnInit } from '@angular/core';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { DsdsService } from '../_services/dsds.service';
@Component({
  selector: 'service-foster-care',
  templateUrl: './service-foster-care.component.html',
  styleUrls: ['./service-foster-care.component.scss']
})
export class ServiceFosterCareComponent implements OnInit {
  scFosterCareList: any[];
  id: string;
  danumber: string;
  selectedPlacement: any;
  placementView: boolean;
  isServiceCase = false;
  isShow: string;
  constructor(private _commonService: CommonHttpService, private _dataStoreService: DataStoreService, private _dsdsService: DsdsService) { }

  ngOnInit() {
    this.isServiceCase = this._dsdsService.isServiceCase();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.danumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.getSCfosterCareList();
    this.placementView = false;
  }

  getSCfosterCareList() {
    this._commonService.getSingle(
        {
            'count': -1,
            'page': 1,
            'limit': 50,
            'where': { 'objectid': this.id, 'casenumber': null },
            'method': 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care
            .fostercarereferallistUrl + '?filter'
    )
        .subscribe(result => {
            if (result && result.length) {
                this.scFosterCareList = result;
            }
        });
}

getPlacementDates(log, isentry) {
  if (log && log.length) {
      const event = { date: '', time: '' };
      if (isentry === 1) {
          event.date = (log[0].entry_dt) ? log[0].entry_dt : '';
          event.time = (log[0].entry_tm) ? log[0].entry_tm : '';
      } else {
          event.date = (log[0].exit_dt) ? log[0].exit_dt : '';
          event.time = (log[0].exit_tm) ? log[0].exit_tm : '';
      }
      return event.date + ' ' + event.time;
  } else {
      return '';
  }
}

showPlacementInfo(placement: any) {
    this.selectedPlacement = placement;
    this.placementView = true;
}
showList() {
    this.placementView = false;
}
toggleClient(modal) {
      this.isShow = modal;
    }

}
