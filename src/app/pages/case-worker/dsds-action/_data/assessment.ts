export const assessmentData = {
    liftdomainfunction: {
        Q1: '0',
        Q2: '0',
        Q3: '0',
        Q4: '0',
        ldffamily: '0',
        Q6: '0',
        Q7: '0',
        Q8: '0',
        Q9: '0',
        Q10: '0',
        Q11: '0',
        Q12: '0',
        ldflegal: '0',
        Q14: '0',
        Q15: '0',
        Q16: '0',
        Q17: '0',
        Q18: '0'
    },
    liftdomainfunction2: {
        Q12: '0',
        Q16: '0',
        Q17: '0',
        Q18: '0'
    },
    childandenvironmentstrength: {
        Q1: '3',
        Q2: '3',
        Q4: '3',
        Q5: '3',
        Q6: '3',
        Q7: '3',
        Q8: '3',
        Q9: '3',
        Q10: '3',
        Q11: '3',
        Q12: '3',
        Q13: '3',
        Q14: '3'
    },
    childandenvironmentstrength2: {
        Q3: '3',
        Q15: '3'
    },
    childemotinalneeds: {
        Q1: '0',
        Q2: '0',
        Q3: '0',
        Q4: '0',
        Q5: '0',
        Q6: '0',
        cbesubstanceuse: '0',
        Q8: '0',
        Q9: '0',
        Q10: '0',
        Q11: '0'
    },
    childriskbehaviour: {
        Q1: '0',
        Q2: '0',
        Q3: '0',
        Q4: '0',
        crbsexaggression: '0',
        Q6: '0',
        crbrunaway: '0',
        Q8: '0',
        crbfiresetting: '0',
        Q10: '0',
        Q11: '0',
        Q12: '0'
    },
    Surveyculturalfactors: {
        Q1: '0',
        Q2: '0',
        Q3: '0',
        Q4: '0'
    },
    surveytrauma: {
        Q1: '0',
        Q2: '0',
        Q3: '0',
        Q4: '0',
        Q5: '0',
        Q6: '0',
        Q7: '0',
        Q8: '0',
        Q9: '0',
        Q10: '0',
        Q11: '0',
        Q12: '0',
        disruptionsInCaregivingAttachmentLosses: '0'
    },
    surveystress: {
        Q1: '0',
        Q2: '0',
        Q3: '0',
        Q4: '0',
        Q5: '0',
        Q6: '0'
    }
};
