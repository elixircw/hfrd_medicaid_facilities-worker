import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PlacementRoutingModule } from './placement-routing.module';
import { PlacementComponent } from './placement.component';
import { PermanencyPlanComponent } from './permanency-plan/permanency-plan.component';
import { MatSelectModule, MatInputModule, MatFormFieldModule, MatExpansionModule, MatDatepickerModule, MatCheckboxModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { PlacementGapComponent } from './placement-gap/placement-gap.component';
import { PlacementAddEditComponent } from './placement-add-edit/placement-add-edit.component';
import {
    MatCardModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTableModule,
    MatTabsModule
  } from '@angular/material';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { PlacementGapService } from './placement-gap/placement-gap.service';
@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        PlacementRoutingModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatExpansionModule,
        A2Edatetimepicker,
        AgmCoreModule,
        PaginationModule,
        MatCardModule,
        MatListModule,
        MatNativeDateModule,
        MatRadioModule,
        MatTableModule,
        MatTabsModule,
        SharedDirectivesModule
    ],
    declarations: [PlacementComponent, PermanencyPlanComponent, PlacementAddEditComponent],

    providers: [DatePipe, PlacementGapService]
})
export class PlacementModule {}
