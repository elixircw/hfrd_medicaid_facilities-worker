import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoAdoptiveFamilyComponent } from './info-adoptive-family.component';

describe('InfoAdoptiveFamilyComponent', () => {
  let component: InfoAdoptiveFamilyComponent;
  let fixture: ComponentFixture<InfoAdoptiveFamilyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoAdoptiveFamilyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoAdoptiveFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
