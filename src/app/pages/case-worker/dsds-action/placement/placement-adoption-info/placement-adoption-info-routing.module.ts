import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlacementAdoptionInfoComponent } from './placement-adoption-info.component';

const routes: Routes = [
  {
    path: '',
    component: PlacementAdoptionInfoComponent,
    children: [
      // { path: 'placement-add-edit', component: PlacementAddEditComponent },
      { path: 'adoptive-family', loadChildren: './info-adoptive-family/info-adoptive-family.module#InfoAdoptiveFamilyModule' },
      { path: 'disruption', loadChildren: './info-disruption/info-disruption.module#InfoDisruptionModule' },
      { path: 'subsidy-review', loadChildren: './info-subsidy-reviews/info-subsidy-reviews.module#InfoSubsidyReviewsModule' },

    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlacementAdoptionInfoRoutingModule { }
