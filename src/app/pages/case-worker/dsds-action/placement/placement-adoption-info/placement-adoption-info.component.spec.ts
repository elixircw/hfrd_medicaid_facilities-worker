import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacementAdoptionInfoComponent } from './placement-adoption-info.component';

describe('PlacementAdoptionInfoComponent', () => {
  let component: PlacementAdoptionInfoComponent;
  let fixture: ComponentFixture<PlacementAdoptionInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacementAdoptionInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacementAdoptionInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
