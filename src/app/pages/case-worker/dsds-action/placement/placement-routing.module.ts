import { PlacementComponent } from './placement.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermanencyPlanComponent } from './permanency-plan/permanency-plan.component';
import { AdoptionSubsidyModule } from './placement-adoption/adoption-subsidy/adoption-subsidy.module';

const routes: Routes = [
    {
        path: '',
        component: PlacementComponent,
        children: [
            // { path: 'placement-add-edit', component: PlacementAddEditComponent },
            { path: 'permanency-plan', component: PermanencyPlanComponent },
            { path: 'placement-gap', loadChildren: './placement-gap/placement-gap.module#PlacementGapModule' },
            // { path: 'kinship', loadChildren: './kinship/kinship.module#KinshipModule' },
            { path: 'appla', loadChildren: './appla-process/appla-process.module#ApplaProcessModule' },
            { path: 'adoption', loadChildren: './placement-adoption/placement-adoption.module#PlacementAdoptionModule' },
            { path: 'adoption-info', loadChildren: './placement-adoption-info/placement-adoption-info.module#PlacementAdoptionInfoModule' },
        ]
    }, {
        path: 'adoption-subsidy', loadChildren: './placement-adoption/adoption-subsidy/adoption-subsidy.module#AdoptionSubsidyModule'
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlacementRoutingModule { }
