import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { KinshipRoutingModule } from './kinship-routing.module';
import { KinshipComponent } from './kinship.component';
import { CaseApprovalComponent } from './case-approval/case-approval.component';
import { KinshipReferralComponent } from './kinship-referral/kinship-referral.component';
import { KinshipAssessmentComponent } from './kinship-assessment/kinship-assessment.component';
import { ServiceCaseComponent } from './service-case/service-case.component';
import { CaseClosureComponent } from './case-closure/case-closure.component';
import { MatExpansionModule, MatSelectModule, MatInputModule, MatFormFieldModule, MatDatepickerModule, MatCheckboxModule, MatRadioModule } from '@angular/material';
import { SharedDirectivesModule } from '../../../../../@core/directives/shared-directives.module';

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, KinshipRoutingModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatExpansionModule, MatRadioModule,SharedDirectivesModule],
    declarations: [KinshipComponent, CaseApprovalComponent, KinshipReferralComponent, KinshipAssessmentComponent, ServiceCaseComponent, CaseClosureComponent]
})
export class KinshipModule {}
