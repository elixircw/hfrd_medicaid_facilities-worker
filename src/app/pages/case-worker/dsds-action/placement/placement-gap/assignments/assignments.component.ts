import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { ActivatedRoute, Router } from '@angular/router';

import { AppUser } from '../../../../../../@core/entities/authDataModel';
import { PaginationInfo, PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { DataStoreService, SessionStorageService } from '../../../../../../@core/services';
import { AlertService } from '../../../../../../@core/services/alert.service';
import { AuthService } from '../../../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { GapAssignment, GapDetails, GapSuspension, RouteToSupervisor, SuspensionReasonType, Suspesion, Placement } from '../../_entities/placement.model';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { PlacementGapService } from '../placement-gap.service';
import { FinanceUrlConfig } from '../../../../../finance/finance.url.config';
import { FinanceService } from '../../../../../finance/finance.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'assignments',
    templateUrl: './assignments.component.html',
    styleUrls: ['./assignments.component.scss']
})
export class AssignmentsComponent implements OnInit {
    id: string;
    daNumber: string;
    currentUrl: string;
    placement: any;
    paginationInfo: PaginationInfo = new PaginationInfo();
    assignmentForm: FormGroup;
    approvalStatusForm: FormGroup;
    assignment: GapAssignment = new GapAssignment();
    disClosure: GapDetails;
    suspesionReason: GapSuspension[];
    suspensionReasonType: SuspensionReasonType;
    isSubmitForReview = false;
    roleId: AppUser;
    isSupervisor = false;
    submitStatus: RouteToSupervisor;
    isExistRecord = false;
    suspensionReason: Suspesion = new Suspesion();
    gapAlertMessage: string;
    isApproved = false;
    isEnableComments = false;
    placementSuspensionId: string;
    isInitialized = false;
    toShowDOD: boolean;
    currentdate: Date;
    maxenddate: any;
    changehistory = [];
    adjustment: any[];
    showSuspensionDetail: boolean;
    reportedChild: any;
    overpayments: any[];
    placmentDetails: any;
    providerDetails: any;
    constructor(
        private _alertService: AlertService,
        private route: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _httpService: CommonHttpService,
        private _dataStoreService: DataStoreService,
        private _authService: AuthService,
        private _route: Router,
        private _session: SessionStorageService,
        private _gapService: PlacementGapService,
        private _financeService: FinanceService
    ) { }

    ngOnInit() {
        this._gapService.checkDataAvailability();
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.role.name === 'apcs') {
            this.isSupervisor = true;
            this.showSuspensionDetail = false;
            this.placementSuspensionId = this._session.getItem('Placement-Suspension-Id');
            this._session.setItem('Placement-Suspension-Id', null);
            this.getPageForSupervisor();
            this.getSuspensionReason();

        } else {
            this.showSuspensionDetail = true;
        }
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.assignmentFormInitilize();
        this.assignmentForm.patchValue({servicecaseid : this.id});
        this.isInitialized = true;
        this._dataStoreService.currentStore.subscribe((store) => {
            if (this.isInitialized && store['placement_child']) {
                this.placement = store['placement_child'];
                if (this.placement) {
                    console.log(this.placement);
                    this.getPage();
                    this.getAgreement();
                    this.getSuspensionReason();
                    this.isInitialized = false;
                }
            }
        });
        this.childPlacementList();
    }
    assignmentFormInitilize() {
        this.assignmentForm = this._formBuilder.group({
            suspensionreasontypekey: [''],
            startdate: [null],
            enddate: [null],
            notes: [''],
            isdraft: ['123'],
            suspensiondesc: [''],
            gapid : [null],
            doddate: [null],
            servicecaseid : [null],
            otherreason: [''],
            guardiansubsidyid: [null],
            activeflag: [1],
            gapsuspensionid: null
        });
        this.approvalStatusForm = this._formBuilder.group({
            routingstatus: [''],
            comments: ['']
        });
        
    }
    getPageForSupervisor() {
        this._httpService
        .getPagedArrayList(
            new PaginationRequest({
                 page: 1,
                 limit: 10,
                where: {
                  objectid: this.placementSuspensionId,
                  objecttype: 'suspension'
                },
                method: 'get'
            }),
                'gapdisclosure/getguardianship' + '?filter'
            )
            .subscribe(
                (checkList) => {
                    if (checkList.data.length) {
                        this.disClosure = checkList.data[0];
                        this.assignmentForm.patchValue({gapid : this.disClosure.gapid});
                        this.suspesionReason =  this.disClosure.gapsuspension;
                        // if(this.suspesionReason && this.suspesionReason .length) {
                        //     const length = this.suspesionReason.length;
                        //     this.patchSuspension(this.suspesionReason[length-1]);
                        // }
                        if (this.suspesionReason && this.suspesionReason.length > 0) {
                            this.currentdate = new Date(this.suspensionReason[this.suspesionReason.length - 1].enddate);
                        }
                        if (!this.isApproved) {
                        this.patchAssignment(this.disClosure);
                        }
                    } else {
                        this.gapAlertMessage = 'Please complete disclosure checklist';
                        (<any>$('#gap-placement')).modal('show');
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    getPage() {
        this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: {
                        permanencyplanid: ( this.placement && this.placement.permanencyplanid ) ? this.placement.permanencyplanid : null
                    },
                    method: 'get'
                }),
                'gapdisclosure/getguardianship' + '?filter'
            )
            .subscribe(
                (checkList) => {
                    if (checkList.data.length) {
                        this.disClosure = checkList.data[0];
                        this.assignmentForm.patchValue({gapid : this.disClosure.gapid});
                        this.suspesionReason =  this.disClosure.gapsuspension;
                        if (this.suspesionReason && this.suspesionReason.length > 0) {
                            const lastSuspension = this.suspesionReason[this.suspesionReason.length - 1];
                            if (lastSuspension.enddate) {
                                this.currentdate = new Date(lastSuspension.enddate);
                                this.showSuspensionDetail = this.isSupervisor ? false : true;
                            } else {
                                this.showSuspensionDetail = false;
                            }
                        } else {
                            this.currentdate = new Date();
                        }

                        //  this.patchAssignment(this.disClosure);
                    } else {
                        this.gapAlertMessage = 'Please complete disclosure checklist';
                        (<any>$('#gap-placement')).modal('show');
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    getSuspensionReason() {
        this._httpService.getSingle({}, 'suspensionreasontype/').subscribe((data) => {
            this.suspensionReasonType = data;
        });
    }

    conditionValidation(): boolean {
        if (!this.assignmentForm.value.startdate) {
            this._alertService.error('Discolsure end date should be greater than start date');
            return false;
        }
        /* if (!this.assignmentForm.value.enddate) {
            this._alertService.error('Discolsure end date should be greater than start date');
            return false;
        } */

        if (this.assignmentForm.value.startdate !== null
            && (this.assignmentForm.value.enddate !== null)
            &&  this.assignmentForm.value.enddate < this.assignmentForm.value.startdate) {
            this._alertService.error('Discolsure end date should be greater than start date');
            return false;
        }
        return true;
    }
    patchAssignment(modal) {
        if (!modal.gapdisclosure) {
            this.gapAlertMessage = 'Please complete disclosure checklist';
            (<any>$('#gap-placement')).modal('show');
        // } else if (!modal.gapagreement) {
            // this.gapAlertMessage = 'Please complete agreement';
            // (<any>$('#gap-placement')).modal('show');
            // } else if (!modal.gapannualreview) {
            //     this.gapAlertMessage = 'Please complete annual review';
            //     (<any>$('#gap-placement')).modal('show');
            //
        } else if (modal.gapsuspension && modal.gapsuspension.length > 0) {
            const gapSuspension = modal.gapsuspension.filter( data => ( data.activeflag === 1 || data.activeflag ) );
            if (gapSuspension && gapSuspension.length) {
            this.isExistRecord = true;

            this.suspensionReason = gapSuspension[0];
            this.assignmentForm.patchValue(this.suspensionReason);
            this.approvalStatusForm.patchValue({
                routingstatus: this.suspensionReason.routingstatus ? this.suspensionReason.routingstatus : '',
                comments: this.suspensionReason.comments ? this.suspensionReason.comments : ''
            });
            if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Rejected') {
                this.isApproved = true;
            }
          }
        } else {
            this.isExistRecord = false;
        }
    }

    resetSuspension() {
        this.assignmentForm.enable();
        this.assignmentForm.reset();
        if (this.suspesionReason && this.suspesionReason.length > 0) {
            const lastSuspension = this.suspesionReason[this.suspesionReason.length - 1];
            if (lastSuspension && lastSuspension.enddate) {
                this.currentdate = new Date(lastSuspension.enddate);
                this.showSuspensionDetail = true;
            } else {
                this.showSuspensionDetail = false;
            }
        }
    }
    patchSuspension(suspension, index) {
        this.showSuspensionDetail = true;
        this.suspensionReason = suspension;
        if (index > 0) {
            const lastSuspension = this.suspesionReason[index - 1];
            this.currentdate = new Date(lastSuspension.enddate);
        }
        this.assignmentForm.patchValue(this.suspensionReason);
        this.approvalStatusForm.patchValue({
            routingstatus: this.suspensionReason.routingstatus ? this.suspensionReason.routingstatus : '',
            comments: this.suspensionReason.comments ? this.suspensionReason.comments : ''
        });
        if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Rejected') {
            this.isApproved = true;
        }
    }

    rejectComments(status) {
        if (status === 'Rejected') {
            this.isEnableComments = true;
        } else {
            this.isEnableComments = false;
            this.approvalStatusForm.patchValue({ comments: '' });
        }
    }
    saveAssignment(modal) {
        const validation = this.conditionValidation();
        this.assignment = Object.assign({}, modal);
        this.assignment.gapid = this.disClosure.gapid;
        console.log(this.assignment);
        if (validation) {
            this._httpService.create(this.assignment, 'gapsuspension/add').subscribe(
                (res) => {
                    this._alertService.success('Suspension Details  Submitted for Supervisor Approval');
                    this.resetSuspension();
                    this.getPage();
                },
                (err) => { }
            );
        }
    }
    routingUpdate() {
       const comment = 'Guardianship Suspension Submitted for review';
        this.submitStatus = Object.assign({
            objectid: this.placementSuspensionId,
            eventcode: 'GASR',
            status: this.approvalStatusForm.value.routingstatus,
            comments: this.approvalStatusForm.value.comments,
            notifymsg: comment,
            routeddescription: comment,
            servicecaseid: this.id
        });

        console.log(this.submitStatus);
        if (this.roleId.role.name === 'apcs' && this.approvalStatusForm.value.routingstatus === '') {
            return this._alertService.error('Please select review status!');
        } else {
            console.log(this.submitStatus);
            this._httpService.create(this.submitStatus, 'routing/routingupdate').subscribe(
                (res) => {
                    this._alertService.success('Suspension Details ' + this.approvalStatusForm.value.routingstatus + ' Successfully!');
                    this.isApproved = true;
                    //  this.getPageForSupervisor();
                    // this.resetSuspension();
                    // Todo permenancy plan id is null, so below function redirecting to disclosure alert.
                   // this.getPage();
                },
                (err) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    navigateTo() {
        if (this.disClosure) {
            const gapdisclosure = this.disClosure.gapdisclosure;
            const gapagreement = this.disClosure.gapagreement;
            const gapannualreview = this.disClosure.gapannualreview;
            (<any>$('#gap-placement')).modal('hide');
            if (!gapdisclosure) {
                this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/disclosure-checklist';
            } else if (!gapagreement) {
                this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/agreement';
            } else if (!gapannualreview) {
                this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/annual-reviews';
            }
        } else {
            this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/disclosure-checklist';
        }
        this._route.navigate([this.currentUrl]);
    }

    reasonSelected(event) {
        console.log(event);
        if (event.value === 'DC') {
            this.toShowDOD = true;
        } else {
            this.toShowDOD = false;
        }
    }

    getAgreement() {
        this._httpService
            .getArrayList(
                {
                    method: 'get',
                    page: 1,
                    limit: 10,
                    where: { gapid: this._gapService.getGapId() }
                },
                'gapagreement/list?filter'
            )
            .subscribe(res => {
                if (res && (res instanceof Array)) {
                    const agreementDetail = res[0];
                    this.maxenddate = agreementDetail.enddate;
                } else {
                    this.maxenddate = null;
                }
            });
    }

    childPlacementList() {
        this._httpService
          .getPagedArrayList(
            new PaginationRequest({
              page: 1,
              limit: 10,
              method: 'get',
              where: { servicecaseid: this.id },
            }),
            'placement/getplacementbyservicecase?filter'
            // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          ).subscribe(result => {
            if (result && result.data) {
                if (this._dataStoreService.getData('placed_child') && this._dataStoreService.getData('placed_child').cjamspid) {
                    this.placmentDetails = result.data.find(item => item.cjamspid === this._dataStoreService.getData('placed_child').cjamspid);
                    this.providerDetails = (this.placmentDetails && this.placmentDetails.placements && this.placmentDetails.placements.length && this.placmentDetails.placements[0].providerdetails) ?
                    this.placmentDetails.placements[0].providerdetails : null;
                    if (this.providerDetails) {
                        this.getOverPaymentList(this.providerDetails.provider_id, this._dataStoreService.getData('placed_child').cjamspid);
                    }
                }
            }
        });
      }
      getOverPaymentList(providerID, clientid) {
        this._httpService.getPagedArrayList({
            where: {
              providerid: providerID,
              client_id: clientid
            },
            page: 1,
            limit: null,
            nolimt: true,
            method: 'get'
          }, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.list).subscribe((res: any) => {
            if (res && res.data && res.data.length) {
              this.overpayments = res.data;
            }
          });
      }

    fiscalAudit() {
        if ( this._dataStoreService.getData('placed_child').cjamspid) {
            this.reportedChild = this._dataStoreService.getData('placed_child').cjamspid;
            this._financeService.getFiscalAudit(this.reportedChild, 1, this.overpayments);
            (<any>$('#fiscal-audit')).modal('show');
        }
        /* this._httpService.getPagedArrayList({
          where: {
            receivable_detail_id: null,
            clientid: this._dataStoreService.getData('placed_child').cjamspid
          },
          page: this.paginationInfo.pageNumber,
          limit: this.paginationInfo.pageSize,
          method: 'get'
        }, FinanceUrlConfig.EndPoint.accountsReceivable.audit.auditList).subscribe((result: any) => {
            if (result && result.data && result.data.length > 0) {
              this.changehistory = result.data;
              for (let i = 0;  i < result.data.length; i++) {
                if (result.data[i].adjstment) {
                  this.adjustment = result.data[i].adjstment;
                }
              }
            }
        }); */
      }
}
