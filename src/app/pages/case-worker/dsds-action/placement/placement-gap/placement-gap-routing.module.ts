import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlacementGapComponent } from './placement-gap.component';
import { DisclosureChecklistComponent } from './disclosure-checklist/disclosure-checklist.component';
import { AgreementComponent } from './agreement/agreement.component';
import { AnnualReviewsComponent } from './annual-reviews/annual-reviews.component';
import { AssignmentsComponent } from './assignments/assignments.component';
import { ApplicationComponent } from './application/application.component';

const routes: Routes = [{
  path: '',
  component: PlacementGapComponent,
  children: [
    {
      path: 'disclosure-checklist', component: DisclosureChecklistComponent,
      children: [
        {
          path: 'disability',
          loadChildren: '../../../../shared-pages/person-disability/person-disability.module#PersonDisabilityModule'
        }
      ]
    },
    { path: 'agreement', component: AgreementComponent },
    { path: 'annual-reviews', component: AnnualReviewsComponent },
    { path: 'assignments', component: AssignmentsComponent },
    { path: 'application', component: ApplicationComponent }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlacementGapRoutingModule { }
