import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AppUser } from '../../../../../../@core/entities/authDataModel';
import { DynamicObject, PaginationRequest, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { AlertService } from '../../../../../../@core/services/alert.service';
import { AuthService } from '../../../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { DataStoreService } from '../../../../../../@core/services/data-store.service';
import { Agreement, GapAgreement, Gapagreementrate, GapDetails, Placement, RouteToSupervisor } from '../../_entities/placement.model';
import { PlacementGapService } from '../placement-gap.service';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { SessionStorageService, CommonDropdownsService } from '../../../../../../@core/services';
import * as moment from 'moment';
import { AppConfig } from '../../../../../../app.config';
import { config } from '../../../../../../../environments/config';
import { HttpHeaders } from '@angular/common/http';
import { NgxfUploaderService, FileError } from 'ngxf-uploader';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { ServiceCasePlacementsService } from '../../../service-case-placements/service-case-placements.service';
import { FinanceUrlConfig } from '../../../../../finance/finance.url.config';
import { FinanceService } from '../../../../../finance/finance.service';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'agreement',
    templateUrl: './agreement.component.html',
    styleUrls: ['./agreement.component.scss']
})
export class AgreementComponent implements OnInit {
    agreementGapForm: FormGroup;
    approvalStatusForm: FormGroup;
    agreementRateForm: FormGroup;
    placement: Placement;
    disClosure: GapDetails;
    submitStatus: RouteToSupervisor;
    agreementList: Agreement = new Agreement();
    id: string;
    daNumber: string;
    roleId: AppUser;
    uploadedFile: any = [];
    isSupervisor = false;
    isExistRecord = false;
    isApproved = false;
    agreement: GapAgreement = new GapAgreement();
    maxDate = new Date();
    deleteAttachmentIndex: number;
    isEnableComments = false;
    gapAlertMessage: string;
    // isInitialized = false;
    isShowAgreementForm = false;
    isShowRateForm = false;
    selectedRate: Gapagreementrate;
    newBtnDisabled: boolean;
    agreementDetail: any;
    store: DynamicObject;
    token: any;
    placementAgreementRateId: any;
    showRateOverride: boolean;
    showRateApprovalStatus: boolean;
    rateAction: string;
    childHasActivePlacement: boolean;
    iscaseworker: boolean;
    childdob: Date;
    paginationInfo: PaginationInfo  = new PaginationInfo();
    pageInfo: PaginationInfo  = new PaginationInfo();
    changehistory = [];
    adjustment: any[];
    approvalCode: any;
    overpayments: any[];
    placmentDetails: any;
    providerDetails: any;
    reportedChild: any;
    isSubmitted: boolean;
    constructor(
        private route: ActivatedRoute,
        private _dataStoreService: DataStoreService,
        private _formBuilder: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _route: Router,
        private _session: SessionStorageService,
        private _placementService: PlacementGapService,
        private _uploadService: NgxfUploaderService,
        private _scPlacementService: ServiceCasePlacementsService,
        private _commonddservice: CommonDropdownsService,
        private _financeService: FinanceService
    ) {
        this.store = this._dataStoreService.getCurrentStore();
    }

    ngOnInit() {
        this._placementService.checkDataAvailability();
        this.roleId = this._authService.getCurrentUser();
        this.iscaseworker = this._authService.selectedRoleIs('field');
        if (this.roleId.role.name === 'apcs') {
            this.isSupervisor = true;
            this.placementAgreementRateId = this._session.getItem('Placement-Agreement-Rate-Id');
            this.approvalCode = this._session.getItem(CASE_STORE_CONSTANTS.APPROVAL_EVENT_CODE);
            const ppid = this._session.getItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID);
            this._session.setItem('Placement-Agreement-Rate-Id', null);
            this._session.setItem(CASE_STORE_CONSTANTS.APPROVAL_EVENT_CODE, null);
            if (this.approvalCode === 'GAAR') {
                this.getPageforSupervisor();
            } else {
                this.getPage(ppid);
            }
            console.log('AgreementId', this.placementAgreementRateId);
        }
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.token = this._authService.getCurrentUser();
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.agreementInitialform();
        // this.isInitialized = true;
        // this._dataStoreService.currentStore.subscribe(store => {
        if (this.store['placement_child']) {
            this.placement = this.store['placement_child'];
            if (this.placement) {
                console.log(this.placement);
                this.agreementRateForm.patchValue({
                    providerid: this.placement.providerinfo ? this.placement.providerinfo.providercode : null
                });
                this.getPage(null);
                // this.isInitialized = false;
            }
            this.agreementRateForm.controls['providerid'].disable();
            const child = this._dataStoreService.getData('placed_child');
            this.childdob = new Date(child.dob);
            const dob = moment(child.dob);
            const agreementEndDate = dob.add('years', 18);
            const agreementEndDateminusOneDay = agreementEndDate.subtract('days', 1).format('MM/DD/YYYY');
            this.agreementGapForm.patchValue({ enddate: new Date(agreementEndDateminusOneDay) });
            // this.agreementGapForm.get('enddate').enable();
        }
        if (this.store['placed_child']) {
            const removalInfo = this.store['placed_child'].removalList;
            const placements = this.store['placed_child'].placements;
            const endRemovalInfo = removalInfo.filter(item => item.exitdate);
            const endPlacments = placements.filter(item => item.enddate);
            if (placements  && placements.length) {
                this.childHasActivePlacement = this._scPlacementService.checkForChildHasActivePlacements(placements);
            } else {
                this.childHasActivePlacement = false;
            }
            if (removalInfo && placements && endRemovalInfo && endPlacments
                && removalInfo.length === endRemovalInfo.length
                && placements.length === endPlacments.length) {
                    this.agreementGapForm.patchValue({isplacementenddate: true});
                }
        }
        this.childPlacementList();
        // this.agreementRateForm.controls['paymenttypekey'].disable();

        // });
        // this.agreementGapForm.get('startdate').valueChanges.subscribe(date => {
        //     const currentDate = moment(date).format('MM/DD/YYYY');
        //     const endDate = moment(currentDate).add(1, 'year');
        //     this.agreementGapForm.patchValue({ enddate: endDate.subtract(1, 'days').format('YYYY-MM-DD') });
        // });
    }

    agreementInitialform() {
        this.agreementGapForm = this._formBuilder.group({
            iscomprehensivehomestudy: [null, Validators.requiredTrue],
            iscgawardedcustody: [null, Validators.requiredTrue],
            isplacementenddate: [null, Validators.requiredTrue],
            ischildreceivetca: ['', Validators.required],
            startdate: [new Date(), Validators.required],
            enddate: [null],
            signaturedate: [null],
            guardianonedate: [new Date(), Validators.required],
            guardiantwodate: [null],
            ldssdate: [new Date()],
            tcaamount: [''],
            iscsnotifiedtocustody: [''],
            isfianotified: [''],
            fianotifieddate: [null],
            isrcnotifiedcontact: [''],
            attachment: ['']
        });
        this.agreementRateForm = this._formBuilder.group({
            providerid: [''],
            ratestartdate: [''],
            minratestartdate: [''],
            rateenddate: [''],
            oldenddate: [null],
            maxrateenddate: [''],
            rateapprovaldate: [null],
            updatedon: [null],
            paymentamout: [''],
            negotiateddate: [null],
            isoverride: [null],
            notes: [''],
            status: [''],
            paymenttypekey : [''],
            gapagreementrateid: [null],
            gapagreementid: [null]
        });
        this.approvalStatusForm = this._formBuilder.group({
            routingstatus: [''],
            comments: ['']
        });
    }
    getPageforSupervisor() {


        this._commonHttpService
        .getArrayList(
            {
                page: 1,
                limit: 10,
               where: {
                 objectid: this.placementAgreementRateId,
                 objecttype: 'agreement'
               },
               method: 'get'
            },
            'gapagreement/list?filter'
        )
        .subscribe(res => {
            if (res && (res instanceof Array)) {
                this.agreementDetail = res[0];
                this.patchAgreement(this.agreementDetail);
            }
        });

    }

    getPage(permanencplanid) {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: {
                        intakeserviceid: this.id,
                        // placementid: this.placement.placement_id,
                        permanencyplanid: (permanencplanid) ? permanencplanid : this.placement.permanencyplanid
                    },
                    method: 'get'
                }),
                'gapdisclosure/getguardianship' + '?filter'
            )
            .subscribe(res => {
                if (res.data && res.data.length) {
                    this.disClosure = res.data[0];
                    // this.patchAgreement(this.disClosure);
                    this.getAgreement(res.data[0].gapid);
                } else {
                    this.gapAlertMessage = 'Please complete disclosure checklist';
                    (<any>$('#gap-placement')).modal('show');
                }
            });
    }
    patchAgreement(modal) {
        // if (!modal.gapdisclosure) {
        //     this.gapAlertMessage = 'Please complete disclosure checklist';
        //     (<any>$('#gap-placement')).modal('show');
        // } else if (modal.gapdisclosure[0] && modal.gapdisclosure[0].routingstatus !== 'Approved') {
        //     // } else if (modal.gapagreement && modal.gapagreement.length > 0) {
        //     this.navigateTo();
        // } else {
        // if (modal.gapdisclosure[0] && modal.gapdisclosure[0].routingstatus !== 'Approved') {
        //     this.navigateTo();
        // }
        // this.agreementList = modal.gapagreement[0];
        if (modal) {
        this.agreementList = modal;
            this.newBtnDisabled = true;
            this.isExistRecord = (['Approved', 'Rejected'].includes(modal.routingstatus)) ? false : true;
            this.agreementList.ischildreceivetca = String(this.agreementList.ischildreceivetca);
            this.agreementGapForm.patchValue(this.agreementList);
            if (this.agreementList.gaprate && this.agreementList.gaprate.length > 0) {
                const gapRate = this.agreementList.gaprate[0];
                this.agreementRateForm.patchValue({
                    providerid: gapRate.providercode,
                    ratestartdate: gapRate.startdate,
                    minratestartdate: gapRate.startdate,
                    rateenddate: this._commonddservice.getValidDate(gapRate.enddate),
                    rateapprovaldate: gapRate.rateapprovaldate,
                    paymentamout: gapRate.paymentamout,
                    isoverride: gapRate.isoverride,
                    oldenddate: gapRate.enddate
                });
            }
            this.approvalStatusForm.patchValue({
                // routingstatus: this.agreementList.routingstatus ? this.agreementList.routingstatus : '',
                comments: this.agreementList.comments ? this.agreementList.comments : ''
            });
            if (['Approved', 'Rejected'].includes(this.agreementList.routingstatus)) {
                let rateList = this.agreementList['agreementrate'];
                rateList = rateList ? rateList : [];
                const pending = rateList.some(item => item.status === 'Review');
                this.isApproved = (pending) ? false : true;
                this.rejectComments(this.agreementList.routingstatus);
            }
            this.agreementList = Object.assign({});
            // this.isExistRecord = false;
        }
        // }
    }

   isSameDateAs(date1, date2) {
       date1 = new Date(date1);
       date2 = new Date(date2);
        return (
          date1.getFullYear() === date1.getFullYear() &&
          date1.getMonth() === date2.getMonth() &&
          date1.getDate() === date2.getDate()
        );
      }
    conditionValidation(): boolean {
        if (this.agreementGapForm.getRawValue().enddate !== null && this.agreementGapForm.getRawValue().enddate < this.agreementGapForm.getRawValue().startdate) {
            this._alertService.error('Agreement end date should be greater than start date');
            return false;
        }
        if (this.agreementGapForm.getRawValue().ldssdate !== null && this.agreementGapForm.getRawValue().ldssdate <= this.agreementGapForm.getRawValue().startdate) {
            const isSameDate = this.isSameDateAs(this.agreementGapForm.getRawValue().startdate, this.agreementGapForm.getRawValue().ldssdate);
            if (!isSameDate) {
            this._alertService.error('LDSS Director date should be greater than or equal to start date');
            return false;
            }
        }
        if (
            this.agreementGapForm.getRawValue().enddate !== null &&
            (this.agreementGapForm.getRawValue().enddate < this.agreementRateForm.getRawValue().ratestartdate)
        ) {
            this._alertService.error('Rate start date should not  be greater than agreement end date');
            return false;
        // tslint:disable-next-line:max-line-length
        }
        console.log(this.agreementRateForm.getRawValue().ratestartdate < this.agreementGapForm.getRawValue().startdate);

        if ( this.agreementRateForm.value.ratestartdate && this.agreementGapForm.getRawValue().startdate
        && this.agreementRateForm.getRawValue().ratestartdate < this.agreementGapForm.getRawValue().startdate )  {
            const isSameDate = this.isSameDateAs(this.agreementRateForm.getRawValue().ratestartdate, this.agreementGapForm.getRawValue().startdate);
            if (!isSameDate) {
            this._alertService.error('Rate start date should be greater than agreement start date');
            return false;
            }
        }
        if (this.agreementRateForm.value.ratestartdate && this.agreementRateForm.value.rateenddate) {
            let date = this.agreementRateForm.getRawValue().ratestartdate;
            date = new Date(date);
            date.setDate(date.getDate() + 364);
            if (this.agreementRateForm.value.rateenddate > date) {
                this._alertService.error('Rate end date cannot be greater than 364 days from rate begin date');
                return false;
            }
        }
        return true;
    }
    rejectComments(status) {
        if (status === 'Rejected') {
            this.isEnableComments = true;
        } else {
            this.isEnableComments = false;
            this.approvalStatusForm.patchValue({ comments: '' });
            this.agreementGapForm.disable();
        }
    }
    addAgreement() {
        if (this.childHasActivePlacement) {
            (<any>$('#active-placement')).modal('show');
            return false;
        }

        if (this.isShowRateForm && this.agreementDetail && this.agreementDetail.agreementrate) {
            (<any>$('#rate-change-alert')).modal('show');
            return false;
        }
        const agreeement = this.agreementGapForm.getRawValue();
        try {
            agreeement.tcaamount = parseInt(agreeement.tcaamount, 10);
        } catch (error) {
            agreeement.tcaamount = 0;
        }
        if ((this.agreementGapForm.valid && this.agreementGapForm.enabled) || this.agreementGapForm.disabled) {
            const validation = this.conditionValidation();
            this.agreement = Object.assign(
                {
                    gapid: (this.disClosure && this.disClosure.gapid) ? this.disClosure.gapid : null,
                    servicecaseid: this.id,
                    gapagreementid: (this.agreementDetail) ? this.agreementDetail.gapagreementid : null
                },
                agreeement
            );
            const rateList = (this.agreementDetail && this.agreementDetail.agreementrate) ? this.agreementDetail.agreementrate : [];
            if (rateList.length) {
                rateList.forEach(element => {
                    if (element.status === 'New') {
                        element.providerid = this.disClosure.guardianoneproviderid;
                        // alternateid: this.disClosure.guardianoneproviderid,
                        const startdate = new Date(element.ratestartdate);
                        element.startdate = moment(startdate).format('YYYY-MM-DD');
                        element.enddate = element.rateenddate;
                        element.rateapprovaldate = element.rateapprovaldate;
                        element.paymentamout = element.paymentamout;
                        element.isoverride = element.isoverride;
                        element.paymenttypekey = element.paymenttypekey;
                        element.notes = element.notes;
                        element.negotiateddate = element.negotiateddate;
                        element.status = 'Review';
                    } else {
                        element.enddate = element.rateenddate;
                        element.notes = element.notes;
                        element.status = element.status;
                    }
                });
                this.agreement.attachment = this.uploadedFile;
                this.agreement.gapagreementrate = rateList;
             } else {
            const ratedetails = this.agreementRateForm.getRawValue();
            const agrementRate = {
                providerid: this.disClosure.guardianoneproviderid,
                // alternateid: this.disClosure.guardianoneproviderid,
                startdate: ratedetails.ratestartdate,
                enddate: ratedetails.rateenddate,
                rateapprovaldate: ratedetails.rateapprovaldate,
                paymentamout: ratedetails.paymentamout,
                isoverride: ratedetails.isoverride,
                paymenttypekey: ratedetails.paymenttypekey,
                notes: ratedetails.notes,
                negotiateddate: ratedetails.negotiateddate,
                status: 'Review'
            };
                this.agreement.attachment = this.uploadedFile;
            this.agreement.gapagreementrate = (Array.isArray(this.agreement.gapagreementrate)) ? this.agreement.gapagreementrate : [];
            this.agreement.gapagreementrate.push(agrementRate);
            console.log(this.agreement);
            }
            this.agreement.gapagreementrate = [this.agreement.gapagreementrate.pop()];
            this.agreement.status = 'Review';
            if (validation) {
                console.log(this.agreement);
                this._commonHttpService.create(this.agreement, 'gapagreement/add').subscribe(
                    res => {
                        this._alertService.success('Agreement Submitted for Supervisor Approval');
                        this.getPage(null);
                    },
                    err => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            }
        }
    }

    routingUpdate() {
        this.isSubmitted = true;
        const comment = 'Guardianship Agreement Submitted for review';
        this.submitStatus = Object.assign({
            objectid: this.placementAgreementRateId ? this.placementAgreementRateId : '',
            eventcode: this.approvalCode,
            status: this.approvalStatusForm.value.routingstatus,
            comments: this.approvalStatusForm.value.comments,
            notifymsg: comment,
            routeddescription: comment,
            servicecaseid: this.id
        });
        if (this.roleId.role.name === 'apcs' && this.approvalStatusForm.value.routingstatus === '') {
            this.isSubmitted = false;
            return this._alertService.error('Please select review status!');
        } else {
            this._commonHttpService.create(this.submitStatus, 'routing/routingupdate').subscribe(
                res => {
                    this._alertService.success('Agreement approved successfully!');
                    this.isApproved = true;
                    // this.getPage();
                    if (this.approvalStatusForm.value.routingstatus === 'Approved') {
                        this.approvalStatusForm.disable();
                    }
                },
                err => {
                    this.isSubmitted = false;
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    navigateTo() {
        if (this.disClosure) {
            const gapdisclosure = this.disClosure.gapdisclosure;
            (<any>$('#gap-placement')).modal('hide');
            if (!gapdisclosure || (gapdisclosure[0] && gapdisclosure[0].routingstatus !== 'Approved')) {
                const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/disclosure-checklist';
                this._route.navigate([currentUrl]);
            }
        } else {
            const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/disclosure-checklist';
            this._route.navigate([currentUrl]);
        }
    }
    showCriteria() {
        (<any>$('#gap-complete-criteria')).modal('show');
    }

    selectRate(action, gapRate?, event?) {
        this.isShowRateForm = true;
        if (action === 'Update') {
            this.rateAction = 'Update';
            this.showRateApprovalStatus = true;
            this.showRateOverride = true;
            this.selectedRate = gapRate;
            this.agreementRateForm.patchValue(gapRate);
            this.agreementRateForm.patchValue({
                oldenddate: gapRate.rateenddate
            });
            if (!['New', 'Rejected'].includes(gapRate.status)) {
                this.agreementRateForm.disable();
                this.agreementRateForm.get('rateenddate').enable();
                this.agreementRateForm.get('notes').enable();
            } else {
                this.agreementRateForm.enable();
                this.agreementRateForm.get('ratestartdate').disable();
            }
            this.ratestartdatechange();
        } else if (action === 'Add') {
            this.rateAction = 'Add';
            this.showRateApprovalStatus = false;
            this.showRateOverride = false;
            if (this.agreementDetail && this.agreementDetail.agreementrate && this.agreementDetail.agreementrate.filter(item => item.annualreviewstatus === 'NO').length) {
                this.isShowRateForm = false;
                this._alertService.error('Annual Review Status Is Pending For Existing Rate');
            } else {
                this.isShowRateForm = true;
                this.selectedRate = Object.assign({}, new Gapagreementrate());
                this.agreementRateForm.reset();
            }
            this.agreementRateForm.enable();
            if (this.agreementDetail && this.agreementDetail.agreementrate.length) {
                const length = this.agreementDetail.agreementrate.length;
                const lastrate = this.agreementDetail.agreementrate[length - 1];
                const startdate = new Date(lastrate.rateenddate);
                startdate.setDate(startdate.getDate() + 1);
                this.agreementRateForm.patchValue({
                    ratestartdate: startdate,
                    minratestartdate: startdate,
                    gapagreementid: this.agreementDetail.gapagreementid
                });
            } else {
            this.agreementRateForm.patchValue({
                ratestartdate: this.agreementGapForm.getRawValue().startdate,
                minratestartdate: this.agreementGapForm.getRawValue().startdate,
                gapagreementid: this.agreementDetail.gapagreementid
            });
            }
            this.ratestartdatechange();
            this.agreementRateForm.valueChanges.subscribe((item) => {
                console.log(item);
            });
            this.agreementRateForm.get('ratestartdate').disable();
        } else if (action === 'View') {
            this.rateAction = 'View';
            this.showRateApprovalStatus = true;
            this.showRateOverride = true;
            this.selectedRate = gapRate;
            this.agreementRateForm.patchValue(gapRate);
            this.agreementRateForm.disable();
        } else {
            this.isShowRateForm = false;
        }
    }

    addedittorate() {
        const rateInput = this.agreementRateForm.getRawValue();
        if (rateInput.gapagreementrateid) {
            this.agreementDetail.agreementrate = Array.isArray(this.agreementDetail.agreementrate) ? this.agreementDetail.agreementrate : [];
            this.agreementDetail.agreementrate.forEach(element => {
                if (element.gapagreementrateid === rateInput.gapagreementrateid) {
                    if (rateInput.status === 'Rejected') {
                        element = rateInput;
                    } else {
                        element.rateenddate = rateInput.rateenddate;
                        element.oldenddate = rateInput.oldenddate;
                    }
                    element.status = 'Review';
                }
            });
            // const index = this.agreementDetail.agreementrate.indexOf(item => item.gapagreementrateid === rateInput.gapagreementrateid);
        } else {
            rateInput.providerid = this.disClosure.guardianoneproviderid;
            // rateInput.alternateid = this.disClosure.guardianoneproviderid;
            rateInput.status = 'New';
            this.agreementDetail.agreementrate = Array.isArray(this.agreementDetail.agreementrate) ? this.agreementDetail.agreementrate : [];
            let exist = false;
            this.agreementDetail.agreementrate = this.agreementDetail.agreementrate.map(item => {
                if (item.status === 'New') {
                    exist = true;
                    item = rateInput;
                }
                return item;
            });
            if (!exist) {
                this.agreementDetail.agreementrate.push(rateInput);
            }
        }
        this.isShowRateForm = false;
    }

    saveRate() {
        // this._alertService.success('Rate Saved Successfully');
        const rateInput = this.agreementRateForm.value;
        rateInput.intakeserviceid = this.id;
        rateInput.providerid = this.disClosure.guardianoneproviderid;
        // rateInput.alternateid = this.disClosure.guardianoneproviderid;
        rateInput.startdate = rateInput.ratestartdate;
        rateInput.enddate = rateInput.rateenddate;
        rateInput.rateapprovaldate = rateInput.rateapprovaldate;
        rateInput.status = 'Review';

        const reqparam = {
            gapagreementid: rateInput.gapagreementid,
            servicecaseid: this.id,
            gapagreementrate: [rateInput]

        };
        this._commonHttpService.create(reqparam, 'gapagreementrate/add').subscribe(
            res => {
                this._alertService.success('Rate Saved Successfully');
                this.agreementRateForm.reset();
            },
            err => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    showAgreementForm(mode, agreement) {
        if (!this.newBtnDisabled || mode === 'edit') {
            this.isShowAgreementForm = true;
            // this.agreementGapForm.get('paymenttypekey').disable();
            if ((agreement && agreement.routingstatus === 'Approved') || (!this.isSupervisor && agreement.routingstatus === 'Review')) {
                this.agreementGapForm.disable();
            } else {
                this.agreementGapForm.enable();
                // this.agreementGapForm.get('paymenttypekey').disable();
                this.agreementGapForm.get('enddate').disable();
            }
        } else if (mode === 'view') {
            this.isShowAgreementForm = true;
            this.agreementGapForm.disable();
        }
        this.agreementDetail = agreement;
        this.uploadedFile = ( agreement && agreement.attachments  ) ? agreement.attachments : null;
        this.patchAgreement(agreement);
    }
    showTCA(event) {
        if (event === 'true') {
            this.agreementGapForm.get('tcaamount').setValidators([Validators.required]);
            this.agreementGapForm.get('tcaamount').updateValueAndValidity();
        } else {
            this.agreementGapForm.get('tcaamount').clearValidators();
            this.agreementGapForm.get('tcaamount').updateValueAndValidity();
        }
    }
    private getAgreement(gapId) {
        const storedgapid = this._placementService.getGapId();
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    page: 1,
                    limit: 10,
                    where: { gapid: (storedgapid) ? storedgapid : gapId }
                },
                'gapagreement/list?filter'
            )
            .subscribe(res => {
                if (res && (res instanceof Array)) {
                    this.agreementDetail = res[0];
                    this.patchAgreement(this.agreementDetail);
                } else {
                    this.isShowAgreementForm = true;
                    this.agreementGapForm.enable();
                     this.agreementGapForm.get('enddate').disable();
                    this.isShowRateForm = true;
                    const startdate = new Date();
                    const enddate = new Date();
                    enddate.setDate(enddate.getDate() + 364);
                    this.agreementRateForm.patchValue({
                    ratestartdate: startdate,
                    minratestartdate: startdate,
                    rateenddate: enddate
                    });
                    this.ratestartdatechange();
                    this.agreementRateForm.get('ratestartdate').disable();
                }
            });
    }

    // onDateChange(event) {
    //     var currentDate = moment(event).format('MM/DD/YYYY');
    //     const endDate = moment(currentDate).add(1, 'year');
    //     this.agreementGapForm.patchValue({enddate: endDate.subtract(1, 'days').format('YYYY-MM-DD')});
    // }

    aggStartDateChange() {
        const startdate = this.agreementGapForm.getRawValue().startdate;
        this.agreementGapForm.patchValue({
            guardianonedate: startdate,
            ldssdate: startdate
        });
        this.agreementRateForm.patchValue({
            ratestartdate: startdate
        });
        setTimeout(() => {
            this.ratestartdatechange(true);
        }, 500);
    }
    ratestartdatechange(force?: boolean) {
        let startdate = this.agreementRateForm.getRawValue().ratestartdate;
        const enddate = this.agreementRateForm.getRawValue().rateenddate;
        startdate = new Date(startdate);
        startdate.setDate(startdate.getDate() + 364);
        this.agreementRateForm.patchValue({
            rateenddate: (force) ? startdate : ((enddate) ? enddate : startdate),
            maxrateenddate: startdate,
        });
    }

    uploadFile(file: File | FileError): void {
        if (!(file instanceof Array)) {
            return;
        }
        file.map((item, index) => {
            const fileExt = item.name
                .toLowerCase()
                .split('.')
                .pop();
            if (
                fileExt === 'mp3' ||
                fileExt === 'ogg' ||
                fileExt === 'wav' ||
                fileExt === 'acc' ||
                fileExt === 'flac' ||
                fileExt === 'aiff' ||
                fileExt === 'mp4' ||
                fileExt === 'mov' ||
                fileExt === 'avi' ||
                fileExt === '3gp' ||
                fileExt === 'wmv' ||
                fileExt === 'mpeg-4' ||
                fileExt === 'pdf' ||
                fileExt === 'txt' ||
                fileExt === 'docx' ||
                fileExt === 'doc' ||
                fileExt === 'xls' ||
                fileExt === 'xlsx' ||
                fileExt === 'jpeg' ||
                fileExt === 'jpg' ||
                fileExt === 'png' ||
                fileExt === 'ppt' ||
                fileExt === 'pptx' ||
                fileExt === 'gif'
            ) {
                this.uploadedFile.push(item);
                const uindex = this.uploadedFile.length - 1;
                if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
                    this.uploadedFile[uindex].percentage = 1;
                }

                this.uploadAttachment(uindex);
                const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
                const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
                if (audio_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[uindex].attachmenttypekey = 'Audio';
                } else if (video_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[uindex].attachmenttypekey = 'Video';
                } else {
                    this.uploadedFile[uindex].attachmenttypekey = 'Document';
                }
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error(fileExt + " format can't be uploaded");
                return;
            }
        });
    }
    uploadAttachment(index) {
        console.log('check');
        const workEnv = config.workEnvironment;
        let uploadUrl = '';
        if (workEnv === 'state') {
            uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
                + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber + '&' + 'docsInfo='; // Need to discuss about the docsInfo
            console.log('state', uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
                '&' + 'srno=' + this.daNumber;
            console.log('local', uploadUrl);
        }

        this._uploadService
            .upload({
                url: uploadUrl,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile[index],
                process: true,
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        const doucumentInfo = response.data;
                        doucumentInfo.documentdate = doucumentInfo.date;
                        doucumentInfo.title = doucumentInfo.originalfilename;
                        doucumentInfo.objecttypekey = 'Court';
                        doucumentInfo.rootobjecttypekey = 'Court';
                        doucumentInfo.activeflag = 1;
                        doucumentInfo.servicerequestid = null;
                        this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                        console.log(index, this.uploadedFile[index]);
                    }

                }, (err) => {
                    console.log(err);
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    this.uploadedFile.splice(index, 1);
                }
            );
    }
    deleteAttachment() {
        this.uploadedFile.splice(this.deleteAttachmentIndex, 1);
        (<any>$('#delete-attachment-popup')).modal('hide');
    }

    downloadFile(s3bucketpathname) {
        const workEnv = config.workEnvironment;
        let downldSrcURL;
        if (workEnv === 'state') {
            downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            // 4200
            downldSrcURL = s3bucketpathname;
        }
        console.log('this.downloadSrc', downldSrcURL);
        window.open(downldSrcURL, '_blank');
    }

    confirmDeleteAttachment(index: number) {
        (<any>$('#delete-attachment-popup')).modal('show');
        this.deleteAttachmentIndex = index;
    }


    childPlacementList() {
        this._commonHttpService
          .getPagedArrayList(
            new PaginationRequest({
              page: 1,
              limit: 10,
              method: 'get',
              where: { servicecaseid: this.id },
            }),
            'placement/getplacementbyservicecase?filter'
            // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          ).subscribe(result => {
            if (result && result.data) {
                this.placmentDetails = result.data.find(item => item.cjamspid === this.store['placed_child'].cjamspid);
                this.providerDetails = (this.placmentDetails && this.placmentDetails.placements && this.placmentDetails.placements.length && this.placmentDetails.placements[0].providerdetails) ?
                this.placmentDetails.placements[0].providerdetails : null;
                if (this.providerDetails) {
                this.getOverPaymentList(this.providerDetails.provider_id, this.store['placed_child'].cjamspid);
                }
            }
        });
      }
      getOverPaymentList(providerID, clientid) {
        this._commonHttpService.getPagedArrayList({
            where: {
              providerid: providerID,
              client_id: clientid
            },
            page: 1,
            limit: null,
            nolimt: true,
            method: 'get'
          }, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.list).subscribe((res: any) => {
            if (res && res.data && res.data.length) {
              this.overpayments = res.data;
            }
          });
      }

    fiscalAudit() {
        if ( this.store['placed_child'] && this.store['placed_child'].cjamspid) {
            this.reportedChild = this._dataStoreService.getData('placed_child').cjamspid;
            this._financeService.getFiscalAudit(this.reportedChild, 1, this.overpayments);
            (<any>$('#fiscal-audit')).modal('show');
        }
    }
}
