import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule, MatDatepickerModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatRadioModule, MatSelectModule } from '@angular/material';

import { AgePipe } from '../../../../../@core/pipes/age.pipe';
import { AgreementComponent } from './agreement/agreement.component';
import { AnnualReviewsComponent } from './annual-reviews/annual-reviews.component';
import { AssignmentsComponent } from './assignments/assignments.component';
import { DisclosureChecklistComponent } from './disclosure-checklist/disclosure-checklist.component';
import { PlacementGapRoutingModule } from './placement-gap-routing.module';
import { PlacementGapComponent } from './placement-gap.component';
import { NgxMaskModule } from 'ngx-mask';
import { SharedDirectivesModule } from '../../../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../../../@core/pipes/shared-pipes.module';
import { Title4eModule } from '../../../../title4e/title4e.module';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { AgmCoreModule } from '@agm/core';
import { PaginationModule } from 'ngx-bootstrap';
import { ApplicationComponent } from './application/application.component';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { ServiceCasePlacementsService } from '../../service-case-placements/service-case-placements.service';
import { FiscalAuditModule } from '../../../../finance/fiscal-audit/fiscal-audit.module';
import { FinanceService } from '../../../../finance/finance.service';

@NgModule({
    imports: [
        CommonModule,
        PlacementGapRoutingModule,
        FormMaterialModule,
        NgxMaskModule.forRoot(),
        SharedDirectivesModule,
        SharedPipesModule,
        Title4eModule,
        NgxfUploaderModule.forRoot(),
        PaginationModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD8CRCgWX0eRvC8XfFnaaMPm-36fb2GN9M'
        }),
        FiscalAuditModule
    ],
    declarations: [AgePipe, PlacementGapComponent, AgreementComponent, AnnualReviewsComponent, AssignmentsComponent, DisclosureChecklistComponent, ApplicationComponent],
    providers: [ServiceCasePlacementsService, FinanceService]
})
export class PlacementGapModule {}
