import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacementGapComponent } from './placement-gap.component';

describe('PlacementGapComponent', () => {
  let component: PlacementGapComponent;
  let fixture: ComponentFixture<PlacementGapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacementGapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacementGapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
