import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AppUser } from '../../../../../../@core/entities/authDataModel';
import { PaginationRequest, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { CommonHttpService, DataStoreService, SessionStorageService } from '../../../../../../@core/services';
import { AlertService } from '../../../../../../@core/services/alert.service';
import { AuthService } from '../../../../../../@core/services/auth.service';
import { InvolvedPerson } from '../../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { DisclosureCheckList, GapDetails, Placement, RouteToSupervisor } from '../../_entities/placement.model';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { PlacementGapService } from '../placement-gap.service';
declare let google: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'disclosure-checklist',
    templateUrl: './disclosure-checklist.component.html',
    styleUrls: ['./disclosure-checklist.component.scss']
})
export class DisclosureChecklistComponent implements OnInit {

    placement: any;
    id: string;
    daNumber: string;
    disclosureCheklistForm: FormGroup;
    approvalStatusForm: FormGroup;
    providerSearchForm: FormGroup;
    roleId: AppUser;
    disClosure: GapDetails[];
    disClosuseCheckList: DisclosureCheckList = new DisclosureCheckList();
    isSupervisor = false;
    isExistRecord = false;
    submitStatus: RouteToSupervisor;
    isApproved = false;
    isEnableComments = false;
    isInitialized = false;
    isViewForm: boolean;
    placementGapDisclosureId: string;
    isSuccessorGuardianExists = false;
    currProcess: string;
    selectedViewProvider: any;
    selectedparent: any;
    adoptiveparent1: string;
    parent1providerid: string;
    parent2providerid: string;

    placementStrType: any;
    adoptiveparent2: string;
    fcpaginationInfo: PaginationInfo = new PaginationInfo();
    selectedProvider: any;
    fcProviderSearch: any;
    parent1providername: string;
    parent2providername: string;
    markersLocation = ([] = []);
    zoom: number;
    defaultLat = 39.29044;
    defaultLng = -76.61233;
    paginationInfo: PaginationInfo = new PaginationInfo();
    fcTotal: any;
    childCharacteristics: any[];
    otherLocalDeptmntType: any[];
    bundledPlcmntServicesType: any[];
    genderDropdownItems: any[];
    minAge: number;
    maxAge: number;
    gender: string;
    lat = 51.678418;
    lng = 7.809007;
    showMap: boolean;
    child: any;
    maxDate = new Date();
    showTitle4eFields = false;
    allowsubmit: boolean;
    gapAlertMessage: string;

    constructor(
        private _authService: AuthService,
        private _httpService: CommonHttpService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _router: Router,
        private _dataStoreService: DataStoreService,
        private _alertService: AlertService,
        private _session: SessionStorageService,
        private _placementService: PlacementGapService
    ) { }

    ngOnInit() {
        this._placementService.checkDataAvailability();
        this.initFormGroup();
        this.currProcess = 'search';
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.role.name === 'apcs') {
            this.isSupervisor = true;
            this.disclosureCheklistForm.disable();
            this.placementGapDisclosureId = this._session.getItem('Placement-Gap-Disclosure-Id');
            this._session.setItem('Placement-Gap-Disclosure-Id', null);
            this.getPageforSupervisor();
        }
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.child = this._dataStoreService.getData('placed_child');
        this.disclosureCheklistForm.patchValue({ servicecaseid: this.id });

        this.disclosureCheklistForm.patchValue({ enteredby: this.roleId.user.username });
        this.isInitialized = true;

        if (this.isInitialized && this._dataStoreService.getData('placement_child')) {
            this.placement = this._dataStoreService.getData('placement_child');
            if (this.placement) {
                this.getDisclosureCheckList();
                this.getPage();

                if (this.placement.providerdetails) {
                    this.disclosureCheklistForm.patchValue({
                        guardianoneid: this.placement.providerdetails.providername,
                        guardianoneproviderid: this.placement.providerdetails.provider_id
                    });
                }

                if (this.placement.permanencyplanid) {
                    this.disclosureCheklistForm.patchValue({
                        permanencyplanid: this.placement.permanencyplanid
                    });
                }



                this.isInitialized = false;
            }
        }

        if (this.roleId.role.name === 'apcs') {
            this.getPageforSupervisor();
        }
    }

    routingUpdate() {
        const comment = 'Guardianship Submitted for review';
        this.submitStatus = Object.assign({
            objectid: this.disClosuseCheckList ? this.disClosuseCheckList.gapdisclosureid : '',
            eventcode: 'GADR',
            status: this.approvalStatusForm.value.routingstatus,
            comments: this.approvalStatusForm.value.comments,
            notifymsg: comment,
            routeddescription: comment,
            servicecaseid: this.id
        });
        console.log(this.submitStatus);
        if (!this.disClosuseCheckList.gapdisclosureid) {
            return this._alertService.error('Please select child');
        } else if (this.roleId.role.name === 'apcs' && this.approvalStatusForm.value.routingstatus === 'Review') {
            return this._alertService.error('Please select review status!');
        } else {
            console.log(this.submitStatus);
            this._httpService.create(this.submitStatus, 'routing/routingupdate').subscribe(
                res => {
                    this._alertService.success('Disclosure Checklist ' + this.submitStatus.status + ' Successfully!');
                    this.isApproved = true;
                    // this.getPage();
                },
                err => { }
            );
        }
    }

    getPageforSupervisor() {
        this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 10,
                    where: {
                        objectid: this.placementGapDisclosureId,
                        objecttype: 'disclosure'
                    },
                    method: 'get'
                }),
                'gapdisclosure/getguardianship' + '?filter'
            )
            .subscribe(
                (checkList) => {
                    if (checkList.data && checkList.data.length) {
                        this.disClosure = checkList.data[0];
                        const data = checkList.data[0];
                        this.patchDisclosureCheckList(this.disClosure);
                        if (!this.placement) {
                            this.disclosureCheklistForm.patchValue({
                                guardianoneid: data.guardianoneprovidername,
                                guardianoneproviderid: data.guardianoneproviderid
                            });
                        }
                    } else if (this.roleId.role.name === 'apcs' && checkList.data && checkList.data.length === 0) {
                        this._alertService.warn('Please fill disclosure checklist');
                        this.isApproved = true;
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }
    getPage() {
        this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: {
                        permanencyplanid: (this.placement && this.placement.permanencyplanid) ? this.placement.permanencyplanid : null
                    },
                    method: 'get'
                }),
                'gapdisclosure/getguardianship' + '?filter'
            )
            .subscribe(
                (checkList) => {
                    if (checkList.data && checkList.data.length) {
                        const gapData = checkList.data[0];
                        const gapapplication = (Array.isArray(gapData.gapapplication)) ? gapData.gapapplication[0] : null;
                        if (gapapplication && gapapplication.routingstatus === 'Approved') {
                            this.disClosure = checkList.data[0];
                            const data = checkList.data[0];
                            this.patchDisclosureCheckList(this.disClosure);
                            this._placementService.setGapId(data.gapid);
                        } else {
                            this.gapAlertMessage = 'Please complete Application';
                            (<any>$('#gap-placement')).modal('show');
                        }
                    } else if (this.roleId.role.name === 'apcs' && checkList.data.length === 0) {
                        this._alertService.warn('Please fill disclosure checklist');
                        this.isApproved = true;
                    } else {
                        this.gapAlertMessage = 'Please complete Application';
                        (<any>$('#gap-placement')).modal('show');
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    navigateTo() {
        (<any>$('#gap-placement')).modal('hide');
        const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/application';
        this._router.navigate([currentUrl]);
    }

    patchDisclosureCheckList(model) {
        if (model.gapdisclosure && model.gapdisclosure.length > 0) {
            this.disClosuseCheckList = model.gapdisclosure[0];
            this.approvalStatusForm.patchValue({
                routingstatus: this.disClosuseCheckList.routingstatus ? this.disClosuseCheckList.routingstatus : '',
                comments: this.disClosuseCheckList.comments ? this.disClosuseCheckList.comments : ''
            });
            if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Rejected') {
                this.isApproved = true;
                this.rejectComments(this.approvalStatusForm.value.routingstatus);
            }
            if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Review') {
                this.isExistRecord = true;
                this.disclosureCheklistForm.disable();
            }
        } else {
            this.disClosuseCheckList = Object.assign({});
            this.isExistRecord = false;
        }
        const placement = this._dataStoreService.getData('placement_child');
        let provider = { guardianoneid: null, guardianoneproviderid: null };
        if (placement && placement.providerdetails) {
          provider = {
            guardianoneid: placement.providerdetails.providername,
            guardianoneproviderid: placement.providerdetails.provider_id
          };
        }

        const disData = this.disclosureCheklistForm.getRawValue();
        this.disclosureCheklistForm.patchValue({
            guardianoneproviderid: (model.guardianoneproviderid) ? model.guardianoneproviderid : provider.guardianoneproviderid,
            guardiantwoproviderid: (model.guardiantwoproviderid) ? model.guardiantwoproviderid : '',
            guardianoneid: (model.guardianoneprovidername) ? model.guardianoneprovidername : provider.guardianoneid,
            guardiantwoid: (model.guardiantwoprovidername) ? model.guardiantwoprovidername : '',
            enteredby: (model.enteredby ? model.enteredby : model.applicationenteredby),
            gapid: model.gapid ? model.gapid : null,
            gapdisclosureid: this.disClosuseCheckList ? this.disClosuseCheckList.gapdisclosureid : null,
            disclosuredate: this.disClosuseCheckList ? this.disClosuseCheckList.disclosuredate : null,
            ischildplacedsixmonths: this.disClosuseCheckList.ischildplacedsixmonths ? this.disClosuseCheckList.ischildplacedsixmonths : disData.ischildplacedsixmonths,
            isproviderapprovedgap: this.disClosuseCheckList.isproviderapprovedgap ? this.disClosuseCheckList.isproviderapprovedgap : disData.isproviderapprovedgap,
            iscourthearingcustody: this.disClosuseCheckList.iscourthearingcustody ? this.disClosuseCheckList.iscourthearingcustody : disData.iscourthearingcustody,
            isreunificationremoved: this.disClosuseCheckList.isreunificationremoved ? this.disClosuseCheckList.isreunificationremoved : disData.isreunificationremoved,
            isadoptionremoved: this.disClosuseCheckList.isadoptionremoved ? this.disClosuseCheckList.isadoptionremoved : disData.isadoptionremoved,
            iscgprovidesafe: this.disClosuseCheckList ? this.disClosuseCheckList.iscgprovidesafe : null,
            isothergapfinsupport: this.disClosuseCheckList ? this.disClosuseCheckList.isothergapfinsupport : null,
            iscgattendedorientation: this.disClosuseCheckList ? this.disClosuseCheckList.iscgattendedorientation : null,
            orientationmeetingdate: this.disClosuseCheckList ? this.disClosuseCheckList.orientationmeetingdate : null,
            isrequirementdiscussed: this.disClosuseCheckList ? this.disClosuseCheckList.isrequirementdiscussed : null,
            iscgparticipategap: this.disClosuseCheckList ? this.disClosuseCheckList.iscgparticipategap : null,
            iscgenteredagreement: this.disClosuseCheckList ? this.disClosuseCheckList.iscgenteredagreement : null,
            iscgcompleteauthorization: this.disClosuseCheckList ? this.disClosuseCheckList.iscgcompleteauthorization : null,
            iscgaftercareservice: this.disClosuseCheckList ? this.disClosuseCheckList.iscgaftercareservice : '',
            isneedadditionalservices: this.disClosuseCheckList ? this.disClosuseCheckList.isneedadditionalservices : null,
            iscgcompleteannualreview: this.disClosuseCheckList ? this.disClosuseCheckList.iscgcompleteannualreview : null,
            issuspendedfromguardian: this.disClosuseCheckList ? this.disClosuseCheckList.issuspendedfromguardian : null,

            consultationguardianshiparrangement: this.disClosuseCheckList ? this.disClosuseCheckList.isconsultationchildage : null,
            strongattachmentprimaryguardian: this.disClosuseCheckList ? this.disClosuseCheckList.isguardianattach : null,
            strongattachmentsecondaryguardian: this.disClosuseCheckList ? this.disClosuseCheckList.isguardiantwoattach : null,

            issuccessorguardianexists: this.disClosuseCheckList ? this.disClosuseCheckList.issuccessorguardianexists : null,
            isconsultationchildage: this.disClosuseCheckList ? this.disClosuseCheckList.isconsultationchildage : null,
            isguardianattach: this.disClosuseCheckList ? this.disClosuseCheckList.isguardianattach : null,
            isguardiantwoattach: this.disClosuseCheckList ? this.disClosuseCheckList.isguardiantwoattach : null,

            successionaddendumdate: model.successionaddendumdate,
            cofinaldate: model.cofinaldate,
            successorguardianname: model.successorguardianname,
            empprogramstartdate: model.empprogramstartdate,
            empprogramname: model.empprogramname,
            fosterhomeapprover: model.fosterhomeapprover,
            primaryrelationshipkey: model.primaryrelationshipkey,
            secondaryrelationshipkey: model.secondaryrelationshipkey,
        });

        this.decideSecondaryGuardianInfo();
    }

    rejectComments(status) {
        if (status === 'Rejected') {
            this.isEnableComments = true;
        } else {
            this.isEnableComments = false;
            this.approvalStatusForm.patchValue({ comments: '' });
        }
    }

    initFormGroup() {

        this.disclosureCheklistForm = this._formBuilder.group({
            servicecaseid: [null],
            gapdisclosureid: [null],
            gapid: [null],
            permanencyplanid: [null],
            guardianoneid: [null],
            guardiantwoid: [null],
            guardianoneproviderid: [null],
            guardiantwoproviderid: [null],
            disclosuredate: [new Date()],
            ischildplacedsixmonths: [null],
            isproviderapprovedgap: [null],
            iscourthearingcustody: [null],
            isreunificationremoved: [null],
            isadoptionremoved: [null],
            iscgprovidesafe: [null, Validators.required],
            isothergapfinsupport: [null, Validators.required],
            iscgattendedorientation: [null, Validators.required],
            orientationmeetingdate: [null],
            isrequirementdiscussed: [null, Validators.required],
            iscgparticipategap: [null, Validators.required],
            iscgenteredagreement: [null, Validators.required],
            iscgcompleteauthorization: [null, Validators.required],
            iscgaftercareservice: [null, Validators.required],
            isneedadditionalservices: [null, Validators.required],
            iscgcompleteannualreview: [null, Validators.required],
            issuspendedfromguardian: [null, Validators.required],
            consultationguardianshiparrangement: [null],
            strongattachmentprimaryguardian: [null],
            strongattachmentsecondaryguardian: [null],
            enteredby: [null],

            issuccessorguardianexists: [null],
            isconsultationchildage: [null],
            isguardianattach: [null],
            isguardiantwoattach: [null],

            successionaddendumdate: [null],
            cofinaldate: [null],
            successorguardianname: [null],
            empprogramstartdate: [null],
            empprogramname: [null],
            fosterhomeapprover: [null],
            primaryrelationshipkey: [null],
            secondaryrelationshipkey: [null],

        });
        this.approvalStatusForm = this._formBuilder.group({
            routingstatus: [''],
            comments: ['']
        });

        this.providerSearchForm = this._formBuilder.group({
            childcharacteristics: [null],
            bundledplacementservices: [null],
            otherLocalDeptmntTypeId: [null],
            placementstructures: [null],
            zipcode: null,
            isLocalDpt: [true],
            firstname: null,
            middlename: null,
            lastname: null,
            isgender: [false],
            isAge: [false],
            providerid: null,
            agemin: null,
            agemax: null,
            gender: null
          });
    }
    addDislosureChecklist(checklist) {
        const saveDisclose = this.conditionValidation();
        if (saveDisclose) {
            const dislosureChecklistDetails = Object.assign(
                {
                    servicecaseid: this.id,
                },
                checklist
            );
            console.log(dislosureChecklistDetails);
            delete dislosureChecklistDetails.guardiantwo;
            delete dislosureChecklistDetails.guardianone;
            this._httpService.create(dislosureChecklistDetails, 'gapdisclosure/add').subscribe(
                res => {
                    if (res === 'Guardianship Already Exist') {
                        this._alertService.error('Guardianship Already Exist');
                    } else {
                        this.isExistRecord = true;
                        this._alertService.success('Disclosure Checklist Submitted for Supervisor Approval!');
                    }
                    this.getPage();
                },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    conditionValidation(): boolean {
        if (!this.placement.placementid) {
            this._alertService.warn('Please select child');
            return false;
        } else if (!this.disclosureCheklistForm.value.orientationmeetingdate && this.disclosureCheklistForm.value.iscgattendedorientation) {
            this._alertService.warn('Please fill orientation meeting date');
            return false;
        }
        return true;
    }

    getDisclosureCheckList() {
        this._httpService
            .getArrayList(
                new PaginationRequest({
                    where: {
                        servicecaseid: this.id,
                        intakeservicerequestactorid: this.placement.intakeservicerequestactorid,
                        placementtype: 'gapdisclosure'
                    },
                    method: 'get'
                }),
                'permanencyplan/permanencyplanvalidation' + '?filter'
            )
            .subscribe(
                (checkList) => {
                    const selectedPlacementChecklist = checkList.find(data =>
                        data.placementid === this.placement.placementid
                    );
                    if (selectedPlacementChecklist) {
                        this.disclosureCheklistForm.patchValue({
                            ischildplacedsixmonths: (selectedPlacementChecklist.ischildincare === 1) ? true : false,
                            iscourthearingcustody: (selectedPlacementChecklist.ischawardcustody === 1) ? true : false,
                            isproviderapprovedgap: true,
                            isreunificationremoved: true,
                            isadoptionremoved: true
                        });
                        const data = this.disclosureCheklistForm.getRawValue();
                        this.allowsubmit = (data.ischildplacedsixmonths && data.iscourthearingcustody) ? true : false;
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    decideSecondaryGuardianInfo() {
        this.isSuccessorGuardianExists = this.disclosureCheklistForm.getRawValue().issuccessorguardianexists;
    }

    openDisabilityForm() {
        this._router.navigate(['disability/' + this.child.personid + '/create'], { relativeTo: this.route });
        // (<any>$('#disability-form')).modal('show');
    }

    openDisabilityList() {
        this._router.navigate(['disability/' + this.child.personid + '/list'], { relativeTo: this.route });
    }

    toggleTitle4e(data) {
        this.showTitle4eFields = data.checked;
    }

    getChildCharacteristics() {
        this._httpService.getArrayList(new PaginationRequest({
          where: {
            'picklist_type_id': '43'
          },
          nolimit: true,
          method: 'get'
        }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.childCharacteristicsUrl).subscribe(result => {
          this.childCharacteristics = result;
        });
      }

    getOtherLocalDeptmntType() {
        this._httpService.getArrayList(new PaginationRequest({
          where: {
            'picklist_type_id': '104'
          },
          nolimit: true,
          method: 'get'
        }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.otherLocalDeptmntTypeUrl).subscribe(result => {
          this.otherLocalDeptmntType = result;
        });
      }

      getBundledPlcmntServicesType() {
        this._httpService.getArrayList(new PaginationRequest({
          nolimit: true,
          method: 'get'
        }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.bundledPlcmntServicesTypeUrl).subscribe(result => {
          this.bundledPlcmntServicesType = result;
        });
      }

      resetproviderSearchForm() {
        this.providerSearchForm.reset();
        this.currProcess = 'search';
      }

    onSearchParent1() {
        console.log('Search Text', this.adoptiveparent1);
        (<any>$('#searchProvider')).modal('show');
        this.selectedparent = 'parent1';

    }
    onSearchParent2() {
        console.log('Search Text', this.adoptiveparent2);
        (<any>$('#searchProvider')).modal('show');
        this.selectedparent = 'parent2';
    }

    selectProvider() {

        if (this.selectedparent === 'parent1') {
            console.log('Selected Provider for parent 1', this.selectedProvider);
            this.disclosureCheklistForm.patchValue({'guardianoneid': (this.selectedProvider && this.selectedProvider.providername) ?  this.selectedProvider.providername : null });
            this.disclosureCheklistForm.patchValue({'guardianoneproviderid': (this.selectedProvider && this.selectedProvider.provider_id) ?  this.selectedProvider.provider_id : null });
            this.parent1providerid = (this.selectedProvider && this.selectedProvider.provider_id) ?  this.selectedProvider.provider_id : null ;
            this.parent1providername = (this.selectedProvider && this.selectedProvider.providername) ?  this.selectedProvider.providername : null ;
        } else if (this.selectedparent === 'parent2') {
            console.log('Selected Provider for parent 2', this.selectedProvider);
            this.disclosureCheklistForm.patchValue({'guardiantwoid': (this.selectedProvider && this.selectedProvider.providername) ?  this.selectedProvider.providername : null });
            this.disclosureCheklistForm.patchValue({'guardiantwoproviderid': (this.selectedProvider && this.selectedProvider.provider_id) ?  this.selectedProvider.provider_id : null });
            this.parent2providerid = (this.selectedProvider && this.selectedProvider.provider_id) ?  this.selectedProvider.provider_id : null ;
            this.parent2providername = (this.selectedProvider && this.selectedProvider.providername) ?  this.selectedProvider.providername : null ;
        }
        (<any>$('#searchProvider')).modal('hide');
        this.resetproviderSearchForm();

    }

    CheckFormControlValue(formControl) {
        return (formControl && formControl !== '') ? formControl : null;
    }

    getFcProviderSearch() {
        if (this.providerSearchForm.invalid) {
          this._alertService.error('Please fill required fields');
          return false;
        }

        const formValues = this.providerSearchForm.getRawValue();
        if (formValues.isgender && !formValues.gender) {
          this._alertService.error('Please select gender');
          return false;
        }
        // formValues.agemin = formValues.isAge ? ( this.minAge ? this.minAge : null ) : null;
        // formValues.agemax = formValues.isAge ? ( this.maxAge ? this.maxAge : null ) : null;
        formValues.gender = formValues.isgender ? (formValues.gender ? formValues.gender : null) : null;
        console.log(formValues);

        Object.keys(this.providerSearchForm.controls).forEach(key => {
          formValues[key] = this.CheckFormControlValue(formValues[key]);
        });
        const body = {};
        Object.assign(body, formValues);
        body['isLocalDpt'] = true;
        body['localdepartmenthomecaregiver'] = true;
        body['childcharacteristics'] = formValues.childcharacteristics ? formValues.childcharacteristics[0] : null;
        body['otherLocalDeptmntTypeId'] = formValues.otherLocalDeptmntTypeId ? formValues.otherLocalDeptmntTypeId[0] : null;
        body['placementstructures'] = formValues.placementstructures ? formValues.placementstructures[0] : null;
        body['bundledplacementservices'] = formValues.bundledplacementservices ? formValues.bundledplacementservices[0] : null;

        // const dob = moment.utc(this.reportedChildDob);
        // const age16 = dob.clone().add(16, 'years');
        // const age21 = dob.clone().add(21, 'years');
        // const isAgeBtwn16And18 = moment.utc().isBetween(age16, age21, null, '[]');
        // if (isAgeBtwn16And18 && formValues.placementStrTypeId === '1') {// Independent Living Residential Program
        //   this._alertService.error('A child between the age of 16 and 21 years cannot be placed in \'Independent Living Residential Program\' Placement Structure.');
        //   return false;
        // }
        // body = {
        //   childcharacteristics: parseInt(formValues.childCharacteristicsid, 10),
        //   placementstructures: parseInt(formValues.placementStrTypeId, 10),
        //   bundledplacementservices: parseInt(formValues.bundledPlcmntServicesTypeId, 10),
        //   providerid: formValues.providerid,
        //   zipcode: parseInt(formValues.zipcode, 10),
        //   localdepartmenthomecaregiver: formValues.isLocalDpt
        // };
        this._httpService.getPagedArrayList(new PaginationRequest({
          where: body,
          page: this.paginationInfo.pageNumber,
          limit: 10,
          method: 'get'
        }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fcProviderSearchUrl).subscribe(result => {
          this.fcProviderSearch = result.data;
          this.fcTotal = result.count;
          (<any>$('#fc_list')).click();
          this.currProcess = 'select';
        });
      }

      backToSearchList() {
        this.currProcess = 'select';
        this.selectedProvider = null;
      }

      backToSearch() {
        this.currProcess = 'search';
        this.selectedProvider = null;
      }

      getRangeArray(n: number): any[] {
        return Array(n);
      }


      listMap(provider) {
        this.zoom = 13;
        this.defaultLat = 39.29044;
        this.defaultLng = -76.61233;
        // this.fcProviderSearch.map((map) => {
        (<any>$('#map-popup')).modal('show');
        this.markersLocation = [];
        // if (map.length > 0) {
        // this.fcProviderSearch.forEach((provider) => {
        //  this._ServiceCasePlacementsService.getGoogleMarker(provider.providerdetails[0].address).subscribe(res => {
        //     console.log(res);
        //   });
        const geocoder = new google.maps.Geocoder();
        if (geocoder) {
          geocoder.geocode({ 'address': provider.providerdetails[0].address }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
              this.markersLocation = [];
              const marker = { lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() };
              console.log(marker);
              this.lat = marker.lat;
              this.lng = marker.lng;
              const mapLocation = {
                lat: +marker.lat,
                lng: +marker.lng,
                draggable: +true,
                providername: provider.providername,
                addressline1: provider.providerdetails[0].address
              };
              this.markersLocation.push(marker);
              this.defaultLat = marker.lat;
              this.defaultLng = marker.lng;
              (<any>$('#map-popup')).modal('show');
              setTimeout(() => {
                this.showMap = true;
              }, 300);

            } else {
              console.log('Geocoding failed: ' + status);
            }
          });
        }

        // json.map(res => {
        //   console.log(res);
        // });
        //   if ( json.results && json.results.length > 0  && json.results[0].geometry.location  ) {
        //   const res = json.results[0].geometry.location;
        //   const mapLocation = {
        //         lat: +res.lat,
        //         lng: +res.lon,
        //         draggable: +true,
        //         providername: provider.providername,
        //         addressline1: provider.providerdetails[0].address
        //     };
        //     this.markersLocation.push(mapLocation);
        //     this.defaultLat = this.markersLocation[0].lat;
        //     this.defaultLng = this.markersLocation[0].lng;
        //   // });
        // // });
        // (<any>$('#map-popup')).modal('show');
        //   }
        //     }
        // });
      }
      mapClose() {
        this.markersLocation = [];
        this.showMap = false;
        // this.zoom = 11;
        // this.defaultLat = 39.219236;
        // this.defaultLng = -76.662834;
      }


      selectedViewProv(provId) {
        this.selectedViewProvider = provId;
      }

      selectedProv(provId) {
        // this.nextDisabled = false;
        console.log(this.selectedProvider);
        this.selectedProvider = provId ;
      //  this.getPlacementStrType(this.selectedProvider.provider_id );
      }

      getPlacementStrType(providerId) {
        this._httpService.getArrayList(new PaginationRequest({
          where: {
            'structure_service_cd': 'P',
            'provider_id': providerId
          },
          nolimit: true,
          method: 'get'
        }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.placementStrTypeUrl).subscribe(result => {
          this.placementStrType = result;
        });
      }

      loadGenderDropdownItems() {
        this._httpService.getArrayList(
          {
            where: { activeflag: 1 },
            method: 'get',
            nolimit: true
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.GenderTypeUrl + '?filter'
        ).subscribe((genderList) => {
          this.genderDropdownItems = genderList;
        });
        // text: res.typedescription,
        // value: res.gendertypekey
      }

      closeMap() {
        this.markersLocation = [];
        this.showMap = false;
        (<any>$('#map-popup')).modal('hide');
      }

}
