import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AlertService, CommonHttpService, DataStoreService, AuthService, SessionStorageService, CommonDropdownsService } from '../../../../../../@core/services';
import { PaginationRequest, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { PlacementGapService } from '../placement-gap.service';
import { RouteToSupervisor } from '../../_entities/placement.model';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import * as moment from 'moment';
@Component({
  selector: 'application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {

  id: string;
  applicationForm: FormGroup;
  approvalStatusForm: FormGroup;
  agreementRateForm: FormGroup;
  providerSearchForm: FormGroup;
  //selectedparent: any;
  adoptiveparent1: string;
  adoptiveparent2: string;
  currProcess = 'search';
  fcProviderSearch: any[];
  fcTotal: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  fcpaginationInfo: PaginationInfo = new PaginationInfo();
  selectedProvider: any;
 // parent2providerid: any;
 //parent2providername: any;
 // parent1providerid: any;
 // parent1providername: any;
  isApproved: boolean;
  isExistRecord: boolean;
  applicationData: any;
  isEnableComments: boolean;
  isSupervisor: boolean;
  submitStatus: RouteToSupervisor;
 // parent2: any;
 // parent1: any;
  store: any;
  maxDate = new Date();
  relationShipToRADropdownItems: DropdownModel[];

  constructor(
    private _store: DataStoreService,
    private _formBuilder: FormBuilder, private _alertService: AlertService,     private _commonHttpService: CommonHttpService,
    private _httpService: CommonHttpService, private _dataStoreService: DataStoreService,
    private _gapService: PlacementGapService, private _authservice: AuthService,
    private _session: SessionStorageService, private _ddservice: CommonDropdownsService) { 
      this.store = this._store.getCurrentStore();
    }

  ngOnInit() {
    this._gapService.checkDataAvailability();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    if (this._authservice.selectedRoleIs('apcs')) {
      this.isSupervisor = true;
    }
    this.initform();
    this.getPage();
    this.getRelationList();
  }

  initform() {

    this.applicationForm = this._formBuilder.group({
      guardianonedate: [new Date()],
      guardiantwodate: [null],
      ldssdirectordate: [null],
      servicecaseid: [null],
      gapapplicationid: [null],
      gapid: [null],
      permanencyplanid: [null],
      guardianoneid: [null],
      guardiantwoid: [null],
      guardianoneproviderid: [null],
      guardiantwoproviderid: [null],
      disclosuredate: [new Date()],
      iscgattendedorientation: [null],
      enteredby: [null],
      issuccessorguardianexists: [null],
      successionaddendumdate: [null],
      successorguardianname: [null],
      primaryrelationshipkey: [null],
      secondaryrelationshipkey: [null],
      planmeetingdate: [null],
      guardoneaddress: [null],
      guardtwoaddress: [null],
      guardonessnno: [null],
      guardtwossnno: [null],
      guardonedob: [null],
      guardtwodob: [null]
    });

    this.approvalStatusForm = this._formBuilder.group({
      routingstatus: [''],
      comments: ['']
    });

    this.providerSearchForm = this._formBuilder.group({
      childcharacteristics: [null],
      bundledplacementservices: [null],
      otherLocalDeptmntTypeId: [null],
      placementstructures: [null],
      zipcode: null,
      isLocalDpt: [true],
      firstname: null,
      middlename: null,
      lastname: null,
      isgender: [false],
      isAge: [false],
      providerid: null,
      agemin: null,
      agemax: null,
      gender: null
    });



  }

  private getAge(dateValue) {
    if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
      const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
      return moment().diff(rCDob, 'years');
    } else {
      return '';
    }
  }
  onSearchParents() {
    const child = this.store['placed_child'];
    if(child) {
      let childage = this.getAge(child.dob);
      let min = 0;
      let max = childage;
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender 
      });
    }
    this.currProcess = 'search';
    (<any>$('#searchProvider')).modal('show');

  }
/*
  onSearchParent1() {
    const child = this.store['placed_child'];
    if(child) {
      let childage = this.getAge(child.dob);
      let min = 0;
      let max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender 
      });
    }
    console.log('Search Text', this.adoptiveparent1);
    this.selectedparent = 'parent1';
    this.currProcess = 'search';
    (<any>$('#searchProvider')).modal('show');

  }
  onSearchParent2() {
    const child = this.store['placed_child'];
    if(child) {
      let childage = this.getAge(child.dob);
      let min = 0;
      let max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender 
      });
    }
    console.log('Search Text', this.adoptiveparent2);
    this.selectedparent = 'parent2';
    this.currProcess = 'search';
    (<any>$('#searchProvider')).modal('show');
  }

*/

  getRelationList() {
    this._commonHttpService.getArrayList(
      {
        where: { activeflag: 1, teamtypekey: this._authservice.getAgencyName() },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
        .RelationshipTypesUrl + '?filter'
    ).subscribe(data => {
      if (data && data.length) {
        this.setRelationList(data);
      }
    });
  }
  setRelationList(data) {
    if (data && data.length) {
      console.log('data = ' + JSON.stringify(data));
      this.relationShipToRADropdownItems = data.map(res => {
        return new DropdownModel({
          text: res.description,
          value: res.relationshiptypekey
        });
      });
    }
  }

  getFcProviderSearch() {
    if (this.providerSearchForm.invalid) {
      this._alertService.error('Please fill required fields');
      return false;
    }
  
    const child = this.store['placed_child'];
    if(child) {
      let childage = this.getAge(child.dob);
      let min = 0;
      let max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender 
      });
    }
    const formValues = this.providerSearchForm.getRawValue();
    if (formValues.isgender && !formValues.gender) {
      this._alertService.error('Please select gender');
      return false;
    }
    // formValues.agemin = formValues.isAge ? ( this.minAge ? this.minAge : null ) : null;
    // formValues.agemax = formValues.isAge ? ( this.maxAge ? this.maxAge : null ) : null;
    formValues.gender = formValues.isgender ? (formValues.gender ? formValues.gender : null) : null;
    console.log(formValues);

    Object.keys(this.providerSearchForm.controls).forEach(key => {
      formValues[key] = this.CheckFormControlValue(formValues[key]);
    });
    const body = {};
    Object.assign(body, formValues);
    body['isLocalDpt'] = true;
    body['localdepartmenthomecaregiver'] = true;
    body['childcharacteristics'] = formValues.childcharacteristics ? formValues.childcharacteristics[0] : null;
    body['otherLocalDeptmntTypeId'] = formValues.otherLocalDeptmntTypeId ? formValues.otherLocalDeptmntTypeId[0] : null;
    body['placementstructures'] = formValues.placementstructures ? formValues.placementstructures[0] : null;
    body['bundledplacementservices'] = formValues.bundledplacementservices ? formValues.bundledplacementservices[0] : null;
    body['guardian'] = true;
    // const dob = moment.utc(this.reportedChildDob);
    // const age16 = dob.clone().add(16, 'years');
    // const age21 = dob.clone().add(21, 'years');
    // const isAgeBtwn16And18 = moment.utc().isBetween(age16, age21, null, '[]');
    // if (isAgeBtwn16And18 && formValues.placementStrTypeId === '1') {// Independent Living Residential Program
    //   this._alertService.error('A child between the age of 16 and 21 years cannot be placed in \'Independent Living Residential Program\' Placement Structure.');
    //   return false;
    // }
    // body = {
    //   childcharacteristics: parseInt(formValues.childCharacteristicsid, 10),
    //   placementstructures: parseInt(formValues.placementStrTypeId, 10),
    //   bundledplacementservices: parseInt(formValues.bundledPlcmntServicesTypeId, 10),
    //   providerid: formValues.providerid,
    //   zipcode: parseInt(formValues.zipcode, 10),
    //   localdepartmenthomecaregiver: formValues.isLocalDpt
    // };
    this._httpService.getPagedArrayList(new PaginationRequest({
      where: body,
      page: this.paginationInfo.pageNumber,
      limit: 10,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fcProviderSearchUrl).subscribe(result => {
      this.fcProviderSearch = result.data;
      this.fcTotal = result.count;
      (<any>$('#fc_list')).click();
      this.currProcess = 'select';
    });
  }

  /*
  getProviderById(id, parent) {
    if (id) {
      const body = { providerid: id };
      this._httpService.getPagedArrayList(new PaginationRequest({
        where: body,
        page: this.paginationInfo.pageNumber,
        limit: 10,
        method: 'get'
      }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fcProviderSearchUrl).subscribe(result => {
        const list = (result && Array.isArray(result.data)) ? result.data : [];
        const provider = (list.length > 0) ? list[0] : null;
        this.parent1 = (parent === 1) ? provider : this.parent1;
        this.parent2 = (parent === 2) ? provider : this.parent2;
      });
    }
  }
*/
  CheckFormControlValue(formControl) {
    return (formControl && formControl !== '') ? formControl : null;
  }
  resetproviderSearchForm() {
    this.providerSearchForm.reset();
    this.currProcess = 'search';
  }

  getRangeArray(n: number): any[] {
    return Array(n);
  }

  selectedProv(provId) {
    console.log(this.selectedProvider);
    this.selectedProvider = provId;
  }

  backToSearch() {
    const child = this.store['placed_child'];
    if(child) {
      let childage = this.getAge(child.dob);
      let min = 0;
      let max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender 
      });
    }
    this.currProcess = 'search';
    this.selectedProvider = null;
  }

  fcPageChanged(pageEvent) {
    this.paginationInfo.pageNumber = pageEvent.page;
    this.getFcProviderSearch();
  }
  
  getFullName(firstName, lastName){
    return ''+
    (firstName? firstName+' ' : '') + 
    (lastName? lastName+' ' : '');
  }
  selectProvider() {
    if(this.selectedProvider && this.selectedProvider.provider_id){
      this._httpService.getArrayList(
        {
          method: 'get',
          where: { provider_id: this.selectedProvider.provider_id}
        },
        'tb_provider?filter'
      ).subscribe(res => {
        if (res && res.length && res[0]) {
          this.applicationForm.patchValue({
            'guardianoneid': this.getFullName(res[0].provider_first_nm, res[0].provider_last_nm),
            'guardiantwoid':this.getFullName(res[0].co_first_nm, res[0].co_last_nm),
            'guardianoneproviderid': res[0].provider_id,
            'guardoneaddress': this.selectedProvider.providerdetails[0].address,
            'guardonedob': new Date(res[0].dob_dt),
            'guardonessnno': res[0].tax_id_no,
            'guardtwodob': new Date(res[0].co_dob_dt),
            'guardtwossnno': res[0].co_ssn_no
          });
        }
      });
    }
    /*
    if (this.selectedparent === 'parent1') {
      console.log('Selected Provider for parent 1', this.selectedProvider);
      this.applicationForm.patchValue({ 'guardianoneid': (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null });
      this.applicationForm.patchValue({ 'guardianoneproviderid': (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null });
      this.parent1providerid = (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null;
      this.parent1providername = (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null;
      this.parent1 = Object.assign({}, this.selectedProvider);
    } else if (this.selectedparent === 'parent2') {
      console.log('Selected Provider for parent 2', this.selectedProvider);
      this.applicationForm.patchValue({ 'guardiantwoid': (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null });
      this.applicationForm.patchValue({ 'guardiantwoproviderid': (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null });
      this.parent2providerid = (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null;
      this.parent2providername = (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null;
      this.parent2 = Object.assign({}, this.selectedProvider);
    }
    */
    (<any>$('#searchProvider')).modal('hide');
    this.resetproviderSearchForm();

  }

  addDislosureChecklist(checklist) {
    // const saveDisclose = this.conditionValidation();

    const dislosureChecklistDetails = Object.assign({}, checklist);
    dislosureChecklistDetails.servicecaseid = this.id;
    dislosureChecklistDetails.permanencyplanid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID);
    console.log(dislosureChecklistDetails);
    delete dislosureChecklistDetails.guardiantwo;
    delete dislosureChecklistDetails.guardianone;
    this._httpService.create(dislosureChecklistDetails, 'gapapplication/add').subscribe(
      res => {
        if (res === 'Guardianship Already Exist') {
          this._alertService.error('Guardianship Already Exist');
        } else {
          this.isExistRecord = true;
          this._alertService.success('Application Submitted for Supervisor Approval!');
        }
        this.getPage();
      },
      error => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );

  }

  getPage() {
    const placement = this._dataStoreService.getData('placement_child');
    let permanencyplanid = '';

    if (this._authservice.selectedRoleIs('apcs')) {
      permanencyplanid = this._session.getItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID);
      const storeplanid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID);
      this._session.removeItem('Placement-Agreement-Rate-Id');
      permanencyplanid = (permanencyplanid) ? permanencyplanid : storeplanid;
    } else {
      permanencyplanid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID);
    }

    const reqParam = {
      permanencyplanid: permanencyplanid
    };

    this._gapService.getGapDisclosureDetails(reqParam)
      .subscribe(
        (result) => {
          if (result.data && result.data.length) {
            const data = result.data[0];
            this.patchDisclosureCheckList(data);
            this._gapService.setGapId(data.gapid);
          } else if (this._authservice.selectedRoleIs('apcs') && result.data.length === 0) {
            this._alertService.warn('Please fill Application');
            this.isApproved = true;
          } else {
            const guard = this._dataStoreService.getData('placement_child');
            if (guard && guard.providerdetails) {
              this.applicationForm.patchValue({
                guardianoneid: guard.providerdetails.providername,
                guardianoneproviderid: guard.providerdetails.provider_id
              });
              //this.getProviderById(guard.providerdetails.provider_id, 1);
            }
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  patchDisclosureCheckList(model) {
    if (model.gapapplication && model.gapapplication.length > 0) {
      this.applicationData = model.gapapplication[0];
      this.approvalStatusForm.patchValue({
        routingstatus: this.applicationData ? this.applicationData.routingstatus : '',
        comments: this.applicationData ? this.applicationData.comments : ''
      });
      if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Rejected') {
        this.isApproved = true;
        this.rejectComments(this.approvalStatusForm.value.routingstatus);
      }
      if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Review') {
        this.isExistRecord = true;
        this.applicationForm.disable();
      }
    } else {
      this.applicationData = Object.assign({});
      this.isExistRecord = false;
    }

    const placement = this._dataStoreService.getData('placement_child');
    let provider = { guardianoneid: null, guardianoneproviderid: null };
    if (placement && placement.providerdetails) {
      provider = {
        guardianoneid: placement.providerdetails.providername,
        guardianoneproviderid: placement.providerdetails.provider_id
      };
    }
   // this.getProviderById((model.guardianoneproviderid) ? model.guardianoneproviderid : provider.guardianoneproviderid, 1);
   // this.getProviderById(model.guardiantwoproviderid, 2);

    this.applicationForm.patchValue({
      guardianoneproviderid: model.guardianoneproviderid ? model.guardianoneproviderid : provider.guardianoneproviderid,
      guardiantwoproviderid: model.guardiantwoproviderid ? model.guardiantwoproviderid : '',
      guardianoneid: (model.guardianoneprovidername) ? model.guardianoneprovidername : model.guardianoneid,
      guardiantwoid: (model.guardiantwoprovidername) ? model.guardiantwoprovidername : model.guardiantwoid,
      guardoneaddress: model.oneaddress ? model.oneaddress : null,
      guardtwoaddress: model.twoaddress ? model.twoaddress : null,
      guardonessnno: model.one_ssno ? model.one_ssno : null,
      guardtwossnno: model.two_ssno ? model.two_ssno : null,
      guardonedob: model.one_dob_dt ? model.one_dob_dt : null,
      guardtwodob: model.two_dob_dt ? model.two_dob_dt : null,
      permanencyplanid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID),
      enteredby: model.applicationenteredby,
      gapid: model.gapapplication ? model.gapid : null,
      gapapplicationid: model ? model.gapapplicationid : null,
      disclosuredate: model ? model : null,
      orientationmeetingdate: model ? model.orientationmeetingdate : null,
      successionaddendumdate: model.successionaddendumdate,
      cofinaldate: model.cofinaldate,
      successorguardianname: model.successorguardianname,
      empprogramstartdate: model.empprogramstartdate,
      empprogramname: model.empprogramname,
      fosterhomeapprover: model.fosterhomeapprover,
      primaryrelationshipkey: model.primaryrelationshipkey,
      secondaryrelationshipkey: model.secondaryrelationshipkey,
      guardiantwodate: this._ddservice.getValidDate(this.applicationData.guardiantwodate),
      guardianonedate: this._ddservice.getValidDate(this.applicationData.guardianonedate),
      ldssdirectordate: this._ddservice.getValidDate(this.applicationData.ldssdirectordate),
      planmeetingdate: this._ddservice.getValidDate(this.applicationData.planmeetingdate)
    });
  }

  rejectComments(status) {
    if (status === 'Rejected') {
      this.isEnableComments = true;
    } else {
      this.isEnableComments = false;
      this.approvalStatusForm.patchValue({ comments: '' });
    }
  }

  routingUpdate() {
    const comment = 'Guardianship Submitted for review';
    this.submitStatus = Object.assign({
      objectid: this.applicationData ? this.applicationData.gapapplicationid : '',
      eventcode: 'GAAP',
      status: this.approvalStatusForm.value.routingstatus,
      comments: this.approvalStatusForm.value.comments,
      notifymsg: comment,
      routeddescription: comment,
      servicecaseid: this.id
    });
    console.log(this.submitStatus);
    if (!this.applicationData.gapapplicationid) {
      return this._alertService.error('Please select child');
    } else if (this._authservice.selectedRoleIs('apcs') && this.approvalStatusForm.value.routingstatus === 'Review') {
      return this._alertService.error('Please select review status!');
    } else {
      console.log(this.submitStatus);
      this._httpService.create(this.submitStatus, 'routing/routingupdate').subscribe(
        res => {
          this._alertService.success('Application ' + this.submitStatus.status + ' successfully!');
          this.isApproved = true;
          // this.getPage();
        },
        err => { }
      );
    }
  }

}
