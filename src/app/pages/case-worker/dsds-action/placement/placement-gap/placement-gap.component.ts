import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService, AuthService, SessionStorageService } from '../../../../../@core/services';
import { InvolvedPerson } from '../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { Placement } from '../../service-plan/_entities/service-plan.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { ServiceCasePermanencyPlanService } from '../../service-case-permanency-plan/service-case-permanency-plan.service';
import { PlacementGapService } from './placement-gap.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'placement-gap',
    templateUrl: './placement-gap.component.html',
    styleUrls: ['./placement-gap.component.scss']
})
export class PlacementGapComponent implements OnInit {
    paginationInfo: PaginationInfo = new PaginationInfo();
    id: string;
    placement: Placement[];
    permanencyPlanList: any;
    cjamsPid: string;
    child: any;
    isSelectChild = false;
    selectedChild: InvolvedPerson;
    involevedPerson$: Observable<InvolvedPerson[]>;
    childForm: FormGroup;
    private daNumber: string;
    constructor(private _formBuilder: FormBuilder, private _httpService: CommonHttpService, private route: ActivatedRoute, private _dataStoreService: DataStoreService,
        private _router: Router, private _gapService: PlacementGapService, private _authService: AuthService, private _session: SessionStorageService) { }

    ngOnInit() {
        this.childForm = this._formBuilder.group({
            selectchild: ['']
        });

        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        //this.getPlacement(1);
        this.child = this._dataStoreService.getData('placed_child');
        console.log('Child Data', this.child);
        // this._ServiceCasePermanencyPlanService.getPermanencyPlanList(1, 10).subscribe(data => {
        //     this.permanencyPlanList = data;
        //     console.log( 'Perm Plan', this.permanencyPlanList);
        this.checkforChildId();
        this.setUpGapData();
        // });
        // this.getInvolvedPerson();

    }

    checkforChildId() {
        this.cjamsPid = this._dataStoreService.getData('childforGAP');
        if (this.cjamsPid && this.cjamsPid !== null) {
            console.log('Cjams Pid Captured');
        } else {
            // this._ServiceCasePermanencyPlanService.broadCastPageRefresh();
            //  this._router.navigate(['../../../'], { relativeTo: this.route });
        }
    }
    getPlacement(page: number) {
        this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: page,
                    limit: 50,
                    where: {
                        intakeserviceid: this.id,
                        casenumber: this.daNumber
                    },
                    method: 'get'
                }),
                // CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementListUrl + '?filter'
                CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fostercarereferallistUrl + '?filter'
            )
            .subscribe(res => {
                this.placement = res.data;
                if (this.placement && this.placement.length) {
                    // this.placement = this.placement.filter((data) => {
                    //     if (data.exitreasontypekey === 'GS') {
                    //         return data;
                    //     }
                    // });
                    // if (this.placement[0].exitreasontypekey === 'GS') {
                    this.childForm.patchValue({
                        selectchild: this.placement[0]
                    });
                    this.selectChild({ value: this.placement[0] });
                    // }
                }
            });
    }
    selectChild(modal) {
        if (modal.value) {
            this._dataStoreService.setData('placement_child', modal.value);
            this.selectedChild = modal.value;
            this.isSelectChild = true;
        }
    }

    isGAPApproved(child) {
        this.cjamsPid = this._dataStoreService.getData('childforGAP');
        let status = false;
        if (child) {
            if (child.cjamspid && child.cjamspid === this.cjamsPid) {
                status = true;
            }
        }

        // For GAP Approval Check
        // if (child && child.permanencyplans && child.permanencyplans.length) {
        //     const permanencyPlan = child.permanencyplans;
        //     permanencyPlan.forEach(plan => {
        //          if (plan.status === 'Approved' && plan.primarypermanency && plan.primarypermanency.length && plan.primarypermanency[0].permanencyplantypekey  === 'Guardianship') {
        //            status = true;
        //          }
        //     });
        // }
        return status;
    }

    openDisabilityForm(child) {
        this._router.navigate(['disability/' + child.personid + '/create'], { relativeTo: this.route });
        // (<any>$('#disability-form')).modal('show');
    }

    openDisabilityList(child) {
        this._router.navigate(['disability/' + child.personid + '/list'], { relativeTo: this.route });
    }

    close() {
        this._router.navigate(['../../'], { relativeTo: this.route });
    }

    setUpGapData() {
        const currentUser = this._authService.getCurrentUser();
        let reqParam = {};
        let permanencyplanid = '';
        if (this._authService.selectedRoleIs('apcs')) {
            const sdisclosureId = this._session.getItem('Placement-Gap-Disclosure-Id');
            const disclosureId = (sdisclosureId) ? sdisclosureId : this._dataStoreService.getData('Placement-Gap-Disclosure-Id');
            permanencyplanid = this._session.getItem(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID);
            const storeplanid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID);
            permanencyplanid = (permanencyplanid) ? permanencyplanid : storeplanid;
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.GAP_PERMANENCYPLANID, permanencyplanid);
            reqParam = {
                permanencyplanid: permanencyplanid
            };
            this._gapService.getGapDisclosureDetails(reqParam).subscribe(result => {
                if (Array.isArray(result.data) && result.data.length > 0) {
                    const disClosure = result.data[0];
                    this._gapService.getInvolvedPerson().subscribe(data => {
                        const persons = data.data;
                        this.child = persons.find(person => person.personid === disClosure.personid);
                    });
                }
            });
        }
    }
}
