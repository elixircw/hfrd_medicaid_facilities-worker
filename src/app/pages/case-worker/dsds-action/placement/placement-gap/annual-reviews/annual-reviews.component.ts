import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AppUser } from '../../../../../../@core/entities/authDataModel';
import { PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { DataStoreService, SessionStorageService } from '../../../../../../@core/services';
import { AlertService } from '../../../../../../@core/services/alert.service';
import { AuthService } from '../../../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { AnnualReview, GapAnnualReview, GapDetails, RouteToSupervisor, Placement } from '../../_entities/placement.model';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { Observable } from 'rxjs/Observable';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { PlacementGapService } from '../placement-gap.service';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'annual-reviews',
    templateUrl: './annual-reviews.component.html',
    styleUrls: ['./annual-reviews.component.scss']
})
export class AnnualReviewsComponent implements OnInit {
    annualReview: GapAnnualReview = new GapAnnualReview();
    id: string;
    annualReviewGapForm: FormGroup;
    approvalStatusForm: FormGroup;
    GapAnnualReview: GapAnnualReview;
    disClosure: GapDetails;
    placement: Placement;
    submitStatus: RouteToSupervisor;
    isSupervisor = false;
    roleId: AppUser;
    currentUrl: string;
    daNumber: string;
    annualReviewList: AnnualReview = new AnnualReview();
    annualReviewList$: Observable<AnnualReview[]>;
    gapAlertMessage: string;
    isExistRecord = false;
    isApproved = false;
    isEnableComments = false;
    isInitialized = false;
    isFormDisplay = false;
    isEditReview = false;
    gapannualreviewid: string;
    gapagreementid: any;
    placementAnnualReviewID: any;
    constructor(
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _dataStoreService: DataStoreService,
        private _alertService: AlertService,
        private _route: Router,
        private _placementService: PlacementGapService,
        private _session: SessionStorageService
    ) {}

    ngOnInit() {
        this._placementService.checkDataAvailability();
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.role.name === 'apcs') {
            this.isSupervisor = true;
            this.placementAnnualReviewID = this._session.getItem('Placement-Review-Id');
            this._session.setItem('Placement-Review-Id', null);
            this.getReviewsforSupervisor();
        }
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.annualReviewInitialform();
        this.isInitialized = true;
        this._dataStoreService.currentStore.subscribe((store) => {
            if (this.isInitialized && store['placement_child']) {
                this.placement = store['placement_child'];
                if (this.placement) {
                    this.getReviews();
                    this.isInitialized = false;
                }
            }
        });
        this.getAgreement();
        this.getReviews();
    }

    getReviewsforSupervisor() {

            const review = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 10,
                    where: {
                        objectid: this.placementAnnualReviewID,
                        objecttype: 'annualreview' },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Gap.AnnualReview + '?filter'
            ).subscribe(
                (resp) => {
                    console.log(resp);
                    this.annualReviewList$ = Observable.of(resp);
                    // this.annualReviewList$ = resp;
                },
                (error) => {
                    console.log(error);
                }
            );

    }

    formAccess(reviewMode) {
        if (reviewMode === 0) {
            this.isFormDisplay = true;
            this.isEditReview = false;
            this.formClear();
        } else {
            this.isFormDisplay = true;
            this.isEditReview = true;
            this.formClear();
        }
    }
    formClear() {
        this.annualReviewGapForm.reset();
    }
    editAnnualReview(reviewModal) {
        this.formAccess(1);
        this.annualReviewGapForm.patchValue(reviewModal);
        this.gapannualreviewid = reviewModal.gapannualreviewid;
    }
    annualReviewInitialform() {
        this.annualReviewGapForm = this.formBuilder.group({
            reviewdate: [null],
            isguardianresponsible: [null],
            isguardiansupportfinance: [null],
            ischildwithguardian: [null],
            ischildattendingschool: [null],
            isdocumentprovided: [null],
            ischildreacheighteen: [null],
            ischilddisability: [null],
            istrainingenrolled: [null],
            isunemployment: [null],
            isformcomplete: [null],
            cgprimarydate: [null, Validators.required],
            cgsecondarydate: [null],
            directorsigndate: [null, Validators.required]
        });

        this.approvalStatusForm = this.formBuilder.group({
            routingstatus: [''],
            comments: ['']
        });
    }
    getReviews() {
        const review = this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: {
                        gapid: this._placementService.getGapId()
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Gap.AnnualReview + '?filter'
            ).map((resp) => {
                return {data: resp};
            }).share();
        this.annualReviewList$ = review.pluck('data');
    }
    addEditAnnualReview(annualreview, editmode) {
        let modal;
        if (editmode === 0) {
             modal = Object.assign(
                {
                    gapid: this._placementService.getGapId(),
                    servicecaseid: this.id,
                    gapagreementid: this.gapagreementid
                },
                annualreview
            );
        } else {
            modal = Object.assign(
                {
                    gapid: this._placementService.getGapId(),
                    servicecaseid: this.id,
                    gapannualreviewid: this.gapannualreviewid
                },
                annualreview
            );
        }
        if (this.annualReviewGapForm.valid) {
            this._commonHttpService.create(modal,
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Gap.AddEditReview).subscribe(
                (res) => {
                    console.log(res);
                    this._alertService.success('Annual Review Submitted for supervisor approval');
                    this.getReviews();
                    this.formClear();
                    this.isFormDisplay = false;
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }
    // getPage() {
    //     this._commonHttpService
    //         .getPagedArrayList(
    //             new PaginationRequest({
    //                 where: {
    //                     intakeserviceid: this.id,
    //                     placementid: this.placement.placementid
    //                 },
    //                 method: 'get'
    //             }),
    //             'gapdisclosure/getguardianship' + '?filter'
    //         )
    //         .subscribe(
    //             (checkList) => {
    //                 if (checkList.data.length) {
    //                     this.disClosure = checkList.data[0];
    //                     this.patchAnnualReview(this.disClosure);
    //                 } else {
    //                     this.gapAlertMessage = 'Please complete disclosure checklist';
    //                     (<any>$('#gap-placement')).modal('show');
    //                 }
    //             },
    //             (error) => {
    //                 console.log(error);
    //             }
    //         );
    // }
    // patchAnnualReview(modal) {
    //     if (!modal.gapdisclosure) {
    //         this.gapAlertMessage = 'Please complete disclosure checklist';
    //         // (<any>$('#gap-placement')).modal('show');
    //     } else if (!modal.gapagreement) {
    //         this.gapAlertMessage = 'Please complete agreement';
    //         (<any>$('#gap-placement')).modal('show');
    //     } else if (modal.gapannualreview && modal.gapannualreview.length > 0) {
    //         this.isExistRecord = true;
    //         this.annualReviewList = modal.gapannualreview[0];
    //         this.annualReviewGapForm.patchValue(this.annualReviewList);
    //         this.approvalStatusForm.patchValue({
    //             routingstatus: this.annualReviewList.routingstatus ? this.annualReviewList.routingstatus : '',
    //             comments: this.annualReviewList.comments ? this.annualReviewList.comments : ''
    //         });
    //         if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Rejected') {
    //             this.isApproved = true;
    //             this.rejectComments(this.approvalStatusForm.value.routingstatus);
    //         }
    //     } else {
    //         this.isExistRecord = false;
    //     }
    // }
    rejectComments(status) {
        if (status === 'Rejected') {
            this.isEnableComments = true;
        } else {
            this.isEnableComments = false;
            this.approvalStatusForm.patchValue({ comments: '' });
        }
    }
    // addAnnualReview(annualreview) {
    //     const modal = Object.assign(
    //         {
    //             gapid: this.disClosure.gapid,
    //             intakeserviceid: this.id
    //         },
    //         annualreview
    //     );
    //     if (this.annualReviewGapForm.valid) {
    //         this._commonHttpService.create(modal, 'gapannualreview/add').subscribe(
    //             (res) => {
    //                 console.log(res);
    //                 this._alertService.success('agreement added successfully');
    //                 this.getPage();
    //             },
    //             (error) => {
    //                 console.log(error);
    //             }
    //         );
    //     }
    // }
    routingUpdate() {
        const comment = 'Annual Review Submitted for review';
        this.submitStatus = Object.assign({
            objectid: this.placementAnnualReviewID ? this.placementAnnualReviewID : '',
            eventcode: 'GAYR',
            status: this.approvalStatusForm.value.routingstatus,
            comments: this.approvalStatusForm.value.comments,
            notifymsg: comment,
            routeddescription: comment,
            servicecaseid: this.id
        });
        console.log(this.submitStatus);
        if (this.roleId.role.name === 'apcs' && this.approvalStatusForm.value.routingstatus === '') {
            return this._alertService.error('Please select review status!');
        } else {
            console.log(this.submitStatus);
            this._commonHttpService.create(this.submitStatus, 'routing/routingupdate').subscribe(
                (res) => {
                    this._alertService.success('Annual Review ' + this.approvalStatusForm.value.routingstatus + ' Successfully!');
                    this.isApproved = true;
                    this.getReviews();
                },
                (err) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    navigateTo() {
        if (this.disClosure) {
            const gapdisclosure = this.disClosure.gapdisclosure;
            const gapagreement = this.disClosure.gapagreement;
            const gapannualreview = this.disClosure.gapannualreview;
            (<any>$('#gap-placement')).modal('hide');
            if (!gapdisclosure) {
                this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/disclosure-checklist';
            } else if (!gapagreement) {
                this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/agreement';
            } else if (!gapannualreview) {
                this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/annual-reviews';
            }
        } else {
            this.currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/placement-gap/disclosure-checklist';
        }
        this._route.navigate([this.currentUrl]);
    }

    private getAgreement() {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    page: 1,
                    limit: 10,
                    where: { gapid: this._placementService.getGapId() }
                },
                'gapagreement/list?filter'
            )
            .subscribe(res => {
                if (res && (res instanceof Array)) {
                    const data = res[0];
                    this.gapagreementid = data.gapagreementid;
                }
            });
    }
}
