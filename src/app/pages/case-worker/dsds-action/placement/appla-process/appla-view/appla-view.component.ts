import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../../../../../environments/environment';
import { AppUser } from '../../../../../../@core/entities/authDataModel';
import {
  AlertService,
  AuthService,
  CommonHttpService,
  DataStoreService,
  SessionStorageService,
} from '../../../../../../@core/services';
import { HttpService } from '../../../../../../@core/services/http.service';
import { DSDSActionSummary, GetintakAssessment, RoutingInfo } from '../../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import {
  AssessmentContactNotes,
  General,
  InvestigationSummary,
} from '../../../disposition/_entities/disposition.data.models';
import { InvolvedPerson } from '../../../involved-persons/_entities/involvedperson.data.model';
import { Placement } from '../../../service-plan/_entities/service-plan.model';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { AppConstants } from '../../../../../../@core/common/constants';

declare var Formio: any;
@Component({
  selector: 'appla-view',
  templateUrl: './appla-view.component.html',
  styleUrls: ['./appla-view.component.scss']
})
export class ApplaViewComponent implements OnInit, AfterViewInit {

  id: string;
  generalSummary: General;
  daNumber: string;
  assessmmentName: string;
  investigationSummary: InvestigationSummary;
  private token: AppUser;
  selectedSafeCDangerInfluence: any[] = [];
  currentTemplateId: string;
  isChildSafe = true;
  safeCKeys: string[];
  involvedPersons: InvolvedPerson[];
  routingInfo: RoutingInfo[];
  placement: Placement[];
  assessmentSummary: AssessmentContactNotes;
  formioOptions: {
    formio: {
      ignoreLayout: true;
      emptyValue: '-';
    };
  };
  isReadOnlyForm = false;
  intakAssessment = new GetintakAssessment();
  dsdsActionsSummary = new DSDSActionSummary();
  isInitialized = false;
  private formTriggered = false;
  isServiceCase: string;
  serviceCaseId: string;
  constructor(
    private route: ActivatedRoute,
    private _authService: AuthService,
    private storage: SessionStorageService,
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _http: HttpService,
    private _router: Router,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService
  ) {
    this.safeCKeys = [
      'caregiverdescribes',
      'caregiverfailstoprotect',
      'caregivermadeaplausible',
      'caregiverrefuses',
      'caregiversemotionalinstability',
      'caregiversexplanation',
      'caregiversjustification',
      'caregiverssuspected',
      'childscurrentimminent',
      'childsexualabuse',
      'childswhereabouts',
      'currentactofmaltreatment',
      'domesticviolence',
      'extremelyanxious',
      'multiplereports',
      'servicestothecaregiver',
      'specialneeds',
      'unabletoprotect',
      'servicestothecaregiver2'
    ];
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
  }

  ngOnInit() {
    this.isServiceCase = this.storage.getItem('ISSERVICECASE');
    if (this.isServiceCase) {
      this.serviceCaseId = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    } else {
      this.serviceCaseId = null;
    }
    this.getInvestigationSummary();
    this.token = this._authService.getCurrentUser();
    this.isInitialized = true;
  }

  ngAfterViewInit() {
    this._dataStoreService.currentStore.subscribe(storeData => {
      if (this.isInitialized && storeData['SUBSCRIPTION_TARGET'] === 'CASEWORKER_ASSESSMENT_LOAD') {
        this.involvedPersons = storeData['CASEWORKER_INVOLVED_PERSON'];
        this.routingInfo = storeData['CASEWORKER_ROUTING_INFO'];
        this.investigationSummary = storeData['INVESTIGATION_SUMMARY'];
        this.placement = storeData['CASEWORKER_PLACEMENT'];
        this.intakAssessment = storeData['CASEWORKER_SELECTED_ASSESSMENT'];
        this.dsdsActionsSummary = storeData['dsdsActionsSummary'];
        if (this.intakAssessment) {
          if (this.intakAssessment.mode === 'start') {
            this.startAssessment(this.intakAssessment);
          } else if (this.intakAssessment.mode === 'submit') {
            this.submittedAssessment(this.intakAssessment);
          } else if (this.intakAssessment.mode === 'update') {
            this.updateAssessment(this.intakAssessment);
          } else if (this.intakAssessment.mode === 'print') {
            this.assessmentPrintView(this.intakAssessment);
          }
        } else {
          this.redirectToAssessment();
        }
        this.isInitialized = false;
      }
    });
  }

  startAssessment(assessment) {
    this.assessmmentName = assessment.description;
    this.currentTemplateId = assessment.external_templateid;
    const _self = this;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`).then(function (form) {
      form.components = form.components.map(item => {
        if (item.key === 'Complete' && item.type === 'button') {
          item.action = 'submit';
        }
        return item;
      });
      form.submission = {
        data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
      };
      form.on('submit', submission => {
        submission.data['child'] = _self._dataStoreService.getData('placed_child');
        let status = '';
        let comments = '';
        if (_self.token.role.name === 'apcs') {
          status = submission.data.assessmentstatus;
          comments = submission.data.supervisorcomments;
        } else if (_self.token.role.name === 'field') {
          if (submission.data.submit) {
            status = 'InProcess';
          } else {
            status = 'Submitted';
          }
          comments = submission.data.caseworkercomments;
        }
        _self._http
          .post('admin/assessment/add', {
            externaltemplateid: _self.currentTemplateId,
            objectid: _self.id,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0,
            ischildsafe: _self.isChildSafe,
            assessmentstatustypekey1: status ? status : null,
            servicecaseid: _self.serviceCaseId,
            comments: comments ? comments : null
          })
          .subscribe(response => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            _self.redirectToAssessment();
            /// (<any>$('#iframe-popup')).modal('hide');
            if (_self.assessmmentName === 'SAFE-C') {
              Observable.timer(500).subscribe(() => {
                _self._router.routeReuseStrategy.shouldReuseRoute = function () {
                  return false;
                };
                _self._router.navigateByUrl(_self._router.url).then(() => {
                  _self._router.navigated = false;
                  _self._router.navigate([_self._router.url]);
                });
              });
            } else {
              if (_self.assessmmentName === 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT') {
                const participantDetail = [];
                const reportedChild = _self.involvedPersons.filter(item => {
                  return item.rolename === 'RC' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RC').length);
                });
                let reportedChildName = '';
                if (reportedChild.length) {
                  reportedChildName = reportedChild[0].lastname;
                  if (reportedChild[0].firstname) {
                    reportedChildName += ', ' + reportedChild[0].firstname;
                  }
                }
                _self.involvedPersons.map(item => {
                  if (item.rolename !== 'RC' && item.rolename !== 'AM') {
                    participantDetail.push({
                      intakeserviceid: _self.id,
                      personid: item.personid,
                      relationship: item.relationship,
                      email: item.email,
                      firstname: item.firstname,
                      lastname: item.lastname,
                      message:
                        'This is to notify you that the child ' +
                        reportedChildName.toUpperCase() +
                        ' is being enrolled in ' +
                        submission.data.selectedcurrentschool +
                        ' school based on the best interest assessment for education'
                    });
                  }
                });
                _self._commonService.create(participantDetail, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.Notification).subscribe(res => {
                  console.log(res);
                });
              }
            }
          });
      });
      form.on('change', formData => {
        _self.safeCProcess(formData);
      });
      form.on('render', formData => {
        /// (<any>$('#iframe-popup')).modal('show');
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });

      form.on('error', error => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
        _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
      });
    });
  }
  submittedAssessment(assessment: GetintakAssessment) {
    this.assessmmentName = assessment.titleheadertext;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
      readOnly: true
    }).then(function (submission) {
      /// (<any>$('#iframe-popup')).modal('show');
      submission.on('render', formData => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });
    });
  }
  updateAssessment(assessment: GetintakAssessment) {
    this.assessmmentName = assessment.titleheadertext;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    // this.assessmmentName = assessment.description;
    // this.currentTemplateId = assessment.external_templateid;
    const _self = this;
    // Formio.setToken(this.storage.getObj('fbToken'));
    // Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
      readOnly: false
    }).then(function (form) {
      form.components = form.components.map(item => {
        if (item.key === 'Complete' && item.type === 'button') {
          item.action = 'submit';
        }
        return item;
      });
      form.submission = {
        data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
      };
      /// (<any>$('#iframe-popup')).modal('show');
      form.on('render', formData => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
      });
      form.on('submit', submission => {
        if (_self.assessmmentName === 'SAFE-C') {
          submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
        }
        let status = '';
        let comments = '';
        if (_self.token.role.name === 'apcs') {
          status = submission.data.assessmentstatus;
          comments = submission.data.supervisorcomments;
        } else if (_self.token.role.name === 'field') {
          if (submission.data.submit) {
            status = 'InProcess';
          } else {
            status = 'Submitted';
          }
          comments = submission.data.caseworkercomments;
        }
        _self._http
          .post('admin/assessment/add', {
            externaltemplateid: _self.currentTemplateId,
            objectid: _self.id,
            submissionid: submission._id,
            submissiondata: submission.data ? submission.data : null,
            form: submission.form ? submission.form : null,
            score: submission.data.score ? submission.data.score : 0,
            assessmentstatustypekey1: status ? status : null,
            servicecaseid: _self.serviceCaseId,
            comments: comments ? comments : null
          })
          .subscribe(response => {
            _self._alertService.success(_self.assessmmentName + ' saved successfully.');
            // _self.getPage(1);
            // _self.showAssessment(
            //     _self.showAssesment,
            //     _self.getAsseesmentHistory
            // );
            _self.redirectToAssessment();
            /// (<any>$('#iframe-popup')).modal('hide');
          });
      });
      form.on('change', formData => {
        _self.safeCProcess(formData);
      });
      // form.on('render', (formData) => {
      //     (<any>$('#iframe-popup')).modal('show');
      //     setTimeout(function () {
      //         $('#assessment-popup').scrollTop(0);
      //     }, 200);
      // });

      form.on('error', error => {
        setTimeout(function () {
          $('#assessment-popup').scrollTop(0);
        }, 200);
        _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
      });
    });
  }

  assessmentPrintView(assessment: GetintakAssessment) {
    const _self = this;
    Formio.setToken(this.storage.getObj('fbToken'));
    Formio.baseUrl = environment.formBuilderHost;
    Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
      readOnly: true
    }).then(function (submission) {
      const options = {
        ignoreLayout: true
      };
      _self.viewHtml(submission._form, submission._submission, options);
    });
  }

  viewHtml(componentData, submissionData, formioOptions) {
    delete submissionData._id;
    delete submissionData.owner;
    delete submissionData.modified;
    const exporter = new FormioExport(componentData, submissionData, formioOptions);
    const appDiv = document.getElementById('divPrintView');
    exporter.toHtml().then(html => {
      html.style.margin = 'auto';
      const iframe = this.createIframe(appDiv);
      const doc = iframe.contentDocument || iframe.contentWindow.document;
      doc.body.appendChild(html);
      window.frames['ifAssessmentView'].focus();
      window.frames['ifAssessmentView'].print();
    });
  }

  private createIframe(el) {
    _.forEach(el.getElementsByTagName('iframe'), _iframe => {
      el.removeChild(_iframe);
    });
    const iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'ifAssessmentView');
    iframe.setAttribute('name', 'ifAssessmentView');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('webkitallowfullscreen', '');
    iframe.setAttribute('mozallowfullscreen', '');
    iframe.setAttribute('allowfullscreen', '');
    iframe.setAttribute('style', 'width: -webkit-fill-available;height: -webkit-fill-available;');
    el.appendChild(iframe);
    return iframe;
  }

  private getFormPrePopulation(formName: string, submissionData: any) {
    return submissionData;
  }

  private safeCProcess($event) {
    if (this.assessmmentName === 'SAFE-C') {
      if ($event.changed) {
        const dangerInfluenceKey = $event.changed.component.key;
        if (dangerInfluenceKey === 'safetydecision4') {
          this.isChildSafe = !$event.data[$event.changed.component.key];
        }
        if (dangerInfluenceKey && this.safeCKeys.indexOf(dangerInfluenceKey) > -1) {
          const dangerInflunceItem = this.selectedSafeCDangerInfluence.find(item => item.value === dangerInfluenceKey);
          if (dangerInflunceItem) {
            if ($event.changed.value === 'no' || $event.data[$event.changed.component.key] === 'no') {
              const itemIndex = this.selectedSafeCDangerInfluence.indexOf(dangerInflunceItem);
              this.selectedSafeCDangerInfluence.splice(itemIndex, 1);
            }
            console.log(this.selectedSafeCDangerInfluence);
          } else {
            if ($event.changed.value === 'yes' || $event.data[$event.changed.component.key] === 'yes') {
              this.selectedSafeCDangerInfluence.push({
                text: $event.changed.component.label,
                value: dangerInfluenceKey
              });
              console.log(this.selectedSafeCDangerInfluence);
            }
          }
        }
      }
    } else {
      this.selectedSafeCDangerInfluence = [];
    }
  }

  private getInvestigationSummary() {
    this._commonHttpService
      .getSingle(
        {
          intakeserviceid: this.id,
          method: 'post'
        },
        'Investigations/getinvestigationsummary'
      )
      .subscribe(data => {
        if (data.length) {
          this.investigationSummary = data[0];
          if (this.investigationSummary.general && this.investigationSummary.general.length) {
            this.generalSummary = this.investigationSummary.general[0];
          }
          if (this.investigationSummary.assessmentcontactnotes && this.investigationSummary.assessmentcontactnotes.length) {
            this.assessmentSummary = this.investigationSummary.assessmentcontactnotes[0];
          }
        }
      });
  }

  redirectToAssessment() {
    this.isInitialized = false;
    if (this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR)) {
      this._router.navigate(['/pages/cjams-dashboard/cw-assessment']);
    } else {
      this._router.navigate(['../list'], { relativeTo: this.route });
    }

    // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-permanency-plan/placement/appla/list']);
  }
}
