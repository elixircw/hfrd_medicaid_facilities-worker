import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplaViewComponent } from './appla-view.component';

describe('ApplaViewComponent', () => {
  let component: ApplaViewComponent;
  let fixture: ComponentFixture<ApplaViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplaViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
