import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplaProcessComponent } from './appla-process.component';
import { ApplaViewComponent } from './appla-view/appla-view.component';
import { ApplaListComponent } from './appla-list/appla-list.component';
import { CaseWorkerViewAssessmentComponent } from '../../assessment/case-worker-view-assessment/case-worker-view-assessment.component';

const routes: Routes = [{
  path: '',
  component: ApplaProcessComponent,
  children: [
    { path: 'view', component: CaseWorkerViewAssessmentComponent },
    { path: 'list', component: ApplaListComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplaProcessRoutingModule { }
