import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { AppUser } from '../../../../../../@core/entities/authDataModel';
import { PaginationInfo, PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService, SessionStorageService } from '../../../../../../@core/services';
import { AuthService } from '../../../../../../@core/services/auth.service';
import { HttpService } from '../../../../../../@core/services/http.service';
import { AppConfig } from '../../../../../../app.config';
import { Assessments, GetintakAssessment, InvolvedPerson, RoutingInfo } from '../../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { ChildRoles } from '../../../child-removal/_entities/childremoval.model';
import { AssessmentBlob } from '../../../involved-persons/_entities/involvedperson.data.model';
import { Placement } from '../../../service-plan/_entities/service-plan.model';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'appla-list',
    templateUrl: './appla-list.component.html',
    styleUrls: ['./appla-list.component.scss']
})
export class ApplaListComponent implements OnInit {
    startAssessment$: Observable<Assessments[]>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    getAsseesmentHistory: GetintakAssessment[] = [];
    id: string;
    daNumber: string;
    showAssesment = -1;
    involvedPersons: InvolvedPerson[];
    routingInfo: RoutingInfo[];
    placement: Placement[];
    relationName: ChildRoles[] = [];
    childDetail: ChildRoles[] = [];
    roleId: AppUser;
    assessmentDetail: AssessmentBlob;
    isServiceCase: string;
    inputRequest: Object;
    assessmentTemplateId: any;

    constructor(
        private _http: HttpService,
        private _service: CommonHttpService,
        private route: ActivatedRoute,
        private _dataStoreService: DataStoreService,
        private _router: Router,
        private _authService: AuthService,
        private storage: SessionStorageService,
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    }

    ngOnInit() {
        this.roleId = this._authService.getCurrentUser();
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        if (this.isServiceCase) {
            this.inputRequest = {
                objecttypekey: 'servicecase',
                objectid: this.id,
                description: 'APPLA'
            };
        } else {
            this.inputRequest = {
                servicerequestid: this.id,
            };
        }
        this.getAssessmentPrefillData();
        this.getPage(1);

    }
    getPage(page: number) {

        this._http.overrideUrl = false;
        this._http.baseUrl = AppConfig.baseUrl;
        const source = this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: page,
                    limit: this.paginationInfo.pageSize,
                    where: this.inputRequest,
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.ListAssessment + '?filter'
            )
            .map((result) => {
                return { data: result.data, count: result.count };
            })
            .share();
        this.startAssessment$ = source.pluck('data');
    }

    private getAssessmentPrefillData() {
        const source = forkJoin(
            this._service.getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: this.inputRequest
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            ),
            this._service.getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 50,
                    method: 'get',
                    where: { 'servicecaseid': this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.RoutingInfoList
            ),
            this._service.getPagedArrayList(
                {
                    where: this.inputRequest,
                    page: 1,
                    limit: 50,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementListUrl + '?filter'
            ),
            this._service.getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: this.inputRequest
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
            )
        ).subscribe(result => {
            if (result && result[0]['data']) {
                this.involvedPersons = result[0]['data'];
            }
            if (result && result[1].length > 0) {
                this.routingInfo = result[1][0]['routinginfo'];
            }
            this.placement = result[2]['data'];
            this.relationName = [];
            this.relationName = result[3].data;
            this.childDetail = [];
            this.relationName.map(res => {
                if (res.roles) {
                    const checkRC = res.roles.filter(role => role.intakeservicerequestpersontypekey === 'CHILD');
                    if (checkRC.length) {
                        this.childDetail.push(res);
                    }
                }
            });
        });
    }
    startAssessment(assessment: GetintakAssessment, mode) {
        assessment.mode = mode;
        const storeData = this._dataStoreService.getCurrentStore();
        storeData['CASEWORKER_INVOLVED_PERSON'] = this.involvedPersons;
        storeData['CASEWORKER_ROUTING_INFO'] = this.routingInfo;
        storeData['CASEWORKER_PLACEMENT'] = this.placement;
        storeData['CASEWORKER_SELECTED_ASSESSMENT'] = assessment;
        storeData['CASEWORKER_CHILD_DETAIL'] = this.childDetail;
        this._dataStoreService.setObject(storeData, true, 'CASEWORKER_ASSESSMENT_LOAD');
        this._router.navigate(['../view'], { relativeTo: this.route });
        // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment/case-worker-view-assessment']);
    }
    showAssessment(id: number, row) {
        this.getAsseesmentHistory = row;
        if (this.showAssesment !== id) {
            this.showAssesment = id;
        } else {
            this.showAssesment = -1;
        }
    }
    actionIconDisplay(modal, status: string): boolean {
        if (status === 'View') {
            return modal.assessmentstatustypekey !== 'Open' && modal !== null;
        } else if (status === 'Edit') {
            const cwstatusList = ['Rejected', 'Accepted'];
            const supStatusList = ['Open', 'Accepted'];
            return (!cwstatusList.includes(modal.assessmentstatustypekey) && modal !== null) || (this.roleId.role.name === 'apcs' && !supStatusList.includes(modal.assessmentstatustypekey));
        } else if (status === 'Print') {
            return modal.assessmentstatustypekey !== 'Open' && modal !== null;
        } else if (status === 'InProcess') {
            return modal.assessmentstatustypekey === 'InProcess' && modal !== null;
        }
        return false;
    }
    isIconDisabled(modal) {
        if (this.roleId.role.name === 'apcs') {
            if (modal.assessmentstatustypekey === 'InProcess' || modal.assessmentstatustypekey === 'Rejected' || modal.assessmentstatustypekey === 'Accepted') {
                return 'icon-disabled';
            }
        } else {
            return;
        }
    }

    confirmDelete(assessmentid, index) {
        // a.index = index;
        console.log('assessmentid....index', assessmentid, index);
        this.assessmentTemplateId = assessmentid;
        (<any>$('#delete-assessment-popup')).modal('show');
    }
}
