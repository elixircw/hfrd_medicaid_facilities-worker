import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplaProcessRoutingModule } from './appla-process-routing.module';
import { ApplaProcessComponent } from './appla-process.component';
import { ApplaViewComponent } from './appla-view/appla-view.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatExpansionModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { AgmCoreModule } from '@agm/core';
import { PaginationModule } from 'ngx-bootstrap';
import { ApplaListComponent } from './appla-list/appla-list.component';
import { ApplaProcessService } from './appla-process.service';
import { DsdsActionModule } from '../../dsds-action.module';

@NgModule({
  imports: [
    CommonModule,
    ApplaProcessRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatExpansionModule,
    A2Edatetimepicker,
    AgmCoreModule,
    PaginationModule,
    DsdsActionModule,
  ],
  declarations: [ApplaProcessComponent, ApplaViewComponent, ApplaListComponent],
  providers: [ApplaProcessService]
})
export class ApplaProcessModule { }
