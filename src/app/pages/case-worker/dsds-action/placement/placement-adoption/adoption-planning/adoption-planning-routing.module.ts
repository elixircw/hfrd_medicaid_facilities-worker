import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdoptionPlanningComponent } from './adoption-planning.component';

const routes: Routes = [{
  path: '',
  component: AdoptionPlanningComponent,
  children: [
    { path: 'efforts', loadChildren: './ap-adoption-efforts/ap-adoption-efforts.module#ApAdoptionEffortsModule' },
    { path: 'emotion-ties', loadChildren: './ap-emotional-tiles/ap-emotional-tiles.module#ApEmotionalTilesModule' },
    { path: 'checklist', loadChildren: './ap-chicklist/ap-chicklist.module#ApChicklistModule' },
    { path: 'narrative', loadChildren: './ap-narrative/ap-narrative.module#ApNarrativeModule' },
    { path: 'aca-form' , loadChildren: './ap-aca-form/ap-aca-form.module#ApAcaFormModule'}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdoptionPlanningRoutingModule { }
