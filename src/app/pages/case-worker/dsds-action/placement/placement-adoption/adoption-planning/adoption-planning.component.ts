import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { DataStoreService, AuthService } from '../../../../../../@core/services';
import { DynamicObject, PaginationRequest, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { ActivatedRoute, Router } from '@angular/router';

import { InvolvedPerson } from '../../../../../../@core/common/models/involvedperson.data.model';
import { ValidateAdoption } from '../_entities/adoption.model';
import { DSDS_STORE_CONSTANTS } from '../../../dsds-action.constants';
import { PlacementAdoptionService } from '../placement-adoption.service';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { FinanceUrlConfig } from '../../../../../finance/finance.url.config';
import { FinanceService } from '../../../../../finance/finance.service';
import { ServiceCasePermanencyPlanService } from '../../../service-case-permanency-plan/service-case-permanency-plan.service';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'adoption-planning',
    templateUrl: './adoption-planning.component.html',
    styleUrls: ['./adoption-planning.component.scss']
})
export class AdoptionPlanningComponent implements OnInit {
    id: string;
    store: DynamicObject;
    reportedChild: InvolvedPerson;
    daNumber: string;
    validationMessage = '';
    selectedurl: any;
    agency = '';
    activeRoute: any;
    isChildInPreAdoption: any;
    changehistory = [];
    adjustment: any[];
    paginationInfo: PaginationInfo  = new PaginationInfo();
    pageInfo: PaginationInfo  = new PaginationInfo();
    isAdoptionCreated: boolean;
    providerDetails: any;
    placmentDetails: any;
    overpayments: any[];

    constructor(private _commonHttp: CommonHttpService,
         private _store: DataStoreService,
         private route: ActivatedRoute,
         private _route: Router,
         private _authService: AuthService,
         private _PlacementAdoptionService: PlacementAdoptionService,
         private _financeService: FinanceService,
         private _serviceCasePermanencyPlanService: ServiceCasePermanencyPlanService) {
        this.id = this._store.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._store.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        // this.daNumber = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.store = this._store.getCurrentStore();
    }

    ngOnInit() {
        this.agency = this._authService.getAgencyName();
        // this.ValidateStoreValues();
        this._PlacementAdoptionService.storeDataPatched$.subscribe(data => {
            if (data === 'TRPList') {
        this.ValidateStoreValues();
          }
        });
        this.paginationInfo.pageNumber = 1;
        this.paginationInfo.pageSize = 10;
        this.getBreaklink();
        this.childPlacementList();

        // this.preValidation();
    }


    childPlacementList() {
        this._commonHttp
          .getPagedArrayList(
            new PaginationRequest({
              page: 1,
              limit: 10,
              method: 'get',
              where: { servicecaseid: this._store.getData(CASE_STORE_CONSTANTS.CASE_UID) },
            }),
            'placement/getplacementbyservicecase?filter'
            // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          ).subscribe(result => {
            if (result && result.data) {
                this.placmentDetails = result.data.find(item => item.cjamspid === this.store['placed_child'].cjamspid);
                this.providerDetails = (this.placmentDetails && this.placmentDetails.placements && this.placmentDetails.placements.length && this.placmentDetails.placements[0].providerdetails) ?
                                    this.placmentDetails.placements[0].providerdetails : null;
                if (this.providerDetails) {
                    this.getOverPaymentList(this.providerDetails.provider_id, this.store['placed_child'].cjamspid);
                }
            }
        });
      }

      getOverPaymentList(providerID, clientid) {
        this._commonHttp.getPagedArrayList({
            where: {
              providerid: providerID,
              client_id: clientid
            },
            page: 1,
            limit: null,
            nolimt: true,
            method: 'get'
          }, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.list).subscribe((res: any) => {
            if (res && res.data && res.data.length) {
              this.overpayments = res.data;
            }
          });
      }
 
    ValidateStoreValues() {
        if ( this.store['placed_child'] && this.store['placed_child'].cjamspid) {
            this.reportedChild = this.store['placed_child'];
        }
        if (this.store['placement_child']) {
            const childActorId = this.store['placement_child'].intakeservicerequestactorid;
            // this.getGender(this.store['placed_child'].gender);
            this.getPlacementHistory();
        } else {
            this.planListing();
        }
    }
    validation() {
        if (this.store[DSDS_STORE_CONSTANTS.TPR_LIST]) {
            const trpList = this.store[DSDS_STORE_CONSTANTS.TPR_LIST];
            let isValid = true;
            this.validationMessage = '';
            trpList.forEach(element => {
                if (element.isappealed === 1 && !element.appealdecisiontypekey) {
                    isValid = false;
                    this.validationMessage = 'Must wait for a court order from the appeals hearing upholding the TPR decision prior to moving to an adoption planning case.';
                } else if (element.isappealed === 1 && element.appealdecisiontypekey === 'APGR') {
                    isValid = false;
                    this.validationMessage = 'Cannot continue with Adoption Planning due to Appeal Status!.';
                }
            });
            if (!isValid) {
                (<any>$('#ap-planning')).modal('show');
            } else {
                (<any>$('#ap-planning-start')).modal('show');
            }
        } else {
            this.validationMessage = 'please complete Termination of Parental Rights';
            (<any>$('#ap-planning')).modal('show');
        }
    }

    getPlacementHistory() {
        this._commonHttp
            .getPagedArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        casenumber: this.daNumber
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fostercarereferallistUrl + '?filter'
            )
            .subscribe((res) => {
                const data = res && res.data ? res.data : [];
                this.isChildInPreAdoption = data.some(ele => (ele.placementstructure === 'Pre-Finalized Adoptive Home' && ele.placemententrydate && !ele.placementexitdate));
                this.validation();
                this._PlacementAdoptionService.broadStoreDataPatched('TRPList_a');
            });
    }

    broadCastStoreData(data) {
        this._PlacementAdoptionService.broadStoreDataPatched('TRPList_a');
    }
    private preValidation() {
        this._commonHttp
            .getSingle(
                new PaginationRequest({
                    where: {
                        intakeserviceid: this.id
                    },
                    method: 'get'
                }),
                'adoptionchecklist/validateadoptionplan?filter'
            )
            .subscribe(res => {
                if (res || res.length) {
                    if (res.statuscode === 500) {
                        this.validationMessage = 'Must wait for a court order from the appeals hearing upholding the TPR decision prior to moving to an adoption planning case.';
                        (<any>$('#ap-planning')).modal('show');
                    }
                }
            });
    }
    navigateTo() {
        const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/tpr-recom';
        this._route.navigate([redirectUrl]);
    }
    alertforSave(selectedUrl) {
        this.selectedurl = selectedUrl;
       (<any>$('#save-alert')).modal('show');
    }

    checkDataSave() {
        const urlSegments = this._route.url.split('/');
        if (urlSegments && urlSegments.length) {
          const routeLength = urlSegments.length;
          this.activeRoute =  urlSegments[routeLength - 1 ] ;
        }

        const isUnsavedData = this._PlacementAdoptionService.AdoptionPlanningSaveState;
        // console.log(isUnsavedData);
        const url = this._PlacementAdoptionService.AdoptionPlanningTab;
        const canRedirect = (isUnsavedData && !this.isAdoptionCreated ) ? true : false;
        return canRedirect;
        // if(isUnsavedData) {
        // const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + 'sc-permanency-plan/placement/adoption/planning/'+url ;
        // this._route.navigate([redirectUrl]);
        // }
    }
    private getReportedChild(childActorId) {
        this._commonHttp
            .getSingle(
                {
                    method: 'get',
                    where: {objecttypekey: 'servicecase', objectid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
            .subscribe(res => {
                if (res && res.data) {
                const repChild = res.data.filter(child => child.personid === childActorId);
                if (repChild && repChild.length) {
                    this.getGender(repChild[0].gender);
                    this.reportedChild = repChild[0];
                }
                 }
            });
    }
    private getGender(gender) {
        this._commonHttp
            .getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.GenderTypeUrl + '?filter'
            )
            .subscribe(res => {
                const childGender = res.filter(item => item.gendertypekey === gender);
                this.reportedChild.genderText = childGender && childGender.length > 0 ? childGender[0].typedescription : null;
            });
    }
    private planListing() {
        this._commonHttp
            .getSingle(
                {
                    method: 'get',
                    page: 1,
                    limit: 10,
                    where: { objectid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PermanencyPlan.PermanencyPlanList + '?filter'
            )
            .subscribe(res => {
                if (res.length) {
                    res.map(plan => {
                        // if (plan.status === 'Approved') {
                        //     if (plan.primarypermanency.length) {
                                // this._store.setData('permanencyPlan', plan, true);
                                this.getReportedChild(plan.personid);
                        //     }
                        // }
                    });
                }
                if (res.data && res.data.length) {
                    res.data.map(plan => {
                        if (plan.status === 'Approved') {
                            if (plan.primarypermanency.length) {
                                this._store.setData('permanencyPlan', plan, true);
                                this.getReportedChild(plan.intakeservicerequestactorid);
                            }
                        }
                    });
                }
            });
    }

    private getBreaklink() {
        this._commonHttp
          .getArrayList({
            method: 'get', where: {
              adoptionplanningid: this._PlacementAdoptionService.getAdoptionPlanning().adoptionplanningid
            }
          }, 'adoptionbreakthelink/getadoptionbreakthelink?filter')
          .subscribe(res => {
            if (res && res.length) {
              res.map((item) => {
                const adoptionBreakLinkList = item && item.getadoptionbreakthelink ? item.getadoptionbreakthelink.map((breaklink) => {
                  this.isAdoptionCreated = (breaklink.adoptioncasenumber) ? true : false;
                  return breaklink;
                }) : [];
              });
            }
          });
      }

    fiscalAudit() {
        if ( this.store['placed_child'] && this.store['placed_child'].cjamspid) {
            this.reportedChild = this.store['placed_child'];
            this._financeService.getFiscalAudit(this.reportedChild.cjamspid, 1, this.overpayments);
            (<any>$('#fiscal-audit')).modal('show');
        }
/*         this._commonHttp.getPagedArrayList({
          where: {
            receivable_detail_id: null,
            clientid: this.reportedChild.cjamspid
          },
          page: this.paginationInfo.pageNumber,
          limit: this.paginationInfo.pageSize,
          method: 'get'
        }, FinanceUrlConfig.EndPoint.accountsReceivable.audit.auditList).subscribe((result: any) => {
            if (result && result.data && result.data.length > 0) {
              this.changehistory = result.data;
              for (let i = 0;  i < result.data.length; i++) {
                if (result.data[i].adjstment) {
                  this.adjustment = result.data[i].adjstment;
                }
              }
            }
        }); */
      }
}
