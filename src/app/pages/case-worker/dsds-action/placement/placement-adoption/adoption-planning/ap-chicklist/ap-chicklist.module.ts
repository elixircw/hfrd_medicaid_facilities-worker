import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { ApChicklistRoutingModule } from './ap-chicklist-routing.module';
import { ApChicklistComponent } from './ap-chicklist.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedDirectivesModule } from '../../../../../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ApChicklistRoutingModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    SharedDirectivesModule
  ],
  declarations: [ApChicklistComponent]
})
export class ApChicklistModule { }
