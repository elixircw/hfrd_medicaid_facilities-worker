import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { ActivatedRoute, Router } from '@angular/router';

import { DynamicObject, DropdownModel, PaginationRequest } from '../../../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../../../@core/entities/constants';
import { DataStoreService } from '../../../../../../../@core/services';
import { AlertService } from '../../../../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../../../../@core/services/common-http.service';
import { EffortDetail } from '../_entities/adoption-planning.model';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { Observable } from 'rxjs/Rx';
import { PlacementAdoptionService } from '../../placement-adoption.service';
import { CASE_STORE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'ap-adoption-efforts',
    templateUrl: './ap-adoption-efforts.component.html',
    styleUrls: ['./ap-adoption-efforts.component.scss']
})
export class ApAdoptionEffortsComponent implements OnInit {
    effortForm: FormGroup;
    planningForm: FormGroup;
    id: string;
    daNumber: string;
    store: DynamicObject;
    effortDetail: EffortDetail[] = [];
    private childActorId: string;
    effortTypes$: Observable<DropdownModel[]>;
    permanencyplanid: string;
    selectedEffort: { index: number; action: string };
    unsavedForm: boolean;
    isAdoptionCreated: boolean;
    constructor(
        private _formBuilder: FormBuilder,
        private _store: DataStoreService,
        private _commonHttp: CommonHttpService,
        private route: ActivatedRoute,
        private _alert: AlertService,
        private _router: Router,
        private _PlacementAdoptionService: PlacementAdoptionService,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.store = this._store.getCurrentStore();
        const placement = this.store['placement_child'];
        this.permanencyplanid = (placement) ? placement.permanencyplanid : null;
        this.childActorId = (placement) ? placement.intakeservicerequestactorid : null;
    }

    ngOnInit() {
        // if (this.store['permanencyPlan']) {
        //     this.childActorId = this.store['permanencyPlan'].intakeservicerequestactorid;
        // }
        this.initializeForm();
        this.loadDropdown();
        this.effortListing();
        this.getBreaklink();
        this.planningForm.get('isnoeffort').valueChanges.subscribe(effort => {
            if (effort && (!this.effortDetail || !this.effortDetail.length)) {
                (<any>$('#info-pop')).modal('show');
            }
        });

    }
    manageEffort(action: string, i?: number) {
        this.effortForm.enable();
        this.effortForm.reset();
        this.selectedEffort = {
            index: i,
            action: action
        };
        (<any>$('#manage-effort')).modal('show');
        if (action !== 'Add') {
            this.effortForm.patchValue(this.effortDetail[i]);
        }
        if (action === 'View') {
            this.effortForm.disable();
        }
    }

    onEffortTypeChange(event) {
        console.log(event);
        this.effortForm.patchValue({ description: '' });
    }

    saveEffort() {
        if (!this.selectedEffort.index && this.selectedEffort.index !== 0) {
            this.effortDetail.push(this.effortForm.value);
        } else if (this.selectedEffort.index || this.selectedEffort.index === 0) {
            this.effortDetail[this.selectedEffort.index] = this.effortForm.value;
        }
        this.unsavedForm = true;
        this._PlacementAdoptionService.setAdoptionPlanningData('effort', this.unsavedForm);
        (<any>$('#manage-effort')).modal('hide');
        this.effortForm.reset();
    }
    saveAdoption(nextNavigation) {
        if (this.validateEffort()) {
            const adoptionEffortInput = Object.assign(this.planningForm.value);
            if (this.effortDetail && this.effortDetail.length) {
                adoptionEffortInput.adoptionefforts = this.effortDetail;
            }
            adoptionEffortInput.intakeserviceid = null;
            adoptionEffortInput.servicecaseid = this.id;
            adoptionEffortInput.permanencyplanid =  this.permanencyplanid;
            adoptionEffortInput.intakeservicerequestactorid = this.childActorId ? this.childActorId : null;
            if (!adoptionEffortInput.adoptionplanningid) {
                delete adoptionEffortInput.adoptionplanningid;
            }
            adoptionEffortInput.isnoeffort = adoptionEffortInput.isnoeffort ? 1 : 0;
            adoptionEffortInput.isexceptiongranted = adoptionEffortInput.isexceptiongranted ? 1 : 0;
            this._commonHttp.create(adoptionEffortInput, CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEffortAdd).subscribe(
                res => {
                    this._alert.success('Adoption Effort saved successfully');
                    this.unsavedForm = false;
                    this._PlacementAdoptionService.setAdoptionPlanningData('effort', this.unsavedForm);
                    // this.planningForm.reset();
                    if (nextNavigation === 'Next') {
                        const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/planning/emotion-ties';
                        this._router.navigate([redirectUrl]);
                    } else {
                        // this.effortListing();
                        this.unsavedForm = false;
                        this._PlacementAdoptionService.setAdoptionPlanningData('effort', this.unsavedForm);
                    }
                },
                err => {
                    this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    private validateEffort() {
        if (!((this.planningForm.value.isnoeffort) || (this.effortDetail.length) ||
                (this.planningForm.value.isexceptiongranted && this.planningForm.value.dateofexceptiongranted))) {
            this._alert.error('Effort Detail Listing or Exception Granted is empty!');
            return false;
        }
        if (this.planningForm.value.isnoeffort && (!this.planningForm.value.remarks && !this.effortDetail.length)) {
            this._alert.error('Please update notes to proceed!');
            return false;
        } else {
            return true;
        }
    }
    private initializeForm() {
        this.planningForm = this._formBuilder.group({
            isnoeffort: [''],
            remarks: [''],
            isexceptiongranted: [''],
            dateofexceptiongranted: [null],
            adoptionplanningid: [null]
        });
        this.effortForm = this._formBuilder.group({
            notes: [''],
            effortdate: [''],
            efforttype: ['']
        });
    }
    private effortListing() {
        this._commonHttp
            .getSingle(
                new PaginationRequest({
                    where: {
                         permanencyplanid: this.permanencyplanid
                        // intakeserviceid: this.id,
                        //  intakeservicerequestactorid: this.childActorId ? this.childActorId : null
                    },
                    method: 'get',
                    page : 1,
                    limit: 10
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEffortList + '?filter'
            )
            .subscribe(res => {
                if (res && res.length && res[0].getadoptionplanning) {
                    this.effortDetail = (res[0].getadoptionplanning[0].adoptioneffortsdetails) ? res[0].getadoptionplanning[0].adoptioneffortsdetails : [];
                    this.planningForm.patchValue(res[0].getadoptionplanning[0]);
                    this._store.setData('adoptionEffort', res[0].getadoptionplanning[0]);
                    this._PlacementAdoptionService.setAdoptionPlanning(res[0].getadoptionplanning[0]);
                }
                this.onChanges();
            });
    }
    private onChanges() {
        this.planningForm.valueChanges.subscribe(effort => {
            // Object.keys(this.planningForm.controls).forEach(key => {
            //     if(this.planningForm.get(key).value() && this.planningForm.get(key).value() !== '') {
            // console.log(key, this.planningForm.get(key).value());
            // if(!this.planningForm.pristine) {
            this.unsavedForm = true;
            this._PlacementAdoptionService.setAdoptionPlanningData('effort', this.unsavedForm);
            // }
            //     }
            //   });
        });
    }
    private loadDropdown() {
        this.effortTypes$ = this._commonHttp
            .getArrayList(
                {
                    where: {
                        referencetypeid: '42',
                        teamtypekey: null
                    },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.hearingoutComeUrl + '?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }

    private getBreaklink() {
        this._commonHttp
          .getArrayList({
            method: 'get', where: {
              adoptionplanningid: this._PlacementAdoptionService.getAdoptionPlanning().adoptionplanningid
            }
          }, 'adoptionbreakthelink/getadoptionbreakthelink?filter')
          .subscribe(res => {
            if (res && res.length) {
              res.map((item) => {
                const adoptionBreakLinkList = item && item.getadoptionbreakthelink ? item.getadoptionbreakthelink.map((breaklink) => {
                  this.isAdoptionCreated = (breaklink.adoptioncasenumber) ? true : false;
                  return breaklink;
                }) : [];
              });
            }
          });
      }
}
