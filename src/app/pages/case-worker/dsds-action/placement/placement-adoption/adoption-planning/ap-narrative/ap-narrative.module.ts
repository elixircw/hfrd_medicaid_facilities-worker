import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import { QuillModule } from 'ngx-quill';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { ApNarrativeRoutingModule } from './ap-narrative-routing.module';
import { ApNarrativeComponent } from './ap-narrative.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ApNarrativeRoutingModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    QuillModule,
    ReactiveFormsModule
  ],
  declarations: [ApNarrativeComponent]
})
export class ApNarrativeModule { }
