import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import { ApEmotionalTilesRoutingModule } from './ap-emotional-tiles-routing.module';
import { ApEmotionalTilesComponent } from './ap-emotional-tiles.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { PaginationModule } from 'ngx-bootstrap';
import { FormMaterialModule } from '../../../../../../../@core/form-material.module';
import { ControlMessagesModule } from '../../../../../../../shared/modules/control-messages/control-messages.module';

@NgModule({
  imports: [
    CommonModule,
    ApEmotionalTilesRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,    
    ControlMessagesModule,
    FormMaterialModule,
    ReactiveFormsModule,
    PaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8CRCgWX0eRvC8XfFnaaMPm-36fb2GN9M'
    })
  ],
  declarations: [ApEmotionalTilesComponent]
})
export class ApEmotionalTilesModule { }
