import { Component, OnInit, Input, Output, EventEmitter,ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { DataStoreService } from '../../../../../../../@core/services';
import { CommonHttpService } from '../../../../../../../@core/services/common-http.service';
import { CASE_STORE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';
@Component({
  selector: 'ap-aca-form',
  templateUrl: './ap-aca-form.component.html',
  styleUrls: ['./ap-aca-form.component.scss']
})
export class ApAcaFormComponent implements OnInit {
  clientIDInput: any;
  removalIDInput : any ;
  hasAdoptionApplicabilityInf: boolean;
  id: any;
  daNumber: any;
  child: any;
  @ViewChild('childassessment') childassessment;
  constructor( private _commonHttp: CommonHttpService, private route: ActivatedRoute,
    private _router: Router,
    private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
    this.clientIDInput = this._dataStoreService.getData('childforGAP');
    this.removalIDInput = this._dataStoreService.getData('childremovalid');
    this._dataStoreService.setData('adoption_clientid',this.clientIDInput);
    this._dataStoreService.setData('adoption_removalid',this.removalIDInput);
    this.hasAdoptionApplicabilityInfo();
  }

  saveData(){
    this.childassessment.saveData();
  }

  saveDataNext(){
    this.childassessment.saveData();
    const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/planning/efforts';
    this._router.navigate([redirectUrl]);
  }

  sendToIVE() {
    this.childassessment.saveData();
    this._commonHttp.create({ ivestatus: 'PENDING',
      clientId:this.clientIDInput,
    removalId:this.removalIDInput},
      'titleive/updatestatus'
    ).subscribe(res => {
      if (res && res.affectedRows) {
        this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-permanency-plan/']);
      }
    });
  }

  hasAdoptionApplicabilityInfo(){
    if(this.hasAdoptionApplicabilityInf)
    return true;
    else
    return this._dataStoreService.getData('hasAdoptionApplicabilityInfo');
  }
}
