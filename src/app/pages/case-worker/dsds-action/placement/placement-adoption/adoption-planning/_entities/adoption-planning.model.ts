export interface EffortDetail {
    notes: string;
    effortdate: string;
    efforttypekey: string;
}

export interface AdoptionPlanningEffort {
    adoptionefforts: EffortDetail[];
    intakeserviceid: string;
    intakeservicerequestactorid: string;
    isnoeffort: string;
    remarks: string;
    adoptionplanningid?: any;
}
