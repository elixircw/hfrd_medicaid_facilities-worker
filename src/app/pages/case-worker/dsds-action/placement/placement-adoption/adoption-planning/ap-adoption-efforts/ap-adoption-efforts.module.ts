import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule
} from '@angular/material';

import { QuillModule } from 'ngx-quill';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { ApAdoptionEffortsRoutingModule } from './ap-adoption-efforts-routing.module';
import { ApAdoptionEffortsComponent } from './ap-adoption-efforts.component';

@NgModule({
    imports: [
        CommonModule,
        ApAdoptionEffortsRoutingModule,
        MatCardModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatNativeDateModule,
        MatRadioModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        A2Edatetimepicker,
        QuillModule,
        ReactiveFormsModule
    ],
    declarations: [ApAdoptionEffortsComponent]
})
export class ApAdoptionEffortsModule {}
