import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { DynamicObject, PaginationRequest } from '../../../../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../../../../@core/services/common-http.service';
import { GLOBAL_MESSAGES } from '../../../../../../../@core/entities/constants';
import { AlertService } from '../../../../../../../@core/services/alert.service';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { PlacementAdoptionService } from '../../placement-adoption.service';
import { CASE_STORE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'ap-narrative',
    templateUrl: './ap-narrative.component.html',
    styleUrls: ['./ap-narrative.component.scss']
})
export class ApNarrativeComponent implements OnInit {
    private id: string;
    apNarrativeForm: FormGroup;
    store: DynamicObject;
    private childActorId: string;
    daNumber: string;
    permanencyplanid: string;
    isAdoptionCreated: boolean;
    constructor(
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _store: DataStoreService,
        private _commonHttp: CommonHttpService,
        private _alertService: AlertService,
        private _router: Router,
        private _PlacementAdoptionService: PlacementAdoptionService,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.store = this._store.getCurrentStore();
        const placement_child = this.store['placement_child'];
        this.permanencyplanid = (placement_child) ? placement_child.permanencyplanid : null;
    }

    ngOnInit() {
        if (this.store['permanencyPlan']) {
            this.childActorId = this.store['permanencyPlan'].intakeservicerequestactorid;
        }
        this.apNarrativeForm = this._formBuilder.group({
            adoptiondate: [null],
            narrative: ['']
        });
        this.getNarrative();
        this.getBreaklink();

    }
    private getNarrative() {
        this._commonHttp
            .getSingle(
                new PaginationRequest({
                    where: {
                        permanencyplanid: this.permanencyplanid
                        // intakeserviceid: this.id,
                        // intakeservicerequestactorid: this.childActorId
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEffortList + '?filter'
            )
            .subscribe(res => {
                if (res && res.length && res[0].getadoptionplanning) {
                    this.apNarrativeForm.patchValue(res[0].getadoptionplanning[0]);
                    this._store.setData('adoptionEffort', res[0].getadoptionplanning[0]);
                }
                this.onChanges();
            });
    }
    private onChanges() {
        this.apNarrativeForm.valueChanges.subscribe(effort => {

            const unsavedForm = true;
            this._PlacementAdoptionService.setAdoptionPlanningData('effort', unsavedForm);

        });
    }
    addNarrative(nextNavigation) {
        const model = this.apNarrativeForm.value;
        model.adoptionplanningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
        this._commonHttp.create(model, 'adoptionplanning/updateadoptionnarrative').subscribe(
            result => {
                this._alertService.success('Adoption Narrative saved successfully!');
                const unsavedForm = false;
                this._PlacementAdoptionService.setAdoptionPlanningData('effort', unsavedForm);
                // this.apNarrativeForm.reset();
                if (nextNavigation === 'Next') {
                    const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/planning/checklist';
                    this._router.navigate([redirectUrl]);
                } else {
                    // this.getNarrative();
                }
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    private getBreaklink() {
        this._commonHttp
          .getArrayList({
            method: 'get', where: {
              adoptionplanningid: this._PlacementAdoptionService.getAdoptionPlanning().adoptionplanningid
            }
          }, 'adoptionbreakthelink/getadoptionbreakthelink?filter')
          .subscribe(res => {
            if (res && res.length) {
              res.map((item) => {
                const adoptionBreakLinkList = item && item.getadoptionbreakthelink ? item.getadoptionbreakthelink.map((breaklink) => {
                  this.isAdoptionCreated = (breaklink.adoptioncasenumber) ? true : false;
                  return breaklink;
                }) : [];
              });
            }
          });
      }
}
