import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { DynamicObject, PaginationRequest } from '../../../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../../../@core/entities/constants';
import { AlertService } from '../../../../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../../../../@core/services/common-http.service';
import { DataStoreService } from '../../../../../../../@core/services/data-store.service';
import { CheckList, Adoption } from '../../_entities/adoption.model';
import { map } from 'rxjs/operator/map';
import { AuthService } from '../../../../../../../@core/services/auth.service';
import { AppConstants } from '../../../../../../../@core/common/constants';
import { PlacementAdoptionService } from '../../placement-adoption.service';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'ap-chicklist',
    templateUrl: './ap-chicklist.component.html',
    styleUrls: ['./ap-chicklist.component.scss']
})
export class ApChicklistComponent implements OnInit {
    apCheckListForm: FormGroup;
    reviewCheckList: CheckList[];
    store: DynamicObject;
    isSupervisor: boolean;
    approvalStatus: string;
    private id: string;
    submitby: string;
    submiton: any;
    approvedby: string;
    approvedon: any;
    
effortDetailSaved : boolean
narrativesaved : boolean;
emotionalTiesSaved: boolean;

    constructor(
        private route: ActivatedRoute,
        private _store: DataStoreService,
        private _commonHttp: CommonHttpService,
        private _formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _router: Router,
        private _alert: AlertService,
        private _authService: AuthService,
        private _PlacementAdoptionService: PlacementAdoptionService
    ) {
        this.store = this._store.getCurrentStore();
        this.id = this.store['CASEUID'];
    }

    ngOnInit() {
        const userInfo = this._authService.getCurrentUser();
        this.isSupervisor = userInfo.role.name === AppConstants.ROLES.SUPERVISOR;
        this.apCheckListForm = this._formBuilder.group({
            disclosuredate: [null, [Validators.required]],
            personinfomation: ['', [Validators.required]],
            siblingage: [null],
            siblinginformation: [''],
            remarks: [''],
            checklistid: [''],
            isselected: [null],
            worker: ['', [Validators.required]],
            nonstaffmember: [''],
            localdepartment: [''],
            adoptionchecklistid: [null]
        });
        this.getCheckList();
        this.getEmotionalTieList();
        this.effortListing();
        this.preFillApCheckListForm();
        this._PlacementAdoptionService.storeDataPatched$.subscribe(data => {
            if (data === 'TRPList_a') {
        this.getCheckList();
        this.preFillApCheckListForm();
        this.getEmotionalTieList();
        this.effortListing();
            }
        });
    }

    preFillApCheckListForm() {
        const workerName = this._authService.getCurrentUser().user.userprofile.fullname;
        this.apCheckListForm.patchValue({ 'worker': workerName });
    }
    onCheckListChange(event, index) {
        this.reviewCheckList[index].isselected = event;
        this.apCheckListForm.markAsDirty();
    }
    onRemarksSave(event, index) {
        if (event.target.value) {
            this.reviewCheckList[index].remarks = event.target.value;
            this.apCheckListForm.markAsDirty();
            this.reviewCheckList[index].isRemarksValid = false;
        } else {
            this.reviewCheckList[index].isRemarksValid = true;
        }
    }

    addCheckList(model) {
        if(this.effortDetailSaved  && 
            this.narrativesaved  &&
            this.emotionalTiesSaved) {

        }
        else {
            
            (<any>$('#planning-validation')).modal('show');
            return false;
        }
        if (this.reviewCheckList && this.reviewCheckList.length) {
            const checkListInalid = this.reviewCheckList.filter(item => item.isselected !== '0' && item.isselected !== '1');
            const remarksValid = this.reviewCheckList.filter(item => item.isselected === '0' && !item.remarks);
            if(checkListInalid && checkListInalid.length) {
                this._alert.error('Please fill required fields to proceed');
                return;
            }
            if (remarksValid && remarksValid.length) {
                this.reviewCheckList.map(item => {
                    if (item.isselected === '0' && !item.remarks) {
                        item.isRemarksValid = true;
                    } else {
                        item.isRemarksValid = false;
                    }
                });
                this._alert.error('Please fill remarks to proceed');
                return;
            }
        }
        if (!model.adoptionchecklistid) {
            delete model.adoptionchecklistid;
        }
        model.adoptionchecklist = [];
        model.intakeserviceid = this.id;
        model.servicecaseid = this.id;
        this.reviewCheckList.map(item => {
            model.adoptionchecklist.push({ checklistid: item.checklistid, isselected: Number(item.isselected), remarks: item.remarks });
        });
        model.adoptionplanningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
        this.approvalStatus = 'Review';
        this._commonHttp.create(model, 'adoptionchecklist/addupdate').subscribe(
            result => {
                this._alertService.success('Adoption Checklist Submitted for Approval Successfully!!');
               // this.apCheckListForm.reset();
               //  this.getCheckList();
               this.getAdoption();
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                this.approvalStatus = null;
            }
        );
    }
    approveAdoption(approvalStatus) {
        this._commonHttp
            .create(
                {
                    objectid: this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null,
                    eventcode: 'ADPR',
                    status: approvalStatus,
                    comments: 'Adoption Plan ' + approvalStatus,
                    notifymsg: 'Adoption Plan ' + approvalStatus,
                    routeddescription: 'Adoption Plan ' + approvalStatus,
                    servicecaseid: this.id
                },
                'routing/routingupdate'
            )
            .subscribe(
                res => {
                    this._alert.success('Adoption Planning is ' + approvalStatus + ' successfully!');
                    this.approvalStatus = approvalStatus;
                    this.getAdoption();
                },
                err => {
                    this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }
    private effortListing() {
        const placement = this.store['placement_child'];
        const permanencyplanid = (placement) ? placement.permanencyplanid : null;
        this._commonHttp
            .getSingle(
                new PaginationRequest({
                    where: {
                         permanencyplanid: permanencyplanid
                        // intakeserviceid: this.id,
                        //  intakeservicerequestactorid: this.childActorId ? this.childActorId : null
                    },
                    method: 'get',
                    page : 1,
                    limit: 10
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEffortList + '?filter'
            )
            .subscribe(res => {
                if (res && res.length && res[0].getadoptionplanning) {
                    this.effortDetailSaved = (res[0].getadoptionplanning[0].adoptioneffortsdetails || res[0].getadoptionplanning[0].isnoeffort) ? true : false;
                    this.narrativesaved = (res[0].getadoptionplanning[0].narrative) ? true : false;
                   
                }
            });
    }

    private getEmotionalTieList() {
        this._commonHttp
            .getArrayList(
                {
                    where: { adoptionplanningid: this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEmotionalTieList + '?filter'
            )
            .subscribe(res => {
                if (res && res.length) {
                    this.emotionalTiesSaved = true;
                    // this.emotionalTies = res[0].adoptionemotionaldetails ? res[0].adoptionemotionaldetails : [];
                }
            });
    }
    private getCheckList() {
        this._commonHttp
            .getArrayList(
                new PaginationRequest({
                    where: { checklisttypekey: 'ADOPT', order: 'displayorder ASC' },
                    method: 'get'
                }),
                'checklist?filter'
            )
            .subscribe(result => {
                result.map(item => {
                    item.isselected = 2;
                });
                this.reviewCheckList = result;
                this.getAdoption();
            });
    }

    private getAdoption() {
        this._commonHttp
            .getArrayList(
                new PaginationRequest({
                    where: { adoptionplanningid: this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null },
                    method: 'get'
                }),
                'adoptionchecklist/getadoptionchecklist?filter'
            )
            .subscribe(result => {
                if (result && result.length) {
                    this.reviewCheckList.map(item => {
                        result[0].adoptionchecklist.map(selItem => {
                            if (selItem.checklistid === item.checklistid) {
                                item.isselected = selItem.isselected + '';
                                item.remarks = selItem.remarks ? selItem.remarks : null;
                                item.isRemarksValid = false;
                            }
                        });
                    });
                    this.approvalStatus = result[0].status;
                    this.submitby = result[0].submitby;
                    this.submiton = result[0].submiton;
                    this.approvedby = result[0].approvedby;
                    this.approvedon = result[0].approvedon;
                    this.apCheckListForm.patchValue(result[0]);
                } else {
                    this.reviewCheckList.map(item => {
                        item.remarks = null;
                        item.isRemarksValid = false;
                    });
                }
            });
    }
}
