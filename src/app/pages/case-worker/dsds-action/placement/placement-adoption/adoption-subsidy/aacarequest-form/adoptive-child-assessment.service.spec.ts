import { TestBed, inject } from '@angular/core/testing';

import { AdoptiveChildAssessmentService } from './adoptive-child-assessment.service';

describe('AdoptiveChildAssessmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdoptiveChildAssessmentService]
    });
  });

  it('should be created', inject([AdoptiveChildAssessmentService], (service: AdoptiveChildAssessmentService) => {
    expect(service).toBeTruthy();
  }));
});
