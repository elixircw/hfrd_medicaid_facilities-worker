import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdoptionSubsidyRoutingModule } from './adoption-subsidy-routing.module';
import { AdoptionSubsidyComponent } from './adoption-subsidy.component';
import { FormMaterialModule } from '../../../../../../@core/form-material.module';
import { AacarequestFormComponent } from './aacarequest-form/aacarequest-form.component';
import { AdoptiveChildAssessmentService } from './aacarequest-form/adoptive-child-assessment.service';
@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    AdoptionSubsidyRoutingModule
  ],
  declarations: [AdoptionSubsidyComponent,AacarequestFormComponent],
  providers: [AdoptiveChildAssessmentService]
})
export class AdoptionSubsidyModule { }
