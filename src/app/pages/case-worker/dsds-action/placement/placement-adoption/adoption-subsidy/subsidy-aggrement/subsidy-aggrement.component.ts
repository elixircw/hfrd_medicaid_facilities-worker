import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicObject, PaginationRequest, PaginationInfo } from '../../../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, DataStoreService, AuthService, SessionStorageService, CommonDropdownsService } from '../../../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { PlacementAdoptionService } from '../../placement-adoption.service';
import { GLOBAL_MESSAGES } from '../../../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../../../../case-worker-url.config';
import { AppUser } from '../../../../../../../@core/entities/authDataModel';
import { Observable } from 'rxjs/Observable';
import { SpeechRecognitionService } from '../../../../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { config } from '../../../../../../../../environments/config';
import { AppConfig } from '../../../../../../../app.config';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { HttpHeaders } from '@angular/common/http';
import { InvolvedPerson } from '../../../../../../../@core/common/models/involvedperson.data.model';
import * as moment from 'moment';
import { AppConstants } from '../../../../../../../@core/common/constants';
import { FinanceService } from '../../../../../../finance/finance.service';
import { FinanceUrlConfig } from '../../../../../../finance/finance.url.config';
declare let google: any;

const LIVING_ARRANGEMENT = 'LA';
const VOID_PLACEMENT = 1;
const RESPONSE_ACCEPTED_YES = '4612';
const RESPONSE_REJECTED = 'Rejected';
@Component({
  selector: 'subsidy-aggrement',
  templateUrl: './subsidy-aggrement.component.html',
  styleUrls: ['./subsidy-aggrement.component.scss']
})
export class SubsidyAggrementComponent implements OnInit, OnDestroy {
  subsidyAgreementRateForm: FormGroup;
  subsidyAgreementForm: FormGroup;
  providerSearchForm: FormGroup;
  routingForm: FormGroup;
  currProcess: string;
  id: string;
  speechData: string;
  selectedViewProvider: any;
  selectedparent: any;
  adoptiveparent1: string;
  parent1providerid: string;
  parent2providerid: string;
  isActivePlacement: boolean;
  placementStrType: any;
  adoptiveparent2: string;
  notification: string;
  daNumber: string;
  fcpaginationInfo: PaginationInfo = new PaginationInfo();
  store: DynamicObject;
  selectedProvider: any;
  permanencyplanid: string;
  agreementRate: any[];
  fcProviderSearch: any;
  parent1providername: string;
  parent2providername: string;
  markersLocation = ([] = []);
  zoom: number;
  defaultLat = 39.29044;
  defaultLng = -76.61233;
  paginationInfo: PaginationInfo = new PaginationInfo();
  fcTotal: any;
  childPlacement: any;
  breakLink: any;
  isView: boolean;
  specialNeedsDropDown: any[];
  relationshipDropDown: any[];
  childCharacteristics: any[];
  otherLocalDeptmntType: any[];
  childPlacedfromDropDown: any[];
  childPlacedbyDropDown: any[];
  bundledPlcmntServicesType: any[];
  involvedPersons$: Observable<any[]>;
  involvedPersons: any[] = [];
  selectedIndex: number;
  adoptionagreementrateid: any;
  genderDropdownItems: any[];
  isEdit: boolean;
  minAge: number;
  maxAge: number;
  gender: string;
  lat = 51.678418;
  lng = 7.809007;
  showMap: boolean;
  agreement: any;
  child: any;
  finalizationdate: any;
  isSupervisor: boolean;
  user: AppUser;
  approvalStatus: string;
  recognizing = false;
  currentLanguage: string;
  speechRecogninitionOn: boolean;
  token: AppUser;
  uploadedFile: any = [];
  deleteAttachmentIndex: number;
  reportedChild: InvolvedPerson;
  isAdoptionCase: boolean;
  childremoval: any;
  ratemaxDate: any;
  agreementMinDate: any;
  rateMinDate: any;
  disableaddrate: boolean;
  isAdoptionCreated: boolean;
  agreementsignedDate: any;
  isRequired: boolean;
  medicalAssistanceOnlyGetChecked = false;
  paymentAmountPattern = '^[1-9][0-9]*([.][0-9]{2}|)$';
  annualReviewList: any;
  isServiceCase: any;
  serviceCase: boolean;
  overpayments: any[];
  placmentDetails: any;
  providerDetails: any;
  provider_id: any;

  constructor(
    private _commonHttp: CommonHttpService,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _alert: AlertService,
    private _store: DataStoreService,
    private _router: Router,
    private _PlacementAdoptionService: PlacementAdoptionService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _session: SessionStorageService,
    private _speechRecognitionService: SpeechRecognitionService,
    private speechRecognizer: SpeechRecognizerService,
    private _uploadService: NgxfUploaderService,
    private _dataStoreService: DataStoreService,
    private _commonDDService: CommonDropdownsService,
    private _financeService: FinanceService
  ) {
    // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    // // console.log(this.id);

    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.store = this._store.getCurrentStore();
    // this.ratemaxDate = new Date();
    this.id = this.store['CASEUID'];
    this.daNumber = this.store['DANUMBER'];
    this.breakLink = this.store['adoptionBreakLink'];
    if (this.store['placement_child']) {
      this.childPlacement = this.store['placement_child'];
      this.permanencyplanid = this.store['placement_child'].permanencyplanid;

      this.child = this.store[CASE_STORE_CONSTANTS.PLACED_CHILD];
    }
    this._PlacementAdoptionService.storeDataPatched$.subscribe(data => {
      this.getAgreementListing(false);
    });
    const caseType = this._store.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);

    if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
        this.isAdoptionCase = true;
    } else {
      this.isAdoptionCase = false;
    }
    this.childPlacementList();
  }

  ngOnInit() {
    this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    if (this.isServiceCase === 'true') {
      this.serviceCase = true;
    } else {
        this.serviceCase = false;
    }
    this.currProcess = 'search';
    this.token = this._authService.getCurrentUser();
    this.effortListing();
    this.formInitialize();
    this.getAgreementListing(false);
    this.getDropDownList();
    this.getPlacementStrType(null);
    this.loadGenderDropdownItems();
    this.getChildCharacteristics();
    this.getOtherLocalDeptmntType();
    this.getBundledPlcmntServicesType();
    this.getReviews();
    this.currentLanguage = 'en-US';
    this.speechRecognizer.initialize(this.currentLanguage);


    this.user = this._authService.getCurrentUser();
    this.getInvolvedPerson();
    if (this.user.role.name === 'apcs') {
      this.isSupervisor = true;
      this.subsidyAgreementForm.disable();
      this.subsidyAgreementRateForm.disable();

    }
    this.getReportedChild();
    if (this.childPlacement && this.childPlacement.providerdetails) {
      // console.log('Saved Provider Details', this.childPlacement.providerdetails);
      this.parent1providerid = this.childPlacement.providerdetails.provider_id ? this.childPlacement.providerdetails.provider_id : null;
      this.parent1providername = this.childPlacement.providerdetails.providername ? this.childPlacement.providerdetails.providername : null;
      this.subsidyAgreementForm.patchValue({ 'parent1providername': this.parent1providername ? this.parent1providername : null });
      this.subsidyAgreementForm.patchValue({ 'parent1providerid': this.parent1providerid ? this.parent1providerid : null });
    }
    this.planListing();
    this.getBreaklink();
    this.subsidyAgreementForm.get('startdate').valueChanges.subscribe(data => {
           this.onchangeAgreementstartdate();
    });
    this.isRequired = true;
    this.decideSSAApproval();
  }

  onchangeAgreementstartdate() {
    const agreementDate = this.subsidyAgreementForm.getRawValue().startdate;
    if (agreementDate) {
    this.agreementsignedDate = agreementDate;
    this.subsidyAgreementForm.patchValue({
      parent1signdate: agreementDate,
      ldssdate: agreementDate
    });
    }
  }

  getReviews() {
    this._commonHttp.getArrayList(
        new PaginationRequest({
            page: 1,
            limit: 10,
            where: {
                adoptioncaseid: this.id },
            method: 'get'
        }),
        'adoptioniverenewal/list' + '?filter'
    ).subscribe(
        (resp) => {
            console.log(resp);
            this.annualReviewList = resp;
        },
        (error) => {
            console.log(error);
        }
    );
  }
  printFun() {
    window.print();
  }

  formInitialize() {
    const childDOB = (this.child) ? this.child.dob : null;
    let childat18 = null;
    if (childDOB) {
      this.agreementMinDate = new Date(childDOB);
      childat18 = new Date(childDOB);
      childat18.setFullYear(childat18.getFullYear() + 18);
    }
    const enddate = new Date();
    enddate.setDate(enddate.getDate() + 364);
    this.subsidyAgreementRateForm = this._formBuilder.group({
      startdate: [new Date()],
      enddate: [enddate],
      provider_id: [null],
      paymentamout: [null],
      isapproval: [null],
      approvaldate: [null],
      isspeacialneeds: [null],
      parent1actorid: [null],
      parent2actorid: [null],
      childrelationship: [null],
      notes: [null],
      specialneedremarks: [null],
      specialneedtypekey: [null],

      transactiondate: [null],
      adoptionagreementrateid: [null],
      adoptionagreementid: [null],
      adoptionplanningid: [null],
      activeflag: [null],

      isssaapproved: [null],
      ssaapproveddate: [null]
   });

    this.subsidyAgreementForm = this._formBuilder.group({
      adoptionagreementid: [null],
      adoptionplanningid: [null],
      servicecaseid: [null],
      isofferedsubsidy: [null],
      offeraccepteddate: [null],
      startdate: [null],
      enddate: [childat18],
      finalizationdate: [(this.finalizationdate) ? this.finalizationdate : null],
      isunderappeal: [null],
      singleparentadoptioncheck: [null],
      parent1signdate: [null],
      parent2signdate: [null],
      ldssdate: [null],
      issubsidypaid: [null],
      adoptionagreementrate: [null],
      ma: [null],
      ismedassist: [null],
      parent1providername: [null, Validators.required],
      parent2providername: [null],
      parent1providerid: [null],
      parent2providerid: [null],
      agreementcomments: [null],
      adoptiveparent1signature: [null],
      adoptiveparent2signature: [null],
      ldssdirectorsignature: [null],
      childplacedby: [null],
      childplacedfrom: [null],
    });
    this.subsidyAgreementForm.patchValue({
      startdate: new Date()
    });
    this.onchangeAgreementstartdate();
    this.providerSearchForm = this._formBuilder.group({
      childcharacteristics: [null],
      bundledplacementservices: [null],
      otherLocalDeptmntTypeId: [null],
      placementstructures: [null],
      zipcode: null,
      isLocalDpt: [true],
      firstname: null,
      middlename: null,
      lastname: null,
      isgender: [false],
      isAge: [false],
      providerid: null,
      agemin: null,
      agemax: null,
      gender: null
    });


  }

 private getAge(dateValue) {
    if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
      const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
      return moment().diff(rCDob, 'years');
    } else {
      return 0;
    }
  }

  resetAgreementForm() {
    this.subsidyAgreementForm.enable();
    this.subsidyAgreementForm.reset();
    this.parent1providerid = null;
    this.parent1providername = null;
    this.parent2providername = null;
    this.parent2providerid = null;
  }

  ngOnDestroy() {
    this._speechRecognitionService.destroySpeechObject();
  }

  activateSpeechToText(): void {
      this.recognizing = true;
      this.speechRecogninitionOn = !this.speechRecogninitionOn;
      if (this.speechRecogninitionOn) {
          this._speechRecognitionService.record().subscribe(
              // listener
              value => {
                this.speechData = value;

                      this.subsidyAgreementForm.patchValue({ description: this.speechData });

              },
              // errror
              err => {
                  // console.log(err);
                  this.recognizing = false;
                  if (err.error === 'no-speech') {
                      this.notification = `No speech has been detected. Please try again.`;
                      this._alertService.warn(this.notification);
                      this.activateSpeechToText();
                  } else if (err.error === 'not-allowed') {
                      this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                      this._alertService.warn(this.notification);
                      // this.activateSpeechToText();
                  } else if (err.error === 'not-microphone') {
                      this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
                      this._alertService.warn(this.notification);
                      // this.activateSpeechToText();
                  }
              },
              // completion
              () => {
                  this.speechRecogninitionOn = true;
                  // console.log('--complete--');
                  this.activateSpeechToText();
              }
          );
      } else {
          this.recognizing = false;
          this.deActivateSpeechRecognition();
      }
  }

  deActivateSpeechRecognition() {
      this.speechRecogninitionOn = false;
      this._speechRecognitionService.destroySpeechObject();
  }

  uploadFile(file: File | FileError): void {
    if (!(file instanceof Array)) {
        return;
    }
    file.map((item, index) => {
        const fileExt = item.name
            .toLowerCase()
            .split('.')
            .pop();
        if (
            fileExt === 'mp3' ||
            fileExt === 'ogg' ||
            fileExt === 'wav' ||
            fileExt === 'acc' ||
            fileExt === 'flac' ||
            fileExt === 'aiff' ||
            fileExt === 'mp4' ||
            fileExt === 'mov' ||
            fileExt === 'avi' ||
            fileExt === '3gp' ||
            fileExt === 'wmv' ||
            fileExt === 'mpeg-4' ||
            fileExt === 'pdf' ||
            fileExt === 'txt' ||
            fileExt === 'docx' ||
            fileExt === 'doc' ||
            fileExt === 'xls' ||
            fileExt === 'xlsx' ||
            fileExt === 'jpeg' ||
            fileExt === 'jpg' ||
            fileExt === 'png' ||
            fileExt === 'ppt' ||
            fileExt === 'pptx' ||
            fileExt === 'gif'
        ) {
            this.uploadedFile.push(item);
            const uindex = this.uploadedFile.length - 1;
            if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
                this.uploadedFile[uindex].percentage = 1;
            }

            this.uploadAttachment(uindex);
            const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
            const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
            if (audio_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Audio';
            } else if (video_ext.indexOf(fileExt) >= 0) {
                this.uploadedFile[uindex].attachmenttypekey = 'Video';
            } else {
                this.uploadedFile[uindex].attachmenttypekey = 'Document';
            }
        } else {
            // tslint:disable-next-line:quotemark
            this._alertService.error(fileExt + " format can't be uploaded");
            return;
        }
    });
}
uploadAttachment(index) {
    // console.log('check');
    const workEnv = config.workEnvironment;
    let uploadUrl = '';
    if (workEnv === 'state') {
        uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
            + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber + '&' + 'docsInfo='; // Need to discuss about the docsInfo
        // console.log('state', uploadUrl);
    } else {
        uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
            '&' + 'srno=' + this.daNumber;
        // console.log('local', uploadUrl);
    }

    this._uploadService
        .upload({
            url: uploadUrl,
            headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
            filesKey: ['file'],
            files: this.uploadedFile[index],
            process: true,
        })
        .subscribe(
            (response) => {
                if (response.status) {
                    this.uploadedFile[index].percentage = response.percent;
                }
                if (response.status === 1 && response.data) {
                    const doucumentInfo = response.data;
                    doucumentInfo.documentdate = doucumentInfo.date;
                    doucumentInfo.title = doucumentInfo.originalfilename;
                    doucumentInfo.objecttypekey = 'AdoptionSubsidy';
                    doucumentInfo.rootobjecttypekey = 'AdoptionSubsidy';
                    doucumentInfo.activeflag = 1;
                    doucumentInfo.servicerequestid = null;
                    this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                    // console.log(index, this.uploadedFile[index]);
                }

            }, (err) => {
                // console.log(err);
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                this.uploadedFile.splice(index, 1);
            }
        );
}
deleteAttachment() {
    this.uploadedFile.splice(this.deleteAttachmentIndex, 1);
    (<any>$('#delete-attachment-popup')).modal('hide');
}
confirmDeleteAttachment(index: number) {
  (<any>$('#delete-attachment-popup')).modal('show');
  this.deleteAttachmentIndex = index;
}


  saveAgreement() {
    if (this.isActivePlacement) {
      (<any>$('#finalization-validation-popup')).modal('show');
      return;
    }
    // console.log(this.store);
    if (!this.subsidyAgreementForm.valid) {
      this._alert.warn('Please enter all required field');
      return;
    }
    const agreementInput = Object.assign(this.subsidyAgreementForm.value);
    // const agreementRateInput = null;
    const agreementRateInput = (this.agreementRate && this.agreementRate.length) ? this.agreementRate[0] : null;
    agreementInput.intakeserviceid = null;
    agreementInput.servicecaseid = this.id ? this.id : null;
    agreementInput.adoptionplanningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
    agreementInput.permanencyplanid = this.permanencyplanid ? this.permanencyplanid : null;
    agreementInput.adoptionagreementrate = this.agreementRate ? this.agreementRate : null;
    agreementInput.provider_id = (agreementInput && agreementInput.parent1providerid) ? agreementInput.parent1providerid : null;
    agreementInput.singleparentadoptioncheck = (agreementInput.parent2signdate === null || agreementInput.parent2signdate === '') ? 1 : 0 ;
    agreementInput.attachment = this.uploadedFile;
    this._commonHttp
      .create(
        agreementInput,
        'adoptionagreement/add'
      )
      .subscribe(
        res => {
          this._alert.success('Subsidy agreement saved successfully');
          this.getAgreementListing(true);
          this.resetAgreementForm();
        },
        err => {
          this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
  }

  addAgreementRate() {
    if (this.isAdoptionCase) {
      this.addAdoptionCaseAgreementRate();
      return;
    }
    let agreementInput;
    agreementInput = Object.assign(this.subsidyAgreementRateForm.getRawValue());


    const isRateRangeValid = this.existingRateRangeValidation(agreementInput.startdate, agreementInput.enddate, 0);
    if (!isRateRangeValid) {
      this._alert.warn('Subsidy Rate already exist between this date range');
      return;
    }
    if (this.agreement && this.agreement.adoptionagreementid) {
      this.saveAgreementRate(true, null);
    } else {
      const agreementRate = Object.assign(this.subsidyAgreementRateForm.getRawValue());
      agreementRate.provider_id =  this.parent1providerid ? this.parent1providerid : null;
      agreementRate.typedescription = 'Review';
      this.resetRateForm();
      if (!this.agreementRate) {
        this.agreementRate = [];
      }
      this.agreementRate.push(agreementRate);
      this.disableaddrate = true;
    }
    (<any>$('#createAdoption')).modal('hide');
  }

  saveAgreementRate(approvalFlag, data) {
    if (this.isAdoptionCase) {
      this.saveAdoptionCaseAgreementRate(approvalFlag, data);
      return;
    }
    if (this.isActivePlacement) {
      (<any>$('#finalization-validation-popup')).modal('show');
      return;
    }
     approvalFlag = this.isAdoptionCase ? true : approvalFlag;
    let planningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
    if (!planningid) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
    if (!planningid) {
      planningid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionplanningid : null;
    }
    if (!planningid) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    if (!this.isSupervisor && this.isAdoptionCase) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    // console.log(this.id);
    let agreementInput;
    if (approvalFlag) {
      agreementInput = Object.assign(this.subsidyAgreementRateForm.getRawValue());
    } else {
      agreementInput = data;
    }


    agreementInput.intakeserviceid = null;
    agreementInput.servicecaseid = this.id ? this.id : null;
    // if(this.isAdoptionCase) {
    //   agreementInput.servicecaseid= this.AdoptionServiceCaseId;
    // }
    agreementInput.approvalflag = approvalFlag;
    agreementInput.adoptionplanningid = planningid ? planningid : null;
    agreementInput.adoptioncaseid = this.id;
    agreementInput.approvalflag = (this.agreement && this.agreement.adoptionagreementid) ? true : false ;
    agreementInput.permanencyplanid = this.permanencyplanid ? this.permanencyplanid : null;
    agreementInput.adoptionagreementid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionagreementid : null;
    agreementInput.provider_id =  this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].parent1providerid : null;
    agreementInput.activeflag = 1;
    // agreementInput.parent1actorid = this.parent1providerid ? this.parent1providerid : null;
    // agreementInput.parent2actorid = this.parent2providerid ? this.parent2providerid : null;

    this._commonHttp
      .create(
        agreementInput,
        'adoptionagreementrate/add'
      )
      .subscribe(
        res => {
          this._alert.success('Subsidy agreement saved successfully');
          // this.approvalStatus = 'Review';
          this.getAgreementListing(false);
          this.resetRateForm();
        },
        err => {
          this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
  }

  addAdoptionCaseAgreementRate() {
    let agreementInput;
    agreementInput = Object.assign(this.subsidyAgreementRateForm.getRawValue());


    const isRateRangeValid = this.existingRateRangeValidation(agreementInput.startdate, agreementInput.enddate, 0);
    if (!isRateRangeValid) {
      this._alert.warn('Subsidy Rate already exist between this date range');
      return;
    }
    if (this.agreement && this.agreement.adoptionagreementid) {
      this.saveAdoptionCaseAgreementRate(true, null);
    } else {
      const agreementRate = Object.assign(this.subsidyAgreementRateForm.getRawValue());
      agreementRate.provider_id =  this.parent1providerid ? this.parent1providerid : null;
      agreementRate.typedescription = 'Review';
      this.resetRateForm();
      if (!this.agreementRate) {
        this.agreementRate = [];
      }
      this.agreementRate.push(agreementRate);
      this.disableaddrate = true;
    }
    (<any>$('#createAdoption')).modal('hide');
  }

  saveAdoptionCaseAgreementRate(approvalFlag, data) {
    if (this.isActivePlacement) {
      (<any>$('#finalization-validation-popup')).modal('show');
      return;
    }
     approvalFlag = this.isAdoptionCase ? true : approvalFlag;
    let planningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
    if (!planningid) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
    if (!planningid) {
      planningid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionplanningid : null;
    }
    if (!planningid) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    if (!this.isSupervisor && this.isAdoptionCase) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    // console.log(this.id);
    let agreementInput;
    if (approvalFlag) {
      agreementInput = Object.assign(this.subsidyAgreementRateForm.getRawValue());
    } else {
      agreementInput = data;
    }


    agreementInput.intakeserviceid = null;
    agreementInput.servicecaseid = this.id ? this.id : null;
    // if(this.isAdoptionCase) {
    //   agreementInput.servicecaseid= this.AdoptionServiceCaseId;
    // }
    agreementInput.approvalflag = approvalFlag;
    agreementInput.adoptionplanningid = planningid ? planningid : null;
    agreementInput.adoptioncaseid = this.id;
    agreementInput.approvalflag = (this.agreement && this.agreement.adoptionagreementid) ? true : false ;
    agreementInput.permanencyplanid = this.permanencyplanid ? this.permanencyplanid : null;
    agreementInput.adoptionagreementid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionagreementid : null;
    agreementInput.provider_id =  this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].parent1providerid : null;
    agreementInput.activeflag = 1;
    // agreementInput.parent1actorid = this.parent1providerid ? this.parent1providerid : null;
    // agreementInput.parent2actorid = this.parent2providerid ? this.parent2providerid : null;

    this._commonHttp
      .create(
        agreementInput,
        'adoptioncaseagreementrate/add'
      )
      .subscribe(
        res => {
          this._alert.success('Subsidy agreement saved successfully');
          // this.approvalStatus = 'Review';
          this.getAgreementListing(false);
          this.resetRateForm();
        },
        err => {
          this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
  }

  downloadFile(s3bucketpathname) {
    const workEnv = config.workEnvironment;
    let downldSrcURL;
    if (workEnv === 'state') {
        downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
    } else {
        // 4200
        downldSrcURL = s3bucketpathname;
    }
    // console.log('this.downloadSrc', downldSrcURL);
    window.open(downldSrcURL, '_blank');
}

  updateAgreementRate() {
    if (this.isAdoptionCase) {
      this.updateAdoptionCaseAgreementRate();
      return;
    }
    if (this.isActivePlacement) {
      (<any>$('#finalization-validation-popup')).modal('show');
      return;
    }
    // const approvalFlag = this.isAdoptionCase ? true : approvalFlag;
    let planningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
    if (!planningid) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
    if (!planningid) {
      planningid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionplanningid : null;
    }
    if (!planningid) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    if (!this.isSupervisor && this.isAdoptionCase) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    const agreementrate = this.subsidyAgreementRateForm.getRawValue();

    if (agreementrate.adoptionagreementid) {
      agreementrate.adoptionagreementrateid = agreementrate.adoptionagreementrateid ? agreementrate.adoptionagreementrateid : null;
      agreementrate.adoptionplanningid = planningid ? planningid : null;
      agreementrate.adoptioncaseid = this.id;
      agreementrate.activeflag = 1;
      agreementrate.intakeserviceid = null;
      // agreementrate.adoptionagreementrateid  = null;
      agreementrate.servicecaseid = this.id ? this.id : null;
      agreementrate.approvalflag = (this.agreement && this.agreement.adoptionagreementid) ? true : false;
      // if(this.isAdoptionCase) {
      //   agreementrate.servicecaseid= this.AdoptionServiceCaseId;
      // }
      // agreementrate.adoptionplanningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
      agreementrate.permanencyplanid = this.permanencyplanid ? this.permanencyplanid : null;
      // agreementrate.adoptionagreementrate = this.agreementRate ? this.agreementRate : null;
      // agreementrate.provider_id = (agreementrate && agreementrate.parent1providerid) ? agreementrate.parent1providerid : null;
      this._commonHttp
        .create(
          agreementrate,
          'adoptionagreementrate/add'
        )
        // this._commonHttp
        //   .create(
        //     agreementrate,
        //     'adoptionagreementrate/rateupdate'
        //   )
        .subscribe(
          res => {
            this._alert.success('Subsidy agreement updated successfully');
            // this.approvalStatus = 'Review';
            this.getAgreementListing(false);
            this.resetRateForm();
            (<any>$('#createAdoption')).modal('hide');
          },
          err => {
            this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          }
        );
    } else {
      this.agreementRate = [];
      this.agreementRate.push(agreementrate);

    }
  }

  updateAdoptionCaseAgreementRate() {

    if (this.isActivePlacement) {
      (<any>$('#finalization-validation-popup')).modal('show');
      return;
    }
    // const approvalFlag = this.isAdoptionCase ? true : approvalFlag;
    let planningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
    if (!planningid) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
    if (!planningid) {
      planningid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionplanningid : null;
    }
    if (!planningid) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    if (!this.isSupervisor && this.isAdoptionCase) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    const agreementrate = this.subsidyAgreementRateForm.getRawValue();

    agreementrate.adoptionagreementrateid = agreementrate.adoptionagreementrateid ? agreementrate.adoptionagreementrateid : null;
    agreementrate.adoptionplanningid =  planningid ? planningid : null;
    agreementrate.adoptioncaseid = this.id;
    agreementrate.activeflag = 1;
    agreementrate.intakeserviceid = null;
    // agreementrate.adoptionagreementrateid  = null;
    agreementrate.servicecaseid = this.id ? this.id : null;
    agreementrate.approvalflag = (this.agreement && this.agreement.adoptionagreementid) ? true : false ;
    agreementrate.permanencyplanid = this.permanencyplanid ? this.permanencyplanid : null;
    this._commonHttp
      .create(
        agreementrate,
        'adoptioncaseagreementrate/add'
      )
      .subscribe(
        res => {
          this._alert.success('Subsidy agreement updated successfully');
          this.getAgreementListing(false);
          this.resetRateForm();
          (<any>$('#createAdoption')).modal('hide');
        },
        err => {
          this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
  }

  getInvolvedPerson() {

    let reqObj = {};
      reqObj = {
        objectid: this.id,
        objecttypekey: 'servicecase'
      };
    this.involvedPersons$ = this._commonHttp
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: reqObj
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
      )
      .share()
      .pluck('data');
    this.involvedPersons$.subscribe((items) => {
      if (items) {

        const person = items.filter((item) => {
          if (!item.rolename || item.rolename === '') {
            if (item.roles && item.roles.length && item.roles[0].intakeservicerequestpersontypekey) {
              item.rolename = item.roles[0].intakeservicerequestpersontypekey;
            }
          }
          if (item.rolename === 'ADOPTPARNT') {
            if (!item.intakeservicerequestactorid) {
              if (item.roles && item.roles.length && item.roles[0].intakeservicerequestactorid) {
                item.intakeservicerequestactorid = item.roles[0].intakeservicerequestactorid;
              }
            }
            // this.petitionDetailsForm.patchValue({'petitionfocusname': this.involvedYouth});
            return item;
          }
        });

        this.involvedPersons = person.map((res) => res);
      }
    });
  }

  getAgreementListing(isSave) {

    let planningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
    if (this.isSupervisor && this.isAdoptionCase) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
    if (!planningid) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
      if (!planningid) {
      planningid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionplanningid : null;
    }
    if (!this.isSupervisor && this.isAdoptionCase) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    let url = '';
    let obj;

    if (this.isAdoptionCase) {
      url = 'adoptionagreement/adoptioncaseagreementlist?filter';
      obj = { adoptioncaseid: this.id };
    } else {
      url = 'adoptionagreement/list?filter';
      obj = { adoptionplanningid: planningid };
    }

    this._commonHttp
      .getSingle(
        new PaginationRequest({
          where: obj,
          method: 'get',
          page: 1,
          limit: 10
        }),
        url
      )
      .subscribe(res => {
        if (res && res.length && Array.isArray(res)) {
          const obcj = res[0];
          const agreementList = this.isAdoptionCase ? obcj.getadoptioncaseagreementlist : obcj.getadoptionagreementlist;
          if (agreementList && agreementList.length) {
            const length = agreementList.length - 1;
            this.agreement = agreementList[length];
            this.permanencyplanid = agreementList[length].permanencyplanid;
            this.uploadedFile = agreementList[length].attachments ? agreementList[length].attachments : [];
            this.subsidyAgreementForm.patchValue(this.agreement);
            this.parent1providerid = this.subsidyAgreementForm.getRawValue().parent1providerid;
            this.parent2providerid = this.subsidyAgreementForm.getRawValue().parent2providerid;
            this.parent1providername = this.subsidyAgreementForm.getRawValue().parent1providername;
            this.parent2providername = this.subsidyAgreementForm.getRawValue().parent2providername;
            this._store.setData('adoptionAgreement', this.agreement);
            if (isSave && this.agreementRate && this.agreementRate.length) {
              this.agreementRate.forEach(rate => {
                // this.saveAgreementRate(false, rate);
              });
            }
            if (this.agreement.agreementrate) {
              this.agreementRate = this.agreement.agreementrate;
              this.approvalStatus = this.agreement.routingstatus;
              this.disableaddrate = this.agreementRate.some(item => ['Review', 'Rejected', null].includes(item.typedescription));
              this.provider_id = this.agreement.agreementrate.provider_id;
            }
          }
        }
      });

  }
  patchForm(data, mode, index) {
    this.isView = mode;
    this.isEdit = !mode;
    this.ratemaxDate = null;
    if (this.isEdit) {
      this.selectedIndex = index;
    }
    if (!data.typedescription || data.typedescription === 'Review') {
      this.adoptionagreementrateid = data.adoptionagreementrateid;
    } else {
      this.adoptionagreementrateid = null;
    }
    data.startdate = this._commonDDService.getValidDate(data.startdate);
    // data.enddate = this._commonDDService.getValidDate(data.enddate);
    this.subsidyAgreementRateForm.patchValue(data);
    if (this.isEdit) {
      if (!data.typedescription || ['Rejected'].includes(data.typedescription)) {
        this.subsidyAgreementRateForm.enable();
        this.subsidyAgreementRateForm.controls.startdate.disable();
      } else {
        this.subsidyAgreementRateForm.disable();
      }
    } else {
      this.subsidyAgreementRateForm.disable();
    }
    if (!mode) {
      this.subsidyAgreementRateForm.controls.enddate.enable();
      const rateEndDate = this.subsidyAgreementRateForm.getRawValue().enddate;
      if (rateEndDate) {
      this.ratemaxDate = new Date(rateEndDate);
      }
      // this.subsidyAgreementRateForm.get('enddate').updateValueAndValidity();
      // this.subsidyAgreementRateForm.controls.paymentamout.enable();
      this.subsidyAgreementRateForm.controls.notes.enable();
      // this.subsidyAgreementRateForm.controls.startdate.enable();
      this.ratestartdatechange();
    }
    setTimeout(() => {
      (<any>$('#createAdoption')).modal('show');
    }, 400);
  }

  getDropDownList() {

    this._commonHttp
      .getArrayList(
        {
          method: 'get',
          where: {
            referencetypeid: 110,
            teamtypekey: 'CW'
          }
        },
        'referencetype/gettypes' + '?filter'
      )
      .subscribe((item) => {
        this.specialNeedsDropDown = item;
      });

      this._commonHttp
      .getArrayList(
        {
          method: 'get',
          where: {
            referencetypeid: 900,
            teamtypekey: 'CW'
          }
        },
        'referencetype/gettypes' + '?filter'
      )
      .subscribe((item) => {
        this.childPlacedfromDropDown = item;
      });

      this._commonHttp
      .getArrayList(
        {
          method: 'get',
          where: {
            referencetypeid: 901,
            teamtypekey: 'CW'
          }
        },
        'referencetype/gettypes' + '?filter'
      )
      .subscribe((item) => {
        this.childPlacedbyDropDown = item;
      });


    this._commonHttp
      .getArrayList(
        {
          method: 'get',
          where: {
            referencetypeid: 109,
            teamtypekey: 'cw'
          }
        },
        'referencetype/gettypes' + '?filter'
      )
      .subscribe((item) => {

        this.relationshipDropDown = item;
      });
  }

  private effortListing() {
    this._commonHttp
      .getSingle(
        new PaginationRequest({
          where: {
            permanencyplanid: this.permanencyplanid
            // intakeserviceid: this.id,
            //  intakeservicerequestactorid: this.childActorId ? this.childActorId : null
          },
          method: 'get',
          page: 1,
          limit: 10
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEffortList + '?filter'
      )
      .subscribe(res => {
        if (res && res.length && res[0].getadoptionplanning) {
          this.finalizationdate = res[0].getadoptionplanning.adoptiondate;
          this.subsidyAgreementForm.patchValue({
            finalizationdate: this.finalizationdate
          });
        }
      });
  }

  resetRateForm() {
    this.ratemaxDate = null;
    this.subsidyAgreementRateForm.enable();
    this.subsidyAgreementRateForm.reset();
    this.decideSSAApproval();

    const currDate = new Date();

    this.subsidyAgreementRateForm.patchValue({ startdate: currDate });
    this.ratestartdatechange();
    this.isView = false;
    this.isEdit = false;
    this.selectedIndex = -1;
  }

  existingRateRangeValidation(startdate, enddate, rateid) {
    let isValid = true;

    if (this.agreementRate && this.agreementRate.length) {

      this.agreementRate.forEach(rate => {
          if (
           ( ( new Date(startdate).getTime() > new Date(rate.startdate).getTime() && new Date(startdate).getTime() < new Date(rate.enddate).getTime() ) ||
            ( new Date(enddate).getTime() > new Date(rate.startdate).getTime() && new Date(enddate).getTime() < new Date(rate.enddate).getTime())
          ) && rateid !== rate.agreementrateid && rate.typedescription !== 'Rejected'
             ) {
            isValid = false;
          }
      });

    }

    return isValid;
  }

  addRate() {
    if (this.agreementRate && this.agreementRate.length) {
      this.subsidyAgreementRateForm.reset();
      this.isView = false;
      this.isEdit = false;
      this.selectedIndex = -1;



    const lastrate = this.agreementRate[this.agreementRate.length - 1];
    const enddate = new Date(lastrate.enddate);
    // enddate.setHours(47, 59, 59, 59);
    // enddate.setUTCHours(0);
    enddate.setDate(enddate.getDate() + 1);
    this.subsidyAgreementRateForm.patchValue({ startdate: enddate });
    let upcomingReviewDate, upcomingReview, completedReview, completedReviewDate;
    if (this.annualReviewList && this.annualReviewList.length) {
      upcomingReview = this.annualReviewList.filter( review => review.status === 'Review' );
      completedReview = this.annualReviewList.filter( review => review.status === 'Approved' );
      upcomingReviewDate = upcomingReview && upcomingReview.length ? upcomingReview[0].assessmentdate : null;
      completedReviewDate = completedReview && completedReview.length ? completedReview[completedReview.length - 1].assessmentdate : null;
    }
      if (new Date(this.subsidyAgreementRateForm.getRawValue().startdate) < new Date(completedReviewDate)) {
      const sdate = new Date(completedReviewDate);
      // enddate.setHours(47, 59, 59, 59);
      // enddate.setUTCHours(0);
      sdate.setDate(sdate.getDate() + 1);
      if (this.isAdoptionCase) {
      this.subsidyAgreementRateForm.patchValue({
        startdate: sdate,
      }); }
    }
    this.ratestartdatechange();


    } else {
    const agreementDate = this.subsidyAgreementForm.getRawValue().startdate;
    if (agreementDate) {
      this.rateMinDate = agreementDate;
      this.subsidyAgreementRateForm.patchValue({ startdate: agreementDate });
      this.ratestartdatechange();
     }
    }

    this.subsidyAgreementRateForm.get('startdate').disable();
    setTimeout(() => {
      (<any>$('#createAdoption')).modal('show'); } , 300 );
    
  }

  routingUpdate(status, eventCode) {
    const comment = 'Adoption agreement Approved';
    let planningid = null;
    if (eventCode === 'ASAR') {
      planningid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionagreementid : null;
      if (!planningid) {
        planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_AGREEMENT_ID];
      }
    }
    if (eventCode === 'AARR') {
      planningid = this.adoptionagreementrateid;
    }


    const submitStatus = Object.assign({
      objectid: planningid,
      eventcode: eventCode,
      status: status,
      comments: comment,
      notifymsg: comment,
      routeddescription: comment,
      servicecaseid: this.id
    });
    // console.log(submitStatus);
    this._commonHttp.create(submitStatus, 'routing/routingupdate').subscribe(
      res => {
        this._alert.success('Adoption agreement ' + status + ' successfully!');
        this.approvalStatus = status;
        if (eventCode === 'AARR') {
          this.resetRateForm();
          (<any>$('#createAdoption')).modal('hide');

        }
        this.getAgreementListing(false);
      },
      err => { }
    );
  }

  onSearchParents() {
    const child = this.store['placed_child'];
    if (child) {
      const childage = this.getAge(child.dob);
      const min = 0;
      const max = childage;
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender
      });
    }
    (<any>$('#searchProvider')).modal('show');
  }

  getChildCharacteristics() {
    this._commonHttp.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '43'
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.childCharacteristicsUrl).subscribe(result => {
      this.childCharacteristics = result;
    });
  }

  getOtherLocalDeptmntType() {
    this._commonHttp.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '104'
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.otherLocalDeptmntTypeUrl).subscribe(result => {
      this.otherLocalDeptmntType = result;
    });
  }

  getBundledPlcmntServicesType() {
    this._commonHttp.getArrayList(new PaginationRequest({
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.bundledPlcmntServicesTypeUrl).subscribe(result => {
      this.bundledPlcmntServicesType = result;
    });
  }

  fcPageChanged(pageEvent) {
    this.paginationInfo.pageNumber = pageEvent.page;
    this.getFcProviderSearch();
  }
  resetproviderSearchForm() {
    this.providerSearchForm.reset();
    this.currProcess = 'search';
  }

  onSearchParent1() {
    const child = this.store['placed_child'];
    if (child) {
      const childage = this.getAge(child.dob);
      const min = 0;
      const max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender
      });
    }
    // console.log('Search Text', this.adoptiveparent1);
    (<any>$('#searchProvider')).modal('show');
    this.selectedparent = 'parent1';

  }
  onSearchParent2() {
    const child = this.store['placed_child'];
    if (child) {
      const childage = this.getAge(child.dob);
      const min = 0;
      const max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender
      });
    }
    // console.log('Search Text', this.adoptiveparent2);
    (<any>$('#searchProvider')).modal('show');
    this.selectedparent = 'parent2';
  }

  selectProvider() {
    if (this.selectedProvider && this.selectedProvider.provider_id) {
      this._commonHttp.getArrayList(
        {
          where: { providerid: this.selectedProvider.provider_id },
          method: 'get'
        },
        'adoptionagreement/getparentnames?filter'
      ).subscribe(res => {
         if (res[0]) {
           const getadoptiveparents = res[0].getadoptiveparents;
           if (getadoptiveparents.length) {
            const parent1 = getadoptiveparents.find(parent => parent.adoptiveparent1);
            const parent2 = getadoptiveparents.find(parent => parent.adoptiveparent2);
            this.subsidyAgreementForm.patchValue({ 'parent1providername': (parent1) ? parent1.adoptiveparent1 : null });
            this.subsidyAgreementForm.patchValue({ 'parent1providerid': (parent1 && parent1.providerid) ? parent1.providerid : null });
            this.parent1providerid = (parent1 && parent1.providerid) ? parent1.providerid : null;
            this.parent1providername = (parent1) ? parent1.adoptiveparent1 : null;
            this.subsidyAgreementForm.patchValue({ 'parent2providername': parent2 ? parent2.adoptiveparent2 : null });
            this.subsidyAgreementForm.patchValue({ 'parent2providerid': (parent2 && parent2.providerid) ? parent2.providerid : null });
            this.parent2providerid = (parent2 && parent2.providerid) ? parent2.providerid : null;
            this.parent2providername = parent2 ? parent2.adoptiveparent2 : null;
           }
         }
      });
    }

    /*
    if (this.selectedparent === 'parent1') {
      // console.log('Selected Provider for parent 1', this.selectedProvider);
      this.subsidyAgreementForm.patchValue({ 'parent1providername': (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null });
      this.subsidyAgreementForm.patchValue({ 'parent1providerid': (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null });
      this.parent1providerid = (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null;
      this.parent1providername = (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null;
    } else if (this.selectedparent === 'parent2') {
      // console.log('Selected Provider for parent 2', this.selectedProvider);
      this.subsidyAgreementForm.patchValue({ 'parent2providername': (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null });
      this.subsidyAgreementForm.patchValue({ 'parent2providerid': (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null });
      this.parent2providerid = (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null;
      this.parent2providername = (this.selectedProvider && this.selectedProvider.providername) ? this.selectedProvider.providername : null;
    }
    */
    (<any>$('#searchProvider')).modal('hide');
    this.resetproviderSearchForm();

  }

  CheckFormControlValue(formControl) {
    return (formControl && formControl !== '') ? formControl : null;
  }

  getPlacementInfoList(pageNumber, limit) {
    this._commonHttp
      .getPagedArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: limit,
          method: 'get',
          where: { servicecaseid: this._store.getData(CASE_STORE_CONSTANTS.CASE_UID) },
        }),
        'placement/getplacementbyservicecase?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      ).subscribe(res => {
        if (res.data) {
          this.childremoval = res.data;
          const personid = (this.child && this.child.personid) ? this.child.personid : null;
          const reportedChildPlacement = this.childremoval.filter(child => child.personid === personid);
          if (reportedChildPlacement && reportedChildPlacement.length) {
            this.isActivePlacement = this.checkForChildHasActivePlacements(reportedChildPlacement[0].placements);
          }
        }
      });
  }




  checkForChildHasActivePlacements(placements) {
    let hasActivePlacement = false;
    if (placements && placements.length) {
      const providerPlacement = placements.filter(placement => placement.placementtypekey !== LIVING_ARRANGEMENT
        && placement.enddate === null
        && placement.isvoided !== VOID_PLACEMENT
        && placement.responseacceptedkey === RESPONSE_ACCEPTED_YES
        && placement.routingstatus !== RESPONSE_REJECTED);
      if (providerPlacement && providerPlacement.length) {
        hasActivePlacement = true;
      }
    }
    return hasActivePlacement;
  }

  getFcProviderSearch() {
    if (this.providerSearchForm.invalid) {
      this._alertService.error('Please fill required fields');
      return false;
    }

    if (this.child) {
      const childage = this.getAge(this.child.dob);
      const min = 0;
      const max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: this.child.gender
      });
    }
    const formValues = this.providerSearchForm.getRawValue();
    if (formValues.isgender && !formValues.gender) {
      this._alertService.error('Please select gender');
      return false;
    }
    // formValues.agemin = formValues.isAge ? ( this.minAge ? this.minAge : null ) : null;
    // formValues.agemax = formValues.isAge ? ( this.maxAge ? this.maxAge : null ) : null;
    formValues.gender = formValues.isgender ? (formValues.gender ? formValues.gender : null) : null;
    // console.log(formValues);

    Object.keys(this.providerSearchForm.controls).forEach(key => {
      formValues[key] = this.CheckFormControlValue(formValues[key]);
    });
    const body = {};
    Object.assign(body, formValues);
    body['isLocalDpt'] = true;
    body['localdepartmenthomecaregiver'] = true;
    body['childcharacteristics'] = formValues.childcharacteristics ? formValues.childcharacteristics[0] : null;
    body['otherLocalDeptmntTypeId'] = formValues.otherLocalDeptmntTypeId ? formValues.otherLocalDeptmntTypeId[0] : null;
    body['placementstructures'] = formValues.placementstructures ? formValues.placementstructures[0] : null;
    body['bundledplacementservices'] = formValues.bundledplacementservices ? formValues.bundledplacementservices[0] : null;
    body['adoption'] = true;

    // const dob = moment.utc(this.reportedChildDob);
    // const age16 = dob.clone().add(16, 'years');
    // const age21 = dob.clone().add(21, 'years');
    // const isAgeBtwn16And18 = moment.utc().isBetween(age16, age21, null, '[]');
    // if (isAgeBtwn16And18 && formValues.placementStrTypeId === '1') {// Independent Living Residential Program
    //   this._alertService.error('A child between the age of 16 and 21 years cannot be placed in \'Independent Living Residential Program\' Placement Structure.');
    //   return false;
    // }
    // body = {
    //   childcharacteristics: parseInt(formValues.childCharacteristicsid, 10),
    //   placementstructures: parseInt(formValues.placementStrTypeId, 10),
    //   bundledplacementservices: parseInt(formValues.bundledPlcmntServicesTypeId, 10),
    //   providerid: formValues.providerid,
    //   zipcode: parseInt(formValues.zipcode, 10),
    //   localdepartmenthomecaregiver: formValues.isLocalDpt
    // };
    this._commonHttp.getPagedArrayList(new PaginationRequest({
      where: body,
      page: this.paginationInfo.pageNumber,
      limit: 10,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fcProviderSearchUrl).subscribe(result => {
      this.fcProviderSearch = result.data;
      this.fcTotal = result.count;
      (<any>$('#fc_list')).click();
      this.currProcess = 'select';
    });
  }

  backToSearchList() {
    this.currProcess = 'select';
    this.selectedProvider = null;
  }

  backToSearch() {

    const child = this.store['placed_child'];
    if (child) {
      const childage = this.getAge(child.dob);
      const min = 0;
      const max = childage;
      // let gender  = this.genderDropdownItems.filter((gender) => gender.name === this.child.gender);
      // let max = parseInt(childage, 10);
      this.providerSearchForm.patchValue({
        isgender: true,
        isAge: true,
        agemin: min,
        agemax: max,
        gender: child.gender
      });
    }
    this.currProcess = 'search';
    this.selectedProvider = null;
  }

  getRangeArray(n: number): any[] {
    return Array(n);
  }


  listMap(provider) {
    this.zoom = 13;
    this.defaultLat = 39.29044;
    this.defaultLng = -76.61233;
    // this.fcProviderSearch.map((map) => {
    (<any>$('#map-popup')).modal('show');
    this.markersLocation = [];
    // if (map.length > 0) {
    // this.fcProviderSearch.forEach((provider) => {
    //  this._ServiceCasePlacementsService.getGoogleMarker(provider.providerdetails[0].address).subscribe(res => {
    //     console.log(res);
    //   });
    const geocoder = new google.maps.Geocoder();
    if (geocoder) {
      geocoder.geocode({ 'address': provider.providerdetails[0].address }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          this.markersLocation = [];
          const marker = { lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() };
          // console.log(marker);
          this.lat = marker.lat;
          this.lng = marker.lng;
          const mapLocation = {
            lat: +marker.lat,
            lng: +marker.lng,
            draggable: +true,
            providername: provider.providername,
            addressline1: provider.providerdetails[0].address
          };
          this.markersLocation.push(marker);
          this.defaultLat = marker.lat;
          this.defaultLng = marker.lng;
          (<any>$('#map-popup')).modal('show');
          setTimeout(() => {
            this.showMap = true;
          }, 300);

        } else {
          // console.log('Geocoding failed: ' + status);
        }
      });
    }

    // json.map(res => {
    //   console.log(res);
    // });
    //   if ( json.results && json.results.length > 0  && json.results[0].geometry.location  ) {
    //   const res = json.results[0].geometry.location;
    //   const mapLocation = {
    //         lat: +res.lat,
    //         lng: +res.lon,
    //         draggable: +true,
    //         providername: provider.providername,
    //         addressline1: provider.providerdetails[0].address
    //     };
    //     this.markersLocation.push(mapLocation);
    //     this.defaultLat = this.markersLocation[0].lat;
    //     this.defaultLng = this.markersLocation[0].lng;
    //   // });
    // // });
    // (<any>$('#map-popup')).modal('show');
    //   }
    //     }
    // });
  }
  mapClose() {
    this.markersLocation = [];
    this.showMap = false;
    // this.zoom = 11;
    // this.defaultLat = 39.219236;
    // this.defaultLng = -76.662834;
  }


  selectedViewProv(provId) {
    this.selectedViewProvider = provId;
  }

  selectedProv(provId) {
    // this.nextDisabled = false;
    // console.log(this.selectedProvider);
    this.selectedProvider = provId;
    //  this.getPlacementStrType(this.selectedProvider.provider_id );
  }



  getPlacementStrType(providerId) {
    this._commonHttp.getArrayList(new PaginationRequest({
      where: {
        'structure_service_cd': 'P',
        'provider_id': providerId
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.placementStrTypeUrl).subscribe(result => {
      this.placementStrType = result;
    });
  }

  loadGenderDropdownItems() {
    this._commonHttp.getArrayList(
      {
        where: { activeflag: 1 },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.GenderTypeUrl + '?filter'
    ).subscribe((genderList) => {
      this.genderDropdownItems = genderList;
    });
    // text: res.typedescription,
    // value: res.gendertypekey
  }

  closeMap() {
    this.markersLocation = [];
    this.showMap = false;
    (<any>$('#map-popup')).modal('hide');
  }

  ratestartdatechange() {
    let startdate = this.subsidyAgreementRateForm.getRawValue().startdate;
    const agenddate = this.subsidyAgreementForm.getRawValue().enddate;
    const enddate = this.subsidyAgreementRateForm.getRawValue().enddate;
    let upcomingReviewDate, upcomingReview, completedReview, completedReviewDate;
    if (this.annualReviewList && this.annualReviewList.length) {
      upcomingReview = this.annualReviewList.filter(review => review.status === 'Review');
      completedReview = this.annualReviewList.filter(review => review.status === 'Approved');
      upcomingReviewDate = upcomingReview && upcomingReview.length ? upcomingReview[0].assessmentdate : null;
      completedReviewDate = completedReview && completedReview.length ? completedReview[completedReview.length - 1].assessmentdate : null;
    }
    if (!upcomingReviewDate && !completedReviewDate) {
      let agstartdate;
      if (this.agreement && this.agreement.startdate) {
        agstartdate = this.agreement.startdate;
      } else {
        agstartdate = this.subsidyAgreementForm.getRawValue().startdate;
      }
      agstartdate = new Date(agstartdate);
      agstartdate.setDate(agstartdate.getDate() + 364);
      upcomingReviewDate = agstartdate;
    }

    startdate = new Date(startdate);
    startdate.setDate(startdate.getDate() + 364);
    this.subsidyAgreementRateForm.patchValue({
      enddate: enddate ? enddate : startdate && new Date(startdate) < new Date(agenddate) ? startdate : agenddate,
    });
    this.rateenddateChange();
    this.ratemaxDate = startdate && new Date(startdate) < new Date(agenddate) ? startdate : agenddate;
    // this.ratemaxDate = startdate;
    // if( new Date(this.subsidyAgreementRateForm.getRawValue().startdate) < new Date(upcomingReviewDate)) {
    //   this.subsidyAgreementRateForm.patchValue({
    //     enddate: enddate ? enddate : upcomingReviewDate,
    //   });
    // } else {
    //   this.subsidyAgreementRateForm.patchValue({
    //     enddate: null,
    //   });
    //   this._alertService.warn('Please complete previous annual Review before putting new aggreement rate');
    //   setTimeout(() => {
    //   (<any>$('#createAdoption')).modal('hide'); } , 500 );
    // }

  }

  rateenddateChange() {
    const startdate = this.subsidyAgreementRateForm.getRawValue().startdate;
    const enddate = this.subsidyAgreementRateForm.getRawValue().enddate;
    let upcomingReviewDate, upcomingReview, completedReview, completedReviewDate;
    if (this.annualReviewList && this.annualReviewList.length) {
      upcomingReview = this.annualReviewList.filter(review => review.status === 'Review');
      completedReview = this.annualReviewList.filter(review => review.status === 'Approved');
      upcomingReviewDate = upcomingReview && upcomingReview.length ? upcomingReview[0].assessmentdate : null;
      completedReviewDate = completedReview && completedReview.length ? completedReview[completedReview.length - 1].assessmentdate : null;
    }
    if (!upcomingReviewDate && !completedReviewDate) {
      let agstartdate;
      if (this.agreement && this.agreement.startdate) {
        agstartdate = this.agreement.startdate;
      } else {
        agstartdate = this.subsidyAgreementForm.getRawValue().startdate;
      }
      agstartdate = new Date(agstartdate);
      agstartdate.setDate(agstartdate.getDate() + 364);
      upcomingReviewDate = agstartdate;
    } else if (!upcomingReviewDate && completedReviewDate) {
      let cmstartdate;
      cmstartdate = completedReviewDate;
      cmstartdate = new Date(cmstartdate);
      cmstartdate.setDate(cmstartdate.getDate() + 364);
      upcomingReviewDate = cmstartdate;
    }
    if (this.isAdoptionCase) {
    if ( new Date(startdate) < new Date(upcomingReviewDate)) {

      this.subsidyAgreementRateForm.patchValue({
        enddate: enddate && new Date(enddate) < new Date(upcomingReviewDate) ? new Date(enddate) : upcomingReviewDate,
      });
    //  this.ratemaxDate =  enddate && new Date(enddate) < new Date(upcomingReviewDate) ? this.ratemaxDate : upcomingReviewDate;
    } else {
      this.subsidyAgreementRateForm.patchValue({
        enddate: null,
      });
      // this._alertService.warn('Annual Review must be completed to continue adding a new Rate Agreement.');
      setTimeout(() => {
        (<any>$('#createAdoption')).modal('hide'); 
      (<any>$('#annual-review-validation')).modal('show'); } , 500 );
    }}
  }



  private planListing() {
    this._commonHttp
        .getSingle(
            {
                method: 'get',
                page: 1,
                limit: 10,
                where: { objectid: this.id }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PermanencyPlan.PermanencyPlanList + '?filter'
        )
        .subscribe(res => {
            if (res.length) {
                res.map(plan => {
                    // if (plan.status === 'Approved') {
                    //     if (plan.primarypermanency.length) {
                            // this._store.setData('permanencyPlan', plan, true);
                            this.getReportedChild(plan.personid);
                    //     }
                    // }
                });
            }
            if (res.data && res.data.length) {
                res.data.map(plan => {
                    if (plan.status === 'Approved') {
                        if (plan.primarypermanency.length) {
                            this._store.setData('permanencyPlan', plan, true);
                            this.getReportedChild(plan.intakeservicerequestactorid);
                        }
                    }
                });
            }
        });
}
private getReportedChild(childActorId?) {
  let reqObj = {};
  if (this.serviceCase) {
    reqObj = {
      objectid: this.id,
      objecttypekey: 'servicecase'
    };
  } else {
    reqObj = {
      intakeserviceid: this.id
    };
  }

  this._commonHttp
      .getSingle(
          {
              method: 'get',
              where: reqObj
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
      )
      .subscribe(res => {
          if (res && res.data) {
            if (childActorId) {
              const repChild = res.data.filter(child => child.personid === childActorId);
              this.child = res.data.find(item => item.rolename === 'CHILD');
              if (repChild && repChild.length) {
                  this.reportedChild = repChild[0];
                  this.getPlacementInfoList(1, 10);
              }
            } else {
              this.child = res.data.find(item => item.rolename === 'CHILD');
            }
           }
      });
}

  private getBreaklink() {
    this._commonHttp
      .getArrayList({
        method: 'get', where: {
          adoptionplanningid: this._PlacementAdoptionService.getAdoptionPlanning().adoptionplanningid
        }
      }, 'adoptionbreakthelink/getadoptionbreakthelink?filter')
      .subscribe(res => {
        if (res && res.length) {
          res.map((item) => {
            const adoptionBreakLinkList = item && item.getadoptionbreakthelink ? item.getadoptionbreakthelink.map((breaklink) => {
              this.isAdoptionCreated = (breaklink.adoptioncasenumber) ? true : false;
              return breaklink;
            }) : [];
          });
        }
      });
  }
  onChange() {
    const getchecked = this.subsidyAgreementForm.getRawValue().singleparentadoptioncheck;
    if (getchecked === true) {
      this.subsidyAgreementForm.controls['parent2providername'].clearValidators();
      this.subsidyAgreementForm.controls['parent2providername'].updateValueAndValidity();
    } else {
      this.subsidyAgreementForm.controls['parent2providername'].setValidators([Validators.required]);
      this.subsidyAgreementForm.controls['parent2providername'].updateValueAndValidity();
    }
  }
  medicalAssistanceOnly() {
    this.medicalAssistanceOnlyGetChecked = this.subsidyAgreementForm.getRawValue().ismedassist;
    if (this.medicalAssistanceOnlyGetChecked === true) {
      this.paymentAmountPattern = '^[1-9][0-9]*([.][0-9]{2}|)$';

    } else {

      this.paymentAmountPattern = '^[0-9][0-9]*([.][0-9]{2}|)$';
    }
  }
  decideSSAApproval() {
    if (this.isSupervisor) {
      this.subsidyAgreementRateForm.controls.isapproval.enable();
      // this.subsidyAgreementForm.controls['isapproval'].enable();
    } else {
      // this.subsidyAgreementForm.controls['isapproval'];
      this.subsidyAgreementRateForm.controls.isapproval.disable();
    }
  }
  childPlacementList() {
    this._commonHttp
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 10,
          method: 'get',
          where: { servicecaseid: this._store.getData(CASE_STORE_CONSTANTS.CASE_UID) },
        }),
        'placement/getplacementbyservicecase?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      ).subscribe(result => {
        if (result && result.data) {
          if (this.serviceCase) {
            this.placmentDetails = result.data.find(item => item.cjamspid === this.store['placed_child'].cjamspid);
            this.providerDetails = (this.placmentDetails && this.placmentDetails.placements && this.placmentDetails.placements.length && this.placmentDetails.placements[0].providerdetails) ?
            this.placmentDetails.placements[0].providerdetails : null;
            if (this.providerDetails) {
            this.getOverPaymentList(this.providerDetails.provider_id, this.store['placed_child'].cjamspid);
            }
          } else {
              if (this.child && this.child.cjamspid) {
                this.placmentDetails = result.data.find(item => item.cjamspid === this.child.cjamspid);
                this.providerDetails = (this.placmentDetails && this.placmentDetails.placements && this.placmentDetails.placements.length && this.placmentDetails.placements[0].providerdetails) ?
                this.placmentDetails.placements[0].providerdetails : null;
                if (this.providerDetails) {
                this.getOverPaymentList(this.providerDetails.provider_id, this.child.cjamspid);
                }
              }
          }
        }
    });
  }

  getOverPaymentList(providerID, clientid) {
    this._commonHttp.getPagedArrayList({
        where: {
          providerid: providerID,
          client_id: clientid
        },
        page: 1,
        limit: null,
        nolimt: true,
        method: 'get'
      }, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.list).subscribe((res: any) => {
        if (res && res.data && res.data.length) {
          this.overpayments = res.data;
        }
      });
  }

  fiscalAudit() {
    if (this.serviceCase) {
      if ( this.store['placed_child'] && this.store['placed_child'].cjamspid) {
        this.reportedChild = this.store['placed_child'];
        this._financeService.getFiscalAudit(this.reportedChild.cjamspid, 1, this.overpayments);
        (<any>$('#fiscal-audit')).modal('show');
      }
    } else {
      if ( this.child && this.child.cjamspid) {
        // this.reportedChild = this.store['placed_child'];
        this._financeService.getFiscalAudit(this.child.cjamspid, 1, this.overpayments);
        (<any>$('#fiscal-audit')).modal('show');
      }
    }

  }
}
