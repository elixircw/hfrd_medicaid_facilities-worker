import { Component, OnInit } from '@angular/core';
import {
    CommonHttpService,
    AlertService,
    DataStoreService,
    AuthService
} from '../../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PlacementAdoptionService } from '../placement-adoption.service';
import { DynamicObject, PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { AppConstants } from '../../../../../../@core/common/constants';

@Component({
    selector: 'adoption-subsidy',
    templateUrl: './adoption-subsidy.component.html',
    styleUrls: ['./adoption-subsidy.component.scss']
})
export class AdoptionSubsidyComponent implements OnInit {
    subsidyAgreementRateForm: FormGroup;
    subsidyAgreementForm: FormGroup;
    id: string;
    daNumber: string;
    store: DynamicObject;
    permanencyplanid: string;
    isAdoptionCase: boolean;
    isSupervisor: boolean;
    constructor(
        private _commonHttp: CommonHttpService,
        private route: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _alert: AlertService,
        private _store: DataStoreService,
        private _router: Router,
        private _PlacementAdoptionService: PlacementAdoptionService,
        private _authService: AuthService,
        private _dataStoreService: DataStoreService
    ) {
        // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        // console.log(this.id);

        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.store = this._store.getCurrentStore();
        const caseType = this._store.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
        this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);

        if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
            this.isAdoptionCase = true;
        }
        if (this.store) {
            this.id = this.store['CASEUID'];
            this.daNumber = this.store['DANUMBER'];
            this.permanencyplanid = (this.store['placement_child']) ? this.store['placement_child'].permanencyplanid : null;
        }
    }

    ngOnInit() {
        this.formInitialize();
        this.getAgreementListing();
        const adoptionPlanningId = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
        if(adoptionPlanningId) {
        this._PlacementAdoptionService.getAdoptionChecklist(adoptionPlanningId).subscribe(data => {
            if(data && data.length) {
                const adoptionData = data[0];
                if(adoptionData.status  && adoptionData.status === 'Approved') {

                } else {
                    (<any>$('#ap-subsidy')).modal('show');
                }
            } else {
                (<any>$('#ap-subsidy')).modal('show');
            }
        });
        }
    }

    navigateTo() {
        const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/planning/aca-form';
        this._router.navigate([redirectUrl]);
    }

    formInitialize() {
        this.subsidyAgreementRateForm = this._formBuilder.group({
            startdate: [null],
            enddate: [null],
            provider_id: [null],
            paymentamout: [null],
            isapproval: [null],
            approvaldate: [null],
            isspeacialneeds: [null],
            parent1actorid: [null],
            parent2actorid: [null],
            childrelationship: [null],
            notes: [null],
            speacialneeds: [null],
            transactiondate: [null],
            adoptionagreementid: [null],
        });

        this.subsidyAgreementForm = this._formBuilder.group({
          adoptionplanningid: [null],
          servicecaseid: [null],
          isofferedsubsidy: [null],
          offeraccepteddate: [null],
          startdate: [null],
          enddate: [null],
          finalizationdate: [null],
          isunderappeal: [null],
          parent1signdate: [null],
          parent2signdate: [null],
          ldssdate: [null],
          issubsidypaid: [null],
          adoptionagreementrate: [null]
      });
    }

    saveAgreement() {
      console.log(this.id);
        const agreementInput = Object.assign(this.subsidyAgreementForm.value);
        const agreementRateInput = Object.assign(this.subsidyAgreementRateForm.value);
        agreementInput.intakeserviceid = null;
        agreementInput.servicecaseid = this.id ? this.id : null;
        agreementInput.adoptionplanningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
        agreementInput.permanencyplanid = this.permanencyplanid  ? this.permanencyplanid : null;
        agreementInput.adoptionagreementrate = agreementRateInput ? agreementRateInput : null;
        this._commonHttp
            .create(
              agreementInput,
               'adoptionagreement/add'
            )
            .subscribe(
                res => {
                    this._alert.success('Subsidy agreement saved successfully');
                    this.getAgreementListing();
                },
                err => {
                    this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }

    saveAgreementRate() {
      console.log(this.id);
        const agreementInput = Object.assign(this.subsidyAgreementRateForm.value);
        agreementInput.intakeserviceid = null;
        agreementInput.servicecaseid = this.id ? this.id : null;
        agreementInput.permanencyplanid = this.permanencyplanid  ? this.permanencyplanid : null;

        this._commonHttp
            .create(
              agreementInput,
               'adoptionagreementrate/add'
            )
            .subscribe(
                res => {
                    this._alert.success('Subsidy agreement saved successfully');
                    this.getAgreementListing();
                },
                err => {
                    this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }

    getAgreementListing() {
        this._commonHttp
            .getSingle(
                new PaginationRequest({
                    where: {
                      adoptionplanningid: this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null
                        // intakeserviceid: this.id,
                        //  intakeservicerequestactorid: this.childActorId ? this.childActorId : null
                    },
                    method: 'get',
                    page : 1,
                    limit: 10
                }),
                'adoptionagreement/list' + '?filter'
            )
            .subscribe(res => {
                if (res && res.length) {
                  console.log(res);
                  this.subsidyAgreementForm.patchValue(res[0]);
                  if (res.agreementrate) {
                  this.subsidyAgreementRateForm.patchValue(res[0].agreementrate); }
                }
            });

    }
}
