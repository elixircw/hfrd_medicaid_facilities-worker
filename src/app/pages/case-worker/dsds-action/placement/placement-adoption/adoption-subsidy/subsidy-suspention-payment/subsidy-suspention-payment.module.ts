import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubsidySuspentionPaymentRoutingModule } from './subsidy-suspention-payment-routing.module';
import { SubsidySuspentionPaymentComponent } from './subsidy-suspention-payment.component';
import { MatFormFieldModule, MatCheckboxModule, MatDatepickerModule, MatInputModule, MatSelectModule, MatExpansionModule, MatCardModule, MatListModule, MatNativeDateModule, MatRadioModule, MatTableModule, MatTabsModule } from '@angular/material';
import { FormMaterialModule } from '../../../../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    SubsidySuspentionPaymentRoutingModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatExpansionModule,
    MatCardModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTableModule,
    MatTabsModule,
    FormMaterialModule
  ],
  declarations: [SubsidySuspentionPaymentComponent]
})
export class SubsidySuspentionPaymentModule { }
