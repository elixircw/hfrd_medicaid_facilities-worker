import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionSubsidyComponent } from './adoption-subsidy.component';

describe('AdoptionSubsidyComponent', () => {
  let component: AdoptionSubsidyComponent;
  let fixture: ComponentFixture<AdoptionSubsidyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionSubsidyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionSubsidyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
