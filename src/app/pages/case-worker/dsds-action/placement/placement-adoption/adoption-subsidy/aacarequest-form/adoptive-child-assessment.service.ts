import { Injectable } from '@angular/core';
import { PaginationRequest } from '../../../../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../../../../@core/services';
import { AppConstants } from '../../../../../../../@core/common/constants';

@Injectable()
export class AdoptiveChildAssessmentService {

  titleIVeUsers: any = [];
  constructor(private _commonHttpService: CommonHttpService) { }

  getTitleIVeUsers() {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'IVEADOP' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      );
  }

  loadTitleIVeUsers(users) {
    this.titleIVeUsers = users;
  }

  getTitleIVeSpecialist() {
    return this.titleIVeUsers.find(user => user.rolecode === AppConstants.ROLES.TITLE_IVE_SPECIALIST);
  }

  sendAdoptiveChildAssessment(data) {
    // http://localhost:3000/api/titleive/ive/iveAdoptionRouting
    return this._commonHttpService.create(data, 'titleive/ive/iveAdoptionRouting');
  }

}
