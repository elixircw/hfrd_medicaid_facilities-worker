import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsidyAggrementComponent } from './subsidy-aggrement.component';

describe('SubsidyAggrementComponent', () => {
  let component: SubsidyAggrementComponent;
  let fixture: ComponentFixture<SubsidyAggrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubsidyAggrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubsidyAggrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
