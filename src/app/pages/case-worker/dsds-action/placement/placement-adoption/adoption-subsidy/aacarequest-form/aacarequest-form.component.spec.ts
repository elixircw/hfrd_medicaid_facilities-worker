import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AacarequestFormComponent } from './aacarequest-form.component';

describe('AacarequestFormComponent', () => {
  let component: AacarequestFormComponent;
  let fixture: ComponentFixture<AacarequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AacarequestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AacarequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
