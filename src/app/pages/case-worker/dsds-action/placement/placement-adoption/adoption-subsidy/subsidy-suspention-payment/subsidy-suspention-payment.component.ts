import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DynamicObject, PaginationRequest } from '../../../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, DataStoreService, AuthService, SessionStorageService } from '../../../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { PlacementAdoptionService } from '../../placement-adoption.service';
import { GLOBAL_MESSAGES } from '../../../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';
import { AppUser } from '../../../../../../../@core/entities/authDataModel';
import { AppConstants } from '../../../../../../../@core/common/constants';

@Component({
  selector: 'subsidy-suspention-payment',
  templateUrl: './subsidy-suspention-payment.component.html',
  styleUrls: ['./subsidy-suspention-payment.component.scss']
})
export class SubsidySuspentionPaymentComponent implements OnInit {

  subsidySuspensionForm: FormGroup;
  id: string;
  daNumber: string;
  store: DynamicObject;
  permanencyplanid: string;
  reasonTypeDropDown: any[];
  suspensionList: any[];
  agreementData: any;
  isSupervisor: boolean;
  isView: boolean;
  user: AppUser;
  approvalStatus: string;
  isAdoptionCase: boolean;
  suspensionMinDate: any;
  suspensionMaxDate: any;
  constructor(
      private _commonHttp: CommonHttpService,
      private route: ActivatedRoute,
      private _formBuilder: FormBuilder,
      private _alert: AlertService,
      private _store: DataStoreService,
      private _router: Router,
      private _PlacementAdoptionService: PlacementAdoptionService,
      private _authService: AuthService,
      private _session: SessionStorageService,
      private _dataStoreService: DataStoreService
  ) {
    // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    // console.log(this.id);

    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    const caseType = this._store.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
  this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
  if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
      this.isAdoptionCase = true;
  } else {
    this.isAdoptionCase = false;
  }
    this.getStoredValues();
    this.user = this._authService.getCurrentUser();
    if (this.user.role.name === 'apcs') {
      this.isSupervisor = true;
  }
    this._PlacementAdoptionService.storeDataPatched$.subscribe(data => {
      this.getStoredValues();
  });  
  }


  ngOnInit() {
    this.suspensionMinDate = new Date();
    this.formInitialize();
      this.loadReasonDropDown();
      if (this.agreementData && this.agreementData.enddate) {
        this.suspensionMaxDate = new Date(this.agreementData.enddate);
      }
      if (this.agreementData && this.agreementData.startdate) {
        this.suspensionMinDate = new Date(this.agreementData.startdate);
      }
      const adoptionplanningid = ( this.store['adoptionEffort'] && this.store['adoptionEffort'].adoptionplanningid ) ? this.store['adoptionEffort'].adoptionplanningid : null;
      if (adoptionplanningid) {
      const BreaktheLink = this._PlacementAdoptionService.getBreaklink(adoptionplanningid);
      if (BreaktheLink) {
        BreaktheLink.subscribe(res => {
            if (res && res.length) {
                res.map((item) => {
                  if (item && item.getadoptionbreakthelink) {
                   item.getadoptionbreakthelink.map((breaklink) => {
                     if (breaklink.status !== 'Approved' && !this.isAdoptionCase) {
                      (<any>$('#suspensionPaymentAlert')).modal('show');
                     }
                  });
                }
              });
                }
              });
      } else {
        // this.redirectToSubsidy();
        if (!!this.isAdoptionCase) {
        (<any>$('#suspensionPaymentAlert')).modal('show');
        }
      }
      }
  }

  redirectToSubsidy() {
    // (<any>$('#subsidy_agreement')).click();
    const currentUrl = '/pages/case-worker/' + this.daNumber  + '/' + this.id + '/dsds-action/sc-permanency-plan/placement/adoption/adoption-subsidy/agreement';
    this._router.navigate([currentUrl]);
  }

  formInitialize() {
      this.subsidySuspensionForm = this._formBuilder.group({
        adoptionsuspensionid: [null],
        adoptionagreementid: [null],
        adoptionplanningid: [null],
        suspensionreasontypekey: [null],
        suspensionbegindate: [null],
        suspensionenddate: [null],
        suspensionremarks: [null],
        servicecaseid: [null]
      });

  }

  getStoredValues() {
    this.store = this._store.getCurrentStore();

    const placement = this.store['placement_child'];
    this.permanencyplanid = (placement) ? placement.permanencyplanid : null;
    this.agreementData = this.store['adoptionAgreement'] ? this.store['adoptionAgreement']  : null;
    // this.suspensionMaxDate = new Date();
    this.id = this.store['CASEUID'];
    this.daNumber = this.store['DANUMBER'];
    this.getSuspensionListing();
  }

  loadReasonDropDown() {
    this._commonHttp
    .getArrayList(
        {
            where: { referencetypeid: 107, teamtypekey: 'CW' },
            method: 'get'
        },
        'referencetype/gettypes' + '?filter'
    ).subscribe( data => {
        this.reasonTypeDropDown = data;
    });
  }

  saveSuspension() {
    console.log(this.id);

    let planningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;
    if (this.isSupervisor && this.isAdoptionCase) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
    if (!planningid) {
      planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
    }
    if (!planningid) {
      planningid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionplanningid : null;
    }
    if (!this.isSupervisor && this.isAdoptionCase) {
      planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);
    }
    const SuspensionInput = Object.assign(this.subsidySuspensionForm.getRawValue());
    SuspensionInput.intakeserviceid = null;
    SuspensionInput.adoptionagreementid = null;
    SuspensionInput.servicecaseid = this.id ? this.id : null;
    SuspensionInput.adoptionagreementid = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionagreementid : null;
    let url;
    if (this.isAdoptionCase) {
      SuspensionInput.adoptioncaseid = this.id;
      url = 'adoptioncasesuspension/addupdate';
    } else {
      SuspensionInput.adoptionplanningid = planningid;
      url = 'adoptionsuspension/addupdate';
    }
    // SuspensionInput.permanencyplanid = this.permanencyplanid  ? this.permanencyplanid : null;
    this._commonHttp
      .create(
        SuspensionInput,
        url
      )
      .subscribe(
        res => {
          this._alert.success('Subsidy Suspension saved successfully');
          this.getSuspensionListing();
        },
        err => {
          this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
  }

  patchForm(data, mode) {
    this.approvalStatus = data.approvalstatus;
    this.subsidySuspensionForm.patchValue(data);
    this.isView = mode;
    if (mode) {
      this.subsidySuspensionForm.disable();
    } else {
      this.subsidySuspensionForm.disable();
      this.subsidySuspensionForm.controls['suspensionenddate'].enable();
    }
  }

  clearForm() {
    this.isView = false;
    this.subsidySuspensionForm.enable();
    this.subsidySuspensionForm.reset();
  }

  getSuspensionListing() {
    let agreementId = null;
    if (this.store['adoptionAgreement']) {
     agreementId = this.store['adoptionAgreement'] ? this.store['adoptionAgreement'].adoptionagreementid : null;
    }
    if (this.store['ADOPTION_AGREEMENT_ID']) {
      agreementId =  this.store['ADOPTION_AGREEMENT_ID'] ? this.store['ADOPTION_AGREEMENT_ID'] : null;
    }
    let url = '';
    let obj;
    if (this.isAdoptionCase) {
      url = 'adoptioncasesuspensionrevision/getsuspensionhistory?filter';
      obj = {
        adoptioncaseid: this.id
      };
    } else {
      url = 'adoptionsuspensionrevision/getsuspensionhistory?filter';
      obj = {
        adoptionagreementid: agreementId
      };
    }
      this._commonHttp
          .getSingle(
              new PaginationRequest({
                  where: obj,
                  method: 'get',
                  page : 1,
                  limit: 10
              }),
              url
          )
          .subscribe(res => {
              if (res && res.length) {
                console.log(res);
                this.suspensionList = res;

              }
          });

  }

  routingUpdate(status) {
    const suspensionId = this.subsidySuspensionForm.getRawValue().adoptionsuspensionid;
    const comment = 'Adoption Suspension ' + status ;

    const submitStatus = Object.assign({
        objectid: suspensionId,
        eventcode: 'ADSR',
        status: status,
        comments: comment,
        notifymsg: comment,
        routeddescription: comment,
        servicecaseid: this.id
    });
    console.log(submitStatus);
    this._commonHttp.create(submitStatus, 'routing/routingupdate').subscribe(
        res => {
            this._alert.success(comment + '  successfully');
            (<any>$('#suspensionPaymentModal')).modal('hide');
            this.clearForm();
            this.getSuspensionListing();
            this.approvalStatus = status;
        },
        err => { }
    );
}
}
