import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdoptionSubsidyComponent } from './adoption-subsidy.component';
import { AacarequestFormComponent } from './aacarequest-form/aacarequest-form.component';

const routes: Routes = [{
  path: '',
  component: AdoptionSubsidyComponent,
  children: [
    { path: 'adoptive-child-assessment', component: AacarequestFormComponent },
    { path: 'agreement', loadChildren: './subsidy-aggrement/subsidy-aggrement.module#SubsidyAggrementModule' },
    { path: 'suspention-payment', loadChildren: './subsidy-suspention-payment/subsidy-suspention-payment.module#SubsidySuspentionPaymentModule' },
    { path: 'subsidy-download', loadChildren: './subsidy-download/subsidy-download.module#SubsidyDownloadModule' },
    { path: 'adoption-annual-reviews', loadChildren: './adoption-annual-reviews/adoption-annual-reviews.module#AdoptionAnnualReviewsModule' },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdoptionSubsidyRoutingModule { }
