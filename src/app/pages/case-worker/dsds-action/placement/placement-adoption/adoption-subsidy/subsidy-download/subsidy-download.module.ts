import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubsidyDownloadRoutingModule } from './subsidy-download-routing.module';
import { SubsidyDownloadComponent } from './subsidy-download.component';
import { MatFormFieldModule,
  MatCheckboxModule, MatDatepickerModule,
   MatInputModule, MatSelectModule, MatExpansionModule, MatCardModule, MatListModule, MatNativeDateModule, MatRadioModule, MatTableModule, MatTabsModule } from '@angular/material';
import { FormMaterialModule } from '../../../../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { QuillModule } from 'ngx-quill';
import { ControlMessagesModule } from '../../../../../../../shared/modules/control-messages/control-messages.module';
import { NgxfUploaderModule } from 'ngxf-uploader';

@NgModule({
  imports: [
    CommonModule,
    SubsidyDownloadRoutingModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    PaginationModule,
    MatExpansionModule,
    MatCardModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTableModule,
    MatTabsModule,
    QuillModule,
    ControlMessagesModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8CRCgWX0eRvC8XfFnaaMPm-36fb2GN9M'
    })
  ],
  declarations: [SubsidyDownloadComponent]
})
export class SubsidyDownloadModule { }
