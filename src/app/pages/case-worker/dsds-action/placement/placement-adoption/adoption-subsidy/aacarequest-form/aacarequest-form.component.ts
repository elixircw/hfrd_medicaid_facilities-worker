import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { MatDialog } from '@angular/material';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../../../@core/services';
import { Titile4eUrlConfig } from '../../../../../../title4e/_entities/title4e-dashboard-url-config';
import { AdoptiveChildAssessmentService } from './adoptive-child-assessment.service';
import { PaginationRequest } from '../../../../../../../@core/entities/common.entities';
import { CASE_STORE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';

@Component({
    selector: 'aacarequest-form',
    templateUrl: './aacarequest-form.component.html',
    styleUrls: ['./aacarequest-form.component.scss']
})
export class AacarequestFormComponent implements OnInit {

    ChildStatusInfo: FormGroup;
    PlacementAndMedicalInfo: FormGroup;
    SpecialNeedsOfChild: FormGroup;
    TitleIVeStatus: FormGroup;
    signatures: FormGroup;
    constructor(
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        public dialog: MatDialog,
        private fb: FormBuilder,
        private service: AdoptiveChildAssessmentService,
        private _dataStoreService: DataStoreService
    ) { }

    ngOnInit() {
        this.createForm();
        this.loadTitleIVeUsers();
    }

    loadTitleIVeUsers() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'IVEADOP' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            ).subscribe(users => {
                console.log('titleIVe', users);
                this.service.titleIVeUsers = users.data;
            });
    }




    createForm() {
        this.signatures = this.fb.group({
            caseworkersign: null,
            ivesplsign: null,
            ivesupervisorsign: null
        });

        this.ChildStatusInfo = this.fb.group({
            ChildHasBeenInCare60Months: [],
            ChildExpectedAdoptionDate: [],
            SiblingAdoptionApplicable: [],
            SiblingName: [],
            SiblingAdoptionDecreeDate: [],
            SiblingApplicableChildAssessmentDate: [],
            SiblingAdoptiveProviderID: [],
        });
        this.PlacementAndMedicalInfo = this.fb.group({
            Removal_Court_Order_Date: [],
            Child_Removal_Date: [],
            VoluntaryRelinquishment: [],
            ChildMeetsSSIMedicalDisabledEligReqts: [],
            ChildReceivingSSIAtRemoval: [],
            StartDateOfReceivingSSI: [],
            MinorParentName: [],
            MinorParentDateOfBirth: [],
            MinorParentRemovalDate: [],
            MinorParentRemovalCourtOrderDate: [],
            MinorParentCurrentPlacementType: [],
            MinorParentPhysicalAddress: [],
            ChildPhysicalAddress: []
        });
        // tslint:disable-next-line:member-ordering
        this.SpecialNeedsOfChild = this.fb.group({
            ChildCanReturnToHome: [],
            ChildHasPhysicalMentalEmotionalDisability: [],
            ChildHasHighRiskOfDisability: [],
            ChildRaceEthnicity: [],
            ChildHasFosterParentEmotionalTies: [],
            ChildAdoptionUnsuccessfulEffortsToPlace: []
        });
        // tslint:disable-next-line:member-ordering
        this.TitleIVeStatus = this.fb.group({
            ChildPreviouslyAdopted: [],
            ChildPreviousAdoptionIVEStatus: [],
            ChildPreviousAdoptiveParentTPRDate: [],
            ChildPreviousAdoptiveParentDeathDate: [],
            ChildCurrentIVEFosterCareEligibilityStatus: [],
            ChildSSIEligibilityStatusOnOrBeforeDateOfAdoption: [],
        });
        // this.adoptionMissingFields = this.fb.group({
        //     Gender: [],
        //     DateOfBirth: [],
        //     ChildApplicableAssessmentDate: [],
        //     AdoptionFinalizationDate: [],
        //     AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_AdoptiveParents_1: [],
        //     AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_AdoptiveParents_2: [],
        //     AdoptionAssistanceAgreement_FutureNeedsAgreement_Date_Agency: [],
        //     SingleParentAdoptionCheck: [],
        //     IsDocumentedPhysicalAndMentalDisability: [],
        //     AdoptionApplicable: [],
        //     AdoptionAssistance: [],
        //     AdoptionAssistanceStartDate: [],
        //     AdoptionDataIncomplete: [],
        //     AdoptionNonApplicable: [],
        //     AdoptionPetitionFiledDate: [],
        //     AnotherReasonForChildNotReturningHome: [],
        //     ChildDeprivedOfParentalSupport: [],
        //     ChildDisabilityEvaluationDocumentionDate: [],
        //     ChildDisabilityStartDate: [],
        //     ChildDisabilityType: [],
        //     ChildExpectedAdoptiveProviderID: [],
        //     ChildIsSSIEligible: [],
        //     ChildMeetContinueEligibilityCriteria: [],
        //     ChildMeetsSSIMedicalDisabilityReqts: [],
        //     ChildNotReturnHomeExplanation: [],
        //     ChildRemovedFromSpecifiedRelative: [],
        //     ClientID: [],
        //     CountyOfJurisdiction_LDSS: [],
        //     DateAndTimeOfDocumentationForEffortsToPlaceWithoutSubsidy: [],
        //     DateAndTimeOfDocumentationForExceptionGrantedInChildsBestInterests: [],
        //     DateOfLatestPaymentOfMinorParentIVEEligibilityForFosterCare: [],
        //     DateOfTPROfParent_1: [],
        //     DateOfTPROfParent_2: [],
        //     DidEffortsToPlaceChildWereMade: [],
        //     EXTAdoption: [],
        //     HoursPerMonthEmployed: [],
        //     IncomeAndAssetsMetAFDCStandards: [],
        //     IsReasonForExceptionRecorded: [],
        //     MinorParentIVEEligibilityForFosterCareStartDate: [],
        //     MinorParentIVEEligibilityForFosterCareStatus: [],
        //     Name: [],
        //     NameOfEmployer: [],
        //     NameOfpostSecondaryOrVocationalEducation: [],

        //     NameOfPromoteToEmploymentProgram: [],

        //     NameOfSecondaryEducationOrEquivalentProgram: [],
        //     QualifiedAlien: [],

        //     ReasonCode: [],
        //     ReasonForNotGrantingTPRForParent: [],

        //     StartDateOfEmployment: [],

        //     StartDateOfpostSecondaryOrVocationalEducation: [],

        //     StartDateOfPromoteToEmploymentProgram: [],

        //     StartDateOfSecondaryEducationOrEquivalentProgram: [],

        //     Step: [],
        //     TPRGrantedtoBothParent: [],
        //     USCitizen: [],

        // });
    }
    Applicability() {
        const clientId = '1234567';

        const obj = {
            'name': 'AdoptionApplicability',
            '__metadataRoot': {},
            'Objects': [{
                'person': {
                    ChildHasFosterParentEmotionalTies: this.SpecialNeedsOfChild.get('ChildHasFosterParentEmotionalTies').value,
                    ChildAdoptionUnsuccessfulEffortsToPlace: this.SpecialNeedsOfChild.get('ChildAdoptionUnsuccessfulEffortsToPlace').value,
                    ChildRaceEthnicity: this.SpecialNeedsOfChild.get('ChildRaceEthnicity').value,
                    // ChildNotReturnHomeExplanation: this.adoptionMissingFields.get('ChildNotReturnHomeExplanation').value,
                    'parentDetails': [{
                        MinorParentRemovalDate: this.PlacementAndMedicalInfo.get('MinorParentRemovalDate').value,
                        MinorParentName: this.PlacementAndMedicalInfo.get('MinorParentName').value,
                        MinorParentCurrentPlacementType: this.PlacementAndMedicalInfo.get('MinorParentCurrentPlacementType').value,
                        MinorParentDateOfBirth: this.PlacementAndMedicalInfo.get('MinorParentDateOfBirth').value,
                        MinorParentRemovalCourtOrderDate: this.PlacementAndMedicalInfo.get('MinorParentRemovalCourtOrderDate').value,
                        '__metadata': {
                            '#type': 'ParentDetails',
                            '#id': 'ParentDetails_id_1'
                        },
                        MinorParentPhysicalAddress: this.PlacementAndMedicalInfo.get('MinorParentPhysicalAddress').value,
                    }],
                    ChildHasHighRiskOfDisability: this.SpecialNeedsOfChild.get('ChildHasHighRiskOfDisability').value,
                    Child_Removal_Date: this.PlacementAndMedicalInfo.get('Child_Removal_Date').value,
                    ChildCanReturnToHome: this.SpecialNeedsOfChild.get('ChildCanReturnToHome').value,
                    // ChildExpectedAdoptiveProviderID: this.adoptionMissingFields.get('ChildExpectedAdoptiveProviderID').value,
                    // ChildHasEmotionalDisturbance: this.SpecialNeedsOfChild.get('ChildHasEmotionalDisturbance').value,
                    ChildCurrentIVEFosterCareEligibilityStatus: this.TitleIVeStatus.get('ChildCurrentIVEFosterCareEligibilityStatus').value,
                    '__metadata': {
                        '#type': 'Person',
                        '#id': 'Person_id_1'
                    },
                    // DateOfBirth: this.adoptionMissingFields.get('DateOfBirth').value,
                    Removal_Court_Order_Date: this.PlacementAndMedicalInfo.get('Removal_Court_Order_Date').value,
                    ChildPreviousAdoptiveParentTPRDate: this.TitleIVeStatus.get('ChildPreviousAdoptiveParentTPRDate').value,
                    // ChildMeetsSSIMedicalDisabilityReqts: this.adoptionMissingFields.get('ChildMeetsSSIMedicalDisabilityReqts').value,
                    ChildHasBeenInCare60Months: this.ChildStatusInfo.get('ChildHasBeenInCare60Months').value,
                    ChildMeetsSSIMedicalDisabledEligReqts: this.PlacementAndMedicalInfo.get('ChildMeetsSSIMedicalDisabledEligReqts').value,
                    ChildPhysicalAddress: this.PlacementAndMedicalInfo.get('ChildPhysicalAddress').value,
                    VoluntaryRelinquishment: this.PlacementAndMedicalInfo.get('VoluntaryRelinquishment').value,
                    ChildPreviousAdoptiveParentDeathDate: this.TitleIVeStatus.get('ChildPreviousAdoptiveParentDeathDate').value,
                    ChildHasPhysicalMentalEmotionalDisability: this.SpecialNeedsOfChild.get('ChildHasPhysicalMentalEmotionalDisability').value,
                    // ChildIsSSIEligible: this.adoptionMissingFields.get('ChildIsSSIEligible').value,
                    'siblingDetails': [{
                        SiblingAdoptionDecreeDate: this.ChildStatusInfo.get('SiblingAdoptionDecreeDate').value,
                        SiblingApplicableChildAssessmentDate: this.ChildStatusInfo.get('SiblingApplicableChildAssessmentDate').value,
                        SiblingAdoptionApplicable: this.ChildStatusInfo.get('SiblingAdoptionApplicable').value,
                        SiblingAdoptiveProviderID: this.ChildStatusInfo.get('SiblingAdoptiveProviderID').value,
                        SiblingName: this.ChildStatusInfo.get('SiblingName').value,
                        '__metadata': {
                            '#type': 'SiblingDetails',
                            '#id': 'SiblingDetails_id_1'
                        }
                    }],
                    ChildPreviousAdoptionIVEStatus: this.TitleIVeStatus.get('ChildPreviousAdoptionIVEStatus').value,
                    StartDateOfReceivingSSI: this.PlacementAndMedicalInfo.get('StartDateOfReceivingSSI').value,
                    ChildExpectedAdoptionDate: this.ChildStatusInfo.get('ChildExpectedAdoptionDate').value,
                    ChildReceivingSSIAtRemoval: this.PlacementAndMedicalInfo.get('ChildReceivingSSIAtRemoval').value,
                    ChildPreviouslyAdopted: this.TitleIVeStatus.get('ChildPreviouslyAdopted').value,
                },
                '__metadata': {
                    '#type': 'Application',
                    '#id': 'Application_id_1'
                },
                'status': {
                    // AdoptionApplicable: this.adoptionMissingFields.get('AdoptionApplicable').value,
                    // AdoptionNonApplicable: this.adoptionMissingFields.get('AdoptionNonApplicable').value,
                    '__metadata': {
                        '#type': 'Status',
                        '#id': 'Status_id_1'
                    },
                    // AdoptionDataIncomplete: this.adoptionMissingFields.get('AdoptionDataIncomplete').value,
                }
            }]
        };
        this._commonHttpService.create(obj, Titile4eUrlConfig.EndPoint.adoptionApplicability)
            .subscribe(
                (response) => {
                    console.log(obj);
                    // this.RedeterminationStatus =  '';
                    // this.RedeteradoptionAssistance = '';
                    // this.AdoptionApplicableInn = '';
                    // this.adoptionAssistance = '';
                    // this.adoptionnonApplicable = '';
                    // this.AdoptionApplicability = response.Objects[0].status.AdoptionApplicable;
                    // this.AdoptionIncomplete = response.Objects[0].status.AdoptionDataIncomplete;
                    // this.AdoptionNonapplicability = response.Objects[0].status.AdoptionNonApplicable;
                    this._alertService.success('Record Create Succesfully');
                    console.log(response.Objects[0].status.AdoptionApplicable);

                },
                (error) => {
                    this._alertService.error('Unable to create record');
                    return false;
                });
    }

    sendForApproval() {
        const title4eSpecialist = this.service.getTitleIVeSpecialist();
        console.log('tf', title4eSpecialist);
        const data = {
            "assignedtoid": title4eSpecialist.userid,
            "eventcode": "IVEADOP",
            "servicecaseid": this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID),
            "status": "Review"
        };
        console.log('routedUser', title4eSpecialist);
        this.service.sendAdoptiveChildAssessment(data).subscribe(response => {
            console.log('response', response);
        });

    }

}
