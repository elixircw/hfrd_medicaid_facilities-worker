import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonHttpService } from '../../../../../../../@core/services/common-http.service';
import { PaginationRequest } from '../../../../../../../@core/entities/common.entities';
import { DataStoreService, AlertService, AuthService, SessionStorageService } from '../../../../../../../@core/services';
import { GLOBAL_MESSAGES } from '../../../../../../../@core/entities/constants';
import { AppUser } from '../../../../../../../@core/entities/authDataModel';
import { CASE_STORE_CONSTANTS } from '../../../../../_entities/caseworker.data.constants';


@Component({
    selector: 'adoption-annual-reviews',
    templateUrl: './adoption-annual-reviews.component.html',
    styleUrls: ['./adoption-annual-reviews.component.scss']
})
export class AdoptionAnnualReviewsComponent implements OnInit {

    annualReviewAdoptionForm: FormGroup;
    approvalStatusForm: FormGroup;
    isFormDisplay = false;
    isEditReview = false;
    adoptioniverenewalid: string;
    store: any;
    id: string;
    annualReviewList: any[];
    submitStatus: any;
    roleId: AppUser;
    placementAnnualReviewID: any;
    isApproved: boolean;
    isSupervisor: boolean;
    isEnableComments: boolean;
    agreement: any;
    isinValidAnnualReview: boolean;
    maxdate: any;
    constructor(
        private formBuilder: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private _store: DataStoreService,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _session: SessionStorageService
    ) {
        this.store = this._store.getCurrentStore();
        this.id = this.store['CASEUID'];
        this.roleId = this._authService.getCurrentUser();
    }

    ngOnInit() {
        this.annualReviewInitialform();
        this.getReviews();
        this.getAgreementListing();
        if (this.roleId.role.name === 'apcs') {
            this.isSupervisor = true;
            this.placementAnnualReviewID = this._session.getItem('Placement-Review-Id');
            this._session.setItem('Placement-Review-Id', null);
            this.getReviews();
        }
    }

    annualReviewInitialform() {
        this.annualReviewAdoptionForm = this.formBuilder.group({
            // adoptionannualreviewid: [null],
            // reviewdate: [null],
            // ischilddisability: [null],
            // ischildspecialneed: [null],
            // isparentlegalresponsible: [null],
            // isrenewalsigned: [null],
            // isfinancialsupport: [null],
            // ischildenrolledschool: [null],
            // isdoumentationimmurization: [null],
            // ischildschoolemployeedisabled: [null],
            // iscompletesecondaryeducation: [null],
            // isenrolledinstitution: [null],
            // isparticipatingemployement: [null],
            // isemployeehrspermonth: [null],
            // isincapableactivities: [null],
            // adoptiveparentonedate: [null, Validators.required],
            // adoptiveparenttwodate: [null],
            // ldssdirectorsigndate: [null, Validators.required],
            // disabilitynotes: [null],
            // notes: [null],


            adoptionid: [null],
            adoptioniverenewalid: [null],
            assessmentdate: [null],
            parentsupportflag:  [null],
            schoolenrollflag:  [null],
            immunizationflag:  [null],
            mededuvoccertflag: [null],
            paytill22educhkflag:  [null],
            paytill22enrollchkflag:  [null],
            paytill22emppgmchkflag:  [null],
            paytill22emp80hrchkflag:  [null],
            paytill22medchkflag:  [null],
            ischilddisability: [null],
            ischildspecialneed:  [null],
            isparentlegalresponsible:  [null],
            isrenewalsigned:  [null],
            fatheragreementdate:  [null, Validators.required],
            motheragreementdate:  [null],
            designeeagreementdate:  [null, Validators.required],
            paytill22medchk:  [null],
            comments:  [null],
            disabilitynotes:  [null],
            enteredby: [null]
        });

        this.approvalStatusForm = this.formBuilder.group({ 
            routingstatus: [null],
            comments: [null]
        });
    }

    formAccess(reviewMode) {
        this.annualReviewAdoptionForm.enable();
        if (reviewMode === 0) {
            let startdate = this.agreement && this.agreement.startdate ? this.agreement.startdate : null;
            let enddate = this.agreement && this.agreement.enddate ? this.agreement.enddate : null;
            if(startdate) {
                startdate = new Date(startdate);
                startdate.setDate(startdate.getDate() + 364);
            } 
            if(enddate) {
                    enddate = new Date(enddate);
            }
            if(enddate < startdate) {
                this._alertService.warn(' Agreement Rate is less than 364 days , So you cant add annual review');
                return;
            }
            this.isFormDisplay = true;
            this.isEditReview = false;
            this.formClear();
            this.setReviewDate();

        } else {
            this.isFormDisplay = true;
            this.isEditReview = true;
            //this.formClear();
        }
    }

    setReviewDate() {
        let startdate = this.agreement && this.agreement.startdate ? this.agreement.startdate : null;
        let enddate = this.agreement && this.agreement.enddate ? this.agreement.enddate : null;
        if(startdate) {
        startdate = new Date(startdate);
        startdate.setDate(startdate.getDate() + 364);
        } 
        if(enddate) {
            enddate = new Date(enddate);
        }
        let existingAnnualReviw =  this.annualReviewList && this.annualReviewList.length ? this.annualReviewList.filter( review => { return  review.status === 'Approved';  }) : [];
        if(existingAnnualReviw && existingAnnualReviw.length) {
            startdate =  existingAnnualReviw[existingAnnualReviw.length-1].assessmentdate;
            startdate = new Date(startdate);
            startdate.setDate(startdate.getDate() + 364);
        }
        this.annualReviewAdoptionForm.patchValue({ assessmentdate : startdate});
        this.maxdate = enddate;
    }

    formClear() {
        this.annualReviewAdoptionForm.reset();
    }

    editAnnualReview(reviewModal) {

        this.formAccess(1);
        if(reviewModal && reviewModal.status === 'Review') {
            this.isApproved = false;
        } else {
            this.isApproved = true;
        }
        this.approvalStatusForm.patchValue({
           routingstatus : reviewModal.status
        });
        this.annualReviewAdoptionForm.patchValue(reviewModal);
        this.adoptioniverenewalid = reviewModal.adoptioniverenewalid;
    }

    viewAnnualReview(reviewModal) {

        this.formAccess(1);
        if(reviewModal && reviewModal.status === 'Review') {
            this.isApproved = false;
        } else {
            this.isApproved = true;
        }
        this.approvalStatusForm.patchValue({
           routingstatus : reviewModal.status
        });
        this.annualReviewAdoptionForm.patchValue(reviewModal);
        this.adoptioniverenewalid = reviewModal.adoptioniverenewalid;
        this.annualReviewAdoptionForm.disable();
    }

    onChangeDisability(key) {
        if(key !== 1 && !this.annualReviewAdoptionForm.getRawValue().ischilddisability ) {
        this.annualReviewAdoptionForm.patchValue({
          
            disabilitynotes: null
        });
        }
        if(key !== 1  ) {
            this.annualReviewAdoptionForm.patchValue({
                paytill22medchkflag: null
            });
            }
    }
    onChangeDisabilityCheckbox2(key) {
        if(key !== 1 && !this.annualReviewAdoptionForm.getRawValue().ischilddisability ) {
            this.annualReviewAdoptionForm.patchValue({
                disabilitynotes: null
            });
            }
    }

    onChangeDisabilityCheckbox1(key) {
        if(key !== 1 && !this.annualReviewAdoptionForm.getRawValue().paytill22medchkflag ) {
            this.annualReviewAdoptionForm.patchValue({
                disabilitynotes: null
            });
            }
    }


    getAgreementListing() {

        let planningid;
        planningid = this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID);

        let url = '';
        let obj;
    
         url = 'adoptionagreement/adoptioncaseagreementlist?filter';
          obj = { adoptioncaseid: this.id };

    
        this._commonHttpService
          .getSingle(
            new PaginationRequest({
              where: obj,
              method: 'get',
              page: 1,
              limit: 10
            }),
            url
          )
          .subscribe(res => {
            if (res && res.length && Array.isArray(res)) {
              const obcj = res[0];
              const agreementList =  obcj.getadoptioncaseagreementlist ;
              if (agreementList && agreementList.length) {
                const length = agreementList.length - 1;
                this.agreement = agreementList[length];
                if(this.agreement && this.agreement.agreementrate && this.agreement.agreementrate.length) {
                const agglength = this.agreement.agreementrate.length - 1;
                const forApproval  = this.agreement.agreementrate[agglength].enddate;
                    //  this.isinValidAnnualReview = forApproval && forApproval.length ?  true : false;
                    }
                
              }
            }
          });
    
      }

    getReviews() {
        this._commonHttpService.getArrayList(
            new PaginationRequest({
                page: 1,
                limit: 10,
                where: {
                    adoptioncaseid: this.id },
                method: 'get'
            }),
            'adoptioniverenewal/list' + '?filter'
        ).subscribe(
            (resp) => {
                console.log(resp);
                this.annualReviewList = resp;
                if(this.annualReviewList && this.annualReviewList.length) {
                   const forApproval  = this.annualReviewList.filter( review => { return review.status === 'Review';});
                     this.isinValidAnnualReview = forApproval && forApproval.length ?  true : false;
                    }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    routingUpdate() {
        const comment = 'Annual Review Submitted for review';
        this.submitStatus = Object.assign({
            objectid: this.placementAnnualReviewID ? this.placementAnnualReviewID : '',
            eventcode: 'ADYR',
            status: this.approvalStatusForm.value.routingstatus,
            comments: this.approvalStatusForm.value.comments,
            notifymsg: comment,
            routeddescription: comment,
            servicecaseid: this.id
        });
        console.log(this.submitStatus);
        if (this.roleId.role.name === 'apcs' && this.approvalStatusForm.value.routingstatus === '') {
            return this._alertService.error('Please select review status!');
        } else {
            console.log(this.submitStatus);
            this._commonHttpService.create(this.submitStatus, 'routing/routingupdate').subscribe(
                (res) => {
                    this._alertService.success('Annual Review ' + this.approvalStatusForm.value.routingstatus + ' Successfully!');
                    this.isApproved = true;
                    this.getReviews();
                },
                (err) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }
    rejectComments(status) {
        if (status === 'Rejected') {
            this.isEnableComments = true;
        } else {
            this.isEnableComments = false;
            this.approvalStatusForm.patchValue({ comments: '' });
        }
    }
    validateAnnualreview() {
        const reviewDate = this.annualReviewAdoptionForm.getRawValue().assessmentdate;
        if(this.agreement && this.agreement.agreementrate) {

        }
    }
    addEditAnnualReview(annualreview, editmode) {
        let modal;
        if (editmode === 0) {
             modal = Object.assign(
                {
                    adoptionid: this.id
                },
                annualreview
            );
        } else {
            modal = Object.assign(
                {
                    adoptionid: this.id
                },
                annualreview
            );
        }
        this.validateAnnualreview();
        if (this.annualReviewAdoptionForm.valid) {
            modal.adoptionid = this.id;
            this._commonHttpService.create(modal,
                'adoptioniverenewal/addupdate').subscribe(
                (res) => {
                    console.log(res);
                    this._alertService.success('Annual Review Saved Successfully');
                    this.getReviews();
                    this.formClear();
                    this.isFormDisplay = false;
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }
}
