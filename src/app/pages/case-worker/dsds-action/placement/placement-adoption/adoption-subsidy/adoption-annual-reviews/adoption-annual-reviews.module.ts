import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdoptionAnnualReviewsRoutingModule } from './adoption-annual-reviews-routing.module';
import { AdoptionAnnualReviewsComponent } from './adoption-annual-reviews.component';
import { MatFormFieldModule,
  MatCheckboxModule, MatDatepickerModule,
   MatInputModule, MatSelectModule, MatExpansionModule, MatCardModule, MatListModule, MatNativeDateModule, MatRadioModule, MatTableModule, MatTabsModule } from '@angular/material';
import { FormMaterialModule } from '../../../../../../../@core/form-material.module';
import { PaginationModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { QuillModule } from 'ngx-quill';
import { ControlMessagesModule } from '../../../../../../../shared/modules/control-messages/control-messages.module';
import { NgxfUploaderModule } from 'ngxf-uploader';

@NgModule({
  imports: [
    CommonModule,
    AdoptionAnnualReviewsRoutingModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    PaginationModule,
    MatExpansionModule,
    MatCardModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTableModule,
    MatTabsModule,
    QuillModule,
    ControlMessagesModule,
    FormMaterialModule,
    NgxfUploaderModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8CRCgWX0eRvC8XfFnaaMPm-36fb2GN9M'
    })
  ],
  declarations: [AdoptionAnnualReviewsComponent]
})
export class AdoptionAnnualReviewsModule { }
