import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { PaginationRequest, DynamicObject } from '../../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService } from '../../../../../../@core/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InvolvedPerson } from '../../../../_entities/caseworker.data.model';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { AlertService } from '../../../../../../@core/services/alert.service';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { TprDetails } from '../_entities/adoption.model';
import { HearingDetails, CourtOrderList } from '../../../court/_entities/court.data.model';
import * as moment from 'moment';
import { DSDS_STORE_CONSTANTS } from '../../../dsds-action.constants';
import { PlacementAdoptionService } from '../placement-adoption.service';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'adoption-tpr',
    templateUrl: './adoption-tpr.component.html',
    styleUrls: ['./adoption-tpr.component.scss']
})
export class AdoptionTprComponent implements OnInit {
    private id: string;
    private childActorId: string;
    store: DynamicObject;
    tprDetailsForm: FormGroup;
    isTermination: boolean;
    involvedPersons: InvolvedPerson[] = [];
    appealDecisionDropdownItems$: Observable<DropdownModel[]>;
    terminationtypDropdownItems$: Observable<DropdownModel[]>;
    methodServiceDropdownItems$: Observable<DropdownModel[]>;
    tprListDetails: TprDetails[];
    addEditLabel: string;
    updateButton: boolean;
    hearingData: any[] = [];
    courtOrder: CourtOrderList[] = [];
    private daNumber: string;
    alertMessage: string;
    remainingPeople: InvolvedPerson[];
    isAdoptionCreated: boolean;
    constructor(
        private route: ActivatedRoute,
        private _store: DataStoreService,
        private _commonHttp: CommonHttpService,
        private _formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _router: Router,
        private _PlacementAdoptionService: PlacementAdoptionService,
        private _dataStoreService: DataStoreService
    ) {
        this.store = this._store.getCurrentStore();
    }

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.addEditLabel = 'Add';

        this.getBreaklink();
        this.tprDetailsForm = this._formBuilder.group({
            intakeservicerequestactorid: ['', [Validators.required]],
            serveddate: [null],
            servicetypekey: [''],
            terminationtypekey: [''],
            isappealed: [null],
            isdssappealed: [null],
            appealdate: [null],
            appealdecisiontypekey: [''],
            decisiondate: [null],
            reason: [''],
            parentname: [''],
            tprdetailsid: [null],
            tprdecisiondate: [null],
            isdenied: [null],
            isgranted: [null]
        });


        this.tprDetailsForm.controls['parentname'].disable();
        console.log(this.store);
        // this.ValidateStoreValues();
        this._PlacementAdoptionService.storeDataPatched$.subscribe(data => {
            if (data === 'LegalCustody') {
        this.ValidateStoreValues();
          }
        });


    }

    
    private getBreaklink() {
        this._commonHttp
          .getArrayList({
            method: 'get', where: {
              adoptionplanningid: this._PlacementAdoptionService.getAdoptionPlanning().adoptionplanningid
            }
          }, 'adoptionbreakthelink/getadoptionbreakthelink?filter')
          .subscribe(res => {
            if (res && res.length) {
              res.map((item) => {
                const adoptionBreakLinkList = item && item.getadoptionbreakthelink ? item.getadoptionbreakthelink.map((breaklink) => {
                  this.isAdoptionCreated = (breaklink.adoptioncasenumber) ? true : false;
                  return breaklink;
                }) : [];
              });
            }
          });
      }

    ValidateStoreValues() {
        if (this.store['CASEUID']) {
        this.id = this.store['CASEUID'];
        }
        if (this.store['permanencyPlan']) {
            this.childActorId = this.store['permanencyPlan'].intakeservicerequestactorid;
        }
        if (
            !this.store['LegalCustodyDetails'] ||
            !this.store['LegalCustodyDetails'].length
        ) {
        (<any>$('#ap-tpr')).modal('show');
            // this._alertService.error('Please complete the TPR recommendation');
        } else {
            if (!this.store['LegalCustodyDetails'].filter(custody => custody.legalcustodytypekey === 'GUARDDSS').length) {
                (<any>$('#ap-tpr')).modal('show');
                this.alertMessage = 'Please complete the TPR recommendation with Guardianship to DSS';
            } else {
            this.getInvolvedPerson();
            this.getTPRList();
            this.getAppealDecisionDropdown();
            this.getTerminationTypeDropdown();
            this.getMethodServiceDropdown();
            this.gethearingDetails(); }
        }
    }
    gethearingDetails() {
        if (this.id) {
            this._commonHttp
                .getArrayList(
                    {
                        method: 'get',
                        where: {
                            intakeserviceid: this.id
                        }
                    },
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.GetCourtOrderUrl + '?filter'
                )
                .subscribe(result => {
                    if (result[0]) {
                        this.courtOrder = result;
                    }
                });
        }
    }
    isSetValidator(item) {
        if (item) {
            this.tprDetailsForm.controls['isdssappealed'].setValidators([Validators.required]);
            this.tprDetailsForm.controls['appealdate'].setValidators([Validators.required]);
            this.tprDetailsForm.controls['appealdate'].updateValueAndValidity();
            this.tprDetailsForm.controls['isdssappealed'].updateValueAndValidity();
        } else {
            this.tprDetailsForm.controls['isdssappealed'].clearValidators();
            this.tprDetailsForm.controls['appealdate'].clearValidators();
            this.tprDetailsForm.controls['appealdate'].updateValueAndValidity();
            this.tprDetailsForm.controls['isdssappealed'].updateValueAndValidity();
        }
    }

    cancelTRPDetails() {
        this.isSetValidator(false);
        this.tprDetailsForm.reset();
        this.updateButton = false;
        this.addEditLabel = 'Add';
    }
    clearTPRDetails() {
        this.tprDetailsForm.reset();
    }

    private getAppealDecisionDropdown() {
        this.appealDecisionDropdownItems$ = this._commonHttp
            .getArrayList(
                {
                    where: {
                        referencetypeid: '29',
                        teamtypekey: null
                    },
                    method: 'get',
                    nolimit: true
                },
                'referencetype/gettypes' + '?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }

    private getMethodServiceDropdown() {
        this.methodServiceDropdownItems$ = this._commonHttp
            .getArrayList(
                {
                    where: {
                        referencetypeid: '35',
                        teamtypekey: null
                    },
                    method: 'get',
                    nolimit: true
                },
                'referencetype/gettypes' + '?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }

    filterRemainingPersons() {
        this.remainingPeople = this.involvedPersons.filter(item => {
            let isExist = false;
            if (this.tprListDetails) {
                isExist = this.tprListDetails.some((ele: any) =>
                    ele.intakeservicerequestactorid === (item.intakeservicerequestactorid)
                );
            } else {
                isExist = false;
            }
            return !isExist;
        });
        // this.remainingPeople = this.involvedPersons;
        this.remainingPeople.sort((a, b) => a.fullname.localeCompare(b.fullname));
    }

    manageTPR(type, item?) {
        if (type === 'add') {
            this.filterRemainingPersons();
            this.addEditLabel = 'Add';
            this.tprDetailsForm.patchValue({ tprdetailsid: null });
            this.tprDetailsForm.enable();
            this.tprDetailsForm.reset();
        } else {
            this.remainingPeople = this.involvedPersons;
            this.remainingPeople.sort((a, b) => a.fullname.localeCompare(b.fullname));
            item.isdssappealed = item.isdssappealed + '';
            this.tprDetailsForm.patchValue(item);
            this.changePerson(item.intakeservicerequestactorid);
            this.isSetValidator(item.isappealed);
            if (type === 'view') {
                this.addEditLabel = 'View';
                this.updateButton = true;
                this.tprDetailsForm.disable();
            } else if (type === 'edit') {
                this.addEditLabel = 'Edit';
                this.updateButton = true;
                this.tprDetailsForm.enable();
            }
        }
        (<any>$('#termination-ofparental')).modal('show');
    }

    private getTerminationTypeDropdown() {
        this.terminationtypDropdownItems$ = this._commonHttp
            .getArrayList(
                {
                    where: {
                        referencetypeid: '30',
                        teamtypekey: null
                    },
                    method: 'get',
                    nolimit: true
                },
                'referencetype/gettypes' + '?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }

    private getTPRList() {
        this._PlacementAdoptionService.getTPRList(this.id)
            .subscribe(res => {
                if (res && res.length) {
                    this.tprListDetails = res;
                    this._store.setData(DSDS_STORE_CONSTANTS.TPR_LIST, res);
                }
            });
    }

    private getInvolvedPerson() {
        this._commonHttp
            .getArrayList(
                {
                    page: 1,
                    method: 'get',
                    where: {objectid: this.id , objecttypekey : 'servicecase'}
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            )
            .subscribe(res => {
                if (res['data'] && res['data'].length) {
                    this.involvedPersons = res['data'].filter(item => {
                        if ( !item.rolename || item.rolename === '') {

                            if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestpersontypekey) {
                                item.rolename  = item.roles[0].intakeservicerequestpersontypekey; }
                        }
                        if (item.rolename &&
                            item.rolename !== 'AV' &&
                           //  item.rolename !== 'AM' &&
                            item.rolename !== 'CHILD' &&
                            item.rolename !== 'RC' &&
                            item.rolename !== 'BIOCHILD' &&
                            item.rolename !== 'NVC' &&
                            item.rolename !== 'OTHERCHILD' &&
                            item.rolename !== 'PAC'
                            // (item.roles.length &&
                            //     item.roles.filter(
                            //         itm =>
                            //             itm.rolename !== 'AV' &&
                            //             itm.rolename !== 'AM' &&
                            //             itm.rolename !== 'CHILD' &&
                            //             itm.rolename !== 'RC' &&
                            //             itm.rolename !== 'BIOCHILD' &&
                            //             itm.rolename !== 'NVC' &&
                            //             itm.rolename !== 'OTHERCHILD' &&
                            //             itm.rolename !== 'PAC'
                            //     ))
                        ) {
                            if (!item.intakeservicerequestactorid) {
                               if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestactorid) {
                                item.intakeservicerequestactorid  = item.roles[0].intakeservicerequestactorid; }
                            }
                            return item;
                        }
                    });
                    //  Filter Relative Type Only
                    // .filter(item => {
                    //     if ( !item.rolename || item.rolename === '') {

                    //         if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestpersontypekey) {
                    //             item.rolename  = item.roles[0].intakeservicerequestpersontypekey; }
                    //     }
                    //     // tslint:disable-next-line:max-line-length
                    //     if (item.rolename === 'RELATIVE') {
                    //         console.log(item);
                    //         return item;
                    //     }
                    // });
                }
            });
    }

    changePerson(id) {
        const personRelation = this.involvedPersons && this.involvedPersons.length ? this.involvedPersons.filter(item => item.intakeservicerequestactorid === id) : [];
        const personnameText = personRelation && personRelation.length ? personRelation[0].firstname + ' ' + personRelation[0].lastname : '';
        this.tprDetailsForm.patchValue({
            parentname: personnameText
        });
    }
    saveTprDetail(model) {
        const TprDetailInput = model;
        TprDetailInput.intakeserviceid = null;
        TprDetailInput.servicecaseid = this.id;
        TprDetailInput.tprrecommendationid = this.store['TPRRecommendationId'];
        TprDetailInput.isdssappealed = Number(model.isdssappealed);
        TprDetailInput.isappealed = model.isappealed ? 1 : 0;
        if (model.tprdetailsid) {
            TprDetailInput.tprdetailsid = model.tprdetailsid;
            this._commonHttp.patch(TprDetailInput.tprdetailsid, TprDetailInput, 'tprdetails').subscribe(
                res => {
                    this.updateButton = true;
                    this._alertService.success('TPR Details saved successfully');
                    (<any>$('#termination-ofparental')).modal('hide');
                    this.tprDetailsForm.reset();
                    this.getTPRList();
                },
                err => {
                    console.log(err);
                }
            );
        } else {
            this._commonHttp.create(TprDetailInput, 'tprdetails/').subscribe(
                res => {
                    this.updateButton = true;
                    this._alertService.success('TPR Details saved successfully');
                    (<any>$('#termination-ofparental')).modal('hide');
                    this.tprDetailsForm.reset();
                    this.getTPRList();
                },
                err => {
                    console.log(err);
                }
            );
        }
    }
    navigateTo() {
        const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/tpr-recom';
        this._router.navigate([redirectUrl]);
    }
}
