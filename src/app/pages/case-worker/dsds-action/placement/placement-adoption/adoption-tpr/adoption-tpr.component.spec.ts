import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionTprComponent } from './adoption-tpr.component';

describe('AdoptionTprComponent', () => {
  let component: AdoptionTprComponent;
  let fixture: ComponentFixture<AdoptionTprComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionTprComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionTprComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
