import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DynamicObject, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { DataStoreService, SessionStorageService, AuthService } from '../../../../../@core/services';
import { CommonHttpService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { PlacementAdoptionService } from './placement-adoption.service';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { DSDS_STORE_CONSTANTS } from '../../dsds-action.constants';
// import { Subject } from 'rxjs/Subject';
import { ServiceCasePermanencyPlanService } from '../../service-case-permanency-plan/service-case-permanency-plan.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'placement-adoption',
    templateUrl: './placement-adoption.component.html',
    styleUrls: ['./placement-adoption.component.scss']
})
export class PlacementAdoptionComponent implements OnInit {
    id: string;
    daNumber: string;
    permanencyPlanId: string;
    private childActorId: string;
    private store: DynamicObject;
    isSupervisor: boolean;
    roleId: AppUser;
    transkey: string;
    transid: string;
    showNavScale = true;
    child: any;
    ivestatus:string;
    ivecomment:string;
    completedIVEReview = false;
    adoptionSubsidyIsEdited = false;
    constructor(private _store: DataStoreService,
        private _commonHttp: CommonHttpService,
        private route: ActivatedRoute,
        private _PlacementAdoptionService: PlacementAdoptionService,
        private _serviceCasePermanencyPlanService: ServiceCasePermanencyPlanService,
        private _session: SessionStorageService,
        private _authService: AuthService,
        private router: Router,
        private _dataStoreService: DataStoreService) {
        this.store = this._store.getCurrentStore();
        this.route.data.subscribe(response => {
            console.log('adoption resolver data:', response);
        });
    }

    ngOnInit() {
        this.roleId = this._authService.getCurrentUser();
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        const caseType = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
        this.showNavScale = (caseType === CASE_TYPE_CONSTANTS.ADOPTION) ? false : true;
        this._PlacementAdoptionService.setAdoptionPlanning(null);
        if (this.roleId.role.name === 'apcs') {
            this._PlacementAdoptionService.setPlacementsDetails();
            this.id = this.store['CASEUID'];
            this.daNumber = this.store['DANUMBER'];
            this.isSupervisor = true;
            // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
            this.transkey = this._session.getItem('transkey');
            this.transid = this._session.getItem('transid');
            console.log(this.permanencyPlanId);
            this._session.setItem('transkey', null);
            this._session.setItem('transid', null);
            this._PlacementAdoptionService.setplacementAdoptionData('PermanencyPlanId', this.permanencyPlanId);
            console.log(this._PlacementAdoptionService.getplacementAdoptionData('PermanencyPlanId'));
            this.getChildDetails(true);
        } else {
            this.getChildDetails(false);
        }
        this.validateStoreValues();
        this.getReportedChild();
    }

    closeIVEPopup(){
        (<any>$('#ivePopUp')).modal('hide');
      }
    private getReportedChild() {
        this._commonHttp
            .getSingle(
                {
                    method: 'get',
                    where: {objecttypekey: 'servicecase', objectid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
            .subscribe(res => {
                if (res && res.data) {
                const cjamspid = this._dataStoreService.getData('childforGAP');
                const repChild = res.data.filter(child => child.cjamspid === cjamspid);
                if (repChild && repChild.length) {
                    this._commonHttp.getSingle(
                        {},
                        'titleive/adoption/adoption-eligibility-worksheet/' +  cjamspid+'/'+ repChild[0].removalid
                        ).subscribe(response => {
                            if(response && response.adoptionApplicabilityInfo && response.adoptionApplicabilityInfo.length > 0 
                                && response.adoptionApplicabilityInfo[0].ivestatus){
                                    this.ivestatus = response.adoptionApplicabilityInfo[0].ivestatus;
                                    this.ivecomment = response.adoptionApplicabilityInfo[0].ivecomment;
                                }
                            if(this.ivestatus==='APPROVED'){
                                this.completedIVEReview = true;
                            }
                            if(this.ivestatus==='RETURNED')
                            (<any>$('#ivePopUp')).modal('show');
                        });
                    this._dataStoreService.setData('childremovalid', repChild[0].removalid); // Verify again
                    /*
                    this._commonHttp.getSingle(
                        {
                            method: 'get',
                            where: {clientId: cjamspid, removalId: repChild[0].removalid}
                        },
                        'titleive/getAdoptionapplicabilityinfo?filter'
                    ).subscribe( response =>{
                        if(response && response[0] && response[0].ivestatus == 'PENDING')
                        this.underIVEReview = true;
                        else
                        this.underIVEReview = false;
                    });
                    */
                }
                 }
            });
    }

    validateStoreValues() {
        if(!this.store['placement_child']) {
            this.store['placement_child'] = this._session.getObj('placement_child');
        }
        if(!this.store['placed_child']) {
            this.store['placed_child'] = this._session.getObj('placed_child');
            if(!this.child) {
                this.child = this.store['placed_child'];
            }
        }
        if (this.store['placement_child']) {
            this.childActorId = this.store['placement_child'].intakeservicerequestactorid;
             this.permanencyPlanId = this.store['placement_child'].permanencyplanid;
             console.log('Store Values', this.store);
            this.id = this.store['CASEUID'];
            this.getRecommendedList();
            this.getLegalCustody(this.childActorId);
            this.getTPRDetails(this.id);
            this.getEffortListing(this.permanencyPlanId);
            
        } else  {
            this._PlacementAdoptionService.getPermanencyPlanId(1, 100, this.transkey, this.transid).subscribe( data => {
                this.permanencyPlanId = (data && data.length) ? data[0].permanencyplanid : null;
                this._PlacementAdoptionService.getPlacementConfig(1, 100, this.permanencyPlanId).subscribe(
                    res => {
                        let adoptionplanningid = null;
                        if (res.length && res[0] && res[0].placements && res[0].placements.length) {
                        console.log('Config From API', res);
                        const placement = res[0];
                        placement.intakeservicerequestactorid = placement['placements'][0].intakeservicerequestactorid;
                        this._dataStoreService.setData('placed_child', placement);
                        placement['placements'][0].permanencyplanid = this.permanencyPlanId;
                         // placement['placements'][0].permanencyplanid = this.permanencyPlanId ;
                        this._dataStoreService.setData('placement_child', placement['placements'][0]);
                        adoptionplanningid = placement['adoptionplanningid'];
                        this._dataStoreService.setData('adoptionplanningid', placement['adoptionplanningid']);
                        this._dataStoreService.setData('tprrecommendationid', placement['tprrecommendationid']);
                        this._dataStoreService.setData('adoptionagreementid', placement['adoptionagreementid']);
                        this.validateStoreValues();
                        console.log(this.store);
                        } else if (res.length && res[0]) {
                            adoptionplanningid = res[0].adoptionplanningid;
                            this._dataStoreService.setData('placement_child', res[0].placements);
                            this._dataStoreService.setData('adoptionplanningid', res[0].adoptionplanningid);
                            this._dataStoreService.setData('tprrecommendationid', res[0].tprrecommendationid);
                            this._dataStoreService.setData('adoptionagreementid', res[0].adoptionplanningid);
                            this.validateStoreValues();
                        }
                        if(res.length && res[0])
                        this.checkAdoptionSubsidyIsEdited(adoptionplanningid);
                    }
                );
             });

        } 
        // else {
        //     this.goBack();
        // }

    }

    getLegalCustody(childActorId) {

        this._PlacementAdoptionService.getLegalCustody(childActorId)
            .subscribe(res => {
                if (res && res.length && res[0].getlegalcustody) {
                    this._store.setData('LegalCustodyDetails', res[0].getlegalcustody);
                    this.broadCastTabLoad('LegalCustody');
                }
            });
    }

   getTPRDetails(id) {
        this._PlacementAdoptionService.getTPRList(id)
            .subscribe(res => {
                if (res && res.length ) {
                    this._store.setData(DSDS_STORE_CONSTANTS.TPR_LIST, res);
                    this.broadCastTabLoad('TRPList');
                }
            });
    }

    getEffortListing(id) {
        this._PlacementAdoptionService.getEffortListing(id).subscribe(res => {
        if (res && res.length && res[0].getadoptionplanning) {
            this._store.setData('adoptionEffort', res[0].getadoptionplanning[0]);
            this._PlacementAdoptionService.setAdoptionPlanning(res[0].getadoptionplanning[0]);
            this.broadCastTabLoad('planning');
            this.getNarrativeListing();
        }
        });
    }

    getNarrativeListing() {
        this._PlacementAdoptionService.getNarrative(this.permanencyPlanId).subscribe(res => {
        if (res && res.length && res[0].getadoptionplanning) {
            this._store.setData('adoptionEffort', res[0].getadoptionplanning[0]);
            this.checkAdoptionSubsidyIsEdited(res[0].getadoptionplanning[0].adoptionplanningid);
            this._PlacementAdoptionService.setAdoptionPlanning(res[0].getadoptionplanning[0]);
            this.broadCastTabLoad('TRPList');
        }
        });
    }

    getPlacementInfoList(pageNumber, limit) {
        console.log('Setting Store Variables onReload');
        return  this._commonHttp
          .getPagedArrayList(
            new PaginationRequest({
              page: pageNumber,
              limit: limit,
              method: 'get',
              where: { servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
            }),
            'placement/getplacementbyservicecase?filter'
            // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          ).map((res) => {
            return {
                data: res.data,
                count: res.count
            };
        })
        .share();

      }

    private getRecommendedList() {
        // this._commonHttp
        //     .getSingle(
        //         new PaginationRequest({
        //             where: {

        //                 intakeservicerequestactorid: this.childActorId ? this.childActorId : null,
        //                 intakeserviceid: this.id
        //             },
        //             method: 'get'
        //         }),
        //         CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.TprRecommendationList + '?filter'
        //     )
            this._PlacementAdoptionService.getRecommendedList(this.id, this.permanencyPlanId)
            .subscribe(res => {
                this._store.setData('TPRRecommendationList', res, true);
                this._store.setData('TPRRecommendationId', res.tprrecommendationid);
                this.broadCastTabLoad('TPRRecommend');
            });
    }

    broadCastTabLoad(tabName) {
        this._PlacementAdoptionService.broadStoreDataPatched(tabName);
    }

    getTabAccess() {
        this._PlacementAdoptionService.getplacementAdoptionData('isRecommended');
      }

    goBack() {
        this.router.navigate(['../../'], { relativeTo: this.route });
    }

    getChildDetails(action) {
        this._serviceCasePermanencyPlanService.getPermanencyPlanList(1, 10).subscribe((data: any) => {
            let permanencyPlanList = data;
            const permanencyplanid = (action) ? this._session.getItem(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID) :
                this._dataStoreService.getData(CASE_STORE_CONSTANTS.ADOPTION_PERMANENCYPLANID);
            permanencyPlanList = Array.isArray(permanencyPlanList) ? permanencyPlanList : [];
            const plan = permanencyPlanList.find(item => {
                const planlist = Array.isArray(item.permanencyplans) ? item.permanencyplans : [];
                const result = planlist.some(ele => ele.permanencyplanid === permanencyplanid);
                return result;
            });
            if (plan) {
                this.child = {
                    clientname: plan.clientname,
                    cjamspid: plan.cjamspid,
                    dob: plan.dob,
                    gender: plan.gender
                };
            }
        });


    }

    private checkAdoptionSubsidyIsEdited(adoptionplanningid) {
        if (adoptionplanningid && adoptionplanningid != '') {
            this._commonHttp
                .getSingle(
                    new PaginationRequest({
                        where: { adoptionplanningid: adoptionplanningid },
                        method: 'get',
                        page: 1,
                        limit: 1
                    }),
                    'adoptionagreement/list?filter'
                )
                .subscribe(res => {
                    if (res && res.length && Array.isArray(res)) {
                        const agreementList = res[0].getadoptionagreementlist;
                        if (agreementList && agreementList.length && agreementList[0]) {
                            this.adoptionSubsidyIsEdited = true;
                        }
                        else
                        this.adoptionSubsidyIsEdited = false;
                    }
                }
                );
        }
    }
}
