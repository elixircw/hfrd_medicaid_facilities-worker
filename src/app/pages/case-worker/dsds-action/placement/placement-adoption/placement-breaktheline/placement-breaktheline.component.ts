import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicObject, PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../../../@core/services/common-http.service';
import { AlertService } from '../../../../../../@core/services/alert.service';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { DataStoreService } from '../../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { GetBreakLine, Getadoptionbreakthelink } from '../_entities/adoption.model';
import { AuthService } from '../../../../../../@core/services/auth.service';
import { AppConstants } from '../../../../../../@core/common/constants';
import { PlacementAdoptionService } from '../placement-adoption.service';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'placement-breaktheline',
  templateUrl: './placement-breaktheline.component.html',
  styleUrls: ['./placement-breaktheline.component.scss']
})
export class PlacementBreakthelineComponent implements OnInit {
  adoptionBreakLinkForm: FormGroup;
  private id: string;
  daNumber: string;
  store: DynamicObject;
  childActorId: string;
  addEditLabel: string;
  updateButton: boolean;
  isSupervisor: boolean;
  approvalStatus: string;
  addDisable = true;
  createAdoptionbtn = false;
  agreement : string;
  agreementapprovalStatus : string;
  breakLinkGrid = false;
  savebtn: string;
  childPlacement: any;
  adoptionBreakLinkList: Getadoptionbreakthelink[] = [];
  newAdoptionCaseId: any;
  adoptionplanningid: string;
  adoptionbreakthelinkid: string;
  breakDlinkPatchObj: any;
  isAgreementReview: boolean;
  currentDate = new Date();
  originalUserList : any[];
  getUsersList : any[];
  mergeUsersList : any[];
  zipCodeIndex : number;
  selectedPerson: any;
  zipCode: string;
  // tslint:disable-next-line:max-line-length
  constructor(private _formBuilder: FormBuilder,
    private _commonHttp: CommonHttpService,
    private _alertService: AlertService,
    private _store: DataStoreService,
    private route: ActivatedRoute,
    private _PlacementAdoptionService: PlacementAdoptionService,
    private _authService: AuthService,
    private _router: Router) {
    this.id = this._store.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._store.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.store = this._store.getCurrentStore();
  }

  ngOnInit() {

    const userInfo = this._authService.getCurrentUser();
    this.isSupervisor = userInfo.role.name === AppConstants.ROLES.SUPERVISOR;

    // this.checkSubsidy();
    this.loadBreaktheLink();
    // if (this.store['adoptionBreakLink']) {
      this._PlacementAdoptionService.storeDataPatched$.subscribe(data => {
      console.log(data);
        if (data !== 'TRPList' ) {
          if (this.store['placement_child']) {
            this.childPlacement = this.store['placement_child'];
            this.childActorId = this.store['placement_child'].intakeservicerequestactorid;
        }
        if (data === 'planning') {
        this.checkSubsidy();
         }
        }
      });
    // }

    this.adoptionBreakLinkForm = this._formBuilder.group({
      legallyfree: [''],
      adoptiveplacement: [''],
      placementagreement: ['', [Validators.required]],
      agreementsigneddate: [null, [Validators.required]],
      adoptionfinalization: ['', [Validators.required]],
      associatedcourtorderdate: ['', [Validators.required]],
      finalizationdate: ['', [Validators.required]],
      isinPreAdoptivePlacement:  ['', [Validators.required]],
      isLegallyFree:  ['', [Validators.required]],
      adoptionbreakthelinkid: [null],
      adoptionplanbegindate: [''],
      narrativecheckliststatus: [''],
      tprmotherdate: [''],
      tprfatherdate: [''],
      providername: [''],
      placementstructure: [''],
      placementstartdate: [''],
      placementenddate: [''],
      placementapprovalstatus: [''],
      status: ['Review']
    });
}

checkSubsidy() {
  let planningid = this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null;

  if (!planningid) {
    planningid = this.store[CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID];
  }

  if (planningid) {
    this._PlacementAdoptionService.getAgreementListing(planningid).subscribe(data => {
      if (data && data.length) {
        const adoptionData = data[0];
        if ( adoptionData && adoptionData.getadoptionagreementlist && adoptionData.getadoptionagreementlist.length) {
            const agreementList = adoptionData.getadoptionagreementlist[0];
            if (agreementList && agreementList.routingstatus === 'Approved') {
              this.loadBreaktheLink();
            } else {
                this.loadBreaktheLink();
                if (!this.store['adoptionBreakLink']) {
                  this.isAgreementReview = true;
                }
            }
        } else {
          if (!this.store['adoptionBreakLink']) {
            (<any>$('#ap-break')).modal('show');
          }
        }
      } else {
        if (!this.store['adoptionBreakLink']) {
            (<any>$('#ap-break')).modal('show');
        }
        }
      });
    }
    else {
        if (!this.store['adoptionBreakLink']) {
            (<any>$('#ap-break')).modal('show');
        }
        }
}
clearBreakLink() {
    this.adoptionBreakLinkForm.reset();
}
navigateTo() {
  const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/adoption-subsidy/agreement';
  this._router.navigate([redirectUrl]);
}

loadBreaktheLink() {
  this.setAdoptionPlanningId();
  this.getBreaklink();
}
  setAdoptionPlanningId() {
    if (this.store['adoptionplanningid']) {
      this.adoptionplanningid = this.store['adoptionplanningid'];
      // this.setProviderDetails();
    } else if (this.store['adoptionEffort'] && this.store['adoptionEffort'].adoptionplanningid ) {
      this.adoptionplanningid = this.store['adoptionEffort'].adoptionplanningid;
    } else if (this.store['ADOPTION_PLANNING_ID']) {
      this.adoptionplanningid = this.store['ADOPTION_PLANNING_ID'];
    }
  }

  setProviderDetails() {
    this.adoptionBreakLinkForm.patchValue({
      adoptiveplacement: (this.childPlacement && this.childPlacement.providerdetails) ? 1 : 0,
      providername: (this.childPlacement && this.childPlacement.providerdetails) ? this.childPlacement.providerdetails.providername : '',
      placementstructure: (this.childPlacement.placementstructuredesc) ? this.childPlacement.placementstructuredesc : '',
      placementstartdate: (this.childPlacement.startdate) ? this.childPlacement.startdate : '',
      placementenddate: (this.childPlacement.enddate) ? this.childPlacement.enddate : '',
      placementapprovalstatus: (this.childPlacement.routingstatus) ? this.childPlacement.routingstatus : '',
    });
    console.log(this.adoptionBreakLinkForm.getRawValue());
  }

  ValidateCheckBoxes() {
    const placementagreement = this.adoptionBreakLinkForm.getRawValue().placementagreement;
    const adoptionfinalization = this.adoptionBreakLinkForm.getRawValue().adoptionfinalization;
    const isinPreAdoptivePlacement = this.adoptionBreakLinkForm.getRawValue().isinPreAdoptivePlacement;
    const isLegallyFree = this.adoptionBreakLinkForm.getRawValue().isLegallyFree;
    if (placementagreement && adoptionfinalization && isinPreAdoptivePlacement && isLegallyFree ) {
      return true;
    } else {
      return false;
    }
  }

  saveBreakLink() {
    const model = this.adoptionBreakLinkForm.value;
     model.adoptionplanningid = this.adoptionplanningid ? this.adoptionplanningid : null;
    model.servicecaseid = this.store['CASEUID'] ? this.store['CASEUID'] : null;
    this._commonHttp.create(model, 'adoptionbreakthelink/addupdate').subscribe(
        result => {
            this._alertService.success('Adoption Break The Link saved successfully!');
            this.adoptionBreakLinkForm.reset();
            (<any>$('#breakLink')).modal('hide');
            this.getBreaklink();
        },
        error => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
    );
}

approveBreakLink(approvalStatus) {
  this._commonHttp
      .create(
          {
              objectid: this.adoptionbreakthelinkid ? this.adoptionbreakthelinkid : null,
            //  objectid: 'ebd99470-e114-4a3a-8440-5f18a8191b99',
              eventcode: 'ABLR',
              status: approvalStatus,
              comments: 'Adoption Plan ' + approvalStatus,
              notifymsg: 'Adoption Plan ' + approvalStatus,
              routeddescription: 'Adoption Plan ' + approvalStatus,
              servicecaseid: this.id
          },
          'routing/routingupdate'
      )
      .subscribe(
          res => {
              this._alertService.success('Break The Link is ' + approvalStatus + ' successfully!');
              this.approvalStatus = approvalStatus;
              (<any>$('#breakLink')).modal('hide');
              this.getBreaklink();
          },
          err => {
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          }
      );
}

manageBreakLink(type, item?) {
  if (type === 'add') {
      this.addEditLabel = 'Add';
      this.savebtn = 'Save & Submit For Approval';
      this.adoptionBreakLinkForm.patchValue({ adoptionbreakthelinkid: null });
      //  this.adoptionBreakLinkForm.enable();
       this.adoptionBreakLinkForm.reset();
       if (this.breakDlinkPatchObj) {
       this.adoptionBreakLinkForm.patchValue(this.breakDlinkPatchObj);
       }
  } else {
      this.adoptionBreakLinkForm.patchValue(item);
      this.approvalStatus = item.status;
      if (type === 'view') {
          this.addEditLabel = 'View';
          this.updateButton = true;
          this.savebtn = 'Update & Submit For Approval';
          this.adoptionBreakLinkForm.disable();
      } else if (type === 'edit') {
          this.addEditLabel = 'Edit';
          this.updateButton = true;
          this.adoptionBreakLinkForm.enable();
          this.savebtn = 'Update & Submit For Approval';
          // this.disableFieldOnInit();
      }
  }
  (<any>$('#breakLink')).modal('show');
}

getFormValue(fieldName) {
  const FormData = this.adoptionBreakLinkForm.getRawValue();
  // console.log(FormData);
  return FormData[fieldName] ? FormData[fieldName] : null ;
}

private getBreaklink() {
  this._commonHttp
      .getArrayList({ method: 'get', where: {
         adoptionplanningid: this.adoptionplanningid ? this.adoptionplanningid : null
        }}, 'adoptionbreakthelink/getadoptionbreakthelink?filter')
      .subscribe(res => {
          if (res && res.length) {
              res.map((item) => {
                this.adoptionBreakLinkList = item && item.getadoptionbreakthelink ? item.getadoptionbreakthelink.map((breaklink) => {
                    if (breaklink.adoptionbreakthelinkid) {
                        this.breakLinkGrid = true;
                        this.addDisable = false;
                    }
                    if (breaklink.status === 'Approved') {
                        this.createAdoptionbtn = true;
                    }
                    if (breaklink.adoptioncasenumber) {
                      this.createAdoptionbtn = false;
                    }
                  this._store.setData('adoptionBreakLink', breaklink);
                  console.log('Break Break', breaklink);
                  this.breakDlinkPatchObj = breaklink;
                  this.adoptionbreakthelinkid = breaklink.adoptionbreakthelinkid;
                  breaklink.tprmotherdate = (this.breakDlinkPatchObj && this.breakDlinkPatchObj.tprdates && this.breakDlinkPatchObj.tprdates.length) ? this.breakDlinkPatchObj.tprdates[0] : null;
                  breaklink.tprfatherdate = (this.breakDlinkPatchObj && this.breakDlinkPatchObj.tprdates && this.breakDlinkPatchObj.tprdates.length && this.breakDlinkPatchObj.tprdates.length > 1) ?
                    this.breakDlinkPatchObj.tprdates[1] : null;
                  this.breakDlinkPatchObj.tprmotherdate = (this.breakDlinkPatchObj && this.breakDlinkPatchObj.tprdates && this.breakDlinkPatchObj.tprdates.length) ?
                    this.breakDlinkPatchObj.tprdates[0] : null;
                  this.breakDlinkPatchObj.tprfatherdate = (this.breakDlinkPatchObj && this.breakDlinkPatchObj.tprdates && this.breakDlinkPatchObj.tprdates.length &&
                    this.breakDlinkPatchObj.tprdates.length > 1) ? this.breakDlinkPatchObj.tprdates[1] : null;
                  if (breaklink.tprmotherdate || breaklink.tprfatherdate) {
                    this.breakDlinkPatchObj.isLegallyFree = true;
                  }
                    if (breaklink.tprmotherdate || breaklink.tprfatherdate ) {
                      this.breakDlinkPatchObj.isLegallyFree = true;
                    }
                    if (breaklink.providername) {
                      this.breakDlinkPatchObj.isinPreAdoptivePlacement = true;
                    }
                    this.breakDlinkPatchObj.adoptionfinalization = (this.breakDlinkPatchObj && this.breakDlinkPatchObj.finalizationdate) ?  1 : null;
                    this.breakDlinkPatchObj.finalizationdate = (this.breakDlinkPatchObj && this.breakDlinkPatchObj.finalizationdate) ?  this.breakDlinkPatchObj.finalizationdate : null;
                    this.breakDlinkPatchObj.placementagreement =  (this.breakDlinkPatchObj && this.breakDlinkPatchObj.parent1signdate) ?  1 : null;
                    this.breakDlinkPatchObj.agreementsigneddate = (this.breakDlinkPatchObj && this.breakDlinkPatchObj.parent1signdate) ?  this.breakDlinkPatchObj.parent1signdate : null;
                    this.adoptionBreakLinkForm.patchValue(this.breakDlinkPatchObj);
                    // this.adoptionBreakLinkForm.patchValue({'tprmotherdate': tprmotherdate});
                    // this.adoptionBreakLinkForm.patchValue({'tprfatherdate': tprfatherdate});
                    console.log('Break Break', this.adoptionBreakLinkForm.getRawValue());
                    return breaklink;
                }) : [];
            });
          } else if(this.isAgreementReview) {
            (<any>$('#ap-break')).modal('show');
          }
      });
}

  createadoptioncase() {
    const provider = this._store.getData(CASE_STORE_CONSTANTS.PLACEMENT_CHILD);
    const child = this._store.getData(CASE_STORE_CONSTANTS.PLACED_CHILD);
    console.log(provider);
    console.log(child);
    const req = this.prepareadoptioncasereq(child, provider);
    this._PlacementAdoptionService.createadoptioncase(req).subscribe(data => {
      console.log(data);
      if (data && data.length) {
        const res = data[0];
        this.newAdoptionCaseId = res.servicecaseno;
        (<any>$('#adoption-caseassign')).modal('hide');
        this.getBreaklink();
        setTimeout(() => {
          (<any>$('#mdchessis-info')).modal('show');
        }, 1000);
      }
     });
  }

  prepareadoptioncasereq(child, provider) {
    const data = {
      'adoptionplanningid': this.store['adoptionEffort'] ? this.store['adoptionEffort'].adoptionplanningid : null,
      'servicecaseid': this.id,
      'assigntoid': this.selectedPerson?this.selectedPerson.userid:null,
      'person': [{
        'personid': child.personid, //@Simar- THIS IS NEEDED to transfer data from old person id to the new person id
        // All else is completely wrong as being hardcoded for the new adopted person child
        'firstname': child.firstname ? child.firstname : '',
        'lastname': ( provider.providerdetails && provider.providerdetails.providername ) ? provider.providerdetails.providername : '',
        'middlename': child.middlename ? child.middlename : '',
        'dob': child.dob,
        'gendertypekey': (child.gender === 'Female') ? 'F' : 'M',
        'role': 'RC', // ROLE to be changed as -> CHILD 
        'address': [{
          'address': '906 Valley St',
          'address2': 'Newcomb',
          'personaddresstypekey': 'C',
          'zipcode': '99999',
          'city': 'albaniya',
          'state': 'AR',
          'country': 'USA',
          'county': 'MD'
        }],
        'contact': [{
          'personphonetypekey': 'P',
          'phonenumber': '199999949',
          'phoneextension': ''
        }]
      },
      ]
    };
    return data;
  }

  getRoutingUser() {
    this._commonHttp
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'INVR'},
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe((result) => {
        this.getUsersList = result.data;
        this.originalUserList = this.getUsersList;
        this.listUser();
      });
  }

  listUser() {
    this.selectedPerson = null;
    this.getUsersList = [];
    this.mergeUsersList = [];
    this.getUsersList = this.originalUserList;
      this.getUsersList = this.getUsersList.filter((res) => {
        if (res.issupervisor === false) {
          this.isSupervisor = false;
          return res;
        }
      });
      this.getUsersList.map((data) => {
        if (data.homelocationcode === this.zipCode || data.worklocationcode === this.zipCode) {
          this.mergeUsersList.push(data);
          this.zipCodeIndex = this.getUsersList.indexOf(data);
          this.getUsersList.splice(this.zipCodeIndex, 1);
        }
      });
      if (this.mergeUsersList !== undefined) {
        this.getUsersList = this.mergeUsersList.concat(this.getUsersList);
      }
  }

  selectPerson(person) {
    this.selectedPerson = person;
}

  assignUser() {
    if (this.selectedPerson) {
            this.createadoptioncase();
    } else {
        this._alertService.warn('Please select a person');
    }
}

closePopup() {
    (<any>$('#adoption-caseassign')).modal('hide');
}
}
