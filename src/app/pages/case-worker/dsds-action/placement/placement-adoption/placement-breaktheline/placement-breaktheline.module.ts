import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PlacementBreakthelineRoutingModule } from './placement-breaktheline-routing.module';
import { PlacementBreakthelineComponent } from './placement-breaktheline.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedPipesModule } from '../../../../../../@core/pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    PlacementBreakthelineRoutingModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    ReactiveFormsModule,
    SharedPipesModule
  ],
  declarations: [PlacementBreakthelineComponent]
})
export class PlacementBreakthelineModule { }
