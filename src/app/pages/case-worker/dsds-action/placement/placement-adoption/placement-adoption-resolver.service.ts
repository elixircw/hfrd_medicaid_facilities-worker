import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AuthService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { PlacementAdoptionService } from './placement-adoption.service';

@Injectable()
export class PlacementAdoptionResolverService {
  daNumber: string;
  caseuid: string;
  childId: string;
  id: string;
  constructor(private _authService: AuthService, private _commonHttpService: CommonHttpService, private _dataStoreService: DataStoreService,
    private route: ActivatedRoute, private _router: Router, private _placementAdoptionService: PlacementAdoptionService,
    private _session: SessionStorageService) {
    console.log('adoption resolver>>>', this.id, this.caseuid);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    // this.childId = route.paramMap.get('childId');
    this.childId = this._dataStoreService.getData('adoptedchildId');
    const placement = this._dataStoreService['placement_child'];
    const permanencyplanid = (placement) ? placement.permanencyplanid : null;
    const childActorId = (placement) ? placement.intakeservicerequestactorid : null;
    return this.getadoptiondata1();
    // if (this.childId) {
    //   console.log(this.daNumber, this.caseuid);
    //   return this.getTprRecommendationList();
    // } else {
    //   const daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    //   const caseuid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    //   const url = '../permanency-plan';
    //   console.log(this._router.url);
    //   const url2 = `/pages/case-worker/${caseuid}/${daNumber}/dsds-action/sc-permanency-plan`;
    //   // this._router.navigate([url2]);
    // }
  }

  private getTprRecommendationList() {
    const plan = this._dataStoreService.getData('permanencyPlan');
    const daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    const caseuid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    let childActorId;
    if (plan) {
      childActorId = plan.intakeservicerequestactorid;
  }
    return  this._commonHttpService
    .getSingle(
        new PaginationRequest({
            where: {
                intakeserviceid: caseuid,
                intakeservicerequestactorid: this.childId
            },
            method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.TprRecommendationList + '?filter'
    );
  }

  getAdoptionData() {
    const user = this._authService.getCurrentUser();
    if (user.role.name === 'apcs') {
      const transkey = this._session.getItem('transkey');
      const transid = this._session.getItem('transid');
      this._placementAdoptionService.getPermanencyPlanId(1, 100, transkey, transid).subscribe(data => {
        const permanencyPlanId = (data && data.length) ? data[0].permanencyplanid : null;
        this._placementAdoptionService.getPlacementConfig(1, 100, permanencyPlanId).subscribe(
          res => {
            if (res.length && res[0] && res[0].placements && res[0].placements.length) {
              console.log('Config From API', res);
              const placement = res[0];
              this._dataStoreService.setData(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID, placement['adoptionplanningid']);
              this._dataStoreService.setData(CASE_STORE_CONSTANTS.TPR_RECOMMENDATION_ID, placement['tprrecommendationid']);
              this._dataStoreService.setData(CASE_STORE_CONSTANTS.ADOPTION_AGREEMENT_ID, placement['adoptionagreementid']);
              this._dataStoreService.setData(CASE_STORE_CONSTANTS.PERMANENCY_PLAN_ID, placement['permanencyplanid']);
            }
          }
        );
      });
    }
  }
  getadoptiondata1() {
    const transkey = this._session.getItem('transkey');
    const transid = this._session.getItem('transid');
    if (transid && transkey) {
      return this._placementAdoptionService.getPermanencyPlanId(1, 100, transkey, transid).concatMap((data) => {
        const permanencyPlanId = (data && data.length) ? data[0].permanencyplanid : null;
        return this._placementAdoptionService.getPlacementConfig(1, 100, permanencyPlanId).concatMap(res=>{
          if (res.length && res[0] && res[0].placements && res[0].placements.length) {
            console.log('Config From API', res);
            const placement = res[0];
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.ADOPTION_PLANNING_ID, placement['adoptionplanningid']);
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.TPR_RECOMMENDATION_ID, placement['tprrecommendationid']);
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.ADOPTION_AGREEMENT_ID, placement['adoptionagreementid']);
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.PERMANENCY_PLAN_ID, placement['permanencyplanid']);
          }
          return Observable.of(true);
        });
      });
    } else {
      return Observable.of(null);
    }
  }
}
