import { Injectable } from '@angular/core';
import { CommonHttpService, DataStoreService } from '../../../../../@core/services';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PlacementAdoptionService {
  public storeDataPatched$ = new Subject<any>();
  placementAdoptionData = {};
  AdoptionPlanningTab = '';
  AdoptionPlanningSaveState = false;
  adoptionplanning: any;
  constructor(
    private _commonHttp: CommonHttpService,
    private _dataStoreService: DataStoreService,
  ) { }

  setAdoptionPlanning(data) {
    this.adoptionplanning = data;
  }

  getAdoptionPlanning() {
    return Object.assign({}, this.adoptionplanning);
  }

  getplacementAdoptionData(dataName) {
    return (this.placementAdoptionData && this.placementAdoptionData[dataName]) ? this.placementAdoptionData[dataName] : null;
  }

  setplacementAdoptionData(dataName, dataValue) {
    this.placementAdoptionData[dataName] = (dataValue) ? dataValue : null;
  }

  setAdoptionPlanningData(url, saveState) {
    this.AdoptionPlanningTab = url;
    this.AdoptionPlanningSaveState = saveState;
  }

  setPlacementsDetails() {
    const data = [
      {
          adoptionplanningid: '1d8a3693-41d1-4ca2-aabb-5f2195b68f5d',
          tprrecommendationid: '44ece3ba-b885-41ae-9b8a-0bddca38e590',
          adoptionagreementid: null,
          intakeservicerequestactorid: '9f0fc6c9-495a-492f-b2f4-a03f18b32020'
      }

  ];

  }

  broadStoreDataPatched(data) {

    this.storeDataPatched$.next(data);
  }

  getPlacementConfig(pageNumber, limit, permanencyplanid) {
    console.log('Setting Store Variables onReload');
    return  this._commonHttp
      .getArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: limit,
          method: 'get',
          where: {
            permanencyplanid: permanencyplanid
          },
        }),
        'permanencyplan/getpermanencyplacement?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      ).map((res) => {
        return res;
    })
    .share();

  }

  getPermanencyPlanId(pageNumber, limit, transkey, transid) {
    console.log('Setting Store Variables onReload');
    return  this._commonHttp
      .getArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: limit,
          method: 'get',
          where: {
            transkey: transkey  , transid: transid
          },
        }),
        'permanencyplan/getpermanencyplacement?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      ).map((res) => {
        return res;
    })
    .share();

  }

   getRecommendedList(id, permanencyPlanId) {
    return this._commonHttp
        .getSingle(
            new PaginationRequest({
                where: {
                    servicecaseid: id,
                permanencyplanid: (permanencyPlanId) ? permanencyPlanId : this.adoptionplanning.permanencyplanid
                },
                method: 'get'
            }),
        'tprrecommendation/gettprrecommendation' + '?filter'
        ).share();
  }

   getAdoptionChecklist(adoptionplanningid) {
    return this._commonHttp
        .getArrayList(
            new PaginationRequest({
                where: { adoptionplanningid: adoptionplanningid ? adoptionplanningid : null },
                method: 'get'
            }),
            'adoptionchecklist/getadoptionchecklist?filter'
        )
        .share();
  }

  getAgreementListing(adoptionplanningid) {


    return this._commonHttp
      .getSingle(
        new PaginationRequest({
          where: {
            adoptionplanningid: adoptionplanningid

          },
          method: 'get',
          page: 1,
          limit: 10
        }),
        'adoptionagreement/list' + '?filter'
      ).share();

  }

  getReasonTypeDropDown(referencetypeid, teamtypekey) {

    return this._commonHttp
        .getArrayList(
            {
                where: { referencetypeid: referencetypeid, teamtypekey: teamtypekey },
                method: 'get'
            },
            'referencetype/gettypes' + '?filter'
        ).share();
  }

  getLegalCustody(personId) {
  return this._commonHttp
        .getArrayList({ method: 'get', where: { personid: personId } },
        'legalcustody/getlegalcustody' + '?filter')
        .share();
  }

  getTPRList(servicecaseid) {
    return this._commonHttp
        .getArrayList(
            new PaginationRequest({
                where: {
                    servicecaseid: servicecaseid ? servicecaseid : null
                },
                method: 'get'
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.TPRDetail + '?filter'
        )
        .share();
}

getEffortListing(permanencyplanid) {
  return this._commonHttp
      .getSingle(
          new PaginationRequest({
              where: {
                   permanencyplanid: permanencyplanid
              },
              method: 'get',
              page : 1,
              limit: 10
          }),
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEffortList + '?filter'
      )
      .share();
}

getNarrative(permanencyplanid) {
  return this._commonHttp
      .getSingle(
          new PaginationRequest({
              where: {
                  permanencyplanid: permanencyplanid
              },
              method: 'get'
          }),
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.AdoptionEffortList + '?filter'
      )
      .share();
}

  createadoptioncase(reqObj) {
    // const reqObj = {
    //   'adoptionplanningid': 'a44ef35b-6d83-4d02-a46c-c5f45e80cfa7',
    //   'servicecaseid': '078cf37d-1f38-4129-984b-1fba4f44fc18'
    // };
    return this._commonHttp.create(reqObj, 'adoptioncase/createadoptioncase');
  }

  setTabAccess(accessStatus) {
    this.setplacementAdoptionData('isRecommended', accessStatus);
  }

  getBreaklink(adoptionplanningid) {
    return this._commonHttp
        .getArrayList({ method: 'get', where: {
           adoptionplanningid: adoptionplanningid
          }}, 'adoptionbreakthelink/getadoptionbreakthelink?filter').
          share();
        // .subscribe(res => {
        //     if (res && res.length) {
        //         res.map((item) => {
        //           if(item && item.getadoptionbreakthelink) {
        //            item.getadoptionbreakthelink.map((breaklink) => {

        //           })
        //         }
        //       })
        //         }
        //       });
            }

   }
