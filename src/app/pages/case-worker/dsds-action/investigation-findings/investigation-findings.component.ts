import { HttpHeaders } from '@angular/common/http';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { ActivatedRoute, Router } from '@angular/router';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { config } from '../../../../../environments/config';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { CommonHttpService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService } from '../../../../@core/services/auth.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { AppConfig } from '../../../../app.config';
import { RoutingUser } from '../../../cjams-dashboard/_entities/dashBoard-datamodel';
import { IntakeUtils } from '../../../_utils/intake-utils.service';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { DSDSActionSummary } from '../../_entities/caseworker.data.model';
import { DispositionAddModal } from '../disposition/_entities/disposition.data.models';
import { Allegedperson, CheckList, InvestigationComar, InvestigationFinding,
    InvestigationFindings, InvestigationFindingType, InvolvedPerson, SubmitForReview } from './_entities/investigation-finding-data.models';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { AppConstants } from '../../../../@core/common/constants';
import { forkJoin } from 'rxjs/observable/forkJoin';



declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'investigation-findings',
    templateUrl: './investigation-findings.component.html',
    styleUrls: ['./investigation-findings.component.scss']
})
export class InvestigationFindingsComponent implements OnInit, AfterViewChecked, OnDestroy {
    investigationAllegedPerson$ = new Subject<Allegedperson[]>();
    investigationComar$ = new Subject<InvestigationComar>();
    paginationInfo: PaginationInfo = new PaginationInfo();
    investigationFindingDropDown: InvestigationFindingType[];
    id: string;
    notification: string;
    approvalStatus: string;
    showExpungement: boolean;
    isAppealWorker: boolean;
    roleId: AppUser;
    alertMessage: string;
    ReviewStatus: any;
    maltreatmentData: any;
    maltreatments: any;
    investigationFindingForm: FormGroup;
    appealDelayForm: FormGroup;
    investigationFindingFormGroup: FormGroup;
    appealFormGroup: FormGroup;
    reviewCheckListForm: FormGroup;
    investigationfindingid: any;
    investigationType: InvestigationFindings[];
    submitForReview = new SubmitForReview();
    investigationFinding: InvestigationFinding = new InvestigationFinding();
    dsdsActionsSummary = new DSDSActionSummary();
    investigation: InvestigationFinding[];
    allegedperson: InvestigationFinding[];
    recognizing = false;
    speechRecogninitionOn: boolean;
    speechData: string;
    submitBtn = false;
    selectedSupervisor: any;
    supervisorsList: RoutingUser[];
    findingsList: Allegedperson[] = [];
    personInfo: InvolvedPerson[] = [];
    viewMaltreatementKey: string;
    daNumber: string;
    findingAssesors = ([] = []);
    reviewCheckList: CheckList[] = [];
    dispositionFormGroup: FormGroup;
    expungementForm: FormGroup;
    expungmentMessage: string;
    isExpungementExist: boolean;
    statusDropdownItems$: Observable<DropdownModel[]>;
    dispositionDropdownItems$: Observable<DropdownModel[]>;
    reasonDropdownItems$: Observable<DropdownModel[]>;
    private daType: string;
    private dispositionDropdownItems: DropdownModel[];
    isMandatory: any;
    showConferenceType: number;
    conferenceTypeTitle: string;
    isFinalize: boolean;
    appealData: any;
    investigationAllegationList: any;
    appealInvestigation: any;
    isView: boolean;
    initialFinding: string;
    intakeserviceid: string;
    isNarrativeSaved: boolean;
    appealFindingKey: any;
    isUnsubstantiated: boolean;
    showCaseTypeForms: boolean;
    maltreatorsAppeal: any;
    juristictionList$: Observable<any[]>;
    hearingReasonList: any[] = [];
    judiLocationList: any[] = [];
    judiAddressList: any[] = [];
    uploadedFile: any = [];
    expert_assessmnts_attachment = [];
    med_assessmnts_attachment  = [];
    law_enforcement_attachment = [];
    token: AppUser;
    appealDocument: any;
    toDeleteAttachment: any;
    enableoah: boolean;
    enablecc: boolean;
    enablecsa: boolean;
    enablecoa: boolean;
    enablesc: boolean;
    showFinalize: boolean;
    reasonDropDown: any;
    allegedMaltreatorName: string;
    MaltreatmentType: string;
    // servicecaseid: string;
    isCaseworker: any;
    isSupervisor: any;
    investigationFindingType: string;
    juridictionList: any;
    courtdataValid = { sc: null, oah: null, cc: null, csa: null, coa: null };
    appealDocumentList: any[];
    showAppeal: boolean;
    reportflag: any;
    finalizeFormGroup: FormGroup;
    MaltreatmentsInfo: any[];
    allegationData: any;
    isClosed = false;
    isChildNotDead = true;

    noMAfoundMsg = true;
    pageLoaded = false;
    constructor(
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService,
        private _formBuilder: FormBuilder,
        private formBuilder: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute,
        private _router: Router,
        private _changeDetect: ChangeDetectorRef,
        private _dispositionAddService: GenericService<DispositionAddModal>,
        private _authService: AuthService,
        private _intakeUtils: IntakeUtils,
        private _uploadService: NgxfUploaderService,
        private _speechRecognitionService: SpeechRecognitionService,
        private speechRecognizer: SpeechRecognizerService,
        private _session: SessionStorageService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        const dataStore = this._dataStoreService.getData('dsdsActionsSummary');
        this.token = this._authService.getCurrentUser();
        this.daType = dataStore ? dataStore.da_typeid : '';
        this.isCaseworker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
        this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
        this.ReviewStatus = {};
        this.intakeserviceid = dataStore ? dataStore.intakeserviceid : '';
        if (this.id === undefined) {
            this.id = this.intakeserviceid;
        }

        this.reasonDropDown = ['Withdrawn', 'Settled', 'Postponed'];

    }



    ngOnInit() {
        this.reportflag = -1;
        this.showExpungement = false;
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId) {

           const roleName = ObjectUtils.getNestedObject(this.roleId, ['role', 'name']);

           if (roleName === AppConstants.ROLES.APPEAL_USER) {
               this.isAppealWorker = true;
           }
        }
        // this.servicecaseid = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        // this.servicecaseid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);

        this.formInitilize();
        this.investigationFindings();
        this.getJurisdictionList();
        this.getHearingReasonList();
        this.getJudiLocationList();
        this.getJudiAddressList();
        this.getJuridictionList();
        this.getAppealDocumentList();
        this.dispositionHistory();

        const store = this._dataStoreService.getCurrentStore();
        if (store['dsdsActionsSummary']) {
            this.dsdsActionsSummary = store['dsdsActionsSummary'];
            if (this.dsdsActionsSummary) {
                this.getFindingList();
                this.getCheckList();
                this.getMaltreatmentInfo();
                this.getMaltreatments();
            }
        } else {
            this.getActionSummary();
        }
        this.getInvolvedPerson();
        this.investigationAllegedPerson$.subscribe((findings) => {
            if (findings) {
            this.findingsList = [];
            this.findingsList = findings;
            this.submitReview(); } else {
                this.getFindingList();
            }
        });
        if (this.isSupervisor) {
            (<any>$('#expungementDetails')).click();
        }

        const da_status = this._session.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }
    }
    ngAfterViewChecked() {
        this._changeDetect.detectChanges();
    }

    filterReason(reasonText) {
        const result =  this.reasonDropDown.filter( item => {
           return item === reasonText.value_text;
        });
        // console.log('Reason Drop Down Check', result);
        return (result && result.length) ? true : false;
    }

    formInitilize() {
        this.initFindingsForm();
        this.reviewCheckListForm = this.formBuilder.group({
            initalfacetoface: false,
            safec: false,
            canf: false,
            mfira: false,
            personrole: false
        });
        this.expungementForm = this.formBuilder.group({
            expungementStatus: [null],
            expungementid: [null],
            investigationfindingid: [null],
            isunsubstansiated: [null],
            isindicated:  [null],
            isremovemaltreator:  [null],
            donotexpunge:  [null],
            manualexpunge:  [null],
            unsubstansiateddate:  [null],
            indicateddate:  [null],
            removemaltreatordate:  [null],
            resultoflawenforcement:  [null],
            investigationfinding: [null],
            appealfinding: [null],
            finalfinding: [null],
            activeflag: [1],
            investigationnarrative: [null],
            intakeserviceid : [null],
            maltreatmentid : [null],
            intakeservicerequestactorid: [null]
        });
        this.expungementForm.patchValue({intakeserviceid: this.id});
        this.dispositionFormGroup = this.formBuilder.group({
            statusid: [''],
            dispositionid: ['', Validators.required],
            closingcodetypekey: [null],
            reviewcomments: [''],
            supervisorid: [null]
        });
        this.initAppealForm();
    }
    private initAppealForm() {

        this.appealDelayForm = this.formBuilder.group({
            delay_reason: [null]
        });
        this.appealFormGroup = this.formBuilder.group({
            scicesentdate: [null],
            scconfheldflag: [null],
            scdecisiontypekey: [null],
            scconferencedetail: [null],
            scconferencedate: [null],
            scappealed: [null],
            type: [null],
            sentdate: [null],
            oahearingdatesetflag: [null],
            hearingdate: [null],
            oanorhearingreason: [null],
            hearingdecision: [null],
            hearingdecisiondate: [null],
            details: [null],
            casenumber: [null],
            appealedflag: [null],
            stayrequestedflag: [null],
            staygrantedflag: [null],
            hearingdecisiontypekey: [null],
            courtdecisionflag: [null],
            compileddate: [null],
            overridedate: [null],
            overrideby: [this._authService.getCurrentUser().user.userprofile.firstname + ' ' + this._authService.getCurrentUser().user.userprofile.lastname],
            overridefindingtypekey: [null],
            overridecomments: [null],
            overrideapprflag: [null],
            scisappealed: [null],
            scappealedby: [null],
            scappealeddate: [null],
            oaldssname: [null],
            oaappellentatrny: [null],
            oalocaldept: [null],
            oarunningmotion: [null],
            ccldssname: [null],
            ccappellentatrny: [null],
            coaldssname: [null],
            coaappellentatrny: [null],
            csaldssname: [null],
            csaappellentatrny: [null],
            csanotifiedtodirector: [null],
            csacertiorari: [null],
            oaicesentdate: [null],
            oahearingdate: [null],
            clientroleid: [null],
            oahearingdecision: [null],
            oahearingdecisiondate: [null],
            oadetails: [null],
            ccstayrequestedflag: [null],
            ccstaygrantedflag: [null],
            ccappealedflag: [null],
            cchearingdecisiontypekey: [null],
            cchearingdecisiondate: [null],
            ccdetails: [null],
            csastayrequestedflag: [null],
            csastaygrantedflag: [null],
            csaappealedflag: [null],
            csahearingdecisiontypekey: [null],
            csahearingdecisiondate: [null],
            csadetails: [null],
            oaappealedflag: [null],
            oacasenumber: [null],
            oacasenumberlast8digit: [null],
            cccasenumber: [null],
            cccasenumberlast8digit: [null],
            csacasenumber: [null],
            csacasenumberlast8digit: [null],
            coajuridiction: [null],
            oajuridiction: [null],
            ccjuridiction: [null],
            csajuridiction: [null],
            cccourtdecisionflag: [null],
            cccompileddate: [null],
            csacourtdecisionflag: [null],
            coastayreqflag: [null],
            coastaygrantedflag: [null],
            coaappealedflag: [null],
            coahearingdecisiontypekey: [null],
            coahearingdecisiondate: [null],
            coadetails: [null],
            coacase: [null],
            coacourtdecisionflag: [null],
            coacompileddate: [null],
            coacasenumber: [null],
            coacasenumberlast8digit: [null],
            isvictim: [null],
            csacompileddate: [null],
            csaarguementheld: [null],
            csaarguementnotheldreason: [null],
            ccwhoappealed: [null],
            csawhoappealed: [null],
            ccnotifiedtodirector: [null],
            ccldssnotifieddate: [null],
            csaldssnotifieddate: [null],
            cccicuitcourtkey: [null],
            scsummarymailed: [null],
            scappealedsetdate: [null],
            scisappealformsent: [null],
            cchearingheld: [null],
            cchearingreason: [null],
            cclocationofhearing: [null],
            coacertioraristatus: [null],
            coacertgranteddate: [null],
            coacertdenieddate: [null],
            oahearingheld: [null],
            oahearingheldreason: [null],
            cchearingheldreason: [null],
            oalocationofhearing: [null],
            oasummarydecisionfiledflag: [null],
            oasummarydecisionfileddate: [null],
            csalocationofhearing: [null],
            coalocationofhearing: [null],
            oamodificationsmade: [null],
            oahearingnarrative: [null],
            oatranslator: [null],
            oarunningmotiondate: [null],
            oamaltreatorunnamedflag: [null],
            oacompiledwithoah: [null]
            // oamaltreatorname: [null]
        });
        this.finalizeFormGroup = this.formBuilder.group({
            scicesentdate: [null],
            scconfheldflag: [null],
            scdecisiontypekey: [null],
            scconferencedetail: [null],
            scconferencedate: [null],
            scappealed: [null],
            type: [null],
            sentdate: [null],
            oahearingdatesetflag: [null],
            hearingdate: [null],
            oanorhearingreason: [null],
            hearingdecision: [null],
            hearingdecisiondate: [null],
            details: [null],
            casenumber: [null],
            appealedflag: [null],
            stayrequestedflag: [null],
            staygrantedflag: [null],
            hearingdecisiontypekey: [null],
            courtdecisionflag: [null],
            compileddate: [null],
            overridedate: [null],
            overrideby: [this._authService.getCurrentUser().user.userprofile.firstname + ' ' + this._authService.getCurrentUser().user.userprofile.lastname],
            overridefindingtypekey: [null],
            overridecomments: [null],
            overrideapprflag: [null],
            scisappealed: [null],
            scappealedby: [null],
            scappealeddate: [null],
            oaldssname: [null],
            oaappellentatrny: [null],
            oalocaldept: [null],
            oarunningmotion: [null],
            ccldssname: [null],
            ccappellentatrny: [null],
            coaldssname: [null],
            coaappellentatrny: [null],
            csaldssname: [null],
            csaappellentatrny: [null],
            csanotifiedtodirector: [null],
            csacertiorari: [null],
            oaicesentdate: [null],
            oahearingdate: [null],
            clientroleid: [null],
            oahearingdecision: [null],
            oahearingdecisiondate: [null],
            oadetails: [null],
            ccstayrequestedflag: [null],
            ccstaygrantedflag: [null],
            ccappealedflag: [null],
            cchearingdecisiontypekey: [null],
            cchearingdecisiondate: [null],
            ccdetails: [null],
            csastayrequestedflag: [null],
            csastaygrantedflag: [null],
            csaappealedflag: [null],
            csahearingdecisiontypekey: [null],
            csahearingdecisiondate: [null],
            csadetails: [null],
            oaappealedflag: [null],
            oacasenumber: [null],
            oacasenumberlast8digit: [null],
            cccasenumber: [null],
            cccasenumberlast8digit: [null],
            csacasenumber: [null],
            csacasenumberlast8digit: [null],
            coajuridiction: [null],
            oajuridiction: [null],
            ccjuridiction: [null],
            csajuridiction: [null],
            cccourtdecisionflag: [null],
            cccompileddate: [null],
            csacourtdecisionflag: [null],
            coastayreqflag: [null],
            coastaygrantedflag: [null],
            coaappealedflag: [null],
            coahearingdecisiontypekey: [null],
            coahearingdecisiondate: [null],
            coadetails: [null],
            coacase: [null],
            coacourtdecisionflag: [null],
            coacompileddate: [null],
            coacasenumber: [null],
            coacasenumberlast8digit: [null],
            isvictim: [null],
            csacompileddate: [null],
            csaarguementheld: [null],
            csaarguementnotheldreason: [null],
            ccwhoappealed: [null],
            csawhoappealed: [null],
            ccnotifiedtodirector: [null],
            ccldssnotifieddate: [null],
            csaldssnotifieddate: [null],
            cccicuitcourtkey: [null],
            scsummarymailed: [null],
            scappealedsetdate: [null],
            scisappealformsent: [null],
            cchearingheld: [null],
            cchearingreason: [null],
            cclocationofhearing: [null],
            coacertioraristatus: [null],
            coacertgranteddate: [null],
            coacertdenieddate: [null],
            oahearingheld: [null],
            oahearingheldreason: [null],
            cchearingheldreason: [null],
            oalocationofhearing: [null],
            oasummarydecisionfiledflag: [null],
            oasummarydecisionfileddate: [null],
            csalocationofhearing: [null],
            coalocationofhearing: [null],
            oamodificationsmade: [null],
            oahearingnarrative: [null],
            oatranslator: [null],
            oarunningmotiondate: [null],
            oamaltreatorunnamedflag: [null],
            oacompiledwithoah: [null]
            // oamaltreatorname: [null]
        });
    }

    private initFindingsForm() {
        this.investigationFindingForm = this.formBuilder.group({
            jointinvestigation: false,
            summary: [''],
            remarks: [''],
            investigationid: [''],
            allegedperson: this._formBuilder.array([])
        });
    }


    setFormValues() {
        const control = new FormArray([]);
        this.investigationFindingForm.removeControl('allegedperson');
        this.clearFormArray(control);
        this.investigation.forEach((x, index) => {
            const maltreators = x.maltreators ? x.maltreators : []; // @TM: identify all maltreators
            if (maltreators.length > 0) {
                const y = x;
                maltreators.forEach( mal => { // @TM: create a form for each maltreator
                                // @TM: replace the maltreators in the investigation finding with the current maltreator
                    y.maltreators = [];
                    y.maltreators.push(mal);
                     // @TM: create form for current maltreator
                });
                control.push(this.buildInvestigationForm(y, index));
            } else {
                control.push(this.buildInvestigationForm(x, index));
            }
        });
        this.investigationFindingForm.addControl('allegedperson', control);
    }

    clearFormArray = (formArray: FormArray) => {
        formArray = this.formBuilder.array([]);
    }

    private buildInvestigationForm(x, index): FormGroup {
        const expungmentStatusObj = ( Array.isArray(x.maltreators) && x.maltreators && x.maltreators.length) ?  x.maltreators.find(data => data.activeflag === 1) : null;
        const investFind = this._formBuilder.group({
            personid: x.personid ? x.personid : '',
            investigationallegationid: x.investigationallegationid ? x.investigationallegationid : null,
            personname: x.personname ? x.personname : '',
            insertedon: x.insertedon,
            name: x.name ? x.name : '',
            dispositionnarrative: x.dispositionnarrative ? x.dispositionnarrative  : null,
            relationship: x.relationship ? x.relationship : '',
             maltreatmentkey: ( x.findings && Array.isArray(x.findings)) ? x.findings.map((item) => item.investigationfindingtypekey) : '',
             maltreatmentkey1: ( x.findings && Array.isArray(x.findings)) ? x.findings.map((item) => item.investigationfindingtypekey) : '',
             maltreatmentkey2: '',
             maltreatmentkey3: ( x.findings && Array.isArray(x.findings)) ? x.findings.map((item) => item.finalfinding) : '',
             investigationfindingtypekey: '',
            reporteddate: x.reporteddate ? x.reporteddate  : null,
            intakeservicerequestactorid: x.intakeservicerequestactorid ? x.intakeservicerequestactorid  : null,
            investigationfindingid: ( x.findings && Array.isArray(x.findings)) ? x.findings.map((item) => item.investigationfindingid) : '',
            allegation: x.allegation ? x.allegation : '',
            intentionalinjurydesc: '',
            findingcomments: '',
            isharm: '',
            isharmsubstantial: '',
            harmdesc: '',
            investigationfindings: '',
            ischildfatality: x.ischildfatality ? x.ischildfatality : 0,
            victim_explanation: x.victim_explanation ? x.victim_explanation : '',
            sibling_explanation: x.sibling_explanation ? x.sibling_explanation : '',
            guardian_explanation: x.guardian_explanation ? x.guardian_explanation : '',
            maltreator_explanation: x.maltreator_explanation ? x.maltreator_explanation : '',
            med_assessmnts: x.med_assessmnts ? x.med_assessmnts : '',
            expert_assessmnts: x.expert_assessmnts ? x.expert_assessmnts : '',
            law_enforcement_inv: x.law_enforcement_inv ? x.law_enforcement_inv : '',
            collateral_interviews: x.collateral_interviews ? x.collateral_interviews : '',
            criminal_history_inv: x.criminal_history_inv ? x.criminal_history_inv : '',
            home_conditions: x.home_conditions ? x.home_conditions : '',
            sextrafficking: x.sextrafficking != null ? x.sextrafficking : '',
            isAppealDone: false,
            isFinalizeDone: false,
            maltreatmentid: x.maltreatmentid ? x.maltreatmentid : null,
            expungementflag: ( expungmentStatusObj && expungmentStatusObj.expungementflag) ? expungmentStatusObj.expungementflag  : '',
            isunsubstansiated: (Array.isArray(x.expungements) && x.expungements && x.expungements.length) ? x.expungements.map((item) => item.isunsubstansiated) : '',
            isindicated: ( Array.isArray(x.expungements) && x.expungements && x.expungements.length) ? x.expungements.map((item) => item.isindicated) : '',
            isremovemaltreator: ( Array.isArray(x.expungements) && x.expungements && x.expungements.length) ? x.expungements.map((item) => item.isremovemaltreator) : '',
            expert_assessmnts_attachment: ( Array.isArray(x.expert_assessmnts_attachment)) ? [x.expert_assessmnts_attachment] : [[]],
            med_assessmnts_attachment: ( Array.isArray(x.med_assessmnts_attachment)) ? [x.med_assessmnts_attachment] : [[]],
            law_enforcement_attachment: ( Array.isArray(x.law_enforcement_attachment)) ? [x.law_enforcement_attachment] : [[]],
        });

        const investFindType = x.findings ? x.findings.map((item) => item.investigationfindingtypekey) : null;
        investFind.controls['investigationfindingtypekey'].patchValue(investFindType);
        const intentionalInjuryDesc = x.findings ? x.findings.map((item) => item.intentionalinjurydesc) : null;
        investFind.controls['intentionalinjurydesc'].patchValue(intentionalInjuryDesc);
        const findingComments = x.findings ? x.findings.map((item) => item.findingcomments) : null;
        investFind.controls['findingcomments'].patchValue(findingComments);
        const isharm = x.findings ? x.findings.map((item) => item.isharm) : null;
        investFind.controls['isharm'].patchValue(isharm ? true : false);
        const isHarmSubstantial = x.findings ? x.findings.map((item) => item.isharmsubstantial) : null;

        investFind.controls['isharmsubstantial'].patchValue(isHarmSubstantial ? true : false);
        const harmDesc = x.findings ? x.findings.map((item) => item.harmdesc) : null;
        investFind.controls['harmdesc'].patchValue(harmDesc);
        const relationShip = x.maltreators ? x.maltreators.map((item) => item.relationship) : null;
        investFind.controls['relationship'].patchValue(relationShip);
        const displayname = x.maltreators ? x.maltreators.map((item) => item.displayname) : null;
        investFind.controls['allegation'].patchValue(displayname);
        const intakeservicerequestactorid = x.maltreators ? x.maltreators.map((item) => item.intakeservicerequestactorid) : null;
        investFind.controls['intakeservicerequestactorid'].patchValue(intakeservicerequestactorid);
        const maltreatmentid = x.maltreatmentid ? x.maltreatmentid : null;
        investFind.controls['maltreatmentid'].patchValue(maltreatmentid);
        const alleagation = this.investigationAllegationList.find(al => al.maltreatmentid === maltreatmentid);

        let appealData = null;
        if (alleagation) {
            const maltreators = alleagation.maltreators ? alleagation.maltreators : [];
            appealData = maltreators.find(mal => intakeservicerequestactorid.includes(mal.intakeservicerequestactorid));
            if (appealData) {
                const hasHeader = !!appealData.scdecisiontypekey;
                const hasOverride = !!appealData.overridefindingtypekey;
                investFind.controls['isAppealDone'].patchValue(hasHeader);
                investFind.controls['isFinalizeDone'].patchValue(hasOverride);
                console.log('Appeal Data', appealData);
            }
        }
        const investigationFindings = x.findings ? x.findings : null;
        investFind.controls['investigationfindings'].patchValue(investigationFindings);
        const findings = investFind.getRawValue().maltreatmentkey ? investFind.getRawValue().maltreatmentkey : null;
        const maltreator = Array.isArray(x.maltreators) ? x.maltreators.find(data => data.activeflag === 1) : null;
        let courtdecision = findings;
        let appealcourtdecision = null;
        investFind.patchValue({
            dispositionnarrative: maltreator.dispositionnarrative
        });
        investFind.patchValue({
            maltreatmentkey1: courtdecision
        });
        if (maltreator) {
            courtdecision = (maltreator.scdecisiontypekey) ? maltreator.scdecisiontypekey : courtdecision;
            courtdecision = (maltreator.oahearingdecision) ? maltreator.oahearingdecision : courtdecision;
            courtdecision = (maltreator.cchearingdecisiontypekey) ? maltreator.cchearingdecisiontypekey : courtdecision;
            courtdecision = (maltreator.csahearingdecisiontypekey) ? maltreator.csahearingdecisiontypekey : courtdecision;
            courtdecision = (maltreator.coahearingdecisiontypekey) ? maltreator.coahearingdecisiontypekey : courtdecision;
            courtdecision = (maltreator.overridefindingtypekey) ? maltreator.overridefindingtypekey : courtdecision;
        }

        if (maltreator) {
            appealcourtdecision = (maltreator.scdecisiontypekey) ? maltreator.scdecisiontypekey : null;
            appealcourtdecision = (maltreator.oahearingdecision) ? maltreator.oahearingdecision : appealcourtdecision;
            appealcourtdecision = (maltreator.cchearingdecisiontypekey) ? maltreator.cchearingdecisiontypekey : appealcourtdecision;
            appealcourtdecision = (maltreator.csahearingdecisiontypekey) ? maltreator.csahearingdecisiontypekey : appealcourtdecision;
            appealcourtdecision = (maltreator.coahearingdecisiontypekey) ? maltreator.coahearingdecisiontypekey : appealcourtdecision;
            appealcourtdecision = (maltreator.overridefindingtypekey) ? maltreator.overridefindingtypekey : appealcourtdecision;
        }

        investFind.patchValue({
            maltreatmentkey: courtdecision,
            maltreatmentkey2: appealcourtdecision
        });
        if (findings) {
             investFind.get('maltreatmentkey').disable();
        }
        if (investFind.controls['maltreatmentkey3'].value) {
            investFind.get('maltreatmentkey3').disable();
        }
        if (investFind.controls['investigationfindingid'].value) {
            const investigationfindingid = investFind.controls['investigationfindingid'].value;
            this.setReviewStatus(investigationfindingid);
        }
        return investFind;
    }

    investigationFindings() {
        this._commonHttpService.getSingle({order: 'displayorder', method: 'get'}, 'investigationfindingtype?filter').subscribe((res) => {
            this.investigationFindingDropDown = res;
        });
    }
    getFindingList() {
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: this.paginationInfo.pageNumber,
                    limit: this.paginationInfo.pageSize,
                    where: {
                        investigationid: this.dsdsActionsSummary.da_investigationid
                    },
                    method: 'get'
                }),
                'Investigationallegations/getmaltreatmentfinding?filter'
            )
            .subscribe((res) => {
                if (res) {
                    this.noMAfoundMsg = false;
                    this.pageLoaded = true;
                    this.investigation = res;
                    if (this.investigation.length > 0) {
                        this.submitBtn = true;
                    }
                    this.investigationAllegationList = res;
                    this.setFormValues();
                    if (this.investigation && Array.isArray(this.investigation) && this.investigation[0]) {
                        this.investigationFindingForm.patchValue({
                            jointinvestigation: this.investigation[0].jointinvestigation,
                            summary: this.investigation[0].investigationsummary,
                            remarks: this.investigation[0].notes

                        });
                        const narrative = this.investigation[0].investigationsummary;
                        this.isNarrativeSaved = (narrative && narrative.length > 0) ? true : false;
                    }
                } else {
                    this.noMAfoundMsg = true;
                    this.pageLoaded = true;
                }
            });
    }

    openAppeanModal() {
        this.appealDelayForm.reset();
        (<any>$('#investigation-appeal')).modal('show');
        (<any>$('#appeals-tab')).click();
    }

    loadFinalizeData(intakeservicerequestactorid: any = null, index: number = -1, finalized: boolean, appealed: boolean, maltreatmentid: string, investigationFind) {
        // this.openAppealDelayPopup(investigationFind.getRawValue());
        console.log('Investigation Findings Data', investigationFind.getRawValue());
        const expungementFlag = investigationFind.getRawValue().expungementflag;
        if (expungementFlag) {
            this.alertMessage = 'Cannot Finalize as Expungement is done for this record.';
            (<any>$('#alert-modal')).modal('show');
             return;
        }
        this.allegedMaltreatorName = investigationFind.getRawValue().allegation;
        this.MaltreatmentType = investigationFind.getRawValue().name;
        this.investigationFindingType = investigationFind.getRawValue().maltreatmentkey;
        this.showFinalize = false;
        // this.isFinalize = false;
        this.appealFormGroup.reset();
        this.appealInvestigation = this.investigationAllegationList.find(al => al.maltreatmentid === maltreatmentid);
        this.appealFindingKey = (this.appealInvestigation && Array.isArray(this.appealInvestigation.findings)) ? this.appealInvestigation.findings[0].investigationfindingtypekey : null;
        this.maltreatorsAppeal = this.appealInvestigation.maltreators ? this.appealInvestigation.maltreators : null;
        this.maltreatorsAppeal = this.maltreatorsAppeal.map(appeal => {
            appeal.iscoapresent = (appeal.coacompileddate) ? true : false;
            appeal.iscsapresent = (appeal.csacompileddate && ( !appeal.coacompileddate)) ? true : false;
            appeal.isccpresent = (appeal.cccompileddate && ( !appeal.csacompileddate || !appeal.coacompileddate)) ? true : false;
            appeal.isoahpresent = (appeal.oaicesentdate && ( !appeal.cccompileddate || !appeal.csacompileddate || !appeal.coacompileddate)) ? true : false;
            appeal.isscpresent = (appeal.scicesentdate && (!appeal.oaicesentdate || !appeal.cccompileddate || !appeal.csacompileddate || !appeal.coacompileddate)) ? true : false;
            return appeal;
         });

         this.appealDocument = this.appealInvestigation.documentprop;
        this.appealData = this.maltreatorsAppeal.find(mal => (intakeservicerequestactorid.includes(mal.intakeservicerequestactorid) && mal.activeflag === 1));
        // this.appealData = null;
        // if(this.maltreatorsAppeal && this.maltreatorsAppeal.length) {
        // this.appealData = this.maltreatorsAppeal[this.maltreatorsAppeal.length-1];
        // }
        let courtdecision = '';
        let requestedDate = new Date();
        if (this.appealData) {
            courtdecision = (this.appealData.scdecisiontypekey) ? this.appealData.scdecisiontypekey : courtdecision;
            courtdecision = (this.appealData.oahearingdecision) ? this.appealData.oahearingdecision : courtdecision;
            courtdecision = (this.appealData.cchearingdecisiontypekey) ? this.appealData.cchearingdecisiontypekey : courtdecision;
            courtdecision = (this.appealData.csahearingdecisiontypekey) ? this.appealData.csahearingdecisiontypekey : courtdecision;
            courtdecision = (this.appealData.coahearingdecisiontypekey) ? this.appealData.coahearingdecisiontypekey : courtdecision;
            courtdecision = (this.appealData.overridefindingtypekey) ? this.appealData.overridefindingtypekey : courtdecision;

            requestedDate = (this.appealData.coacompileddate) ? this.appealData.coacompileddate : requestedDate;
            requestedDate = (this.appealData.csacompileddate && (!this.appealData.coacompileddate)) ? this.appealData.csacompileddate : requestedDate;
            requestedDate = (this.appealData.cccompileddate && (!this.appealData.csacompileddate || !this.appealData.coacompileddate)) ? this.appealData.cccompileddate : requestedDate;
            requestedDate = (this.appealData.oaicesentdate && (!this.appealData.cccompileddate || !this.appealData.csacompileddate || !this.appealData.coacompileddate)) ?
                this.appealData.oaicesentdate : requestedDate;
            requestedDate = (this.appealData.scicesentdate && (!this.appealData.oaicesentdate || !this.appealData.cccompileddate || !this.appealData.csacompileddate ||
                 !this.appealData.coacompileddate)) ? this.appealData.scicesentdate : requestedDate;
        }
        this.appealData.requestedDate = requestedDate;
        this.appealInvestigation.findings.forEach((item) => {
                    this.initialFinding = this.investigationFindingDropDown.find(i => i.investigationfindingtypekey === courtdecision).description;
                });
        // this.initialFinding = courtdecision;
        this.uploadedFile = this.appealInvestigation.documentprop ? this.appealInvestigation.documentprop : [];
        // this.patchAppealForm(this.appealData, finalized, appealed);
        if ( !this.appealData.iscoapresent &&
            !this.appealData.iscsapresent &&
            !this.appealData.isccpresent &&
            !this.appealData.isoahpresent &&
            !this.appealData.isscpresent
            ) {
                this._alertService.warn('Please Fill Appeal Data');
                return;
            }
        this.finalizeFormGroup.patchValue(this.appealData);
        this.finalizeFormGroup.enable();
        if (this.appealData.overrideapprflag) {
            this.finalizeFormGroup.disable();
        }
        this.validateAppealsFlow();
        (<any>$('#finalize-appeal')).modal('show');
    }

    maltreatmentValidation() {
        this.MaltreatmentsInfo = [];
        let isMaltreatmentMissing = false;
        // console.log('Maltreatment Data :',this.maltreatmentData);
        // console.log('Investigation Finding Data :',this.investigation);
        if (this.personInfo && this.personInfo.length) {
            const abusedChildList = this.personInfo.filter(person => {
                const roles = (Array.isArray(person.roles)) ? person.roles : [];
                const victim = roles.some(role => ['AV'].includes(role.intakeservicerequestpersontypekey));
                return victim;
            });
            let  allegedMaltreators = this.personInfo.filter(person => {
                const roles = (Array.isArray(person.roles)) ? person.roles : [];
                const maltreator = roles.some(role => ['AM'].includes(role.intakeservicerequestpersontypekey));
                return maltreator;
            });
            allegedMaltreators = allegedMaltreators && allegedMaltreators.length ? allegedMaltreators : [];
            if (abusedChildList) {
                abusedChildList.forEach(person => {
                    const maltreatmentArray = [];
                    let missedMaltreatment = 0;
                    if (this.maltreatmentData && this.maltreatmentData.length) {
                        this.maltreatmentData.forEach(maltreatment => {

                            allegedMaltreators.forEach(maltreator => {
                                const isMaltreatmentAdded = this.investigation && this.investigation.length ?
                                    this.investigation.find(investigation => investigation.name === maltreatment.name && investigation.personid === person.personid
                                        && (investigation.maltreators[0].personid === maltreator.personid)) : null;
                                const allegedMaltreatment = this.allegationData && this.allegationData.length ?
                                    this.allegationData.find(investigation => (investigation.investigationallegation && investigation.investigationallegation.length
                                        && (investigation.investigationallegation[0].maltreators[0].personid === maltreator.personid)
                                        ? investigation.investigationallegation[0].allegationname === maltreatment.name : false) && investigation.personid === person.personid) : null;
                                let isMaltreatmentNotApplicable = false;
                            let isMaltreatorSame = false;
                            if (allegedMaltreatment && allegedMaltreatment.investigationallegation && allegedMaltreatment.investigationallegation.length) {
                                allegedMaltreatment.investigationallegation.forEach(allg => {
                                   if (allg.maltreators && allg.maltreators.length &&  allg.maltreators[0].personid) {
                                    if (maltreator.personid ===  allg.maltreators[0].personid) {
                                        isMaltreatorSame = true;
                                        isMaltreatmentNotApplicable = false;
                                    }
                                   }
                                   if (allg.isnotapplicable) {
                                    isMaltreatmentNotApplicable = true;
                                   }
                               });
                            }
                            if (!isMaltreatmentAdded &&  !isMaltreatmentNotApplicable && !isMaltreatorSame) {
                                missedMaltreatment = missedMaltreatment + 1;
                                isMaltreatmentMissing = true;
                            }
                            const maltreatmentObj = { 'maltreatmentName': maltreatment.name, 'maltreator': maltreator.firstname + ' ' +
                            maltreator.lastname, 'isMaltreatmentAdded':  ( (isMaltreatmentAdded || isMaltreatmentNotApplicable ) && isMaltreatorSame) ? true : false };
                            maltreatmentArray.push(maltreatmentObj);
                           });

                         });
                    }
                    const personObj = { 'personId': person.personid , 'personName': person.firstname + ' ' +
                    person.lastname, 'maltreatments': maltreatmentArray, 'missedMaltreatments': missedMaltreatment};
                    this.MaltreatmentsInfo.push(personObj);
                });
            }
        }

        // console.log('Maltreatment Added Info ', this.MaltreatmentsInfo);
        // console.log('isMaltreatment Missing', isMaltreatmentMissing)
        if (isMaltreatmentMissing) {
            return false;
        } else {
            return true;
        }

    }

    getMaltreatments() {
        this._commonHttpService.getArrayList(
            {
                nolimit: true,
                method: 'get',
                where: {

                    investigationid: this.dsdsActionsSummary.da_investigationid
                }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.MaltreatmentInformation + '?filter'
        ).subscribe( data => {
             this.maltreatments = data;
        });
    }

    getMaltreatmentInfo() {
        this._commonHttpService.getArrayList(
            {
                nolimit: true,
                method: 'get',
                where: {
                    intakeservicereqtypeid: this.dsdsActionsSummary.da_typeid,
                    intakeservicereqsubtypeid: this.dsdsActionsSummary.da_subtypeid,
                    investigationid: this.dsdsActionsSummary.da_investigationid
                }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.GetMaltreatementTypeUrl + '?filter'
        ).subscribe( data => {
             this.maltreatmentData = data;
             this.getmaltreatmentData();
        });
    }

    getmaltreatmentData() {
        const sourceData = forkJoin(
            this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        servicerequestid: this.id
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmvaluesUrl + '?filter'
            ),
            this._commonHttpService
            .getArrayList(
                {
                    where: { investigationid: this.dsdsActionsSummary.da_investigationid },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.MaltreatmentInformation + '?filter'
            )
        ).subscribe(list => {
            if (list && list.length) {
             if (list[1]) {
                 this.allegationData = list[1];
             }
             if (list[0]) {
                const res = list[0];
                if (res) {
                    const sdm = res[0].getintakeservicerequestsdm ? res[0].getintakeservicerequestsdm[0] : null;
                    if (sdm) {
                        if (sdm.ismalpa_suspeciousdeath || sdm.ismalpa_nonaccident || sdm.ismalpa_injuryinconsistent ||
                            sdm.ismalpa_insjury || sdm.ismalpa_childtoxic || sdm.ismalpa_caregiver
                        ) { // this.sdmAllegation['phyabuse'] = true;
                        } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Physical Abuse');
                            if (index >= 0) {
                            this.maltreatmentData.splice(index, 1);
                             }
                            }
                        }
                        if (
                            sdm.ismalsa_sexualmolestation || sdm.ismalsa_sexualact || sdm.ismalsa_sexualexploitation || sdm.ismalsa_physicalindicators || sdm.ismalsa_sex_trafficking
                        ) { // this.sdmAllegation['sexabuse'] = true;
                        } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Sexual Abuse');
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                             }
                        }
                        if (sdm.ismenab_psycologicalability) {
                            // this.sdmAllegation['miabuse'] = true;
                        } else {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Mental Injury- Abuse');
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                                }
                        }
                        if (sdm.ismenng_psycologicalability) {
                           // this.sdmAllegation['mineglect'] = true;
                        } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Mental Injury- Neglect');
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                                }

                        }
                        if (
                            sdm.isnegmn_unreasonabledelay || sdm.isneguc_leftunsupervised || sdm.isneguc_leftaloneinappropriatecare || sdm.isneguc_leftalonewithoutsupport || sdm.isnegab_abandoned ||
                            sdm.isnegfp_cargiverintervene || sdm.isnegrh_treatmenthealthrisk || sdm.isneggn_inadequatesupervision || sdm.isneggn_inadequateclothing || sdm.isneggn_exposuretounsafe ||
                            sdm.isneggn_childdischarged || sdm.isneggn_inadequatefood || sdm.isneggn_signsordiagnosis || sdm.isneggn_suspiciousdeath
                        ) { // this.sdmAllegation['neglect'] = true;
                    } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Neglect');
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                                }
                        }
                    }
                }
             }
            }
        });
    }

    getSDM() {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        servicerequestid: this.id
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmvaluesUrl + '?filter'
            )
            .subscribe((res) => {
                if (res) {
                    const sdm = res[0].getintakeservicerequestsdm ? res[0].getintakeservicerequestsdm[0] : null;
                    if (sdm) {
                        if (sdm.ismalpa_suspeciousdeath || sdm.ismalpa_nonaccident || sdm.ismalpa_injuryinconsistent ||
                            sdm.ismalpa_insjury || sdm.ismalpa_childtoxic || sdm.ismalpa_caregiver
                        ) { // this.sdmAllegation['phyabuse'] = true;
                        } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Physical Abuse');
                            if (index >= 0) {
                            this.maltreatmentData.splice(index, 1);
                             }
                            }
                        }
                        if (
                            sdm.ismalsa_sexualmolestation || sdm.ismalsa_sexualact || sdm.ismalsa_sexualexploitation || sdm.ismalsa_physicalindicators || sdm.ismalsa_sex_trafficking
                        ) { // this.sdmAllegation['sexabuse'] = true;
                        } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Sexual Abuse');
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                             }
                        }
                        if (sdm.ismenab_psycologicalability) {
                            // this.sdmAllegation['miabuse'] = true;
                        } else {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Mental Injury- Abuse');
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                                }
                        }
                        if (sdm.ismenng_psycologicalability) {
                           // this.sdmAllegation['mineglect'] = true;
                        } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Mental Injury- Neglect');
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                                }

                        }
                        if (
                            sdm.isnegmn_unreasonabledelay || sdm.isneguc_leftunsupervised || sdm.isneguc_leftaloneinappropriatecare || sdm.isneguc_leftalonewithoutsupport || sdm.isnegab_abandoned ||
                            sdm.isnegfp_cargiverintervene || sdm.isnegrh_treatmenthealthrisk || sdm.isneggn_inadequatesupervision || sdm.isneggn_inadequateclothing || sdm.isneggn_exposuretounsafe ||
                            sdm.isneggn_childdischarged || sdm.isneggn_inadequatefood || sdm.isneggn_signsordiagnosis || sdm.isneggn_suspiciousdeath
                        ) { // this.sdmAllegation['neglect'] = true;
                    } else {
                            if (this.maltreatmentData && this.maltreatmentData.length) {
                            const index = this.maltreatmentData.findIndex(p => p.name === 'Neglect');
                            if (index >= 0) {
                                this.maltreatmentData.splice(index, 1);
                                 }
                                }
                        }
                    }
                }
            });
    }

    loadAppealData(intakeservicerequestactorid: any = null, index: number = -1, finalized: boolean, appealed: boolean, maltreatmentid: string, investigationFind) {
        // this.openAppealDelayPopup(investigationFind.getRawValue());
        const expungementFlag = investigationFind.getRawValue().expungementflag;
        if (expungementFlag) {
            this.alertMessage = 'Cannot Appeal as Expungement is done for this record.';
           (<any>$('#alert-modal')).modal('show');
            return;
        }
        console.log('Investigation Findings Data', investigationFind.getRawValue());
        this.allegedMaltreatorName = investigationFind.getRawValue().allegation;
        this.MaltreatmentType = investigationFind.getRawValue().name;
        this.investigationFindingType = investigationFind.getRawValue().maltreatmentkey;
        this.showFinalize = false;
        this.appealFormGroup.reset();
        this.appealInvestigation = this.investigationAllegationList.find(al => al.maltreatmentid === maltreatmentid);
        this.appealFindingKey = (this.appealInvestigation && Array.isArray(this.appealInvestigation.findings)) ? this.appealInvestigation.findings[0].investigationfindingtypekey : null;
        this.maltreatorsAppeal = this.appealInvestigation.maltreators ? this.appealInvestigation.maltreators : null;
        this.maltreatorsAppeal = this.maltreatorsAppeal.map(appeal => {
            appeal.iscoapresent = (appeal.coacompileddate) ? true : false;
            appeal.iscsapresent = (appeal.csacompileddate && ( !appeal.coacompileddate)) ? true : false;
            appeal.isccpresent = (appeal.cccompileddate && ( !appeal.csacompileddate || !appeal.coacompileddate)) ? true : false;
            appeal.isoahpresent = (appeal.oaicesentdate && ( !appeal.cccompileddate || !appeal.csacompileddate || !appeal.coacompileddate)) ? true : false;
            appeal.isscpresent = (appeal.scicesentdate && (!appeal.oaicesentdate || !appeal.cccompileddate || !appeal.csacompileddate || !appeal.coacompileddate)) ? true : false;
            return appeal;
         });
         this.appealDocument = this.appealInvestigation.documentprop;
        this.appealData = this.maltreatorsAppeal.find(mal => (intakeservicerequestactorid.includes(mal.intakeservicerequestactorid) && mal.activeflag === 1));
        this.uploadedFile = this.appealInvestigation.documentprop ? this.appealInvestigation.documentprop : [];
        this.patchAppealForm(this.appealData, finalized, appealed);
        this.validateAppealsFlow();
        (<any>$('#investigation-appeal')).modal('show');
        (<any>$('#appeals-tab')).click();
    }
    onUnsAppealedChange($event) {
        this.showCaseTypeForms = $event.checked;
    }
    patchAppealForm(appealData: any, finalized: boolean, appealed: boolean) {
        const patchValue = {};
        this.appealFormGroup.reset();
        this.appealFormGroup.patchValue(this.appealData);
        this.prepareOAHdata();
        this.prepareCCdata();
        this.prepareCSAdata();
        this.prepareCOAdata();
        this.prepareSCdata();
        const appealedBy = this.appealData.scappealedby;
        const maltreator = this.appealData.displayname;
        this.isUnsubstantiated = (this.appealFindingKey === 'UD') ? true : false;
        if (this.isUnsubstantiated) {
            this.showConferenceType = 5;
            this.appealFormGroup.patchValue({ type: 5, scappealedby: (appealedBy && appealedBy !== '') ? appealedBy : maltreator });
            this.appealFormGroup.get('type').disable();
        } else {
            this.showConferenceType = null;
            this.appealFormGroup.patchValue({ type: null });
            this.appealFormGroup.get('type').enable();
        }
        this.showCaseTypeForms = (this.appealFindingKey !== 'UD') ? true : false;
        this.isFinalize = appealed && !finalized;
        this.isView = appealed && finalized;
        console.log(this.appealData, this.appealInvestigation);
        // to be uncommented after finalize concept is set

        // if (this.isFinalize) {
        //     this.appealFormGroup.get('overridedate').enable();
        //     this.appealFormGroup.get('overrideby').enable();
        //     this.appealFormGroup.get('overridefindingtypekey').enable();
        //     this.appealFormGroup.get('overridecomments').enable();
        //     this.appealFormGroup.get('overrideapprflag').enable();
        //     this.appealFormGroup.get('overridefindingtypekey').setValidators(Validators.required);
        //     console.log('key', this.appealInvestigation.findings.map((item) => item.investigationfindingtypekey));
        //     this.appealInvestigation.findings.forEach((item) => {
        //         this.initialFinding = this.investigationFindingDropDown.find(i => i.investigationfindingtypekey === item.investigationfindingtypekey).description;
        //     });
        // } else {
        //      this.appealFormGroup.get('overridefindingtypekey').clearValidators();
        // }
        //  this.appealFormGroup.get('overridefindingtypekey').updateValueAndValidity();
    }

    getFindingDescription(findingsId) {
        const findingsData = this.investigationFindingDropDown.filter( item => item.investigationfindingtypekey === findingsId);
        if (findingsData && findingsData.length) {
            return findingsData[0].description;
        } else {
            return '';
        }
    }

    getInvolvedPerson() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: this.paginationInfo.pageNumber,
                    limit: this.paginationInfo.pageSize,
                    where: {
                        intakeserviceid: this.id === undefined ? this.intakeserviceid : this.id
                    },
                    method: 'get'
                }),
                'People/getpersondetail?filter'
            )
            .subscribe((res) => {
                if (res.data) {
                    this.personInfo = res.data;
                }
            });
    }

    openMalTreatmentTypePopup(maltreatmentkey, malTreatName, canOpenOpopup) {
        if (maltreatmentkey && malTreatName) {
            const investigation = this.investigation.filter((item) => item.investigationallegationid === malTreatName.investigationallegationid);
            const investComar = {
                investigationfinding: investigation[0],
                involvedPerson: this.personInfo,
                maltreatmentkey: maltreatmentkey,
                isInitialLoad: canOpenOpopup,
                displayMode: (this.roleId.role.name === 'apcs') ? 'view' : 'edit'
            };
            this.investigationComar$.next(investComar);
        }
    }
    maltreatmentType(event, malTreatName) {
        if (malTreatName.value) {
            this.openMalTreatmentTypePopup(event.value, malTreatName.value, true);
        } else {
            this.openMalTreatmentTypePopup(event.value, malTreatName, false);
        }
    }
    openReportDialog(index): void {
        // console.log(index, 'gfhgfhgffhgfhg');
        this.reportflag = index;
    }
    actionIconDisplay(malTreatName, displayMode): boolean {
        // hide edit button for supervisor
        if (displayMode === 'edit' && this.roleId.role.name === 'apcs') {
            return false;
        }
        const investigation = this.investigation ? this.investigation.filter((item) => item.investigationallegationid === malTreatName.value.investigationallegationid) : [];
        const findingsList = this.findingsList.filter((res) => res.investigationallegationid === malTreatName.value.investigationallegationid);
        if (findingsList.length > 0) {
            return true;
            // return (this.isNarrativeSaved) ? true : false;
        } else if (investigation.length && investigation[0].findings !== null) {
            return true;
            // return (this.isNarrativeSaved) ? true : false;
        }
        return false;
    }
    viewComar(malTreatName, displayMode = 'edit'): void {
        if (malTreatName.value) {
            const investigation = this.investigation.filter((item) => item.investigationallegationid === malTreatName.value.investigationallegationid);
            const findingsList = this.findingsList.filter((res) => res.investigationallegationid === malTreatName.value.investigationallegationid);
            if (findingsList.length > 0) {
                this.viewMaltreatementKey = findingsList[0].investigationfindingtypekey;
            } else if (investigation[0].findings !== null) {
                this.viewMaltreatementKey = investigation[0].findings[0].investigationfindingtypekey;
            }
            const investComar = {
                investigationfinding: investigation[0],
                involvedPerson: this.personInfo,
                maltreatmentkey: this.viewMaltreatementKey,
                isInitialLoad: true,
                displayMode
            };
            this.investigationComar$.next(investComar);
        }
    }

    submitReview() {
            this.commarValidation();
            this._commonHttpService.create(this.submitForReview, 'investigationmaltreatment/addfindings').subscribe(
                (result) => {
                    this._alertService.success('Investigation findings saved successfully!');
                    this.closePopup();
                    // this.getFindingList();
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }
    saveInvestigation() {
        if (this.investigationFindingForm.valid) {
            this.commarValidation();
            this._commonHttpService.create(this.submitForReview, 'investigationmaltreatment/addfindings').subscribe(
                (result) => {
                    this._alertService.success('Investigation findings saved successfully!');
                    this.closePopup();
                    // this.getFindingList();
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._alertService.error('Please enter Investigation Narrative');
        }
    }
    selectPerson(row) {
        this.selectedSupervisor = row;
    }
    loadSupervisor() {
        this.selectedSupervisor = '';
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'CWIF' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorsList = result.data;
                this.supervisorsList = this.supervisorsList.filter(
                    users => users.rolecode === 'SP'
                );
                if (this.supervisorsList && this.supervisorsList.length) {
                    const reviewer = this.dsdsActionsSummary.da_assignedby;
                    const selectedSupervisor = this.supervisorsList.find(user => user.username === reviewer);
                    if (selectedSupervisor) {
                        this.dispositionFormGroup.patchValue({supervisorid: selectedSupervisor.userid });
                    }
                }


            });
        if (this.reviewCheckList.length > 0) {
            const checkList = this.conditionCheckList();
            (<any>$('#checklist-investigation-findings-review')).modal('hide');
            if (checkList) {
                this.loadStatuses();

                (<any>$('#intake-caseassign')).modal('show');
            }
        } else {
            (<any>$('#checklist-investigation-findings-review')).modal('hide');
        }
    }
    conditionCheckList(): boolean {
        if (this.reviewCheckList && this.reviewCheckList.length) {
            /* const validateSafe = this.reviewCheckList.filter(
                data => data.taskname === 'Safe C Assessment' && data.isvalid === 1 && data.status === 'OPEN'
            );
            const validateMFIRA = this.reviewCheckList.filter(
                data => data.taskname === 'MFIRA assessment' && data.isvalid === 1 && data.status === 'OPEN'
            );
            const validateCANSF = this.reviewCheckList.filter(
                data => data.taskname === 'CANS-F assessment' && data.isvalid === 1 && data.status === 'OPEN'
            );
            const validateInitial = this.reviewCheckList.filter(
                data => data.taskname === 'initalfacetoface' && data.status === 'NO'
            );
            const ValidatePersonRole = this.reviewCheckList.filter(
                data => data.taskname === 'Personrole' && data.status === 'NO'
            );
            if (validateSafe.length > 0 || validateMFIRA.length > 0 || validateCANSF.length > 0 || validateInitial.length > 0 || ValidatePersonRole.length > 0) {
                return false;
            }
            return true; */
            const isFacetoFace = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.status === 'YES');
                    const personRole = this.reviewCheckList.find(item => item.taskname === 'Personrole' && item.status === 'YES');
                    const safec = this.reviewCheckList.find(item => item.taskname === 'SAFE-C' && item.status === 'Accepted');
                    const safecohp = this.reviewCheckList.find(item => item.taskname === 'SAFE-C OHP' && item.status === 'Accepted');
                    const mifra = this.reviewCheckList.find(item => item.taskname === 'MFIRA' && item.status === 'Accepted');
                    const cansf = this.reviewCheckList.find(item => item.taskname === 'cans-v2' && item.status === 'Accepted');
                    const isChildDead = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.isvalid === 0);
            if (isFacetoFace && personRole && isChildDead) {
                return true;
            } else if (isFacetoFace && personRole && !isChildDead && (safec || safecohp) && mifra && cansf) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    commarValidation() {
        this.submitForReview = Object.assign(new SubmitForReview(), this.investigationFindingForm.value);
        this.submitForReview.investigationid = this.dsdsActionsSummary.da_investigationid;
        if (this.investigation[0]) {
            this.submitForReview.maltreatmentid = this.investigation[0].maltreatmentid;
        }

        if (this.submitForReview.allegedperson) {
            this.submitForReview.allegedperson.map((item) => {
                this.findingsList.filter((res) => {
                    if (item.investigationallegationid === res.investigationallegationid) {
                        item.investigationfindings = [];
                        if (!res.isfindingassessor) {
                            this.findingAssesors = null;
                        } else {
                            this.findingAssesors = [
                                {
                                    professiontypekey: res.professiontypekey,
                                    isassessor: 1,
                                    firstname: res.firstname,
                                    lastname: res.lastname,
                                    comments: res.assessorcomments
                                },
                                {
                                    isassessor: 0,
                                    securityusersid: res.securityusersid,
                                    comments: res.caseworkercomments,
                                    professiontypekey: null
                                }
                            ];
                        }

                        item.investigationfindings.push(
                            Object.assign({
                                investigationfindingtypekey: res.investigationfindingtypekey ? res.investigationfindingtypekey : '',
                                intentionalinjurydesc: res.intentionalinjurydesc ? res.intentionalinjurydesc : '',
                                findingcomments: res.findingcomments ? res.findingcomments : '',
                                isharm: res.isharm ? res.isharm : '',
                                isharmsubstantial: res.isharmsubstantial ? res.isharmsubstantial : '',
                                harmdesc: res.harmdesc ? res.harmdesc : '',
                                omissiondesc: res.omissiondesc ? res.omissiondesc : '',
                                findingassesors: this.findingAssesors,
                            })
                        );
                    }
                });
            });
            this.submitForReview.allegedperson = this.submitForReview.allegedperson.map((data) => {
                data.med_assessmnts_attachment.forEach(element => {
                    element.objectid = data.investigationallegationid;
                    element.objecttypekey = 'InvestigationFindingMed';
                });
                data.expert_assessmnts_attachment.forEach(element => {
                    element.objectid = data.investigationallegationid;
                    element.objecttypekey = 'InvsFindPhysican';
                });
                data.law_enforcement_attachment.forEach(element => {
                    element.objectid = data.investigationallegationid;
                    element.objecttypekey = 'InvsFindlawEnforcement';
                });
                return Object.assign({
                    personid: data.personid,
                    investigationallegationid: data.investigationallegationid,
                    investigationfindings: data.investigationfindings,
                    ischildfatality: Number(data.ischildfatality),
                    victim_explanation: data.victim_explanation,
                    sibling_explanation: data.sibling_explanation,
                    guardian_explanation: data.guardian_explanation,
                    maltreator_explanation: data.maltreator_explanation,
                    med_assessmnts: data.med_assessmnts,
                    expert_assessmnts: data.expert_assessmnts,
                    law_enforcement_inv: data.law_enforcement_inv,
                    collateral_interviews: data.collateral_interviews,
                    criminal_history_inv: data.criminal_history_inv,
                    home_conditions: data.home_conditions,
                    med_assessmnts_attachment: data.med_assessmnts_attachment,
                    expert_assessmnts_attachment: data.expert_assessmnts_attachment,
                    law_enforcement_attachment: data.law_enforcement_attachment
                });
            });

        }
    }
    reviewCheck() {
        const isValid = this.maltreatmentValidation();
        if (!isValid) {
            (<any>$('#maltreatment-validation-modal')).modal('show');
            return;
        }
        this.commarValidation();
        this._commonHttpService.create(this.submitForReview, 'investigationmaltreatment/addfindings').subscribe(
            (result) => {
               /*  this.reviewCheckList.map((review) => {
                    if (review.taskname === 'initalfacetoface' && review.status === 'YES') {
                        this.reviewCheckListForm.patchValue({ initalfacetoface: true });
                    } else if (review.taskname === 'Safe C Assessment' && review.status === 'CLOSED') {
                        this.reviewCheckListForm.patchValue({ safec: true });
                    } else if (review.taskname === 'CANS-F assessment' && review.status === 'CLOSED') {
                        this.reviewCheckListForm.patchValue({ canf: true });
                    } else if (review.taskname === 'MFIRA assessment' && review.status === 'CLOSED') {
                        this.reviewCheckListForm.patchValue({ mfira: true });
                    } else if (review.taskname === 'Personrole' && review.status === 'YES') {
                        this.reviewCheckListForm.patchValue({ personrole: true });
                    }
                });
 */
                    const isFacetoFace = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.status === 'YES');
                    const personRole = this.reviewCheckList.find(item => item.taskname === 'Personrole' && item.status === 'YES');
                    const safec = this.reviewCheckList.find(item => item.taskname === 'SAFE-C' && item.status === 'Accepted');
                    const safecohp = this.reviewCheckList.find(item => item.taskname === 'SAFE-C OHP' && item.status === 'Accepted');
                    const mifra = this.reviewCheckList.find(item => item.taskname === 'MFIRA' && item.status === 'Accepted');
                    const cansf = this.reviewCheckList.find(item => item.taskname === 'cans-v2' && item.status === 'Accepted');
                    const isChildDead = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.isvalid === 0);
                    if (isChildDead) {
                        this.isChildNotDead = false;
                    } else {
                        this.isChildNotDead = true;
                    }
                    if (isFacetoFace) {
                        this.reviewCheckListForm.patchValue({ initalfacetoface: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ initalfacetoface: false });
                    }
                    if (personRole) {
                        this.reviewCheckListForm.patchValue({ personrole: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ personrole: false });
                    }
                    if (safec || safecohp ) {
                        this.reviewCheckListForm.patchValue({ safec: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ safec: false });
                    }
                    if (mifra) {
                        this.reviewCheckListForm.patchValue({ mfira: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ mfira: false });
                    }
                    if (cansf) {
                        this.reviewCheckListForm.patchValue({ canf: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ canf: false });
                    }

                const commar = this.submitForReview.allegedperson.filter((data) => data.investigationfindings === null);
                // if (!this.investigationFindingForm.value.summary) {
                //     this._alertService.warn('Please fill Narrative!');
                // } else if (!this.investigationFindingForm.value.remarks) {
                //     this._alertService.warn('Please fill Result of Law enforcement Investigation!');
                // } else
                if (commar.length > 0) {
                    this._alertService.warn('Please select investigation finding');
                } else {
                    this.checkListMandatory();
                    (<any>$('#checklist-investigation-findings-review')).modal('show');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    getCheckList() {
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    where: { intakeserviceid: this.id },
                    method: 'get'
                }),
                'Investigationfindings/getfacetofacedetails?filter'
            )
            .subscribe((result) => {
                this.reviewCheckList = result;
            });
    }
    closePopup() {
        (<any>$('#intake-caseassign')).modal('hide');
        (<any>$('#checklist-investigation-findings-review')).modal('hide');
    }
    loadDispositon(statusId) {
        const statusKey = statusId.split('~')[0];
        this.dispositionDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        statuskey: statusKey,
                        intakeservreqtypeid: this.daType,
                        servicerequestsubtypeid: this.daType
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.DispositionUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.servicerequesttypeconfigiddispostionid
                        })
                );
            });
        this.dispositionDropdownItems$.subscribe(data => { this.dispositionDropdownItems = data; });
    }
    onFatalityChange(event, investigation, index) {
        console.log('On Fatality Change', event);
        if (event.value === 1) {
            const selectedPerson = this.personInfo.filter(person => person.personid === investigation.value.personid);
            if (selectedPerson && selectedPerson.length && !selectedPerson[0].dateofdeath) {
                const control = <FormArray>this.investigationFindingForm.controls['allegedperson'];
                control.controls[index].patchValue({ ischildfatality: 0 });
                this._alertService.error('Please select person date of death');
            }
        }
    }
    confirmDisposition() {
        const disposition = this.dispositionDropdownItems.find(item => item.value === this.dispositionFormGroup.value.dispositionid);
        this.saveDisposition();

    }
    cancelConfirmDisposition() {
        (<any>$('#status-disposition')).modal('show');
    }
    saveDisposition() {
        const dispositionModal = new DispositionAddModal();
        dispositionModal.disposition = Object.assign({
            intakeserviceid: this.id,
            intakeserreqstatustypeid: this.dispositionFormGroup.getRawValue().statusid.split('~')[1],
            dispostionid: this.dispositionFormGroup.value.dispositionid,
            reviewcomments: this.dispositionFormGroup.value.reviewcomments,
            closingcodetypekey: this.dispositionFormGroup.value.closingcodetypekey,
            dateseen: new Date()
        });
        dispositionModal.investigation = {
            summary: '',
            filelocdesc: null,
            appevent: 'INVR'
        };
        this._dispositionAddService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.DispositionAddUrl;
        this._dispositionAddService.create(dispositionModal).subscribe(
            (response) => {
                this.dispositionFormGroup.reset();
                (<any>$('#intake-caseassign')).modal('hide');
                this._alertService.success('Disposition updated successfully');
                Observable.timer(2000).subscribe(() => {
                    this._router.routeReuseStrategy.shouldReuseRoute = function () {
                        return false;
                    };
                    const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition';
                    this._router.navigateByUrl(currentUrl).then(() => {
                        this._router.navigated = false;
                        this._router.navigate([currentUrl]);
                    });
                });
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    private loadStatuses() {
        this.statusDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        intakeservreqtypeid: this.daType,
                        servicerequestsubtypeid: this.daType
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.StatusUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.intakeserreqstatustypekey + '~' + res.intakeserreqstatustypeid
                        })
                );
            });
            this.statusDropdownItems$.subscribe( data => {
                const completeStatus = data.find(item => item.text === 'Completed');
                this.dispositionFormGroup.patchValue({
                    statusid : completeStatus.value
                });
                this.loadDispositon(completeStatus.value);
                this.dispositionFormGroup.get('statusid').disable();
            });
    }
    private getActionSummary() {
        this._commonHttpService.getById(this.daNumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe(
            (response) => {
                this.dsdsActionsSummary = response[0];
                if (this.dsdsActionsSummary) {
                    this.daType = this.dsdsActionsSummary.da_typeid;
                    this.getFindingList();
                    this.getCheckList();
                    this.getMaltreatmentInfo();
                } else {
                    this._alertService.error('DSDS Action Summary is Empty, Please Check Your Data.');
                }
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    private checkListMandatory() {
        this.isMandatory = {
            initialfacetoface: false,
            safec: false,
            cansf: false,
            mfira: false,
            personrole: false
        };
        this.reviewCheckList.map((review) => {
            if (review.taskname === 'initalfacetoface' && review.isvalid === 1) {
                this.isMandatory.initialfacetoface = true;
            } else if (review.taskname === 'Safe C Assessment' && review.isvalid === 1) {
                this.isMandatory.safec = true;
            } else if (review.taskname === 'CANS-F assessment' && review.isvalid === 1) {
                this.isMandatory.cansf = true;
            } else if (review.taskname === 'MFIRA assessment' && review.isvalid === 1) {
                this.isMandatory.mfira = true;
            } else if (review.taskname === 'Personrole' && review.isvalid === 1) {
                this.isMandatory.personrole = true;
            }
        });
    }
    onConfDecisonChange(event) {
        if (['UD', 'ID'].includes(event.value)) {
            this.appealFormGroup.get('scappealed').enable();
        } else {
            this.appealFormGroup.get('scappealed').disable();
        }
    }

    onConfTypeChange(event) {
        (<any>$('#appeals-tab')).click();
        this.appealFormGroup.reset();
        this.appealFormGroup.patchValue({ type: event.value });
        console.log('OnConfType Change', event);
        if (event.value === '1') {
            this.conferenceTypeTitle = 'OAH';
            this.showConferenceType = 1;
            this.prepareOAHdata();
            const fileList = (this.appealDocument && Array.isArray(this.appealDocument)) ? this.appealDocument : [];
            this.uploadedFile = fileList.filter(item => item.rootobjecttypekey === 'oah');
        } else if (event.value === '2') {
            this.conferenceTypeTitle = 'Circuit Court';
            this.showConferenceType = 2;
            this.prepareCCdata();
            const fileList = (this.appealDocument && Array.isArray(this.appealDocument)) ? this.appealDocument : [];
            this.uploadedFile = fileList.filter(item => item.rootobjecttypekey === 'circuitcourt');
        } else if (event.value === '3') {
            this.conferenceTypeTitle = 'CSA';
            this.showConferenceType = 3;
            this.prepareCSAdata();
            const fileList = (this.appealDocument && Array.isArray(this.appealDocument)) ? this.appealDocument : [];
            this.uploadedFile = fileList.filter(item => item.rootobjecttypekey === 'courtspecial');
        } else if (event.value === '4') {
            this.conferenceTypeTitle = 'COA';
            this.showConferenceType = 4;
            this.prepareCOAdata();
            const fileList = (this.appealDocument && Array.isArray(this.appealDocument)) ? this.appealDocument : [];
            this.uploadedFile = fileList.filter(item => item.rootobjecttypekey === 'coa');
        } else if (event.value === '5') {
            this.conferenceTypeTitle = 'SC';
            this.showConferenceType = 5;
            this.prepareSCdata();
            this.appealFormGroup.get('scappealed').disable();
            const fileList = (this.appealDocument && Array.isArray(this.appealDocument)) ? this.appealDocument : [];
            this.uploadedFile = fileList.filter(item => item.rootobjecttypekey === 'supervisor');
        } else {
            this.showConferenceType = -1;
        }
    }

    saveAppeal() {
        console.log('Save Appeal', this._intakeUtils.findInvalidControls(this.appealFormGroup));
        this.findInvalidControls(this.appealFormGroup);
        if (this.appealFormGroup.invalid) {
            this._alertService.warn('Please fill mandatory feilds.');
        } else {
            const formValue = this.appealFormGroup.getRawValue();
            const reqJSON = Object.assign({}, this.appealData);
            reqJSON.investigationallegationmaltreatorsid = this.appealData.investigationallegationmaltreatorsid;
            switch (this.conferenceTypeTitle) {
                case 'Circuit Court':
                    reqJSON.ccstayrequestedflag = formValue.ccstayrequestedflag ? 1 : 0;
                    reqJSON.ccstaygrantedflag = formValue.ccstaygrantedflag ? 1 : 0;
                    reqJSON.ccappealedflag = formValue.ccappealedflag ? 1 : 0;
                    reqJSON.cchearingdecisiontypekey = formValue.cchearingdecisiontypekey;
                    reqJSON.cchearingdecisiondate = formValue.cchearingdecisiondate;
                    reqJSON.ccdetails = formValue.ccdetails;
                    reqJSON.cccasenumber = formValue.cccasenumber;
                    // reqJSON.cccasenumber = 'DHS-' + ( formValue.ccjuridiction ?  formValue.ccjuridiction : '' ) + '-' + ( formValue.cccasenumberlast8digit ? formValue.cccasenumberlast8digit : '' );
                    reqJSON.cccasenumberlast8digit = formValue.cccasenumberlast8digit;
                    reqJSON.cccourtdecisionflag = formValue.cccourtdecisionflag ? 1 : 0;
                    reqJSON.cccompileddate = formValue.cccompileddate;
                    reqJSON.ccldssname = formValue.ccldssname;
                    reqJSON.ccappellentatrny = formValue.ccappellentatrny;
                    reqJSON.ccwhoappealed = formValue.ccwhoappealed;
                    reqJSON.ccnotifiedtodirector = formValue.ccnotifiedtodirector;
                    reqJSON.ccldssnotifieddate = formValue.ccldssnotifieddate;
                    reqJSON.cchearingheld = formValue.cchearingheld;
                    reqJSON.cchearingreason = formValue.cchearingreason;
                    reqJSON.cclocationofhearing = formValue.cclocationofhearing;
                    reqJSON.cccicuitcourtkey = formValue.cccicuitcourtkey;
                    reqJSON.cchearingheldreason = formValue.cchearingheldreason;
                    reqJSON.ccjuridiction = formValue.ccjuridiction;
                    break;
                case 'CSA':
                    reqJSON.csastayrequestedflag = formValue.csastayrequestedflag ? 1 : 0;
                    reqJSON.csastaygrantedflag = formValue.csastaygrantedflag ? 1 : 0;
                    reqJSON.csaappealedflag = formValue.csaappealedflag ? 1 : 0;
                    reqJSON.csahearingdecisiontypekey = formValue.csahearingdecisiontypekey;
                    reqJSON.csahearingdecisiondate = formValue.csahearingdecisiondate;
                    reqJSON.csadetails = formValue.csadetails;
                    reqJSON.csacasenumber = formValue.csacasenumber;
                    // reqJSON.csacasenumber = 'DHS-' +  (formValue.csajuridiction ? formValue.csajuridiction : '') + '-' + ( formValue.csacasenumberlast8digit ?
                    // formValue.csacasenumberlast8digit : '' );
                    reqJSON.csacasenumberlast8digit = formValue.csacasenumberlast8digit;
                    reqJSON.csacourtdecisionflag = formValue.csacourtdecisionflag ? 1 : 0;
                    reqJSON.csacompileddate = formValue.csacompileddate;
                    reqJSON.csaldssnotifieddate = formValue.csaldssnotifieddate;
                    reqJSON.csawhoappealed = formValue.csawhoappealed;
                    reqJSON.csaarguementheld = formValue.csaarguementheld;
                    reqJSON.csaarguementnotheldreason = formValue.csaarguementnotheldreason;
                    reqJSON.csaldssname = formValue.csaldssname;
                    reqJSON.csaappellentatrny = formValue.csaappellentatrny;
                    reqJSON.csanotifiedtodirector = formValue.csanotifiedtodirector;
                    reqJSON.csalocationofhearing = formValue.csalocationofhearing;
                    reqJSON.csajuridiction = formValue.csajuridiction;
                    break;
                case 'COA':
                    reqJSON.coastayreqflag = formValue.coastayreqflag ? 1 : 0;
                    reqJSON.coastaygrantedflag = formValue.coastaygrantedflag ? 1 : 0;
                    reqJSON.coaappealedflag = formValue.coaappealedflag ? 1 : 0;
                    reqJSON.coahearingdecisiontypekey = formValue.coahearingdecisiontypekey;
                    reqJSON.coahearingdecisiondate = formValue.coahearingdecisiondate;
                    reqJSON.coadetails = formValue.coadetails;
                    reqJSON.coacasenumber =  formValue.coacasenumber;
                    // reqJSON.coacasenumber = 'DHS-' +  ( formValue.coajuridiction ? formValue.coajuridiction : '' ) + '-' +
                    ( formValue.coacasenumberlast8digit ? formValue.coacasenumberlast8digit : '' );
                    reqJSON.coacasenumberlast8digit = formValue.coacasenumberlast8digit;
                    reqJSON.coacourtdecisionflag = formValue.coacourtdecisionflag ? 1 : 0;
                    reqJSON.coacompileddate = formValue.coacompileddate;
                    reqJSON.csaldssname = formValue.csaldssname;
                    reqJSON.coacertioraristatus = formValue.coacertioraristatus;
                    reqJSON.coacertgranteddate = formValue.coacertgranteddate;
                    reqJSON.coacertdenieddate  = formValue.coacertdenieddate;
                    reqJSON.csaappellentatrny = formValue.csaappellentatrny;
                    reqJSON.coaldssname = formValue.coaldssname;
                    reqJSON.coaappellentatrny = formValue.coaappellentatrny;
                    reqJSON.coalocationofhearing = formValue.coalocationofhearing;
                    reqJSON.coajuridiction = formValue.coajuridiction;
                    break;
                case 'SC': {
                    reqJSON.scicesentdate = formValue.scicesentdate;
                    reqJSON.scconfheldflag = formValue.scconfheldflag ? 1 : 0;
                    reqJSON.scdecisiontypekey = formValue.scdecisiontypekey;
                    reqJSON.scconferencedetail = formValue.scconferencedetail;
                    reqJSON.scconferencedate = formValue.scconferencedate;
                    reqJSON.scisappealed = formValue.scisappealed;
                    reqJSON.scappealedby = formValue.scappealedby;
                    reqJSON.scappealeddate = formValue.scappealeddate;
                    reqJSON.scdecisiontypekey = formValue.scdecisiontypekey;
                    reqJSON.scappealedsetdate = formValue.scappealedsetdate;
                    reqJSON.scisappealformsent = formValue.scisappealformsent;
                    reqJSON.scsummarymailed = formValue.scsummarymailed;
                    break;
                }
                case 'OAH':
                    reqJSON.oahearingdatesetflag = formValue.oahearingdatesetflag;
                    reqJSON.oahearingdate = formValue.oahearingdate;
                    reqJSON.oahearingdecision = formValue.oahearingdecision;
                    reqJSON.oahearingdecisiondate = formValue.oahearingdecisiondate;
                    reqJSON.oacasenumber = 'DHS-' +  ( formValue.oajuridiction ? formValue.oajuridiction : '' ) + '-' + ( formValue.oacasenumberlast8digit ? formValue.oacasenumberlast8digit : '' );
                    reqJSON.oacasenumberlast8digit = formValue.oacasenumberlast8digit;
                    reqJSON.oaicesentdate = formValue.oaicesentdate;
                    reqJSON.oanorhearingreason = formValue.oanorhearingreason;
                    reqJSON.oadetails = formValue.oadetails;
                    reqJSON.oaappealedflag = formValue.oaappealedflag ? 1 : 0;
                    reqJSON.oaldssname = formValue.oaldssname;
                    reqJSON.oaappellentatrny = formValue.oaappellentatrny;
                    reqJSON.oalocaldept = formValue.oalocaldept;
                    reqJSON.oarunningmotion = formValue.oarunningmotion;
                    reqJSON.oahearingnarrative = formValue.oahearingnarrative;
                    reqJSON.oamodificationsmade = formValue.oamodificationsmade;
                    reqJSON.oarunningmotiondate = formValue.oarunningmotiondate;
                    reqJSON.oamaltreatorunnamedflag = formValue.oamaltreatorunnamedflag;
                    reqJSON.oacompiledwithoah = formValue.oacompiledwithoah;
                    // reqJSON.oamaltreatorname = formValue.oamaltreatorname;
                    reqJSON.oatranslator = formValue.oatranslator;
                    reqJSON.oahearingheld = formValue.oahearingheld;
                    reqJSON.oahearingheldreason = formValue.oahearingheldreason;
                    reqJSON.oalocationofhearing = formValue.oalocationofhearing;
                    reqJSON.oasummarydecisionfiledflag = formValue.oasummarydecisionfiledflag;
                    reqJSON.oasummarydecisionfileddate = formValue.oasummarydecisionfileddate;
                    reqJSON.oajuridiction = formValue.oajuridiction;
                    break;
            }
            // ObjectUtils.removeEmptyProperties(reqJSON);
            reqJSON.investigationallegationmaltreatorsid = this.appealData.investigationallegationmaltreatorsid,
            reqJSON.attachment = this.appealData.documentprop;
            reqJSON.intakeservicerequestactorid = this.appealData.intakeservicerequestactorid;
            reqJSON.allegationid = this.appealInvestigation.investigationallegationid;
            // this._commonHttpService.create(reqJSON, 'Investigationallegationmaltreators/updateappeal').subscribe(res => {
            this._commonHttpService.create(reqJSON, 'Investigationallegationmaltreators/updateappealhistory').subscribe(res => {
                this._alertService.success(this.isFinalize ? 'Appeal finalized successfully' : 'Appeal saved successfully');
                (<any>$('#investigation-appeal')).modal('hide');
                this.getFindingList();
            });
        }
    }
    saveFinalize() {
        console.log('Save finalize', this._intakeUtils.findInvalidControls(this.finalizeFormGroup));
        this.findInvalidControls(this.finalizeFormGroup);
        if (this.finalizeFormGroup.invalid) {
            this._alertService.warn('Please fill mandatory feilds.');
        } else {
            const formValue = this.finalizeFormGroup.getRawValue();
            const reqJSON = Object.assign({}, this.appealData);
            reqJSON.investigationallegationmaltreatorsid = this.appealData.investigationallegationmaltreatorsid;
            reqJSON.overrideapprflag = formValue.overrideapprflag ? 1 : 0;
            reqJSON.overridecomments = formValue.overridecomments;
            reqJSON.overridefindingtypekey = formValue.overridefindingtypekey;
            reqJSON.finalizeflag = true;
            // switch (this.conferenceTypeTitle) {
            //     case 'Circuit Court':
            //         reqJSON.ccstayrequestedflag = formValue.ccstayrequestedflag ? 1 : 0;
            //         reqJSON.ccstaygrantedflag = formValue.ccstaygrantedflag ? 1 : 0;
            //         reqJSON.ccappealedflag = formValue.ccappealedflag ? 1 : 0;
            //         reqJSON.cchearingdecisiontypekey = formValue.cchearingdecisiontypekey;
            //         reqJSON.cchearingdecisiondate = formValue.cchearingdecisiondate;
            //         reqJSON.ccdetails = formValue.ccdetails;
            //         reqJSON.cccasenumber = 'DHS-' + ( formValue.ccjuridiction ?  formValue.ccjuridiction : '' ) + '-' + ( formValue.cccasenumberlast8digit ? formValue.cccasenumberlast8digit : '' );
            //         reqJSON.cccasenumberlast8digit = formValue.cccasenumberlast8digit;
            //         reqJSON.cccourtdecisionflag = formValue.cccourtdecisionflag ? 1 : 0;
            //         reqJSON.cccompileddate = formValue.cccompileddate;
            //         reqJSON.ccldssname = formValue.ccldssname;
            //         reqJSON.ccappellentatrny = formValue.ccappellentatrny;
            //         reqJSON.ccwhoappealed = formValue.ccwhoappealed;
            //         reqJSON.ccnotifiedtodirector = formValue.ccnotifiedtodirector;
            //         reqJSON.ccldssnotifieddate = formValue.ccldssnotifieddate;
            //         reqJSON.cchearingheld = formValue.cchearingheld;
            //         reqJSON.cchearingreason = formValue.cchearingreason;
            //         reqJSON.cclocationofhearing = formValue.cclocationofhearing;
            //         reqJSON.cccicuitcourtkey = formValue.cccicuitcourtkey;
            //         reqJSON.cchearingheldreason = formValue.cchearingheldreason;
            //         reqJSON.ccjuridiction = formValue.ccjuridiction;
            //         break;
            //     case 'CSA':
            //         reqJSON.csastayrequestedflag = formValue.csastayrequestedflag ? 1 : 0;
            //         reqJSON.csastaygrantedflag = formValue.csastaygrantedflag ? 1 : 0;
            //         reqJSON.csaappealedflag = formValue.csaappealedflag ? 1 : 0;
            //         reqJSON.csahearingdecisiontypekey = formValue.csahearingdecisiontypekey;
            //         reqJSON.csahearingdecisiondate = formValue.csahearingdecisiondate;
            //         reqJSON.csadetails = formValue.csadetails;
            //         reqJSON.csacasenumber = 'DHS-' +  (formValue.csajuridiction ? formValue.csajuridiction : '') + '-' +
            // ( formValue.csacasenumberlast8digit ? formValue.csacasenumberlast8digit : '' );
            //         reqJSON.csacasenumberlast8digit = formValue.csacasenumberlast8digit;
            //         reqJSON.csacourtdecisionflag = formValue.csacourtdecisionflag ? 1 : 0;
            //         reqJSON.csacompileddate = formValue.csacompileddate;
            //         reqJSON.csaldssnotifieddate = formValue.csaldssnotifieddate;
            //         reqJSON.csawhoappealed = formValue.csawhoappealed;
            //         reqJSON.csaarguementheld = formValue.csaarguementheld;
            //         reqJSON.csaarguementnotheldreason = formValue.csaarguementnotheldreason;
            //         reqJSON.csaldssname = formValue.csaldssname;
            //         reqJSON.csaappellentatrny = formValue.csaappellentatrny;
            //         reqJSON.csanotifiedtodirector = formValue.csanotifiedtodirector;
            //         reqJSON.csalocationofhearing = formValue.csalocationofhearing;
            //         reqJSON.csajuridiction = formValue.csajuridiction;
            //         break;
            //     case 'COA':
            //         reqJSON.coastayreqflag = formValue.coastayreqflag ? 1 : 0;
            //         reqJSON.coastaygrantedflag = formValue.coastaygrantedflag ? 1 : 0;
            //         reqJSON.coaappealedflag = formValue.coaappealedflag ? 1 : 0;
            //         reqJSON.coahearingdecisiontypekey = formValue.coahearingdecisiontypekey;
            //         reqJSON.coahearingdecisiondate = formValue.coahearingdecisiondate;
            //         reqJSON.coadetails = formValue.coadetails;
            //         reqJSON.coacasenumber = 'DHS-' +  ( formValue.coajuridiction ? formValue.coajuridiction : '' ) + '-' +
            //         ( formValue.coacasenumberlast8digit ? formValue.coacasenumberlast8digit : '' );
            //         reqJSON.coacasenumberlast8digit = formValue.coacasenumberlast8digit;
            //         reqJSON.coacourtdecisionflag = formValue.coacourtdecisionflag ? 1 : 0;
            //         reqJSON.coacompileddate = formValue.coacompileddate;
            //         reqJSON.csaldssname = formValue.csaldssname;
            //         reqJSON.coacertioraristatus = formValue.coacertioraristatus;
            //         reqJSON.coacertgranteddate = formValue.coacertgranteddate;
            //         reqJSON.coacertdenieddate  = formValue.coacertdenieddate;
            //         reqJSON.csaappellentatrny = formValue.csaappellentatrny;
            //         reqJSON.coaldssname = formValue.coaldssname;
            //         reqJSON.coaappellentatrny = formValue.coaappellentatrny;
            //         reqJSON.coalocationofhearing = formValue.coalocationofhearing;
            //         reqJSON.coajuridiction = formValue.coajuridiction;
            //         break;
            //     case 'SC': {
            //         reqJSON.scicesentdate = formValue.scicesentdate;
            //         reqJSON.scconfheldflag = formValue.scconfheldflag ? 1 : 0;
            //         reqJSON.scdecisiontypekey = formValue.scdecisiontypekey;
            //         reqJSON.scconferencedetail = formValue.scconferencedetail;
            //         reqJSON.scconferencedate = formValue.scconferencedate;
            //         reqJSON.scisappealed = formValue.scisappealed;
            //         reqJSON.scappealedby = formValue.scappealedby;
            //         reqJSON.scappealeddate = formValue.scappealeddate;
            //         reqJSON.scdecisiontypekey = formValue.scdecisiontypekey;
            //         reqJSON.scappealedsetdate = formValue.scappealedsetdate;
            //         reqJSON.scisappealformsent = formValue.scisappealformsent;
            //         reqJSON.scsummarymailed = formValue.scsummarymailed;
            //         break;
            //     }
            //     case 'OAH':
            //         reqJSON.oahearingdatesetflag = formValue.oahearingdatesetflag;
            //         reqJSON.oahearingdate = formValue.oahearingdate;
            //         reqJSON.oahearingdecision = formValue.oahearingdecision;
            //         reqJSON.oahearingdecisiondate = formValue.oahearingdecisiondate;
            //         reqJSON.oacasenumber = 'DHS-' +  ( formValue.oajuridiction ? formValue.oajuridiction : '' ) + '-' + ( formValue.oacasenumberlast8digit ? formValue.oacasenumberlast8digit : '' );
            //         reqJSON.oacasenumberlast8digit = formValue.oacasenumberlast8digit;
            //         reqJSON.oaicesentdate = formValue.oaicesentdate;
            //         reqJSON.oanorhearingreason = formValue.oanorhearingreason;
            //         reqJSON.oadetails = formValue.oadetails;
            //         reqJSON.oaappealedflag = formValue.oaappealedflag ? 1 : 0;
            //         reqJSON.oaldssname = formValue.oaldssname;
            //         reqJSON.oaappellentatrny = formValue.oaappellentatrny;
            //         reqJSON.oalocaldept = formValue.oalocaldept;
            //         reqJSON.oarunningmotion = formValue.oarunningmotion;
            //         reqJSON.oahearingnarrative = formValue.oahearingnarrative;
            //         reqJSON.oamodificationsmade = formValue.oamodificationsmade;
            //         reqJSON.oarunningmotiondate = formValue.oarunningmotiondate;
            //         reqJSON.oamaltreatorunnamedflag = formValue.oamaltreatorunnamedflag;
            //         reqJSON.oacompiledwithoah = formValue.oacompiledwithoah;
            //         // reqJSON.oamaltreatorname = formValue.oamaltreatorname;
            //         reqJSON.oatranslator = formValue.oatranslator;
            //         reqJSON.oahearingheld = formValue.oahearingheld;
            //         reqJSON.oahearingheldreason = formValue.oahearingheldreason;
            //         reqJSON.oalocationofhearing = formValue.oalocationofhearing;
            //         reqJSON.oasummarydecisionfiledflag = formValue.oasummarydecisionfiledflag;
            //         reqJSON.oasummarydecisionfileddate = formValue.oasummarydecisionfileddate;
            //         reqJSON.oajuridiction = formValue.oajuridiction;
            //         break;
            // }
            // ObjectUtils.removeEmptyProperties(reqJSON);
            // reqJSON.investigationallegationmaltreatorsid = this.appealData.investigationallegationmaltreatorsid,
            // reqJSON.attachment = this.appealData.documentprop;
            // reqJSON.displayname = null;
            // reqJSON.intakeservicerequestactorid = this.appealData.intakeservicerequestactorid;
            // reqJSON.allegationid = this.appealInvestigation.investigationallegationid;
            this._commonHttpService.create(reqJSON, 'Investigationallegationmaltreators/updateappeal').subscribe(res => {
            // this._commonHttpService.create(reqJSON, 'Investigationallegationmaltreators/updateappealhistory').subscribe(res => {
                this._alertService.success( 'Appeal finalized successfully');
                (<any>$('#finalize-appeal')).modal('hide');
                this.getFindingList();
            });
        }
    }
    getJurisdictionList() {
        this.juristictionList$ = this._commonHttpService.getArrayList({
            order: 'countyname asc',
            method: 'get',
            nolimit: true
        }, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter');
    }

    getHearingReasonList() {
        this._commonHttpService.getArrayList({
            method: 'get',
            where: { tablename: 'orderdescription', teamtypekey: 'CW' }
        }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes + '?filter').subscribe(data => {
            this.hearingReasonList = data;
        });
    }
    getAppealDocumentList() {
        this._commonHttpService.getArrayList({
            method: 'get',
            where: { tablename: 'appealdocuments', teamtypekey: 'CW' }
        }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes + '?filter').subscribe(data => {
            this.appealDocumentList = data;
        });
    }

    getJuridictionList() {
        this._commonHttpService.getArrayList({
            method: 'get',
            where: { tablename: 'jurisdictioncode', teamtypekey: 'CW' }
        }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes + '?filter').subscribe(data => {
            this.juridictionList = data;
        });
    }
    getJudiLocationList() {
        this._commonHttpService.getArrayList({
            method: 'get',
            where: { tablename: 'jurilocation', teamtypekey: 'CW' },
            order: 'displayorder ASC'
        }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes + '?filter').subscribe(data => {
            this.judiLocationList = data;
        });
    }
    getJudiAddressList() {
        this._commonHttpService.getArrayList({
            order: 'attorneyname asc',
            method: 'get',
            where: {}
        }, CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.attorneyaddress + '?filter').subscribe(data => {
            this.judiAddressList = data;
        });
    }

    prepareSCdata() {
        this.appealFormGroup.patchValue({
            scicesentdate: this.appealData.scicesentdate ? new Date(this.appealData.scicesentdate) : null,
            scdecisiontypekey: this.appealData.scdecisiontypekey,
            scconferencedate: this.appealData.scconferencedate ? new Date(this.appealData.scconferencedate) : null,
            scappealedby: (this.appealData && (this.appealData.scappealedby || this.appealData.scappealedby === '')) ? this.appealData.scappealedby : this.appealData.displayname,
            scconfheldflag: this.appealData.scconfheldflag,
            scappealeddate: this.appealData.scappealeddate ? new Date(this.appealData.scappealeddate) : null,
            scisappealed: this.appealData.scisappealed,
            scconferencedetail: this.appealData.scconferencedetail,
            scappealedsetdate: this.appealData.scappealedsetdate ? new Date(this.appealData.scappealedsetdate) : null,
            scisappealformsent: this.appealData.scisappealformsent,
            scsummarymailed: this.appealData.scsummarymailed
        });
    }
    extractfromCaseNumber(data, controlName) {
    if (data) {
      const splitArray  = data.split('-', 3);
      // const preText = (splitArray && splitArray.length) ? splitArray[0] : '';
      const juridiction = (splitArray && splitArray.length && splitArray.length >= 2 ) ? splitArray[1] : '';
      const last8digit = (splitArray && splitArray.length && splitArray.length >= 3 ) ? splitArray[2] : '';
      switch (controlName) {
          case 'juridiction':
              return juridiction;
          case 'last_8_digit':
              return last8digit ;
          default:
              return last8digit;
      }
     } else {
         return '';
     }
    }
    prepareOAHdata() {
        this.appealFormGroup.patchValue({
            oaicesentdate: this.appealData.oaicesentdate ? new Date(this.appealData.oaicesentdate) : null,
            oacasenumber: this.appealData.oacasenumber,
            oacasenumberlast8digit: this.extractfromCaseNumber(this.appealData.oacasenumber, 'last_8_digit'),
            oahearingdecisiondate: this.appealData.oahearingdecisiondate ? new Date(this.appealData.oahearingdecisiondate) : null,
            oaldssname: this.appealData.oaldssname,
            oahearingdatesetflag: this.appealData.oahearingdatesetflag,
            oaappellentatrny: this.appealData.oaappellentatrny,
            oanorhearingreason: this.appealData.oanorhearingreason,
            oaappealedflag: this.appealData.oaappealedflag,
            oalocaldept: this.appealData.oalocaldept,
            oarunningmotion: this.appealData.oarunningmotion,
            oadetails: this.appealData.oadetails,
            oahearingnarrative: this.appealData.oahearingnarrative,
            oamodificationsmade: this.appealData.oamodificationsmade,
            oarunningmotiondate:  this.appealData.oarunningmotiondate ? new Date(this.appealData.oarunningmotiondate) : null,
            oamaltreatorunnamedflag: this.appealData.oamaltreatorunnamedflag,
            oacompiledwithoah: this.appealData.oacompiledwithoah,
            // oamaltreatorname: this.appealData.oamaltreatorname,
            oatranslator: this.appealData.oatranslator,
            oahearingheld: this.appealData.oahearingheld,
            oahearingheldreason: this.appealData.oahearingheldreason,
            oalocationofhearing: this.appealData.oalocationofhearing,
            oasummarydecisionfiledflag: this.appealData.oasummarydecisionfiledflag,
            oasummarydecisionfileddate: this.appealData.oasummarydecisionfileddate ? new Date(this.appealData.oasummarydecisionfileddate) : null,
            oahearingdate: this.appealData.oahearingdate ? new Date(this.appealData.oahearingdate) : null,
            oahearingdecision: this.appealData.oahearingdecision,
            oajuridiction: this.extractfromCaseNumber(this.appealData.oacasenumber, 'juridiction'),
        });
    }

    prepareCCdata() {
        this.appealFormGroup.patchValue({
            cchearingdecisiondate:  this.appealData.cchearingdecisiondate ? new Date(this.appealData.cchearingdecisiondate) : null,
            cchearingdecisiontypekey: this.appealData.cchearingdecisiontypekey,
            cccicuitcourtkey: this.appealData.cccicuitcourtkey,
            cccasenumber: this.appealData.cccasenumber,
            cccasenumberlast8digit: this.extractfromCaseNumber(this.appealData.cccasenumber, 'last_8_digit'),
            ccldssname: this.appealData.ccldssname,
            ccappellentatrny: this.appealData.ccappellentatrny,
            ccwhoappealed: this.appealData.ccwhoappealed,
            cccourtdecisionflag: this.appealData.cccourtdecisionflag,
            ccappealedflag: this.appealData.ccappealedflag,
            ccnotifiedtodirector: this.appealData.ccnotifiedtodirector,
            ccldssnotifieddate: this.appealData.ccldssnotifieddate ? new Date(this.appealData.ccldssnotifieddate) : null,
            ccdetails: this.appealData.ccdetails,
            cccompileddate:  this.appealData.cccompileddate ? new Date(this.appealData.cccompileddate) : null,
            cchearingreason: this.appealData.cchearingreason,
            cchearingheld: this.appealData.cchearingheld,
            cclocationofhearing: this.appealData.cclocationofhearing,
            ccjuridiction: this.extractfromCaseNumber(this.appealData.cccasenumber, 'juridiction'),
        });
}

    prepareCSAdata() {
        this.appealFormGroup.patchValue({
            csacompileddate: this.appealData.csacompileddate ? new Date(this.appealData.csacompileddate) : null,
            csaarguementheld: this.appealData.csaarguementheld,
            csaarguementnotheldreason: this.appealData.csaarguementnotheldreason,
            csahearingdecisiontypekey: this.appealData.csahearingdecisiontypekey,
            csaldssname: this.appealData.csaldssname,
            csacasenumber: this.appealData.csacasenumber,
            csacasenumberlast8digit: this.extractfromCaseNumber(this.appealData.csacasenumber, 'last_8_digit'),
            csaappellentatrny: this.appealData.csaappellentatrny,
            csahearingdecisiondate: this.appealData.csahearingdecisiondate,
            csacourtdecisionflag: this.appealData.csacourtdecisionflag,
            csanotifiedtodirector: this.appealData.csanotifiedtodirector,
            csaldssnotifieddate: this.appealData.csaldssnotifieddate ? new Date(this.appealData.csaldssnotifieddate) : null,
            csaappealedflag: this.appealData.csaappealedflag,
            csadetails: this.appealData.csadetails,
            csawhoappealed: this.appealData.csawhoappealed,
            csalocationofhearing: this.appealData.csalocationofhearing,
            csajuridiction: this.extractfromCaseNumber(this.appealData.csacasenumber, 'juridiction'),
        });
    }

    prepareCOAdata() {
        this.appealFormGroup.patchValue({
            coacompileddate:  this.appealData.coacompileddate ? new Date(this.appealData.coacompileddate) : null,
            coacertioraristatus: this.appealData.coacertioraristatus,
            coacertgranteddate: this.appealData.coacertgranteddate ? new Date(this.appealData.coacertgranteddate) : null,
            coacertdenieddate: this.appealData.coacertdenieddate ? new Date(this.appealData.coacertdenieddate) : null,
            coahearingdecisiontypekey: this.appealData.coahearingdecisiontypekey,
            coaldssname: this.appealData.coaldssname,
            coacasenumber: this.appealData.coacasenumber,
            coacasenumberlast8digit: this.extractfromCaseNumber(this.appealData.coacasenumber, 'last_8_digit'),
            coaappellentatrny: this.appealData.coaappellentatrny,
            coahearingdecisiondate: this.appealData.coahearingdecisiondate,
            coacourtdecisionflag: this.appealData.coacourtdecisionflag,
            coaappealedflag: this.appealData.coaappealedflag,
            coadetails: this.appealData.coadetails,
            coalocationofhearing: this.appealData.coalocationofhearing,
            coajuridiction: this.extractfromCaseNumber(this.appealData.coacasenumber, 'juridiction'),
        });
     }

    validateAppealsFlow() {
        this.enablesc = false;
        this.enableoah = false;
        this.enablecc = false;
        this.enablecsa = false;
        this.enablecoa = false;
        this.courtdataValid.sc = (this.appealData && this.appealData.scicesentdate) ? true : false;
        this.courtdataValid.oah = (this.appealData && this.appealData.oaicesentdate) ? true : false;
        this.courtdataValid.cc = (this.appealData && this.appealData.cccompileddate) ? true : false;
        this.courtdataValid.csa = (this.appealData && this.appealData.csacompileddate) ? true : false;
        this.courtdataValid.coa = (this.appealData && this.appealData.coacompileddate) ? true : false;
        if (this.courtdataValid.sc) {
            this.enablesc = true;
            this.enableoah = true;
            this.enablecc = (this.isUnsubstantiated) ? false : true;
            this.enablecsa = (this.isUnsubstantiated) ? false : true;
            this.enablecoa = (this.isUnsubstantiated) ? false : true;
            this.showConferenceType = 5;
            this.conferenceTypeTitle = 'SC';
        }
        if (this.courtdataValid.oah) {
            this.enablesc = false;
            this.enableoah = true;
            this.enablecc = true;
            this.enablecsa = (this.isUnsubstantiated) ? false : true;
            this.enablecoa = (this.isUnsubstantiated) ? false : true;
            this.showConferenceType = 1;
            this.conferenceTypeTitle = 'OAH';
        }
        if (this.courtdataValid.cc) {
            this.enablesc = false;
            this.enableoah = false;
            this.enablecc = true;
            this.enablecsa = true;
            this.enablecoa = (this.isUnsubstantiated) ? false : true;
            this.showConferenceType = 2;
            this.conferenceTypeTitle = 'Circuit Court';
        }
        if (this.courtdataValid.csa) {
            this.enablesc = false;
            this.enableoah = false;
            this.enablecc = false;
            this.enablecsa = true;
            this.enablecoa = true;
            this.showConferenceType = 3;
            this.conferenceTypeTitle = 'CSA';
        }
        if (this.courtdataValid.coa) {
            this.enablesc = false;
            this.enableoah = false;
            this.enablecc = false;
            this.enablecsa = false;
            this.enablecoa = true;
            this.showConferenceType = 4;
            this.conferenceTypeTitle = 'COA';
        }
        if (!this.courtdataValid.sc && !this.courtdataValid.oah && !this.courtdataValid.cc && !this.courtdataValid.csa && !this.courtdataValid.coa) {
            this.courtdataValid.oah = (this.isUnsubstantiated) ? false : true;
            this.courtdataValid.sc = (this.isUnsubstantiated) ? true : false;
            this.conferenceTypeTitle = (this.isUnsubstantiated) ? 'SC' : 'OAH';
            this.showConferenceType = (this.isUnsubstantiated) ? 5 : 1;
            this.enablesc = (this.isUnsubstantiated) ? true : false;
            this.enableoah = (this.isUnsubstantiated) ? false : true;
            this.enablecc = (this.isUnsubstantiated) ? false : true;
            this.enablecsa = (this.isUnsubstantiated) ? false : true;
            this.enablecoa = (this.isUnsubstantiated) ? false : true;
        }
        this.prepareOAHdata();
        this.disableformOption();

    }

    uploadFile(file: File | FileError): void {
        if (this.showConferenceType && this.showConferenceType <= 5 && this.showConferenceType >= 1) {
            console.log('upload file', 'its true');

            if (!(file instanceof Array)) {
                return;
            }
            file.map((item, index) => {
                const fileExt = item.name
                    .toLowerCase()
                    .split('.')
                    .pop();
                if (
                    fileExt === 'mp3' ||
                    fileExt === 'ogg' ||
                    fileExt === 'wav' ||
                    fileExt === 'acc' ||
                    fileExt === 'flac' ||
                    fileExt === 'aiff' ||
                    fileExt === 'mp4' ||
                    fileExt === 'mov' ||
                    fileExt === 'avi' ||
                    fileExt === '3gp' ||
                    fileExt === 'wmv' ||
                    fileExt === 'mpeg-4' ||
                    fileExt === 'pdf' ||
                    fileExt === 'txt' ||
                    fileExt === 'docx' ||
                    fileExt === 'doc' ||
                    fileExt === 'xls' ||
                    fileExt === 'xlsx' ||
                    fileExt === 'jpeg' ||
                    fileExt === 'jpg' ||
                    fileExt === 'png' ||
                    fileExt === 'ppt' ||
                    fileExt === 'pptx' ||
                    fileExt === 'gif'
                ) {
                    this.uploadedFile.push(item);
                    const uindex = this.uploadedFile.length - 1;
                    if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
                        this.uploadedFile[uindex].percentage = 1;
                    }

                    this.uploadAttachment(uindex);
                    const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
                    const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
                    if (audio_ext.indexOf(fileExt) >= 0) {
                        this.uploadedFile[uindex].attachmenttypekey = 'Audio';
                    } else if (video_ext.indexOf(fileExt) >= 0) {
                        this.uploadedFile[uindex].attachmenttypekey = 'Video';
                    } else {
                        this.uploadedFile[uindex].attachmenttypekey = 'Document';
                    }
                } else {

                    this._alertService.error(fileExt + ' format can\'t be uploaded');
                    return;
                }
            });
        } else {
            this._alertService.warn('Please select a appeal type.');
        }
    }
    downloadFile(s3bucketpathname) {
        const workEnv = config.workEnvironment;
        let downldSrcURL;
        if (workEnv === 'state') {
            downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {

            downldSrcURL = s3bucketpathname;
        }
        console.log('this.downloadSrc', downldSrcURL);
        window.open(downldSrcURL, '_blank');
    }
    navigatetoattachements() {
        this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment/attachment-upload']);
    }
    uploadAttachment(index) {
        console.log('upload attachment', 'check');
        const workEnv = config.workEnvironment;
        let uploadUrl = '';
        if (workEnv === 'state') {
            uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
                + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber + '&' + 'docsInfo=';
            console.log('state', uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
                '&' + 'srno=' + this.daNumber;
            console.log('local', uploadUrl);
        }

        this._uploadService
            .upload({
                url: uploadUrl,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile[index],
                process: true,
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        const doucumentInfo = response.data;
                        doucumentInfo.documentdate = doucumentInfo.date;
                        doucumentInfo.title = doucumentInfo.originalfilename;
                        doucumentInfo.objecttypekey = 'investigation';
                        if (this.showConferenceType === 1) {
                            doucumentInfo.rootobjecttypekey = 'oah';
                        } else if (this.showConferenceType === 2) {
                            doucumentInfo.rootobjecttypekey = 'circuitcourt';
                        } else if (this.showConferenceType === 3) {
                            doucumentInfo.rootobjecttypekey = 'courtspecial';
                            // this.prepareCSAdata();
                        } else if (this.showConferenceType === 4) {
                            doucumentInfo.rootobjecttypekey = 'coa';
                        } else if (this.showConferenceType === 5) {
                            doucumentInfo.rootobjecttypekey = 'supervisor';
                        } else {
                            doucumentInfo.rootobjecttypekey = '';
                        }
                        doucumentInfo.activeflag = 1;
                        doucumentInfo.servicerequestid = null;
                        this.appealData.documentprop = (this.appealData && Array.isArray(this.appealData.documentprop)) ? this.appealData.documentprop : [];
                        this.appealData.documentprop.push(doucumentInfo);
                        this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                        console.log(index, this.uploadedFile[index]);
                    }

                }, (err) => {
                    console.log(err);
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    this.uploadedFile.splice(index, 1);
                }
            );
    }

    openAppealDelayPopup(appeal) {
        (<any>$('#appeal-delay-popup')).modal('show');
    }

    confirmDeleteAttachment(attachment) {
        // (<any>$('#delete-attachment-popup')).modal('show');
        this.toDeleteAttachment = attachment;
        this.deleteAttachment();
    }
    deleteAttachment() {
        this.uploadedFile = this.uploadedFile.filter(data => data.documentpropertiesid !== this.toDeleteAttachment.documentpropertiesid);

        (<any>$('#delete-attachment-popup')).modal('hide');
    }
    public findInvalidControls(form: FormGroup) {
        const invalid = [];
        const controls = form.controls;
        for (const name in controls) {
            if (controls[name].invalid) {
                invalid.push(name);
            }
        }
        console.log('invalid controls', invalid);
        return invalid;
    }

    disableformOption() {
        this.appealFormGroup.enable();
        if (!this.enablesc) {
            this.appealFormGroup.get('scsummarymailed').disable();
            this.appealFormGroup.get('scappealedsetdate').disable();
            this.appealFormGroup.get('scisappealformsent').disable();
            this.appealFormGroup.get('scicesentdate').disable();
            this.appealFormGroup.get('scconfheldflag').disable();
            this.appealFormGroup.get('scdecisiontypekey').disable();
            this.appealFormGroup.get('scconferencedetail').disable();
            this.appealFormGroup.get('scconferencedate').disable();
            this.appealFormGroup.get('scappealed').disable();
            this.appealFormGroup.get('scisappealed').disable();
            this.appealFormGroup.get('scappealedby').disable();
            this.appealFormGroup.get('scappealeddate').disable();
        }

        if (!this.enableoah) {
            this.appealFormGroup.get('oahearingdatesetflag').disable();
            this.appealFormGroup.get('oanorhearingreason').disable();
            this.appealFormGroup.get('oaldssname').disable();
            this.appealFormGroup.get('oaappellentatrny').disable();
            this.appealFormGroup.get('oalocaldept').disable();
            this.appealFormGroup.get('oarunningmotion').disable();
            this.appealFormGroup.get('oaicesentdate').disable();
            this.appealFormGroup.get('oahearingdate').disable();
            this.appealFormGroup.get('oahearingdecision').disable();
            this.appealFormGroup.get('oahearingdecisiondate').disable();
            this.appealFormGroup.get('oadetails').disable();
            this.appealFormGroup.get('oaappealedflag').disable();
            this.appealFormGroup.get('oacasenumber').disable();
            this.appealFormGroup.get('oacasenumberlast8digit').disable();
            this.appealFormGroup.get('oahearingnarrative').disable();
            this.appealFormGroup.get('oamodificationsmade').disable();
            this.appealFormGroup.get('oarunningmotiondate').disable();
            this.appealFormGroup.get('oamaltreatorunnamedflag').disable();
            // this.appealFormGroup.get('oamaltreatorname').disable();
            this.appealFormGroup.get('oatranslator').disable();
            this.appealFormGroup.get('oahearingheld').disable();
            this.appealFormGroup.get('oahearingheldreason').disable();
            this.appealFormGroup.get('oalocationofhearing').disable();
            this.appealFormGroup.get('oasummarydecisionfiledflag').disable();
            this.appealFormGroup.get('oasummarydecisionfileddate').disable();
            this.appealFormGroup.get('oajuridiction').disable();
            this.appealFormGroup.get('oacompiledwithoah').disable();
        }

        if (!this.enablecc) {
            this.appealFormGroup.get('ccldssname').disable();
            this.appealFormGroup.get('ccappellentatrny').disable();
            this.appealFormGroup.get('ccstayrequestedflag').disable();
            this.appealFormGroup.get('ccstaygrantedflag').disable();
            this.appealFormGroup.get('ccappealedflag').disable();
            this.appealFormGroup.get('cchearingdecisiontypekey').disable();
            this.appealFormGroup.get('cchearingdecisiondate').disable();
            this.appealFormGroup.get('ccdetails').disable();
            this.appealFormGroup.get('cccasenumber').disable();
            this.appealFormGroup.get('cccasenumberlast8digit').disable();
            this.appealFormGroup.get('cccourtdecisionflag').disable();
            this.appealFormGroup.get('cccompileddate').disable();
            this.appealFormGroup.get('ccwhoappealed').disable();
            this.appealFormGroup.get('ccnotifiedtodirector').disable();
            this.appealFormGroup.get('ccldssnotifieddate').disable();
            this.appealFormGroup.get('cccicuitcourtkey').disable();
            this.appealFormGroup.get('cchearingheld').disable();
            this.appealFormGroup.get('cchearingreason').disable();
            this.appealFormGroup.get('cclocationofhearing').disable();
            this.appealFormGroup.get('ccjuridiction').disable();
        }
        if (!this.enablecsa) {
            this.appealFormGroup.get('csaldssname').disable();
            this.appealFormGroup.get('csaappellentatrny').disable();
            this.appealFormGroup.get('csanotifiedtodirector').disable();
            this.appealFormGroup.get('csacertiorari').disable();
            this.appealFormGroup.get('csastayrequestedflag').disable();
            this.appealFormGroup.get('csastaygrantedflag').disable();
            this.appealFormGroup.get('csaappealedflag').disable();
            this.appealFormGroup.get('csahearingdecisiontypekey').disable();
            this.appealFormGroup.get('csahearingdecisiondate').disable();
            this.appealFormGroup.get('csadetails').disable();
            this.appealFormGroup.get('csacasenumber').disable();
            this.appealFormGroup.get('csacasenumberlast8digit').disable();
            this.appealFormGroup.get('csacourtdecisionflag').disable();
            this.appealFormGroup.get('csacompileddate').disable();
            this.appealFormGroup.get('csawhoappealed').disable();
            this.appealFormGroup.get('csaldssnotifieddate').disable();
            this.appealFormGroup.get('csaarguementheld').disable();
            this.appealFormGroup.get('csaarguementnotheldreason').disable();
            this.appealFormGroup.get('csalocationofhearing').disable();
            this.appealFormGroup.get('csajuridiction').disable();
        }
        if (!this.enablecoa) {
            this.appealFormGroup.get('coaldssname').disable();
            this.appealFormGroup.get('coaappellentatrny').disable();
            this.appealFormGroup.get('coastayreqflag').disable();
            this.appealFormGroup.get('coastaygrantedflag').disable();
            this.appealFormGroup.get('coaappealedflag').disable();
            this.appealFormGroup.get('coahearingdecisiontypekey').disable();
            this.appealFormGroup.get('coahearingdecisiondate').disable();
            this.appealFormGroup.get('coadetails').disable();
            this.appealFormGroup.get('coacase').disable();
            this.appealFormGroup.get('coacourtdecisionflag').disable();
            this.appealFormGroup.get('coacompileddate').disable();
            this.appealFormGroup.get('coacasenumber').disable();
            this.appealFormGroup.get('coacasenumberlast8digit').disable();
            this.appealFormGroup.get('coacertioraristatus').disable();
            this.appealFormGroup.get('coacertgranteddate').disable();
            this.appealFormGroup.get('coacertdenieddate').disable();
            this.appealFormGroup.get('coalocationofhearing').disable();
            this.appealFormGroup.get('coajuridiction').disable();
        }

        // this.appealFormGroup.get('hearingdecision')
        // this.appealFormGroup.get('hearingdecisiondate')
        // this.appealFormGroup.get('details')
        // this.appealFormGroup.get('casenumber')
        // this.appealFormGroup.get('appealedflag')
        // this.appealFormGroup.get('stayrequestedflag')
        // this.appealFormGroup.get('staygrantedflag')
        // this.appealFormGroup.get('hearingdecisiontypekey')
        // this.appealFormGroup.get('courtdecisionflag')
        // this.appealFormGroup.get('compileddate')
        // this.appealFormGroup.get('overridedate')
        // this.appealFormGroup.get('overrideby')
        // this.appealFormGroup.get('overridefindingtypekey')
        // this.appealFormGroup.get('overridecomments')
        // this.appealFormGroup.get('overrideapprflag')
        // this.appealFormGroup.get('isvictim')
        // this.appealFormGroup.get('type')
        // this.appealFormGroup.get('sentdate')
        // this.appealFormGroup.get('hearingdate')
        // this.appealFormGroup.get('clientroleid')
    }

    showFinalizeSection() {
        this.showFinalize = true;
    }

    onChangesummaryDecision() {
        this.appealFormGroup.patchValue({'oarunningmotion': null});
        this.appealFormGroup.patchValue({'oasummarydecisionfileddate': null});
    }

    onChangemaltreatorUnnamed(event, formControlName) {
         if (event.checked) {
             const obj = {};
             obj[formControlName] = 'Unnamed';
             this.appealFormGroup.patchValue(obj);
         }
    }

    dispositionHistory() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    where: {
                        servicerequestid: this.id
                    },
                    method: 'get'
                }),
                'Intakeservicerequestdispositioncodes/GetHistory?filter'
            )
            .subscribe(
                (result) => {
                    const history = (result.data) ? result.data : [];
                    this.showAppeal = false;
                    if (history && history.length > 0) {
                        const isCompleted = history.find(dispo => (dispo.dispstatus === 'Completed') && (dispo.routingstatus === 'Approved'));
                        if (isCompleted) {
                        this.showAppeal = true;
                        }
                    }


                    return result.data;
                }
            );
    }

    getReviewStatus(formControl) {
        const investigationfindingid =  formControl.investigationfindingid.value ?  formControl.investigationfindingid.value  : null;
        if ( this.ReviewStatus && this.ReviewStatus[investigationfindingid] ) {
            return this.ReviewStatus[investigationfindingid];
        }
    }

    setReviewStatus(investigationfindingid) {

            this._commonHttpService.getSingle(
                    {
                      count: -1,
                      limit: 10,
                      page: 1,
                      method: 'get',
                      where: { investigationfindingid: investigationfindingid}
                    }
                    , 'expungement/getexpungement?filter').subscribe((res) => {
                    if (res && res.length && res[0].getexpungement && res[0].getexpungement.length) {
                        const length = res[0].getexpungement.length;
                        const expungement = res[0].getexpungement[length - 1];
                        // const expungement = res[0].getexpungement[0];
                        this.ReviewStatus[investigationfindingid] = expungement.status;
                    // return expungement.status;
                    //   switch(expungement.routingstatustypeid) {
                    //       case 15:  return  'Review';

                    //       case 16:  return  'Approved';

                    //       case 17:  return  'Rejected';
                    //       default: return null;

                    //   }

                    }
                    // return null;

                });
                // return null;

    }
    resetExpungementValues() {
        this.expungementForm.reset();
        this.approvalStatus = null;
        this.investigationfindingid = null;
        this.isExpungementExist = false;
        this.expungementForm.enable();
        this.showExpungement = false;
    }
    loadExpungement(formControl) {
        this.resetExpungementValues();
        const intakeservicerequestactorid = formControl.intakeservicerequestactorid.value ? formControl.intakeservicerequestactorid.value : null;
        const reporteddate =  formControl.reporteddate.value ? formControl.reporteddate.value : null;
        let hearingDecision =  formControl.maltreatmentkey1.value ?  formControl.maltreatmentkey1.value  : null;
        hearingDecision =  formControl.maltreatmentkey2.value ?  formControl.maltreatmentkey2.value  : hearingDecision;
        hearingDecision =  formControl.maltreatmentkey3.value ?  formControl.maltreatmentkey3.value  : hearingDecision;
        const maltreatmentkey = formControl.maltreatmentkey.value ?  formControl.maltreatmentkey.value  : null;
        const maltreatmentkey1 = formControl.maltreatmentkey1.value ?  formControl.maltreatmentkey1.value  : null;
        const maltreatmentkey2 = formControl.maltreatmentkey2.value ?  formControl.maltreatmentkey2.value  : null;
        const maltreatmentkey3 = formControl.maltreatmentkey3.value ?  formControl.maltreatmentkey3.value  : null;
        const investigationfindingid =  formControl.investigationfindingid.value ?  formControl.investigationfindingid.value  : null;
        const maltreatmentid =  formControl.maltreatmentid.value ?  formControl.maltreatmentid.value  : null;
        this.expungementForm.patchValue({investigationfinding: maltreatmentkey1});
        this.expungementForm.patchValue({appealfinding: maltreatmentkey2});
        this.expungementForm.patchValue({finalfinding: maltreatmentkey3});
        this.expungementForm.patchValue({ maltreatmentid: maltreatmentid});
        this.expungementForm.patchValue({ investigationfindingid: investigationfindingid});
        this.expungementForm.patchValue({ intakeservicerequestactorid: intakeservicerequestactorid});
        if (investigationfindingid) {
        this.investigationfindingid = investigationfindingid;
        this.getExpungement(investigationfindingid);
        // this.setReviewStatus(investigationfindingid);
        } else {
            this.expungmentMessage = 'Cannot Expunge as the Investigation is not completed.';
            (<any>$('#expunge-alert')).modal('show');
            return;
        }
        switch (hearingDecision) {
            case 'RO':  this.showExpungement = true;
                        break;
            case 'UD':  const ElapedYears = this.getElapedYears(reporteddate);
                        if (ElapedYears && ElapedYears >=  5) {
                         this.showExpungement = true;
                        } else {
                        this.expungmentMessage = 'You cannot Expunge this record.';
                        (<any>$('#expunge-alert')).modal('show');
                        }
                        break;
            case 'ID':  this.expungmentMessage = 'You cannot Expunge this record.';
                        (<any>$('#expunge-alert')).modal('show');
                        break;
        }


    }
    getElapedYears(reportedDate) {
        if (reportedDate) {
        const reportedDateFormatted = new Date(reportedDate);
        const yearsDiff = Date.now() - reportedDateFormatted.getTime();
        const date = new Date(yearsDiff); // miliseconds from epoch
        return Math.abs(date.getUTCFullYear() - 1970);
        } else {
            return null;
        }
    }

    investigationFindingDesc(id) {
        if (id && this.investigationFindingDropDown && this.investigationFindingDropDown.length) {
            const finding = this.investigationFindingDropDown.filter(item => item.investigationfindingtypekey === id);
            if (finding && finding.length) {
                return finding[0].description;
            }
        } else {
            return null;
        }
    }
    resetExpungement() {
        this.showExpungement  = false;
        this.expungementForm.enable();
        this.expungementForm.reset();
    }

    resetExpungementForm() {
        this.expungementForm.enable();
        this.expungementForm.patchValue({expungementStatus : null});
        this.expungementForm.patchValue({donotexpunge: null});
        this.expungementForm.patchValue({manualexpunge: null});
        this.expungementForm.patchValue({resultoflawenforcement: null});
        this.expungementForm.patchValue({investigationnarrative: null});
    }

    getExpungement(investigationId) {

        this._commonHttpService.getSingle(
            {
                count: -1,
                limit: 10,
                page: 1,
                method: 'get',
                where: { investigationfindingid: investigationId }
            }
            , 'expungement/getexpungement?filter').subscribe((res) => {
                if (res && res.length && res[0].getexpungement && res[0].getexpungement.length) {
                    const length = res[0].getexpungement.length;
                    const expungement = res[0].getexpungement[length - 1];
                    if (expungement && expungement.status && expungement.status !== 'Review') {
                        this.approvalStatus = expungement.status;
                    }
                    if (expungement && expungement.expungementid) {
                        const isunsubstansiated = expungement.isunsubstansiated;
                        const isindicated = expungement.isindicated;
                        const isremovemaltreator = expungement.isremovemaltreator;
                        let expungementStatus = null;
                        if (isunsubstansiated) {
                            expungementStatus = 'isunsubstansiated';
                        } else if (isindicated) {
                            expungementStatus = 'isindicated';
                        } else if (isremovemaltreator) {
                            expungementStatus = 'isremovemaltreator';
                        }

                        this.expungementForm.patchValue({ expungementStatus: expungementStatus });
                        this.expungementForm.patchValue(expungement);
                        // if (expungement && expungement.status !== 'Approved' && expungement.status !== 'Review') {
                        //     this.isExpungementExist = false;
                        // } else {
                            this.isExpungementExist = true;
                            this.expungementForm.disable();
                        // }
                    }
                } else {
                    this.approvalStatus = 'No Data';
                }

            });

    }

    confirmExpungement() {
        this.isExpungementExist = true;
        const investigationfinding = this.expungementForm.getRawValue().investigationfinding;
        const appealfinding = this.expungementForm.getRawValue().appealfinding;
        const finalfinding = this.expungementForm.getRawValue().finalfinding;
        const intakeservicerequestactorid = this.expungementForm.getRawValue().intakeservicerequestactorid;
        let finalFindingValue = finalfinding;
        if (!finalFindingValue) {
            finalFindingValue = appealfinding ? appealfinding : investigationfinding;
        }
        this.expungementForm.patchValue({intakeservicerequestactorid : intakeservicerequestactorid});
        this.expungementForm.patchValue({ intakeserviceid: this.id});
        this.expungementForm.patchValue({ investigationfindingid : this.investigationfindingid});
        this.expungementForm.patchValue({finalfinding: finalFindingValue});
        const expungementStatus =  this.expungementForm.getRawValue().expungementStatus;
        switch (expungementStatus) {
            case 'isunsubstansiated' :
                                      this.expungementForm.patchValue({isunsubstansiated: true});
                                      break;
            case 'isindicated' :
                                      this.expungementForm.patchValue({isindicated: true});
                                      break;
            case 'isremovemaltreator' :
                                      this.expungementForm.patchValue({isremovemaltreator: true});
                                      break;
            default:
                   break;
        }
        const expungeFormData = this.expungementForm.getRawValue();
        // console.log('Expunge Data :::::::', expungeFormData);

        if (this.expungementForm.valid) {
            this._commonHttpService.create(expungeFormData, 'expungement/addupdate').subscribe(
                (result) => {
                    this._alertService.success('Expungement saved successfully!');
                    if (this.investigationfindingid) {
                        this.setReviewStatus(this.investigationfindingid);
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    this.isExpungementExist = false;
                }
            );
        }
    }

    onChangeExpungementStatus() {
        const expungementStatus =  this.expungementForm.getRawValue().expungementStatus;
        if (expungementStatus) {
        this.expungementForm.patchValue({ 'donotexpunge' : null});
        }
    }

    ngOnDestroy() {
        this._speechRecognitionService.destroySpeechObject();
    }

    activateSpeechToText(type): void {
        this.recognizing = type;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
            this._speechRecognitionService.record().subscribe(
                // listener
                value => {
                    this.speechData = value;
                    switch (type) {
                        case 'resultoflawenforcement':
                          const resultoflawenforcement = this.expungementForm.getRawValue().resultoflawenforcement;
                          this.expungementForm.patchValue({ resultoflawenforcement: resultoflawenforcement + ' ' + this.speechData });
                          break;
                        case 'investigationnarrative':
                          const investigationnarrative = this.expungementForm.getRawValue().investigationnarrative;
                          this.expungementForm.patchValue({ investigationnarrative: investigationnarrative + ' ' + this.speechData });
                          break;

                        default: break;
                      }
                    // this.recordingForm.patchValue({ description: this.speechData });

                },
                // errror
                err => {
                    console.log(err);
                    this.recognizing = false;
                    if (err.error === 'no-speech') {
                        this.notification = `No speech has been detected. Please try again.`;
                        this._alertService.warn(this.notification);
                        this.activateSpeechToText(type);
                    } else if (err.error === 'not-allowed') {
                        this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    } else if (err.error === 'not-microphone') {
                        this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    }
                },
                // completion
                () => {
                    this.speechRecogninitionOn = true;
                    console.log('--complete--');
                    this.activateSpeechToText(type);
                }
            );
        } else {
            this.recognizing = false;
            this.deActivateSpeechRecognition();
        }
    }
    deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
    }

    routingUpdate(status) {
        const objId = this._session.getItem('Expungement_Obj_ID');
        const comment = 'Expungment ' + status + ' Sucessfully';
        const routingObject = {
            objectid: objId ? objId : null,
            eventcode: 'EXPR',
            status: status,
            comments: comment,
            notifymsg: comment,
            routeddescription: comment
        };
        this.approvalStatus = status;
        this._commonHttpService.create(routingObject, 'routing/routingupdate').subscribe((res) => {
            this._alertService.success( 'Expungment ' + status + ' Sucessfully');
            if (this.investigationfindingid) {
                this.setReviewStatus(this.investigationfindingid);
            }
        }, (error) => {
            console.log(error);
        });
    }
    changeSupervisor($event) {
        console.log($event);
        if ($event && $event.value) {
            this._commonHttpService.create({
                appeventcode: 'INVT',
                objectid: this.id,
                fromuserid: $event.value
            }, 'routing/changereviewer')
                .subscribe(result => {
                    console.log('supervisor changed', result);
                });

        }

    }



}
