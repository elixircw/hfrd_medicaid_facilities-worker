export class ComarFindings {
  public static ComarFindingsList = {
    'SEXUAL-ABUSEID': {
      value: 'Sexual Abuse of ${comar?.victimname} is "INDICATED" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(b)(2), (m), (y)(1) and COMAR 07.02.07.12A(2). It is the professional assessment of this worker that \
      sexual abuse occurred because the evidence supports all of the following elements:<br>\
      1)An act that involves sexual molestation or sexual exploitation (describe in detail):${findingcomments}<br>\
      2)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      3)The act involving sexual molestation or exploitation was by a parent, caregiver, authority figure or by a household or family member \
      (insert name and whether the person is a parent,OR caregiver, OR authority figure, OR household OR family member): ${comar?.caretakername}<br>'},
    'SEXUAL-ABUSERO': {
      value: 'Sexual Abuse of ${comar?.victimname} is "RULED OUT" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(b)(2), (w), (y)(1) and COMAR 07.02.07.12C. It is the professional assessment of this worker that sexual\
      abuse did not occur because the evidence does not support one or more of the following elements: (Indicate which element \
      or elements were not met in a bulleted format or short narrative.)<br>\
      1)An act that involves sexual molestation or sexual exploitation (describe in detail):${findingcomments}<br>\
      2)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      3)The act involving sexual molestation or exploitation was by a parent, caregiver, authority figure or by a household or family member\
      (insert name and whether the person is a parent, OR caregiver, OR authority figure, OR household OR family member): ${comar?.caretakername}<br>'},
    'SEXUAL-ABUSEUD': {
      value: 'Sexual Abuse of ${comar?.victimname} is "UNSUBSTANTIATED" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(b)(2), (y)(1), (aa) and COMAR 07.02.07.12B. It is the professional assessment of this worker that some credible\
      evidence supports each of the following elements but is insufficient to conclude that abuse occurred: (Specify which element or \
      elements caused the finding to be "unsubstantiated" and not "indicated" or "ruled out".)<br>\
      1)An act that involves sexual molestation or sexual exploitation (describe in detail):${findingcomments}<br>\
      2)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      3)The act involving sexual molestation or exploitation was by a parent, caregiver, authority figure or by a household or family member\
      (insert name and whether the person is a parent,OR caregiver, OR authority figure, OR household OR family member): ${comar?.caretakername}<br>'},
    'SEX-TRAFFICKING-INDICATED': {
      value: 'Sexual Abuse – Sex Trafficking of ${comar?.victimname} is "INDICATED" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(b)(2), (m), (x),(y)(2) and COMAR 07.02.07.12A(2). It is the professional assessment of this worker that sexual abuse \
      occurred because the evidence supports all of the following elements:<br>\
      1)An act that involves sex trafficking (describe in detail):${findingcomments}<br>\
      2)The victim was a child (child’s DOB): ${comar?.victimdob}<br>'},
    'SEX-TRAFFICKING-RULED-OUT': {
      value: 'Sexual Abuse – Sex Trafficking of ${comar?.victimname} is "RULED OUT" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(b)(2), (W), (x),(y)(2) and COMAR 07.02.07.11C. It is the professional assessment of this worker that sexual abuse did \
      not occur because the evidence does not support one or more of the following elements: (Indicate which element or elements were not supported\
      in a bulleted format or short narrative)<br>\
      1)An act that involves sex trafficking (describe in detail):${findingcomments}<br>\
      2)The victim was a child (child’s DOB): ${comar?.victimdob}<br>'},
    'SEX-TRAFFICKING-UNSUBSTANTIATED': {
      value: 'Sexual Abuse – Sex Trafficking of ${comar?.victimname} is "UNSUBSTANTIATED " in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(b)(2), (x),(y)(2), (aa) and COMAR 07.02.07.11B.It is the professional assessment of this worker that some credible evidence \
      supports each of the following elements but is insufficient to conclude that abuse occurred: (Specify which element or elements caused the finding\
      to be "unsubstantiated" and not "indicated" or "ruled out")<br>\
      1)An act that involves sex trafficking (describe in detail):${findingcomments}<br>\
      2)The victim was a child (child’s DOB): ${comar?.victimdob}<br>'},
    'PHYSICAL-ABUSEUD': {
      value: 'Physical Abuse of ${comar?.victimname} is "UNSUBSTANTIATED" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(b)(1), (aa) and COMAR 07.02.07.12B. It is the professional assessment of this worker that some credible evidence\
      supports each of the following elements but is insufficient to conclude that abuse occurred: (Specify the element or elements that caused\
      the finding to be "unsubstantiated" and not "indicated" or "ruled out").<br>\
      1)A physical injury (briefly describe the current orprior injury):${findingcomments}<br>\
      2)The injury was caused by a parent, caregiver, authority figure, or household or family member (insert name and whether the person\
      is a parent, OR caregiver, OR authority figure, OR household OR family member):${comar?.caretakername}<br>\
      3)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      4)Circumstances including the nature, extent, and location of the injury indicate (describe either OR both using a bulleted format or short narrative)<br>\
        i)Harm to a child: ${harmchild} <br>\
        ii)During the incident, a substantial risk of harm to a child: ${riskofharm} <br> ${circumstancescomments}<br>\
      5)The injury was not caused by accidental means (provide details supporting either an intentional act or reckless disregard for the child’s health or \
      welfare causing the injury): ${intentionalinjurydesc}'},
    'PHYSICAL-ABUSERO': {
      value: 'Physical Abuse of ${comar?.victimname} is "RULED OUT" in accordance with the provisions of Maryland Code Ann., Fam. Law§ 5-701(b)(1), (w), \
      and COMAR 07.02.07.12C. It is the professional assessment of this worker that physical abuse did not occur because the evidence does not support one \
      or more of the following elements: (Indicate which element or elements were not supported in a bulleted format orshort narrative.)<br>\
      1)A physical injury (briefly describe the current orprior injury):${findingcomments}<br>\
      2)The injury was caused by a parent, caregiver, authority figure, or household or family member(insert name and whether the person is a parent, OR caregiver,\
      OR authority figure, OR household OR family member):${comar?.caretakername}<br>\
      3)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      4)Circumstances including the nature, extent, and location of the injury indicate (describe either OR both using a bulleted format or short narrative)<br>\
        i)Harm to a child: ${harmchild} <br> \
        ii)During the incident, a substantial risk of harm to a child: ${riskofharm} <br> ${circumstancescomments}<br>\
      5)The injury was not caused by accidental means (provide details supporting either an intentional act or reckless disregard for the child’s health or welfare\
      causing the injury): ${intentionalinjurydesc}<br>'},
    'PHYSICAL-ABUSEID': {
      value: 'Physical Abuse of ${comar?.victimname} is "INDICATED" in accordance with the provisions of Maryland Code Ann.,Fam. Law § 5-701(b)(1),\
      (m) and COMAR 07.02.07.12A(1). It is the professional assessment of this worker that physical abuse occurred because the evidence supports all of\
      the following elements:<br>\
      1)A physical injury (briefly describe the current orprior injury):${findingcomments}<br>\
      2)The injury was caused by a parent, caregiver, authority figure, or household or family member (insert name and whether\the person is a parent, OR caregiver,\
      OR authority figure, OR household OR family member):${comar?.caretakername}<br>\
      3)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      4)Circumstances including the nature, extent, and location of the injury indicate (describe either OR both using a bulleted format or short narrative)<br>\
        i)Harm to a child: ${harmchild} <br> \
        ii)During the incident, a substantial risk of harm to a child: ${riskofharm} <br> ${circumstancescomments}<br>\
      5)The injury was not caused by accidental means (provide details supporting either an intentional act or reckless disregard for the child’s health or welfare\
      causing the injury): ${intentionalinjurydesc}<br>'},
    'NEGLECTUD': {
      value: 'Neglect of ${comar?.victimname} is "UNSUBSTANTIATED" in accordance with the provisions of Maryland Code Ann., Fam. Law § 5-701(s),(aa) and COMAR COMAR 07.02.07.13B.\
      It is the professional assessment of this worker that some credible evidence supports each of the following elements but is insufficient to \
      conclude that neglect occurred: (Specify which element or elements caused the finding to be "unsubstantiated" and not "indicated" or "ruled out").<br>\
      1)A failure to provide proper care including leaving a child unattended (briefly describe the lack ofproper care or attention):${findingcomments}<br>\
      2) The failure was caused by a parent or caregiver (insert name and indicate whether the person is a parent OR caregiver): ${comar?.caretakername}<br>\
      3)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      4)Circumstances including the nature, extent, and causeof the alleged neglect indicate that there was (describe either OR both using a bulleted format or short narrative)<br>\
      i)Harm to a child: ${harmchild} <br> \
      ii)During the incident, a substantial risk of harm to a child: ${riskofharm}<br> ${circumstancescomments}<br>'},
    'NEGLECTRO': {
      value: 'Neglect of ${comar?.victimname} is "RULED OUT" in accordance with the provisions of Maryland Code Ann., Fam. Law$ 5-701(s), (w) and 07.02.07.13C.\
      It is the professional assessment of this worker that neglect did not occur because the evidence does not support one or more ofthe following\
      elements: (Indicate which element or elements were not supported in a bulleted format or short narrative.)<br>\
      1)A failure to provide proper care including leaving a child unattended (briefly describe the lack ofproper care or attention):${findingcomments}<br>\
      2) The failure was caused by a parent or caregiver (insert name and indicate whether the person is a parent OR caregiver): ${comar?.caretakername}<br>\
      3)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      4)Circumstances including the nature, extent, and causeof the alleged neglect indicate that there was (describe either OR both using a bulleted format or short narrative)<br>\
      i)Harm to a child: ${harmchild} <br>\
      ii)During the incident, a substantial risk of harm to a child: ${riskofharm}<br> ${circumstancescomments}<br>'},
    'NEGLECTID': {
      value: 'Neglect of ${comar?.victimname} is "INDICATED" in accordance with the provisions of Maryland Code Ann., Fam. Law $ 5-701(m), (s) and COMAR 07.02.07.13A(1).\
      It is the professional assessment of this worker that neglect occurred because the evidence supports all of the following elements:<br>\
      1)A failure to provide proper care including leaving a child unattended (briefly describe the lack ofproper care or attention):${findingcomments}<br>\
      2) The failure was caused by a parent or caregiver (insert name and indicate whether the person is a parent OR caregiver): ${comar?.caretakername}<br>\
      3)The victim was a child (child’s DOB): ${comar?.victimdob}<br>\
      4)Circumstances including the nature, extent, and causeof the alleged neglect indicate that there was (describe either OR both using a bulleted format or short narrative)<br>\
      i)Harm to a child: ${harmchild} <br>\
      ii)During the incident, a substantial risk of harm to a child: ${riskofharm} <br> ${circumstancescomments}<br>'},
    'MENTAL-INJURY-ABUSE-ID': {
      value: 'Mental Injury Abuse of ${comar?.victimname} is "INDICATED" in accordance with the provisions of Maryland Code Ann., Fam. Law § 5-701(b)(1),\
      (m), (r) and COMAR 07.02.07.12A(3). It is the professional assessment of this worker that mental injury abuse occurred because the evidence supports \
      all of the following elements:<br>\
      1)An injury characterized by an observable, identifiable, and substantial impairment of a mental or psychological ability to function (explain the impairment):${findingcomments}<br>\
      2)The injury was caused by an intentional act or series of intentional acts (regardless of whether there was an intent to harm the child)(provide details of an intentional act or \
      series of acts that caused the impairment): ${intentionalinjurydesc}<br>\
      3)The act was caused by a parent, caregiver, authority figure, or household or family member (insert name and whether the person is a parent,OR caregiver, OR authority figure, OR \
      household OR family member):${comar?.caretakername}<br>\
      4)The victim was a child under the age of 18 (child’s DOB): ${comar?.victimdob}<br>'},
    'MENTAL-INJURY-ABUSE-RO': {
      value: 'Mental Injury Abuse of ${comar?.victimname} is "RULED OUT" in accordance with the provisions of Maryland Code Ann., Fam. Law § 5-701(b)(1),\
      (r), (w) and COMAR 07.02.07.12C(1).It is the professional assessment of this worker that mental injury abuse did not occur because the evidence does not\
      support one or more of the following elements: (Indicate which element or elements were not supported in a bulleted format or short narrative.)<br>\
      1)An injury characterized by an observable, identifiable, and substantial impairment of a mental or psychological ability to function (explain the impairment):${findingcomments}<br>\
      2)The injury was caused by an intentional act or series of intentional acts (regardless of whether there was an intent to harm the child) (provide details of an intentional act or\
      series of acts that caused the impairment): ${intentionalinjurydesc}<br>\
      3)The act was caused by a parent, caregiver, authority figure, or household or family member (insert name and whether the person is a parent,OR caregiver, OR authority figure,\
      OR household OR family member):${comar?.caretakername}<br>\
      4)The victim was a child under the age of 18 (child’s DOB): ${comar?.victimdob}<br>'},
    'MENTAL-INJURY-ABUSE-UD': {
      value: 'Mental Injury Abuse of ${comar?.victimname} is "UNSUBSTANTIATED" in accordance with the provisions of Maryland Code Ann., Fam. Law § 5-701(b)(1),\
      (r), (aa) and 07.02.07.12B. It is the professional assessment of this worker that some credible evidence supports each of the following elements bus is \
      insufficient to conclude that mental injury abuse occurred: (Specify which element or elements caused the finding to be "unsubstantiated" and not "indicated" or "ruled out")<br>\
      1)An injury characterized by an observable, identifiable, and substantial impairment of a mental or psychological ability to function (explain the impairment):${findingcomments}<br>\
      2)An intentional act or series of intentional acts (regardless of whether there was an intent to harm the child) caused the injury (provide details of a current or prior act that\
      caused the impairment): ${intentionalinjurydesc}<br>\
      3)The act was caused by a parent, caregiver, authority figure, or household or family member (insert name and whether the person is a parent,\
      OR caregiver, OR authority figure, OR household OR family member):${comar?.caretakername}<br>\
      4)The victim was a child under the age of 18 (child’s DOB): ${comar?.victimdob}<br>'},
    'MENTAL-INJURY-NEGLECT-ID': {
      value: 'Mental Injury Neglect of ${comar?.victimname} is "INDICATED" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(m), (r), (s)(2) and COMAR 07.02.07.13A(2).It is the professional assessment of this worker that mental injury \
      neglect occurred because the evidence supports all of the following elements:<br>\
      1)An injury characterized by an observable, identifiable, and substantial impairment of a mental or psychological ability to function (explain the impairment):${findingcomments}<br>\
      2)An omission or other failure to provide proper care or attention causing the substantial impairment (provide details of the current or\
      prior omission or other failure): ${intentionalinjurydesc}<br>\
      3) The substantial impairment was caused by a parent, caregiver, or household or family member (insert name and whether the person is a parent, OR caregiver,\
      OR household, OR family member):${comar?.caretakername}<br>\
      4)The victim was a child (child’s DOB): ${comar?.victimdob}<br>'},
    'MENTAL-INJURY-NEGLECT-RO': {
      value: 'Mental Injury Neglect of ${comar?.victimname} is "RULED OUT" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(r), (s)(2), (w) and COMAR COMAR 07.02.07.13C. It is the professional assessment of this worker that mental injury\
      neglect did not occur because the evidence does not support one or more of the following elements: (Indicate which element or elements\
      were not met in a bulleted format or short narrative.)<br>\
      1)An injury characterized by an observable, identifiable, and substantial impairment of a mental or psychological ability to function (explain the impairment):${findingcomments}<br>\
      2)An omission or other failure to provide proper care or attention causing the substantial impairment (provide details of the current or\
      prior omission or other failure): ${intentionalinjurydesc}<br>\
      3) The substantial impairment was caused by a parent, caregiver, or household or family member (insert name and whether the person is a parent, OR caregiver,\
      OR household, OR family member):${comar?.caretakername}<br>\
      4)The victim was a child (child’s DOB): ${comar?.victimdob}<br>'},
    'MENTAL-INJURY-NEGLECT-UD': {
      value: 'Mental Injury Neglect of ${comar?.victimname} is "UNSUBSTANTIATED" in accordance with the provisions of Maryland Code Ann.,\
      Fam. Law § 5-701(r), (s)(2), (aa) and COMAR 07.02.07.13B. It is the professional assessment of this worker that there is some but \
      insufficient credible evidence to support a finding of either "indicated" or "ruled out" mental injury abuse because the following elements\
      were either present or were supported by some, but insufficient, evidence: (Specify which elements caused the finding to be "unsubstantiated" and not "indicated" or "ruled out".)<br>\
      1)An injury characterized by an observable, identifiable, and substantial impairment of a mental or psychological ability to function (explain the impairment):${findingcomments}<br>\
      2)An omission or other failure to provide proper care or attention causing the substantial impairment (provide details of the current or\
      prior omission or other failure): ${intentionalinjurydesc}<br>\
      3) The substantial impairment was caused by a parent, caregiver, or household or family member (insert name and whether the person is a parent, OR caregiver,\
      OR household, OR family member):${comar?.caretakername}<br>\
      4)The victim was a child (child’s DOB): ${comar?.victimdob}<br>'}

  };
}
