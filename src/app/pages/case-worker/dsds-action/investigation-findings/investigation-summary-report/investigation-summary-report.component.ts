import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as jsPDF from 'jspdf';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, ValidationService, AlertService } from '../../../../../@core/services';
import { DSDSActionSummary } from '../../../../providers/_entities/provider.data.model';
import { DataStoreService } from '../../../../../@core/services/data-store.service';
import { DsdsService } from '../../_services/dsds.service';
import { Observable } from 'rxjs/Observable';
import { InvolvedPerson } from '../../involved-persons/_entities/involvedperson.data.model';
import { HttpService } from '../../../../../@core/services/http.service';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import {PersonComar } from '../_entities/investigation-finding-data.models';
import {ComarFindings} from './investigation-summary-report-config';
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
    selector: 'investigation-summary-report',
    templateUrl: './investigation-summary-report.component.html',
    styleUrls: ['./investigation-summary-report.component.scss']
})
export class InvestigationSummaryReportComponent implements OnInit {
    id: string;
    currentDate = new Date();
    @Input() investigationFind: any;
    @Input() addedpersons: any;
    dsdsActionsSummary = new DSDSActionSummary();
    isServiceCase: string;
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    reporter_nm: any;
    dsdsobject: any;
    involevedPerson$: Observable<InvolvedPerson[]>;
    personComar = new PersonComar();
    selectedallegedperson: any;
    selectedmaltreator: any;
    supervisorApprovalDetails= [];
    emailForm: FormGroup;
    reportcomarfindings: any;
    comarFindingsKey: any;

    constructor(private _commonHttpService: CommonHttpService,
        private _http: HttpService,
        private formBuilder: FormBuilder,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService) {
    }
    ngOnInit() {
        this.dsdsobject = this._dataStoreService.getData('object');
        console.log(this.dsdsobject, 'this.dsdsobjecthis.dsdsobjecthis.dsdsobjec');
        this.getdata(this.addedpersons);
        this.getSupervisorApprovalInfo();
        this.getcomardata();
        this.victimInfo(this.addedpersons);
        this.emailForm = this.formBuilder.group({
            email: ['', ValidationService.mailFormat]
        });
    }
    getdata(persons) {
        if (persons) {
        const allegedarray =  persons.filter(person => person.personid === this.investigationFind.value.personid);
        this.selectedallegedperson = allegedarray[0];
        // @Simar -> I don't know what the following is trying to do, this.selectedmaltreator was assigned value down below and the check is being done here ?
        // this.selectedmaltreator.race is breaking because this.selectedmaltreator is undefined at this place
        // I'm guessing this is trying to populate the selectedallegedperson info so making the change for now
        if (this.selectedallegedperson && this.selectedallegedperson.race) {
            let allegedrace = Array.isArray(this.selectedallegedperson.race) ? this.selectedallegedperson.race : [];
            allegedrace = allegedrace.map(item => item.value_text);
            allegedrace = Array.from((new Set(allegedrace)).values());
            this.selectedallegedperson.raceList = [...allegedrace];
        }

        const maltreatorarray = persons.filter(person => person.fullname === this.investigationFind.value.allegation[0]);
        this.selectedmaltreator = maltreatorarray[0];
        if (this.selectedmaltreator && this.selectedmaltreator.race) {
            let maltreatorrace = Array.isArray(this.selectedmaltreator.race) ? this.selectedmaltreator.race : [];
            maltreatorrace = maltreatorrace.map(item => item.value_text);
            // maltreatorrace = new Set(maltreatorrace);
            maltreatorrace = Array.from((new Set(maltreatorrace)).values());
            this.selectedmaltreator.raceList = [...maltreatorrace];
            }
        }
    }

    getcomardata() {
        if (this.investigationFind.value.investigationfindings) {
           const key = this.investigationFind.value.investigationfindings[0].investigationfindingtypekey;
           const type = this.investigationFind.value.name ? this.investigationFind.value.name.toUpperCase() : this.investigationFind.value.name;
            if (type === 'MENTAL INJURY- ABUSE' && key === 'ID') {
                this.comarFindingsKey = 'MENTAL-INJURY-ABUSE-ID';
            } else if (type === 'MENTAL INJURY- ABUSE' && key === 'RO') {
                this.comarFindingsKey = 'MENTAL-INJURY-ABUSE-RO';
            } else if (type === 'MENTAL INJURY- ABUSE' && key === 'UD') {
                this.comarFindingsKey = 'MENTAL-INJURY-ABUSE-UD';
            } else if (type === 'MENTAL INJURY- NEGLECT' && key === 'ID') {
                this.comarFindingsKey = 'MENTAL-INJURY-NEGLECT-ID';
            } else if (type === 'MENTAL INJURY- NEGLECT' && key === 'RO') {
                this.comarFindingsKey = 'MENTAL-INJURY-NEGLECT-RO';
            } else if (type === 'MENTAL INJURY- NEGLECT' && key === 'UD') {
                this.comarFindingsKey = 'MENTAL-INJURY-NEGLECT-UD';
            } else if (type === 'NEGLECT' && key === 'ID') {
                this.comarFindingsKey = 'NEGLECTID';
            } else if (type === 'NEGLECT' && key === 'RO') {
                this.comarFindingsKey = 'NEGLECTRO';
            } else if (type === 'NEGLECT' && key === 'UD') {
                this.comarFindingsKey = 'NEGLECTUD';
            } else if (type === 'PHYSICAL ABUSE' && key === 'ID') {
                this.comarFindingsKey = 'PHYSICAL-ABUSEID';
            } else if (type === 'PHYSICAL ABUSE' && key === 'RO') {
                this.comarFindingsKey = 'PHYSICAL-ABUSERO';
            } else if (type === 'PHYSICAL ABUSE' && key === 'UD') {
                this.comarFindingsKey = 'PHYSICAL-ABUSEUD';
            } else if (type === 'SEXUAL ABUSE' && key === 'ID' && this.investigationFind.value.sextrafficking === 0) {
                this.comarFindingsKey = 'SEXUAL-ABUSEID';
            } else if (type === 'SEXUAL ABUSE' && key === 'RO' && this.investigationFind.value.sextrafficking === 0) {
                this.comarFindingsKey = 'SEXUAL-ABUSERO';
            } else if (type === 'SEXUAL ABUSE' && key === 'UD' && this.investigationFind.value.sextrafficking === 0) {
                this.comarFindingsKey = 'SEXUAL-ABUSEUD';
            } else if (type === 'SEXUAL ABUSE' && key === 'ID' && this.investigationFind.value.sextrafficking === 1) {
                this.comarFindingsKey = 'SEX-TRAFFICKING-INDICATED';
            } else if (type === 'SEXUAL ABUSE' && key === 'RO' && this.investigationFind.value.sextrafficking === 1) {
                this.comarFindingsKey = 'SEX-TRAFFICKING-RULED-OUT';
            } else if (type === 'SEXUAL ABUSE' && key === 'UD' && this.investigationFind.value.sextrafficking === 1) {
                this.comarFindingsKey = 'SEX-TRAFFICKING-UNSUBSTANTIATED';
            }
        }
    }

    victimInfo(person: InvolvedPerson[]) {

        this.personComar.victimname = '';
        this.personComar.caretakername = '';
        this.personComar.victimdob = null;
        let victimdob = null;

        if (person) {
            const careTaker = person.filter((res) => {
                return res.roles.length && res.roles.filter((role) => role.intakeservicerequestpersontypekey === 'CARTKR').length;
            });
            if (careTaker.length > 0) {
                this.personComar.caretakername = careTaker[0].firstname + ' ' + careTaker[0].lastname;
            } else {
                this.personComar.caretakername = '';
            }
            const victimName = person.filter((res) => {
                return res.roles.length && res.roles.filter((role) => role.intakeservicerequestpersontypekey === 'RC').length;
            });

            const personData = person.filter((item) => item.personid === this.investigationFind.value.personid);
            if (personData.length > 0) {
                this.personComar.victimdob = new Date(personData[0].dob);
                if (this.personComar.victimdob) {
                    victimdob =  moment(this.personComar.victimdob).format('MM/DD/YYYY');
                }
            } else {
                this.personComar.victimdob = null;
            }

            this.personComar.victimname = this.investigationFind.value.personname;
        }
        this.reportcomarfindings = _.cloneDeep(ComarFindings.ComarFindingsList[this.comarFindingsKey]);

        if (this.reportcomarfindings) {
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${comar?.victimname}', this.personComar.victimname);
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${comar?.victimdob}', victimdob);
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${harmchild}', (this.investigationFind.value.investigationfindings[0].isharm === 1 ? 'Yes' : 'No'));
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${riskofharm}',
                (this.investigationFind.value.investigationfindings[0].isharmsubstantial === 1 ? 'Yes' : 'No'));
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${circumstancescomments}', this.investigationFind.value.investigationfindings[0].harmdesc);
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${intentionalinjurydesc}', this.investigationFind.value.investigationfindings[0].intentionalinjurydesc);
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${comar?.caretakername}', this.investigationFind.value.investigationfindings[0].omissiondesc);
            this.reportcomarfindings.value = this.reportcomarfindings.value.toString().replace('${findingcomments}', this.investigationFind.value.investigationfindings[0].findingcomments);
        }
    }

    getSupervisorApprovalInfo() {
        if (this.dsdsobject) {
        this._http.post( CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CpsIntakeReport, {
            'intakenumber': this.dsdsobject.intakenumber
        }).subscribe((response) => {
            this.supervisorApprovalDetails = response.data.getsupervisorapprovaldetails;
            console.log(response);
            });
        }
    }

    sendEmail () {
        const caseID = this.dsdsobject.intakenumber;
        const request = {
            email : this.emailForm.getRawValue().email,
            caseNumber: caseID,
            body: document.getElementById('investigation-summary-report').innerHTML
        };
        this._commonHttpService.create(request, 'Intakeservicerequestpurposes/sendemailcontact').subscribe(
            (result) => {
                this._alertService.success('Email Sent successfully!');
            },
            (error) => {
               console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }


    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
            await html2canvas(<HTMLElement>pages.item(i)).then(canvas => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
            });
        }
        const pageName = 'Investigation Summary';
        this.pdfFiles.push({ fileName: pageName, images: pageImages });
        pageImages = [];
        this.convertImageToPdf();
    }
    convertImageToPdf() {
        this.pdfFiles.forEach(pdfFile => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 3, 5);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#investigation-summary')).modal('hide');
        this.pdfFiles = [];
    }

}
