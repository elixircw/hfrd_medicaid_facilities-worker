import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestigationAppealComponent } from './investigation-appeal.component';

describe('InvestigationAppealComponent', () => {
  let component: InvestigationAppealComponent;
  let fixture: ComponentFixture<InvestigationAppealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestigationAppealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestigationAppealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
