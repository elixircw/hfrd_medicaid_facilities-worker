import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { PaginationInfo } from '../../../../../@core/entities/common.entities';
import { AlertService, DataStoreService } from '../../../../../@core/services';
import { AuthService } from '../../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { Allegedperson, Assessors, Findings, InvestigationComar, InvestigationFinding, InvolvedPerson, PersonComar, ProfessionType } from '../_entities/investigation-finding-data.models';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

// tslint:disable-next-line:import-blacklist
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'investigation-coma',
    templateUrl: './investigation-coma.component.html',
    styleUrls: ['./investigation-coma.component.scss']
})
export class InvestigationComaComponent implements OnInit {
    @Input()
    investigationComar$: Subject<InvestigationComar>;
    @Input()
    investigationAllegedPerson$: Subject<Allegedperson[]>;
    neglectIndicatedForm: FormGroup;
    neglectRuledForm: FormGroup;
    neglectUnsubstantiatedForm: FormGroup;
    physicalAbuseIndicatedForm: FormGroup;
    physicalAbuseRuledOutForm: FormGroup;
    physicalAbuseRuledUnsubstantiatedForm: FormGroup;
    sexualAbuseIndicatedForm: FormGroup;
    sexualAbuseRuledOutForm: FormGroup;
    sexualAbuseUnsubstantiatedForm: FormGroup;
    mentalInjuryAbuseIndicatedForm: FormGroup;
    mentalInjuryAbuseRuledOutForm: FormGroup;
    mentalInjuryAbuseUnsubstantiatedForm: FormGroup;
    mentalInjuryNeglectedIndicatedForm: FormGroup;
    mentalInjuryNeglectedRuledOutForm: FormGroup;
    mentalInjuryNeglectedUnsubstantiatedForm: FormGroup;
    sexualAbuseSexTraffickingIndicatedForm: FormGroup;
    sexualAbuseSexTraffickingRuledOutForm: FormGroup;
    sexualAbuseSexTraffickingUnsubstantiatedForm: FormGroup;
    paginationInfo: PaginationInfo = new PaginationInfo();
    fromInvestigationMain: InvestigationFinding;
    id: string;
    allegedPerson: Allegedperson[] = [];
    personComar = new PersonComar();
    setInvestigation: Findings[] = [];
    professionType: ProfessionType[];
    roleId: AppUser;
    securityusersId: string;
    displayMode = 'edit';
    assessorsList = new Assessors();
    constructor(
        private _alertService: AlertService,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    }

    ngOnInit() {
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.user.securityusersid) {
            this.securityusersId = this.roleId.user.securityusersid;
        }
        this.formInitilize();
        this.getProferssion();
        this.investigationComar$.subscribe((data) => {
            this.displayMode = data.displayMode || 'edit';
            this.fromInvestigationMain = data.investigationfinding;
            const type = this.fromInvestigationMain.name.toUpperCase();
            const key = data.maltreatmentkey.toUpperCase();
            this.victimInfo(data.involvedPerson);
            if (data.isInitialLoad) {
                if (type === 'MENTAL INJURY- ABUSE' && key === 'ID') {
                    this.setFormValue(key, type);
                    (<any>$('#MENTAL-INJURY-ABUSE-ID')).modal('show');
                } else if (type === 'MENTAL INJURY- ABUSE' && key === 'RO') {
                    this.setFormValue(key, type);
                    (<any>$('#MENTAL-INJURY-ABUSE-RO')).modal('show');
                } else if (type === 'MENTAL INJURY- ABUSE' && key === 'UD') {
                    this.setFormValue(key, type);
                    (<any>$('#MENTAL-INJURY-ABUSE-UD')).modal('show');
                } else if (type === 'MENTAL INJURY- NEGLECT' && key === 'ID') {
                    this.setFormValue(key, type);
                    (<any>$('#MENTAL-INJURY-NEGLECT-ID')).modal('show');
                } else if (type === 'MENTAL INJURY- NEGLECT' && key === 'RO') {
                    this.setFormValue(key, type);
                    (<any>$('#MENTAL-INJURY-NEGLECT-RO')).modal('show');
                } else if (type === 'MENTAL INJURY- NEGLECT' && key === 'UD') {
                    this.setFormValue(key, type);
                    (<any>$('#MENTAL-INJURY-NEGLECT-UD')).modal('show');
                } else if (type === 'NEGLECT' && key === 'ID') {
                    this.setFormValue(key, type);
                    (<any>$('#NEGLECTID')).modal('show');
                } else if (type === 'NEGLECT' && key === 'RO') {
                    this.setFormValue(key, type);
                    (<any>$('#NEGLECTRO')).modal('show');
                } else if (type === 'NEGLECT' && key === 'UD') {
                    this.setFormValue(key, type);
                    (<any>$('#NEGLECTUD')).modal('show');
                } else if (type === 'PHYSICAL ABUSE' && key === 'ID') {
                    this.setFormValue(key, type);
                    (<any>$('#PHYSICAL-ABUSEID')).modal('show');
                } else if (type === 'PHYSICAL ABUSE' && key === 'RO') {
                    this.setFormValue(key, type);
                    (<any>$('#PHYSICAL-ABUSERO')).modal('show');
                } else if (type === 'PHYSICAL ABUSE' && key === 'UD') {
                    this.setFormValue(key, type);
                    (<any>$('#PHYSICAL-ABUSEUD')).modal('show');
                } else if (type === 'SEXUAL ABUSE' && key === 'ID' && ( this.fromInvestigationMain.sextrafficking === 0 || this.fromInvestigationMain.sextrafficking !== 1 )) {
                    this.setFormValue(key, type);
                    (<any>$('#SEXUAL-ABUSEID')).modal('show');
                } else if (type === 'SEXUAL ABUSE' && key === 'RO' && ( this.fromInvestigationMain.sextrafficking === 0 || this.fromInvestigationMain.sextrafficking !== 1 )) {
                    this.setFormValue(key, type);
                    (<any>$('#SEXUAL-ABUSERO')).modal('show');
                } else if (type === 'SEXUAL ABUSE' && key === 'UD' && ( this.fromInvestigationMain.sextrafficking === 0 ||  this.fromInvestigationMain.sextrafficking !== 1 )) {
                    this.setFormValue(key, type);
                    (<any>$('#SEXUAL-ABUSEUD')).modal('show');
                } else if (type === 'SEXUAL ABUSE' && key === 'ID' && this.fromInvestigationMain.sextrafficking === 1) {
                    this.setFormValue(key, type);
                    (<any>$('#SEX-TRAFFICKING-INDICATED')).modal('show');
                } else if (type === 'SEXUAL ABUSE' && key === 'RO' && this.fromInvestigationMain.sextrafficking === 1) {
                    this.setFormValue(key, type);
                    (<any>$('#SEX-TRAFFICKING-RULED-OUT')).modal('show');
                } else if (type === 'SEXUAL ABUSE' && key === 'UD' && this.fromInvestigationMain.sextrafficking === 1) {
                    this.setFormValue(key, type);
                    (<any>$('#SEX-TRAFFICKING-UNSUBSTANTIATED')).modal('show');
                }
            }
        });
    }
    formInitilize() {
        const generalForm = {
            personid: [''],
            investigationallegationid: [''],
            personname: [''],
            intentionalinjurydesc: [''],
            findingcomments: [''],
            investigationfindingtypekey: [''],
            isharm: false,
            isharmsubstantial: false,
            harmdesc: [''],
            name: [''],
            omissiondesc: [''],
            sextrafficking: [''],
            assessors: [''],
            firstname: [''],
            lastname: [''],
            assessorcomments: [''],
            caseworkercomments: [''],
            typedescription: [''],
            professiontypekey: [''],
            isfindingassessor: false,
            securityusersid: [''],
            investigationComarCareGiverName : ['']
        };

        this.neglectIndicatedForm = this.formBuilder.group(generalForm);
        this.neglectRuledForm = this.formBuilder.group(generalForm);
        this.neglectUnsubstantiatedForm = this.formBuilder.group(generalForm);
        this.physicalAbuseIndicatedForm = this.formBuilder.group(generalForm);
        this.physicalAbuseRuledOutForm = this.formBuilder.group(generalForm);
        this.physicalAbuseRuledUnsubstantiatedForm = this.formBuilder.group(generalForm);
        this.sexualAbuseIndicatedForm = this.formBuilder.group(generalForm);
        this.sexualAbuseRuledOutForm = this.formBuilder.group(generalForm);
        this.sexualAbuseUnsubstantiatedForm = this.formBuilder.group(generalForm);
        this.mentalInjuryAbuseIndicatedForm = this.formBuilder.group(generalForm);
        this.mentalInjuryAbuseRuledOutForm = this.formBuilder.group(generalForm);
        this.mentalInjuryAbuseUnsubstantiatedForm = this.formBuilder.group(generalForm);
        this.mentalInjuryNeglectedIndicatedForm = this.formBuilder.group(generalForm);
        this.mentalInjuryNeglectedRuledOutForm = this.formBuilder.group(generalForm);
        this.mentalInjuryNeglectedUnsubstantiatedForm = this.formBuilder.group(generalForm);
        this.sexualAbuseSexTraffickingIndicatedForm = this.formBuilder.group(generalForm);
        this.sexualAbuseSexTraffickingRuledOutForm = this.formBuilder.group(generalForm);
        this.sexualAbuseSexTraffickingUnsubstantiatedForm = this.formBuilder.group(generalForm);
    }
    victimInfo(person: InvolvedPerson[]) {
        if (person) {
            const careTaker = person.filter((res) => {
                return res.roles.length && res.roles.filter((role) => role.intakeservicerequestpersontypekey === 'CARTKR').length;
            });
            if (careTaker.length > 0) {
                this.personComar.caretakername = careTaker[0].firstname + ' ' + careTaker[0].lastname;
            } else {
                this.personComar.caretakername = '';
            }
            const victimName = person.filter((res) => {
                return res.roles.length && res.roles.filter((role) => role.intakeservicerequestpersontypekey === 'RC').length;
            });

            const personData = person.filter((item) => item.personid === this.fromInvestigationMain.personid);
            if (personData.length > 0) {
                this.personComar.victimdob = personData[0].dob;
            } else {
                this.personComar.victimdob = null;
            }

            this.personComar.victimname = this.fromInvestigationMain.personname;
        }
    }
    setFormValue(findingTypeKey, selectType) {
        this.setInvestigation = [];
        this.assessorsList = Object.assign({}, new Assessors());
        const newFindings = this.allegedPerson.filter(
            (item) => item.investigationallegationid === this.fromInvestigationMain.investigationallegationid && item.investigationfindingtypekey === findingTypeKey && item.name === selectType
        );
        if (newFindings.length > 0) {
            this.setInvestigation.push({
                intentionalinjurydesc: newFindings[0].intentionalinjurydesc,
                findingcomments: newFindings[0].findingcomments,
                investigationfindingtypekey: newFindings[0].investigationfindingtypekey,
                isharm: newFindings[0].isharm,
                isharmsubstantial: newFindings[0].isharmsubstantial,
                harmdesc: newFindings[0].harmdesc,
                omissiondesc: newFindings[0].omissiondesc,
                firstname: newFindings[0].firstname,
                lastname: newFindings[0].lastname,
                assessorcomments: newFindings[0].assessorcomments,
                caseworkercomments: newFindings[0].caseworkercomments,
                typedescription: newFindings[0].typedescription,
                professiontypekey: newFindings[0].professiontypekey,
                isfindingassessor: newFindings[0].isfindingassessor,
                securityusersid: newFindings[0].securityusersid
            });
        } else if (this.fromInvestigationMain.findings && this.fromInvestigationMain.findings.length > 0) {
            this.fromInvestigationMain.findings.filter((data) => {
                if (data.investigationfindingtypekey === findingTypeKey && this.fromInvestigationMain.name.toUpperCase() === selectType) {
                    if (this.fromInvestigationMain.findings && this.fromInvestigationMain.findings.length > 0) {
                        if (data.assessors && data.assessors.length) {
                            this.assessorsList.firstname = data.assessors[0].firstname ? data.assessors[0].firstname : '';
                            this.assessorsList.lastname = data.assessors[0].lastname ? data.assessors[0].lastname : '';
                            this.assessorsList.assessorcomments = data.assessors[0].comments ? data.assessors[0].comments : '';
                            this.assessorsList.typedescription = data.assessors[0].typedescription ? data.assessors[0].typedescription : '';
                            this.assessorsList.professiontypekey = data.assessors[0].professiontypekey ? data.assessors[0].professiontypekey : '';
                            this.assessorsList.caseworkercomments = data.assessors[1].comments ? data.assessors[1].comments : '';
                        }
                        this.setInvestigation.push({
                            intentionalinjurydesc: data.intentionalinjurydesc,
                            findingcomments: data.findingcomments,
                            investigationfindingtypekey: data.investigationfindingtypekey,
                            isharm: data.isharm,
                            isharmsubstantial: data.isharmsubstantial,
                            harmdesc: data.harmdesc,
                            omissiondesc: data.omissiondesc,
                            firstname: this.assessorsList ? this.assessorsList.firstname : '',
                            lastname: this.assessorsList ? this.assessorsList.lastname : '',
                            assessorcomments: this.assessorsList ? this.assessorsList.assessorcomments : '',
                            caseworkercomments: this.assessorsList ? this.assessorsList.caseworkercomments : '',
                            typedescription: this.assessorsList ? this.assessorsList.typedescription : '',
                            professiontypekey: this.assessorsList ? this.assessorsList.professiontypekey : '',
                            isfindingassessor: false,
                            securityusersid: ''
                        });
                    }
                }
            });
        } else {
            this.setInvestigation = [];
        }
        const setGeneralForm = {
            personid: this.fromInvestigationMain.personid,
            investigationallegationid: this.fromInvestigationMain.investigationallegationid,
            personname: this.fromInvestigationMain.personname,
            intentionalinjurydesc: this.setInvestigation.length > 0 ? this.setInvestigation[0].intentionalinjurydesc : '',
            findingcomments: this.setInvestigation.length > 0 ? this.setInvestigation[0].findingcomments : '',
            investigationfindingtypekey: findingTypeKey,
            isharm: this.setInvestigation.length > 0 ? this.setInvestigation[0].isharm : '',
            isharmsubstantial: this.setInvestigation.length > 0 ? this.setInvestigation[0].isharmsubstantial : '',
            harmdesc: this.setInvestigation.length > 0 ? this.setInvestigation[0].harmdesc : '',
            omissiondesc: this.setInvestigation.length > 0 ? this.setInvestigation[0].omissiondesc : '',
            firstname: this.setInvestigation.length > 0 ? this.setInvestigation[0].firstname : '',
            lastname: this.setInvestigation.length > 0 ? this.setInvestigation[0].lastname : '',
            assessorcomments: this.setInvestigation.length > 0 ? this.setInvestigation[0].assessorcomments : '',
            professiontypekey: this.setInvestigation.length > 0 ? this.setInvestigation[0].professiontypekey : '',
            caseworkercomments: this.setInvestigation.length > 0 ? this.setInvestigation[0].caseworkercomments : '',
            name: this.fromInvestigationMain.name,
            sextrafficking: this.fromInvestigationMain.sextrafficking,
            isfindingassessor: this.setInvestigation.length > 0 ? this.setInvestigation[0].isfindingassessor : false,
            securityusersid: this.securityusersId
        };
        if (selectType === 'NEGLECT' && findingTypeKey === 'ID') {
            this.updateForm('neglectIndicatedForm', setGeneralForm);
        } else if (selectType === 'NEGLECT' && findingTypeKey === 'RO') {
            this.updateForm('neglectRuledForm', setGeneralForm);
        } else if (selectType === 'NEGLECT' && findingTypeKey === 'UD') {
            this.updateForm('neglectUnsubstantiatedForm', setGeneralForm);
        } else if (selectType === 'PHYSICAL ABUSE' && findingTypeKey === 'ID') {
            this.updateForm('physicalAbuseIndicatedForm', setGeneralForm);
        } else if (selectType === 'PHYSICAL ABUSE' && findingTypeKey === 'RO') {
            this.updateForm('physicalAbuseRuledOutForm', setGeneralForm);
        } else if (selectType === 'PHYSICAL ABUSE' && findingTypeKey === 'UD') {
            this.updateForm('physicalAbuseRuledUnsubstantiatedForm', setGeneralForm);
        } else if (selectType === 'SEXUAL ABUSE' && findingTypeKey === 'ID' && (this.fromInvestigationMain.sextrafficking === 0 || this.fromInvestigationMain.sextrafficking !== 1)) {
            this.updateForm('sexualAbuseIndicatedForm', setGeneralForm);
        } else if (selectType === 'SEXUAL ABUSE' && findingTypeKey === 'RO' && (this.fromInvestigationMain.sextrafficking === 0 || this.fromInvestigationMain.sextrafficking !== 1)) {
            this.updateForm('sexualAbuseRuledOutForm', setGeneralForm);
        } else if (selectType === 'SEXUAL ABUSE' && findingTypeKey === 'UD' && (this.fromInvestigationMain.sextrafficking === 0 || this.fromInvestigationMain.sextrafficking !== 1)) {
            this.updateForm('sexualAbuseUnsubstantiatedForm', setGeneralForm);
        } else if (selectType === 'SEXUAL ABUSE' && findingTypeKey === 'ID' && this.fromInvestigationMain.sextrafficking === 1) {
            this.updateForm('sexualAbuseSexTraffickingIndicatedForm', setGeneralForm);
        } else if (selectType === 'SEXUAL ABUSE' && findingTypeKey === 'RO' && this.fromInvestigationMain.sextrafficking === 1) {
            this.updateForm('sexualAbuseSexTraffickingRuledOutForm', setGeneralForm);
        } else if (selectType === 'SEXUAL ABUSE' && findingTypeKey === 'UD' && this.fromInvestigationMain.sextrafficking === 1) {
            this.updateForm('sexualAbuseSexTraffickingUnsubstantiatedForm', setGeneralForm);
        } else if (selectType === 'MENTAL INJURY- ABUSE' && findingTypeKey === 'ID') {
            setGeneralForm.isfindingassessor = true;
            this.updateForm('mentalInjuryAbuseIndicatedForm', setGeneralForm);
        } else if (selectType === 'MENTAL INJURY- ABUSE' && findingTypeKey === 'RO') {
            setGeneralForm.isfindingassessor = true;
            this.updateForm('mentalInjuryAbuseRuledOutForm', setGeneralForm);
        } else if (selectType === 'MENTAL INJURY- ABUSE' && findingTypeKey === 'UD') {
            setGeneralForm.isfindingassessor = true;
            this.updateForm('mentalInjuryAbuseUnsubstantiatedForm', setGeneralForm);
        } else if (selectType === 'MENTAL INJURY- NEGLECT' && findingTypeKey === 'ID') {
            setGeneralForm.isfindingassessor = true;
            this.updateForm('mentalInjuryNeglectedIndicatedForm', setGeneralForm);
        } else if (selectType === 'MENTAL INJURY- NEGLECT' && findingTypeKey === 'RO') {
            setGeneralForm.isfindingassessor = true;
            this.updateForm('mentalInjuryNeglectedRuledOutForm', setGeneralForm);
        } else if (selectType === 'MENTAL INJURY- NEGLECT' && findingTypeKey === 'UD') {
            setGeneralForm.isfindingassessor = true;
            this.updateForm('mentalInjuryNeglectedUnsubstantiatedForm', setGeneralForm);
        }
    }
    updateForm(formKey, setGeneralForm) {
        this[formKey].patchValue(setGeneralForm);
        switch (this.displayMode) {
            case 'view':
                this[formKey].disable();
                break;
            default:
                this[formKey].enable();
                break;
        }
    }
    displayButton() {
        return this.displayMode === 'edit';
    }

    getProferssion() {
        this._commonHttpService.getSingle({}, 'professiontype/').subscribe((res) => {
            this.professionType = res;
        });
    }
    addAllegedPerson(form, selectPopup) {
        const item = selectPopup.split('-');
        const maltreatmentName = item[0];
        const maltreatmentKey = item[1];
        if (maltreatmentName === 'NEGLECT' && maltreatmentKey === 'ID') {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#NEGLECTID')).modal('hide');
        } else if (maltreatmentName === 'NEGLECT' && maltreatmentKey === 'RO') {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#NEGLECTRO')).modal('hide');
        } else if (maltreatmentName === 'NEGLECT' && maltreatmentKey === 'UD') {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#NEGLECTUD')).modal('hide');
        } else if (maltreatmentName === 'PHYSICAL ABUSE' && maltreatmentKey === 'ID') {
            const isMandatory = this.conditionValidations(form, maltreatmentName + maltreatmentKey);
            if (isMandatory) {
                return this._alertService.warn('Please fill mandatory field!');
            } else {
                this.generalAllegedPerson(form, maltreatmentName);
            }
            (<any>$('#PHYSICAL-ABUSEID')).modal('hide');
        } else if (maltreatmentName === 'PHYSICAL ABUSE' && maltreatmentKey === 'RO') {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#PHYSICAL-ABUSERO')).modal('hide');
        } else if (maltreatmentName === 'PHYSICAL ABUSE' && maltreatmentKey === 'UD') {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#PHYSICAL-ABUSEUD')).modal('hide');
        } else if (maltreatmentName === 'SEXUAL ABUSE' && maltreatmentKey === 'ID' && form.sextrafficking === 1) {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#SEX-TRAFFICKING-INDICATED')).modal('hide');
        } else if (maltreatmentName === 'SEXUAL ABUSE' && maltreatmentKey === 'RO' && form.sextrafficking === 1) {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#SEX-TRAFFICKING-RULED-OUT')).modal('hide');
        } else if (maltreatmentName === 'SEXUAL ABUSE' && maltreatmentKey === 'UD' && form.sextrafficking === 1) {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#SEX-TRAFFICKING-UNSUBSTANTIATED')).modal('hide');
        } else if (maltreatmentName === 'SEXUAL ABUSE' && maltreatmentKey === 'ID' && ( form.sextrafficking === 0 || form.sextrafficking !== 1)) {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#SEXUAL-ABUSEID')).modal('hide');
        } else if (maltreatmentName === 'SEXUAL ABUSE' && maltreatmentKey === 'RO' && ( form.sextrafficking === 0 || form.sextrafficking !== 1)) {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#SEXUAL-ABUSERO')).modal('hide');
        } else if (maltreatmentName === 'SEXUAL ABUSE' && maltreatmentKey === 'UD' && ( form.sextrafficking === 0 || form.sextrafficking !== 1)) {
            this.generalAllegedPerson(form, maltreatmentName);
            (<any>$('#SEXUAL-ABUSEUD')).modal('hide');
        } else if (maltreatmentName === 'MENTAL INJURY ABUSE' && maltreatmentKey === 'ID') {
            const isMandatory = this.conditionValidations(form, maltreatmentName + maltreatmentKey);
            if (isMandatory) {
                return this._alertService.warn('Please fill mandatory fields!');
            } else {
                this.generalAllegedPerson(form, 'MENTAL INJURY- ABUSE');
            }
            (<any>$('#MENTAL-INJURY-ABUSE-ID')).modal('hide');
        } else if (maltreatmentName === 'MENTAL INJURY ABUSE' && maltreatmentKey === 'RO') {



            const isMandatory = this.conditionValidations(form, maltreatmentName + maltreatmentKey);
            if (isMandatory) {
                return this._alertService.warn('Please fill mandatory fields!');
            } else {
                this.generalAllegedPerson(form, 'MENTAL INJURY- ABUSE');
            }
            (<any>$('#MENTAL-INJURY-ABUSE-RO')).modal('hide');
        } else if (maltreatmentName === 'MENTAL INJURY ABUSE' && maltreatmentKey === 'UD') {
            const isMandatory = this.conditionValidations(form, maltreatmentName + maltreatmentKey);
            if (isMandatory) {
                return this._alertService.warn('Please fill mandatory fields!');
            } else {
                this.generalAllegedPerson(form, 'MENTAL INJURY- ABUSE');
            }
            (<any>$('#MENTAL-INJURY-ABUSE-UD')).modal('hide');
        } else if (maltreatmentName === 'MENTAL INJURY NEGLECT' && maltreatmentKey === 'ID') {
            const isMandatory = this.conditionValidations(form, maltreatmentName + maltreatmentKey);
            if (isMandatory) {
                return this._alertService.warn('Please fill mandatory fields!');
            } else {
                this.generalAllegedPerson(form, 'MENTAL INJURY- NEGLECT');
            }
            (<any>$('#MENTAL-INJURY-NEGLECT-ID')).modal('hide');
        } else if (maltreatmentName === 'MENTAL INJURY NEGLECT' && maltreatmentKey === 'RO') {
            this.generalAllegedPerson(form, 'MENTAL INJURY- NEGLECT');
            (<any>$('#MENTAL-INJURY-NEGLECT-RO')).modal('hide');
        } else if (maltreatmentName === 'MENTAL INJURY NEGLECT' && maltreatmentKey === 'UD') {
            this.generalAllegedPerson(form, 'MENTAL INJURY- NEGLECT');
            (<any>$('#MENTAL-INJURY-NEGLECT-UD')).modal('hide');
        }
        this.investigationAllegedPerson$.next(this.allegedPerson);
    }
    resetFindingType() {
        this.investigationAllegedPerson$.next(null);
    }
    conditionValidations(formValues, formName): boolean {
        if (formName === 'MENTAL INJURY ABUSEID' || formName === 'MENTAL INJURY NEGLECTID') {
            if (!formValues.firstname || !formValues.lastname || !formValues.assessorcomments || !formValues.professiontypekey || !formValues.caseworkercomments) {
                return true;
            } else {
                return false;
            }
        } else if (formName === 'PHYSICAL ABUSEID') {
            if (!formValues.intentionalinjurydesc) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
    private generalAllegedPerson(form, selectPopup): Allegedperson[] {
        if (this.allegedPerson.length > 0) {
            this.allegedPerson = this.allegedPerson.filter((res) => res.investigationallegationid !== this.fromInvestigationMain.investigationallegationid);
        }
        let isHarm = 0;
        if (typeof form.isharm === 'boolean') {
            isHarm = form.isharm === true ? 1 : 0;
        } else {
            isHarm = form.isharm;
        }
        let isharmsubstantial = 0;
        if (typeof form.isharmsubstantial === 'boolean') {
            isharmsubstantial = form.isharmsubstantial === true ? 1 : 0;
        } else {
            isharmsubstantial = form.isharmsubstantial;
        }
        this.allegedPerson.push({
            personid: form.personid,
            investigationallegationid: form.investigationallegationid,
            name: selectPopup,
            intentionalinjurydesc: form.intentionalinjurydesc,
            findingcomments: form.findingcomments,
            investigationfindingtypekey: form.investigationfindingtypekey,
            isharm: isHarm,
            isharmsubstantial: isharmsubstantial,
            harmdesc: form.harmdesc,
            omissiondesc: form.omissiondesc,
            sextrafficking: form.sextrafficking,
            firstname: form.firstname ? form.firstname : null,
            lastname: form.lastname ? form.lastname : null,
            typedescription: form.typedescription ? form.typedescription : null,
            professiontypekey: form.professiontypekey ? form.professiontypekey : null,
            assessorcomments: form.assessorcomments ? form.assessorcomments : null,
            caseworkercomments: form.caseworkercomments ? form.caseworkercomments : null,
            isfindingassessor: form.isfindingassessor,
            securityusersid: form.securityusersid
        });
        return this.allegedPerson;
    }

    discardAction(modalID: string) {
        this.investigationAllegedPerson$.next(null);
        (<any>$('#' + modalID)).modal('hide');
    }
}
