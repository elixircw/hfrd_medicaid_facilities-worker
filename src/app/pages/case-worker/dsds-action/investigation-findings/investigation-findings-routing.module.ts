import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvestigationFindingsComponent } from './investigation-findings.component';

const routes: Routes = [{
  path: '', component: InvestigationFindingsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvestigationFindingsRoutingModule { }
