import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestigationFindingsRoutingModule } from './investigation-findings-routing.module';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { InvestigationFindingsComponent } from './investigation-findings.component';
import { InvestigationAppealComponent } from './investigation-appeal/investigation-appeal.component';
import { InvestigationComaComponent } from './investigation-coma/investigation-coma.component';
import { QuillModule } from 'ngx-quill';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { MatTooltipModule } from '@angular/material';
import { NgxMaskModule } from 'ngx-mask';
import { DocumentUploadListComponent } from '../service-plan/youth-transition-plan/shared-feature/document-upload-list/document-upload-list.component';
import { InvestigationSummaryReportComponent } from './investigation-summary-report/investigation-summary-report.component';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
@NgModule({
  imports: [
    CommonModule,
    InvestigationFindingsRoutingModule,
    MatTooltipModule,
    FormMaterialModule, QuillModule, ControlMessagesModule, NgSelectModule,
    NgxfUploaderModule.forRoot(),
    QuillModule,
    ControlMessagesModule,
    SharedPipesModule,
    NgxMaskModule.forRoot()],
  declarations: [InvestigationFindingsComponent, InvestigationAppealComponent, InvestigationComaComponent,DocumentUploadListComponent, InvestigationSummaryReportComponent],
  exports: [DocumentUploadListComponent, InvestigationSummaryReportComponent]
})
export class InvestigationFindingsModule { }
