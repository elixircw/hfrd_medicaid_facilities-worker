export class DSDSConstants {
    public static DSDS = {
        PersonsInvolved: {
            Health: {
                Health: 'health',
                Physician: 'health_physician',
                PhysicianFromIntake: 'intake_health_physician',
                Insurance: 'health_insurance'
            }
        }
    };
}

export const DSDS_STORE_CONSTANTS = {
    'LEAVE_AND_TANSFER': 'LEAVE_AND_TANSFER',
    'IS_TEMP_PLACEMENT': 'IS_TEMP_PLACEMENT',
    'TEMP_LEAVE_DATA': 'TEMP_LEAVE_DATA',
    'PROVIDER_SEARCH_MODEL': 'PROVIDER_SEARCH_MODEL',
    TPR_LIST: 'TPR_LIST',
    TPR_RECOMENDATION_LIST: 'TPR_RECOMENDATION_LIST'
};

