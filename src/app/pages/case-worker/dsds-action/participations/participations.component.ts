import { Component, OnInit } from '@angular/core';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';
import { ServiceCaseParticipants } from './_entity';
import { Observable } from 'rxjs/Observable';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'participations',
  templateUrl: './participations.component.html',
  styleUrls: ['./participations.component.scss']
})
export class ParticipationsComponent implements OnInit {
  id: string;
  isShow: string;
  ServicecaseParticipants$: Observable<ServiceCaseParticipants[]>;
  constructor(private _commonHttpService: CommonHttpService, private route: ActivatedRoute, private _dataStoreService: DataStoreService) {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
  }

  ngOnInit() {
    this.getParticipants();
  }
  getParticipants() {
    this.ServicecaseParticipants$ = this._commonHttpService
        .getArrayList(
            {
                where: { servicecaseid : this.id},
                method: 'get'
            },
            'Intakedastagings/getpriorbyservicecase?filter'
        );
        // this.ServicecaseParticipants$.subscribe((result) => {
        // });
}
toggleClient(modal) {
  this.isShow = modal;
}
}
