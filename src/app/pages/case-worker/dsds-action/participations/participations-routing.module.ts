import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParticipationsComponent } from './participations.component';
const routes: Routes = [
    {
    path: '',
    component: ParticipationsComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParticipationsRoutingModule { }
