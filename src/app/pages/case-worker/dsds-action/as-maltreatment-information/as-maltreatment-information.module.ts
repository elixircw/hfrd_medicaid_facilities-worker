import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { QuillModule } from 'ngx-quill';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { AsMaltreatmentInformationRoutingModule } from './as-maltreatment-information-routing.module';
import { AsMaltreatmentInformationComponent } from './as-maltreatment-information.component';

@NgModule({
  imports: [
    CommonModule,
    AsMaltreatmentInformationRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    QuillModule,
    FormsModule, ReactiveFormsModule,
    PaginationModule
  ],
  declarations: [AsMaltreatmentInformationComponent]
})
export class AsMaltreatmentInformationModule { }
