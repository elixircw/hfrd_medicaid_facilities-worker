export enum InvestigationFindingStoreConstants {
    awardLetter = 'Award Letter',
    bankStatement = 'Bank Statement',
    payStub = 'Pay Stub',
    courtOrder = 'Court Order',
    rentalReceipts = 'Rental Receipts',
    accountStatements = 'Account Statements',
    contract = 'Contract'
}