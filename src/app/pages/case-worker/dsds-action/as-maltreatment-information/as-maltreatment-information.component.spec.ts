import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsMaltreatmentInformationComponent } from './as-maltreatment-information.component';

describe('AsMaltreatmentInformationComponent', () => {
  let component: AsMaltreatmentInformationComponent;
  let fixture: ComponentFixture<AsMaltreatmentInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsMaltreatmentInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsMaltreatmentInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
