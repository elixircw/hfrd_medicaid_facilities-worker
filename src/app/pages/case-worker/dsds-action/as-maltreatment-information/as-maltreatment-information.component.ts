import { Component, OnInit, AfterViewInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';

import { CommonHttpService, AlertService, DataStoreService, GenericService } from '../../../../@core/services';
import { ActivatedRoute } from '@angular/router';

import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { ReportSummary, SearchRecording, DSDSActionSummary } from '../../_entities/caseworker.data.model';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { Subject } from 'rxjs/Subject';
import { RecordingNotes } from '../recording/_entities/recording.data.model';
import { supportsPassiveEventListeners } from '@angular/cdk/platform';
import { CountyDetail, IncomeDetail, AssetDetail, InvestigationFinding } from './_entities/investigation-finding.data.model';
import { InvolvedPerson, Medicationinformation, Medicalinformation, MedicalCondition } from '../involved-persons/_entities/involvedperson.data.model';
import { InvestigationFindingStoreConstants } from './as-maltreatment-information-constant';
import { MaltreatmentInformation } from '../maltreatment-information/_entites/maltreatment.data.model';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'as-maltreatment-information',
  templateUrl: './as-maltreatment-information.component.html',
  styleUrls: ['./as-maltreatment-information.component.scss']
})
export class AsMaltreatmentInformationComponent implements OnInit, AfterViewChecked {

  mdCountys$: Observable<CountyDetail[]>;
  maltreatementForm: FormGroup;
  serviceworkerForm: FormGroup;
  clientsLivingSituationFormGroup: FormGroup;
  countyAddresses = [];
  dsdsActionsSummary = new DSDSActionSummary();
  id: string;
  reportSummary: ReportSummary;
  paginationInfo: PaginationInfo = new PaginationInfo();
  recording$: Observable<RecordingNotes[]>;
  totalRecords$: Observable<number>;
  private pageSubject$ = new Subject<number>();
  private recordSearch = new SearchRecording();
  personType = 'Involved';
  supportedPersons = [];
  supportedPersonsSave = [];
  guardian = [];
  suporter: { personType: string, person: string, type: string, desc: string };
  personRoles = ([] = []);
  isInitialLoad = false;
  guardianRole: InvolvedPerson[] = [];
  legalInformationFormGroup: FormGroup;
  medicationForm: FormGroup;
  clientSupport: FormGroup;
  financialDisclosureSubmissionData: any;
  medications: Medicationinformation[];
  medicalinformation: Medicalinformation[];
  medicalcondition: MedicalCondition[];
  guardianDescription: string[] = [];
  incomeDetails: IncomeDetail[];
  assetDetails: AssetDetail[];
  psychiatricimportinfo = '0';
  financialimportinfo = '0';
  finacialDesc: string;
  clientCapacity: string;
  closingReason: string;
  maltreatmentInformation: MaltreatmentInformation[] = [];
  maltreatmentReported: MaltreatmentInformation[] = [];
  maltreatmentIdentified: MaltreatmentInformation[] = [];
  investigationFinding: InvestigationFinding;
  getinvestigationFinding: InvestigationFinding;
  juridictionAddress: any;
  constructor(private _commonHttpService: CommonHttpService,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private formBuilder: FormBuilder,
    private _detect: ChangeDetectorRef,
    private _dataStoreService: DataStoreService,
    private _reportSummaryService: GenericService<ReportSummary>) {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
  }

  ngOnInit() {

    this.initForm();
    this.isInitialLoad = true;
    // this._dataStoreService.currentStore.subscribe(store => {
      const store = this._dataStoreService.getCurrentStore();
      if (this.isInitialLoad && store['dsdsActionsSummary']) {
        this.dsdsActionsSummary = store['dsdsActionsSummary'];
        if (this.dsdsActionsSummary) {
          this.setDefaults();
          this.getinvestigationallegation();
          this.getMaltreatmentInformation();
          this.isInitialLoad = false;
        }
      }
    // });
    this.loadDropdown();
    this.loadSummaryNarrative();
    this.getInvolvedPerson();
    this.getPage(1);
    this.getInvestigationFinding();

  }

  ngAfterViewChecked() {
    this._detect.detectChanges();
  }

  getInvestigationFinding() {

    this._commonHttpService.getSingle(new PaginationRequest({
      where: {
        intakeserviceid: this.id
      },
      method: 'get'
    }), 'Investigationfindings/list?filter').subscribe((data) => {
      if (data) {
        this.getinvestigationFinding = data[0];
        this.maltreatementForm.patchValue({
           countyid: this.getinvestigationFinding.invsfindingjurisdiction ? this.getinvestigationFinding.invsfindingjurisdiction : '',
           address: this.getinvestigationFinding.invsfindingaddress ? this.getinvestigationFinding.invsfindingaddress : '',
           insertedon: this.getinvestigationFinding.investigationfindingdate ? this.getinvestigationFinding.investigationfindingdate : ''
        });
        this.serviceworkerForm.patchValue({
          workersigndate: this.getinvestigationFinding.apsworkersigndate ? this.getinvestigationFinding.apsworkersigndate : '',
          supervisorsigndate: this.getinvestigationFinding.supervisorsigndate ? this.getinvestigationFinding.supervisorsigndate : ''
        });
        this.clientsLivingSituationFormGroup.patchValue({
          description: this.getinvestigationFinding.socialhistorydesc ? this.getinvestigationFinding.socialhistorydesc : '',
          familyhistory: this.getinvestigationFinding.familyhistorydesc ? this.getinvestigationFinding.familyhistorydesc : '',
          education: this.getinvestigationFinding.educationalfactors ? this.getinvestigationFinding.educationalfactors : ''
        });
        this.medicationForm.patchValue({
          psychiatricdesc: this.getinvestigationFinding.psychiatricdesc ? this.getinvestigationFinding.psychiatricdesc : ''
        });

          this.clientCapacity = this.getinvestigationFinding.clentcapacitydesc ? this.getinvestigationFinding.clentcapacitydesc : '';
          this.closingReason = this.getinvestigationFinding.reasonclosingdesc ? this.getinvestigationFinding.reasonclosingdesc : '';
          this.finacialDesc = this.getinvestigationFinding.assetdetailsdesc ? this.getinvestigationFinding.assetdetailsdesc : '';

          if (this.getinvestigationFinding) {
            this.getinvestigationFinding.investigationfindingtypeperson.map((list) => {
              this.supportedPersons.push({
                person: list.invesfindingpersonname ? list.invesfindingpersonname : list.personname,
                description: list.invesfindingpersondesc,
                personType: list.invsfindingpersonsupporttype,
                type: list.investigationfindingpersontype
              });
              this.supportedPersonsSave.push(list);
            });

          // tslint:disable-next-line:max-line-length
          const guardianLit = this.getinvestigationFinding.investigationfindingguardian ? this.getinvestigationFinding.investigationfindingguardian.map(items =>  items.intakeservicerequestactorid) : '';
              this.legalInformationFormGroup.patchValue({
                  poa: this.getinvestigationFinding.legalinfopoa ? this.getinvestigationFinding.legalinfopoa : '',
                  reppayee: this.getinvestigationFinding.legalinforeppayee ? this.getinvestigationFinding.legalinforeppayee : '',
                  courtinvolvement: this.getinvestigationFinding.legalinfocourtinvolved ? this.getinvestigationFinding.legalinfocourtinvolved : '',
                  description: this.getinvestigationFinding.legalinfodesc ? this.getinvestigationFinding.legalinfodesc : '',
                  guardian: guardianLit
              });
          }
      }
    });
  }

  private loadDropdown() {
    // tslint:disable-next-line:max-line-length
    this.mdCountys$ = this._commonHttpService.getArrayList({ where: { state: 'MD' }, order: 'countyname', nolimit: true, method: 'get' }, CommonUrlConfig.EndPoint.Listing.CountryListUrl + '?filter').map(result => {
      return result;
    });
  }

  loadSummaryNarrative() {
    this._reportSummaryService.getSingle(new PaginationRequest({}), CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.ReportSummary + this.id).subscribe(result => {
      this.reportSummary = result;
    });
  }


  getMaltreatmentInformation() {
    this._commonHttpService
                .getArrayList(
                    {
                        where: { investigationid: this.dsdsActionsSummary.da_investigationid },
                        method: 'get'
                    },
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.MaltreatmentInformation + '?filter'
                ).subscribe((data) => {
                    this.maltreatmentInformation = data;
                    if (this.maltreatmentInformation)  {
                      this.maltreatmentReported = data.filter(reporter => reporter.isreported === 'reported' && reporter.investigationallegation);
                      this.maltreatmentIdentified = data.filter(reporter => reporter.isreported === 'identifer' && reporter.investigationallegation);
                    }
                });
  }

  onCountyChange(countyId) {
    // let address = '';
    this.mdCountys$.subscribe(item => {
      if (item) {
        const county = item.find(ca => ca.countyid === countyId);
        if (county) {
          if (county.countyname === 'Anne Arundel County') {
            this.juridictionAddress = '80 West Street Annapolis, Maryland 21401';
          }
          if (county.countyname === 'Baltimore City') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = 'Talmadge Branch Building 1910 N. Broadway Street Baltimore, Maryland 21213';
          }
          if (county.countyname === 'Baltimore County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '6401 York Road Baltimore, Maryland 21212';
          }
          if (county.countyname === 'Calvert County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '200 Duke Street Prince Frederick, Maryland 20678';

          }
          if (county.countyname === 'Caroline County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '207 South Third Street Denton, Maryland 21629';
          }

          if (county.countyname === 'Carroll County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '1232 Tech Court Westminster, Maryland 21157';

          }
          if (county.countyname === 'Cecil County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = 'Elkton District Court/Multi Service Building 170 East Main Street Elkton,Maryland 21921';

          }
          if (county.countyname === 'Charles County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '200 Kent Avenue LaPlata, Maryland 20646';

          }
          if (county.countyname === 'Dorchester County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '627 Race Street Cambridge, Maryland 21613';

          }
          if (county.countyname === 'Frederick County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '1888 North Market Street Frederick, Maryland 21701';

          }
          if (county.countyname === 'Garrett County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '12578 Garrett Highway Oakland, Maryland 21550';

          }
          if (county.countyname === 'Harford County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '2 South Bond Street Suite 300 Bel Air, Maryland 21014';

          }
          if (county.countyname === 'Howard County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '7121 Columbia Gateway Drive Columbia, Maryland 21046';

          }
          if (county.countyname === 'Kent County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '350 High Street P.O. Box 670 Chestertown, MD 21620';

          }
          if (county.countyname === 'Montgomery Count') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '401 Hungerford Drive, 5th Floor Rockville, Maryland 20850';

          }
          if (county.countyname === "Prince George's County") {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '805 Brightseat Road Landover, Maryland 20785-4723';

          }
          if (county.countyname === "Queen Anne's County") {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '125 Comet Drive Centreville, Maryland 21617';

          }
          if (county.countyname === "Saint Mary's County") {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '23110 Leonard Hall Dr Leonardtown, MD 20650';

          }
          if (county.countyname === 'Somerset County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '30397 Mt. Vernon Road Princess Anne, Maryland 21853';

          }
          if (county.countyname === 'Washington County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '122 North Potomac Street Hagerstown, Maryland 21740';

          }
          if (county.countyname === 'Wicomico County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '201 Baptist Street, Suite 27 Salisbury, Maryland 21801';

          }
          if (county.countyname === 'Worcester County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '299 Commerce Street Snow Hill, Maryland 21863';

          }

          // tslint:disable-next-line:max-line-length
          this.maltreatementForm.patchValue({ address: this.juridictionAddress });
         // tslint:disable-next-line:max-line-length
         // this.maltreatementForm.patchValue({ address: (county.statecountycode ? county.statecountycode : '') + ' ' + (county.city ? county.city : '') + ' ' + (county.state ? county.state : '') + ' ' + (county.zipcode ? county.zipcode : '') });
        }
      }
    });
  }

  private setDefaults() {
    const adultDetail = {
      clientname: this.dsdsActionsSummary.da_focus,
      clientid: this.dsdsActionsSummary.cjamspid,
      insertedon: new Date(this.dsdsActionsSummary.da_insertedon),
      actuatlRefferal: ''
    };
    const serviceWorker = {
      caseworker: this.dsdsActionsSummary.da_assignedto,
      supervisor: '',
    };
    this.maltreatementForm.patchValue(adultDetail);
    this.serviceworkerForm.patchValue(serviceWorker);
  }

  private initForm() {
    this.maltreatementForm = this.formBuilder.group({
      countyid: [''],
      address: [''],
      clientname: [''],
      clientid: [''],
      clientaddress: [''],
      insertedon: [''],
      actuatlRefferal: ['']
    });
    this.clientSupport = this.formBuilder.group({
      personType: ['Involved'],
      person: [''],
      type: [''],
      description: ['']
    });
    this.serviceworkerForm = this.formBuilder.group({
      caseworker: [''],
      workersigndate: new Date(),
      supervisor: [''],
      supervisorsigndate: new Date()
    });
    this.legalInformationFormGroup = this.formBuilder.group({
      poa: [''],
      reppayee: [''],
      guardian: [''],
      courtinvolvement: [''],
      description: ['']
    });
    this.clientsLivingSituationFormGroup = this.formBuilder.group({
      ethinicity: [''],
      religion: [''],
      livingsituation: [''],
      familyhistory: [''],
      education: [''],
      description: ['']
    });
    this.medicationForm = this.formBuilder.group({
      psychiatricdesc: ['']
    });

  }

  getPage(page: number) {
    ObjectUtils.removeEmptyProperties(this.recordSearch);
    const source = this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          page: this.paginationInfo.pageNumber,
          limit: this.paginationInfo.pageSize,
          where: this.recordSearch,
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetAllDaRecordingUrl + '/' + this.id + '?data'
      )
      .map((result) => {
        return {
          data: result.data,
          count: result.count,
          canDisplayPager: result.count > this.paginationInfo.pageSize
        };
      })
      .share();
    this.recording$ = source.pluck('data');
    if (page === 1) {
      this.totalRecords$ = source.pluck('count');
    }
  }
  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.pageSubject$.next(this.paginationInfo.pageNumber);
  }

  involvedChange(event) {
    this.clientSupport.reset();
    this.clientSupport.patchValue({personType: event});
 }

  addSupportedPerson(suporter) {
    const supoterObject = {
      person: suporter.person.personname ? suporter.person.personname : suporter.person,
      description: suporter.description,
      type: suporter.type,
      personType: suporter.personType,
    };
    if (this.clientSupport.get('personType').value === 'Involved') {
      const supoterSave = {
        intakeservicerequestactorid: suporter.person.intakeservicerequestactorid,
        invesfindingpersondesc: suporter.description,
        invsfindingpersonsupporttype: suporter.type,
        investigationfindingpersontype: suporter.personType
      };
      this.supportedPersonsSave.push(Object.assign({}, supoterSave));
    }
    if (this.clientSupport.get('personType').value === 'Non-Involved') {
      const supoterSave = {
        invesfindingpersonname: suporter.person,
        invesfindingpersondesc: suporter.description,
        invsfindingpersonsupporttype: suporter.type,
        investigationfindingpersontype: suporter.personType
      };
      this.supportedPersonsSave.push(Object.assign({}, supoterSave));
    }
    this.supportedPersons.push(Object.assign({}, supoterObject));
    this.clientSupport.reset();
    this.clientSupport.patchValue({personType: 'Involved'});
  }

  removeSuporter(index) {
    this.supportedPersons.splice(index, 1);
    this.supportedPersonsSave.splice(index, 1);
  }

  private getInvolvedPerson1() {

    this._commonHttpService
      .getPagedArrayList(
        {
          where: { intakeserviceid: this.id },
          page: 1,
          limit: 20,
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.InvolvedPersonUrl + '?filter'
      )
      .subscribe((itm) => {
        if (itm.data) {
          this.personRoles = [];
          this.guardianRole = [];
          itm.data.map((list: InvolvedPerson) => {
            const role = list.roles.filter(role => role.intakeservicerequestpersontypekey === 'RA');
            const grd = list.roles.filter(role => role.intakeservicerequestpersontypekey === 'GRD');
            if (grd.length) {
              this.guardianRole.push(list);
            }
            if (role.length) {
              // tslint:disable-next-line:max-line-length
              const address = (list.address ? list.address : '') + '' + (list.address2 ? list.address2 : '') + '' + (list.city ? list.city : '') + '' + (list.county ? list.county : '') + '' + (list.zipcode ? list.zipcode : '');
              this.maltreatementForm.patchValue({ clientaddress: address });
              this.clientsLivingSituationFormGroup.patchValue({ religion: list.religion, ethinicity: list.ethinicity });
            }
            return this.personRoles.push({
              intakeservicerequestactorid: list.roles[0].intakeservicerequestactorid,
              displayname: list.firstname + ' ' + list.lastname + '(' + list.roles[0].typedescription + ')',
              personname: list.firstname + ' ' + list.lastname,
              role: list.roles
            });
          });
        }
      });
  }

  private getInvolvedPerson() {
    Observable.forkJoin([
      this._commonHttpService
        .getPagedArrayList(
          {
            where: { intakeserviceid: this.id },
            page: 1,
            limit: 20,
            method: 'get'
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.InvolvedPersonUrl + '?filter'
        ),
      this._commonHttpService.getPagedArrayList({
        assessment: [
          {
            titleheadertext: '248 C Financial Disclosure'
          }
        ],
        intakeserviceid: this.id
        ,
        method: 'post'
      }, 'admin/assessment/getassessmentdetailsbytitleheadertext')]).subscribe((result) => {
        if (result[0].data) {
          this.personRoles = [];
          this.guardianRole = [];
          result[0].data.map((list: InvolvedPerson) => {
            const role = list.roles.filter(role => role.intakeservicerequestpersontypekey === 'RA');
            const grd = list.roles.filter(role => role.intakeservicerequestpersontypekey === 'GRD');
            if (grd.length) {
              this.guardianRole.push(list);
            }
            if (role.length) {
              // tslint:disable-next-line:max-line-length
              const address = (list.address ? list.address : '') + '' + (list.address2 ? list.address2 : '') + '' + (list.city ? list.city : '') + '' + (list.county ? list.county : '') + '' + (list.zipcode ? list.zipcode : '');
              this.medications = list.medicationinformation;
              this.medicalcondition = list.medicalcondition.filter(item => item.description);
              this.medicalinformation = list.medicalinformation;
              this.maltreatementForm.patchValue({ clientaddress: address });
              this.clientsLivingSituationFormGroup.patchValue({ religion: list.religion, ethinicity: list.ethinicity });
            }
            return this.personRoles.push({
              intakeservicerequestactorid: list.intakeservicerequestactorid,
              displayname: list.firstname + ' ' + list.lastname + '(' + list.roles[0].typedescription + ')',
              personname: list.firstname + ' ' + list.lastname,
              role: list.roles
            });
          });
        }
        if (result[1].data.length && result[1].data[0].submissiondata) {
          this.financialDisclosureSubmissionData = result[1].data[0].submissiondata;
          this.patchFinanceAssessment(result[1].data[0].submissiondata);
        }
      });
  }

  getinvestigationallegation() {
    this._commonHttpService
      .getArrayList(
        {
          where: { investigationid: this.dsdsActionsSummary.da_investigationid },
          nolimit: true,
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.MaltreatmentInformation + '?filter'
      ).subscribe((result) => {
        if (result) {
          this.maltreatmentInformation = result;
        }
      });
  }

  selectedGuardian(event) {
    if (event) {
      const getperson = this.guardianRole.filter(item => event.includes(item.intakeservicerequestactorid));
      this.guardian = getperson.map(list => { return { intakeservicerequestactorid: list.intakeservicerequestactorid };
      });
      if (getperson.length) {
        this.guardianDescription = getperson.map(res => (res.firstname + ' ' + res.lastname));
      }
    }
  }

  psychiatricImportant(event) {
    this.psychiatricimportinfo = event.checked ? '1' : '0';
  }

  financeImportant(event) {
    this.financialimportinfo = event.checked ? '1' : '0';
  }

  patchFinanceAssessment(assessment) {
    this.incomeDetails = [];
    this.assetDetails = [];
    const income = Object.assign({}, new IncomeDetail());
    if (assessment.c1 || assessment.d1 || assessment.SocialSecuritySelectField1) {
      // tslint:disable-next-line:max-line-length
      this.incomeDetails.push({ category: 'Social Security', source: InvestigationFindingStoreConstants[assessment.SocialSecuritySelectField1], clientIncome: assessment.c1, spouseIncome: assessment.d1 });
    }
    if (assessment.c2 || assessment.d2 || assessment.SalarySelectField2) {
      this.incomeDetails.push({ category: 'Salary', source: InvestigationFindingStoreConstants[assessment.SalarySelectField2], clientIncome: assessment.c2, spouseIncome: assessment.d2 });
    }

    if (assessment.c3 || assessment.d3 || assessment.VeteransBenefitsSelectField3) {
      // tslint:disable-next-line:max-line-length
      this.incomeDetails.push({ category: 'Veterans Benefits', source: InvestigationFindingStoreConstants[assessment.VeteransBenefitsSelectField3], clientIncome: assessment.c3, spouseIncome: assessment.d3 });
    }
    if (assessment.c4 || assessment.d4 || assessment.RailroadRetirementSelectField4) {
      // tslint:disable-next-line:max-line-length
      this.incomeDetails.push({ category: 'Railroad Retirement', source: InvestigationFindingStoreConstants[assessment.RailroadRetirementSelectField4], clientIncome: assessment.c4, spouseIncome: assessment.d4 });
    }

    if (assessment.c5 || assessment.d5 || assessment.CivilServiceSelectField5) {
      this.incomeDetails.push({ category: 'Civil Service', source: InvestigationFindingStoreConstants[assessment.CivilServiceSelectField5], clientIncome: assessment.c5, spouseIncome: assessment.d5 });
    }

    if (assessment.c6 || assessment.d6 || assessment.PensionSelectField6) {
      this.incomeDetails.push({ category: 'Pension', source: InvestigationFindingStoreConstants[assessment.PensionSelectField6], clientIncome: assessment.c6, spouseIncome: assessment.d6 });
    }

    if (assessment.c7 || assessment.d7 || assessment.AlimonySelectField7) {
      this.incomeDetails.push({ category: 'Alimony', source: InvestigationFindingStoreConstants[assessment.AlimonySelectField7], clientIncome: assessment.c7, spouseIncome: assessment.d7 });
    }

    if (assessment.c8 || assessment.d8 || assessment.RentalIncomeSelectField8) {
      this.incomeDetails.push({ category: 'Rental Income', source: InvestigationFindingStoreConstants[assessment.RentalIncomeSelectField8], clientIncome: assessment.c8, spouseIncome: assessment.d8 });
    }
    if (assessment.c9 || assessment.d9 || assessment.InterestIncomeSelectField9) {
      // tslint:disable-next-line:max-line-length
      this.incomeDetails.push({ category: 'Interest Income', source: InvestigationFindingStoreConstants[assessment.InterestIncomeSelectField9], clientIncome: assessment.c9, spouseIncome: assessment.d9 });
    }

    if (assessment.c10 || assessment.d10 || assessment.AnnuitiesSelectField10) {
      this.incomeDetails.push({ category: 'Annuities', source: InvestigationFindingStoreConstants[assessment.AnnuitiesSelectField10], clientIncome: assessment.c10, spouseIncome: assessment.d10 });
    }

    if (assessment.c11 || assessment.d11 || assessment.ReverseMortgageSelectField11) {
      // tslint:disable-next-line:max-line-length
      this.incomeDetails.push({ category: 'Reverse Mortgage', source: InvestigationFindingStoreConstants[assessment.ReverseMortgageSelectField11], clientIncome: assessment.c11, spouseIncome: assessment.d11 });
    }

    if (assessment.c12 || assessment.d12 || assessment.Other111TextField1) {
      this.incomeDetails.push({ category: 'Other', source: assessment.Other111TextField1 ? assessment.Other111TextField1 : '', clientIncome: assessment.c12, spouseIncome: assessment.d12 });
    }






    if (assessment.e1) {
      this.assetDetails.push({ category: 'Saving Account', source: 'Account Statement', totalValue: assessment.e1 });
    }
    if (assessment.e2) {
      this.assetDetails.push({ category: 'Checking Account', source: 'Account Statement', totalValue: assessment.e2 });
    }
    if (assessment.e3) {
      this.assetDetails.push({ category: 'Certificate Of Deposit', source: 'Account Statement', totalValue: assessment.e3 });
    }
    if (assessment.e4) {
      this.assetDetails.push({ category: 'Value Of Stock', source: 'Account Statement', totalValue: assessment.e4 });
    }
    if (assessment.e5) {
      this.assetDetails.push({ category: 'Value Of Bonds', source: 'Account Statement', totalValue: assessment.e5 });
    }
    if (assessment.e6) {
      this.assetDetails.push({ category: 'Value Of IRA', source: 'Account Statement', totalValue: assessment.e6 });
    }
    if (assessment.e7) {
      this.assetDetails.push({ category: 'Value Of Deferred Compensation', source: 'Account Statement', totalValue: assessment.e7 });
    }
    if (assessment.e8) {
      this.assetDetails.push({ category: 'Property Value (Not Primary Residence)', source: 'Tax Assessment', totalValue: assessment.e8 });
    }
    if (assessment.e9) {
      this.assetDetails.push({ category: 'Trust Fund', source: 'Policy/Fund Statement', totalValue: assessment.e9 });
    }
    if (assessment.e10) {
      this.assetDetails.push({ category: 'Cash Surrender Value Of Life Insurance', source: 'Policy(ies)', totalValue: assessment.e10 });
    }
    if (assessment.e11) {
      this.assetDetails.push({ category: 'Other', source: assessment.OtherTextField123 ? assessment.OtherTextField123 : '', totalValue: assessment.e11 });
    }

  }

  saveInvestigationFinding() {
    this.investigationFinding = Object.assign({}, new InvestigationFinding);
    const maltreamentForm = this.maltreatementForm.value;
    const serviceworkerForm = this.serviceworkerForm.value;
    const legalInformation = this.legalInformationFormGroup.value;
    const clientLiving = this.clientsLivingSituationFormGroup.value;
    const medicationForm = this.medicationForm.value;
    this.investigationFinding.intakeserviceid = this.id;
    this.investigationFinding.invsfindingjurisdiction = maltreamentForm.countyid;
    this.investigationFinding.invsfindingaddress = maltreamentForm.address;
    this.investigationFinding.investigationfindingdate = maltreamentForm.insertedon;
    this.investigationFinding.socialhistorydesc = clientLiving.description;
    this.investigationFinding.familyhistorydesc = clientLiving.familyhistory;
    this.investigationFinding.educationalfactors = clientLiving.education;
    this.investigationFinding.psychiatricdesc = medicationForm.psychiatricdesc;
    this.investigationFinding.psychiatricimportinfo = this.psychiatricimportinfo;
    this.investigationFinding.financialimportinfo = this.financialimportinfo;
    this.investigationFinding.assetdetailsdesc = this.finacialDesc;
    this.investigationFinding.legalinfopoa = legalInformation.poa;
    this.investigationFinding.legalinforeppayee = legalInformation.reppayee;
    this.investigationFinding.legalinfocourtinvolved = legalInformation.courtinvolvement;
    this.investigationFinding.legalinfodesc = legalInformation.description;
    this.investigationFinding.clentcapacitydesc = this.clientCapacity;
    this.investigationFinding.reasonclosingdesc = this.closingReason;
    this.investigationFinding.apsworkersigndate = serviceworkerForm.workersigndate;
    this.investigationFinding.supervisorsigndate = serviceworkerForm.supervisorsigndate;
    this.investigationFinding.investigationfindingtypeperson = this.supportedPersonsSave;
    this.investigationFinding.investigationfindingguardian = this.guardian;
    this._commonHttpService.create(this.investigationFinding, 'Investigationfindings/addupdate').subscribe((data) => {
      if (data) {
        this._alertService.success('Investigation Finding Added Successfully');
      }
    });
    console.log(this.investigationFinding);
  }
}
