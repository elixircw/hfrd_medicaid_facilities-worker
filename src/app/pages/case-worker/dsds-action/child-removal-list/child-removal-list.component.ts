import { ActivatedRoute } from '@angular/router';
import { CaseWorkerUrlConfig } from './../../case-worker-url.config';
import {  Component, OnInit } from '@angular/core';
import { CommonHttpService } from './../../../../@core/services/common-http.service';
import { DsdsService } from '../_services/dsds.service';
import { DataStoreService } from '../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
  selector: 'child-removal-list',
  templateUrl: './child-removal-list.component.html',
  styleUrls: ['./child-removal-list.component.scss']
})

export class ChildRemovalListComponent implements OnInit {

    id: string;
    childRemovalList: any;
    isServiceCase = false;
    isShow: string;
    constructor(
        private _commonService: CommonHttpService,
        private route: ActivatedRoute,
        private _dsdsService: DsdsService,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    }
    ngOnInit() {
          this.getChildRemoval();
          this.isServiceCase = this._dsdsService.isServiceCase();
    }


    getChildRemoval() {
        let inputRequest: Object;
            inputRequest = {
                objectid: this.id,
                // objectid: '1ab13583-6d94-4929-92e0-09723df839f3',
                objecttypekey: 'servicecase'
            };
        this._commonService
            .getSingle(
                {
                    where: inputRequest,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .GetChildRemovalList + '?filter'
            )
            .subscribe(result => {
                if (result && result.length) {
                    this.childRemovalList = result;
                }
            });
    }
    toggleClient(modal) {
        this.isShow = modal;
      }
}

