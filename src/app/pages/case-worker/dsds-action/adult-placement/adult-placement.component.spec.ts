import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdultPlacementComponent } from './adult-placement.component';

describe('AdultPlacementComponent', () => {
  let component: AdultPlacementComponent;
  let fixture: ComponentFixture<AdultPlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultPlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdultPlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
