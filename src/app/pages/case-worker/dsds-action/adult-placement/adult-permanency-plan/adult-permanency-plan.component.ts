import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import {
    CommonHttpService,
    DataStoreService,
    AlertService
} from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { PermanencyPlanType, PermanencyPlan, PermanencyPlanSubType } from '../../placement/_entities/placement.model';
import { InvolvedPerson } from '../../service-plan/_entities/service-plan.model';
import { DSDSActionSummary } from '../../../_entities/caseworker.data.model';
import * as moment from 'moment';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../../@core/services/auth.service';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var $: any;

@Component({
  selector: 'adult-permanency-plan',
  templateUrl: './adult-permanency-plan.component.html',
  styleUrls: ['./adult-permanency-plan.component.scss']
})
export class AdultPermanencyPlanComponent implements OnInit {

  childAge: string | number;
  id: string;
  isAddForm = true;
  userInfo: AppUser;
  permanencyPlanViewDetail: PermanencyPlan;
  permanencyPlanForm: FormGroup;
  involvedPerson$: Observable<InvolvedPerson[]>;
  planTypeSubType$: Observable<PermanencyPlanType[]>;
  permanencyPlan$: Observable<PermanencyPlan[]>;
  primaryPlanSubType =  new PermanencyPlanType;
  concurrentPlanSubType= new PermanencyPlanType;
  dsdsActionsSummary = new DSDSActionSummary();
  private selectedChild: string;
  maxDate = new Date();
  private selectedPermanencyPlanCP: PermanencyPlanSubType[] = [];
  private selectedPermanencyPlanPP: PermanencyPlanSubType[] = [];
  constructor(
      private form: FormBuilder,
      private _httpService: CommonHttpService,
      private route: ActivatedRoute,
      private _dataStoreService: DataStoreService,
      private _alert: AlertService,
      private _authService: AuthService,
  ) {}

  ngOnInit() {
      this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
      this.userInfo = this._authService.getCurrentUser();
      this.initializeForm();
      this.planListing();

      if (this.userInfo.user.userprofile.displayname) {
          this.permanencyPlanForm.patchValue({
              caseworkername: this.userInfo.user.userprofile.displayname
          });
      }
      this.permanencyPlanForm
          .get('establisheddate')
          .valueChanges.subscribe(result => {
              this.permanencyPlanForm.patchValue({
                  achieveddate: null,
                  projecteddate: null
              });
          });
      this.permanencyPlanForm
          .get('projecteddate')
          .valueChanges.subscribe(result => {
              this.permanencyPlanForm.patchValue({ achieveddate: null });
          });
  }

  pesonDateOfBirth(item) {
      this.selectedChild = item.value.split('~')[0];
      if (item.value.split('~')[1]) {
          this.permanencyPlanForm.patchValue({
              dateofbirth: item.value.split('~')[1]
          });
          this.childAge = this.getAge(
              this.permanencyPlanForm.value.dateofbirth
          );
          this.permanencyPlanForm
              .get('dateofbirth').disable();
          const conPlan = this.permanencyPlanForm.value.primaryPlan;
          if (conPlan && conPlan.length) {
              conPlan.forEach((appla, index) => {
                  if (appla.permanencyplantypekey === 'APPLA') {
                      conPlan.splice(index, 1);
                  }
              });
              this.permanencyPlanForm.patchValue({ primaryPlan: conPlan });
          }
      }
  }
  addPlan() {
      this.permanencyPlanForm.enable();
      this.permanencyPlanForm.patchValue({
          caseworkername: this.userInfo.user.userprofile.displayname
      });
      this.loadDropdown();
  }
  resetPlan() {
      this.primaryPlanSubType.permanencyplansubtype = [];
      this.selectedPermanencyPlanPP = [];
      this.selectedPermanencyPlanCP = [];
      this.concurrentPlanSubType.permanencyplansubtype = [];
      this.permanencyPlanForm.reset();
      this.isAddForm = true;
  }
  onPrimaryPlanChange(event, init) {
      if (init === 0) {
      this.primaryPlanSubType = Object.assign({}, event);
      } else {
          this.primaryPlanSubType = Object.assign({}, event.value);
      }
      this.selectedPermanencyPlanPP = [];
      // event.value.map((subType) => {
          if (!this.primaryPlanSubType.permanencyplansubtype.length) {
              this.selectedPermanencyPlanPP.push({
                  permanencyplantypekey: this.primaryPlanSubType.permanencyplantypekey,
                  permanencyplansubtypekey: null,
                  plantype: 'PP'
              });
          }
      // });
      if (this.selectedPermanencyPlanPP.length !== this.primaryPlanSubType.permanencyplansubtype.length) {
          this.permanencyPlanForm.markAsPristine();
      } else {
          this.permanencyPlanForm.markAsDirty();
      }
  }
  onPrimarySubtypeChange(event) {
      this.selectedPermanencyPlanPP.push({
          permanencyplantypekey: event.value.permanencyplantypekey,
          permanencyplansubtypekey: event.value.permanencyplansubtypekey,
          plantype: 'PP'
      });
      this.permanencyPlanForm.markAsDirty();
  }
  onConcurrentPlanChange(event) {
      this.concurrentPlanSubType = Object.assign({}, event.value);
      this.selectedPermanencyPlanCP = [];
      // event.value.map((subType) => {
          if (!this.concurrentPlanSubType.permanencyplansubtype.length) {
              this.selectedPermanencyPlanCP.push({
                  permanencyplantypekey: this.concurrentPlanSubType.permanencyplantypekey,
                  permanencyplansubtypekey: null,
                  plantype: 'CP'
              });
          }
      // });
      if (this.selectedPermanencyPlanCP.length !== event.value.length) {
          this.permanencyPlanForm.markAsPristine();
      } else {
          this.permanencyPlanForm.markAsDirty();
      }
  }
  onConcurrentSubtypeChange(event) {
      console.log(event);
      this.selectedPermanencyPlanCP.push({
          permanencyplantypekey: event.value.permanencyplantypekey,
          permanencyplansubtypekey: event.value.permanencyplansubtypekey,
          plantype: 'CP'
      });
      this.permanencyPlanForm.markAsDirty();
  }
  savePlan() {
      const permanencyPlanInput = this.permanencyPlanForm.value;
      permanencyPlanInput.intakeserviceid = this.id;
      permanencyPlanInput.intakeservicerequestactorid = this.selectedChild;
      permanencyPlanInput.permanencyplan = this.selectedPermanencyPlanPP.concat(
          this.selectedPermanencyPlanCP
      );
      delete permanencyPlanInput.concurrentPlan;
      delete permanencyPlanInput.primaryPlan;
      this._httpService.create(permanencyPlanInput, CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PermanencyPlan.PermanencyPlanAdd).subscribe((response) => {
          console.log(response);
          (<any>$('#addpermanencyplan')).modal('hide');
          this.resetPlan();
          this.planListing();
      });
  }
  viewPlan(modal) {
      this.loadDropdown();
      this.permanencyPlanForm.disable();
      console.log(modal);
      modal.intakeservicerequestactorid = modal.intakeservicerequestactorid + '~' + modal.dateofbirth;
      this.permanencyPlanForm.patchValue(modal);
      this.permanencyPlanForm.patchValue({projecteddate: modal.projecteddate});
      setTimeout(() => {this.permanencyPlanForm.patchValue({achieveddate: modal.achieveddate}); }, 100);
      this.isAddForm = false;
      this.permanencyPlanViewDetail = modal;
  }
  private initializeForm() {
      this.permanencyPlanForm = this.form.group({
          intakeservicerequestactorid: ['', [Validators.required]],
          dateofbirth: [null],
          establisheddate: [null, [Validators.required]],
          projecteddate: [null, [Validators.required]],
          primaryPlan: ['', [Validators.required]],
          caseworkername: [''],
          reviseddate: [null],
          achieveddate: [null],
          concurrentPlan: ['', [Validators.required]],
          concurrentPlanSubType: [null, [Validators.required]],
          primaryPlanSubType: [null, [Validators.required]]
      });
  }
  private planListing() {
      const planList = this._httpService
          .getArrayList(
              {
                  method: 'get',
                  page: 1,
                  limit: 10,
                  where: { intakeserviceid: this.id }
              },
              CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PermanencyPlan
                  .PermanencyPlanList +
            '?filter'
          )
          .share();
      this.permanencyPlan$ = planList.pluck('data');
  }
  private loadDropdown() {
      const source = Observable.forkJoin([
          this._httpService.getArrayList(
              {
                  method: 'get',
                  where: { intakeservreqid: this.id }
              },
              CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                  .InvolvedPersonListUrl +
            '?data'
          ),
          this._httpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PermanencyPlan.PermanancyPlanTypeSubType + '?filter={}')
      ])
          .map((item) => {
              return {
                  children: item[0]['data'].filter((child) => child.rolename === 'RA' || child.rolename === 'AM'),
                  typeSubType: item[1]
              };
          })
          .share();
      this.involvedPerson$ = source.pluck('children');
      this.planTypeSubType$ = source.pluck('typeSubType');
      this.planTypeSubType$.subscribe(item => {
           item.map(res => {
              if (res.permanencyplantypekey === 'Reunification') {
                  this.onPrimaryPlanChange(res, 0);
                  this.permanencyPlanForm.patchValue({ primaryPlan: res });
              }
          });
      });
  }
  private getAge(dateValue) {
      if (
          dateValue &&
          moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()
      ) {
          const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
          return moment().diff(rCDob, 'years');
      } else {
          return '';
      }
  }

}
