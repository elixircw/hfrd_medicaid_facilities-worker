import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdultPermanencyPlanComponent } from './adult-permanency-plan.component';

describe('AdultPermanencyPlanComponent', () => {
  let component: AdultPermanencyPlanComponent;
  let fixture: ComponentFixture<AdultPermanencyPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultPermanencyPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdultPermanencyPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
