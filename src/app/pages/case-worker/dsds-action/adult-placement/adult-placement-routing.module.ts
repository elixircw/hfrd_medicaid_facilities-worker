import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdultPlacementComponent } from './adult-placement.component';

const routes: Routes = [{
  path: '',
  component: AdultPlacementComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdultPlacementRoutingModule { }
