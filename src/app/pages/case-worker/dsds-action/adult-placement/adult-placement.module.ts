import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdultPlacementRoutingModule } from './adult-placement-routing.module';
import { AdultPlacementComponent } from './adult-placement.component';
import { AdultPermanencyPlanComponent } from './adult-permanency-plan/adult-permanency-plan.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatExpansionModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { AgmCoreModule } from '@agm/core';
import { PaginationModule } from 'ngx-bootstrap';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    AdultPlacementRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatExpansionModule,
    A2Edatetimepicker,
    AgmCoreModule,
    PaginationModule,
    SharedDirectivesModule
  ],
  declarations: [AdultPlacementComponent, AdultPermanencyPlanComponent]
})
export class AdultPlacementModule { }
