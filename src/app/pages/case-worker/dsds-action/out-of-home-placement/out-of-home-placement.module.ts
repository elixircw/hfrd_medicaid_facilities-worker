import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OutOfHomePlacementRoutingModule } from './out-of-home-placement-routing.module';
import { OutOfHomePlacementComponent } from './out-of-home-placement.component';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';

@NgModule({
  imports: [
    CommonModule,
    OutOfHomePlacementRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker
  ],
  declarations: [OutOfHomePlacementComponent]
})
export class OutOfHomePlacementModule { }
