import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaltreatmentInformationComponent } from './maltreatment-information.component';

describe('MaltreatmentInformationComponent', () => {
  let component: MaltreatmentInformationComponent;
  let fixture: ComponentFixture<MaltreatmentInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaltreatmentInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaltreatmentInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
