import { DatePipe } from '@angular/common';
import { uniqBy } from 'lodash';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';


import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, DataStoreService, GenericService, CommonDropdownsService, SessionStorageService } from '../../../../@core/services';
import { DSDSActionSummary, InvolvedPerson } from '../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { AddMaltreatment, Areaofinjury, Indicator, Injurycharactertic, Maltreatmentcharacterstic, MaltreatmentInformation, ProviderMaltreatment } from './_entites/maltreatment.data.model';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'maltreatment-information',
    templateUrl: './maltreatment-information.component.html',
    styleUrls: ['./maltreatment-information.component.scss'],
    providers: [DatePipe]
})
export class MaltreatmentInformationComponent implements OnInit, AfterViewChecked {
    maltreatmentFormGroup: FormGroup;
    isAnotherJurisdiction: boolean;
    maxDate = new Date();
    minDate = new Date();
    isSubmitting: boolean;
    maltreatmentInformation: MaltreatmentInformation[] = [];
    jurisdictionDropdownItems$: Observable<DropdownModel[]>;
    maltreatmentTypeDropdownItems$: Observable<DropdownModel[]>;
    injuryTypeDropdownItems$: Observable<DropdownModel[]>;
    maltreatmentCharactersticsTypeDropdownItems$: Observable<DropdownModel[]>;
    injuryCharactersticsTypeDropdownItems$: Observable<DropdownModel[]>;
    providerMaltreatmentTypeItems: DropdownModel[] = [];
    id: string;
    dsdsActionsSummary = new DSDSActionSummary();
    indicator: Indicator[] = [];
    areaOfInjury: Areaofinjury[] = [];
    maltreatmentCharactersticType: Maltreatmentcharacterstic[] = [];
    injuryCharacterticType: Injurycharactertic[] = [];
    addMaltreatment = new AddMaltreatment();
    isShowJurisdiction = false;
    childDetails: DropdownModel[] = [];
    incidentLocation: DropdownModel[] = [];
    isInitialLoad = false;
    personInfo: string;
    personInf: InvolvedPerson[] = [];
    involvedPersons: InvolvedPerson[] = [];
    providermaltreatment: ProviderMaltreatment[] = [];
    sdmAllegation: any = {};
    maltreatmentTypeDropdownItems: DropdownModel[] = [];
    allegationForm: FormGroup;
    abusedChildList: any;
    selectedChild: any;
    searchprovider: any = {};
    selectedProvider: any;
    providersList = [];
    selectedallegationForm: FormGroup;
    isProbabilityPresent: string;
    foundAllegation: boolean;
    allegedChild: any;
    reportSummary: any;
    disableForm: boolean;
    isClosed = false;
    daNumber: string;
    minincidentDate= new Date();
    incidentdate= new Date();
    showAppeal: boolean;
    constructor(
        private _commonHttpService: CommonHttpService,
        private _formBuilder: FormBuilder,
        private _route: ActivatedRoute,
        private _service: GenericService<AddMaltreatment>,
        private _dataStoreService: DataStoreService,
        private _commonDDService: CommonDropdownsService,
        private _alertService: AlertService,
        private _authService: AuthService,
        private cdRef: ChangeDetectorRef,
        private _router: Router,
        private datePipe: DatePipe,
        private storage: SessionStorageService
    ) {
        this._router.routeReuseStrategy.shouldReuseRoute = function(){
            return false;
         }

         this._router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
               // trick the Router into believing it's last link wasn't previously loaded
               this._router.navigated = false;
               // if you need to scroll back to top, here is the right place
               window.scrollTo(0, 0);
            }
        });
     }
    ngOnInit() {
        this.setsearchprovider();
        // this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.id = this._commonDDService.getStoredCaseUuid();
        this.daNumber = this._commonDDService.getStoredCaseNumber();
        this.disableForm = false;
        this.initialFormGroup();
        // this.getInvolvedPersons();
        this.getJurisdictionDropdown();
        this.getSDM();
        this._authService.currentUser.subscribe((userInfo) => { });
        this.isInitialLoad = true;
        this._dataStoreService.currentStore.subscribe((store) => {
            if (this.isInitialLoad && store['dsdsActionsSummary']) {
                this.dsdsActionsSummary = store['dsdsActionsSummary'];
                if (this.dsdsActionsSummary) {
                    this.getDropdown();
                    this.getInvolvedPerson();
                    this.getIncidentLocation();
                    this.getMaltreatmentInformation();
                    this.providerMaltreatmentType();
                    this.isInitialLoad = false;

                    //@TM: disable Maltreatment Allegation edit function for closed cases
                    if(this.dsdsActionsSummary.da_disposition === 'Close Case' || this.dsdsActionsSummary.da_status === 'Closed') {
                        this.disableForm = true;
                    }
                }
            }
        });
        this.setSelectedChildData();
        const da_status = this.storage.getItem('da_status');
        if (da_status) {
         if (da_status === 'Closed' || da_status === 'Completed') {
             this.isClosed = true;
         } else {
             this.isClosed = false;
         }
        }
        this.setMinincidentDate();


        this._commonHttpService.getPagedArrayList(
                    new PaginationRequest({
                        page: 1,
                        limit: 20,
                        where: {
                            servicerequestid: this.id
                        },
                        method: 'get'
                    }), 'Intakeservicerequestdispositioncodes/GetHistory?filter'
                ).subscribe(
                    (result) => {
                        const history = (result.data) ? result.data : [];
                        this.showAppeal = false;
                        if (history && history.length > 0) {
                            const isCompleted = history.find(dispo => (dispo.dispstatus === 'Completed') && (dispo.routingstatus === 'Review'));
                            if (isCompleted) {
                            this.showAppeal = true;
                            }
                        }
                        return result.data;
                    }
                );
    }
    ngAfterViewChecked() {
        this.cdRef.markForCheck();
        this.cdRef.detectChanges();
    }

    setSelectedChildData() {
        const X = this._dataStoreService.getData("holdChildValue");
        if(X) {
            this.selectChild(X);
            this._dataStoreService.setData("holdChildValue", null);
        }
    }
    setMinincidentDate() {
        const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
         // For case min incident date will be the Case received date
        if (caseInfo) {
            this.minincidentDate = caseInfo.da_receiveddate;
            }

    }

initialFormGroup() {
        this.maltreatmentFormGroup = this._formBuilder.group({
            countyid: [null],
            incidentlocationtypekey: [null],
            isjurisdiction: ['0'],
            isnotapplicable: [null],
            notapplicablecomments: [null],
            caseworker: [''],
            supervisor: [''],
            person: this._formBuilder.array([]),
            allegations: this._formBuilder.array([])
        });
    }

    setFormValues() {
        if (this.maltreatmentInformation) {
            const control = <FormArray>this.maltreatmentFormGroup.controls['person'];
            this.maltreatmentInformation.forEach((x, index) => {
                control.push(this.buildPersonForm(x, index));
            });
        }
    }

    private buildPersonForm(x, personIndex): FormGroup {
        if (x.dateofdeath) {
            const childDod = this.datePipe.transform(x.dateofdeath, 'MM/dd/yyyy');
            this.personInfo = x.personname + ' ' + childDod;
        } else {
            this.personInfo = x.personname;
        }
        return this._formBuilder.group({
            personname: this.personInfo,
            personid: x.personid ? x.personid : '',
            intakeservicerequestactorid: x.intakeservicerequestactorid ? x.intakeservicerequestactorid : '',
            investigationallegation: this.buildInvestigationallegation(x, personIndex)
        });
    }

    showJurisdiction(isShow) {
        isShow === '1' ? this.maltreatmentFormGroup.get('countyid').enable() : this.maltreatmentFormGroup.get('countyid').disable();
        if (!this.isShowJurisdiction) {
            this.maltreatmentFormGroup.patchValue({ countyid: null });
        }
        if (isShow === '0') {
            this.isAnotherJurisdiction = false;
        } else {
            this.isAnotherJurisdiction = true;
        }
    }

    buildInvestigationallegation(inverstigation, personIndex) {
        let investigation: FormGroup[] = [];
        if (inverstigation.investigationallegation) {
            investigation = inverstigation.investigationallegation.map((item) => {
                return this.buildMaltreatmentTypeFormGroup(item, personIndex);
            });
            const invFG = this._formBuilder.array([...investigation]);
            return invFG;
            // return this._formBuilder.array([...investigation]);
        } else {
            return this._formBuilder.array([this.maltreatmentTypeFormGroup()]);
        }
    }

    // D-06597 - Fix for Maltreatment type and Alleged Maltreators and Relationship to Victim fields should be mandatory fields
    maltreatmentTypeFormGroup() {
        const controls = this.providerMaltreatmentTypeItems.map(c => {
            return new FormControl(false);
        });
        return this._formBuilder.group({
            investigationallegationid: [''],
            incidentdate: [null],
            allegationid: ['', Validators.required],
            isShowAllegation: [false],
            maltreatoractorid: [null, Validators.required],
            injurytypekey: [''],
            maltreatmentcharactersticstypekey: [''],
            injurycharactersticstypekey: [''],
            comments: ['', Validators.required],
            injurycomments: ['', Validators.required],
            enddate: [null],
            isapproximatedate: [false],
            timeofincidence: [null],
            sextrafficking: [''],
            injurytypedescription: [''],
            maltreatmentcharactersticstypedescription: [''],
            injurycharactersticstypedescription: [''],
            maltreatorname: '',
            allegationname: '',
            isShowSextrafficking: false,
            isproviderinvolved: [''],
            ischildfatality: [0],
            isDisplayProviderInvolved: [''],
            providerMaltreatmentItems: new FormArray(controls)
        });
    }

    addMaltreatmentType(index) {
        const control = <FormArray>this.maltreatmentFormGroup.controls['person'];
        control.controls[index]['controls']['investigationallegation'].push(this.maltreatmentTypeFormGroup());
    }

    private buildMaltreatmentTypeFormGroup(x, personIndex) {
        const controls = this.providerMaltreatmentTypeItems.map(c => {
            if (x.providermaltreatment && x.providermaltreatment.filter(p => p.providermaltreatmenttypekey === c.value).length) {
                return new FormControl(true);
            } else {
                return new FormControl(false);
            }

        });
        const invFG = this._formBuilder.group({
            investigationallegationid: x.investigationallegationid ? x.investigationallegationid : '',
            incidentdate: x.incidentdate ? x.incidentdate : null,
            allegationid: x.allegationid ? x.allegationid : '',
            isShowAllegation: x.allegationid ? true : false,
            maltreatoractorid: '',
            injurytypekey: '',
            maltreatmentcharactersticstypekey: '',
            injurycharactersticstypekey: '',
            comments: x.comments ? x.comments : '',
            injurycomments: x.injurycomments ? x.injurycomments : '',
            enddate: x.enddate ? x.enddate : null,
            isapproximatedate: x.isapproximatedate === 1 ? true : false,
            timeofincidence: x.timeofincidence ? x.timeofincidence : null,
            sextrafficking: x.sextrafficking ? x.sextrafficking.toString() : '0',
            injurytypedescription: '',
            maltreatmentcharactersticstypedescription: '',
            injurycharactersticstypedescription: '',
            maltreatorname: '',
            allegationname: x.allegationname ? x.allegationname : '',
            ischildfatality: x.ischildfatality ? x.ischildfatality : 0,
            isShowSextrafficking: x.allegationname ? (x.allegationname === 'Sexual Abuse' ? true : false) : false,
            isproviderinvolved: x.isproviderinvolved ? x.isproviderinvolved.toString() : (x.isproviderinvolved === 0 ? '0' : null),
            isDisplayProviderInvolved: x.isproviderinvolved ? (x.isproviderinvolved === 1 ? true : false) : false,
            providerMaltreatmentItems: new FormArray(controls),
        });


        const injurytype = x.injurytype ? x.injurytype.map((item) => item.injurytypekey) : null;
        const injurycharactersticstype = x.injurycharactersticstype ? x.injurycharactersticstype.map((item) => item.injurycharactersticstypekey) : null;
        const maltreatmentcharactersticstypekey = x.maltreatmentcharactersticstypekey ? x.maltreatmentcharactersticstypekey.map((item) => item.maltreatmentcharactersticstypekey) : null;
        const maltreators = x.maltreators ? x.maltreators.map((item) => item.intakeservicerequestactorid) : null;
        invFG.controls['injurytypekey'].patchValue(injurytype);
        invFG.controls['injurycharactersticstypekey'].patchValue(injurycharactersticstype);
        invFG.controls['maltreatmentcharactersticstypekey'].patchValue(maltreatmentcharactersticstypekey);
        invFG.controls['maltreatoractorid'].patchValue(maltreators);

        const injurytypedescription = x.injurytype ? x.injurytype.map((item) => item.typedescription) : null;
        const injurycharactersticstypedescription = x.injurycharactersticstype ? x.injurycharactersticstype.map((item) => item.typedescription) : null;
        const maltreatmentcharactersticstypedescription = x.maltreatmentcharactersticstypekey ? x.maltreatmentcharactersticstypekey.map((item) => item.typedescription) : null;
        const maltreatorstypedescription = x.maltreators
            ? x.maltreators.map((item) => {
                if (item && item.relationship) {
                    return item.personname + ' - ' + item.relationship;
                } else {
                    return item.personname;
                }
            })
            : null;

        invFG.controls['injurytypedescription'].patchValue(injurytypedescription);
        invFG.controls['injurycharactersticstypedescription'].patchValue(injurycharactersticstypedescription);
        invFG.controls['maltreatmentcharactersticstypedescription'].patchValue(maltreatmentcharactersticstypedescription);
        invFG.controls['maltreatorname'].patchValue(maltreatorstypedescription);
        if (x.allegationid) {
            invFG.disable();
        }
        return invFG;
    }

    onChangeIndicator(event) {
        if (event) {
            // const indicator = event.map((res) => {
            //     return { maltreatoractorid: res };
            // });
            const indicators = [];
            indicators.push({ maltreatoractorid: event });
            return indicators;
        }
    }
    onChangeInjuryTypeKey(event) {
        if (event) {
            const areaOfInjury = event.map((res) => {
                return { injurytypekey: res };
            });
            return areaOfInjury;
        }
    }
    onChangeMaltreatmentCharacteristicsType(event) {
        if (event) {
            const maltreatmentCharactersticType = event.map((res) => {
                return { maltreatmentcharactersticstypekey: res };
            });
            return maltreatmentCharactersticType;
        }
    }

    onProviderMaltreatmentChange(personIndex: number, investigationIndex: number, selectedValue: string, $event: any) {
        const control = <FormArray>this.maltreatmentFormGroup.controls['person'];
        const providerMaltreatmentFormArray = <FormArray>
            control.controls[personIndex]['controls']['investigationallegation']['controls'][investigationIndex].controls['providerMaltreatmentItems']['controls'];
        if ($event.checked) {
            providerMaltreatmentFormArray.push(new FormControl(selectedValue));
        } else {
            const index = providerMaltreatmentFormArray.controls.findIndex(x => x.value === selectedValue);
            providerMaltreatmentFormArray.removeAt(index);
        }
    }

    onChangeInjuryCharactersticsType(event) {
        if (event) {
            const injuryCharacterticType = event.map((res) => {
                return { injurycharactersticstypekey: res };
            });
            return injuryCharacterticType;
        }
    }

    private getMaltreatmentInformation() {
        Observable.forkJoin([
            this._commonHttpService
                .getArrayList(
                    {
                        where: { investigationid: this.dsdsActionsSummary.da_investigationid },
                        method: 'get'
                    },
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.MaltreatmentInformation + '?filter'
                ),
            this._commonHttpService.getArrayList(
                {
                    order: 'displayorder ASC'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.providermaltreatmenttypeUrl
            ),
            this._commonHttpService.getSingle(new PaginationRequest({}), CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.ReportSummary + this.id)])
            .subscribe((result) => {
                console.log('report summary', result[2]);
                this.reportSummary = result[2];
                this.maltreatmentInformation = (Array.isArray(result[0])) ? result[0] : []; // uniqBy(result[0], "personid");
                this.providerMaltreatmentTypeItems = result[1].map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.providermaltreatmenttypekey
                        })
                );
                if (this.maltreatmentInformation && this.maltreatmentInformation[0]) {
                    if (this.maltreatmentInformation[0].isjurisdiction) {
                        this.maltreatmentInformation[0].isjurisdiction = this.maltreatmentInformation[0].isjurisdiction.toString();
                        this.isAnotherJurisdiction = true;
                    } else {
                        this.maltreatmentInformation[0].isjurisdiction = '0';
                    }
                    this.setFormValues();
                    this.maltreatmentFormGroup.patchValue({
                        incidentlocationtypekey: this.maltreatmentInformation[0].incidentlocationtypekey ? this.maltreatmentInformation[0].incidentlocationtypekey : null,
                        countyid: this.maltreatmentInformation[0].countyid,
                        isjurisdiction: this.maltreatmentInformation[0].isjurisdiction
                    });
                    if (this.maltreatmentInformation[0].roles && this.maltreatmentInformation[0].roles.length) {
                        this.maltreatmentInformation[0].roles.map((item) => {
                            if (item.role === 'CW') {
                                this.maltreatmentFormGroup.patchValue({ caseworker: item.username });
                            }
                            if (item.role === 'SP') {
                                this.maltreatmentFormGroup.patchValue({ supervisor: item.username });
                            }
                        });
                    }
                    this.maltreatmentInformation[0].isjurisdiction === '1' ? this.maltreatmentFormGroup.get('countyid').enable() : this.maltreatmentFormGroup.get('countyid').disable();
                }
            });
    }

    reloadCurrPage() {
        const url = '/pages/case-worker/'+ this.id +'/'+ this.daNumber +'/dsds-action/maltreatment-information';
        this._router.navigate([url]);
    }
    // D-06597 - Fix for Maltreatment type and Alleged Maltreators and Relationship to Victim fields should be mandatory fields
    saveMaltreatment(person, data, personIndex, InvestigationIndex) {
        console.log(data);
        if (this.isProbabilityPresent === 'No Maltreator found.') {
            (<any>$('#maltreatment-popup')).modal('show');
            return;
        } else if (this.isProbabilityPresent === 'No Maltreatment Present.'){
            (<any>$('#maltreatment-popup')).modal('show');
            return;
        }
        this.addMaltreatment = Object.assign(new AddMaltreatment(), data.value);

        // D-07005 Start
        if (!this.addMaltreatment.comments && !this.addMaltreatment.isnotapplicable) {
            this._alertService.error('Please Enter Maltreatment Characteristics Comments');
            return;
        }
        // D-07005 End
        if (!this.addMaltreatment.injurycomments && !this.addMaltreatment.isnotapplicable) {
            this._alertService.error('Please Enter Injury Characteristics Comments');
            return;
        }
        if (!this.addMaltreatment.allegationid) {
            this._alertService.error('Please Select Maltreatment Type and Alleged Maltreators.');
            return;
        }
        if (!data.value.maltreatoractorid) {
            this._alertService.error('Please Alleged Maltreators.');
            return;
        }
        if (this.addMaltreatment.allegationid) {
            this.addMaltreatment.allegationid = this.addMaltreatment.allegationid.split('~')[0];
            this.addMaltreatment.maltreatmentid = null; // this.maltreatmentInformation[0].maltreatmentid;
            this.addMaltreatment.personid = person.value.personid;
            this.addMaltreatment.intakeservicerequestactorid = person.value.intakeservicerequestactorid;
            this.addMaltreatment.maltreators = this.onChangeIndicator(data.value.maltreatoractorid);
            this.addMaltreatment.areaofinjury = this.onChangeInjuryTypeKey(data.value.injurytypekey);
            this.addMaltreatment.maltreatmentcharacterstics = this.onChangeMaltreatmentCharacteristicsType(data.value.maltreatmentcharactersticstypekey);
            this.addMaltreatment.injurycharactertics = this.onChangeInjuryCharactersticsType(data.value.injurycharactersticstypekey);
            this.addMaltreatment.isjurisdiction = this.maltreatmentFormGroup.value.isjurisdiction;
            this.addMaltreatment.isnotapplicable = this.maltreatmentFormGroup.value.isnotapplicable ? '1' : '0';
            this.addMaltreatment.notapplicablecomments = this.maltreatmentFormGroup.value.notapplicablecomments;
            this.addMaltreatment.countyid = this.maltreatmentFormGroup.value.countyid ? this.maltreatmentFormGroup.value.countyid : null;
            this.addMaltreatment.incidentlocationtypekey = this.maltreatmentFormGroup.value.incidentlocationtypekey;
            this.addMaltreatment.investigationid = this.dsdsActionsSummary.da_investigationid;
            this.addMaltreatment.investigationallegationid = this.addMaltreatment.investigationallegationid ? this.addMaltreatment.investigationallegationid : null;
            this.addMaltreatment.timeofincidence = this.addMaltreatment.timeofincidence ? this.addMaltreatment.timeofincidence : null;
            this.addMaltreatment.isapproximatedate = this.addMaltreatment.isapproximatedate ? '1' : '0';
            this.addMaltreatment.isproviderinvolved = this.addMaltreatment.isproviderinvolved ? this.addMaltreatment.isproviderinvolved : null;
            this.addMaltreatment.providerMaltreatmentItems.map((item, index) => {
                if (item) {
                    this.providermaltreatment.push({ providermaltreatmenttypekey: this.providerMaltreatmentTypeItems[index].value });
                }
            });
            this.addMaltreatment.providermaltreatment = this.providermaltreatment;
            this.addMaltreatment.isnotapplicable = this.addMaltreatment.isnotapplicable ? '1' : '0';
            // Validate Date of Incident Start and End dates
            const eDateStr = data.value.enddate ? data.value.enddate + '' : null;
            let eDate;
            const incidentdateStr = data.value.incidentdate ? data.value.incidentdate + '' : null;
            let iDate;
            const receivedDateStr = this._dataStoreService.getData('da_receiveddate');
            let receivedDate;
            if (receivedDateStr) {
                receivedDate = moment(new Date(receivedDateStr.substr(0, 16)));
            }
            if (eDateStr) {
                eDate = moment(new Date(eDateStr.substr(0, 16)));
                data.value.enddate = eDate.format('MM/DD/YYYY');
            }
            if (incidentdateStr) {
                iDate = moment(new Date(incidentdateStr.substr(0, 16)));
                data.value.incidentdate = iDate.format('MM/DD/YYYY');
            }
            if (eDate && iDate && iDate.isAfter(eDate)) {
                this._alertService.error('Date of Incident End Date should be after Start Date');
                return false;
            }

            if (receivedDate && iDate && !receivedDate.isSame(iDate, 'day') && iDate.isAfter(receivedDate)) {
                this._alertService.error('Date of Incident should be on or before received date [' + receivedDate.format('MM/DD/YYYY') + ']');
                return false;
            }

            // End of Validation
            this.addMaltreatment.enddate = data.value.enddate ? data.value.enddate : null;
            this.addMaltreatment.incidentdate = data.value.incidentdate ? data.value.incidentdate : null;

            if (this.addMaltreatment.maltreators) {
                this.addMaltreatment.maltreators.map((item) => {
                    if (item.maltreatoractorid === 'unknown' || item.maltreatoractorid === 'nomaltreator') {
                        if (item.maltreatoractorid === 'unknown') {
                            item.othermaltreator = 'unknown';
                        }
                        if (item.maltreatoractorid === 'nomaltreator') {
                            item.othermaltreator = 'nomaltreator';
                        }
                        item.maltreatoractorid = null;
                    }
                });
                if (this.maltreatmentFormGroup.value.caseworker) {
                    const jurisdictionuser = Object.assign({
                        name: this.maltreatmentFormGroup.value.caseworker,
                        role: 'CW'
                    });
                    this.addMaltreatment.jurisdictionuser.push(jurisdictionuser);
                }
            }
            if (this.maltreatmentFormGroup.value.supervisor) {
                const jurisdictionuser = Object.assign({
                    name: this.maltreatmentFormGroup.value.supervisor,
                    role: 'SP'
                });
                this.addMaltreatment.jurisdictionuser.push(jurisdictionuser);
            }
            this.addMaltreatment.objectid = this.id;
            this.addMaltreatment.objecttype = 'servicerequest';
            if (this.addMaltreatment.investigationallegationid) {
                this._service.create(this.addMaltreatment, CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.AddMaltreatmentINformationUrl).subscribe(
                    (response) => {
                        if (response) {

                            this._alertService.success('Maltreatment Updated successfully');
                            window.location.reload(true);
                        }
                    },
                    (error) => {
                        this.isSubmitting = true;
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                this._service.create(this.addMaltreatment, CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.AddMaltreatmentINformationUrl).subscribe(
                    (response) => {
                        if (response) {
                            this.isSubmitting = true;
                            this._alertService.success('Maltreatment added successfully');
                            this.providermaltreatment = [];
                            this.initialFormGroup();
                            this.getMaltreatmentInformation();
                            window.location.reload(true);
                        }
                    },
                    (error) => {
                        this.isSubmitting = true;
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            }
        }
    }

    saveMaltreatmentAllegation(person) {
        console.log(person);
        if (this.isProbabilityPresent === 'No Maltreator found.') {
            (<any>$('#maltreatment-popup')).modal('show');
            return;
        } else if (this.isProbabilityPresent === 'No Maltreatment Present.'){
            (<any>$('#maltreatment-popup')).modal('show');
            return;
        }
        const maltreatment = person.value;
        this.addMaltreatment = Object.assign(new AddMaltreatment(), person.value);
        if (!this.addMaltreatment.notapplicablecomments && this.addMaltreatment.isnotapplicable) {
            this._alertService.error('Please Enter Comments');
            return;
        }
        // D-07005 Start
        if (!this.addMaltreatment.comments && !this.addMaltreatment.isnotapplicable) {
            this._alertService.error('Please Enter Maltreatment Characteristics Comments');
            return;
        }
        // D-07005 End
        if (!this.addMaltreatment.injurycomments && !this.addMaltreatment.isnotapplicable) {
            this._alertService.error('Please Enter Injury Characteristics Comments');
            return;
        }
        if (!this.addMaltreatment.allegationid) {
            this._alertService.error('Please Select Maltreatment Type and Alleged Maltreators.');
            return;
        }
        if (!maltreatment.maltreatoractorid) {
            this._alertService.error('Please Alleged Maltreators.');
            return;
        }
        if (this.addMaltreatment.allegationid) {
            this.addMaltreatment.allegationid = this.addMaltreatment.allegationid.split('~')[0];
            this.addMaltreatment.maltreatmentid = null;
            this.addMaltreatment.personid = this.selectedChild.personid;
            this.addMaltreatment.intakeservicerequestactorid = this.selectedChild.intakeservicerequestactorid;
            this.addMaltreatment.maltreators = this.onChangeIndicator(maltreatment.maltreatorid);
            this.addMaltreatment.areaofinjury = this.onChangeInjuryTypeKey(maltreatment.injurytypekey);
            this.addMaltreatment.maltreatmentcharacterstics = this.onChangeMaltreatmentCharacteristicsType(maltreatment.maltreatmentcharactersticstypekey);
            this.addMaltreatment.injurycharactertics = this.onChangeInjuryCharactersticsType(maltreatment.injurycharactersticstypekey);
            this.addMaltreatment.isjurisdiction = maltreatment.isjurisdiction;
            this.addMaltreatment.isnotapplicable = maltreatment.isnotapplicable ? 1 : 0;
            this.addMaltreatment.notapplicablecomments = maltreatment.notapplicablecomments;
            this.addMaltreatment.countyid = maltreatment.countyid ? maltreatment.countyid : null;
            this.addMaltreatment.incidentlocationtypekey = maltreatment.incidentlocationtypekey;
            this.addMaltreatment.investigationid = this.dsdsActionsSummary.da_investigationid;
            this.addMaltreatment.investigationallegationid = this.addMaltreatment.investigationallegationid ? this.addMaltreatment.investigationallegationid : null;
            this.addMaltreatment.timeofincidence = this.addMaltreatment.timeofincidence ? this.addMaltreatment.timeofincidence : null;
            this.addMaltreatment.isapproximatedate = this.addMaltreatment.isapproximatedate;
            this.addMaltreatment.isproviderinvolved = this.addMaltreatment.isproviderinvolved ? this.addMaltreatment.isproviderinvolved : null;
            this.addMaltreatment.providerMaltreatmentItems.map((item, index) => {
                if (item) {
                    this.providermaltreatment.push({ providermaltreatmenttypekey: this.providerMaltreatmentTypeItems[index].value });
                }
            });
            this.addMaltreatment.providermaltreatment = this.providermaltreatment;
            this.addMaltreatment.isnotapplicable = this.addMaltreatment.isnotapplicable ? 1 : 0;
            // Validate Date of Incident Start and End dates
            const eDateStr = maltreatment.enddate ? maltreatment.enddate + '' : null;
            let eDate;
            const incidentdateStr = maltreatment.incidentdate ? maltreatment.incidentdate + '' : null;
            let iDate;
            const receivedDateStr = this._dataStoreService.getData('da_receiveddate');
            let receivedDate;
            if (receivedDateStr) {
                receivedDate = moment(new Date(receivedDateStr.substr(0, 16)));
            }
            if (eDateStr) {
                eDate = moment(new Date(eDateStr.substr(0, 16)));
                maltreatment.enddate = eDate.format('MM/DD/YYYY');
            }
            if (incidentdateStr) {
                iDate = moment(new Date(incidentdateStr.substr(0, 16)));
                maltreatment.incidentdate = iDate.format('MM/DD/YYYY');
            }
            if (eDate && iDate && iDate.isAfter(eDate)) {
                this._alertService.error('Date of Incident End Date should be after Start Date');
                return false;
            }

            if (receivedDate && iDate && !receivedDate.isSame(iDate, 'day') && iDate.isAfter(receivedDate)) {
                this._alertService.error('Date of Incident should be on or before received date [' + receivedDate.format('MM/DD/YYYY') + ']');
                return false;
            }

            // End of Validation
            this.addMaltreatment.enddate = maltreatment.enddate ? maltreatment.enddate : null;
            this.addMaltreatment.incidentdate = maltreatment.incidentdate ? maltreatment.incidentdate : null;

            if (this.addMaltreatment.maltreators) {
                this.addMaltreatment.maltreators.map((item) => {
                    if (item.maltreatoractorid === 'unknown' || item.maltreatoractorid === 'nomaltreator') {
                        if (item.maltreatoractorid === 'unknown') {
                            item.othermaltreator = 'unknown';
                        }
                        if (item.maltreatoractorid === 'nomaltreator') {
                            item.othermaltreator = 'nomaltreator';
                        }
                        item.maltreatoractorid = null;
                    }
                });
            }
            if (maltreatment.caseworker) {
                const jurisdictionuser = Object.assign({
                    name: maltreatment.caseworker,
                    role: 'CW'
                });
                this.addMaltreatment.jurisdictionuser.push(jurisdictionuser);
            }

            if (maltreatment.supervisor) {
                const jurisdictionuser = Object.assign({
                    name: maltreatment.supervisor,
                    role: 'SP'
                });
                this.addMaltreatment.jurisdictionuser.push(jurisdictionuser);
            }
            this.isSubmitting = true;
            this.addMaltreatment.objectid = this.id;
            this.addMaltreatment.objecttype = 'servicerequest';
            if (this.addMaltreatment.investigationallegationid) {
                this.addMaltreatment.maltreatmentid = maltreatment.maltreatmentid;
                this._service.create(this.addMaltreatment, CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.AddMaltreatmentINformationUrl).subscribe(
                    (response) => {
                        this._dataStoreService.setData("holdChildValue", this.selectedChild);
                        this.reloadCurrPage();
                        if (response) {
                            this._alertService.success('Maltreatment Updated successfully');
                        }
                    },
                    (error) => {
                        this.isSubmitting = false;
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                this._service.create(this.addMaltreatment, CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.AddMaltreatmentINformationUrl).subscribe(
                    (response) => {

                        if (response) {
                            this._alertService.success('Maltreatment added successfully', true);
                            this.providermaltreatment = [];
                            this.initialFormGroup();
                            this.getMaltreatmentInformation();

                            setTimeout(() => {
                                this._dataStoreService.setData("holdChildValue", this.selectedChild);
                                this.reloadCurrPage();
                            }, 1000);
                        }
                    },
                    (error) => {
                        this.isSubmitting = false;
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            }
        }
    }

    private getJurisdictionDropdown() {
        this.jurisdictionDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    method: 'get',
                    order: 'countyname asc',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.JurisdictionListUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }

    private getDropdown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    nolimit: true,
                    method: 'get',
                    where: {
                        intakeservicereqtypeid: this.dsdsActionsSummary.da_typeid,
                        intakeservicereqsubtypeid: this.dsdsActionsSummary.da_subtypeid,
                        investigationid: this.dsdsActionsSummary.da_investigationid
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.GetMaltreatementTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    nolimit: true,
                    method: 'get',
                    order: 'displayorder ASC'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.InjuryTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    nolimit: true,
                    method: 'get',
                    order: 'displayorder ASC'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.MaltreatmentCharactersticsTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    nolimit: true,
                    method: 'get',
                    order: 'displayorder ASC'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.injuryCharactersticsTypeUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    maltreatmentType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.name,
                                value: res.allegationid,
                                additionalProperty: res.isenablesextraffic
                            })
                    ),
                    injuryType: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.injurytypekey
                            })
                    ),
                    maltreatmentCharactersticsType: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.maltreatmentcharactersticstypekey
                            })
                    ),
                    injuryCharactersticsType: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.injurycharactersticstypekey
                            })
                    )
                };
            })
            .share();
        this.maltreatmentTypeDropdownItems$ = source.pluck('maltreatmentType');
        this.maltreatmentTypeDropdownItems$.subscribe(data => {
            this.maltreatmentTypeDropdownItems = data;
            let i = 0;
            data.forEach(maltreatment => {
                this.filterAllegations(maltreatment.text)
                if (this.foundAllegation) {
                    i++;
                }
            });
            this.isProbabilityPresent = (i > 0) ? null : 'No Maltreatment Present.';
            // this.createFormarray();
        });
        this.injuryTypeDropdownItems$ = source.pluck('injuryType');
        this.maltreatmentCharactersticsTypeDropdownItems$ = source.pluck('maltreatmentCharactersticsType');
        this.injuryCharactersticsTypeDropdownItems$ = source.pluck('injuryCharactersticsType');

    }

    private providerMaltreatmentType() {


    }

    private getInvolvedPerson() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: this.getRequestParam()
                }),
                'People/getpersondetailcw?filter'
            )
            .subscribe((items) => {
                if (items.data) {
                    this.childDetails = [];
                    items.data.map((list) => {
                        this.involvedPersons = items['data'].filter(item => item.rolename === 'RC');
                        this.abusedChildList = items['data'].filter(person => {
                            const roles = (Array.isArray(person.roles)) ? person.roles : [];
                           // const victim = roles.some(role => ['CHILD', 'AV'].includes(role.intakeservicerequestpersontypekey));
                           // Non-Victim should not under Maltreatment tab
                            const victim = roles.some(role => ['AV'].includes(role.intakeservicerequestpersontypekey));
                            return victim;
                        });
                        if (list.roles) {
                            const getAllRoles = list.roles.filter((role) => role.intakeservicerequestpersontypekey === 'AM');
                            if (getAllRoles.length) {
                                return this.childDetails.push(
                                    new DropdownModel({
                                        value: getAllRoles[0].intakeservicerequestactorid,
                                        text: list.fullname
                                    })
                                );
                            }
                        }
                    });
                    this.isProbabilityPresent = (this.childDetails.length > 0) ? null : 'No Maltreator found.';
                    // this.createFormarray();
                }
            });
    }
    getRequestParam() {
        let inputRequest: Object;
        const caseID = this.id;
        const isservicecase = this._dataStoreService.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        if (isservicecase) {
          inputRequest = {
            objectid: caseID,
            objecttypekey: 'servicecase'
          };
        } else {
          inputRequest = {
            intakeserviceid: caseID
          };
        }
        return inputRequest;
      }

    private getIncidentLocation() {
        this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.MaltreatmentInformation.IncidentLocationDropDown).subscribe((data) => {
            this.incidentLocation = [];
            data.map((values) => {
                this.incidentLocation.push(new DropdownModel({ value: values.incidentlocationtypekey, text: values.typename }));
            });
        });
    }
    sexualAbuse(data, index, InvestigationIndex) {
        const items = data.value.split('~');
        const control = <FormArray>this.maltreatmentFormGroup.controls['person'];
        if (items[1] === '1') {
            control.controls[index]['controls']['investigationallegation']['controls'][InvestigationIndex].controls['isShowSextrafficking'].patchValue(true);
        } else {
            control.controls[index]['controls']['investigationallegation']['controls'][InvestigationIndex].controls['isShowSextrafficking'].patchValue(false);
        }
    }

    onChangeMaltreatment(item, index, InvestigationIndex) {
        console.log(item);
        const control = <FormArray>this.maltreatmentFormGroup.controls['person'];
        const allegationscontrol = <FormArray>this.maltreatmentFormGroup.controls['allegations'];
        if (item === '1') {
            control.controls[index]['controls']['investigationallegation']['controls'][InvestigationIndex].controls['isDisplayProviderInvolved'].patchValue(true);
            // allegationscontrol.controls[index]['controls']['investigationallegation']['controls'][InvestigationIndex].controls['isDisplayProviderInvolved'].patchValue(true);
        } else {
            control.controls[index]['controls']['investigationallegation']['controls'][InvestigationIndex].controls['isDisplayProviderInvolved'].patchValue(false);
            // allegationscontrol.controls[index]['controls']['investigationallegation']['controls'][InvestigationIndex].controls['isDisplayProviderInvolved'].patchValue(false);
        }
    }

    onChangeProviderinvolvedType(value) {
        console.log(value);
        const type = this.providermaltreatment.filter(item => item.providermaltreatmenttypekey === value);
        if (type.length) {
            const index = this.providermaltreatment.indexOf(type[0]);
            this.providermaltreatment.splice(index, 1);
        } else {
            this.providermaltreatment.push({ providermaltreatmenttypekey: value });
        }
    }

    onFatalityChange(event, index) {
        console.log(event);
        if (event.value === 1) {
            const selectedPerson = this.involvedPersons.map(person => person.dateofdeath);
            if (selectedPerson && selectedPerson.length && !selectedPerson[0]) {
                const control = <FormArray>this.maltreatmentFormGroup.controls['person'];
                control.controls[index].patchValue({ ischildfatality: 0 });
                this._alertService.error('Please select person date of death');
            }
        }
    }
    private getInvolvedPersons() {
        this._commonHttpService
            .getArrayList(
                {
                    page: 1,
                    method: 'get',
                    where: { intakeservreqid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            )
            .subscribe(res => {
                if (res['data'] && res['data'].length) {
                    const persons = res['data'];
                    this.involvedPersons = res['data'].filter(item => item.rolename === 'RC');
                    this.abusedChildList = persons.filter(person => {
                        const roles = (Array.isArray(person.roles)) ? person.roles : [];
                        const victim = roles.some(role => ['CHILD', 'AV'].includes(role.intakeservicerequestpersontypekey));
                        return victim;
                    });
                }
            });
    }

    startDateChanged(investigationForm) {
        const empForm = investigationForm.getRawValue();
        this.minDate = new Date(empForm.incidentdate);
    }

    endDateChanged(investigationForm) {
        const empForm = investigationForm.getRawValue();
        this.maxDate = new Date(empForm.enddate);
    }

    getSDM() {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        servicerequestid: this.id
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmvaluesUrl + '?filter'
            )
            .subscribe((res) => {
                if (res) {
                    const sdm = res[0].getintakeservicerequestsdm ? res[0].getintakeservicerequestsdm[0] : null;
                    if (sdm) {
                        if (sdm.ismalpa_suspeciousdeath || sdm.ismalpa_nonaccident || sdm.ismalpa_injuryinconsistent ||
                            sdm.ismalpa_insjury || sdm.ismalpa_childtoxic || sdm.ismalpa_caregiver
                        ) { this.sdmAllegation['phyabuse'] = true; }
                        if (
                            sdm.ismalsa_sexualmolestation || sdm.ismalsa_sexualact || sdm.ismalsa_sexualexploitation || sdm.ismalsa_physicalindicators || sdm.ismalsa_sex_trafficking
                        ) { this.sdmAllegation['sexabuse'] = true;  }
                        if (sdm.ismenab_psycologicalability) {
                            this.sdmAllegation['miabuse'] = true;
                        }
                        if (sdm.ismenng_psycologicalability) {
                            this.sdmAllegation['mineglect'] = true;
                        }
                        if (
                            sdm.isnegmn_unreasonabledelay || sdm.isneguc_leftunsupervised || sdm.isneguc_leftaloneinappropriatecare || sdm.isneguc_leftalonewithoutsupport || sdm.isnegab_abandoned ||
                            sdm.isnegfp_cargiverintervene || sdm.isnegrh_treatmenthealthrisk || sdm.isneggn_inadequatesupervision || sdm.isneggn_inadequateclothing || sdm.isneggn_exposuretounsafe ||
                            sdm.isneggn_childdischarged || sdm.isneggn_inadequatefood || sdm.isneggn_signsordiagnosis || sdm.isneggn_suspiciousdeath
                        ) { this.sdmAllegation['neglect'] = true; }
                    }
                }
            });
    }

    filterAllegations(text) {
        this.foundAllegation = false;
        if (this.sdmAllegation) {
            if (text === 'Mental Injury- Abuse') {
                (this.sdmAllegation['miabuse']) ? this.foundAllegation = true : this.findAllegationsInMaltreatmentInfo(text);
            }

            if (text === 'Mental Injury- Neglect') {
                (this.sdmAllegation['mineglect']) ? this.foundAllegation = true : this.findAllegationsInMaltreatmentInfo(text);
            }

            if (text === 'Neglect') {
                (this.sdmAllegation['neglect']) ? this.foundAllegation = true : this.findAllegationsInMaltreatmentInfo(text);
            }

            if (text === 'Physical Abuse') {
                (this.sdmAllegation['phyabuse']) ? this.foundAllegation = true : this.findAllegationsInMaltreatmentInfo(text);
            }

            if (text === 'Sexual Abuse') {
                (this.sdmAllegation['sexabuse']) ? this.foundAllegation = true : this.findAllegationsInMaltreatmentInfo(text);
            }
        } else {
            this.findAllegationsInMaltreatmentInfo(text);
        }
    }

    findAllegationsInMaltreatmentInfo(text) {
        var allegations = [];
        const chosenchild = (this.selectedChild && this.selectedChild.actorid) ? this.selectedChild.actorid : null;
        if (this.maltreatmentInformation && this.maltreatmentInformation.length) {
            allegations = this.maltreatmentInformation.filter(item => {
                return item.actorid === chosenchild;
            });
        }
        
        if (allegations.length > 0) {
            allegations.forEach(alle => {
                alle.investigationallegation.forEach(element => {
                    if (element.allegationname === text) {
                        this.foundAllegation = true;
                    } else {
                        this.foundAllegation = false;
                    }
                });
            });
        } else {
            return false;
        }
    }

    createFormarray() {
        // this.allegationForm = this._formBuilder.array([]);
        this.maltreatmentTypeDropdownItems.forEach(maltreatment => {
            this.filterAllegations(maltreatment.text);
            if (this.foundAllegation) {
                const control = <FormArray>this.maltreatmentFormGroup.controls['allegations'];
                this.childDetails.forEach(maltreator => {
                    const newForm = this.createFormElement(maltreatment, maltreator);
                    control.push(newForm);
                    const maltreatmentcase = this.maltreatmentInformation.find(info => {
                        const ischildpresent = ((info.intakeservicerequestactorid === this.selectedChild.intakeservicerequestactorid) ||
                                            (info.actorid && info.actorid === this.selectedChild.actorid));
                        const investigationallegation = info.investigationallegation[0];
                        const ismaltreatorpresent = investigationallegation.maltreators.some(item => item.intakeservicerequestactorid === maltreator.value);
                        const isallegation = investigationallegation.allegationid === maltreatment.value;
                        return (ischildpresent && ismaltreatorpresent && isallegation) ? true : false;
                    }
                    );
                    if (maltreatmentcase) {
                        newForm.disable();
                        newForm.patchValue(maltreatmentcase);
                    }
                });
            }
        });

    }

    private createFormElement(maltreat, person): FormGroup {
        // if (x.dateofdeath) {
        //     const childDod = this.datePipe.transform(x.dateofdeath, 'MM/dd/yyyy');
        //     this.personInfo = x.personname + ' ' + childDod;
        // } else {
        //     this.personInfo = x.personname;
        // }
        const controls = this.providerMaltreatmentTypeItems.map(c => {
            return new FormControl(false);
        });
        let incidentdate = null;
        let incidentlocationtypekey = null
        if (this.reportSummary && this.reportSummary.reporterincidentdate) {
            incidentdate = this.reportSummary.reporterincidentdate;
            this.incidentdate = incidentdate;
        }
        if (this.reportSummary && this.reportSummary.reporterincidentlocation) {
            incidentlocationtypekey = this.reportSummary.reporterincidentlocation;
        }
        return this._formBuilder.group({
            personname: person.text,
            maltreatorid: person.value,
            maltreatment: maltreat.text,
            maltreatmentid: maltreat.value,
            childname: this.allegedChild ? this.allegedChild.fullname : '',
            incidentlocationtypekey: [incidentlocationtypekey],
            isjurisdiction: [''],
            countyid: [''],
            caseworker: [''],
            supervisor: [''],
            isnotapplicable: [null],
            notapplicablecomments: [null],
            investigationallegationid: [''],
            incidentdate: [ incidentdate ? incidentdate : null],
            allegationid: maltreat.value + '~' + maltreat.additionalProperty,
            isShowAllegation: [false],
            maltreatoractorid: person.value,
            injurytypekey: [''],
            maltreatmentcharactersticstypekey: [''],
            injurycharactersticstypekey: [''],
            comments: ['', Validators.required],
            injurycomments: ['', Validators.required],
            enddate: [null],
            isapproximatedate: [false],
            timeofincidence: [null],
            sextrafficking: [''],
            injurytypedescription: [''],
            maltreatmentcharactersticstypedescription: [''],
            injurycharactersticstypedescription: [''],
            maltreatorname: person.text,
            allegationname: '',
            isShowSextrafficking: false,
            isproviderinvolved: [0],
            ischildfatality: [0],
            isDisplayProviderInvolved: [''],
            providerMaltreatmentItems: new FormArray(controls),
            expungementStatus: [null],
            providername : [''],
            providerid : [''],
            providerphone : [''],
        });
    }

    selectChild(child) {
        this.selectedChild = null;
        this.allegedChild = child;
        // this.allegationForm = null;
        if (this.allegationForm) {
            const control = <FormArray>this.allegationForm.controls['allegations'];

            while (control.length !== 0) {
                control.removeAt(0);
            }

        }
        setTimeout(() => {
            this.selectedChild = child;
            this.createMaltreatmentForm();
        }, 1000);
        // if (this.isProbabilityPresent === 'No Maltreator found.') {
        //     (<any>$('#maltreatment-popup')).modal('show');
        // } else if (this.isProbabilityPresent === 'No Maltreatment Present.'){
        //     (<any>$('#maltreatment-popup')).modal('show');
        // }
    }

    createMaltreatmentForm() {

        this.allegationForm = this._formBuilder.group({
            allegations: this._formBuilder.array([])
        });

        this.maltreatmentTypeDropdownItems.forEach(maltreatment => {
            this.filterAllegations(maltreatment.text)
            if (this.foundAllegation) {
                const control = <FormArray>this.allegationForm.controls['allegations'];
                this.childDetails.forEach(maltreator => {
                    const newForm = this.createFormElement(maltreatment, maltreator);
                    control.push(newForm);
                    const maltreatmentcase = this.maltreatmentInformation.find(info => {
                        const ischildpresent = ((info.intakeservicerequestactorid === this.selectedChild.intakeservicerequestactorid) ||
                                            (info.actorid && info.actorid === this.selectedChild.actorid));
                        if (info.investigationallegation && Array.isArray(info.investigationallegation) && info.investigationallegation.length > 0) {
                            const investigationallegation = info.investigationallegation[0];
                            const maltreatorObj = Array.isArray(investigationallegation.maltreators) ? investigationallegation.maltreators : [];
                            const ismaltreatorpresent = maltreatorObj.some(item => item.intakeservicerequestactorid === maltreator.value);
                            const isallegation = investigationallegation.allegationid === maltreatment.value;
                            return (ischildpresent && ismaltreatorpresent && isallegation) ? true : false;
                        }
                    }
                    );
                    if (maltreatmentcase) {
                        // newForm.disable();
                        newForm.patchValue(maltreatmentcase);
                        this.patchMaltreatmentForm(maltreatmentcase, newForm);
                    }
                });
            }
        });
        if(!this.childDetails.length) {
            (<any>$('#no-maltreator-popup')).modal('show');
        }

    }

    patchMaltreatmentForm(investigation, form) {
        const investigationallegation = investigation.investigationallegation[0];
        const providermaltreatment = this.providerMaltreatmentTypeItems.map(c => {
            if (investigationallegation.providermaltreatment && investigationallegation.providermaltreatment.filter(p => p.providermaltreatmenttypekey === c.value).length) {
                return true;
            } else {
                return false;
            }
        });
        const isExpunged =   ( investigationallegation && investigationallegation.expungement && investigationallegation.expungement.length  ) ? true : false;
        form.patchValue({
            childname: investigation.personname,
            countyid: investigation.countyid,
            incidentlocationtypekey: investigation.incidentlocationtypekey,
            isjurisdiction: investigation.isjurisdiction ? investigation.isjurisdiction.toString() : '0',
            isnotapplicable: investigation.isnotapplicable ? (investigation.isnotapplicable !== 0 ? true : null) : null,
            notapplicablecomments: investigation.notapplicablecomments ? investigation.notapplicablecomments.toString() : null,
            caseworker: investigation.caseworker,
            supervisor: investigation.supervisor,
            injurycharactersticstypekey: (investigationallegation.injurycharactersticstype) ? investigationallegation.injurycharactersticstype.map(item => item.injurycharactersticstypekey) : [],
            injurycomments: investigationallegation.injurycomments,
            maltreatmentcharactersticstypekey: (investigationallegation.maltreatmentcharactersticstypekey) ?
                investigationallegation.maltreatmentcharactersticstypekey.map(item => item.maltreatmentcharactersticstypekey) : [],
            injurytypekey: (investigationallegation.injurytype) ? investigationallegation.injurytype.map(item => item.injurytypekey) : [],
            timeofincidence: investigationallegation.timeofincidence,
            isapproximatedate: investigationallegation.isapproximatedate,
            enddate: investigationallegation.enddate,
            incidentdate: investigationallegation.incidentdate,
            isproviderinvolved: investigationallegation.isproviderinvolved ? investigationallegation.isproviderinvolved.toString() : (investigationallegation.isproviderinvolved === 0 ? '0' : null),
            ischildfatality: investigationallegation.ischildfatality,
            comments: investigationallegation.comments,
            investigationallegationid: investigationallegation.investigationallegationid,
            maltreatmentid: investigationallegation.maltreatmentid,
            sextrafficking: investigationallegation.sextrafficking ? investigationallegation.sextrafficking.toString() : '0',
            providerMaltreatmentItems: providermaltreatment,
            providername : investigation.providername,
            providerid : investigation.providerid,
            providerphone : investigation.providerphone,
            expungementStatus : isExpunged
        });

        if (investigation.roles && investigation.roles.length) {
            investigation.roles.map((item) => {
                if (item.role === 'CW') {
                    form.patchValue({ caseworker: item.username });
                }
                if (item.role === 'SP') {
                    form.patchValue({ supervisor: item.username });
                }
            });
        }
    }

    setsearchprovider() {
        this.searchprovider.providerid = '';
        this.searchprovider.providernm = '';
        this.searchprovider.providerfname = '';
        this.searchprovider.providerlname = '';
        this.providersList = [];
    }
    searchProvider() {
        // this.providersList = [];
        this._commonHttpService.getArrayList(
            {
              method: 'post',
              nolimit: true,
            //   providercategorycd: this.searchprovider.providerid,
              providerid: this.searchprovider.providerid,
              providername: this.searchprovider.providernm,
              filter: {}
            },
            'providerreferral/providersearch'
        // )
          ).subscribe(providers => {
              console.log('Providers List' , providers );
            //   if (providers && providers.length) {
                this.providersList = providers;
            //   }
            //   this.setsearchprovider();
          });
      }
      selectProvider(provider: any) {
        this.selectedProvider = {providername : provider.provider_nm , providerid : provider.provider_id, providerphone : provider.adr_work_phone_tx};
      }
      onConfirmProvider() {
        if (this.selectedProvider) {
            (<any>$('#providersearch')).modal('hide');
            this.setsearchprovider();
            console.log('in selected provider');
            if (this.selectedallegationForm) {
                this.selectedallegationForm.patchValue(this.selectedProvider);
            }
        } else {
          this._alertService.warn('Please select any provider');
        }
    }

    selectAllegationForm(allegation) {
        this.selectedallegationForm = allegation;
    }

    onMaltreatorChange(form, index, event) {
        if (event.checked) {
            const control: FormArray = form.get('providerMaltreatmentItems');
            if (Array.isArray(control.controls)) {
                const list = [];
                control.controls.forEach((item, i) => {
                    list.push((index === i) ? true : false);
                });
                control.setValue(list);
            }
        }
    }
}
