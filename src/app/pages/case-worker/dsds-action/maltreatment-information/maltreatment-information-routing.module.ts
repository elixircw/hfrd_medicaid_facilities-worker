import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaltreatmentInformationComponent } from './maltreatment-information.component';

const routes: Routes = [{
  path: '',
  component: MaltreatmentInformationComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaltreatmentInformationRoutingModule { }
