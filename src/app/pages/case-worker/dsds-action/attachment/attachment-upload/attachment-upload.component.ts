import { HttpHeaders } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Rx';

import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, GenericService, DataStoreService } from '../../../../../@core/services';
import { AppConfig } from '../../../../../app.config';
import { AttachmentUpload } from '../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { Attachment } from '../_entities/attachment.data.models';
import { AttachmentDetailComponent } from '../attachment-detail/attachment-detail.component';
import { config } from '../../../../../../environments/config';
import { DsdsService } from '../../_services/dsds.service';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'attachment-upload',
    templateUrl: './attachment-upload.component.html',
    styleUrls: ['./attachment-upload.component.scss']
})
export class AttachmentUploadComponent implements OnInit {
    curDate: Date;
    fileToSave = [];
    uploadedFile = [];
    tabActive = false;
    daNumber: string;
    id: string;
    attachmentResponse: AttachmentUpload;
    // attachmentClassificationTypeDropDown$: Observable<DropdownModel[]>;
    attachmentTypeDropdown$: Observable<DropdownModel[]>;
    token: AppUser;
    attachmentClassificationtypelookup = [];
    attachmentClassificationtype = [];
    isAttachType = '';
    isCate = '';
    issubCate= '';
    attachmenttype= 'case';
    personid= '';
    isServiceCase = false;
    isCW: boolean;
    constructor(
        private router: Router,
        private _service: GenericService<Attachment>,
        private _dropDownService: CommonHttpService,
        private route: ActivatedRoute,
        private _uploadService: NgxfUploaderService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _dsdsService: DsdsService,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.token = this._authService.getCurrentUser();
        if (route.snapshot.params) {
            this.attachmenttype = route.snapshot.params['attachmenttype'] || 'case';
            this.personid = route.snapshot.params['personid'] || '';
        }
    }

    ngOnInit() {
        this.isCW = this._authService.isCW();
        this.loadDropdown();
        this.isServiceCase = this._dsdsService.isServiceCase();

        this.curDate = new Date();
        (<any>$('#upload-attachment')).modal('show');
    }

    uploadFile(file: File | FileError): void {
        if (!(file instanceof Array)) {
            return;
        }
        file.map((item, index) => {
            const fileExt = item.name
                .toLowerCase()
                .split('.')
                .pop();
            if (
                fileExt === 'mp3' ||
                fileExt === 'ogg' ||
                fileExt === 'wav' ||
                fileExt === 'acc' ||
                fileExt === 'flac' ||
                fileExt === 'aiff' ||
                fileExt === 'mp4' ||
                fileExt === 'mov' ||
                fileExt === 'avi' ||
                fileExt === '3gp' ||
                fileExt === 'wmv' ||
                fileExt === 'mpeg-4' ||
                fileExt === 'pdf' ||
                fileExt === 'txt' ||
                fileExt === 'docx' ||
                fileExt === 'doc' ||
                fileExt === 'xls' ||
                fileExt === 'xlsx' ||
                fileExt === 'jpeg' ||
                fileExt === 'jpg' ||
                fileExt === 'png' ||
                fileExt === 'ppt' ||
                fileExt === 'pptx' ||
                fileExt === 'gif'
            ) {
                this.uploadedFile.push(item);
                index = this.uploadedFile.length - 1;
                this.uploadAttachment(index);
                const audio_ext = ['mp3', 'ogg' , 'wav', 'acc', 'flac', 'aiff'];
                const video_ext = ['mp4', 'avi' , 'mov', '3gp', 'wmv', 'mpeg-4'];
                if ( audio_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[index].attachmenttypekey = 'Audio'
                } else if ( video_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[index].attachmenttypekey = 'Video';
                } else {
                    this.uploadedFile[index].attachmenttypekey = 'Document';
                }
                this.isAttachType = this.uploadedFile[index].attachmenttypekey;
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error(fileExt + " format can't be uploaded");
                return;
            }
        });
    }

    humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    uploadAttachment(index) {
        this.isAttachType = '';
        this.isCate = '';
        const _self = this;
        const isDataFilled =  setInterval(function() {
          //  if (response.status === 1 && response.data) {
                if (_self.isAttachType !== '' && _self.isCate !== ''  &&  _self.issubCate !== ''  ) {
                    console.log('filed');
                    clearInterval(isDataFilled);
                    _self.processResponseData(index);
                } else {
                    console.log('data not filled');
                }
           // }
          }, 1000);

        // var workEnv = config.workEnvironment;
        // let uploadUrl = '';
        // if(workEnv == 'state') {
        //     uploadUrl =  AppConfig.baseUrl +'/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl +
        // '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber;
        //     console.log("state",uploadUrl);
        // } else {
        //     uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber;
        //     console.log("local",uploadUrl);
        // }

        // this._uploadService
        //     .upload({
        //         url: uploadUrl,
        //         headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
        //         filesKey: ['file'],
        //         files: this.uploadedFile[index],
        //         process: true
        //     })
        //     .subscribe(
        //         (response) => {
        //             if (response.status) {
        //                 this.uploadedFile[index].percentage = response.percent;
        //                 this.uploadedFile[index].judistriction = 'CENTRAL';
        //             }
        //             if (response.status === 1 && response.data) {
        //                 this.attachmentResponse = response.data;
        //                 this.fileToSave.push(response.data);
        //                 this.fileToSave[this.fileToSave.length - 1].documentattachment = {
        //                     attachmenttypekey: '',
        //                     attachmentclassificationtypekey: '',
        //                     judistriction:'CENTRAL',
        //                     administration:'',
        //                     site:'',
        //                     attachmentdate: new Date(),
        //                     sourceauthor: '',
        //                     attachmentsubject: '',
        //                     sourceposition: '',
        //                     attachmentpurpose: '',
        //                     sourcephonenumber: '',
        //                     acquisitionmethod: '',
        //                     sourceaddress: '',
        //                     locationoforiginal: '',
        //                     insertedby: this.token.user.userprofile.displayname,
        //                     note: '',
        //                     updatedby: this.token.user.userprofile.displayname,
        //                     activeflag: 1
        //                 };
        //                 this.fileToSave[this.fileToSave.length - 1].description = '';
        //                 this.fileToSave[this.fileToSave.length - 1].documentdate = new Date();
        //                 this.fileToSave[this.fileToSave.length - 1].title = '';
        //                 this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
        //                 this.fileToSave[this.fileToSave.length - 1].objecttypekey = 'ServiceRequest';
        //                 this.fileToSave[this.fileToSave.length - 1].rootobjecttypekey = 'ServiceRequest';
        //                 this.fileToSave[this.fileToSave.length - 1].activeflag = 1;
        //                 this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
        //                 this.fileToSave[this.fileToSave.length - 1].insertedby = this.token.user.userprofile.displayname;
        //                 this.fileToSave[this.fileToSave.length - 1].updatedby = this.token.user.userprofile.displayname;
        //                 this.JudistrictionUpdate(null,this.fileToSave.length - 1,1);
        //             }
        //         },
        //         (err) => {
        //             console.log(err);
        //             this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        //             this.uploadedFile.splice(index, 1);
        //         }
        //     );
    }

    processResponseData(index) {
        console.log('check');
        const workEnv = config.workEnvironment;
        let uploadUrl = '';
        const dynam  = this.isAttachType + '|' + this.isCate + '|' + this.issubCate ;
        if (workEnv === 'state') {
            uploadUrl =  AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
            + '?access_token=' + this.token.id + '&' + 'srno=' + this.daNumber + '&' + 'docsInfo=' + dynam + '&attachmenttype=' + this.attachmenttype + '&personid=' + this.personid;
            console.log('state', uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
            '&' + 'srno=' + this.daNumber + '&attachmenttype=' + this.attachmenttype + '&personid=' + this.personid;
            console.log('local', uploadUrl);
        }

        this._uploadService
            .upload({
                url: uploadUrl,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile[index],
                process: true,
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        this.attachmentResponse = response.data;
                        this.fileToSave.push(response.data);
                        this.fileToSave[this.fileToSave.length - 1].documentattachment = {
                            attachmenttypekey: this.isAttachType,
                            attachmentclassificationtypekey: this.isCate,
                            attachmentclassificationsubtypekey: this.issubCate,
                            attachmentdate: new Date(),
                            sourceauthor: '',
                            attachmentsubject: '',
                            sourceposition: '',
                            attachmentpurpose: '',
                            sourcephonenumber: '',
                            acquisitionmethod: '',
                            sourceaddress: '',
                            locationoforiginal: '',
                            insertedby: this.token.user.userprofile.displayname,
                            note: '',
                            updatedby: this.token.user.userprofile.displayname,
                            activeflag: 1
                        };
                        const objecttypekey = this.isServiceCase ? 'Servicecase' : 'ServiceRequest';
                        this.fileToSave[this.fileToSave.length - 1].description = '';
                        this.fileToSave[this.fileToSave.length - 1].documentdate = new Date();
                        this.fileToSave[this.fileToSave.length - 1].title = '';
                        this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
                        this.fileToSave[this.fileToSave.length - 1].objecttypekey = objecttypekey;
                        this.fileToSave[this.fileToSave.length - 1].rootobjecttypekey = objecttypekey;
                        this.fileToSave[this.fileToSave.length - 1].activeflag = 1;
                        this.fileToSave[this.fileToSave.length - 1].daNumber = this.daNumber;
                        this.fileToSave[this.fileToSave.length - 1].insertedby = this.token.user.userprofile.displayname;
                        this.fileToSave[this.fileToSave.length - 1].updatedby = this.token.user.userprofile.displayname;
                    }

                }, (err) => {
                        console.log(err);
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                        this.uploadedFile.splice(index, 1);
                    }
                );
    }
    deleteUpload(index) {
        this.uploadedFile.splice(index, 1);
        this.fileToSave.splice(index, 1);
    }
    clearAllUpload() {
        (<any>$('#upload-attachment')).modal('hide');
        this.uploadedFile = [];
        this.fileToSave = [];
        let currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment';
        if (this.attachmenttype === 'person') {
            currentUrl = currentUrl + '/person';
        }
        this.router.navigateByUrl(currentUrl).then(() => {
            this.router.navigated = false;
            this.router.navigate([currentUrl]);
        });
    }
    titleUpdate(event, index) {
        this.uploadedFile[index].title = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidTitle = false;
        } else {
            this.uploadedFile[index].invalidTitle = true;
        }
    }
    descUpdate(event, index) {
        this.uploadedFile[index].description = event.target.value;
    }
    docDateUpdate(event, index) {
        this.uploadedFile[index].docDate = event.target.value;
    }
    typeUpdate(event, index) {
        if (event.target.value !== '')  {
            this.isAttachType  = event.target.value;
        }
        this.uploadedFile[index].attachmenttypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentType = false;
        } else {
            this.uploadedFile[index].invalidAttachmentType = true;
        }
    }
    categoryUpdate(event, index) {
        if (event.target.value !== '')  {
            this.isCate  = event.target.value;
        }
        this.issubCate  = '';
        this.uploadedFile[index].attachmentclassificationsubtypekey = '';
        this.uploadedFile[index].attachmentClassificationsubtype = [];
        this.uploadedFile[index].attachmentclassificationtypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentClassify = false;
            for (let i = 0; i < this.attachmentClassificationtypelookup.length; i++) {
                if (this.attachmentClassificationtypelookup[i].typedescription === this.uploadedFile[index].attachmentclassificationtypekey) {
                    this.uploadedFile[index].attachmentClassificationsubtype.push({subcategory: this.attachmentClassificationtypelookup[i].subcategory});
                }
            }
        } else {
            this.uploadedFile[index].invalidAttachmentClassify = true;
        }
    }
    subcategoryUpdate(event, index) {
        if (event.target.value !== '')  {
            this.issubCate  = event.target.value;
        }
        this.uploadedFile[index].attachmentclassificationsubtypekey = event.target.value;
        if (event.target.value) {
            this.uploadedFile[index].invalidAttachmentsubClassify = false;
        } else {
            this.uploadedFile[index].invalidAttachmentsubClassify = true;
        }
    }
    saveAttachmentDetails() {
        if (this.uploadedFile.length !== this.fileToSave.length) {
            this._alertService.error('Please wait till files get uploaded');
        } else {
            this.uploadedFile.map((item, index) => {
                // Getting the correct index by matching the filename ,as values in uploadedFile and fileToSave are not matching sequencially
                const xindex = this.fileToSave.findIndex( data => data.originalfilename === this.uploadedFile[index].name);
                this.fileToSave[xindex].servicerequestid = this.isServiceCase ? null : this.id;
                this.fileToSave[xindex].servicecaseid = this.isServiceCase ? this.id : null;
                this.fileToSave[xindex].title = item.title;
                this.fileToSave[xindex].attachmenttype = this.attachmenttype;
                this.fileToSave[xindex].personid = this.personid;
                this.fileToSave[xindex].description = item.description;
                this.fileToSave[xindex].documentattachment.attachmenttypekey = item.attachmenttypekey;
                this.fileToSave[xindex].documentattachment.attachmentclassificationtypekey = item.attachmentclassificationtypekey;
                this.fileToSave[xindex].documentattachment.attachmentclassificationsubtypekey = item.attachmentclassificationsubtypekey;
            });
            const AttachValidate = this.fileToSave.filter((wer) => !wer.documentattachment.attachmentclassificationtypekey || !wer.documentattachment.attachmenttypekey || !wer.title  || !wer.documentattachment.attachmentclassificationsubtypekey /*|| !wer.documentattachment.administration  || !wer.documentattachment.site */ );
            if (AttachValidate.length === 0) {
                this._service.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.SaveAttachmentUrl;
                this._service.createArrayList(this.fileToSave).subscribe(
                    (response) => {
                        response.map((item, index) => {
                            if (item.documentpropertiesid) {
                                response.splice(index, 1);
                                this.fileToSave.splice(index, 1);
                                this.uploadedFile.splice(index, 1);
                            }
                            const docProp = response.filter((docId) => docId.Documentattachment);
                            if (docProp.length === 0) {
                                this._alertService.success('Attachment(s) added successfully!');
                                (<any>$('#upload-attachment')).modal('hide');
                                this.fileToSave = [];
                                this.uploadedFile = [];
                                this.router.routeReuseStrategy.shouldReuseRoute = function() {
                                    return false;
                                };
                                let currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment';
                                if (this.attachmenttype === 'person') {
                                    currentUrl = currentUrl + '/person';
                                }
                                // this.router.navigateByUrl(currentUrl).then(() => {
                                    // this.router.navigated = false;
                                    this.router.navigate([currentUrl]);
                                // });
                            }
                            if (item.Documentattachment) {
                                const attPos = index + 1;
                                this._alertService.error(item.Documentattachment + ' for Attachment ' + attPos);
                            } else if (!item.documentpropertiesid) {
                                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                            }
                        });
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error('Please fill all mandatory fields');
                this.uploadedFile.map((item) => {
                    if (!item.title) {
                        item.invalidTitle = true;
                    } else {
                        item.invalidTitle = false;
                    }
                    if (!item.attachmentclassificationtypekey) {
                        item.invalidAttachmentClassify = true;
                    } else {
                        item.invalidAttachmentClassify = false;
                    }
                    if (!item.attachmenttypekey) {
                        item.invalidAttachmentType = true;
                    } else {
                        item.invalidAttachmentType = false;
                    }
                    if (!item.attachmentclassificationsubtypekey) {
                        item.invalidAttachmentsubClassify = true;
                    } else {
                        item.invalidAttachmentsubClassify = false;
                    }
                });
            }
        }
    }
    private loadDropdown() {
        this._dropDownService
        .getSingle(
            {},
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
        )
        .subscribe(data => {
            const dp_att_arr = [];
            if (data && data.length > 0) {
                this.attachmentClassificationtypelookup = data;
                for (let i = 0; i < this.attachmentClassificationtypelookup.length; i++) {
                    if (this.attachmentClassificationtypelookup[i].typedescription && dp_att_arr.indexOf(this.attachmentClassificationtypelookup[i].typedescription) < 0 ) {
                        if (this.isCW) {
                            if (this.attachmentClassificationtypelookup[i].typedescription.startsWith('CW-')) {
                                this.attachmentClassificationtype.push({typedescription: this.attachmentClassificationtypelookup[i].typedescription});
                                dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                            }
                        } else {
                            this.attachmentClassificationtype.push({typedescription:this.attachmentClassificationtypelookup[i].typedescription});
                            dp_att_arr.push(this.attachmentClassificationtypelookup[i].typedescription);
                        }
                    }
                }

            }
        });
        const source = forkJoin(
            this._dropDownService.getArrayList(
                {
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentTypeUrl + '?filter={"nolimit": true}'
            ),
            // this._dropDownService.getArrayList(
            //     {
            //         nolimit: true
            //     },
            //     CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentClassificationTypeUrl + '?filter={"nolimit": true}'
            // )
        )
            .map((result) => {
                return {
                    attachmentType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.attachmenttypekey
                            })
                    ),
                    // attachmentClassificationType: result[1].map(
                    //     (res) =>
                    //         new DropdownModel({
                    //             text: res.typedescription,
                    //             value: res.attachmentclassificationtypekey
                    //         })
                    // )
                };
            })
            .share();
        this.attachmentTypeDropdown$ = source.pluck('attachmentType');
        // this.attachmentClassificationTypeDropDown$ = source.pluck('attachmentClassificationType');
    }
}
