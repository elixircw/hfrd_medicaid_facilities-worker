import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlacementMenuComponent } from './placement-menu.component';

const routes: Routes = [{
  path: '',
  component: PlacementMenuComponent,
  children: [
        { path: 'placement', loadChildren: './../placement/placement.module#PlacementModule' },
        { path: 'foster-care', loadChildren: './../foster-care/foster-care.module#FosterCareModule' },
  ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlacementMenuRoutingModule { }
