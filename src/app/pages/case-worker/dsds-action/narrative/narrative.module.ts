import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaskModule } from 'ngx-mask';
import { PopoverModule } from 'ngx-popover';
import { NarrativeRoutingModule } from './narrative-routing.module';
import { NarrativeComponent } from './narrative.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { QuillModule } from 'ngx-quill';
import { FormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material';

@NgModule({
  imports: [
    SharedDirectivesModule,
    CommonModule, 
    NarrativeRoutingModule,
    FormsModule,
    FormMaterialModule,
    PopoverModule,
    NgxMaskModule.forRoot(),
    ControlMessagesModule,
    SharedDirectivesModule,
    QuillModule,
    MatAutocompleteModule,
 
  ],
  exports: [NarrativeComponent],
  declarations: [NarrativeComponent]
})
export class NarrativeModule { }
