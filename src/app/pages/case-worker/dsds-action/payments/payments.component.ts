import { Component, OnInit } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { ActivatedRoute } from '@angular/router';
import { DataStoreService } from '../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  id: string;
  paymentList: any[];

  constructor(
    private _commonService: CommonHttpService,
    private route: ActivatedRoute, private _dataStoreService: DataStoreService) {   this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID); }

  ngOnInit() {
    this.getPaytmentList();
  }
  getPaytmentList() {
    this._commonService.getSingle(
      {
          'count': -1,
          'page': 1,
          'limit': 50,
          'where': { 'objectid': this.id, 'casenumber': null },
          'method': 'get'
      },
      'servicecase/servicecasepaymentlist?filter'
  )
      .subscribe(result => {
          if (result && result.length) {
              this.paymentList = result;
          }
      });
  }

}
