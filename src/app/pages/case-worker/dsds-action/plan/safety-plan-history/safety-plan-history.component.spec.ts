import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyPlanHistoryComponent } from './safety-plan-history.component';

describe('SafetyPlanHistoryComponent', () => {
  let component: SafetyPlanHistoryComponent;
  let fixture: ComponentFixture<SafetyPlanHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyPlanHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyPlanHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
