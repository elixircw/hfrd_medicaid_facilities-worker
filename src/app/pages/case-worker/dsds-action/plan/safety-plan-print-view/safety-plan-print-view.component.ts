import { Component, Input, OnInit } from '@angular/core';
import * as jsPDF from 'jspdf';
import { Subject } from 'rxjs/Subject';
import { ActivatedRoute } from '@angular/router';
import { SafetyPlanView, SaftyPlan } from '../../../_entities/caseworker.data.model';
import { DataStoreService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

// tslint:disable-next-line:import-blacklist

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'safety-plan-print-view',
    templateUrl: './safety-plan-print-view.component.html',
    styleUrls: ['./safety-plan-print-view.component.scss']
})
export class SafetyPlanPrintViewComponent implements OnInit {
    @Input()
    saftyPlan$: Subject<SaftyPlan>;
    @Input()
    safetyPlanView: SafetyPlanView;
    daNumber: string;
    constructor(private route: ActivatedRoute, private _dataStoreService: DataStoreService) {

    }

    ngOnInit() {
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.saftyPlan$.subscribe(sf => {
            console.log(JSON.stringify(sf));
        });
    }
    generatePDF() {
        const self = this;
        const doc = new jsPDF('p', 'mm', 'a4');
        html2canvas(document.getElementById('printView')).then(function(
            canvas
        ) {
            const imgData = canvas.toDataURL('image/png');
            const pageHeight = 300;
            const imgWidth = 205;
            const imgHeight = (canvas.height * imgWidth) / canvas.width;
            let heightLeft = imgHeight;
            let position = 0;

            doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
            heightLeft -= pageHeight;

            while (heightLeft >= 0) {
                position = heightLeft - imgHeight;
                doc.addPage();
                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;
            }

            doc.save(self.safetyPlanView.daNumber + '.pdf');
        });
    }
}
