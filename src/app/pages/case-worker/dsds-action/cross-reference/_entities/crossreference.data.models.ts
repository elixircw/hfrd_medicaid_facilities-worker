export class CrossReference {
    fromintakeservicerequestid: string;
    withintakeservicerequestid: string;
    intakeservicerequestcrossreferencereasontypekey: string;
    servicerequestsubtypedescription: string;
    intakeservicerequestcrossrefernceid: string;
    intakeservicerequest: IntakeServiceRequest;
    intakeservicerequestcrossreferencereasontype: ReasonType;
}
export class IntakeServiceRequest {
    intakeserviceid: string;
    servicerequestnumber: string;
    intakeservreqtypeid: string;
    intakeserreqstatustypeid: string;
    intakeservicerequestclassid: string;
    servicerequestsubtype: SubType;
    intakeservicerequesttype: RequestType;
    areateammemberservicerequest: AreaTeamMember[];
    intakeserreqstatustype: StatusType;
}
export class SubType {
    servicerequestsubtypeid: string;
    classkey: string;
}

export class RequestType {
    intakeservreqtypeid: string;
    description: string;
 }

 export class AreaTeamMember {

 }
 export class StatusType {
    intakeserreqstatustypeid: string;
    description: string;
 }
export class ReasonType {
    intakeservicerequestcrossreferencereasontypekey: string;
    typedescription: string;
}
export class CrossReferenceModal {
    totalcount: string;
    intakeserviceid: string;
    servicerequestnumber: string;
    srtype: string;
    srsubtype: string;
    raname: string;
    zipcode: string;
    county: string;
    region: string;
    datereceived: Date;
    timereceived: Date;
    datedue: Date;
    timedue: Date;
    overdue: boolean;
    count: number;
    providername: string;
    ssbg: string;
    contractstatus: string;
    provideragrementid: string;
    routingstatustypekey: string;
    areateammemberservicerequestid: string;
    groupnumber: string;
    agencyid: string;
    assignedto: string;
    teamname: string;
    disposition: string;
    srstatus: string;
    insertedby: string;
    teamtypekey: string;
    assignedtosid: string;
    revisited: boolean;
    investigationid: string;
    fromintakeservicerequestid: string;
    withintakeservicerequestid: string;
    intakeservicerequestcrossreferencereasontypekey: string;
    updatedby: string;
}
export class CrossReferenceSearchRequestModal {
    persontype: string;
    reporteddate: string;
    reportedenddate: string;
    servicerequestnumber: string;
    activeflag: string;
    activeflag1: string;
    status: string[];
    intakeserviceid: string;
}
