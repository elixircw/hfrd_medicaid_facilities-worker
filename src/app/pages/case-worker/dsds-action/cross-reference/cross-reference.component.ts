import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CustomValidators } from 'ng4-validators';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService } from '../../../../@core/services/alert.service';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { CrossReference, CrossReferenceModal, CrossReferenceSearchRequestModal } from './_entities/crossreference.data.models';
import { DataStoreService } from '../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cross-reference',
    templateUrl: './cross-reference.component.html',
    styleUrls: ['./cross-reference.component.scss']
})
export class CrossReferenceComponent implements OnInit {
    id: string;
    crossRefernceAddFormGroup: FormGroup;
    crossRefernceSearchFormGroup: FormGroup;
    paginationInfo: PaginationInfo = new PaginationInfo();
    crossReference$: Observable<CrossReference[]>;
    crossReferenceSearchList$: Observable<CrossReferenceModal[]>;
    reasonCrossReferenceDropdownItems$: Observable<DropdownModel[]>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    crossReference = new CrossReference();
    crossReferenceSearchList = new CrossReferenceModal();
    private pageSubject$ = new Subject<number>();
    private crossReferenceSearchRequestModal: CrossReferenceSearchRequestModal;
    private searchUrl: string;
    private methodType: string;
    agency = '';
    constructor(
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _crossRefService: GenericService<CrossReference>,
        private _crossRefSearchService: GenericService<CrossReferenceModal>,
        private _crossRefSearchRequestService: GenericService<CrossReferenceSearchRequestModal>,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private route: ActivatedRoute,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.agency = this._authService.getAgencyName();
    }

    ngOnInit() {
        this.getPage();
        this.loadReasonTypes();
        this.crossRefernceSearchFormGroup = this.formBuilder.group(
            {
                servicerequestNo: [''],
                reporteddate: ['', CustomValidators.maxDate(new Date())],
                reportedenddate: ['', CustomValidators.maxDate(new Date())],
                statusdsdsaction: ['']
            },
            { validator: this.checkDateRange }
        );
        this.crossRefernceAddFormGroup = this.formBuilder.group({
            reasontocrossref: ['']
        });
    }
    getCrossReferenceSearch(persontype: string) {
        this.crossReferenceSearchList$ = Observable.empty();
        this.crossReferenceSearchRequestModal = Object.assign(new CrossReferenceSearchRequestModal());
        this.crossReferenceSearchRequestModal.persontype = persontype;
        this.search(1, 'get', CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.SameRaReporterApUrl + '/' + this.id + '?filter');
    }
    getCrossReferenceProviderSearch() {
        this.crossReferenceSearchList$ = Observable.empty();
        this.crossReferenceSearchRequestModal = Object.assign(new CrossReferenceSearchRequestModal());
        this.crossReferenceSearchRequestModal.intakeserviceid = this.id;
        this.search(1, 'get', CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.CrossRefProviderUrl + '?filter');
    }
    getCrossReferenceDaSearch() {
        this.crossReferenceSearchList$ = Observable.empty();
        this.crossReferenceSearchRequestModal = Object.assign(new CrossReferenceSearchRequestModal());
        if (this.crossRefernceSearchFormGroup.get('statusdsdsaction').value) {
            this.crossReferenceSearchRequestModal.status = ['Open'];
        }
        this.crossReferenceSearchRequestModal.servicerequestnumber = this.crossRefernceSearchFormGroup.value.servicerequestNo;
        this.crossReferenceSearchRequestModal.reporteddate = this.crossRefernceSearchFormGroup.value.reporteddate;
        this.crossReferenceSearchRequestModal.reportedenddate = this.crossRefernceSearchFormGroup.value.reportedenddate;
        this.crossReferenceSearchRequestModal.activeflag = '1';
        this.crossReferenceSearchRequestModal.activeflag1 = '1';
        this.search(1, 'post', CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.CrossRefDaUrl);
    }
    viewCrossReference(crossRefview: CrossReferenceModal) {
        this.crossReferenceSearchList = crossRefview;
    }
    addCrossReference(crossRefSearch: CrossReferenceModal) {
        const user = this._authService.getCurrentUser();
        this.crossReference$.subscribe((selectCrossReference) => {
            const reference = selectCrossReference.filter(item => item.intakeservicerequest.servicerequestnumber === crossRefSearch.servicerequestnumber);
            if (!reference.length) {
                if (this.crossRefernceAddFormGroup.valid && this.crossRefernceAddFormGroup.dirty) {
                    this._crossRefSearchService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.CrossRefAddUrl;
                    this.crossReferenceSearchList.fromintakeservicerequestid = crossRefSearch.intakeserviceid;
                    this.crossReferenceSearchList.withintakeservicerequestid = this.id;
                    this.crossReferenceSearchList.intakeservicerequestcrossreferencereasontypekey = this.crossRefernceAddFormGroup.value.reasontocrossref;
                    this.crossReferenceSearchList.updatedby = user.userId ? user.userId : '0';
                    this.crossReferenceSearchList.insertedby = user.userId ? user.userId : '0';
                    this._crossRefSearchService.create(this.crossReferenceSearchList).subscribe(
                        (response) => {
                            this._alertService.success('Cross Reference added successfully');
                            this.getPage();
                        },
                        (error) => {
                            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                        }
                    );
                } else {
                    ControlUtils.validateAllFormFields(this.crossRefernceAddFormGroup);
                    ControlUtils.setFocusOnInvalidFields();
                    this._alertService.error('Please select Reason to Cross Reference');
                }

            } else {
                this._alertService.error('Cross Reference alredy exists');
            }
        });
    }
    deleteCrossReference() {
        this._crossRefSearchService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.CrossRefAddUrl;
        this._crossRefSearchService.remove(this.crossReference.intakeservicerequestcrossrefernceid).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Cross Reference deleted successfully');
                    this.getPage();
                    (<any>$('#delete-popup')).modal('hide');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    declineDelete() {
        (<any>$('#delete-popup')).modal('hide');
    }
    confirmDelete(crossRef: CrossReference) {
        this.crossReference = crossRef;
        (<any>$('#delete-popup')).modal('show');
    }
    clearCrossRefernceSearchForm() {
        this.crossRefernceSearchFormGroup.reset();
    }
    checkDateRange(crossRefernceSearch) {
        if (crossRefernceSearch.controls.reportedenddate.value) {
            if (crossRefernceSearch.controls.reportedenddate.value < crossRefernceSearch.controls.reporteddate.value) {
                return { notValid: true };
            }
            return null;
        }
    }
    loadReasonTypes() {
        this.reasonCrossReferenceDropdownItems$ = this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.CrossReferenceReasonTypesUrl).map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.typedescription,
                        value: res.intakeservicerequestcrossreferencereasontypekey
                    })
            );
        });
    }
    private getPage() {
        const source = this._crossRefService
            .getPagedArrayList(
                {
                    method: 'get',
                    where: {
                        intakerequestid: this.id
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.CrossReference.CrossReferenceUrl + '?filter'
            )
            .map((result) => {
                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.crossReference$ = source.pluck('data');
        if (this.paginationInfo.pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }
    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageSubject$.next(this.paginationInfo.pageNumber);
        this.getSearchPage(this.paginationInfo.pageNumber);
    }
    private search(pageNumber: number, methodtype, url: string) {
        this.searchUrl = url;
        this.methodType = methodtype;
        this.getSearchPage(pageNumber);
    }
    private getSearchPage(pageNumber: number) {
        this.crossRefernceAddFormGroup.patchValue({ reasontocrossref: '' });
        ObjectUtils.removeEmptyProperties(this.crossReferenceSearchRequestModal);
        const source = this._crossRefSearchRequestService
            .getPagedArrayList(
                {
                    limit: this.paginationInfo.pageSize,
                    order: '',
                    page: this.paginationInfo.pageNumber,
                    count: this.paginationInfo.total,
                    where: this.crossReferenceSearchRequestModal,
                    method: this.methodType
                },
                this.searchUrl
            )
            .map((result) => {
                return {
                    data: result.data.map((res) => {
                        res.servicerequestnumber = res.servicerequestnumber.replace('(New)', '');
                        return res;
                    }),
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.crossReferenceSearchList$ = source.pluck('data');
        if (pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }
}
