//import { ReportSummary } from './../../../providers/_entities/provider.data.model';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrossReferenceComponent } from './cross-reference.component';
import { CrossReferenceRoutingModule } from './cross-reference-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    CrossReferenceRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    QuillModule,
    PaginationModule
  ],
  declarations: [
    CrossReferenceComponent
  ],
  providers: []
})
export class CrossReferenceModule { }
