import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Rx';

import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, GenericService, DataStoreService } from '../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { InvolvedEntity, InvolvedEntitySearchResponse, InvolvedEntitySearch, AgencyRole, IntakeServiceAgencyRole } from '../../../newintake/my-newintake/_entities/newintakeModel';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'involved-entity',
    templateUrl: './involved-entity.component.html',
    styleUrls: ['./involved-entity.component.scss']
})
export class InvolvedEntityComponent implements OnInit {
    daNumber: string;
    userId: string;
    id: string;
    expiredDatehide: Boolean = true;
    canDisplayPager$: Observable<boolean>;
    entityagencyid: string;
    involvedEntity: FormGroup;
    deleteChoice: FormGroup;
    paginationInfo: PaginationInfo = new PaginationInfo();
    entityDetail: InvolvedEntity;
    entityToDelete: InvolvedEntity;
    rolesList: AgencyRole[] = [];
    selectedRolesList: IntakeServiceAgencyRole[] = [];
    tempRoleList: AgencyRole[] = [];
    stateDropdownItems$: Observable<DropdownModel[]>;
    countyDropdownItems$: Observable<DropdownModel[]>;
    regionDropdownItems$: Observable<DropdownModel[]>;
    agencyCategoryDropdownItems$: Observable<DropdownModel[]>;
    totalRecords$: Observable<number>;
    involvedEntitySearchResponses$: Observable<InvolvedEntitySearchResponse[]>;
    involvedEntities$: Observable<InvolvedEntity[]>;
    expiredDateControl: AbstractControl;
    private pageSubject$ = new Subject<number>();
    private involvedEntitySearch: InvolvedEntitySearch;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _involvedEntitySeachService: GenericService<InvolvedEntitySearchResponse>,
        private _commonHttpService: CommonHttpService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.deleteChoice = this.formBuilder.group({
            deleteOption: [''],
            expiredDate: ['']
        });
    }

    ngOnInit() {
        this.involvedEntityDropdown();
        this.involvedEntity = this.formBuilder.group({
            agencyname: [''],
            agencycategorykey: [''],
            facid: [''],
            ssbg: [''],
            // lastName: [''],
            // firstName: [''],
            // contactNum: [''],
            address1: [''],
            address2: [''],
            zipcode: [''],
            city: [''],
            region: [''],
            state: [''],
            county: [''],
            phonenumber: [''],
            isSatelliteOffice: ['']
        });
        this.listEntity();
        this.deleteChoice.patchValue({ deleteOption: 'expire' });
        this.pageSubject$.subscribe((pageNumber) => {
            this.paginationInfo.pageNumber = pageNumber;
            this.getPage(this.paginationInfo.pageNumber);
        });
        this.expiredDateControl = this.deleteChoice.get('expiredDate');
    }

    selectInvolvedEntity(model: InvolvedEntitySearchResponse) {
        this.involvedEntities$.subscribe((agencyItem) => {
            const agency = agencyItem.filter((item) => item.agencyid === model.agencyid);
            if (!agency.length) {
                this._authService.currentUser.subscribe((userInfo) => {
                    this.userId = userInfo.userId ? userInfo.userId : '0';
                });
                this._commonHttpService
                    .create(
                        {
                            intakeserviceid: this.id,
                            agencytype: model.agencytypedesc,
                            agencyid: model.agencyid,
                            description: '',
                            insertedby: this.userId,
                            updatedby: this.userId
                        },
                        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedEntity.AddEntityRoleUrl
                    )
                    .subscribe(
                        (response) => {
                            this.listEntity();
                            this._alertService.success('Entity added successfully.');
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
            } else {
                this._alertService.error('Entity already exists');
            }
        });
    }

    listRoles(entityId: string) {
        this.entityagencyid = entityId;
        const roleTypeList = this._commonHttpService
            .getSingle(
                {
                    where: { intakeservicerequestagencyid: entityId },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedEntity.EntityRoleGridUrl + '/' + this.id + '?data'
            )
            .subscribe(
                (response) => {
                    this.rolesList = response.roleTypes;
                    this.selectedRolesList = response.selectedRoleTypes.intakeserviceagencyroletype;
                    this.rolesList = this.rolesList.map((role1) => {
                        this.selectedRolesList.map((item) => {
                            if (role1.agencyroletypekey === item.agencyroletypekey) {
                                role1.isSelected = true;
                                role1.intakeserviceagencyroletypeid = item.intakeserviceagencyroletypeid;
                            }
                        });
                        return role1;
                    });
                    (<any>$('#entityRole')).modal('show');
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }

    addUpdateRoles() {
        this._authService.currentUser.subscribe((userInfo) => {
            this.userId = userInfo.userId ? userInfo.userId : '0';
        });
        this.tempRoleList = this.rolesList.filter((item) => item.isSelected).map((item) =>
            Object.assign({
                intakeserviceagencyroletypeid: item.intakeserviceagencyroletypeid,
                agencyroletypekey: item.agencyroletypekey,
                activeflag: item.activeflag,
                intakeservicerequestagencyid: this.entityagencyid,
                effectivedate: new Date(),
                insertedby: this.userId,
                updatedby: this.userId
            })
        );

        this._commonHttpService.update(this.entityagencyid, { intakeserviceagencyroletype: this.tempRoleList }, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedEntity.UpdateEntityRoleUrl).subscribe(
            (response) => {
                (<any>$('#entityRole')).modal('hide');
                this.listEntity();
            },
            (error) => {
                console.log(error);
            }
        );
        this.tempRoleList = [];
    }

    checkedEntityRole(model: AgencyRole, control) {
        const index = this.rolesList.indexOf(model);
        this.rolesList[index].isSelected = control.target.checked;
    }

    clearDetails() {
        this.involvedEntity.reset();
        this.involvedEntity.patchValue({ agencycategorykey: '', region: '', state: '', county: '' });
        this.involvedEntitySearchResponses$ = Observable.empty();
        this.totalRecords$ = Observable.empty();
    }

    showEntityDetails(data: InvolvedEntity) {
        this.entityDetail = data;
    }

    removeEntity(model: InvolvedEntity) {
        this.entityToDelete = model;
        this.expiredDatehide = true;
        this.deleteChoice.patchValue({ deleteOption: 'expire', expiredDate: '' });
        this.expiredDateControl.enable();
        (<any>$('#removeRole')).modal('show');
    }

    disableDate(deleteType: string) {
        if (deleteType === 'delete') {
            this.expiredDatehide = false;
            this.expiredDateControl.disable();
            this.deleteChoice.patchValue({ expiredDate: '' });
        } else {
            this.expiredDateControl.enable();
            this.expiredDatehide = true;
        }
    }

    deleteEntity(entityId: string) {
        if (this.deleteChoice.value.deleteOption === 'delete') {
            this._commonHttpService.remove(entityId, {}, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedEntity.DeleteEntityUrl).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Entity deleted succesfully');
                        this.listEntity();
                        (<any>$('#removeRole')).modal('hide');
                        this.entityToDelete = new InvolvedEntity();
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else if (this.deleteChoice.value.deleteOption === 'expire') {
            if (this.deleteChoice.value.expiredDate) {
                this._commonHttpService.patch(entityId, { expirationdate: this.deleteChoice.value.expiredDate }, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedEntity.DeleteEntityUrl).subscribe(
                    (response) => {
                        if (response) {
                            this._alertService.success('Entity expire date saved succesfully');
                            this.listEntity();
                            (<any>$('#removeRole')).modal('hide');
                            this.entityToDelete = new InvolvedEntity();
                        }
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                this._alertService.error('Select Expire date');
            }
        }
    }

    entitySearch(model: InvolvedEntitySearch) {
        this.involvedEntitySearch = model;
        this.getPage(1);
    }

    pageChanged(event: any) {
        this.paginationInfo.pageNumber = event.page;
        this.paginationInfo.pageSize = event.itemsPerPage;
        this.pageSubject$.next(this.paginationInfo.pageNumber);
    }

    private listEntity() {
        this.involvedEntities$ = this._commonHttpService
            .getArrayList(
                {
                    where: { intakeserviceid: this.id, order: 'agencyname' },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedEntity.EntityGridUrl + '?filter'
            )
            .share();
    }

    private getPage(pageNumber: number) {
        ObjectUtils.removeEmptyProperties(this.involvedEntitySearch);
        const source = this._involvedEntitySeachService
            .getAllFilter(
                {
                    limit: this.paginationInfo.pageSize,
                    order: '',
                    page: this.paginationInfo.pageNumber,
                    count: this.paginationInfo.total,
                    where: this.involvedEntitySearch
                },
                NewUrlConfig.EndPoint.Intake.InvolvedEnititesSearchUrl
            )
            .map((result) => {
                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.involvedEntitySearchResponses$ = source.pluck('data');
        if (pageNumber === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }

    private involvedEntityDropdown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.CountryListUrl),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.StateListUrl),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: '1' },
                    method: 'get'
                },
                NewUrlConfig.EndPoint.Intake.RegionListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList({}, NewUrlConfig.EndPoint.Intake.AgencyCategoryUrl)
        ])
            .map((result) => {
                return {
                    counties: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyname
                            })
                    ),
                    stateList: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    regions: result[2]['data'].map(
                        (res) =>
                            new DropdownModel({
                                text: res.regionname,
                                value: res.regionid
                            })
                    ),
                    categories: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.agencycategorykey
                            })
                    )
                };
            })
            .share();
        this.countyDropdownItems$ = source.pluck('counties');
        this.stateDropdownItems$ = source.pluck('stateList');
        this.regionDropdownItems$ = source.pluck('regions');
        this.agencyCategoryDropdownItems$ = source.pluck('categories');
    }
}
