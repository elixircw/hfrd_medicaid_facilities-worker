import { Sdm } from './../../../providers/_entities/provider.data.model';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SdmComponent } from './sdm.component';
import { SdmRoutingModule } from './sdm-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
@NgModule({
  imports: [
    CommonModule,
    SdmRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
  ],
  declarations: [
    SdmComponent
  ],
  providers: []
})
export class SdmModule { }
