import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SdmComponent } from './sdm.component';
const routes: Routes = [
    {
    path: '',
    component: SdmComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SdmRoutingModule { }
