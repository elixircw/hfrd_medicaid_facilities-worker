import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { AlertService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { MaltreatorsName, ProviderName, Sdm, VictimName } from '../../../case-worker/_entities/caseworker.data.model';
import { MyNewintakeConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import { SdmData } from '../../../newintake/my-newintake/_entities/newintakeModel';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import * as moment from 'moment';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'sdm',
    templateUrl: './sdm.component.html',
    styleUrls: ['./sdm.component.scss']
})
export class SdmComponent implements OnInit {
    id: string;
    sdmFormGroup: FormGroup;
    isDisplayProvider = false;
    isChildInderOneYear: string;
    isScreenOutIN = 'No';
    scnRecommendOveride = '';
    isImmediate: string;
    sdm = new Sdm();
    getPageSdm: any;
    populateSdm: Sdm;
    allegedVictim: VictimName[] = [];
    allegedMaltreator: MaltreatorsName[] = [];
    provider: ProviderName[] = [];
    maxDate = new Date();
    searchprovider: any = {};
    roleId: AppUser;
    dayToOverride: Number;
    disableCaseWorker = false;
    isUpdatePathway: Boolean = false;
    sdmCountyValuesDropdownItems$: Observable<DropdownModel[]>;
    savedSDMs: any;
    apporvalDatetime: any;
    sdmSettings: {
        issexualabuse: boolean;
        isoutofhome: boolean;
        isdeathorserious: boolean;
        issignordiagonises: boolean;
        isPopulate: boolean;
        isDisableOverride: boolean;
        isSupervisor: boolean;
        isDisablescrnin: boolean;
        isDisablescrnout: boolean;
    };
    pathwayChange = false;
    pathwaySdm = new SdmData();
    initialScreenInMessage: string;
    selectedProvider: any = [];
    providersList = [];
    childFatality = 'no';
    involvedPersonList: any= [];
    isIRFlag: boolean;
    childunderoneyear: boolean;
    isSENFlag= false;
    truePropertyPAFlag = false;
    truePropertyGNFlag = false;
    CPSFlag: string;
    isAcptNonCPS: boolean;
    isClosed = false;
    isServiceCase = false;
    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private router: Router,
        private _alertService: AlertService,
        private dataStoreService: DataStoreService,
        private _session: SessionStorageService
    ) {
        this.id = this.dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.sdmSettings = {
            issexualabuse: false,
            isoutofhome: false,
            isdeathorserious: false,
            issignordiagonises: false,
            isPopulate: false,
            isDisableOverride: false,
            isSupervisor: false,
            isDisablescrnin: false,
            isDisablescrnout: false,
        };
    }

    ngOnInit() {
        this.CPSFlag = this.dataStoreService.getData('CPSFlag');
        this.roleId = this._authService.getCurrentUser();
        this.apporvalDatetime = this.dataStoreService.getData('dsdsActionsSummary').da_insertedon; // get case approval datetime
        this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        this.buildFormGroup();
        this.setsearchprovider();
        // this.getCountyDropdown();
        this.getPage();

        this.listSDMHistory().subscribe((data) => {
            if (this.isServiceCase) {
                this.savedSDMs = this.setVersion(data[0].getservicecasesdm);
            } else {
                this.savedSDMs = this.setVersion(data[0].getintakeservicerequestsdm);
            }

        });

        this.getInvolvedPerson();
        // if(this.pathwayChange) {
        //     this.setSubscribers();
        // }
        this.decideScreenInMessage();

        const da_status = this._session.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }
    }

    setSubscribers() {
        this.sdmFormGroup.valueChanges.subscribe(() => {
            this.isAcptNonCPS = false;
            this.sdm = Object.assign({}, this.sdmFormGroup.value);

            const screeningRecommendVal = this.sdmFormGroup.get('screeningRecommend').value;

            // @TM: Screening Recommendation
            if (screeningRecommendVal === 'ScreenOUT') {
                // this.sdmFormGroup.patchValue({ screeningRecommend: 'ScreenOUT' }, { emitEvent: false });
                this.sdmSettings.isDisablescrnin = false;
                this.sdmSettings.isDisablescrnout = true;
                // this.resetOverrides();
            } else if (screeningRecommendVal === 'Scrnin') {
                // this.sdmFormGroup.patchValue({ screeningRecommend: 'Scrnin' }, { emitEvent: false });
                this.sdmSettings.isDisablescrnin = true;
                this.sdmSettings.isDisablescrnout = false;
            } else if (screeningRecommendVal === 'accept_as_noncps') {
                // this.isAcptNonCPS = true;
                // this.sdmFormGroup.patchValue({ screeningRecommend: 'accept_as_noncps' }, { emitEvent: false });
                this.sdmSettings.isDisablescrnin = false;
                this.sdmSettings.isDisablescrnout = false;
            }

            this.sdmFormGroup.patchValue({ isfinalscreenin: 'true' }, { emitEvent: false });
            if (this.sdmFormGroup.controls['scnRecommendOveride'].value === 'OvrScrnout') {
                this.sdmFormGroup.patchValue({ isfinalscreenin: 'false' }, { emitEvent: false });
            }

            if (!this.sdmSettings.isPopulate) {
                this.cpsResponseValidation(this.sdmFormGroup.value);
            }

            if (this.sdmFormGroup.get('isfinalscreenin').value === 'true') {
                this.sdmFormGroup.patchValue({ screeningRecommend: 'Scrnin' }, { emitEvent: false });
            } else {
                this.sdmFormGroup.patchValue({ screeningRecommend: 'ScreenOUT' }, { emitEvent: false });
            }

            this.validateIRAR(this.sdmFormGroup.value);

            setTimeout(() => {
                if (this.sdmFormGroup.controls['isfinalscreenin'].value === 'true') {
                    this.sdmFormGroup.controls['immediate'].setValidators([Validators.required]);
                    this.sdmFormGroup.controls['immediate'].updateValueAndValidity();
                } else {
                    this.sdmFormGroup.controls['immediate'].clearValidators();
                    this.sdmFormGroup.controls['immediate'].updateValueAndValidity();
                }
            }, 2000);
        });
    }

    // Begin - D-06631 - Defect Fix
    setVersion(savedSDMs) {
        // D-06631: Added proper version title for each row
        let isActiveStatusExist = true;
        const newSDMs = [];
        const isSenb = this.involvedPersonList.find(person => (person.drugexposednewbornflag === 1 || person.fetalalcoholspctrmdisordflag === 1 ));
        for (let index = 0; index < savedSDMs.length; index++) {
            const element = savedSDMs[index];
            const suffix = element.isar ? 'AR' : ( element.isir ? 'IR' : '');

            element.version = 'Previous Version - ' + suffix;
            element.isUpdatePathway = false;
            if (index === 0 && (element.pathwaystatus === 'Review' || element.pathwaystatus === 'Rejected')) {
                element.version = 'New Version - ' + suffix;

            }

            if (element.pathwaystatus === 'Review') {
                element.isUpdatePathway = true;
                this.isUpdatePathway = true;
            }

            if (isActiveStatusExist && element.pathwaystatus === 'Accepted') {
                element.version = 'Current Version';
                isActiveStatusExist = false;
                if (this.isUpdatePathway === false) {
                    element.isUpdatePathway = true;
                }
            }
            element.isUpdatePathway = (isSenb) ? false : element.isUpdatePathway;
            // disable change pathway btn
            element.disableChangePathwaybtn = false;
            if (this.apporvalDatetime !== '') {
                const date1 = new Date(this.apporvalDatetime);
                const date2 = new Date();
                const timeDiff = Math.abs(date2.getTime() - date1.getTime());
                const totalTime = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if (totalTime > 120) {
                    element.disableChangePathwaybtn = true;
                }
            }
            newSDMs.push(element);
        }
        return newSDMs;
    }
    // End - D-06631 - Defect Fix

    buildFormGroup() {
        this.sdmFormGroup = this.formBuilder.group({
            referralname: [''],
            referraldob: [new Date()],
            referralid: [''],
            countyid: [null],
            county: [''],
            // allegedvictim: this.formBuilder.array([this.createFormGroup('allegedvictim')]),
            // allegedmaltreator: this.formBuilder.array([this.createFormGroup('allegedmaltreator')]),
            // provider: this.formBuilder.array([]),
            ismaltreatment: [false],
            maltreatment: [null],
            providerKnown: [null],
            childfatality: ['no'],
            ischildfatality: [false],
            isfcplacementsetting: [false],
            isprivateplacement: [false],
            islicenseddaycare: [false],
            isschool: [false],
            physicalAbuse: this.formBuilder.group({
                ismalpa_suspeciousdeath: [false],
                ismalpa_nonaccident: [false],
                ismalpa_injuryinconsistent: [false],
                ismalpa_insjury: [false],
                ismalpa_childtoxic: [false],
                ismalpa_caregiver: [false]
            }),
            sexualAbuse: this.formBuilder.group({
                ismalsa_sexualmolestation: [false],
                ismalsa_sexualact: [false],
                ismalsa_sexualexploitation: [false],
                ismalsa_physicalindicators: [false],
                ismalsa_sex_trafficking: [false]
            }),
            generalNeglect: this.formBuilder.group({
                isneggn_suspiciousdeath: [false],
                isneggn_signsordiagnosis: [false],
                isneggn_inadequatefood: [false],
                isneggn_childdischarged: [false]
            }),
            arGeneralNeglect: this.formBuilder.group({
                isneggn_exposuretounsafe: [false],
                isneggn_inadequateclothing: [false],
                isneggn_inadequatesupervision: [false],
                isnegrh_treatmenthealthrisk: [false]
            }),
            isnegfp_cargiverintervene: [false],
            isnegab_abandoned: [false],
            unattendedChild: this.formBuilder.group({
                isneguc_leftunsupervised: [false],
                isneguc_leftaloneinappropriatecare: [false],
                isneguc_leftalonewithoutsupport: [false]
            }),
            riskofHarm: this.formBuilder.group({
                isnegrh_priordeath: [false],
                isnegrh_exposednewborn: [{value: false, disabled: true}],
                // isnegrh_domesticviolence: [false],
                // isnegrh_sexualperpetrator: [false],
                isnegrh_basicneedsunmet: [false],
                // isnegrh_substantial_risk: [false],
                isnegrh_sex_offender: [false],
                isnegrh_risk_dv: [false],
                isnegrh_fatality_can: [false],
                isnegrh_indicated_unsub: [false],
                isnegrh_survivor: [false],
                isnegrh_birth_match: [false],
                isnegrh_sex_trafficking: [false]

            }),
            isnegmn_unreasonabledelay: [false],
            ismenab_psycologicalability: [false],
            ismenng_psycologicalability: [false],
            screeningRecommend: [''],
            scnRecommendOveride: [''],
            screenOut: this.formBuilder.group({
                isscrnoutrecovr_insufficient: [false],
                isscrnoutrecovr_information: [false],
                isscrnoutrecovr_historicalinformation: [false],
                isscrnoutrecovr_otherspecify: [false],
                scrnout_description: ['']
            }),
            screenIn: this.formBuilder.group({
                isscrninrecovr_courtorder: [false],
                isscrninrecovr_otherspecify: [false],
                scrnin_description: ['']
            }),
            immediate: [''],
            immediateList: this.formBuilder.group({
                isimmed_childfaatility: [false],
                isimmed_seriousinjury: [false],
                isimmed_childleftalone: [false],
                isimmed_allegation: [false],
                isimmed_otherspecify: [false],
                immediateList6: ['']
            }),
            noImmediateList: this.formBuilder.group({
                isnoimmed_physicalabuse: [false],
                isnoimmed_sexualabuse: [false],
                isnoimmed_neglectresponse: [false],
                isnoimmed_mentalinjury: [false],
                isnoimmed_substantial_risk: [false],
                isnoimmed_screeninoverride: [false],
                isnoimmed_risk_harm: [false]

            }),
            childunderoneyear: [''],
            childUnderOneYear: [''],
            officerfirstname: [''],
            officermiddlename: [''],
            officerlastname: [''],
            badgenumber: [''],
            recordnumber: [''],
            reportdate: [null],
            worker: [''],
            comments: [''],
            workerdate: [null],
            supervisor: [''],
            supervisordate: [null],
            disqualifyingCriteria: this.formBuilder.group({
                issexualabuse: [false],
                isoutofhome: [false],
                isdeathorserious: [false],
                isrisk: [false],
                isreportmeets: [false],
                issignordiagonises: [false],
                ismaltreatment3yrs: [false],
                ismaltreatment12yrs: [false],
                ismaltreatment24yrs: [false],
                isactiveinvestigation: [false]
            }),
            disqualifyingFactors: this.formBuilder.group({
                isreportedhistory: [false],
                ismultiple: [false],
                isdomesticvoilence: [false],
                iscriminalhistory: [false],
                isthread: [false],
                islawenforcement: [false],
                iscourtiinvestigation: [false]
            }),
            cpsResponseType: [null],
            isar: [true],
            isir: [false],
            // iscps: [null],
            isfinalscreenin: [null]
        });
        this.sdmFormGroup.addControl('allegedvictim', this.formBuilder.array([this.createFormGroup('allegedvictim')]));
        this.sdmFormGroup.addControl('allegedmaltreator', this.formBuilder.array([this.createFormGroup('allegedmaltreator')]));
        this.sdmFormGroup.addControl('provider', this.formBuilder.array([]));
    }

    updateSubstantialRisk() {
        if (this.involvedPersonList && this.involvedPersonList.length) {
            this.sdmFormGroup.get('riskofHarm').patchValue({'isnegrh_exposednewborn': false }, { emitEvent: false });
            this.sdmFormGroup.get('riskofHarm').get('isnegrh_exposednewborn').disable();
            this.isSENFlag = false;
            // this.validateIRARScreenIn();
            const hasSubstantialRiskPerson = this.involvedPersonList.find(person => (person.drugexposednewbornflag === 1 || person.fetalalcoholspctrmdisordflag === 1 ));
            if (hasSubstantialRiskPerson) {
                this.sdmFormGroup.get('riskofHarm').patchValue({'isnegrh_exposednewborn': true }, { emitEvent: false });
                this.isSENFlag = true;
                this.setImmediates();
                // this.validateIRARScreenIn();
            }
        }
    }

    setFormValues() {
        const control = <FormArray>this.sdmFormGroup.controls.allegedvictim;
        if (this.allegedVictim) {
            this.allegedVictim.forEach((x) => {
                control.push(this.buildAllegedVictimForm(x));
            });
        }

        const allegedMaltreatorControl = <FormArray>this.sdmFormGroup.controls.allegedmaltreator;
        if (this.allegedMaltreator) {
            this.allegedMaltreator.forEach((x) => {
                allegedMaltreatorControl.push(this.buildAllegedMaltreatorForm(x));
            });
        }

        const providerControl = <FormArray>this.sdmFormGroup.controls.provider;
        if (this.provider) {
            this.provider.forEach((x) => {
                providerControl.push(this.buildProviderForm(x));
            });
        }
    }

    getPage() {
        let sdmUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmvaluesUrl;
        let requestParam ;
        if (this.isServiceCase) {
            sdmUrl = 'servicecase/getservicecasesdm';
            requestParam = {
                servicecaseid : this.id
            };
        } else {
            requestParam = {
                servicerequestid: this.id
            };
        }
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: requestParam,
                },
                sdmUrl + '?filter'
            )
            .subscribe((res) => {
                let sdm;
                if (res && Array.isArray(res) && res.length) {
                    if (this.isServiceCase) {
                        sdm = res[0].getservicecasesdm[0];
                    } else {
                        sdm = res[0].getintakeservicerequestsdm[0];
                    }
                    if (sdm) {
                        this.patchSDM(sdm, false);
                    }
                } else {
                    if (this.isServiceCase) {
                    this.sdmFormGroup.disable();
                    }
                 }
            });
    }

    patchSDM(sdm, changePathway) {
        this.sdmSettings.isPopulate = true;
        this.populateSdm = Object.assign({}, sdm);

        /* Patch start */
        this.populateSdm.physicalAbuse = Object.assign({
            ismalpa_suspeciousdeath: sdm.ismalpa_suspeciousdeath,
            ismalpa_nonaccident: sdm.ismalpa_nonaccident,
            ismalpa_injuryinconsistent: sdm.ismalpa_injuryinconsistent,
            ismalpa_insjury: sdm.ismalpa_insjury,
            ismalpa_childtoxic: sdm.ismalpa_childtoxic,
            ismalpa_caregiver: sdm.ismalpa_caregiver
        });
        this.populateSdm.sexualAbuse = Object.assign({
            ismalsa_sexualmolestation: sdm.ismalsa_sexualmolestation,
            ismalsa_sexualact: sdm.ismalsa_sexualact,
            ismalsa_sexualexploitation: sdm.ismalsa_sexualexploitation,
            ismalsa_physicalindicators: sdm.ismalsa_physicalindicators,
            ismalsa_sex_trafficking: sdm.ismalsa_sex_trafficking
        });
        this.populateSdm.generalNeglect = Object.assign({
            isneggn_suspiciousdeath: sdm.isneggn_suspiciousdeath,
            isneggn_signsordiagnosis: sdm.isneggn_signsordiagnosis,
            isneggn_inadequatefood: sdm.isneggn_inadequatefood,
            isneggn_childdischarged: sdm.isneggn_childdischarged
        });
        this.populateSdm.arGeneralNeglect = Object.assign({
            isneggn_exposuretounsafe: sdm.isneggn_exposuretounsafe,
            isneggn_inadequateclothing: sdm.isneggn_inadequateclothing,
            isneggn_inadequatesupervision: sdm.isneggn_inadequatesupervision,
            isnegrh_treatmenthealthrisk: sdm.isnegrh_treatmenthealthrisk
        });
        this.populateSdm.unattendedChild = Object.assign({
            isneguc_leftunsupervised: sdm.isneguc_leftunsupervised,
            isneguc_leftaloneinappropriatecare: sdm.isneguc_leftaloneinappropriatecare,
            isneguc_leftalonewithoutsupport: sdm.isneguc_leftalonewithoutsupport
        });
        this.populateSdm.riskofHarm = Object.assign({
            isnegrh_priordeath: sdm.isnegrh_priordeath,
            isnegrh_exposednewborn: sdm.isnegrh_exposednewborn,
            isnegrh_basicneedsunmet: sdm.isnegrh_basicneedsunmet,
            isnegrh_sex_offender: sdm.isnegrh_sex_offender,
            isnegrh_risk_dv: sdm.isnegrh_risk_dv,
            isnegrh_sex_trafficking: sdm.isnegrh_sex_trafficking,
            isnegrh_fatality_can: sdm.isnegrh_fatality_can,
            isnegrh_indicated_unsub: sdm.isnegrh_indicated_unsub,
            isnegrh_survivor: sdm.isnegrh_survivor,
            isnegrh_birth_match: sdm.isnegrh_birth_match
        });
        this.populateSdm.screenOut = Object.assign({
            isscrnoutrecovr_insufficient: sdm.isscrnoutrecovr_insufficient,
            isscrnoutrecovr_information: sdm.isscrnoutrecovr_information,
            isscrnoutrecovr_historicalinformation: sdm.isscrnoutrecovr_historicalinformation,
            isscrnoutrecovr_otherspecify: sdm.isscrnoutrecovr_otherspecify,
            scrnout_description: sdm.scrnout_description
        });
        this.populateSdm.screenIn = Object.assign({
            isscrninrecovr_courtorder: sdm.isscrninrecovr_courtorder,
            isscrninrecovr_otherspecify: sdm.isscrninrecovr_otherspecify,
            scrnin_description: sdm.scrnin_description
        });

        if (sdm.isnoimmed_substantial_risk) {
            this.populateSdm.immediate = 'No Immediate';
        } else if (sdm.isnoimmed_physicalabuse || sdm.isnoimmed_sexualabuse || sdm.isnoimmed_neglectresponse || sdm.isnoimmed_mentalinjury || sdm.isnoimmed_risk_harm) {
            this.populateSdm.immediate = 'No Immediate';
        } else if (sdm.isimmed_childfaatility || sdm.isimmed_seriousinjury || sdm.isimmed_childleftalone || sdm.isimmed_allegation || sdm.isimmed_otherspecify) {
            this.populateSdm.immediate = 'Immediate';
        } else {
            this.populateSdm.immediate = '';
        }
        this.changeImmediate(this.populateSdm.immediate);

        this.populateSdm.immediateList = Object.assign({
            isimmed_childfaatility: sdm.isimmed_childfaatility,
            isimmed_seriousinjury: sdm.isimmed_seriousinjury,
            isimmed_childleftalone: sdm.isimmed_childleftalone,
            isimmed_allegation: sdm.isimmed_allegation,
            isimmed_otherspecify: sdm.isimmed_otherspecify
        });
        this.populateSdm.noImmediateList = Object.assign({
            isnoimmed_physicalabuse: sdm.isnoimmed_physicalabuse,
            isnoimmed_sexualabuse: sdm.isnoimmed_sexualabuse,
            isnoimmed_neglectresponse: sdm.isnoimmed_neglectresponse,
            isnoimmed_mentalinjury: sdm.isnoimmed_mentalinjury,
            isnoimmed_substantial_risk: sdm.isnoimmed_substantial_risk ,
            isnoimmed_screeninoverride: sdm.isnoimmed_screeninoverride,
            isnoimmed_risk_harm: sdm.isnoimmed_risk_harm,
        });
        this.populateSdm.disqualifyingCriteria = Object.assign({
            issexualabuse: sdm.issexualabuse,
            isoutofhome: sdm.isoutofhome,
            isdeathorserious: sdm.isdeathorserious,
            isrisk: sdm.isrisk,
            isreportmeets: sdm.isreportmeets,
            issignordiagonises: sdm.issignordiagonises,
            ismaltreatment3yrs: sdm.ismaltreatment3yrs,
            ismaltreatment12yrs: sdm.ismaltreatment12yrs,
            ismaltreatment24yrs: sdm.ismaltreatment24yrs,
            isactiveinvestigation: sdm.isactiveinvestigation
        });
        this.populateSdm.disqualifyingFactors = Object.assign({
            isreportedhistory: sdm.isreportedhistory,
            ismultiple: sdm.ismultiple,
            isdomesticvoilence: sdm.isdomesticvoilence,
            iscriminalhistory: sdm.iscriminalhistory,
            isthread: sdm.isthread,
            islawenforcement: sdm.islawenforcement,
            iscourtiinvestigation: sdm.iscourtiinvestigation
        });

        if (sdm.ismaltreatment) {
            this.populateSdm.maltreatment = 'yes';
        } else if (sdm.ismaltreatment === null || sdm.ismaltreatment === '') {
            this.populateSdm.maltreatment = '';
        } else {
            this.populateSdm.maltreatment = 'no';
        }

        if (sdm.isrecsc_screenout) {
            this.populateSdm.screeningRecommend = 'ScreenOUT';
        } else if (sdm.isrecsc_scrrenin) {
            this.populateSdm.screeningRecommend = 'Scrnin';
        }

        if (sdm.isir) {
            this.populateSdm.cpsResponseType = 'CPS-IR';
        } else if (sdm.isar) {
            this.populateSdm.cpsResponseType = 'CPS-AR';
        }

        this.populateSdm.isnegfp_cargiverintervene = sdm.isnegfp_cargiverintervene;
        this.populateSdm.isnegab_abandoned = sdm.isnegab_abandoned;

        if (sdm.isscrnoutrecovr_insufficient || sdm.isscrnoutrecovr_information || sdm.isscrnoutrecovr_historicalinformation || sdm.isscrnoutrecovr_otherspecify) {
            this.scnRecommendOveride = 'OvrScrnout';
        } else if (sdm.isscrninrecovr_courtorder || sdm.isscrninrecovr_otherspecify) {
            this.scnRecommendOveride = 'Ovrscrnin';
        } else {
            this.scnRecommendOveride = '';
        }

        this.populateSdm.county = sdm.countyid;

        /* Patch end */

        this.populateSdm.referraldob = new Date(this.populateSdm.referraldob);
        this.populateSdm.reportdate = this.populateSdm.reportdate ? new Date(this.populateSdm.reportdate) : null;

        // @TM: handled in getInvolvedPerson()--------------------------------
        // this.sdmFormGroup.setControl('allegedvictim', this.formBuilder.array([]));
        // this.sdmFormGroup.setControl('allegedmaltreator', this.formBuilder.array([]));
        // this.sdmFormGroup.setControl('provider', this.formBuilder.array([]));
        // this.allegedVictim = this.populateSdm.allegedvictim;
        // this.allegedMaltreator = this.populateSdm.allegedmaltreator;
        this.provider = this.populateSdm.provider;

        this.sdmFormGroup.patchValue(this.populateSdm);

        if (sdm.isfinalscreenin === true) {
            this.populateSdm.isfinalscreenin = true;
            this.sdmFormGroup.patchValue({ isfinalscreenin: 'true' }, { emitEvent: false });
        } else if (sdm.isfinalscreenin === false) {
            this.populateSdm.isfinalscreenin = false;
            this.sdmFormGroup.patchValue({ isfinalscreenin: 'false' }, { emitEvent: false });
        } else if (this.sdmFormGroup.get('screeningRecommend').value === 'accept_as_noncps') {
            this.sdmFormGroup.patchValue({ isfinalscreenin: 'Ovr_as_noncps' }, { emitEvent: false });
        }

        this.isScreenOutIN = this.sdmFormGroup.value.scnRecommendOveride;

        if (this.populateSdm.maltreatment === 'yes') {
            this.isDisplayProvider = true;
        }

        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.role.name === 'apcs') {
            this.sdmSettings.isSupervisor = true;
        }

        if (changePathway) {
            this.sdm = sdm;
            this.sdmFormGroup.enable();
            this.sdmSettings.isPopulate = false;
        } else {
            this.sdmFormGroup.disable();
        }

        this.disableCaseWorker = true;
        if (this.isServiceCase) {
            this.sdmFormGroup.disable();
        }
    }

    switchPathway(sdmData, changePathway) {
        this.pathwayChange = changePathway;
        if (this.pathwayChange === true) {
            this.setSubscribers();
        }
        this.patchSDM(sdmData, changePathway);
        this.getInvolvedPerson();
        (<any>$('#dec-mak-step1')).click();
    }

    savePathway() {
        if (this.pathwayChange) {
            let sdm = Object.assign({}, this.sdmFormGroup.value);
            sdm = Object.assign(sdm, sdm.physicalAbuse);
            sdm = Object.assign(sdm, sdm.sexualAbuse);
            sdm = Object.assign(sdm, sdm.generalNeglect);
            sdm = Object.assign(sdm, sdm.arGeneralNeglect);
            sdm = Object.assign(sdm, sdm.unattendedChild);
            sdm = Object.assign(sdm, sdm.riskofHarm);
            sdm = Object.assign(sdm, sdm.screenOut);
            sdm = Object.assign(sdm, sdm.screenIn);
            sdm = Object.assign(sdm, sdm.immediateList);
            sdm = Object.assign(sdm, sdm.noImmediateList);
            sdm = Object.assign(sdm, sdm.disqualifyingCriteria);
            sdm = Object.assign(sdm, sdm.disqualifyingFactors);

            if (sdm.cpsResponseType === 'CPS-IR') {
                sdm.isir = true;
                sdm.isar = false;
            } else if (sdm.cpsResponseType === 'CPS-AR') {
                sdm.isir = false;
                sdm.isar = true;
            } else {
                sdm.isir = false;
                sdm.isar = false;
            }

            if (sdm.maltreatment === 'yes') {
                sdm.ismaltreatment = true;
            } else {
                sdm.ismaltreatment = false;
            }
            if (sdm.childfatality === 'yes') {
                sdm.ischildfatality = true;
            } else {
                sdm.ischildfatality = false;
            }

            if (sdm.screeningRecommend === 'ScreenOUT') {
                sdm.isrecsc_screenout = true;
                sdm.isrecsc_scrrenin = false;
            } else if (sdm.screeningRecommend === 'Scrnin') {
                sdm.isrecsc_screenout = false;
                sdm.isrecsc_scrrenin = true;
            } else {
                sdm.isrecsc_screenout = false;
                sdm.isrecsc_scrrenin = false;
            }
            this.saveSdmPathway(sdm)
                .flatMap((value) => {
                    this._alertService.success('Sdm pathway changes sent for approval.');
                    return this.listSDMHistory();
                })
                .subscribe((data) => {
                    this.savedSDMs = this.setVersion(data[0].getintakeservicerequestsdm);
                    (<any>$('#tab-step0')).click();
                    this.pathwayChange = false;
                });
        }
    }

    saveSdmPathway(sdm) {
        sdm.reportdate = null;
        this.pathwaySdm.sdmdata = Object.assign({}, sdm);
        this.pathwaySdm.intakenumber = null;
        this.pathwaySdm.servicerequestid = this.id;
        return this._commonHttpService.create(this.pathwaySdm, CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.PathwaySdmCreateUrl);
    }

    authorizedPathway(status, sdm) {
        if (sdm) {
            this._commonHttpService
            .create(
                {
                    method: 'post',
                    servicerequestid: this.id,
                    status: status,
                    intakeservicerequestsdmid: sdm.intakeservicerequestsdmid
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.PathwayAutorizationUrl
            )
            .subscribe((data) => {
                if (status === 'Approved') {
                    this._alertService.success('Sdm pathway changes approved successfully.');
                } else if (status === 'Rejected') {
                    this._alertService.warn('Sdm pathway changes declined.');
                }
                Observable.timer(2000).subscribe((res) => {
                    if (this.roleId.role.name === 'field') {
                        this.router.navigate(['/pages/home-dashboard']);
                    } else if (this.roleId.role.name === 'apcs') {
                        this.router.navigate(['/pages/cjams-dashboard']);
                    }
                });
            });
        }
    }

    private listSDMHistory() {
        let sdmUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmvaluesUrl;
        let requestParam ;
        if (this.isServiceCase) {
            sdmUrl = 'servicecase/getservicecasesdm';
            requestParam = {
                servicecaseid : this.id
            };
        } else {
            requestParam = {
                servicerequestid: this.id
            };
        }
        return this._commonHttpService.getArrayList(
            new PaginationRequest({
                where: requestParam,
                method: 'get'
            }),
            sdmUrl + '?filter'
        );
    }

    private buildAllegedVictimForm(x): FormGroup {
        return this.formBuilder.group({
            victimname: x.victimname ? x.victimname : ''
        });
    }
    private buildAllegedMaltreatorForm(x): FormGroup {
        return this.formBuilder.group({
            maltreatorsname: x.maltreatorsname ? x.maltreatorsname : ''
        });
    }
    private buildProviderForm(x): FormGroup {
        return this.formBuilder.group({
            providername: x.maltreatorsname ? x.maltreatorsname : '',       // @TM: the variable name is maltreatorsname for some reason, correct this later
            // providername: x.providername ? x.providername : '',
            providerid: x.providerid ? x.providerid : '',
            providerphone: x.providerphone ? x.providerphone : ''
        });
    }

    createFormGroup(formGroupName) {
        if (formGroupName === 'allegedvictim') {
            return this.formBuilder.group({
                victimname: ['']
            });
        } else if (formGroupName === 'allegedmaltreator') {
            return this.formBuilder.group({
                maltreatorsname: ['']
            });
        } else if (formGroupName === 'provider') {
            return this.formBuilder.group({
                providername: ['']
            });
        }
    }

    addNewFormGroup(formGroupName) {
        const control = <FormArray>this.sdmFormGroup.controls[formGroupName];
        control.push(this.createFormGroup(formGroupName));
    }

    deleteFormGroup(index: number, formGroupName) {
        const control = <FormArray>this.sdmFormGroup.controls[formGroupName];
        control.removeAt(index);
    }

    onChangeMaltreatment(item) {
        // console.log(item);
        this.sdmFormGroup.patchValue({ providerKnown : null});
        if (item === 'yes') {
            this.isDisplayProvider = true;
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isoutofhome: true }, { emitEvent: false });
            this.sdmSettings.isoutofhome = true;
        } else {
            this.isDisplayProvider = false;
            const control = <FormArray>this.sdmFormGroup.controls['provider'];
            control.controls = [];
            // control.push(this.createFormGroup('provider'));
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isoutofhome: false }, { emitEvent: false });
            this.sdmSettings.isoutofhome = false;
        }
    }

    changeScreenOutIN(item) {
        this.isScreenOutIN = item;
        if (item === 'Scrnin') {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        } else if (item === 'ScreenOUT') {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-AR' }, { emitEvent: false });
        } else {
        }
    }
    changeOverScreenOutIN(item) {
        this.isScreenOutIN = item;
        if (item === 'Ovrscrnin') {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        } else if (item === 'OvrScrnout') {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-AR' }, { emitEvent: false });
        } else {
        }
    }
    changeImmediate(item) {
        this.isImmediate = item;
    }
    changeChildInderOneYear(item) {
        this.isChildInderOneYear = item;
        this.validateIRAR(this.sdmFormGroup.value);
    }

    // private validateGroup(formGroup: FormGroup) {
    //     for (const key in formGroup.controls) {
    //         if (formGroup.controls.hasOwnProperty(key)) {
    //             const control: FormControl = <FormControl>formGroup.controls[key];
    //             if (control.value) {
    //                 return null;
    //             }
    //         }
    //     }
    //     return {
    //         validateGroup: {
    //             valid: false
    //         }
    //     };
    // }

    private cpsResponseValidation(sdm) {
        this.populateSdm = Object.assign({}, sdm);
        this.sdmFormGroup.patchValue({ immediate: '' }, { emitEvent: false });
        // this.isNoImmediate = false;
        this.isImmediate = '';

        if (ObjectUtils.checkTrueProperty(this.populateSdm.sexualAbuse) >= 1) { // Sexual Abuse - any selection
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            // this.isNoImmediate = true;
            // this.isImmediate = false;
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_sexualabuse: true, disabled: true }, { emitEvent: false });
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issexualabuse: true }, { emitEvent: false });
        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_sexualabuse: false, disabled: false }, { emitEvent: false });
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issexualabuse: false }, { emitEvent: false });
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.physicalAbuse) >= 1) { // Physical Abuse
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            // this.isNoImmediate = true;
            // this.isImmediate = false;
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_physicalabuse: true, disabled: true }, { emitEvent: false });

            if (this.populateSdm.physicalAbuse.ismalpa_suspeciousdeath === true) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: true }, { emitEvent: false });
                this.truePropertyPAFlag = true;
            } else {
                if (!this.truePropertyGNFlag) {
                    this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
                }
                this.truePropertyPAFlag = false;
            }
        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_physicalabuse: false, disabled: false }, { emitEvent: false });

            if (!this.truePropertyGNFlag) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
            }
            this.truePropertyPAFlag = false;
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.generalNeglect) >= 1) { // General Neglect
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            // this.isNoImmediate = true;
            // this.isImmediate = false;
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_neglectresponse: true, disabled: true }, { emitEvent: false });

            if (this.populateSdm.generalNeglect.isneggn_suspiciousdeath === true) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: true }, { emitEvent: false });
                this.truePropertyGNFlag = true;
            } else {
                if (!this.truePropertyPAFlag) {
                    this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
                }
                this.truePropertyGNFlag = false;
            }

            if (this.populateSdm.generalNeglect.isneggn_signsordiagnosis === true) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issignordiagonises: true }, { emitEvent: false });
            } else {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issignordiagonises: false }, { emitEvent: false });
            }

        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_neglectresponse: false, disabled: false }, { emitEvent: false });

            if (!this.truePropertyPAFlag) {
                this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isdeathorserious: false }, { emitEvent: false });
            }
            this.truePropertyGNFlag = false;

            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ issignordiagonises: false }, { emitEvent: false });
        }

        /*
        Fix for "Unable to swap CPS AR to IR pathway change - need to update ability to only change 
        discretionary factors and update pathway (need to update SDM options which is incorrect - 
        shouldn't be able to update SDM) 
        */

        if (ObjectUtils.checkTrueProperty(this.populateSdm.arGeneralNeglect) >= 1) {
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_neglectresponse: true, disabled: true }, { emitEvent: false });
        }
        
        if (this.populateSdm.ismenab_psycologicalability === true || this.populateSdm.ismenng_psycologicalability === true) { // Mental Abuse or Neglect
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            // this.isNoImmediate = true;
            // this.isImmediate = false;
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_mentalinjury: true, disabled: true }, { emitEvent: false });
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isreportmeets: true }, { emitEvent: false });
        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_mentalinjury: false, disabled: false }, { emitEvent: false });
            this.sdmFormGroup.controls['disqualifyingCriteria'].patchValue({ isreportmeets: false }, { emitEvent: false });
        }
        if (ObjectUtils.checkTrueProperty(this.populateSdm.riskofHarm) >= 1) { // Risk of Harm
            this.sdmFormGroup.patchValue({ immediate: 'No Immediate' }, { emitEvent: false });
            // this.isNoImmediate = true;
            // this.isImmediate = false;

            if (this.isSENFlag === true) {
                this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_substantial_risk: true }, { emitEvent: false });
                this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_risk_harm: false, disabled: true }, { emitEvent: false });
            } else {
                this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_substantial_risk: false }, { emitEvent: false });
                this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_risk_harm: true, disabled: true }, { emitEvent: false });
            }
        } else {
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_risk_harm: false, disabled: false }, { emitEvent: false });
            this.sdmFormGroup.controls['noImmediateList'].patchValue({ isnoimmed_substantial_risk: false, disabled: true }, { emitEvent: false });    // @TM: should be always disabled
        }

        this.isImmediate = this.sdmFormGroup.value.immediate;
        this.validateCPSIRAR(this.sdmFormGroup.value);
    }

    validateIRAR(sdm: Sdm) {
        if (sdm.scnRecommendOveride !== '') {
            if (sdm.scnRecommendOveride === 'Ovrscrnin') {
                this.validateIRARScreenIn();
            } else if (sdm.scnRecommendOveride === 'OvrScrnout') {
                this.validateIRARScreenOut();
            } else {
                this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
            }
        } else if (sdm.screeningRecommend === 'Scrnin') {
            this.validateIRARScreenIn();
        } else if (ObjectUtils.checkTrueProperty(sdm.disqualifyingCriteria)) {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        } else if (ObjectUtils.checkTrueProperty(sdm.disqualifyingFactors)) {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        } else {
            this.validateIRARScreenOut();
        }
        if (this.isChildInderOneYear === 'Yes' || (this.childFatality === 'yes' &&  this.sdmFormGroup.getRawValue().childfatality === 'yes')) {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        }
    }

    validateIRARScreenIn() { // if Screen In or Over-ride Screen In

        // Code pulled from intake-sdm.component.ts ----------------------------------
        if (this.CPSFlag && this.CPSFlag === 'disable') {
            if (                                                                              // ----------IR Conditions
                !this.sdm.physicalAbuse.ismalpa_suspeciousdeath &&
                !this.sdm.generalNeglect.isneggn_suspiciousdeath &&
                !this.sdm.generalNeglect.isneggn_signsordiagnosis &&
                !this.sdm.ismenab_psycologicalability &&
                !this.sdm.ismenng_psycologicalability &&
                !this.sdm.sexualAbuse.ismalsa_physicalindicators &&
                !this.sdm.sexualAbuse.ismalsa_sex_trafficking &&
                !this.sdm.sexualAbuse.ismalsa_sexualact &&
                !this.sdm.sexualAbuse.ismalsa_sexualexploitation &&
                !this.sdm.sexualAbuse.ismalsa_sexualmolestation
            ) {
                this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
                this.isIRFlag = false;
            } else if
            (
                this.sdm.physicalAbuse.ismalpa_suspeciousdeath ||
                this.sdm.generalNeglect.isneggn_suspiciousdeath ||
                this.sdm.generalNeglect.isneggn_signsordiagnosis ||
                this.sdm.ismenab_psycologicalability ||
                this.sdm.ismenng_psycologicalability ||
                this.sdm.sexualAbuse.ismalsa_physicalindicators ||
                this.sdm.sexualAbuse.ismalsa_sex_trafficking ||
                this.sdm.sexualAbuse.ismalsa_sexualact ||
                this.sdm.sexualAbuse.ismalsa_sexualexploitation ||
                this.sdm.sexualAbuse.ismalsa_sexualmolestation
            ) {
                this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
                this.isIRFlag = true;
            }

        } else {
            if (                                                                              // ----------IR Conditions
                !this.sdm.physicalAbuse.ismalpa_suspeciousdeath &&
                !this.sdm.generalNeglect.isneggn_suspiciousdeath &&
                !this.sdm.generalNeglect.isneggn_signsordiagnosis &&
                !this.sdm.ismenab_psycologicalability &&
                !this.sdm.ismenng_psycologicalability &&
                !this.sdm.sexualAbuse.ismalsa_physicalindicators &&
                !this.sdm.sexualAbuse.ismalsa_sex_trafficking &&
                !this.sdm.sexualAbuse.ismalsa_sexualact &&
                !this.sdm.sexualAbuse.ismalsa_sexualexploitation &&
                !this.sdm.sexualAbuse.ismalsa_sexualmolestation &&
                // Mandatory Disqualifying Criteria - this is still required to ensure correct precedence
                !this.sdm.disqualifyingCriteria.isrisk &&
                !this.sdm.disqualifyingCriteria.ismaltreatment3yrs &&
                !this.sdm.disqualifyingCriteria.ismaltreatment12yrs &&
                !this.sdm.disqualifyingCriteria.ismaltreatment24yrs &&
                !this.sdm.disqualifyingCriteria.isactiveinvestigation &&
                // Discretionary Disqualifying Factors - this is still required to ensure correct precedence
                !this.sdm.disqualifyingFactors.isreportedhistory &&
                !this.sdm.disqualifyingFactors.ismultiple &&
                !this.sdm.disqualifyingFactors.isdomesticvoilence &&
                !this.sdm.disqualifyingFactors.iscriminalhistory &&
                !this.sdm.disqualifyingFactors.isthread &&
                !this.sdm.disqualifyingFactors.islawenforcement &&
                !this.sdm.disqualifyingFactors.iscourtiinvestigation
            ) {
                this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
                this.isIRFlag = false;
            } else if
            (
                this.sdm.physicalAbuse.ismalpa_suspeciousdeath ||
                this.sdm.generalNeglect.isneggn_suspiciousdeath ||
                this.sdm.generalNeglect.isneggn_signsordiagnosis ||
                this.sdm.ismenab_psycologicalability ||
                this.sdm.ismenng_psycologicalability ||
                this.sdm.sexualAbuse.ismalsa_physicalindicators ||
                this.sdm.sexualAbuse.ismalsa_sex_trafficking ||
                this.sdm.sexualAbuse.ismalsa_sexualact ||
                this.sdm.sexualAbuse.ismalsa_sexualexploitation ||
                this.sdm.sexualAbuse.ismalsa_sexualmolestation ||
                // Disqualifying Criteria - this is still required to ensure correct precedence
                this.sdm.disqualifyingCriteria.isrisk ||
                this.sdm.disqualifyingCriteria.ismaltreatment3yrs ||
                this.sdm.disqualifyingCriteria.ismaltreatment12yrs ||
                this.sdm.disqualifyingCriteria.ismaltreatment24yrs ||
                this.sdm.disqualifyingCriteria.isactiveinvestigation ||
                // Discretionary Disqualifying Factors - this is still required to ensure correct precedence
                this.sdm.disqualifyingFactors.isreportedhistory ||
                this.sdm.disqualifyingFactors.ismultiple ||
                this.sdm.disqualifyingFactors.isdomesticvoilence ||
                this.sdm.disqualifyingFactors.iscriminalhistory ||
                this.sdm.disqualifyingFactors.isthread ||
                this.sdm.disqualifyingFactors.islawenforcement ||
                this.sdm.disqualifyingFactors.iscourtiinvestigation
            ) {
                this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
                this.isIRFlag = true;
            }
        }

        if (                                                              // ------------------------AR Conditions
            !this.isIRFlag &&
            (
            this.sdm.physicalAbuse.ismalpa_caregiver ||
            this.sdm.physicalAbuse.ismalpa_childtoxic ||
            this.sdm.physicalAbuse.ismalpa_injuryinconsistent ||
            this.sdm.physicalAbuse.ismalpa_insjury ||
            this.sdm.physicalAbuse.ismalpa_nonaccident ||
            this.sdm.generalNeglect.isneggn_inadequatefood ||
            this.sdm.arGeneralNeglect.isneggn_exposuretounsafe ||
            this.sdm.arGeneralNeglect.isneggn_inadequateclothing ||
            this.sdm.arGeneralNeglect.isneggn_inadequatesupervision ||
            this.sdm.arGeneralNeglect.isnegrh_treatmenthealthrisk ||
            this.sdm.generalNeglect.isneggn_childdischarged ||
            this.sdm.isnegfp_cargiverintervene ||
            this.sdm.isnegab_abandoned ||
            this.sdm.unattendedChild.isneguc_leftaloneinappropriatecare ||
            this.sdm.unattendedChild.isneguc_leftalonewithoutsupport ||
            this.sdm.unattendedChild.isneguc_leftunsupervised ||
            this.sdm.isnegmn_unreasonabledelay
            )
        ) {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-AR' }, { emitEvent: false });
        } else if (this.isIRFlag) {
            this.sdmFormGroup.patchValue({ cpsResponseType: 'CPS-IR' }, { emitEvent: false });
        } else {
            this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
        }
    }

    validateIRARScreenOut() {
        this.sdmFormGroup.patchValue({ cpsResponseType: null }, { emitEvent: false });
    }

    goToNextPage(pageToGo: string) {
        const pageId = $('#' + pageToGo);
        pageId.click();
        $('html,body').animate({ scrollTop: pageId.offset().top }, 'slow');
    }

    decideScreenInMessage() {
        const purpose = this.dataStoreService.getData('da_type');
        if (purpose === MyNewintakeConstants.PURPOSE.RISK_OF_HARM_INTAKE) {
            this.initialScreenInMessage = 'NON-CPS RISK OF HARM';
        } else {
            this.initialScreenInMessage = 'one or more maltreatment types are marked';
        }
    }

    setsearchprovider() {
        this.searchprovider.providerid = '';
        this.searchprovider.providernm = '';
        this.searchprovider.providerfname = '';
        this.searchprovider.providerlname = '';
    }

    searchProvider() {
        // this.providersList = [];
        this._commonHttpService.getArrayList(
            {
              method: 'post',
              nolimit: true,
            //   providercategorycd: this.searchprovider.providerid,
              providerid: this.searchprovider.providerid,
              providername: this.searchprovider.providernm,
              filter: {}
            },
            'providerreferral/providersearch'
        // )
        ).subscribe(providers => {
            console.log('Providers List' , providers );
            if (providers && providers.length) {
            this.providersList = providers;
            }
            this.setsearchprovider();
        });
    }

    selectProvider(provider: any) {
        // this.selectedProvider.push(provider);

        const provider1 = {providername : provider.provider_nm , providerid : provider.provider_id, providerphone : provider.adr_work_phone_tx};

        const providerControl = <FormArray>this.sdmFormGroup.controls.provider;
        if (providerControl) {
            const isProviderExist = providerControl.controls.find(item => item.get('providername').value === provider1.providername);
            console.log('Provider exist ' + isProviderExist);
            if (!isProviderExist) {
                providerControl.push(this.buildProviderForm(provider1));
            }

        }
        // const providerControl = <FormArray>this.sdmFormGroup.controls.provider;
        // if (this.provider) {
        //     this.provider.forEach((x) => {
        //         providerControl.push(this.buildProviderForm(x));
        //     });
        // }
    }

    onConfirmProvider() {
        if (this.selectedProvider) {
            (<any>$('#providersearch')).modal('hide');
            this.providersList = [];
            console.log('in selected provider');
        } else {
          this._alertService.warn('Please select any provider');
        }

    }

    resetOverrides(): any {
        this.sdmFormGroup.patchValue({
            scnRecommendOveride: '',
            isfinalscreenin: null,
            immediate: '',
        }, { emitEvent: false });

        // this.sdmFormGroup.controls['screenOut'].patchValue({
        //     isscrnoutrecovr_insufficient: false,
        //     isscrnoutrecovr_information: false,
        //     isscrnoutrecovr_historicalinformation: false,
        //     isscrnoutrecovr_otherspecify: false,
        //     scrnout_description: ''
        // }, { emitEvent: false });

        // this.sdmFormGroup.controls['screenIn'].patchValue({
        //     isscrninrecovr_courtorder: false,
        //     isscrninrecovr_otherspecify: false,
        //     scrnin_description: ''
        // }, { emitEvent: false });

        // this.sdmFormGroup.controls['immediateList'].patchValue({
        //     isimmed_childfaatility: false,
        //     isimmed_seriousinjury: false,
        //     isimmed_childleftalone: false,
        //     isimmed_allegation: false,
        //     isimmed_otherspecify: false,
        //     immediateList6: '',
        // }, { emitEvent: false });

        // this.sdmFormGroup.controls['noImmediateList'].patchValue({
        //     isnoimmed_physicalabuse: false,
        //     isnoimmed_sexualabuse: false,
        //     isnoimmed_neglectresponse: false,
        //     isnoimmed_mentalinjury: false,
        //     isnoimmed_screeninoverride: false,
        //     isnoimmed_substantial_risk: false,
        //     isnoimmed_risk_harm: false,
        // }, { emitEvent: false });

        this.sdmFormGroup.get('screenOut').reset();
        this.sdmFormGroup.get('screenIn').reset();
        this.sdmFormGroup.get('immediateList').reset();
        this.sdmFormGroup.get('noImmediateList').reset();
    }

    validateChildFatality(childfatality) {
        if (this.childFatality !== childfatality && childfatality === 'yes') {
            this._alertService.error('Please update child profile with valid D.O.D to proceed.');
            this.sdmFormGroup.patchValue({ childfatality: 'no' });
        } else if (this.childFatality !== childfatality && childfatality === 'no') {
            this._alertService.error('Child profile have a valid D.O.D please update child profile to proceed.');
            this.sdmFormGroup.patchValue({ childfatality: 'yes' });
        }
    }

    getInvolvedPerson() {
        let inputRequest: Object;
        inputRequest = {
            intakeserviceid: this.id
        };
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: inputRequest
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PersonList + '?filter'
            )
            .subscribe((data) => {
                this.involvedPersonList = data.data;

                const addedPersons = data.data;
                addedPersons.map((item) => {
                    item.fullName = item.firstname + ' ' + item.lastname;
                    item.roles.map((roleval) => {
                        if (roleval.intakeservicerequestpersontypekey === 'AV') {
                            this.sdmFormGroup.setControl('allegedvictim', this.formBuilder.array([]));
                            this.allegedVictim = this.allegedVictim && this.allegedVictim.length !== 0 ?
                                                    this.allegedVictim : [{ victimname: item.fullName }];
                            const avData = this.allegedVictim.filter((name) => name.victimname === item.fullName);
                            if (avData.length === 0) {
                                const allegedV = {
                                    victimname: item.fullName
                                };
                                this.allegedVictim.push(allegedV);
                            }
                            // this.sdmFormGroup.controls['allegedvictim'].disable();
                        }

                        if (roleval.intakeservicerequestpersontypekey === 'AM') {
                            this.sdmFormGroup.setControl('allegedmaltreator', this.formBuilder.array([]));
                            this.allegedMaltreator = this.allegedMaltreator && this.allegedMaltreator.length !== 0 ?
                                                        this.allegedMaltreator : [{ maltreatorsname: item.fullName }];
                            const amData = this.allegedMaltreator.filter((name) => name.maltreatorsname === item.fullName);
                            if (amData.length === 0) {
                                const allegedM = {
                                    maltreatorsname: item.fullName
                                };
                                this.allegedMaltreator.push(allegedM);
                            }
                            // this.sdmFormGroup.controls['allegedmaltreator'].disable();
                        }

                    });
                });

                const rcPerson = this.involvedPersonList.filter((person) => {
                    const roles = person.roles;
                    if (roles && Array.isArray(roles)) {
                        const valid = roles.some(role => ['CHILD', 'RC'].includes(role.intakeservicerequestpersontypekey));
                        return valid;
                    } else {
                        return false;
                    }
                });
                rcPerson.map((res) => {
                    if (res.dateofdeath) {
                        this.childFatality = 'yes';
                        this.sdmFormGroup.patchValue({ childfatality: this.childFatality });
                    }
                });
                this.childunderoneyear = false;
                const smallchild = this.involvedPersonList.filter((person) => {
                    const roles = person.roles;
                    if (roles && Array.isArray(roles)) {
                        const valid = roles.some(role => {
                            if (['CHILD', 'AV'].includes(role.intakeservicerequestpersontypekey)) { return role.intakeservicerequestpersontypekey; }
                        });

                        if (valid) {
                            const age = this.calculateAge(person.dob);
                            if (age >= 0 && age <= 1) {
                                this.childunderoneyear = true;
                            }
                        }
                        return valid;
                    } else {
                        return false;
                    }
                });

                this.setFormValues();
                this.sdmFormGroup.controls['allegedvictim'].disable();
                this.sdmFormGroup.controls['allegedmaltreator'].disable();
                this.updateSubstantialRisk();
            });
    }

    calculateAge(dob) {
        // const dob = this.selectedPerson.dob;
        let age = 0;
        if (dob && moment(new Date(dob), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dob), 'MM/DD/YYYY').toDate();
            age = moment().diff(rCDob, 'years');
        }
        return age;
    }

    validateRiskForm(event: any) {
        console.log(event);
        const sdmForm = this.sdmFormGroup.getRawValue();
        const disqualifyingCriteria = sdmForm.disqualifyingCriteria;
        const riskofHarm = sdmForm.riskofHarm;
        if (riskofHarm.isnegrh_exposednewborn || riskofHarm.isnegrh_priordeath) {
            if (disqualifyingCriteria.issexualabuse) {
                this.sdmFormGroup.get('disqualifyingCriteria').enable();
            } else {
                this.sdmFormGroup.get('disqualifyingCriteria').disable();
            }
        }
        if (riskofHarm.isnegrh_exposednewborn) {
            this.isSENFlag = true;
        } else {
            this.isSENFlag = false;
        }
    }

    validateCPSIRAR(sdm) {
        if (sdm.scnRecommendOveride !== '') {
            if (sdm.scnRecommendOveride === 'Ovrscrnin') {
                sdm.isir = true;
                sdm.isar = false;
            }
            if (sdm.scnRecommendOveride === 'OvrScrnout') {
                sdm.isir = false;
                sdm.isar = true;
            }
        } else {
            if (sdm.cpsResponseType === 'CPS-IR') {
                sdm.isir = true;
                sdm.isar = false;
            } else if (sdm.cpsResponseType === 'CPS-AR') {
                sdm.isir = false;
                sdm.isar = true;
            } else {
                sdm.isir = false;
                sdm.isar = false;
            }
        }
        if (this.isAcptNonCPS) {
            sdm.isir = false;
            sdm.isar = false;
        }

        if (this.isSENFlag) {
            sdm.isir = false;
            sdm.isar = false;
        }
    }

    clearAllImmediateSelections() {
        this.sdmFormGroup.get('noImmediateList').reset();
        // this.sdmFormGroup.get('noImmediateList').patchValue({
        //     isnoimmed_physicalabuse: [false],
        //     isnoimmed_sexualabuse: [false],
        //     isnoimmed_neglectresponse: [false],
        //     isnoimmed_mentalinjury: [false],
        //     isnoimmed_substantial_risk: [false],
        //     isnoimmed_risk_harm: [false]
        // });
    }

    setImmediates() {
        const sdmForm = this.sdmFormGroup.getRawValue();
        this.clearAllImmediateSelections();

        this.cpsResponseValidation(sdmForm);
        const noImmediateList = sdmForm.noImmediateList;

        if (noImmediateList.isnoimmed_screeninoverride) {
            this.sdmFormGroup.get('noImmediateList').get('isnoimmed_screeninoverride').disable();
        }
    }

    onMaltreatorChange(key, event) {
        const sdm = this.sdmFormGroup.getRawValue();
        if (event.checked) {
            this.sdmFormGroup.patchValue({
                isfcplacementsetting: false,
                isprivateplacement: false,
                islicenseddaycare: false,
                isschool: false,
            }, { emitEvent: false });
            this.sdmFormGroup.get(key).setValue(event.checked);
        }
    }
}
