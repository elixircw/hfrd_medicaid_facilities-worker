import { Component, OnInit, Input } from '@angular/core';
import { PersonDetail } from '../../_entities/involvedperson.data.model';
import { Observable, Subject } from 'rxjs/Rx';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'involved-person-view-person',
  templateUrl: './involved-person-view-person.component.html',
  styleUrls: ['./involved-person-view-person.component.scss']
})
export class InvolvedPersonViewPersonComponent implements OnInit {

    @Input() person: PersonDetail[] = [];
    @Input() person$ = new Subject<PersonDetail>();
  constructor() { }

  ngOnInit() {
  }

}
