import { Component, OnInit, Input } from '@angular/core';
import { PersonDetail } from '../../_entities/involvedperson.data.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'involved-person-view-dhs-action',
  templateUrl: './involved-person-view-dhs-action.component.html',
  styleUrls: ['./involved-person-view-dhs-action.component.scss']
})
export class InvolvedPersonViewDhsActionComponent implements OnInit {

    @Input() dhsAction: PersonDetail[] = [];
  constructor() { }

  ngOnInit() {
  }

}
