import { Component, OnInit, Input } from '@angular/core';
import { PersonDetail } from '../../_entities/involvedperson.data.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'involved-person-view-clearing',
  templateUrl: './involved-person-view-clearing.component.html',
  styleUrls: ['./involved-person-view-clearing.component.scss']
})
export class InvolvedPersonViewClearingComponent implements OnInit {

    @Input() clearing: PersonDetail[] = [];
  constructor() { }

  ngOnInit() {
  }

}
