import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsInvestigationPlanRoutingModule } from './as-investigation-plan-routing.module';
import { AsInvestigationPlanComponent } from './as-investigation-plan.component';
import { MatCardModule, MatCheckboxModule, MatDatepickerModule, MatExpansionModule,
   MatFormFieldModule, MatInputModule, MatListModule, MatNativeDateModule, MatRadioModule, MatSelectModule, MatTableModule, MatTabsModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { QuillModule } from 'ngx-quill';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    AsInvestigationPlanRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    QuillModule,
    FormsModule, ReactiveFormsModule
  ],
  declarations: [AsInvestigationPlanComponent]
})
export class AsInvestigationPlanModule { }
