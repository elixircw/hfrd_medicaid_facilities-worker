import { Location } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DataStoreService, CommonHttpService, AuthService, SessionStorageService } from '../../../@core/services';
import { Observable } from 'rxjs/Rx';
import { AppUser } from '../../../@core/entities/authDataModel';
import { AppConstants } from '../../../@core/common/constants';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../_entities/caseworker.data.constants';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dsds-action',
    templateUrl: './dsds-action.component.html',
    styleUrls: ['./dsds-action.component.scss']
})
export class DsdsActionComponent implements OnInit, AfterViewInit, OnDestroy {
    id: string;
    daNumber: string;
    tabList: any;
    selected: any;
    currentTab: string;
    tabList$: Observable<any[]>;
    roleId: AppUser;
    canDisplayChildRemoval$: Observable<boolean>;
    role: string;
    intakeCaseStore: any;
    appEvent: any;

    constructor(
        private _dataStoreService: DataStoreService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private ref: ChangeDetectorRef,
        private _authServie: AuthService,
        private _commonService: CommonHttpService,
        private _session: SessionStorageService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.intakeCaseStore = this._session.getObj('IntakeCaseStore');
        this._dataStoreService.setData('IntakeCaseStore', this.intakeCaseStore);
        this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        console.log('case uid', this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID));
        console.log('case number', this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER));
        this.appEvent = this._dataStoreService.getData('appevent');
    }

    ngOnInit() {
        this.roleId = this._authServie.getCurrentUser();
        this.role = this.roleId.role.name;
        this.loadTabs();
    }
    ngOnDestroy() {
        // commenting below two line ng destroy is calling on every other child tabs loading
       // this._session.removeItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
       // this._session.removeItem(CASE_STORE_CONSTANTS.CASE_TYPE);
        // this._dataStoreService.clearStore();
    }
    ngAfterViewInit() {
        const __this = this;
        $('.caseworker-tabs').scrollingTabs({
            disableScrollArrowsOnFullyScrolled: true,
            scrollToActiveTab: true,
            tabClickHandler: function (e) {
                $(this).addClass('active');
                __this.router.navigateByUrl($(this).attr('target'));
            }
        });
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

        this._dataStoreService.currentStore.subscribe((store) => {
            if (store['dsdsActionsSummary']) {
                const actionSummary = store['dsdsActionsSummary'];
                if (actionSummary.da_subtype === 'CPS-AR') {
                    $('#arCaseClosureTab').show();
                } else {
                    $('#arCaseClosureTab').hide();
                }
                // if (actionSummary.isenablekinship === 1) {
                //     $('#kinshipTab').show();
                // } else {
                //     $('#kinshipTab').hide();
                // }
            }
        });
        if (this._authServie.isDJS() && this.intakeCaseStore && this.intakeCaseStore.action === 'view') {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('i')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
            (<any>$('th a')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
        }
    }

    isActiveTab(item) {
        return this.selected === item;
    }
    loadTabs() {
        let agency = this._authServie.getCurrentUser().user.userprofile.teamtypekey;
      
        if(localStorage.getItem('iveagency')){
         let  iveagency = localStorage.getItem('iveagency')
            if (iveagency === 'IV-E'){
                if (this._authServie.getCurrentUser().user.userprofile.teamtypekey === 'IV-E'){
                    agency = 'CW'
                }
    }
            localStorage.removeItem('iveagency');
           
        }
        const actionSummary = this._dataStoreService.getData('dsdsActionsSummary');
        // this._dataStoreService.currentStore.subscribe((store) => {
        if (agency === 'DJS') {
            this.currentTab = 'Supervision Plan';
            this.tabList = [
                {
                    id: 'summaryTab', tabName: 'Summary', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/report-summary-djs', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'supervisionPlanTab', tabName: this.currentTab, route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-plan', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // TODO DJS New Person Module
                {
                    id: 'personsTab', tabName: 'Persons', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/involved-person', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // {
                //     id: 'personsTab', tabName: 'Persons', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/person', isActive: false,
                //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                //     screenKey: ''
                // },
                {
                    id: 'appointmentsTab', tabName: 'Appointments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/appointment', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'notesTab', tabName: 'Contacts', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/recording/djsnotes', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'assessmentsTab', tabName: 'Assessments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'placementTab', tabName: 'Placement', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/djs-placement', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'documentsTab', tabName: 'Documents', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'dispositionTab', tabName: 'Decision', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'leagalActionTab', tabName: 'Legal Action', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/legal-action-history', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'transport', tabName: 'Transport', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/transport', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'visitir', tabName: 'Visitor', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/visitor', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'courtactions', tabName: 'Court Actions', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/court-action', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'caseTimelineTab', tabName: 'Event Timeline', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/time-line-view-djs', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                }
            ];
            const actionSummary = this._dataStoreService.getData('dsdsActionsSummary');
            if (actionSummary && actionSummary.isrestitution) {
                const restitutionTab = {
                    id: 'restitution', tabName: 'Restitution', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/djs-restitution/payment-schedule', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR]
                };
                this.tabList.splice(this.tabList.length - 1, 0, restitutionTab);
                // this.tabList.push(paymentScheduleTab);
            }

        } else if (agency === 'AS') {
            this.currentTab = 'Check List';
            this.tabList = [
                {
                    id: 'summaryTab', tabName: 'Summary', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/report-summary', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'checkListTab', tabName: this.currentTab, route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-plan', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'personsTab', tabName: 'Persons', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/involved-person', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // { id: 'sdmTab', tabName: 'SDM', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sdm', isActive: false },
                {
                    id: 'adultScreenTab', tabName: 'Adult Screen', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/case-adult-screen-tool', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'notesTab', tabName: 'Contacts', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/as-recording/as-notes', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'crossReferencesTab', tabName: 'Cross References', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/cross-reference', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'cwExEntities', tabName: 'Entities', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/entities', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'adultassessmentsTab', tabName: 'Assessments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/adult-assessment', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'oas', tabName: 'Action Letters', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/as-oas', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // tslint:disable-next-line:max-line-length
                {
                    id: 'maltreatmentAllegationInfoTab',
                    tabName: 'Investigation Findings',
                    route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/as-maltreatment-information',
                    isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'monthlyIncome', tabName: 'Monthly Report', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/monthly-report', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'asInvestigationPlan', tabName: 'Maltreatment Allegation', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/as-invstgn-plan',
                    isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // tslint:disable-next-line:max-line-length
                // { id: 'investigationFindingsTab', tabName: 'Investigation Findings', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-findings-as', isActive: false },
                {
                    id: 'placementTab', tabName: 'Placement', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/adult-placement', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // { tabName: 'Allegations', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/allegation', isActive: false },
                {
                    id: 'attachmentsTab', tabName: 'Documents', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // { id: 'servicePlanTab', tabName: 'Service Plan', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/service-plan', isActive: false ,role:[] },
                {
                    id: 'servicePlanTab', tabName: 'Service Plan', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/as-service-plan', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'dispositionTab', tabName: 'Disposition', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'courtTab', tabName: 'Court', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/court/petition-detail', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                {
                    id: 'childRemovalTab', tabName: 'Child Removal', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action//child-removal/details', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // {
                //     id: 'referralsTab', tabName: 'Referrals', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/referral', isActive: false,
                //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                //     screenKey: ''
                // },
                {
                    id: 'referralsTab', tabName: 'Referrals', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/as-referral', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                },
                // { id: 'auditTrailTab', tabName: 'Audit Trail', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/history', isActive: false },
                {
                    id: 'caseTimelineTab', tabName: 'Case Timeline', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/time-line-view', isActive: false,
                    role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                    screenKey: ''
                }
            ];
        } else if (agency === 'CW') {
            const isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
            const caseType = this._session.getItem(CASE_STORE_CONSTANTS.CASE_TYPE);
            if (caseType) { this._dataStoreService.setData(CASE_STORE_CONSTANTS.CASE_TYPE, caseType); }
            if (isServiceCase &&   caseType !== 'ADOPTION') {
                this.currentTab = 'CheckList';
                this.tabList = [
                    // {
                    //     id: 'relationshipTab', tabName: 'Family', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/relationship', isActive: false,
                    //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    // },
                    {
                        id: 'summaryTab', tabName: 'Summary', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/report-summary', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-summary'
                    },
                    {
                        id: 'personsTab', tabName: 'Persons', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/person-cw', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    },
                   /*  {
                        id: 'collateralTab', tabName: 'Collateral', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/collateral', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    }, */
                     {
                        id: 'relationshipTab', tabName: 'Relationship', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/relationship', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'notesTab', tabName: 'Contacts', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/recording/notes', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'sdmTab', tabName: 'SDM', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sdm', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: 'caseworker-sdm'
                    },

                    {
                        id: 'checkListTab', tabName: this.currentTab, route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-plan', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    },
                    {
                        id: 'childRemovalTab', tabName: 'Child Removal', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/child-removal/details', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'placementTab', tabName: 'Placement', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-placements/list', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'permanencyPlan', tabName: 'permanency Plan', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-permanency-plan', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'socialHistoryTab', tabName: 'Social History', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/social-history', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-case-plan'
                    },
                    {
                        id: 'servicePlanTab', tabName: 'Services', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/service-plan/sc-gc', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'casePlanTab', tabName: 'Case Plan', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/case-plan', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-case-plan'
                    },
                    {
                        id: 'assignmentsTab', tabName: 'Assignments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/cw-assignments', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'assessmentsTab', tabName: 'Assessments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'attachmentsTab', tabName: 'Documents', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: ''
                    },
                    {
                         id: 'courtTab', tabName: 'Court', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/court/petition-detail', isActive: false,
                         role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                     },
                     {
                         id: 'participationTab', tabName: 'Participation', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/participation', isActive: false,
                         role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                     },
                     {
                         id: 'paytmentsTab', tabName: 'Payments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/payments', isActive: false,
                         role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                     },
                    {
                        id: 'dispositionTab', tabName: 'Decision', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    }
                ];

                if (actionSummary && Array.isArray(actionSummary.programarea) ){
                    const hasIHS = actionSummary.programarea.find(item => item.programkey === 'IHSFP');
                    const SATab = {
                        id: 'serviceAgreementTab',  tabName: 'Service Agreement',  route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/in-home-service/service-agreement',
                        isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR], screenKey: ''
                    };
                    if (hasIHS) {
                        this.tabList.splice(12, 0, SATab);
                    }
                }

            } else if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
                this.tabList = [
                    {
                        id: 'personsTab', tabName: 'Persons', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/adoption-persons', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-persons-screen'
                    },

                    // {
                    //     id: 'personsTab', tabName: 'Persons', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/person-cw', isActive: false,
                    //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                    //     screenKey: 'cw-persons-screen'
                    // },
                    {
                        id: 'notesTab', tabName: 'Contacts', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/recording/notes', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-contacts-screen'
                    },
                    // {
                    //     id: 'placementTab', tabName: 'Placement', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-placements/list', isActive: false,
                    //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                    //     screenKey: ''
                    // },
                    // {
                    //     id: 'permanencyPlan', tabName: 'permanency Plan', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-permanency-plan', isActive: false,
                    //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                    //     screenKey: ''
                    // },
                    // {
                    //     id: 'Subsidy', tabName: 'Subsidy', route: '/pages/case-worker/' + this.id + '/' + this.daNumber +
                    //     '/dsds-action/placement/adoption/adoption-subsidy/agreement', isActive: false,
                    //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                    //     screenKey: ''
                    // },

                    {
                        id: 'assessmentsTab', tabName: 'Assessments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'servicePlanTab', tabName: 'Services', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/service-plan/sc-gc', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'paymentHistory', tabName: 'Payments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/payment-history', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'attachmentsTab', tabName: 'Documents', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'agreementDocsTab', tabName: 'Agreement Documents',route: '/pages/case-worker/' + this.id + '/' + this.daNumber +
                        '/dsds-action/placement/adoption/adoption-subsidy/agreement', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'assignmentsTab', tabName: 'Assignments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/cw-assignments', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'narrative', tabName: 'Narrative', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/narrative', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-summary'
                    },
                    {
                        id: 'dispositionTab', tabName: 'Decision', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-disposition-screen'
                    }
                ];
            } else if (actionSummary && (actionSummary.da_subtype === 'IHS') && actionSummary.teamtypekey === 'CW') {
                this.currentTab = 'CheckList';
                this.tabList = [
                    {
                        id: 'summaryTab', tabName: 'Summary', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/report-summary', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-summary-screen'
                    },
                    {
                        id: 'personsTab', tabName: 'Household', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/person-cw', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    },
                   /*  {
                        id: 'collateralTab', tabName: 'Collateral', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/collateral', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    }, */
                    {
                        id: 'relationshipTab', tabName: 'Relationship', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/relationship', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'checkListTab', tabName: this.currentTab, route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-plan', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    },
                    {
                        id: 'notesTab', tabName: 'Contacts', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/recording/notes', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-contacts-screen'
                    },
                    {
                        id: 'assessmentsTab', tabName: 'Assessments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'attachmentsTab', tabName: 'Documents', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-attachment-screen'
                    },
                    {
                        id: 'servicePlanTab', tabName: 'Service Plan', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/service-plan', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-service-plan'
                    },
                    {
                        id: 'serviceAgreementTab',  tabName: 'Service Agreement',  route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/in-home-service/service-agreement',
                        isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR], screenKey: ''
                    },
                    {
                        id: 'childRemovalTab', tabName: 'Child Removal', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/child-removal/details', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    },
                    {
                        id: 'courtTab', tabName: 'Court', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/court/petition-detail', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-court-screen'
                    },
                    {
                        id: 'dispositionTab', tabName: 'Disposition', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-disposition-screen'
                    },
                    {
                        id: 'caseTimelineTab', tabName: 'Case Timeline', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/time-line-view', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-case-timeline-screen'
                    }

                ];
            } else {
                this.currentTab = 'CheckList';
                this.tabList = [
                    {
                        id: 'summaryTab', tabName: 'Summary', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/report-summary', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-summary'
                    },
                    {
                        id: 'checkListTab', tabName: this.currentTab, route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-plan', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-checklist'
                    },
                    {
                        id: 'personsTab', tabName: 'Persons', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/person-cw', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-persons'
                    },
                  /*    {
                        id: 'collateralTab', tabName: 'Collateral', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/collateral', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    }, */
                    {
                        id: 'relationshipTab', tabName: 'Relationship', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/relationship', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'notesTab', tabName: 'Contacts', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/recording/notes', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-contacts'
                    },
                    {
                        id: 'sdmTab', tabName: 'SDM', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sdm', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: 'caseworker-sdm'
                    },
                    {
                        id: 'assignmentsTab', tabName: 'Assignments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/cw-assignments', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR],
                        screenKey: ''
                    },
                    {
                        id: 'assessmentsTab', tabName: 'Assessments', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-assessment'
                    },
                    {
                        id: 'childRemovalTab', tabName: 'Child Removal', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/child-removal/details', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-child-removal'
                    },
                    // {
                    //     id: 'fosterCareTab', tabName: 'Foster Care Placement', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/foster-care', isActive: false,
                    //     role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    // },
                    // {
                    //     id: 'placementTab', tabName: 'Out of Home Placement', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement/permanency-plan',
                    //     isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR]
                    // },
                    /* {
                        id: 'placementTab', tabName: 'Placement', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/foster-care', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-placement-screen'
                    },
                    {
                        id: 'kinshipTab', tabName: 'Kinship', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/kinship-placement/kinship-assessment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'cw-kinship-screen'
                    }, */
                    {
                        id: 'courtTab', tabName: 'Court', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/court/petition-detail', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-court'
                    },
                    {
                        id: 'attachmentsTab', tabName: 'Documents', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-attachments'
                    },
                    {
                        id: 'servicePlanTab', tabName: 'Services', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/service-plan/sc-gc', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-service-plan'
                    },
                    {
                        id: 'dispositionTab', tabName: 'Decision', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-disposition'
                    },
                    {
                        id: 'caseTimelineTab', tabName: 'Case Timeline', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/time-line-view', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                        screenKey: 'caseworker-case-timeline'
                    }


                    // { id: 'crossReferencesTab', tabName: 'Cross References', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/cross-reference', isActive: false },
                    // // { tabName: 'Providers / External Entities', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/involved-entity', isActive: false },
                    // { id: 'safetyPlanTab', tabName: 'Safety Plan', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/plan/safety-plan', isActive: false },
                    // // { tabName: 'Allegations', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/allegation', isActive: false },
                    // { id: 'arCaseClosureTab', tabName: 'AR Case Closure', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/ar-case-closure', isActive: false },
                    // { id: 'referralsTab', tabName: 'Referrals', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/referral', isActive: false },
                    // { id: 'auditTrailTab', tabName: 'Audit Trail', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/history', isActive: false },
                    // { id: 'ohplacements', tabName: 'OH Placements', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/oh-placement', isActive: false },
                   /*  {
                        id: 'kinshipCareTab', tabName: 'Kinship Care', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/kinship', isActive: false,
                        role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR],
                         screenKey: 'caseworker-kinship'
                    }, */
                ];
            }

            if (actionSummary && (actionSummary.da_subtype === 'CPS-IR' || actionSummary.da_subtype === 'CPS-AR')) {
                const MaltreatmentAllegationTab = {
                    id: 'maltreatmentAllegationInfoTab',
                    tabName: 'Maltreatment Allegation',
                   screenKey: 'caseworker-maltreatment-allegation',
                    route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/maltreatment-information',
                    isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR]

                };
                this.tabList.splice(6, 0, MaltreatmentAllegationTab);
                if ( actionSummary && actionSummary.da_subtype === 'CPS-IR') {
                const InvestigationFindingTab = {
                    id: 'investigationFindingsTab',
                    tabName: 'Investigation Findings',
                    screenKey: 'caseworker-investigation-findings',
                    route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-findings',
                    isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR]
                };
                this.tabList.splice(7, 0, InvestigationFindingTab);
            }


            if ( actionSummary && actionSummary.da_subtype === 'CPS-AR') {
            const AlternativeResponseSummaryTab = {
                id: 'alternativeResponseSummaryTab',
                tabName: 'AR Summary',
                route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/alternative-response-summary',

                isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR]
            };
            this.tabList.splice(7, 0, AlternativeResponseSummaryTab);
          }

        }
        if (actionSummary && actionSummary.service_old_id) {
            const prTab = {
                id: 'progressReviewTab', tabName: 'Progress Review', route: '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/in-home-service/progress-review',
                isActive: false, role: [AppConstants.ROLES.CASE_WORKER, AppConstants.ROLES.SUPERVISOR, AppConstants.ROLES.KINSHIP_SUPERVISOR], screenKey: ''
            };
            this.tabList.push(prTab);
        }
    }
        const seletectedTab = this.tabList.filter((element) => {
            return this.location.path().search(element.route) !== -1;
        });
        if (seletectedTab.length) {
            this.selected = seletectedTab[0];
        } else {
            this.selected = this.tabList[0];
            const appEvents = ['GAARR', 'GAAR', 'GADR', 'GAYR', 'GASR', 'ADPR', 'ASAR', 'ABLR'];
            if (appEvents.indexOf(this.appEvent) !== -1 ) {
                this.selected = this.tabList[6];
            }

        }
        if (agency !== 'CW') {
            const index = this.tabList.findIndex((tab) => tab.tabName === 'Child Removal');
            if (index > 0) {
                this.tabList.splice(index, 1);
            }
        }
        this.selected.isActive = true;
        this.tabList$ = Observable.of(this.tabList);
        this.ref.markForCheck();
    }

    getAgencyKey() { }
}
