import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceCasePlacementsRoutingModule } from './service-case-placements-routing.module';
import { ServiceCasePlacementsComponent } from './service-case-placements.component';
import { ServiceCasePlacementsResolverService } from './service-case-placements-resolver.service';
import { ServiceCasePlacementsService } from './service-case-placements.service';
import { RemovedChildListComponent } from './removed-child-list/removed-child-list.component';
import { SelectedChildListComponent } from './selected-child-list/selected-child-list.component';
import { PlacmentWrapperComponent } from './placment-wrapper/placment-wrapper.component';
import { LivingArrangementFormComponent } from './placment-wrapper/living-arrangement-form/living-arrangement-form.component';
import { PlacementReferralComponent } from './placment-wrapper/placement-referral/placement-referral.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { PlacementListComponent } from './placement-list/placement-list.component';
import { PlacementDetailsComponent } from './placement-details/placement-details.component';
import { LivingArrangmentDetailsComponent } from './placement-details/living-arrangment-details/living-arrangment-details.component';
import { ProviderPlacmentDetailsComponent } from './placement-details/provider-placment-details/provider-placment-details.component';
import { NgxMaskModule } from 'ngx-mask';

import { PaginationModule } from 'ngx-bootstrap';
import { PlacementAddressInfoComponent } from './placement-address-info/placement-address-info.component';
import { MatMenuModule, MatListModule, MatTooltipModule } from '@angular/material';
import { ExitPlacementsComponent } from './exit-placements/exit-placements.component';
import { ExitPlacementService } from './exit-placements/exit-placement.service';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';

import { AgmCoreModule } from '@agm/core';
import { VoidPlacementFormComponent } from './void-placement-form/void-placement-form.component';
import { FiscalAuditModule } from '../../../finance/fiscal-audit/fiscal-audit.module';
import { FinanceService } from '../../../finance/finance.service';
@NgModule({
  imports: [
    CommonModule,
    ServiceCasePlacementsRoutingModule,
    FormMaterialModule,
    MatMenuModule,
    MatTooltipModule,
    NgxMaskModule.forRoot(),
    PaginationModule,
    MatListModule,
    SharedPipesModule,
    FiscalAuditModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8CRCgWX0eRvC8XfFnaaMPm-36fb2GN9M'
    })
  ],
  declarations: [ServiceCasePlacementsComponent,
    RemovedChildListComponent,
    SelectedChildListComponent,
    PlacmentWrapperComponent,
    LivingArrangementFormComponent,
    PlacementReferralComponent,
    PlacementListComponent,
    PlacementDetailsComponent,
    LivingArrangmentDetailsComponent,
    ProviderPlacmentDetailsComponent,
    PlacementAddressInfoComponent,
    ExitPlacementsComponent,
    VoidPlacementFormComponent],
  providers: [ServiceCasePlacementsResolverService,
    ServiceCasePlacementsService,
    ExitPlacementService,
    FinanceService]
})
export class ServiceCasePlacementsModule { }
