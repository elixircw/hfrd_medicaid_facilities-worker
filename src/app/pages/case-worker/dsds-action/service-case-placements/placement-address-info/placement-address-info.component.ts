import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'placement-address-info',
  templateUrl: './placement-address-info.component.html',
  styleUrls: ['./placement-address-info.component.scss']
})
export class PlacementAddressInfoComponent implements OnInit {

  @Input() placement: any;
  constructor() { }

  ngOnInit() {
  }

}
