import { Component, OnInit } from '@angular/core';
import { ExitPlacementService } from './exit-placement.service';
import { FormGroup } from '@angular/forms';
import { PlacementConstants } from '../constants';

@Component({
  selector: 'exit-placements',
  templateUrl: './exit-placements.component.html',
  styleUrls: ['./exit-placements.component.scss']
})
export class ExitPlacementsComponent implements OnInit {

  exitTypes = [];
  reasonsForExit = [];
  exitPlacementForm: FormGroup;
  reasonForExitRequired = false;
  exitMinDate:any;
  constructor(private exitService: ExitPlacementService) {
    this.exitPlacementForm = this.exitService.getFormInstance();
  }

  ngOnInit() {
    this.loadDropDowns();
    this.exitPlacementForm.get('exitreasontypekey').disable();
    this.exitMinDate = this.exitService.minExitDate;
  }

  loadDropDowns() {
    this.exitService.getExitTypes().subscribe(result => {
      if (result && result.length) {
        this.exitTypes = result;
      }

    });
  }

  onExitTypeChange() {
    const exitTypeKey = this.exitPlacementForm.getRawValue().exittypekey;
    if (exitTypeKey !== PlacementConstants.EXIT_TYPES.CHANGE_IN_PLACEMENT_STR) {
      this.reasonForExitRequired = true;
      this.exitPlacementForm.patchValue({
        exitreasontypekey : null
      })
      this.exitPlacementForm.get('exitreasontypekey').enable();
      this.exitService.getReasonForExit(exitTypeKey).subscribe(result => {
        if (result && result.length) {
          this.reasonsForExit = result;
        }
      });
    } else {
      this.reasonForExitRequired = false;
      this.exitPlacementForm.get('exitreasontypekey').disable();
      this.reasonsForExit = [];
    }

  }

}
