import { Injectable } from '@angular/core';
import { CommonControlsModule } from '../../../../../shared/modules/common-controls/common-controls.module';
import { CommonDropdownsService } from '../../../../../@core/services';
import { ServiceCasePlacementsService } from '../service-case-placements.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Injectable()
export class ExitPlacementService {


  exitPlacementForm: FormGroup;
  voidPlacementForm: FormGroup;
  minExitDate: any;

  constructor(private _commonDropDownService: CommonDropdownsService,
    private formBuilder: FormBuilder) {

  }

  getExitTypes() {
    return this._commonDropDownService.getDropownsByTable('placement_exit_type');
  }

  getReasonForExit(exitType) {
    return this._commonDropDownService.getDropownsByTable(exitType);
  }

  getVoidReasonTypes() {
    return this._commonDropDownService.getListByTableID(82);
  }

  initializeExitForm() {
    this.exitPlacementForm = this.formBuilder.group({
      exittypekey: [null],
      exitreasontypekey: [null,Validators.required],
      remarks: [null],
      enddate: [null, Validators.required],
      endtime: [null, Validators.required],
    });
  }

  initializeVoidForm() {
    this.voidPlacementForm = this.formBuilder.group({
      voidreasontypekey: [null],
      voidremarks: [null],
      voiddate: [null]
    });
  }

  getFormInstance() {
    this.initializeExitForm();
    return this.exitPlacementForm;
  }

  getVoidPlacementFormInstance() {
    this.initializeVoidForm();
    return this.voidPlacementForm;
  }




}
