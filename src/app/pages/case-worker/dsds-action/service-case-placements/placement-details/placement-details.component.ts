import { Component, OnInit } from '@angular/core';
import { ServiceCasePlacementsService } from '../service-case-placements.service';
import { AuthService, AlertService, CommonHttpService, DataStoreService } from '../../../../../@core/services';
import { AppConstants } from '../../../../../@core/common/constants';
import { ActivatedRoute, Router } from '@angular/router';
import { PlacementConstants } from '../constants';
import { ExitPlacementService } from '../exit-placements/exit-placement.service';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'placement-details',
  templateUrl: './placement-details.component.html',
  styleUrls: ['./placement-details.component.scss']
})
export class PlacementDetailsComponent implements OnInit {

  children = [];
  cpaHomesHistory = [];
  isSupervisor = false;
  action: string;
  Reason: '';
  updateButton = false;
  exiting = false;
  updateId: '';
  approval= true;
  CPAHomeList = [];
  exitTypeCodeList = [];
  exitReasonCodeList = [];
  addCPAHomeFormGroup: FormGroup;
  cpaProviderAddress= '';
  cpaProviderId= '';
  providerDetailsShow= false;
  onExitPlacement = false;
  isSubmitting: boolean;
  onEditPlacement = false;
  onVoidPlacement = false;
  onReview = false;
  ApproveorReturntowork= 'Yes';
  startDate = '';
  startTime = '';

  isCompleted = false;

  isLivingArrangement = false;

  placementCheck: any[];
  
  minDate: Date;
  minEntryDate: Date;
  placementid: string;
  alternatePlacementId: string;
  contractprogramid: string;
  
  inHome= false;
  updating=false;
  currentHome: any;
  exitFields = true;
  isDOCExit = false;

  constructor(private _service: ServiceCasePlacementsService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private _exitPlacementService: ExitPlacementService,
    private _commonHttpService: CommonHttpService,
    private _datastore: DataStoreService,
    private formBuilder: FormBuilder) {
    this.action = this.route.snapshot.paramMap.get('action');
    console.log('action', this.action);
    this.isSubmitting = false;
    if (this.action === PlacementConstants.ACTIONS.EXIT) {
      this.onExitPlacement = true;
    } else {
      this.onExitPlacement = false;
    }

    if (this.action === PlacementConstants.ACTIONS.REVIEW) {
      this.onReview = true;
    } else {
      this.onReview = false;
    }

    if (this.action === PlacementConstants.ACTIONS.EDIT) {
      this.onEditPlacement = true;
    } else {
      this.onEditPlacement = false;
    }

    if (this.action === PlacementConstants.ACTIONS.VOID) {
      this.onVoidPlacement = true;
    } else {
      this.onVoidPlacement = false;
    }
  }

  ngOnInit() {
    // this.children = this._service.selectedChildren;
    this.children = this._service.placementDetails;
    this.initializeCPAHomeFormGroup();
    this.ApproveorReturntowork = 'Yes';
    if (!this.children || !this.children.length) {
      this.goBack();
    } 
    else {
      if(this.action === PlacementConstants.ACTIONS.ADD) {
        if (this.children && this.children.length && this.children[0].placement) {
          this.placementid = this.children[0].placement.placementid;
          this.contractprogramid = this.children[0].placement.contractprogramid;
          console.log("CONTRACT ID", this.contractprogramid);
        }
        (<any>$('#addCPAHome')).modal('show');
        this.getProviderList();
        this.getCodes();
        this.getCPAHomesHistory();
        
        this.currentHome = this.cpaHomesHistory.find(prevHome => prevHome.exit_dt == null);
        if(this.currentHome)
        {
          console.log("Current home", this.currentHome);
          this.inHome = true;
          this.exitFields = true;
          this.viewCpaHome(this.currentHome);
        }
        else{
          this.inHome = false; //add new
          this.exitFields = false;
          this.addCPAHomeFormGroup.reset();
        }
      }
      else {
      (<any>$('#placementDetails')).modal('show');
      if (this.action === PlacementConstants.ACTIONS.EDIT) {
      if (this.children && this.children.length && this.children[0].placement && this.children[0].placement.startdate ) {
        this.isLivingArrangement = this.children[0].placement.placementtypekey === 'LA';
        this.startDate = this.children[0].placement.startdate;
        }
      if (this.children && this.children.length && this.children[0].placement && this.children[0].placement.starttime ) {
          this.startTime = this.children[0].placement.starttime;
        }
      }
      if (this.children && this.children.length && this.children[0].placement) {
        this._exitPlacementService.minExitDate = this.children[0].placement.startdate;
      }
      
      this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
      this._service.placementApprovalQueue$.subscribe(data => {
        this._alertService.success(data, true);
        this.isCompleted = true;
        if (data === 'Permanency Plan Approved Successfully' || data === 'Permanency Plan Rejected Successfully' || data === 'Placement approved successfully' ||
        data === 'Placement returned to the worker successfully.') {
          this.goBack();
          }
      });
      const childplacementobj = this.children[0];
      const childremovalobj = this._service.removedChildList.find(item => item.personid === childplacementobj.personid);
      this.validatePlacementDates(childremovalobj);
    }
    }

  }

  initializeCPAHomeFormGroup(){
    //console.log("INIT CPA FORM: children", this.children);
    if (Array.isArray(this.children) && this.children.length) {
      this.minEntryDate = this.children[0].placement.startdate;
      //console.log("MINENTRY START DATE",this.minEntryDate);  
    }
      this.addCPAHomeFormGroup = this.formBuilder.group({
        providerId: [],
        providerName: [],
        entryDate: [],
        entryTime: [],
        exitDate: [],
        exitTime: [],
        cpaHomeAddress: [],
        exitTypeCode: [],
        exitReasonCode: [],
    });

  }
  goBack() {
    this._service.getChildRemovalInfoAndPlacements().subscribe(response => {
      console.log('reload', response);
      this._service.broadCastPageRefresh();
      if(this.action === PlacementConstants.ACTIONS.ADD){
        (<any>$('#addCPAHome')).modal('hide');
      }
      else{
        (<any>$('#placementDetails')).modal('hide');
      }
      this.isSubmitting = false;
      if (this.isDOCExit){
        (<any>$('#docalert')).modal('show');
      } else {
        this.router.navigate(['../../list'], { relativeTo: this.route });
      }
    });
  }

  acknowledgeDOC() {
    this.isDOCExit = false;
    (<any>$('#docalert')).modal('hide');
    this.router.navigate(['../../list'], { relativeTo: this.route });
  }

  getCodes(){
    this._exitPlacementService.getExitTypes().subscribe(result => {
      if(result && result.length){
        this.exitTypeCodeList = result;
      }
    });
  }

  onExitTypeChange() {
    const exitTypeKey = this.addCPAHomeFormGroup.getRawValue().exitTypeCode;
      this._exitPlacementService.getReasonForExit(exitTypeKey).subscribe(result => {
        if (result && result.length) {
          this.exitReasonCodeList = result;
        }
      });
  }

  getProviderList() {
    this._commonHttpService.getArrayList(
      {
        where: {
          //program_id: '3714'// for testing on localhost
          program_id: this.contractprogramid 
        },
        method: 'get'
      },
      'programfacility/getprogramfacilitydetials?filter'
    ).subscribe(response => {
      if (response && Array.isArray(response) && response.length) {
        if (response[0].getprogramfacilitydetials && Array.isArray(response[0].getprogramfacilitydetials) && response[0].getprogramfacilitydetials.length)
        // tslint:disable-next-line: one-line
        {
          this.CPAHomeList = response[0].getprogramfacilitydetials;
          //console.log("CPA HOME LIST", this.CPAHomeList);
        }
      }
    });
  }
  
  getCPAHomesHistory(){
    this.alternatePlacementId = this.children[0].placement.alternateid;
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where : {
          placement_id : this.alternatePlacementId
        }
      },
      'tb_placement_cpa_homes?filter'
    ).subscribe(response => {
      if (response && Array.isArray(response) && response.length) {
        this.cpaHomesHistory = response;
        response.forEach(cpahome => {
          if(cpahome.provider_id){
              this._commonHttpService.getArrayList(
                {
                  method: 'get',
                  where: { provider_id: cpahome.provider_id }
                },
                'tb_provider?filter'
              ).subscribe(response => {
                if (response && response.length) {
                  cpahome['providername'] = this.getProviderFullName(response[0]);
                }
              });
            } 
          }
        )
        }
      }
    );
  }

  getProviderFullName(providerInfo){
    if(providerInfo){
      return '' + 
      (providerInfo.provider_prefix_cd?providerInfo.provider_prefix_cd+' ':'') + 
      (providerInfo.provider_first_nm?providerInfo.provider_first_nm+' ':'') + 
      (providerInfo.provider_middle_nm?providerInfo.provider_middle_nm+' ':'') + 
      (providerInfo.provider_last_nm?providerInfo.provider_last_nm+' ':'') + 
      (providerInfo.provider_suffix_cd?providerInfo.provider_suffix_cd+' ':'') ;
    }
    return '';

  }
  approve() {
    this._service.startPlacementApprovalInQueue();
    (<any>$('#placementDetails')).modal('hide');
    (<any>$('#placement-approval-request')).modal('hide');
  }

  placementCheckError() {
    if (!this.children[0].placement.voiddate) {
      this._commonHttpService.endpointUrl = 'placement/placementAutoValidation';
      const placement = this.children[0].placement;
      const revisionupdate = (placement) ? placement.revisionupdate : null;
      const model = {
        placementid: placement.alternateid,
        startdate: (revisionupdate) ? revisionupdate.entrydate : ((placement) ? placement.startdate : null),
        enddate: (revisionupdate) ? revisionupdate.enddate : null,
        update_sw: 'P',
        isbefore: false
      };
      this._commonHttpService.create(model).subscribe(
      (response) => {});
    }
  }

  cpaSelect(cpaHomeSelected){
    const home = this.CPAHomeList.find(elem => elem.provider_id == cpaHomeSelected);
    console.log(home);
    this.cpaProviderId = home.provider_id;
    this.cpaProviderAddress = home.provideraddress;
    this.providerDetailsShow = true;
    // this.addCPAHomeFormGroup.patchValue({
    //   providerId : home.provider_id,
    //   cpaHomeAddress: home.provideraddress
    // });
    this.currentHome = home;
  }

  // Date and time
  convertDateTimeToTimestamp(date, time) {
    if(moment(date).isValid() && time){
      return moment(moment(date).format('MM/DD/YYYY') + ' ' + time).format();
    }
    else
      return null;
  }

  cpaHomeSave(){
    if(!this.inHome && !this.updating){ //new
      this._commonHttpService.create(
        {
            placement_id: this.alternatePlacementId,
            //provider_id: this.addCPAHomeFormGroup.controls['providerId'].value,
            provider_id: this.cpaProviderId,
            entry_dt:this.addCPAHomeFormGroup.controls['entryDate'].value,
            entry_tm:this.convertDateTimeToTimestamp(this.addCPAHomeFormGroup.controls['entryDate'].value, this.addCPAHomeFormGroup.controls['entryTime'].value),
            exit_dt:this.addCPAHomeFormGroup.controls['exitDate'].value,
            exit_tm:this.convertDateTimeToTimestamp(this.addCPAHomeFormGroup.controls['exitDate'].value, this.addCPAHomeFormGroup.controls['exitTime'].value),
            exit_type_cd:this.addCPAHomeFormGroup.controls['exitTypeCode'].value,
            exit_reason_cd:this.addCPAHomeFormGroup.controls['exitReasonCode'].value
        },
        'tb_placement_cpa_homes/cpahomeplacementadd'
      ).subscribe(
        (result) => {
          console.log("CPA HOME ADDED",result);
          this.addCPAHomeFormGroup.reset();
          this.getCPAHomesHistory();
          this._alertService.success('CPA Home Saved Successfully');
          this.inHome = true;
          this.updating = false;
          this.exitFields = true;
          this.viewCpaHome(this.currentHome);
        },
        (error) => {
          console.log(error);
        }
      );
    }
    else if(this.inHome && this.updating){ //exiting ready= exit options on and others disabled.
      this._commonHttpService.patch( this.updateId,
        {
            placement_id: this.alternatePlacementId,
            //provider_id: this.addCPAHomeFormGroup.controls['providerId'].value,
            provider_id: this.cpaProviderId,

            entry_dt:this.addCPAHomeFormGroup.controls['entryDate'].value,
            entry_tm:this.convertDateTimeToTimestamp(this.addCPAHomeFormGroup.controls['entryDate'].value, this.addCPAHomeFormGroup.controls['entryTime'].value),
            exit_dt:this.addCPAHomeFormGroup.controls['exitDate'].value,
            exit_tm:this.convertDateTimeToTimestamp(this.addCPAHomeFormGroup.controls['exitDate'].value, this.addCPAHomeFormGroup.controls['exitTime'].value),
            exit_type_cd:this.addCPAHomeFormGroup.controls['exitTypeCode'].value,
            exit_reason_cd:this.addCPAHomeFormGroup.controls['exitReasonCode'].value
        },
        'tb_placement_cpa_homes'
      ).subscribe(
        (result) => {
          console.log("CPA HOME EXITED",result);
          this.addCPAHomeFormGroup.reset();
          this.getCPAHomesHistory();
          this._alertService.success('CPA Home Exited Successfully');
          this.inHome = false;
          this.updating = false;
          this.exitFields = this.currentHome.exit_dt? true: false;
        },
        (error) => {
          console.log(error);
        }
      );
    }
    else if(this.updating){// updating existing
      this._commonHttpService.patch( this.updateId,
        {
            placement_id: this.alternatePlacementId,
            provider_id: this.cpaProviderId,
            // provider_id: this.addCPAHomeFormGroup.controls['providerId'].value,
            entry_dt:this.addCPAHomeFormGroup.controls['entryDate'].value,
            entry_tm:this.convertDateTimeToTimestamp(this.addCPAHomeFormGroup.controls['entryDate'].value, this.addCPAHomeFormGroup.controls['entryTime'].value),
            exit_dt:this.addCPAHomeFormGroup.controls['exitDate'].value,
            exit_tm:this.convertDateTimeToTimestamp(this.addCPAHomeFormGroup.controls['exitDate'].value, this.addCPAHomeFormGroup.controls['exitTime'].value),
            exit_type_cd:this.addCPAHomeFormGroup.controls['exitTypeCode'].value,
            exit_reason_cd:this.addCPAHomeFormGroup.controls['exitReasonCode'].value
        },
        'tb_placement_cpa_homes'
      ).subscribe(
        (result) => {
          console.log("CPA HOME UPDATED",result);
          this.addCPAHomeFormGroup.reset();
          this.getCPAHomesHistory();
          this.updating = false;
          this._alertService.success('CPA Home Updated Successfully');

        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  viewCpaHome(cpaHome){
    this.updating = false;
    this.updateId = cpaHome.placement_cpa_home_id;
    if(cpaHome.exit_dt)
    {
      this.exitFields = true;
    }
    else{
      this.exitFields = false;
    }
    const home = this.CPAHomeList.find(elem => elem.provider_id == cpaHome.provider_id);
    this.patchCPAHome(cpaHome, home);
    
  }

  editCpaHome(cpaHome){    
    this.updating = true;
    this.updateId = cpaHome.placement_cpa_home_id;
    // this.currentHome = cpaHome;

    const home = this.CPAHomeList.find(elem => elem.provider_id == cpaHome.provider_id);
    if(cpaHome.exit_dt)
    {
      this.exitFields = true;
    }
    else{
      this.exitFields = false;
    }
    this.patchCPAHome(cpaHome, home);
  }

  patchCPAHome(cpaHome, home){
    this.addCPAHomeFormGroup.patchValue({
      providerId : cpaHome.provider_id,
      providerName : home.provider_id,
      cpaHomeAddress: home.provideraddress,
      entryDate : cpaHome.entry_dt,
      entryTime : moment(cpaHome.entry_tm).format("HH:mm:ss"),
      exitDate : cpaHome.exit_dt,
      exitTime : moment(cpaHome.exit_tm).format("HH:mm:ss"),
      exitTypeCode : cpaHome.exit_type_cd,
      exitReasonCode : cpaHome.exit_reason_cd
    });
  }
  exitCpaHome(cpaHome){
    this.updating =true;
    this.exitFields = true;
    this.updateId = cpaHome.placement_cpa_home_id;
    this.currentHome = cpaHome;

    const home = this.CPAHomeList.find(elem => elem.provider_id == cpaHome.provider_id);    
    this.patchCPAHome(cpaHome, home);
  }

  approveRequest() {
    if (!this.children[0].placement.voiddate) {
      this._commonHttpService.endpointUrl = 'placement/placementAutoValidation';
      const placement = this.children[0].placement;
      const revisionupdate = (placement) ? placement.revisionupdate : null;
      const model = {
        placementid: placement.alternateid,
        startdate: (revisionupdate) ? revisionupdate.entrydate : ((placement) ? placement.startdate : null),
        enddate: (revisionupdate) ? revisionupdate.enddate : null,
        update_sw: 'P',
        isbefore: true,
        servicecaseid: this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID)
      };
      this._commonHttpService.create(model).subscribe(
      (response) => {
          if (response && response.length > 0) {
             this.placementCheck = response;
             (<any>$('#placementDetails')).modal('hide');
             (<any>$('#placement-approval-check')).modal('show');
          } else {
          // (<any>$('#placementDetails')).modal('hide');
          this._service.approveorRejectPlacementasSupervisor(this.children[0].placement.placementid, 'Approved', this.Reason, this.children[0]);
          // this._service.startPlacementApprovalInQueue();
        }
      });
    } else {
      (<any>$('#placementDetails')).modal('hide');
      this._service.startPlacementApprovalInQueue();
      this.placementCheckError();
    }
  }
  approveOrReturn() {
    if (this.ApproveorReturntowork === 'No') {
      this.reject();
    } else {
      this.approveRequest();
    }
  }

  reject() {
    // this._service.startPlacementRejectionInQueue();
    if (!this.Reason) {
      this._alertService.error('Please update the reason for return to worker.');
      return;
    }
    this._service.approveorRejectPlacementasSupervisor(this.children[0].placement.placementid, 'Rejected', this.Reason, this.children[0]);
    // this.goBack();
  }

  exit() {

    if (this._exitPlacementService.exitPlacementForm.invalid) {
      this._alertService.error('Please fill required fields');
      return;
    }
    const exitFormData = this._exitPlacementService.exitPlacementForm.getRawValue();
    if (exitFormData.exitreasontypekey === 'PLCCCORH' && exitFormData.exittypekey === 'PLCC') {
      this._datastore.setData('showexitchecklist', true);
      this._datastore.setData('exitFormData', exitFormData);
      this.goBack();
    } else {
      console.log('exitdata', exitFormData);
      console.log("CHILDRENNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN", this.children);
      if (this.children && this.children.length && this.children[0].placement) {
        exitFormData.placementid = this.children[0].placement.placementid;
        exitFormData.servicecaseid = this.children[0].placement.servicecaseid;
        if (this.children[0].placement.providerdetails) {
          exitFormData.providerid = this.children[0].placement.providerdetails.provider_id;
        }
      }
      this.isSubmitting = true;
      this._commonHttpService
        .create(exitFormData, 'placement/exitplacement')
        .subscribe(response => {
          console.log('void inserted', response);
          this._alertService.success('Placement exit recorded successfully', true);
          if (exitFormData.exitreasontypekey === 'PLCCDOC'){
            this.isDOCExit = true;
          }
          this.goBack();
        });
    }
  }

  update() {
    console.log('children', this.children, this.startDate);
    if (!this.startDate) {
      this._alertService.error('Please fill required fields');
      return;
    }

    const editData = {
      startdate : this.startDate,
      starttime : this.startTime,
      placementid: '',
      servicecaseid: '',
      providerid: ''
    };

    console.log('editData', editData);
    console.log('children', this.children);

    if (this.children && this.children.length && this.children[0].placement) {
      editData.placementid = this.children[0].placement.placementid;
      editData.servicecaseid = this.children[0].placement.servicecaseid;
      if (this.children[0].placement.providerdetails) {
        editData.providerid = this.children[0].placement.providerdetails.provider_id;
      }
    }
    
    this.isSubmitting = true;
    this._commonHttpService
      .create(editData, 'placement/exitplacement')
      .subscribe(response => {
        console.log('void inserted', response);
        this._alertService.success('Placement edit recorded successfully', true);
        this.goBack();
      });

  }

  void() {
    if (this._exitPlacementService.voidPlacementForm.invalid) {
      this._alertService.error('Please fill required fields');
      return;
    }
    const voidPlacementFormData = this._exitPlacementService.voidPlacementForm.getRawValue();
    voidPlacementFormData.isvoided = 1;
    if (this.children && this.children.length && this.children[0].placement) {
      voidPlacementFormData.placementid = this.children[0].placement.placementid;
      voidPlacementFormData.servicecaseid = this.children[0].placement.servicecaseid;
      if (this.children[0].placement.providerdetails) {
        voidPlacementFormData.providerid = this.children[0].placement.providerdetails.provider_id;
      }
    }
    voidPlacementFormData.voiddate = new Date().toDateString;

    this.isSubmitting = true;
    this._commonHttpService
      .create(voidPlacementFormData, 'placement/voidplacementadd')
      .subscribe(response => {
        console.log('void inserted', response);
        this._alertService.success('Placement voided successfully', true);
        this.goBack();
      });

    console.log('void children', this.children, voidPlacementFormData);

  }

  validatePlacementDates(child) {

      let minDob = new Date();
      let minRemovalDate = new Date();

        if (child && child.dob) {
          const dob = new Date(child.dob);
          if (dob < minDob) {
            minDob = dob;
          }
        }
        if (child && child.childremoval && child.childremoval.length && Array.isArray(child.childremoval)) {
          const childRemoval = child.childremoval[0];
          const removalDate = new Date(childRemoval.removaldate);
          if (removalDate < minRemovalDate) {
            minRemovalDate = removalDate;
          }
        }
      if (minRemovalDate === new Date()) {
        this.minDate = minDob;
      } else {
        this.minDate = minRemovalDate;
      }
      
  }

}
