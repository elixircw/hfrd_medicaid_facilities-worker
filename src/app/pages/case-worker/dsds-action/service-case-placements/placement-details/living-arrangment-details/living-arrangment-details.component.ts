import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'living-arrangment-details',
  templateUrl: './living-arrangment-details.component.html',
  styleUrls: ['./living-arrangment-details.component.scss']
})
export class LivingArrangmentDetailsComponent implements OnInit {

  @Input() child: any;
  constructor() { }

  ngOnInit() {
  }

}
