import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'provider-placment-details',
  templateUrl: './provider-placment-details.component.html',
  styleUrls: ['./provider-placment-details.component.scss']
})
export class ProviderPlacmentDetailsComponent implements OnInit {

  @Input() child: any;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.child);
  }

  formatPhoneNumber(phoneNumberString) {
    const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
    const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      return '(' + match[1] + ') ' + match[2] + '-' + match[3];
    }
    return null;
  }


}
