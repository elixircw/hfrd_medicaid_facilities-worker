import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderPlacmentDetailsComponent } from './provider-placment-details.component';

describe('ProviderPlacmentDetailsComponent', () => {
  let component: ProviderPlacmentDetailsComponent;
  let fixture: ComponentFixture<ProviderPlacmentDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderPlacmentDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderPlacmentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
