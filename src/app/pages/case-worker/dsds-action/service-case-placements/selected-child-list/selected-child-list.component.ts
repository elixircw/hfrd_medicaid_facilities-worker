import { Component, OnInit } from '@angular/core';
import { ServiceCasePlacementsService } from '../service-case-placements.service';

@Component({
  selector: 'selected-child-list',
  templateUrl: './selected-child-list.component.html',
  styleUrls: ['./selected-child-list.component.scss']
})
export class SelectedChildListComponent implements OnInit {

  selectedChildren = [];
  constructor(private _service: ServiceCasePlacementsService) { }

  ngOnInit() {
    this.selectedChildren = this._service.selectedChildren;
  }

}
