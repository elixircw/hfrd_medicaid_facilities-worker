import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ServiceCasePlacementsService } from './service-case-placements.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServiceCasePlacementsResolverService implements Resolve<any> {

  constructor(private _service: ServiceCasePlacementsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

    return this._service.getChildRemovalInfoAndPlacements();
  }
}
