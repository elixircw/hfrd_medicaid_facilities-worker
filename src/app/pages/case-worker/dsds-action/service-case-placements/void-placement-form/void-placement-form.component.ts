import { Component, OnInit } from '@angular/core';
import { ExitPlacementService } from '../exit-placements/exit-placement.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'void-placement-form',
  templateUrl: './void-placement-form.component.html',
  styleUrls: ['./void-placement-form.component.scss']
})
export class VoidPlacementFormComponent implements OnInit {

  voidPlacementReasonTypes = [];
  voidPlacementForm: FormGroup;
  constructor(private _service: ExitPlacementService) { }

  ngOnInit() {
    this.voidPlacementForm = this._service.getVoidPlacementFormInstance();
    this._service.getVoidReasonTypes().subscribe(response => {
      if (response && response.length) {
        this.voidPlacementReasonTypes = response;
      }

    });
  }

}
