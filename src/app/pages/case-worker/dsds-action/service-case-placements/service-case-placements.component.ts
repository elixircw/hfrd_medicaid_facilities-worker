import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceCasePlacementsService } from './service-case-placements.service';

@Component({
  selector: 'service-case-placements',
  templateUrl: './service-case-placements.component.html',
  styleUrls: ['./service-case-placements.component.scss']
})
export class ServiceCasePlacementsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private _service: ServiceCasePlacementsService,
    private router: Router) {
    this.route.data.subscribe(res => {
      console.log('response', res);
      if (res && res.config) {
        console.log('res', res);
      }

    });
  }

  ngOnInit() {

  }

  openLivingArrangementOrReferral() {
    this.router.navigate(['wrapper']);
  }

}
