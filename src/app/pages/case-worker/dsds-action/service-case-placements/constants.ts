export class PlacementConstants {
    public static RUN_AWAY = 'RNW';
    public static ACTIONS = {
        EXIT : 'exit',
        EDIT : 'edit',
        VIEW : 'view',
        REVIEW: 'review',
        VOID: 'void',
        ADD: 'add'
    };

    public static EXIT_TYPES = {
        CHANGE_IN_PLACEMENT_STR : 'CIPS'
    };
}

