import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonHttpService, DataStoreService, AuthService } from '../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { PlacementConstants } from './constants';
import { AppConstants } from '../../../../@core/common/constants';
import { HttpService } from '../../../../@core/services/http.service';
import { ErrorInfo } from '../../../../@core/common/errorDisplay';
import { HttpClient } from '@angular/common/http';

const LIVING_ARRANGEMENT = 'LA';
const VOID_PLACEMENT = 1;
const RESPONSE_ACCEPTED_YES = '4612';
const RESPONSE_REJECTED = 'Rejected';
@Injectable()
export class ServiceCasePlacementsService {

  removedChildList = [];
  placementList = [];
  selectedChildren = [];
  placementDetails = [];
  isCaseWorker = false;
  isSuperVisor = false;
  error: ErrorInfo = new ErrorInfo();

  queueIndex = 0;
  public placementApprovalQueue$ = new Subject<any>();
  public refresh$ = new Subject<any>();
  public childSelection$ = new Subject<any>();

  constructor(private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private http: HttpClient) {
    this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
    this.isSuperVisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
  }

  getChildRemoval() {
    return this._commonService
      .getSingle(
        {
          where: { objectid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID), 'objecttypekey': 'servicecase' , isgroup: 1},
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
          .GetChildRemovalList + '?filter'
      );
  }

  resetValues() {
    this.selectedChildren = [];
  }

  getPlacementInfoList(pageNumber, limit) {
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: pageNumber,
          limit: limit,
          method: 'get',
          where: { servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
        }),
        'placement/getplacementbyservicecase?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );
  }

  getChildRemovalInfoAndPlacements() {
    return forkJoin([this.getPlacementInfoList(1, 10), this.getChildRemoval()])
      .map(([children, childRemoval]) => {
        if (children && children.data) {
          children.data.forEach(child => {
            child.hasAlert = this.checkForChildHasAlert(child.placements);
            child.hasActivePlacement = this.checkForChildHasActivePlacements(child.placements);
            child.placements.map(placement => {
              if(placement.alternateid){
                this._commonService.getArrayList({
                  method: 'get',
                  nolimit: true,
                  where: {
                    placement_id: placement.alternateid
                  }
                },
                'tb_placement_cpa_homes?filter').subscribe(response => {
                  if (response && Array.isArray(response) && response.length) {
                    placement.cpahomerevision = response;
                    response.forEach(cpahome => {
                      if(cpahome.provider_id){
                          this._commonService.getArrayList(
                            {
                              method: 'get',
                              where: { provider_id: cpahome.provider_id }
                            },
                            'tb_provider?filter'
                          ).subscribe(response => {
                            if (response && response.length) {
                              cpahome['providername'] =  this.getProviderFullName(response[0]);
                              }
                          });
                        } 
                      });
                  }
                });
              }
              return this.processForPlacementActions(placement);
            });
          });
          this.placementList = children.data;
        }
        if (childRemoval) {
          this.removedChildList = childRemoval.filter(item => item.childremoval !== null && item.childremoval.find(ritem => ritem.approvalstatus === 'Approved'));
        }
        this.processRemovedChildListStatus();
        return { placements: this.placementList, removedChildList: this.removedChildList };
      });
  }

  getProviderFullName(providerInfo){
    if(providerInfo){
      return '' + 
      (providerInfo.provider_prefix_cd?providerInfo.provider_prefix_cd+' ':'') + 
      (providerInfo.provider_first_nm?providerInfo.provider_first_nm+' ':'') + 
      (providerInfo.provider_middle_nm?providerInfo.provider_middle_nm+' ':'') + 
      (providerInfo.provider_last_nm?providerInfo.provider_last_nm+' ':'') + 
      (providerInfo.provider_suffix_cd?providerInfo.provider_suffix_cd+' ':'') ;
    }
    return '';

  }

  checkForChildHasAlert(placements) {
    let hasAlert = false;
    const runAwayPlacement = placements.find(placement => placement.livingarrangementtypekey === PlacementConstants.RUN_AWAY 
      && placement.enddate === null);
    if (runAwayPlacement) {
      hasAlert = true;
    }
    return hasAlert;
  }
  checkForChildHasActivePlacements(placements) {
    let hasActivePlacement = false;
    if (placements && placements.length) {
      const providerPlacement = placements.filter(placement => placement.placementtypekey !== LIVING_ARRANGEMENT 
        && placement.enddate === null
        && placement.isvoided !== VOID_PLACEMENT
        && placement.responseacceptedkey === RESPONSE_ACCEPTED_YES
        && placement.routingstatus !== RESPONSE_REJECTED);
      if (providerPlacement && providerPlacement.length) {
        hasActivePlacement = true;
      }
    }
   // return false; // for dev check
    return hasActivePlacement;
  }

  broadCastPageRefresh() {
    this.refresh$.next('refresh');
  }

  processRemovedChildListStatus() {
    this.resetValues();
    if (this.placementList && this.placementList.length) {
      this.removedChildList.map(removedChild => {

        const childPlaced = this.placementList.find(placedChild => placedChild.personid === removedChild.personid);
        if (childPlaced && childPlaced.placements) {
          childPlaced.placements.map(placement => {
            placement.placementTypeDesc = this.getPlacementTypeDescription(placement.placementtypekey);
          });
          removedChild.hasActivePlacement = this.checkForChildHasActivePlacements(childPlaced.placements);
          removedChild.placements = childPlaced.placements;
          const reviewPlacement = childPlaced.placements.find(placement => placement.routingstatus === 'Review');
          if (reviewPlacement) {
            reviewPlacement.fullAddress = this.processAddress(reviewPlacement);
            removedChild.placementStatus = reviewPlacement.routingstatus;
            removedChild.placementType = reviewPlacement.placementtypekey;
            removedChild.placement = reviewPlacement;
            // removedChild.isPlaced = true;
            removedChild.isSelected = true;
            this.selectedChildren.push(removedChild);
          }


        }
      });
    }
  }

  processAddress(placement) {
    const fullAddress = `${placement.address1} , ${placement.address2} , ${placement.cityname}, ${placement.statetypekey}, ${placement.zipcode}`;
    return fullAddress;
  }

  processForPlacementActions(placement) {
    if ( ( placement.routingstatus === 'Approved' || placement.routingstatus === 'Rejected' ) && this.isCaseWorker) {
      placement.isEditable = true;
    } else {
      placement.isEditable = false;
    }
    if (placement.enddate || placement.isvoided === 1) {
      placement.isEditable = false;
    }
    return placement;
  }

  getPlacementTypeDescription(type) {
    switch (type) {
      case 'LA':
        return 'Living Arrangement';
      default:
        return 'Provider Placement';
    }
  }

  selectChild(selectedChild, selection) {
    selectedChild.isSelected = selection;
    if (selection) {
      this.selectedChildren.push(selectedChild);
    } else {
      this.selectedChildren = this.selectedChildren.filter(child => child.personid !== selectedChild.personid);
    }
    this.childSelection$.next('SELECTED');
    console.log(selection, 'selected', this.selectedChildren);

  }

  getSelectedChildren() {
    return this.selectedChildren ? this.selectedChildren : [];
  }

  sendApprovalInQueue(data) {
    this.queueIndex = 0;
    this.sendForApproval(data);
  }

  getPropertyFromChildRemoval(propertyName) {
    if (this.selectedChildren[this.queueIndex].childremoval && this.selectedChildren[this.queueIndex].childremoval.length) {
      const childRemoval = this.selectedChildren[this.queueIndex].childremoval[0];
      return childRemoval[propertyName];
    } else {
      return null;
    }

  }

  sendForApproval(formData) {
    if (this.isQueueAvailable()) {
      formData.servicecaseid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
      formData.personid = this.selectedChildren[this.queueIndex].personid;
      formData.intakeservreqchildremovalid = this.getPropertyFromChildRemoval('intakeservreqchildremovalid');
      formData.intakeservicerequestactorid = this.getPropertyFromChildRemoval('intakeservicerequestactorid');
      formData.placementid = formData.placementid ? formData.placementid : null;
      formData.livingid = formData.livingid ? formData.livingid : null;
      console.log('send for approval', formData);

      this._commonService
        .create(formData, 'placement/addupdate')
        .subscribe(response => {
          console.log('la inserted', response);
          this.queueIndex++;
          if (this.isQueueAvailable()) {
            this.sendForApproval(formData);
          } else {
            console.log('queue completed after success');
            this.placementApprovalQueue$.next('completed');
          }
        });


    } else {
      console.log('queue completed outside');
    }

  }

  isQueueAvailable() {
    return this.queueIndex < this.selectedChildren.length;
  }

  extractPlacementId() {
    return this.selectedChildren.map(child => {
      return child.placement.placementid;
    });
  }

  approvePlacements(placementId, reason) {
    return this.approveOrRejectPlacement(placementId, 'Approved', reason);
  }

  rejectPlacements(placementId, reason) {
    return this.approveOrRejectPlacement(placementId, 'Rejected', reason);
  }

  placementCheckError(childData) {
    if (!childData.placement.voiddate) {
      this._commonService.endpointUrl = 'placement/placementAutoValidation';
      const placement = childData.placement;
      const revisionupdate = (placement) ? placement.revisionupdate : null;
      const model = {
        placementid: placement.alternateid,
        startdate: (revisionupdate) ? revisionupdate.entrydate : ((placement) ? placement.startdate : null),
        enddate: (revisionupdate) ? revisionupdate.enddate : null,
        update_sw: 'P',
        isbefore: false
      };
      this._commonService.create(model).subscribe(
      (response) => {});
    }
  }

  approveorRejectPlacementasSupervisor(placementId, status, reason, childData?) {
    if (status === 'Approved') {
     this.approvePlacements(placementId, reason).subscribe(response => {
       this.placementCheckError(childData);
       this.placementApprovalQueue$.next('Placement approved successfully');
     });
    } else  if (status === 'Rejected') {
       this.rejectPlacements(placementId, reason).subscribe(response => {
         this.placementApprovalQueue$.next('Placement returned to the worker successfully.');
       });
      }
    }

  approveOrRejectPlacement(placementId, status, reason) {
    const data = {
      'objectid': placementId,
      'eventcode': 'PLTR',
      'status': status,
      'comments': reason,
      'notifymsg': 'Child Placement ' + status,
      'routeddescription': 'Child Placement' + status
    };

    return this._commonService
      .create(data, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.approveOrReject);
  }


  getGoogleMarker(address) {
    const url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=AIzaSyD8CRCgWX0eRvC8XfFnaaMPm-36fb2GN9M';
    // let request: Observable<any>;
    let request;
    request = this.http.request('get', url).subscribe(response => {
      console.log('response', response);
    });
    return request;
    // return request;
    // return request.map((result: any) => {
    //   return result;
    // }, (err) => {
    //   console.log(err);
    //   this.error.error(err);
    // });

  }
  startPlacementApprovalInQueue() {
    this.queueIndex = 0;
    this.startPlacementApproval();
  }

  startPlacementApproval() {
    if (this.isQueueAvailable()) {
      const placementId = this.extractPlacementId()[this.queueIndex];
      this.approvePlacements(placementId, '').subscribe(response => {
        this.queueIndex++;
        if (this.isQueueAvailable()) {
          this.startPlacementApproval();
        } else {
          this.placementApprovalQueue$.next('Placement Approved Successfully');
        }
      });
    }

  }

  startPlacementRejectionInQueue() {
    this.queueIndex = 0;
    this.startPlacementRejection();
  }

  startPlacementRejection() {
    if (this.isQueueAvailable()) {
      const placementId = this.extractPlacementId()[this.queueIndex];
      this.rejectPlacements(placementId, '').subscribe(response => {
        this.queueIndex++;
        if (this.isQueueAvailable()) {
          this.startPlacementRejection();
        } else {
          this.placementApprovalQueue$.next('Placement returned to the worker successfully.');
        }
      });
    }
  }
}
