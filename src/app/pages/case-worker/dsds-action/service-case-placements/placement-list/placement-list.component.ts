import { Component, OnInit } from '@angular/core';
import { ServiceCasePlacementsService } from '../service-case-placements.service';
import { AuthService, AlertService, CommonHttpService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { AppConstants } from '../../../../../@core/common/constants';
import { Router, ActivatedRoute } from '@angular/router';
import { PlacementConstants } from '../constants';
import { PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';

@Component({
  selector: 'placement-list',
  templateUrl: './placement-list.component.html',
  styleUrls: ['./placement-list.component.scss']
})
export class PlacementListComponent implements OnInit {

  placementList = [];
  isCaseWorker = false;
  isSuperVisor = false;
  accountpayableList = [];
  selectedChild: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  noPlacementsInReview = true;
  ALERT_MESSAGE = 'There is a living arrangement marked alongside a provider placement. Please take action to end the provider placement not later than 30 calendar days.';
  placementHistory: any[];
  placementRevison: any[];
  cpahomerevision: any[];
  exitPlacementvalid: boolean;
  exitValidationObj: any;
  isClosed = false;
  constructor(private _service: ServiceCasePlacementsService,
    private _authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private storage: SessionStorageService,
    private commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.placementList = this._service.placementList;
    if (this.placementList && this.placementList.length > 0) {
      this.placementList.forEach(res => {
        if (res && res.placements && res.placements.length > 0) {
          for (let i = 0; i < res.placements.length; i++) {
            if (!(res.placements[i].enddate) && res.placements[i].placementtypekey !== 'LA' && res.placements[i].responseacceptedkey !== '4612') {
              res.active = false;
            } else {
              res.active = true;
              break;
            }
          }
        }
      });
    }
    this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
    this.isSuperVisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
    const showexitchecklist = this._dataStoreService.getData('showexitchecklist');
    if (showexitchecklist) {
      this._dataStoreService.setData('showexitchecklist', false);
      const child = this._service.placementDetails;
      const placement = this._service.placementDetails[0].placement;
      this.exitPlacement(child, placement, 0);
    }
  }

  placementHistoryDetail (placement) {
    this.placementHistory = placement;
  }

  editPlacement(child, placement) {
    const placementDetails = child;
    child.placementType = placement.placementtypekey;
    placementDetails.placement = placement;
    this._service.placementDetails = [placementDetails];
    this.router.navigate(['../details/' + PlacementConstants.ACTIONS.EDIT], { relativeTo: this.route });
  }

  exitPlacement(child, placement, action) {
    if (action) {
      const placementDetails = child;
      child.placementType = placement.placementtypekey;
      placementDetails.placement = placement;
      this._service.placementDetails = [placementDetails];
      this.router.navigate(['../details/' + PlacementConstants.ACTIONS.EXIT], { relativeTo: this.route });
    } else {
      this.getExitPlacementValidation(child, placement);
    }
  }

  voidPlacement(child, placement) {
    const placementDetails = child;
    child.placementType = placement.placementtypekey;
    placementDetails.placement = placement;
    this._service.placementDetails = [placementDetails];
    this.router.navigate(['../details/' + PlacementConstants.ACTIONS.VOID], { relativeTo: this.route });

  }

  reviewPlacement(child, placement) {
    (<any>$('#historyPlacement')).modal('hide');
    const placementDetails = child;
    child.placementType = placement.placementtypekey;
    placementDetails.placement = placement;
    this._service.placementDetails = [placementDetails];
    this.router.navigate(['../details/' + PlacementConstants.ACTIONS.REVIEW], { relativeTo: this.route });
  }

  addCPAHome(child, placement) {
    (<any>$('#historyPlacement')).modal('hide');
    const placementDetails = child;
    child.placementType = placement.placementtypekey;
    placementDetails.placement = placement;
    this._service.placementDetails = [placementDetails];
    this.router.navigate(['../details/' + PlacementConstants.ACTIONS.ADD], { relativeTo: this.route });
  }

  showPlacementDetails(child, placement) {
    (<any>$('#historyPlacement')).modal('hide');
    const placementDetails = child;
    child.placementType = placement.placementtypekey;
    placementDetails.placement = placement;
    this._service.placementDetails = [placementDetails];
    this.router.navigate(['../details/' + PlacementConstants.ACTIONS.VIEW], { relativeTo: this.route });
  }

  toggleTable(id, index, placementrevison) {
    this.placementRevison = placementrevison;
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle');
    (<any>$('.provider-details tr')).removeClass('selected-bg');
    (<any>$(`#provider-details-${index}`)).addClass('selected-bg');
  }

  showCPAHomes(id, index, cpahomerevision) {
    this.cpahomerevision = cpahomerevision;
    (<any>$('.collapse.in')).collapse('hide');
    (<any>$('#' + id)).collapse('toggle');
    (<any>$('.provider-details tr')).removeClass('selected-bg');
    (<any>$(`#cpa-homes-table${index}`)).addClass('selected-bg');
  }



  // [routerLink]="[ '../wrapper']"
  addPlacements() {
    console.log('sc', this._service.selectedChildren);
    if (this._service.selectedChildren && this._service.selectedChildren.length === 0) {
      this._alertService.error('Please select child/children ');
      return;
    }
    this.router.navigate(['../wrapper'], { relativeTo: this.route });
  }

  reviewPlacements(placement, child) {
    this._service.resetValues();
    const selectedChild = [];
    child.placementStatus = placement.routingstatus;
    child.placementType = placement.placementtypekey;
    selectedChild.push(child);
    if (selectedChild && selectedChild.length) {
      selectedChild[0].placement = placement;
    }
    this._service.placementDetails = selectedChild;
    this.router.navigate(['../details/' + PlacementConstants.ACTIONS.REVIEW], { relativeTo: this.route });

  }

  reviewPlacements_old() {
    if (this._service.selectedChildren && this._service.selectedChildren.length === 0) {
      this._alertService.error('Please select child/children ');
      return;
    }
    this._service.placementDetails = this._service.selectedChildren;
    let reviewCount = 0;

    this._service.placementDetails.forEach(child => {
      if (child.placementStatus === 'Review') {
        reviewCount++;
      }
    });

    if (reviewCount === 0) {
      this._alertService.error('No placements in review');
      return;
    }
    this.router.navigate(['../details/' + PlacementConstants.ACTIONS.REVIEW], { relativeTo: this.route });
  }

  openLivingArrangement() {
    if (this._service.selectedChildren && this._service.selectedChildren.length === 0) {
      this._alertService.error('Please select child/children ');
      return;
    }
    this.router.navigate(['../wrapper/living-arrangement'], { relativeTo: this.route });
  }

  openPlacement() {
    if (this._service.selectedChildren && this._service.selectedChildren.length === 0) {
      this._alertService.error('Please select child/children ');
      return;
    }
    if (this._service.selectedChildren) {
      const activePlacementChildren = this._service.selectedChildren.filter(child => child.hasActivePlacement);
      if (activePlacementChildren.length > 0) {
        // this._alertService.error('Selected child/children have an active placement');
        (<any>$('#placementExistAlert')).modal('show');
        return;
      }
    }
    this.router.navigate(['../wrapper/referral'], { relativeTo: this.route });
  }
  showChildHistory(child) {
    this.accountpayableList = [];
    this.paginationInfo.pageNumber = 1;
    this.paginationInfo.pageSize = 10;
    this.selectedChild = child;
    this.commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'post',
        where: {
          clientid: this.selectedChild.cjamspid
        }
      }), 'tb_payment_header/getAccountsPayableHeaderForCase'
    ).subscribe((result: any) => {
      if (result) {
        this.accountpayableList = result.data;
      }
    });
  }

  getExitPlacementValidation(child, placement) {
    this.commonHttpService.getArrayList(
      {
        method: 'get',
        where: {
          personid: child.personid
        }
      }, 'placement/placementvalidation?filter'
    ).subscribe((result: any) => {
      if (Array.isArray(result) && result.length) {
        const data = result[0].getplacementvalidations;
        const obj = data[0];
        this.exitValidationObj = obj;

        this.exitPlacementvalid = (obj.iscaseplan && obj.iscaseplaneligibility && obj.ischildage && obj.is_servicelog && obj.islivingarrgangement) ? true : false;
        // if (this.exitPlacementvalid) {
        //   this.exitPlacement(child, placement, 1);
        // } else {
          (<any>$('#exitplacementvalidation')).modal('show');
        // }
      }
    });
  }
  placementExit() {
    const exitFormData = this._dataStoreService.getData('exitFormData');
    const children = this._service.placementDetails;
    console.log('exitdata', exitFormData);
    console.log('children', children);
    if (children && children.length && children[0].placement) {
      exitFormData.placementid = children[0].placement.placementid;
      exitFormData.servicecaseid = children[0].placement.servicecaseid;
      if (children[0].placement.providerdetails) {
        exitFormData.providerid = children[0].placement.providerdetails.provider_id;
      }
    }
    this.commonHttpService
      .create(exitFormData, 'placement/exitplacement')
      .subscribe(response => {
        console.log('void inserted', response);
        this._alertService.success('Placement exit recorded successfully', true);
      });
  }

}
