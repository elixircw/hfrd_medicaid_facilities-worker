import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServiceCasePlacementsService } from '../service-case-placements.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonHttpService } from '../../../../../@core/services';
import { PaginationRequest, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { PlacementConstants } from '../constants';
import * as moment from 'moment';
import { FinanceUrlConfig } from '../../../../finance/finance.url.config';
import { FinanceService } from '../../../../finance/finance.service';

@Component({
  selector: 'removed-child-list',
  templateUrl: './removed-child-list.component.html',
  styleUrls: ['./removed-child-list.component.scss']
})
export class RemovedChildListComponent implements OnInit, OnDestroy {

  childList = [];
  accountpayableList = [];
  paymentDetails = [];
  selectedChild: any;
  paginationInfo: PaginationInfo  = new PaginationInfo();
  pageInfo: PaginationInfo  = new PaginationInfo();
  searchParams: any;
  changehistory = [];
  adjustment: any[];
  providerDetails: any;
  overpayments: any[];
  clientID: any;
  constructor(private placementService: ServiceCasePlacementsService,
    private commonHttpService: CommonHttpService,
    private router: Router,
    private route: ActivatedRoute,
    private _financeSerice: FinanceService) { }

  ngOnInit() {
    this.childList = this.placementService.removedChildList;
    this.placementService.refresh$.subscribe(_ => {
      this.childList = this.placementService.removedChildList;
    });
    this.childList.map(res => {
      return res.fullname = res.prefx + ' ' + res.firstname + ' ' + res.middlename + ' ' + res.lastname + ' ' + res.suffix
    });
  }

  ngOnDestroy() {
    this.placementService.removedChildList = [];
  }

  onChildChecked(event, child) {
    this.placementService.selectChild(child, event.checked);
  }

  showPlacementDetails(placement) {
    this.placementService.placementDetails = this.placementService.removedChildList;
    this.router.navigate(['details/' + PlacementConstants.ACTIONS.REVIEW], { relativeTo: this.route });
  }

  showChildHistory(child) {
    this.accountpayableList = [];
    this.paginationInfo.pageNumber = 1;
    this.paginationInfo.pageSize = 10;
    this.selectedChild = child;
    this.providerDetails = (child && child.placements && child.placements.length && child.placements[0].providerdetails) ? child.placements[0].providerdetails : null;
    if (this.providerDetails) {
      this.getOverPaymentList(this.providerDetails.provider_id, this.selectedChild.cjamspid);
    }
    this.commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: this.paginationInfo.pageSize,
        page: this.paginationInfo.pageNumber,
        method: 'post',
        where: {
          clientid: this.selectedChild.cjamspid,
          payment_statuses_cd: [1636, 1634], // get approved(1634) and interfaced(1636) records
        }
      }), 'tb_payment_header/getAccountsPayableHeaderForCase'
    ).subscribe((result: any) => {
      if (result) {
        this.accountpayableList = result.data;
      }
    });
  }

  getOverPaymentList(providerid, clientid) {
    this.commonHttpService.getPagedArrayList({
      where: {
        providerid: providerid,
        client_id: clientid
      },
      page: 1,
      limit: null,
      nolimt: true,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.overpayments.list).subscribe((res: any) => {
      if (res && res.data && res.data.length) {
        this.overpayments = res.data;
      }
    });
  }

  toggleTable(id, index, paymentid) {
    // this.id = id;
    // this.searchParams.paymentid = paymentid;
   // if (mode === 'Add') {
      (<any>$('.collapse.in')).collapse('hide');
      (<any>$('#' + id)).collapse('toggle');
   // }

    (<any>$('.provider-details tr')).removeClass('selected-bg');
    (<any>$(`#provider-details-${index}`)).addClass('selected-bg');

    this.commonHttpService.getPagedArrayList(new PaginationRequest({
      where: {
        paymentid: paymentid,
        clientid : this.selectedChild.cjamspid
      },
      limit : this.pageInfo.pageSize,
      page: this.pageInfo.pageNumber,
      method: 'post'
    }), 'tb_payment_header/getAccountsPayableInfo').subscribe(res => {
      this.paymentDetails = res.data;
     // this.totalPage = (this.accountpayableList && this.accountpayableList.length > 0) ? this.accountpayableList[0].totalcount : 0;
    });
  }


  private getAge(dateValue) {
    if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
        const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
        return moment().diff(rCDob, 'years');
    } else {
        return '';
    }
  }

  fiscalAudit() {
    this._financeSerice.getFiscalAudit(this.selectedChild.cjamspid, 1, this.overpayments);
    (<any>$('#fiscal-audit')).modal('show');
    /* this.commonHttpService.getPagedArrayList({
      where: {
        receivable_detail_id: null,
        clientid: this.selectedChild.cjamspid
      },
      page: this.paginationInfo.pageNumber,
      limit: this.paginationInfo.pageSize,
      method: 'get'
    }, FinanceUrlConfig.EndPoint.accountsReceivable.audit.auditList).subscribe((result: any) => {
        if (result && result.data && result.data.length > 0) {
          this.changehistory = result.data;
          for (let i = 0;  i < result.data.length; i++) {
            if (result.data[i].adjstment) {
              this.adjustment = result.data[i].adjstment;
            }
          }
        }
    }); */
  }
}
