import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, AlertService } from '../../../../../@core/services';
import { AppConstants } from '../../../../../@core/common/constants';
import { ServiceCasePlacementsService } from '../service-case-placements.service';


@Component({
  selector: 'placment-wrapper',
  templateUrl: './placment-wrapper.component.html',
  styleUrls: ['./placment-wrapper.component.scss']
})
export class PlacmentWrapperComponent implements OnInit {

  decidedPage: string;
  LIVING_ARRANGEMENT = 'LR';
  PLACEMENT_REFERRAL = 'PR';
  isSupervisor = false;
  isCaseWorker = false;
  isChildSelected = false;
  constructor(private router: Router, private route: ActivatedRoute,
    private _authService: AuthService,
    private _service: ServiceCasePlacementsService) { }

  ngOnInit() {
    // this.decidePage(this.LIVING_ARRANGEMENT);
    this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
    this._service.childSelection$.subscribe(_ => {
      if (this._service.selectedChildren.length > 0) {
        this.isChildSelected = true;
      }
    });
  }
  decidePage(event) {
    console.log(this.decidedPage, event);
    if (this.decidedPage === this.LIVING_ARRANGEMENT) {
      this.router.navigate(['living-arrangement'], { relativeTo: this.route });
    } else if (this.decidedPage === this.PLACEMENT_REFERRAL) {
      if (this._service.selectedChildren) {
        const activePlacementChildren = this._service.selectedChildren.filter(child => child.hasActivePlacement);
        if (activePlacementChildren.length > 0) {
          (<any>$('#placementExistAlert')).modal('show');
          setTimeout(() => {
            this.decidedPage = null;
          }, 100);
          return;
        } else {
          this.router.navigate(['referral'], { relativeTo: this.route });
        }
      }

    }


  }

  goBack() {
    this._service.getChildRemovalInfoAndPlacements().subscribe(response => {
      console.log('reload', response);
      this._service.broadCastPageRefresh();
      this.router.navigate(['../list'], { relativeTo: this.route });
    });
  }

}
