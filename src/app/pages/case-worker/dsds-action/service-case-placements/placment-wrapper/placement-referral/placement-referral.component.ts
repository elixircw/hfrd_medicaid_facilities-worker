import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaginationRequest, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, DataStoreService, SessionStorageService } from '../../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import * as moment from 'moment';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
import { InvolvedPerson } from '../../../as-service-plan/_entities/as-service-plan.model';
import { Observable } from 'rxjs/Observable';
import { ServiceCasePlacementsService } from '../../service-case-placements.service';
import { Router, ActivatedRoute } from '@angular/router';
declare let google: any;
const RESPONSE_ACCEPTED_YES = '4612';
import * as _ from 'lodash';
@Component({
  selector: 'placement-referral',
  templateUrl: './placement-referral.component.html',
  styleUrls: ['./placement-referral.component.scss']
})
export class PlacementReferralComponent implements OnInit {

  providerSearchForm: FormGroup;
  referalForm: FormGroup;
  childCharacteristics: any[];
  otherLocalDeptmntType: any[];
  placementStrType: any[];
  validplacementStrType: any[];
  validfcProviderSearch: any[];
  bundledPlcmntServicesType: any[];
  paginationInfo: PaginationInfo = new PaginationInfo();
  fcProviderSearch: any[];
  childRemovalData: any[];
  intakeservicerequestactorid: string;
  isPublicProvider: boolean;
  isAddPlacement: boolean;
  selectedResponseStructure: any;
  reportedChildDob: any;
  selectedProvider: any;
  selectedViewProvider: any;
  isPrivateProvider: boolean;
  isChildRemoval = false;
  selectedPlacement: any;
  isDateTime: boolean;
  isSubmitting: boolean;
  daNumber: string;
  markersLocation = ([] = []);
  zoom: number;
  defaultLat = 39.29044;
  defaultLng = -76.61233;
  id: string;
  isServiceCase: boolean;
  comarRateRequired: boolean;
  fcTotal: number;
  selectedPlacementStructure: any;
  selectedChild: InvolvedPerson;
  fcpaginationInfo: PaginationInfo = new PaginationInfo();
  inputRequest: Object;
  placement$: Observable<any[]>;
  placementCount$: Observable<number>;
  ResponseAcceptedList: any[];
  ResponseRejectedList: any[];
  genderDropdownItems: any[];
  selectedChildren: any[];
  minAge: number;
  maxAge: number;
  gender: string;
  minDate: any;
  currProcess: string;
  involevedPerson$: Observable<InvolvedPerson[]>;
  lat = 51.678418;
  lng = 7.809007;
  showMap: boolean;
  currentCounty: any;
  isResponseAccepted = false;
  maxDate: Date;
  approval: boolean;
  comarRateId : any;
  kinshipMaxdate: any;
  comarValues : any[];
  comarValueExclude= ['8','76', '1', '74', '15', '14' , '75' , '167','78'];
  constructor(
    private formBuilder: FormBuilder,
    private _commonService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private _session: SessionStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private _ServiceCasePlacementsService: ServiceCasePlacementsService
  ) { }

  ngOnInit() {
    this.currProcess = 'search';
    this.minDate = new Date();
    this.maxDate = new Date();
    this._ServiceCasePlacementsService.placementApprovalQueue$.subscribe(response => {
      (<any>$('#placement-ackmt')).modal('show');
    });
    this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    if (this.isServiceCase) {
      this.inputRequest = {
        objectid: this.id,
        objecttypekey: 'servicecase',
        isgroup: 1
      };
    } else {
      this.inputRequest = { intakeservreqid: this.id };
    }
    this.forminitialize();
    this.getChildCharacteristics();
    this.getOtherLocalDeptmntType();
    this.getResponseAcceptedList();
    this.getResponseRejectedList();
    this.loadGenderDropdownItems();
    this.getBundledPlcmntServicesType();
    this.getComarlist();
    this.getPlacementStrType(null);
    this.getSelectedChildren();
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.getChildRemoval();
    this.isPublicProvider = false;
    this.isPrivateProvider = false;
    this.isDateTime = false;
    this.listReportSummary(this.id);
    this.selectedChildren = this._ServiceCasePlacementsService.selectedChildren;
    this.validatePlacementDates(this.selectedChildren);
    const caseData = this._session.getObj(CASE_STORE_CONSTANTS.CASE_DASHBOARD);
    console.log(caseData);
    this._ServiceCasePlacementsService.childSelection$.subscribe(children => {
      this.selectedChildren = this._ServiceCasePlacementsService.selectedChildren;
      this.referalForm.patchValue({
        startdate: null
      });
      this.validatePlacementDates(this.selectedChildren);
    });
    this.localdptSelected(true);
  }

 /*  beginDateChange(referalForm) {
    const startDate = referalForm.getRawValue().startdate;
    const placements = [];
    const selectedChildren = this._ServiceCasePlacementsService.placementList;
    if(selectedChildren && selectedChildren.length) {
      selectedChildren.forEach(child => {
        if(child && child.placements && child.placements.length) {
          child.placements.forEach(placement => {
            placements.push(placement);
          });
        }
      });
    }
    const isRangeExist = placements.filter( (placement) => 
      placement.startdate &&  new Date(startDate).getTime() >= new Date(placement.startdate).getTime() &&
      new Date(startDate).getTime() <= new Date(placement.enddate).getTime()
     );
    if(isRangeExist && isRangeExist.length) {
      this._alertService.error('Placement begin date should be greater than existing placement range');
      this.referalForm.patchValue({ 'startdate' : null });
    }
  } */

  private listReportSummary(id: string) {
    let caseid = '';
    const isServicecase = this._dataStoreService.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    const cpscaseid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CPS_CASE_ID);
    caseid = (isServicecase) ? cpscaseid : this.id;
    this._commonService.getSingle(new PaginationRequest({}), CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.ReportSummary + caseid)
    .subscribe(result => {
      // this.reportSummary = result;
      if (result && result.county && result.county.statecountycode) {
         this.currentCounty =  result.county.statecountycode;
      }
      // console.log('Report Summary', this.reportSummary);
      // this._dataStoreService.setData('reportSummaryData',result);
 });

}


  validatePlacementDates(selectedChildren) {
    if (selectedChildren && selectedChildren.length && Array.isArray(selectedChildren)) {
      let  minDob = new Date();
      let minRemovalDate = new Date();
      const lastPlacements = [];
    selectedChildren.forEach((child, index) => {
       if (child.dob) {
         const dob = new Date(child.dob);
         if (dob < minDob) {
           minDob = dob;
         }
       }
       if (child.childremoval && child.childremoval.length && Array.isArray(child.childremoval)) {
         const childRemoval =  child.childremoval[0];
         const removalDate = new Date(childRemoval.removaldate);
         if (removalDate < minRemovalDate || index === 0) {
          minRemovalDate =  removalDate;
         }
       }
       // if child has placement end date 
       if (Array.isArray(child.placements)) {
        const providerPlacemnts = child.placements.filter(item => item.placementtypekey === 'PRPL' && !item.isvoided);
        if (providerPlacemnts && providerPlacemnts.length) {
          const orderedPlacements = _.orderBy(providerPlacemnts, function(placement) { return moment(placement.enddate).format('YYYYMMDD'); }, ['desc']);
          const lastPlacement = orderedPlacements[0];
          lastPlacements.push(lastPlacement);
        }
       }
     });

     if (minRemovalDate === new Date()) {
       this.minDate = minDob;

     } else {
      this.minDate = minRemovalDate;

     }

     if (lastPlacements.length) {
       const orderedPlacements = _.orderBy(lastPlacements, function(placement) { return moment(placement.enddate).format('YYYYMMDD'); }, ['desc']);
       const lastPlacement = orderedPlacements[0];
       if (lastPlacement.enddate) {
         this.minDate = lastPlacement.enddate;
       }
     }

     // setting kinship max date 18th birthdate of selected children
     this.kinshipMaxdate = moment(minDob).add('years', 18).add('day', 1).format('YYYY-MM-DD');
     console.log('KINSHIP MAX DATE', this.kinshipMaxdate );

     

   }
  }

  forminitialize() {
    this.providerSearchForm = this.formBuilder.group({
      childcharacteristics: [null],
      bundledplacementservices: [null],
      otherLocalDeptmntTypeId: [null],
      placementstructures: [null],
      zipcode: null,
      isLocalDpt: [true],
      firstname: null,
      middlename: null,
      lastname: null,
      providername: null,
      isgender: [true],
      isAge: [true],
      providerid: null,
      agemin: null,
      agemax: null,
      gender: null
    });
    this.providerSearchForm.get('firstname').disable();
    this.providerSearchForm.get('middlename').disable();
    this.providerSearchForm.get('lastname').disable();


    this.referalForm = this.formBuilder.group({
      providersentdate: [null],
      providerdesc: [null],
      responseacceptedkey: [null, Validators.required],
      rejectreasonkey: [null],
      service_id: ['', Validators.required],
      ratestructureid: [{value: '', disabled: true}, Validators.required],
      startdate: [null],
      starttime: ['08:00'],
      isssaapproval: [false],
      ifcapprovaldate: [null],
      remarks: [''],
      placementtypekey: ['PRPL'],
      ischildplacedoutside: [null],
      casecounty: ['']
    });
    this.referalForm.get('isssaapproval').disable();
    this.referalForm.get('ifcapprovaldate').disable();

  }
  closeMap() {
    this.markersLocation = [];
    this.showMap = false;
    (<any>$('#map-popup')).modal('hide');
  }
  close() {
    (<any>$('#placement-ackmt')).modal('hide');
    this.goBack();
  }

  goBack() {
    this._ServiceCasePlacementsService.getChildRemovalInfoAndPlacements().subscribe(response => {
      console.log('reload', response);
      this._ServiceCasePlacementsService.broadCastPageRefresh();
      this.router.navigate(['../../list'], { relativeTo: this.route });
    });
  }

  getRangeArray(n: number): any[] {
    return Array(n);
  }

  getChildRemoval() {
    this._commonService.getSingle(
      {
        where: this.inputRequest,
        method: 'get'
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
        .GetChildRemovalList + '?filter'
    )
      .subscribe(result => {
        if (result && result.length) {
          this.isChildRemoval = true;
          this.childRemovalData = result;
        }
      });
  }


  getSelectedChildren() {
    this.selectedChildren = this._ServiceCasePlacementsService.getSelectedChildren();
    if (this.selectedChildren && this.selectedChildren.length > 0) {
      // for min,max age calculation
      if (this.selectedChildren.length > 1) {
        this.minAge = this.filterAge(this.selectedChildren, 'min');
        this.maxAge = this.filterAge(this.selectedChildren, 'max');
        this.providerSearchForm.patchValue({
          agemin: this.minAge,
          agemax: this.maxAge
        });

      }

      if (this.selectedChildren.length === 1) {
        this.minAge = this.filterAge(this.selectedChildren, 'min');
        this.maxAge = this.filterAge(this.selectedChildren, 'max');
        this.gender = this.selectedChildren[0].typedescription;
        this.providerSearchForm.patchValue({
          gender: this.gender,
          agemin: this.minAge,
          agemax: this.maxAge
        });
      }

    }
  }


  filterAge(selectedChildern, type) {
    let age;
    let firstChildAge;
    firstChildAge = this.getAge(selectedChildern[0].dob);
    let min = parseInt(firstChildAge, 10);
    let max = parseInt(firstChildAge, 10);
    let result = parseInt(firstChildAge, 10);
    selectedChildern.forEach(child => {
      age = this.getAge(child.dob);
      age = parseInt(age, 10);
      switch (type) {
        case 'min': if (age < min) {
          min = age;
          result = min;
        }
          break;
        case 'max':
          if (age > max) {
            max = age;
            result = max;
          }
          break;
        default: break;
      }
    });
    return result;
  }



  getResponseAcceptedList() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'referencetypeid': 79
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.GetReasonTypes).subscribe(result => {
      this.ResponseAcceptedList = result;
    });
    // this._commonService.getArrayList(new PaginationRequest({
    //   where: {
    //     'picklist_type_id': '756'
    //   },
    //   nolimit: true,
    //   method: 'get'
    // }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.childCharacteristicsUrl).subscribe(result => {
    //   this.ResponseAcceptedList = result;
    // });
  }
  getChildCharacteristics() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '43'
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.childCharacteristicsUrl).subscribe(result => {
      this.childCharacteristics = result;
    });
  }

  CheckFormControlValue(formControl) {
    return (formControl && formControl !== '') ? formControl : null;
  }

  getOtherLocalDeptmntType() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'picklist_type_id': '104'
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.otherLocalDeptmntTypeUrl).subscribe(result => {
      this.otherLocalDeptmntType = result;
    });
  }


  localdptSelected(value) {
    console.log(value);

    if (value) {
      this.providerSearchForm.get('firstname').enable();
      this.providerSearchForm.get('middlename').enable();
      this.providerSearchForm.get('lastname').enable();
      this.providerSearchForm.get('otherLocalDeptmntTypeId').enable();
    } else {
      this.providerSearchForm.get('providername').enable();
      //this.providerSearchForm.get('firstname').disable();
      //this.providerSearchForm.get('middlename').disable();
      //this.providerSearchForm.get('lastname').disable();
      this.providerSearchForm.get('otherLocalDeptmntTypeId').reset();
      this.providerSearchForm.get('otherLocalDeptmntTypeId').disable();
    }
  }

  getComarlist(){
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'structure_service_cd': 'P',
        'comar_sw': 'Y'
      },
      nolimit: true,
      method: 'get'
    }),CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.comarlisteUrl).subscribe(result => {
      this.comarValues = result;
    });
    }

  getcomarlistValues(){
    const placementStructure = this.referalForm.getRawValue().service_id;
    if (placementStructure == '500' || placementStructure == '77') {
      this.referalForm.get('ratestructureid').enable();
      this.referalForm.get('ratestructureid').clearValidators();
      this.referalForm.get('ratestructureid').updateValueAndValidity();
      this.comarRateRequired = true;
      this.placementStrType = this.comarValues;
    }
  }
  getPlacementStrType(providerId) {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'structure_service_cd': 'P',
        'provider_id': providerId
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.placementStrTypeUrl).subscribe(result => {
      this.placementStrType = result.filter(item =>  (item.service_id !== 501 && item.service_id !== 71 && item.service_id !== 503));
      // 71 - Emergency Foster Care Retainer
      // 501 - Adoptive Home
      // 503 - Guardianship Assistance Program
      this.validplacementStrType = this.placementStrType.filter(item => item.service_status === 'Active' && (item.service_id !== 501 && item.service_id !== 71 && item.service_id !== 503));

      this.getcomarlistValues();

    });
  }

  getBundledPlcmntServicesType() {
    this._commonService.getArrayList(new PaginationRequest({
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.bundledPlcmntServicesTypeUrl).subscribe(result => {
      this.bundledPlcmntServicesType = result;
    });
  }

  resetproviderSearchForm() {
    this.providerSearchForm.reset();
  }

  getFcProviderSearch() {
    if (this.providerSearchForm.invalid) {
      this._alertService.error('Please fill required fields');
      return false;
    }


    if (this._ServiceCasePlacementsService.selectedChildren.length === 0) {
      this._alertService.error('Please select the children');
      return false;
    }

    const formValues = this.providerSearchForm.getRawValue();
    formValues.casecounty = this.currentCounty ? this.currentCounty : '';
    // formValues.casecounty = '1439';
    if (formValues.isgender && !formValues.gender) {
      this._alertService.error('Please select gender');
      return false;
    }
    // formValues.agemin = formValues.isAge ? ( this.minAge ? this.minAge : null ) : null;
    // formValues.agemax = formValues.isAge ? ( this.maxAge ? this.maxAge : null ) : null;
    formValues.gender = formValues.isgender ? (formValues.gender ? formValues.gender : null) : null;
    console.log(formValues);

    Object.keys(this.providerSearchForm.controls).forEach(key => {
      formValues[key] = this.CheckFormControlValue(formValues[key]);
    });
    const body = {};
    Object.assign(body, formValues);
    body['isLocalDpt'] = formValues.isLocalDpt ? formValues.isLocalDpt : false;
    body['localdepartmenthomecaregiver'] = formValues.isLocalDpt ? formValues.isLocalDpt : false;
    body['childcharacteristics'] = formValues.childcharacteristics ? formValues.childcharacteristics[0] : null;
    body['otherLocalDeptmntTypeId'] = formValues.otherLocalDeptmntTypeId ? formValues.otherLocalDeptmntTypeId[0] : null;
    body['placementstructures'] = formValues.placementstructures ? formValues.placementstructures[0] : null;
    body['bundledplacementservices'] = formValues.bundledplacementservices ? formValues.bundledplacementservices[0] : null;
    body['fromproviderplacement'] = true;
    const dob = moment.utc(this.reportedChildDob);
    const age16 = dob.clone().add(16, 'years');
    const age21 = dob.clone().add(21, 'years');
    const isAgeBtwn16And18 = moment.utc().isBetween(age16, age21, null, '[]');
    if (isAgeBtwn16And18 && formValues.placementStrTypeId === '1') {// Independent Living Residential Program
      this._alertService.error('A child between the age of 16 and 21 years cannot be placed in \'Independent Living Residential Program\' Placement Structure.');
      return false;
    }
    // body = {
    //   childcharacteristics: parseInt(formValues.childCharacteristicsid, 10),
    //   placementstructures: parseInt(formValues.placementStrTypeId, 10),
    //   bundledplacementservices: parseInt(formValues.bundledPlcmntServicesTypeId, 10),
    //   providerid: formValues.providerid,
    //   zipcode: parseInt(formValues.zipcode, 10),
    //   localdepartmenthomecaregiver: formValues.isLocalDpt
    // };
    this._commonService.getPagedArrayList(new PaginationRequest({
      where: body,
      page: this.paginationInfo.pageNumber,
      limit: 10,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fcProviderSearchUrl).subscribe(result => {
      this.fcProviderSearch = result.data;
      this.validfcProviderSearch = this.fcProviderSearch.filter(item => item.service_status === 'Active');
      this.fcTotal = result.count;
      (<any>$('#fc_list')).click();
      this.currProcess = 'select';
    });
  }

  selectedViewProv(provId) {
    this.selectedViewProvider = provId;
  }

  selectedProv(provId) {
    // this.nextDisabled = false;
    console.log(this.selectedProvider);
    this.selectedProvider = provId;
    this.getPlacementStrType(this.selectedProvider.provider_id);
  }

  chooseReferral() {

  }
  formatPhoneNumber(phoneNumberString) {

    const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
    const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      return '(' + match[1] + ') ' + match[2] + '-' + match[3];
    }
    return null;

  }

  backToSearchList() {
    this.currProcess = 'select';
    this.selectedProvider = null;
  }

  backToSearch() {
    this.currProcess = 'search';
    this.selectedProvider = null;
    // this.providerSearchForm.reset();
    this.getPlacementStrType(null);
  }

  listMap(provider) {
    this.zoom = 13;
    this.defaultLat = 39.29044;
    this.defaultLng = -76.61233;
    // this.fcProviderSearch.map((map) => {
    // (<any>$('#map-popup')).modal('show');
    this.markersLocation = [];
    // if (map.length > 0) {
    // this.fcProviderSearch.forEach((provider) => {
    //  this._ServiceCasePlacementsService.getGoogleMarker(provider.providerdetails[0].address).subscribe(res => {
    //     console.log(res);
    //   });
    const geocoder = new google.maps.Geocoder();
    if (geocoder) {
      geocoder.geocode({ 'address': provider.providerdetails[0].address }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          this.markersLocation = [];
          const marker = { lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() };
          console.log(marker);
          this.lat = marker.lat;
          this.lng = marker.lng;
          const mapLocation = {
            lat: +marker.lat,
            lng: +marker.lng,
            draggable: +true,
            providername: provider.providername,
            addressline1: provider.providerdetails[0].address
          };
          this.markersLocation.push(marker);
          this.defaultLat = marker.lat;
          this.defaultLng = marker.lng;
          (<any>$('#map-popup')).modal('show');
          setTimeout(() => {
            this.showMap = true;
          }, 300);

        } else {
          this._alertService.error('Invalid/no address found.');
          console.log('Geocoding failed: ' + status);
        }
      });
    }

    // json.map(res => {
    //   console.log(res);
    // });
    //   if ( json.results && json.results.length > 0  && json.results[0].geometry.location  ) {
    //   const res = json.results[0].geometry.location;
    //   const mapLocation = {
    //         lat: +res.lat,
    //         lng: +res.lon,
    //         draggable: +true,
    //         providername: provider.providername,
    //         addressline1: provider.providerdetails[0].address
    //     };
    //     this.markersLocation.push(mapLocation);
    //     this.defaultLat = this.markersLocation[0].lat;
    //     this.defaultLng = this.markersLocation[0].lng;
    //   // });
    // // });
    // (<any>$('#map-popup')).modal('show');
    //   }
    //     }
    // });
  }
  mapClose() {
    this.markersLocation = [];
    this.showMap = false;
    // this.zoom = 11;
    // this.defaultLat = 39.219236;
    // this.defaultLng = -76.662834;
  }

  getLicenseCoordinatorName(obj) {
    let name = '';
    if (obj && obj.length > 0) {
      obj.forEach((namObj, index) => {
        name = name + namObj.license_cordinator;
        name = ((index + 1) < obj.length) ? name + ',' : name;
      });
      return name;
    } else {
      return name;
    }

  }

  saveReferal() {
    if (this.referalForm.valid) {
      const referalDetails = this.referalForm.getRawValue();

      const referalDate: any = new Date(referalDetails.startdate);
      const timeSplit = referalDetails.starttime.split(':');
      if (timeSplit && Array.isArray(timeSplit) && timeSplit.length >= 2) {
        let referalTimeHour: number = Number(timeSplit[0]);
        const referalTimeMinPlusMeridiem = timeSplit[1];
        const referalTimeMinAndMeridiem = referalTimeMinPlusMeridiem.split(' ');
        const appointmentTimeMin = referalTimeMinAndMeridiem[0];
        const meridiem = referalTimeMinAndMeridiem[1];
        if (meridiem === 'PM') {
          referalTimeHour += 12;
        }

        referalDate.setHours(referalTimeHour);
        referalDate.setMinutes(appointmentTimeMin);
      }

      // D-07165 Validate: Child removal date should be less than remval date.
      if (this.isChildRemoval && this.childRemovalData && this.childRemovalData[0].hasOwnProperty('removaldate')) {
        const removalDate = new Date(this.childRemovalData[0].removaldate);

        if (referalDate < removalDate) {
          this._alertService.warn('Placement entry begin date should be greater than child removal date.');
          return false;
        }
      }

      referalDetails.isssaapproval = referalDetails.isssaapproval ? referalDetails.isssaapproval : 0;
      if (this.selectedProvider && this.selectedProvider.provider_id) {
        referalDetails.providerid = this.selectedProvider.provider_id;
        referalDetails.contractprogramid = this.selectedProvider.contract_program_id;
        if (this.selectedProvider.affiliate_provider_id) {
          referalDetails.providerorganizationid = this.selectedProvider.affiliate_provider_id;
        } else {
          if (this.selectedProvider.providerdetails && this.selectedProvider.providerdetails.length) {
            referalDetails.providerorganizationid = this.selectedProvider.providerdetails[0].provider_organization_id;
          }
        }
      }
      // referalDetails.providerid = (this.selectedProvider && this.selectedProvider.provider_id) ? this.selectedProvider.provider_id : null;
      referalDetails.placementtypekey = 'PRPL';
      referalDetails.remarks = (referalDetails.remarks) ? referalDetails.remarks : ' ';
      // data = {

      // 'remarks': referalDetails.remarks,
      // 'placementtypekey': 'PRPL',
      // 'providerid': this.selectedProvider.providerid,
      // 'startdate': referalDetails.startdate,
      // 'starttime': referalDetails.starttime,
      // 'service_id': referalDetails.placementStructure,
      // 'ratestructureid': referalDetails.comarRate,
      // 'providersentdate': referalDetails.sentDate,
      // 'providerdesc': referalDetails.description,
      // 'responseacceptedkey': referalDetails.responseAccepted,
      // 'rejectreasonkey': referalDetails.responseRejected,
      // 'isssaapproval': referalDetails.isssaapproval,
      // 'ifcapprovaldate': referalDetails.ifcapprovaldate

      //  };
      this.isSubmitting = true;
      this._ServiceCasePlacementsService.sendApprovalInQueue(referalDetails);
      this._alertService.success('Placement sent for Approval');
    } else {
      this.isSubmitting = false;
      this._alertService.error('Please fill required fields');
    }
  }
  placementStructureSelect(item) {
    this.selectedPlacementStructure = item.value;
    if (this.selectedProvider.provider_category_cd !== '1783') {
      this.referalForm.get('ratestructureid').disable();
      this.referalForm.get('ratestructureid').clearValidators();
      this.referalForm.get('ratestructureid').updateValueAndValidity();
      this.comarRateRequired = false;
    } else if (this.selectedProvider.provider_category_cd === '1783' && this.selectedPlacementStructure.comar_sw === 'Y') {
      const placementStructure = this.referalForm.getRawValue().service_id;
      if(placementStructure != '8' && placementStructure != '76') {
        this.referalForm.patchValue({
          ratestructureid: placementStructure
        });
      }
      
      this.referalForm.get('ratestructureid').disable();
      this.referalForm.get('ratestructureid').clearValidators();
      this.referalForm.get('ratestructureid').updateValueAndValidity();
      this.comarRateRequired = false;
    } else {
      this.referalForm.get('ratestructureid').enable();
      this.referalForm.get('ratestructureid').setValidators([Validators.required]);
      this.referalForm.get('ratestructureid').updateValueAndValidity();
      this.comarRateRequired = true;
    }
    if (item.source.triggerValue === 'Intermediate Foster Care Difficulty of Care') {
      this.referalForm.get('isssaapproval').enable();
      this.referalForm.get('ifcapprovaldate').enable();
    } else {
      this.referalForm.patchValue({
        ifcapprovaldate: null,
        isssaapproval: false,
      });
      this.referalForm.get('isssaapproval').disable();
      this.referalForm.get('ifcapprovaldate').disable();
    }
    const placementSt = this.referalForm.getRawValue().service_id;
    
    if (placementSt != '8' && placementSt != '76') {
      this.referalForm.patchValue({
        ratestructureid: placementSt
      });
    }
    this.referalForm.get('ratestructureid').disable();
    this.getcomarlistValues();
    this.checkForKinshipValidation();
  }






  // const body = {
  //     childcharacteristics: parseInt(formValues.childCharacteristicsid, 10),
  //     placementstructures: parseInt(formValues.placementStrTypeId, 10),
  //     bundledplacementservices: parseInt(formValues.bundledPlcmntServicesTypeId, 10),
  //     providerid: formValues.providerId,
  //     zipcode: parseInt(formValues.zipcode, 10),
  //     localdepartmenthomecaregiver: formValues.isLocalDpt
  // };
  // initProviderSearchForm() {
  //   this.providerSearchForm = this.formBuilder.group({
  //     childcharacteristics: [null],
  //     bundledplacementservices: [null],
  //     otherLocalDeptmntTypeId: [null],
  //     placementstructures: [null],
  //     zipcode: [''],
  //     isLocalDpt: [true],
  //     firstname: [''],
  //     middlename: [''],
  //     lastname: [''],
  //     isgender: [false],
  //     isAge: [true],
  //     providerid: [''],
  //     agemin: null,
  //     agemax: null,
  //     gender: null
  //   });
  //   // TODO: GENDER, needs to be the picklist valuse code so first need get API for the dropdown

  //   // this.providerSearchForm.get('firstname').disable();
  //   // this.providerSearchForm.get('middlename').disable();
  //   // this.providerSearchForm.get('lastname').disable();
  // }

  loadGenderDropdownItems() {
    this._commonService.getArrayList(
      {
        where: { activeflag: 1 },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.GenderTypeUrl + '?filter'
    ).subscribe((genderList) => {
      this.genderDropdownItems = genderList;
    });
    // text: res.typedescription,
    // value: res.gendertypekey
  }



  initReferalForm() {
    this.referalForm = this.formBuilder.group({
      sentDate: [null],
      description: [null],
      responseAccepted: [null],
      responseRejected: [null],
      placementStructure: [''],
      ratestructureid: [''],
      startdate: [null],
      starttime: ['08:00'],
      isssaapproval: [0],
      ifcapprovaldate: [null],
      remarks: ['']
    });
    this.referalForm.get('isssaapproval').disable();
    this.referalForm.get('ifcapprovaldate').disable();
  }





  isdateTimeChanged() {
    this.isDateTime = true;
  }
  getPlacement(page: number) {
    const source = this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: page,
          limit: this.paginationInfo.pageSize,
          where: {
            casenumber: this.daNumber
          },
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fostercarereferallistUrl + '?filter'
      )
      .map((res) => {
        return {
          data: res.data,
          count: res.count
        };
      })
      .share();
    this.placement$ = source.pluck('data');
    if (page === 1) {
      this.placementCount$ = source.pluck('count');
    }
  }


  // D-07665 Start
  getPlacementWithPersonId(page: number, personidtemp: string) {
    const source = this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: page,
          limit: this.paginationInfo.pageSize,
          where: {
            casenumber: this.daNumber,
            personid: personidtemp
          },
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.fostercarereferallistUrl + '?filter'
      )
      .map((res) => {
        return {
          data: res.data,
          count: res.count
        };
      })
      .share();
    this.placement$ = source.pluck('data');
    if (page === 1) {
      this.placementCount$ = source.pluck('count');
    }
  }
  // D-07665 End



  getInvolvedPerson() {
    this.involevedPerson$ = this._commonService
      .getArrayList(
        {
          method: 'get',
          where: this.inputRequest
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
      )
      .map((res) => {
        return res['data'].filter((item) => item.rolename === 'CHILD' || item.rolename === 'RC' || item.rolename === 'AV');
      });
  }




  getResponseRejectedList() {
    this._commonService.getArrayList(new PaginationRequest({
      where: {
        'referencetypeid': 80
      },
      nolimit: true,
      method: 'get'
    }), CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.GetReasonTypes).subscribe(result => {
      this.ResponseRejectedList = result;
    });
  }



  // removePlacementRequest() {
  //     const body = {
  //         'exit_reason_cd': this.exitForm.getRawValue().reasonforexit,
  //         'exit_type_cd': this.exitForm.getRawValue().exitType,
  //         'exit_explanation_tx': this.exitForm.getRawValue().explanation,
  //         'exit_dt': moment(this.exitForm.getRawValue().enddate).format('YYYY-MM-DD'),
  //         'exit_tm': this.exitForm.getRawValue().endtime
  //     };

  //     this._commonService.patch(this.selectedPlacement.placement_id, body, CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.removeplacement).subscribe(result => {
  //         this.selectedChild = null;
  //         this.getPlacement(1);
  //         (<any>$('#exit')).modal('hide');
  //         this._alertService.success('Successfully exited from placement');

  //     });
  // }




  approveOrReject(placementID: string, isApprove: boolean) {
    const request = isApprove ? {
      objectid: placementID,
      eventcode: 'PLTR',
      status: 'Approved',
      comments: 'Placement Exit Approved',
      notifymsg: 'Placement Exit Approved',
      routeddescription: 'Placement Exit Approved'
    } : {
        objectid: placementID,
        eventcode: 'PLTR',
        status: 'Rejected',
        comments: 'Placement Exit Rejected',
        notifymsg: 'Placement Exit Rejected',
        routeddescription: 'Placement Exit Rejected'
      };

    this._commonService.create(request, CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.approveOrReject)
      .subscribe(result => {
        this.getPlacement(1);
        this._alertService.success(`${isApprove ? 'Approved successfully.' : 'Rejected successfully.'}`);

      });
  }

  approvePlacementRequest(placement) {
    const body = {
      'objectid': this.id,
      'eventcode': 'PLAREF',
      'status': 'Approved',
      'comments': 'placement Approved',
      'notifymsg': 'placement Approved',
      'routeddescription': 'placement Approved',
      'approval_status_cd': '3047'
    };
    this._commonService.patch(placement.placement_id, body,
      CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.approvePlacementRequestUrl).subscribe(result => {
        this._alertService.success('Successfully approved placement');
        this.getPlacement(1);
      });
  }

  rejectPlacementRequest(placement) {
    this._commonService.patch(placement.placement_id, { 'approval_status_cd': '3281' }
      , CaseWorkerUrlConfig.EndPoint.DSDSAction.foster_care.rejectPlacementRequestUrl).subscribe(result => {
        this._alertService.success('Successfully rejected placement');
        this.getPlacement(1);
      });
  }


  fcPageChanged(pageEvent) {
    this.paginationInfo.pageNumber = pageEvent.page;
    this.getFcProviderSearch();
  }




  resetSearch() {
    this.providerSearchForm.reset();
    this.referalForm.reset();
    this.referalForm.patchValue({
      starttime: '08:00',
    });

  }
  resetReferal() {
    this.referalForm.reset();
    this.referalForm.patchValue({
      starttime: '08:00',
    });
  }





  goToReferal() {
    this.isSubmitting = false;
    if (this.selectedProvider && this.selectedProvider.vacancy < this.selectedChildren.length) {
      (<any>$('#placement-vacancy-error')).modal('show');
      return;
    }
    this.resetReferal();
    this.referalForm.patchValue({
      providersentdate: new Date()
    });
    // Patch placement Structure
    if (this.selectedProvider) {
      const providerid = this.placementStrType.find(ele => ele.service_nm === this.selectedProvider.placementstructure);
      this.referalForm.patchValue({
        service_id: providerid ? String(providerid.service_id) : null,
      });
      const placementSt = this.referalForm.getRawValue().service_id;
      if (!this.comarValueExclude.includes(placementSt) ) {
        this.referalForm.patchValue({
          ratestructureid: placementSt
        });
      }
      this.checkForKinshipValidation();
    }
    if (this.selectedProvider.provider_category_cd === '1783') {
      // this.referalForm.get('ratestructureid').enable();
      this.referalForm.get('ratestructureid').setValidators([Validators.required]);
      this.referalForm.get('ratestructureid').updateValueAndValidity();
      this.comarRateRequired = true;
    } else {
      this.referalForm.get('ratestructureid').disable();
      this.referalForm.get('ratestructureid').clearValidators();
      this.referalForm.get('ratestructureid').updateValueAndValidity();
      this.comarRateRequired = false;
    }
    this.getResponseAcceptedList();
    this.getResponseRejectedList();
    this.getPlacementStrType(this.selectedProvider.provider_id);
    // this.getPlacementStrType(null);
    if (this.selectedProvider && this.selectedProvider.casecounty) {
      const ischildplacedoutside = this.selectedProvider.casecounty === '1' ? false : true;
      this.referalForm.patchValue({ischildplacedoutside: ischildplacedoutside});
    }
    this.currProcess = 'send';

  }
  goToSearch() {
    this.resetSearch();
    (<any>$('#fc_search')).click();
  }

  // localdptSelected(event) {
  //     console.log(event);

  //     if (event.checked) {
  //         this.providerSearchForm.get('childCharacteristicsid').disable();
  //         this.providerSearchForm.get('bundledPlcmntServicesTypeId').disable();
  //         this.providerSearchForm.get('otherLocalDeptmntTypeId').disable();
  //         this.providerSearchForm.get('placementStrTypeId').disable();
  //         this.providerSearchForm.get('isgender').disable();
  //         this.providerSearchForm.get('isAge').disable();

  //         this.providerSearchForm.get('firstname').enable();
  //         this.providerSearchForm.get('middlename').enable();
  //         this.providerSearchForm.get('lastname').enable();
  //     } else {
  //         this.providerSearchForm.get('childCharacteristicsid').enable();
  //         this.providerSearchForm.get('bundledPlcmntServicesTypeId').enable();
  //         this.providerSearchForm.get('otherLocalDeptmntTypeId').enable();
  //         this.providerSearchForm.get('placementStrTypeId').enable();
  //         this.providerSearchForm.get('isgender').enable();
  //         this.providerSearchForm.get('isAge').enable();

  //         this.providerSearchForm.get('firstname').disable();
  //         this.providerSearchForm.get('middlename').disable();
  //         this.providerSearchForm.get('lastname').disable();
  //     }
  // }


  responseSelect(item) {
    this.selectedResponseStructure = item.value;
    if (this.selectedResponseStructure === RESPONSE_ACCEPTED_YES) {
      this.isResponseAccepted = true;
      this.referalForm.get('providersentdate').setValidators([Validators.required]);
      this.referalForm.get('startdate').setValidators([Validators.required]);
      this.referalForm.get('starttime').setValidators([Validators.required]);
      this.referalForm.get('rejectreasonkey').setValidators(null);
      this.referalForm.get('rejectreasonkey').reset();
      this.referalForm.get('remarks').setValidators(null);
      this.referalForm.patchValue({rejectreasonkey: null});
      this.referalForm.get('rejectreasonkey').disable();
      this.approval = true;
    } else {
      this.isResponseAccepted = true;
      this.approval = false;
      // this.isResponseAccepted = false; // once api accepts statdate as null remove above line
      this.referalForm.get('providersentdate').setValidators(null);
      this.referalForm.get('rejectreasonkey').setValidators([Validators.required]);
      this.referalForm.get('remarks').setValidators([Validators.required]);
      this.referalForm.get('providersentdate').setErrors(null);
      this.referalForm.get('providersentdate').clearValidators();
      this.referalForm.get('startdate').setValidators(null);
      this.referalForm.get('startdate').setErrors(null);
      this.referalForm.get('startdate').clearValidators();
      this.referalForm.get('starttime').setValidators(null);
      this.referalForm.get('starttime').setErrors(null);
      this.referalForm.get('starttime').clearValidators();
      this.referalForm.get('rejectreasonkey').enable();
    }
    this.referalForm.get('providersentdate').updateValueAndValidity();
    this.referalForm.get('startdate').updateValueAndValidity();
    this.referalForm.get('starttime').updateValueAndValidity();
  }




  openTitle4e() {
    (<any>$('#title4e')).modal('show');
  }





  private getAge(dateValue) {
    if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
      const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
      return moment().diff(rCDob, 'years');
    } else {
      return '';
    }
  }


  providerTypeSelected(event) {
    if (event.value === '1') {
      this.isPublicProvider = true;
      this.isPrivateProvider = false;
    } else {
      this.isPublicProvider = false;
      // this.providerSearchForm.get('lastname').clearValidators();
      this.isPrivateProvider = true;
    }
  }

  scShowPlacement(placement) {
    this.selectedPlacement = placement;
    (<any>$('#fc-view')).modal('show');
  }

  getPlacementDates(log, isentry) {
    if (log && log.length) {
      const event = { date: '', time: '' };
      if (isentry === 1) {
        event.date = (log[0].entry_dt) ? log[0].entry_dt : '';
        event.time = (log[0].entry_tm) ? log[0].entry_tm : '';
      } else {
        event.date = (log[0].exit_dt) ? log[0].exit_dt : '';
        event.time = (log[0].exit_tm) ? log[0].exit_tm : '';
      }
      return event.date + ' ' + event.time;
    } else {
      return '';
    }
  }

  pesonDateOfBirth(item) {

    this.reportedChildDob = item.value.dob;
    this.intakeservicerequestactorid = item.value.intakeservicerequestactorid;
    // if (item.value) {
    this.selectedChild = item.value;
    // D-07665 Start
    this.getPlacementWithPersonId(1, this.selectedChild.personid);
    // D-07665 End

    //   this.isAddPlacement = true;
    // } else {
    //   this.isAddPlacement = false;
    // }
    this.placement$
      .map((res) => {
        const id = item.value.cjamspid;
        const blah = res.find((ite) => (ite.clientid === id && item.placementexitdate));
        if (blah) {
          this.isAddPlacement = false;
        } else {
          this.isAddPlacement = true;
        }
      })
      .subscribe();
  }

  checkForKinshipValidation() {
    const placmentSturctureID = this.referalForm.getRawValue().service_id;
    if (placmentSturctureID == 8) {
      this.maxDate = this.kinshipMaxdate;
     } else {
       this.maxDate = new Date();
     }
  }


}
