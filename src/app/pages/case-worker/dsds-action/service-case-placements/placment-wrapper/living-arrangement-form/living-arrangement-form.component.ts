import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ServiceCasePlacementsService } from '../../service-case-placements.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../../../../newintake/newintake-url.config';
import { DropdownModel } from '../../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { CommonHttpService, CommonDropdownsService, AlertService, DataStoreService, SessionStorageService } from '../../../../../../@core/services';
import { Observable, Subject } from 'rxjs/Rx';
import { CountyDetails } from '../../../../../find/_entities/find-entity.module';
import { Router, ActivatedRoute } from '@angular/router';
import { IntakeUtils } from '../../../../../_utils/intake-utils.service';
import { PlacementConstants } from '../../constants';
import * as moment from 'moment';

@Component({
  selector: 'living-arrangement-form',
  templateUrl: './living-arrangement-form.component.html',
  styleUrls: ['./living-arrangement-form.component.scss']
})
export class LivingArrangementFormComponent implements OnInit {
  placementForm: FormGroup;
  referalForm: FormGroup;
  minDate: any;
  endMinDate: any;
  countyDropDownItems = [];
  stateDropDownItems = [];
  livingDropDownItems = [];
  selectedChildren: any[];
  isRunaway = false;
  isRunawayReported = false;
  isClosed = false;
  suggestedAddress$: Observable<any[]>;
  constructor(private _ServiceCasePlacementsService: ServiceCasePlacementsService,
    private _dropDownService: CommonDropdownsService,
    private formBuilder: FormBuilder,
    private _alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private _dataStoreService: DataStoreService,
    private _intakeUtils: IntakeUtils,
    private _httpService: CommonHttpService,
    private storage: SessionStorageService
  ) {
  }

  ngOnInit() {
    this.forminitialize();
    this.minDate = new Date();
    this.endMinDate = new Date();
    this.setFormControlValidators();
    this.loadDropdownItems();
    this._ServiceCasePlacementsService.placementApprovalQueue$.subscribe(response => {
      (<any>$('#placement-ackmt')).modal('show');
    });
    this.placementForm
    .get('startdate')
    .valueChanges.subscribe(result => {
      this.placementForm.patchValue({enddate: null});
      if(result){
       this.endMinDate =  new Date(result);
      }
    });
    this.selectedChildren = this._ServiceCasePlacementsService.selectedChildren;
    this.validatePlacementDates(this.selectedChildren);
    this._ServiceCasePlacementsService.childSelection$.subscribe(children => {
      this.selectedChildren = this._ServiceCasePlacementsService.selectedChildren;
      console.log('Selected Children', this.selectedChildren);
      this.placementForm.patchValue({
        startdate: null, 
        enddate: null
      });
      this.validatePlacementDates(this.selectedChildren);
    });
    
    
    /* if (this._ServiceCasePlacementsService.selectedChildren.length === 0) {
      this.goBack();
    } */
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

  validatePlacementDates(selectedChildren) {
    if(selectedChildren && selectedChildren.length && Array.isArray(selectedChildren)) {
      let  minDob = new Date();
      let minRemovalDate = new Date();
    selectedChildren.forEach((child, index) => {
       if (child.dob) {
         const dob = new Date(child.dob);
         if(dob < minDob) {
           minDob = dob;
         }
       }
       if(child.childremoval && child.childremoval.length && Array.isArray(child.childremoval)) {
         const childRemoval =  child.childremoval[0];
         const removalDate = new Date(childRemoval.removaldate);
         if(removalDate < minRemovalDate || index === 0) {
          minRemovalDate =  removalDate;
         }
       }
     });
     if(minRemovalDate === new Date()) {
       this.minDate = minDob;
       this.endMinDate = minDob;
     } else {
      this.minDate = minRemovalDate;
      this.endMinDate = minRemovalDate;
     }
   }
  }

  close() {
    (<any>$('#placement-ackmt')).modal('hide');
    this.goBack();
  }

  goBack() {
    this._ServiceCasePlacementsService.getChildRemovalInfoAndPlacements().subscribe(response => {
      console.log('reload', response);
      this._ServiceCasePlacementsService.broadCastPageRefresh();
      this.router.navigate(['../../list'], { relativeTo: this.route });
    });
  }

  forminitialize() {
    this.placementForm = this.formBuilder.group({
      placementtypekey: ['LA'],
      livingarrangementtypekey: [null, Validators.required],
      contactname: [null],
      startdate: [null, Validators.required],
      remarks: [null, Validators.required],
      contactphone: [null],
      enddate: [null],
      add1: [null, Validators.required],
      add2: [null],
      cityname: [null],
      statetypekey: [null],
      zipcode: [null],
      countytypekey: [null],
      runawayreported: [null],
      runawayreportnumber: [null],
      runawaynotreportedreason: [null],
      endtime: [null],
      starttime: [null]
    });
  }

  setFormControlValidators() {

    // If living arrangement is Runaway runawayreported is required otherwise not
    const runawayReportedControl = this.placementForm.get('runawayreported');
    const address1Control = this.placementForm.get('add1');
    const runAwayReportNumber = this.placementForm.get('runawayreportnumber');
    const runAwayNotReportedReason = this.placementForm.get('runawaynotreportedreason');
    this.placementForm.get('livingarrangementtypekey')
      .valueChanges
      .subscribe(livingArrangement => {
        if (livingArrangement === PlacementConstants.RUN_AWAY) {
         // runawayReportedControl.setValidators([Validators.required]);
          address1Control.setValidators(null);
          address1Control.setErrors(null);
          address1Control.clearValidators();
        } else {
          address1Control.setValidators([Validators.required]);
          runawayReportedControl.setValidators(null);
          runawayReportedControl.setErrors(null);
          runawayReportedControl.clearValidators();
        }
        runawayReportedControl.updateValueAndValidity();
        address1Control.updateValueAndValidity();
      });
    /* this.placementForm.get('runawayreported')
      .valueChanges
      .subscribe(runAwayReported => {
        if (runAwayReported === 'YES') {
          runAwayReportNumber.setValidators([Validators.required]);
          runAwayNotReportedReason.setValidators(null);
          runAwayNotReportedReason.setErrors(null);
          runAwayNotReportedReason.clearValidators();
        } else {
          runAwayNotReportedReason.setValidators([Validators.required]);
          runAwayReportNumber.setValidators(null);
          runAwayReportNumber.setErrors(null);
          runAwayReportNumber.clearValidators();
        }
        runAwayReportNumber.updateValueAndValidity();
        address1Control.updateValueAndValidity();
      }); */

  }

  
  getSuggestedAddress() {
    if (this.placementForm.value.add1 &&
      this.placementForm.value.add1.length >= 3) {
      this.suggestAddress();
    }
  }

  suggestAddress() {
    this._httpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.placementForm.value.add1,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        NewUrlConfig.EndPoint.Intake.SuggestAddressUrl
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
            this.suggestedAddress$ = result;
          }
        }
      );
  }


  selectedAddress(model) {
    this.placementForm.patchValue({
      add1: model.streetLine ? model.streetLine : '',
      cityname: model.city ? model.city : '',
      statetypekey: model.state ? model.state : ''
    });
    this.loadStateDropdownItems(model.state);
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._httpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        NewUrlConfig.EndPoint.Intake.ValidateAddressUrl
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            setTimeout(() => {
              this.placementForm.patchValue({
                zipcode: result[0].components.zipcode ? result[0].components.zipcode : '',
                countytypekey: result[0].metadata.countyName ? result[0].metadata.countyName : ''
              });
            }, 500);
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  private loadDropdownItems() {
    forkJoin([
      this._dropDownService.getStateList(),
      this._dropDownService.getListByTableID(76)
    ]).subscribe(([stateList, livingArrangements]) => {
      console.log(stateList, livingArrangements);
      this.stateDropDownItems = stateList;
      this.livingDropDownItems = livingArrangements;
    });

  }

  loadStateDropdownItems(countyId) {
      this._httpService
          .getArrayList(
              {
                  where: {
                      activeflag: '1',
                      state: countyId
                  },
                  order: 'countyname asc',
                  method: 'get',
                  nolimit: true
              },
              'admin/county?filter'
          )
          .subscribe(result => {
            this.countyDropDownItems = result;
          });
  }

  submitPlacementForm() {
    console.log('errors', this._intakeUtils.findInvalidControls(this.placementForm));
    if (this.placementForm.invalid) {
      this._alertService.error('Please fill required fields');
    } else {
      if (this._ServiceCasePlacementsService.selectedChildren.length === 0) {
        this._alertService.error('Please select the children');
      }
     // const formData = this.resetUnwantedControls(this.placementForm.getRawValue());
     const formData = this.placementForm.getRawValue();
     formData.startdate = this.convertMatinputTimeToTimestamp(formData.startdate, formData.starttime);
     if (formData.endtime) {
      formData.enddate = this.convertMatinputTimeToTimestamp(formData.enddate, formData.endtime);
     }
      this._ServiceCasePlacementsService.sendApprovalInQueue(formData);
    }

  }

  convertMatinputTimeToTimestamp(date, time) {
    return moment(moment(date).format('MM/DD/YYYY') + ' ' + time).format();
}

  resetUnwantedControls(formValues) {
    if (formValues.livingarrangementtypekey === PlacementConstants.RUN_AWAY) {
      formValues.contactname = null;
      formValues.startdate = null;
      formValues.remarks = null;
      formValues.contactphone = null;
      formValues.enddate = null;
      formValues.add1 = null;
      formValues.add2 = null;
      formValues.cityname = null;
      formValues.statetypekey = null;
      formValues.zipcode = null;
      formValues.countytypekey = null;
    } else {
      formValues.runawayreported = null;
      formValues.runawayreportnumber = null;
      formValues.runawaynotreportedreason = null;
    }
    return formValues;
  }

  onLivingArrangementChange() {
    const livingArrangementKey = this.placementForm.getRawValue().livingarrangementtypekey;
    if (livingArrangementKey === PlacementConstants.RUN_AWAY) {
      this.isRunaway = true;
    } else {
      this.isRunaway = false;
    }
  }

  onRunawayReportedChange() {
    const isRunawayReported = this.placementForm.getRawValue().runawayreported;
    if (isRunawayReported === 'YES') {
      this.isRunawayReported = true;
    } else {
      this.isRunawayReported = false;
    }
  }

}
