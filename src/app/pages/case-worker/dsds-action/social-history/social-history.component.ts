import { Component, OnInit, OnDestroy } from '@angular/core';
import { SocialHistoryService } from './social-history.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';

@Component({
  selector: 'social-history',
  templateUrl: './social-history.component.html',
  styleUrls: ['./social-history.component.scss']
})

export class SocialHistoryComponent implements OnInit, OnDestroy {
  childList = [];
  selectedChild: any;
  selection: any;
  selectedHist: any;
  history: any;
  socialHistoryFormGroup: FormGroup;
  childCardFormGroup:  FormGroup;
  isClosed = false;
  speechData: string;
  speechRecogninitionOn: boolean;
  recognizing = false;
  notification: string;
  constructor(private _SocialHistoryService: SocialHistoryService,
    private _formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _dataStoreService: DataStoreService,
    private storage: SessionStorageService,
    private _speechRecognitionService: SpeechRecognitionService,
    ) { }

  ngOnInit() {
    const childList = this._SocialHistoryService.getChildList();
    this.childList = childList.filter(data => data.removalStatus === 'Approved');
    this.initForm();
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

  initForm() {
    this.socialHistoryFormGroup = this._formBuilder.group({
      placement: [''],
      familyhistory: [''],
      childdesc: [''],
      socialhistoryid : [null],
      personid: [null]
    });

    this.childCardFormGroup = this._formBuilder.group({
      child: [null],
    });
  }

  viewChildInfo(child) {
    if (child &&  child.intakeservicerequestactorid) {
      this.selectedChild = child;

      this._SocialHistoryService.getSocialHistory(child.intakeservicerequestactorid)
      .subscribe(data => {
        if (data && data.length) {
          data.forEach(element => {
            element.placement = element.placement!==null?element.placement : '';
            element.familyhistory = element.familyhistory!==null?element.familyhistory : '';
            element.childdesc  = element.childdesc!==null?element.childdesc : '';
            
          });
          this.history = data;
        }
      });
    }  else  {
      this.history = [];
    }
  }

  selectHist(item) {
    this.selectedHist = item;
    this.socialHistoryFormGroup.patchValue(item);
  }

  resetSelectedChild() {
    this.selectedChild = null;
    this.selectedHist = null;
    this.clearForm();
  }

  clearForm() {
    this.socialHistoryFormGroup.reset();
    this.childCardFormGroup.reset();
  }

  addHist(selectedChild) {
    this._SocialHistoryService.createSocialHistory(selectedChild.intakeservicerequestactorid).subscribe((item) => {
      this._alertService.success('History created successfully!');

      this._SocialHistoryService.getSocialHistory(selectedChild.intakeservicerequestactorid).subscribe((item) => {
        console.log(item);
        console.log(JSON.stringify(item));
        item.forEach(element => {
          element.placement = element.placement!==null?element.placement : '';
          element.familyhistory = element.familyhistory!==null?element.familyhistory : '';
          element.childdesc  = element.childdesc!==null?element.childdesc : '';
          
        });
        this.history = item;
      });
    });
  }

  save() {
    const data = this.socialHistoryFormGroup.getRawValue();
    // this._SocialHistoryService.addSocialHistory(socialHistoryData).subscribe(data => {
    //   this._alertService.success('Social History Information Saved Successfully');
    //   this.resetSelectedChild();
    // });
    this._SocialHistoryService.patchData(data)
    .subscribe(
      response => {
        this._alertService.success('Social history information saved successfully!');

        this._SocialHistoryService.getSocialHistory(this.selectedChild.intakeservicerequestactorid)
      .subscribe(data => {
        if (data && data.length) {
          data.forEach(element => {
            element.placement = element.placement!==null?element.placement : '';
            element.familyhistory = element.familyhistory!==null?element.familyhistory : '';
            element.childdesc  = element.childdesc!==null?element.childdesc : '';
            
          });
          this.history = data;
        }
      });
        // this.resetSelectedChild();
      },
      error => {
        this._alertService.error('Error in entering social history information!');
      }
    );
  }
  activateSpeechToText(type): void {
    this.recognizing = type;
    this.speechRecogninitionOn = !this.speechRecogninitionOn;
    if (this.speechRecogninitionOn) {
      this._speechRecognitionService.record().subscribe(
        // listener
        (value) => {
          this.speechData = value;
          switch (type) {
            case 'placement':
              const placement = this.socialHistoryFormGroup.getRawValue().placement;
              this.socialHistoryFormGroup.patchValue({ placement: placement + ' ' + this.speechData });
              break;
            case 'familyhistory':
              const familyhistory = this.socialHistoryFormGroup.getRawValue().familyhistory;
              this.socialHistoryFormGroup.patchValue({ familyhistory: familyhistory + ' ' + this.speechData });
              break;
            case 'childdesc':
              const childdesc = this.socialHistoryFormGroup.getRawValue().childdesc;
              this.socialHistoryFormGroup.patchValue({ childdesc: childdesc + ' ' + this.speechData });
              break;
            default: break;
          }
        },
        // errror
        (err) => {
          console.log(err);
          this.recognizing = false;
          if (err.error === 'no-speech') {
            this.notification = `No speech has been detected. Please try again.`;
            this._alertService.warn(this.notification);
            this.activateSpeechToText(type);
          } else if (err.error === 'not-allowed') {
            this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
            this._alertService.warn(this.notification);
            // this.activateSpeechToText(type);
          } else if (err.error === 'not-microphone') {
            this.notification = `Microphone is not available. Please verify the connection of your microphone and try again.`;
            this._alertService.warn(this.notification);
            // this.activateSpeechToText(type);
          }
        },
        // completion
        () => {
          this.speechRecogninitionOn = true;
          console.log('--complete--');
          this.activateSpeechToText(type);
        }
      );
    } else {
      this.recognizing = false;
      this.deActivateSpeechRecognition();
    }
  }

  deActivateSpeechRecognition() {
    this.speechRecogninitionOn = false;
    this._speechRecognitionService.destroySpeechObject();
  }

  ngOnDestroy(): void {
    this._speechRecognitionService.destroySpeechObject();
  }

}
