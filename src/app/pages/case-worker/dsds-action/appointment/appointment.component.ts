import { Component, Input, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs/Rx';
import { AuthService, CommonHttpService, AlertService, DataStoreService, CommonDropdownsService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { PaginationInfo, DropdownModel } from '../../../../@core/entities/common.entities';
import { RoutingUser } from '../../../cjams-dashboard/_entities/dashBoard-datamodel';
import * as moment from 'moment';

import { IntakeAppointment, TitleDetails } from '../../../newintake/my-newintake/_entities/newintakeSaveModel';
import { InvolvedPerson } from '../../_entities/caseworker.data.model';
import { GenericService } from '../../../../@core/services/generic.service';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { ActivatedRoute } from '@angular/router';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { HttpService } from '../../../../@core/services/http.service';
import { Role } from '../involved-persons/_entities/involvedperson.data.model';
import { AppConstants } from '../../../../@core/common/constants';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
declare var $: any;
const APPOINTMENT_SCHEDULED = 'Scheduled';
const APPOINTMENT_COMPLETED = 'Completed';
const APPOINTMENT_RESCHEDULED = 'Rescheduled';
const DEFAULT_APPOINTMENT_TITLE = 'Intake interview';
const SCREENING_WORKER = 'SCRNW';
const OTHER_APPOINTMENT_TITLE = 'OTHER';
const YOUTH_ORIENTATION_TITLE = 'YOP';
const ADMISSION_APPOINTMENT_TITLE = 'ADM_INTW';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'appointment',
    templateUrl: './appointment.component.html',
    styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent implements OnInit, AfterViewInit {
    id: string;
    daNumber: string;
    addedPersons: InvolvedPerson[] = [];
    appointmentForm: FormGroup;
    currentUser: AppUser;
    intakeWorkers$: Observable<RoutingUser[]>;
    intakeWorkerList: RoutingUser[] = [];
    times: any[] = [];
    titleDropdown: TitleDetails[] = [];
    appointments: IntakeAppointment[] = [];
    isEditAppointment = false;
    isViewAppointment = false;
    isNotesRequired = false;
    showyouth = false;
    actionText = 'Create';
    titleText = 'Create';
    isYouthSelectedforAppmnt: boolean;
    isParentSelectedforAppmnt: number;
    appointmentInAction: IntakeAppointment;
    maxDate = new Date();
    minDate = new Date();
    relations: any = [];
    roles: any = [];
    completionNotes = '';
    appointmentHistoryObj: IntakeAppointment[];
    notes = 'Notes...';
    readOnly = false;
    token: AppUser;
    isOtherAppointmentTitle = false;
    isDjs: boolean;
    constructor(
        private formBuilder: FormBuilder,
        private _httpService: CommonHttpService,
        private _authService: AuthService,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService,
        private _service: GenericService<InvolvedPerson>,
        private route: ActivatedRoute,
        private _dropDownService: CommonDropdownsService
    ) { }

    ngOnInit() {
        this.loadIntakeWorkers();
        this.getTitle();
        this.maxDate.setDate(new Date().getDate() + 10);
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.currentUser = this._authService.getCurrentUser();
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.initializeAppointmentForm();
        this.isYouthSelectedforAppmnt = false;
        this.isParentSelectedforAppmnt = 0;

        this.getAppointments();
        this.getInvolvedPerson();
        this.times = this.generateTimeList(false);
        this.token = this._authService.getCurrentUser();
        this.isDjs = this._authService.isDJS();
    }
    ngAfterViewInit() {
        const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
        if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('i')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
            (<any>$('th a')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
        }
    }
    private initializeAppointmentForm() {
        this.token = this._authService.getCurrentUser();
        console.log('Token Info:', this.token);
        this.appointmentForm = this.formBuilder.group({
            id: [''],
            title: [''],
            titleid: [''],
            appointmentdate: [''],
            appointmentDate: [''],
            appointmentTime: ['08:00'],
            appointmentworkerid: null,
            actors: [''],
            notes: [''],
            worker: [''],
            youthid: [''],
            youthssn: [''],
            youthname: ['']
        });
    }
    getAppointments() {
        this._dataStoreService.currentStore.subscribe(store => {
            if (store['dsdsActionsSummary']) {
                const actionSummary = store['dsdsActionsSummary'];
                const jsonData = actionSummary['intake_jsondata'];
                const appointments = jsonData ? jsonData['appointments'] : [];
                appointments.forEach(appointmentObj => {
                    const app = this.appointments.find(appmntObj => appmntObj.id === appointmentObj.id);
                    if (!app) {
                        this.appointments.push(appointmentObj);
                    }
                    // this.appointments = [...appointments, ...this.appointments];
                });
            }
        });
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get',
                    where: {
                        intakeserviceid: this.id,
                        appointmentid: null
                    }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.appointment.getAppointmentList + '?filter'
            )
            .subscribe(caseworkerAppointments => {
                if (caseworkerAppointments) {
                    this.appointments = [...this.appointments, ...caseworkerAppointments];
                    // response.forEach(appointment => {
                    //     this.appointments.push(appointment);
                    // });
                }
            });
    }

    getTitle() {
        if (this._authService.isDJS()) {
            this._dropDownService.getListByTableID('135').subscribe(result => {
                this.titleDropdown = result.map(data => {
                    return {titleid: data.ref_key, titlekey: data.description};
                });
            });
        } else {
            this.titleDropdown = [
                {
                    titleid: 'YOP',
                    titlekey: ' Youth Orientation Program'
                },
                {
                    titleid: 'COP',
                    titlekey: 'Community Orientation Appointment'
                },
                {
                    titleid: 'ADM_INTW',
                    titlekey: 'Admissions Interview'
                },
                {
                    titleid: 'OTHER',
                    titlekey: 'Other'
                }
            ];
        }
    }

    getInvolvedPerson() {
        this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeservreqid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            )
            .subscribe(response => {
                if (response.data) {
                    this.addedPersons = response.data;
                }
            });
    }
    getPersonName(personId: string) {
        const person = this.addedPersons.find(p => p.personid === personId);
        if (person) {
            return person.lastname + ',' + person.firstname;
        }
    }
    generateAppointmentID() {
        return new Date().getTime();
    }

    resetForm() {
        this.appointmentForm.enable();
        this.appointmentForm.reset();
        this.appointmentForm.patchValue({ appointmentTime: '08:00' });
        this.isEditAppointment = false;
        this.isViewAppointment = false;
        this.isParentSelectedforAppmnt = 0;
        this.isYouthSelectedforAppmnt = false;
        this.showyouth = false;
    }

    showHistory(appointment) {
        console.log(appointment);
        this.appointmentHistoryObj = appointment.history;
    }

    getCWAppointmentHistory(appointment) {
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get',
                    where: {
                        intakeserviceid: appointment.intakeserviceid,
                        appointmentid: appointment.id
                    }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.appointment.getAppointmentHistory + '?filter'
            )
            .subscribe(response => {
                if (response) {
                    if (this._authService.isDJS()) {
                        this.appointmentHistoryObj = response.map(data => {
                            data.titleid = data.title;
                            return data;
                        });
                    } else {
                        this.appointmentHistoryObj = response;
                    }
                }
            });
    }

    updateRescheduledAppointmentWithHistory(rescheduledAppointment) {
        const appointmentIndex = this.appointments.findIndex(appointment => rescheduledAppointment.id === appointment.id);
        const histories = [...this.appointmentInAction.history];
        this.appointmentInAction.history = [];
        histories.push(this.appointmentInAction);
        rescheduledAppointment.history = [...histories];
        this.appointments[appointmentIndex] = rescheduledAppointment;
        console.log('updated', this.appointments);
    }

    updateScheduledAppointment(scheduledAppointment) {
        const appointmentIndex = this.appointments.findIndex(appointment => scheduledAppointment.id === appointment.id);
        this.appointments[appointmentIndex] = scheduledAppointment;
        console.log('updated', this.appointments);
    }

    private setFormValidity() {
        if (this.isEditAppointment) {
            this.isNotesRequired = true;
        } else {
            this.isNotesRequired = false;
        }
    }

    getappointmentDate(date) {
        return moment(date).format('YYYY-MM-DD');
    }

    getappointmentTime(date) {
        return moment.utc(date).format('hh:mm A');
    }

    onEditAppointment(appointment) {
        this.actionText = 'Update';
        this.titleText = 'Edit';
        this.isEditAppointment = true;
        this.appointmentInAction = appointment;
        appointment.appointmentTime = moment.utc(appointment.appointmentdate).format('HH:mm');
        appointment.appointmentDate = moment(appointment.appointmentdate).format('YYYY-MM-DD');
        appointment.titleid = appointment.title;
        this.appointmentForm.patchValue(appointment);

        this.appointmentForm.patchValue({ actors: appointment.actors.map(actor => actor.actorid) });
        this.setFormValidity();
        this.processTitle();
    }

    confirmDelete(appointment) {
        this.appointmentInAction = appointment;
    }

    isDateTimeChanged(appointment) {
        if (appointment.appointmentDate === this.appointmentInAction.appointmentDate && appointment.appointmentTime === this.appointmentInAction.appointmentTime) {
            return false;
        }

        return true;
    }

    processTitle() {
        const tilteID = this.appointmentForm.getRawValue().titleid;
        switch (tilteID) {
            case ADMISSION_APPOINTMENT_TITLE:
                this.showyouth = true;
                this.appointmentForm.controls['worker'].disable();
                this.appointmentForm.controls['youthid'].disable();
                this.appointmentForm.controls['youthname'].disable();
                this.appointmentForm.controls['youthssn'].disable();
                this.appointmentForm.controls['actors'].clearValidators();
                const data = this.addedPersons.find((res) => res.rolename === 'Youth');
                this.appointmentForm.patchValue({
                    worker: this.token.user.userprofile.displayname,
                    youthid: data.cjamspid,
                    youthname: data.firstname + ' ' + data.lastname,
                    youthssn: data.ssn,
                    title: this.titleDropdown
                });
                break;
            case OTHER_APPOINTMENT_TITLE:
                this.isOtherAppointmentTitle = true;
                this.showyouth = false;
                break;

            default:
                this.showyouth = false;
                this.isOtherAppointmentTitle = false;
                this.appointmentForm.controls['actors'].setValidators(Validators.required);


        }

        this.appointmentForm.controls['actors'].updateValueAndValidity();
        if (tilteID !== OTHER_APPOINTMENT_TITLE) {
            const title = this.titleDropdown.find(appointmentTitle => appointmentTitle.titleid === tilteID);
            this.appointmentForm.patchValue({
                title: title.titlekey
            });
        } else {
            this.appointmentForm.patchValue({
                title: ''
            });
        }
    }
    initAppointmentForm() {
        this.actionText = 'Create';
        this.titleText = 'Create';
        this.appointmentForm.patchValue({ titleid: YOUTH_ORIENTATION_TITLE, appointmentTime: '08:00' });
        this.processTitle();
    }

    hasYouthAndParentOrGaurdian() {
        return this.hasYouth() && this.hasParentOrGaurdian();
    }

    getSelectedPersons() {
        const appointmentForm = this.appointmentForm.getRawValue();
        const selectedActors = appointmentForm.actors;
        if (selectedActors) {
            return selectedActors.map(actorid => this.getPerson(actorid));
        }
        return null;
    }

    hasPersonObject(propertyKey: string, role: string) {
        const persons = this.getSelectedPersons();
        let isRoleFound = false;
        if (persons) {
            const involvedperson = persons.find(person => person[propertyKey] === role);
            if (involvedperson) {
                isRoleFound = true;
            } else {
                isRoleFound = false;
            }
        }
        return isRoleFound;

    }

    hasYouth() {
        return this.hasPersonObject('rolename', AppConstants.INVOLVED_PERSON_ROLE.YOUTH);
    }

    hasParentOrGaurdian() {
        const father = this.hasPersonObject('relationship', AppConstants.INVOLVED_PERSON_ROLE.FATHER);
        const mother = this.hasPersonObject('relationship', AppConstants.INVOLVED_PERSON_ROLE.MOTHER);
        const guardian = this.hasPersonObject('relationship', AppConstants.INVOLVED_PERSON_ROLE.GUARDIAN);
        return father || mother || guardian;
    }

    createOrUpdateAppointment() {
        if (this.appointmentForm.valid) {
            const appointmentForm = this.appointmentForm.getRawValue();
            const titleID = this.appointmentForm.getRawValue().titleid;
            if (!this.hasYouthAndParentOrGaurdian() && titleID !== ADMISSION_APPOINTMENT_TITLE) {
                this._alertService.error('Please Choose Parent / Guardian and Youth');
            } else {
                if (this.isEditAppointment) {
                    const appointmentId = this.appointments.findIndex(appointment => appointment.id === this.appointmentInAction.id);
                    const editAppointmentObject = this.appointments.find(appointment => appointment.id === this.appointmentInAction.id);
                    const AppointmentAction = this.isDateTimeChanged(appointmentForm) ? APPOINTMENT_RESCHEDULED : APPOINTMENT_SCHEDULED;
                    const editAppointment = this.createAppointmentObject(AppointmentAction);
                    editAppointment.servreqaptmtid = this.appointmentInAction.servreqaptmtid;
                    editAppointment.id = this.appointmentInAction.id;
                    editAppointment.title = editAppointment.titleid;
                    this._commonHttpService.create(editAppointment, CaseWorkerUrlConfig.EndPoint.DSDSAction.appointment.addupdateAppointment).subscribe(
                        res => {
                            if (appointmentId !== -1) {
                                this.appointments[appointmentId] = res;
                            }

                            this._alertService.success('Appointment Updated successfully!');
                            this.appointmentForm.reset();
                        },
                        error => {
                            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                            // this.appointmentForm.reset();
                        }
                    );
                } else {
                    const newAppointment = this.createAppointmentObject(APPOINTMENT_SCHEDULED);
                    newAppointment.id = this.generateAppointmentID();
                    newAppointment.title = newAppointment.titleid;
                    this._commonHttpService.create(newAppointment, CaseWorkerUrlConfig.EndPoint.DSDSAction.appointment.addupdateAppointment).subscribe(
                        res => {
                            this.appointments.push(res);
                            this.appointmentForm.reset();
                            this._alertService.success('Appointment Created successfully!');
                        },
                        error => {
                            // this.appointmentForm.reset();
                            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                        }
                    );
                }

                this.resetForm();

                (<any>$('#intake-appointment')).modal('hide');
            }
        } else {
            this._alertService.error('Please fill the required fields');
        }
        this.getAppointments();
    }

    createAppointmentObject(status) {
        let actors = [];
        if (this.appointmentForm.getRawValue().titleid === ADMISSION_APPOINTMENT_TITLE) {
            const youth = this.addedPersons.find((res) => res.rolename === 'Youth');
            actors = [{ actorid: youth.personid, isoptional: false }];
        } else {
            actors = this.createActors();
        }

        const appointmentObject = new IntakeAppointment();
        const appointmentForm = this.appointmentForm.getRawValue();
        appointmentObject.title = appointmentForm.title;
        appointmentObject.titleid = appointmentForm.titleid;
        appointmentObject.youthid = appointmentForm.youthid;
        appointmentObject.youthname = appointmentForm.youthname;
        appointmentObject.youthssn = appointmentForm.youthssn;
        appointmentObject.worker = appointmentForm.worker;
        appointmentObject.status = status;
        appointmentObject.intakeserviceid = this.id;
        appointmentObject.notes = appointmentForm.notes;
        appointmentObject.isChanged = false;
        appointmentObject.appointmentworkerid = appointmentForm.appointmentworkerid;
        appointmentObject.appointmentworkername = this.token.user.userprofile.displayname;
        appointmentObject.scheduledBy = this.currentUser.user.userprofile.fullname;
        // modal.hearingtime = moment.utc(modal.hearingdatetime).format('HH:mm');
        //  modal.hearingdatetime = moment(modal.hearingdatetime).format('YYYY-MM-DD');
        // const appointmentDate: any = new Date(appointmentForm.appointmentDate);
        // const timeSplit = appointmentForm.appointmentTime.split(':');
        // const appointmentTimeHour = timeSplit[0];
        // const appointmentTimeMin = timeSplit[1];

        // appointmentDate.setHours(appointmentTimeHour);
        // appointmentDate.setMinutes(appointmentTimeMin);

        // appointmentObject.appointmentDate = appointmentDate;
        // appointmentObject.appointmentTime = appointmentForm.appointmentTime;
        appointmentObject.appointmentdate = moment(this.appointmentForm.value.appointmentDate).format('YYYY/MM/DD') + ' ' + this.appointmentForm.value.appointmentTime;
        appointmentObject.actors = actors;

        return appointmentObject;
    }

    createActors() {
        return this.appointmentForm.value.actors.map(actor => {
            const person = this.getPerson(actor);
            return { actorid: actor, isoptional: false, firstName: person.firstname, lastName: person.lastname };
        });
    }

    getPerson(personId: string) {
        const person = this.addedPersons.find(p => p.personid === personId);
        if (person) {
            return person;
        }
        return null;
    }

    deleteAppointmentConfirm() {
        const appointmentIndex = this.appointments.findIndex(appointment => appointment.id === this.appointmentInAction.id);
        // tslint:disable-next-line:max-line-length
        this._commonHttpService.remove(this.appointmentInAction.servreqaptmtid, this.appointmentInAction, CaseWorkerUrlConfig.EndPoint.DSDSAction.appointment.deleteAppointment).subscribe(
            result => {
                this._alertService.success('Deleted Successfully');
                this.appointments.splice(appointmentIndex, 1);
                this.resetForm();
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    isCaseWorkerAppointment(appointment) {
        return !appointment.intakeWorkerId;
    }

    onCompleteAppointment(appointment) {
        this.completionNotes = '';
        this.appointmentInAction = appointment;
        this.readOnly = false;
    }

    appointmentCompleteConfirm() {
        if (this.appointmentInAction) {
            const appointmentIndex = this.appointments.findIndex(appointment => this.appointmentInAction.id === appointment.id);
            if (this.completionNotes !== '') {
                this.appointments[appointmentIndex].notes = this.completionNotes;
                this.appointments[appointmentIndex].status = APPOINTMENT_COMPLETED;
                this._commonHttpService.create(this.appointments[appointmentIndex], CaseWorkerUrlConfig.EndPoint.DSDSAction.appointment.addupdateAppointment).subscribe(
                    res => {
                        // this.appointments.push(res);
                        this.appointmentForm.reset();
                        this._alertService.success('Appointment Completed successfully!');
                        (<any>$('#complete-appointment-popup')).modal('hide');
                    },
                    error => {
                        // this.appointmentForm.reset();
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
            } else {
                this._alertService.error('Completion notes is required');
            }
        }
    }

    openAppointmentComments(appointment) {
        this.completionNotes = appointment.notes;
        this.readOnly = true;
    }

    private loadIntakeWorkers() {
        this._commonHttpService
            .getPagedArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    page: 1,
                    limit: 50,
                    order: 'username'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.appointment.intakeWorkerList + '?filter'
            )
            .subscribe(response => {
                this.intakeWorkerList = response.data;
            });
    }

    getIntakeWorkerName(appointmentworkerid: string) {
        const intakeWorker = this.intakeWorkerList.find(iw => iw.userid === appointmentworkerid);
        if (intakeWorker) {
            return intakeWorker.username;
        }
        return null;
    }

    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = tt % 60; // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }
}
