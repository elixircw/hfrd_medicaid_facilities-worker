export class AllegationSearchDetail {
    investigationallegationid: string;
    intakeservicerequestactorid: string;
    investigationid: string;
    allegationid: string;
    allegationname: string;
    name: string;
    reported: boolean;
    indicators: string;
    financial: string;
    investigationallegationactor: InvestigationAllegationActor[];
    investigationallegation: InvestigationAllegation;
    insertedby: string;
    updatedby: string;
    indicatorname: string;
    isCollapsed = true;
}
export class InvestigationAllegation {
    investigationallegationid: string;
    indicators: string;
    investigationid: string;
    allegationid: string;
    reported: boolean;
    activeflag: number;
    financial: string;
}
export class InvestigationAllegationActor {
    investigationallegationid: string;
    intakeservicerequestactorid: string;
    investigationallegationactorid: string;
    investigationallegationstatustypekey: string;
    edl: string;
    addedindicators: string;
    investigationallegationstatustype: InvestigationAllegationStatusType;
    statementofevidence: string;
    intakeservicerequestpersontypekey: string;
    intakeservicerequestactor: IntakeServiceRequestActor;
}
export class InvestigationAllegationStatusType {
    investigationallegationstatustypekey: string;
    typedescription: string;
}

export class IntakeServiceRequestActor {
    actorid: string;
    intakeservicerequestactorid: string;
    intakeservicerequestpersontypekey: string;
    actor: Actor;
    isAdded: boolean;
}
export class Actor {
    actorid: string;
    personid: string;
    actortype: string;
    Person: Person;
}
export class Person {
    firstname: string;
    lastname: string;
    middlename: string;
    personid: string;
}
export class Indicator {
    indicatorid: string;
    indicatorname: string;
    isSelected: boolean;
}
