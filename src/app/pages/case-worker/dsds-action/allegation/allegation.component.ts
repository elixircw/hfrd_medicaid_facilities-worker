import value from '*.json';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { DropdownModel } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, DataStoreService } from '../../../../@core/services';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { DSDSActionSummary } from '../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { AllegationSearchDetail, InvestigationAllegationActor, Indicator, IntakeServiceRequestActor } from './_entities/allegation.data.model';
import { map } from 'rxjs/operators';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'allegation',
    templateUrl: './allegation.component.html',
    styleUrls: ['./allegation.component.scss']
})
export class AllegationComponent implements OnInit {
    selectedActors: InvestigationAllegationActor;
    selectedActor: IntakeServiceRequestActor[];
    indicatorList: Indicator[];
    id: string;
    daNumber: string;
    investigationId: string;
    daTypeId: string;
    daSubTypeId: string;
    allegtionEditForm: FormGroup;
    personForm: FormGroup;
    allegationIndicatorForm: FormGroup;
    allegationForm: FormGroup;
    allegationIndicator: DropdownModel[];
    dsdsActionsSummary = new DSDSActionSummary();
    investigationSearch: AllegationSearchDetail = new AllegationSearchDetail();
    allegationModelData: AllegationSearchDetail = new AllegationSearchDetail();
    selectedAllegation: AllegationSearchDetail = new AllegationSearchDetail();
    allegationItems: AllegationSearchDetail[];
    linkPersonList: IntakeServiceRequestActor[] = [];
    allegationSearch: AllegationSearchDetail[] = [];
    allegationEdit: InvestigationAllegationActor;
    allegationDropdownItems$: Observable<DropdownModel[]>;
    statusDropDownItems$: Observable<DropdownModel[]>;
    private investigationallegationid: string;
    private investigationid: string;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _allegationSevice: GenericService<AllegationSearchDetail>,
        private _actorService: GenericService<IntakeServiceRequestActor>,
        private _allegationEditService: GenericService<InvestigationAllegationActor>,
        private _investigationIdService: GenericService<DSDSActionSummary>,
        private _commonHttpService: CommonHttpService,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
    }

    ngOnInit() {
        this.loadDropdown();
        this.allegationIndicatorForm = this.formBuilder.group({
            allegationid: ['', Validators.required]
        });
        this.allegtionEditForm = this.formBuilder.group({
            investigationallegationstatustypekey: [''],
            statementofevidence: [''],
            addedindicators: [''],
            financial: ['']
        });
        this.personForm = this.formBuilder.group({
            isAdded: ['']
        });
        this.allegationForm = this.formBuilder.group({
            alegation: ['']
        });
        this.getActionSummary();
    }
    allegationSearches(event: any) {
        this.investigationallegationid = null;
        this.investigationid = this.investigationId;
        if (event) {
            if (event.value) {
                const item = event.value.split('$');
                this.investigationallegationid = item[0];
            }
        }
        this._allegationSevice
            .getArrayList(
                {
                    method: 'post',
                    where: {
                        investigationallegationid: this.investigationallegationid,
                        investigationid: this.investigationid
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationInvestigationUrl
            )
            .subscribe(
                (result) => {
                    this.allegationSearch = result;
                    if (this.investigationSearch.allegationid) {
                        this.allegationSearch.map((res) => {
                            if (res.allegationid === this.investigationSearch.allegationid) {
                                res.isCollapsed = true;
                            }
                        });
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }
    allegationEditList(model: AllegationSearchDetail, actor) {
        this.allegtionEditForm.markAsPristine();
        this.selectedAllegation = model;
        this.selectedActors = actor;
        this._allegationEditService.getSingle({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationEditUrl + actor.investigationallegationactorid).subscribe(
            (result) => {
                this.allegationEdit = result;
                this.statusDropdown();
                this.getIndicatorsList(model.allegationid);
                this.allegationPatchValue(model, actor);
            },
            (error) => {
                console.log(error);
            }
        );
    }
    allegationPatchValue(allegation, actor) {
        this.allegtionEditForm.patchValue({
            financial: allegation.financial,
            statementofevidence: actor.statementofevidence,
            investigationallegationstatustypekey: actor.investigationallegationstatustypekey,
            addedindicators: actor.addedindicators
        });
    }
    allegationList() {
        this._commonHttpService
            .getArrayList(
                {
                    where: {
                        intakeservicereqtypeid: this.daTypeId,
                        intakeservicereqsubtypeid: this.daSubTypeId,
                        investigationid: this.investigationId
                    },
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationIndicatorUrl + '?filter'
            )
            .subscribe((result) => {
                this.allegationItems = result;
                this.allegationIndicator = this.allegationItems.map(
                    (item) =>
                        new DropdownModel({
                            text: item.name,
                            value: item.allegationid
                        })
                );
            });
    }

    linkPerson(model: AllegationSearchDetail) {
        this.investigationSearch = model;
        this.allegationSearch.map((res) => {
            if (res.allegationid === this.investigationSearch.allegationid) {
                res.isCollapsed = true;
            }
        });
        this._actorService
            .getPagedArrayList(
                {
                    method: 'get',
                    where: {
                        intakeserviceid: this.id
                    },
                    limit: '5',
                    page: '1'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.LinkPersonUrl + model.investigationallegationid + '?filter'
            )
            .subscribe((result) => {
                this.linkPersonList = result.data;
            });
        this.personForm.reset();
        this.allegtionEditForm.reset();
    }
    allegationOnChange(option: any) {
        this.allegationModelData.allegationid = option.value;
        this.allegationModelData.name = option.label;
    }
    addAllegation() {
        this._allegationSevice.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationAddUpdateUrl;
        this.allegationModelData = Object.assign({
            investigationid: this.investigationId,
            allegationid: this.allegationIndicatorForm.value.allegationid,
            name: this.allegationModelData.name,
            reported: false
        });
        this._allegationSevice.create(this.allegationModelData).subscribe(
            (response) => {
                if (response) {
                    this.allegationSearches(null);
                    this.loadDropdown();
                    this.allegationIndicatorForm.reset();
                    this.allegationIndicatorForm.patchValue({
                        allegationid: ''
                    });
                    this._alertService.success('New allegation added successfully');
                    (<any>$('#myModal-new-allegation')).modal('hide');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                this.allegationIndicatorForm.reset();
                this.allegationIndicatorForm.patchValue({
                    allegationid: ''
                });
            }
        );
    }
    selectLinkPerson(model: IntakeServiceRequestActor, event) {
        this.linkPersonList.map((item) => {
            if (item.intakeservicerequestactorid === model.intakeservicerequestactorid) {
                item.isAdded = event.target.checked;
            }
        });
        const selectedActor = [...this.linkPersonList.filter((item) => item.isAdded).map((item) => item.intakeservicerequestactorid)];
        this.selectedActor = selectedActor.map((item) => {
            return JSON.parse(
                JSON.stringify({
                    intakeservicerequestactorid: item
                })
            );
        });
    }
    addLinkPerson() {
        this._allegationSevice.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AddLinkPersonUrl + this.investigationSearch.investigationallegationid;
        this.allegationModelData = Object.assign({
            data: this.selectedActor
        });
        this._allegationSevice.create(this.allegationModelData).subscribe(
            (response) => {
                if (response) {
                    this.allegationSearches(event);
                    this.loadDropdown();
                    this._alertService.success('Person saved successfully');
                    (<any>$('#myModal-link-allegation')).modal('hide');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    selectIndicators($event) {
        const indicatorName = $event.target.value;
        const isChecked = $event.target.checked;
        this.indicatorList.map((item) => {
            if (item.indicatorname === indicatorName) {
                item.isSelected = isChecked;
            }
        });
        this.allegtionEditForm.markAsDirty();
    }
    allegationUpdate() {
        const selectedIndicatorList = this.indicatorList
            .filter((item) => item.isSelected === true)
            .map(function(elem) {
                return elem.indicatorname;
            })
            .join(',');
        this.allegationModelData = Object.assign({
            investigationallegationid: this.selectedAllegation.investigationallegationid,
            intakeservicerequestactorid: this.selectedActors.intakeservicerequestactorid,
            investigationallegationstatustypekey: this.allegtionEditForm.value.investigationallegationstatustypekey,
            statementofevidence: this.allegtionEditForm.value.statementofevidence,
            addedindicators: selectedIndicatorList
        });
        this.allegationModelData.investigationallegation = Object.assign({
            investigationallegationid: this.selectedAllegation.investigationallegationid,
            financial: this.allegtionEditForm.value.financial
        });
        this._commonHttpService
            .getArrayList(
                {
                    method: 'post',
                    where: this.allegationModelData
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationUpdateUrl + '/' + this.selectedActors.investigationallegationactorid
            )
            .subscribe(
                (response) => {
                    this.allegationSearches(null);
                    if (response) {
                        this._alertService.success('Allegation update successfully');
                        (<any>$('#myModal-edit-allegation')).modal('hide');
                        this.allegtionEditForm.reset();
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }
    toggleAllegation(model: AllegationSearchDetail) {
        this.allegationSearch.map((res) => {
            if (res.allegationid === model.allegationid) {
                res.isCollapsed = !res.isCollapsed;
            }
        });
    }
    deleteAllegation() {
        this._allegationSevice.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationDeleteUrl;
        this._allegationSevice.remove(this.allegationModelData.investigationallegationid).subscribe(
            (response) => {
                this.loadDropdown();
                this.allegationSearches(event);
                this._alertService.success('Allegation deleted successfully');
                (<any>$('#delete-popup')).modal('hide');
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                (<any>$('#delete-popup')).modal('hide');
            }
        );
    }

    confirmDelete(id) {
        this.allegationModelData = id;
        this.allegationSearch.map((res) => {
            if (res.allegationid === this.investigationSearch.allegationid) {
                res.isCollapsed = true;
            }
        });
        (<any>$('#delete-popup')).modal('show');
    }

    deleteActor(model) {
        this._allegationSevice.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationDeleteUrl;
        this._allegationSevice
            .remove(model.investigationallegationid, {
                investigationallegationactorid: model.investigationallegationactorid
            })
            .subscribe(
                (response) => {
                    this.loadDropdown();
                    this.allegationSearches(event);
                    this._alertService.success('Actor deleted successfully');
                    (<any>$('#delete-popup')).modal('hide');
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    (<any>$('#delete-popup')).modal('hide');
                }
            );
    }
    private loadDropdown() {
        this.allegationDropdownItems$ = this._allegationSevice
            .getPagedArrayList(
                {
                    method: 'get',
                    where: {
                        intakeserviceid: this.id,
                        reported: '1',
                        activeflag: '1',
                        nolimit: true
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationListUrl + '?filter'
            )
            .map((result) => {
                return result[0].investigationallegation.map((item) => {
                    return new DropdownModel({
                        text: item.name,
                        value: item.investigationallegationid
                    });
                });
            });
    }
    private statusDropdown() {
        this.statusDropDownItems$ = this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    fields: ['investigationallegationstatustypekey', 'typedescription'],
                    where: { activeflag: '1' },
                    order: 'typedescription',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationStatusDropdownUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.investigationallegationstatustypekey
                        })
                );
            });
    }
    private getIndicatorsList(allegationId) {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.IndicatorListUrl + allegationId + '?filter'
            )
            .subscribe(
                (result) => {
                    this.indicatorList = result;
                    if (this.allegationEdit.addedindicators) {
                        this.indicatorList = result.map((item) => {
                            this.allegationEdit.addedindicators.split(',').map((id) => {
                                if (item.indicatorname === id) {
                                    item.isSelected = true;
                                }
                            });
                            return item;
                        });
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }
    private getActionSummary() {
        this._investigationIdService.getById(this.daNumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe(
            (response) => {
                this.dsdsActionsSummary = response[0];
                this.investigationId = this.dsdsActionsSummary.da_investigationid;
                this.daTypeId = this.dsdsActionsSummary.da_typeid;
                this.daSubTypeId = this.dsdsActionsSummary.da_subtypeid;
                this.allegationSearches(null);
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
}
