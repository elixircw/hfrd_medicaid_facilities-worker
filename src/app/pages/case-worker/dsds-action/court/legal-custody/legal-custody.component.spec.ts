import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalCustodyComponent } from './legal-custody.component';

describe('LegalCustodyComponent', () => {
  let component: LegalCustodyComponent;
  let fixture: ComponentFixture<LegalCustodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalCustodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalCustodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
