import { Component, OnInit } from '@angular/core';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { DropdownModel, DynamicObject } from '../../../../../@core/entities/common.entities';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { PlacementAdoptionService } from '../../placement/placement-adoption/placement-adoption.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataStoreService, AlertService, CommonHttpService, CommonDropdownsService, SessionStorageService } from '../../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { Getlegalcustody } from '../../placement/placement-adoption/_entities/adoption.model';
import { InvolvedPersonsService } from '../../../../shared-pages/involved-persons/involved-persons.service';
import { InvolvedPerson, Role } from '../../../../../@core/common/models/involvedperson.data.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
@Component({
  selector: 'legal-custody',
  templateUrl: './legal-custody.component.html',
  styleUrls: ['./legal-custody.component.scss']
})
export class LegalCustodyComponent implements OnInit {

  caseid: string;
  involvedYouth: string;
  addEditLabel: string;
  updateButton: boolean;
  involvedPersons: InvolvedPerson[] = [];
  intakeservicerequestpetitionactors = [];
  legalForm: FormGroup;
  legalCustodyDropdownItems$: Observable<DropdownModel[]>;
  permanencyPlanId: any;
  private id: string;
  private store: DynamicObject;
  private daNumber: string;
  childActorId: string;
  personId: string;
  involevedPerson$: Observable<InvolvedPerson[]>;
  involvedPersonList: InvolvedPerson[];
  personList: InvolvedPerson[];
  legalPerson: InvolvedPerson[];
  legalCustodyDetails: any[];
  addDisable: boolean;
  viewDate: boolean;
  minDate: Date;
  selectChild: boolean;
  isClosed = false;
  constructor(
      private _commonHttpService: CommonHttpService,
      private route: ActivatedRoute,
      private _formBuilder: FormBuilder,
      private _alertService: AlertService,
      private _store: DataStoreService,
      private _router: Router,
      private _service: InvolvedPersonsService,
      private storage: SessionStorageService,
      private _commonDropDownService: CommonDropdownsService,
      private _dataStoreService: DataStoreService
     // private _PlacementAdoptionService: PlacementAdoptionService
  ) {
      this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
      this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
      this.caseid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
      this.store = this._store.getCurrentStore();
  }

  ngOnInit() {
      this.selectChild = false;
      this.legalCustodyDetails = [];
      this.addEditLabel = 'Add';
      this.legalForm = this._formBuilder.group({
          intakeservicerequestactorid: [''],
          fromdate: ['', [Validators.required]],
          todate: [''],
          relationship: [''],
          Address: [''],
          phonenumber: [''],
          workphone: [''],
          code: [''],
          reason: [''],
          legalcustodytypekey: ['', [Validators.required]],
          legalcustodyid: [null],
          permanencyplanid: [null]
      });
      this.addDisable = true;
      this.getLegalCustodyDropdown();
      this.disableFieldOnInit();
      this.loadPersons();

      const da_status = this.storage.getItem('da_status');
      if (da_status) {
       if (da_status === 'Closed' || da_status === 'Completed') {
           this.isClosed = true;
       } else {
           this.isClosed = false;
       }
      }
  }
  private getLegalCustodyDropdown() {
    this.legalCustodyDropdownItems$ = this._commonHttpService
        .getArrayList(
            {
                where: {
                    referencetypeid: '28',
                    teamtypekey: null
                },
                method: 'get',
                nolimit: true
            },
            'referencetype/gettypes' + '?filter'
        )
        .map(result => {
            return result.map(
                res =>
                    new DropdownModel({
                        text: res.description,
                        value: res.ref_key
                    })
            );
        });
}

  beginDateChange(form) {
    this.minDate = new Date(form.value.fromdate);
    form.get('todate').reset();
  }

  addLegalCustody(type, item?) {
    this.viewDate = false;
    if (type === 'add') {
        this.addEditLabel = 'Add';
        this.legalForm.patchValue({ legalcustodyid: null });
        this.legalForm.enable();
        this.disableFieldOnInit();
        this.legalForm.reset();
    } else {
        this.legalForm.patchValue(item);
        this.changePerson(item.intakeservicerequestactorid);
        if (type === 'view') {
            this.addEditLabel = 'View';
            this.viewDate = true;
            this.updateButton = true;
            this.legalForm.disable();
        } else if (type === 'edit') {
            this.addEditLabel = 'Update';
            this.updateButton = true;
            this.legalForm.enable();
            this.disableFieldOnInit();
        }
    }
    (<any>$('#addlegalcustody')).modal('show');
}
cancelAdoption() {
  this.legalForm.reset();
  this.updateButton = false;
  this.addEditLabel = 'Add';
}

  loadPersons() {
    this.personList = [];
    this.legalPerson = [];
    this.involevedPerson$ = this._service.getInvolvedPerson(1, 20)
      .share()
      .pluck('data');
    this.involevedPerson$.subscribe(response => {
        if (response && response.length) {
            // const avoidRoles = ['AV', 'AM', 'CHILD', 'RC', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC'];
            response.map(item => {
                 if ( !item.rolename || item.rolename === '') {

                    if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestpersontypekey) {
                        item.rolename  = item.roles[0].intakeservicerequestpersontypekey; }
                }
                if ( item.rolename === 'AV' || item.rolename === 'CHILD') {
                    if (!item.intakeservicerequestactorid) {
                       if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestactorid) {
                        item.intakeservicerequestactorid  = item.roles[0].intakeservicerequestactorid; }
                    }
                    this.personList.push(item);
                } else {
                  if (!item.intakeservicerequestactorid) {
                    if ( item.roles &&  item.roles.length &&  item.roles[0].intakeservicerequestactorid) {
                     item.intakeservicerequestactorid  = item.roles[0].intakeservicerequestactorid; }
                 }
                 this.legalPerson.push(item);
                }
            });
          }
          });
  }

  getLegalCustody() {
    this._commonHttpService.getArrayList({
      method: 'get',
      where: {
        personid: this.childActorId
      }
    }, 'legalcustody/getlegalcustody?filter').subscribe( res => {
      if (res && res.length && res[0].getlegalcustody) {
        this.legalCustodyDetails = res[0].getlegalcustody;
       /* for (let i = 0; i < this.legalCustodyDetails.length; i++ ) {
          this.legalCustodyDetails[i].fromdate = this._commonDropDownService.getValidDate(this.legalCustodyDetails[i].fromdate);
          this.legalCustodyDetails[i].todate = this._commonDropDownService.getValidDate(this.legalCustodyDetails[i].todate);
        } */
      } else {
        this.legalCustodyDetails = [];
      }
    });
  }

  isSelectedPerson(modal) {
    const index = this.personList
        ? this.personList.findIndex(
            item =>
                item.intakeservicerequestactorid === modal.intakeservicerequestactorid
        )
        : -1;
    if (index >= 0) {
        return true;
    } else {
        return false;
    }
}
  changePerson(id) {
    let AddressText;
    const personRelation = this.legalPerson && this.legalPerson.length ? this.legalPerson.filter(item => item.intakeservicerequestactorid === id) : [];
    const relationshipText = personRelation && personRelation.length ? personRelation[0].relationship : '';
    if (personRelation && personRelation.length) {
        AddressText = personRelation[0].address ? personRelation[0].address : '';
        AddressText += personRelation[0].address2 ? ', ' + personRelation[0].address2 : '';
        AddressText += personRelation[0].state ? ', ' + personRelation[0].state : '';
        AddressText += personRelation[0].city ? ', ' + personRelation[0].city : '';
        AddressText += personRelation[0].county ? ', ' + personRelation[0].county : '';
        AddressText += personRelation[0].zipcode ? ' - ' + personRelation[0].zipcode : '';
    }
    const Phoneno = personRelation && personRelation.length ? personRelation[0].phonenumber : '';
    this.legalForm.patchValue({
        relationship: relationshipText,
        Address: AddressText,
        phonenumber: Phoneno
    });
}
  onChildChecked(child, event) {
    if (event.checked) {
      this.childActorId = child.intakeservicerequestactorid;
      this.personId = child.personid;
      this.getLegalCustody();
      this.addDisable = false;
      for (let i = 0; i < this.personList.length; i++) {
        if (this.personList[i].intakeservicerequestactorid === this.childActorId) {
          this.personList[i].isSelected = true;
        } else {
          this.personList[i].isSelected = false;
        }
      }
      this.selectChild = true;
    } else {
      this.addDisable = true;
      this.legalCustodyDetails = [];
      this.selectChild = false;
    }
  }

  saveLegalCustody(model) {
    const legalCustoduInput = model;
    // legalCustoduInput.fromdate = this._commonDropDownService.getValidDate(legalCustoduInput.fromdate);
    // legalCustoduInput.todate = this._commonDropDownService.getValidDate(legalCustoduInput.todate);
    const isservicecase = this._store.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    if (isservicecase) {
      legalCustoduInput.servicecaseid = this.id;
      delete legalCustoduInput.intakeserviceid;
    } else {
      legalCustoduInput.intakeserviceid = this.id;
      delete legalCustoduInput.servicecaseid;
    }
    legalCustoduInput.permanencyplanid = null;
    if (!legalCustoduInput.intakeservicerequestactorid) {
      legalCustoduInput.intakeservicerequestactorid = this.childActorId;
    }
    // legalCustoduInput.personid = this.childActorId;
    legalCustoduInput.personid = this.personId;
    this._commonHttpService.create(legalCustoduInput, CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.Adoption.LegalCustodyAddUpdate).subscribe(
      res => {
        this.updateButton = true;
        this._alertService.success('Court - Legal custody saved successfully');
        (<any>$('#addlegalcustody')).modal('hide');
        this.legalForm.reset();
        this.getLegalCustody();
        // }
      },
      err => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  clearLeagalCustody() {
      this.legalForm.reset();
      this.disableFieldOnInit();
  }
  private disableFieldOnInit() {
      this.legalForm.get('relationship').disable();
      this.legalForm.get('Address').disable();
      this.legalForm.get('phonenumber').disable();
      this.legalForm.get('workphone').disable();
      this.legalForm.get('code').disable();
  }
  navigateTo() {
  const redirectUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/placement-menu/placement/adoption/tpr-recom';
              this._router.navigate([redirectUrl]);
      }


}
