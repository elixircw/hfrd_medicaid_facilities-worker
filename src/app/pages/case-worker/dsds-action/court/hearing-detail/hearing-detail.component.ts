import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as _ from 'lodash';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, DataStoreService, CommonDropdownsService, SessionStorageService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { CourtAction, PersonDteails, PetitionList } from '../_entities/court.data.model';
import { Petition } from '../../../../newintake/my-newintake/_entities/newintakeSaveModel';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';
const PETITION_ID_UPDATE_WINDOW = 30;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'hearing-detail',
    templateUrl: './hearing-detail.component.html',
    styleUrls: ['./hearing-detail.component.scss']
})
export class HearingDetailComponent implements OnInit {
    offenceCategories$: Observable<DropdownModel[]>;
    courtActionsType$: Observable<DropdownModel[]>;
    conditionTypes$: Observable<DropdownModel[]>;
    courtOrder$: Observable<DropdownModel[]>;
    findingsType$: Observable<DropdownModel[]>;
    adijuctedDecisions$: Observable<DropdownModel[]>;
    hearingStatusTypeItem$: Observable<DropdownModel[]>;
    hearingType$: Observable<DropdownModel[]>;
    stateList$: Observable<DropdownModel[]>;
    countyList$: Observable<DropdownModel[]>;
    courtDetailsForm: FormGroup;
    hearingForm: FormGroup;
    userInfo: AppUser;
    times: string[] = [];
    hours: string[] = [];
    mins: string[] = [];
    meridiem: string[];
    modalInt: number;
    isPetitionFilled: boolean;
    isUpcomingHearing: boolean;
    id: string;
    courtDetails: any;
    courtAction: CourtAction[] = [];
    petitionDetails: PetitionList = new PetitionList();
    hearingData: any[] = [];
    hearingstatustypeArray: string[] = [];
    daNumber: string;
    hearingDetails: any[] = [];
    courtActionDescription: string[] = [];
    courtOrderDescription: string[] = [];
    courtConditionnDescription: string[] = [];
    hearingtypeArray: string[] = [];
    reportMode: string;
    editMode: boolean;
    hearingId: string;
    isHearingDetailsForm: boolean;
    petitonListDetails: PetitionList = new PetitionList();
    petitionId: PetitionList[];
    isNextHearingScheduled = false;
    hearingDetailAlert: any;
    hearingTypeList: any[];
    selectedPetionDate: any;
    hearingMaxDate: Date;
    isClosed = false;
    savenexthearingflag = false;

    constructor(private _commonHttpService: CommonHttpService, private _formBuilder: FormBuilder,
        private _authService: AuthService, private _route: ActivatedRoute, private _datastore: DataStoreService,
        private _alertService: AlertService, private _router: Router,
        private _intakeUtils: IntakeUtils,
        private _commonDDservice: CommonDropdownsService,
        private storage: SessionStorageService) { }

    ngOnInit() {
        this.id = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._datastore.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;

        this.modalInt = -1;
        this.reportMode = 'add';
        this.isPetitionFilled = false;
        this.editMode = true;
        this.userInfo = this._authService.getCurrentUser();
        this.hearingId = '';
        this.initiateFormGroup();
        this.gethearingDetails();
        this.getPetitionDetails();
        this.loadDroddowns();
        this.times = this.generateTimeList(false);

        const da_status = this.storage.getItem('da_status');
        if (da_status) {
         if (da_status === 'Closed' || da_status === 'Completed') {
             this.isClosed = true;
         } else {
             this.isClosed = false;
         }
        }
    }

    initiateFormGroup() {
        this.courtDetailsForm = this._formBuilder.group({
            legalcounselname: [''],
            intakeservicerequestid: [''],
            hearingtypekey: [''],
            hearingdatetime: [''],
            hearingDate: [''],
            hearingTime: ['08:00'],
            statekey: [''],
            countyid: null,
            focusname: [''],
            judgename: [''],
            hearingstatustypekey: [''],
            hearingdetails: this.hearingDetails,
            hearingnotes: [''],
            intakeservicerequestpetitionid: null,
            nofurtherinvolvementflag: [false],
            exceptionappealfiledflag: [false],
            exceptionappealflag: [false],
            nexthearingdate: [''],
            nexthearingtime: ['08:00'],
            //     {
            //         "nexthearingtypekey": null,
            //         "nexthearingdatetime": null,
            //         "nexthearingnotes": null
            //     }
        });
        // this.hearingForm = this._formBuilder.group({

        //     nexthearingtypekey: [''],
        //     nexthearingdatetime: [''],
        //     nexthearingnotes: [''],
        //     nexthearingtime: ['']

        // });
        this.hearingForm = this._formBuilder.group({

            legalcounselname: null,
            intakeservicerequestid: [''],
            hearingtypekey: [''],
            hearingdatetime: ['08:00'],
            hearingDate: [''],
            hearingTime: [''],
            statekey: null,
            countyid: null,
            focusname: null,
            judgename: null,
            hearingstatustypekey: null,
            // hearingdetails: this.hearingDetails,
            hearingnotes: null,
            intakeservicerequestpetitionid: null
        });
    }

    patchForm(data) {
        console.log('patch form data', data);
        if (data) {
            this.hearingMaxDate = this._commonDDservice.getValidDate(data.hearingdatetime);
            data.hearingDate = this._commonDDservice.getValidDate(data.hearingdatetime);
            data.hearingTime = moment(data.hearingdatetime).format('HH:mm');
            if (data.nexthearingtime) {
                data.nexthearingtime = moment(data.nexthearingtime).format('HH:mm');
            }
            data.nofurtherinvolvementflag = data.nofurtherinvolvementflag ? true : false;
            data.exceptionappealfiledflag = data.exceptionappealfiledflag ? true : false;
            data.exceptionappealflag = data.exceptionappealflag ? true : false;

            this.courtDetailsForm.patchValue(data);
            this.processNextHearingDate();
        }
    }
    patchHearingForm() {
        // const data = { hearingTime: '08:00' };
        if (!this.isPetitionFilled) {
            this._alertService.error('Please Fill Petition Details to proceed');
        } else {
            this.hearingForm.reset();
            this.hearingForm.patchValue({ hearingTime: '08:00' });
            (<any>$('#add-hearing')).modal('show');
        }

    }
    addHearingDetail() {
        if (this.courtDetailsForm.valid) {
            this.courtDetails = Object.assign({}, this.courtDetailsForm.getRawValue());

            const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
            if (isServiceCase) {
                delete this.courtDetails.intakeservicerequestid;
                this.courtDetails.servicecaseid = this.id;
                 // Sending Default value for intakeservicerequestid as the API throws error for null value
                this.courtDetails.intakeservicerequestid = 'deda9fed-a731-4151-ab8b-5f6cef7b60c4';
            } else {
                delete this.courtDetails.servicecaseid;
                this.courtDetails.intakeservicerequestid = this.id;
            }

            // this.courtDetails.hearingdatetime = moment(this.courtDetailsForm.value.hearingDate).format('YYYY-MM-DD') + 'T'
            //     + this.courtDetailsForm.value.hearingTime + ':00.000Z';
            // @Simar - date time issue
            this.courtDetails.hearingdatetime = this.convertDateTimeToTimestamp(this.courtDetailsForm.value.hearingDate, this.courtDetailsForm.value.hearingTime);

            this.courtDetails.hearingdetails = this.hearingDetails;
            this.courtDetails.courtaction = this.courtAction;
            this.courtDetails.intakeservicerequestpetitionid = this.petitionDetails.intakeservicerequestpetitionid;
            this.courtDetails.nofurtherinvolvementflag = this.courtDetails.nofurtherinvolvementflag ? 1 : 0;
            this.courtDetails.exceptionappealfiledflag = this.courtDetails.exceptionappealfiledflag ? 1 : 0;
            this.courtDetails.exceptionappealflag = this.courtDetails.exceptionappealflag ? 1 : 0;
            if (this.hearingData.length <= 0) {
                this._commonHttpService.create(this.courtDetails, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.HearingAddUrl).subscribe(result => {
                    this._alertService.success('Hearing details saved successfully!');
                    this.gethearingDetails();
                    this.hearingData[0] = result;
                    this.resetForm();
                },
                    error => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    });
            } else {
                this._alertService.error('Hearing already Exists');
                this.resetForm();
            }
        } else {
            this._alertService.error('Please fill all fields');
            console.log(this.courtDetailsForm.value);
        }
    }
    updateHearingDetail() {
        if (this.courtDetailsForm.valid) {
            this.courtDetails = Object.assign({}, this.courtDetailsForm.value);

            const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
            if (isServiceCase) {
                delete this.courtDetails.intakeservicerequestid;
                this.courtDetails.servicecaseid = this.id;
                 // Sending Default value for intakeservicerequestid as the API throws error for null value
                this.courtDetails.intakeservicerequestid = 'deda9fed-a731-4151-ab8b-5f6cef7b60c4';
            } else {
                delete this.courtDetails.servicecaseid;
                this.courtDetails.intakeservicerequestid = this.id;
            }
            // this.courtDetails.hearingdatetime = moment(this.courtDetailsForm.value.hearingDate).format('YYYY/MM/DD') + ' '
            //     + this.courtDetailsForm.value.hearingTime + ':00.000Z';

            // @Simar: date time issue
            this.courtDetails.hearingdatetime = this.convertDateTimeToTimestamp(this.courtDetailsForm.value.hearingDate, this.courtDetailsForm.value.hearingTime);

            if (this.isNextHearingScheduled) {
                if (this.courtDetails.nexthearingtime) {
                    const timeSplit = this.courtDetailsForm.value.nexthearingtime.split(':');

                    // if (!(this.courtDetails.nexthearingtime instanceof Date)) {
                    //     this.courtDetails.nexthearingtime = moment(this.courtDetails.nexthearingtime).toDate();
                    // }
                    if (!(this.courtDetails.nexthearingdate instanceof Date)) {
                        this.courtDetails.nexthearingtime = this._commonDDservice.getValidDate(this.courtDetails.nexthearingdate); // moment(this.courtDetails.nexthearingdate).toDate();
                    } else {
                        this.courtDetails.nexthearingtime = this.courtDetails.nexthearingdate;
                    }
                    this.courtDetails.nexthearingtime.setHours(timeSplit[0]);
                    this.courtDetails.nexthearingtime.setMinutes(timeSplit[1]);
                }
                /*  this.courtDetails.nexthearingtime = moment(this.courtDetailsForm.value.nexthearingdate).format('YYYY/MM/DD') + ' '
                     + this.courtDetailsForm.value.nexthearingtime; */
            } else  {
                this.courtDetails.nexthearingtime = null;
            }
            this.courtDetails.hearingdetails = this.hearingDetails;
            this.courtDetails.courtaction = this.courtAction;
            this.courtDetails.intakeservicerequestcourthearingid = this.hearingId;
            this.courtDetails.intakeservicerequestpetitionid = this.courtDetails.intakeservicerequestpetitionid;
            this.courtDetails.nofurtherinvolvementflag = this.courtDetails.nofurtherinvolvementflag ? 1 : 0;
            this.courtDetails.exceptionappealfiledflag = this.courtDetails.exceptionappealfiledflag ? 1 : 0;
            this.courtDetails.exceptionappealflag = this.courtDetails.exceptionappealflag ? 1 : 0;
            this._commonHttpService.patch(this.hearingId, this.courtDetails, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.HearingUpdateUrl).subscribe(result => {
                this._alertService.success('Hearing details updated successfully!');
                this.hearingData[this.modalInt] = this.courtDetails;
                this.resetForm();
                this.isHearingDetailsForm = false;
                this.gethearingDetails();
                if (this.isNextHearingScheduled) {
                    this.cloneHearing(this.courtDetails);
                }
            },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        } else {
            this._alertService.error('Please fill all fields');
            //  console.log(this.courtDetailsForm.value);
        }
    }

    addnextHearing() {
        if (this.hearingForm.valid) {
            this.hearingForm.value.hearingstatustypekey = 'SCHULD';
            this.courtDetails = Object.assign({
            }, this.hearingForm.value);

            const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
            if (isServiceCase) {
                delete this.courtDetails.intakeservicerequestid;
                this.courtDetails.servicecaseid = this.id;
                 // Sending Default value for intakeservicerequestid as the API throws error for null value
                this.courtDetails.intakeservicerequestid = 'deda9fed-a731-4151-ab8b-5f6cef7b60c4';
            } else {
                delete this.courtDetails.servicecaseid;
                this.courtDetails.intakeservicerequestid = this.id;
            }
            // 2019-03-17T17:24:07.000Z expected format
            // this.courtDetails.hearingdatetime = moment(this.hearingForm.value.hearingDate).format('YYYY-MM-DD') + 'T'
            //     + this.hearingForm.value.hearingTime + ':00.000Z';
            // @Simar: Changing all the time logic
            this.courtDetails.hearingdatetime = this.convertDateTimeToTimestamp(this.hearingForm.value.hearingDate, this.hearingForm.value.hearingTime);

            this.courtDetails.hearingdetails = this.hearingDetails;
            this.courtDetails.courtaction = this.courtAction;
            // this.courtDetails.intakeservicerequestpetitionid = this.petitionDetails.intakeservicerequestpetitionid;
            this._commonHttpService.create(this.courtDetails, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.HearingAddUrl).subscribe(result => {
                this._alertService.success('Hearing details saved successfully!');
                this.hearingData.push(result);
                this.hearingForm.reset();
                this.gethearingDetails();
            },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        } else {
            console.log('error', this._intakeUtils.findInvalidControls(this.hearingForm));
            this._alertService.error('Please fill all fields');
            //  console.log(this.courtDetailsForm.value);
        }
    }

    selectCourtActionType(event) {
        if (event) {
            const courtActionType = event.map(res => {
                return { courtactiontypekey: res };
            });
            this.courtAction = courtActionType;
            this.courtActionsType$.subscribe(items => {
                if (items) {
                    const getActiontems = items.filter(item => {
                        if (event.includes(item.value)) {
                            return item;
                        }
                    });
                    this.courtActionDescription = getActiontems.map(res => res.text);
                }
            });
        }
    }

    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = (tt % 60); // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }

    private loadDroddowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationURL + '?filter={"nolimit":true,"order":"name asc"}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtActionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.ConditionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtOrderTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.FindingTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.AdjudicatedDecisionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, 'hearingstatustype' + '?filter={"nolimit":true,"order":"description"}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter={"nolimit":true}'),
            // this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyListUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({
                where: { state: 'MD' }, order: 'countyname asc', nolimit: true, method: 'get'
            }, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'),

            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.HearingTypeUrl + '?filter={"where": {"teamtypekey": "CW"},"nolimit":true,"order":"description"}'),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true,
                    order: 'description'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionTypeUrl + '?filter'
            )
        ])
            .map(([offenceCategories, courtActionsType, conditionTypes, courtOrder, findingsType, adijuctedDecisions, hearingStatusType, stateList, countyList, hearingType, petitionType]) => {
                hearingType.forEach(type => {
                    this.hearingtypeArray[type.hearingtypekey] = type.description;
                });
                hearingStatusType.forEach(type => {
                    this.hearingstatustypeArray[type.hearingstatustypekey] = type.description;
                });
                petitionType.forEach(type => {
                    this.hearingstatustypeArray[type.petitiontypekey] = type.description;
                });
                return {
                    offenceCategories: offenceCategories['data'].map(
                        (res) =>
                            new DropdownModel({
                                text: res.name,
                                value: res.allegationid
                            })
                    ),
                    courtActionsType: courtActionsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtactiontypekey
                            })
                    ),
                    conditionTypes: conditionTypes.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.conditiontypekey
                            })
                    ),
                    courtOrder: courtOrder.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtordertypekey
                            })
                    ),
                    findingsType: findingsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.findingtypekey
                            })
                    ),
                    adijuctedDecisions: adijuctedDecisions.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.adjudicateddecisiontypekey
                            })
                    ),
                    hearingStatusType: hearingStatusType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.hearingstatustypekey
                            })
                    ),
                    stateList: stateList.map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    countyList: countyList.map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyid
                            })
                    ),
                    hearingType: hearingType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.hearingtypekey
                            })
                    ),
                };
            })
            .share();
        this.offenceCategories$ = source.pluck('offenceCategories');
        this.courtActionsType$ = source.pluck('courtActionsType');
        this.conditionTypes$ = source.pluck('conditionTypes');
        this.courtOrder$ = source.pluck('courtOrder');
        this.findingsType$ = source.pluck('findingsType');
        this.adijuctedDecisions$ = source.pluck('adijuctedDecisions');
        this.hearingStatusTypeItem$ = source.pluck('hearingStatusType');
        this.stateList$ = source.pluck('stateList');
        this.countyList$ = source.pluck('countyList');
        this.hearingType$ = source.pluck('hearingType');
        this.hearingType$.subscribe(hl => {
            this.hearingTypeList = hl;
        });

        this.courtActionsType$.subscribe(data => {
            if (data) {
                const courtActionValue = this.courtAction.map(res => res.courtactiontypekey);
                const getActiontems = data.filter(item => {
                    if (courtActionValue.includes(item.value)) {
                        return item;
                    }
                });
                this.courtActionDescription = getActiontems.map(res => res.text);
            }
        });
    }

    gethearingDetails() {
        if (this.id) {
            const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
            let courtReqObj = {};
            if (isServiceCase) {
                courtReqObj = {
                    objectid: this.id,
                    objecttype: 'servicecase'
                };
            } else {
                courtReqObj = { intakeservicerequestid: this.id };
            }
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: courtReqObj
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court
                    .getHearingUrl + '?filter'
            )
                .subscribe(
                    (result) => {
                        if (result && result[0]) {
                            this.hearingData = result;
                            this.hearingData = _.orderBy(this.hearingData, ['hearingdatetime'], ['desc']);
                            this.hearingData.forEach(hearing => {
                                // @Simar these type of UI side time conversions are wrong and should be avoided
                                // hearing.hearingdatetime = this._commonDDservice.getValidDate(hearing.hearingdatetime); // moment(hearing.hearingdatetime).format('YYYY-MM-DD hh:mm A');
                                // hearing.nexthearingdate = this._commonDDservice.getValidDate(hearing.nexthearingdate);
                                if (hearing.intakeservicerequestpetition &&
                                    hearing.intakeservicerequestpetition.petitionid === 'To be confirmed') {
                                    const daysTogo = moment().diff(hearing.hearingdatetime, 'day', false) * -1;
                                    hearing.daysTogo = daysTogo;
                                    if (hearing.daysTogo <= PETITION_ID_UPDATE_WINDOW) {
                                        hearing.petitionUpdateAlertMessage = 'You have an upcoming hearing within the next '
                                            + PETITION_ID_UPDATE_WINDOW + ' days, and have not yet filled the petition ID. Please fill the Petition ID.';
                                        hearing.hasPetitionIDUpdateAlert = true;
                                    } else {
                                        hearing.daysTogo = 0;
                                        hearing.hasPetitionIDUpdateAlert = false;
                                    }

                                } else {
                                    hearing.daysTogo = 0;
                                    hearing.hasPetitionIDUpdateAlert = false;
                                }
                            });
                            if (result[0].hearingdetails) {
                                this.hearingDetails = result[0].hearingdetails;
                            }
                            const contuce = result.filter((item => item.hearingstatustypekey === 'DISMIS' || item.hearingstatustypekey === 'CONCULD'));
                            if (contuce && contuce.length) {
                                this.isUpcomingHearing = true;
                            } else {
                                this.isUpcomingHearing = false;
                            }
                            // this.patchForm(result[0]);
                        } else {
                            this.isHearingDetailsForm = false;
                        }
                    },
                    (error) => {
                    }
                );
        }
    }
    private getPetitionDetails() {
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        let courtReqObj = {};
        if (isServiceCase) {
            courtReqObj = {
                objectid: this.id,
                objecttype: 'servicecase'
            };
        } else {
            courtReqObj = { intakeservicerequestid: this.id };
        }
        this._commonHttpService
            .getArrayList(
                {
                    where: courtReqObj,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court
                    .PetitionListUrl + '?filter'
            )
            .subscribe(res => {
                if (res) {
                    // this.petitionDetails = res[0];
                    this.isPetitionFilled = res[0] ? true : false;
                    this.petitionId = res.map((actor) => {
                        return this.petitonListDetails = {
                            intakeservicerequestpetitionid: actor.intakeservicerequestpetitionid,
                            associatedattorneys: actor.associatedattorneys,
                            petitionid: actor.petitionid,
                            petitiontypekey: actor.petitiontypekey,
                            actordetails: actor.intakeservicerequestpetitionactor,
                            petitiondate: actor.petitiondate
                        };
                    });
                } else {
                    // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/court/petition-detail']);
                }
            });
    }
    // resetForm() {
    //     this.hearingForm.reset();
    //     this.modalInt = -1;
    //     this.editMode = false;
    //     this.reportMode = 'add';
    //     this.hearingForm.enable();
    //     // this.medAssistance = false;
    // }

    resetForm() {
        this.hearingForm.reset();
        this.hearingForm.patchValue({ hearingTime: '08:00' });
        this.courtDetailsForm.reset();
        this.courtDetailsForm.patchValue({ hearingTime: '08:00' });
        this.modalInt = -1;
        this.editMode = false;
        this.reportMode = 'add';
        this.courtDetailsForm.enable();
        this.hearingId = '';
        // this.medAssistance = false;
    }
    private update() {
        if (this.modalInt !== -1) {

            this.hearingDetails[this.modalInt] = this.hearingForm.getRawValue();
            this.hearingDetails[this.modalInt].intakeservicerequestcourthearingid = this.hearingId;
        }
        this.resetForm();
        this._alertService.success('Updated Successfully');
    }

    viewHearing(modal, i) {
        this.petitionId.map((res) => {
            if (res.petitionid === modal.intakeservicerequestpetition.petitionid) {
                this.petitonListDetails = res;
            }
        });
        this.hearingId = modal.intakeservicerequestcourthearingid;
        this.reportMode = 'edit';
        this.editMode = false;
        this.isHearingDetailsForm = true;
        this.patchForm(modal);
        this.courtDetailsForm.disable();
        setTimeout(() => {
            window.scrollTo(0, document.body.scrollHeight);
        }, 300);
    }

    editHearing(modal, i) {

        // Validation must be done in court order screen not here.

        /*
        let toUpdate = false;
        const hearingDate = new Date(modal.hearingdatetime);
        const currentDate = new Date();
        const diffc = currentDate.getTime() - hearingDate.getTime();

        const days = Math.round(Math.abs(diffc / (1000 * 60 * 60 * 24)));
        toUpdate = (days >= 30) ? true : false;
        if (!toUpdate && (['TGU', 'TGC'].includes(modal.hearingtypekey))) {
            this.cancel();
            this._alertService.warn('Hearing Outcome can be documented only after 30 days of waiting period.');
        } else */ {
            this.petitionId.map((res) => {
                if (res.petitionid === modal.intakeservicerequestpetition.petitionid) {
                    this.petitonListDetails = res;
                }
            });
            this.hearingId = modal.intakeservicerequestcourthearingid;
            this.reportMode = 'edit';
            this.editMode = true;
            this.modalInt = i;
            this.patchForm(modal);
            this.isHearingDetailsForm = true;
            this.courtDetailsForm.enable();
        }
        setTimeout(() => {
            window.scrollTo(0, document.body.scrollHeight);
        }, 300);
    }
    deleteHearing(index, hearingId) {
        const data = this.hearingData[index];

        this._commonHttpService.remove(hearingId, this.courtDetails, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.deleteHearing).subscribe(result => {
            // this._alertService.success('Hearing details saved successfully!');
            // this.hearingData[0] = this.courtDetails;
            this._alertService.success('Hearing details deleted successfully');
            this.isUpcomingHearing = false;
            this.hearingData.splice(index, 1);
            this.gethearingDetails();
            this.resetForm();
        },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            });
        // this.resetForm();
    }

    // private view(modal) {
    //     modal.nexthearingtime = moment(modal.nexthearingdatetime).format('HH:mm');
    //     modal.nexthearingdatetime = moment(modal.nexthearingdatetime).format('YYYY-MM-DD');
    //     this.reportMode = 'edit';
    //     this.editMode = false;
    //     this.patchHearingForm(modal);
    //     this.hearingForm.disable();
    // }

    // private edit(modal, i) {
    //     modal.nexthearingtime = moment(modal.nexthearingdatetime).format('HH:mm');
    //     modal.nexthearingdatetime = moment(modal.nexthearingdatetime).format('YYYY-MM-DD');
    //     this.reportMode = 'edit';
    //     this.editMode = true;
    //     this.modalInt = i;
    //     this.patchHearingForm(modal);
    //     this.hearingForm.enable();
    // }

    // private delete(index) {
    //     this.hearingDetails.splice(index, 1);
    //     this._alertService.success('Deleted Successfully');
    //     this.resetForm();
    // }

    private cancel() {
        this.resetForm();
        this.isHearingDetailsForm = false;
    }

    processNextHearingDate() {
        this.isNextHearingScheduled = this.isNextHearingSelected();
        if (this.isNextHearingScheduled) {
            this.courtDetailsForm.get('nexthearingdate').setValidators(Validators.required);
            this.courtDetailsForm.get('nexthearingdate').updateValueAndValidity();
            this.courtDetailsForm.get('nexthearingtime').setValidators(Validators.required);
            this.courtDetailsForm.get('nexthearingtime').updateValueAndValidity();
        } else {
            this.courtDetailsForm.get('nexthearingdate').reset();
            this.courtDetailsForm.get('nexthearingdate').clearValidators();
            this.courtDetailsForm.get('nexthearingdate').updateValueAndValidity();
            this.courtDetailsForm.get('nexthearingtime').reset();
            this.courtDetailsForm.get('nexthearingtime').clearValidators();
            this.courtDetailsForm.get('nexthearingtime').updateValueAndValidity();
            // console.log('Validity Status',this.courtDetailsForm);
        }
    }
    isNextHearingSelected() {
        return this.courtDetailsForm.getRawValue().exceptionappealflag;
    }

    cloneHearing(hearingDetails) {
        hearingDetails.hearingDate = this._commonDDservice.getValidDate(hearingDetails.nexthearingdate);
        hearingDetails.hearingTime = moment(hearingDetails.hearingdatetime).format('HH:mm');

        this.hearingForm.patchValue(hearingDetails);
        console.log('clone', hearingDetails);
        console.log('new', this.hearingForm.getRawValue());
        if (this.savenexthearingflag === true) {
            this.addnextHearing();
            this.savenexthearingflag = false;
        }
    }

    showPetitionUpdateAlert(hearingDetail) {
        this.hearingDetailAlert = hearingDetail;
        (<any>$('#alert-box')).modal('show');
    }

    pettionChanged() {
        const petitionid = this.hearingForm.getRawValue().intakeservicerequestpetitionid;
        const selectedPetition = this.petitionId.find(p => p.intakeservicerequestpetitionid === petitionid);
        if (selectedPetition && selectedPetition.petitiondate) {
            this.selectedPetionDate = selectedPetition.petitiondate;
        }
    }

    // Date and time
    convertDateTimeToTimestamp(date, time) {
        return moment(moment(date).format('MM/DD/YYYY') + ' ' + time).format();
    }

    //set next hearing flag
    saveNextHearing() {
        this.savenexthearingflag = !this.savenexthearingflag;
    }
}
