import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonHttpService, AuthService, DataStoreService } from '../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'court',
  templateUrl: './court.component.html',
  styleUrls: ['./court.component.scss']
})
export class CourtComponent implements OnInit {
  id: string;
  isCursorDisble: boolean;
  agency;
  constructor(
    private _commonHttpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _authService: AuthService,
    private _dataStoreService: DataStoreService
  ) {
    this.isCursorDisble = false;
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.agency = this._authService.getAgencyName();
  }

  ngOnInit() {
    // this.getPetitionDetails();
  }

  getPetitionDetails() {
    this._commonHttpService
      .getArrayList(
        {
          where: {
            intakeservicerequestid: this.id
          },
          method: 'get'
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Court
          .PetitionDetailslist + '?filter'
      )
      .subscribe(res => {
        console.log(res[0]);
        if (res[0]) {
          this.isCursorDisble = false;
        } else {
          this.isCursorDisble = true;
        }
      });
  }


}
