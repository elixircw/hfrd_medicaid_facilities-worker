export class PetitionDetails {
    intakeservicerequestpetitionid?: string;
    intakeservicerequestid: string;
    intakenumber: string;
    petitionid: string;
    associatedattorneys: string;
    complaintid: string;
    transferpetitionid: string;
    petitionfiled: boolean;
    hearingdatetime: string;
    hearingtypekey: string;
    hearingnotes: string;
    hearingDate: string;
    hearingTime: string;
    typeofpetitiontypekey: string;
    petitionname: string;
    petitiontypekey: string;
    focusname: string[];
    petitionfocusname: string;
    servicecaseid: string;
}
export class CourtDetails {
    intakeservicerequestpetitionid?: string;
    legalcounselname: string;
    magistratename: string;
    workername: string;
    hearingdatetime: string;
    courtactiontypekey: string;
    courtordertypekey: string;
    courtorderdatetime: string;
    conditiontypekey: string;
    conditiontypedescription: string;
    conditiontypecompletiondatetime: string;
    terminationdatetime: string;
    adjudicationdatetime: string;
    adjudicationdecision: string;
    allegationid: string;
    intakeservicerequestid: string;
    adjudicationDate: string;
    adjudicationTime: string;
    hearingDate: string;
    hearingTime: string;
    terminationDate: string;
    terminationTime: string;
    orderDate: string;
    orderTime: string;
    completionDate: string;
    completionTime: string;
    courtaction: CourtAction[];
    hearingdetails: any[];
    courtorder: CourtOrder[];
    courtcondition: ConditionType[];
}

export class CourtAction {
    courtactiontypekey: string;
}

export class CourtOrder {
    courtordertypekey: string;
}

export class ConditionType {
    conditiontypekey: string;
}

export class PetitionList {
    petitionid: string;
    associatedattorneys: string;
    petitiontypekey: string;
    intakeservicerequestpetitionid?: string;
    actordetails: PetitionActor[];
    petitiondate?: any;
}

export class PetitionActor {
    intakeservicerequestpetitionid: string;
    intakeservicerequestactorid: string;
    intakeservicerequestactor: Actor;
}

export class Actor {
    intakeservicerequestactorid: string;
    personid: string;
    person: Person;
}

export class Person {
    firstname: string;
    lastname: string;
    personid: string;
}

export class CourtOrderList {
    intakeservreqcourtorderid: string;
    intakeservicerequestpetitionid: string;
    courtorderdate: string;
    hearingoutcome?: any;
    hearingoutcometypekey: string;
    hearingoutcomedesc: string;
    status?: string;
    courtdetails: CourtOrderDetail[];
    comments?: string;
    childpermanencyplankey?: string;
    attachments?: any[];
    intakeservicerequesthearingid: string;
    intakeservicerequestactorid: any;
}

export class CourtOrderAdd {
    intakeservreqcourtorderid?: string;
    intakeservicerequestpetitionid: string;
    intakeserviceid: string;
    courtorderdate: Date;
    hearingoutcometypekey: string;
    hearingoutcome: any;
    courtorderdelayremoval: boolean;
    courtorderdelaytimeframe: Number;
    courtordercopp: CourtOrderCoppLag[];
    courtordercolang: CourtOrderCoppLag[];
    courtordercoho: CourtOrderCoppLag[];
    intakeservreqcourtorderdetails: CourtOrderDetail[];
    remarks: string;
    childpermanencyplankey: any;
    attachment: any[];
}
export class CourtOrderDetail {
    intakeservreqcourtorderdetailsid?: string;
    intakeservreqcourtorderid?: string;
    checklistid: string;
    checklisttypekey: string;
    isselected: number;
    remarks: string;
    checklistname?: string;
    checklistdesc?: string;
}

export class Checklist {
    checklistid: string;
    name: string;
    description: string;
    checklisttypekey: string;
    activeflag: number;
    effectivedate: Date;
    expirationdate: Date;
    oldId: string;
    isselected?: boolean;
    remarks?: string;
}
export class CourtOrderCoppLag {
    checklistid: string;
    description: string;
    isselected: string;
    checklisttypekey: string;
    remarks: string;
}

export class HearingDetails {
    hearingdatetime: string;
    hearingtypekey: string;
    hearingstatustypekey: string;
    hearingnotes: string;
    intakeservicerequestcourthearingid: string;
}
export class PersonDteails {
    actordetails: PetitionActor[];
}
