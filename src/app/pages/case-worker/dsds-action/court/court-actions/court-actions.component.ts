import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { CourtAction, CourtDetails, CourtOrder, ConditionType, PetitionDetails } from '../_entities/court.data.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'court-actions',
  templateUrl: './court-actions.component.html',
  styleUrls: ['./court-actions.component.scss']
})
export class CourtActionsComponent implements OnInit {
    offenceCategories$: Observable<DropdownModel[]>;
    courtActionsType$: Observable<DropdownModel[]>;
    conditionTypes$: Observable<DropdownModel[]>;
    courtOrder$: Observable<DropdownModel[]>;
    findingsType$: Observable<DropdownModel[]>;
    adijuctedDecisions$: Observable<DropdownModel[]>;
    hearingStatusTypeItem$: Observable<DropdownModel[]>;
    stateList$: Observable<DropdownModel[]>;
    countyList$: Observable<DropdownModel[]>;
    courtDetailsForm: FormGroup;
    userInfo: AppUser;
    times: string[] = [];
    id: string;
    courtDetails: CourtDetails;
    courtAction: CourtAction[] = [];
    courtOrder: CourtOrder[] = [];
    conditionType: ConditionType[] = [];
    petitionDetails: PetitionDetails;
    daNumber: string;
    courtActionDescription: string[] = [];
    courtOrderDescription: string[] = [];
    courtConditionDescription: string[] = [];
    isHearingFilled: boolean;

    constructor(private _commonHttpService: CommonHttpService, private _formBuilder: FormBuilder,
         private _authService: AuthService,  private _route: ActivatedRoute,
          private _alertService: AlertService, private _router: Router,private _dataStoreService: DataStoreService) { }

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.getPetitionDetails();
        this.isHearingFilled = false;
        this.userInfo = this._authService.getCurrentUser();
        this.initiateFormGroup();
        this.getHearingdetails();
        this.courtDetailsForm.get('workername').disable();
         if (this.id) {
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        intakeservicerequestid: this.id
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court
                .getCourtActions + '?filter'
            )
            .subscribe(
                (result) => {
                    if (result[0]) {
                        if (result[0]['courtaction']) {
                        result[0].courtactiontypekey = result[0].courtaction.map(item => item.courtactiontypekey);
                        result[0].courtaction = result[0].courtaction.map(item => {
                            return {courtactiontypekey: item.courtactiontypekey};
                        });
                        this.courtAction = result[0].courtaction;
                    }
                    if (result[0]['courtorder']) {
                        result[0].courtordertypekey = result[0].courtorder.map(item => item.courtordertypekey);
                        result[0].courtorder = result[0].courtorder.map(item => {
                            return {courtordertypekey: item.courtordertypekey};
                        });
                        this.courtOrder = result[0].courtorder;
                    }
                    if (result[0]['courtcondition']) {
                        result[0].conditiontypekey = result[0].courtcondition.map(item => item.conditiontypekey);
                        result[0].courtcondition = result[0].courtcondition.map(item => {
                            return {conditiontypekey: item.conditiontypekey};
                        });
                        this.conditionType = result[0].courtcondition;
                    }
                        this.patchForm(result[0]);
                    }
                },
                (error) => {
                }
            );
         }
        this.times = this.generateTimeList(true);
        this.courtDetailsForm.get('workername').disable();
    }
    getHearingdetails() {
        if (this.id) {
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        intakeservicerequestid: this.id
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court
                .getHearingUrl + '?filter'
            )
                .subscribe(
                    (result) => {
                        if (result[0]) {
                             this.isHearingFilled = true;
                        }
                    },
                    (error) => {
                    }
                );
        }
    }
    initiateFormGroup() {
        this.courtDetailsForm = this._formBuilder.group({
            legalcounselname: null,
            magistratename: null,
            workername: this.userInfo ? this.userInfo.user.userprofile.displayname : null,
            hearingdatetime: null,
            courtactiontypekey: null,
            courtordertypekey: null,
            courtorderdatetime: null,
            conditiontypekey: null,
            conditiontypedescription: null,
            conditiontypecompletiondatetime: null,
            terminationdatetime: null,
            adjudicationdatetime: null,
            adjudicationdecision: null,
            allegationid: null,
            adjudicationDate: null,
            adjudicationTime: '08:00',
            hearingDate: null,
            hearingTime: '08:00',
            terminationDate: null,
            terminationTime: '08:00',
            orderDate: null,
            orderTime: '08:00',
            completionDate: null,
            completionTime: '08:00',
            courtcasenumber: null,
            jurisdiction: null,
            // hearingstatustypekey: [''],
            decisionnotes: null,
            // nexthearingdate: [''],
            courtorderedlanguage: null,
            hearingoutcome: null
        });
    }
    patchForm(data: CourtDetails) {
        if (data) {
            data.hearingDate = moment(data.hearingdatetime).format('YYYY-MM-DD');
            data.hearingTime = moment.utc(data.hearingdatetime).format('HH:mm');

            data.adjudicationDate = moment(data.adjudicationdatetime).format('YYYY-MM-DD');
            data.adjudicationTime = moment.utc(data.adjudicationdatetime).format('HH:mm');

            data.completionDate = moment(data.conditiontypecompletiondatetime).format('YYYY-MM-DD');
            data.completionTime = moment.utc(data.conditiontypecompletiondatetime).format('HH:mm');

            data.orderDate = moment(data.courtorderdatetime).format('YYYY-MM-DD');
            data.orderTime = moment.utc(data.courtorderdatetime).format('HH:mm');

            data.terminationDate = moment(data.terminationdatetime).format('YYYY-MM-DD');
            data.terminationTime = moment.utc(data.terminationdatetime).format('HH:mm');
            this.courtDetailsForm.patchValue(data);
        }
      }
    updateCourtDetail() {
        if (this.courtDetailsForm.valid) {
        this.courtDetails = Object.assign({}, this.courtDetailsForm.value );

        this.courtDetails.intakeservicerequestid = this.id;

        this.courtDetails.adjudicationdatetime = moment(this.courtDetailsForm.value.adjudicationDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.adjudicationTime;
        this.courtDetails.conditiontypecompletiondatetime = moment(this.courtDetailsForm.value.completionDate   ).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.completionTime;
        this.courtDetails.courtorderdatetime = moment(this.courtDetailsForm.value.orderDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.orderTime;

        this.courtDetails.hearingdatetime = moment(this.courtDetailsForm.value.hearingDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.hearingTime;

        this.courtDetails.terminationdatetime = moment(this.courtDetailsForm.value.terminationDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.terminationTime;
        this.courtDetails.courtaction = this.courtAction;
        this.courtDetails.courtorder = this.courtOrder;
        this.courtDetails.courtcondition = this.conditionType;
        this.courtDetails.intakeservicerequestpetitionid = this.petitionDetails.intakeservicerequestpetitionid;
         this._commonHttpService.create( this.courtDetails , CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtActionAddUrl).subscribe( result => {
            this._alertService.success('Court details saved successfully!');
        },
        error => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        });
    } else {
        this._alertService.error('Please fill all fields');
        console.log(this.courtDetailsForm.value);
    }
    }

    selectCourtActionType(event) {
        if (event) {
            const courtActionType = event.map(res => {
                return { courtactiontypekey: res };
            });
            this.courtAction = courtActionType;
            this.courtActionsType$.subscribe(items => {
                if (items) {
                    const getActiontems = items.filter(item => {
                        if (event.includes(item.value)) {
                        return item;
                        }
                    });
                    this.courtActionDescription = getActiontems.map(res => res.text);
                }
            });
        }
    }

    selectCourtOrderType(event) {
        if (event) {
            const courtOrderType = event.map(res => {
                return { courtordertypekey: res };
            });
            this.courtOrder = courtOrderType;
            this.courtOrder$.subscribe(items => {
                if (items) {
                    const getOrdertems = items.filter(item => {
                        if (event.includes(item.value)) {
                        return item;
                        }
                    });
                    this.courtOrderDescription = getOrdertems.map(res => res.text);
                }
            });
        }
    }

    selectCourtConditionType(event) {
        if (event) {
            const courtConditionType = event.map(res => {
                return { conditiontypekey: res };
            });
            this.conditionType = courtConditionType;
            this.conditionTypes$.subscribe(items => {
                if (items) {
                    const getConditiontems = items.filter(item => {
                        if (event.includes(item.value)) {
                        return item;
                        }
                    });
                    this.courtConditionDescription = getConditiontems.map(res => res.text);
                }
            });
        }
    }

    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = (tt % 60); // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }
    private loadDroddowns() {
        const courtActionUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtActionTypeUrl +  '?filter={"nolimit":true,"where": {"intakeservicerequestid": "' + this.id + '"}}';
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationURL + '?filter={"nolimit":true,"order":"name asc"}'),
            // this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtActionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, courtActionUrl),
            // tslint:disable-next-line:max-line-length
            // this._commonHttpService.getPagedArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtActionTypeUrl + '?filter={"nolimit": true,"where":{"intakeservicerequestid":' + this.id ? this.id : null + ' }}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.ConditionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtOrderTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.FindingTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.AdjudicatedDecisionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.HearingStatusTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyListUrl + '?filter={"nolimit":true,"order":"countyname asc"}')
        ])
            .map(([offenceCategories, courtActionsType, conditionTypes, courtOrder, findingsType, adijuctedDecisions, hearingStatusType, stateList, countyList]) => {
                return {
                    offenceCategories: offenceCategories['data'].map(
                        (res) =>
                            new DropdownModel({
                                text: res.name,
                                value: res.allegationid
                            })
                    ),
                    courtActionsType: courtActionsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtactiontypekey
                            })
                    ),
                    conditionTypes: conditionTypes.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.conditiontypekey
                            })
                    ),
                    courtOrder: courtOrder.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtordertypekey
                            })
                    ),
                    findingsType: findingsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.findingtypekey
                            })
                    ),
                    adijuctedDecisions: adijuctedDecisions.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.adjudicateddecisiontypekey
                            })
                    ),
                    hearingStatusType: hearingStatusType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.hearingstatustypekey
                            })
                    ),
                    stateList: stateList.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.hearingstatustypekey
                            })
                    ),
                    countyList: countyList.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.hearingstatustypekey
                            })
                    ),
                };
            })
            .share();
        this.offenceCategories$ = source.pluck('offenceCategories');
        this.courtActionsType$ = source.pluck('courtActionsType');
        this.conditionTypes$ = source.pluck('conditionTypes');
        this.courtOrder$ = source.pluck('courtOrder');
        this.findingsType$ = source.pluck('findingsType');
        this.adijuctedDecisions$ = source.pluck('adijuctedDecisions');
        this.hearingStatusTypeItem$ = source.pluck('hearingStatusType');
        this.stateList$ = source.pluck('stateList');
        this.countyList$ = source.pluck('countyList');

        this.courtActionsType$.subscribe(data => {
            if (data) {
                const courtActionValue = this.courtAction.map(res => res.courtactiontypekey);
                const getActiontems = data.filter(item => {
                    if (courtActionValue.includes(item.value)) {
                       return item;
                    }
                });
                this.courtActionDescription = getActiontems.map(res => res.text);
            }
        });
        this.conditionTypes$.subscribe(data => {
            if (data) {
                const conditionTypeValue = this.conditionType.map(res => res.conditiontypekey);
                const getConditionItems = data.filter(item => {
                    if (conditionTypeValue.includes(item.value)) {
                       return item;
                    }
                });
                this.courtConditionDescription = getConditionItems.map(res => res.text);
            }
        });
        this.courtOrder$.subscribe(data => {
            if (data) {
                const courtOrderValue = this.courtOrder.map(res => res.courtordertypekey);
                const getOrderItems = data.filter(item => {
                    if (courtOrderValue.includes(item.value)) {
                       return item;
                    }
                });
                this.courtOrderDescription = getOrderItems.map(res => res.text);
            }
        });
    }

    private getPetitionDetails() {
        this._commonHttpService
            .getArrayList(
                {
                    where: {
                        intakeservicerequestid: this.id
                    },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court
                    .PetitionDetailslist + '?filter'
            )
            .subscribe(res => {
                if (res[0]) {
                    this.loadDroddowns();
                 this.petitionDetails = res[0];
                } else {
                    // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/court/petition-detail']);
                }
            });
    }
}
