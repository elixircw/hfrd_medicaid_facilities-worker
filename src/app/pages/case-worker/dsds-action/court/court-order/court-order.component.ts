import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Rx';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { PetitionList, CourtOrderList, Checklist, CourtOrderAdd, HearingDetails } from '../_entities/court.data.model';
import { AlertService } from '../../../../../@core/services/alert.service';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AuthService, DataStoreService, SessionStorageService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { SpeechRecognitionService } from '../../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { config } from '../../../../../../environments/config';
import { AppConfig } from '../../../../../app.config';
import { HttpHeaders } from '@angular/common/http';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AppConstants } from '../../../../../@core/common/constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'court-order',
    templateUrl: './court-order.component.html',
    styleUrls: ['./court-order.component.scss']
})
export class CourtOrderComponent implements OnInit {
    courtOrderFormGroup: FormGroup;
    approvalStatusForm: FormGroup;
    id: string;
    isHearingFill: boolean;
    hearingOutpcomeDetails$: Observable<DropdownModel[]>;
    childPermanencyPlanData: Observable<DropdownModel[]>;
    petitonListDetails: PetitionList = new PetitionList();
    selectedPetition: PetitionList = new PetitionList();
    petitionList: PetitionList[] = [];
    courtOrderList: CourtOrderList;
    courtOrderColang: Checklist[] = [];
    courtOrderCopp: Checklist[] = [];
    courtOrderCoho: Checklist[] = [];
    courtDetailsList: CourtOrderList[] = [];
    hearingDetails: HearingDetails = new HearingDetails();
    hearingData: any[] = [];
    approvalDisable: boolean;
    caseNumber: string;
    hearingtypeArray: string[] = [];
    hearingstatustypeArray: string[] = [];
    roleId: AppUser;
    isSupervisor: boolean;
    isRouting: boolean;
    isSupervisorSubmit: boolean;
    viewCourt: boolean;

    recognizing = false;
    speechRecogninitionOn: boolean;
    speechData: string;
    notification: string;
    enableAppend = false;
    uploadedFile: any = [];
    minDate: any;
    token: AppUser;
    deleteAttachmentIndex: number;
    childPermanencyPlanKey: string;
    isClosed = false;
    familyStructure: any;
    childRemovaltype: any;
    childremovalList: any;
    involvedPerson: any[];

    // tslint:disable-next-line:max-line-length
    constructor(private _formBuilder: FormBuilder, private route: ActivatedRoute,
        private _commonHttpService: CommonHttpService, private _alertService: AlertService,
        private _authService: AuthService, private _datastore: DataStoreService,
        private _speechRecognitionService: SpeechRecognitionService,
        private storage: SessionStorageService,
        private _uploadService: NgxfUploaderService) { }

    ngOnInit() {
        this.id = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.caseNumber = this._datastore.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.token = this._authService.getCurrentUser();
        this.loadDropDownList();
        this.getChildRemoval();
        this.initializeForm();
        this.hearingOutcome();
        this.childPermanencyPlan();
        this.getPetitionDetailsList();
        this.getChecklist();
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.role.name === 'apcs' || this.roleId.role.name === 'IV-E Specialist') {
            this.isSupervisor = true;
            this.viewCourt = true;
        }


        const da_status = this.storage.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }

    }

    private initializeForm() {
        this.courtOrderFormGroup = this._formBuilder.group({
            courtorderdelayremoval: [''],
            courtorderdelaytimeframe: [''],
            courtorderdate: [''],
            hearingoutcome: [null],
            removalepisode: [''],
            childpermanencyplankey: [''],
            courtordercolang: this._formBuilder.array([]),
            courtordercopp: this._formBuilder.array([]),
            courtordercoho: this._formBuilder.array([]),
            remarks: [''],
            intakeservreqcourtorderid: [null]
        });
        this.approvalStatusForm = this._formBuilder.group({
            routingstatus: [''],
            comments: ['']
        });
    }

    setFormValues() {
        if (this.courtOrderColang) {
            const control = <FormArray>this.courtOrderFormGroup.controls['courtordercolang'];
            this.courtOrderColang.forEach((x) => {
                if (this.courtOrderList && this.courtOrderList.courtdetails) {
                    const checkList = this.courtOrderList.courtdetails.find((item) => item.checklistid === x.checklistid);
                    if (checkList) {
                        x.isselected = (checkList && checkList.isselected) ? true : false;
                        x.remarks = (checkList && checkList.remarks) ? checkList.remarks : '';
                    } else {
                        x.isselected = false;
                        x.remarks = '';
                    }

                } else {
                    x.isselected = false;
                    x.remarks = '';
                }

                control.push(this.buildCourtOrderColangForm(x));
            });
        }
        if (this.courtOrderCopp) {
            const control = <FormArray>this.courtOrderFormGroup.controls['courtordercopp'];
            this.courtOrderCopp.forEach((x) => {
                control.push(this.buildCourtOrderColangForm(x));
            });
        }
        if (this.courtOrderCoho) {
            const control = <FormArray>this.courtOrderFormGroup.controls['courtordercoho'];
            this.courtOrderCoho.forEach((x) => {
                control.push(this.buildCourtOrderColangForm(x));
            });
        }
    }

    private buildCourtOrderColangForm(x): FormGroup {
        if (this.courtOrderList && this.courtOrderList.courtdetails) {
            const getChecklist = this.courtOrderList.courtdetails.filter((item) => item.checklistid === x.checklistid);
            if (getChecklist.length) {
                return this._formBuilder.group({
                    checklistid: getChecklist[0].checklistid ? getChecklist[0].checklistid : '',
                    description: x.description ? x.description : '',
                    isselected: (getChecklist[0].isselected === 1) ? true : false,
                    remarks: getChecklist[0].remarks ? getChecklist[0].remarks : ''
                });
            } else {
                return this._formBuilder.group({
                    checklistid: x.checklistid ? x.checklistid : '',
                    description: x.description ? x.description : '',
                    isselected: x.isselected ? x.isselected.toString() : '',
                    remarks: x.remarks ? x.remarks : ''
                });
            }
        } else {
            return this._formBuilder.group({
                checklistid: x.checklistid ? x.checklistid : '',
                description: x.description ? x.description : '',
                isselected: x.isselected ? x.isselected.toString() : '',
                remarks: x.remarks ? x.remarks : ''
            });
        }
    }
    private getPetitionDetailsList() {
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        let petitionReqObj = {};
        if (isServiceCase) {
            petitionReqObj = {
                objectid: this.id,
                objecttype: 'servicecase'
            };
        } else {
            petitionReqObj = { intakeservicerequestid: this.id };
        }
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: petitionReqObj
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionListUrl + '?filter'
            )
            .subscribe((item) => {
                if (item && item.length) {
                    this.petitionList = item.map((actor) => {
                        const petition = {
                            intakeservicerequestpetitionid: actor.intakeservicerequestpetitionid,
                            associatedattorneys: actor.associatedattorneys,
                            petitionid: actor.petitionid,
                            petitiontypekey: actor.petitiontypekey,
                            actordetails: actor.intakeservicerequestpetitionactor
                        };
                        this.petitonListDetails = petition;
                        return petition;
                    });
                }
            });
    }
    private hearingOutcome() {
        this.hearingOutpcomeDetails$ = this._commonHttpService
            .getArrayList({
                method: 'get',
                where: {
                    referencetypeid: 32,
                    teamtypekey: this._authService.getAgencyName()
                }
            },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.OutComeURL + '?filter')
            .map((item) => {
                return item.map((res) => {
                    return new DropdownModel({
                        text: res.description,
                        value: res.ref_key
                    });
                });
            });
    }
    private childPermanencyPlan() {
        this.childPermanencyPlanData = this._commonHttpService
            .getArrayList({
                method: 'get',
                where: {
                    tablename: 'Childpermanencyplan',
                    teamtypekey: 'CW'
                }
            },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.ChildpermanencyplanUrl + '?filter')
            .map((item) => {
                return item.map((res) => {
                    return new DropdownModel({
                        text: res.description,
                        value: res.ref_key
                    });
                });
            });
    }
    private getChecklist() {
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        let courtReqObj = {};
        if (isServiceCase) {
            courtReqObj = {
                objectid: this.id,
                objecttype: 'servicecase'
            };
        } else {
            courtReqObj = { intakeserviceid: this.id };
        }
        Observable.forkJoin([
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: courtReqObj
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.GetCourtOrderUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        checklisttypekey: 'COLANG'
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.checklistdisplayorder + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        checklisttypekey: 'COPP'
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.ChecklistUrl + '?filter'
            ),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.HearingTypeUrl + '?filter={"where": {"teamtypekey": "CW"}, "nolimit":true,"order":"description"}'),
            this._commonHttpService.getArrayList({}, 'hearingstatustype' + '?filter={"nolimit":true,"order":"description"}'),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        checklisttypekey: 'COHO'
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.ChecklistUrl + '?filter'
            ),
        ]).subscribe((data) => {
            if (data) {
                if (data[0].length) {
                    this.courtDetailsList = data[0];
                }
                this.courtOrderColang = data[1];
                this.courtOrderCopp = data[2];
                data[3].forEach(type => {
                    this.hearingtypeArray[type.hearingtypekey] = type.description;
                });
                data[4].forEach(type => {
                    this.hearingstatustypeArray[type.hearingstatustypekey] = type.description;
                });
                this.courtOrderCoho = data[5];
                this.gethearingDetails();
            }
        });
    }
    changeSelectedChecklist(event, checklistKey, index) {
        const value = event.checked;
        if (checklistKey === 'COLANG') {
            const colang = <FormArray>this.courtOrderFormGroup.controls['courtordercolang'];
            colang.controls[index]['controls']['isselected'].patchValue(value);
            if (!value) {
                colang.controls[index]['controls']['remarks'].patchValue('');
            }
        } else if (checklistKey === 'COHO') {
            const coho = <FormArray>this.courtOrderFormGroup.controls['courtordercoho'];
            coho.controls[index]['controls']['isselected'].patchValue(value);
            if (!value) {
                coho.controls[index]['controls']['remarks'].patchValue(false);
            }
        } else {
            const copp = <FormArray>this.courtOrderFormGroup.controls['courtordercopp'];
            copp.controls[index]['controls']['isselected'].patchValue(value);
            if (!value) {
                copp.controls[index]['controls']['remarks'].patchValue(false);
            }
        }
    }

    // viewCourtOrder(hearing) {
    //     this.updateCourtOrder(hearing);
    // }

    updateCourtOrder(hearing, mode) {
        this.isHearingFill = true;
        this.courtOrderFormGroup.reset();
        this.courtOrderFormGroup.setControl('courtordercolang', this._formBuilder.array([]));
        this.courtOrderFormGroup.setControl('courtordercopp', this._formBuilder.array([]));
        this.courtOrderFormGroup.setControl('courtordercoho', this._formBuilder.array([]));
        this.courtOrderList = Object.assign({}, new CourtOrderList());

        this.hearingDetails = Object.assign(new HearingDetails(), hearing);
        if (this.hearingDetails && this.hearingDetails.hearingdatetime) {
            this.minDate = new Date(this.hearingDetails.hearingdatetime);
        }
        this.petitonListDetails.petitionid = hearing.intakeservicerequestpetition.petitionid;
        this.selectedPetition = this.petitionList.find(item => item.intakeservicerequestpetitionid === hearing.intakeservicerequestpetitionid);
        if (this.courtDetailsList.length) {
            const courtOrder = this.courtDetailsList.find(item => ((item.intakeservicerequesthearingid === hearing.intakeservicerequestcourthearingid)
                && (item.intakeservicerequestactorid === hearing.intakeservicerequestactorid)));
            if (courtOrder) {
                this.approvalStatusForm.patchValue({
                    routingstatus: courtOrder.status,
                    comments: courtOrder.comments ? courtOrder.comments : ''
                });
                // this.approvalDisable = true;
                // if (courtOrder.status && (courtOrder.status === 'Approved' || courtOrder.status === 'Review')) {
                //     this.isRouting = true;
                // } else {
                //     this.isRouting = false;
                // }
                if (this.roleId.role.name === 'apcs') {
                    if (courtOrder.status === 'Review' || courtOrder.status === 'Rejected') {
                        this.isSupervisorSubmit = false;
                    } else {
                        this.isSupervisorSubmit = true;
                    }
                }
                if (Array.isArray(courtOrder.hearingoutcome)) {
                    courtOrder.hearingoutcome = courtOrder.hearingoutcome.map(ho => {
                        return ho.hasOwnProperty('hearingoutcometypekey') ? ho.hearingoutcometypekey : ho;

                    });
                }
                this.uploadedFile = courtOrder.attachments ? courtOrder.attachments : [];
                this.courtOrderList = courtOrder;
                this.childPermanencyPlanKey = courtOrder.childpermanencyplankey;
                this.courtOrderFormGroup.patchValue(courtOrder);
            } else {
                this.uploadedFile = [];
                this.childPermanencyPlanKey = null;
                this.isRouting = false;
                this.approvalDisable = false;
            }
        } else {
            this.isRouting = false;
        }
        this.setFormValues();
        if (mode === 'view') {
            this.courtOrderFormGroup.disable();
            this.viewCourt = true;
        } else {
            this.courtOrderFormGroup.enable();
            this.viewCourt = false;
            //this.viewCourt = this.roleId.role.name === 'apcs' ? true : false;
        }
        setTimeout(() => {
            // window.scrollTo(0, document.body.scrollHeight);
            $('html, body').animate({
                scrollTop: $('#Hearing-Fill').offset().top - 280
            }, 10);
        }, 300);
    }

    getCourtOrderToSave() {
        const courtDetails: CourtOrderAdd = this.courtOrderFormGroup.getRawValue();
        if (courtDetails.courtordercopp) {
            courtDetails.courtordercopp.map((item) => {
                delete item.description;
                item.checklisttypekey = 'COPP';
            });
        }
        if (courtDetails.courtordercolang) {
            courtDetails.courtordercolang = this.courtOrderColang.map(item => {
                return {
                    checklisttypekey: 'COLANG',
                    isselected: item.isselected ? '1' : '0',
                    remarks: item.remarks,
                    checklistid: item.checklistid,
                    description: item.description
                };
            });
            courtDetails.courtordercolang.map((item) => {
                delete item.description;
                item.checklisttypekey = 'COLANG';
            });
        }
        if (courtDetails.courtordercoho) {
            courtDetails.courtordercoho.map((item) => {
                delete item.description;
                item.checklisttypekey = 'COHO';
            });
        }
        let hearingOutcomes = null;
        if (courtDetails.hearingoutcome && Array.isArray(courtDetails.hearingoutcome)) {
            hearingOutcomes = courtDetails.hearingoutcome.map(hOutcome => ({ 'hearingoutcometypekey': hOutcome }));
        }
        const courtOrderToSave = Object.assign({
            intakeservreqcourtorderid: courtDetails.intakeservreqcourtorderid,
            courtorderdate: courtDetails.courtorderdate,
            hearingoutcometypekey: null,
            remarks: courtDetails.remarks,
            intakeserviceid: null,
            servicecaseid: this.id,
            hearingoutcomedetails: hearingOutcomes,
            courtorderdelayremoval: courtDetails.courtorderdelayremoval,
            courtorderdelaytimeframe: courtDetails.courtorderdelaytimeframe,
            childpermanencyplankey: this.childPermanencyPlanKey,
            intakeservicerequestpetitionid: this.selectedPetition ? this.selectedPetition.intakeservicerequestpetitionid : '',
            intakeservreqcourtorderdetails: courtDetails.courtordercopp.concat(courtDetails.courtordercolang).concat(courtDetails.courtordercoho)
        });
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        if (isServiceCase) {
            // delete courtOrderToSave.intakeserviceid;
            courtOrderToSave.servicecaseid = this.id;
             // Sending Default value for intakeservicerequestid as the API throws error for null value
            courtOrderToSave.intakeservicerequestid = 'deda9fed-a731-4151-ab8b-5f6cef7b60c4';
        } else {
            delete courtOrderToSave.servicecaseid;
            courtOrderToSave.intakeserviceid = this.id;
        }
        courtOrderToSave.intakeservreqcourtorderdetails.map((item) => {
            item.isselected = (item.isselected && item.isselected !== '' && item.isselected !== '0') ? 1 : 0;
        });
        courtOrderToSave.attachment = this.uploadedFile;

        return courtOrderToSave;
    }

    validateCourtOrder() {
        const courtOrderToSave = this.getCourtOrderToSave();
        if (this.validateCheckList(courtOrderToSave.intakeservreqcourtorderdetails)) {
            console.log(courtOrderToSave);
            // console.log(courtordercolang);
            if (this.uploadedFile.length === 0) {
                (<any>$('#no-attachment-popup')).modal('show');
                return;
            }
            if (this.hasAnyUploadInProgress()) {
                this._alertService.error('Document upload in progress, please wait.');
                return;
            }
            this.saveCourtOrder();

        } else {
            this._alertService.error('Please fill all mandatory fields!');
        }
    }

    saveCourtOrder() {
        const courtOrderToSave = this.getCourtOrderToSave();
        courtOrderToSave.intakeservicerequesthearingid = this.hearingDetails.intakeservicerequestcourthearingid;
        courtOrderToSave.intakeservicerequestactorid = this.hearingDetails['intakeservicerequestactorid'];
        this._commonHttpService.create(courtOrderToSave, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtOrderSaveUrl).subscribe(
            (item) => {
                this._alertService.success('Court Order details saved successfully!');
                this.approvalDisable = true;
                this.getChecklist();
                this.isHearingFill = false;
            },
            (error) => {
                console.log(error);
            }
        );
    }

    hasAnyUploadInProgress() {
        if (this.uploadedFile.length === 0) {
            return false;
        }
        const inProgressAttachments = this.uploadedFile.filter(attachment => {
            if (attachment && attachment.hasOwnProperty('percentage') && attachment.percentage !== 100) {
                return true;
            } else {
                return false;
            }
        });

        if (inProgressAttachments.length > 0) {
            return true;
        }

        return false;
    }
    routingUpdate() {
        const routingObject = {
            objectid: this.courtOrderList.intakeservreqcourtorderid,
            eventcode: 'CORR',
            status: this.approvalStatusForm.controls['routingstatus'].value,
            comments: this.approvalStatusForm.controls['comments'].value,
            notifymsg: '',
            routeddescription: ''
        };
        this._commonHttpService.create(routingObject, 'routing/routingupdate').subscribe((res) => {
            this._alertService.success('saved successfully!');
            this.isSupervisorSubmit = true;
            this.getChecklist();
        }, (error) => {
            console.log(error);
        });
    }
    gethearingDetails() {
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        let hearingReqObj = {};
        if (isServiceCase) {
            hearingReqObj = {
                objectid: this.id,
                objecttype: 'servicecase'
            };
        } else {
            hearingReqObj = { intakeservicerequestid: this.id };
        }
        if (this.id) {
            this._commonHttpService
                .getArrayList(
                    {
                        method: 'get',
                        where: hearingReqObj
                    },
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.getHearingUrl + '?filter'
                )
                .subscribe(
                    (result) => {
                        if (Array.isArray(result) && result.length) {
                            this.hearingDetails = result[0];
                            this.hearingData = [];
                            result.map((res) => {
                                if (res.hearingstatustypekey === 'CONCULD' || res.hearingstatustypekey === 'DISMIS') {
                                    const actorList = res.intakeservicerequestpetition.intakeservicerequestpetitionactor.filter(item => item.petitionactortype === 'PA');
                                    const list = actorList.map(actor => {
                                        const obj = Object.assign({}, res);
                                        obj.intakeservicerequestpetition.intakeservicerequestpetitionactor = actor;
                                        if (actor.intakeservicerequestactor) {
                                            const person = actor.intakeservicerequestactor.person;
                                            obj.personid = person.personid;
                                            obj.personname = person.fullname;
                                            obj.intakeservicerequestactorid = actor.intakeservicerequestactor.intakeservicerequestactorid;
                                        }                                    
                                        obj.petitionid = res.intakeservicerequestpetition.petitionid;
                                        if (this.courtDetailsList.length) {
                                            const courtOrder = this.courtDetailsList.find(item => ((item.intakeservicerequesthearingid === res.intakeservicerequestcourthearingid)
                                                && (item.intakeservicerequestactorid === obj.intakeservicerequestactorid)));
                                                if (courtOrder) {
                                                    obj.isEditable = (this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR)
                                                    || this._authService.selectedRoleIs('IV-E Specialist')
                                                    || this._authService.selectedRoleIs('IV-E Supervisor'));
                                                } else {
                                                    obj.isEditable = true;
                                                }
                                        } else {
                                            obj.isEditable = true;
                                        }
                                        return obj;
                                    });
                                    // this.hearingData.push(list);
                                    this.hearingData = this.hearingData.concat(list);
                                }
                            });
                            this.hearingData = _.orderBy(this.hearingData, ['hearingdatetime'], ['desc']);
                            this.hearingData.forEach((hearing) => {
                                hearing.hearingdatetime = moment(hearing.hearingdatetime).format('YYYY-MM-DD hh:mm A');
                            });
                            // this.patchForm(result[0]);
                        } else {
                            this.isHearingFill = false;
                        }
                    },
                    (error) => { }
                );
        }
    }
    private validateCheckList(checkList) {
        if (checkList) {
            const checkData = [];
            checkList.map((data) => {
                if (data.isselected === '') {
                    checkData.push(data);
                }
            });
            if (checkData.length) {
                return false;
            } else {
                return true;
            }
        }
    }

    // speech recognition
    activateSpeechToText(): void {
        this.recognizing = true;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
            this._speechRecognitionService.record().subscribe(
                // listener
                (value) => {
                    this.speechData = value;
                    this.courtOrderFormGroup.patchValue({ remarks: this.speechData });
                },
                // errror
                (err) => {
                    console.log(err);
                    this.recognizing = false;
                    if (err.error === 'no-speech') {
                        this.notification = `No speech has been detected. Please try again.`;
                        this._alertService.warn(this.notification);
                        this.activateSpeechToText();
                    } else if (err.error === 'not-allowed') {
                        this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    } else if (err.error === 'not-microphone') {
                        this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
                        this._alertService.warn(this.notification);
                        // this.activateSpeechToText();
                    }
                },
                // completion
                () => {
                    this.speechRecogninitionOn = true;
                    console.log('--complete--');
                    this.activateSpeechToText();
                }
            );
        } else {
            this.recognizing = false;
            this.deActivateSpeechRecognition();
        }
    }
    deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
    }

    uploadFile(file: File | FileError): void {
        if (!(file instanceof Array)) {
            return;
        }
        file.map((item, index) => {
            const fileExt = item.name
                .toLowerCase()
                .split('.')
                .pop();
            if (
                fileExt === 'mp3' ||
                fileExt === 'ogg' ||
                fileExt === 'wav' ||
                fileExt === 'acc' ||
                fileExt === 'flac' ||
                fileExt === 'aiff' ||
                fileExt === 'mp4' ||
                fileExt === 'mov' ||
                fileExt === 'avi' ||
                fileExt === '3gp' ||
                fileExt === 'wmv' ||
                fileExt === 'mpeg-4' ||
                fileExt === 'pdf' ||
                fileExt === 'txt' ||
                fileExt === 'docx' ||
                fileExt === 'doc' ||
                fileExt === 'xls' ||
                fileExt === 'xlsx' ||
                fileExt === 'jpeg' ||
                fileExt === 'jpg' ||
                fileExt === 'png' ||
                fileExt === 'ppt' ||
                fileExt === 'pptx' ||
                fileExt === 'gif'
            ) {
                this.uploadedFile.push(item);
                const uindex = this.uploadedFile.length - 1;
                if (!this.uploadedFile[uindex].hasOwnProperty('percentage')) {
                    this.uploadedFile[uindex].percentage = 1;
                }

                this.uploadAttachment(uindex);
                const audio_ext = ['mp3', 'ogg', 'wav', 'acc', 'flac', 'aiff'];
                const video_ext = ['mp4', 'avi', 'mov', '3gp', 'wmv', 'mpeg-4'];
                if (audio_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[uindex].attachmenttypekey = 'Audio';
                } else if (video_ext.indexOf(fileExt) >= 0) {
                    this.uploadedFile[uindex].attachmenttypekey = 'Video';
                } else {
                    this.uploadedFile[uindex].attachmenttypekey = 'Document';
                }
            } else {
                // tslint:disable-next-line:quotemark
                this._alertService.error(fileExt + " format can't be uploaded");
                return;
            }
        });
    }
    uploadAttachment(index) {
        console.log('check');
        const workEnv = config.workEnvironment;
        let uploadUrl = '';
        if (workEnv === 'state') {
            uploadUrl = AppConfig.baseUrl + '/attachment/v1' + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl
                + '?access_token=' + this.token.id + '&' + 'srno=' + this.caseNumber + '&' + 'docsInfo='; // Need to discuss about the docsInfo
            console.log('state', uploadUrl);
        } else {
            uploadUrl = AppConfig.baseUrl + '/' + CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id +
                '&' + 'srno=' + this.caseNumber;
            console.log('local', uploadUrl);
        }

        this._uploadService
            .upload({
                url: uploadUrl,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile[index],
                process: true,
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        this.uploadedFile[index].percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        const doucumentInfo = response.data;
                        doucumentInfo.documentdate = doucumentInfo.date;
                        doucumentInfo.title = doucumentInfo.originalfilename;
                        doucumentInfo.objecttypekey = 'Court';
                        doucumentInfo.rootobjecttypekey = 'Court';
                        doucumentInfo.activeflag = 1;
                        doucumentInfo.servicerequestid = null;
                        this.uploadedFile[index] = { ...this.uploadedFile[index], ...doucumentInfo };
                        console.log(index, this.uploadedFile[index]);
                        this._alertService.success('File Upload successful.');
                    } 
                }, (err) => {
                    console.log(err);
                    //this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    this._alertService.error('Upload failed due to Server error, please try again later.');
                    this.uploadedFile.splice(index, 1);
                   
                }
            );
    }
    deleteAttachment() {
        this.uploadedFile.splice(this.deleteAttachmentIndex, 1);
        (<any>$('#delete-attachment-popup')).modal('hide');
    }

    downloadFile(s3bucketpathname) {
        const workEnv = config.workEnvironment;
        let downldSrcURL;
        if (workEnv === 'state') {
            downldSrcURL = AppConfig.baseUrl + '/attachment/v1' + s3bucketpathname;
        } else {
            // 4200
            downldSrcURL = s3bucketpathname;
        }
        console.log('this.downloadSrc', downldSrcURL);
        window.open(downldSrcURL, '_blank');
    }

    confirmDeleteAttachment(index: number) {
        (<any>$('#delete-attachment-popup')).modal('show');
        this.deleteAttachmentIndex = index;
    }

    getChildRemoval() {
        const requestData = { ...this.getRequestParam(), ...{ isgroup: 0 } };
        this._commonHttpService
            .getSingle(
                {
                    where: requestData,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .GetChildRemovalList + '?filter'
            ).subscribe(response => {
                const list = (Array.isArray(response)) ? response : [];
                this.childremovalList = list.filter(item => item.approvalstatus === 'Approved');
                this.setchildRemovaldata();
            });
    }

    getRequestParam() {

        const intakeserviceid = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
        const daNumber = this._datastore.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        let requestData;
        if (isServiceCase) {
            requestData = {
                objectid: intakeserviceid,
                objecttypekey: 'servicecase'
            };

        } else {
            requestData = { intakeserviceid: intakeserviceid };
        }
        return requestData;
    }

    loadDropDownList() {
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        let reqObj = {};
        if (isServiceCase) {
            reqObj = {
                objectid: this.id,
                objecttypekey: 'servicecase'
            };
        } else {
            reqObj = {
                intakeserviceid: this.id,
            };
        }
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { referencetypeid: '52', teamtypekey: this._authService.getAgencyName() },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
                '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { referencetypeid: '53', teamtypekey: null, order: 'description asc' },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.GetTypes +
                '?filter'
            ),
            this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: reqObj
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListCWUrl + '?filter'
            )
        ])
            .subscribe(([familyStructure, childRemovaltype, personobj]) => {
                this.familyStructure = familyStructure;
                this.childRemovaltype = childRemovaltype;
                this.involvedPerson = personobj['data'];
                this.setchildRemovaldata();
            });
    }

    setchildRemovaldata() {
        const isValidfs = Array.isArray(this.familyStructure);
        const isValidcrt = Array.isArray(this.childRemovaltype);
        const isValiddata = Array.isArray(this.childremovalList);
        if (isValidcrt && isValiddata && isValidfs) {
            this.childremovalList.forEach(element => {
                const fsdesc = this.familyStructure.find(item => item.ref_key === element.familystructuretypekey);
                element.fsdesc = (fsdesc) ? fsdesc.description : '';
                const cdtdesc = this.childRemovaltype.find(item => item.ref_key === element.removaltypekey);
                element.cdtdesc = (cdtdesc) ? cdtdesc.description : '';
                const child = this.involvedPerson.find(item => item.personid === element.personid);
                element.childname = (child) ? child.fullname : '';
            });
        }
    }

    courtOrderLanguageChanged(item) {
        if (item.description === 'Reasonable efforts were made to finalize the child\'s permanency plan') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Reasonable efforts were not made to finalize the child\'s permanency plan') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
        if (item.description === 'Reasonable efforts were not made to finalize the child\'s permanency plan') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Reasonable efforts were made to finalize the child\'s permanency plan') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
        if (item.description === 'Continuation of the child in the child\'s home is contrary to the child\'s welfare') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Continuation of the child in child\'s home is not contrary to the child\'s welfare') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
        if (item.description === 'Continuation of the child in child\'s home is not contrary to the child\'s welfare') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Continuation of the child in the child\'s home is contrary to the child\'s welfare') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
        if (item.description === 'Reasonable efforts were made to prevent removal') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Reasonable efforts were not made to prevent removal') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
        if (item.description === 'Reasonable efforts were not made to prevent removal') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Reasonable efforts were made to prevent removal') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
        if (item.description === 'Voluntary placement is in the best interest of the child') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Voluntary placement is not in the best interest of the child') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
        if (item.description === 'Voluntary placement is not in the best interest of the child') {
            this.courtOrderColang.forEach(order => {
                if (order.description === 'Voluntary placement is in the best interest of the child') {
                    order.isselected = false;
                    order.remarks = '';
                }
            });
        }
    }

}
