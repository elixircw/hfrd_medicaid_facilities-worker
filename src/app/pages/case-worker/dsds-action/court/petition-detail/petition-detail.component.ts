import { map } from 'rxjs/operator/map';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, AlertService, DataStoreService, SessionStorageService, AuthService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { PetitionDetails, PetitionList } from '../_entities/court.data.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { AppConfig } from '../../../../../app.config';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { HttpClient } from '@angular/common/http';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { SpeechRecognizerService } from '../../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../../../@core/services/speech-recognition.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'petition-detail',
    templateUrl: './petition-detail.component.html',
    styleUrls: ['./petition-detail.component.scss']
})

export class PetitionDetailComponent implements OnInit {
    id: string;
    involvedYouth: string;
    hearingType$: Observable<any[]>;
    petitionType$: Observable<any[]>;
    countyList$: Observable<any[]>;
    countyList: any;
    notification: string;
    petitionTypeList: any;
    speechRecogninitionOn: boolean;
    speechData: string;
    recognizing = false;
    recordDropDown: any;
    placementDropDown: any;
    petitionDetailsForm: FormGroup;
    hearingDetailsForm: FormGroup;
    witnessForm: FormGroup;
    viewMode: boolean;
    cinaForm: FormGroup;
    cinasubpoenadForm: FormGroup;
    cinaSiblingForm: FormGroup;
    petitionDetails: PetitionDetails;
    involvedPersons$: Observable<any[]>;
    involvedPersons: any[] = [];
    clientActor: any[] = [];
    attorneyList: any[] = [];
    selectedPetition: any;
    times: string[] = [];
    petitonListDetails: PetitionList[] = [];
    updateButton: boolean;
    petitionfocusid: any; // changing to single select
    clientactorsid = [];
    intakeservicerequestpetitionactors = [];
    clientActors = [];
    isTPRvalidate: boolean;
    showWaiverReason: boolean;
    aggravatedCircumstancesList: any[];
    petitionwitness: any[];
    showDownloadButton: boolean;
    baseUrl: string;
    cinaPeitition: any;
    userInfo: AppUser;
    supervisorsList: any[];
    cinasubpoenad: any[];
    cinaSibling: any[];
    selectedIndex: number;
    typeOfRecord: any[];
    intakeReceivedDate: Date;
    isClosed = false;
    // collateralroleDropdownItems$: Observable<any[]>;
    // collateralroleList: any[];

    constructor(private _commonHttpService: CommonHttpService, private _formBuilder: FormBuilder,
        private _datastore: DataStoreService, private _session: SessionStorageService,
        private route: ActivatedRoute, private _alertService: AlertService,
        private http: HttpClient,
        private _authService: AuthService,
        private speechRecognizer: SpeechRecognizerService,
        private _dataStoreService: DataStoreService,
        private _speechRecognitionService: SpeechRecognitionService) {
        this.baseUrl = AppConfig.baseUrl;
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    }
    @Input()
    petitionDetailsInputSubject$ = new Subject<PetitionDetails>();
    @Input()
    petitionDetailsOutputSubject$ = new Subject<PetitionDetails>();
    ngOnInit() {
        // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.loadDroddowns();
        this.getAggravatedCircumstancesList();
        this.getInvolvedPerson();
        this.getCollateralPerson();
        this.initiateFormGroup();
        this.getPetitionDetailsList();
        this.getSupervisorsList();
        this.loadRecordDropDown();
        this.loadPlacementDropDown();
        this.isTPRvalidate = false;
        this.typeOfRecord = [];
        this.selectedIndex = -1;
        this.notification = null;
        this.petitionDetailsForm.valueChanges.subscribe((val) => {
            this.petitionDetailsInputSubject$.next(this.petitionDetailsForm.getRawValue());
        });
        this.petitionDetailsOutputSubject$.subscribe((data) => {
            // this.patchForm(data);
        });
        this.times = this.generateTimeList(false);
        // this.petitionDetailsForm.get('petitionfocusname').disable();
        // this.patchValuseData();
        // dsdsActionsSummary?.da_receiveddate
        const dsdsActionsSummary = this._datastore.getData('object');
        if (dsdsActionsSummary && dsdsActionsSummary.da_receiveddate) {
            this.intakeReceivedDate = new Date(dsdsActionsSummary.da_receiveddate);
        }

        const da_status = this._session.getItem('da_status');
        if (da_status) {
         if (da_status === 'Closed' || da_status === 'Completed') {
             this.isClosed = true;
         } else {
             this.isClosed = false;
         }
        }

    }
    getInvolvedPerson() {
        // const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        const isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        this.userInfo = this._authService.getCurrentUser();

        let reqObj = {};
        if (isServiceCase) {
            reqObj = {
                objectid: this.id,
                objecttypekey: 'servicecase'
            };
        } else {
            reqObj = {
                intakeserviceid: this.id,
            };
        }
        this.involvedPersons$ = this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: reqObj
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListCWUrl + '?filter'
            )
            .share()
            .pluck('data');
        this.involvedPersons$.subscribe((items) => {
            if (items) {

                const person = items.filter((item) => {
                    if (!item.rolename || item.rolename === '') {
                        if (item.roles && item.roles.length && item.roles[0].intakeservicerequestpersontypekey) {
                            item.rolename = item.roles[0].intakeservicerequestpersontypekey;
                        }
                    }
                    if ((item.rolename === 'RC' || item.rolename === 'AV' || item.rolename === 'CHILD' ) && item.dateofdeath === null) {
                        this.involvedYouth = item.firstname + ' ' + item.lastname;
                        if (!item.intakeservicerequestactorid) {
                            if (item.roles && item.roles.length && item.roles[0].intakeservicerequestactorid) {
                                item.intakeservicerequestactorid = item.roles[0].intakeservicerequestactorid;
                            }
                        }
                        // this.petitionDetailsForm.patchValue({'petitionfocusname': this.involvedYouth});
                        return item;
                    }
                });

                // this.collateralroleDropdownItems$ = this._commonDropdownService.getPickListByName('collateralroles');
                // this.collateralroleDropdownItems$.subscribe( (data) => {
                //     this.collateralroleList = data.filter( data => {
                //         return (data.ref_key === 'AT' || data.ref_key === 'CHADCA' || data.ref_key === 'CLAT')
                //     });
                // });

                this.attorneyList = items.filter((item) => {
                    const roles = item.roles;
                    const clientAttorney = ['ADV', 'AT', 'CHADCA', 'CLAT'];
                    if (roles && roles.length && Array.isArray(roles)) {

                        const rolePresent = roles.some(r => clientAttorney.includes(r.intakeservicerequestpersontypekey));
                        if (rolePresent) {
                            return item;
                        }
                    } else {
                        if (item.rolename) {
                            const roleExist = clientAttorney.includes(item.rolename);
                            if (roleExist) {
                                return item;
                            }
                        }
                    }
                });
                const client = items.filter((item) => {
                    if (!item.rolename || item.rolename === '') {
                        if (item.roles && item.roles.length && item.roles[0].intakeservicerequestpersontypekey) {
                            item.rolename = item.roles[0].intakeservicerequestpersontypekey;
                        }
                    }
                    if (item.rolename !== 'RC' && item.rolename !== 'AV' && item.rolename !== 'CHILD') {
                        this.involvedYouth = item.firstname + ' ' + item.lastname;
                        if (!item.intakeservicerequestactorid) {
                            if (item.roles && item.roles.length && item.roles[0].intakeservicerequestactorid) {
                                item.intakeservicerequestactorid = item.roles[0].intakeservicerequestactorid;
                            }
                        }
                        // this.petitionDetailsForm.patchValue({'petitionfocusname': this.involvedYouth});
                        return item;
                    }
                });
                this.involvedPersons = person.map((res) => res);
                this.clientActor = client.map((res) => res);
                this.prepareRelatioships();
            }
        });
    }

    getCollateralPerson(){
        const request = {
            objectid: this.id,
            objecttype: 'case'
        };
        this._commonHttpService.getArrayList(
            {
                where: request,
                method: 'get',
                nolimit: true
            },
            'collateral/list?filter'
        ).subscribe(res => {
            if (res && res.length && res[0].getcollateraldetails && res[0].getcollateraldetails.length) {
                const collateralDetails = res[0].getcollateraldetails;
                if(collateralDetails && collateralDetails.length>0){
                    const collateralAttorney = collateralDetails.filter((item) => {
                        const roles = item.collateralroleconfig;
                        const clientAttorney = ['ADV', 'AT', 'CHADCA', 'CLAT'];
                        if (roles && roles.length && Array.isArray(roles)) {
                            const rolePresent = roles.some(r => clientAttorney.includes(r.actortypekey));
                            if (rolePresent) {
                                return item;
                            }
                        }
                    });
                    if(collateralAttorney && collateralAttorney.length>0){
                        collateralAttorney.forEach(item => {
                            let roleList = [];
                                item.collateralroleconfig.forEach(role =>{
                                    roleList.push({typedescription :role.description})
                                });
                            this.attorneyList.push(
                                {intakeservicerequestactorid: item.collateralid, firstname:item.firstname, lastname: item.lastname, roles: roleList}
                            )
                        })
                    }
                }
            }
        });

      }

    prepareRelatioships() {
        const relationship$ = this.clientActor.map(person => {
            const ob$ = this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        page: 1,
                        limit: 100,
                        method: 'get',
                        where: this.getRequestParam(person.personid)
                    }),
                    'People/getallpersonrelationbyprovidedpersonid?filter'
                );
            return { personid: person.personid, relation$: ob$ };
        });
        relationship$.forEach(obj => {
            obj.relation$.subscribe(data => {
                const person = this.clientActor.find(item => item.personid === obj.personid);
                if (person) {
                    person.relationshipList = data;
                }
            });
        });
    }

    getRequestParam(personid: string) {
        let inputRequest;
        const caseID = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
        const isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        if (isServiceCase) {
          inputRequest = {
            objectid: caseID,
            objecttypekey: 'servicecase',
          };
        } else {
          inputRequest = {
            intakeserviceid: caseID
          };
        }
        if (personid) {
            inputRequest.personid = personid;
        }
        return inputRequest;
      }

    private getPetitionDetailsList() {
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        let courtReqObj = {};
        if (isServiceCase) {
            courtReqObj = {
                objectid: this.id,
                objecttype: 'servicecase'
            };
        } else {
            courtReqObj = { intakeservicerequestid: this.id };
        }
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: courtReqObj
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionListUrl + '?filter'
            )
            .subscribe((item) => {
                if (item && item.length) {
                    this.petitonListDetails = item.map((actor) => {
                        return {
                            intakeservicerequestpetitionid: actor.intakeservicerequestpetitionid,
                            associatedattorneys: actor.associatedattorneys,
                            petitionid: actor.petitionid,
                            petitiontypekey: actor.petitiontypekey,
                            actordetails: actor.petitionactors,
                            clientActordetails: actor.clientactorsid,
                            clientActorsdetails: actor.clientactors,
                            courtcasenumber: actor.courtcasenumber,
                            petitiondate: actor.petitiondate,
                            witness1 : actor.witness1
                        };
                    });
                    console.log('Petition List Details', this.petitonListDetails);

                    // Below logic moved to hearing details
                   /*  const hasPetitionDateAlert = this.petitonListDetails.filter(item => item.petitionid === 'To be confirmed');
                    if (hasPetitionDateAlert.length > 0) {
                        (<any>$('#alert-box')).modal('show');
                    } */
                }
            });
    }

    getPetitionTypeDesc(pKey: string) {
        if (this.petitionTypeList) {
            return  this.petitionTypeList.filter(k => k.value === pKey)[0].text;
        }
    }

    getAggravatedCircumstancesList() {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: { 'referencetypeid': 41, 'teamtypekey': null }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.AggravatedcircumstancesUrl + '?filter'
            )
            .subscribe((item) => {
                if (item && item.length) {
                    this.aggravatedCircumstancesList = item;
                }
            });
    }

    loadRecordDropDown() {

        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 136, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            ).subscribe ( (data) => {
                this.recordDropDown = data;
                    if (this.recordDropDown && this.recordDropDown.length) {
                    this.recordDropDown.forEach(element => {
                        this.typeOfRecord[element.ref_key] =   element.value_text;
                    });
                }
            });
    }

    loadPlacementDropDown() {
        this._commonHttpService
        .getArrayList(
            {
                where: { referencetypeid: 77 , teamtypekey: 'CW' },
                method: 'get'
            },
            'referencetype/gettypes' + '?filter'
        ).subscribe ( (data) => {
            this.placementDropDown = data;
        });
    }


    viewPetiton(item) {
        this.editPetiton(item);
        this.petitionDetailsForm.disable();
    }

    editPetiton(item) {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        this.updateButton = true;
        let clientactors ;
        this.intakeservicerequestpetitionactors = [];
        this.petitionDetailsForm.patchValue(item);
        if (item.actordetails && item.actordetails.length) {
            this.petitionfocusid = item.actordetails.map((res) => {
                return res.intakeservicerequestactorid;
            });
        }
        if (item.actordetails) {
            this.intakeservicerequestpetitionactors = item.actordetails;
        }
        if (item.clientActordetails && item.clientActordetails.length && Array.isArray(item.clientActordetails)) {
            this.clientactorsid = item.clientActordetails.map((res) => {
                return res.intakeservicerequestactorid;
            });
        } else {
            if (item.clientActordetails) {
                this.clientactorsid = item.clientActordetails;
            }

        }
        if (item.clientActorsdetails && item.clientActorsdetails.length && Array.isArray(item.clientActorsdetails)) {
            clientactors = item.clientActorsdetails.map((res) => {
                return res.intakeservicerequestactorid;
            });
        }
        if (item.petitiontypekey === 'CINA') {
            this.patchCinaForm(item.intakeservicerequestpetitionid, false);
            this.showDownloadButton = true;
        }
        const iskinhome = ( item.iskinhome && item.iskinhome === 1 ) ? true : false;
        this.cinaForm.controls['iskinhome'].patchValue(iskinhome);
        this.petitionDetailsForm.controls['focusname'].patchValue(this.petitionfocusid);
        this.petitionDetailsForm.controls['clientactorsid'].patchValue(this.clientactorsid);
        this.petitionDetailsForm.controls['clientactors'].patchValue(clientactors);
        this.petitionDetailsForm.enable();
    }
    changePerson(item) {
        this.intakeservicerequestpetitionactors = item.map((res) => {
            return {
                intakeservicerequestactorid: res,
                petitionactortype: 'PA'
            };
        });
        // this.intakeservicerequestpetitionactors = [{
        //     intakeservicerequestactorid: item,
        //     petitionactortype: 'PA'
        // }];
        this.setCinaChild(item);
    }
    changeClientActor(item) {
        this.clientActors = item.map((res) => {
            return {
                intakeservicerequestactorid: res,
                petitionactortype: 'CA'
            };
        });

    }
    initiateFormGroup() {
        this.petitionDetailsForm = this._formBuilder.group({
            servicecaseid: [null],
            intakenumber: '',
            petitiontypekey: ['', Validators.required],
            petitionfocusname: [null],
            petitionid: [null],
            // new FormControl('', Validators.compose([
            //     Validators.pattern('^[a-zA-Z0-9]*$')
            //   ])),
            associatedattorneys: new FormControl('', Validators.compose([
                Validators.pattern('^[a-zA-Z-\' ]*$')
              ])),
            complaintid: '',
            transferpetitionid: '',
            petitionfiled: [''],
            focusname: ['', Validators.required],
            clientactorsid: [null],
            courtcasenumber: [''],
            petitiondate: [null, Validators.required],
            intakeservicerequestpetitionid: [null],
            aggravations: [[]],
            witness1 : '',
            clientactors: [null]
        });

        this.witnessForm = this._formBuilder.group({
            witnessName: [null, Validators.required],
            petitionwitnessaddress: [null]
        });

        this.cinasubpoenadForm = this._formBuilder.group(
            {
               intakeservicerequestpetitionid: [null],
               cinapetitionid: [null],
               insertedby: [null],
               insertedon: [null],
               updatedby: [null],
               updatedon: [null],
               activeflag: [null],
               institution: [null],
               custodianname: [null],
               typeofrecord: [null],
               address: [null],
               zipcode: [null],
               personto: [null],
               isreleasenecessary: [null],
               isitinfile: [null],
               filedetails: [null]
            });

        this.cinaSiblingForm = this._formBuilder.group(
            {
                cinasiblingid: [null],
                intakeservicerequestpetitionid: [null],
                cinapetitionid: [null],
                insertedby: [null],
                insertedon: [null],
                updatedby: [null],
                updatedon: [null],
                activeflag: [null],
                siblingname: [null],
                issibinginchildcare: [null],
                whysiblinginchildcare: [null],
                isabuseneglect: [null],
                isother: [null],
                otherreason: [null],
                siblingcps: [null],
                siblingchildwelfareservices: [null],
                sibingcina: [null],
                siblingrelationshipstatus: [null],
                narrative: [null]
            });

        this.cinaForm = this._formBuilder.group({
            cinapetitionid : [null],
            intakeservicerequestpetitionid : [null],
            insertedby: [null],
            insertedon: [null],
            updatedby: [null],
            updatedon: [null],
            activeflag: [null],
            isnew: [null, Validators.required],
            isemergency: [null, Validators.required],
            policecomplaintnumber: [null],
            color: [null],
            legalservicefilenumber: [null],
            childname: [null],
            isfosterhome: [null],
            iskinhome: [1],
            fosterhomename: [null],
            kinhomename: [null],
            kinaddress: [null],
            kinrelation: [null],
            personwithlegalcustody: [null],
            personphysicalcustody: [null],
            legalcustodianrelationship: [null],
            parent1name: [null],
            parent1address: [null],
            isparent1notifiedbyacdss: [null],
            isparent1notified: [null],
            reasonforparent1notnotified: [null],
            parent2name: [null],
            parent2address: [null],
            isparent2notifiedbyacdss: [null],
            isparent2notified: [null],
            reasonforparent2notnotified: [null],
            caseworker: this.userInfo ? this.userInfo.user.userprofile.displayname : null,
            supervisorname: [null],
            daterequestcompleted: [null],
            childinsheltercareon: [null],
            dateofemergencysheltercare: [null],
            ispreviousjuvenilecourt: [null],
            ischildorsibling: [null],
            physicalabusenature: [null],
            physicalabusemedicalexam: [null],
            physicalabusedocumentation: [null],
            physicalabusefailedtoprotect: [null],
            physicalabusedisclosedto: [null],
            sexualabusenature: [null],
            sexualabusemedicalexam: [null],
            sexualabusedocumentation: [null],
            sexualabusefailedtoprotect: [null],
            sexualabusedisclosedto: [null],
            neglectabusenature: [null],
            neglectabusemedicalexam: [null],
            neglectabusedocumentation: [null],
            neglectabusefailedtoprotect: [null],
            neglectabusedisclosedto: [null],
            within12months: [null],
            severechronicdisability: [null],
            mentalhealthdisorder: [null],
            physicalissues: [null],
            bornsubstanceexposed: [null],
            cinachildmedical: [null],
            psychological: [null],
            disability: [null],
            currentlocation: [null],
            medical: [null],
            childrelationshipwithparentsreason: [null],
            legalstatusreason: [null],
            homeconditiondescription: [null],
            inadequatehousing: [null],
            parentcannotidentified: [null],
            parentcannotidentifiedreason: [null],
            parentlocationunknown: [null],
            parentlocationunknownreason: [null],
            departmentattempttolocateparents: [null],
            parentphysicalmentalissues: [null],
            isparentincarcerated: [null],
            parentincarcerated: [null],
            isparenteconomicstatus: [null],
            parenteconomicstatus: [null],
            isparentnotcareforchild: [null],
            parentnotcareforchild: [null],
            isparentsubstance: [null],
            parentsubstance: [null],
            isparentadmitted: [null],
            parentadmitted: [null],
            isparentrefused: [null],
            parentrefused: [null],
            isparentnotcompletetreatment: [null],
            parentnotcompletetreatment: [null],
            isparentuncooperative: [null],
            parentuncooperative: [null],
            isparentsafetyplan: [null],
            parentsafetyplan: [null],
            parentcps: [null],
            parentchildwelfareservices: [null],
            parentcriminal: [null],
            parentcina: [null],
            siblingname: [null],
            siblingcps: [null],
            siblingchildwelfareservices: [null],
            sibingcina: [null],
            issiblingrelationship: [null],
            activechildwelfare: [null],
            effortsforpreventremoval: [null],
            ismonitoredchildsafety: [null],
            monitoredchildsafety: [null],
            isofferedchildwelfareservices: [null],
            offeredchildwelfareservices: [null],
            ismedicalservices: [null],
            medicalservices: [null],
            isparentingclasses: [null],
            parentingclasses: [null],
            isdisorderscreening: [null],
            disorderscreening: [null],
            ismentalhealth: [null],
            mentalhealth: [null],
            isexploredrelative: [null],
            exploredrelative: [null],
            isotherreasons: [null],
            otherreasons: [null],
            werereasonableeffortsmade: [null],
            reasonableeffortsmade: [null],
            wasfamilymeetingheld: [null],
            familymeetingdate: [null],
            familymeetingparticipants: [null],
            familymeetingoutcome: [null],
            dateofremoval: [null],
            timeofremoval: [null],
            typeofplacement: [null],
            otherinformation: [null],
            photoinformationexists: [null],
            whohasevidence: [null],
            // typeofrecord: [null],
            cinasubpoenad: [null],
            cinasibling: [null],
            petitionwitness: [null],
            isdepartmentattempttolocateparents: [null],
            isparentphysicalmentalissues: [null],
            caseworkerphonenumber: [null],
            supervisorphonenumber: [null],
            personwithlegalcustodyname: [null],
            personphysicalcustodyname: [null],
            nameofthechild: [null],
            childdob: [null],
            childrace: [null],
            childgender: [null]

        });

        this.cinaForm.patchValue({daterequestcompleted : new Date()});
    }
    activateSpeechToText(type): void {
        this.recognizing = type;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
          this._speechRecognitionService.record().subscribe(
            // listener
            (value) => {

                  const narrative = this.cinaSiblingForm.getRawValue().narrative;
                  this.cinaSiblingForm.patchValue({ narrative: narrative + ' ' + this.speechData });

            },
            // errror
            (err) => {
              console.log(err);
              this.recognizing = false;
              if (err.error === 'no-speech') {
                this.notification = `No speech has been detected. Please try again.`;
                this._alertService.warn(this.notification);
                this.activateSpeechToText(type);
              } else if (err.error === 'not-allowed') {
                this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                this._alertService.warn(this.notification);
                // this.activateSpeechToText(type);
              } else if (err.error === 'not-microphone') {
                this.notification = `Microphone is not available. Please verify the connection of your microphone and try again.`;
                this._alertService.warn(this.notification);
                // this.activateSpeechToText(type);
              }
            },
            // completion
            () => {
              this.speechRecogninitionOn = true;
              console.log('--complete--');
              this.activateSpeechToText(type);
            }
          );
        } else {
          this.recognizing = false;
          this.deActivateSpeechRecognition();
        }
      }

      deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
      }

      ngOnDestroy(): void {
        this._speechRecognitionService.destroySpeechObject();
      }

    addCinaSubpoenad() {
      const cinaSubpoenadObj = this.cinasubpoenadForm.getRawValue();
      if (this.cinasubpoenad && this.cinasubpoenad.length) {
          this.cinasubpoenad.push(cinaSubpoenadObj);
      } else {
        this.cinasubpoenad = [];
        this.cinasubpoenad.push(cinaSubpoenadObj);
      }
      this.cinasubpoenadForm.reset();
      this.reset();
    }

    addCinaSibling() {
        const cinaSiblingObj = this.cinaSiblingForm.getRawValue();
        if (this.cinaSibling && this.cinaSibling.length) {
            this.cinaSibling.push(cinaSiblingObj);
        } else {
          this.cinaSibling = [];
          this.cinaSibling.push(cinaSiblingObj);
        }
        this.cinaSiblingForm.reset();
        this.reset();
      }

    getSupervisorsList() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'PCAUTH' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.supervisorsList = result.data;
            });
     }

    onChangeLegalCustody(persionIdArray) {
        if (persionIdArray && persionIdArray.length && Array.isArray(persionIdArray)) {
            let legalcustodianrelationship = '';
            persionIdArray.forEach(persionId => {
                legalcustodianrelationship = legalcustodianrelationship + (legalcustodianrelationship ? ',' : '') + this.getRelationShip(persionId);
                if (legalcustodianrelationship) {
                    this.cinaForm.patchValue({ legalcustodianrelationship: legalcustodianrelationship });
                }
            });
        }
    }
    getRelationShip(id) {
        const selectedChilds = this.petitionDetailsForm.getRawValue().focusname;
        const childid = (Array.isArray(selectedChilds) && selectedChilds.length >= 1) ? selectedChilds[0] : null;
        const child = this.involvedPersons.find(item => item.intakeservicerequestactorid === childid);
        if (child) {
            const SelectedPerson = this.clientActor.find(person => person.intakeservicerequestactorid === id);
            if (SelectedPerson) {
                const relationshipList = (Array.isArray(SelectedPerson.relationshipList)) ? SelectedPerson.relationshipList : [];
                const relationitem = relationshipList.find(item =>
                    item.personid === child.personid
                );
                return (relationitem) ? relationitem.relation : '';
            }
        }
    }
    addWitness() {
        const witnessId = this.witnessForm.getRawValue().witnessName;
        const witnessAddress = this.witnessForm.getRawValue().petitionwitnessaddress;
        const witnessObj = { 'personid': witnessId, 'petitionwitnessaddress': witnessAddress };
        if (this.petitionwitness && this.petitionwitness.length) {
            this.petitionwitness.push(witnessObj);
        } else {
            this.petitionwitness = [];
            this.petitionwitness.push(witnessObj);
        }
        this.witnessForm.reset();
    }

    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = tt % 60; // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }
    private loadDroddowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1 },
                    method: 'get',
                    nolimit: true,
                    order: 'description'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'countyname'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmCountyListUrl + '?filter'
            ),
        ])
            .map(([petitionType, countyList]) => {
                return {
                    petitionType: petitionType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.petitiontypekey
                            })
                    ),
                    countyList: countyList.map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyid
                            })
                    ),
                };
            })
            .share();

        this.petitionType$ = source.pluck('petitionType');
        this.countyList$ = source.pluck('countyList');
        this.petitionType$.subscribe(pt => {
                      this.petitionTypeList  = pt;
         });
        this.countyList$.subscribe(res => {
            this.countyList = res;
        });
    }

    patchValuseData() {
        this._commonHttpService
            .getArrayList(
                {
                    where: {
                        intakeservicerequestid: null,
                        objectid: this.id,
                        objecttype: 'servicecase'
                    },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionDetailslist + '?filter'
            )
            .subscribe((res) => {
                // this.patchForm(res[0]);
            });
    }
    // patchForm(data: PetitionDetails) {
    //     if (data) {
    //         data.hearingDate = moment(data.hearingdatetime).format('YYYY-MM-DD');
    //         data.hearingTime = moment.utc(data.hearingdatetime).format('hh:mm A');
    //         this.petitionDetailsForm.patchValue(data);
    //     }
    // }

    setCinaChild(item) {
        let firstName , lastName, childdob, childrace, childgender, removalDate, removalTime = null;
        firstName = this.getPersonPropertybyId(item, 'firstname');
        lastName = this.getPersonPropertybyId(item, 'lastname');
        childdob = this.getPersonPropertybyId(item, 'dob');
        childrace = this.getPersonPropertybyId(item, 'racetypekey');
        childgender = this.getPersonPropertybyId(item, 'gender');
        removalDate = this.getPersonPropertybyId(item, 'removaldate');
        removalTime = removalDate ? moment.utc(removalDate).format('hh:mm A') : null;
        this.cinaForm.patchValue({nameofthechild: firstName + ' ' + lastName});
        this.cinaForm.patchValue({childdob: childdob});
        this.cinaForm.patchValue({childrace: childrace});
        this.cinaForm.patchValue({childgender: childgender});
        this.cinaForm.patchValue({dateofremoval: removalDate});
        this.cinaForm.patchValue({timeofremoval: removalTime});

    }
    petitionCinaSave(petition) {
        let petitionId;
        if (petition && petition.Intakeservicerequestpetition) {
            petitionId = petition.Intakeservicerequestpetition.intakeservicerequestpetitionid;
        } else {
            petitionId = this.cinaForm.getRawValue().intakeservicerequestpetitionid;
        }
        if (this.petitionwitness && this.petitionwitness.length) {
            this.petitionwitness.forEach(element => {
                element.petitionid = petitionId;
                element.clientmergeid = petitionId;
            });
        }
        if (this.cinasubpoenad && this.cinasubpoenad.length ) {
            this.cinasubpoenad.forEach(element => {
                element.intakeservicerequestpetitionid = petitionId;
                element.cinapetitionid = petitionId;
            });
        }
        if (this.cinaSibling && this.cinaSibling.length ) {
            this.cinaSibling.forEach(element => {
                element.intakeservicerequestpetitionid = petitionId;
                element.cinapetitionid = petitionId;
            });
        }
        const personphysicalcustody = this.cinaForm.getRawValue().personphysicalcustody;
        const personwithlegalcustody = this.cinaForm.getRawValue().personwithlegalcustody;
        const personwithlegalcustodyname  = this.getPersonFullName(personphysicalcustody);
        const personphysicalcustodyname  = this.getPersonFullName(personwithlegalcustody);
        this.cinaForm.patchValue({personwithlegalcustodyname: personwithlegalcustodyname});
        this.cinaForm.patchValue({personphysicalcustodyname: personphysicalcustodyname});
        this.cinaForm.patchValue({cinasubpoenad: this.cinasubpoenad});
        this.cinaForm.patchValue({ petitionwitness: this.petitionwitness });
        this.cinaForm.patchValue({ cinasibling: this.cinaSibling });
        this.cinaForm.patchValue({ intakeservicerequestpetitionid: petitionId ? petitionId : null });
        const cinaForm = this.cinaForm.getRawValue();
        this._commonHttpService.create(cinaForm, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionCinaDetailsSave).subscribe(
            (res) => {
                //  this._alertService.success('Petition Cina details saved successfully!');
                this.cinaForm.reset();
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    ParentCircumstanceChange(isSelected, formControlName) {
        if (isSelected) {
            this.cinaForm.controls[formControlName].setValidators([Validators.required]);
            this.cinaForm.controls[formControlName].updateValueAndValidity();
        } else {
            // this.recordingForm.controls['intakeservicerequestactorid'].reset();
            this.cinaForm.controls[formControlName].clearValidators();
            this.cinaForm.controls[formControlName].updateValueAndValidity();
        }
    }

    getPersonPropertybyId(personid, propertyName) {
        return this.getChildPropertyValue(personid, propertyName);
    }

    getChildPropertyValue(id, propertyName) {
        const SelectedPerson = this.involvedPersons.filter(person => person.intakeservicerequestactorid === id);
        if (SelectedPerson && SelectedPerson.length) {
            return SelectedPerson[0][propertyName];
        }
        return null;
    }

    getPropertyValue(id, propertyName) {

        const SelectedPerson = this.clientActor.filter(person => person.intakeservicerequestactorid === id);
        if (SelectedPerson && SelectedPerson.length) {
            return SelectedPerson[0][propertyName];
        }
        return null;
    }

    petitionDetailsSave() {
        console.log(this.petitionDetailsForm.getRawValue());
        const petitionID = this.petitionDetailsForm.getRawValue().petitionid;
        if (!petitionID) {
            this.petitionDetailsForm.patchValue({ petitionid: 'To be confirmed' });
        }
        this.petitionDetailsForm.patchValue({ servicecaseid: this.id });
        this.petitionDetails = Object.assign(
            {
                petitionactors: this.intakeservicerequestpetitionactors,
                clientactors: this.clientActors
            },
            this.petitionDetailsForm.value
        );
        this.petitionDetails['clientactors'] = this.clientActors;

        console.log(this.petitionDetails);
        // const hearingDatetimeformate = moment(
        //     this.petitionDetailsForm.value.hearingDate
        // ).format('YYYY/MM/DD');

        // this.petitionDetails.hearingdatetime =
        //     hearingDatetimeformate +
        //     ' ' +
        //     this.petitionDetailsForm.value.hearingTime;

        // this.petitionDetails.servicecaseid =  this.id;
        const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        if (isServiceCase) {
            this.petitionDetails['servicecaseid'] = this.id;
            // Sending Default value for intakeservicerequestid as the API throws error for null value
            this.petitionDetails['intakeservicerequestid'] = 'deda9fed-a731-4151-ab8b-5f6cef7b60c4';
        } else {
            delete this.petitionDetails.servicecaseid;
            this.petitionDetails['intakeservicerequestid'] = this.id;
        }

        delete this.petitionDetails.focusname;

        this._commonHttpService.create(this.petitionDetails, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionDetailsSave).subscribe(
            (res: PetitionDetails) => {
                // this.patchForm(res[0]);
                this.petitionDetailsForm.reset();
                this.updateButton = false;
                this.getPetitionDetailsList();
                if (this.petitionDetails.petitiontypekey === 'CINA') {
                    this.petitionCinaSave(res);
                }
                if (this.petitionDetails.intakeservicerequestpetitionid === null) {
                    this._alertService.success('Petition details saved successfully!');
                } else {
                    this._alertService.success('Petition details updated successfully!');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    validateForTPR($event) {
        if ($event.value === 'TPR') {
            this.isTPRvalidate = true;
        } else {
            this.isTPRvalidate = false;
        }
        if ($event.value === 'Waiver') {
            this.showWaiverReason = true;
        } else {
            this.showWaiverReason = false;
        }
    }

    onChangeParent2Det(key) {
        const parent2DetKey = ['parentsmarriedatchildbirth', 'parentaffidavit', 'paternityproceeding', 'paternitydone', 'parentdeterminedby'];
        parent2DetKey.forEach(element => {
            if (element !== key) {
                const formObj = {};
                formObj[element] = null;
                this.cinaForm.patchValue(formObj);
            }
        });
    }

    onChangeSiblingDD(key) {
        const siblinkKey = ['isabuseneglect', 'isother'];
        siblinkKey.forEach(element => {
            if (element !== key) {
                const formObj = {};
                formObj[element] = null;
                this.cinaSiblingForm.patchValue(formObj);
            }
        });
    }


    onChangePlacementOpt(key) {
        const placementOpt = ['placedinfosterfamily', 'isplacedinother'];
        placementOpt.forEach(element => {
            if (element !== key) {
                const formObj = {};
                formObj[element] = null;
                this.cinaForm.patchValue(formObj);
            }
        });

    }

    onChangeParent1Notify(key) {
        const parent1NotifyOpt = ['isparent1notifiedbyacdss', 'isparent1notified'];
        parent1NotifyOpt.forEach(element => {
            if (element !== key) {
                const formObj = {};
                formObj[element] = null;
                this.cinaForm.patchValue(formObj);
            }
        });
    }


    onChangeHome(key) {
        const FormOpt = ['iskinhome', 'isfosterhome'];
        FormOpt.forEach(element => {
            if (element !== key) {
                const formObj = {};
                formObj[element] = null;
                this.cinaForm.patchValue(formObj);
            }
        });
    }

    onChangeParent2Notify(key) {
        const parent2NotifyOpt = ['isparent2notifiedbyacdss', 'isparent2notified'];
        parent2NotifyOpt.forEach(element => {
            if (element !== key) {
                const formObj = {};
                formObj[element] = null;
                this.cinaForm.patchValue(formObj);
            }
        });
    }

    getPersonAddress(personid) {
        const selectedPerson = this.clientActor.filter(person => person.intakeservicerequestactorid === personid);

        if (selectedPerson && selectedPerson.length) {
            const fullAddress = (selectedPerson[0].address ? selectedPerson[0].address : '') +
                (selectedPerson[0].address2 ? ',' + selectedPerson[0].address2 : '') +
                (selectedPerson[0].city ? ',' + selectedPerson[0].city : '') +
                (selectedPerson[0].state ? ',' + selectedPerson[0].state : '') +
                (selectedPerson[0].county ? ',' + selectedPerson[0].county : '') +
                (selectedPerson[0].country ? ',' + selectedPerson[0].country : '');
            return fullAddress;
        }
        return null;
    }

    patchPersonAddress(persionId, FieldName) {
        const personAddress = this.getPersonAddress(persionId);
        const obj = {};
        obj[FieldName] = personAddress;
        this.cinaForm.patchValue(obj);
    }

    resetForm() {
        this.cinaForm.reset();
        this.petitionwitness = [];
        this.petitionDetailsForm.reset();
    }
    patchCinaForm(petitionid, isDownload) {
        this._commonHttpService
            .getSingle(
                {
                    where: {
                        intakeservicerequestpetitionid: petitionid
                    },
                    limit: 10,
                    page: 1,
                    count: -1,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionCinaDetailList
            )
            .subscribe((cinaRes) => {
                // this.patchForm(res[0]);
                // this._commonHttpService
                //     .getSingle(
                //         {
                //             where: {
                //                 petitionid: petitionid
                //             },
                //             limit: 10,
                //             page: 1,
                //             count: -1,
                //             method: 'get'
                //         },
                //         CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.PetitionCinaWitnessList
                //     )
                //     .subscribe((witnessRes) => {
                        // this.patchForm(res[0]);
                        this.petitionwitness = [];
                        this.cinaSibling = [];
                        this.cinasubpoenad = [];

                        if (cinaRes && cinaRes.data && cinaRes.data.length) {
                        this.petitionwitness = cinaRes.petitionwitness;
                        this.cinaSibling = cinaRes.cinasibling;
                        this.cinasubpoenad = cinaRes.cinasubpoenad;
                        this.cinaPeitition = cinaRes.data[0];
                        this.cinaPeitition.personphysicalcustody  =  (this.cinaPeitition.personphysicalcustody && this.cinaPeitition.personphysicalcustody !== 'null') ?
                                                                    this.cinaPeitition.personphysicalcustody.split(',') : null;
                        this.cinaPeitition.personwithlegalcustody =  (this.cinaPeitition.personwithlegalcustody && this.cinaPeitition.personwithlegalcustody !== 'null') ?
                                                                    this.cinaPeitition.personwithlegalcustody.split(',') : null;

                        // if(this.cinaPeitition && this.cinaPeitition.removaltime) {
                        // this.cinaPeitition.removaltime = moment(this.cinaPeitition.removaltime).format('hh:mm A');
                        // }
                        if (isDownload) {
                            this.downloadCINAForm(this.cinaPeitition, this.petitionwitness);
                        } else {
                            this.cinaForm.patchValue(this.cinaPeitition);
                            if(this.cinaPeitition && this.cinaPeitition.removaltime) {
                            const removalTime =  moment(this.cinaPeitition.removaltime).format('hh:mm A');
                             this.petitionDetailsForm.controls['removaltime'].patchValue(removalTime);
                            }
                        }
                      }
                    // });

            });
    }

    getPersonFullName(personId) {
        const selectedPerson = this.clientActor.filter(person => person.intakeservicerequestactorid === personId);

        if (selectedPerson && selectedPerson.length) {
            const fullName = (selectedPerson[0].firstname ? selectedPerson[0].firstname : '') + ' ' +
                (selectedPerson[0].lastname ? selectedPerson[0].lastname : '');
            return fullName;
        }
        return null;
    }

    getCINAForm(petition) {
        this.selectedPetition = petition;
        this.patchCinaForm(petition.intakeservicerequestpetitionid, true);
    }

    downloadCINAForm(petition, witness) {
        this.petitionfocusid = this.selectedPetition.actordetails.map((res) => {
            return res.intakeservicerequestactorid;
        })[0];
        const detailsForm = this.petitionDetailsForm.getRawValue();
        // const details = {
        //     courtcasenumber: detailsForm.courtcasenumber,
        //     childid: this.getPersonPropertybyId(detailsForm.focusname, 'cjamspid'),
        //     name: this.getPersonPropertybyId(detailsForm.focusname, 'firstname') + ' ' + this.getPersonPropertybyId(detailsForm.focusname, 'lastname'),
        //     associatedattorneys: detailsForm.associatedattorneys
        // };
        // petition.courtcasenumber = detailsForm.courtcasenumber;
        petition.cjamspid = this.getPersonPropertybyId(this.petitionfocusid, 'cjamspid');
        petition.name = this.getPersonPropertybyId(this.petitionfocusid, 'firstname') + ' ' + this.getPersonPropertybyId(this.petitionfocusid, 'lastname');
        petition.associatedattorneys = this.selectedPetition.associatedattorneys;

        petition.parent1namevalue = this.getPersonFullName(petition.parent1name);
        petition.parent2namevalue = this.getPersonFullName(petition.parent2name);
        petition.personwithlegalcustodyvalue = this.getPersonFullName(petition.personwithlegalcustody);
        petition.personphysicalcustodyvalue = this.getPersonFullName(petition.personphysicalcustody);
        petition.countyname = this.countyList.find(county => county.value === petition.countyname).text;
        if (witness && witness.length) {
            witness.forEach(element => {
                element.personidvalue = this.getPersonFullName(element.personid);
                element.address = this.getPersonAddress(element.personid);
            });
        }
        const json = {
            cina: this.cinaPeitition,
            witness: this.petitionwitness// ,
            // details: details
        };
        const condition = {
            'count': -1,
            'where': {
                'documenttemplatekey': [
                    'cinaForm'
                ],
                'isheaderrequired': false,
                'json': json
            },
            'method': 'post',
            'limit': 10, 'page': 1
        };
        this.http.post(this.baseUrl  + '/' + 'Evaluationdocument/v1' + '/' +  'evaluationdocument/generateintakedocument', condition, { responseType: 'arraybuffer' })
            .subscribe(res => {
                const blob = new Blob([new Uint8Array(res)]);
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = 'CINA_FORM.pdf';

                document.body.appendChild(link);

                link.click();

                document.body.removeChild(link);
            });
        // return this._httpService.create(condition, CommonUrlConfig.EndPoint.RESTITUTION.REPORTS.GENERATE + documentType);
    }

    patchTestData(isResetForm) {
// tslint:disable-next-line: max-line-length
        const testData = {'cinapetitionid': null, 'intakeservicerequestpetitionid': '76dbfe04-f70a-4330-b39e-a85f2db26229', 'insertedby': null, 'insertedon': null, 'updatedby': null, 'updatedon': null, 'activeflag': null, 'isnew': false, 'isemergency': false, 'policecomplaintnumber': 'asdasd', 'color': 'asdas', 'legalservicefilenumber': 'asdaas', 'childname': null, 'isfosterhome': true, 'fosterhomename': 'asas', 'kinhomename': 'asdas', 'kinaddress': 'dasd', 'kinrelation': 'asdsad', 'personwithlegalcustody': '0073467a-242b-4276-9f48-7904e494acb2', 'personphysicalcustody': '0073467a-242b-4276-9f48-7904e494acb2', 'legalcustodianrelationship': '0073467a-242b-4276-9f48-7904e494acb2', 'parent1name': '0073467a-242b-4276-9f48-7904e494acb2', 'parent1address': null, 'isparent1notifiedbyacdss': true, 'isparent1notified': null, 'reasonforparent1notnotified': null, 'parent2name': '0073467a-242b-4276-9f48-7904e494acb2', 'parent2address': null, 'isparent2notifiedbyacdss': true, 'isparent2notified': null, 'reasonforparent2notnotified': null, 'caseworker': 'dasda', 'supervisorname': 'sadasdad', 'daterequestcompleted': null, 'childinsheltercareon': '2019-03-27T18:30:00.000Z', 'dateofemergencysheltercare': '2019-03-20T18:30:00.000Z', 'ispreviousjuvenilecourt': 'no', 'ischildorsibling': null, 'physicalabusenature': 'adas', 'physicalabusemedicalexam': 'asd', 'physicalabusedocumentation': 'asd', 'physicalabusefailedtoprotect': 'asd', 'physicalabusedisclosedto': 'asd', 'sexualabusenature': 'asd', 'sexualabusemedicalexam': 'asd', 'sexualabusedocumentation': 'asa', 'sexualabusefailedtoprotect': 'asdasd', 'sexualabusedisclosedto': 'asd', 'neglectabusenature': 'asdasd', 'neglectabusemedicalexam': 'dsfdsfdf', 'neglectabusedocumentation': 'sdfsdfssdf', 'neglectabusefailedtoprotect': 'sdfsdsd', 'neglectabusedisclosedto': 'sdfsdf', 'within12months': 0, 'severechronicdisability': 0, 'mentalhealthdisorder': 0, 'physicalissues': 'sfdsfsd', 'bornsubstanceexposed': 1, 'cinachildmedical': true, 'psychological': true, 'disability': true, 'currentlocation': 'sdfsd', 'medical': null, 'childrelationshipwithparentsreason': 'sdfsd', 'legalstatusreason': 'sfsdsdf', 'homeconditiondescription': 'sdfsdfsd', 'inadequatehousing': 1, 'parentcannotidentified': 1, 'parentcannotidentifiedreason': 'sdfsdf', 'parentlocationunknown': 'sdfsdfsd', 'parentlocationunknownreason': 'ssdfsdfsf', 'departmentattempttolocateparents': null, 'parentphysicalmentalissues': null, 'isparentincarcerated': 0, 'parentincarcerated': 'sfdfsdfsd', 'isparenteconomicstatus': null, 'parenteconomicstatus': 'sdfsdf', 'isparentnotcareforchild': null, 'parentnotcareforchild': 'fsdfsdsd', 'isparentsubstance': null, 'parentsubstance': 'sdsdfs', 'isparentadmitted': null, 'parentadmitted': 'sfsdfs', 'isparentrefused': null, 'parentrefused': 'sdfsdfs', 'isparentnotcompletetreatment': null, 'parentnotcompletetreatment': 'sdfdfsdf', 'isparentuncooperative': null, 'parentuncooperative': 'sdfsd', 'isparentsafetyplan': null, 'parentsafetyplan': 'sdfsdfsdf', 'parentcps': 'sdfsdf', 'parentchildwelfareservices': 'sdfsd', 'parentcriminal': 'sdfsd', 'parentcina': 'sdfsd', 'siblingcps': 'sdfsd', 'siblingchildwelfareservices': 'sdfsd', 'sibingcina': 'dfsfsd', 'issiblingrelationship': 'sdfsd', 'activechildwelfare': 0, 'effortsforpreventremoval': 'll that apply', 'ismonitoredchildsafety': true, 'monitoredchildsafety': 'In-Home Services', 'isofferedchildwelfareservices': true, 'offeredchildwelfareservices': 'Welfare Services', 'ismedicalservices': true, 'medicalservices': 'necessary monitoring', 'isparentingclasses': true, 'parentingclasses': ' necessary monitoring', 'isdisorderscreening': true, 'disorderscreening': 'provided ', 'ismentalhealth': true, 'mentalhealth': ' monitoring', 'isexploredrelative': true, 'exploredrelative': 'sdfsdfsfs', 'isotherreasons': true, 'otherreasons': 'sdfsdf', 'werereasonableeffortsmade': null, 'reasonableeffortsmade': 'Efoorts', 'wasfamilymeetingheld': true, 'familymeetingdate': '2019-03-13T18:30:00.000Z', 'familymeetingparticipants': 'as', 'familymeetingoutcome': 'asd', 'dateofremoval': '2019-03-28T18:30:00.000Z', 'timeofremoval': '00:00', 'otherinformation': 'asdas', 'photoinformationexists': true, 'whohasevidence': 'asdas', 'typeofrecord': null};
        if (!isResetForm) {
        this.cinaForm.patchValue(testData);
        }  else {
            this.cinaForm.reset();
        }
    }

    reset() {
        this.cinaSiblingForm.enable();
        this.cinaSiblingForm.reset();
        this.selectedIndex = -1;
        this.cinasubpoenadForm.enable();
        this.cinasubpoenadForm.reset();
        this.viewMode = false;
        (<any>$('#add-sibling')).modal('hide');
        (<any>$('#add-sub')).modal('hide');
    }

    viewSibling(item, isView) {
      this.viewMode = isView;
      this.cinaSiblingForm.patchValue(item);
      this.cinaSiblingForm.disable();
    }

    editSibling(item, index) {
      this.cinaSiblingForm.enable();
      this.selectedIndex = index;
      this.cinaSiblingForm.patchValue(item);
    }

    updateSibling() {
        const cinaSiblingData = this.cinaSiblingForm.getRawValue();
        if (this.cinaSibling && this.cinaSibling.length) {
            this.cinaSibling[this.selectedIndex] = cinaSiblingData;
        }
    }


    viewSub(item, isView) {
        this.viewMode = isView;
        this.cinasubpoenadForm.patchValue(item);
        this.cinasubpoenadForm.disable();
      }

    editSub(item, index) {
        // const index = this.cinaSibling.indexOf(item.cinasiblingid);
        this.selectedIndex = index;
        this.cinasubpoenadForm.enable();
        this.selectedIndex = index;
        this.cinasubpoenadForm.patchValue(item);
      }

    updateSub() {
          const cinaSub = this.cinasubpoenadForm.getRawValue();
          if (this.cinasubpoenad && this.cinasubpoenad.length) {
              this.cinasubpoenad[this.selectedIndex] = cinaSub;
          }
      }

      documentGenerate(petitionId) {
        const modal = {
          count: -1,
          where: {
            documenttemplatekey: ['cinaForm'],
            intakeservicerequestpetitionid: petitionId,
            isheaderrequired: false,
            // date_sw: childReport.date_sw,
            // date_from: childReport.date_from,
            // date_to: childReport.date_to,
            format: 'pdf'
          },
          method: 'post'
        };
          this._commonHttpService.download('evaluationdocument/generateintakedocument', modal)
            .subscribe(res => {
              const blob = new Blob([new Uint8Array(res)]);
              const link = document.createElement('a');
              link.href = window.URL.createObjectURL(blob);
              link.download = 'CinaForm.pdf';
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
            //   (<any>$('#downloadReport')).modal('hide');
            });
      }

      getClientAttorney(clientAttorney) {
        if (clientAttorney && Array.isArray(clientAttorney)) {
            return clientAttorney.join();
        }
        return '';
      }
}

