import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsCourtProcessingComponent } from './as-court-processing.component';

describe('AsCourtProcessingComponent', () => {
  let component: AsCourtProcessingComponent;
  let fixture: ComponentFixture<AsCourtProcessingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsCourtProcessingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsCourtProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
