import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourtRoutingModule } from './court-routing.module';
import { CourtTabComponent } from './court-tab/court-tab.component';
import { NotesComponent } from './notes/notes.component';
import { CourtComponent } from './court.component';
import { HearingDetailComponent } from './hearing-detail/hearing-detail.component';
import { PetitionDetailComponent } from './petition-detail/petition-detail.component';
import { CourtActionsComponent } from './court-actions/court-actions.component';
import { CourtDetailComponent } from './court-detail/court-detail.component';
import { CourtOrderComponent } from './court-order/court-order.component';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { AsCourtProcessingComponent } from './as-court-processing/as-court-processing.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxMaskModule } from 'ngx-mask';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { QuillModule } from 'ngx-quill';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { LegalCustodyComponent } from './legal-custody/legal-custody.component';
import { InvolvedPersonsService } from '../../../shared-pages/involved-persons/involved-persons.service';
import { CommonControlsModule } from '../../../../shared/modules/common-controls/common-controls.module';
import { MatTooltipModule } from '@angular/material';
@NgModule({
    imports: [
        CommonModule,
        CourtRoutingModule,
        SharedDirectivesModule,
        FormMaterialModule,
        NgxfUploaderModule.forRoot(),
        NgxMaskModule.forRoot(),
        A2Edatetimepicker,
        QuillModule,
        ControlMessagesModule,
        MatTooltipModule,
        CommonControlsModule
    ],
    declarations: [
        CourtComponent,
        NotesComponent,
        CourtTabComponent,
        HearingDetailComponent,
        PetitionDetailComponent,
        CourtActionsComponent,
        CourtDetailComponent,
        CourtOrderComponent,
        AsCourtProcessingComponent,
        LegalCustodyComponent
    ],
    providers: [InvolvedPersonsService]
})
export class CourtModule {}
