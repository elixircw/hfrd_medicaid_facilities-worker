import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { CourtAction, CourtDetails, PetitionDetails } from '../_entities/court.data.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'court-detail',
  templateUrl: './court-detail.component.html',
  styleUrls: ['./court-detail.component.scss']
})
export class CourtDetailComponent implements OnInit {
    offenceCategories$: Observable<DropdownModel[]>;
    courtActionsType$: Observable<DropdownModel[]>;
    conditionTypes$: Observable<DropdownModel[]>;
    courtOrder$: Observable<DropdownModel[]>;
    findingsType$: Observable<DropdownModel[]>;
    adijuctedDecisions$: Observable<DropdownModel[]>;
    hearingStatusTypeItem$: Observable<DropdownModel[]>;
    courtDetailsForm: FormGroup;
    userInfo: AppUser;
    times: string[] = [];
    id: string;
    courtDetails: CourtDetails;
    courtAction: CourtAction[] = [];
    petitionDetails: PetitionDetails;
    daNumber: string;
    courtActionDescription: string[] = [];

    constructor(private _commonHttpService: CommonHttpService, private _formBuilder: FormBuilder,
         private _authService: AuthService,  private _route: ActivatedRoute,
          private _alertService: AlertService, private _router: Router,private _dataStoreService: DataStoreService) { }

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        this.getPetitionDetails();
        this.userInfo = this._authService.getCurrentUser();
        this.initiateFormGroup();
         if (this.id) {
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: {
                        intakeservicerequestid: this.id
                    }
                },
                'intakeservicerequestcourtaction/getcourtaction' + '?filter'
            )
            .subscribe(
                (result) => {
                    if (result[0]) {
                        if (result[0]['courtaction']) {
                        result[0].courtactiontypekey = result[0].courtaction.map(item => item.courtactiontypekey);
                        result[0].courtaction = result[0].courtaction.map(item => {
                            return {courtactiontypekey: item.courtactiontypekey};
                        });
                        this.courtAction = result[0].courtaction;
                    }
                        this.patchForm(result[0]);
                    }
                },
                (error) => {
                }
            );
         }
        this.times = this.generateTimeList(true);
    }

    initiateFormGroup() {
        this.courtDetailsForm = this._formBuilder.group({
            legalcounselname: ['', Validators.required],
            magistratename: ['', Validators.required],
            workername: this.userInfo ? this.userInfo.user.userprofile.displayname : '',
            hearingdatetime: [''],
            courtactiontypekey: [''],
            courtordertypekey: ['', Validators.required],
            courtorderdatetime: [''],
            conditiontypekey: ['', Validators.required],
            conditiontypedescription: ['', Validators.required],
            conditiontypecompletiondatetime: [''],
            terminationdatetime: [''],
            adjudicationdatetime: [''],
            adjudicationdecision: ['', Validators.required],
            allegationid: ['', Validators.required],
            adjudicationDate: ['', Validators.required],
            adjudicationTime: ['', Validators.required],
            hearingDate: ['', Validators.required],
            hearingTime: ['', Validators.required],
            terminationDate: ['', Validators.required],
            terminationTime: ['', Validators.required],
            orderDate: ['', Validators.required],
            orderTime: ['', Validators.required],
            completionDate: ['', Validators.required],
            completionTime: ['', Validators.required],
            courtcasenumber: [''],
            jurisdiction: [''],
            hearingstatustypekey: ['', Validators.required],
            decisionnotes: ['', Validators.required],
            nexthearingdate: ['']
        });
    }
    patchForm(data: CourtDetails) {
        if (data) {
            data.hearingDate = moment(data.hearingdatetime).format('YYYY-MM-DD');
            data.hearingTime = moment.utc(data.hearingdatetime).format('HH:mm');

            data.adjudicationDate = moment(data.adjudicationdatetime).format('YYYY-MM-DD');
            data.adjudicationTime = moment.utc(data.adjudicationdatetime).format('HH:mm');

            data.completionDate = moment(data.conditiontypecompletiondatetime).format('YYYY-MM-DD');
            data.completionTime = moment.utc(data.conditiontypecompletiondatetime).format('HH:mm');

            data.orderDate = moment(data.courtorderdatetime).format('YYYY-MM-DD');
            data.orderTime = moment.utc(data.courtorderdatetime).format('HH:mm');

            data.terminationDate = moment(data.terminationdatetime).format('YYYY-MM-DD');
            data.terminationTime = moment.utc(data.terminationdatetime).format('HH:mm');
            this.courtDetailsForm.patchValue(data);
        }
      }
    updateCourtDetail() {
        if (this.courtDetailsForm.valid) {
        this.courtDetails = Object.assign({}, this.courtDetailsForm.value );

        this.courtDetails.intakeservicerequestid = this.id;

        this.courtDetails.adjudicationdatetime = moment(this.courtDetailsForm.value.adjudicationDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.adjudicationTime;
        this.courtDetails.conditiontypecompletiondatetime = moment(this.courtDetailsForm.value.completionDate   ).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.completionTime;
        this.courtDetails.courtorderdatetime = moment(this.courtDetailsForm.value.orderDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.orderTime;

        this.courtDetails.hearingdatetime = moment(this.courtDetailsForm.value.hearingDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.hearingTime;

        this.courtDetails.terminationdatetime = moment(this.courtDetailsForm.value.terminationDate).format('YYYY/MM/DD') + ' ' + this.courtDetailsForm.value.terminationTime;
        this.courtDetails.courtaction = this.courtAction;
        this.courtDetails.intakeservicerequestpetitionid = this.petitionDetails.intakeservicerequestpetitionid;
         this._commonHttpService.create( this.courtDetails , CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtActionAddUrl).subscribe( result => {
            this._alertService.success('Court details saved successfully!');
        },
        error => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        });
    } else {
        this._alertService.error('Please fill all fields');
        console.log(this.courtDetailsForm.value);
    }
    }

    selectCourtActionType(event) {
        if (event) {
            const courtActionType = event.map(res => {
                return { courtactiontypekey: res };
            });
            this.courtAction = courtActionType;
            this.courtActionsType$.subscribe(items => {
                if (items) {
                    const getActiontems = items.filter(item => {
                        if (event.includes(item.value)) {
                        return item;
                        }
                    });
                    this.courtActionDescription = getActiontems.map(res => res.text);
                }
            });
        }
    }

    private generateTimeList(is24hrs = true) {
        const x = 15; // minutes interval
        const times = []; // time array
        let tt = 0; // start time
        const ap = [' AM', ' PM']; // AM-PM

        // loop to increment the time and push results in array
        for (let i = 0; tt < 24 * 60; i++) {
            const hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            const mm = (tt % 60); // getting minutes of the hour in 0-55 format
            if (is24hrs) {
                times[i] = ('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
            } else {
                times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }
        return times;
    }
    private loadDroddowns() {
        const source = forkJoin([
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Allegation.AllegationURL + '?filter={"nolimit":true,"order":"name asc"}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtActionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.ConditionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.CourtOrderTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.FindingTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.AdjudicatedDecisionTypeUrl + '?filter={"nolimit":true}'),
            this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Court.HearingStatusTypeUrl + '?filter={"nolimit":true}')
        ])
            .map(([offenceCategories, courtActionsType, conditionTypes, courtOrder, findingsType, adijuctedDecisions, hearingStatusType]) => {
                return {
                    offenceCategories: offenceCategories['data'].map(
                        (res) =>
                            new DropdownModel({
                                text: res.name,
                                value: res.allegationid
                            })
                    ),
                    courtActionsType: courtActionsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtactiontypekey
                            })
                    ),
                    conditionTypes: conditionTypes.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.conditiontypekey
                            })
                    ),
                    courtOrder: courtOrder.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.courtordertypekey
                            })
                    ),
                    findingsType: findingsType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.findingtypekey
                            })
                    ),
                    adijuctedDecisions: adijuctedDecisions.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.adjudicateddecisiontypekey
                            })
                    ),
                    hearingStatusType: hearingStatusType.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.hearingstatustypekey
                            })
                    ),
                };
            })
            .share();
        this.offenceCategories$ = source.pluck('offenceCategories');
        this.courtActionsType$ = source.pluck('courtActionsType');
        this.conditionTypes$ = source.pluck('conditionTypes');
        this.courtOrder$ = source.pluck('courtOrder');
        this.findingsType$ = source.pluck('findingsType');
        this.adijuctedDecisions$ = source.pluck('adijuctedDecisions');
        this.hearingStatusTypeItem$ = source.pluck('hearingStatusType');

        this.courtActionsType$.subscribe(data => {
            if (data) {
                const courtActionValue = this.courtAction.map(res => res.courtactiontypekey);
                const getActiontems = data.filter(item => {
                    if (courtActionValue.includes(item.value)) {
                       return item;
                    }
                });
                this.courtActionDescription = getActiontems.map(res => res.text);
            }
        });
    }

    private getPetitionDetails() {
        this._commonHttpService
            .getArrayList(
                {
                    where: {
                        intakeservicerequestid: this.id
                    },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Court
                    .PetitionDetailslist + '?filter'
            )
            .subscribe(res => {
                if (res[0]) {
                    this.loadDroddowns();
                 this.petitionDetails = res[0];
                } else {
                    this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/court/petition-detail']);
                }
            });
    }
}
