import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsMonthlyReportComponent } from './as-monthly-report.component';

const routes: Routes = [{
  path: '',
  component: AsMonthlyReportComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsMonthlyReportRoutingModule { }
