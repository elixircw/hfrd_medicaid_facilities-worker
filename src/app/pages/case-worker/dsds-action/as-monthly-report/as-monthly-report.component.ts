import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as jsPDF from 'jspdf';
import { CommonHttpService, AuthService, DataStoreService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { ActivatedRoute } from '@angular/router';
import { InvolvedPerson } from '../../_entities/caseworker.data.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MonthlyReport, MonthlyReportListDetails } from './entities';
import { AlertService } from '../../../../@core/services/alert.service';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { InvestigationSummary } from '../disposition/_entities/disposition.data.models';
import value from '*.json';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'as-monthly-report',
    templateUrl: './as-monthly-report.component.html',
    styleUrls: ['./as-monthly-report.component.scss']
})
export class AsMonthlyReportComponent implements OnInit, AfterViewInit {
    id: string;
    involvedPersons: InvolvedPerson;
    clientDetailsForm: FormGroup;
    calculationTableForm: FormGroup;
    aideTableForm: FormGroup;
    caseWorkerName: AppUser;
    monthlyReportSave: MonthlyReport;
    monthlyreportdays = ([] = []);
    reportListDetails$: Observable<MonthlyReportListDetails[]>;
    ihasprovidermonthlyreportid: string;
    investigationSummary: InvestigationSummary;
    providerNames = ([] = []);
    totalAmount = 0;
    showListInstrction = false;
    dsdsActionSummary: any;
    // tslint:disable-next-line:max-line-length
    constructor(private _commonService: CommonHttpService, private _dataStoreService: DataStoreService, private route: ActivatedRoute, private _formBuilder: FormBuilder, private _alertService: AlertService, private _authService: AuthService) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    }

    ngOnInit() {
        this.intializeForm();
        this.getPreFillData();
        this.getMonthlyReportDetails();
        this.calculationTableForm.disable();
        this.caseWorkerName = this._authService.getCurrentUser();

        this.aideTableForm.get('choreOnly').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'CHORES') {
                    this.calculationTableForm.patchValue({
                        choreOnly: sumNumber,
                        choreOnlyTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('personalCareOnly').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'PERSONAL') {
                    this.calculationTableForm.patchValue({
                        personalCareOnly: sumNumber,
                        personalCareOnlyTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('heavyChore').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'CHORES') {
                    this.calculationTableForm.patchValue({
                        heavyChore: sumNumber,
                        heavyChoreTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('trasportationGeneral').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'PERSONAL') {
                    this.calculationTableForm.patchValue({
                        trasportationGeneral: sumNumber,
                        trasportationGeneralTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('trasportationMedical').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'PERSONAL') {
                    this.calculationTableForm.patchValue({
                        trasportationMedical: sumNumber,
                        trasportationMedicalTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('therapeuticParentAide').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'NURSING CARE') {
                    this.calculationTableForm.patchValue({
                        therapeuticParentAide: sumNumber,
                        therapeuticParentAideTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('therapeuticAdultAide').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'NURSING CARE') {
                    this.calculationTableForm.patchValue({
                        therapeuticAdultAide: sumNumber,
                        therapeuticAdultAideTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('respiteCare').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            this.investigationSummary.l_serviceplanrate.map(list => {
                if (list.servicetsubtypename === 'RESPITE') {
                    this.calculationTableForm.patchValue({
                        respiteCare: sumNumber,
                        respiteCareTotal: sumNumber * list.rate
                    });
                }
            });
        });

        this.aideTableForm.get('otherClientRelatedServices').valueChanges.subscribe(data => {
            const result = Object.values(data);
            const sumNumber = result.reduce((acc, cur) => acc + Number(cur), 0);
            if (this.investigationSummary && this.investigationSummary.l_serviceplanrate) {
                this.investigationSummary.l_serviceplanrate.map(list => {
                    if (list.servicetsubtypename === 'PERSONAL') {
                        this.calculationTableForm.patchValue({
                            otherClientRelatedServices: sumNumber,
                            otherClientRelatedServicesTotal: sumNumber * list.rate
                        });
                    }
                });
            }
        });

        this.calculationTableForm.valueChanges.subscribe(data => {
            this.totalAmount = 0;
            this.totalAmount += Number(data.choreOnlyTotal);
            this.totalAmount += Number(data.personalCareOnlyTotal);
            this.totalAmount += Number(data.heavyChoreTotal);
            this.totalAmount += Number(data.trasportationGeneralTotal);
            this.totalAmount += Number(data.trasportationMedicalTotal);
            this.totalAmount += Number(data.therapeuticParentAideTotal);
            this.totalAmount += Number(data.therapeuticAdultAideTotal);
            this.totalAmount += Number(data.respiteCareTotal);
            this.totalAmount += Number(data.otherClientRelatedServicesTotal);
        });
    }

    ngAfterViewInit() {
        this._dataStoreService.currentStore.subscribe(storeData => {
            if (storeData['dsdsActionsSummary']) {
                this.dsdsActionSummary = storeData['dsdsActionsSummary'];
            }
        });
    }

    intializeForm() {
        this.clientDetailsForm = this._formBuilder.group({
            clientName: [''],
            clientID: [''],
            dob: [''],
            providername: [''],
            providerssn: [''],
            reportedmonthyear: [null],
            provideraddress: [''],
            providercity: [''],
            providerstate: [''],
            providerzipcode: [''],
            providerphoneno: [''],
            categoryihasfamily: [false],
            categoryihasadults: [false],
            categoryeligible: [false],
            categoryihasanothereservice: [false],
            providerihasfamily: [false],
            isapproxdod: [''],
            providerihasadults: [false],
            providereligible: [false],
            providerihasanothereservice: [false],
            pca: [''],
            agencyobject: [''],
            aidesignature: [''],
            caregiversignature: [''],
            caregiversigndate: [null],
            ldsssignature: [''],
            ldsssignaturedate: [null],
            aidesignaturedate: [null],
            accountclerksign: [null]
        });

        this.aideTableForm = this._formBuilder.group({
            aideTimeIn: this.intializeFormDay(),
            choreOnly: this.intializeFormDay(),
            personalCareOnly: this.intializeFormDay(),
            heavyChore: this.intializeFormDay(),
            trasportationGeneral: this.intializeFormDay(),
            trasportationMedical: this.intializeFormDay(),
            therapeuticParentAide: this.intializeFormDay(),
            therapeuticAdultAide: this.intializeFormDay(),
            respiteCare: this.intializeFormDay(),
            otherClientRelatedServices: this.intializeFormDay(),
            totalDirectServiceHours: this.intializeFormDay(),
            aideTravelHours: this.intializeFormDay(),
            aideHoursMiscellaneousAdmin: this.intializeFormDay(),
            indicateCodesforTasksPerformed: this.intializeFormDay(),
            aideTimeOut: this.intializeFormDay()
        });
        this.calculationTableForm = this._formBuilder.group({
            choreOnly: [],
            personalCareOnly: [],
            heavyChore: [],
            trasportationGeneral: [],
            trasportationMedical: [],
            therapeuticParentAide: [],
            therapeuticAdultAide: [],
            respiteCare: [],
            otherClientRelatedServices: [],
            totalDirectServiceHours: [],
            aideTravelHours: [],
            aideHoursMiscellaneousAdmin: [],
            indicateCodesforTasksPerformed: [],
            aideTimeOut: [],
            choreOnlyRate: [],
            personalCareOnlyRate: [],
            heavyChoreRate: [],
            trasportationGeneralRate: [],
            trasportationMedicalRate: [],
            therapeuticParentAideRate: [],
            therapeuticAdultAideRate: [],
            respiteCareRate: [],
            otherClientRelatedServicesRate: [],
            choreOnlyTotal: [],
            personalCareOnlyTotal: [],
            heavyChoreTotal: [],
            trasportationGeneralTotal: [],
            trasportationMedicalTotal: [],
            therapeuticParentAideTotal: [],
            therapeuticAdultAideTotal: [],
            respiteCareTotal: [],
            otherClientRelatedServicesTotal: []
        });
    }

    intializeFormDay() {
        return this._formBuilder.group({
            day01: [],
            day02: [],
            day03: [],
            day04: [],
            day05: [],
            day06: [],
            day07: [],
            day08: [],
            day09: [],
            day10: [],
            day11: [],
            day12: [],
            day13: [],
            day14: [],
            day15: [],
            day16: [],
            day17: [],
            day18: [],
            day19: [],
            day20: [],
            day21: [],
            day22: [],
            day23: [],
            day24: [],
            day25: [],
            day26: [],
            day27: [],
            day28: [],
            day29: [],
            day30: [],
            day31: []
        });
    }

    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-content').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .table-theme .table tr td {
            padding:8px;
        }
        .table-theme.checklist-table .table tr td {
            text-align: center !important;
            padding:0;
        }
        .table-theme.checklist-table .table tr td:first-child {
            width: 260px;
            text-align: left !important;
            padding:8px;
        }
        h5 {font-weight: 600;}
        .table-highlight {
            padding: 10px 0 10px 10px;
            background: #f9f9f9;
            margin-bottom: 0;
            font-size: 13px;
            font-weight: 600;
        }
        input[type="text"] {
            width: 35px;
            height: 30px;
            background: transparent;
            text-align: center;
            border:none;
            font-size: 12px;
        }
        .monthlyTable input[type="text"] {
            width: 75px;
            height: 19px;
            background: transparent;
            text-align: center;
            border:none;
            font-size: 12px;
        }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }

    private getMonthlyReportDetails() {
        this.reportListDetails$ = this._commonService
            .getArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    method: 'get'
                },
                'ihasprovidermonthlyreport/list?filter'
            )
            .map(res => {
                return res;
            });
    }

    showListInstruction(event) {
        if (event.checked) {
            this.showListInstrction = true;
        } else {
            this.showListInstrction = false;
        }
    }

    getPreFillData() {
        const sourceData = forkJoin(
            this._commonService.getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeservreqid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            ),
            this._commonService.getSingle(
                {
                    intakeserviceid: this.id,
                    method: 'post'
                },
                'Investigations/getinvestigationsummary'
            ),
            this._commonService.getPagedArrayList(
                new PaginationRequest({
                    limit: 20,
                    where: { objectid: this.id },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ActivityUrl + '?filter'
            )
        ).subscribe(list => {
            if (list) {
                if (list[0]['data']) {
                    const source = list[0]['data'].filter(data => data.rolename === 'RA');
                    this.involvedPersons = source[0];
                    this.clientDetailsForm.patchValue({
                        clientName: this.involvedPersons.lastname.toUpperCase() + ', ' + this.involvedPersons.firstname.toUpperCase(),
                        clientID: this.involvedPersons.cjamspid,
                        dob: this.involvedPersons.dob,
                        ssn: this.involvedPersons.ssn ? this.involvedPersons.ssn : '',
                        provideraddress: this.involvedPersons.address ? this.involvedPersons.address : '',
                        providercity: this.involvedPersons.city ? this.involvedPersons.city : '',
                        providerstate: this.involvedPersons.state ? this.involvedPersons.state : '',
                        zipcode: this.involvedPersons.zipcode ? this.involvedPersons.zipcode : '',
                        providerphoneno: this.involvedPersons.phonenumber ? this.involvedPersons.phonenumber : ''
                    });
                }
                this.investigationSummary = list[1][0];
                if (this.investigationSummary && this.investigationSummary.l_serviceplanrate) {
                    this.investigationSummary.l_serviceplanrate.map(data => {
                        if (data.servicetsubtypename === 'PERSONAL') {
                            this.calculationTableForm.patchValue({
                                personalCareOnlyRate: data.rate ? data.rate : 0,
                                trasportationGeneralRate: data.rate ? data.rate : 0,
                                trasportationMedicalRate: data.rate ? data.rate : 0,
                                otherClientRelatedServicesRate: data.rate ? data.rate : 0
                            });
                        }
                        if (data.servicetsubtypename === 'CHORES') {
                            this.calculationTableForm.patchValue({
                                choreOnlyRate: data.rate ? data.rate : 0,
                                heavyChoreRate: data.rate ? data.rate : 0
                            });
                        }
                        if (data.servicetsubtypename === 'NURSING CARE') {
                            this.calculationTableForm.patchValue({
                                therapeuticParentAideRate: data.rate ? data.rate : 0,
                                therapeuticAdultAideRate: data.rate ? data.rate : 0
                            });
                        }
                        if (data.servicetsubtypename === 'RESPITE') {
                            this.calculationTableForm.patchValue({
                                respiteCareRate: data.rate ? data.rate : 0
                            });
                        }
                    });
                }
                if (list.length && list[2]) {
                    if (list[2]['data'] && list[2]['data'].length) {
                        list[2]['data'].map(item => {
                            if (item.services && item.services.length) {
                                return item.services.map(names => {
                                    this.providerNames.push(names);
                                });
                            }
                        });
                        const providerName = this.providerNames.map(item => {
                            return item.vendorname;
                        });
                        this.clientDetailsForm.patchValue({ providername: providerName.join() });
                    }
                }
            }
        });
    }

    saveMonthlyReport(status) {
        const aideTimeIn = this.aideTableForm.get('aideTimeIn').value;
        aideTimeIn.activitykey = 'AIDE TIME IN';

        const choreOnly = this.aideTableForm.get('choreOnly').value;
        const choreOnlyTotal = this.calculationTableForm.get('choreOnlyTotal').value;
        choreOnly.activitykey = 'Chore Only';
        choreOnly.total = choreOnlyTotal;

        const personalCareOnly = this.aideTableForm.get('personalCareOnly').value;
        const personalCareOnlyTotal = this.calculationTableForm.get('personalCareOnlyTotal').value;
        personalCareOnly.total = personalCareOnlyTotal;
        personalCareOnly.activitykey = 'Personal Care Only';

        const heavyChore = this.aideTableForm.get('heavyChore').value;
        const heavyChoreTotal = this.calculationTableForm.get('heavyChoreTotal').value;
        heavyChore.total = heavyChoreTotal;
        heavyChore.activitykey = 'Heavy Chore';

        const trasportationGeneral = this.aideTableForm.get('trasportationGeneral').value;
        const trasportationGeneralTotal = this.calculationTableForm.get('trasportationGeneralTotal').value;
        trasportationGeneral.total = trasportationGeneralTotal;
        trasportationGeneral.activitykey = 'Trasportation/Escort General';

        const trasportationMedical = this.aideTableForm.get('trasportationMedical').value;
        const trasportationMedicalTotal = this.calculationTableForm.get('trasportationMedicalTotal').value;
        trasportationMedical.total = trasportationMedicalTotal;
        trasportationMedical.activitykey = 'Trasportation/Escort Medical';

        const therapeuticParentAide = this.aideTableForm.get('therapeuticParentAide').value;
        const therapeuticParentAideTotal = this.calculationTableForm.get('therapeuticParentAideTotal').value;
        therapeuticParentAide.total = therapeuticParentAideTotal;
        therapeuticParentAide.activitykey = 'Therapeutic Parent Aide';

        const therapeuticAdultAide = this.aideTableForm.get('therapeuticAdultAide').value;
        const therapeuticAdultAideTotal = this.calculationTableForm.get('therapeuticAdultAideTotal').value;
        therapeuticAdultAide.total = therapeuticAdultAideTotal;
        therapeuticAdultAide.activitykey = 'Therapeutic Adult Aide';

        const respiteCare = this.aideTableForm.get('respiteCare').value;
        const respiteCareTotal = this.calculationTableForm.get('respiteCareTotal').value;
        respiteCare.total = respiteCareTotal;
        respiteCare.activitykey = 'Respite Care';

        const otherClientRelatedServices = this.aideTableForm.get('otherClientRelatedServices').value;
        const otherClientRelatedServicesTotal = this.calculationTableForm.get('otherClientRelatedServicesTotal').value;
        otherClientRelatedServices.total = otherClientRelatedServicesTotal;
        otherClientRelatedServices.activitykey = 'Other Client Related Services';

        const totalDirectServiceHours = this.aideTableForm.get('totalDirectServiceHours').value;
        totalDirectServiceHours.activitykey = 'Total Direct Service Hours';

        const aideTravelHours = this.aideTableForm.get('aideTravelHours').value;
        aideTravelHours.activitykey = 'Aide Travel Hours';

        const aideHoursMiscellaneousAdmin = this.aideTableForm.get('aideHoursMiscellaneousAdmin').value;
        aideHoursMiscellaneousAdmin.activitykey = 'Aide Hours Miscellaneous Admin';

        const indicateCodesforTasksPerformed = this.aideTableForm.get('indicateCodesforTasksPerformed').value;
        indicateCodesforTasksPerformed.activitykey = 'Indicate Codes(s) for Tasks Performed';

        const aideTimeOut = this.aideTableForm.get('aideTimeOut').value;
        aideTimeOut.activitykey = 'AIDE TIME OUT';
        this.monthlyreportdays.push(
            choreOnly,
            personalCareOnly,
            heavyChore,
            trasportationGeneral,
            trasportationMedical,
            therapeuticParentAide,
            therapeuticAdultAide,
            respiteCare,
            otherClientRelatedServices,
            totalDirectServiceHours,
            aideTravelHours,
            aideHoursMiscellaneousAdmin,
            indicateCodesforTasksPerformed,
            aideTimeOut,
            aideTimeIn
        );
        this.monthlyReportSave = Object.assign(this.clientDetailsForm.value);
        this.monthlyReportSave.monthlyreportdays = this.monthlyreportdays;
        this.monthlyReportSave.intakeserviceid = this.id;
        this.monthlyReportSave.totalinvoiceamount = this.totalAmount;
        this.monthlyReportSave.reportstatus = status;
        this.monthlyReportSave.ihasprovidermonthlyreportid = this.ihasprovidermonthlyreportid ? this.ihasprovidermonthlyreportid : null;
        this._commonService.create(this.monthlyReportSave, 'ihasprovidermonthlyreport/addupdate').subscribe(data => {
            if (data) {
                this.getMonthlyReportDetails();
                this._alertService.success('Monthly Report Saved Successfully');
            } else {
                this._alertService.error('Unable to Save Monthly Report');
            }
        });
        console.log('Final Save:', this.monthlyReportSave);
    }

    editReport(report, action) {
        this.ihasprovidermonthlyreportid = report.ihasprovidermonthlyreportid;
        this.clientDetailsForm.patchValue(report);
        report.monthlyreportdays.map(data => {
            if (data.activitykey === 'Chore Only') {
                this.aideTableForm.get('choreOnly').patchValue(data);
            }
            if (data.activitykey === 'AIDE TIME IN') {
                this.aideTableForm.get('aideTimeIn').patchValue(data);
            }
            if (data.activitykey === 'Personal Care Only') {
                this.aideTableForm.get('personalCareOnly').patchValue(data);
            }
            if (data.activitykey === 'Heavy Chore') {
                this.aideTableForm.get('heavyChore').patchValue(data);
            }
            if (data.activitykey === 'Trasportation/Escort General') {
                this.aideTableForm.get('trasportationGeneral').patchValue(data);
            }
            if (data.activitykey === 'Trasportation/Escort Medical') {
                this.aideTableForm.get('trasportationMedical').patchValue(data);
            }
            if (data.activitykey === 'Therapeutic Parent Aide') {
                this.aideTableForm.get('therapeuticParentAide').patchValue(data);
            }
            if (data.activitykey === 'Therapeutic Adult Aide') {
                this.aideTableForm.get('therapeuticAdultAide').patchValue(data);
            }
            if (data.activitykey === 'Respite Care') {
                this.aideTableForm.get('respiteCare').patchValue(data);
            }
            if (data.activitykey === 'Other Client Related Services') {
                this.aideTableForm.get('otherClientRelatedServices').patchValue(data);
            }
            if (data.activitykey === 'Total Direct Service Hours') {
                this.aideTableForm.get('totalDirectServiceHours').patchValue(data);
            }
            if (data.activitykey === 'Aide Travel Hours') {
                this.aideTableForm.get('aideTravelHours').patchValue(data);
            }
            if (data.activitykey === 'Aide Hours Miscellaneous Admin') {
                this.aideTableForm.get('aideHoursMiscellaneousAdmin').patchValue(data);
            }
            if (data.activitykey === 'Indicate Codes(s) for Tasks Performed') {
                this.aideTableForm.get('indicateCodesforTasksPerformed').patchValue(data);
            }
            if (data.activitykey === 'AIDE TIME OUT') {
                this.aideTableForm.get('aideTimeOut').patchValue(data);
            }
        });

        if (action === 'view') {
            this.clientDetailsForm.disable();
            this.calculationTableForm.disable();
            this.aideTableForm.disable();
        } else {
            this.clientDetailsForm.enable();
            this.calculationTableForm.enable();
            this.aideTableForm.enable();
        }
    }

    printReport() {
        this._commonService.getSingle({
            count: -1,
            where: {
                documenttemplatekey: ['Ihasmonthlyreport'],
                intakeserviceid: this.id,
                intakenumber: this.dsdsActionSummary.intakenumber
            },
            method: 'post'
        }, 'evaluationdocument/generateintakedocument').subscribe((data) => {
            if (data) {
                window.open(data.data[0].documentpath, '_blank');
            }
        });
    }
}
