import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsMonthlyReportComponent } from './as-monthly-report.component';

describe('AsMonthlyReportComponent', () => {
  let component: AsMonthlyReportComponent;
  let fixture: ComponentFixture<AsMonthlyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsMonthlyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsMonthlyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
