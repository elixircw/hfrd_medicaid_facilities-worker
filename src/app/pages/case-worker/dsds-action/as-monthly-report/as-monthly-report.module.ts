import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
} from '@angular/material';

import { AsMonthlyReportRoutingModule } from './as-monthly-report-routing.module';
import { AsMonthlyReportComponent } from './as-monthly-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    AsMonthlyReportRoutingModule,
    FormsModule, ReactiveFormsModule,
    SharedDirectivesModule
  ],
  declarations: [AsMonthlyReportComponent]
})
export class AsMonthlyReportModule { }
