import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InHomeServiceRoutingModule } from './in-home-service-routing.module';
import { InHomeServiceComponent } from './in-home-service.component';
import { ServiceAgreementComponent } from './service-agreement/service-agreement.component';
import { ProgressReviewComponent } from './progress-review/progress-review.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgxMaskModule } from 'ngx-mask';
import { QuillModule } from 'ngx-quill';
import { PersonService } from './person.service';
import { ChildRemovalResolverService } from '../child-removal/child-removal-resolver.service';
import { ChildRemovalMapperService } from '../child-removal/child-removal-mapper.service';
import { PersonDisabilityService } from '../../../shared-pages/person-disability/person-disability.service';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    InHomeServiceRoutingModule,
    FormMaterialModule,
    A2Edatetimepicker,
    NgxMaskModule.forRoot(),
    QuillModule,
    ControlMessagesModule, NgSelectModule
  ],
  declarations: [InHomeServiceComponent, ServiceAgreementComponent, ProgressReviewComponent],
  providers: [PersonService]

})
export class InHomeServiceModule { }
