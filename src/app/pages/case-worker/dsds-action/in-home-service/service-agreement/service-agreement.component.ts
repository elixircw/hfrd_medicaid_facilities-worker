import { Component, OnInit } from '@angular/core';
import { PersonService } from '../person.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStoreService, CommonHttpService, AlertService, SessionStorageService, AuthService, CommonDropdownsService } from '../../../../../@core/services';
import { RouteToSupervisor, Agreement, GapAgreement } from '../../placement/_entities/placement.model';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DynamicObject, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';

@Component({
  selector: 'service-agreement',
  templateUrl: './service-agreement.component.html',
  styleUrls: ['./service-agreement.component.scss']
})
export class ServiceAgreementComponent implements OnInit {
  personList = [];
  personidlist = [];
  showSignedPanel = false;
  serviceAgreementFormGroup: FormGroup;
  maxDate = new Date();
  agreementDate = new Date();
  householdName: string;
  //@TM: Agreement related properties
  approvalStatusForm: FormGroup;
  submitStatus: RouteToSupervisor;
  agreementList: any = [];
  agreementid: any;
  id: string;
  daNumber: string;
  roleId: AppUser;
  isSupervisor = false;
  isApproved = false;
  agreement: any;
  isEnableComments = false;
  gapAlertMessage: string;
  isShowAgreementForm = false;
  newBtnDisabled = false;
  agreementDetail: any;
  store: DynamicObject;
  placementAgreementRateId: any;
  checkValidation: boolean;
  approveAgreementResponse: any;
  approvalStatus: boolean;
  supervisorsList: any[] = [];
  workerList: any[] = [];
  collaterals: any;
  isClosed = false;
  agreementMinDate: Date;
  isEditable = false;
  constructor(
    private _personService: PersonService,
    private formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService,
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _authService: AuthService,
    private storage: SessionStorageService,
    private _route: Router,
    private _intakeUitls: IntakeUtils,
    private _commondDDService: CommonDropdownsService

  ) {
    this.store = this._dataStoreService.getCurrentStore();
  }

  ngOnInit() {
    const actionSummary = this._dataStoreService.getData('dsdsActionsSummary');
    if (actionSummary) {
      this.agreementMinDate = new Date(actionSummary.da_receiveddate);
    }
    this.maxDate.setHours(0, 0, 0, 0);
    this.agreementMinDate.setHours(0, 0, 0, 0);
    this.buildFormGroup();
    this._personService.getPersonsList().subscribe((item) => {
      this.personList = item.data;

      const hoh = this.personList.find(person => person.isheadofhousehold);
      if (hoh) {
        this.householdName = hoh.fullname;
      }
      this.getAgreement();
    });


    //@TM: Get Case ID, Check for Supervisor role
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.roleId = this._authService.getCurrentUser();

    if (this.roleId.role.name === 'apcs') {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.loadSupervisor();
    this.getcollateral();
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isEditable = false;
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

  buildFormGroup() {
    this.serviceAgreementFormGroup = this.formBuilder.group({
      agreementid: [null],
      persons: [null],
      attentiontx: ['', Validators.required],
      agreementdate: [null, Validators.required],
      signeddate: [null],
      signatureobtflag: [false],
      supervisorid: [null, Validators.required],
      staffid: [null],
      collateralid: [null],
      associateid: [null],
      approvalstatustypekey: [null]
    });

    //@TM: Approval Status FormGroup
    this.approvalStatusForm = this._formBuilder.group({
      routingstatus: [''],
      comments: ['']
    });
  }

  onPersonChecked(event, person) {
    console.log(event, person);
    person.isSingned = event.checked;
    if (this.checkProperty() >= 1) {
      this.showSignedPanel = true;
    } else {
      this.showSignedPanel = false;
    }
  }

  checkProperty() {
    let count = 0;
    this.personList.forEach(o => {
      if (o.isSingned) {
        count++;
      }
    });


    return count;
  }

  // @TM: Methods for Service Agreement -----

  private getAgreement() {
    this._commonHttpService
      .getArrayList(
        {
          method: 'get',
          page: 1,
          limit: 10,
          where: { caseid: this.id }
        },
        'serviceagreement/list?filter'
      )
      .subscribe(res => {
        if (res && (res instanceof Array)) {
          this.agreementList = res.map(item => {
            item.agreementdate = this._commondDDService.getValidDate(item.agreementdate);
            return item;
          });
        }
      });
  }

  patchAgreement(modal) {
    this.agreementList = modal;
    // this.newBtnDisabled = true;
    this.serviceAgreementFormGroup.patchValue(this.agreementList);
    this.approvalStatusForm.patchValue({
      routingstatus: this.agreementList.routingstatus ? this.agreementList.routingstatus : '',
      comments: this.agreementList.comments ? this.agreementList.comments : ''
    });

    if (this.approvalStatusForm.value.routingstatus === 'Approved' || this.approvalStatusForm.value.routingstatus === 'Rejected') {
      this.isApproved = true;
      this.rejectComments(this.approvalStatusForm.value.routingstatus);
    }
    this.agreementList = Object.assign({});
  }

  conditionValidation(): boolean {
    if (this.serviceAgreementFormGroup.value.approvaldate !== null && this.serviceAgreementFormGroup.value.agreementdate !== null &&
      this.serviceAgreementFormGroup.value.approvaldate < this.serviceAgreementFormGroup.value.agreementdate) {
      this._alertService.error('Agreement Signed Date should be greater than Date of Agreement');
      return false;
    }
    return true;
  }

  rejectComments(status) {
    if (status === 'Rejected') {
      this.isEnableComments = true;
    } else {
      this.isEnableComments = false;
      this.approvalStatusForm.patchValue({ comments: '' });
      this.serviceAgreementFormGroup.disable();
    }
  }

  addAgreement(agreement, status) {
    this.personidlist = [];
    // if (this.serviceAgreementFormGroup.valid) {
    const validation = this.conditionValidation();
    let personObj, activeflag;
    for (let person of this.personList) {

      if (person.isSingned) {
        activeflag = 1;
      } else {
        activeflag = 0;
      }

      personObj = {
        "personid": person.personid,
        "activeflag": activeflag
      }
      this.personidlist.push(personObj);
    }
    this.agreement = Object.assign(
      {
        caseid: this.id,
        personidlist: this.personidlist,
        approvalstatustypekey: status ? status : 'pending',
      },
      agreement
    );

    if (validation) {
      this._commonHttpService.create(this.agreement, 'serviceagreement/add').subscribe(
        res => {
          if (status) {
            this._alertService.success('Operation completed Successfully!');
          } else {
            this._alertService.success('Saved Successfully!');
          }
          this.getAgreement();
          this.serviceAgreementFormGroup.disable();
          this.newBtnDisabled = true;
          // this.submitIntake(agreement, 'IHSA');
        },
        err => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        }
      );
    }
    // }
  }


  showApproveAgreementAckmnt(approveResponse: any) {
    this.approveAgreementResponse = approveResponse;
    this._alertService.success('Completed successfully.');
    const url = `/pages/case-worker/dsds-action/in-home-service`;
    this._route.navigate([url]);
    (<any>$('#approve-agreement-ackmt')).modal('show');
  }

  openAgreement(mode: string) {
    if (mode === 'ADD') {
      this.serviceAgreementFormGroup.reset();
      this.serviceAgreementFormGroup.enable();
      this.isEditable = this.isClosed ? false : true;
    }
    (<any>$('#agreement-form')).modal('show');
    this.loadSupervisor();
  }

  loadSupervisor() {
    this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          where: { appevent: 'CWIF' },
          method: 'post'
        }),
        'Intakedastagings/getroutingusers'
      )
      .subscribe(result => {
        this.supervisorsList = result.data;
        this.supervisorsList = this.supervisorsList.filter(
          users => users.rolecode === 'SP'
        );
        this.workerList = result.data.filter(
          users => users.rolecode !== 'SP'
        );
      });
  }

  getCaseUuid() {
    const caseID = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    if (caseID) {
      return caseID;
    }
    const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
    let caseUUID = null;
    if (caseInfo) {
      caseUUID = caseInfo.intakeserviceid;
    }
    return caseUUID;
  }

  getcollateral() {
    const data = {
      caseid: this.getCaseUuid(),
      objecttype: 'servicecase',
      intakenumber: null,
    };
    const request = {
      objectid: data.caseid,
      objecttype: 'case'
    };
    this._commonHttpService.getArrayList(
      {
        where: request,
        method: 'get',
        nolimit: true
      },
      'collateral/list?filter'
    ).subscribe(data => {
      if (data && data.length && data[0].getcollateraldetails && data[0].getcollateraldetails.length) {
        this.collaterals = data[0].getcollateraldetails;
      } else {
        this.collaterals = [];
      }
    });
  }

  closeAgreement() {
    this.serviceAgreementFormGroup.reset();
    (<any>$('#agreement-form')).modal('hide');
  }

  saveAgreement(status: string) {
    const agreementData = this.serviceAgreementFormGroup.getRawValue();
    if (this.serviceAgreementFormGroup.invalid) {
      console.log(this._intakeUitls.findInvalidControls(this.serviceAgreementFormGroup));
      this._alertService.error('Please fill required fields');
      return;
    }
    if (agreementData.signatureobtflag && !agreementData.signeddate) {
      this._alertService.error('Please fill required fields');
      return;
    }

    if (!agreementData.persons && !agreementData.collateralid) {
      this._alertService.error('Please select Family Member/Collateral');
      return;
    }
    agreementData.persons = [agreementData.persons];
    agreementData.signatureobtflag = agreementData.signatureobtflag ? 1 : 0;
    agreementData.caseid = this.getCaseUuid();
    agreementData.approvalstatustypekey = status;
    if (status === 'approved') {
      agreementData.approvaldate = new Date();
    }
    this._commonHttpService.create(agreementData, 'serviceagreement/add').subscribe(
      _ => {
        this._alertService.success('Saved Successfully!');
        this.closeAgreement();
        this.getAgreement();
      },
      err => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  agreementDateChanged() {
    this.agreementDate = new Date(this.serviceAgreementFormGroup.getRawValue().agreementdate);
    this.serviceAgreementFormGroup.patchValue({signeddate : null});
  }

  viewAgreement(data) {
    this.openAgreement('VIEW');
    this.patchServiceAgreement(data);
    this.isEditable = false;
    this.serviceAgreementFormGroup.disable();
  }

  reviewAgreement(data) {
    this.openAgreement('VIEW');
    this.isEditable = this.isClosed ? false : true;
    this.patchServiceAgreement(data);
  }

  patchServiceAgreement(data) {
    this.agreementDate = new Date(data.agreementdate);
    if (Array.isArray(data.serviceagreementlist) && data.serviceagreementlist.length) {
      const participant = data.serviceagreementlist[0];
      data.persons = participant.personid;
      data.signatureobtflag = data.signatureobtflag ? true : false;
      data.signeddate = this._commondDDService.getValidDate(participant.signeddate);
      data.collateralid = participant.collateralid;
    }
    this.serviceAgreementFormGroup.patchValue(data);
  }

  downloadAgreement(data) {

    const modal = {
      count: -1,
      where: {
        documenttemplatekey: ['agreement'],
        agreementid: data.agreementid,
      },
      method: 'post'
    };
    console.log('download', modal);
    /* this._commonHttpService.getSingle(modal, 'evaluationdocument/generateintakedocument').subscribe((data) => {
      if (data && Array.isArray(data.data) && data.data.length  && data.data[0] ) {

        window.open(data.data[0].documentpath, '_blank');
      }
    }); */
    this._commonHttpService.download('evaluationdocument/generateintakedocument', modal)
        .subscribe(res => {
          const blob = new Blob([new Uint8Array(res)]);
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          link.download = 'Serivce Agreement.pdf';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        });

  }


}
