import { Injectable } from '@angular/core';
import { CommonHttpService, DataStoreService, AuthService } from '../../../../@core/services';
import { DropdownModel, DynamicObject, PaginationInfo,PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';
import { PersonDisabilityService } from '../../../shared-pages/person-disability/person-disability.service';
const UNSAFE = 0;

@Injectable()
export class PersonService {

  intakeserviceid: string;
  daNumber: string;
  personList = [];
  teamTypeKey: string;
  involvedPersons: any;
  results: Object;
  constructor(private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService    ) {
    this.teamTypeKey = this._authService.getAgencyName();
  }

  getPersonsList() {
    this.intakeserviceid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: { 
            objectid: this.intakeserviceid,
            objecttypekey: 'servicecase'
          }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
      );
  }


  loadProgressReviewDropdowns() {
      return forkJoin([
          this._commonHttpService.getArrayList(
              {
                nolimit: true,
                where: { referencetypeid: 113 }, order: 'displayorder ASC', method: 'get'
              },
              'referencevalues?filter'
          ),
          this._commonHttpService.getArrayList(
              {
                nolimit: true,
                where: { referencetypeid: 114 }, order: 'displayorder ASC', method: 'get'
              },
              'referencevalues?filter'
          ),
          this._commonHttpService.getArrayList(
              {
                nolimit: true,
                where: { referencetypeid: 115 }, order: 'displayorder ASC', method: 'get'
              },
              'referencevalues?filter'
          ),
          this._commonHttpService.getArrayList(
              {
                nolimit: true,
                where: { referencetypeid: 116 }, order: 'displayorder ASC', method: 'get'
              },
              'referencevalues?filter'
          )
      ]);
  }


  getCaseEvaluationDetails(id) {
    return this._commonHttpService
      .getArrayList(
        {
          method: 'get',
          page: 1,
          limit: 10,
          where: { caseid: id }
        },
        'caseevaluation/list?filter'
      );
  }


  getServicePlanList(id) {
    return this._commonService.getArrayList({
      method: 'get',
      nolimit: true,
      where: { objectid: id }
    }, 'serviceplan/goal?filter');
  }


  getAgencyServiceList(id) {
    return this._commonHttpService.getArrayList(
      {
        where: { daNumber: id },
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.agencyServiceLog + '?filter');
  }


    
  getAssessments(id) {
    return this._commonService.getArrayList({
            page: 1,
            limit: 25,
            where: {
              servicerequestid:id,
              assessmentstatus:null
            },
            method: 'get'
    }, 'admin/assessment/list?filter');
  }

}
