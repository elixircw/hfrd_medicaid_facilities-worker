import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AlertService, CommonHttpService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { AddActivity, ServicePlanActivity, ServicePlanActivityRes } from '../_entities/service-plan.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'service-plan-activity',
    templateUrl: './service-plan-activity.component.html',
    styleUrls: ['./service-plan-activity.component.scss']
})
export class ServicePlanActivityComponent implements OnInit {
    servicePlanActivity$: Observable<ServicePlanActivity[]>;
    servicePlanActivityCount$: Observable<ServicePlanActivityRes>;
    totalResulRecords$: Observable<number>;
    servicePlanId$ = new Subject<string>();
    @Input()
    servicePlanActivityiId: string;
    @Input()
    activityPlanActivityiId: string;
    id: string;
    // @Input()
    // activityPlanActivityi: string;
    @Input()
    editServicePlanOutputSubject$ = new Subject<AddActivity>();

    constructor(private _httpService: CommonHttpService, private _formBuilder: FormBuilder, private _alertService: AlertService,
         private route: ActivatedRoute, private _dataStoreService: DataStoreService) {}

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.getServicePlanActivity(1);
        this.viewServiceLog(null);
    }
    servicePlane() {
        this.getServicePlanActivity(1);
    }
    updateServicePlanActivityiId(id) {
        this.servicePlanActivityiId = id;
    }
    getServicePlanActivity(pageNo: number) {
        const source = this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    limit: 10,
                    page: pageNo,
                    where: { objectid: this.id },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ActivityUrl + '?filter'
            )
            .map(res => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.servicePlanActivity$ = source.pluck('data');
        this.servicePlanActivityCount$ = source.pluck('count');
    }

    editServiceEmpty() {
        this.editServicePlanOutputSubject$.next();
    }

    editServicePlan(ativity: AddActivity) {
        this.activityPlanActivityiId = ativity.serviceplanactivityid;
        ativity.isAddEdit = 'Edit';
        this.editServicePlanOutputSubject$.next(ativity);
    }

    deleteServicePlan(activity) {
        this._httpService.patch(activity.serviceplanactivityid, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServicePlanUrl).subscribe(
            res => {
                this._alertService.success('Successfully deleted Activity');
                this.getServicePlanActivity(1);
            },
            err => {}
        );
    }

    viewServiceLog(serviceplanid) {
        this.servicePlanId$.next(serviceplanid);
    }
}
