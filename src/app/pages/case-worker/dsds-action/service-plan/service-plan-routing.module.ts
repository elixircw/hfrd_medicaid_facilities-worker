import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicePlanComponent } from './service-plan.component';
import { GoalStrategyComponent } from './goal-strategy/goal-strategy.component';
import { ServicePlanActivityComponent } from './service-plan-activity/service-plan-activity.component';
import { ScServicePlanModule } from './sc-service-plan/sc-service-plan.module';

const routes: Routes = [
    {
        path: '',
        component: ServicePlanComponent,
        children: [
            {
                path: 'goal-strategy',
                component: GoalStrategyComponent
            },
            {
                path: 'service-plan-activity',
                component: ServicePlanActivityComponent
            },
            { path: 'service-log-activity', loadChildren: './service-log-activity/service-log-activity.module#ServiceLogActivityModule' },
            //  Below modules are not used, removed as part of performance improvement activity
            // { path: 'sc-sp', loadChildren: './sc-service-plan/sc-service-plan.module#ScServicePlanModule' },
            // { path: 'sc-gc', loadChildren: './service-plan-gc/service-plan-gc.module#ServicePlanGcModule' },
            { path: 'sc-gc', loadChildren: '././service-plan-core/service-plan-core.module#ServicePlanCoreModule' },
            { path: 'service-case-management', loadChildren: '../../../shared-pages/service-case-management/service-case-management.module#ServiceCaseManagementModule' },
            { path: 'youth-transition-plan', loadChildren: './youth-transition-plan/youth-transition-plan.module#YouthTransitionPlanModule'}
        ],
    },
    { path: 'sc-sp', loadChildren: './sc-service-plan/sc-service-plan.module#ScServicePlanModule' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ServicePlanRoutingModule {}
