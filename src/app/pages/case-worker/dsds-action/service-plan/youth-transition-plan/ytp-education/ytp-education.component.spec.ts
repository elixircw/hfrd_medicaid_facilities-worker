/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { YtpEducationComponent } from './ytp-education.component';

describe('YtpEducationComponent', () => {
  let component: YtpEducationComponent;
  let fixture: ComponentFixture<YtpEducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtpEducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtpEducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
