import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChildRemovalService } from '../../../child-removal/child-removal.service';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../../../@core/services';
import { YouthTransitionPlanService } from '../youth-transition-plan.service';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import * as moment from 'moment';

@Component({
  selector: 'ytp-summary',
  templateUrl: './ytp-summary.component.html',
  styleUrls: ['./ytp-summary.component.scss']
})
export class YtpSummaryComponent implements OnInit {
  summaryFormGroup: FormGroup;
  persons = [];
  ytpData: any;
  ytpSummary: any = {};
  participants = [];
  assessments = [];
  participantTypes = [];
  assessment = { name: '', completionDate: null };

  constructor(private formBuilder: FormBuilder,
    private childRemovalService: ChildRemovalService,
    private _alertservice: AlertService,
    private _ytpService: YouthTransitionPlanService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.summaryFormGroup = this.formBuilder.group({
      name: [null],
      dob: [null],
      dateenteredfostercare: [null],
      caseno: [null],
      permanencyplangoal: [null],
      caseworkername: [null],
      completiondate: [null],
      transplancompleted: [null],
      planfollowupdate: [null],
      participantsinvolved: [null],
      participants: [],
      assessments: []
    });
    this.ytpData = this._dataStoreService.getData('YTPDATA');
    this.ytpSummary = this.ytpData.summary_json;
    this.getSummaryDetails();
    this.childRemovalService.getPersonsList().subscribe(persons => {
      if (persons && persons.data) {
        this.persons = persons.data;
      }
    }
    );
    this.loadParticipantTypes();
  }
  getFullName(person) {
      const nameKeys = [ 'prefx' , 'firstname' , 'middlename' , 'lastname' , 'suffix'];
      let name = '';
      nameKeys.forEach(key => {
      //console.log( key , person[key] );
      if(person && person.hasOwnProperty(key)){
        if ( !(person[key] == null) && !(person[key] == 'null') && !(person[key] == '') ) {
          name = name + person[key] + ' ';
          console.log(name);
      }}
      });
      //console.log("NAME",name);
      return name;
  }
  formatDateToString(date: Date): string {
    let convertedDate;
    convertedDate = moment(date);
    if (convertedDate.isValid()) {
      return convertedDate.format('MM/DD/YYYY');
    } else {
      return null;
    }
  }
  
  saveSummary() {
    const summary = this.ytpSummary;
    summary.assessments = this.assessments;
    summary.participants = this.participants;
    this._ytpService.patchData('summary_json', summary)
    .subscribe(
      response => {
        this._alertservice.success('Summary entered successfully!');
      },
      error => {
        this._alertservice.error('Error in entering Summary details!');
      }
    );
  }

  getSummaryDetails() {
    this.ytpData = this._dataStoreService.getData('YTPDATA');
    if(this.ytpData && this.ytpData.summary_json){
      // this.assessments = this.ytpData.summary_json.assessments ? this.ytpData.summary_json.assessments : [];
      this.participants = this.ytpData.summary_json.participants ? this.ytpData.summary_json.participants : [];
      
      this.assessments = [];    //@TM: Convert date to enable patching and display on UI
      if(this.ytpData.summary_json.assessments && this.ytpData.summary_json.assessments.length > 0) {
        for(let assessment of this.ytpData.summary_json.assessments) {
          assessment.completionDate = new Date(assessment.completionDate);
          this.assessments.push(assessment);
        }
      }
    }
  }

  clearSummary() {
    this.summaryFormGroup.reset();
  }

  loadParticipantTypes() {
    this._commonHttpService.getArrayList(
      {
        method: 'get'
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.Fim.ParticipantTypesUrl + '?filter'
    ).subscribe(response => {
      if (response && Array.isArray(response)) {
        this.participantTypes = response;
      }
    });
  }

  onInvolvedPersonChanged() {
    console.log(this.summaryFormGroup.getRawValue().participants);
  }

  changeParticipants(event) {
    console.log('selected', event.source.value);
    const person = event.source.value
    const newParticipant = {
      type: 'Involved person',
      firstname: person.firstname,
      lastname: person.lastname,
      relationship: person.relationship
    };
    this.participants.push(newParticipant);
  }

  deleteParticipant(index) {
    this.participants.splice(index, 1);
  }

  addOtherParticipant() {
    const newParticipant = {
      type: '',
      firstname: '',
      lastname: '',
      relationship: ''
    };
    this.participants.push(newParticipant);
  }

  addAssessment() {
    // this.assessments.push(JSON.parse(JSON.stringify(this.assessment)));
    this.assessments.push({ name: '', completionDate: null });
  }

  deleteAssessment(index) {
    this.assessments.splice(index, 1);
  }

}
