/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { YtpHealthComponent } from './ytp-health.component';

describe('YtpHealthComponent', () => {
  let component: YtpHealthComponent;
  let fixture: ComponentFixture<YtpHealthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtpHealthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtpHealthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
