export const YTP_TABS = [{
    tabName: 'summary',
    title: 'Summary',
    path: 'ytp-summary',
    keyName: 'summary_json',
    isCompleted: true,
},
{
    tabName: 'youth-thoughts-and-Ideas',
    title: 'Youth\'s Thoughts',
    path: 'ytp-thoughts-and-ideas',
    keyName: 'youththoughts_json',
    isCompleted: false
},
{
    tabName: 'education',
    title: 'Education',
    path: 'ytp-education',
    keyName: 'education_json',
    isCompleted: false
},
{
    tabName: 'employment',
    title: 'Employment',
    path: 'ytp-employment',
    keyName: 'employment_json',
    isCompleted: false
},
{
    tabName: 'money-management',
    title: 'Money Management',
    path: 'ytp-money-management',
    keyName: 'moneymanagement_json',
    isCompleted: false
},
{
    tabName: 'housing',
    title: 'Housing',
    path: 'ytp-housing',
    keyName: 'housing_json', isCompleted: false
},
{
    tabName: 'supportive-relationships',
    title: 'Supportive Relationships',
    path: 'ytp-supportive-relationships',
    keyName: 'sracc_json', isCompleted: false
},
{
    tabName: 'health',
    title: 'Health',
    path: 'ytp-health',
    keyName: 'health_json',
    isCompleted: false
},
{
    tabName: 'documentation',
    title: 'Documentation',
    path: 'ytp-documentation',
    keyName: 'documentation_json',
    isCompleted: false
}];
