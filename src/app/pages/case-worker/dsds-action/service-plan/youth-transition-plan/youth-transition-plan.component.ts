import { Component, OnInit, Output } from '@angular/core';
import { YouthTransitionPlanService } from './youth-transition-plan.service';
import { DataStoreService, AlertService, AuthService, CommonHttpService } from '../../../../../@core/services';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import * as jsPDF from 'jspdf';
import { YTP_TABS } from './ytp-config';
import { Router, ActivatedRoute } from '@angular/router';
import { ChildRemovalService } from '../../child-removal/child-removal.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'youth-transition-plan',
  templateUrl: './youth-transition-plan.component.html',
  styleUrls: ['./youth-transition-plan.component.scss']
})
export class YouthTransitionPlanComponent implements OnInit {
  ytpPlanGoalList: any[] = [];
  selectedYTPPlan: any;
  ytpData: any;
  selectedClientId: string;
  approvalProcess: string;
  selectedItem: string;
  selectedPlan: any;
  selectedPlanId: string;
  getUsersList = [];
  selectedPerson: any;
  selectService: any;
  id: any;
  daNumber: any;
  user: AppUser;
  returnFormGroup: FormGroup;
  viewReturnReason: any;

  tabs = YTP_TABS;
  constructor(
    private _ytpService: YouthTransitionPlanService,
    private _dataStoreService: DataStoreService,
    private alertService: AlertService,
    private _datastore: DataStoreService,
    private _authService: AuthService,
    private _commonhttp: CommonHttpService,
    private _router: Router,
    private route: ActivatedRoute,
    private childRemovalService: ChildRemovalService,
    private formBuilder: FormBuilder) { }

  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
  selYouth: any;
  ytpSummary: any = {};

  ngOnInit() {
    this.id = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID); // '9725f731-43db-456d-bc82-7b4270218bc3'; test id
    this.daNumber = this._datastore.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.user = this._authService.getCurrentUser();
    this.initForms();
    this._ytpService.onGetPerson.subscribe(data => {
      if (data) {
        this.selectedClientId = data;
        this._ytpService.getYTPPlanList(data).subscribe((item) => {
          this.ytpPlanGoalList = item;
          this.selectedYTPPlan = null;
          this._dataStoreService.setData('YTPDATA', this.selectedYTPPlan);
        });
      }
    });
  }

  initForms() {
    this.returnFormGroup = this.formBuilder.group({
      returnreason: [null, Validators.required],
    });
  }

  getPersonName(id) {
    return this._ytpService.getPersonName(id);
  }

  selectPlan(item) {
    this.selectedYTPPlan = item;
    this.getYTPSummary(item);
    this.selectedYTPPlan.summary_json = this.ytpSummary;
    this._dataStoreService.setData('YTPDATA', this.selectedYTPPlan);
    this._ytpService.youthtransitionplanid = this.selectedYTPPlan.youthtransitionplanid;
    this.tabs = this._ytpService.processTabState(this.tabs, item);
    this._ytpService.onGetYTPData.emit(this.selectedYTPPlan);
    this._router.navigate(['ytp-summary'], { relativeTo: this.route });
  }

  getYTPSummary(item) {
    this.ytpSummary.clientName = this._dataStoreService.getData('YTP_SEL_CHILD').personname;
    this.ytpSummary.dob = this._dataStoreService.getData('YTP_SEL_CHILD').dob;
    this.ytpSummary.caseno = this.childRemovalService.daNumber;
    this.ytpSummary.transplancompleted = item.completiondate ? item.completiondate : '';
    this.ytpSummary.planfollowupdate = item.nextduedate ? item.nextduedate : '';
    if(item.summary_json){        //@TM: Show data from DB once the plan is created and saved
      this.ytpSummary.effectivedate = item.summary_json.effectivedate;
      this.ytpSummary.caseworkername = item.summary_json.caseworkername;
      this.ytpSummary.primarypermanencytype = item.summary_json.primarypermanencytype;
      // this.ytpSummary.completionDate = item.completiondate;
      // this.ytpSummary.planfollowupdate = item.nextduedate;
      this.ytpSummary.assessments = item.summary_json.assessments ? item.summary_json.assessments : [];
      this.ytpSummary.participants = item.summary_json.participants ? item.summary_json.participants : [];
    } else {          //@TM: Show data from api if the plan is created for the 1st time
      this.ytpSummary.caseworkername = this.user.user.username;
      this._ytpService.getYTPSummary(item.clientid).subscribe(
        response => {
          if (response && Array.isArray(response) && response.length) {
            this.ytpSummary.effectivedate = response[0].effectivedate ? response[0].effectivedate : '';
            this.ytpSummary.primarypermanencytype = response[0].primarypermanencytype ? response[0].primarypermanencytype : '';
          }
        }
      );
    }
  }

  async downloadYTPPdf() {
    const pages = document.getElementsByClassName('pdf-page');
    let pageImages = [];
    for (let i = 0; i < pages.length; i++) {
      const pageName = pages.item(i).getAttribute('data-page-name');
      if (pageName === 'Youth Transitional Plan') {
        await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
          const img = canvas.toDataURL('image/png');
          pageImages.push(img);
        });
      }
    }

    this.pdfFiles.push({ fileName: 'Youth Transitional Plan', images: pageImages });
    pageImages = [];
    this.convertImageToPdf();
  }

  convertImageToPdf() {
    this.pdfFiles.forEach((pdfFile) => {
      var doc = null;
      doc = new jsPDF();
      var width = doc.internal.pageSize.getWidth() - 10;
      var heigth = doc.internal.pageSize.getHeight() - 10;

      pdfFile.images.forEach((image, index) => {

        doc.addImage(image, 'PNG', 3, 5, width, heigth);
        if (pdfFile.images.length > index + 1) {
          doc.addPage();
        }
      });
      doc.save(pdfFile.fileName);
    });
    (<any>$('#youthTransPlan')).modal('hide');
    this.pdfFiles = [];
  }

  youthTransPlanPrint(modal) {
    this.selYouth = modal;
    // this.getYTPSummary(modal);
    this.selYouth.summary_json = this.ytpSummary;

    if (this.selYouth.summary_json && this.selYouth.summary_json.documentation_json 
      && this.selYouth.summary_json.documentation_json.youthSign) {
      setTimeout(() => {
      this.createImage(this.selYouth.documentation_json.youthSign)
      }, 1000);
    }
    (<any>$('#youthTransPlan')).modal('show');
  }

  createImage(data) {
    var image = new Image();
    var blob = new Blob([data], { type: 'image/png' });
    image.src = URL.createObjectURL(blob);
    document.body.appendChild(image);
  }

  createPlan(clientId) {
    this._ytpService.createYTP(clientId).subscribe((item) => {
      this.alertService.success('YTP created successfully!');
      this._ytpService.getYTPPlanList(clientId).subscribe((item) => {
        console.log(item);
        console.log(JSON.stringify(item));
        this.ytpPlanGoalList = item;
        this.selectedYTPPlan = null;
        this._dataStoreService.setData('YTPDATA', this.selectedYTPPlan);
      });

    });
  }

  /**
   * Public provider assignment & Routing
   */
  getRoutingUser(approval, selectedYTP) {
    this.approvalProcess = approval;
    this.selectedPlan = selectedYTP;
    this.selectedPlanId = selectedYTP.youthtransitionplanid;
    this.selectService = selectedYTP;
    this.getUsersList = [];
    if (approval === 'Pending') {
      this._commonhttp
        .getPagedArrayList(
          new PaginationRequest({
            where: { appevent: 'YTP' },
            method: 'post'
          }),
          'Intakedastagings/getroutingusers'
        )
        .subscribe(result => {
          this.getUsersList = result.data;
          this.getUsersList = this.getUsersList.filter(
            users => users.userid !== this._authService.getCurrentUser().user.securityusersid
          );
        });
    } else {
      (<any>$('#confirm-decision')).modal('show');
    }
  }

  confirmDecision() {
    this.assignNewUser();
    (<any>$('#confirm-decision')).modal('hide');
  }

  cancelDecision() {
    this.returnFormGroup.reset();
    (<any>$('#confirm-decision')).modal('hide');
  }

  showReturnReason(item: any) {
    this.viewReturnReason = item.returnreason ? item.returnreason : 'No return reason found!';
  }

  resetReturnReason() {
    this.viewReturnReason = null;
  }


  selectPerson(row) {
    this.selectedPerson = row;
  }

  assignNewUser() {
    const payload = {
      eventcode: 'YTP',
      tosecurityusersid: this.selectedPerson ? this.selectedPerson.userid : '',
      objectid: this.id, // This is the intakeserviceid
      serviceNumber: this.daNumber,
      approvalstatustypekey: this.approvalProcess,
      serviceplanid: this.selectedPlanId,
      returnreason: this.returnFormGroup.get('returnreason').value
    };

    this._commonhttp.create(
      payload,
      'youthtransitionplan/youthTransitionPlanRouting'
    ).subscribe(
      (response) => {
        this.alertService.success('Submitted successful!');
        this.selectedPlan.approvalstatuskey = this.approvalProcess;
        if (this.approvalProcess === 'Return') {
          this.selectedPlan.returnreason = this.returnFormGroup.get('returnreason').value;
        }
        (<any>$('#intake-caseassignnewX')).modal('hide');
      },
      (error) => {
        this.alertService.error('Unable to submit!');
      });
  }
  onTabClick(tabItem, contentElement: HTMLElement) {
    contentElement.scrollIntoView();
    this.tabs = this._ytpService.processTabState(this.tabs, this._dataStoreService.getData('YTPDATA'));
  }


}
