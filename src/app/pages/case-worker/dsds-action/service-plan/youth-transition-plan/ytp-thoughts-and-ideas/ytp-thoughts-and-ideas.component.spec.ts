/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { YtpThoughtsAndIdeasComponent } from './ytp-thoughts-and-ideas.component';

describe('YtpThoughtsAndIdeasComponent', () => {
  let component: YtpThoughtsAndIdeasComponent;
  let fixture: ComponentFixture<YtpThoughtsAndIdeasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtpThoughtsAndIdeasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtpThoughtsAndIdeasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
