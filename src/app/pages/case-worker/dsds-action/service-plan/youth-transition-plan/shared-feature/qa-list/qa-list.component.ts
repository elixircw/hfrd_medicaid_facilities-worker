import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'qa-list',
  templateUrl: './qa-list.component.html',
  styleUrls: ['./qa-list.component.scss']
})
export class QaListComponent implements OnInit {

  @Input() questions;
  constructor() { }

  ngOnInit() {
  }

}
