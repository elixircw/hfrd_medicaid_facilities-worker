import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, DataStoreService } from '../../../../../../@core/services';
import { YouthTransitionPlanService } from '../youth-transition-plan.service';

@Component({
  selector: 'ytp-housing',
  templateUrl: './ytp-housing.component.html',
  styleUrls: ['./ytp-housing.component.scss']
})
export class YtpHousingComponent implements OnInit {
  housingFormGroup: FormGroup;
  housingShortTermGoals = [];
  housingDoYouKnowItems: any;
  ytpData: any;
  store: any;

  constructor(private formBuilder: FormBuilder,
    private _alertservice: AlertService,
    private _ytpService: YouthTransitionPlanService,
    private _dataStoreService: DataStoreService) {
    this.store = this._dataStoreService.getCurrentStore();
  }

  ngOnInit() {
    this.housingFormGroup = this.formBuilder.group({
      currentlivingsituation: [null, Validators.required],
      futuregoals: [null, Validators.required],
      planforhousing: [null, Validators.required]
    });

    this.housingDoYouKnowItems = [{
      question: 'All your housing options?',
      value: null,
      comments: ''
    },
    {
      question: 'How to secure funding for housing? How to apply for section 8 housing? Or, how to find information for low income housing in the area, if needed? ',
      value: null,
      comments: ''
    },
    {
      question: 'About SILA (Semi-independent living program)?',
      value: null,
      comments: ''
    },
    {
      question: 'How to get on the HUD list?',
      value: null,
      comments: ''
    },
    {
      question: 'What’s needed to get housing (i.e. criminal background, leasing agreement)?',
      value: null,
      comments: ''
    },
    {
      question: 'About the Family Reunification Program (FUP)?',
      value: null,
      comments: ''
    },
    ];

    this.ytpData = this.store['YTPDATA'];

    if (this.ytpData && this.ytpData.housing_json) {
      this.housingFormGroup.patchValue(this.ytpData.housing_json);
      if (this.ytpData.housing_json.goals) {
        this.housingShortTermGoals = this.ytpData.housing_json.goals;
        if (Array.isArray(this.housingShortTermGoals) && this.housingShortTermGoals.length) {
          this.housingShortTermGoals.forEach(item => {
            item.projected_date = item.projected_date ? new Date(item.projected_date) : '';
          });
        }
      }
      if (this.ytpData.housing_json.doyouknow) {
        this.housingDoYouKnowItems = this.ytpData.housing_json.doyouknow;
      }
    }
  }

  save() {
    const data = this.housingFormGroup.getRawValue();
    data.goals = this.housingShortTermGoals;
    data.doyouknow = this.housingDoYouKnowItems;

    let isCompleted = false;
    if (this.housingFormGroup.valid) {
      const filledQuestionsLength = this.housingDoYouKnowItems.filter(question => question.value !== null).length;
      if (filledQuestionsLength === this.housingDoYouKnowItems.length && data.goals.length > 0) {
        isCompleted = true;
      }
    }
    data.isCompleted = isCompleted;

    this._ytpService.patchData('housing_json', data)
      .subscribe(
        response => {
          this.store['YTPDATA'].housing_json = data;
          this._alertservice.success('Housing details entered successfully!');
        },
        error => {
          this._alertservice.error('Error in entering housing details!');
        }
      );
  }

  clear() {
    this.housingFormGroup.reset();
    this.housingShortTermGoals = [];
  }

}
