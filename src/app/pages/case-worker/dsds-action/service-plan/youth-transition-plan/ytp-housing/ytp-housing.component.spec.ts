/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { YtpHousingComponent } from './ytp-housing.component';

describe('YtpHousingComponent', () => {
  let component: YtpHousingComponent;
  let fixture: ComponentFixture<YtpHousingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtpHousingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtpHousingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
