/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { YtpSupportiveRelationshipsComponent } from './ytp-supportive-relationships.component';

describe('YtpSupportiveRelationshipsComponent', () => {
  let component: YtpSupportiveRelationshipsComponent;
  let fixture: ComponentFixture<YtpSupportiveRelationshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtpSupportiveRelationshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtpSupportiveRelationshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
