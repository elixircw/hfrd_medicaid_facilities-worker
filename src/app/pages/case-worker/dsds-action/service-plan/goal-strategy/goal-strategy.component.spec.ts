import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalStrategyComponent } from './goal-strategy.component';

describe('GoalStrategyComponent', () => {
  let component: GoalStrategyComponent;
  let fixture: ComponentFixture<GoalStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoalStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoalStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
