import { TestBed, inject } from '@angular/core/testing';

import { ScServicePlanService } from './sc-service-plan.service';

describe('ScServicePlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScServicePlanService]
    });
  });

  it('should be created', inject([ScServicePlanService], (service: ScServicePlanService) => {
    expect(service).toBeTruthy();
  }));
});
