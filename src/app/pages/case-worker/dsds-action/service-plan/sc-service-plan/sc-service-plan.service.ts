import { Injectable } from '@angular/core';
import { CommonHttpService, DataStoreService, AuthService } from '../../../../../@core/services';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { AddServicePlanGoal, AddActivity } from '../_entities/service-plan.model';

@Injectable()
export class ScServicePlanService {
  teamTypeKey: string;
  id: string;
  daNumber: string;

  constructor(private _commonService: CommonHttpService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService) {
    this.teamTypeKey = this._authService.getAgencyName();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
  }



  getPersonsList() {
    const id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    return this._commonService
      .getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: { 'objectid': this.id, 'objecttypekey': 'servicecase' }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
        // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
      );
  }
  getActivityCategoryTypeList() {
    return this._commonService.getArrayList( { where: {}, nolimit: true, method: 'get' },
    'servicetype?filter');
  }
  getActivityCategorySubTypeList() {
    return this._commonService.getArrayList({method: 'get', order: 'servicesubtypedescription', nolimit: true}, 'servicesubtype?filter');
  }
  getActivityGoalList() {
    return this._commonService.getArrayList({ method: 'get', where: { objectid: this.id } }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.GoalUrl + '?filter');
  }
  getActivityStatusList() {
    return this._commonService.getArrayList({ nolimit: true, method: 'get' }, 'serviceplan/activitystatustype?filter');
  }

  addServicePlanGoal(goal: AddServicePlanGoal) {
    const addServciePlanGoal = Object.assign(goal);
    addServciePlanGoal.objecttypekey = 'ServiceRequest';
    addServciePlanGoal.objectid = this.id;
    this._commonService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.GoalUrl + '/'; // WSO2;
    return this._commonService.create(addServciePlanGoal);
  }

  getServicePlanGoal(pageNo: number) {
    return this._commonService
      .getArrayList(
        new PaginationRequest({
          nolimit: true,
          page: pageNo,
          where: { objectid: this.id },
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.GoalUrl + '?filter'
      )
      .map(res => {
        return res;
      });
  }

  saveActivity(ativity: AddActivity) {
    const activityAdd = Object.assign(ativity);
    activityAdd.servicetypekey = ativity.servicetypekey;
    activityAdd.objecttypekey = 'ServiceRequest';
    activityAdd.objectid = this.id;
    this._commonHttpService.endpointUrl = 'serviceplan/activity/';
    return this._commonHttpService.create(activityAdd);
  }

  getServicePlanActivity(pageNo: number) {
    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          limit: 10,
          page: pageNo,
          where: { objectid: this.id },
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ActivityUrl + '?filter'
      );
  }

}
