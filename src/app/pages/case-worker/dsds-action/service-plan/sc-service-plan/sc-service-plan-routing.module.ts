import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScServicePlanComponent } from './sc-service-plan.component';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';

const routes: Routes = [{
  path: '', component: ScServicePlanComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScServicePlanRoutingModule { }
