import { Component, OnInit } from '@angular/core';
import { ScServicePlanService } from './sc-service-plan.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { DataStoreService, CommonHttpService, AlertService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AddServicePlanGoal, ServicePlanGoal } from '../_entities/service-plan.model';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'sc-service-plan',
  templateUrl: './sc-service-plan.component.html',
  styleUrls: ['./sc-service-plan.component.scss']
})
export class ScServicePlanComponent implements OnInit {

  inHouseHoldList = [];
  showAddGoals = false;
  goalForm: FormGroup;
  showAddActivity: boolean;
  activityCategoryTypeList = [];
  activityCategorySubTypeList = [];
  activityGoalList = [];
  activityStatusList = [];
  activityStrategy: FormGroup;
  id: string;
  caseNumber: string;
  servicePlanGoal$: Observable<ServicePlanGoal[]>;
  selectedGoal: any;
  goalActivty$: Observable<any[]>;
  goalActivityCount$: Observable<number>;
  constructor(private _service: ScServicePlanService, private formBuilder: FormBuilder,
    private _dataStoreService: DataStoreService, private _alertService: AlertService,
    private _httpService: CommonHttpService) { }

  ngOnInit() {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.caseNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.getServicePlanGoal(1);
    this.getServicePlanActivity(1);
    this.goalforminit();
    this.activityforminit();
    this._service.getPersonsList().subscribe(res => {
      const personList = res.data;
      this.inHouseHoldList = personList.filter(person => person.ishousehold === 1);
    });
    this._service.getActivityCategoryTypeList().subscribe(res => {
      this.activityCategoryTypeList = res;
    });
    this._service.getActivityCategorySubTypeList().subscribe(res => {
      this.activityCategorySubTypeList = res;
    });
    this._service.getActivityGoalList().subscribe(res => {
      this.activityGoalList = res;
    });
    this._service.getActivityStatusList().subscribe(res => {
      this.activityStatusList = res;
    });
  }

  showAddGoalsForm(event) {
    if (event === 1) {
      this.showAddGoals = true;
    } else if (event === 0) {
      this.goalForm.reset();
      this.showAddGoals = false;
    }
  }

  showAddActivityForm(event, goal?) {
    if (event === 1) {
      this.showAddActivity = true;
      this.selectedGoal = (goal) ? goal : null;
    } else if (event === 0) {
      this.activityStrategy.reset();
      this.showAddActivity = false;
    }
  }

  goalforminit() {
    this.goalForm = this.formBuilder.group({
      goal: [null],
      strategy: [null],
      goaldate: [new Date()],
    });
  }

  activityforminit() {
    this.activityStrategy = this.formBuilder.group({
      activity: ['', Validators.required],
      servicetypekey: [null],
      servicesubtypekey: [null],
      personinvolved: [null],
      responsibleperson: [null],
      reevaluationdate: [null],
      completiondate: [null],
      serviceplangoalid: [null],
      serviceplanactivitystatustypekey: ['', Validators.required]
    });
  }

  addServicePlanGoal(goal: AddServicePlanGoal) {
    this._service.addServicePlanGoal(goal).subscribe(
      response => {
        this._alertService.success('Goal added successfully');
        this.showAddGoalsForm(0);
        this.getServicePlanGoal(1);
      },
      error => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
      }
    );
  }

  getServicePlanGoal(pageNo: number) {
    this.servicePlanGoal$ = this._service.getServicePlanGoal(pageNo);
  }

  editGoal(goal) {
    this.selectedGoal = goal;
  }

  addActivity() {
    const reqObj = Object.assign(this.activityStrategy.getRawValue());
    reqObj.serviceplangoalid = this.selectedGoal.serviceplangoalid;
    this._service.addServicePlanGoal(reqObj).subscribe((data) => {
      // this.activityStrategy.reset();
      this.showAddActivityForm(0);
      this._alertService.success('Activity added successfully');
    },
      (error) => console.log(error)
    );
  }

  getServicePlanActivity(pageNo: number) {
    this._service.getServicePlanActivity(pageNo).subscribe((res) => {
      this.goalActivty$ = Observable.of(res.data);
      this.goalActivityCount$ = Observable.of(res.count);
    });
  }
}
