import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScServicePlanRoutingModule } from './sc-service-plan-routing.module';
import { ScServicePlanComponent } from './sc-service-plan.component';
import { ScServicePlanService } from './sc-service-plan.service';
import { FormMaterialModule } from '../../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    ScServicePlanRoutingModule,
    FormMaterialModule
  ],
  declarations: [ScServicePlanComponent],
  providers: [ScServicePlanService]
})
export class ScServicePlanModule { }
