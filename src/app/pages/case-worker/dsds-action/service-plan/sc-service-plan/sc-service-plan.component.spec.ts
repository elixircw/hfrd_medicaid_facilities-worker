import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScServicePlanComponent } from './sc-service-plan.component';

describe('ScServicePlanComponent', () => {
  let component: ScServicePlanComponent;
  let fixture: ComponentFixture<ScServicePlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScServicePlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScServicePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
