import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import { AlertService, AuthService, CommonHttpService, ValidationService, DataStoreService, GenericService, SessionStorageService } from '../../../../../../@core/services';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AppUser } from '../../../../../../@core/entities/authDataModel';
import * as jsPDF from 'jspdf';
import { Occurence, RepeatsOn, SearchPlan, SearchPlanRes, ServicePlanConfig, PurchaseAuthorization, ServiceLogApproval } from '../../_entities/service-plan.model';
import { DatePipe } from '@angular/common';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { date } from 'ng4-validators/src/app/date/validator';
import { AppConstants } from '../../../../../../@core/common/constants';
import { ActivatedRoute, Router } from '@angular/router';
import { InvolvedPerson } from '../../../involved-person/_entities/involvedperson.data.model';
import * as moment from 'moment';
import { MatDatepickerInputEvent } from '@angular/material';
import { FinanceUrlConfig } from '../../../../../finance/finance.url.config';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';

@Component({
    selector: 'referred-services',
    templateUrl: './referred-services.component.html',
    styleUrls: ['./referred-services.component.scss'],
    providers: [DatePipe]
})
export class ReferredServicesComponent implements OnInit {

    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];

    referredServiceList$: Observable<any[]>;
    referredServiceForm: FormGroup;
    addNewPurchaseAuthorization: FormGroup;
    EditreferredServiceForm: FormGroup;
    purchaseServiceForm: FormGroup;

    addNewReferredServiceSecondForm: FormGroup;
    refrredSearchFormDiv: boolean;
    searchData: boolean = false;
    selectedServicePlan: boolean = false;

    categoryTypes$: Observable<DropdownModel[]>;
    categorySubTypes$: Observable<DropdownModel[]>;
    paymentTypes$: Observable<DropdownModel>;
    vendorServices$: Observable<DropdownModel[]>;
    serviceEndReason$: Observable<DropdownModel[]>;
    reasonServiceNotReceived$: Observable<DropdownModel[]>;

    clientProgramNames$: Observable<DropdownModel[]>;
    frequencyCds$: Observable<DropdownModel[]>;
    durationCds$: Observable<DropdownModel[]>;

    servicePlanForm: FormGroup;
    addServicePlanForm: FormGroup;
    timesInDayHide = false;
    id: string;
    // D-06581
    daNumber: string;
    searchPlan: any;
    ReferredServices$: Observable<any[]>;
    SearchPlan$: Observable<SearchPlan[]>;
    SearchPlanRes$: Observable<SearchPlanRes>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    servicePlanCountyValuesDropdownItems$: Observable<DropdownModel[]>;
    providerId: string;
    nextDisabled = true;
    isrepeatschecked: boolean;
    isRepeats = 0;
    zoom: number;
    defaultLat: number;
    defaultLng: number;
    mapBtn = false;
    markersLocation = ([] = []);
    minEstDate = new Date();
    endMinDate = new Date();
    referredService: any;
    purchaseAuthorizationGet: PurchaseAuthorization[];
    singlePurchaseAuthorization: PurchaseAuthorization;
    private token: AppUser;
    isSupervisor: boolean;
    private headerSummary: any;
    serviceLogApproval: ServiceLogApproval;
    getUsersList: any[];
    enableSaveServiceLog = false;
    selectedPerson: any;
    assignedTo: any;
    originalUserList: any[];
    status: number;
    involevedPerson$: Observable<InvolvedPerson>;
    rcCjamsID: number;
    authId: string;
    sendApprovalEnable: boolean;
    isExceed: number;
    referredServiceCP: any;
    resetForm = true;
    referredServiceList: any[];
    reason_tx: any;
    isRejected: boolean;
    serviceStartDate: Date;
    serviceEndDate: Date;
    disableApprove = true;
    minActDate: Date;
    childList = [];
    clientGender: string;
    clientDob: Date;
    firstChild: any;
    clientAge: any;
    clientName: string;
    timestamp: Date;
    approveBtnTxt: string;
    reasonDesc: boolean;
    payableApprovalHistory: any[];
    pageInfo: PaginationInfo = new PaginationInfo();
    pagination: PaginationInfo = new PaginationInfo();
    totalPage: number;
    personid: any;
    activityService: any[];
    activityServicePlan: any[];
    servicePlan: any[];
    serviceplanID: any;
    addNewService: boolean;
    purchaseAuthEnable: boolean;
    currentDate: Date;
    serviceReceived: boolean;
    serviceNotReceived: boolean;
    clientProgramNames: any[];
    startdate: any;
    enddate: any;
    estimatedenddate: any;
    clientprogramselection: any;
    isServiceCase: any;
    serviceCase: boolean;
    otherCase: boolean;
    servicetypeid: any;
    fiscalCodes: any[];
    directorApproval: boolean;
    roleBased: boolean;
    userRole: string;
    eventCode: any;
    roletypekey: string;
    role: any;
    rolename: string;
    clientEligibility: any[];
    clientEligibilityStatus: any;
    totalcount: any;
    isapproved: boolean;
    purchaseAuthorization: any;
    isClosed = false;
    purchaseAuthorizationList: any = [];
    newAuthorization: boolean;
    isExpired: Boolean = false;
    dedicatedSelect: any;
    checkDedicated: any;
    childAccountsList = [];
    listPageInfo: PaginationInfo = new PaginationInfo();
    listCount: any;
    purchaseAuthorizationServiceLog: any;
    referredServiceCheck: any;
    disbleService: boolean;
    return_tx: string;
    financeApprove: string;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _commonHttpService: CommonHttpService,
        private _genericService: GenericService<any>,
        private _authService: AuthService,
        private _alertService: AlertService,
        private datePipe: DatePipe,
        private _dataStoreService: DataStoreService,
        private _session: SessionStorageService,
        private _router: Router
    ) {
        this.formInitialize();
        this.token = this._authService.getCurrentUser();
    }

    private formInitialize() {
        this.purchaseServiceForm = this.formBuilder.group(
            {
                fiscalCode: ['', Validators.required],
                voucherRequested: ['', Validators.required],
                costnottoexceed: ['', Validators.required],
                justificationCode: ['', Validators.required],
                startDt: [null, Validators.required],
                endDt: [null, Validators.required],
                fundingstatus: '',
                authorization_id: '',
                paymentstatus: '',
                final_amount_no: '',
                client_account_no: '',
                reason_tx: ''
            });

        this.referredServiceForm = this.formBuilder.group(
            {
                servicetypeid: [''],
                providerid: '',
                providername: '',
                taxid: '',
                zipcode: ''
            });

        this.EditreferredServiceForm = this.formBuilder.group(
            {
                servicetypeid: [{ value: '', disabled: true }],
                serviceNotReceivedReason: [''],
                serviceReceivedReason: [''],
                providerid: [{ value: '', disabled: true }],
                providername: [{ value: '', disabled: true }],
                taxid: [{ value: '', disabled: true }],
                zipcode: [{ value: '', disabled: true }],
                clientprogramnameid: [{ value: '', disabled: true }],
                estbegindate: [null, Validators.required],
                actbegindate: [null, Validators.required],
                estenddate: [null, Validators.required],
                actenddate: [null],
                actbegintime: [null, Validators.required],
                actendtime: [null, Validators.required],
                agencynotes: '',
                outcome: '',
                frequencyCdId: '',
                durationCdId: '',
                dateReffered: '',
                courtOrderedSw: '',
                serviceplanactionid: [null],
                serviceplanid: [null],
                serviceplanname: ''
            });

        this.addNewPurchaseAuthorization = this.formBuilder.group(
            {

            });

        this.addNewReferredServiceSecondForm = this.formBuilder.group(
            {
                clientprogramnameid: ['', Validators.required],
                servicetypeid: [''],
                frequencyCdId: ['', Validators.required],
                durationCdId: ['', Validators.required],
                estbegindate: ['', Validators.required],
                actbegindate: [''],
                estenddate: ['', Validators.required],
                actenddate: [''],
                dateReffered: ['', Validators.required],
                courtOrderedSw: '',
                actbegintime: '',
                actendtime: '',
                agencynotes: '',
                serviceplanactionid: [null],
                serviceplanid: [null],
                serviceplanname: '',
                endServiceReasonCd: [null],
                outcome: '',
                noServiceReasonCd: [null],
                servicereceived: [null],
                servicenotreceived: [null],
                serviceReceivedReason: [null],
                // End D-06585
            });
    }

    searchReferredServices(mode) {
        const searchForm = this.referredServiceForm.getRawValue();
        if (!(searchForm.servicetypeid)) {
            this.servicetypeid = '';
        } else {
            this.servicetypeid = searchForm.servicetypeid.value;
        }
        if (mode === 'new') {
            this.pagination.pageNumber = 1;
            this.totalcount = 0;
        }
        this.searchData = true;
        const providerId = this.referredServiceForm.get('providerid').value ? this.referredServiceForm.get('providerid').value : '';
        const providerName = this.referredServiceForm.get('providername').value ? this.referredServiceForm.get('providername').value : '';
        const taxid = this.referredServiceForm.get('taxid').value ? this.referredServiceForm.get('taxid').value : '';
        const zipcode = this.referredServiceForm.get('zipcode').value ? this.referredServiceForm.get('zipcode').value : '';
        const services = this.servicetypeid ? this.servicetypeid : ''; // this.referredServiceForm.get('servicetypeid').value ? this.referredServiceForm.get('servicetypeid').value : '';

        this.ReferredServices$ = this._commonHttpService.getArrayList(
            {
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.VendorServiceSearch + '?providerId=' + providerId +
            '&providerName=' + providerName +
            '&taxId=' + taxid +
            '&serviceId=' + services +
            '&zipCd=' + zipcode +
            '&access_token=' + this.token.id +
            '&page=' + this.pagination.pageNumber +
            '&limit=' + this.pagination.pageSize
        )
            .map(res => {
                if (res && res['UserToken'].length > 0) {
                   // this.servicetypeid = '' ;
                    this.totalcount = (res['UserToken'] && res['UserToken'].length > 0) ? res['UserToken'][0].totalcount : 0;
                    return res['UserToken'];
                }
            });
    }

    pageChanged(page) {
        this.pagination.pageNumber = page;
        this.searchReferredServices('page');
    }

    ngOnInit() {
        this.pagination.pageNumber = 1;
        this.pageInfo.pageNumber = 1;
        this.isSupervisor = (this._authService.getCurrentUser().role.name === AppConstants.ROLES.SUPERVISOR) ? true : false;
        this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        if (this.isSupervisor) {
            this.purchaseAuthorization = this._session.getItem('PurchaseAuthorization');
            this.purchaseAuthorizationServiceLog = this._session.getItem('PurchaseAuthorizationServiceLog');
            this._session.setItem('PurchaseAuthorization', null);
            this._session.setItem('PurchaseAuthorizationServiceLog', null);
        }
        if (this.isServiceCase === 'true') {
            this.otherCase = false;
        } else {
            this.otherCase = true;
        }
        this.timestamp = new Date();
        this.minActDate = new Date();
        this.minEstDate = new Date();
        this.currentDate = new Date();
        this.refrredSearchFormDiv = true;
        // D-06581
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        if (this.daNumber.toLowerCase().indexOf('cw') !== -1) {
           this.daNumber = this.daNumber.slice(2);
        }
        this.getInvolvedPerson();
        this.purchaseServiceForm.get('final_amount_no').disable();
        this.purchaseServiceForm.get('paymentstatus').disable();
        this.purchaseServiceForm.get('fundingstatus').disable();
        this.purchaseServiceForm.get('authorization_id').disable();
        this.purchaseServiceForm.get('client_account_no').disable();
        this.disableApprove = true;
        const da_status = this._session.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }
    }

    onServiceSelect(service) {
        this.pagination.pageNumber = 1;
        this.servicetypeid = service.value;
        if (service.additionalProperty === '3335') {
            this.purchaseAuthEnable = false;
        } else if (service.additionalProperty === '3334') {
            this.purchaseAuthEnable = true;
        }
    }
    onActivity() {
        this.pagination.pageNumber = 1;
    }

    // vendor GET list
    getList(pageNo = 1) {
        // get paged arraylist sample
        // D-06581

        this.referredServiceList$ = this._commonHttpService.getArrayList(
            {
                where: { daNumber: this.daNumber, clientid: this.rcCjamsID },
                method: 'get',
                limit: 10,
                page: this.listPageInfo.pageNumber
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.vendorServiceLog + '?filter'
        )
            .map(res => {
                this.referredServiceList = res['servicelogData'];
                this.referredServiceCheck = this.referredServiceList.find(item => item.service_log_id === this.purchaseAuthorizationServiceLog);
                if (this.referredServiceCheck && this.purchaseAuthorizationServiceLog) {
                    this.openpurchaseautherization(this.referredServiceCheck);
                }
                this.listCount = (res && res['servicelogData'] && res['servicelogData'].length) ? res['servicelogData'][0].totalcount : 0;
                return res['servicelogData'];
            });
    }

    listPageChanged(page) {
        this.listPageInfo.pageNumber = page;
        this.getList();
    }

    intiReferredServiceForm() {
        this.addNewService = true;
        this.minActDate = new Date();
        this.minEstDate = new Date();
        this.searchData = false;
        this.referredServiceForm.reset();
        this.addNewReferredServiceSecondForm.reset();
        this.addNewReferredServiceSecondForm.patchValue({
            dateReffered : new Date()
        });
        this.addNewReferredServiceSecondForm.patchValue({
            clientprogramnameid: this.clientprogramselection ? this.clientprogramselection : null
        });
        // this.addNewReferredServiceSecondForm.get('clientprogramnameid').disable();
        this.getActivityService();
        this.addNewReferredServiceSecondForm.controls['actenddate'].disable();
        this.addNewReferredServiceSecondForm.controls['actbegindate'].disable();
        this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].disable();
    }

    listMap() {
        this.zoom = 11;
        this.defaultLat = 39.29044;
        this.defaultLng = -76.61233;
        this.ReferredServices$.subscribe(map => {
            this.markersLocation = [];
            if (map.length > 0) {
                map.forEach(res => {
                    if (res.latitude !== null && res.longitude !== null) {
                        const mapLocation = {
                            lat: +res.latitude,
                            lng: +res.longitude,
                            draggable: +true,
                            providername: res.providername !== null ? res.providername : 'NA',
                            addressline1: res.addressline1 !== null ? res.addressline1 : 'NA',
                        };
                        this.markersLocation.push(mapLocation);
                    } else {
                        const mapLocation = {
                            lat: +this.defaultLat,
                            lng: +this.defaultLng,
                            draggable: +true,
                            providername: res.providername !== null ? res.providername : 'NA',
                            addressline1: res.addressline1 !== null ? res.addressline1 : 'NA',
                        };
                        this.markersLocation.push(mapLocation);
                    }
                });
                if (
                    !this.markersLocation[0].lat !== null &&
                    !this.markersLocation[0].lng !== null
                ) {
                    this.defaultLat = this.markersLocation[0].lat;
                    this.defaultLng = this.markersLocation[0].lng;
                } else {
                    this.defaultLat = 39.29044;
                    this.defaultLng = -76.61233;
                }
                (<any>$('#iframe-popup')).modal('show');
            }
        });
    }

    mapClose() {
        this.markersLocation = [];
    }

    closePopUp() {
        (<any>$('#view-referredservice')).modal('hide');
    }

    selectReferredService() {
        (<any>$('#select-referredservice')).modal('show');
    }

    previous() {
        this.searchData = false;
        this.referredServiceForm.reset();
        this.addNewReferredServiceSecondForm.reset();
        (<any>$('#add-newreferredservice')).modal('show');
    }

    openNewAuthorization() {
        this.dedicatedSelect = null;
        this.checkDedicated = null;
        this.newAuthorization = true;
        this.enableSaveServiceLog = true;
        this.purchaseServiceForm.reset();
        this.purchaseServiceForm.patchValue({
            voucherRequested: 'N'
        });
        this.payableApprovalHistory = [];
        this.getFiscalCategoryCode(this.referredService.agency_program_area_id);
        this.disableApprove = true;
        this.serviceStartDate = this.referredService.actual_start_date;
        this.serviceEndDate = this.referredService.actual_end_date;
        // PurchaseAuthorizationGet
        if (this.referredService && this.referredService.status === 'Returned' && this.isSupervisor) {
            this.isRejected = true;
            this.reasonDesc = true;
            this.purchaseServiceForm.disable();
        } else if (this.referredService && this.referredService.status === 'Denied') {
            this.isRejected = true;
            this.reasonDesc = true;
            this.purchaseServiceForm.disable();
        } else {
            this.approveBtnTxt = 'Send for approval';
            this.reasonDesc = false;
            this.purchaseServiceForm.enable();
            this.purchaseServiceForm.get('final_amount_no').disable();
            this.purchaseServiceForm.get('paymentstatus').disable();
            this.purchaseServiceForm.get('fundingstatus').disable();
            this.purchaseServiceForm.get('authorization_id').disable();
            this.purchaseServiceForm.get('client_account_no').disable();
            this.isRejected = false;
        }
    }

    openPurchaseAutherizationList(purchaseDetail) {
        this.newAuthorization = false;
        if (purchaseDetail && +purchaseDetail.cost_no >= 1000) {
            this.financeApprove = 'Send for Director Approval';
        } else {
            this.financeApprove = 'Send for Finance Approval';
        }
        this.singlePurchaseAuthorization = purchaseDetail ? purchaseDetail : {};
            if (this.singlePurchaseAuthorization.fiscal_category_cd) {
                this.enableSaveServiceLog = false;
            }
            if (this.referredService && this.referredService.status === 'Returned' && this.isSupervisor) {
                this.isRejected = true;
                this.reasonDesc = true;
                this.purchaseServiceForm.disable();
            } else if (this.referredService && this.referredService.status === 'Denied') {
                this.isRejected = true;
                this.reasonDesc = true;
                this.purchaseServiceForm.disable();
            } else {
                this.approveBtnTxt = 'Send for approval';
                this.reasonDesc = false;
                this.purchaseServiceForm.enable();
                this.purchaseServiceForm.get('final_amount_no').disable();
                this.purchaseServiceForm.get('paymentstatus').disable();
                this.purchaseServiceForm.get('fundingstatus').disable();
                this.purchaseServiceForm.get('authorization_id').disable();
                this.purchaseServiceForm.get('client_account_no').disable();
                this.isRejected = false;
            }
            if (this.singlePurchaseAuthorization.authorization_id) {
                this.authId = this.singlePurchaseAuthorization.authorization_id;
                this.getApproveHistory(this.authId);
                if (this.singlePurchaseAuthorization.cost_no >= 1000) {
                    this.directorApproval = true;
                } else {
                    this.directorApproval = false;
                }
                const model = {
                    fiscalCode: this.singlePurchaseAuthorization.fiscal_category_cd,
                    voucherRequested: this.singlePurchaseAuthorization.voucher_requested,
                    costnottoexceed: this.singlePurchaseAuthorization.cost_no ? this.singlePurchaseAuthorization.cost_no : '0.00',
                    justificationCode: this.singlePurchaseAuthorization.justification_text,
                    fundingstatus: this.singlePurchaseAuthorization.fundingstatus,
                    authorization_id: this.singlePurchaseAuthorization.authorization_id ? this.singlePurchaseAuthorization.authorization_id : '',
                    startDt: this.singlePurchaseAuthorization.startdt,
                    endDt: this.singlePurchaseAuthorization.enddt,
                    paymentstatus: this.singlePurchaseAuthorization.paymentstatus,
                    final_amount_no: this.singlePurchaseAuthorization.final_amount_no ? this.singlePurchaseAuthorization.final_amount_no : '0.00',
                    client_account_no: this.singlePurchaseAuthorization.client_account_no ? this.singlePurchaseAuthorization.client_account_no : '',
                    reason_tx: this.singlePurchaseAuthorization.reason_tx ? this.singlePurchaseAuthorization.reason_tx : ''
                };
                this.purchaseServiceForm.setValue(model, { emitEvent: true, onlySelf: false });
                if (this.singlePurchaseAuthorization && !this.singlePurchaseAuthorization.status) {
                    if (!this.isSupervisor) {
                        this.disableApprove = true;
                    } else {
                        this.disableApprove = false;
                        this.purchaseServiceForm.disable();
                    }
                } else {
                    if (this.isSupervisor) {
                        if (this.singlePurchaseAuthorization.status === 39) {
                            this.purchaseServiceForm.disable();
                            this.purchaseServiceForm.get('justificationCode').enable();
                            this.disableApprove = true;
                        } else {
                            this.disableApprove = false;
                            this.purchaseServiceForm.disable();
                        }
                    } else if (!this.isSupervisor && this.singlePurchaseAuthorization.status === 850) {
                        this.serviceStartDate = this.singlePurchaseAuthorization.startdt;
                        this.approveBtnTxt = 'Resend for approval';
                        this.reasonDesc = true;
                        this.enableSaveServiceLog = true;
                        this.disableApprove = true;
                        this.purchaseServiceForm.enable();
                        this.purchaseServiceForm.get('final_amount_no').disable();
                        this.purchaseServiceForm.get('paymentstatus').disable();
                        this.purchaseServiceForm.get('fundingstatus').disable();
                        this.purchaseServiceForm.get('authorization_id').disable();
                        this.purchaseServiceForm.get('client_account_no').disable();
                        this.purchaseServiceForm.get('reason_tx').disable();
                    } else {
                        this.disableApprove = false;
                        this.purchaseServiceForm.disable();
                    }
                }
            }

    }

    sendApprove() {
        this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ServiceLogApproval;
            const modal = {
                'case_id': this.daNumber,
                'fiscalcategorycd' : this.singlePurchaseAuthorization.fiscal_category_cd,
                'costno' : this.singlePurchaseAuthorization.cost_no ? this.singlePurchaseAuthorization.cost_no : '0.00',
                'authorization_id' : this.singlePurchaseAuthorization.authorization_id ? this.singlePurchaseAuthorization.authorization_id : null,
                'provider_id' : this.referredService.provider_id,
                'intakeserviceid' : this.id,
                'assignedtoid' : '',
                'eventcode' : 'PCAUTH',
                'status' : 39,
                'startDt' : this.singlePurchaseAuthorization.startdt,
                'endDt' : this.singlePurchaseAuthorization.enddt,
                'client_account_id': this.singlePurchaseAuthorization.client_account_id,
                'client_id' : this.singlePurchaseAuthorization.client_id,
                'bmanualrouting': this.otherCase
            };
        this._commonHttpService.create(modal).subscribe(
            (response) => {
                console.log('openPurchase', response);
                this._alertService.success('Approval sent successfully!');
                (<any>$('#purchase-autherization')).modal('hide');
                (<any>$('#purchase-autherization-list')).modal('show');
                this.sendApprovalEnable = false;
                this.openpurchaseautherization(this.referredService);
                this.getList(1);
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                this.sendApprovalEnable = false;
            }
        );
    }

    openpurchaseautherization(referredService, mode?: string) {
        if (this.purchaseAuthorizationServiceLog && this.purchaseAuthorizationServiceLog !== 'null' && this.isSupervisor) {
            this.purchaseAuthorizationServiceLog = null;
            (<any>$('#purchase-autherization-list')).modal('show');
        }
        this.payableApprovalHistory = [];
        this.isExpired = false;
        if (referredService && referredService.agency_program_area_id) {
            this.getFiscalCategoryCode(referredService.agency_program_area_id);
        }
        this.disableApprove = true;
        this.serviceStartDate = referredService.actual_start_date;
        this.serviceEndDate = referredService.actual_end_date;
        this.referredService = referredService;
        let actualEndDate = this.referredService.actual_end_date ? new Date(this.referredService.actual_end_date) : null;
        let endDate = this.datePipe.transform(actualEndDate, 'dd/MM/yyyy');
        let today = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
        if ( endDate && endDate < today) {
            this.isExpired = true;
        }
        // PurchaseAuthorizationGet
        if (mode !== 'New') {
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    where: { service_log_id: this.referredService.service_log_id },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.PurchaseAuthorizationGet + '?filter'
            ).subscribe(result => {
                if (result && result['data'].length > 0) {
                    this.purchaseAuthorizationList = result['data'];
                    this.singlePurchaseAuthorization = result['data'][0];
                    if (this.sendApprovalEnable) {
                        if (!this.isSupervisor) {
                            this.sendApprove();
                        }
                    }
                } else {
                    this.purchaseAuthorizationList = [];
                }
            });
        }
    }

    serviceStartDateChange(startDate: MatDatepickerInputEvent<Date>) {
        if (this.purchaseServiceForm.get('endDt').value && this.purchaseServiceForm.get('endDt').value < startDate) {
            this.purchaseServiceForm.get('endDt').reset();
        }
    }


    print() {
        (<any>$('#view-log')).modal('show');
        (<any>$('#purchase-autherization')).modal('hide');
    }

    fiscalCodeChanges(category) {
        if (category === '7503') {
            this.dedicatedSelect = null;
            this.checkDedicated = null;
            (<any>$('#purchase-autherization')).modal('hide');
            (<any>$('#dedicated-check')).modal('show');
        } else if (category === '2110') {
            this.purchaseServiceForm.patchValue({
                client_account_no: null
            });
            this._alertService.warn('SSA approval is required for the fiscal category code Non Recurring OTO Subsidy Expenses');
        } else if (!(category === '7502' && category === '7503')) {
            this.purchaseServiceForm.patchValue({
                client_account_no: null
            });
        }
        /* switch (category.toString()) {
            case "7503":
                this._alertService.info(`Either expense must be related to child's impairment or written authorization must be received from SSA for the expenses`);
                break;
        } */

    }

    dedCheck(value) {
        if (value === '0') {
            (<any>$('#purchase-autherization')).modal('hide');
            (<any>$('#dedicated-check')).modal('hide');
            (<any>$('#purchase-autherization-list')).modal('show');
        } else if (value === '1') {
            (<any>$('#dedicated-check')).modal('hide');
            (<any>$('#purchase-autherization')).modal('show');
        }
    }


    savePurchaseAuth(purchaseServiceForm) {
        this.referredServiceCP = this.referredService;
            // this.savePurchaseAuth(purchaseServiceForm);
            // Start D-06715, D-06716
            if (this.purchaseServiceForm.get('fiscalCode').hasError('required')) {
                this._alertService.warn('Please select Fiscal category code!');
                return;
            } else if (this.purchaseServiceForm.get('voucherRequested').hasError('required')) {
                this._alertService.warn('Please select Voucher requested!');
                return;
            }
            // End D-06715, D-06716

            if (this.purchaseServiceForm.valid) {
                if (+this.purchaseServiceForm.get('costnottoexceed').value <= 0) {
                    this._alertService.error(`The amount should be greater than '$0.00' to proceed further`);
                    return false;
                }

                let age = 0;
                const purchaseDate = moment(this.purchaseServiceForm.get('endDt').value);
                if (this.clientDob && moment(new Date(this.clientDob), 'MM/DD/YYYY', true).isValid()) {
                    const pDob = moment(new Date(this.clientDob), 'MM/DD/YYYY').toDate();
                    age = Number(this.clientAge.split('Yrs')[0]);
                    if(age == 0){
                        age = purchaseDate.diff(pDob, 'years');
                    }
                    if (this.purchaseServiceForm.get('fiscalCode').value === '2127' || this.purchaseServiceForm.get('fiscalCode').value === '7127') {
                        if (age <= 5) {
                            if (+this.purchaseServiceForm.get('costnottoexceed').value > 60) {
                                this._alertService.error(`The amount cannot exceed '$60.00' for ages 5 and less`);
                                return false;
                            }
                        } else if ( age > 5 && age <= 11) {
                            if (+this.purchaseServiceForm.get('costnottoexceed').value > 75) {
                                this._alertService.error(`The amount cannot exceed '$75.00' for ages is between 6 and 11`);
                                return false;
                            }
                        } else if ( age > 12) {
                            if (+this.purchaseServiceForm.get('costnottoexceed').value > 100) {
                                this._alertService.error(`The amount cannot exceed '$100.00' for ages 12 and more`);
                                return false;
                            }
                        } else {
                            if (+this.purchaseServiceForm.get('costnottoexceed').value > 60) {
                                this._alertService.error(`The amount cannot exceed '$60.00' for ages 5 and less`);
                                return false;
                            }
                        }
                    }
                    if (this.referredServiceCP.service_id === 11333) {
                        if (age <= 5) {
                            if (+this.purchaseServiceForm.get('costnottoexceed').value > 60) {
                                this._alertService.error(`The amount should be less than '$60.00' for Initial Clothing Allowance service`);
                                return false;
                            }
                        } else if ( age > 5 && age <= 11) {
                            if (+this.purchaseServiceForm.get('costnottoexceed').value > 75) {
                                this._alertService.error(`The amount should be less than '$75.00' for Initial Clothing Allowance service`);
                                return false;
                            }
                        } else {
                            if (+this.purchaseServiceForm.get('costnottoexceed').value > 100) {
                                this._alertService.error(`The amount should be less than '$100.00' for Initial Clothing Allowance service`);
                                return false;
                            }
                        }
                    } else if (this.referredServiceCP.service_id === 11334) {
                            if (age > 11) {
                                if (+this.purchaseServiceForm.get('costnottoexceed').value > 75) {
                                    this._alertService.error(`The amount should be greater than '$75.00' for (Monthly Clothing Allowance service`);
                                    return false;
                                }
                            } else {
                                if (+this.purchaseServiceForm.get('costnottoexceed').value > 60) {
                                    this._alertService.error(`The amount should be greater than '$60.00' for (Monthly Clothing Allowance service`);
                                    return false;
                                }
                            }
                    }
                    if (this.purchaseServiceForm.get('fiscalCode').value === '5113') {
                        if (age < 14 || age > 18) {
                        this._alertService.error('Invalid Fiscal Category Code due to Client age');
                        return false;
                        }
                    } else if (this.purchaseServiceForm.get('fiscalCode').value === '5114' ||
                               this.purchaseServiceForm.get('fiscalCode').value === '5115') {
                        if (age < 18 || age > 21) {
                            this._alertService.error('Invalid Fiscal Category Code due to Client age');
                            return false;
                        }
                    } else if (this.purchaseServiceForm.get('fiscalCode').value === '5116') {
                        if (age < 18) {
                            this._alertService.error('Invalid Fiscal Category Code due to Client age');
                            return false;
                          }
                      } else if (this.purchaseServiceForm.get('fiscalCode').value === '5117') {
                        if (age < 18 || age > 21) {
                            this._alertService.error('Invalid Fiscal Category Code due to Client age');
                            return false;
                        }
                    } else if (this.purchaseServiceForm.get('fiscalCode').value === '5118') {
                        if (age < 16 || age > 21) {
                            this._alertService.error('Invalid Fiscal Category Code due to Client age');
                            return false;
                        }
                    } else if (this.purchaseServiceForm.get('fiscalCode').value === '7110' ||
                            this.purchaseServiceForm.get('fiscalCode').value === '2110') {
                        if (+this.purchaseServiceForm.value.costnottoexceed > 2000) {
                            this._alertService.error('Selected Fiscal Category will be not allowed if the amount is more than $2000.00');
                            return false;
                        }
                    }
                }
               // if() { this._alertService.error('Insufficicant Account balance, please raise at lower cost request')} else {
                    let method;
                   if (this.newAuthorization) {
                     method = 'purchaseAuthorize';
                     this.singlePurchaseAuthorization = null;
                   } else {
                     method = this.singlePurchaseAuthorization && this.singlePurchaseAuthorization.authorization_id ? 'updatePurchaseAuthorize' : 'purchaseAuthorize';
                     this.newAuthorization = false;
                   }
                   // method = this.singlePurchaseAuthorization && this.singlePurchaseAuthorization.authorization_id ? 'updatePurchaseAuthorize' : 'purchaseAuthorize';
                    const url = 'purchaseAuthorizations/' + method + '?access_token=' + this.token.id;
                    const purchaseForm = this.purchaseServiceForm.getRawValue();
                    const model = {
                        'fiscalCategoryCd': this.purchaseServiceForm.value.fiscalCode,
                        'voucherSw': this.purchaseServiceForm.value.voucherRequested,
                        'justificationTx': this.purchaseServiceForm.value.justificationCode,
                        'costNo': this.purchaseServiceForm.value.costnottoexceed ? this.purchaseServiceForm.value.costnottoexceed : '0.00',
                        'startDt': moment(this.purchaseServiceForm.value.startDt).format(),
                        'endDt': moment(this.purchaseServiceForm.value.endDt).format(),
                        // D-06581
                        'daNumber': this.daNumber,
                        'serviceLogId': this.referredService.service_log_id,
                        'authorizationId': (this.singlePurchaseAuthorization && this.singlePurchaseAuthorization.authorization_id ) ? this.singlePurchaseAuthorization.authorization_id : null,
                        'client_id': this.rcCjamsID,
                        'bmanualrouting': this.otherCase
                        // 'justificationTx': this.singlePurchaseAuthorization.justification_tx ? this.singlePurchaseAuthorization.justification_tx : ''
                    };

                    const promise = this.singlePurchaseAuthorization && this.singlePurchaseAuthorization.authorization_id ?
                                    this._commonHttpService.updateWithoutid(model, url) : this._commonHttpService.create(model, url);
                    promise.subscribe((response) => {
                        console.log('save response....', response.UserToken);
                        this.isExceed = response.UserToken.isexceed;
                        // (<any>$('#Edit-newreferredservice')).modal('hide');
                        this.getList();
                        // Start D-06715
                        if (this.isExceed === 3) {
                            (<any>$('#purchase-autherization')).modal('hide');
                            (<any>$('#purchase-autherization-list')).modal('show');
                            this._alertService.success('Purchase Authorization request saved successfully');
                            this.getList(1);
                            this.openpurchaseautherization(this.referredService);
                            this.enableSaveServiceLog = false;
                            this.resetForm = true;
                        } else if (this.isExceed === 2) {
                            this.referredService = this.referredServiceCP;
                            this._alertService.error(`Insufficient Client Account balance, please raise a request at lower cost`);
                            this.sendApprovalEnable = false;
                            this.resetForm = false;
                            this.openpurchaseautherization(this.referredServiceCP, 'Edit');
                        } else if (this.isExceed === 1) {
                            this.referredService = this.referredServiceCP;
                            this._alertService.error(`Account is not available, kindly please add account for the selected client`);
                            this.sendApprovalEnable = false;
                            this.resetForm = false;
                            this.openpurchaseautherization(this.referredServiceCP, 'Edit');
                        } else if (this.isExceed === 4) {
                            this.referredService = this.referredServiceCP;
                            this._alertService.error(`Requested purchase authorization is already available on the selected date`);
                            this.sendApprovalEnable = false;
                            this.resetForm = false;
                            this.openpurchaseautherization(this.referredServiceCP, 'Edit');
                        } else if (this.isExceed === 5) {
                            this.referredService = this.referredServiceCP;
                            this._alertService.error(`Finanl Disbursement is inprogress for the selected client`);
                            this.sendApprovalEnable = false;
                            this.resetForm = false;
                            this.openpurchaseautherization(this.referredServiceCP, 'Edit');
                        }
                        // End D-06715
                    },
                        (error) => {
                            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                        });
               // }
            }
    }
    private sendForApproval(purchaseServiceFormData) {
        const purchaseServiceForm = this.purchaseServiceForm.getRawValue();
        if (this.isSupervisor) {
            if (purchaseServiceForm.costnottoexceed >= 1000) {
                this.getRoutingUser();
                this.status = 42;
            } else {
                this.status = 40;
                this.getRoutingUser();
            }
        } else {
           // if (this.enableSaveServiceLog) {
                this.sendApprovalEnable = true;
                this.savePurchaseAuth(purchaseServiceForm);
           // } else {
                /* this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ServiceLogApproval;
                // if (this.purchaseServiceForm.valid) {
                    const modal = {
                        'case_id': this.daNumber,
                        'fiscalcategorycd' : this.singlePurchaseAuthorization.fiscal_category_cd,
                        'costno' : this.singlePurchaseAuthorization.cost_no ? this.singlePurchaseAuthorization.cost_no : '0.00',
                        'authorization_id' : this.authId,
                        'provider_id' : this.referredService.provider_id,
                        'intakeserviceid' : this.id,
                        'assignedtoid' : '',
                        'eventcode' : 'PCAUTH',
                        'status' : 39,
                        'startDt' : this.singlePurchaseAuthorization.startdt,
                        'endDt' : this.singlePurchaseAuthorization.enddt,
                        'client_account_id': this.singlePurchaseAuthorization.client_account_id ? this.singlePurchaseAuthorization.client_account_id : 0,
                        'bmanualrouting': this.otherCase
                        // 'paymentstatus' : this.singlePurchaseAuthorization.paymentstatus
                    };
                this._commonHttpService.create(modal).subscribe(
                    (response) => {
                        // console.log(response);
                        this._alertService.success('Approval sent successfully!');
                        (<any>$('#purchase-autherization')).modal('hide');
                        (<any>$('#purchase-autherization-list')).modal('show');
                        this.sendApprovalEnable = false;
                        this.getList(1);
                        this.openpurchaseautherization(this.referredService, 'Edit');
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                        this.sendApprovalEnable = false;
                    }
                );
            } */
    }

    }

    private assignUser() {
        this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ServiceLogApproval;
            const modal = {
                'case_id': this.daNumber,
                'bmanualrouting': this.otherCase,
                'fiscalcategorycd' : this.singlePurchaseAuthorization.fiscal_category_cd,
                'costno' : this.singlePurchaseAuthorization.cost_no ? this.singlePurchaseAuthorization.cost_no : '0.00',
                'authorization_id' : this.singlePurchaseAuthorization.authorization_id,
                'provider_id' : this.referredService.provider_id,
                'intakeserviceid' : this.id,
                'assignedtoid' : this.assignedTo ? this.assignedTo : null,
                'eventcode' : this.eventCode ? this.eventCode : 'PCAUTH',
                'status' : this.status,
                'startDt' : this.singlePurchaseAuthorization.startdt,
                'endDt' : this.singlePurchaseAuthorization.enddt,
                'client_account_id': this.singlePurchaseAuthorization.client_account_id ? this.singlePurchaseAuthorization.client_account_id : 0,
                'roletypekey': this.roletypekey ? this.roletypekey : null
                // 'paymentstatus' : this.singlePurchaseAuthorization.paymentstatus
            };
        this._commonHttpService.create(modal).subscribe(
            (response) => {
                // console.log(response);
                this._alertService.success('Approval sent successfully!');
                (<any>$('#payment-approval')).modal('hide');
                (<any>$('#purchase-autherization-list')).modal('show');
                this.openpurchaseautherization(this.referredService);
                this.getList(1);
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );

      }

      documentGenerate(authorization_id, status) {
          if (status === 'Approved') {
            this.isapproved = true;
          } else {
              this.isapproved = false;
          }
          const modal = {
            count: -1,
            where: {
                documenttemplatekey: ['purchaseauthorization'],
                authorizationid: authorization_id,
                isapproved: this.isapproved
            },
            method: 'post'
          };
          this._commonHttpService.download('evaluationdocument/generateintakedocument', modal)
        .subscribe(res => {
          const blob = new Blob([new Uint8Array(res)]);
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          const timestamp = moment(this.timestamp).format('MM/DD/YYYY HH:mm:ss');
          link.download = 'Purchase-authorization-Form-' + authorization_id + '.' + timestamp + '.pdf';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        });
      }

      selectPerson(row) {
        if (row) {
          this.selectedPerson = row;
          this.assignedTo = row.userid;
        }
      }

      getRoutingUser() {
        (<any>$('#purchase-autherization')).modal('hide');
        (<any>$('#payment-approval')).modal('show');
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'PCAUTH' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.getUsersList = result.data;
                console.log('this.getUsersList...', this.getUsersList);
                this.originalUserList = this.getUsersList;

                this.listUser('TOBEASSIGNED');
            });
     }

     enableUserRole(type) {
         if (type === 'user') {
            this.roleBased = false;
            this.eventCode = 'PCAUTH';
            this.roletypekey = null;
         } else {
            this.roleBased = true;
            this.assignedTo = null;
            this.eventCode = 'PCAUTHR';
            this.roletypekey = 'FNS' + this.role;
         }
     }

     listUser(assigned: string) {
      this.userRole = 'role';
      this.selectedPerson = '';
      this.getUsersList = [];
      this.getUsersList = this.originalUserList;
        if (assigned === 'TOBEASSIGNED') {
          this.getUsersList = this.getUsersList.filter((res) => {
            if (+this.singlePurchaseAuthorization.cost_no >= 1000) {
                let isDF = res.resourcename.includes('manage_over1000_approval');
                if (res.rolecode === 'DF' || isDF === true)  {
                    this.role = 'DF';
                    this.rolename = 'LDSS Directors';
                    return res;
                }
            } else {
                let isFS= res.resourcename.includes('manage_payment_approval');
                if (res.rolecode === 'FS' && isFS === true ){
                    this.role = res.rolecode;
                    this.rolename = 'Fiscal Supervisors';
                    return res;
                }
            }
        });
        } else {
            this.getUsersList = this.getUsersList.filter((res) => {
                let isFW= res.resourcename.includes('manage_funding_approval');
                    if (res.rolecode === 'FW' && isFW === true ){
                        this.role = res.rolecode;
                        this.rolename = 'Fiscal Workers';
                        return res;
                    }
            });
        }
        this.enableUserRole('role');
      }

      confirmReject() {
          this.reason_tx = '';
      }
      confirmReturn() {
          this.return_tx = '';
      }

      rejectApproval() {
          this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.PurchaseAuthorizationReject + this.authId;
          const modal = {
            'case_id': this.daNumber,
              reason_tx : this.reason_tx
          };
          this._commonHttpService.create(modal).subscribe(
              (response) => {
                this._alertService.success('Purchase Authorization Denied successfully');
                (<any>$('#reject-approval')).modal('hide');
                (<any>$('#purchase-autherization-list')).modal('show');
                this.getList(1);
                this.openpurchaseautherization(this.referredService);
              },
              (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
              });
      }
      returnApproval() {
        this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.PurchaseAuthorizationReturn + this.authId;
        const modal = {
          'case_id': this.daNumber,
            return_tx : this.return_tx
        };
        this._commonHttpService.create(modal).subscribe(
            (response) => {
              if (response) {
                this._alertService.success('Purchase Authorization Returned to the worker successfully');
                (<any>$('#return-approval')).modal('hide');
                (<any>$('#purchase-autherization-list')).modal('show');
                this.getList(1);
                this.openpurchaseautherization(this.referredService);
              } else {
                this._alertService.success('Purchase Authorization Returned to the worker successfully');
                (<any>$('#return-approval')).modal('hide');
                (<any>$('#purchase-autherization-list')).modal('show');
                this.getList(1);
                this.openpurchaseautherization(this.referredService);
              }
            },
            (error) => {
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            });
    }

    getClientDropdown() {
        var data = [];
        this._commonHttpService.getArrayList(
            {
            where: {intakeserviceid: this.id, person_id: this.personid},
            order: 'agency_program_nm',
            method: 'get'
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientProgramName + '?filter',

        ).subscribe(response => {
            // By Default Area program assignment has been mad disabled
            if (response && response.length) {
            //    if(this.referredService) {
            //        data = response.filter((item) => this.referredService && item.agency_program_area_id === this.referredService.agency_program_area_id && !item.enddate );
            //        this.clientProgramNames = data;
            //     } else {
            //        data = response.filter((item) =>  !item.enddate );
            //        this.clientProgramNames = data;
            //     }
            //     if(data && data.length){
            //         this.startdate = data[0].startdate;
            //         this.enddate = data[0].enddate;
            //         this.clientprogramselection = data[0].agency_program_area_id ? data[0].agency_program_area_id : null;
            //         this.getFiscalCategoryCode(this.clientprogramselection);
            //     }
                this.clientProgramNames = response;
                this.addNewReferredServiceSecondForm.patchValue({
                    clientprogramnameid: response[0].agency_program_area_id ? response[0].agency_program_area_id : null
                });
                this.startdate = response[0].startdate;
                this.enddate = response[0].enddate;
                this.estimatedenddate = response[0].enddate;
                this.clientprogramselection = response[0].agency_program_area_id ? response[0].agency_program_area_id : null;
                if (this.referredService) {
                    this.startdate = this.referredService.estbegindate ? new Date(this.referredService.estbegindate) : response[0].startdate;
                    this.estimatedenddate = this.referredService.estimated_end_date ? new Date(this.referredService.estimated_end_date) : response[0].enddate;
                }
                const currDate = new Date();
                if(this.enddate) {
                    if(new Date(this.enddate) < currDate) {
                        this.disbleService = true;
                        this._alertService.warn('Please add required Program Service for the selected client');
                    } else {
                        this.disbleService = false;
                    }
                } else {
                    this.disbleService = false;
                }
                this.getFiscalCategoryCode(this.clientprogramselection);
            } else {
                this.disbleService = true;
                this.clientprogramselection = null;
                this._alertService.warn('Please add required Program Service for the selected client');
            }
        });
    }

    changeClientProgram(clientProgramid) {
        const clientProgram = this.clientProgramNames.filter(clientid => clientid.agency_program_area_id === clientProgramid);
        this.startdate = (clientProgram && clientProgram[0]) ? clientProgram[0].startdate : null;
        this.enddate = (clientProgram && clientProgram[0]) ? clientProgram[0].enddate : null;
        this.estimatedenddate = (clientProgram && clientProgram[0]) ? clientProgram[0].enddate : null;
        this.clientprogramselection = clientProgramid ? clientProgramid : null;
        this.getFiscalCategoryCode(this.clientprogramselection);
    }

    onPurchaseAmount(amount) {
        if (!_.isNaN(_.toNumber(amount))) {
            this.purchaseServiceForm.patchValue ({
                costnottoexceed : _.toNumber(amount).toFixed(2)
            });
        }
    }

    checkDec(el) {
        if (el.target.value !== '') {
          return el.target.value = isNaN(el.target.value) ? '' : el.target.value.replace(/[^0-9\/\\]/g, '');
        }
        return '';
      }

    getFiscalCategoryCode(vendorprogramid) {
        if (!vendorprogramid) {
            return;
        }
        this._commonHttpService.getArrayList({
                where: {agencyprogramareaid: vendorprogramid},
                method: 'get'
              }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.fiscalCodes + '?filter',
            ).subscribe( result => {
                this.fiscalCodes = result;
                // THIS RULE APPLICABLE FOR IV-E - Should not block service-log, Filter needs to be revisited
                // this.fiscalCodes = this.fiscalCodes.filter(item => item.eligibility_cd === this.clientEligibilityStatus);
                if (this.referredService && this.referredService.service_id) {
                    if (this.clientEligibilityStatus === '2913') {
                       // this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                    } else {
                        this.fiscalCodes = this.fiscalCodes.filter(item => this.startsWith(item));
                    }
                    if (this.referredService.service_id === 11333) {
                       // if (this.clientEligibilityStatus === '3951') {
                        if (this.clientEligibilityStatus === '2913') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7126');
                        }
                   } else if (this.referredService.service_id === 11334) {
                        // if (this.clientEligibilityStatus === '3951') {
                            if (this.clientEligibilityStatus === '2913') {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2127');
                        } else {
                            this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '7127');
                        }
                   } else {
                       if (this.clientEligibilityStatus === '3951') {
                        this.fiscalCodes = this.fiscalCodes.filter(item => item.fiscalcateforycd === '2126');
                    }
                   }
                }
            });
    }

    private VendorDropDown() {
       this.getClientDropdown();
        const source = forkJoin([
             this._commonHttpService.getArrayList({
                order: 'service_nm'
              },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.VendorServices ,

            ),
            this._commonHttpService.getArrayList({
                order: 'value_tx'
              }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Frequency),
              this._commonHttpService.getArrayList({
                order: 'sort_order_no'
              }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Duration),
            this._commonHttpService.getArrayList(
                {
                    order: 'service_end_reason'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ServiceEndReason + '?access_token=' + this.token.id,
            ),
            this._commonHttpService.getArrayList(
                {
                    order: 'reason_service_not_received'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ReasonServiceNotReceived + '?access_token=' + this.token.id,
            ),
        ])
            .map((result) => {
                return {
                    servicetypeid: result[0]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: res.service_nm,
                                value: res.service_id,
                                additionalProperty: res.service_type
                            })
                    ),
                    frequencyCdId: result[1]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.value_tx),
                                value: (res.picklist_value_cd),
                            })
                    ),
                    durationCdId: result[2]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.value_tx),
                                value: (res.picklist_value_cd),
                            })
                    ),
                    serviceEndReason: result[3]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.service_end_reason),
                                value: (res.service_end_reason_cd),
                            })
                    ),
                    ReasonServiceNotReceived: result[4]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.reason_service_not_received),
                                value: (res.service_not_received_cd),
                            })
                    )
                };
            })
            .share();

        this.vendorServices$ = source.pluck('servicetypeid');
        this.frequencyCds$ = source.pluck('frequencyCdId');
        this.durationCds$ = source.pluck('durationCdId');
        this.serviceEndReason$ = source.pluck('serviceEndReason');
        this.reasonServiceNotReceived$ = source.pluck('ReasonServiceNotReceived');
    }

    selectedProv(searchPlan) {
        if (searchPlan.service) {
            this.selectedServicePlan = true;
        }
        this.searchPlan = searchPlan;
        console.log('serachplan...', searchPlan);
    }

    pleaseSelectReferredService() {
        this._alertService.error('Please select a service');
    }

    getInvolvedPerson() {
        this.childList = [];
        let personDetail = {};
        if (this.isServiceCase === 'true') {
            this.serviceCase = true;
            personDetail = {
                objectid: this.id,
                objecttypekey: 'servicecase'
            };
        } else {
            this.serviceCase = false;
            personDetail = {
                intakeserviceid: this.id
            };
        }
        this._commonHttpService
            .getPagedArrayList( new PaginationRequest({
                method: 'get',
                page: 1, limit : 20,
                where: personDetail
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
            )
           /* .map((res) => {
                console.log('res', res);
                return res['data'].filter((item) => item.intakeservicerequestpersontypekey === 'CHILD');
            })*/.subscribe(response => {
                console.log('response...', response);
                if (response && response.data.length) {
                    for (let i = 0; i < response.data.length; i++) {
                        if (response.data[i].roles) {
                            if (this.isSupervisor && this.purchaseAuthorization === response.data[i].cjamspid) {
                                    this.childList.push(response.data[i]);
                                    this.firstChild = response.data[i];
                                    this.rcCjamsID = response.data[i].cjamspid;
                                    this.clientDob = response.data[i].dob;
                                    this.clientGender = response.data[i].gender;
                                    this.clientAge = response.data[i].age;
                                    this.clientName = response.data[i].firstname + ' ' + response.data[i].lastname;
                                    this.personid = response.data[i].personid;
                            } else if (!this.isSupervisor) {
                                this.childList.push(response.data[i]);
                                this.firstChild = response.data[i];
                                this.rcCjamsID = response.data[i].cjamspid;
                                this.clientDob = response.data[i].dob;
                                this.clientGender = response.data[i].gender;
                                this.clientAge = response.data[i].age;
                                this.clientName = response.data[i].firstname + ' ' + response.data[i].lastname;
                                this.personid = response.data[i].personid;
                            } else if (this.isSupervisor && this.purchaseAuthorization === 'null') {
                                this.childList.push(response.data[i]);
                                this.firstChild = response.data[i];
                                this.rcCjamsID = response.data[i].cjamspid;
                                this.clientDob = response.data[i].dob;
                                this.clientGender = response.data[i].gender;
                                this.clientAge = response.data[i].age;
                                this.clientName = response.data[i].firstname + ' ' + response.data[i].lastname;
                                this.personid = response.data[i].personid;
                            }
                           // for ( let j = 0; j < response.data[i].roles.length; j++) {
                               // if (response.data[i].roles[j].intakeservicerequestpersontypekey === 'CHILD') {
                                   // this.rcCjamsID = response.data[i].cjamspid ? response.data[i].cjamspid : null;

                               // }
                           // }
                        }
                    }
                    this.getClientEligibility(this.rcCjamsID, this.daNumber);
                    this.getList();
                    this.VendorDropDown();
                }
            });
    }
    getFullName(person) {
        const nameKeys = [ 'prefx' , 'firstname' , 'middlename' , 'lastname' , 'suffix'];
        let name = '';
        nameKeys.forEach(key => {
        //console.log( key , person[key] );
      if(person && person.hasOwnProperty(key)){
        if ( !(person[key] == null) && !(person[key] == 'null') && !(person[key] == '') ) {
          name = name + person[key] + ' ';
          //console.log(name);
        }}
        });
        //console.log("NAME",name);
        return name;
    }

    getClientEligibility(client_id, caseNumber) {
        this._commonHttpService.getArrayList({
            where: {
                client_id: client_id,
                case_id: caseNumber,
            },
            method: 'get'
        }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientEligibility).subscribe((response) => {
            this.clientEligibility = response['data'];
            this.clientEligibilityStatus = this.clientEligibility[0].eligibility_status_cd;
        });
    }

    childSelect(clientdetail) {
        this.clientName = clientdetail.firstname + ' ' + clientdetail.lastname;
        this.rcCjamsID = clientdetail.cjamspid;
        this.clientDob = clientdetail.dob;
        this. clientGender = clientdetail.gender;
        this.clientAge = clientdetail.age;
        this.personid = clientdetail.personid;
        this.getClientEligibility(this.rcCjamsID, this.daNumber);
        this.getList();
        this.VendorDropDown();
    }

    getActivityService() {
        this._commonHttpService.getPagedArrayList(
            new PaginationRequest({
              where: {personid: this.personid},
              limit : 10,
              page: 1,
              method: 'get'
            }), 'serviceLogs/getServicePlanActionList' + '?filter'
          ).subscribe((result: any) => {
            this.activityService = result.data;
          //  this.activityServicePlan = result.data.serviceplanname;
          });
    }

    onActivityService(serviceplanactionid, form) {
        if (form === 'add') {
            this.addNewReferredServiceSecondForm.get('serviceplanid').reset();
        } else {
            this.EditreferredServiceForm.get('serviceplanid').reset();
        }
        this.getServicePlan(serviceplanactionid);
    }

    getServicePlan(serviceplanactionid) {
        this._commonHttpService.getPagedArrayList(
            new PaginationRequest({
              where: {serviceplanactionid: serviceplanactionid},
              limit : 10,
              page: 1,
              method: 'get'
            }), 'serviceLogs/getServicePlanList' + '?filter'
          ).subscribe((result: any) => {
            this.servicePlan = result.data;
            if  (this.servicePlan && this.servicePlan.length) {
                this.addNewReferredServiceSecondForm.patchValue({
                    serviceplanid: this.servicePlan[0].serviceplanid,
                    serviceplanname: this.servicePlan[0].serviceplanname
                });
                this.addNewReferredServiceSecondForm.get('serviceplanname').disable();
            }
          });
    }

    saveReferredService() {
        this.servicetypeid = '';
        if (this.addNewReferredServiceSecondForm.valid) {
            this._dataStoreService.currentStore.subscribe((store) => {
                if (store['dsdsActionsSummary']) {
                    this.headerSummary = store['dsdsActionsSummary'];
                }
            });
            const referredForm = this.addNewReferredServiceSecondForm.getRawValue();
            const case_id = this.headerSummary.da_number;
            const url = 'serviceLogs/vendorSaveServiceLog';
            const model = {
                'providerServiceId': this.searchPlan.provider_service_id,
                'startDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actbegindate, 'yyyy-MM-dd'),
                'endDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actenddate, 'yyyy-MM-dd'),
                'descriptionTx': this.addNewReferredServiceSecondForm.value.agencynotes,
                // 'startTm': '9:10PM',
                // 'endTm': '9:10PM',
                'noServiceReasonCd': 'abcd',
                'estimatedStartDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.estbegindate, 'yyyy-MM-dd'),
                'estimatedEndDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.estenddate, 'yyyy-MM-dd'),
                'frequencyCd': this.addNewReferredServiceSecondForm.value.frequencyCdId,
                'durationCd': this.addNewReferredServiceSecondForm.value.durationCdId,
                'courtOrderedSw': this.addNewReferredServiceSecondForm.value.courtOrderedSw ? 'Y' : 'N',
                'agencyProgramAreaId': referredForm.clientprogramnameid,
                'referredDt': this.addNewReferredServiceSecondForm.value.dateReffered,
                'serviceLogId': this.addNewReferredServiceSecondForm ? this.addNewReferredServiceSecondForm.value.service_log_id : null,
                'case_id': case_id,
                // D-06581
                'daNumber': this.daNumber,
                'client_id': this.rcCjamsID ? this.rcCjamsID : null,
                serviceplanactionid: this.addNewReferredServiceSecondForm ? this.addNewReferredServiceSecondForm.value.serviceplanactionid : null,
                serviceplanname: referredForm ? referredForm.serviceplanname : null,
                serviceplanid: referredForm ? referredForm.serviceplanid : null,
                endServiceReasonCd : referredForm ? referredForm.endServiceReasonCd : null,
                outcome: referredForm ? referredForm.outcome : null,
                servicereceived: referredForm.servicereceived ? referredForm.servicereceived : null,
                servicenotreceived: referredForm.servicenotreceived ? referredForm.servicenotreceived : null,
            };
            this._commonHttpService.create(model, url).subscribe((response) => {
                this._alertService.success('Service Added successfully');
                (<any>$('#Edit-newreferredservice')).modal('hide');
                this.getList();
                this.resetServiceDetail();
                if (response) {
                    (<any>$('#add-newreferredservice')).modal('hide');
                    (<any>$('#select-referredservice')).modal('hide');
                }
            },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });

            (<any>$('#add-newreferredservice')).modal('hide');
            (<any>$('#select-referredservice')).modal('hide');

        }
        // Start D-06585
        else {
            this._alertService.warn('Please fill mandatory fields');
        }
        // End D-06585
    }

    getApproveHistory(authid) {
        this.payableApprovalHistory = [];
        const source = this._commonHttpService.getPagedArrayList(
          new PaginationRequest({
            where: {authorization_id: authid},
            limit : this.pageInfo.pageSize,
            page: this.pageInfo.pageNumber,
            method: 'get'
          }), FinanceUrlConfig.EndPoint.accountsPayable.listPayableApprovalHistory + '?filter'
        ).subscribe((result: any) => {
          this.payableApprovalHistory = result;
          this.totalPage = (this.payableApprovalHistory && this.payableApprovalHistory.length > 0) ? this.payableApprovalHistory.length : 0;
        });
      }


    edit(referredService) {
        this.addNewService = false;
        this.getActivityService();
        this.referredService = referredService;
        this.getClientDropdown();
        this.getServicePlan(referredService.serviceplanactionid);
        this.startdate = new Date(referredService.estimated_start_date);
        const model = {
            clientprogramnameid: referredService.agency_program_area_id,
            servicetypeid: referredService.service_id,
            frequencyCdId: referredService.frequency_cd,
            durationCdId: referredService.duration_cd,
            estbegindate: new Date(referredService.estimated_start_date),
            actbegindate: referredService.actual_start_date ? new Date(referredService.actual_start_date) : null,
            estenddate: new Date(referredService.estimated_end_date),
            actenddate: referredService.actual_end_date ? new Date(referredService.actual_end_date) : null,
            actbegintime: referredService.start_time,
            actendtime: referredService.end_time,
            agencynotes: referredService.notes,
            outcome: referredService.outcome,
            noServiceReasonCd: '',
            serviceReceivedReason: referredService.serviceReceivedReason ? referredService.serviceReceivedReason : null,
           // providerid: referredService.provider_id,
           // providername: referredService.provider_nm,
           // taxid: referredService.tax_id,
           // zipcode: referredService.zip,
            dateReffered: new Date(referredService.referred_date),
            courtOrderedSw: (referredService.courtorder === 'Y') ? true : false,
            serviceplanactionid: referredService.serviceplanactionid ? referredService.serviceplanactionid : null,
            serviceplanname: referredService.serviceplanname ? referredService.serviceplanname : null,
            serviceplanid: referredService.serviceplanid ? referredService.serviceplanid : null,
            servicenotreceived: referredService.servicenotreceived ? referredService.servicenotreceived : null,
            servicereceived: referredService.servicereceived ? referredService.servicereceived : null,
            endServiceReasonCd: referredService.service_end_reason_cd ? (referredService.service_end_reason_cd).trim() : null
        };
        /* this.addNewReferredServiceSecondForm.get('serviceplanname').disable();
        this.addNewReferredServiceSecondForm.get('clientprogramnameid').disable(); */
        this.addNewReferredServiceSecondForm.setValue(model, { emitEvent: true, onlySelf: false });
        if (!referredService.no_service_reason) {
            this.serviceNotReceived = false;
            this.addNewReferredServiceSecondForm.get('noServiceReasonCd').disable();
        } else {
            this.serviceNotReceived = true;
            this.addNewReferredServiceSecondForm.patchValue({
                servicenotreceived: true,
                noServiceReasonCd: referredService.no_service_reason_cd
            });
        }

        if (referredService.authflag && referredService.actual_end_date) {
            this.addNewReferredServiceSecondForm.disable();
        } else if (referredService.authflag && (!(referredService.actual_end_date && referredService.ongoingtransflag))) {
            this.addNewReferredServiceSecondForm.disable();
            this.addNewReferredServiceSecondForm.get('actenddate').enable();
            this.addNewReferredServiceSecondForm.get('endServiceReasonCd').disable();
            this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].clearValidators();
            this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].updateValueAndValidity();
        } else {
            this.addNewReferredServiceSecondForm.enable();
            if (!referredService.actual_end_date) {
                this.addNewReferredServiceSecondForm.get('endServiceReasonCd').disable();
                this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].clearValidators();
                this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].updateValueAndValidity();
            }
            if (referredService.actual_start_date) {
                this.serviceReceived = true;
                this.addNewReferredServiceSecondForm.patchValue({
                    servicereceived: true
                });
            } else {
                this.serviceReceived = false;
                this.addNewReferredServiceSecondForm.patchValue({
                    servicereceived: false
                });
                this.addNewReferredServiceSecondForm.get('endServiceReasonCd').disable();
                this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].clearValidators();
                this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].updateValueAndValidity();
                this.addNewReferredServiceSecondForm.get('actbegindate').disable();
                this.addNewReferredServiceSecondForm.controls['actbegindate'].clearValidators();
                this.addNewReferredServiceSecondForm.controls['actbegindate'].updateValueAndValidity();
                this.addNewReferredServiceSecondForm.get('actenddate').disable();
            }
        }
        this.minEstDate = new Date(this.addNewReferredServiceSecondForm.value.estbegindate);
        this.minActDate = new Date(this.addNewReferredServiceSecondForm.value.actbegindate);
    }

    updateReferredService() {
        if (this.addNewReferredServiceSecondForm.valid) {
            const referredForm = this.addNewReferredServiceSecondForm.getRawValue();
            const url = 'serviceLogs/editVendorServiceLog';// + '?access_token=' + this.token.id;
            const model = {
                'providerServiceId': this.referredService.provider_service_id,
                'courtOrderedSw': referredForm.courtOrderedSw ? 'Y' : 'N',
                'endServiceReasonCd': referredForm.endServiceReasonCd, // this.EditreferredServiceForm.value.serviceNotReceivedReason,
                // D-06581
                'daNumber': this.daNumber,
                'client_id': this.rcCjamsID,
                // 'case_id': this.daNumber,
                'startDt': this.datePipe.transform(referredForm.actbegindate, 'yyyy-MM-dd'),
                'endDt': this.datePipe.transform(referredForm.actenddate, 'yyyy-MM-dd'),
                'descriptionTx': referredForm.agencynotes,
                'outcome': referredForm.outcome,
                // 'startTm': this.datePipe.transform(referredForm.actbegintime, 'hh-mm aa'),
                // 'endTm': this.datePipe.transform(referredForm.actendtime, 'hh-mm aa'),
                'estimatedStartDt': this.datePipe.transform(referredForm.estbegindate, 'yyyy-MM-dd'),
                'estimatedEndDt': this.datePipe.transform(referredForm.estenddate, 'yyyy-MM-dd'),
                'frequencyCd': referredForm.frequencyCdId,
                'durationCd': referredForm.durationCdId,
                'agencyProgramAreaId': referredForm.clientprogramnameid,
                'serviceLogId': this.referredService.service_log_id,
                'referredDt': this.datePipe.transform(referredForm.dateReffered, 'yyyy-MM-dd'),
                noServiceReasonCd: referredForm.noServiceReasonCd ? referredForm.noServiceReasonCd : null
            };
            //This PUT remote method does not work on model entities so does not need an Id
            //It is actually calling a stored proc so removing the id and passing request payload
            this._commonHttpService.updateWithoutid(model, url).subscribe((response) => {
                this._alertService.success('Service Updated successfully');
                this.getList();
                this.resetServiceDetail();
                (<any>$('#select-referredservice')).modal('hide');
                if (response) {
                }
            },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        } else {
            this._alertService.error('Please fill the mandatory fields');
        }
    }

    changeServiceReceived(checked) {
        if (!checked) {
            this.serviceReceived = false;
            this.addNewReferredServiceSecondForm.controls['actenddate'].reset();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].reset();
            this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].reset();
            this.addNewReferredServiceSecondForm.controls['actenddate'].disable();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].disable();
            this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].disable();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].clearValidators();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].updateValueAndValidity();
        } else {
            this.serviceReceived = true;
            this.serviceNotReceived = false;
            this.addNewReferredServiceSecondForm.get('servicenotreceived').reset();
            this.addNewReferredServiceSecondForm.controls['noServiceReasonCd'].disable();
            this.addNewReferredServiceSecondForm.controls['noServiceReasonCd'].reset();
            this.addNewReferredServiceSecondForm.controls['actenddate'].enable();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].enable();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].setValidators(Validators.required);
            this.addNewReferredServiceSecondForm.controls['actbegindate'].updateValueAndValidity();
        }
    }

    onChangeActualEndDate(event: MatDatepickerInputEvent<Date>) {
        this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].enable();
        this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].setValidators(Validators.required);
        this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].updateValueAndValidity();
        const endDate = new Date(this.addNewReferredServiceSecondForm.get('actenddate').value);
       /* if ( endDate < this.currentDate) {
            this.purchaseAuthEnable = false;
        } else {
            this.purchaseAuthEnable = true;
        } */
      }

    changeServiceNotReceived(checked) {
        if (!checked) {
            this.serviceNotReceived = false;
            this.addNewReferredServiceSecondForm.controls['noServiceReasonCd'].reset();
            this.addNewReferredServiceSecondForm.controls['noServiceReasonCd'].disable();
        } else {
            this.serviceReceived = false;
            this.serviceNotReceived = true;
            this.addNewReferredServiceSecondForm.get('servicereceived').reset();
            this.addNewReferredServiceSecondForm.controls['noServiceReasonCd'].setValidators(Validators.required);
            this.addNewReferredServiceSecondForm.controls['noServiceReasonCd'].updateValueAndValidity();
            this.addNewReferredServiceSecondForm.controls['noServiceReasonCd'].enable();
            this.addNewReferredServiceSecondForm.controls['actenddate'].reset();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].reset();
            this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].reset();
            this.addNewReferredServiceSecondForm.controls['actenddate'].disable();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].disable();
            this.addNewReferredServiceSecondForm.controls['endServiceReasonCd'].disable();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].clearValidators();
            this.addNewReferredServiceSecondForm.controls['actbegindate'].updateValueAndValidity();
        }
    }

    delete(referredService) {
        this.referredService = referredService;
    }

    closeNoDelete() {
        (<any>$('#Delete-newreferredservice')).modal('hide');
    }

    deleteItem() {
        this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServiceLog;
        this._commonHttpService.update('',
        {
            service_log_id: this.referredService.service_log_id,
            case_id: this.daNumber,
            client_id: this.rcCjamsID
        }).subscribe(
            (response) => {
                this._alertService.success('Service has been deleted successfully');
                (<any>$('#Delete-newreferredservice')).modal('hide');
                this.getList(1);
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );

       /* this._commonHttpService.(this.referredService.service_log_id, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServiceLog).subscribe(
            res => {
                this._alertService.success('Service deleted successfully');
                (<any>$('#Delete-newreferredservice')).modal('hide');
                this.getList(1);
            },
            err => { }
        );*/
    }

    printpdf(): void {
        let printContents, popupWin;
        printContents = document.getElementById('serviceForm').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }


    // pdfFiles:any;
    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {

            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
            });
        }
        const pageName = 'pageName';
        this.pdfFiles.push({ fileName: pageName, images: pageImages });
        pageImages = [];
        this.convertImageToPdf();
    }

    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        this.pdfFiles = [];
        // this.downloadInProgress = false;
    }

    onChangeDate(form, field) {
        console.log(field);
        if ( field === 'estbegindate' ) {
            this.minEstDate = new Date(form.value.estbegindate);
            form.get('estenddate').reset();
        } else if ( field === 'actbegindate') {
            this.minActDate = new Date(form.value.actbegindate);
            form.get('actenddate').reset();
        }
    }

    viewChildAccount(clientid, accno) {
        (<any>$('#purchase-autherization')).modal('hide');
        this._commonHttpService.getPagedArrayList(new PaginationRequest({
          where: {client_id: clientid },
          nolimit: true,
          method: 'get'
        }), FinanceUrlConfig.EndPoint.childAccounts.getchildaccountslistUrl + '?filter').subscribe(result => {
          this.childAccountsList = result.data;
          if (accno && this.childAccountsList && this.childAccountsList.length > 0) {
            this.childAccountsList = this.childAccountsList.filter(data => data.account_no_tx == accno);
          }
          (<any>$('#view-child-account')).modal('show');
        });
      }

      showPrevious(id) {
        (<any>$(`#${id}`)).modal('show');
      }

      resetServiceDetail() {
          this.addNewReferredServiceSecondForm.reset();
          this.addNewReferredServiceSecondForm.enable();
          // this.estimatedenddate = new Date();
          // this.enddate = new Date();
      }

      startsWith = function (item) {
        if (item.fiscalcateforycd.indexOf('21') !== 0) {
            return item;
        }
    }
}
