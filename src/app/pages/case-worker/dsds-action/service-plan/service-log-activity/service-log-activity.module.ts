import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule
} from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxMaskModule } from 'ngx-mask';
import { NgSelectModule } from '@ng-select/ng-select';

import { CommonHttpService } from '../../../../../@core/services';
import { ServiceLogActivityRoutingModule } from './service-log-activity-routing.module';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../../../../../environments/environment';
import { AgencyProvidedServicesComponent } from '../service-log-activity/agency-provided-services/agency-provided-services.component';
import { ReferredServicesComponent } from '../service-log-activity/referred-services/referred-services.component';
import { ServiceLogActivityComponent } from './service-log-activity.component';
import { SharedDirectivesModule } from '../../../../../@core/directives/shared-directives.module';

@NgModule({
    imports: [
        CommonModule,
        ServiceLogActivityRoutingModule,
        MatTabsModule,
        MatSelectModule,
        MatTableModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        FormsModule,
        PaginationModule,
        A2Edatetimepicker,
        NgxMaskModule.forRoot(),
        NgSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatRadioModule,
        MatTabsModule,
        MatCheckboxModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        // AgmCoreModule
        AgmCoreModule,
        SharedDirectivesModule
    ],
    declarations: [
        AgencyProvidedServicesComponent,
        ReferredServicesComponent,
        ServiceLogActivityComponent
    ],
    providers: [CommonHttpService]
})
export class ServiceLogActivityModule {}
