import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { ActivatedRoute } from '@angular/router';
import { AppUser } from '../../../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, SessionStorageService, DataStoreService } from '../../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { AgencyCategories, AgencyServices } from '../../../../_entities/caseworker.data.model';
import value from '*.json';
import { DatePipe } from '@angular/common';
import { FormArray, Validators } from '@angular/forms';
import * as moment from 'moment';
import { CASE_STORE_CONSTANTS } from '../../../../_entities/caseworker.data.constants';
@Component({
  selector: 'agency-provided-services',
  templateUrl: './agency-provided-services.component.html',
  styleUrls: ['./agency-provided-services.component.scss'],
  providers: [DatePipe]
})
export class AgencyProvidedServicesComponent implements OnInit {

  agencyServiceList$: Observable<any[]>;
  totalRecord$: Observable<number>;
  paginationInfo: PaginationInfo = new PaginationInfo();
  agencyService: any;
  id: string;
  // D-06581
  daNumber: string;
  addNewProviderForm: FormGroup;
  viewProviderForm: FormGroup;
  clientProgramNames$: Observable<DropdownModel[]>;
  agencyServiceNames$: Observable<DropdownModel[]>;

  frequencyCds$: Observable<DropdownModel[]>;
  durationCds$: Observable<DropdownModel[]>;

  agencyCategories$: Observable<AgencyCategories[]>;
  agencyServices$: Observable<AgencyServices[]>;
  agencyDetailsFirstDiv: boolean;
  agencyDetailsSecondDiv: boolean;
  private token: AppUser;
  a2eOptions: any;

  addEditlabel: string;
  childList = [];
  personInvolved$: Observable<DropdownModel[]>;
  firstChild: any;
  rcCjamsID: any;
  clientDob: any;
  clientGender: any;
  clientAge: any;
  clientName: string;
  minActDate: Date;
  minEstDate: Date;
  personid: any;
  activityService: any[];
  activityServicePlan: any[];
  servicePlan: any[];
  clientProgramNames: any[];
  clientprogramselection: any;
  startdate: Date;
  enddate: Date;
  isServiceCase: any;
  serviceCase: boolean;
  isClosed = false;
  disbleService: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private datePipe: DatePipe,
    private _dataStoreService: DataStoreService,
    private _session: SessionStorageService
  ) {
    this.formInitialize();
    this.token = this._authService.getCurrentUser();
    this.a2eOptions = { format: 'MM/DD/YYYY', useCurrent: false };
  }

  private formInitialize() {
    this.addNewProviderForm = this.formBuilder.group(
      {
        // intakeservicerequestactorid: ['', Validators.required],
        clientprogramnameid: ['', Validators.required],
        servicetypeid: ['', Validators.required],
        frequencyCdId: ['', Validators.required],
        durationCdId: ['', Validators.required],
        estbegindate: ['', Validators.required],
        actbegindate: [''],
        estenddate: ['', Validators.required],
        actenddate: [''],
        actbegintime: [''],
        actendtime: [''],
        agencynotes: [''],
        serviceplanactionid: [null],
        serviceplanid: [null],
        serviceplanname: ''
      });
  }

  public selectService() {
    this.agencyDetailsFirstDiv = false;
    this.agencyDetailsSecondDiv = true;
  }

  private clientProgramDropdown() {
    this._commonHttpService.getArrayList(
      {
      where: {intakeserviceid: this.id, person_id: this.personid},
      order: 'agency_program_nm',
      method: 'get'
    },
    CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientProgramName + '?filter',

  ).subscribe(response => {
      if (response && response.length) {
          var arrayProg  = [];
          arrayProg = response.filter((item) => !item.enddate );
          if(arrayProg && arrayProg.length) {
            this.clientProgramNames = response;
            this.startdate = arrayProg[0].startdate;
            this.enddate = arrayProg[0].enddate;
            this.clientprogramselection = arrayProg[0].agency_program_area_id ? arrayProg[0].agency_program_area_id : null;
          } else {
            this.clientProgramNames = response;
            this.startdate = response[0].startdate;
            this.enddate = response[0].enddate;
            this.clientprogramselection = response[0].agency_program_area_id ? response[0].agency_program_area_id : null;
          }
          const currDate = new Date();
          if(this.enddate) {
            if(new Date(this.enddate) < currDate) {
                this.disbleService = true;
                this._alertService.warn('Please add required Program Service for the selected client');
            } else {
                this.disbleService = false;
            }
        } else {
            this.disbleService = false;
        }

      } else {
        this.disbleService = true;
        this.clientprogramselection = null;
        this._alertService.warn('Please add required program service for the selected client');
      }
  });
    const source = forkJoin([
      this._commonHttpService.getArrayList({
        order: 'SERVICE_NM'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientprogramServices),
      this._commonHttpService.getArrayList({
        order: 'value_tx'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Frequency),
      this._commonHttpService.getArrayList({
        order: 'value_tx'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Duration)
    ])
      .map((result) => {

        return {
          servicetypeid: result[0]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: res.SERVICE_NM,
                value: res.SERVICE_ID
              })
          ),
          frequencyCdId: result[1]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: (res.value_tx),
                value: (res.picklist_value_cd),
              })
          ),
          durationCdId: result[2]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: (res.value_tx),
                value: (res.picklist_value_cd),
              })
          )
        };
      })
      .share();

    this.agencyServiceNames$ = source.pluck('servicetypeid');
    this.frequencyCds$ = source.pluck('frequencyCdId');
    this.durationCds$ = source.pluck('durationCdId');
  }

  ngOnInit() {
    this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    // D-06581
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
    this.getInvolvedPerson();

    const da_status = this._session.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }

  getInvolvedPerson() {
    let personDetail = {};
    if (this.isServiceCase) {
      this.serviceCase = true;
      personDetail = {
          objectid: this.id,
          objecttypekey: 'servicecase'
      };
    } else {
        this.serviceCase = false;
        personDetail = {
            intakeserviceid: this.id
        };
    }
    this.childList = [];
    this._commonHttpService
        .getPagedArrayList( new PaginationRequest({
            method: 'get',
            page: 1, limit : 20,
            where: personDetail
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
        )
       /* .map((res) => {
            console.log('res', res);
            return res['data'].filter((item) => item.intakeservicerequestpersontypekey === 'CHILD');
        })*/.subscribe(response => {
            console.log('response...', response);
            if (response && response.data.length) {
                for (let i = 0; i < response.data.length; i++) {
                    if (response.data[i].roles) {
                       // for ( let j = 0; j < response.data[i].roles.length; j++) {
                            // if (response.data[i].roles[j].intakeservicerequestpersontypekey === 'CHILD') {
                               // this.rcCjamsID = response.data[i].cjamspid ? response.data[i].cjamspid : null;
                                this.childList.push(response.data[i]);
                                this.firstChild = response.data[i];
                                this.rcCjamsID = response.data[i].cjamspid;
                                this.clientDob = response.data[i].dob;
                                this.clientGender = response.data[i].gender;
                                this.clientAge = response.data[i].age;
                                this.clientName = response.data[i].firstname + ' ' + response.data[i].lastname;
                                this.personid = response.data[i].personid;
                           // }
                      //  }
                    }
                }
                this.getList();
                this.clientProgramDropdown();
            }
        });
}

getActivityService() {
  this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        where: {personid: this.personid},
        limit : 10,
        page: 1,
        method: 'get'
      }), 'serviceLogs/getServicePlanActionList' + '?filter'
    ).subscribe((result: any) => {
      this.activityService = result.data;
    });
}

onActivityService(serviceplanactionid) {
  this.addNewProviderForm.get('serviceplanname').reset();
  this.getServicePlan(serviceplanactionid);
}

getServicePlan(serviceplanactionid) {
  this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        where: {serviceplanactionid: serviceplanactionid},
        limit : 10,
        page: 1,
        method: 'get'
      }), 'serviceLogs/getServicePlanList' + '?filter'
    ).subscribe((result: any) => {
      this.servicePlan = result.data;
      if  (this.servicePlan && this.servicePlan.length) {
        this.addNewProviderForm.patchValue({
            serviceplanid: this.servicePlan[0].serviceplanid,
            serviceplanname: this.servicePlan[0].serviceplanname
        });
        this.addNewProviderForm.get('serviceplanname').disable();
    }
    });
}
  getFullName(person) {
    const nameKeys = [ 'prefx' , 'firstname' , 'middlename' , 'lastname' , 'suffix'];
    let name = '';
    nameKeys.forEach(key => {
    //console.log( key , person[key] );
      if(person && person.hasOwnProperty(key)){
        if ( !(person[key] == null) && !(person[key] == 'null') && !(person[key] == '') ) {
          name = name + person[key] + ' ';
          //console.log(name);
        }}
    });
    //console.log("NAME",name);
    return name;
  }
  pageChanged(page: number) {
    this.getList(page);
  }

  getList(pageNo = 1) {

    /*
        this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.agencyServiceLog ;
        const body = new PaginationRequest({
          where: {},
          limit: 10,
          method: 'post',
          page: pageNo
        });

        const source = this._commonHttpService.getPagedArrayList(body).share();
        this.agencyServiceList$ = source.pluck('data');
        if (pageNo === 1) {
          this.totalRecord$ = source.pluck('count');
        } */
    // D-06581
    this.agencyServiceList$ = this._commonHttpService.getArrayList(
      {
        where: { daNumber: this.daNumber, client_id: this.rcCjamsID },
        method: 'get',
        nolimit: true
      },

      CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.agencyServiceLog + '?filter'

    )
      .map(res => {
        return res['servicelogData'];
      });
  }

  initaddNewProviderForm() {
    this.minActDate = new Date();
    this.minActDate = new Date();
    this.agencyDetailsFirstDiv = true;
    this.agencyDetailsSecondDiv = false;
    this.agencyService = null;
    this.addEditlabel = 'Add';
    this.addNewProviderForm.reset();
    this.addNewProviderForm.reset();
    this.addNewProviderForm.patchValue({
      estbegindate : this.startdate,
      actbegindate: this.startdate
    });
    this.addNewProviderForm.patchValue({
      clientprogramnameid: this.clientprogramselection ? this.clientprogramselection : null
    });
    //this.addNewProviderForm.get('clientprogramnameid').disable();
    this.getActivityService();
  }

  formtString(param: string) {
    param = param.replace(',', '');
    param = param.replace('"', '');
    param = param.replace('(', '');
    param = param.replace(')', '');
    return param;
  }

  addAgency() {
    if (this.addNewProviderForm.valid) {
      const url = 'serviceLogs/' + (this.agencyService ? 'update' : 'save');
      const agencyForm = this.addNewProviderForm.getRawValue();

      const model = {
        'agencyServiceLdssId': this.addNewProviderForm.value.servicetypeid,
        'startDt': this.datePipe.transform(this.addNewProviderForm.value.actbegindate, 'yyyy-MM-dd'),
        'endDt': this.datePipe.transform(this.addNewProviderForm.value.actenddate, 'yyyy-MM-dd'),
        'descriptionTx': this.addNewProviderForm.value.agencynotes,
        'startTm': (typeof this.addNewProviderForm.value.actbegintime === 'string') ?
          this.addNewProviderForm.value.actbegintime : this.datePipe.transform(this.addNewProviderForm.value.actbegintime, 'hh-mm aa'),
        'endTm': (typeof this.addNewProviderForm.value.actendtime === 'string') ?
          this.addNewProviderForm.value.actendtime : this.datePipe.transform(this.addNewProviderForm.value.actendtime, 'hh-mm aa'),
        'estimatedStartDt': this.datePipe.transform(this.addNewProviderForm.value.estbegindate, 'yyyy-MM-dd'),
        'estimatedEndDt': this.datePipe.transform(this.addNewProviderForm.value.estenddate, 'yyyy-MM-dd'),
        'frequencyCd': this.addNewProviderForm.value.frequencyCdId,
        'durationCd': this.addNewProviderForm.value.durationCdId,
        // D-06581
        'daNumber': this.daNumber,
        'agencyProgramAreaId': agencyForm.clientprogramnameid,
        'serviceLogId': this.agencyService ? this.agencyService.service_log_id : null,
        'client_id': this.rcCjamsID ? this.rcCjamsID : null,
          serviceplanactionid: this.addNewProviderForm ? this.addNewProviderForm.value.serviceplanactionid : null,
          serviceplanid: agencyForm ? agencyForm.serviceplanid : null,
          serviceplanname: this.addNewProviderForm ? agencyForm.serviceplanname : null,
          // 'intakeservicerequestactorid': this.addNewProviderForm.value.intakeservicerequestactorid
      };
      const promise = this.agencyService && this.agencyService.service_log_id ? this._commonHttpService.update('', model, url) : this._commonHttpService.create(model, url);
      this.addNewProviderForm.get('serviceplanname').disable();

      promise.subscribe((response) => {
        console.log(response);
        if (response) {
          if (this.agencyService) {
            this._alertService.success('Agency Service updated successfully');
          } else {
            this._alertService.success('Agency Service saved successfully');
          }
          this.getList();
          this.closeItem();
        }
      },
        (error) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);

          this.closeItem();
        });
    } else {
      this._alertService.warn('Please fill mandatory fields');
    }
  }

  closeItem() {
    (<any>$('#add-newagencyprovider')).modal('hide');
  }

  view(agency) {
    this.agencyService = agency;
    (<any>$('#view-newagencyprovider')).modal('show');
  }

  delete(agencyService) {
    this.agencyService = agencyService;
  }

  edit(agency) {
    this.getActivityService();
    this.getServicePlan(agency.serviceplanactionid);
    this.agencyService = agency;
    this.updateForm(agency);
    this.addEditlabel = 'Edit';
  }

  updateForm(agency) {
    console.log('agency...', agency);
    const model = {
      // intakeservicerequestactorid: agency.intakeservicerequestactorid,
      // client_id: this.rcCjamsID ? this.rcCjamsID : null,
      clientprogramnameid: agency.agency_program_area_id,
      servicetypeid: agency.service_id,
      frequencyCdId: agency.frequency_cd,
      durationCdId: agency.duration_cd,
      estbegindate: new Date(agency.estimated_start_date),
      actbegindate: new Date(agency.actual_begin_date),
      estenddate: new Date(agency.estimated_end_date),
      actenddate: agency.actual_end_date ? new Date(agency.actual_end_date) : null,
      actbegintime: agency.actual_start_time,
      actendtime: agency.actual_end_time,
      agencynotes: agency.notes,
      serviceplanactionid: agency ? agency.serviceplanactionid : null,
      serviceplanid: agency ? agency.serviceplanid : null,
      serviceplanname: agency ? agency.serviceplanname :  null
    };
    this.addNewProviderForm.patchValue(model);
   // this.addNewProviderForm.setValue(model, { emitEvent: true, onlySelf: false });
    this.addNewProviderForm.get('clientprogramnameid').disable();
    this.minEstDate = new Date(this.addNewProviderForm.value.estbegindate);
    this.minActDate = new Date(this.addNewProviderForm.value.actbegindate);
    this.addNewProviderForm.get('serviceplanname').disable();
    (<any>$('#add-newagencyprovider')).modal('show');
  }

  deleteItem() {
    this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServiceLog;
        this._commonHttpService.update('',
        {
            service_log_id: this.agencyService.service_log_id,
            case_id: this.daNumber,
            client_id: this.rcCjamsID
        }).subscribe(
            (response) => {
                this._alertService.success('Service has been deleted successfully');
                (<any>$('#Delete-newagencyprovider')).modal('hide');
                this.getList(1);
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );


  }

  childSelect(clientdetail) {
    console.log('clientdetails...', clientdetail);
    this.clientName = clientdetail.firstname + ' ' + clientdetail.lastname;
    this.rcCjamsID = clientdetail.cjamspid;
    this.clientDob = clientdetail.dob;
    this. clientGender = clientdetail.gender;
    this.clientAge = clientdetail.age;
    this.personid = clientdetail.personid;
    this.getList();
    this.clientProgramDropdown();
  }

  onChangeDate(form, field) {
    console.log(field);
    if ( field === 'estbegindate' ) {
        this.minEstDate = new Date(form.value.estbegindate);
        form.get('estenddate').reset();
    } else if ( field === 'actbegindate') {
        this.minActDate = new Date(form.value.actbegindate);
        form.get('actenddate').reset();
    }
  }
}
