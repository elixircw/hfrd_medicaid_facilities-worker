import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicePlanCoreRoutingModule } from './service-plan-core-routing.module';
import { ServicePlanCoreComponent } from './service-plan-core.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { MatTooltipModule } from '@angular/material';
import { ServiceCaseManagementModule } from '../../../../shared-pages/service-case-management/service-case-management.module';
import { ServiceCaseManagementComponent } from '../../../../shared-pages/service-case-management/service-case-management.component';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    CommonModule,
    ServicePlanCoreRoutingModule,
    ServiceCaseManagementModule,
    FormMaterialModule,
    MatTooltipModule,
    QuillModule
  ],
  declarations: [ServicePlanCoreComponent]
 
})
export class ServicePlanCoreModule { }
