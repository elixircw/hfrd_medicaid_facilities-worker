import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService, SessionStorageService, CommonDropdownsService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { CansFConfig } from './service-plan-core-config';
import { ServicePlanService, ServicePlanFocus, ServicePlanAction, DSDSActionSummary } from '../../../_entities/caseworker.data.model';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import * as jsPDF from 'jspdf';
import { IntakeServiceRequest } from '../../cross-reference/_entities/crossreference.data.models';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';
import { SignatureFieldComponent } from '../../../../provider-management/license-management/license-change-request/signature-field/signature-field.component';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../../../../app.config';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { DsdsService } from '../../_services/dsds.service';
import { Router } from '@angular/router';
import { AppConstants } from '../../../../../@core/common/constants';
import * as _ from 'lodash';
import { IntakeStoreConstants } from '../../../../newintake/my-newintake/my-newintake.constants';


@Component({
  selector: 'service-plan-core',
  templateUrl: './service-plan-core.component.html',
  styleUrls: ['./service-plan-core.component.scss']
})
export class ServicePlanCoreComponent implements OnInit {
  @ViewChild(SignatureFieldComponent) public signaturePad: SignatureFieldComponent;
  dsdsActionsSummary = new DSDSActionSummary();
  percentageInprogress = 0;
  percentageAchieved = 0;
  percentageNotAchieved = 0;

  actionPersonList: any[];
  approvalProcess: string;
  selectedItem: string;
  selectedPlan: string;

  // selectedList: ServicePlanService[] = [];
  // idx: number;


  serviceplanstartdate: any;
  serviceplanenddate: any;
  selectedServiceList = [];
  listSnapShot = [];
  selectedActionList = [];
  selectedFocusList = [];

  servicePlanFormGroup: FormGroup;
  servicePlanForm: FormGroup;
  focusPlanFormGroup: FormGroup;
  actionPlanFormGroup: FormGroup;
  candidacyFormGroup: FormGroup;

  selectedService: ServicePlanService;
  backupService: ServicePlanService;
  currSelectedService: ServicePlanService;
  selectedAction: ServicePlanAction;
  selectedFocus: ServicePlanFocus;
  signatureFormGroup: FormGroup;

  getUsersList = [];
  selectedPerson: any;
  cansfData: any;
  intakeserviceid = '';
  daNumber = '';
  cansFNeedsList: any[];
  cansFStrengthList: any[];
  sectionList: string[] = [];
  personList: any[];
  personListName: any;
  personDataList: any[];
  childList: any[] = [];
  childCandidates: any[] = [];
  childrenHeader: string = '';
  legalGuardian = '';
  caseWorker = '';
  caseWorkerPh: any = null;
  user: AppUser;

  cansFNeedsData: any[];
  cansFStrengthsData: any[];
  cansNeedsData: any[];
  cansStrengthsData: any[];

  focusNeeds: any[];
  focusStrengths: any[];
  isAssementCompleted: boolean;
  totalActions: number;
  completedAction: number;
  deletetype: any;
  deleteObject: any;

  baseUrl: string;

  // Visitation Plan
  selectedServiceVisitationPlans: any [] = [];
  visitationPlansFormGroup: FormGroup;

  //--new visitation plan
  visitationPlanFormGroup: FormGroup;
  visitationplans: any[] = [];

  // YTP Variables
  ytpList: any[] = [];
  selectedYTP: any;

    // @Simar: New stuff
    // @Simar
    serviceplanslist: ServicePlanService[] = [];
    selectedServicePlan: any;

    serviceplangoals: any[] = [];
    serviceplanobjectives: any[] = [];
    color = 'primary';
    mode = 'determinate';

    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    servicePlanGoal: FormGroup;

    //Reference Dropdown values

    childandvisitortransportation = [];
    frequencyofplannedvisits = [];
    lengthofplannedvisit = [];

    // Goals
    splanGoalsFormGroup: FormGroup;
    selectedObjective: any;
    selectedGoal: any;
    goalReferenceValues = [];
    servicePlanNameReferenceValues = [];
    isOtherGoal = false;
    servicePlanNeed: FormGroup;
    servicePlanStrength: FormGroup;
    isSupervisor = false;
    isServiceCase = false;
    isAdoptionCase = false;

    //Status values
    goalStatusReferenceValues = [];
    objectiveStatusReferenceValues = [];
    actionStatusReferenceValues = [];

    //New Signature
    personSignatureFormGroup: FormGroup;
    signaturePersonTypes = [];
    personlistforsnapshot: any[];

    //Snapshot Version
    selectedSnapshotVersion: any;
    selectedSignaturesList = [];
    achievedActions: number;
    inProgressActions: number;
    notAchievedActions: number;
    completedPercentage: number;
    minContactDate = new Date();
    // splanversionpersoninvolved= ['066e7657-9f51-4271-929f-c157df45885a'];
    count: any;
    countobj: number;
    // versionfilterstartdate: any;
    // versionfilterenddate: any;

    snapshotFilterFormGroup: FormGroup;
    isClosed = false;
    constructor(private _formBuilder: FormBuilder, private _commonhttp: CommonHttpService,
        private _alertservice: AlertService, private _datastore: DataStoreService,
        private _intakeUtils: IntakeUtils,
        private _authService: AuthService,
        private _router: Router,
        private storage: SessionStorageService,
        private _dataStoreService: DataStoreService,
        private http: HttpClient,
        private _commonDropdownService: CommonDropdownsService
        ) {
            this.baseUrl = AppConfig.baseUrl;
    }

  ngOnInit() {
    this.dsdsActionsSummary= this._dataStoreService.getData('dsdsActionsSummary');
    this.cansFNeedsList = [];
    this.cansFStrengthList = [];

    this.intakeserviceid = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.personDataList = [];
    this.setMinContactDate();

    // TODO: Decide when or where to call this DB persistence of Assessments
    // this.saveAssessmentStrengthNeeds();
    this.daNumber = this._datastore.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.user = this._authService.getCurrentUser();
    if (this.user.role.id === '71') {
      this.caseWorker = this.user.user.userprofile.displayname;
      this.caseWorkerPh = this.user.user.userprofile.userprofilephonenumber.length > 0 ? this.user.user.userprofile.userprofilephonenumber[0].phonenumber : '';
    }
    this.getServicePlanNameReferenceValues();
    this.getInvolvedPerson();
    this.getCollateralPerson();
    this.getPersonListWithProgramAssignment();
    this.getServicePlans();
    this.getNeeds();
    this.getStrengths();
    this.selectedItem = 'Empty';

    this.initForms();
    this.getReferenceDropdowns();
    this.getGoalReferenceValues();
    this.getStatusReferenceValues();
    this.getYTPList();
    
    //Signatures
    this.getSignaturePersonTypeReferenceValues();
    this.initPersonSignatureFormGroup();
    this.initSnapshotFilterFormGroup();
    
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);

    this.isAssementCompleted = false;
    this.isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    const caseType = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
    this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
        
    if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
        this.isAdoptionCase = true;
    }
    const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
     } else {
         this.isClosed = false;
     }
    }
  }
  
  getReferenceDropdowns() {
    this._commonDropdownService.getReferenveValuesByTypeId('50042')
      .subscribe( (response) => { this.childandvisitortransportation = response });
    this._commonDropdownService.getReferenveValuesByTypeId('50084')
      .subscribe( (response) => { this.frequencyofplannedvisits = response });    
    this._commonDropdownService.getReferenveValuesByTypeId('500108')
      .subscribe( (response) => { this.lengthofplannedvisit = response });
  }

  initForms() {
    this.servicePlanFormGroup = this._formBuilder.group({
      serviceplanname: [''],
      effectivedate: [null, Validators.required],
      serviceplanid: [null],
      enddate: [null],
      numberofdays: [null],
      activeflag: [1],
      targetenddate: [null, Validators.required]
    });

    this.servicePlanForm = this._formBuilder.group({
     enddate: [null]
    });

    this.servicePlanGoal = this._formBuilder.group({
      goalname: [null],
      splangoalid: [null],
      status: ['']
     });

     this.servicePlanNeed = this._formBuilder.group({
      serviceplanneedname: [null]
     });

     this.servicePlanStrength = this._formBuilder.group({
      serviceplanstrengthname: [null]
     });

    this.focusPlanFormGroup = this._formBuilder.group({
      objectivename: ['', Validators.maxLength(150)],
      needs: [null],
      strengths: [null],
      splanobjectiveid: [null],
      otherneeds: [null],
      otherstrength: [null],
      comments: [''],
      status: ['']
    });

    this.actionPlanFormGroup = this._formBuilder.group({
      serviceplanactionname: [''],
      serviceplanactionid: [''],
      enddate: [null, Validators.required],
      startdate: [null, Validators.required],
      status: [''],
      // plantype: [null],
      planfor: ['In Home'],
      personresponsible: ['', Validators.required],
      // serviceplanpersoninvolved: [null, Validators.required]
      personinvolved: [null, Validators.required],
      serviceplanoutcome: [''],
      comments: ['']
    });

    this.initVisitationPlansFormGroup();
    this.initVisitationPlanFormGroup();
    this.initCandidacyForm();
    this.initSignatureFormGroup();

    // @Simar - new stuff
    // this.initSPlanGoalsFormGroup();
  }

  /**
   * Service Plan
   */
  getServicePlans() {
    const payload = {
      method: 'get',
      where: {
        caseid : this.intakeserviceid
      }
    };
    this._commonhttp.getArrayList(payload, 'serviceplan/listbyallrelation?filter').subscribe(
      response => {
        // @Simar: ANSHUL => New requirement that Supervisor can add and work on draft service plans
        // if (this.user.role.name !== 'field') {
        //   this.serviceplanslist = response.filter((item) =>  item.approvalstatustypekey !== null && item.approvalstatustypekey !== '');
        // } else {
          this.serviceplanslist = response;
        // }

        if (this.serviceplanslist && this.serviceplanslist.length) {
            this.serviceplanstartdate = this.serviceplanslist[0].effectivedate;
            this.serviceplanenddate = this.serviceplanslist[0].enddate;
            this.serviceplanslist.forEach((plan) => {
                // this.selectedService = plan;
                plan['isReady'] = this.isSpReadyForReview();
            });
            // this.selectedService = null;
        }

        if (this.selectedService) {
          this.selectedService = response.find(item => item.serviceplanid === this.selectedService.serviceplanid);
          this.calculation(this.selectedService);
          if (this.selectedGoal) {
            this.selectedGoal = this.selectedService.splangoal.find(item => item.splangoalid === this.selectedGoal.splangoalid);
            if (this.selectedObjective) {
              this.selectedObjective = this.selectedGoal.splanobjective.find(item => item.splanobjectiveid === this.selectedObjective.splanobjectiveid);
            }
          }
        }

        response.map(service => {
          if (Array.isArray(service.serviceplanfocus)) {
            service.serviceplanfocus.map(objective => {
              this.timecheck(objective);
            });
          }
        });
      });
    }

    addServicePlan(activeflag) {
        const service = (activeflag === 1) ? this.servicePlanFormGroup.getRawValue() : this.selectedService;
        // service['title'] = service.serviceplanname;
        service.objectid = this.intakeserviceid;
        service.activeflag = activeflag;
        // this.serviceplanslist.push(service);
        this.servicePlanFormGroup.reset();
        this._commonhttp.create(service, 'serviceplan/addupdate').subscribe(
          response => {
          this.selectedItem = (activeflag === 1) ? 'service' : 'Empty';
          delete response.where;
          this.getServicePlans();
          if (activeflag === 1) {
              this._alertservice.success('Service plan created successfully.');
          } else {
              this._alertservice.success('Service plan deleted successfully.');
          }
          (<any>$('#add-service')).modal('hide');
          (<any>$('#delete-popup')).modal('hide');
          },
          error => {
          this._alertservice.error('Error in creating Service plan.');
          (<any>$('#delete-popup')).modal('hide');
          }
      );
    }

    selectPlan(item, index) {
        this.selectedServicePlan = item;

        //Scrolll the screen to the service plan workspace when selected a plan
        var contentElement = document.getElementById("splan_content");
        // contentElement.scrollIntoView({behavior: "smooth", block: "nearest", inline: "start"});
        if (contentElement) {
          contentElement.scrollIntoView(true);
        }

        this.selectService(item, index);
        this.getVisitationPlans();
      }

      selectService(service, index: number) {
        this.selectedItem = 'service';
        this.selectedService = service;
        this.selectedService.approvalstatustypekey = service.approvalstatustypekey;
        this.selectedFocus = null;
        this.selectedAction = null;
        this.calculation(this.selectedService);
        this.selectedFocusList = service.focus;
        this.initCandidacyForm();
        if (this.selectedService.serviceplancandidacy) {
          this.updateCandidacy();
          this.candidacyFormGroup.patchValue(this.selectedService.serviceplancandidacy);
          this.childCandidates = this.candidacyFormGroup['controls'].candidates['controls'];
        } else {
          this.updateCandidacy();
        }

        this.selectedService['isReady'] = this.isSpReadyForReview();

        this.setGoalsData();
      }

    setGoalsData() {
        this.serviceplangoals = this.selectedService.splangoal;
    }

    setSelectedGoal() {

    }

    /**
     * Goals
     */
    // initSPlanGoalsFormGroup() {
    //     this.splanGoalsFormGroup = this._formBuilder.group({
    //         splangoal : this._formBuilder.array([])
    //     });
    // }

    setGoals() {
        this.splanGoalsFormGroup.patchValue(this.selectedService.splangoal);
    }

    // createGoalFormGroup() {
    //     return this._formBuilder.group({
    //         serviceplanid : [null],
    //         splangoalid: [null],
    //         goalname: [null],
    //         approvalstatustypekey: [null]
    //         // activeflag: 1
    //     });
    // }

    // openAddGoalModal() {
    //     if (this.selectedService && this.selectedService.serviceplanvisitation) {
    //     this.visitationPlansFormGroup.patchValue(this.selectedService.serviceplanvisitation);
    //     }
    //     (<any>$('#add-visitation')).modal('show');
    // }

    // addSPGoal() {
    //     const control = <FormArray>this.splanGoalsFormGroup.controls.splangoal;
    //     control.push(this.initVisitationFormGroup());
    // }

  addGoal(activeflag, item) {
    const goal = (activeflag === 1) ? this.servicePlanGoal.getRawValue() : item;
    goal.activeflag = activeflag;
    goal['serviceplanid'] = this.selectedService.serviceplanid;
    this._commonhttp.create(goal, 'splangoal/addupdate').subscribe(
      response => {
        (<any>$('#add-goal')).modal('hide');
        this.servicePlanGoal.reset();
        this._alertservice.success('Goal added successfully!');
        this.getServicePlans();
        this.resetGoalsModal();
        // this.selectedService.splangoal.unshift(goal);
      }
    );
  }

  addservicePlanneed() {
    const needs = this.servicePlanNeed.getRawValue();
    needs.intakeserviceid = this.intakeserviceid;
    this._commonhttp.create(needs, 'serviceplanneed').subscribe(
      response => {
        console.log(response, 'response');
        this.getNeeds();
        (<any>$('#add-need')).modal('hide');
        (<any>$('#add-focus')).modal('show');
        this.servicePlanNeed.reset();
        this._alertservice.success('Need added successfully!');

      }
    );

  }

  addservicePlanstrength() {
    const strength = this.servicePlanStrength.getRawValue();
    strength.intakeserviceid = this.intakeserviceid;
    this._commonhttp.create(strength, 'serviceplanstrength').subscribe(
      response => {
        console.log(response, 'response');
        this.getStrengths();
        (<any>$('#add-strength')).modal('hide');
        (<any>$('#add-focus')).modal('show');
        this.servicePlanStrength.reset();
        this._alertservice.success('Strength added successfully!');

      }
    );

  }


  /**
   * Visitation Plans
   */
  initVisitationPlansFormGroup() {
    this.visitationPlansFormGroup = this._formBuilder.group({
      visitationplans: this._formBuilder.array([])
    });
  }

  initVisitationFormGroup() {
    return this._formBuilder.group({
      personinvolved: [null, Validators.required],
      frequency: [null],
      length: [null],
      location: [''],
      conditions: [''],
      childtransportation: [''],
      visitortransportation: ['']
    });
  }

  //New Visitaion plans
  getVisitationPlans() {
    const payload = {
      method: 'get',
      where: {
        serviceplanid : this.selectedService.serviceplanid
      }
    };
    this._commonhttp.getArrayList(payload, 'serviceplanvisitationplanmapping/listallvisitation?filter').subscribe(
      response => {
        this.visitationplans = response.map(item => item.visitationplan[0]);
        this.selectedService.visitationplans = this.visitationplans;
        console.log(this.visitationplans, 'this.visitationplans');
        console.log(response, 'responseresponse');  
      }
    );
  }

  initVisitationPlanFormGroup() {
    this.visitationPlanFormGroup = this._formBuilder.group({
      personid: [null],
      caseid: [null],
      visitfrequencytypekey: [null],
      visitdurationtypekey: [null],
      visittypekey: [null],
      planexplain: [''],
      planlocation: [''],
      childtransportationtypekey: [null],
      transportexplain: [''],
      visitortransportationtypekey: [null],
      visitortransportexplain: [''],
      frequencyexplain: [''],
      // activeflag: [null],
      courtorderedflag: [null],
      supervisedflag: [2],
      supervisecomments: [''],
      visitnotallowedflag: [null],
      datavalidflag: [null],
      establisheddate: [null],
      enddate: [null],
      visitationplanclients: [null],
      visitationplanid: [null]
    });
  }
  
  openVisitationPlanModal() {
    this.getVisitationPlans();
    (<any>$('#visitation-plans')).modal('show');
  }

  addVisitationPlan() {
    // Save the visitation plan
    const visitationplan = this.visitationPlanFormGroup.getRawValue();
    visitationplan.caseid = this.intakeserviceid;
    visitationplan.serviceplanid = this.selectedService.serviceplanid;
    this._commonhttp.create(visitationplan, 'visitationplan/addupdate').subscribe(
      response => {
        this.getVisitationPlans();
        (<any>$('#add-visitation-plan')).modal('hide');
        (<any>$('#visitation-plans')).modal('show');
        this.visitationPlanFormGroup.reset();
        this._alertservice.success('Visitation plan added successfully!');

      }
    );
  }

  setMinContactDate() {
    const caseInfo = this._dataStoreService.getData('dsdsActionsSummary');
    const intakeReceivedDate = this._dataStoreService.getData(IntakeStoreConstants.receivedDate);

    if (this.isIntakeMode() && intakeReceivedDate) {
        // For intake min contact date will be the Intake/Referral received date
        this.minContactDate = intakeReceivedDate;
    } else {
        // For case min contact date will be the Case received date
        if (caseInfo) {
            this.minContactDate = caseInfo.da_receiveddate;
        }
    }
}

  isIntakeMode() {
    return this.getIntakeNumber() ? true : false;
  }

  getIntakeNumber() {
    const intakeStore = this.storage.getObj('intake');
    if (intakeStore && intakeStore.number) {
        return intakeStore.number;
    } else {
        return null;
    }
  }

  viewVisitationPlan(item) {
    this.editVisitationPlan(item);
    this.visitationPlanFormGroup.disable();
  }

  editVisitationPlan(item) {
    let formdata = item;
    if (formdata.visitationplanclients) {
      formdata.visitationplanclients = formdata.visitationplanclients.map(x => x.personid);
    }
    this.visitationPlanFormGroup.patchValue(formdata);
    this.visitationPlanFormGroup.enable();
    (<any>$('#visitation-plans')).modal('hide');
    (<any>$('#add-visitation-plan')).modal('show');
  }

  // openAddVisitationModal() {
  //   if (this.selectedService && this.selectedService.serviceplanvisitation) {
  //     this.visitationPlansFormGroup.patchValue(this.selectedService.serviceplanvisitation)
  //   }
  //   (<any>$('#add-visitation')).modal('show');
  // }

  // addVisitationPlan() {
  //   const control = <FormArray>this.visitationPlansFormGroup.controls.visitationplans;
  //   control.push(this.initVisitationFormGroup());
  // }

  // saveVisitationPlans() {
  //   const payload = {};
  //   payload['serviceplanid'] = this.selectedService.serviceplanid;
  //   payload['serviceplanvisitation'] = this.visitationPlansFormGroup.value; // this.visitationFormGroup.value;
  //   this._commonhttp.patch(
  //     this.selectedService.serviceplanid,
  //     payload,
  //     'serviceplan'
  //   ).subscribe(
  //     response => {
  //       // this.getServicePlans();
  //       this.selectedService.serviceplanvisitation = this.visitationPlansFormGroup.value;
  //       this._alertservice.success('Visitation details entered successfully!');

  //       // TODO: SIMAR do I need this ?
  //       // this.visitationPlansFormGroup.reset();
  //       // (<any>$('#add-visitation')).modal('hide');
  //     },
  //     error => {
  //       this._alertservice.error('Error in entering visitation details!');
  //     }
  //   );
  // }


  /**
   * Candidacy
   */
  initCandidacyForm() {
    this.candidacyFormGroup = this._formBuilder.group({
      candidates: this._formBuilder.array([])
    });
  }

  updateCandidacy() {
    const control = <FormArray>this.candidacyFormGroup.controls.candidates;
    const actionPersonList = (this.actionPersonList && Array.isArray(this.actionPersonList)) ? this.actionPersonList : [];
    // const personList =  actionPersonList.filter(item =>
    //   item.programkey === 'IHSFP' && item.role === 'CHILD');
    const personList = this.childList.filter(item => 
      item.programarea && item.programarea.filter(element =>
          element.programkey === 'IHSFP' || element.programkey === 'CPS'
        )
      );

      personList.forEach(child => {
        control.push(this._formBuilder.group({
          id: child.id ? child.id : '',
          name: child.name ? child.name : '',
          candidacy: [null]
          })
        );
      });
    }

  openCandidacyModal() {
    if (this.selectedService && this.selectedService.serviceplancandidacy) {
      this.candidacyFormGroup.patchValue(this.selectedService.serviceplancandidacy);
    }
    (<any>$('#candidacy')).modal('show');
  }

  addCandidacy() {
    const payload = {};
    payload['serviceplanid'] = this.selectedService.serviceplanid;
    payload['serviceplancandidacy'] = this.candidacyFormGroup.value;
    this._commonhttp.patch(
      this.selectedService.serviceplanid,
      payload,
      'serviceplan'
    ).subscribe(
      response => {
        // this.getServicePlans();
        this.selectedService.serviceplancandidacy = this.candidacyFormGroup.value;
        this.candidacyFormGroup.reset();
        this._alertservice.success('Candidacy information entered successfully!');
        (<any>$('#candidacy')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in entering Candidacy information!');
      }
    );
  }

  /**
   * Signatures
   */
  initSignatureFormGroup() {
    this.signatureFormGroup = this._formBuilder.group({
      ihmcaseworkersign: [null],
      ihmcaseworkersigndate: [null],
      ihmcaseworkername: [null],
      // ihmcwuploadsign: [null],
      // ihmcwrefusesign: [null],
      oohcaseworkersign: [null],
      oohcaseworkersigndate: [null],
      oohcaseworkername: [null],
      // oohcwuploadsign: [null],
      // oohcwrefusesign: [null],
      familymembersign: [null],
      familymembersigndate: [null],
      familymembername: [null],
      familymemuploadsign: [null],
      familymemrefusesign: [null],

      secondfamilymembersign: [null],
      secondfamilymembersigndate: [null],
      secondfamilymembername: [null],
      secondfamilymemuploadsign: [null],
      secondfamilymemrefusesign: [null],

      supervisorsign: [null],
      supervisorsigndate: [null],
      supervisorname: [null],
      // supervisoruploadsign: [null],
      // supervisorrefusesign: [null]
    });
  }

  initPersonSignatureFormGroup() {
    this.personSignatureFormGroup = this._formBuilder.group({
      personsign: [null],
      personsigndate: [null],
      personname: [null],
      persontype: [null],
      persontypekey: [null],
      personsignuploaded: [null],
      personsignrefused: [null]
    });
  }

  //Snapshot version
  initSnapshotFilterFormGroup() {
    this.snapshotFilterFormGroup = this._formBuilder.group({
      personlist: [null],
      startdate: [null],
      enddate: [null]
    });
  }

  async downloadCasePdf(val) {
    const pages = document.getElementsByClassName('pdf-page');
    let pageImages = [];
    for (let i = 0; i < pages.length; i++) {
      const pageName = pages.item(i).getAttribute('data-page-name');
      const isPageEnd = pages.item(i).getAttribute('data-page-end');
      if (val === 'ihm') {
        if (pageName === 'In Home Service Plan') {
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
            const img = canvas.toDataURL('image/png');
            pageImages.push(img);
          });
        }
      } else {
        if (pageName === 'Out of Home Service Plan') {
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
            const img = canvas.toDataURL('image/png');
            pageImages.push(img);
          });
        }
      }
    }

    this.pdfFiles.push({ fileName: val === 'ihm' ? 'In Home Service Plan' : 'Out of Home Service Plan', images: pageImages });
    pageImages = [];
    this.convertImageToPdf();
  }

  convertImageToPdf() {
    this.pdfFiles.forEach((pdfFile) => {
        let doc = null;
        doc = new jsPDF();
        const width = doc.internal.pageSize.getWidth() - 10;
        const heigth = doc.internal.pageSize.getHeight() - 10;

        pdfFile.images.forEach((image, index) => {

          doc.addImage(image, 'PNG', 3, 5, width, heigth);
          if (pdfFile.images.length > index + 1) {
              doc.addPage();
          }
        });
        doc.save(pdfFile.fileName);
    });
    (<any>$('#servicePlanIhmPrint')).modal('hide');
    (<any>$('#servicePlanOohPrint')).modal('hide');
    (<any>$('#youthTransPlan')).modal('hide');
    this.pdfFiles = [];
  }

  async downloadYTPPdf() {
    const pages = document.getElementsByClassName('pdf-page');
    let pageImages = [];
    for (let i = 0; i < pages.length; i++) {
      const pageName = pages.item(i).getAttribute('data-page-name');
      if (pageName === 'Youth Transitional Plan') {
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
            const img = canvas.toDataURL('image/png');
            pageImages.push(img);
          });
        }
    }

    this.pdfFiles.push({ fileName: 'Youth Transitional Plan', images: pageImages });
    pageImages = [];
    this.convertImageToPdf();
  }

  youthTransPlanPrint() {
    (<any>$('#youthTransPlan')).modal('show');
  }

  servicePlanIhmPrint(item,index) {
   const inputRequest = {
        'intakeserviceid': this.intakeserviceid,
        'id': this.selectedService.serviceplanid,
        'type': 'IHSFP',
        'snapshotid': item,
        'documenttemplatekey': [
            'inhome'
        ],
        'isheaderrequired': false,
        personinvolved: this.personlistforsnapshot[index]
    };
    const payload = {
      method: 'post',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest,
      documntkey: [
        'inhome'
    ],
    };
    this._commonhttp.create(payload, 'serviceplan/getreportserviceplan').subscribe(
      response => {
        console.log(response, 'pdf response');
        setTimeout(() => window.open(response.data.documentpath), 2000);
     });
    // this.backupService = this.selectedService ? this.selectedService : null;
    // this.selectedService = item;
    // (<any>$('#servicePlanIhmPrint')).modal('show');
  }

  viewDownload(item) {
    this.backupService = this.selectedService ? this.selectedService : null;
    this.selectedService = item;
    this.getHist();
    (<any>$('#splan-snapshot-version')).modal('show');
  }


  documentGenerate(item, typeData) {
    const modal = {
      count: -1,
      where: {
        documenttemplatekey: ['inhome'],
        id: item.id,
        type: typeData,
        // date_sw: childReport.date_sw,
        // date_from: childReport.date_from,
        // date_to: childReport.date_to,
        format: 'pdf'
      },
      method: 'post'
    };
      this._commonhttp.download('evaluationdocument/generateintakedocument', modal)
        .subscribe(res => {
          const blob = new Blob([new Uint8Array(res)]);
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          link.download = 'inHome.pdf';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
    });
  }



  servicePlanOohPrint(item,index) {
    const inputRequest = {
      'intakeserviceid': this.intakeserviceid,
      'id': this.selectedService.serviceplanid,
      'type': 'OOH',
      'snapshotid': item,
      'documenttemplatekey': [
          'inhome'
      ],
      'isheaderrequired': false,
      personinvolved: this.personlistforsnapshot[index]
  };
  const payload = {
    method: 'post',
    count: -1,
    page: 1,
    limit: 20,
    where: inputRequest,
    documntkey: [
      'oohome'
  ],
  };
  this._commonhttp.create(payload, 'serviceplan/getreportserviceplan').subscribe(
    response => {
      console.log(response, 'pdf response');
      setTimeout(() => window.open(response.data.documentpath), 2000);
   });
    // this.backupService = this.selectedService ? this.selectedService : null;
    // this.selectedService = item;
    // (<any>$('#servicePlanOohPrint')).modal('show');
  }

  closeView() {
    this.selectedService = this.backupService ? this.backupService : null;
  }

  onSelectedChange(event) {
    console.log(event);
  }


  closeServicePlan() {
    this.calculation(this.selectedService);
    if (this.completedPercentage !== 100) {
      this._alertservice.error('Please close all goals for this service plan.');
      return;
    }
    const service =  this.servicePlanForm.getRawValue();
    service.serviceplanid = this.selectedService.serviceplanid;
    this._commonhttp.patch(
      this.selectedService.serviceplanid,
      service,
      'serviceplan'
    ).subscribe(
      response => {
        this.getServicePlans();
        this.servicePlanForm.reset();
        (<any>$('#close-service')).modal('hide');
       },
      error => {
        this._alertservice.error('Error in closing service plan.');
      }
    );
  }



  addFocusPlan(activeflag, focusPlan?: ServicePlanFocus) {
    var objectivealertmsg;
    // const focus = this.focusPlanFormGroup.getRawValue();
    const objective = (activeflag === 1) ? this.focusPlanFormGroup.getRawValue() : focusPlan;
    if(objective.splanobjectiveid == undefined || objective.splanobjectiveid == null || objective.splanobjectiveid == '') {
      objectivealertmsg = 'Objective is added successfully.';
    }
    else {
      objectivealertmsg = 'Objective is updated successfully.';
    }
    objective.needs = JSON.stringify(objective.needs);
    objective.strengths = JSON.stringify(objective.strengths);
    objective.activeflag = activeflag;
    objective.splangoalid = this.selectedGoal.splangoalid;
    objective.serviceplanid = this.selectedService.serviceplanid;
    this.selectedService.serviceplanfocus = (this.selectedService.serviceplanfocus && this.selectedService.serviceplanfocus.length) ? this.selectedService.serviceplanfocus : [];
    // focus.needs =
    // focus.strengths = (focus && Array.isArray(focus.serviceplanstrength)) ? focus.serviceplanstrength : [];
    this._commonhttp.create(objective, 'splanobjective/addupdate').subscribe(
      response => {
        objective.splanobjectiveid = response.splanobjectiveid;

        const objectFocus =  this.selectedService.serviceplanfocus.filter(fdata =>
          fdata.splanobjectiveid === response.splanobjectiveid
        );
        if (objectFocus && objectFocus.length === 0) {
           this.selectedService.serviceplanfocus.unshift(objective);
        } else {
          for (const i in this.selectedService.serviceplanfocus) {
            if (this.selectedService.serviceplanfocus[i].splanobjectiveid === response.splanobjectiveid) {

              if (objective.activeflag === 1) {
                 this.selectedService.serviceplanfocus[i] = objective;
                 break;
               } else {
                  this.selectedService.serviceplanfocus.splice(objective, 1);
                  break;
               }
               // Stop this loop, we found it!
            }
          }
        }
        this.focusPlanFormGroup.reset();
        const message = (activeflag === 1) ? objectivealertmsg : 'Objective is deleted successfully.';
        this._alertservice.success(message);
        this.getServicePlans();
        // this.selectPlan(this.selectedList[0], this.idx);
        (<any>$('#add-focus')).modal('hide');
      },
      error => {
        this.focusPlanFormGroup.reset();
        const message = (activeflag === 1) ? 'Error in updating Objective.' : 'Error in deleting Objective.';
        this._alertservice.error(message);
        (<any>$('#delete-popup')).modal('hide');
      }
    );
  }

  compareNeedsObjects(o1: any, o2: any): boolean {
    return o1.serviceplanneedname === o2.serviceplanneedname;
  }

  getPlanByPersonID(personId, action) {
    if (this.actionPersonList && Array.isArray(this.actionPersonList)) {
      const id = (action === 1) ? personId[0] : personId[0].personinvolved;
      const person = this.actionPersonList.find(item => item.personid === id);
      const plan = (person) ? (person.programkey) : '';
      return plan;
    }
    return '';
  }

  addActionPlan(activeflag, actionPlan?: ServicePlanAction) {
    var actionalertmsg;
    const action = (activeflag === 1) ? this.actionPlanFormGroup.getRawValue() : actionPlan;
    if(action.serviceplanactionid == undefined || action.serviceplanactionid == null|| action.serviceplanactionid == '') {
      actionalertmsg = 'Action is created successfully.';
    }
    else {
      actionalertmsg = 'Action is updated successfully.';
    }
    action.serviceplanactionid = (action.serviceplanactionid !== '') ? action.serviceplanactionid : null;
    // action.splanobjectiveid = (this.selectedFocus) ? this.selectedFocus.splanobjectiveid : null;
    action.splanobjectiveid = this.selectedObjective.splanobjectiveid;
    action.objectid = this.intakeserviceid;
    action.activeflag = activeflag;
    action.serviceplanpersoninvolved = (activeflag === 1) ? action.personinvolved : action.serviceplanpersoninvolved;
    action.planfor = this.getPlanByPersonID((activeflag === 1) ?
    action.personinvolved : action.serviceplanpersoninvolved, activeflag);
    action.personinvolved = action.personinvolved;
    action.serviceplanoutcome = action.serviceplanoutcome;
    // this.selectedFocus.serviceplanaction = (this.selectedFocus.serviceplanaction && this.selectedFocus.serviceplanaction.length) ? this.selectedFocus.serviceplanaction : [];
    // this.selectedFocus.serviceplanaction.push(action);
    this.actionPlanFormGroup.reset();
    this.actionPlanFormGroup.patchValue({status: 'open'});
    // this.actionPlanFormGroup.get('plantype').clearValidators();
    // this.actionPlanFormGroup.get('plantype').updateValueAndValidity();
    this._commonhttp.create(action, 'serviceplanaction/addupdate').subscribe(
      response => {
       // this.getServicePlans();
        const message = (activeflag === 1) ? actionalertmsg : 'Action is deleted successfully.';
        if (this.selectedObjective.serviceplanaction && this.selectedObjective.serviceplanaction.length) {

          const objectAction = this.selectedObjective.serviceplanaction.filter(anaction =>
            anaction.serviceplanactionid === response.serviceplanactionid
          );
          if (objectAction && objectAction.length === 0) {
            action.serviceplanactionid = response.serviceplanactionid;
            this.selectedObjective.serviceplanaction.unshift(action);
          } else {
            for (const i in this.selectedObjective.serviceplanaction) {
              if (this.selectedObjective.serviceplanaction[i].serviceplanactionid === response.serviceplanactionid) {
                 action.serviceplanactionid = response.serviceplanactionid;
                if (action.activeflag === 1) {
                   this.selectedObjective.serviceplanaction[i] = action;
                   break;
                 } else {
                    this.selectedObjective.serviceplanaction.splice(action, 1);
                    break;
                 }
                 // Stop this loop, we found it!
              }
            }
          }

        } else {
          this.selectedObjective.serviceplanaction = [];
          this.selectedObjective.serviceplanaction.unshift(response);
        }
        this._alertservice.success(message);
        this.getServicePlans();
        (<any>$('#add-action')).modal('hide');
        (<any>$('#delete-popup')).modal('hide');
      },
      error => {
        const message = (activeflag === 1) ? 'Error in updating Action.' : 'Error in deleting Action.';
        this._alertservice.error(message);
        this.actionPlanFormGroup.reset();
        this.actionPlanFormGroup.patchValue({ status: 'open' });
        // this.actionPlanFormGroup.get('plantype').clearValidators();
        // this.actionPlanFormGroup.get('plantype').updateValueAndValidity();
        (<any>$('#delete-popup')).modal('hide');
      }
    );
  }


  openAddServiceModal() {
    // this.selectService = null;
    if (this.cansFNeedsList && this.cansFNeedsList.length === 0) {
      // this._alertservice.warn('Please fill CANS-F and CANS assessment');
    }
    this._commonhttp.getArrayList({},
      'Nextnumbers/getNextNumber?apptype=Serviceplannumber') .subscribe(
        (result) => {
          this.servicePlanFormGroup.patchValue({
            serviceplanname:  result['nextNumber']
          });
          (<any>$('#add-service')).modal('show');
        });
  }

  captureSign(item) {
    this.currSelectedService = item;
    if (this.currSelectedService && this.currSelectedService.serviceplansignatures) {
      this.signatureFormGroup.patchValue(this.currSelectedService.serviceplansignatures);
    }
    (<any>$('#signature')).modal('show');
  }

  signServicePlan() {
    const payload = {};
    payload['serviceplanid'] = this.currSelectedService.serviceplanid;
    payload['serviceplansignatures'] = this.signatureFormGroup.value;
    this._commonhttp.patch(
      this.currSelectedService.serviceplanid,
      payload,
      'serviceplan'
    ).subscribe(
      response => {
        this.getServicePlans();
        this.signatureFormGroup.reset();
        this._alertservice.success('Signatures captured successfully!');
        (<any>$('#signature')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in capturing signatures!');
      }
    );
  }

  calculation(selectedService) {
    let goalToalList = [];
    let goalAchievedList = [];
    let goalInprogressList = [];
    let goalNotAchievdedList = [];
    this.percentageInprogress = 0;
    this.percentageAchieved = 0;
    this.percentageNotAchieved = 0;
    const goals = Array.isArray(selectedService.splangoal) ? selectedService.splangoal : [];
    goalToalList = goals;
    if (goalToalList.length) {
      goalAchievedList = goalToalList.filter((item) => item.status === 'Achieved');
      goalNotAchievdedList = goalToalList.filter((item) => item.status === 'Not Achieved');
      goalInprogressList = goalToalList.filter((item) => item.status === 'In Progress');
    }
    if (goalToalList.length && goalAchievedList.length) {
      this.percentageAchieved = goalAchievedList.length / goalToalList.length * 100;
      this.percentageAchieved = Math.floor(this.percentageAchieved);
    }
    if (goalToalList.length && goalNotAchievdedList.length) {
      this.percentageNotAchieved = goalNotAchievdedList.length / goalToalList.length * 100;
      this.percentageNotAchieved = Math.floor(this.percentageNotAchieved);
    }
    if (goalToalList.length && goalInprogressList.length) {
      this.percentageInprogress = goalInprogressList.length / goalToalList.length * 100;
      this.percentageInprogress = Math.floor(this.percentageInprogress);
    }
    this.totalActions = goalToalList.length;
    this.achievedActions = goalAchievedList.length;
    this.notAchievedActions = goalNotAchievdedList.length;
    this.inProgressActions = goalInprogressList.length;

    this.completedPercentage = this.percentageAchieved + this.percentageNotAchieved;

    // this.completedAction = goalCompletedList.length;
  }

  getPersonListWithProgramAssignment() {
    let inputRequest = {};
    inputRequest = {
      objectid: this.intakeserviceid,
    };
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest
    };
    this._commonhttp.getArrayList(payload, 'People/getPersonWithProgramAssignment?filter').subscribe(
      response => {
        console.log(response);
        this.actionPersonList = response;
     });

  }

  getInvolvedPerson() {
    let inputRequest = {};
    const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    if (isServiceCase || this.isAdoptionCase) {
      inputRequest = {
        objectid: this.intakeserviceid,
        objecttypekey: 'servicecase'
      };
    } else {
      inputRequest = {
        intakeserviceid: this.intakeserviceid
      };
    }
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest
    };
    this._commonhttp.getPagedArrayList(payload, 'People/getpersondetail?filter').subscribe(
      response => {
        console.log(response);
        this.personList = response.data;

        if (response.data && response.data.length) {
          response.data.forEach(person => {
            this.personDataList.push({'name': this.getFullName(person)});
            let obj;
            if (person.roles) {
              person.roles.forEach(role => {
                if (role.intakeservicerequestpersontypekey === 'CHILD') {
                  obj = {
                    'id': person.cjamspid,
                    'personid': person.personid,
                    'name': this.getFullName(person),
                    'candidacy': null,
                    'programarea': person.programarea,
                  };
                  this.childList.push(obj);
                }
                if (role.intakeservicerequestpersontypekey === 'LG') {
                  this.legalGuardian = this.getFullName(person);
                }
              });
            }
          });
          this.personDataList.push({'name': this.user.user.userprofile.displayname});
          if (this.personDataList && this.personDataList.length) {
          this.personListName = this.personDataList[0].name;
          }
          console.log(this.childList);
          this.updateCandidacy();
        }
      });
  }

  getCollateralPerson(){
    const request = {
        objectid: this.intakeserviceid,
        objecttype: 'case'
    };
    this._commonhttp.getArrayList(
        {
            where: request,
            method: 'get',
            nolimit: true
        },
        'collateral/list?filter'
    ).subscribe(res => {
        if (res && res.length && res[0].getcollateraldetails && res[0].getcollateraldetails.length) {
            const collateralDetails = res[0].getcollateraldetails;
            if(collateralDetails && collateralDetails.length>0){
              collateralDetails.forEach(collateralDetail => {
                this.personDataList.push({'name': this.getCollateralFullName(collateralDetail)})
              });
            }
        }
    });

  }

  getFullName(person) {
    const nameKeys = [ 'prefx' , 'firstname' , 'middlename' , 'lastname' , 'suffix'];
    let name = '';
    nameKeys.forEach(key => {
      //console.log( key , person[key] );
      if(person && person.hasOwnProperty(key)){
      if ( !(person[key] == null) && !(person[key] == 'null') && !(person[key] == '') ) {
        name = name + person[key] + ' ';
        //console.log(name);
      }}
    });
    //console.log("NAME",name);
    return name;
  }

  getCollateralFullName(collateral) {
    const nameKeys = [ 'prefixtypekey' , 'firstname' , 'middlename' , 'lastname' , 'suffixtypekey'];
    let name = '';
    nameKeys.forEach(key => {
      console.log( key , collateral[key] );
      if ( !(collateral[key] == null) && !(collateral[key] == 'null') && !(collateral[key] == '') && collateral.hasOwnProperty(key) ) {
        name = name + collateral[key] + ' ';
        console.log(name);
      }
    });
    console.log("NAME",name);
    return name;
  }

  editServicePlan() {
    this.servicePlanForm.patchValue(this.selectedService);
    (<any>$('#close-service')).modal('show');
  }

  editGoal(goal) {
    this.servicePlanGoal.patchValue(goal);
    (<any>$('#add-goal')).modal('show');
  }

  editFocus(objective, goal) {
    this.selectedGoal = goal;
    this.focusPlanFormGroup.patchValue(objective);
    if (objective.serviceplanneed && Array.isArray(objective.serviceplanneed)) {
      const serviceplanneedOne = objective.serviceplanneed.map(item => item.serviceplanneedname ? item.serviceplanneedname : item);
      this.focusPlanFormGroup.patchValue({ serviceplanneed: serviceplanneedOne });
    }
    if (objective.serviceplanstrength && Array.isArray(objective.serviceplanstrength)) {
      const serviceplanstrengthOne = objective.serviceplanstrength.map(item => item.serviceplanstrengthname ? item.serviceplanstrengthname : item);
      this.focusPlanFormGroup.patchValue({ serviceplanstrength: serviceplanstrengthOne });
    }

    (<any>$('#add-focus')).modal('show');
  }

  editAction(action, objective) {
    this.selectedObjective = (objective) ? objective : this.selectedObjective;
    this.actionPlanFormGroup.patchValue(action);
    if (action.serviceplanpersoninvolved && Array.isArray(action.serviceplanpersoninvolved) && action.serviceplanpersoninvolved.length >0) {
      const persons = action.serviceplanpersoninvolved.map(item => item.personinvolved ? item.personinvolved : item);
      this.actionPlanFormGroup.patchValue({ personinvolved: persons });
    }
    (<any>$('#add-action')).modal('show');
  }

  getPersonName(id) {
    if (this.personList && Array.isArray(this.personList)) {
      const person = this.personList.find(item => item.personid === id);
      let name='';
      if(person)
        name = (person) ? (this.getFullName(person)) : '';
      return name;
    }
    return '';
  }

  /**
   * Needs and strengths
   */
  saveAssessmentStrengthNeeds() {
    // How do we handle cans-outofhome?
    const payload = {
      'objectid': this.intakeserviceid,
      'templatename': 'cansF',
      'status': 'accepted'
    };
    this._commonhttp.create(payload, 'serviceplan/saveAssessmentStrengthNeeds').subscribe(
      response => {
        this.getNeeds();
        this.getStrengths();
      });
  }


  getNeeds() {
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: {
        intakeserviceid : this.intakeserviceid
      }
    };
    this._commonhttp.getPagedArrayList(payload, 'serviceplanneed/list?filter').subscribe(
      response => {
        if (response && Array.isArray(response.data)) {
          this.cansFNeedsData = response.data.filter(x => x.assessmenttype === 'cansF');
          this.cansNeedsData = response.data.filter(x => x.assessmenttype === 'cansOutOfHomePlacementService');
          this.focusNeeds = response.data.map(x => x.serviceplanneedname);
          this.cansFNeedsList = response.data;
          //console.log("NEEDS", this.cansFNeedsList);
          if (this.cansFNeedsList.length) {
             this.isAssementCompleted = true;
          }
         }
      });
  }

  getStrengths() {
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: {
        intakeserviceid : this.intakeserviceid
      }
    };
    this._commonhttp.getPagedArrayList(payload, 'serviceplanstrength/list?filter').subscribe(
      response => {
        if (response && response.data && response.data.length) {
        this.cansFStrengthsData = response.data.filter(x => x.assessmenttype === 'cansF');
        this.cansStrengthsData = response.data.filter(x => x.assessmenttype === 'cansOutOfHomePlacementService');
        this.focusStrengths = response.data.map(x => x.serviceplanstrengthname);
        this.cansFStrengthList = response.data;
        //console.log("STRENGTHS", this.cansFStrengthList);

        if (this.cansFStrengthList.length) {
          this.isAssementCompleted = true;
        }

        }
      });
  }

  /**
   * Assignment & Routing
   */
  getRoutingUser(appoval, selectService) {
       this.approvalProcess = appoval;
       this.selectedPlan = selectService.serviceplanid;
       
       this.getUsersList = [];
       if (appoval === 'Pending') {
        this._commonhttp
        .getPagedArrayList(
            new PaginationRequest({
                where: { appevent:  'SPLAN' },
                method: 'post'
            }),
            'Intakedastagings/getroutingusers'
        )
        .subscribe(result => {
            this.getUsersList = result.data;
            this.getUsersList = this.getUsersList.filter(
                users => users.userid !== this._authService.getCurrentUser().user.securityusersid
            );
        });
      }  else {
        this.assignNewUser(selectService);
      }
  }

  selectPerson(row) {
    this.selectedPerson = row;
  }

  assignNewUser(selectedService) {
      
      // Instead of the service plan, the snapshot version is sent for approval 
      // selectedService.approvalstatustypekey = this.approvalProcess;
      
      this.selectedSnapshotVersion.approvalstatus = this.approvalProcess;

      const payload = {
        eventcode: 'SPLAN',
        tosecurityusersid: this.selectedPerson ? this.selectedPerson.userid : '',
        objectid: this.intakeserviceid,
        serviceNumber: this.daNumber,
        approvalstatustypekey: this.approvalProcess,
        serviceplanid: this.selectedPlan
      };
      this._commonhttp.create(
         payload,
        'serviceplan/serviceplanrouting'
      ).subscribe(
        (response) => {
          this._alertservice.success('Decision submitted successful!');
          // if (this.approvalProcess && this.approvalProcess.toLowerCase() === 'approved') {
          //       this.saveSnapshothist();
          // }
          this.updateSnapshotVersionStatus();
          (<any>$('#intake-caseassignnewX')).modal('hide');
        },
        (error) => {
          this._alertservice.error('Unable to submit decision!');
        });
    }

    saveSnapshothist(versionsnapshot) {
      const payload = {
        'objectid': this.selectedService.serviceplanid,
        'objecttype': 'SPLAN',
        'snapshotdata': versionsnapshot
      };
      this._commonhttp.create(payload, 'snapshothist').subscribe(
        response => {
            this.getHist();
        });
    }

    getHist() {
      this._commonhttp
      .getArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          order: 'insertedon desc',
          where: {
           objectid: this.selectedService.serviceplanid,
           objecttype: 'SPLAN'
           }
        }),
        'snapshothist' + '?filter'
      ).subscribe(
        response => {
          this.listSnapShot = response;
          this.personlistforsnapshot = [];
          this.listSnapShot.forEach(element => {
            this.personlistforsnapshot.push([]);
          });
       });
    }

  timecheck(objective) {
    if (objective && Array.isArray(objective.serviceplanaction)) {
      objective.serviceplanaction.map(item => {
        const startdate = new Date(item.startdate);
        const enddate = new Date(item.enddate);
        const currdate = new Date();
        const diff = Math.abs(enddate.getTime() - currdate.getTime());
        const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
        const actionDiff = Math.abs(enddate.getTime() - startdate.getTime());
        const nod = Math.ceil(actionDiff / (1000 * 3600 * 24));
        item.nod = nod;
        if (diffDays > 0 && diffDays < 30 && item.status === 'open') {
          item.alert = 1;
        } else if (diffDays >= 30 && diffDays < 60 && item.status === 'open') {
          item.alert = 2;
        } else if (diffDays >= 60 && item.status === 'open') {
          item.alert = 3;
        }
      });
    }
  }

  confirmDelete(type, item) {
    this.deletetype = type;
    this.deleteObject = item;
    (<any>$('#delete-popup')).modal('show');
  }

  deleteItem() {
    if (this.deletetype === 'service') {
      this.addServicePlan(0);
    } else if (this.deletetype === 'goal') {
      this.addGoal(0, this.deleteObject);
    } else if (this.deletetype === 'objective') {
      this.addFocusPlan(0, this.deleteObject);
    } else if (this.deletetype === 'action') {
      this.addActionPlan(0, this.deleteObject);
    }
  }

  isSpReadyForReview() {

    //Signature checks
    // && this.selectedService.serviceplansignatures['familymembersign']
    // && this.selectedService.serviceplansignatures['ihmcaseworkersign'] && this.selectedService.serviceplansignatures['oohcaseworkersign']

    if (this.selectedService && Array.isArray(this.selectedService.splangoal)) {
        const goals = this.selectedService.splangoal;
        if (goals.length > 0) {
            const o = goals.some(goal =>
              (!goal.approvalstatustypekey && (Array.isArray(goal.splanobjective) && goal.splanobjective.length > 0))
            );
            return o;
        } else {
          return false;
        }
      } else {
        return false;
      }
    // if (this.selectedService && Array.isArray(this.selectedService.serviceplanfocus)) {
    //   const focuses = this.selectedService.serviceplanfocus;
    //   if (focuses.length > 0) {
    //       const o = focuses.some(focus =>
    //         (!focus.approvalstatustypekey && (Array.isArray(focus.serviceplanaction) && focus.serviceplanaction.length > 0))
    //       );
    //       return o;
    //   } else {
    //     return false;
    //   }
    // } else {
    //   return false;
    // }
  }

  // YTP Stuff
  getYTPList() {
    return this._commonhttp.getArrayList(
        {
          page: 1,
          limit: 20,
          method: 'get',
          where: {
            intakeserviceid: this.intakeserviceid,
            approvalstatuskey: 'Approved'
          },
          order: 'insertedon desc'
        }, 'youthtransitionplan' + '?filter'
      ).subscribe(
        (response) => {
          this.ytpList = response;
          this.selectedYTP = null;
      });
  }

  selectYTP(item) {
    this.selectedYTP = item;
  }

  selectImportYTP() {
    (<any>$('#import-ytp')).modal('show');
  }

  importYTP() {
    const payload = {};
    payload['servicePlanID'] = this.selectedService.serviceplanid;
    payload['ytpID'] = this.selectedYTP.youthtransitionplanid;
    this._commonhttp.create(
      payload,
      'youthtransitionplan/importYTPtoSP'
    ).subscribe(
      response => {
        this.getServicePlans();
        this._alertservice.success('Youth Transition Plan data imported successfully.');
        (<any>$('#import-ytp')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in importing Youth Transition Plan.');
      }
    );
  }

  routeServiceCaseToCaseWorker(item: any) {
    (<any>$('#serviceCaseHistory')).modal('hide');
    (<any>$('#confirm-request-case')).modal('hide');
    this.storage.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
    if (item) {
        this._datastore.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, item.objecttypekey);
    }
    const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.servicecaseid + '/casetype';
    this._commonhttp.getAll(url).subscribe((response) => {
        const dsdsActionsSummary = response[0];
        if (dsdsActionsSummary) {
            this._datastore.setData('da_status', dsdsActionsSummary.da_status);
            this._datastore.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
            const currentUrl = '/pages/case-worker/' + item.servicecaseid + '/' + item.servicecasenumber + '/dsds-action/person-cw';
            // const currentUrl = '/pages/case-worker/' + item.caseid + '/' + item.servicecasenumber + '/dsds-action/involved-person';
            // this._router.navigate([currentUrl]);
            this._router.navigateByUrl('/', { skipLocationChange: true }).then(() => this._router.navigate([currentUrl]));
        }
    });
  }

  selectGoal(goal) {
    this.selectedGoal = goal;
  }

  selectObjective(objective) {
    this.selectedObjective = objective;
    this.actionPlanFormGroup.patchValue({status: 'open'});
  }

  // We are still waiting for the business requirement for these dropdown value
  // So for now just adding some values on the UI to show the ability
  getGoalReferenceValues() {
    this._commonDropdownService.getReferenveValuesByTypeIdandTeam('106', this._authService.getAgencyName() ).subscribe(
      (resultresp) => {
        this.goalReferenceValues = resultresp;
      });
  }

  getServicePlanNameReferenceValues() {
    this.servicePlanNameReferenceValues = [
        'Family service plan',
        'Individual Service Plan'
    ];
  }

  addOtherGoal() {
    this.isOtherGoal = true;
    this.servicePlanGoal.patchValue({goalname : 'Other'});
  }

  resetGoalsModal() {
      this.isOtherGoal = false;
      this.servicePlanGoal.reset();
  }

  //Status values
  //Goal to have 3 valid statuses: in progress, achieved, not achieved
  //Objective and Action to have 2 valid statuses: achieved or not achieved
  getStatusReferenceValues() {
    this.goalStatusReferenceValues = ['In Progress', 'Achieved', 'Not Achieved'];
    this.objectiveStatusReferenceValues = ['Achieved', 'Not Achieved'];
    this.actionStatusReferenceValues = ['Achieved', 'Not Achieved'];
  }

  setDefaultGoalFormValues() {
    this.servicePlanGoal.patchValue({
      status: 'In Progress'
    })
  }

  setDefaultObjectiveFormValues() {
    this.focusPlanFormGroup.patchValue({
      status: 'Not Achieved'
    })
  }

  setDefaultActionFormValues() {
    this.actionPlanFormGroup.patchValue({
      status: 'Not Achieved'
    })
  }

  setDefaultSnapshotFilterValues() {
    this.snapshotFilterFormGroup.patchValue({
      startdate: this.selectedService.effectivedate,
      enddate: this.selectedService.targetenddate
    });
  }

  /**
   * Snapshot work 
   * */
  createSnapshot() {
    
    const versionfilterstartdate = this.snapshotFilterFormGroup.get('startdate').value;
    const versionfilterenddate = this.snapshotFilterFormGroup.get('enddate').value;
    const splanversionpersoninvolved = this.snapshotFilterFormGroup.get('personlist').value;
    if (splanversionpersoninvolved) {
      this.childList.forEach(child => {
      if(splanversionpersoninvolved.indexOf(child.personid) > -1) {
        if (this.childrenHeader !== '') {
          this.childrenHeader += ',';
        }
        this.childrenHeader = child.name;
      }
      });
    }
    this.selectedService.childrenHeader = this.childrenHeader;
    this.selectedService.legalGuardian = this.legalGuardian;
    const versionsnapshot = _.cloneDeep(this.selectedService);
    const versiongoal = [];
    const versionvisitation = [];
    const goals = Array.isArray(versionsnapshot.splangoal) ? versionsnapshot.splangoal : [];
    const visitations = Array.isArray(versionsnapshot.visitationplans) ? versionsnapshot.visitationplans : [];
    goals.forEach(goal => {
      const objectives = Array.isArray(goal.splanobjective) ? goal.splanobjective : [];
      const versionobjective = [];
      this.countobj = 0;
      objectives.forEach(objective => {
        const actions = Array.isArray(objective.serviceplanaction) ? objective.serviceplanaction : [];
        const versionactions = [];
        actions.forEach(action => {
          this.count = 0;
          action.serviceplanpersoninvolved.forEach(serviceplanperinvolved => {
            if (splanversionpersoninvolved.indexOf(serviceplanperinvolved.personinvolved) > -1) {
              this.count++;
            }
          });
          if (this.count > 0 && new Date(versionfilterstartdate) <= new Date(action.startdate) && new Date(versionfilterenddate) >= new Date(action.enddate)) {
            versionactions.push(action);
          }
        });
        if (versionactions.length > 0) {
          objective.serviceplanaction = versionactions;
          versionobjective.push(objective);
        } else {
          objective = {};
          this.countobj++;
        }
      });
      if (this.countobj === objectives.length) {
        goal = {};
      } else {
        goal.splanobjective = versionobjective;
        versiongoal.push(goal);
      }
    });
    versionsnapshot.splangoal = versiongoal;
    visitations.forEach(visitation => {
      if (splanversionpersoninvolved.indexOf(visitation.personid) > -1) {
        let temp = this.frequencyofplannedvisits.find(x => x.ref_key === visitation.visitfrequencytypekey);
        visitation.visitfrequencytypekey = (temp) ? (temp.value_text) : '';
        temp = this.lengthofplannedvisit.find(x => x.ref_key === visitation.visitdurationtypekey);
        visitation.visitdurationtypekey = (temp) ? (temp.value_text) : '';
        temp = this.childandvisitortransportation.find(x => x.ref_key === visitation.childtransportationtypekey);
        visitation.childtransportationtypekey = (temp) ? (temp.value_text) : '';
        temp = this.childandvisitortransportation.find(x => x.ref_key === visitation.visitortransportationtypekey);
        visitation.visitortransportationtypekey = (temp) ? (temp.value_text) : '';
        versionvisitation.push(visitation);
      }
    });
    versionsnapshot.visitationplans = versionvisitation;
    versionsnapshot.personsinvolved = this.personList.filter( x => splanversionpersoninvolved.includes(x.personid));
    this.saveSnapshothist(versionsnapshot);
  }

  setSelectedSnapshotVersion(item) {
    this.selectedSnapshotVersion = item;
  }

  updateSnapshotVersionStatus() {
    const payload = {};
    payload['id'] = this.selectedSnapshotVersion.id;
    payload['approvalstatus'] = this.approvalProcess;

    this._commonhttp.patch(
      this.selectedSnapshotVersion.id,
      payload,
      'snapshothist'
    ).subscribe(
      response => {
        this._alertservice.success('Service plan version decision submitted successfully!');
      },
      error => {
        this._alertservice.error('Error in submitting service plan version decision!');
      }
    );
  }


  /**
   * Signature
   */
  getSignaturePersonTypeReferenceValues() {
    this.signaturePersonTypes = [
      {key: 'CWIHM', value: 'Case Worker (In-home Services)'},
      {key: 'CWOOH', value: 'Case Worker (Out-of-home Services)'},
      {key: 'FAM', value: 'Family Member'},
      {key: 'SUP', value: 'Supervisor'}
    ];
  }

  getSignaturePersonTypeByKey(key) {
    return this.signaturePersonTypes.find(item => item.key === key);
  }

  saveSignatureDetails() {
    this.personSignatureFormGroup.get('persontype').setValue(
      this.getSignaturePersonTypeByKey(
        this.personSignatureFormGroup.get('persontypekey').value
      ).value
    ); 

    this.selectedSignaturesList.push(this.personSignatureFormGroup.value)
    
    const payload = {};
    payload['id'] = this.selectedSnapshotVersion.id;
    payload['signatures'] = this.selectedSignaturesList;
    
    this._commonhttp.patch(
      this.selectedSnapshotVersion.id,
      payload,
      'snapshothist'
    ).subscribe(
      response => {
        // this.getServicePlans();
        this.resetSignatureCapture();
        this._alertservice.success('Signatures captured successfully!');
        (<any>$('#capture-signature')).modal('hide');
        (<any>$('#signatures-list')).modal('show');
      },
      error => {
        this._alertservice.error('Error in capturing signatures!');
      }
    );
  }

  setSignaturesList(item) {
    this.setSelectedSnapshotVersion(item);
    this.selectedSignaturesList = [];
    if (item && item.signatures) {
      this.selectedSignaturesList = item.signatures;
    }
  }

  public clearSignature(): void {
    this.signaturePad.clear();
  }

  resetSignatureCapture() {
    this.clearSignature();
    this.personSignatureFormGroup.reset();
  }

  // getReferenceValues(referencetype) {
  //   return this._commonhttp.getArrayList(
  //     {
  //       nolimit: true,
  //       where: { referencetypeid: referencetype}, 
  //       method: 'get'
  //     },
  //     'referencevalues?filter'
  //   )
  // }

  // setReferenceValues() {
  //    this.getReferenceValues(123).subscribe(
  //     (resultresp) => {
  //       console.log(resultresp, 'resultrespresultresp');
  //       this.goalReferenceValues = resultresp;
  //       });
  // }
}
