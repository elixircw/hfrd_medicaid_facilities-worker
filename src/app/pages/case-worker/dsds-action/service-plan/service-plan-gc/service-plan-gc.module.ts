import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicePlanGcRoutingModule } from './service-plan-gc-routing.module';
import { ServicePlanGcComponent } from './service-plan-gc.component';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ServicePlanGcRoutingModule,
    FormMaterialModule,
    MatTooltipModule
  ],
  declarations: [ServicePlanGcComponent]
})
export class ServicePlanGcModule { }
