import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicePlanGcComponent } from './service-plan-gc.component';

describe('ServicePlanGcComponent', () => {
  let component: ServicePlanGcComponent;
  let fixture: ComponentFixture<ServicePlanGcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicePlanGcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicePlanGcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
