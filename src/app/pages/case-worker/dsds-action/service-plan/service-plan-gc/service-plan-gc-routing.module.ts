import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicePlanGcComponent } from './service-plan-gc.component';

const routes: Routes = [{
  path:'', component: ServicePlanGcComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicePlanGcRoutingModule { }
