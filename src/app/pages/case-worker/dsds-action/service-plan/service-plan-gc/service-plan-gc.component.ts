import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
import { CansFConfig } from './service-plan-gc-config';
import { ServicePlanService, ServicePlanFocus, ServicePlanAction } from '../../../_entities/caseworker.data.model';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import * as jsPDF from 'jspdf';
import { IntakeServiceRequest } from '../../cross-reference/_entities/crossreference.data.models';
import { IntakeUtils } from '../../../../_utils/intake-utils.service';
import { SignatureFieldComponent } from '../../../../provider-management/license-management/license-change-request/signature-field/signature-field.component';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../../../../app.config';

@Component({
  selector: 'service-plan-gc',
  templateUrl: './service-plan-gc.component.html',
  styleUrls: ['./service-plan-gc.component.scss']
})
export class ServicePlanGcComponent implements OnInit {
  @ViewChild(SignatureFieldComponent) public signaturePad: SignatureFieldComponent;
  percentageCompleted = 0;
  actionPersonList: any[];
  approvalProcess: string;
  selectedItem: string;
  selectedPlan: string;
  list: ServicePlanService[] = [];
  selectedList: ServicePlanService[] = [];
  idx: number;
  serviceplanstartdate: any;
  serviceplanenddate: any;
  selectedServiceList = [];
  listSnapShot = [];
  selectedActionList = [];
  selectedFocusList = [];
  selectedOutcomeList = [];
  servicePlanFormGroup: FormGroup;
  servicePlanForm: FormGroup;
  focusPlanFormGroup: FormGroup;
  actionPlanFormGroup: FormGroup;
  candidacyFormGroup: FormGroup;
  outcomePlanFormGroup: FormGroup;
  selectedService: ServicePlanService;
  backupService: ServicePlanService;
  currSelectedService: ServicePlanService;
  selectedAction: ServicePlanAction;
  selectedFocus: ServicePlanFocus;
  signatureFormGroup: FormGroup;
  initialClick: boolean;
  getUsersList = [];
  selectedPerson: any;
  cansfData: any;
  intakeserviceid = '';
  daNumber = '';
  cansFNeedsList: any[];
  cansFStrengthList: any[];
  sectionList: string[] = [];
  personList: any[];
  personListName: any;
  personDataList: any[];
  childList: any[] = [];
  childCandidates: any[] = [];
  childrenHeader: String = '';
  legalGuardian = '';
  caseWorker = '';
  caseWorkerPh: any = null;
  user: AppUser;

  cansFNeedsData: any[];
  cansFStrengthsData: any[];
  cansNeedsData: any[];
  cansStrengthsData: any[];

  focusNeeds: any[];
  focusStrengths: any[];
  isAssementCompleted: boolean;
  totalActions: number;
  completedAction: number;
  deletetype: any;
  deleteObject: any;
  selectedOutCome: any;
  baseUrl: string;

  // Visitation Plan
  selectedServiceVisitationPlans: any [] = [];
  visitationPlansFormGroup: FormGroup;

  // YTP Variables
  ytpList: any[] = [];
  selectedYTP: any;

  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
  selected: boolean;

  constructor(private _formBuilder: FormBuilder, private _commonhttp: CommonHttpService,
    private _alertservice: AlertService, private _datastore: DataStoreService,
    private _intakeUtils: IntakeUtils,
    private _authService: AuthService,
    private http: HttpClient, ) {
      this.baseUrl = AppConfig.baseUrl;
    }

  ngOnInit() {
    this.selected = false;
    this.cansFNeedsList = [];
    this.cansFStrengthList = [];
    this.intakeserviceid = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.personDataList = [];

    // TODO: Decide when or where to call this DB persistence of Assessments
    // this.saveAssessmentStrengthNeeds();
    this.daNumber = this._datastore.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    this.user = this._authService.getCurrentUser();
    if (this.user.role.id === '71') {
      this.caseWorker = this.user.user.userprofile.displayname;
      this.caseWorkerPh = this.user.user.userprofile.userprofilephonenumber.length > 0 ? this.user.user.userprofile.userprofilephonenumber[0].phonenumber : '';
    }
    this.getInvolvedPerson();
    this.getPersonListWithProgramAssignment();
    this.getServicePlans();
    this.getNeeds();
    this.getStrengths();
    this.selectedItem = 'Empty';
    this.menubar();
    this.initForms();
    this.getYTPList();

    setTimeout(() => {
      this.initialClick = true;
      (<any>$('#level_0')).click();
      this.selectedItem = 'Empty';
    }, 100);

    this.isAssementCompleted = false;
  }

  initForms() {
    this.servicePlanFormGroup = this._formBuilder.group({
      serviceplanname: [''],
      effectivedate: [null, Validators.required],
      serviceplanid: [null],
      enddate: [null],
      numberofdays: [null],
      activeflag: [1]
    });
    this.servicePlanForm = this._formBuilder.group({
     enddate: [null]
    });
    this.focusPlanFormGroup = this._formBuilder.group({
      focusname: [''],
      serviceplanneed: [[]],
      serviceplanstrength: [[]],
      serviceplanfocusid: [null],
      otherneeds: [null],
      otherstrength: [null]
    });
    this.actionPlanFormGroup = this._formBuilder.group({
      serviceplanactionname: [''],
      serviceplanactionid: [''],
      enddate: [null, Validators.required],
      startdate: [null, Validators.required],
      status: ['open'],
      // plantype: [null],
      planfor: ['In Home'],
      personresponsible: ['', Validators.required],
      // serviceplanpersoninvolved: [null, Validators.required]
      personinvolved: [null, Validators.required]
    });
    this.outcomePlanFormGroup = this._formBuilder.group({
      serviceplanoutcomename: ['', Validators.required],
      actualserviceplanoutcomename: ['']
    });

    this.initVisitationPlansFormGroup();
    this.initCandidacyForm();
    this.initSignatureFormGroup();
  }

  /**
   * Visitation Plans
   */
  initVisitationPlansFormGroup() {
    this.visitationPlansFormGroup = this._formBuilder.group({
      visitationplans: this._formBuilder.array([])
    });
  }

  initVisitationFormGroup() {
    return this._formBuilder.group({
      personinvolved: [null, Validators.required],
      frequency: [null],
      length: [null],
      location: [''],
      conditions: [''],
      childtransportation: [''],
      visitortransportation: ['']
    });
  }

  openAddVisitationModal() {
    if (this.selectedService && this.selectedService.serviceplanvisitation) {
      this.visitationPlansFormGroup.patchValue(this.selectedService.serviceplanvisitation);
      // TODO: SIMAR do we need the array ?
      // this.selectedServiceVisitationPlans = this.selectedService.serviceplanvisitation['plans'];
    }
    (<any>$('#add-visitation')).modal('show');
  }

  addVisitationPlan() {
    const control = <FormArray>this.visitationPlansFormGroup.controls.visitationplans;
    control.push(this.initVisitationFormGroup());
  }

  saveVisitationPlans() {
    const payload = {};
    payload['serviceplanid'] = this.selectedService.serviceplanid;
    payload['serviceplanvisitation'] = this.visitationPlansFormGroup.value; // this.visitationFormGroup.value;
    this._commonhttp.patch(
      this.selectedService.serviceplanid,
      payload,
      'serviceplan'
    ).subscribe(
      response => {
        // this.getServicePlans();
        this.selectedService.serviceplanvisitation = this.visitationPlansFormGroup.value;
        this._alertservice.success('Visitation details entered successfully!');

        // TODO: SIMAR do I need this ?
        // this.visitationPlansFormGroup.reset();
        // (<any>$('#add-visitation')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in entering visitation details!');
      }
    );
  }


  /**
   * Candidacy
   */
  initCandidacyForm() {
    this.candidacyFormGroup = this._formBuilder.group({
      candidates: this._formBuilder.array([])
    });
  }

  updateCandidacy() {
    const control = <FormArray>this.candidacyFormGroup.controls.candidates;
    const actionPersonList = (this.actionPersonList && Array.isArray(this.actionPersonList)) ? this.actionPersonList : [];
    // const personList =  actionPersonList.filter(item => 
    //   item.programkey === 'IHSFP' && item.role === 'CHILD');
    const personList =  this.childList.filter(item => {
      if (item && item.programarea) {
        item.programarea.map(element => {
          element.programkey === 'IHSFP' || element.programkey === 'CPS';
        });
      }
      });
    personList.forEach(child => {
      control.push(this._formBuilder.group({
        id: child.id ? child.id : '',
        name: child.name ? child.name : '',
        candidacy: [null]
        })
      );
    });
  }

  openCandidacyModal() {
    if (this.selectedService && this.selectedService.serviceplancandidacy) {
      this.candidacyFormGroup.patchValue(this.selectedService.serviceplancandidacy);
    }
    (<any>$('#candidacy')).modal('show');
  }

  addCandidacy() {
    const payload = {};
    payload['serviceplanid'] = this.selectedService.serviceplanid;
    payload['serviceplancandidacy'] = this.candidacyFormGroup.value;
    this._commonhttp.patch(
      this.selectedService.serviceplanid,
      payload,
      'serviceplan'
    ).subscribe(
      response => {
        // this.getServicePlans();
        this.selectedService.serviceplancandidacy = this.candidacyFormGroup.value;
        this.candidacyFormGroup.reset();
        this._alertservice.success('Candidacy information entered successfully!');
        (<any>$('#candidacy')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in entering Candidacy information!');
      }
    );
  }

  /**
   * Signatures
   */
  initSignatureFormGroup() {
    this.signatureFormGroup = this._formBuilder.group({
      ihmcaseworkersign: [null],
      ihmcaseworkersigndate: [null],
      ihmcaseworkername: [null],
      // ihmcwuploadsign: [null],
      // ihmcwrefusesign: [null],
      oohcaseworkersign: [null],
      oohcaseworkersigndate: [null],
      oohcaseworkername: [null],
      // oohcwuploadsign: [null],
      // oohcwrefusesign: [null],
      familymembersign: [null],
      familymembersigndate: [null],
      familymembername: [null],
      familymemuploadsign: [null],
      familymemrefusesign: [null],
      supervisorsign: [null],
      supervisorsigndate: [null],
      supervisorname: [null],
      // supervisoruploadsign: [null],
      // supervisorrefusesign: [null]
    });
  }

  async downloadCasePdf(val) {
    const pages = document.getElementsByClassName('pdf-page');
    let pageImages = [];
    for (let i = 0; i < pages.length; i++) {
      const pageName = pages.item(i).getAttribute('data-page-name');
      const isPageEnd = pages.item(i).getAttribute('data-page-end');
      if (val === 'ihm') {
        if (pageName === 'In Home Service Plan') {
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
            const img = canvas.toDataURL('image/png');
            pageImages.push(img);
          });
        }
      } else {
        if (pageName === 'Out of Home Service Plan') {
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
            const img = canvas.toDataURL('image/png');
            pageImages.push(img);
          });
        }
      }
    }

    this.pdfFiles.push({ fileName: val === 'ihm' ? 'In Home Service Plan' : 'Out of Home Service Plan', images: pageImages });
    pageImages = [];
    this.convertImageToPdf();
  }

  convertImageToPdf() {
    this.pdfFiles.forEach((pdfFile) => {
        let doc = null;
        doc = new jsPDF();
        const width = doc.internal.pageSize.getWidth() - 10;
        const heigth = doc.internal.pageSize.getHeight() - 10;

        pdfFile.images.forEach((image, index) => {

          doc.addImage(image, 'PNG', 3, 5, width, heigth);
          if (pdfFile.images.length > index + 1) {
              doc.addPage();
          }
        });
        doc.save(pdfFile.fileName);
    });
    (<any>$('#servicePlanIhmPrint')).modal('hide');
    (<any>$('#servicePlanOohPrint')).modal('hide');
    (<any>$('#youthTransPlan')).modal('hide');
    this.pdfFiles = [];
  }

  async downloadYTPPdf() {
    const pages = document.getElementsByClassName('pdf-page');
    let pageImages = [];
    for (let i = 0; i < pages.length; i++) {
      const pageName = pages.item(i).getAttribute('data-page-name');
      if (pageName === 'Youth Transitional Plan') {
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
            const img = canvas.toDataURL('image/png');
            pageImages.push(img);
          });
        }
    }

    this.pdfFiles.push({ fileName: 'Youth Transitional Plan', images: pageImages });
    pageImages = [];
    this.convertImageToPdf();
  }

  youthTransPlanPrint() {
    (<any>$('#youthTransPlan')).modal('show');
  }

  servicePlanIhmPrint(item) {
    this.backupService = this.selectedService ? this.selectedService : null;
    this.selectedService = item;
    (<any>$('#servicePlanIhmPrint')).modal('show');
  }

  viewDownload(item) {
    this.backupService = this.selectedService ? this.selectedService : null;
    this.selectedService = item;
    this.getHist();
    (<any>$('#add-snapshot')).modal('show');
  }


  documentGenerate(item, typeData) {
    const modal = {
      count: -1,
      where: {
        documenttemplatekey: ['inhome'],
        id: item.id,
        type: typeData,
        // date_sw: childReport.date_sw,
        // date_from: childReport.date_from,
        // date_to: childReport.date_to,
        format: 'pdf'
      },
      method: 'post'
    };
      this._commonhttp.download('evaluationdocument/generateintakedocument', modal)
        .subscribe(res => {
          const blob = new Blob([new Uint8Array(res)]);
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          link.download = 'inHome.pdf';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
    });
  }



  servicePlanOohPrint(item) {
    this.backupService = this.selectedService ? this.selectedService : null;
    this.selectedService = item;
    (<any>$('#servicePlanOohPrint')).modal('show');
  }

  closeView() {
    this.selectedService = this.backupService ? this.backupService : null;
  }

  onSelectedChange(event) {
    console.log(event);
  }

  selectPlan(item, index) {
    this.selected = true;
    this.idx = index;
    this.selectedList = [];
    this.list.forEach(i => {
      i.isChecked = false;
    });
    item.isChecked = true;
    this.selectedList.push(item);
    this.selectService(item, index);
  }

  selectService(service, index: number) {
    this.selectedItem = 'service';
    this.selectedService = service;
    this.selectedService.approvalstatustypekey = service.approvalstatustypekey;
    this.selectedFocus = null;
    this.selectedAction = null;
    this.calculation(this.selectedService);
    this.selectedFocusList = service.focus;
    this.initCandidacyForm();
    if (this.selectedService.serviceplancandidacy) {
      this.updateCandidacy();
      this.candidacyFormGroup.patchValue(this.selectedService.serviceplancandidacy);
      this.childCandidates = this.candidacyFormGroup['controls'].candidates['controls'];
    } else {
      this.updateCandidacy();
    }
    setTimeout(() => {
      if (this.selectedFocusList && !this.initialClick) {
        this.selectedFocusList.forEach((element, subIndex) => {
          console.log(`#level_${index}_${subIndex}`);
          (<any>$(`#level_${index}_${subIndex}`)).click();
          this.selectedItem = 'service';
        });
      }
      this.initialClick = false;
    }, 10);
    this.selectedService['isReady'] = this.isSpReadyForReview();
  }
  selectAction(action, focus, service) {
    this.selectedItem = 'action';
    this.selectedAction = action;
    this.selectedService = service;
    this.selectedFocus = focus;
    this.selectedOutcomeList = action.serviceplanoutcome;
    this.outcomePlanFormGroup.patchValue(action.serviceplanoutcome);
  }

  selectFocus(focus, service) {
    this.selectedItem = 'focus';
    this.selectedFocus = focus;
    this.selectedAction = null;
    this.selectedService = service;
    this.selectedActionList = focus.action;
    if (focus.serviceplanneed && Array.isArray(focus.serviceplanneed)) {
      const serviceplanneedOne = focus.serviceplanneed.map(item => item.serviceplanneedname ? item.serviceplanneedname : item);
      this.selectedFocus.serviceplanneed = serviceplanneedOne;
    }
    if (focus.serviceplanstrength && Array.isArray(focus.serviceplanstrength)) {
      const serviceplanstrengthOne = focus.serviceplanstrength.map(item => item.serviceplanstrengthname ? item.serviceplanstrengthname : item);
      this.selectedFocus.serviceplanstrength = serviceplanstrengthOne;
    }
  }

  menubar() {
    $(function () {
      // $('.tree li.parent_li > span').attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
      $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
      $('.tree li.parent_li > span').on('click', function (e) {
        const children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(':visible')) {
          children.hide('fast');
          $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
          children.show('fast');
          $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
      });
    });
  }

  closeServicePlan() {
    this.calculation(this.selectedService);
    if (this.percentageCompleted !== 100) {
      this._alertservice.error('Please close all action for this service plan.');
      return;
    }
    const service =  this.servicePlanForm.getRawValue();
    service.serviceplanid = this.selectedService.serviceplanid;
    this._commonhttp.patch(
      this.selectedService.serviceplanid,
      service,
      'serviceplan'
    ).subscribe(
      response => {
        this.getServicePlans();
        this.servicePlanForm.reset();
        (<any>$('#close-service')).modal('hide');
       },
      error => {
        this._alertservice.error('Error in creating Outcome.');
      }
    );
  }

  addServicePlan(activeflag) {
    const service = (activeflag === 1) ? this.servicePlanFormGroup.getRawValue() : this.selectedService;
    // service['title'] = service.serviceplanname;
    service.objectid = this.intakeserviceid;
    service.activeflag = activeflag;
    // this.list.push(service);
    this.servicePlanFormGroup.reset();
    this._commonhttp.create(service, 'serviceplan/addupdate').subscribe(
      response => {
        // service.serviceplanchildid = response.serviceplanchildid;
        // this.list.push(service);
        this.selectedItem = (activeflag === 1) ? 'service' : 'Empty';
        delete response.where;
        // this.selectedService = response;
        this.getServicePlans();
        if (activeflag === 1) {
          this._alertservice.success('Service plan is created successfully.');
        } else {
          this._alertservice.success('Service plan is deleted successfully.');
        }
        (<any>$('#add-service')).modal('hide');
        (<any>$('#delete-popup')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in creating Service plan.');
        (<any>$('#delete-popup')).modal('hide');
      }
    );
  }

  addFocusPlan(activeflag, focusPlan?: ServicePlanFocus) {
    // const focus = this.focusPlanFormGroup.getRawValue();
    const focus = (activeflag === 1) ? this.focusPlanFormGroup.getRawValue() : focusPlan;
    focus.activeflag = activeflag;
    focus.objectid = this.intakeserviceid;
    focus.serviceplanid = this.selectedService.serviceplanid;
    this.selectedService.serviceplanfocus = (this.selectedService.serviceplanfocus && this.selectedService.serviceplanfocus.length) ? this.selectedService.serviceplanfocus : [];
    focus.serviceplanneed = (focus && Array.isArray(focus.serviceplanneed)) ? focus.serviceplanneed : [];
    focus.serviceplanstrength = (focus && Array.isArray(focus.serviceplanstrength)) ? focus.serviceplanstrength : [];
    const otherneedsExist = focus.serviceplanneed.includes('other');
    const otherstrengthsExist = focus.serviceplanstrength.includes('other');
    if (otherneedsExist) {
      focus.serviceplanneed = focus.serviceplanneed.filter(item => item !== 'other');
      focus.serviceplanneed.push(focus.otherneeds);
      this.cansFNeedsList.push({serviceplanneedname: focus.otherneeds});
    }
    if (otherstrengthsExist) {
      focus.serviceplanstrength = focus.serviceplanstrength.filter(item => item !== 'other');
      focus.serviceplanstrength.push(focus.otherstrength);
      this.cansFStrengthList.push({serviceplanstrengthname: focus.otherstrength});
    }
    focus.intakeserviceid = this.intakeserviceid;
    // this.selectedService.focus.push(focus);
    // this.focusPlanFormGroup.reset();
    this._commonhttp.create(focus, 'serviceplanfocus/addupdate').subscribe(
      response => {
        focus.serviceplanfocusid = response.serviceplanfocusid;

        const objectFocus =  this.selectedService.serviceplanfocus.filter(fdata =>
          fdata.serviceplanfocusid === response.serviceplanfocusid
        );
        if (objectFocus && objectFocus.length === 0) {
           this.selectedService.serviceplanfocus.unshift(focus);
        } else {
          for (const i in this.selectedService.serviceplanfocus) {
            if (this.selectedService.serviceplanfocus[i].serviceplanfocusid === response.serviceplanfocusid) {

              if (focus.activeflag === 1) {
                 this.selectedService.serviceplanfocus[i] = focus;
                 break;
               } else {
                  this.selectedService.serviceplanfocus.splice(focus, 1);
                  break;
               }
               // Stop this loop, we found it!
            }
          }
        }
        this.focusPlanFormGroup.reset();
        const message = (activeflag === 1) ? 'Focus is updated successfully.' : 'Focus is deleted successfully.';
        this._alertservice.success(message);
        this.selectPlan(this.selectedList[0], this.idx);
        (<any>$('#delete-popup')).modal('hide');
      },
      error => {
        this.focusPlanFormGroup.reset();
        const message = (activeflag === 1) ? 'Error in updating Focus.' : 'Error in deleting Focus.';
        this._alertservice.error(message);
        (<any>$('#delete-popup')).modal('hide');
      }
    );
  }
  setValidate(check) {
    // if(check.value==='In Home') {
    //   this.actionPlanFormGroup.get('plantype').clearValidators();
    // } else {
    //   this.actionPlanFormGroup.get('plantype').setValidators(Validators.required);
    // }

  }

  cleanSelection() {
    this.selectedOutCome = '';
  }

  compareNeedsObjects(o1: any, o2: any): boolean {
    return o1.serviceplanneedname === o2.serviceplanneedname;
  }

  getPlanByPersonID(personId) {
    if (this.actionPersonList && Array.isArray(this.actionPersonList)) {
      const person = this.actionPersonList.find(item => item.personid === personId[0]);
      const plan = (person) ? (person.programkey) : '';
      return plan;
    }
    return '';
  }

  addActionPlan(activeflag, actionPlan?: ServicePlanAction) {
    const action = (activeflag === 1) ? this.actionPlanFormGroup.getRawValue() : actionPlan;
    action.serviceplanactionid = (action.serviceplanactionid !== '') ? action.serviceplanactionid : null;
    action.serviceplanfocusid = (this.selectedFocus) ? this.selectedFocus.serviceplanfocusid : null;
    action.objectid = this.intakeserviceid;
    action.activeflag = activeflag;
    action.serviceplanpersoninvolved = action.personinvolved;
    action.planfor = this.getPlanByPersonID(action.personinvolved);
    action.personinvolved = action.personinvolved;
    // this.selectedFocus.serviceplanaction = (this.selectedFocus.serviceplanaction && this.selectedFocus.serviceplanaction.length) ? this.selectedFocus.serviceplanaction : [];
    // this.selectedFocus.serviceplanaction.push(action);
    this.actionPlanFormGroup.reset();
    this.actionPlanFormGroup.patchValue({status: 'open'});
    // this.actionPlanFormGroup.get('plantype').clearValidators();
    // this.actionPlanFormGroup.get('plantype').updateValueAndValidity();
    this._commonhttp.create(action, 'serviceplanaction/addupdate').subscribe(
      response => {
       // this.getServicePlans();
        const message = (activeflag === 1) ? 'Action is created successfully.' : 'Action is deleted successfully.';
        if (this.selectedFocus.serviceplanaction && this.selectedFocus.serviceplanaction.length) {

          const objectAction = this.selectedFocus.serviceplanaction.filter(action =>
            action.serviceplanactionid === response.serviceplanactionid
          );
          if (objectAction && objectAction.length === 0) {
            action.serviceplanactionid = response.serviceplanactionid;
            this.selectedFocus.serviceplanaction.unshift(action);
          } else {
            for (const i in this.selectedFocus.serviceplanaction) {
              if (this.selectedFocus.serviceplanaction[i].serviceplanactionid === response.serviceplanactionid) {
                 action.serviceplanactionid = response.serviceplanactionid;
                if (action.activeflag === 1) {
                   this.selectedFocus.serviceplanaction[i] = action;
                   break;
                 } else {
                    this.selectedFocus.serviceplanaction.splice(action, 1);
                    break;
                 }
                 // Stop this loop, we found it!
              }
            }
          }

        } else {
          this.selectedFocus.serviceplanaction = [];
          this.selectedFocus.serviceplanaction.unshift(response);
        }
        this._alertservice.success(message);
        (<any>$('#add-action')).modal('hide');
        (<any>$('#delete-popup')).modal('hide');
      },
      error => {
        const message = (activeflag === 1) ? 'Error in updating Action.' : 'Error in deleting Action.';
        this._alertservice.error(message);
        this.actionPlanFormGroup.reset();
        this.actionPlanFormGroup.patchValue({ status: 'open' });
        // this.actionPlanFormGroup.get('plantype').clearValidators();
        // this.actionPlanFormGroup.get('plantype').updateValueAndValidity();
        (<any>$('#delete-popup')).modal('hide');
      }
    );
  }
  addOutcome() {
    this.selectedOutCome = '';
    const newoutcome = this.outcomePlanFormGroup.getRawValue().serviceplanoutcomename;
    this.selectedAction.outcomes = (    this.selectedAction.serviceplanoutcome &&     this.selectedAction.serviceplanoutcome.length) ?     this.selectedAction.serviceplanoutcome : [];
    const payload = {
      'serviceplanoutcomeid': null,
      'serviceplanactionid': this.selectedAction.serviceplanactionid,
      'serviceplanoutcomename': newoutcome,
      // 'serviceplanoutcomename': [...this.selectedAction.outcomes, newoutcome],
      objectid: this.intakeserviceid
    };

    this._commonhttp.create(payload, 'serviceplanoutcome/').subscribe(
      response => {
        // this.selectedFocus.action.push(newoutcome);

        if (this.selectedAction.serviceplanoutcome && this.selectedAction.serviceplanoutcome.length) {
          this.selectedAction.serviceplanoutcome.unshift(response);
        } else {
          this.selectedAction.serviceplanoutcome = [];
          this.selectedAction.serviceplanoutcome.unshift(response);
        }
        this.outcomePlanFormGroup.reset();
        this._alertservice.success('Outcome is created successfully.');
        (<any>$('#add-outcome')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in creating Outcome.');
      }
    );
  }

  patchData(payload, deleteFlag) {
   if (deleteFlag === 'Y') {
      payload.activeflag = 0;
   } else {
     const newoutcome = this.outcomePlanFormGroup.getRawValue().serviceplanoutcomename;
     const actualoutcome = this.outcomePlanFormGroup.getRawValue().actualserviceplanoutcomename;
     if (newoutcome) {
      payload.serviceplanoutcomename = newoutcome;
     }

     payload.actualserviceplanoutcomename = actualoutcome;

   }
   this._commonhttp.patch(
      payload.serviceplanoutcomeid,
      payload,
      'serviceplanoutcome'
    ).subscribe(
      response => {
        for (const i in this.selectedAction.serviceplanoutcome) {
          if (this.selectedAction.serviceplanoutcome[i].serviceplanoutcomeid === response.serviceplanoutcomeid) {
            if (response.activeflag === 1) {
               this.selectedAction.serviceplanoutcome[i] = response;
               break;
             } else {
                this.selectedAction.serviceplanoutcome.splice(response, 1);
                break;
             }
             // Stop this loop, we found it!
          }
        }
        this.outcomePlanFormGroup.reset();
        if (payload.activeflag === 0) {
          this._alertservice.success('Outcome is deleted successfully.');
        } else {
          this._alertservice.success('Outcome is updated successfully.');
          (<any>$('#add-outcome')).modal('hide');
        }

      },
      error => {
        this._alertservice.error('Error in creating Outcome.');
      }
    );
  }

  openAddServiceModal() {
    //this.selectService = null;
    if (this.cansFNeedsList && this.cansFNeedsList.length === 0) {
      // this._alertservice.warn('Please fill CANS-F and CANS assessment');
    }
    (<any>$('#add-service')).modal('show');
  }

  captureSign(item) {
    this.currSelectedService = item;
    if (this.currSelectedService && this.currSelectedService.serviceplansignatures) {
      this.signatureFormGroup.patchValue(this.currSelectedService.serviceplansignatures);
    }
    (<any>$('#signature')).modal('show');
  }

  signServicePlan() {
    const payload = {};
    payload['serviceplanid'] = this.currSelectedService.serviceplanid;
    payload['serviceplansignatures'] = this.signatureFormGroup.value;
    this._commonhttp.patch(
      this.currSelectedService.serviceplanid,
      payload,
      'serviceplan'
    ).subscribe(
      response => {
        this.getServicePlans();
        this.signatureFormGroup.reset();
        this._alertservice.success('Signatures captured successfully!');
        (<any>$('#signature')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in capturing signatures!');
      }
    );
  }

  getServicePlans() {
    const payload = {
      method: 'get',
      where: {
        caseid : this.intakeserviceid
      }
    };
    this._commonhttp.getArrayList(payload, 'serviceplan/listbyallrelation?filter').subscribe(
      response => {
        if (this.user.role.name !== 'field') {
          this.list = response.filter((item) =>  item.approvalstatustypekey !== null && item.approvalstatustypekey !== '');
        if (this.list && this.list.length) {
          this.serviceplanstartdate = this.list[0].effectivedate;
          this.serviceplanenddate = this.list[0].enddate;
          this.list.forEach((plan) => {
            this.selectedService = plan;
            plan['isReady'] = this.isSpReadyForReview();
            plan.isChecked = false;
          });
          this.selectedService = null;
        }
        } else {
          this.list = response;
          if (this.list && this.list.length) {
            this.serviceplanstartdate = this.list[0].effectivedate;
            this.serviceplanenddate = this.list[0].enddate;
            this.list.forEach((plan) => {
              this.selectedService = plan;
              plan['isReady'] = this.isSpReadyForReview();
            });
            this.selectedService = null;
          }
        }
        if (this.selectedService) {
          this.selectedService = response.find(item => item.serviceplanid === this.selectedService.serviceplanid);
          if (this.selectedFocus) {
            this.selectedFocus = this.selectedService.serviceplanfocus.find(item => item.serviceplanfocusid === this.selectedFocus.serviceplanfocusid);
          }
        }
        response.map(service => {
          if (Array.isArray(service.serviceplanfocus)) {
            service.serviceplanfocus.map(focus => {
              this.timecheck(focus);
            });
          }
        });
      });

  }
  calculation(selectedService) {
    const actionToalList = [];
    let actionCompletedList = [];
    this.percentageCompleted = 0;
    if (selectedService && selectedService.serviceplanfocus && selectedService.serviceplanfocus.length) {
      selectedService.serviceplanfocus.forEach(o => {
        if (o.serviceplanaction && o.serviceplanaction.length) {
          o.serviceplanaction.forEach(odata => {
            actionToalList.push(odata);
          });
        }
      });
    }
    if (actionToalList.length) {
      actionCompletedList = actionToalList.filter((item) => item.status === 'closed');
    }
    if (actionToalList.length && actionCompletedList.length) {
      this.percentageCompleted = actionCompletedList.length / actionToalList.length * 100;
      this.percentageCompleted = Math.ceil(this.percentageCompleted);
    }
    this.totalActions = actionToalList.length;
    this.completedAction = actionCompletedList.length;
  }

  getPersonListWithProgramAssignment() {
    let inputRequest = {};
    inputRequest = {
      objectid: this.intakeserviceid,
    };
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest
    };
    this._commonhttp.getArrayList(payload, 'People/getPersonWithProgramAssignment?filter').subscribe(
      response => {
        console.log(response);
        this.actionPersonList = response;
     });

  }

  getFullName(person) {
    const nameKeys = [ 'prefx' , 'firstname' , 'middlename' , 'lastname' , 'suffix'];
    let name = '';
    nameKeys.forEach(key => {
      console.log( key , person[key] );
      if ( !(person[key] == null) && !(person[key] == 'null') && !(person[key] == ' ') && person.hasOwnProperty(key) ) {
        name = name + person[key] + ' ';
        console.log(name);
      }
    });
    console.log("NAME",name);
    return name;
  }

  getInvolvedPerson() {
    let inputRequest = {};
    const isServiceCase = this._datastore.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    if (isServiceCase) {
      inputRequest = {
        objectid: this.intakeserviceid,
        objecttypekey: 'servicecase'
      };
    } else {
      inputRequest = {
        intakeserviceid: this.intakeserviceid
      };
    }
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest
    };
    this._commonhttp.getPagedArrayList(payload, 'People/getpersondetail?filter').subscribe(
      response => {
        console.log(response);
        this.personList = response.data;

        if (response.data && response.data.length) {
          response.data.forEach(person => {
            this.personDataList.push({'name': this.getFullName(person)});
            let obj;
            if(person.roles) {
              person.roles.forEach(role => {
                if (role.intakeservicerequestpersontypekey === 'CHILD') {
                  obj = {
                    'id': person.cjamspid,
                    'name': this.getFullName(person),
                    'candidacy': null,
                    'programarea': person.programarea
                  };
                  this.childList.push(obj);
  
                  if (this.childrenHeader !== '') {
                    this.childrenHeader += ',';
                  }
                  this.childrenHeader = this.getFullName(person);
                }
                if (role.intakeservicerequestpersontypekey === 'LG') {
                  this.legalGuardian = this.getFullName(person);
                }
              });
            }
            
          });
          this.personDataList.push({'name': this.user.user.userprofile.displayname});
          if (this.personDataList && this.personDataList.length) {
          this.personListName = this.personDataList[0].name;
          }
          console.log(this.childList);
          this.updateCandidacy();
        }
      });
  }


  editServicePlan() {
    this.servicePlanForm.patchValue(this.selectedService);
    (<any>$('#close-service')).modal('show');
  }

  editFocus(focus) {
    this.focusPlanFormGroup.patchValue(focus);
    if (focus.serviceplanneed && Array.isArray(focus.serviceplanneed)) {
      const serviceplanneedOne = focus.serviceplanneed.map(item => item.serviceplanneedname ? item.serviceplanneedname : item);
      this.focusPlanFormGroup.patchValue({ serviceplanneed: serviceplanneedOne });
    }
    if (focus.serviceplanstrength && Array.isArray(focus.serviceplanstrength)) {
      const serviceplanstrengthOne = focus.serviceplanstrength.map(item => item.serviceplanstrengthname ? item.serviceplanstrengthname : item);
      this.focusPlanFormGroup.patchValue({ serviceplanstrength: serviceplanstrengthOne });
    }

    (<any>$('#add-focus')).modal('show');
  }
  editAction(action, focus) {
    this.selectedFocus = (focus) ? focus : this.selectedFocus;
    this.actionPlanFormGroup.patchValue(action);
    if (action.serviceplanpersoninvolved && Array.isArray(action.serviceplanpersoninvolved)) {
      const persons = action.serviceplanpersoninvolved.map(item => item.personinvolved ? item.personinvolved : item);
      this.actionPlanFormGroup.patchValue({ personinvolved: persons });
    }
    (<any>$('#add-action')).modal('show');
  }
  editOutcome(outcome) {
     this.selectedOutCome = outcome;
     this.outcomePlanFormGroup.patchValue(outcome);
    (<any>$('#add-outcome')).modal('show');
  }

  getPersonName(id) {
    if (this.personList && Array.isArray(this.personList)) {
      const person = this.personList.find(item => item.personid === id);
      const name = (person) ? (this.getFullName(person)) : '';
      return name;
    }
    return '';
  }

  /**
   * Needs and strengths
   */
  saveAssessmentStrengthNeeds() {
    // How do we handle cans-outofhome?
    const payload = {
      'objectid': this.intakeserviceid,
      'templatename': 'cansF',
      'status': 'accepted'
    };
    this._commonhttp.create(payload, 'serviceplan/saveAssessmentStrengthNeeds').subscribe(
      response => {
        this.getNeeds();
        this.getStrengths();
      });
  }


  getNeeds() {
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: {
        intakeserviceid : this.intakeserviceid
      }
    };
    this._commonhttp.getPagedArrayList(payload, 'serviceplanneed/list?filter').subscribe(
      response => {
        if (response && Array.isArray(response.data)) {
          this.cansFNeedsData = response.data.filter(x => x.assessmenttype === 'cansF');
          this.cansNeedsData = response.data.filter(x => x.assessmenttype === 'cansOutOfHomePlacementService');
          this.focusNeeds = response.data.map(x => x.serviceplanneedname);
          this.cansFNeedsList = response.data;
          if (this.cansFNeedsList.length) {
             this.isAssementCompleted = true;
          }
         }
      });
  }

  getStrengths() {
    const payload = {
      method: 'get',
      count: -1,
      page: 1,
      limit: 20,
      where: {
        intakeserviceid : this.intakeserviceid
      }
    };
    this._commonhttp.getPagedArrayList(payload, 'serviceplanstrength/list?filter').subscribe(
      response => {
        if (response && response.data && response.data.length) {
        this.cansFStrengthsData = response.data.filter(x => x.assessmenttype === 'cansF');
        this.cansStrengthsData = response.data.filter(x => x.assessmenttype === 'cansOutOfHomePlacementService');
        this.focusStrengths = response.data.map(x => x.serviceplanstrengthname);
        this.cansFStrengthList = response.data;
        if (this.cansFStrengthList.length) {
          this.isAssementCompleted = true;
        }

        }
      });
  }

  /**
   * Public provider assignment & Routing
   */
  getRoutingUser(appoval, selectService) {
       this.approvalProcess = appoval;
       this.selectedPlan = selectService.serviceplanid;
       this.getUsersList = [];
       if (appoval === 'Pending') {
        this._commonhttp
        .getPagedArrayList(
            new PaginationRequest({
                where: { appevent:  'SPLAN' },
                method: 'post'
            }),
            'Intakedastagings/getroutingusers'
        )
        .subscribe(result => {
            this.getUsersList = result.data;
            this.getUsersList = this.getUsersList.filter(
                users => users.userid !== this._authService.getCurrentUser().user.securityusersid
            );
        });
      }  else {
        this.assignNewUser(selectService);
      }
  }

  selectPerson(row) {
    this.selectedPerson = row;
  }

  assignNewUser(selectedService) {
      selectedService.approvalstatustypekey = this.approvalProcess;
      const payload = {
        eventcode: 'SPLAN',
        tosecurityusersid: this.selectedPerson ? this.selectedPerson.userid : '',
        objectid: this.intakeserviceid,
        serviceNumber: this.daNumber,
        approvalstatustypekey: this.approvalProcess,
        serviceplanid: this.selectedPlan
      };
      this._commonhttp.create(
         payload,
        'serviceplan/serviceplanrouting'
      ).subscribe(
        (response) => {
          this._alertservice.success('Decision submitted successful!');
          if (this.approvalProcess && this.approvalProcess.toLowerCase() === 'approved') {
                this.saveSnapshothist();
          }
          (<any>$('#intake-caseassignnewX')).modal('hide');
        },
        (error) => {
          this._alertservice.error('Unable to submit decision!');
        });
    }

    saveSnapshothist() {
      const payload = {
        'objectid': this.selectedPlan,
        'objecttype': 'SPLAN',
        'snapshotdata': this.selectedService
      };
      this._commonhttp.create(payload, 'snapshothist').subscribe(
        response => {
            this.getHist();
        });
    }
    getHist() {
      this._commonhttp
      .getArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: {
           objectid: this.selectedService.serviceplanid
           }
        }),
        'snapshothist' + '?filter'
      ).subscribe(
        response => {
          this.listSnapShot = response;
       });
    }

  viewFocus(focus) {
    this.selectedItem = 'focus';
    this.selectedFocus = focus;
    if (focus.serviceplanneed && Array.isArray(focus.serviceplanneed)) {
      const serviceplanneedOne = focus.serviceplanneed.map(item => item.serviceplanneedname ? item.serviceplanneednamem : item);
      this.selectedFocus.serviceplanneed = serviceplanneedOne;
    }
    if (focus.serviceplanstrength && Array.isArray(focus.serviceplanstrength)) {
      const serviceplanstrengthOne = focus.serviceplanstrength.map(item => item.serviceplanstrengthname ? item.serviceplanstrengthname : item);
      this.selectedFocus.serviceplanstrength = serviceplanstrengthOne;
    }
  }
  viewAction(action) {
    this.selectedItem = 'action';
    this.selectedAction = action;
    // (<any>$('#view-action')).modal('show');
  }

  timecheck(focus) {
    if (focus && Array.isArray(focus.serviceplanaction)) {
      focus.serviceplanaction.map(item => {
        const startdate = new Date(item.startdate);
        const enddate = new Date(item.enddate);
        const currdate = new Date();
        const diff = Math.abs(enddate.getTime() - currdate.getTime());
        const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
        const actionDiff = Math.abs(enddate.getTime() - startdate.getTime());
        const nod = Math.ceil(actionDiff / (1000 * 3600 * 24));
        item.nod = nod;
        if (diffDays > 0 && diffDays < 30 && item.status === 'open') {
          item.alert = 1;
        } else if (diffDays >= 30 && diffDays < 60 && item.status === 'open') {
          item.alert = 2;
        } else if (diffDays >= 60 && item.status === 'open') {
          item.alert = 3;
        }
      });
    }
  }

  confirmDelete(type, item) {
    this.deletetype = type;
    this.deleteObject = item;
    (<any>$('#delete-popup')).modal('show');
  }

  deleteItem() {
    if (this.deletetype === 'service') {
      this.addServicePlan(0);
    } else if (this.deletetype === 'focus') {
      this.addFocusPlan(0, this.deleteObject);
    } else if (this.deletetype === 'action') {
      this.addActionPlan(0, this.deleteObject);
    } else if (this.deletetype === 'outcome') {
      this.patchData(this.deleteObject, 'Y');
    }
  }

  isSpReadyForReview() {
    if (this.selectedService && Array.isArray(this.selectedService.serviceplanfocus)) {
      const focuses = this.selectedService.serviceplanfocus;
      if (focuses.length > 0) {
          const o = focuses.some(focus =>
            (!focus.approvalstatustypekey && (Array.isArray(focus.serviceplanaction) && focus.serviceplanaction.length > 0))
          );
          return o;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  // YTP Stuff
  getYTPList() {
    return this._commonhttp.getArrayList(
        {
          page: 1,
          limit: 20,
          method: 'get',
          where: {
            intakeserviceid: this.intakeserviceid,
            approvalstatuskey: 'Approved'
          },
          order: 'insertedon desc'
        }, 'youthtransitionplan' + '?filter'
      ).subscribe(
        (response) => {
          this.ytpList = response;
          this.selectedYTP = null;
      });
  }

  selectYTP(item) {
    this.selectedYTP = item;
  }

  selectImportYTP() {
    (<any>$('#import-ytp')).modal('show');
  }

  importYTP() {
    const payload = {};
    payload['servicePlanID'] = this.selectedService.serviceplanid;
    payload['ytpID'] = this.selectedYTP.youthtransitionplanid;
    this._commonhttp.create(
      payload,
      'youthtransitionplan/importYTPtoSP'
    ).subscribe(
      response => {
        this.getServicePlans();
        this._alertservice.success('Youth Transition Plan data imported successfully.');
        (<any>$('#import-ytp')).modal('hide');
      },
      error => {
        this._alertservice.error('Error in importing Youth Transition Plan.');
      }
    );
  }

  public clearSignature(): void {
    this.signaturePad.clear();
  }
}
