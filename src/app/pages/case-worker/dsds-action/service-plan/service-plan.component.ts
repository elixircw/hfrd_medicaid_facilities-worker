import { Subject } from 'rxjs/Subject';
import { AddActivity } from './_entities/service-plan.model';
import { Component, OnInit, Input } from '@angular/core';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { SessionStorageService, CommonHttpService, DataStoreService } from '../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { PaginationRequest, DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AlertService } from '../../../../@core/services/alert.service';
declare var $: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'service-plan',
  templateUrl: './service-plan.component.html',
  styleUrls: ['./service-plan.component.scss']
})
export class ServicePlanComponent implements OnInit {
  isServiceCase: boolean;
  id: any;
  servicePlans$: Observable<any[]>;
  servicePlansCount$: Observable<number>;
  addNewService: FormGroup;
  clientProgramNames$: Observable<DropdownModel[]>;
  agencyServiceNames$: Observable<DropdownModel[]>;
  frequencyCds$: Observable<DropdownModel[]>;
  durationCds$: Observable<DropdownModel[]>;
  personInvolved$: Observable<DropdownModel[]>;

  @Input()
  activityPlanActivityiId: string;
  // @Input()
  // activityPlanActivityi: string;
  @Input()
  editServicePlanOutputSubject$ = new Subject<AddActivity>();
  constructor(private _session: SessionStorageService, private _commonService: CommonHttpService,
    private _datastore: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    this.id = this._datastore.getData(CASE_STORE_CONSTANTS.CASE_UID);
    if (this.isServiceCase) {
      this.getPlacement(1);
      this.formInitialize();
      this.clientProgramDropdown();
    }
  }
  private formInitialize() {
    this.addNewService = this.formBuilder.group(
      {
        intakeservicerequestactorid: [''],
        clientprogramnameid: [''],
        servicetypeid: [''],
        frequencyCdId: [''],
        durationCdId: [''],
        estbegindate: [''],
        actbegindate: [''],
        estenddate: [''],
        actenddate: [''],
        actbegintime: [''],
        actendtime: [''],
        agencynotes: ['']
      });
  }
  private clientProgramDropdown() {
    const source = forkJoin([
      this._commonHttpService.getArrayList({
        order: 'agency_program_nm'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientProgramName),
      this._commonHttpService.getArrayList({
        order: 'SERVICE_NM'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientprogramServices),
      this._commonHttpService.getArrayList({
        order: 'value_tx'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Frequency),
      this._commonHttpService.getArrayList({
        order: 'value_tx'
      }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Duration),
      this._commonHttpService.getPagedArrayList(
        new PaginationRequest({
            page: 1,
            limit: 20,
            method: 'get',
            where: { intakeservreqid: this.id }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
    ),
    ])
      .map((result) => {

        return {
          clientprogramnameid: result[0]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: res.agency_program_nm,
                value: res.agency_program_area_id
              })
          ),
          servicetypeid: result[1]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: res.SERVICE_NM,
                value: res.SERVICE_ID
              })
          ),
          frequencyCdId: result[2]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: (res.value_tx),
                value: (res.picklist_value_cd),
              })
          ),
          durationCdId: result[3]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: (res.value_tx),
                value: (res.picklist_value_cd),
              })
          ),
          persons: result[4]['data'].filter((child) => child.rolename === 'RC' || child.rolename === 'AV' || child.rolename === 'CHILD'),
        };
      })
      .share();

    this.clientProgramNames$ = source.pluck('clientprogramnameid');
    this.agencyServiceNames$ = source.pluck('servicetypeid');
    this.frequencyCds$ = source.pluck('frequencyCdId');
    this.durationCds$ = source.pluck('durationCdId');
    this.personInvolved$ = source.pluck('persons');
  }
  getPlacement(page: number) {
    const source = this._commonService
        .getArrayList(
            new PaginationRequest({
                page: page,
                limit: 10,
                where: {
                  objectid: this.id // '1ab13583-6d94-4929-92e0-09723df839f3'
                },
                method: 'get'
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Getservicecaseplan + '?filter'
        )
        .map((res) => {
            return {
                data: res,
                // count: res.count
            };
        })
        .share();
    this.servicePlans$ = source.pluck('data');
    // if (page === 1) {
    //     this.servicePlansCount$ = source.pluck('count');
    // }
}

editServicePlan(ativity: AddActivity) {
    this.activityPlanActivityiId = ativity.serviceplanactivityid;
    ativity.isAddEdit = 'view';
    this.editServicePlanOutputSubject$.next(ativity);
}

addAgency() {
  (<any>$('#add-newagencyprovider')).modal('hide');
  this._alertService.success('Added successfully.');
}

}
