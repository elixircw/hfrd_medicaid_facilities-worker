import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule
} from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxMaskModule } from 'ngx-mask';

import { CommonHttpService } from '../../../../@core/services';
import { GoalStrategyComponent } from './goal-strategy/goal-strategy.component';
import { ServiceLogComponent } from './service-plan-activity/service-log/service-log.component';
import { ServicePlanActivityComponent } from './service-plan-activity/service-plan-activity.component';
import { ServicePlanAddEditActivityComponent } from './service-plan-add-edit-activity/service-plan-add-edit-activity.component';
import { ServicePlanAddEditServiceComponent } from './service-plan-add-edit-service/service-plan-add-edit-service.component';
import { ServicePlanRoutingModule } from './service-plan-routing.module';
import { ServicePlanComponent } from './service-plan.component';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../../../../environments/environment';
import { AgencyProvidedServicesComponent } from './service-log-activity/agency-provided-services/agency-provided-services.component';
import { ReferredServicesComponent } from './service-log-activity/referred-services/referred-services.component';
import { ServiceLogActivityComponent } from './service-log-activity/service-log-activity.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { ServiceCaseManagementModule } from '../../../shared-pages/service-case-management/service-case-management.module';


@NgModule({
    imports: [
        CommonModule,
        ServicePlanRoutingModule,
        MatTabsModule,
        MatSelectModule,
        MatTableModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        FormsModule,
        PaginationModule,
        A2Edatetimepicker,
        NgSelectModule,
        NgxMaskModule.forRoot(),
        AgmCoreModule,
        SharedDirectivesModule,
        MatTooltipModule,
        ServiceCaseManagementModule
        //AgmCoreModule.forRoot({
            //apiKey: environment.googleMapApi
        //})
    ],
    declarations: [
        ServicePlanComponent,
        GoalStrategyComponent,
        ServicePlanActivityComponent,
        ServicePlanAddEditActivityComponent,
        ServicePlanAddEditServiceComponent,
        ServiceLogComponent,
        // AgencyProvidedServicesComponent,
        // ReferredServicesComponent,
        // ServiceLogActivityComponent
    ],
    providers: [CommonHttpService]
})
export class ServicePlanModule {}
