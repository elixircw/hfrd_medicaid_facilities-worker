import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsRecordingComponent } from './as-recording.component';

describe('AsRecordingComponent', () => {
  let component: AsRecordingComponent;
  let fixture: ComponentFixture<AsRecordingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsRecordingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsRecordingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
