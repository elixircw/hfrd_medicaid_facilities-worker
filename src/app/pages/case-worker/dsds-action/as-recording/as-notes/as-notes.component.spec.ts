import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsNotesComponent } from './as-notes.component';

describe('AsNotesComponent', () => {
  let component: AsNotesComponent;
  let fixture: ComponentFixture<AsNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
