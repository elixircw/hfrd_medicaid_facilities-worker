import { DatePipe } from '@angular/common';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { Subject } from 'rxjs/Rx';

import { ObjectUtils } from '../../../../../@core/common/initializer';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, ValidationService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerContactRoles, CaseWorkerRecording, CaseWorkerRecordingEdit, CaseWorkerRecordingType, ProgressNoteRoleType, SearchRecording } from '../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { Attachment } from '../../attachment/_entities/attachment.data.models';
import * as category from '../_configurations/category.json';
import * as status from '../_configurations/status.json';
import * as type from '../_configurations/type.json';
import { ParticipantType, ReasonForContact, RecordingNotes } from '../../recording/_entities/recording.data.model';
import { SpeechRecognitionService } from '../../../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

declare var $: any;
@Component({
  selector: 'as-notes',
  templateUrl: './as-notes.component.html',
  styleUrls: ['./as-notes.component.scss']
})
export class AsNotesComponent implements OnInit, OnDestroy {
  attachmentGrid$: Observable<Attachment[]>;
  viewEdit: string;
  categoryForm: FormGroup;
  updateAppend: FormGroup;
  courtForm: FormGroup;
  clientDetailsForm: FormGroup;
  // isOthers: boolean;
  id: string;
  daNumber: string;
  multipleRoles: string;
  recordingForm: FormGroup;
  searchCategoryForm: FormGroup;
  saveButton: boolean;
  isCW: boolean;
  isInvolvePerson: boolean;
  isotherPerson: boolean;
  isUploadClicked: boolean;
  recordingCategory = false;
  recordDate = false;
  contactDate = false;
  paginationInfo: PaginationInfo = new PaginationInfo();
  recordingedit: CaseWorkerRecordingEdit = new CaseWorkerRecordingEdit();
  editRecord: RecordingNotes = new RecordingNotes();
  appendNoteControl: AbstractControl;
  typeDropdown: DropdownModel[];
  statusDropdown: DropdownModel[];
  categoryDropdown: DropdownModel[];
  userInfo: AppUser;
  addNotes: RecordingNotes = new RecordingNotes();
  recordingDetail$: Observable<CaseWorkerRecording>;
  recording$: Observable<RecordingNotes[]>;
  totalRecords$: Observable<number>;
  canDisplayPager$: Observable<boolean>;
  recordingType$: Observable<CaseWorkerRecordingType[]>;
  contactRoles$: Observable<CaseWorkerContactRoles[]>;
  participantType$: Observable<ParticipantType[]>;
  recordingSubTypeDropDown$: Observable<CaseWorkerRecordingType[]>;
  progressNoteRoleType: ProgressNoteRoleType[] = [];
  personNameDescription: string[] = [];
  duration: string;
  isCourtDetails = false;
  progressNoteActor = ([] = []);
  private pageSubject$ = new Subject<number>();
  private recordSearch = new SearchRecording();
  reasonForContact$: Observable<ReasonForContact[]>;
  personRoles = ([] = []);
  edittimeduration = false;
  isSerachResultFound: boolean;
  minDate = new Date();
  maxDate = new Date();
  actors = [];
  Others = { intakeservicerequestactorid: 'Others' };
  attempText: boolean;
  initialFace = false;
  stateValuesDropdownItems$: Observable<DropdownModel[]>;
  CountyValuesDropdownItems$: Observable<DropdownModel[]>;
  recognizing = false;
  speechRecogninitionOn: boolean;
  speechData: string;
  notification: string;
  agency: string;
  currentLanguage: string;
  enableAppend = false;
  isAs = false;
  showClientDetails = false;
  involvedPersons = [];
  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private _commonHttpService: CommonHttpService,
      private _authService: AuthService,
      private _alertService: AlertService,
      private _route: Router,
      private datePipe: DatePipe,
      private _speechRecognitionService: SpeechRecognitionService,
      private speechRecognizer: SpeechRecognizerService,
      private _dataStoreService: DataStoreService
  ) {
      this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
      this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
  }

  ngOnInit() {
      this.viewEdit = 'Add';
      this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
      this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
      this.agency = this._authService.getAgencyName();
      this.currentLanguage = 'en-US';
      this.isUploadClicked = false;
      this.speechRecognizer.initialize(this.currentLanguage);
      this.statusDropdown = <any>status;
      this.typeDropdown = <any>type;
      this.categoryDropdown = <any>category;
      this.isCW = this._authService.isCW();
      this.isAs = this._authService.isAS();
      this.getStateDropdown();
      this.getCountyDropdown();
      this.formInitilize();

      this.pageSubject$.subscribe(pageNumber => {
          this.paginationInfo.pageNumber = pageNumber;
          this.getPage(this.paginationInfo.pageNumber);
      });
      this.getPage(1);
      this.recordingDropDown();
      this.attachmentDropdown();
      this.getInvolvedPerson();
      this.getReasonForContact();
      this.appendNoteControl = this.recordingForm.get('appendtitle');
      this.userInfo = this._authService.getCurrentUser();
  }

  private formInitilize() {
      this.recordingForm = this.formBuilder.group(
          {
              progressnotetypeid: ['', Validators.required],
              progressnotesubtypeid: [null],
              contactdate: ['', Validators.required],
              // contactname: [null],
              locationname: [''],
              documentpropertiesid: [''],
              city: [''],
              state: '',
              county: '',
              phonenumber: [''],
              attemptindicator: [''],
              description: ['', Validators.required],
              appendtitle: [''],
              starttime: [null],
              endtime: [null],
              participanttypekey: [null],
              intakeservicerequestactorid: [''],
              progressnotereasontypekey: [''],
              travelhours: ['', Validators.maxLength(3)],
              travelminutes: ['', Validators.maxLength(2)],
              durationhours: [null, Validators.maxLength(3)],
              durationminutes: ['', Validators.maxLength(2)],
              progressnotepurposetypekey: [null],
              progressnoteroletype: [null],
              progressnoteid: [null]
          },
          { validator: this.checkTimeValidation }
      );
      this.courtForm = this.formBuilder.group({
          issuedesc: [''],
          safetydesc: [''],
          services_childdesc: [''],
          services_parentdesc: [''],
          permanencystepdesc: [''],
          placementdesc: [''],
          educationdesc: [''],
          healthdesc: [''],
          socialareadesc: [''],
          financialliteracydesc: [''],
          familyplanningdesc: [''],
          skillissuedesc: [''],
          transitionplandesc: ['']
      });
      this.updateAppend = this.formBuilder.group({
          appendtitle: ['', Validators.required]
      });
      this.searchCategoryForm = this.formBuilder.group(
          {
              draft: [''],
              type: [''],
              datefrom: [new Date(), Validators.required],
              dateto: [new Date(), Validators.required],
              contactdatefrom: [new Date(), Validators.required],
              contactdateto: [new Date(), Validators.required]
          },
          {
              validator: Validators.compose([ValidationService.checkDateRange('starttime', 'endtime')])
          }
      );
      this.categoryForm = this.formBuilder.group({
          category: ['']
      });
      this.clientDetailsForm = this.formBuilder.group({
          name: [''],
          primaryphoneno: [''],
          email: ['', ValidationService.mailFormat],
          relationship: ['']
      });
  }

  private getInvolvedPerson() {
      this._commonHttpService
          .getPagedArrayList(
              {
                  where: { intakeserviceid: this.id },
                  page: 1,
                  limit: 10,
                  method: 'get'
              },
              CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
          )
          .subscribe(itm => {
              if (itm.data) {
                  this.personRoles = [];
                  this.involvedPersons = [];
                  itm.data.map(item => {
                      this.involvedPersons.push({
                          personid: item.personid,
                          name: item.lastname + ', ' + item.firstname,
                          personName: item.lastname + ', ' + item.firstname,
                          primaryphoneno: item.phonedetails ? item.phonedetails[0].phonenumber : '',
                          email: item.email ? item.email : '',
                          relationship: item.relationship ? item.relationship : ''
                      });
                  });
                  itm.data.map(list => {
                      return this.personRoles.push({
                          intakeservicerequestactorid: list.roles[0].intakeservicerequestactorid,
                          displayname: list.firstname + ' ' + list.lastname + '(' + list.roles[0].typedescription + ')',
                          personname: list.firstname + ' ' + list.lastname,
                          role: list.roles
                      });
                  });
              }
          });
  }

  getStateDropdown() {
      this.stateValuesDropdownItems$ = this._commonHttpService
          .getArrayList(
              {
                  method: 'get',
                  nolimit: true
              },
              'States?filter'
          )
          .map(result => {
              return result.map(
                  res =>
                      new DropdownModel({
                          text: res.statename,
                          value: res.stateid
                      })
              );
          });
  }

  showClientFields(event) {
      if (event === 'CTCM') {
          this.showClientDetails = true;
      } else {
          this.showClientDetails = false;
      }
  }

  savePersonDetails(value) {
      this.involvedPersons.push(value);
      this.clientDetailsForm.reset();
  }

  removePerson(index) {
      this.involvedPersons.splice(index, 1);
  }
  getCountyDropdown() {
      this.CountyValuesDropdownItems$ = this._commonHttpService
          .getArrayList(
              {
                  where: {
                      activeflag: '1',
                      state: 'MD'
                  },
                  order: 'countyname asc',
                  method: 'get',
                  nolimit: true
              },
              'admin/county?filter'
          )
          .map(result => {
              return result.map(
                  res =>
                      new DropdownModel({
                          text: res.countyname,
                          value: res.countyid
                      })
              );
          });
  }

  getReasonForContact() {
      this.reasonForContact$ = this._commonHttpService.getSingle({}, 'Progressnotereasontypes/').map(itm => {
          return itm;
      });
  }
  getPage(page: number) {
      ObjectUtils.removeEmptyProperties(this.recordSearch);
      const source = this._commonHttpService
          .getPagedArrayList(
              new PaginationRequest({
                  page: this.paginationInfo.pageNumber,
                  limit: this.paginationInfo.pageSize,
                  where: this.recordSearch,
                  method: 'get'
              }),
              CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetAllDaRecordingUrl + '/' + this.id + '?data'
          )
          .map(result => {
              return {
                  data: result.data,
                  count: result.count,
                  canDisplayPager: result.count > this.paginationInfo.pageSize
              };
          })
          .share();
      this.recording$ = source.pluck('data');
      if (page === 1) {
          this.totalRecords$ = source.pluck('count');
      }
  }
  pageChanged(pageInfo: any) {
      this.paginationInfo.pageNumber = pageInfo.page;
      this.paginationInfo.pageSize = pageInfo.itemsPerPage;
      this.pageSubject$.next(this.paginationInfo.pageNumber);
  }
  changeCategory(event: any) {
      this.isSerachResultFound = true;
      this.searchCategoryForm.reset();
      this.recordSearch = Object.assign({});
      if (event.value === 'RecordingCategory') {
          this.recordingCategory = true;
          this.recordDate = false;
          this.contactDate = false;
      } else if (event.value === 'RecordingDate') {
          this.recordDate = true;
          this.contactDate = false;
          this.recordingCategory = false;
      } else if (event.value === 'ContactDate') {
          this.contactDate = true;
          this.recordDate = false;
          this.recordingCategory = false;
      } else {
          this.recordDate = false;
          this.recordingCategory = false;
          this.contactDate = false;
          this.getPage(1);
      }
      this.searchCategoryForm.patchValue({ draft: '', type: '' });
  }
  recordingSubType(progressnotetypeid: string) {
      const progressNoteTypeKey = progressnotetypeid.split('~');
      if (progressNoteTypeKey[1] === 'Court approved Trial Home Visit') {
          this.isCourtDetails = true;
      } else {
          this.isCourtDetails = false;
      }
      if (progressNoteTypeKey[1] === 'Email' || progressNoteTypeKey[1] === 'Hand deliverd') {
          this.recordingForm.get('progressnotesubtypeid').reset();
          this.recordingForm.get('progressnotesubtypeid').disable();
      } else {
          this.recordingForm.get('progressnotesubtypeid').enable();
      }
      if (progressNoteTypeKey[0]) {
          this.recordingSubTypeDropDown$ = this._commonHttpService
              .getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.ListProgressSubTypeUrl + '?prognotetypeid=' + progressNoteTypeKey[0])
              .map(result => {
                  return result;
              });
      }
      if (progressNoteTypeKey[1] !== 'Initialfacetoface') {
          // this.checkInitialFaceToFace();
          this.initialFace = false;
      } else {
          this.initialFace = true;
      }
  }
  searchRecording(modal: SearchRecording) {
      if (modal.contactdateto) {
          modal.contactdateto = moment(modal.contactdateto)
              .utc()
              .format();
      }
      if (modal.contactdatefrom) {
          modal.contactdatefrom = moment(modal.contactdatefrom)
              .utc()
              .format();
      }
      if (modal.dateto) {
          modal.dateto = moment(modal.dateto)
              .utc()
              .format();
      }
      if (modal.datefrom) {
          modal.datefrom = moment(modal.datefrom)
              .utc()
              .format();
      }
      this.recordSearch = modal;
      this.getPage(1);
  }
  personRoleTypeChange(model) {
      if (model === 'IP') {
          this.recordingForm.controls['intakeservicerequestactorid'].setValidators([Validators.required]);
          this.recordingForm.controls['intakeservicerequestactorid'].updateValueAndValidity();
      } else {
          // this.recordingForm.controls['intakeservicerequestactorid'].reset();
          this.recordingForm.controls['intakeservicerequestactorid'].clearValidators();
          this.recordingForm.controls['intakeservicerequestactorid'].updateValueAndValidity();
      }
  }
  personRoleList(model) {
      if (model) {
          const removalReasonItems = this.personRoles.filter(item => {
              if (model.includes(item.intakeservicerequestactorid)) {
                  return item;
              }
          });
          this.personNameDescription = removalReasonItems.map(res => {
              return res.displayname;
          });
          // if (this.recordingForm.controls['participanttypekey'].value === 'IP') {
          if (model) {
              this.multipleRoles = '';
              const progressNoteRoleType = model.map(res => {
                  if (res && res.intakeservicerequestactorid === 'Others') {
                      (<any>$('#myModal-recordings')).modal('hide');
                      this._speechRecognitionService.destroySpeechObject();
                      (<any>$('#personsTab')).click();
                      // this._route.routeReuseStrategy.shouldReuseRoute = function() {
                      //     return false;
                      // };
                      const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/involved-person';
                      // this._route.navigateByUrl(currentUrl).then(() => {
                      //     this._route.navigated = false;
                      //     this._route.navigate([currentUrl]);
                      // });
                      this._route.navigate([currentUrl], { relativeTo: this.route });
                  }
                  if (this.multipleRoles === '') {
                      this.multipleRoles = this.multipleRoles + '' + res;
                  } else {
                      this.multipleRoles = this.multipleRoles + ', ' + res;
                  }
                  return { intakeservicerequestactorid: res, participanttypekey: 'IP' };
              });
              this.progressNoteRoleType = progressNoteRoleType;
          }
          // }
      }
  }
  timeDuration() {
      const start_date = moment(this.recordingForm.value.starttime, 'HH:mm a');
      const end_date = moment(this.recordingForm.value.endtime, ' HH:mm a');
      const duration = moment.duration(end_date.diff(start_date));
      if (duration['_data'] && this.recordingForm.value.starttime && this.recordingForm.value.endtime) {
          this.recordingForm.get('durationhours').reset();
          this.recordingForm.get('durationminutes').reset();
          this.duration = duration['_data'].hours + ' Hr :' + duration['_data'].minutes + ' Min';
      }
  }

  checkTimeValidation(group: FormGroup) {
      if (!group.controls.endtime.value || group.controls.endtime.value !== '') {
          if (group.controls.endtime.value < group.controls.starttime.value) {
              return { notValid: true };
          }
          return null;
      }
  }
  saveRecording(recording, saveType: string) {
      if (this.checkParticipantsRole(saveType)) {
          if (recording.contactdate) {
              recording.contactdate = moment(recording.contactdate)
                  .utc()
                  .format();
          }
          const progressNoteTypeKey = this.recordingForm.get('progressnotetypeid').value.split('~');
          recording.progressnotetypeid = progressNoteTypeKey[0];
          if (recording.attemptindicator !== null) {
              recording.attemptindicator = recording.attemptindicator === 'yes' ? true : false;
          }
          if (this.involvedPersons && this.showClientDetails) {
            recording.progressnotereason = this.involvedPersons;
          }
          if (this.recordingForm.controls['participanttypekey'].value) {
              const data = Object.assign({
                  intakeservicerequestactorid: null,
                  participanttypekey: this.recordingForm.value.participanttypekey ? this.recordingForm.value.participanttypekey : '',
                  firstname: this.recordingForm.value.firstname ? this.recordingForm.value.firstname : '',
                  lastname: this.recordingForm.value.lastname ? this.recordingForm.value.lastname : '',
                  address1: this.recordingForm.value.address1 ? this.recordingForm.value.address1 : '',
                  address2: this.recordingForm.value.address2 ? this.recordingForm.value.address2 : '',
                  city: this.recordingForm.value.city ? this.recordingForm.value.city : '',
                  state: this.recordingForm.value.state ? this.recordingForm.value.state : '',
                  zipcode: this.recordingForm.value.zipcode ? this.recordingForm.value.zipcode : '',
                  email: this.recordingForm.value.email ? this.recordingForm.value.email : '',
                  phonenumber: this.recordingForm.value.phonenumber ? this.recordingForm.value.phonenumber : ''
              });
              this.progressNoteRoleType.push(data);
          }
          this.addNotes = Object.assign(
              {
                  contactparticipant: this.progressNoteRoleType,
                  entitytypeid: this.id,
                  savemode: saveType === 'SAVE' ? 1 : 0,
                  contacttrialvisit: this.courtForm.value,
                  entitytype: 'intakeservicerequest',
                  stafftype: 1,
                  instantresults: 1,
                  contactstatus: false,
                  drugscreen: false,
                  description: recording.detail,
                  traveltime: recording.travelhours ? recording.travelhours + ':' + recording.travelminutes : '',
                  totaltime: recording.durationhours ? recording.durationhours + ':' + recording.durationminutes : null
              },
              recording
          );

          if (!this.recordingForm.value.documentpropertiesid) {
              delete this.addNotes.documentpropertiesid;
          }
          this._commonHttpService.create(this.addNotes, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.AddRecordingUrl).subscribe(
              result => {
                  this.cancelRecording();
                  this.getPage(1);
                  if (this.isUploadClicked) {
                      this.isUploadClicked = false;
                      this._route.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/attachment/attachment-upload']);
                  }
                  this._alertService.success('Contact details saved successfully!');
              },
              error => {
                  this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
              }
          );
      }
  }

  editRecording(recording, viewEdit: string, typeOfEdit: string) {
      this.editRecord = recording;
      this.recordingDetail$ = this._commonHttpService.getPagedArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetRecordingDetailUrl + '/' + recording.progressnoteid).map(result => {
          result.data[0]['progressnoteroletypeview'] = result.data[0]['progressnoteroletype'].map(item => item.contactroletypekey);
          return result.data[0];
      });
      this.viewEdit = viewEdit;
      if (viewEdit === 'View') {
          this.updateAppend.disable();
      } else {
          this.updateAppend.enable();
      }
      if (typeOfEdit === 'draftEdit') {
          const address = recording.contactparticipant.map(res => {
              if (res.participanttypekey === 'Oth') {
                  return {
                      firstname: res.firstname,
                      lastname: res.lastname,
                      address1: res.address1,
                      address2: res.address2,
                      city: res.city,
                      state: res.state,
                      zipcode: res.zipcode,
                      email: res.email,
                      phonenumber: res.phonenumber
                  };
              }
          });
          if (recording.progressnotereason.length > 1) {
            this.showClientDetails = true;
            this.involvedPersons = [];
            recording.progressnotereason.map(item => {
                this.involvedPersons.push({
                    personid: item.personid ? item.personid : null,
                    name: item.name,
                    personName: item.name,
                    primaryphoneno: item.phonedetails ? item.phonedetails[0].phonenumber : '',
                    email: item.email ? item.email : '',
                    relationship: item.relationship ? item.relationship : ''
                });
            });
          }
          this.recordingForm.patchValue(
              Object.assign(
                  {
                      participanttypekey: recording.contactparticipant[0].participanttypekey,
                      progressnotereasontypekey: recording.progressnotereasontypekey,
                      description: recording.detail,
                      travelhours: recording.traveltime ? recording.traveltime.split(':')[0] : '',
                      travelminutes: recording.traveltime ? recording.traveltime.split(':')[1] : '',
                      durationhours: recording.totaltime ? recording.totaltime.split(':')[0] : '',
                      durationminutes: recording.totaltime ? recording.totaltime.split(':')[1] : ''
                  },
                  recording,
                  address[0] ? address[0] : ''
              )
          );
          if (recording.progressnotetypeid) {
              this.recordingForm.patchValue({
                  progressnotetypeid: recording.progressnotetypeid + '~' + recording.recordingtype
              });
          }
          if (recording.progressnotecontacttrialvisit && recording.progressnotecontacttrialvisit.length) {
              this.courtForm.patchValue(recording.progressnotecontacttrialvisit[0]);
          }
          const serviceReqId = [];
          recording.contactparticipant.map(res => {
              serviceReqId.push(res.intakeservicerequestactorid);
              this.personRoleList(serviceReqId);
          });
          this.recordingForm.controls['intakeservicerequestactorid'].patchValue(serviceReqId);
          this.recordingSubType(recording.progressnotesubtypeid);
          if (recording.recordingtype === 'Court approved Trial Home Visit') {
              this.isCourtDetails = true;
          } else {
              this.isCourtDetails = false;
          }
      } else {
          this.recordingForm.reset();
      }
      const start_date = this.datePipe.transform(recording.starttime, 'hh:mm a');
      const end_date = this.datePipe.transform(recording.endtime, 'hh:mm a');
      this.recordingForm.controls['starttime'].patchValue(start_date);
      this.recordingForm.controls['endtime'].patchValue(end_date);
      this.timeDuration();
      if (recording.attemptind !== null) {
          this.recordingForm.patchValue({
              attemptindicator: recording.attemptind === true ? 'yes' : 'no'
          });
          this.attempText = false;
      } else {
          this.attempText = true;
      }
  }
  changeMinit(model, name) {
      const minit = Number(model);
      if (minit && minit > 59) {
          this._alertService.warn('Please enter valid time');
          if (name === 'duration') {
              this.recordingForm.controls['durationminutes'].reset();
          } else if (name === 'travel') {
              this.recordingForm.controls['travelminutes'].reset();
          }
      }
  }
  updateRecording(recording, updateType: string) {
      if (recording.attemptindicator !== null) {
          recording.attemptindicator = recording.attemptindicator === 'yes' ? true : false;
      }
      if (updateType === 'darft') {
          const progressNoteTypeKey = this.recordingForm.get('progressnotetypeid').value.split('~');
          recording.progressnotetypeid = progressNoteTypeKey[0];
      } else {
          this.recordingForm.get('progressnotetypeid').reset();
      }
      if (recording.contactdate) {
          recording.contactdate = moment(recording.contactdate)
              .utc()
              .format();
      }
      const data = Object.assign({
          intakeservicerequestactorid: null,
          participanttypekey: this.recordingForm.value.participanttypekey,
          firstname: this.recordingForm.value.firstname ? this.recordingForm.value.firstname : '',
          lastname: this.recordingForm.value.lastname ? this.recordingForm.value.lastname : '',
          address1: this.recordingForm.value.address1 ? this.recordingForm.value.address1 : '',
          address2: this.recordingForm.value.address2 ? this.recordingForm.value.address2 : '',
          city: this.recordingForm.value.city ? this.recordingForm.value.city : '',
          state: this.recordingForm.value.state ? this.recordingForm.value.state : '',
          zipcode: this.recordingForm.value.zipcode ? this.recordingForm.value.zipcode : '',
          email: this.recordingForm.value.email ? this.recordingForm.value.email : '',
          phonenumber: this.recordingForm.value.phonenumber ? this.recordingForm.value.phonenumber : ''
      });
      const otherPerson = [];
      otherPerson.push(data);
      this.recordingedit = Object.assign(
          {
              contactparticipant: this.recordingForm.value.participanttypekey === 'IP' ? this.progressNoteRoleType : otherPerson,
              entitytypeid: this.id,
              savemode: this.editRecord.draft ? 1 : 0,
              contacttrialvisit: this.courtForm.value,
              entitytype: 'intakeservicerequest',
              stafftype: 1,
              instantresults: 1,
              contactstatus: false,
              drugscreen: false,
              description: recording.appendtitle,
              traveltime: recording.travelhours ? recording.travelhours + ':' + recording.travelminutes : '',
              totaltime: recording.durationhours ? recording.durationhours + ':' + recording.durationminutes : null
          },
          recording
      );
      this._commonHttpService.patch(this.editRecord.progressnoteid, this.recordingedit, CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.UpdateRecordingUrl).subscribe(
          result => {
              this.cancelRecording();
              this.getPage(1);
              this._alertService.success('Contact details updated successfully!');
              (<any>$('#myModal-recordings-edit')).modal('hide');
              this._speechRecognitionService.destroySpeechObject();
          },
          error => {
              this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
          }
      );
  }

  cancelRecording() {
      (<any>$('#myModal-recordings')).modal('hide');
      (<any>$('#myModal-recordings-edit')).modal('hide');
      this._speechRecognitionService.destroySpeechObject();
      this.recordingForm.reset();
      this.courtForm.reset();
      this.progressNoteActor = [];
      this.updateAppend.reset();
      this.clientDetailsForm.reset();
      this.getInvolvedPerson();
      this.duration = '';
      this.isSerachResultFound = false;
      this.isCourtDetails = false;
      this.showClientDetails = false;
      this.viewEdit = 'Add';
  }
  private recordingDropDown() {
      const source = forkJoin([
          this._commonHttpService.getArrayList(
              new PaginationRequest({
                  where: { activeflag: 1 },
                  method: 'get',
                  nolimit: true
              }),
              CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.ListProgressTypeUrl + '?filter'
          ),
          this._commonHttpService.getArrayList(
              new PaginationRequest({
                  where: { activeflag: 1 },
                  method: 'get',
                  nolimit: true
              }),
              'Contactroletypes' + '?filter'
          ),
          this._commonHttpService.getArrayList({ where: { objecttypekey: 'CPT' }, nolimit: true, method: 'get' }, 'Participanttypes' + '?filter')
      ])
          .map(result => {
              return {
                  recordingType: result[0],
                  contactRoles: result[1],
                  participantType: result[2]
              };
          })
          .share();
      this.recordingType$ = source.pluck('recordingType');
      this.contactRoles$ = source.pluck('contactRoles');
      this.participantType$ = source.pluck('participantType');
  }
  private attachmentDropdown() {
      this.attachmentGrid$ = this._commonHttpService.getArrayList(
          new PaginationRequest({
              nolimit: true,
              method: 'get'
          }),
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/' + this.id + '?data'
      );
  }
  checkInitialFaceToFace() {
      let isInitialFTF: RecordingNotes[] = [];
      this.recording$.subscribe(result => {
          isInitialFTF = result.filter(res => res.recordingtype === 'Initialfacetoface');
          this.recordingType$.subscribe(res => {
              const progressnote = res.filter(item => item.progressnotetypekey === 'Initialfacetoface');
              if (!isInitialFTF.length && progressnote.length && this.userInfo.user.userprofile.teamtypekey === 'CW') {
                  this.initialFace = true;
                  this.recordingForm.get('progressnotesubtypeid').enable();
                  this.isCourtDetails = false;
                  this.recordingForm.patchValue({
                      progressnotetypeid: progressnote[0].progressnotetypeid + '~' + progressnote[0].progressnotetypekey
                  });
                  this._alertService.error('Initial Face to Face is Mandatory');
              }
          });
      });
  }
  checkParticipantsRole(saveType) {
      const progressNoteTypeKey = this.recordingForm.get('progressnotetypeid').value.split('~');
      if (progressNoteTypeKey[1] === 'Initialfacetoface' && saveType === 'SAVE') {
          const avPersonRoles = [];
          let intakeSRActorId: any = [];
          const formAVPersonRoles = [];
          this.personRoles.map(result => {
              // Removed RC (Reported child) in the validation.
              const avrole = result.role.filter(res => res.intakeservicerequestpersontypekey === 'AV' || res.intakeservicerequestpersontypekey === 'CHILD');
              if (avrole.length) {
                  avPersonRoles.push(avrole[0]);
              }
          });
          intakeSRActorId = this.recordingForm.get('intakeservicerequestactorid').value;
          if (intakeSRActorId && intakeSRActorId.length) {
              intakeSRActorId.map(person => {
                  avPersonRoles.map(avperson => {
                      if (avperson.intakeservicerequestactorid === person) {
                          formAVPersonRoles.push(person);
                      }
                  });
              });
          }
          if (formAVPersonRoles.length) {
              return true;
          } else {
              this._alertService.error('Please select atleast one alleged victim or child.');
              return false;
          }
      } else {
          return true;
      }
  }

  ngOnDestroy() {
      this._speechRecognitionService.destroySpeechObject();
  }

  activateSpeechToText(): void {
      this.recognizing = true;
      this.speechRecogninitionOn = !this.speechRecogninitionOn;
      if (this.speechRecogninitionOn) {
          this._speechRecognitionService.record().subscribe(
              // listener
              value => {
                  this.speechData = value;
                  if (this.viewEdit === 'Edit') {
                      this.enableAppend = true;
                      this.updateAppend.patchValue({ appendtitle: this.speechData });
                  } else {
                      this.recordingForm.patchValue({ description: this.speechData });
                  }
              },
              // errror
              err => {
                  console.log(err);
                  this.recognizing = false;
                  if (err.error === 'no-speech') {
                      this.notification = `No speech has been detected. Please try again.`;
                      this._alertService.warn(this.notification);
                      this.activateSpeechToText();
                  } else if (err.error === 'not-allowed') {
                      this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                      this._alertService.warn(this.notification);
                      // this.activateSpeechToText();
                  } else if (err.error === 'not-microphone') {
                      this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`;
                      this._alertService.warn(this.notification);
                      // this.activateSpeechToText();
                  }
              },
              // completion
              () => {
                  this.speechRecogninitionOn = true;
                  console.log('--complete--');
                  this.activateSpeechToText();
              }
          );
      } else {
          this.recognizing = false;
          this.deActivateSpeechRecognition();
      }
  }
  deActivateSpeechRecognition() {
      this.speechRecogninitionOn = false;
      this._speechRecognitionService.destroySpeechObject();
  }

  startDateChanged(investigationForm) {
      const empForm = investigationForm.getRawValue();
      this.minDate = new Date(empForm.contactdatefrom);
  }

  endDateChanged(investigationForm) {
      const empForm = investigationForm.getRawValue();
      this.maxDate = new Date(empForm.contactdateto);
  }

  redirectToUpload() {
      this.isUploadClicked = true;
      this.saveRecording(this.recordingForm.value, 'SAVE');
  }
}
