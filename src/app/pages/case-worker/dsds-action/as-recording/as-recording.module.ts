import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { AsRecordingRoutingModule } from './as-recording-routing.module';
import { AsRecordingComponent } from './as-recording.component';
import { AsNotesComponent } from './as-notes/as-notes.component';
import { MatSelectModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatCardModule, MatInputModule, MatNativeDateModule, MatRadioModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaginationModule } from 'ngx-bootstrap';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { SharedPipesModule } from '../../../../@core/pipes/shared-pipes.module';
import { QuillModule } from 'ngx-quill';
import { NgxMaskModule } from 'ngx-mask';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { AsGuardianshipReviewComponent } from './as-guardianship-review/as-guardianship-review.component';

@NgModule({
  imports: [
    CommonModule,
    AsRecordingRoutingModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    ControlMessagesModule,
    NgSelectModule,
    PaginationModule,
    A2Edatetimepicker,
    SharedPipesModule,
    QuillModule,
    NgxMaskModule,
    SharedDirectivesModule
  ],
  declarations: [AsRecordingComponent, AsNotesComponent, AsGuardianshipReviewComponent],
  providers: [DatePipe]
})
export class AsRecordingModule { }
