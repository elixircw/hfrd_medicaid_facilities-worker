import { Component, OnInit } from '@angular/core';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { AuthService } from '../../../../@core/services';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'as-recording',
  templateUrl: './as-recording.component.html',
  styleUrls: ['./as-recording.component.scss']
})
export class AsRecordingComponent implements OnInit {
  userRole: AppUser;
  isGuardianshipReview: boolean;
  constructor(private _authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.userRole = this._authService.getCurrentUser();
    if (this.userRole.role.name === 'DSSA') {
      this.isGuardianshipReview = true;
    }
  }

  redirectToDash() {
    this.router.navigate(['/pages/as-dss-admin']);
  }

}
