import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsNotesComponent } from './as-notes/as-notes.component';
import { AsRecordingComponent } from './as-recording.component';
import { AsGuardianshipReviewComponent } from './as-guardianship-review/as-guardianship-review.component';

const routes: Routes = [{
  path: '',
  component: AsRecordingComponent,
  children: [{
      path: 'as-notes',
      component: AsNotesComponent
  },
  {
    path: 'as-guardianship-review',
    component: AsGuardianshipReviewComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsRecordingRoutingModule { }
