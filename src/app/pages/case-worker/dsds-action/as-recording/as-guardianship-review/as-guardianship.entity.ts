

    export class Guardinmeetingboardmember {
        boardmembertype: string;
        firstname: string;
        lastname: string;
        email: string;
        phoneno: string;
    }

    export class Guardianshipreview {
        countyid: string;
        dateofmeeting: Date;
        starttime: Date;
        endtime: Date;
        meetingstatus: string;
        meetingtype: string;
        intakeserviceid: string;
        guardinmeetingboardmembers: Guardinmeetingboardmember[];
    }

