import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsGuardianshipReviewComponent } from './as-guardianship-review.component';

describe('AsGuardianshipReviewComponent', () => {
  let component: AsGuardianshipReviewComponent;
  let fixture: ComponentFixture<AsGuardianshipReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsGuardianshipReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsGuardianshipReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
