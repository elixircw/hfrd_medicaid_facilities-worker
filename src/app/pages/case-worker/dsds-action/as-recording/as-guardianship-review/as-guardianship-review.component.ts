import { Component, OnInit } from '@angular/core';
import { DropdownModel, DynamicObject, PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ActivatedRoute } from '@angular/router';
// import { CommonHttpService, AlertService } from '../../../../../@core/services';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { DSDSActionSummary } from '../../../_entities/caseworker.data.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var $: any;
@Component({
    // tslint:disable-next-line: component-selector
    selector: 'as-guardianship-review',
    templateUrl: './as-guardianship-review.component.html',
    styleUrls: ['./as-guardianship-review.component.scss']
})
export class AsGuardianshipReviewComponent implements OnInit {
    daTypeDropdownItems$: Observable<DropdownModel[]>;
    planTypeDropdownItems$: Observable<DropdownModel[]>;
    guardianboardMembertype$: Observable<DropdownModel[]>;
    guardinshipMeetingType$: Observable<DropdownModel[]>;
    guardianMeetingStatus$: Observable<DropdownModel[]>;
    activityCategoryTypes$: Observable<DropdownModel>;
    guardianshipForm: FormGroup;
    id: string;
    newCase: boolean;
    existingCase: boolean;
    guardianshipresult = [];
    CountyValuesDropdownItems$: Observable<DropdownModel[]>;
    isUpdate = false;
    guardinshipmeetingid: string;
    events: string[] = [];
    meetingTypes: string[] = [];
    isAddDisable = false;
    dobdisable = true;
    dsdsActionSummary = new DSDSActionSummary();
    isexistingCaseDisabled: boolean;
    // tslint:disable-next-line:max-line-length
    constructor(private _dropDownService: CommonHttpService,
                private route: ActivatedRoute,
                private _formBuild: FormBuilder,
                private _commonHttp: CommonHttpService,
                private _alert: AlertService,
                private _dataStore: DataStoreService
                ) {
        this.id = this._dataStore.getData(CASE_STORE_CONSTANTS.CASE_UID);
    }

    ngOnInit() {
        this.loadMainDropdowns();
        this.getCountyDropdown();
        this.getGuardianshipList();
        const county =  this._dataStore.getData('countyid');
        this.dsdsActionSummary = this._dataStore.getData('dsdsActionsSummary');
        this.guardianshipForm.controls['countyid'].patchValue(county);
    }
    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        console.log('type');
        const dateofmeeting = event.value;
        this._commonHttp
        .getPagedArrayList(
          new PaginationRequest({
            page: 1,
            limit: 10,
            method: 'get',
            where: {  dateofmeeting: dateofmeeting}
          }),
           'Guardinshipmeeting/existingguardianshipmeeting?filter'
          )
          .subscribe((result) => {
            if (result.data.length === 0) {
                this._alert.error('No Pending Meetings found.You can proceed with new meeting.');
                this.onChangeGuardianship('1');
                this.isexistingCaseDisabled = true;
            }
            result.data.map((item) => {
                item.meetingtype = this.meetingTypes[item.meetingtype];
                return item;
            });
            this.existingGuardianShip(result.data[0]);
        });
      }
      addEventt(type: string, event: MatDatepickerInputEvent<Date>) {
        const dateofmeeting = event.value;
        this._commonHttp
        .getPagedArrayList(
          new PaginationRequest({
            page: 1,
            limit: 10,
            method: 'get',
            where: {  dateofmeeting: dateofmeeting}
          }),
           'Guardinshipmeeting/existingguardianshipmeeting?filter'
          )
          .subscribe((result) => {
            // this.existingGuardianShip(result);
            if (result.data.length) {
                this._alert.error('Already Meeting exists!');
                this.dobdisable = true;
            } else {
                this.dobdisable = false;
            }
        });
      }
    getCountyDropdown() {
        this.CountyValuesDropdownItems$ = this._dropDownService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                'admin/county?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }

    private initializeForm() {
        this.guardianshipForm = this._formBuild.group({
            countyid: [''],
            dateofmeeting: ['', Validators.required],
            starttime: [''],
            endtime: [''],
            meetingstatus: [''],
            meetingtype: ['']
        });
        this.guardianshipForm.setControl('guardinmeetingboardmembers', this._formBuild.array([]));
    }
    addParticipant() {
        this.addMeetingParticipants({});
        this.isUpdate = false;
        this.onChangeGuardianship('1');
    }
    closeParticipant() {
        this.guardianshipForm.setControl('guardinmeetingboardmembers', this._formBuild.array([]));
        (<any>$('#add-new')).modal('hide');
    }
    addMeetingParticipants(modal) {
        const control = <FormArray>this.guardianshipForm.controls['guardinmeetingboardmembers'];
        control.push(this.createFormGroup(modal));
    }
    deleteMeetingParticipants(index: number, e) {
        const control = <FormArray>this.guardianshipForm.controls['guardinmeetingboardmembers'];
        control.removeAt(index);
    }
    boardMemberTypeChange(modal, index) {
        if (modal === 'ASSCASEWORKER') {
            this.guardianshipForm.controls['guardinmeetingboardmembers']['controls'][index].patchValue({
                firstname: this.dsdsActionSummary.da_assignedto.split(' ')[0],
                lastname: this.dsdsActionSummary.da_assignedto.split(' ')[1]
            });
        } else {
            this.guardianshipForm.controls['guardinmeetingboardmembers']['controls'][index].controls['firstname'].reset();
            this.guardianshipForm.controls['guardinmeetingboardmembers']['controls'][index].controls['lastname'].reset();
            this.guardianshipForm.controls['guardinmeetingboardmembers']['controls'][index].controls['email'].reset();
            this.guardianshipForm.controls['guardinmeetingboardmembers']['controls'][index].controls['phoneno'].reset();
        }
    }
    private createFormGroup(modal) {
        return this._formBuild.group({
            boardmembertype: modal.boardmembertype ? modal.boardmembertype : '',
            firstname:  modal.firstname ? modal.firstname : '',
            lastname:  modal.lastname ? modal.lastname : '',
            email:  modal.email ? modal.email : '',
            phoneno:  modal.phoneno ? modal.phoneno : '',
        });
    }

    private loadMainDropdowns() {
        const source = forkJoin([
            this._dropDownService.getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: { tablename: 'GuardinshipMeetingType', teamtypekey: 'AS' },
                    method: 'get'
                }),
                'referencetype/gettypes?filter'
            ),
            this._dropDownService.getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: { tablename: 'Guardinboardmembertype', teamtypekey: 'AS' },
                    method: 'get'
                }),
                'referencetype/gettypes?filter'
            ),
            this._dropDownService.getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: { tablename: 'GuardinshipMeetingStatus', teamtypekey: 'AS' },
                    method: 'get'
                }),
                'referencetype/gettypes?filter'
            )
        ])
            .map(data => {
                if (data && data.length) {
                    data[0].forEach(item => {
                        this.meetingTypes[item.description] = item.ref_key;
                    });
                }
                return {
                    meetingType: data[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    memberType: data[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    ),
                    meetingStatus: data[2].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.ref_key
                            })
                    )
                };
            }).share();
        this.guardinshipMeetingType$ = source.pluck('meetingType');
        this.guardianboardMembertype$ = source.pluck('memberType');
        this.guardianMeetingStatus$ = source.pluck('meetingStatus');

        this.initializeForm();

    }
    existingGuardianShip(modal) {
        (<any>$('#add-new')).modal('show');
        this.guardianshipForm.patchValue({
        countyid: modal.countyid,
        dateofmeeting: modal.dateofmeeting,
        starttime: modal.starttime,
        endtime: modal.endtime,
        meetingstatus: modal.meetingstatus,
        meetingtype: modal.meetingtype
    });
    this.guardinshipmeetingid = modal.guardinshipmeetingid;
    if (modal.guardinmeetingboardmembers) {
        modal.guardinmeetingboardmembers.map(element => {
            this.addMeetingParticipants(element);
        });
    }
    this.onChangeGuardianship('0');
    this.isUpdate = true;
}
    updateGuardianShip(modal) {
        (<any>$('#add-new')).modal('show');
        this.guardianshipForm.patchValue({
            countyid: modal.countyid,
            dateofmeeting: modal.dateofmeeting,
            starttime: modal.starttime,
            endtime: modal.endtime,
            meetingstatus: modal.meetingstatus,
            meetingtype: modal.meetingtypekey
        });
        this.guardinshipmeetingid = modal.guardinshipmeetingid;
        if (modal.guardinmeetingboardmembers) {
            console.log(modal.guardinmeetingboardmembers);
            modal.guardinmeetingboardmembers.map(element => {
                this.addMeetingParticipants(element);
            });
        }
        this.onChangeGuardianship('1');
        this.isUpdate = true;
    }
    saveGuardianShip(modal, mode){
        const guardianshipInput = this.guardianshipForm.value;
        if (mode) {
            guardianshipInput.guardinshipmeetingid = this.guardinshipmeetingid;
        } else {
            guardianshipInput.guardinshipmeetingid = null;
        }
        guardianshipInput.intakeserviceid = this.id;
         this._dropDownService.create(guardianshipInput, 'Guardinshipmeeting/addUpdate').subscribe(
          res => {
                      this.guardianshipForm.reset();
                      this._alert.success('Guardianship Review saved successfully!');
                      this.closeParticipant();
                      this.getGuardianshipList();
                      },
                  err => {
                      this._alert.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                  }
         );
      }
      onChangeGuardianship(event) {
        if (event === '1') {
            this.newCase = true;
        } else {
            this.newCase = false;
            this.existingCase = true;
        }
      }
      getGuardianshipList() {
        this._commonHttp
         .getPagedArrayList(
           new PaginationRequest({
             page: 1,
             limit: 10,
             method: 'get',
             where: {  intakeserviceid: this.id}
           }),
            'Guardinshipmeeting/list?filter'
           )
           .subscribe((result) => {
               this.guardianshipresult  = result.data;
               this.guardianshipresult.map(res => {
                if (res.meetingstatus === 'Pending') {
                    this.isAddDisable = true;
                }
             });
         });

       }

}
