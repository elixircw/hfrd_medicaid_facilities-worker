import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Assignments } from './assignments.data.model';
import { SessionStorageService, LocalStorageService } from '../../../../@core/services/storage.service';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { CommonHttpService, AlertService, AuthService } from '../../../../@core/services';
import * as moment from 'moment';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { ActivatedRoute } from '@angular/router';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { ErrorStateMatcher } from '@angular/material';
import { AppConstants } from '../../../../@core/common/constants';
declare var $: any;
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'cw-assignments',
    templateUrl: './cw-assignments.component.html',
    styleUrls: ['./cw-assignments.component.scss']
})
export class CwAssignmentsComponent implements OnInit {
    eventcode: string;
    caseTransfer: FormGroup;
    caseWorkerList;
    assignmentsList$: Observable<Assignments[]>;
    serviceCaseId: string;
    isServiceCase: string;
    teamList;
    user;
    matcher = new MyErrorStateMatcher();
    isSupervisor: boolean = false;
    iscurrentcaseended: boolean = false;
    isAddDisable: boolean;
    fromWorkerDetails: any;
    toWorkerDetails: any;
    id: string;
    departmentList: Array<any> = [];
    caseType: string
    isClosed = false;
    childList$: Observable<any[]>;
    isAdoptionCase: boolean;
    constructor(
        private fb: FormBuilder,
        private _commonService: CommonHttpService,
        private _authService: AuthService,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService,
        private storage: SessionStorageService,
        private localstore: LocalStorageService
        ) { }

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
        const caseWorker = this._authService.getCurrentUser();
        if (caseWorker.role.description === 'Case Worker,CW') {
            this.isAddDisable = false;
        } else {
            this.isAddDisable = true;
        }

        this.user = this.localstore.getObj('userProfile');
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        const caseType = this.storage.getItem(CASE_STORE_CONSTANTS.CASE_TYPE);
        this.isAdoptionCase = caseType === CASE_TYPE_CONSTANTS.ADOPTION;
        if (this.isServiceCase) {
            this.serviceCaseId = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        } else {
            this.serviceCaseId = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        }
        if (this.isServiceCase) {
          this.eventcode  = 'SRVC';
        } else if (this.isAdoptionCase) {
            this.eventcode = 'ADPC';
        } else {
            this.eventcode  = 'INVT';
        }
        this.caseTransfer = this.fb.group({
            selectDeptGroup: [''],
            toteamid: [''],
            appeventcode: [this.eventcode],
            servicecaseid: [this.serviceCaseId?this.serviceCaseId:this.id],
            assigneduser: [''],
            responsibilitytypekey: [null, Validators.required],
            startdate: [{ value: moment().format(), disabled: true }],
            enddate: [''],
            caseassignmentid: [null],
            remarks: [''],
            child: ['']
        });
        this.getAssignmentsList();
        this.getPersonsList();
        this.getCountyList();
        this.caseTransfer.get('selectDeptGroup').valueChanges.subscribe(res => this.getteamlist());
        this.caseTransfer.get('toteamid').valueChanges.subscribe(res => this.getteamusers(res));
        const da_status = this.storage.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }
    }
        getAssignmentsList() {
        this.assignmentsList$ = this._commonService.getArrayList(
            {
                where: { servicecaseid: this.serviceCaseId ? this.serviceCaseId : this.id},
                method: 'get'
            },
            'Caseassignments/getworkload?filter'
        );
    }
    getCountyList() {
        this._commonService.getArrayList({ where: {},
          order: 'countyname',
          nolimit: true,
          method: 'get'
        }, 'admin/county?filter').subscribe((item) => {
          this.departmentList = item;
        });
      }

    getPersonsList() {
        this.childList$ = this._commonService.getPagedArrayList(
            {
                page: 1,
                limit: 20,
                method: 'get',
                where: { 'caseid': this.id }
            }, 'caseassignments/getresponsibilitychild?filter').map((item: any) => {
                return item;
            });
        }
    toggleTable(id) {
        (<any>$('#' + id)).collapse('toggle');
    }

    getteamlist() {
        this.caseTransfer.get('toteamid').disable();
        this.caseTransfer.get('assigneduser').disable();
        let obj = {
            activeflag: 1
        }
        if (this.caseTransfer.get('selectDeptGroup').value === 'department') {
            obj['teamtypekey'] = 'LDSS';
        } else {
            obj['teamtypekey'] = this.user.teamtypekey;
        }
        this.caseTransfer.get('assigneduser').disable();
        this._commonService.getPagedArrayList(new PaginationRequest({
            where: obj,
            method: 'get',
            nolimit: true
        }), 'manage/team/getteamlist?filter').subscribe((result: any) => {
            this.teamList = result;
            let activeteam = result.find(v => v.isdefault === 1)
            if (activeteam) {
                this.caseTransfer.get('toteamid').patchValue(activeteam.teamid);
            }
            if (this.caseTransfer.get('selectDeptGroup').value === 'workers') {
                this.caseTransfer.get('toteamid').disable();
            } else {
                this.caseTransfer.get('toteamid').enable();
            }
        });
    }
    contactDetails(item) {
        if (item['fromworkerdetails'] && item['fromworkerdetails'].length) {
            this.fromWorkerDetails = item['fromworkerdetails'][0];
        }
        if (item['toworkerdetails'] && item['toworkerdetails'].length) {
            this.toWorkerDetails = Object.assign({
                responsibilitytypekey: item.responsibilitytypekey
            }, item['toworkerdetails'][0]);
        }
    }
    close() {
        this.caseTransfer.controls['child'].reset();
    }

    getteamusers(data, modalid?) {
        let obj = {
            teamid: '',
            filtertypekey: ''
        };
        obj.teamid = data === '' ? null : data;
        if (this.caseTransfer.get('selectDeptGroup').value === 'workers') {
            obj.filtertypekey = 'worker';
        } else if (this.caseTransfer.get('selectDeptGroup').value === 'unit') {
            obj.filtertypekey = 'unit';
        } else if (this.caseTransfer.get('selectDeptGroup').value === 'department') {
            obj.filtertypekey = 'ldss';
        }
        this._commonService.getPagedArrayList(new PaginationRequest({
            where: obj,
            method: 'get',
            nolimit: true
        }), 'manage/team/getteamusers?filter').subscribe((result: any) => {
            this.caseWorkerList = result;
            $(modalid).modal('show');
            this.caseTransfer.get('assigneduser').enable();
        });
    }
    openadd(id) {
        let obj = {
            assigneduser: '',
            responsibilitytypekey: '',
            enddate: '',
            remarks: '',
            selectDeptGroup: 'workers',
            caseassignmentid: null,
        };
        this.iscurrentcaseended = false;
        this.caseTransfer.patchValue(obj);
        this.caseTransfer.get('enddate').disable();
        this.caseTransfer.get('responsibilitytypekey').enable();
        $(id).modal('show');
    }
    editcase(data, id) {
        // console.log(data);
        this.iscurrentcaseended = (data.enddate) ? true : false;
        if (this.isSupervisor) {
            if (data.responsibilitytypekey === null || data.responsibilitytypekey === '' || data.responsibilitytypekey === undefined) {
                this.caseTransfer.get('responsibilitytypekey').enable();
            } else {
                this.caseTransfer.get('responsibilitytypekey').disable();
            }

            let obj = {
                servicecaseid: this.serviceCaseId,
                assigneduser: data.toworkeridno,
                responsibilitytypekey: data.responsibilitytypekey,
                startdate: data.startdate,
                enddate: data.enddate,
                caseassignmentid: data.caseassignmentid,
                remarks: data.remarks,
                selectDeptGroup: 'workers',
                toteamid: data.toteamid
            };
            this.caseTransfer.patchValue(obj);
            if (data.childinfo) {
                const childInfo = data.childinfo.map((item) => {
                    return item.intakeservicerequestactorid;
                });
                this.caseTransfer.controls['child'].patchValue(childInfo);
            }
            this.caseTransfer.get('enddate').enable();
            if (obj.toteamid) {
                this.getteamusers(obj.toteamid, id);
            } else {
                $(id).modal('show');
            }
        } else {
            this._alertService.error('Unauthorized.');
        }
    }
    caseTransfersubmit(val, id) {
        this.caseTransfer.get('toteamid').enable();
        this.caseTransfer.get('responsibilitytypekey').enable();
        $(id).modal('hide');
        if (this.caseTransfer.value.child && this.caseTransfer.value.child.length) {
            this.caseTransfer.value.child =  this.caseTransfer.value.child.map((item) => {
                return {
                        intakeservicerequestactorid: item
                       };
                });
        }

        const data = { ...this.caseTransfer.value };
        if (data.selectDeptGroup === 'department') {
            data.assignmenttype = 'T';
        } else if (data.selectDeptGroup === 'workers') {
            data.assignmenttype = 'W' ;
        } else if (data.selectDeptGroup === 'unit') {
            data.assignmenttype = 'U' ;
        }
        delete data.selectDeptGroup;
        if (this.caseTransfer.get('selectDeptGroup').value === 'department') {
            data.isanotherunit = 1;
        } else {
            data.isanotherunit = null;
        }
        if (!val.caseassignmentid) {
            delete data.enddate;
        } else {
            data.enddate = data.enddate ? moment(data.enddate).format('YYYY-MM-DD HH:mm:ss') : '';
        }

        // console.log(data);
        this._commonService.create(data, CaseWorkerUrlConfig.EndPoint.DSDSAction.Assignment.Reassigncase).subscribe((res) => {
            if (res && res.length && res[0].statuscode === 200) {
                this._alertService.success(res[0].status_description);
                this.getAssignmentsList();
                this._dataStoreService.setData('DSDS_ACTION_UPDATE', true);
                this.caseTransfer.controls['child'].reset();
            } else {
                this.caseTransfer.controls['child'].reset();
                this._alertService.error('Service Case Assignment Error');
            }
        });
    }
}
