import { ReportSummary } from './../../../providers/_entities/provider.data.model';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CwAssignmentsComponent } from './cw-assignments.component';
import { CwAssignmentsRoutingModule } from './cw-assignments-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    CwAssignmentsRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    QuillModule,
    PaginationModule
  ],
  declarations: [
    CwAssignmentsComponent
  ],
  providers: []
})
export class CwAssignmentsModule { }
