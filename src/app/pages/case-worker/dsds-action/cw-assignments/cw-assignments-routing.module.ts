import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CwAssignmentsComponent } from './cw-assignments.component';
const routes: Routes = [
    {
    path: '',
    component: CwAssignmentsComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CwAssignmentsRoutingModule { }
