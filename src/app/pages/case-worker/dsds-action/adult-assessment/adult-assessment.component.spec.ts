import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdultAssessmentComponent } from './adult-assessment.component';

describe('AdultAssessmentComponent', () => {
  let component: AdultAssessmentComponent;
  let fixture: ComponentFixture<AdultAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdultAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
