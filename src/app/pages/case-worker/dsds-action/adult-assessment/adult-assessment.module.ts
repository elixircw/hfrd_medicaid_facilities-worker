import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdultAssessmentComponent } from './adult-assessment.component';
import { AdultAssessmentRoutingModule } from './adult-assessment-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    AdultAssessmentRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    PaginationModule
  ],
  declarations: [
    AdultAssessmentComponent
  ],
  providers: []
})
export class AdultAssessmentModule { }
