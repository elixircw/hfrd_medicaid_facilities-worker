export enum AdultAssessmentMode {
    ADD,
    EDIT,
    VIEW,
    PRINT
}
