import * as moment from 'moment';
import { InvolvedPerson, Medicalinformation, MedicalCondition, BehaviourHealth } from '../involved-persons/_entities/involvedperson.data.model';
import { RoutingInfo } from '../../_entities/caseworker.data.model';
import { titleCase } from '../../../../@core/common/initializer';
import { assessmentData } from '../_data/assessment';
import { Placement } from '../service-plan/_entities/service-plan.model';
import { DataStoreService } from '../../../../@core/services';
import { Task } from '../investigation-plan/_entities/investigationplan.data.models';

export class AdultAssessmentPreFill {
    baseLocation: string;
    receivedDate: any;
    intakeAcceptedDate: any;
    vendorProviderName = [];
    constructor(private involvedPersons: InvolvedPerson[], private routingInfo: RoutingInfo[], private token: any, private _datastore: DataStoreService, private location: Location) {}
    get caseHeadDetail() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'CASEHD' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'CASEHD').length);
        });
    }
    get careGiverDetails() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'CARTKR' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'CARTKR').length);
        });
    }
    get reportedChildDetails() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'RC' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RC').length);
        });
    }

    get reportedAdultDetails() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'RA' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RA').length);
        });
    }

    get childDetails() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'CHILD' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'CHILD').length);
        });
    }

    get isHouseHold() {
        return this.involvedPersons.filter(item => item.ishousehold === 1 && item.rolename !== 'RA');
    }

    get nonHouseHold() {
        return this.involvedPersons.filter(item => item.ishousehold === 0 && item.rolename !== 'RA');
    }

    get supervisorDetail() {
        return this.routingInfo.find(item => item.fromrole === 'Supervisor');
    }
    get caseWorkerDetail() {
        return this.routingInfo.find(item => item.torole === 'Case Worker');
    }
    get intakeWorkerDetails() {
        return this.routingInfo.find(item => item.fromrole === 'Intake Worker');
    }
    get participantDetail() {
        return this.involvedPersons.filter(item => {
            if (item.roles) {
                // tslint:disable-next-line:max-line-length
                const getRole = item.roles.filter(
                    items => items.intakeservicerequestpersontypekey === 'BIOPPARNT' || items.intakeservicerequestpersontypekey === 'PARENT' || items.intakeservicerequestpersontypekey === 'LG'
                );
                if (getRole.length) {
                    return item;
                }
            }
            // return item.rolename === 'BIOPPARNT' || item.rolename === 'PARENT' || item.rolename === 'LG';
        });
    }
    get houseHoldDetail() {
        return this.involvedPersons.filter(item => {
            return item.ishousehold === 1;
        });
    }

    get intakeJsonData() {
        return this._datastore.currentStore;
    }

    get involvedPersonOtherRA() {
        return this.involvedPersons.filter(item => item.rolename !== 'RA');
    }

    private getFullName(involvedPerson: InvolvedPerson) {
        let fullName = '';
        if (involvedPerson.firstname) {
            fullName += involvedPerson.firstname;
        }
        if (involvedPerson.lastname) {
            fullName += ', ' + involvedPerson.lastname;
        }
        return fullName.toUpperCase();
    }
    private getAge(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }
    private getFormattedDate(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            return moment(new Date(dateValue)).format('YYYY-MM-DD');
        } else {
            return '';
        }
    }
    private getAddress(involvedPerson: InvolvedPerson) {
        let address = involvedPerson.address ? involvedPerson.address : '';
        if (involvedPerson.address) {
            address += involvedPerson.address;
        }
        if (involvedPerson.city) {
            address += involvedPerson.city;
        }
        if (involvedPerson.zipcode) {
            address += involvedPerson.zipcode;
        }
        return address.toUpperCase();
    }

    private getFullAddress(involvedPerson: InvolvedPerson) {
        let address = involvedPerson.address ? involvedPerson.address : '';
        if (involvedPerson.address2) {
            address += ' ' + involvedPerson.address2;
        }
        if (involvedPerson.city) {
            address += ' ' + involvedPerson.city;
        }
        if (involvedPerson.county) {
            address += ' ' + involvedPerson.county;
        }
        if (involvedPerson.state) {
            address += ' ' + involvedPerson.state;
        }
        if (involvedPerson.zipcode) {
            address += ' ' + involvedPerson.zipcode;
        }
        return address.toUpperCase();
    }

    private getMedicationAddress(addressDetail: Medicalinformation) {
        let address = addressDetail.address1 ? addressDetail.address1 : '';

        if (addressDetail.city) {
            address += ' ' + addressDetail.city;
        }
        if (addressDetail.zip) {
            address += ' ' + addressDetail.zip;
        }
        return address.toUpperCase();
    }

    private getBehaviourAddress(addressDetail: BehaviourHealth) {
        let address = '';
        if (addressDetail.behavioraladdress1) {
            address += addressDetail.behavioraladdress1;
        }
        if (addressDetail.behavioralcity) {
            address += addressDetail.behavioralcity;
        }
        if (addressDetail.behavioralzip) {
            address += addressDetail.behavioralzip;
        }
        return address.toUpperCase();
    }

    public fillHomeHealthReport(submissionData, daNumber) {
        submissionData['userrole'] = this.token.role.name;
        submissionData['casenumber'] = daNumber;
        if (this.caseHeadDetail.length) {
            submissionData['nameofcaretaker'] = this.getFullName(this.caseHeadDetail[0]);
        }
        if (this.careGiverDetails.length) {
            submissionData['nameofcaretaker'] = this.getFullName(this.careGiverDetails[0]);
            submissionData['caregiverdob'] = this.getFormattedDate(this.careGiverDetails[0].dob);
            submissionData['age'] = this.getAge(this.careGiverDetails[0].dob);
            if (this.careGiverDetails[0].address && this.careGiverDetails[0].address.length) {
                submissionData['address'] = this.careGiverDetails[0].address;
                submissionData['city'] = this.careGiverDetails[0].city;
                submissionData['state'] = this.careGiverDetails[0].state;
                submissionData['zip'] = this.careGiverDetails[0].zipcode;
            }
        }
        if (this.caseWorkerDetail) {
            submissionData['workername1'] = this.caseWorkerDetail.tousername;
        }
        if (this.reportedChildDetails.length) {
            submissionData['childrendetails'] = [];
            this.reportedChildDetails.map(child => {
                submissionData['childrendetails'].push({
                    childerenname: child.firstname,
                    DOB: child.dob
                });
            });
        }
        return submissionData;
    }
    public fillTransportationPlan(submissionData, daNumber) {
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedChildDetails.length) {
            submissionData['studentname'] = this.getFullName(this.reportedChildDetails[0]);
            submissionData['studentdob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
                submissionData['currentgrade'] = this.reportedChildDetails[0].school[0].typedescription;
                submissionData['localdepartmentofsocialservices'] = this.reportedChildDetails[0].school[0].educationname;
            }
        }
        return submissionData;
    }
    public fillBestInterestDetermination(submissionData, daNumber, currentUser) {
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedChildDetails.length) {
            submissionData['date'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['studentsname'] = this.getFullName(this.reportedChildDetails[0]);
            submissionData['dob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
                submissionData['grade'] = this.reportedChildDetails[0].school[0].typedescription;
                submissionData['currentschool'] = this.reportedChildDetails[0].school[0].educationname;
                submissionData['displaypreviousSchool'] = this.reportedChildDetails[0].school[0].educationname;
                if (this.reportedChildDetails[0].school.length > 1) {
                    submissionData['currentschool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                    submissionData['displaypreviousSchool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                    submissionData['previousschool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 2].educationname;
                }
            }
            // submissionData['saseworkername'] = currentUser;
            if (this.caseWorkerDetail) {
                submissionData['saseworkername'] = this.caseWorkerDetail.tousername;
            }
        }
        if (this.participantDetail.length) {
            submissionData['bestInterestsDeterminationMeetingParticipants'] = [];
            this.participantDetail.map(person => {
                submissionData['bestInterestsDeterminationMeetingParticipants'].push({
                    fname: person.firstname,
                    lname: person.lastname,
                    relationshiptostudent: person.relationship.replace(/\w\S*/g, txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase()),
                    // otherrelation: person,
                    phonenumber: person.phonenumber,
                    emailid: person.email
                });
            });
        }
        return submissionData;
    }
    public fillNotificationOfPlacement(submissionData, daNumber, currentUser) {
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedChildDetails.length) {
            submissionData['date'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
            submissionData['studentsname'] = this.getFullName(this.reportedChildDetails[0]);
            submissionData['dob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
                submissionData['grade'] = this.reportedChildDetails[0].school[0].typedescription;
                submissionData['previousschool'] = this.reportedChildDetails[0].school[0].educationname;
                if (this.reportedChildDetails[0].school.length > 1) {
                    submissionData['currentschool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                }
            }
            submissionData['saseworkername'] = currentUser;
        }
        return submissionData;
    }
    public fillCansOutOfHomePlacement(submissionData, daNumber, currentUser) {
        submissionData['liftdomainfunction'] = assessmentData.liftdomainfunction;
        submissionData['liftdomainfunction2'] = assessmentData.liftdomainfunction2;
        submissionData['childandenvironmentstrength'] = assessmentData.childandenvironmentstrength;
        submissionData['childandenvironmentstrength2'] = assessmentData.childandenvironmentstrength2;
        submissionData['childemotinalneeds'] = assessmentData.childemotinalneeds;
        submissionData['childriskbehaviour'] = assessmentData.childriskbehaviour;
        submissionData['Surveyculturalfactors'] = assessmentData.Surveyculturalfactors;
        submissionData['surveytrauma'] = assessmentData.surveytrauma;
        submissionData['surveystress'] = assessmentData.surveystress;
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedChildDetails.length) {
            submissionData['date'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
            submissionData['assessmentdate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['source3'] = this.getFullName(this.reportedChildDetails[0]);
            // submissionData['assignedstudent'] = this.reportedChildDetails.Pid;
            submissionData['dob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            submissionData['age'] = this.getAge(this.reportedChildDetails[0].dob);
            if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
                submissionData['grade'] = this.reportedChildDetails[0].school[0].typedescription;
                submissionData['previousschool'] = this.reportedChildDetails[0].school[0].educationname;
                if (this.reportedChildDetails[0].school.length > 1) {
                    submissionData['currentschool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                }
            }
            submissionData['saseworkername'] = currentUser;
        }
        if (this.caseHeadDetail.length) {
            submissionData['caseheadsname'] = this.getFullName(this.caseHeadDetail[0]);
            submissionData['relationship'] = titleCase(this.caseHeadDetail[0].relationship);
            submissionData['source'] = this.getAge(this.caseHeadDetail[0].dob);
        }
        if (this.caseWorkerDetail) {
            submissionData['columns8Columns2CaseWorkerNameField2'] = this.caseWorkerDetail.fromusername;
            submissionData['workertitle'] = this.caseWorkerDetail.fromrole;
            submissionData['score2'] = this.caseWorkerDetail.tousername;
            submissionData['columns8Columns2CaseWorkerNameField'] = this.caseWorkerDetail.tousername;
        }
        if (this.careGiverDetails.length) {
            submissionData['panel5079156779560240Columns44CaregiverFirstName'] = titleCase(this.careGiverDetails[0].firstname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName2'] = titleCase(this.careGiverDetails[0].lastname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName3'] = titleCase(this.careGiverDetails[0].relationship);
            submissionData['panel5079156779560240Columns44CaregiverFirstName4'] = titleCase(this.careGiverDetails[0].firstname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName5'] = titleCase(this.careGiverDetails[0].lastname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName6'] = titleCase(this.careGiverDetails[0].relationship);
            submissionData['panel5079156779560240Columns44CaregiverFirstName7'] = titleCase(this.careGiverDetails[0].firstname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName8'] = titleCase(this.careGiverDetails[0].lastname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName9'] = titleCase(this.careGiverDetails[0].relationship);
            submissionData['nameofcaretaker'] = this.getFullName(this.careGiverDetails[0]);
            submissionData['casenumber'] = daNumber;
            if (this.careGiverDetails[0].address && this.careGiverDetails[0].address.length) {
                submissionData['address'] = this.careGiverDetails[0].address;
                submissionData['city'] = this.careGiverDetails[0].city;
                submissionData['state'] = this.careGiverDetails[0].state;
                submissionData['zip'] = this.careGiverDetails[0].zipcode;
            }
        }
        if (this.childDetails.length) {
            submissionData['familygrid'] = [];
            this.childDetails.forEach(child => {
                submissionData['familygrid'].push({
                    childrenname: this.getFullName(child),
                    childrendob: this.getFormattedDate(child.dob),
                    childrenage: this.getAge(child.dob),
                    childrenrelationship: titleCase(child.relationship),
                    primarycaregiver: ''
                });
            });
        }
        return submissionData;
    }

    public fillClientAssessmentForm716A(submissionData, daNumber, workerName) {
        submissionData['userrole'] = this.token.role.name;
        if (workerName) {
            submissionData['WorkerName'] = workerName.da_assignedto.toUpperCase();
            submissionData['workerdate'] = moment(new Date()).format('YYYY-MM-DD');
        }
        if (this.reportedAdultDetails.length) {
            submissionData['clientname'] = this.getFullName(this.reportedAdultDetails[0]);
        }

        if (this.isHouseHold.length !== 0) {
            submissionData['otherinhousehold'] = [];
            this.isHouseHold.forEach(item => {
                submissionData['otherinhousehold'].push({
                    name: this.getFullName(item),
                    relationtoclient: item.relationship
                });
            });
        }

        if (this.nonHouseHold.length !== 0) {
            submissionData['relativesandinterestedothers'] = [];
            this.nonHouseHold.forEach(item => {
                submissionData['relativesandinterestedothers'].push({
                    nameandaddress: this.getFullName(item) + ', ' + this.getAddress(item),
                    reationshiptoclient: item.relationship
                });
            });
        }
        submissionData['casenumber'] = daNumber;
        submissionData['panel7290293051369339ColumnsDateoffirstfacetocontact'] = '';

        return submissionData;
    }

    public fillInvestigationOutcomeReport716B(submissionData, daNumber, workerName) {
        submissionData['userrole'] = this.token.role.name;
        submissionData['clientnumber'] = daNumber;
        submissionData['workerdate'] = moment(new Date()).format('YYYY-MM-DD');
        // submissionData['supervisordate'] = moment(new Date()).format('YYYY-MM-DD');

        if (this.reportedAdultDetails.length) {
            submissionData['clientname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['dateofreferral'] = this.getFormattedDate(this.intakeWorkerDetails.routedon);
        }

        if (this.intakeWorkerDetails) {
            submissionData['supervisorname'] = this.intakeWorkerDetails.tousername;
            submissionData['officername'] = this.intakeWorkerDetails.fromusername;
        }
        if (workerName) {
            submissionData['workername'] = workerName.da_assignedto.toUpperCase();
            submissionData['reportnumber'] = workerName.intakenumber;
        }

        return submissionData;
    }

    public fillProjectHomeApplication(submissionData) {
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedAdultDetails) {
            submissionData['phonenumber'] = this.reportedAdultDetails[0].phonenumber;
            submissionData['applicantsname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['dob'] = this.getFormattedDate(this.reportedAdultDetails[0].dob);
            submissionData['ethnicity'] = this.reportedAdultDetails[0].ethnicgrouptypekey ? this.reportedAdultDetails[0].ethnicgrouptypekey : '';
            submissionData['secondaryphone'] = this.reportedAdultDetails[0].secondaryphone ? this.reportedAdultDetails[0].secondaryphone : '';
            submissionData['currentaddress'] = this.reportedAdultDetails[0].currentaddress ? this.reportedAdultDetails[0].currentaddress : '';
            submissionData['address'] = this.getAddress(this.reportedAdultDetails[0]);
            if (this.reportedAdultDetails[0].gender === 'M') {
                submissionData['gender'] = 'male';
            }
            if (this.reportedAdultDetails[0].gender === 'F') {
                submissionData['gender'] = 'female';
            }
            if (this.reportedAdultDetails[0].medicalinformation) {
                this.reportedAdultDetails[0].medicalinformation.map(item => {
                    submissionData['privatehealthinsuranceid'] = item.policyname;
                    if (item.ismedicaidmedicare) {
                        submissionData['privatehealthinsurance'] = 'Yes';
                        submissionData['medicare'] = 'Yes';
                    }
                    if (item.isprimaryphycisian) {
                        submissionData['primarycareprovider'] = item.name;
                        submissionData['primarycareproviderphone'] = item.phone;
                        submissionData['primarycareprovideraddress'] = this.getMedicationAddress(item);
                    }
                    if (!item.isprimaryphycisian) {
                        submissionData['medicalspecialists'] = item.name;
                        submissionData['medicalspecialistsphone'] = item.phone;
                        submissionData['medicalspecialistsaddress'] = this.getMedicationAddress(item);
                    }
                });
            }

            if (this.reportedAdultDetails[0].behavioralhealth) {
                const behaviour = this.reportedAdultDetails[0].behavioralhealth[0];
                submissionData['currentpsychiatricdiagnoses'] = behaviour.currentdiagnoses;
                submissionData['psychiatricprovider'] = behaviour.clinicianname;
                submissionData['psychiatricproviderphone'] = behaviour.behavioralphone;
                submissionData['psychiatricprovideraddress'] = this.getBehaviourAddress(behaviour);
            }

            if (this.reportedAdultDetails[0].medicalcondition) {
                let medicalCondition = '';
                this.reportedAdultDetails[0].medicalcondition.forEach(item => {
                    medicalCondition += item.description + ',';
                });
                submissionData['currentmedicaldiagnoses'] = medicalCondition;
            }
            if (this.reportedAdultDetails[0].medicationinformation) {
                submissionData['medicationinformation'] = [];
                this.reportedAdultDetails[0].medicationinformation.forEach(item => {
                    submissionData['medicationinformation'].push({
                        medication: item.medicationname,
                        dosage: item.dosage,
                        frequency: item.frequency,
                        purpose: item.compliant
                    });
                });
            }
            if (this.reportedAdultDetails[0].addendum) {
                submissionData['chemicaldependencyInformation'] = [];
                if (this.reportedAdultDetails[0].addendum[0].isusedrug) {
                    submissionData['chemicaldependencyInformation'].push({
                        substance: '5',
                        lastdateused: '',
                        howused: '',
                        frequency: this.reportedAdultDetails[0].addendum[0].drugfrequencydetails
                    });
                }
                if (this.reportedAdultDetails[0].addendum[0].isusealcohol) {
                    submissionData['chemicaldependencyInformation'].push({
                        substance: '1',
                        lastdateused: '',
                        howused: '',
                        frequency: this.reportedAdultDetails[0].addendum[0].drugfrequencydetails
                    });
                }
                if (this.reportedAdultDetails[0].addendum[0].isusetobacco) {
                    submissionData['chemicaldependencyInformation'].push({
                        substance: '16',
                        lastdateused: '',
                        howused: '',
                        frequency: this.reportedAdultDetails[0].addendum[0].drugfrequencydetails
                    });
                }
            }
        }
        if (this.intakeWorkerDetails) {
            submissionData['referralsourcename'] = this.intakeWorkerDetails.fromusername;
            submissionData['agency'] = 'Adult Service';
            submissionData['reasonforrefferal'] = this.intakeWorkerDetails.purpose;
            submissionData['agencyphone'] = this.intakeWorkerDetails.phonenumber;
            submissionData['refferaladdress'] = this.intakeWorkerDetails.address;
        }
        return submissionData;
    }

    public fillResidentAgreement(submissionData) {
        submissionData['witnessacknowledgedate'] = moment(new Date()).format('DD/MM/YYYY');
        submissionData['responsiblepartydate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['providerdate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['casemanagerdate'] = moment(new Date()).format('YYYY-MM-DD');
        if (this.intakeWorkerDetails) {
            submissionData['casemanager'] = this.intakeWorkerDetails.tousername;
        }
        return submissionData;
    }

    public fillMedicalPractitioner(submissionData, caseworker) {
        submissionData['printdate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['practitionersdate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['printname'] = caseworker;
        if (this.reportedAdultDetails) {
            submissionData['applicantname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['applicantdob'] = this.getFormattedDate(this.reportedAdultDetails[0].dob);
        }
        return submissionData;
    }

    public fillFosterCare(submissionData) {
        submissionData['clientname'] = this.getFullName(this.reportedAdultDetails[0]);
        submissionData['clientdob'] = this.getFormattedDate(this.reportedAdultDetails[0].dob);
        submissionData['dateofscoring'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    public fillPsychiatricPractitioner(submissionData) {
        if (this.reportedAdultDetails) {
            submissionData['applicantname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['applicantdob'] = this.getFormattedDate(this.reportedAdultDetails[0].dob);
        }

        return submissionData;
    }

    public form248aServiceApplication(submissionData) {
        if (this.intakeWorkerDetails) {
            submissionData['panel45452126397085246ColumnsServicesRequested'] = this.intakeWorkerDetails.intakeservreqservicekey;
        }
        if (this.reportedAdultDetails) {
            submissionData['panel45452126397085246ColumnsApplicantsName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['panel45452126397085246Columns2Date'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['panel45452126397085246Columns2Date2'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['panel45452126397085246Columns4Date'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['panel45452126397085246Columns4Date2'] = moment(new Date()).format('YYYY-MM-DD');
        }
        return submissionData;
    }

    public adultProtectiveServicesInvestigation(submissionData, caseworker, assessmentSummary) {
        if (this.reportedAdultDetails) {
            submissionData['clientname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['clientid'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['clientaddress'] =
                this.reportedAdultDetails[0].address +
                ', ' +
                this.reportedAdultDetails[0].address2 +
                ', ' +
                this.reportedAdultDetails[0].county +
                ', ' +
                this.reportedAdultDetails[0].city +
                ', ' +
                this.reportedAdultDetails[0].state +
                ', ' +
                this.reportedAdultDetails[0].zipcode;
            submissionData['caseWorkerName'] = caseworker;
            submissionData['workerdate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['admindate'] = moment(new Date()).format('YYYY-MM-DD');
        }
        if (assessmentSummary) {
            const narrative = assessmentSummary.general[0].narrative.replace(/<\/?[^>]+(>|$)/g, '');
            submissionData['referalrecieved'] = narrative;
        }
        if (assessmentSummary) {
            submissionData['closedate'] = assessmentSummary.assessmentcontactnotes[0].submittedupdatedate;
            assessmentSummary.assessmentcontactnotes.forEach(element => {
                submissionData['facetofacedate'] = (element.contactdate = moment(new Date()).format('MM-DD-YYYY')) + ' - ' + element.progressreasonnote + ' - ' + element.description;
            });
        }
        if (this.intakeWorkerDetails) {
            submissionData['supervisorName'] = this.intakeWorkerDetails.tousername;
            submissionData['clientdate'] = this.intakeWorkerDetails.routedon ? this.intakeWorkerDetails.routedon : '';
        }
        return submissionData;
    }

    public rankingScale(submissionData, caseworker, dsdsAction) {
        if (this.reportedAdultDetails) {
            submissionData['panel15437179833037074Clientname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['panel15437179833037074Date'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['Formcompletedby'] = caseworker;
        }
        if (dsdsAction) {
            submissionData['panel15437179833037074Clientname3'] = dsdsAction.da_number;
        }
        return submissionData;
    }

    public caseloadPriorityAnalysisWorkSheet(submissionData, caseworker, dsdsAction) {
        if (this.reportedAdultDetails) {
            submissionData['Date'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['caseWorkerName'] = caseworker;
            submissionData['panel08875273220862745Columns2TextField'] = caseworker;
        }
        if (caseworker) {
            submissionData['clientName'] = this.reportedAdultDetails[0].cjamspid;
        }
        submissionData['panel08875273220862745Columns2Date'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    public crissisInterventionSummary(submissionData, caseworker, generalSummary, assessmentSummary) {
        if (this.reportedAdultDetails) {
            submissionData['Clientname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ClientAddress'] =
                this.reportedAdultDetails[0].address +
                ', ' +
                this.reportedAdultDetails[0].address2 +
                ', ' +
                this.reportedAdultDetails[0].county +
                ', ' +
                this.reportedAdultDetails[0].city +
                ', ' +
                this.reportedAdultDetails[0].state +
                ', ' +
                this.reportedAdultDetails[0].zipcode;
            submissionData['Casemanager'] = caseworker;
            submissionData['caseManagerDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['adultServiceAdminDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['Dateofinvestigationsummary'] = moment(new Date()).format('YYYY-MM-DD');
        }
        if (caseworker) {
            submissionData['ClientID'] = this.reportedAdultDetails[0].cjamspid;
        }
        if (generalSummary) {
            const narrative = generalSummary.narrative.replace(/<\/?[^>]+(>|$)/g, '');
            submissionData['Content'] = narrative;
        }
        if (assessmentSummary) {
            assessmentSummary.assessmentcontactnotes.forEach(element => {
                submissionData['DatesOfContact'] = (element.contactdate = moment(new Date()).format('MM-DD-YYYY')) + ' - ' + element.progressreasonnote + ' - ' + element.description;
            });
        }

        if (this.intakeWorkerDetails) {
            submissionData['AdultServiceAdmin'] = this.intakeWorkerDetails.tousername;
            submissionData['Dateofinvestigationsummary2'] = this.intakeWorkerDetails.routedon ? this.intakeWorkerDetails.routedon : '';
        }
        return submissionData;
    }

    fillSocialServiceToAdult(submissionData, caseWorkerName, investigationSummary) {
        if (this.reportedAdultDetails) {
            submissionData['clientname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['clientid'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['clientaddresssetClientaddress'] = this.getFullAddress(this.reportedAdultDetails[0]);
        }
        if (investigationSummary) {
            const narrative = investigationSummary.general[0].narrative.replace(/<\/?[^>]+(>|$)/g, '');
            submissionData['ACTUALREFERRALRECEIVED'] = narrative;
            let facetoFace = '';
            if (investigationSummary.assessmentcontactnotes) {
                investigationSummary.assessmentcontactnotes.forEach(element => {
                    // tslint:disable-next-line:max-line-length
                    facetoFace +=
                        (moment(element.contactdate).format('MM-DD-YYYY') ? moment(element.contactdate).format('MM-DD-YYYY') : 'N/A') +
                        ' - ' +
                        (element.progressreasonnote ? element.progressreasonnote : 'N/A') +
                        ' - ' +
                        (element.description ? element.description : 'N/A') +
                        ', ';
                });
            }
            submissionData['DATESOFFACETOFACE'] = facetoFace;
        }
        if (caseWorkerName) {
            submissionData['CaseManager'] = caseWorkerName;
            submissionData['caseManagerDate'] = moment(new Date()).format('YYYY-MM-DD');
        }
        if (this.intakeWorkerDetails) {
            submissionData['AdministratorDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['initialreferraldate'] = moment(this.intakeWorkerDetails.routedon).format('YYYY-MM-DD');
            submissionData['Administrator'] = this.intakeWorkerDetails.tousername;
        }
        return submissionData;
    }
    public caseworkerPreparesConsentForReleaseOfInformation(submissionData, daNumber, caseWorkerName) {
        if (this.reportedAdultDetails) {
            submissionData['We'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ResidingAt'] =
                this.reportedAdultDetails[0].address +
                ', ' +
                this.reportedAdultDetails[0].address2 +
                ', ' +
                this.reportedAdultDetails[0].county +
                ', ' +
                this.reportedAdultDetails[0].city +
                ', ' +
                this.reportedAdultDetails[0].state +
                ', ' +
                this.reportedAdultDetails[0].zipcode;
            submissionData['NameOfIndividuals'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['IndividualAgency'] = caseWorkerName;
        }
        submissionData['CurrentDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['CaseworkerDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['AdultServiceDate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    fillSSTAassessment(submissionData, investigationSummary) {
        if (this.reportedAdultDetails) {
            submissionData['CLIENTNAME'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['CLIENTID'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['CLIENTADDRESS'] = this.getFullAddress(this.reportedAdultDetails[0]);
        }
        if (this.caseWorkerDetail) {
            submissionData['DATEOFINITIALREFERRAL'] = moment(this.caseWorkerDetail.routedon).format('YYYY-MM-DD');
            submissionData['AdultServicesAdmin'] = this.caseWorkerDetail.fromusername;
            submissionData['CaseManager'] = this.caseWorkerDetail.tousername;
        }

        if (investigationSummary) {
            const narrative = investigationSummary.general[0].narrative.replace(/<\/?[^>]+(>|$)/g, '');
            submissionData['ACTUALREFERRALRECEIVED'] = narrative;
        }

        submissionData['DATESOFCONTACT'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['CaseManagerDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['AdminDate'] = moment(new Date()).format('YYYY-MM-DD');

        return submissionData;
    }
    /**
     * Assessment - Placement Management - 050.9 - D-216 - start
     * projectHomeHospitalToHomeMedicalAssessment - Retrieving RA full name , Case Worker name and current date
     * @param submissionData
     * @param caseworker
     */

    public projectHomeHospitalToHomeMedicalAssessment(submissionData, caseWorkerName) {
        if (this.reportedAdultDetails) {
            submissionData['Resident'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['Date'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['CurrentDate1'] = moment(new Date()).format('YYYY-MM-DD');
        }
        if (caseWorkerName) {
            submissionData['SocialWorker'] = caseWorkerName;
        }
        return submissionData;
    }

    /**
     * Assessment - Placement Management - 050.9 - D-216 - end
     */

    fiill248C(submissionData) {
        if (this.reportedAdultDetails) {
            submissionData['CLIENTNAME'] = this.getFullName(this.reportedAdultDetails[0]);
        }

        submissionData['panel745516953337998Columns29Date'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['panel745516953337998Columns29Date2'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    fillPaymentWorkSheet(submissionData, investigationSummary, financialDisclosureSubmissionData) {
        if (this.reportedAdultDetails) {
            submissionData['panel7343066446014732FacilityName2'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (financialDisclosureSubmissionData) {
            submissionData['TWM'] = financialDisclosureSubmissionData.salary;
            submissionData['UWI'] = +financialDisclosureSubmissionData.totalIncome - +financialDisclosureSubmissionData.salary;
        }
        submissionData['month'] = moment(new Date())
            .format('MMM-YYYY')
            .toString();
        let facetoFace = '';
        if (investigationSummary.providername) {
            investigationSummary.providername.forEach(element => {
                // tslint:disable-next-line:max-line-length
                facetoFace += element.providername + ', ';
            });
        }
        submissionData['panel7343066446014732FacilityName'] = facetoFace;
        return submissionData;
    }

    fillCareRatingInstrument(submissionData) {
        if (this.reportedAdultDetails) {
            submissionData['panel5760450622454003ColumnsClientName'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (this.supervisorDetail) {
            submissionData['panel5760450622454003ColumnsCaseManager'] = this.caseWorkerDetail.tousername;
        }
        return submissionData;
    }
    fillProjectHomeMedication(submissionData) {
        if (this.reportedAdultDetails) {
            submissionData['ResidentsName'] = this.getFullName(this.reportedAdultDetails[0]);
            if (this.reportedAdultDetails[0].medicationinformation) {
                submissionData['panel06311922059450592Columns2DataGrid'] = [];
                this.reportedAdultDetails[0].medicationinformation.forEach(data => {
                    submissionData['panel06311922059450592Columns2DataGrid'].push({
                        TreatmentsWithDirections: data.medicationname + ', ' + data.frequency + ', ' + data.dosage,
                        ReasonforMedicationorTreatment: data.prescriptionreason,
                        RelatedTestingorMonitoring: data.monitoring
                    });
                });
            }
        }
        submissionData['date'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }
    fillRegisteredNurse(submissionData, intakeAssessment, investigationSummary) {
        if (this.reportedAdultDetails) {
            submissionData['Resident'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (intakeAssessment.assignedto) {
            submissionData['RegisteredNurseConsultant'] = intakeAssessment.assignedto;
        }
        if (investigationSummary.providername) {
            let providerName = '';
            investigationSummary.providername.forEach(element => {
                providerName += element.providername + ', ';
            });
            submissionData['CareProvider'] = providerName;
        }
        return submissionData;
    }
    fillAdministrationOfMedication(submissionData, intakeAssessment) {
        if (intakeAssessment.assignedto) {
            submissionData['RegisteredNurseConsultant'] = intakeAssessment.assignedto;
        }
        submissionData['mainDate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }
    clientrightsandresponsibilities(submissionData, caseWorkerName) {
        if (this.reportedAdultDetails) {
            submissionData['CoustomersName'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (caseWorkerName) {
            submissionData['CaseManagerName'] = caseWorkerName;
        }
        submissionData['CoustomersDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['CaseworkerManagerDate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    projecthomeregisterednurseconsultantinitialassessmentofresident(submissionData, caseworker, placement: Placement[]) {
        submissionData['DateCompleted'] = moment(new Date()).format('YYYY-MM-DD');
        if (this.reportedAdultDetails) {
            submissionData['RESIDENTSNAME'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ResidentsDOB1'] = this.reportedAdultDetails[0].dob;
            submissionData['ResidentsName2'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['Date2'] = this.reportedAdultDetails[0].dob;
            submissionData['ResidentsName3'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['Date3'] = this.reportedAdultDetails[0].dob;
            submissionData['NextReviewDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['NurseConsDate'] = moment(new Date()).format('YYYY-MM-DD');

            if (placement.length) {
                submissionData['CAREProvider'] = placement[0].providerinfo.providername;
                submissionData['PlacementDate'] = placement[0].placedatetime;
                submissionData['panel5000110429470297Columns2PhoneNumber'] = placement[0].providerinfo.phonenumber;
            }
        }
        return submissionData;
    }

    welcometoadultservices(submissionData, caseWorkerName) {
        submissionData['DateReviewed'] = moment(new Date()).format('YYYY-MM-DD');
        if (caseWorkerName) {
            submissionData['WorkerSupervisor'] = caseWorkerName;
        }
        return submissionData;
    }
    fillQaterlyInterimReview(submissionData, intakAssessment, investigationSummary) {
        submissionData['DateCompleted'] = moment(new Date()).format('YYYY-MM-DD');
        if (this.reportedAdultDetails) {
            submissionData['ResidentsName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ResidentsDOB'] = moment(this.reportedAdultDetails[0].dob).format('YYYY-MM-DD');
            submissionData['PhoneNumber'] = this.reportedAdultDetails[0].phonenumber ? this.reportedAdultDetails[0].phonenumber : '';
        }
        if (intakAssessment) {
            if (investigationSummary.providername) {
                let providerName = '';
                investigationSummary.providername.forEach(element => {
                    providerName += element.providername + ', ';
                });
                submissionData['CAREProvider'] = providerName;
            }
        }
        if (this.reportedAdultDetails[0].medicationinformation) {
            submissionData['subPanel2DataGrid'] = [];
            this.reportedAdultDetails[0].medicationinformation.forEach(data => {
                submissionData['subPanel2DataGrid'].push({
                    Medicationname: data.medicationname,
                    Dosage: data.dosage,
                    Frequency: data.frequency,
                    Prescriptionreason: data.prescriptionreason
                });
            });
        }
        if (intakAssessment.assignedto) {
            submissionData['NurseConsultant'] = intakAssessment.assignedto;
        }
        submissionData['Date'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['NextDate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    fillBackupPlan(submissionData, daNumber, caseWorkerName) {
        submissionData['Date'] = moment(new Date()).format('YYYY-MM-DD');
        if (this.reportedAdultDetails) {
            submissionData['CaseName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['Client'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (daNumber) {
            submissionData['CaseNumber'] = daNumber;
        }
        if (caseWorkerName) {
            submissionData['Worker'] = caseWorkerName;
        }
        return submissionData;
    }

    fill515B(submissionData, caseWorkerName, daNumber) {
        if (caseWorkerName) {
            submissionData['WORKER'] = caseWorkerName;
        }
        if (this.reportedAdultDetails) {
            submissionData['ClientName'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (daNumber) {
            submissionData['CaseNumber'] = daNumber;
        }
        submissionData['Date'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    fill515BpartB(submissionData, caseWorkerName, daNumber) {
        if (this.reportedAdultDetails) {
            submissionData['ClientName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ClientID'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['MedicationDataGrid'] = [];
            this.reportedAdultDetails[0].medicationinformation.forEach(data => {
                submissionData['MedicationDataGrid'].push({
                    Medicationprescribed: data.medicationname,
                    Dosage: data.dosage,
                    Frequency: data.frequency,
                    Physiciansdiagnosis: data.prescriptionreason
                });
            });
        }
        /* if (daNumber) {
            submissionData['ClientID'] = daNumber;
        } */
        if (caseWorkerName) {
            submissionData['WorkerSupervisor'] = caseWorkerName;
        }
        submissionData['Date'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['SignDate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    fill515AIHasReferralForm(submissionData, caseWorkerName, daNumber, caseWorkerEmail, caseWorkerPhone, rankScoreData, financialDisclosureSubmissionData) {
        submissionData['Date1'] = moment(new Date()).format('YYYY-MM-DD');
        if (this.reportedAdultDetails) {
            submissionData['ClientName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ClientId'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['SSN'] = this.reportedAdultDetails[0].ssn ? this.reportedAdultDetails[0].ssn : '';
            submissionData['Dob'] = moment(this.reportedAdultDetails[0].dob).format('YYYY-MM-DD');
            if (this.reportedAdultDetails[0].gender === 'M') {
                submissionData['Gender'] = 'male';
            }
            if (this.reportedAdultDetails[0].gender === 'F') {
                submissionData['Gender'] = 'female';
            }
            submissionData['Race'] = this.reportedAdultDetails[0].racetypekey ? this.reportedAdultDetails[0].racetypekey : '';
            // submissionData['PrimaryLanguage'] = ;
            submissionData['ClientPhone'] = this.reportedAdultDetails[0].phonenumber ? this.reportedAdultDetails[0].phonenumber : '';
            submissionData['Clientemail'] = this.reportedAdultDetails[0].email ? this.reportedAdultDetails[0].email : '';
            submissionData['ClientAddress'] = this.getAddress(this.reportedAdultDetails[0]);
            submissionData['MailingAddress'] = this.reportedAdultDetails[0].currentaddress ? this.reportedAdultDetails[0].currentaddress : '';
        }
        submissionData['panel38033198743883956DataGrid'] = [];
        this.involvedPersonOtherRA.forEach(item => {
            submissionData['panel38033198743883956DataGrid'].push({
                DataGridInvolvedResourcesName: this.getFullName(item)
            });
        });
        if (caseWorkerName) {
            submissionData['CaseManager'] = caseWorkerName;
            submissionData['Phone'] = caseWorkerPhone;
            submissionData['Email'] = caseWorkerEmail;
        }
        /* this._datastore.currentStore.subscribe(store => {
            this.receivedDate = this._datastore.getData('dsdsActionsSummary');
            this.intakeAcceptedDate = this._datastore.getData('CASEWORKER_ROUTING_INFO');
            for (let i = 0; i < this.intakeAcceptedDate.length; i++) {
                submissionData['DateReceived'] = moment(this.intakeAcceptedDate[0].routedon).format('YYYY-MM-DD');
             }
            submissionData['AcceptedDate'] = moment(this.receivedDate.da_receiveddate).format('YYYY-MM-DD');
        });*/
        if (financialDisclosureSubmissionData) {
            submissionData['Monthlyinc'] = financialDisclosureSubmissionData.totalIc;
            submissionData['Assets'] = financialDisclosureSubmissionData.totalAssets;
        }
        if (this.isHouseHold.length !== 0) {
            submissionData['panel4342720049881712DataGrid'] = [];
            this.isHouseHold.forEach(item => {
                submissionData['panel4342720049881712DataGrid'].push({
                    HouseholdMembersName: this.getFullName(item),
                    HouseholdMembersRelationship: item.relationship
                });
            });
        }
        if (this.reportedAdultDetails[0].medicationinformation) {
            submissionData['panel9744967065395473DataGrid'] = [];
            this.reportedAdultDetails[0].medicationinformation.forEach(item => {
                submissionData['panel9744967065395473DataGrid'].push({
                    DataGridPhysiciansDiagnosis: item.prescriptionreason,
                    DataGridMedicationPrescribed: item.medicationname,
                    DataGridDosage: item.dosage,
                    DataGridFrequency: item.frequency,
                    DataGridSourceofInformation: item.informationsource
                });
            });
        }
        if (this.reportedAdultDetails[0].medicalinformation) {
            this.reportedAdultDetails[0].medicalinformation.forEach(item => {
                if (item.ismedicaidmedicare === true) {
                    submissionData['CurrentParticipantMedicaidYesNo'] = 'yes';
                }
                if (item.ismedicaidmedicare === false) {
                    submissionData['CurrentParticipantMedicaidYesNo'] = 'no';
                }
            });
        }
        if (rankScoreData) {
            submissionData['Rank'] = rankScoreData.score;
        }
        return submissionData;
    }

    fillIHASServicePlan(submissionData, investigationSummary, caseWorkerName) {
        if (this.reportedAdultDetails) {
            submissionData['Client'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ClientId'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['RequestedDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['CaretakerDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['ClientCaretaker'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (this.reportedAdultDetails[0].emergency) {
            let emergencyContact = '';
            let emergencyPhone = '';
            this.reportedAdultDetails[0].emergency.forEach(element => {
                emergencyContact += (element.firstname ? element.firstname : '') + ' ' + (element.firstname ? element.lastname : '') + ', ';
                if (element.phonenumber) {
                    emergencyPhone += (element.phonenumber ? element.phonenumber : '') + ', ';
                }
            });
            submissionData['EmergencyContactPerson'] = emergencyContact;
            submissionData['PIDTextField'] = emergencyPhone;
        }
        if (caseWorkerName) {
            submissionData['CaseManager'] = caseWorkerName;
            submissionData['CaseManagerDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['IHASSupervisorDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['DateServiceAgreementiseffective'] = moment(new Date()).format('YYYY-MM-DD');
        }
        if (investigationSummary.serviceplan) {
            submissionData['mainPanelDataGrid'] = [];
            investigationSummary.serviceplan.forEach(element => {
                if (element.goal === 'To promote a safe environment: Assisting client to limit activities') {
                    submissionData['Assistingclienttolimitactivities'] = true;
                }
                if (element.goal === 'To provide long term maintenance to:  retard disease progression') {
                    submissionData['retarddiseaseprogression'] = true;
                }
                if (element.goal === 'To provide long term maintenance to: maintain client capacity') {
                    submissionData['Maintainclientcapacity'] = true;
                }
                if (element.goal === 'To promote a safe environment: Teach client/caretaker to avoid frustrating/unsafe procedures') {
                    submissionData['toavoidfrustratingunsafeprocedures'] = true;
                }
                if (element.goal === 'To care for disabled adult or child when primary caregiver is absent') {
                    submissionData['primarycaregiverisabsent'] = true;
                }
                if (element.goal === 'To achieve a satisfactory level of comfort and dignity during illness') {
                    submissionData['dignityduringillness'] = true;
                }
                if (element.goal === 'To teach home management, self-care or caretaking skills') {
                    submissionData['Careorcaretakingskills'] = true;
                }
                if (element.goal === 'To provide short-term care to persons who are acutely ill') {
                    submissionData['whoareacutelyill'] = true;
                }
                if (element.goal === 'To promote: Social contact') {
                    submissionData['Socialcontact'] = true;
                }
                if (element.goal === 'To promote: Activity') {
                    submissionData['Activity'] = true;
                }
                if (element.goal === 'To promote: Proper diet') {
                    submissionData['Properdiet'] = true;
                }
                if (element.goal === 'To promote: Independence') {
                    submissionData['Independence'] = true;
                }
                if (element.task === 'Personal Care') {
                    submissionData['PersonalCareSelectBoxesField']['PersonalCareServices'] = true;
                    submissionData['PersonalTextField'] = element.noofhours;
                }
                if (element.task === 'Assist with:budgeting / paying bills') {
                    submissionData['payingbillsSelectBox']['payingServices'] = true;
                    submissionData['payingbillsTextField'] = element.noofhours;
                }
                if (element.task === 'Make/Change bed') {
                    submissionData['ChangebedSelectBox']['ChangeBedServices'] = true;
                    submissionData['ChangebedTextField'] = element.noofhours;
                }
                if (element.task === 'Vacuum/sweep/mop floor') {
                    submissionData['mopfloorSelectBox']['mopfloorServices'] = true;
                    submissionData['mopfloorTextField'] = element.noofhours;
                }
                if (element.task === 'Tidy living areas/empty trash') {
                    submissionData['emptytrashSelectBox']['emptyTrashService'] = true;
                    submissionData['emptytrashTextField'] = element.noofhours;
                }
                if (element.task === 'Wash dishes/clean kitchen area') {
                    submissionData['WashdishesSelectBox']['washdishServices'] = true;
                    submissionData['WashdishesTextField'] = element.noofhours;
                }
                if (element.task === 'Laundry') {
                    submissionData['LaundrySelectBox']['LaunderyServices'] = true;
                    submissionData['LaundryTextField'] = element.noofhours;
                }
                if (element.task === 'Plan/Prepare B L D S / Shop for food') {
                    submissionData['ShopforfoodSelectBox']['ShopServices'] = true;
                    submissionData['ShopforfoodTextField'] = element.noofhours;
                }
                if (element.task === 'Care of Assistive Devices') {
                    submissionData['AssistiveDevicesSelectBox']['AssistiveDevicesServices'] = true;
                    submissionData['AssistiveDevicesTextField'] = element.noofhours;
                }
                if (element.task === 'Transportation/Escort:Medical/Rx') {
                    submissionData['TransportationSelectBox']['MedicalServices'] = true;
                    submissionData['TransportationTextField'] = element.noofhours;
                }
                if (element.task === 'Trans/Escort:supplies etc') {
                    submissionData['TransSelectBox']['TransServices'] = true;
                    submissionData['TransTextField'] = element.noofhours;
                }
                if (element.task === 'Teach') {
                    submissionData['TeachSelectBox']['TeachServices'] = true;
                    submissionData['TeachTextField'] = element.noofhours;
                }
                if (element.task === 'Respite Care/Supervision') {
                    submissionData['RespiteCareSelectBoxesField']['SupervisionServices'] = true;
                    submissionData['RespiteCareTextField'] = element.noofhours;
                }

                submissionData['mainPanelDataGrid'].push({
                    TasksIndicatetask: element.task ? element.task : '',
                    Daysweek: element.noofweeks ? element.noofweeks : '',
                    Hoursday: element.noofhours ? element.noofhours : '',
                    Agency: element.providertypekey ? element.providertypekey : ''
                });
            });
        }
        return submissionData;
    }

    fillIHASCASEMONTHLYUPDATE(submissionData, daNumber, investigationSummary) {
        if (this.reportedAdultDetails) {
            submissionData['Clientname'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        if (daNumber) {
            submissionData['Casenumber'] = daNumber;
        }
        if (investigationSummary.serviceplan) {
            let vendorName = '';
            investigationSummary.serviceplan.forEach(element => {
                if (element.vendorname) {
                    vendorName += element.vendorname + ', ';
                }
            });
            submissionData['Aidename'] = vendorName;
        }
        submissionData['Reportdate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['signatureDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['supervisorSignatureDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['dateReport'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }
    fillPurchaseServiceOrder(submissionData, investigationSummary) {
        if (this.reportedAdultDetails[0]) {
            submissionData['Client'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['Primaryclientnumber'] = this.reportedAdultDetails[0].cjamspid;
        }
        if (investigationSummary.serviceplan) {
            this.vendorProviderName = [];
            investigationSummary.serviceplan.forEach(element => {
                if (element.vendorname) {
                    this.vendorProviderName.push(element.vendorname);
                }
                submissionData['VendorProvider'] = this.vendorProviderName;
                /* for (let i = 0; i < submissionData['VendorProvider'].length; i++) {
                    if (submissionData['VendorProvider'][i] === element.providername) {
                        submissionData['Bpo'] = element.bponumber;
                        submissionData['Ldss'] = element.countyname;
                    }
                } */
            });
        }
        submissionData['AideDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['FiscalDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['VendorDate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    fillGuardianShip(submissionData, daNumber, investigationSummary) {
        submissionData['Familycase'] = daNumber;
        if (this.reportedAdultDetails) {
            submissionData['ClientName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['Dob'] = moment(this.reportedAdultDetails[0].dob).format('YYYY-MM-DD');
            let age = moment(new Date()).diff(moment(this.reportedAdultDetails[0].dob)) / 1000;
            age /= 60 * 60 * 24;
            const ageYear = Math.abs(Math.round(age / 365.25));
            submissionData['Age'] = ageYear;

            if (this.reportedAdultDetails[0].guardianinfo) {
                submissionData['GuardianDataGrid'] = [];
                this.reportedAdultDetails[0].guardianinfo.forEach(element => {
                    submissionData['GuardianDataGrid'].push({
                        GuardianName: element.lastname || '' + ', ' + element.firstname || '',
                        StartDateField: element.startdate || '',
                        EndDate: element.enddate || ''
                    });
                });
            }

            if (this.reportedAdultDetails[0].guardianpropertyinfo) {
                submissionData['GuardianEstateDataGrid'] = [];
                this.reportedAdultDetails[0].guardianpropertyinfo.forEach(element => {
                    submissionData['GuardianEstateDataGrid'].push({
                        GuardianName2: element.lastname || '' + ', ' + element.firstname || '',
                        Columns15Address: element.address || '',
                        Columns15Phone: element.phonenumber || ''
                    });
                });
            }

            if (this.reportedAdultDetails[0].guardianattornyinfo) {
                submissionData['AttorneyDataGrid'] = [];
                this.reportedAdultDetails[0].guardianattornyinfo.forEach(element => {
                    submissionData['AttorneyDataGrid'].push({
                        GuardianName3: element.lastname || '' + ', ' + element.firstname || '',
                        Columns16Address: element.address || '',
                        Columns16Phone: element.phonenumber || ''
                    });
                });
            }

            if (this.reportedAdultDetails[0].guardianworkerinfo) {
                submissionData['PreviewDataGrid'] = [];
                this.reportedAdultDetails[0].guardianworkerinfo.forEach(element => {
                    submissionData['PreviewDataGrid'].push({
                        GuardianName5: element.lastname || '' + ', ' + element.firstname || '',
                        Columns18Address: element.address || '',
                        Columns18Phone: element.phonenumber || ''
                    });
                });
            }

            if (this.reportedAdultDetails[0].guardiafuneralinfo) {
                submissionData['FuneralDataGrid'] = [];
                this.reportedAdultDetails[0].guardiafuneralinfo.forEach(element => {
                    submissionData['FuneralDataGrid'].push({
                        GuardianName4: element.contact || '',
                        Columns17Address: element.arrangementsdescription || '',
                        Columns17Phone: element.contactnumber || ''
                    });
                });
            }

            let hhsCode = '';
            let codeStatus = '';
            this.reportedAdultDetails[0].guardiacodeinfo.forEach(element => {
                hhsCode = element.hhsclientid + ', ' || '';
                codeStatus = element.codestatustypekey + ', ' || '';
            });
            submissionData['HHSClient'] = hhsCode;
            submissionData['Columns10CodeStatus'] = codeStatus;

            if (this.reportedAdultDetails[0].medicalinformation) {
                submissionData['PhysicianInformationDataGrid'] = [];
                submissionData['BehavioralHealthDataGrid'] = [];
                this.reportedAdultDetails[0].medicalinformation.forEach(data => {
                    if (data.isprimaryphycisian) {
                        submissionData['PhysicianInformationDataGrid'].push({
                            Columns7PCP: 'Yes',
                            Columns7NAME: data.name || '',
                            Columns7Phone2: data.phone || '',
                            Columns12County: data.state
                        });
                    }
                    if (!data.isprimaryphycisian) {
                        submissionData['BehavioralHealthDataGrid'].push({
                            Columns7CLINICIAN: data.clinicianname || '',
                            Columns7Address: data.behavioraladdress1 || '',
                            Columns7ReportName: data.reportname || ''
                        });
                    }
                });
            }
        }

        if (this.nonHouseHold) {
            submissionData['FriendFamilyDataGrid'] = [];
            this.nonHouseHold.forEach(data => {
                submissionData['FriendFamilyDataGrid'].push({
                    Columns7Name: this.getFullName(data),
                    Columns7Relationship: data.relationship || '',
                    Columns7Phone: data.phonenumber || ''
                });
            });
        }

        if (investigationSummary) {
            submissionData['PlacementDataGrid2'] = [];
            investigationSummary.serviceplan.forEach(element => {
                submissionData['PlacementDataGrid2'].push({
                    Columns6Name: element.vendorname,
                    Columns6Address: element.countyname
                });
            });
        }
        return submissionData;
    }

    fillCAREMedicalBackup(submissionData, countyAddress) {
        if (countyAddress) {
            submissionData['countyDetailsText'] = countyAddress[0].countyname + ', ' + countyAddress[0].address + ', ' + countyAddress[0].zipcode;
        }
        return submissionData;
    }

    fillMarylandAPS(submissionData, investigationSummary, countyAddress, caseWorkerName) {
        if (countyAddress) {
            submissionData['LocalDss'] = countyAddress[0].countyname;
        }
        if (caseWorkerName) {
            submissionData['Worker'] = caseWorkerName;
        }

        if (investigationSummary) {
            submissionData['panel874281714840814Columns2DateofReferral'] = investigationSummary.general[0].reporteddate;
            submissionData['DateCompletedAssessment'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['ApsCaseId'] = investigationSummary.general[0].servicerequestnumber;
        }

        if (this.reportedAdultDetails) {
            submissionData['ClientId'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['panel5951609357989913ColumnsFirstName'] = this.reportedAdultDetails[0].firstname.toUpperCase() || '';
            submissionData['panel5951609357989913Columns3LastName'] = this.reportedAdultDetails[0].lastname.toUpperCase() || '';
            submissionData['panel5951609357989913Columns4Dob'] = this.getFormattedDate(this.reportedAdultDetails[0].dob);
            if (this.reportedAdultDetails[0].gender === 'M') {
                submissionData['panel5951609357989913Columns5Gender'] = 'Male';
            }
            if (this.reportedAdultDetails[0].gender === 'F') {
                submissionData['panel5951609357989913Columns5Gender'] = 'Female';
            }
            submissionData['panel5951609357989913Columns7Race'] = this.reportedAdultDetails[0].racetypekey ? this.reportedAdultDetails[0].racetypekey : '';
            submissionData['panel5951609357989913Columns6Ethnicity'] = this.reportedAdultDetails[0].ethinicity ? this.reportedAdultDetails[0].ethinicity : '';
            submissionData['panel5951609357989913Columns3StreetAddress'] = this.reportedAdultDetails[0].address ? this.reportedAdultDetails[0].address : '';
            submissionData['panel5951609357989913Columns4CityZipCode'] = this.reportedAdultDetails[0].zipcode ? this.reportedAdultDetails[0].zipcode : '';
            submissionData['panel5951609357989913Columns5Phone'] = this.reportedAdultDetails[0].phonenumber ? this.reportedAdultDetails[0].phonenumber : '';
            submissionData['panel5951609357989913ColumnsMaritalStatus'] = this.reportedAdultDetails[0].maritalstatus ? this.reportedAdultDetails[0].maritalstatus : '';
        }

        if (this.isHouseHold.length > 0) {
            submissionData['Othersinthehouseholdinterestedothers'] = [];
            this.isHouseHold.map(item => {
                submissionData['Othersinthehouseholdinterestedothers'].push({
                    othersinthehouseholdinterestedothersName: item.lastname.toUpperCase() + ', ' + item.firstname.toUpperCase(),
                    othersinthehouseholdinterestedothersRelationship: item.relationship ? item.relationship : '',
                    othersinthehouseholdinterestedothersPhone: item.phonenumber ? item.phonenumber : ''
                });
            });
        }

        if (this.reportedAdultDetails[0].medicalinformation) {
            let name = '';
            let phone = '';
            let address = '';
            this.reportedAdultDetails[0].medicalinformation.map(data => {
                if (data.isprimaryphycisian && data.name) {
                    name += data.name + ', ';
                }
                if (data.isprimaryphycisian && data.phone) {
                    phone += data.phone + ', ';
                }
                if (data.isprimaryphycisian && data.address1 && data.zip) {
                    address += data.address1 + ' ' + data.zip + ', ';
                }
            });
            submissionData['PrimaryCarePhysicianName'] = name;
            submissionData['PcpPhoneNumber'] = phone;
            submissionData['PcpAddress'] = address;
        }

        if (this.reportedAdultDetails[0].medicationinformation) {
            submissionData['currentMedicationDataGrid'] = [];
            this.reportedAdultDetails[0].medicationinformation.map(list => {
                submissionData['currentMedicationDataGrid'].push({
                    currentMedication: list.medicationname ? list.medicationname : ''
                });
            });
        }
        return submissionData;
    }

    fillRespiteApplicationForAdults(submissionData, investigationSummary, caseWorker) {
        if (investigationSummary && investigationSummary.l_county) {
            let countyAddress = '';
            if (investigationSummary.l_county[0].countyname) {
                countyAddress += investigationSummary.l_county[0].countyname + ', ';
            }
            if (investigationSummary.l_county[0].address) {
                countyAddress += investigationSummary.l_county[0].address + ', ';
            }
            if (investigationSummary.l_county[0].zipcode) {
                countyAddress += investigationSummary.l_county[0].zipcode + ', ';
            }
            if (investigationSummary.l_county[0].phonenumber) {
                countyAddress += investigationSummary.l_county[0].phonenumber;
            }
            submissionData['CountyDetails'] = countyAddress;
        }

        if (caseWorker) {
            submissionData['ReporterDetails'] = caseWorker;
        }

        if (this.reportedAdultDetails) {
            submissionData['ApplicantsName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['TodaysDate'] = moment(new Date()).format('YYYY-MM-DD');
        }

        return submissionData;
    }

    fillRespitePhysicianStatement(submissionData, investigationSummary, caseWorker) {

        if (investigationSummary && investigationSummary.l_county) {
            let countyAddress = '';
            if (investigationSummary.l_county[0].countyname) {
                countyAddress += investigationSummary.l_county[0].countyname + ', ';
            }
            if (investigationSummary.l_county[0].address) {
                countyAddress += investigationSummary.l_county[0].address + ', ';
            }
            if (investigationSummary.l_county[0].zipcode) {
                countyAddress += investigationSummary.l_county[0].zipcode + ', ';
            }
            if (investigationSummary.l_county[0].phonenumber) {
                countyAddress += investigationSummary.l_county[0].phonenumber;
            }
            submissionData['CountyDetails'] = countyAddress;
        }

        if (caseWorker) {
            submissionData['RespiteContact'] = caseWorker;
        }
        if (this.reportedAdultDetails) {
            submissionData['TodaysDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['PatientsName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['DateofBirth'] = this.getFormattedDate(this.reportedAdultDetails[0].dob);
            submissionData['panel9277666126438151Date'] = moment(new Date()).format('YYYY-MM-DD');
        }
        return submissionData;
    }

    fillAdultChild(submissionData, investigationSummary, caseWorker) {

        if (investigationSummary && investigationSummary.l_county) {
            let countyAddress = '';
            if (investigationSummary.l_county[0].countyname) {
                countyAddress += investigationSummary.l_county[0].countyname + ', ';
            }
            if (investigationSummary.l_county[0].address) {
                countyAddress += investigationSummary.l_county[0].address + ', ';
            }
            if (investigationSummary.l_county[0].zipcode) {
                countyAddress += investigationSummary.l_county[0].zipcode + ', ';
            }
            if (investigationSummary.l_county[0].phonenumber) {
                countyAddress += investigationSummary.l_county[0].phonenumber;
            }
            submissionData['CountyDetails'] = countyAddress;
        }

        if (caseWorker) {
            submissionData['ReporterDetails'] = caseWorker;
        }
        if (this.reportedAdultDetails) {
            submissionData['TodaysDate'] = moment(new Date()).format('YYYY-MM-DD');
            submissionData['ApplicantsName'] = this.getFullName(this.reportedAdultDetails[0]);
        }
        return submissionData;
    }

    fillFamilyDesignation(submissionData, investigationSummary, caseWorker) {

        if (investigationSummary && investigationSummary.l_county) {
            let countyAddress = '';
            if (investigationSummary.l_county[0].countyname) {
                countyAddress += investigationSummary.l_county[0].countyname + ', ';
            }
            if (investigationSummary.l_county[0].address) {
                countyAddress += investigationSummary.l_county[0].address + ', ';
            }
            if (investigationSummary.l_county[0].zipcode) {
                countyAddress += investigationSummary.l_county[0].zipcode + ', ';
            }
            if (investigationSummary.l_county[0].phonenumber) {
                countyAddress += investigationSummary.l_county[0].phonenumber;
            }
            submissionData['CountyDetails'] = countyAddress;
        }

        if (caseWorker) {
            submissionData['RespiteContact'] = caseWorker;
        }
        if (this.involvedPersons) {
            let names = '';
            this.involvedPersons.map(data => names += data.firstname + ' ' + data.lastname + ', ');
            submissionData['Printfamilymember'] = names;
            submissionData['Printnamesofpersons'] = names;
            submissionData['panel28354855851372474Columns5Date'] = moment(new Date()).format('YYYY-MM-DD');
        }
        return submissionData;
    }

    universalCare(submissionData, investigationSummary, caseWorker) {
        if (investigationSummary && investigationSummary.l_county) {
            let countyAddress = '';
            if (investigationSummary.l_county[0].countyname) {
                countyAddress += investigationSummary.l_county[0].countyname + ', ';
            }
            if (investigationSummary.l_county[0].address) {
                countyAddress += investigationSummary.l_county[0].address + ', ';
            }
            if (investigationSummary.l_county[0].zipcode) {
                countyAddress += investigationSummary.l_county[0].zipcode + ', ';
            }
            if (investigationSummary.l_county[0].phonenumber) {
                countyAddress += investigationSummary.l_county[0].phonenumber;
            }
            submissionData['CountDetails'] = countyAddress;
        }

        if (caseWorker) {
            submissionData['RespiteContact'] = caseWorker;
        }
        submissionData['TodaysDate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    fillServicepanAgreement(submissionData, investigationSummary, caseWorker) {

        if (this.reportedAdultDetails) {
            submissionData['FirstName'] = this.reportedAdultDetails[0].firstname.toUpperCase();
            submissionData['LastName'] = this.reportedAdultDetails[0].lastname.toUpperCase();
            submissionData['PlanName'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['ContactId'] = this.reportedAdultDetails[0].cjamspid;
            submissionData['Ssn'] = this.reportedAdultDetails[0].ssn;
        }
        if (caseWorker) {
            const splitedName = caseWorker.split(/[ ,]+/);
            submissionData['ContactFirstName'] = splitedName[0];
            submissionData['ContactLastName'] = splitedName[1];
        }
        if (investigationSummary.l_serviceplangoal) {
            submissionData['panel8533942300266406Serviceplangoal'] = [];
            investigationSummary.l_serviceplangoal.map((list, index) => {
                    submissionData['panel8533942300266406Serviceplangoal'].push({
                        IntialConditionProblem: list.initialcondition ? list.initialcondition : '',
                        GoalCategory: list.goalcategory ? list.goalcategory : '',
                        Challenges: list.challenges ? list.challenges : '',
                        Strengths: list.strengths ? list.strengths : '',
                        Goal: list.goal ? list.goal : '',
                        PlannedStart: list.plannedstartdate ? list.plannedstartdate : '',
                        PlannedEnd: list.plannedenddate ? list.plannedenddate : ''
                    });
                    const currentGoalLength = submissionData['panel8533942300266406Serviceplangoal'].length;
                    submissionData['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'] = [];
                    submissionData['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'].push({
                        serviceplanactivitygridObjectives: '',
                        Status: '',
                        serviceplanactivitygridPlannedStart: '',
                        serviceplanactivitygridPlannedEnd: '',
                        serviceplanactivitygridInterventionTaskTaskOwner: '',
                        serviceplanactivitygridPersonresponsible: '',
                        serviceplanactivitygridProviderNendor: ''
                    });
                if (list.activitydetails) {
                    list.activitydetails.map((data) => {
                        submissionData['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'].push({
                            serviceplanactivitygridObjectives: data.activity ? data.activity : '',
                            Status: data.serviceplanactivitystatustypekey ? data.serviceplanactivitystatustypekey : '',
                            serviceplanactivitygridPlannedStart: data.plannedstartdate ? data.plannedstartdate : '',
                            serviceplanactivitygridPlannedEnd: data.plannedenddate ? data.plannedenddate : '',
                            serviceplanactivitygridInterventionTaskTaskOwner: data.activitysubtype ? data.activitysubtype : '',
                            serviceplanactivitygridPersonresponsible: data.responsibleperson ? data.responsibleperson : '',
                            serviceplanactivitygridProviderNendor: data.vendorname ? data.vendorname : ''
                        });
                    });
                }
            });
        }
        if (investigationSummary) {
            submissionData['CaseName'] = investigationSummary.servicesubtype ? investigationSummary.servicesubtype : '';
            submissionData['Program'] = investigationSummary.servicetype ? investigationSummary.servicetype : '';
            submissionData['IntakeDate'] = investigationSummary.general[0].reporteddate ? moment(investigationSummary.general[0].reporteddate).format('YYYY-MM-DD') : '';
            submissionData['CreatedBy'] = investigationSummary.general[0].caseworkername ? investigationSummary.general[0].caseworkername : '';
            submissionData['CaseStatus'] = investigationSummary.status ? investigationSummary.status : 'ACTIVE';
            submissionData['StartDate'] = investigationSummary.startdate ? moment(investigationSummary.startdate).format('YYYY-MM-DD') : '';
            submissionData['EndDate'] = investigationSummary.enddate ? moment(investigationSummary.enddate).format('YYYY-MM-DD') : '';
            submissionData['PlanStatus'] = investigationSummary.status ? investigationSummary.status : 'ACTIVE';
            submissionData['CaseNumber'] = investigationSummary.general[0].servicerequestnumber ? investigationSummary.general[0].servicerequestnumber : '';
        }
        submissionData['panel23741618812634702ColumnsDateTimeField'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['panel42571741492674553ColumnsDate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['Date1'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['Date4'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['Date3'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['Date2'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }
}
