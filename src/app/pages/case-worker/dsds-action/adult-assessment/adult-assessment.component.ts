import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Assessments, AssessmentSummary, GetintakAssessment, InvolvedPerson, RoutingInfo, Documentproperty, CompleteAssessment } from '../../_entities/caseworker.data.model';
import { PaginationInfo, PaginationRequest, ListDataItem } from '../../../../@core/entities/common.entities';
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash';
import FormioExport from 'formio-export';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Placement } from '../service-plan/_entities/service-plan.model';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericService, CommonHttpService, DataStoreService, AuthService, AlertService, SessionStorageService } from '../../../../@core/services';
import { HttpService } from '../../../../@core/services/http.service';
import { AppConfig } from '../../../../app.config';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Attachment } from '../attachment/_entities/attachment.data.models';
import { map } from 'rxjs/operators';
import { InvestigationSummary } from '../disposition/_entities/disposition.data.models';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { environment } from '../../../../../environments/environment.dev';
import * as moment from 'moment';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

declare var Formio: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'adult-assessment',
    templateUrl: './adult-assessment.component.html',
    styleUrls: ['./adult-assessment.component.scss']
})
export class AdultAssessmentComponent implements OnInit {
    id: string;
    daNumber: string;
    assessmmentName: string;
    showAssmnt: boolean;
    startAssessment$: Assessments[];
    assessmentSummary$: Observable<AssessmentSummary[]>;
    totalRecords$: number;
    paginationInfo: PaginationInfo = new PaginationInfo();
    private pageSubject$ = new Subject<number>();
    formBuilderUrl: string;
    safeUrl: SafeResourceUrl;
    showAssesment = -1;
    getAsseesmentHistory: GetintakAssessment[] = [];
    templateComponentData: any;
    templateSubmissionData: any;
    getScoreOnClose: boolean;
    refreshForm: any;
    involvedPersons: InvolvedPerson[];
    routingInfo: RoutingInfo[];
    placement: Placement[];
    roleId: AppUser;
    intakersList: ListDataItem<any>;
    selectedPractitionar: any;
    selectedAssessment: any;
    filteredAttachmentGrid: Attachment[] = [];
    linkedAttachment: string[] = [];
    documentPropertyID: Documentproperty[] = [];
    completeAssessment = new CompleteAssessment();
    investigationSummary: InvestigationSummary;
    financialDisclosureSubmissionData: any;
    rankScoreData: any;
    assessmentTemplateId: any;
    EMail: string;
    firstName: string;
    lastName: string;
    enableflag = true;
    dsdsActionSummary: any;
    countyid: string;
    countyAddress = [];
    formioOptions: {
        formio: {
            ignoreLayout: true;
            emptyValue: '-';
        };
    };
    constructor(
        private route: ActivatedRoute,
        private _service: GenericService<Assessments>,
        private _commonService: CommonHttpService,
        public sanitizer: DomSanitizer,
        private _http: HttpService,
        private _dataStoreService: DataStoreService,
        private _router: Router,
        private _authService: AuthService,
        private _alertService: AlertService,
        private storage: SessionStorageService,
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
    }
    ngOnInit() {
        this.roleId = this._authService.getCurrentUser();
        const storeData = this._dataStoreService.getCurrentStore();
        storeData['CASEWORKER_SELECTED_ASSESSMENT'] = '';
        this._dataStoreService.setObject(storeData, true, 'CASEWORKER_ASSESSMENT_LOAD');

        if (storeData['dsdsActionsSummary']) {
            this.dsdsActionSummary = storeData['dsdsActionsSummary'];
            if (this.dsdsActionSummary.countyid) {
                this.countyid = this.dsdsActionSummary.countyid;
                this.getCountyDetails();
            }
        }

        this.getPage(1);
        this.getAssessmentPrefillData();
        this.getAssessmentSubmission();
        this.attachment();
    }

    getPage(page: number) {
        this._http.overrideUrl = false;
        this._http.baseUrl = AppConfig.baseUrl;
        this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: page,
                    limit: 10,
                    where: {
                        servicerequestid: this.id,
                        categoryid: null,
                        subcategoryid: null,
                        targetid: null,
                        assessmentstatus: null
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.ListAssessment + '?filter'
            )
            .subscribe((result: any) => {
                this.startAssessment$ = result.data;
                if (page === 1) {
                    this.totalRecords$ = result.count;
                }
                // return { data: result.data, count: result.count };
            });

        this.assessmentSummary$ = this._commonService
            .getPagedArrayList(
                new PaginationRequest({
                    where: {
                        investigationid: this.id
                    },
                    method: 'get'
                }),
                'admin/assessment/GetSummary' + '?filter'
            )
            .map(result => {
                return result.data;
            });
    }

    getCountyDetails() {
        this._commonService.getArrayList({}, 'stateoffice/list?filter={}').subscribe(data => {
            if (data) {
                this.countyAddress = data.filter(list => list.countyid === this.countyid);
            }
        });
    }

    getAssessmentSubmission() {
        this._commonService
            .getPagedArrayList(
                {
                    assessment: [
                        {
                            titleheadertext: '248 C Financial Disclosure'
                        },
                        {
                            titleheadertext: '515C RANKING SCALE'
                        }
                    ],
                    intakeserviceid: this.id,
                    method: 'post'
                },
                'admin/assessment/getassessmentdetailsbytitleheadertext'
            )
            .subscribe(list => {
                if (list.data.length) {
                    for (let i = 0; i < list.data.length; i++) {
                        if (list.data[i].titleheadertext === '248 C Financial Disclosure') {
                            this.financialDisclosureSubmissionData = {
                                salary: list.data[i].submissiondata.c2,
                                totalIncome: list.data[i].submissiondata.tc,
                                totalAssets: list.data[i].submissiondata.ta1,
                                totalIc: list.data[i].submissiondata.ti1
                            };
                        }
                        if (list.data[i].titleheadertext === '515C RANKING SCALE') {
                            this.rankScoreData = {
                                score: list.data[i].submissiondata.score
                            };
                        }
                    }
                }
            });
    }
    startAssessment(assessment: GetintakAssessment, mode) {
        assessment.mode = mode;
        const storeData = this._dataStoreService.getCurrentStore();
        storeData['CASEWORKER_INVOLVED_PERSON'] = this.involvedPersons;
        storeData['CASEWORKER_ROUTING_INFO'] = this.routingInfo;
        storeData['CASEWORKER_PLACEMENT'] = this.placement;
        storeData['INVESTIGATION_SUMMARY'] = this.investigationSummary;
        storeData['CASEWORKER_SELECTED_ASSESSMENT'] = assessment;
        storeData['248CSubmission'] = this.financialDisclosureSubmissionData;
        storeData['515CRankScale'] = this.rankScoreData;
        storeData['CountyAddress'] = this.countyAddress;
        this._dataStoreService.setObject(storeData, true, 'CASEWORKER_ASSESSMENT_LOAD');
        this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/adult-assessment/adult-caseworker-assessment']);
    }
    assessmentPrintView(assessment: GetintakAssessment, needData) {
        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        let url = '';
        if (needData) {
            url = environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`;
        } else {
            url = environment.formBuilderHost + `/form/${assessment.external_templateid}`;
        }
        Formio.createForm(document.getElementById('assessmentForm'), url, {
            readOnly: true
        }).then(function (submission) {
            const options = {
                ignoreLayout: true
            };
            _self.viewHtml(submission._form, submission._submission, options);
        });
    }
    viewHtml(componentData, submissionData, formioOptions) {
        delete submissionData._id;
        delete submissionData.owner;
        delete submissionData.modified;
        const exporter = new FormioExport(componentData, submissionData, formioOptions);
        if (this._authService.isDJS() && componentData.title === 'Intake Detention Risk Assessment Instrument') {
            exporter.component.components[0].components[2].components[0]._value = ('' + exporter.component.components[0].components[2].components[0]._value);
            exporter.component.components[0].components[3].components[0]._value = ('' + exporter.component.components[0].components[3].components[0]._value);
            exporter.component.components[0].components[4].components[0]._value = ('' + exporter.component.components[0].components[4].components[0]._value);
            exporter.component.components[0].components[5].components[0]._value = ('' + exporter.component.components[0].components[5].components[0]._value);
            exporter.component.components[0].components[6].components[0]._value = ('' + exporter.component.components[0].components[6].components[0]._value);
            exporter.component.components[0].components[7].components[0]._value = ('' + exporter.component.components[0].components[7].components[0]._value);
            exporter.component.components[0].components[0].columns[0].components[1]._value =
                (moment(new Date(exporter.component.components[0].components[0].columns[0].components[1]._value.slice(0, 10))).format('MM/DD/YYYY'));
            exporter.component.components[0].components[0].columns[1].components[3]._value =
                (moment(new Date(exporter.component.components[0].components[0].columns[1].components[3]._value.slice(0, 10))).format('MM/DD/YYYY'));
        }
        const appDiv = document.getElementById('divPrintView');
        exporter.toHtml().then((html) => {
            if (this._authService.isDJS() && componentData.title === 'Intake Detention Risk Assessment Instrument') {
                html.innerHTML = this.replaceAll(html.innerHTML, ' UTC</div>', '</div>');
            }
            html.style.margin = 'auto';
            const iframe = this.createIframe(appDiv);
            const doc = iframe.contentDocument || iframe.contentWindow.document;
            doc.body.appendChild(html);
            window.frames['ifAssessmentView'].focus();
            window.frames['ifAssessmentView'].print();
        });
    }
    escapeRegExp(string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    replaceAll(str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    }
    private createIframe(el) {
        _.forEach(el.getElementsByTagName('iframe'), (_iframe) => {
            el.removeChild(_iframe);
        });
        const iframe = document.createElement('iframe');
        iframe.setAttribute('id', 'ifAssessmentView');
        iframe.setAttribute('name', 'ifAssessmentView');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('webkitallowfullscreen', '');
        iframe.setAttribute('mozallowfullscreen', '');
        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('style', 'width: -webkit-fill-available;height: -webkit-fill-available;');
        el.appendChild(iframe);
        return iframe;
    }
    pageChanged(page: any) {
        this.showAssesment = -1;
        this.getPage(page);
    }

    showAssessment(id: number, row) {
        this.getAsseesmentHistory = row;
        if (this.showAssesment !== id) {
            this.showAssesment = id;
        } else {
            this.showAssesment = -1;
        }
    }
    actionIconDisplay(modal, status: string): boolean {
        if (status === 'View') {
            return modal.assessmentstatustypekey !== 'Open' && modal !== null;
        } else if (status === 'Edit') {
            return (modal.assessmentstatustypekey === 'Rejected' && modal !== null) || (this.roleId.role.name === 'apcs' && modal.assessmentstatustypekey !== 'Open');
        } else if (status === 'Print') {
            return modal.assessmentstatustypekey !== 'Open' && modal !== null;
        } else if (status === 'InProcess') {
            return modal.assessmentstatustypekey === 'InProcess' && modal !== null;
        }
        return false;
    }

    private getAssessmentPrefillData() {
        const source = forkJoin(
            this._commonService.getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeservreqid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            ),
            this._commonService.getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 50,
                    method: 'get',
                    where: { intakeserviceid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.RoutingInfoList
            ),
            this._commonService.getPagedArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    page: 1,
                    limit: 50,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementListUrl + '?filter'
            ),
            this._commonService.getSingle(
                {
                    intakeserviceid: this.id,
                    method: 'post'
                },
                'Investigations/getinvestigationsummary'
            )
        ).subscribe(result => {
            this.involvedPersons = result[0]['data'];
            this.routingInfo = result[1][0]['routinginfo'];
            this.placement = result[2]['data'];
            this.investigationSummary = result[3][0];
            this._dataStoreService.setData('CaseWorkerPhone', this.investigationSummary.general[0].caseworkerphoneno);
            this.involvedPersons.map(data => {
                if (data.emailID && data.rolename === 'PA') {
                    this.enableflag = true;
                }
            });
        });
    }

    getPractitionerList(assessment) {
        if (assessment.description === 'C.A.R.E. Provider - References Backup' || assessment.description === 'C.A.R.E. Home - Provider Back-up Medical Form') {
            for (let i = 0; i < this.involvedPersons.length; i++) {
                if (this.involvedPersons[i].rolename === 'PA') {
                    this.EMail = this.involvedPersons[i].email;
                    this.firstName = this.involvedPersons[i].firstname;
                    this.lastName = this.involvedPersons[i].lastname;

                    this._http
                        .post('Assignedassessments/assessmentassign', {
                            assessmenttemplateid: assessment.assessmenttemplateid,
                            securityusersid: null,
                            intakeserviceid: this.id,
                            email: this.EMail,
                            firstname: this.firstName,
                            lastname: this.lastName
                        })
                        .subscribe(response => {
                            if (response) {
                                this._alertService.success('Assigned Successfully');
                            }
                        });
                }
            }
        } else {
            this.selectedAssessment = assessment;
            const url = 'Assignedassessments/getusersbyrole?filter';
            this._commonService
                .getPagedArrayList(
                    {
                        where: {
                            roletypekey: assessment.targetroletypekey
                        },
                        method: 'get'
                    },
                    url
                )
                .subscribe(list => {
                    this.intakersList = list;
                    (<any>$('#type-select')).modal('show');
                });
        }
    }

    private attachment() {
        this._commonService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/' + this.id + '?data'
            )
            .subscribe(result => {
                result.map(item => {
                    item.numberofbytes = this.humanizeBytes(item.numberofbytes);
                });
                this.filteredAttachmentGrid = result;
                console.log('attachements', this.filteredAttachmentGrid);
            });
    }

    private humanizeBytes(bytes: number): string {
        if (bytes === 0) {
            return '0 Byte';
        }
        if (!bytes) {
            return '';
         }
        const k = 1024;
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        const i: number = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    addAttachment(event) {
        this.documentPropertyID = event.map(items => {
            return { documentpropertiesid: items.documentpropertiesid };
        });
        this.linkedAttachment = event.map(files => files.title);
    }

    completeCaseworkerAssessment(event) {
        this.completeAssessment.assessmenttemplateid = event.assessmenttemplateid;
        this.completeAssessment.objectid = this.id;
        this.completeAssessment.assessmentid = event.assessmentid;

        this._commonService.create(this.completeAssessment, 'Assignedassessments/approvedbycw').subscribe(data => {
            if (data) {
                console.log(data);

                this.getPage(this.paginationInfo.pageNumber);
            } else {
                this._alertService.error('Unable to Process Request');
            }
        });
    }

    rejectAssessment(event) {
        this._commonService.create({ assessmenttemplateid: event.assessmenttemplateid }, 'Assignedassessments/assessmentreject').subscribe(data => {
            if (data) {
                console.log(data);
                this.getPage(1);
            } else {
                this._alertService.error('Unable to Process Request');
            }
        });
    }

    onChangePractitioner(practitionar: any) {
        console.log(practitionar);
        this.selectedPractitionar = practitionar;
    }

    assignToPractitioner() {
        console.log(this.selectedPractitionar);
        let url = '';
        if (this.selectedAssessment['isAssigned']) {
            url = 'Assignedassessments/reassignassessment';
        } else {
            url = 'Assignedassessments/assessmentassign';
        }
        const data = {
            assessmenttemplateid: this.selectedAssessment.assessmenttemplateid,
            securityusersid: this.selectedPractitionar.userid,
            intakeserviceid: this.id
        };
        this._commonService.create(data, url).subscribe(
            result => {
                console.log('practitionar', result);
                this._alertService.success('Assessment assigned successfully');
                this.selectedAssessment['isAssigned'] = true;
                (<any>$('#type-select')).modal('hide');
            },
            error => {
                console.log('practitionar', error);
                this._alertService.success('Error in Assessment assignement.');
            }
        );
    }

    isAssmentCompelete(submissionData: any) {
        if (submissionData) {
            if (submissionData.Complete === true) {
                return 1;
            }
            if (submissionData.submit === true) {
                return 2;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    confirmDelete(assessmentid, index) {
        // a.index = index;
        console.log('assessmentid....index', assessmentid, index);
        this.assessmentTemplateId = assessmentid;
        (<any>$('#delete-assessment-popup')).modal('show');
    }

    deleteAssessment() {
        this._commonService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.DeleteAssessment;
        this._commonService
            .create({
                assessmentid: this.assessmentTemplateId
                // method: 'post'
            })
            .subscribe(
                response => {
                    if (response) {
                        this._alertService.success('Assessment deleted successfully');
                        (<any>$('#delete-assessment-popup')).modal('hide');
                        this.getAsseesmentHistory = null;
                        this.getPage(1);
                        this.showAssesment = -1;
                    }
                },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }

    viewPrintOption(assessment) {
        if (assessment.description) {
            switch (assessment.description.toUpperCase()) {
                case 'C.A.R.E. PROVIDER - REFERENCES BACKUP':
                    return true;
                case 'C.A.R.E. HOME - PROVIDER BACK-UP MEDICAL FORM':
                    return true;
                case 'RESPITE CARE APPLICATION: PHYSICIAN’S STATEMENT':
                    return true;
                case 'RESPITE APPLICATION: FINANCIAL DISCLOSURE FORM FOR CHILDREN AGES 17 AND UNDER':
                    return true;
                case 'RESPITE APPLICATION: FINANCIAL DISCLOSURE FORM FOR ADULTS':
                    return true;
                case 'UNIVERSAL RESPITE CARE APPLICATION':
                    return true;
                case 'FAMILY DESIGNATED CAREWORKER':
                    return true;
            }
        }
    }
}
