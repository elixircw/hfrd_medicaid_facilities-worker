import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdultCaseworkerAssessmentComponent } from './adult-caseworker-assessment.component';
import { AdultCaseworkerAssessmentRoutingModule } from './adult-caseworker-assessment-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    AdultCaseworkerAssessmentRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    PaginationModule
  ],
  declarations: [
    AdultCaseworkerAssessmentComponent
  ],
  providers: []
})
export class AdultCaseworkerAssessmentModule { }
