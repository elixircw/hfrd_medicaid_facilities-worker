import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { RoutingInfo, GetintakAssessment, DSDSActionSummary } from '../../../_entities/caseworker.data.model';
import { Placement } from '../../service-plan/_entities/service-plan.model';
import { ActivatedRoute, Router } from '@angular/router';
import { InvolvedPerson } from '../../involved-persons/_entities/involvedperson.data.model';
import { AuthService, SessionStorageService, CommonHttpService, AlertService, DataStoreService } from '../../../../../@core/services';
import { HttpService } from '../../../../../@core/services/http.service';
import { environment } from '../../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { AdultAssessmentPreFill } from '../adult-assessment-prefill';
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { InvestigationSummary, General, AssessmentContactNotes } from '../../disposition/_entities/disposition.data.models';
import { Provider } from '../../../../provider-applicant/new-applicant/_entities/newintakeModel';
import { ProviderInfo, Phonenumber } from '../../placement/_entities/placement.model';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var Formio: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'adult-caseworker-assessment',
    templateUrl: './adult-caseworker-assessment.component.html',
    styleUrls: ['./adult-caseworker-assessment.component.scss']
})
export class AdultCaseworkerAssessmentComponent implements OnInit, AfterViewInit {
    id: string;
    generalSummary: General;
    daNumber: string;
    assessmmentName: string;
    investigationSummary: InvestigationSummary;
    providerInfo: ProviderInfo;
    phonenumber: Phonenumber[] = [];
    private token: AppUser;
    selectedSafeCDangerInfluence: any[] = [];
    currentTemplateId: string;
    isChildSafe = true;
    safeCKeys: string[];
    involvedPersons: InvolvedPerson[];
    routingInfo: RoutingInfo[];
    placement: Placement[];
    assessmentSummary: AssessmentContactNotes;
    formioOptions: {
        formio: {
            ignoreLayout: true;
            emptyValue: '-';
        };
    };
    isReadOnlyForm = false;
    intakAssessment = new GetintakAssessment();
    dsdsActionsSummary = new DSDSActionSummary();
    isInitialized = false;
    financialDisclosureSubmissionData: any;
    private formTriggered = false;
    rankScoreData: any;
    caseWorkerPhone: number;
    countyAddress = [];
    constructor(
        private route: ActivatedRoute,
        private _authService: AuthService,
        private storage: SessionStorageService,
        private _commonService: CommonHttpService,
        private _alertService: AlertService,
        private _http: HttpService,
        private _router: Router,
        private _dataStoreService: DataStoreService,
        private _commonHttpService: CommonHttpService
    ) {
        this.safeCKeys = [
            'caregiverdescribes',
            'caregiverfailstoprotect',
            'caregivermadeaplausible',
            'caregiverrefuses',
            'caregiversemotionalinstability',
            'caregiversexplanation',
            'caregiversjustification',
            'caregiverssuspected',
            'childscurrentimminent',
            'childsexualabuse',
            'childswhereabouts',
            'currentactofmaltreatment',
            'domesticviolence',
            'extremelyanxious',
            'multiplereports',
            'servicestothecaregiver',
            'specialneeds',
            'unabletoprotect',
            'servicestothecaregiver2'
        ];
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
    }

    ngOnInit() {
        this.getInvestigationSummary();
        if (this._dataStoreService.getData('CaseWorkerPhone')) {
            this.caseWorkerPhone = this._dataStoreService.getData('CaseWorkerPhone');
        }
        this.token = this._authService.getCurrentUser();
        this.isInitialized = true;
    }

    ngAfterViewInit() {
        this._dataStoreService.currentStore.subscribe(storeData => {
            if (this.isInitialized && storeData['SUBSCRIPTION_TARGET'] === 'CASEWORKER_ASSESSMENT_LOAD') {
                this.involvedPersons = storeData['CASEWORKER_INVOLVED_PERSON'];
                this.routingInfo = storeData['CASEWORKER_ROUTING_INFO'];
                this.investigationSummary = storeData['INVESTIGATION_SUMMARY'];
                this.placement = storeData['CASEWORKER_PLACEMENT'];
                this.intakAssessment = storeData['CASEWORKER_SELECTED_ASSESSMENT'];
                this.dsdsActionsSummary = storeData['dsdsActionsSummary'];
                this.financialDisclosureSubmissionData = storeData['248CSubmission'];
                this.rankScoreData = storeData['515CRankScale'];
                this.countyAddress = storeData['CountyAddress'];
                if (this.intakAssessment) {
                    if (this.intakAssessment.mode === 'start') {
                        this.startAssessment(this.intakAssessment);
                    } else if (this.intakAssessment.mode === 'submit') {
                        this.submittedAssessment(this.intakAssessment);
                    } else if (this.intakAssessment.mode === 'update') {
                        this.updateAssessment(this.intakAssessment);
                    } else if (this.intakAssessment.mode === 'print') {
                        this.assessmentPrintView(this.intakAssessment);
                    }
                } else {
                    this.redirectToAssessment();
                }
                this.isInitialized = false;
            }
        });
    }

    startAssessment(assessment) {
        this.assessmmentName = assessment.description;
        this.currentTemplateId = assessment.external_templateid;
        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`).then(function(form) {
            form.components = form.components.map(item => {
                if (item.key === 'Complete' && item.type === 'button') {
                    item.action = 'submit';
                }
                return item;
            });
            form.submission = {
                data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
            };
            form.on('submit', submission => {
                if (_self.assessmmentName === 'SAFE-C') {
                    submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
                    submission.data['isChildSafe'] = _self.isChildSafe;
                }
                let status = '';
                let comments = '';
                if (_self.token.role.name === 'apcs') {
                    status = submission.data.assessmentstatus;
                    comments = submission.data.supervisorcomments;
                } else if (_self.token.role.name === 'field') {
                    if (submission.data.submit) {
                        status = 'InProcess';
                    } else {
                        status = 'Submitted';
                    }
                    comments = submission.data.caseworkercomments;
                }
                _self._http
                    .post('admin/assessment/Add', {
                        externaltemplateid: _self.currentTemplateId,
                        objectid: _self.id,
                        submissionid: submission._id,
                        submissiondata: submission.data ? submission.data : null,
                        form: submission.form ? submission.form : null,
                        score: submission.data.score ? submission.data.score : 0,
                        ischildsafe: _self.isChildSafe,
                        assessmentstatustypekey1: status ? status : null,
                        comments: comments ? comments : null
                    })
                    .subscribe(response => {
                        _self._alertService.success(_self.assessmmentName + ' saved successfully.');
                        _self.redirectToAssessment();
                        /// (<any>$('#iframe-popup')).modal('hide');
                        if (_self.assessmmentName === 'SAFE-C') {
                            Observable.timer(500).subscribe(() => {
                                _self._router.routeReuseStrategy.shouldReuseRoute = function() {
                                    return false;
                                };
                                _self._router.navigateByUrl(_self._router.url).then(() => {
                                    _self._router.navigated = false;
                                    _self._router.navigate([_self._router.url]);
                                });
                            });
                        } else {
                            if (_self.assessmmentName === 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT') {
                                const participantDetail = [];
                                const reportedChild = _self.involvedPersons.filter(item => {
                                    return item.rolename === 'RC' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RC').length);
                                });
                                let reportedChildName = '';
                                if (reportedChild.length) {
                                    reportedChildName = reportedChild[0].lastname;
                                    if (reportedChild[0].firstname) {
                                        reportedChildName += ', ' + reportedChild[0].firstname;
                                    }
                                }
                                _self.involvedPersons.map(item => {
                                    if (item.rolename !== 'RC' && item.rolename !== 'AM') {
                                        participantDetail.push({
                                            intakeserviceid: _self.id,
                                            personid: item.personid,
                                            relationship: item.relationship,
                                            email: item.email,
                                            firstname: item.firstname,
                                            lastname: item.lastname,
                                            message:
                                                'This is to notify you that the child ' +
                                                reportedChildName.toUpperCase() +
                                                ' is being enrolled in ' +
                                                submission.data.selectedcurrentschool +
                                                ' school based on the best interest assessment for education'
                                        });
                                    }
                                });
                                _self._commonService.create(participantDetail, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.Notification).subscribe(res => {
                                    console.log(res);
                                });
                            }
                        }
                    });
            });
            form.on('change', formData => {
                form.submission = {
                    data: _self.vendorProvider(formData)
                };
            });
            form.on('render', formData => {
                /// (<any>$('#iframe-popup')).modal('show');
                setTimeout(function() {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });

            form.on('error', error => {
                setTimeout(function() {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
                _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
            });
        });
    }
    submittedAssessment(assessment: GetintakAssessment) {
        this.assessmmentName = assessment.titleheadertext;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
            readOnly: true
        }).then(function(submission) {
            /// (<any>$('#iframe-popup')).modal('show');
            submission.on('render', formData => {
                setTimeout(function() {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });
        });
    }
    updateAssessment(assessment: GetintakAssessment) {
        this.assessmmentName = assessment.titleheadertext;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        // this.assessmmentName = assessment.description;
        // this.currentTemplateId = assessment.external_templateid;
        const _self = this;
        // Formio.setToken(this.storage.getObj('fbToken'));
        // Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
            readOnly: false
        }).then(function(form) {
            form.components = form.components.map(item => {
                if (item.key === 'Complete' && item.type === 'button') {
                    item.action = 'submit';
                }
                return item;
            });
            form.submission = {
                data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
            };
            /// (<any>$('#iframe-popup')).modal('show');
            form.on('render', formData => {
                setTimeout(function() {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });
            form.on('submit', submission => {
                if (_self.assessmmentName === 'SAFE-C') {
                    submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
                }
                let status = '';
                let comments = '';
                if (_self.token.role.name === 'apcs') {
                    status = submission.data.assessmentstatus;
                    comments = submission.data.supervisorcomments;
                } else if (_self.token.role.name === 'field') {
                    if (submission.data.submit) {
                        status = 'InProcess';
                    } else {
                        status = 'Submitted';
                    }
                    comments = submission.data.caseworkercomments;
                }
                _self._http
                    .post('admin/assessment/Add', {
                        externaltemplateid: _self.currentTemplateId,
                        objectid: _self.id,
                        submissionid: submission._id,
                        submissiondata: submission.data ? submission.data : null,
                        form: submission.form ? submission.form : null,
                        score: submission.data.score ? submission.data.score : 0,
                        assessmentstatustypekey1: status ? status : null,
                        comments: comments ? comments : null
                    })
                    .subscribe(response => {
                        _self._alertService.success(_self.assessmmentName + ' saved successfully.');
                        // _self.getPage(1);
                        // _self.showAssessment(
                        //     _self.showAssesment,
                        //     _self.getAsseesmentHistory
                        // );
                        _self.redirectToAssessment();
                        /// (<any>$('#iframe-popup')).modal('hide');
                    });
            });
            form.on('change', formData => {
                form.submission = {
                    data: _self.vendorProvider(formData)
                };
            });
            // form.on('render', (formData) => {
            //     (<any>$('#iframe-popup')).modal('show');
            //     setTimeout(function () {
            //         $('#assessment-popup').scrollTop(0);
            //     }, 200);
            // });

            form.on('error', error => {
                setTimeout(function() {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
                _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
            });
        });
    }

    vendorProvider($event) {
        if ($event.changed) {
            if (this.investigationSummary.serviceplan && $event.changed.component.key === 'VendorProvider') {
                this.investigationSummary.serviceplan.map(data => {
                    if (data.vendorname === $event.data['VendorProvider']) {
                        $event.data['DataGrid'] = [];
                        data.details.forEach(element => {
                            $event.data['Bpo'] = element.bponumber || '';
                            $event.data['Ldss'] = element.countyname || '';
                            $event.data['DataGrid'].push({
                                Begindate: element.startdate || '',
                                EndDate: element.enddate || '',
                                hrswk: element.noofhours || 0,
                                noofwks: element.noofweeks || 0,
                                HrRate: element.rate || 0,
                                Service: this.getService(element.categorysubtypekey.toUpperCase())
                            });
                        });
                    }
                });
            }

            if (this.investigationSummary.serviceplan && $event.changed.component.key === 'panel5247102642654851RadioField' && $event.changed.value === 'fullServicePlan') {
                if (this.investigationSummary.l_serviceplangoal) {
                    $event.data['panel8533942300266406Serviceplangoal'] = [];
                    this.investigationSummary.l_serviceplangoal.map((list, index) => {
                            $event.data['panel8533942300266406Serviceplangoal'].push({
                                IntialConditionProblem: list.initialcondition ? list.initialcondition : '',
                                GoalCategory: list.goalcategory ? list.goalcategory : '',
                                Challenges: list.challenges ? list.challenges : '',
                                Strengths: list.strengths ? list.strengths : '',
                                Goal: list.goal ? list.goal : ''
                            });
                            const currentGoalLength = $event.data['panel8533942300266406Serviceplangoal'].length;
                            $event.data['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'] = [];
                            $event.data['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'].push({
                                serviceplanactivitygridObjectives: '',
                                Status: '',
                                serviceplanactivitygridPlannedStart: '',
                                serviceplanactivitygridPlannedEnd: '',
                                serviceplanactivitygridInterventionTaskTaskOwner: '',
                                serviceplanactivitygridPersonresponsible: '',
                                serviceplanactivitygridProviderNendor: ''
                            });
                            if (list.activitydetails) {
                                list.activitydetails.map(data => {
                                    $event.data['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'].push({
                                        serviceplanactivitygridObjectives: data.activity ? data.activity : '',
                                        Status: data.serviceplanactivitystatustypekey ? data.serviceplanactivitystatustypekey : '',
                                        serviceplanactivitygridPlannedStart: data.plannedstartdate ? data.plannedstartdate : '',
                                        serviceplanactivitygridPlannedEnd: data.plannedenddate ? data.plannedenddate : '',
                                        serviceplanactivitygridInterventionTaskTaskOwner: data.activitysubtype ? data.activitysubtype : '',
                                        serviceplanactivitygridPersonresponsible: data.responsibleperson ? data.responsibleperson : '',
                                        serviceplanactivitygridProviderNendor: data.vendorname ? data.vendorname : ''
                                    });
                                });
                            }
                    });
                }
            }

            if (this.investigationSummary.serviceplan && $event.changed.component.key === 'panel5247102642654851RadioField' && $event.changed.value === 'projectHome') {
                if (this.investigationSummary.l_serviceplangoal) {
                    $event.data['panel8533942300266406Serviceplangoal'] = [];
                    this.investigationSummary.l_serviceplangoal.map((list, index) => {
                        if (list.isprojecthome) {
                            $event.data['panel8533942300266406Serviceplangoal'].push({
                                IntialConditionProblem: list.initialcondition ? list.initialcondition : '',
                                GoalCategory: list.goalcategory ? list.goalcategory : '',
                                Challenges: list.challenges ? list.challenges : '',
                                Strengths: list.strengths ? list.strengths : '',
                                Goal: list.goal ? list.goal : ''
                            });
                            const currentGoalLength = $event.data['panel8533942300266406Serviceplangoal'].length;
                            $event.data['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'] = [];
                            $event.data['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'].push({
                                serviceplanactivitygridObjectives: '',
                                Status: '',
                                serviceplanactivitygridPlannedStart: '',
                                serviceplanactivitygridPlannedEnd: '',
                                serviceplanactivitygridInterventionTaskTaskOwner: '',
                                serviceplanactivitygridPersonresponsible: '',
                                serviceplanactivitygridProviderNendor: ''
                            });
                            if (list.activitydetails) {
                                list.activitydetails.map(data => {
                                    $event.data['panel8533942300266406Serviceplangoal'][currentGoalLength - 1]['Serviceplanactivitygrid'].push({
                                        serviceplanactivitygridObjectives: data.activity ? data.activity : '',
                                        Status: data.serviceplanactivitystatustypekey ? data.serviceplanactivitystatustypekey : '',
                                        serviceplanactivitygridPlannedStart: data.plannedstartdate ? data.plannedstartdate : '',
                                        serviceplanactivitygridPlannedEnd: data.plannedenddate ? data.plannedenddate : '',
                                        serviceplanactivitygridInterventionTaskTaskOwner: data.activitysubtype ? data.activitysubtype : '',
                                        serviceplanactivitygridPersonresponsible: data.responsibleperson ? data.responsibleperson : '',
                                        serviceplanactivitygridProviderNendor: data.vendorname ? data.vendorname : ''
                                    });
                                });
                            }
                        }
                    });
                }
            }

            return $event.data;
        } else {
            return $event.data;
        }
    }

    getService(item) {
        switch (item) {
            case 'CHORES':
                return 'A';
            case 'PERSONAL':
                return 'C';
            case 'NURSING CARE':
                return 'D';
            case 'RESPITE':
                return 'F';
        }
    }

    assessmentPrintView(assessment: GetintakAssessment) {
        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
            readOnly: true
        }).then(function(submission) {
            const options = {
                ignoreLayout: true
            };
            _self.viewHtml(submission._form, submission._submission, options);
        });
    }

    viewHtml(componentData, submissionData, formioOptions) {
        delete submissionData._id;
        delete submissionData.owner;
        delete submissionData.modified;
        const exporter = new FormioExport(componentData, submissionData, formioOptions);
        const appDiv = document.getElementById('divPrintView');
        exporter.toHtml().then(html => {
            html.style.margin = 'auto';
            const iframe = this.createIframe(appDiv);
            const doc = iframe.contentDocument || iframe.contentWindow.document;
            doc.body.appendChild(html);
            window.frames['ifAssessmentView'].focus();
            window.frames['ifAssessmentView'].print();
        });
    }

    private createIframe(el) {
        _.forEach(el.getElementsByTagName('iframe'), _iframe => {
            el.removeChild(_iframe);
        });
        const iframe = document.createElement('iframe');
        iframe.setAttribute('id', 'ifAssessmentView');
        iframe.setAttribute('name', 'ifAssessmentView');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('webkitallowfullscreen', '');
        iframe.setAttribute('mozallowfullscreen', '');
        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('style', 'width: -webkit-fill-available;height: -webkit-fill-available;');
        el.appendChild(iframe);
        return iframe;
    }

    private getFormPrePopulation(formName: string, submissionData: any) {
        const prefillUtil = new AdultAssessmentPreFill(this.involvedPersons, this.routingInfo, this.token, this._dataStoreService, location);
        switch (formName.toUpperCase()) {
            case 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT':
                submissionData = prefillUtil.fillTransportationPlan(submissionData, this.daNumber);
                break;
            case 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT':
                submissionData = prefillUtil.fillBestInterestDetermination(submissionData, this.daNumber, this.token.user.userprofile.displayname);
                break;
            case 'CANS-OUT OF HOME PLACEMENT SERVICE':
                submissionData = prefillUtil.fillCansOutOfHomePlacement(submissionData, this.daNumber, this.token.user.userprofile.displayname);
                break;
            case 'CLIENT ASSESSMENT FORM - 716 A':
                submissionData = prefillUtil.fillClientAssessmentForm716A(submissionData, this.daNumber, this.dsdsActionsSummary);
                break;
            case 'INVESTIGATION OUTCOME REPORT - 716 B':
                submissionData = prefillUtil.fillInvestigationOutcomeReport716B(submissionData, this.daNumber, this.dsdsActionsSummary);
                break;
            case 'ADULT PROTECTIVE SERVICE PROGRAM - PROJECT HOME APPLICATION':
                submissionData = prefillUtil.fillProjectHomeApplication(submissionData);
                break;

            case 'PROJECT HOME - RESIDENT AGREEMENT':
                submissionData = prefillUtil.fillResidentAgreement(submissionData);
                break;

            case 'PROJECT HOME - MEDICAL HEALTH CARE PRACTITIONER ASSESSMENT':
                submissionData = prefillUtil.fillMedicalPractitioner(submissionData, this.token.user.userprofile.displayname);
                break;
            case 'PRIORITY RANKING FOR ADULT FOSTER CARE PROJECT HOME':
                submissionData = prefillUtil.fillFosterCare(submissionData);
                break;
            case 'PROJECT HOME - PSYCHIATRIC HEALTH CARE PRACTITIONER ASSESSMENT':
                submissionData = prefillUtil.fillPsychiatricPractitioner(submissionData);
                break;
            case '248-A SERVICE APPLICATION':
                submissionData = prefillUtil.form248aServiceApplication(submissionData);
                break;
            case 'ADULT PROTECTIVE SERVICES INVESTIGATION SUMMARY':
                submissionData = prefillUtil.adultProtectiveServicesInvestigation(submissionData, this.token.user.userprofile.displayname, this.investigationSummary);
                break;
            case '515C RANKING SCALE':
                submissionData = prefillUtil.rankingScale(submissionData, this.token.user.userprofile.displayname, this.dsdsActionsSummary);
                break;
            case 'CASELOAD PRIORITY ANALYSIS WORK SHEET':
                submissionData = prefillUtil.caseloadPriorityAnalysisWorkSheet(submissionData, this.token.user.userprofile.displayname, this.dsdsActionsSummary);
                break;
            case 'CRISIS INTERVENTION SUMMARY':
                submissionData = prefillUtil.crissisInterventionSummary(submissionData, this.token.user.userprofile.displayname, this.generalSummary, this.investigationSummary);
                break;
            case 'SSTA RECON / CLOSING':
                submissionData = prefillUtil.fillSocialServiceToAdult(submissionData, this.token.user.userprofile.displayname, this.investigationSummary);
                break;
            case 'CONSENT FOR RELEASE OF INFORMATION':
                submissionData = prefillUtil.caseworkerPreparesConsentForReleaseOfInformation(submissionData, this.daNumber, this.token.user.userprofile.displayname);
                break;
            // case 'NOTIFICATION OF PLACEMENT-ENTRY AND EXIT RECEIPT':
            // submissionData = prefillUtil.fillNotificationOfPlacement(submissionData, this.daNumber, this.token.user.userprofile.displayname);
            /**
             * Assessment - Case Management - 020.02_Caseworker completes  Financial Disclosure Form (formerly 248C) - start
             */
            // case '248 C FINANCIAL DISCLOSURE':
            //     submissionData = prefillUtil.financialDisclosure(submissionData, this.token.user.userprofile.displayname);
            //     break;
            /**
             * Assessment - Case Management - 020.02_Caseworker completes  Financial Disclosure Form (formerly 248C) - end
             */
            // case 'D216-PROJECT HOME HOSPITAL TO HOME MEDICAL ASSESSMENT':
            //     submissionData = prefillUtil.d216ProjectHomeHospitalToHomeMedicalAssessment(submissionData, this.daNumber);
            //     break;

            /**
             * Assessment - Placement Management - 050.9 - D-216 - start
             */
            case 'D216-PROJECT HOME HOSPITAL TO HOME MEDICAL ASSESSMENT':
                submissionData = prefillUtil.projectHomeHospitalToHomeMedicalAssessment(submissionData, this.token.user.userprofile.displayname);
                break;
            /**
             * Assessment - Placement Management - 050.9 - D-216 - end
             */
            case 'SSTA ASSESSMENT':
                submissionData = prefillUtil.fillSSTAassessment(submissionData, this.investigationSummary);
                break;
            case '248 C FINANCIAL DISCLOSURE':
                submissionData = prefillUtil.fiill248C(submissionData);
                break;
            case 'COMMUNITY RESIDENTIAL SERVICES PLACEMENT - PAYMENT WORKSHEET':
                submissionData = prefillUtil.fillPaymentWorkSheet(submissionData, this.investigationSummary, this.financialDisclosureSubmissionData);
                break;
            case 'CLIENT RIGHTS AND RESPONSIBILITIES':
                submissionData = prefillUtil.clientrightsandresponsibilities(submissionData, this.token.user.userprofile.displayname);
                break;
            case 'PROJECT HOME REGISTERED NURSE CONSULTANT INITIAL ASSESSMENT OF RESIDENT':
                submissionData = prefillUtil.projecthomeregisterednurseconsultantinitialassessmentofresident(submissionData, this.token.user.userprofile.displayname, this.placement);
                break;
            case 'PROJECT HOME LEVEL OF CARE RATING INSTRUMENT':
                submissionData = prefillUtil.fillCareRatingInstrument(submissionData);
                break;
            case 'REGISTERED NURSE CONSULTANT HEALTH SERVICE PLAN':
                submissionData = prefillUtil.fillRegisteredNurse(submissionData, this.intakAssessment, this.investigationSummary);
                break;
            case 'PROJECT HOME MEDICATION/TREATMENT ORDERS':
                submissionData = prefillUtil.fillProjectHomeMedication(submissionData);
                break;
            case 'RESIDENT ASSESSMENT OF SELF-ADMINISTRATION OF MEDICATION':
                submissionData = prefillUtil.fillAdministrationOfMedication(submissionData, this.intakAssessment);
                break;
            case 'WELCOME TO ADULT SERVICES':
                submissionData = prefillUtil.welcometoadultservices(submissionData, this.token.user.userprofile.displayname);
                break;
            case 'PROJECT HOME REGISTERED NURSE CONSULTANT ASSESSMENT QUARTERLY/INTERIM REVIEW':
                submissionData = prefillUtil.fillQaterlyInterimReview(submissionData, this.intakAssessment, this.investigationSummary);
                break;
            case 'IHAS BACK-UP PLAN':
                submissionData = prefillUtil.fillBackupPlan(submissionData, this.daNumber, this.token.user.userprofile.displayname);
                break;
            case 'IHAS FUNCTIONAL ASSESSMENT (515B)':
                submissionData = prefillUtil.fill515B(submissionData, this.token.user.userprofile.displayname, this.daNumber);
                break;
            case '515 B PART B - ELIGIBILITY FOR A NURSING HOME':
                submissionData = prefillUtil.fill515BpartB(submissionData, this.token.user.userprofile.displayname, this.daNumber);
                break;
            case '515 A IHAS REFERRAL FORM':
                submissionData = prefillUtil.fill515AIHasReferralForm(
                    submissionData,
                    this.token.user.userprofile.displayname,
                    this.daNumber,
                    this.token.user.userprofile.email,
                    this.caseWorkerPhone,
                    this.rankScoreData,
                    this.financialDisclosureSubmissionData
                );
                break;
            case 'IHAS SERVICE PLAN':
                submissionData = prefillUtil.fillIHASServicePlan(submissionData, this.investigationSummary, this.token.user.userprofile.displayname);
                break;
            case 'IHAS - CASE MONTHLY UPDATE':
                submissionData = prefillUtil.fillIHASCASEMONTHLYUPDATE(submissionData, this.daNumber, this.investigationSummary);
                break;
            case 'PURCHASE OF SERVICES ORDER':
                submissionData = prefillUtil.fillPurchaseServiceOrder(submissionData, this.investigationSummary);
                break;
            case 'GUARDIANSHIP FACESHEET':
                submissionData = prefillUtil.fillGuardianShip(submissionData, this.daNumber, this.investigationSummary);
                break;
            case 'C.A.R.E. HOME - PROVIDER BACK-UP MEDICAL FORM':
                submissionData = prefillUtil.fillCAREMedicalBackup(submissionData, this.countyAddress);
                break;
            case 'MARYLAND APS ASSESSMENT TOOL':
                submissionData = prefillUtil.fillMarylandAPS(submissionData, this.investigationSummary, this.countyAddress, this.token.user.userprofile.displayname);
                break;
            case 'RESPITE APPLICATION: FINANCIAL DISCLOSURE FORM FOR ADULTS':
                submissionData = prefillUtil.fillRespiteApplicationForAdults(submissionData, this.investigationSummary, this.token.user.userprofile.displayname);
                break;
            case 'RESPITE CARE APPLICATION: PHYSICIAN’S STATEMENT':
                submissionData = prefillUtil.fillRespitePhysicianStatement(submissionData, this.investigationSummary, this.token.user.userprofile.displayname);
                break;
            case 'RESPITE APPLICATION: FINANCIAL DISCLOSURE FORM FOR CHILDREN AGES 17 AND UNDER':
                submissionData = prefillUtil.fillAdultChild(submissionData, this.investigationSummary, this.token.user.userprofile.displayname);
                break;

            case 'FAMILY DESIGNATED CAREWORKER':
                submissionData = prefillUtil.fillFamilyDesignation(submissionData, this.investigationSummary, this.token.user.userprofile.displayname);
                break;
            case 'UNIVERSAL RESPITE CARE APPLICATION':
                submissionData = prefillUtil.universalCare(submissionData, this.investigationSummary, this.token.user.userprofile.displayname);
                break;
            case 'CASE SERVICE PLAN AGREEMENT':
                submissionData = prefillUtil.fillServicepanAgreement(submissionData, this.investigationSummary, this.token.user.userprofile.displayname);
                break;
        }
        return submissionData;
    }

    private safeCProcess($event) {
        if (this.assessmmentName === 'SAFE-C') {
            if ($event.changed) {
                const dangerInfluenceKey = $event.changed.component.key;
                if (dangerInfluenceKey === 'safetydecision4') {
                    this.isChildSafe = !$event.data[$event.changed.component.key];
                }
                if (dangerInfluenceKey && this.safeCKeys.indexOf(dangerInfluenceKey) > -1) {
                    const dangerInflunceItem = this.selectedSafeCDangerInfluence.find(item => item.value === dangerInfluenceKey);
                    if (dangerInflunceItem) {
                        if ($event.changed.value === 'no' || $event.data[$event.changed.component.key] === 'no') {
                            const itemIndex = this.selectedSafeCDangerInfluence.indexOf(dangerInflunceItem);
                            this.selectedSafeCDangerInfluence.splice(itemIndex, 1);
                        }
                        console.log(this.selectedSafeCDangerInfluence);
                    } else {
                        if ($event.changed.value === 'yes' || $event.data[$event.changed.component.key] === 'yes') {
                            this.selectedSafeCDangerInfluence.push({
                                text: $event.changed.component.label,
                                value: dangerInfluenceKey
                            });
                            console.log(this.selectedSafeCDangerInfluence);
                        }
                    }
                }
            }
        } else {
            this.selectedSafeCDangerInfluence = [];
        }
    }

    private getInvestigationSummary() {
        this._commonHttpService
            .getSingle(
                {
                    intakeserviceid: this.id,
                    method: 'post'
                },
                'Investigations/getinvestigationsummary'
            )
            .subscribe(data => {
                if (data.length) {
                    this.investigationSummary = data[0];
                }
            });
    }

    private getPlacementSummary() {
        this._commonHttpService
            .getSingle(
                {
                    intakeserviceid: this.id,
                    method: 'post'
                },
                'placement/list'
            )
            .subscribe(data => {
                if (data.length) {
                    this.providerInfo = data[0];
                    // this.phonenumber = this.providerInfo.phonenumber[0];
                    this.assessmentSummary = this.investigationSummary.assessmentcontactnotes[0];
                }
            });
    }

    redirectToAssessment() {
        this.isInitialized = false;
        this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/adult-assessment']);
    }
}
