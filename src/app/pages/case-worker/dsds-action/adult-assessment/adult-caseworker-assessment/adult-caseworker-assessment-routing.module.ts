import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdultCaseworkerAssessmentComponent } from './adult-caseworker-assessment.component';
const routes: Routes = [
    {
    path: '',
    component: AdultCaseworkerAssessmentComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdultCaseworkerAssessmentRoutingModule { }
