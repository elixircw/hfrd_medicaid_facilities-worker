import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdultCaseworkerAssessmentComponent } from './adult-caseworker-assessment.component';

describe('AdultCaseworkerAssessmentComponent', () => {
  let component: AdultCaseworkerAssessmentComponent;
  let fixture: ComponentFixture<AdultCaseworkerAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultCaseworkerAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdultCaseworkerAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
