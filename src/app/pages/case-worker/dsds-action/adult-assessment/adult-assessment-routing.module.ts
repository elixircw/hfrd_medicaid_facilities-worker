import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdultAssessmentComponent } from './adult-assessment.component';
const routes: Routes = [
    {
    path: '',
    component: AdultAssessmentComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdultAssessmentRoutingModule { }
