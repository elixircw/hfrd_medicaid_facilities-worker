import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FosterCareListComponent } from './foster-care-list.component';

describe('FosterCareListComponent', () => {
  let component: FosterCareListComponent;
  let fixture: ComponentFixture<FosterCareListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FosterCareListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FosterCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
