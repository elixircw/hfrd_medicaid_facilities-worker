import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { FosterCareRoutingModule } from './foster-care-routing.module';
import { FosterCareComponent } from './foster-care.component';
import { FosterCareSearchComponent } from './foster-care-search/foster-care-search.component';
import { FosterCareListComponent } from './foster-care-list/foster-care-list.component';
import { FosterCareReferalComponent } from './foster-care-referal/foster-care-referal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';
import { PaginationModule } from 'ngx-bootstrap';
// import { Title4eModule } from '../../../title4e/title4e.module';

@NgModule({
  imports: [
    CommonModule,
    FosterCareRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    A2Edatetimepicker,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule,
    SharedDirectivesModule,
    PaginationModule
    // Title4eModule
  ],
  declarations: [FosterCareComponent, FosterCareSearchComponent, FosterCareListComponent, FosterCareReferalComponent]
})
export class FosterCareModule { }
