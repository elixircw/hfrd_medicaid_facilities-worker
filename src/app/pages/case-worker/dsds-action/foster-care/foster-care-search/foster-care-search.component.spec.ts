import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FosterCareSearchComponent } from './foster-care-search.component';

describe('FosterCareSearchComponent', () => {
  let component: FosterCareSearchComponent;
  let fixture: ComponentFixture<FosterCareSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FosterCareSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FosterCareSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
