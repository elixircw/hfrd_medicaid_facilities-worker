import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { ComponentLoaderFactory, PaginationModule, PopoverConfig, PopoverModule, PositioningService } from 'ngx-bootstrap';
import { PopoverContent } from 'ngx-popover';
import { NgxfUploaderService } from 'ngxf-uploader';

import { CoreModule } from '../../../../@core/core.module';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { AdoptionPersonsComponent } from './adoption-persons.component';

describe('AdoptionPersonsComponent', () => {
    let component: AdoptionPersonsComponent;
    let fixture: ComponentFixture<AdoptionPersonsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            // tslint:disable-next-line:max-line-length
            imports: [
                RouterTestingModule,
                CoreModule.forRoot(),
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                ControlMessagesModule,
                PaginationModule,
                NgSelectModule,
                A2Edatetimepicker,
                RouterModule,
                PopoverModule
            ],
            declarations: [AdoptionPersonsComponent, PopoverContent],
            providers: [SpeechRecognizerService, NgxfUploaderService, SpeechRecognitionService, PopoverConfig, ComponentLoaderFactory, PositioningService]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AdoptionPersonsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
