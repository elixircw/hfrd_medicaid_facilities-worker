import { DispositionAddModal, Investigation, InvestigationSummary, General, ChildRemoval } from './_entities/disposition.data.models';
import { GenericService } from './../../../../@core/services/generic.service';
import { DataStoreService } from './../../../../@core/services/data-store.service';
import { CaseWorkerUrlConfig } from './../../case-worker-url.config';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as jsPDF from 'jspdf';

import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService } from '../../../../@core/services/alert.service';
import { CommonHttpService } from '../../../../@core/services/common-http.service';
import { PaginationInfo, DropdownModel } from './../../../../@core/entities/common.entities';
import { CaseWorkerHistory, Assessment } from './../../_entities/caseworker.data.model';
import { AuthService } from '../../../../@core/services/auth.service';
import { DSDSActionSummary } from '../../_entities/caseworker.data.model';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { SessionStorageService } from '../../../../@core/services/storage.service';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { IntakeStoreConstants } from '../../../newintake/my-newintake/my-newintake.constants';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { Narrative, IntakeCommunication, CpsDocInput } from '../../../newintake/my-newintake/_entities/newintakeModel';
import { AppConstants } from '../../../../@core/common/constants';
import { ValidationService } from '../../../../@core/services';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { forkJoin } from 'rxjs/observable/forkJoin';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'disposition',
    templateUrl: './disposition.component.html',
    styleUrls: ['./disposition.component.scss']
})
export class DispositionComponent implements OnInit, AfterViewInit {
    daNumber: number;
    dsdsActionsSummary = new DSDSActionSummary();
    id: string;
    dispositionFormGroup: FormGroup;
    getHistory$: Observable<CaseWorkerHistory[]>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    statusDropdownItems$: Observable<DropdownModel[]>;
    programTypeDropdownItems$: Observable<DropdownModel[]>;
    statusDropdownItems: any[];
    intakeCommunication: IntakeCommunication[] = [];
    cpsdocData: CpsDocInput = new CpsDocInput();
    closeCaseDropdownItems$: Observable<DropdownModel[]>;
    dispositionDropdownItems$: Observable<DropdownModel[]>;
    programCodeItems$: Observable<DropdownModel[]>;
    formalSupport$: Observable<DropdownModel[]>;
    dispositionDropdownItems: DropdownModel[];
    programCodeDescription: string[] = [];
    programCodeSelection: any[] = [];
    dispositionStatus: any;
    adultManditory = false;
    requestForService = false;
    disableCPSIntakeReport = false;
    careHomeApplication = false;
    private daType: string;
    private daSubTypeId: string;
    isDjs = false;
    isCW = false;
    showProgramCode = false;
    enableAdultDisposition = false;
    isDefaultsLoaded = false;
    showFamilySupport = false;
    isSupervisor: boolean;
    roleId: AppUser;
    approvalStatusForm: FormGroup;
    isAS = false;
    dsdsActionSummary: DSDSActionSummary = new DSDSActionSummary();
    currentDate = new Date();
    investigationSummary: InvestigationSummary;
    generalSummary: General;
    assessmentSummary: Assessment[];
    childRemovalSummary: ChildRemoval[];
    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    isServiceCase: string;
    isProgramingType: boolean;
    disporitioncode: string[] = [];
    checklistItems = [];

    allChecked: boolean;
    selectedDisposition: any;
    selectedStatus: any;
    approvalDispositionId: any;
    intakedata: any[];
    intakeflag= false;
    reviewStatus: any;
    countyAddress: any;
    countyid: any;
    county: any;
    emailForm: FormGroup;
    daSubType: string;
    ARSummaryApproved: boolean;
    supervisorsList: any[];
    isAppealWorker: boolean;
    isClosed = false;
    isAdoptionCase: boolean;
    vendorlist: any;
    agencylist: any;
    constructor(
        private _router: Router,
        private formBuilder: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _dispositionAddService: GenericService<DispositionAddModal>,
        private _dataStoreService: DataStoreService,
        private _authService: AuthService,
        private storage: SessionStorageService,
    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        const dataStore = this._dataStoreService.getData('dsdsActionsSummary');
        this.daType = dataStore ? dataStore.da_typeid : '';
        this.daSubTypeId = dataStore ? dataStore.da_subtypeid : '';
        this.dsdsActionSummary = dataStore;
    }
    ngOnInit() {
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        const caseType = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
        if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
            this.isAdoptionCase = true;
            this.getAssignmentsList();
            this.getAgreementListing();
        } else {
            this.isAdoptionCase = false;
        }
        this.initializeDispositionForm();
        this.loadCheckListItems();
        this.getAssessments();
        this.getPlacementInfoList();
        this.getChildRemoval();
        this.getInvolvedPerson();

        this.roleId = this._authService.getCurrentUser();
        if (this.roleId.role.name === 'apcs') {
            this.isSupervisor = true;
            this.approvalDispositionId = this.storage.getItem(CASE_STORE_CONSTANTS.DISPOSITIONID_FOR_APPROVAL);
        }
        this.approvalStatusForm = this.formBuilder.group({
            routingstatus: [''],
            comments: [''],
            objid: ['']
        });
        this._dataStoreService.setData(IntakeStoreConstants.communications , this._dataStoreService.getData('da_intakenumber'));
        this.populateIntake(this._dataStoreService.getData('da_intakenumber'));
        this.dsdsActionsSummary = this._dataStoreService.getData('dsdsActionsSummary');
        if (this.dsdsActionSummary) {
            this.daSubType = this.dsdsActionSummary.da_subtype;
            if (this.daSubType === 'CPS-AR') {
                this.checkForARSummaryApproved();
            }
        }

        this._dataStoreService.currentStore.subscribe((store) => {
         if (store['dsdsActionsSummary']) {
                this.dsdsActionsSummary = store['dsdsActionsSummary'];
                this.daType = this.dsdsActionsSummary.da_typeid;

                if (!this.isDefaultsLoaded) {
                    this.loadStatuses();
                    this.isDefaultsLoaded = true;
                }
            }
        });
        if ((this.dsdsActionsSummary.da_type === 'Maltreatment Intake') && (this._authService.getAgencyName() === 'AS')) {
            this.dispositionFormGroup.get('reviewcomments').setValidators([Validators.required]);
            this.dispositionFormGroup.get('closingcodetypekey').setValidators([Validators.required]);
            this.dispositionFormGroup.updateValueAndValidity();
            this.adultManditory = true;
        }
        if ((this.dsdsActionsSummary.da_type === 'Request for services' || this.dsdsActionsSummary.da_type === 'Provider') && (this._authService.getAgencyName() === 'CW')) {
            this.disableCPSIntakeReport = true;
        }
        if ((this.dsdsActionsSummary.da_type === 'Request for services' || this.dsdsActionsSummary.da_type === 'Provider') && (this._authService.getAgencyName() === 'AS')) {
            this.dispositionFormGroup.get('reviewcomments').setValidators([Validators.required]);
            this.dispositionFormGroup.get('closingcodetypekey').setValidators([Validators.required]);
            this.dispositionFormGroup.updateValueAndValidity();
            this.requestForService = true;
        }
        this.isDjs = this._authService.isDJS();
        this.isCW = this._authService.isCW();
        this.isAS = this._authService.isAS();
        this.loadStatuses();
        this.loadSupervisor();
        if (!this.isAS) {
            this.loadCloseCase();
        }
        this.dispositionHistory();
        this.programType();
        this.emailForm = this.formBuilder.group({
            email: ['', [ValidationService.mailFormat, Validators.required]]
        });
        this.getCommunicationList();
        this.roleId = this._authService.getCurrentUser();
        if (this.roleId) {
           const roleName = ObjectUtils.getNestedObject(this.roleId, ['role', 'name']);

           if (roleName === AppConstants.ROLES.APPEAL_USER) {
               this.isAppealWorker = true;
           }
        }
        const da_status = this.storage.getItem('da_status');
    if (da_status) {
     if (da_status === 'Closed' || da_status === 'Completed') {
         this.isClosed = true;
         this.dispositionFormGroup.disable();
     } else {
         this.isClosed = false;
     }
    }
    }
    getCommunicationList() {
        const checkInput = {
          nolimit: true,
          where: { teamtypekey: this._authService.getAgencyName() },
          method: 'get',
          order: 'description'
        };
        return this._commonHttpService.getArrayList(new PaginationRequest(checkInput), NewUrlConfig.EndPoint.Intake.IntakeCommunications + '/list?filter').subscribe(data => {
            console.log('---in communication---');
            console.log(data);
            this.intakeCommunication = data;
            const general = this._dataStoreService.getData(IntakeStoreConstants.general);
            if (general && general.InputSource) {
                this.prepareCpsDocDetails(general.InputSource);
            }
        });
      }
      prepareCpsDocDetails(input: string) {
        if (this.intakeCommunication) {
            this.intakeCommunication.map(data => {
                // if (data.value === input) {
                //     this.cpsdocData.InputSource = data.text;
                // }
                if (data.intakeservreqinputtypeid === input) {
                    this.cpsdocData.InputSource = data.description;
                    this._dataStoreService.setData(
                        IntakeStoreConstants.REFERALSOURCE,
                        {label: this.cpsdocData.InputSource || input}
                    );
                }
            });
        }
    }

    getCountyDetails() {
        this._commonHttpService.getArrayList({}, 'admin/county?filter').subscribe(data => {
            if (data) {
                this.intakeflag = true;
                this.countyAddress = data.filter(list => list.countyid === this.countyid);
                if (this.countyAddress.length > 0) {
                    this.county = this.countyAddress[0].countyname;
                    this._dataStoreService.setData('countyId', this.county);
                }
            }
        });
    }

    fixNarrativeHistoryClearanceText(narr) {
            let n: string = narr;
            if (n) {
                n = n.replace(/(\\n)/g, '<br>');
                n = n.replace(/(\\r)/g, '');
                // console.log("NARRATIVE AFTER CHANGE", n);
            }
            return n;
    }


    populateIntake(intakenumber) {
        this._commonHttpService
            .create(
                {
                    page: 1,
                    limit: 10,
                    where: {
                        status: 'intake',
                        intakenumber: intakenumber
                    }
                },
                NewUrlConfig.EndPoint.Intake.TemporarySavedIntakeUrl
            ).subscribe(data => {
                // this.intakeflag = true;
                const response = data;
                let addedPersons = [];
                const narrativeDetails = new Narrative();
                if (response.data[0]) {
                    this.intakeflag = false;
                    const intakeModel = response.data[0].jsondata;
                    const general = intakeModel.General;
                    this.countyid = general.countyid;
                    this.getCountyDetails();
                    addedPersons = intakeModel.persons
                        ? intakeModel.persons
                        : intakeModel.persondetails
                            ? intakeModel.persondetails.Person
                            : [];
                    this.reviewStatus = intakeModel.reviewstatus.status;
                    if (this.reviewStatus === 'Closed' ||
                        this.reviewStatus === 'Approved') {
                        narrativeDetails.Firstname = general.Firstname;
                        narrativeDetails.Middlename = general.Middlename;
                        narrativeDetails.Lastname = general.Lastname;
                        narrativeDetails.ZipCode = general.offenselocation;
                    } else {
                        if (intakeModel.narrative &&
                            intakeModel.narrative.length > 0) {
                            narrativeDetails.Firstname = intakeModel.narrative ? intakeModel.narrative[0].Firstname : '';
                            narrativeDetails.Middlename = intakeModel.narrative ? intakeModel.narrative[0].Middlename : '';
                            narrativeDetails.Lastname = intakeModel.narrative ? intakeModel.narrative[0].Lastname : '';
                            narrativeDetails.ZipCode = intakeModel.narrative ? intakeModel.narrative[0].ZipCode : '';
                            narrativeDetails.PhoneNumber = intakeModel.narrative ? intakeModel.narrative[0].PhoneNumber : '';
                            narrativeDetails.incidentlocation = intakeModel.narrative ? intakeModel.narrative[0].incidentlocation : '';
                            narrativeDetails.incidentdate = intakeModel.narrative ? intakeModel.narrative[0].incidentdate : '';
                            narrativeDetails.isapproximate = intakeModel.narrative ? intakeModel.narrative[0].isapproximate : '';
                            narrativeDetails.email = intakeModel.narrative ? intakeModel.narrative[0].email : '';
                        }
                    }
                    if (general.Narrative && general.Narrative != null && general.Narrative !== '') { narrativeDetails.Narrative = this.fixNarrativeHistoryClearanceText(general.Narrative); }
                    if (general.cpsHistoryClearance && general.cpsHistoryClearance !== null && general.cpsHistoryClearance !== '') {
                        narrativeDetails.cpsHistoryClearance = this.fixNarrativeHistoryClearanceText(general.cpsHistoryClearance);
                    }
                    narrativeDetails.IsAnonymousReporter = general.IsAnonymousReporter === true ? true : false;
                    narrativeDetails.IsUnknownReporter = general.IsUnknownReporter === true ? true : false;
                    narrativeDetails.RefuseToShareZip = general.RefuseToShareZip === true ? true : false;
                    narrativeDetails.requesteraddress1 = general.requesteraddress1;
                    narrativeDetails.requesteraddress2 = general.requesteraddress2;
                    narrativeDetails.requestercity = general.requestercity;
                    narrativeDetails.requesterstate = general.requesterstate;
                    narrativeDetails.requestercounty = general.requestercounty;
                    narrativeDetails.isacknowledgementletter = general.isacknowledgementletter ? true : false;
                    this.prepareCpsDocDetails(general.InputSource);
                    this._dataStoreService.setData(IntakeStoreConstants.general, general);
                    this._dataStoreService.setData(IntakeStoreConstants.intakeSDM, intakeModel.sdm);
                    this._dataStoreService.setData(IntakeStoreConstants.addedPersons, addedPersons);
                    this._dataStoreService.setData(IntakeStoreConstants.reviewstatus, this.reviewStatus);
                    this._dataStoreService.setData(IntakeStoreConstants.addNarrative, narrativeDetails);
                    this._dataStoreService.setData(IntakeStoreConstants.evalFields, intakeModel.evaluationFields);
                } else {
                    this.intakeflag = true;
                }
            });
    }

      sendEmail () {
        const caseID = this._dataStoreService.getData('da_intakenumber');
        if (this.emailForm.valid) {
        const request = {
            email : this.emailForm.getRawValue().email,
            caseNumber: caseID,
            body: document.getElementById('CPS-Intake-Report').innerHTML
        };
        this._commonHttpService.create(request, 'Intakeservicerequestpurposes/sendemailcontact').subscribe(
            (result) => {
                this._alertService.success('Email Sent successfully!');
            },
            (error) => {
               console.log(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
     } else {
            this._alertService.error('Please enter valid email address!');
        }
    }

      printCasePdf(element: string) {
        (<any>$('#intake-cps-doc1')).modal('hide');
        let printContents, popupWin;
        printContents = document.getElementById('CPS-Intake-Report').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>CPS Intake Report</title>
          <style>
          .section-header {
            height: 25px;
            border: 1px solid black;
            text-align: center;
            padding-bottom: 15px;
            background-color: #556080;
            font-weight: bold;
            margin-bottom: 5px;
        }
        .section-body table td{border: 1px solid #ccc; padding: 5px;}
        .section-lable {
            text-align: left;
            padding-right: 15px;
            font-weight: 600;
            margin-bottom: 5px;
        }.section-value {
            min-height: 22px;
            padding-left: 0px;
        }
        .sizeA4 {
                width: 21cm;
                height: 29.7cm;
            }.margin-wrapper {
                margin-left: 1.2cm;
                margin-right: 1.2cm;
            }
            .pdf-page .row {
                margin-left: 0px;
                margin-right: 0px;
            }
            .align-center {
                text-align: center
            }
            </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }

    ngAfterViewInit() {
        const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
        if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('i')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
            (<any>$('th a')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
        }
    }
    onChangeDispositon(event) {
        console.log(event);
        this.selectedDisposition = this.dispositionDropdownItems.find(item => item.value === event.value);
        console.log(this.selectedDisposition);
    }

    loadDispositon(statusId, event) {
        this.selectedStatus = this.statusDropdownItems.find(item => item.text === event.label);
        if (!this.isAS) {
            const statusKey = statusId.split('~')[0];
            this.dispositionDropdownItems$ = this._commonHttpService
                .getArrayList(
                    new PaginationRequest({
                        nolimit: true,
                        where: {
                            statuskey: statusKey, //
                            intakeservreqtypeid: this.daType,
                            servicerequestsubtypeid: this.daSubTypeId
                        },
                        method: 'get'
                    }),
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.DispositionUrl + '?filter'
                )
                .map((result) => {
                    return result.map(
                        (res) => {
                            return {
                                text: res.description,
                                value: res.servicerequesttypeconfigiddispostionid,
                                code: res.dispositioncode
                            };
                        }
                    ).filter(statusItem => {
                        console.log('status item', statusItem);
                        if (this.isCW && (this.daSubType === 'CPS-IR') ) {
                            if (this.roleId.role.name === AppConstants.ROLES.APPEAL_USER) {
                            if (statusItem.text === 'Screen Out') {
                                return true;
                            } else {
                                return false;
                            }
                            } else {
                                if (statusItem.text !== 'Close Case') {
                                    return true;
                                } else {
                                    return false;
                                }
                            }

                        } else if (this.isCW && (this.daSubType === 'CPS-AR' && !this.ARSummaryApproved) ) {
                            if (this.roleId.role.name === AppConstants.ROLES.APPEAL_USER) {
                            if (statusItem.text === 'Screen Out') {
                                return true;
                            } else {
                                return false;
                            }

                            } else {
                                if (statusItem.text !== 'Close Case') {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        } else {
                            return true;
                        }
                    });
                });
        } else {
            const statusKey = statusId.split('~')[0];
            if (statusKey === 'Closed') {
                this.getProgramCode();
                this.showProgramCode = true;
            } else {
                this.showProgramCode = false;
            }
            this.dispositionDropdownItems$ = this._commonHttpService
                .getArrayList(
                    new PaginationRequest({
                        nolimit: true,
                        where: {
                            statuskey: statusKey, //
                            intakeservreqtypeid: this.daType,
                            servicerequestsubtypeid: this.daSubTypeId
                        },
                        method: 'get'
                    }),
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.DispositionUrl + '?filter'
                )
                .map((result) => {
                    if (result && result.length) {
                        result.map((item) => {
                            this.disporitioncode[item.servicerequesttypeconfigid] = item.dispositioncode;
                        });
                    }
                    return result.map(
                        (res) =>
                            new DropdownModel({
                                text: res.description,
                                value: res.servicerequesttypeconfigid + '~' + res.servicerequesttypeconfigiddispostionid
                            })
                    );
                });
        }
        this.dispositionDropdownItems$.subscribe(data => { this.dispositionDropdownItems = data; });
    }

    showReason(value) {
        if (value === 'false') {
            this.showFamilySupport = true;
            this.getFormalSupport();
        } else {
            this.showFamilySupport = false;
        }

    }

    getFormalSupport() {
        this.formalSupport$ = this._commonHttpService
        .getArrayList(
            new PaginationRequest({
                nolimit: true,
                where: {
                    tablename: 'DispostionInformalSupport',
                    teamtypekey: 'AS'
                },
                method: 'get'
            }),
            'referencetype/gettypes?filter'
        )
        .map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.description,
                        value: res.ref_key
                    })
            );
        });
    }
    openConfirmationPopup() {
        (<any>$('#closer-checklist')).modal('hide');
        (<any>$('#confirmation-popup')).modal('show');
    }

    confirmDisposition() {
        const disposition = this.dispositionDropdownItems.find(item => item.value === this.dispositionFormGroup.value.dispositionid);

        if (this.isCW && (this.isServiceCase || this.isAdoptionCase)) {
            if (disposition.text === 'Close Case') {
                (<any>$('#status-disposition')).modal('hide');
                (<any>$('#closer-checklist')).modal('show');
                // @Simar - I think this is not needed here as the validations are already done before to set the flag
                // this.allChecked = false;
            } else {
                this.saveDisposition();
            }
        } else {
            if (disposition.text === 'Close Case') {
                if (!this.adultManditory) {
                    (<any>$('#status-disposition')).modal('hide');
                    (<any>$('#confirm-disposition')).modal('show');
                }
                if (this.adultManditory) {
                    this._commonHttpService.getSingle({
                        where: {
                            'intakeserviceid': this.id
                        },
                        method: 'get'
                    }, 'intakeservicerequestdispositioncodes/taskcloseddisposition?filter').subscribe((result) => {
                        if (result) {
                            // tslint:disable-next-line:max-line-length
                            this.dispositionStatus = result.filter((data) => data.activitytaskname === 'Client Assessment Form (716 A)' || data.activitytaskname === 'Investigation Outcomes Form (716 B)');
                            result.map((list) => {
                                if (list.activitytaskname === 'Client Assessment Form (716 A)' || list.activitytaskname === 'Investigation Outcomes Form (716 B)') {
                                    if (list.status === 'Open') {
                                        this.enableAdultDisposition = true;
                                    }
                                }
                            });
                            (<any>$('#status-disposition')).modal('hide');
                            (<any>$('#confirm-disposition-AS')).modal('show');
                        }
                    });
                }
            } else {
                this.saveDisposition();
            }
        }




    }
    cancelConfirmDisposition() {
        (<any>$('#status-disposition')).modal('show');
    }
    saveDisposition() {
        const dispositionModal = new DispositionAddModal();
        dispositionModal.disposition = Object.assign({
            intakeserviceid: this.id,
            servicecaseid: this.id,
            intakeserreqstatustypeid: this.dispositionFormGroup.value.statusid.split('~')[1],
            dispostionid: this.isAS ? this.dispositionFormGroup.value.dispositionid.split('~')[1] : this.dispositionFormGroup.value.dispositionid,
            reviewcomments: this.dispositionFormGroup.value.reviewcomments,
            closingcodetypekey: this.dispositionFormGroup.value.closingcodetypekey,
            programcode: this.showProgramCode ? this.programCodeSelection : [],
            issupport: this.showProgramCode ? this.dispositionFormGroup.value.issupport : null,
            supporttypekey: this.showFamilySupport ? this.dispositionFormGroup.value.supporttypekey : null,
            dateseen: new Date()
        });
        dispositionModal.investigation = {
            summary: '',
            filelocdesc: null,
            appevent: 'INVR'
        };
        if (this.dsdsActionSummary.da_subtype === 'IHAS' || this.dsdsActionSummary.da_subtype === 'SSTA') {
            dispositionModal.disposition.intakeservtypekey = this.dsdsActionSummary.da_subtype;
            dispositionModal.disposition.reqforservdispostion = this.disporitioncode[this.dispositionFormGroup.value.dispositionid.split('~')[0]];
            dispositionModal.disposition.reqforservstatus = this.dispositionFormGroup.value.statusid.split('~')[0];
            dispositionModal.disposition.programtype = this.dispositionFormGroup.value.programtype;
            dispositionModal.disposition.receiveddate = new Date();
        }
        this._dispositionAddService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.DispositionAddUrl;
        if (this.isCW && this.isServiceCase) {
            delete dispositionModal.disposition.intakeserviceid;
            delete dispositionModal.disposition.intakeserreqstatustypeid;
            delete dispositionModal.disposition.dispostionid;
            dispositionModal.disposition.servicecaseid = this.id;
            dispositionModal.disposition.intakeserreqstatustypekey = this.selectedDisposition.code;
            dispositionModal.disposition.dispositioncode = this.selectedStatus.code;
            dispositionModal.investigation.appevent = 'SCDR';
            this._dispositionAddService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.ServiceCaseDispositionAddUrl;
        } else if (this.isCW && this.isAdoptionCase) {
            delete dispositionModal.disposition.intakeserviceid;
            delete dispositionModal.disposition.intakeserreqstatustypeid;
            delete dispositionModal.disposition.dispostionid;
            delete dispositionModal.disposition.servicecaseid;
            delete dispositionModal.disposition.reviewcomments;
            dispositionModal.disposition.comments = this.dispositionFormGroup.value.reviewcomments,
            dispositionModal.disposition.assignsecurityuserid = this.dispositionFormGroup.value.supervisorid;
            dispositionModal.disposition.adoptioncaseid = this.id;
            dispositionModal.disposition.intakeserreqstatustypekey = this.selectedDisposition.code;
            dispositionModal.disposition.dispositioncode = this.selectedStatus.code;
            dispositionModal.investigation.appevent = 'ACDR';
            this._dispositionAddService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.AdoptionCaseDispositionAddUrl;
        }
        this._dispositionAddService.create(dispositionModal).subscribe(
            (response) => {
                (<any>$('#confirmation-popup')).modal('hide');
                this.dispositionFormGroup.reset();
                // this.dispositionHistory();
                (<any>$('#status-disposition')).modal('hide');
                (<any>$('#closer-checklist')).modal('hide');
                if (this.roleId.role.name === AppConstants.ROLES.APPEAL_USER) {
                this.routingUpdate(null);
                }
                this._alertService.success('Disposition updated successfully');
                this.dispositionHistory();
                // Observable.timer(2000).subscribe(() => {
                //     this._router.routeReuseStrategy.shouldReuseRoute = function () {
                //         return false;
                //     };
                //     const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition';
                //     this._router.navigateByUrl(currentUrl).then(() => {
                //         this._router.navigated = false;
                //         this._router.navigate([currentUrl]);
                //     });
                // });
            },
            (error) => {
                if (this.isCW && this.isServiceCase && this.allChecked) {
                    this.allChecked = false;
                    this.openConfirmationPopup();
                    this._alertService.success('Disposition updated successfully');
                } else {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            }
        );
    }
    routingUpdate(daHistModal) {
        if (this.isCW && this.roleId.role.name !== AppConstants.ROLES.APPEAL_USER) {
            if (this.approvalStatusForm.controls['routingstatus'].value !== 'Rejected' && this.approvalStatusForm.controls['routingstatus'].value !== 'Approved') {
                this._alertService.error('Please select the approval status');
                return;
            }
        }
        let routingObject = {};
        if (daHistModal) {
            routingObject = {
                objectid: daHistModal.intakeservicerequestdispositioncodeid,
                intakeserviceid: this.id,
                eventcode: 'INDR',
                status: this.approvalStatusForm.controls['routingstatus'].value,
                comments: this.approvalStatusForm.controls['comments'].value,
                notifymsg: 'Disposition Approved',
                routeddescription: 'Disposition Approved'
            };
            if (this.isCW && this.isAdoptionCase) {
                const status = this.approvalStatusForm.controls['routingstatus'].value;
                routingObject = {
                    'objectid': daHistModal.adoptioncasedispositionid,
                    'eventcode': 'ACDR',
                    'status': status,
                    'comments': (status === 'Approved') ? 'Disposition Approved' : this.approvalStatusForm.controls['comments'].value,
                    'notifymsg': (status === 'Approved') ? 'Disposition Approved' : 'Disposition Rejected',
                    'routeddescription': (status === 'Approved') ? 'Disposition Approved' : 'Disposition Rejected',
                };
            } else if (this.isCW && this.isServiceCase) {
                const status = this.approvalStatusForm.controls['routingstatus'].value;
                routingObject = {
                    'objectid': daHistModal.servicecasedispositionid,
                    'eventcode': 'SCDR',
                    'status': status,
                    'comments': (status === 'Approved') ? 'Disposition Approved' : this.approvalStatusForm.controls['comments'].value,
                    'notifymsg': (status === 'Approved') ? 'Disposition Approved' : 'Disposition Rejected',
                    'routeddescription': (status === 'Approved') ? 'Disposition Approved' : 'Disposition Rejected',
                };
            }
        }
        if (this.isCW && this.roleId.role.name === AppConstants.ROLES.APPEAL_USER) {
            routingObject = {
                objectid: this.id,
                intakeserviceid: this.id,
                eventcode: 'APPL',
                status: 'Approved',
                comments: '',
                notifymsg: 'Appeal Approved',
                routeddescription: 'Appeal Approved'
            };
        }
        this._commonHttpService.create(routingObject, 'routing/routingupdate').subscribe((res) => {
            this._alertService.success('saved successfully!');
            this.dispositionHistory();
        }, (error) => {
            console.log(error);
        });
    }
    previewCpsDoc() {
          (<any>$('#cps-doc')).modal('show');
    }
    private initializeDispositionForm() {
        this.dispositionFormGroup = this.formBuilder.group({
            statusid: [''],
            dispositionid: ['', Validators.required],
            closingcodetypekey: [null],
            reviewcomments: [''],
            programCode: [''],
            issupport: [null],
            supporttypekey: [null],
            programtype: [''],
            supervisorid: [null]
        });
    }
    private dispositionHistory() {
        if (this.isCW && this.isAdoptionCase) {
            this.getHistory$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: this.paginationInfo.pageNumber,
                    limit: this.paginationInfo.pageSize,
                    where: {
                        adoptioncaseid: this.id
                    },
                    method: 'get'
                }),
                'adoptioncasedisposition/getadoptioncasedisposition?filter'
            );
        } else if (this.isCW && this.isServiceCase) {
            this.getHistory$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: this.paginationInfo.pageNumber,
                    limit: this.paginationInfo.pageSize,
                    where: {
                        servicecaseid: this.id
                    },
                    method: 'get'
                }),
                'servicecasedisposition/getservicecasedisposition?filter'
            );
        } else {

            this.getHistory$ = this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        page: this.paginationInfo.pageNumber,
                        limit: this.paginationInfo.pageSize,
                        where: {
                            servicerequestid: this.id
                        },
                        method: 'get'
                    }),
                    'Intakeservicerequestdispositioncodes/GetHistory?filter'
                )
                .map(
                    (result) => {
                        return result.data;
                    },
                    (error) => {
                        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                    }
                );
        }
    }
    private loadStatuses() {
        this.statusDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        intakeservreqtypeid: this.daType,
                        servicerequestsubtypeid: this.daSubTypeId
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.StatusUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) => {
                        return {
                            text: res.description,
                            value: res.intakeserreqstatustypekey + '~' + res.intakeserreqstatustypeid,
                            code: res.intakeserreqstatustypekey
                        };
                    }
                ).filter(statusItem => {
                    console.log('status item', statusItem);
                    if (this.isCW && (this.daSubType === 'CPS-IR') ) {
                        if (this.roleId.role.name === AppConstants.ROLES.APPEAL_USER) {
                            if (statusItem.text === 'Closed') {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            if (statusItem.text !== 'Completed' && statusItem.text !== 'Closed') {
                                return true;
                            } else {
                                return false;
                            }
                        }

                    } else if (this.isCW && (this.daSubType === 'CPS-AR' && !this.ARSummaryApproved) ) {
                        if (this.roleId.role.name === AppConstants.ROLES.APPEAL_USER) {
                            if (statusItem.text === 'Closed') {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            if (statusItem.text !== 'Completed' && statusItem.text !== 'Closed') {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return true;
                    }
                });
            });
        this.statusDropdownItems$.subscribe(data => { this.statusDropdownItems = data; });
    }
    private programType() {
        this.programTypeDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        tablename: 'programtype',
                        teamtypekey: 'AS'
                    },
                    method: 'get'
                }),
                'referencetype/gettypes?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }
    private loadCloseCase() {
        this.closeCaseDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get'
                }),
                'closingcodetype?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.closingcodetypekey
                        })
                );
            });
    }
    getProgramCode() {
        this.programCodeItems$ = this._commonHttpService
        .getArrayList(
            new PaginationRequest({
                nolimit: true,
                method: 'get',
                where: { 'tablename' : 'DispostionProgramCode', 'teamtypekey' : 'AS' }
            }),
            'referencetype/gettypes?filter'
        )
        .map((result) => {
            return result.map(
                (res) =>
                    new DropdownModel({
                        text: res.description,
                        value: res.ref_key
                    })
            );
        });
    }
    /* Two functions found
    loadAdultCloseCase(event) {
        const statusID = this.dispositionFormGroup.get('statusid').value.split('~')[1];
        this.closeCaseDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get',
                    count: -1,
                    where: { 'intakeserreqstatustypeid': statusID, 'servicerequesttypeconfigid': event.split('~')[0] }
                }),
                'daconfig/servicerequesttypeconfigdispositioncode/getclosingcodelist?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.closingcodetypekey
                        })
                );
            });
    } */

    programCode(event) {
        this.programCodeItems$.subscribe(data => {
            if (data) {
                const removalReasonItems = data.filter(item => {
                    if (event.includes(item.value)) {
                        return item;
                    }
                });
                this.programCodeDescription = removalReasonItems.map(
                    res => res.text
                );
            }
        });

        this.programCodeSelection = event.map(sel => {
            return {
                programcodetypekey: sel
            };
        });
    }

    clearDisposition() {
        this.dispositionFormGroup.reset();
        this.dispositionDropdownItems$ = Observable.empty();
        this.isProgramingType = false;
        this.initializeDispositionForm();
        this.showProgramCode = false;
        this.showFamilySupport = false;
        this.programCodeDescription = [];
    }

    loadAdultCloseCase(event, labelname) {
        if (labelname === 'Waiting List') {
            this.isProgramingType = true;
            this.dispositionFormGroup.controls['programtype'].setValidators([Validators.required]);
            this.dispositionFormGroup.controls['programtype'].updateValueAndValidity();
            this.dispositionFormGroup.controls['closingcodetypekey'].clearValidators();
            this.dispositionFormGroup.controls['closingcodetypekey'].updateValueAndValidity();
        } else {
            this.isProgramingType = false;
            this.dispositionFormGroup.controls['programtype'].clearValidators();
            this.dispositionFormGroup.controls['programtype'].updateValueAndValidity();
            this.dispositionFormGroup.controls['closingcodetypekey'].setValidators([Validators.required]);
            this.dispositionFormGroup.controls['closingcodetypekey'].updateValueAndValidity();
        }
        if (event) {
            const statusID = this.dispositionFormGroup.get('statusid').value.split('~')[1];
            this.closeCaseDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get',
                    count: -1,
                    where: { 'intakeserreqstatustypeid' : statusID, 'servicerequesttypeconfigid' : event.split('~')[0] }
                }),
                'daconfig/servicerequesttypeconfigdispositioncode/getclosingcodelist?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.closingcodetypekey
                        })
                );
            });
        }
    }


    async downloadCasePdf(element: string) {
        const source = document.getElementById(element);
        const pages = source.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {

            await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
            });
        }
        const pageName = 'pageName';
        this.pdfFiles.push({ fileName: pageName, images: pageImages });
        pageImages = [];
        this.convertImageToPdf();

    }
    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            const doc = new jsPDF();
            pdfFile.images.forEach((image, index) => {
                doc.addImage(image, 'JPEG', 0, 0);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#cps-doc')).modal('hide');
        this.pdfFiles = [];
        // this.downloadInProgress = false;
    }

    doPerformApproval(d: any) {

        if (this.isSupervisor) {
            for (let i = 0; i < d.length - 1; i++) {
                if (d[i].routingstatus === 'Review') {
                    return d[i];
                }
            }
            return null;
        }
    }

    updateChecked() {
        const unChecked = this.checklistItems.filter(item => (!item.isChecked && item.show));
        this.allChecked = !unChecked.length;
    }

    loadCheckListItems() {
        this.checklistItems =

            [
                {
                    id: 15,
                    name: 'No clients have an open program assignment',
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 7,
                    name: 'All Placements have been end-dated',
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 8,
                    name: 'All Homes Removals have been end-dated',
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 9,
                    name: 'All Service Log Records (both agency and Referred) have been end-dated',
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 10,
                    name: 'All Adoption/Guardianship Subsidy payments have an end-date that does not exceed the date of case closing',
                    isChecked: false,
                    autofilled: false,
                    show: true
                },
                {
                    id: 17,
                    name: 'All Adoption Subsidy payments have an end-date that does not exceed the date of case closing',
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 11,
                    name: 'All Service Log payments have been completed',
                    isChecked: false,
                    autofilled: false,
                    show: true
                },
                {
                    id: 12,
                    name: 'All Placement Validations have been completed',
                    isChecked: true,
                    autofilled: false,
                    show: true
                },
                {
                    id: 16,
                    name: 'All Provider payments have been completed',
                    isChecked: false,
                    autofilled: false,
                    show: true
                },
                {
                    id: 13,
                    name: 'All IV-E eligibility determinations have been completed',
                    isChecked: false,
                    autofilled: false,
                    show: true
                },
                {
                    id: 14,
                    name: 'All Placement changes have Supervisory Approval',
                    isChecked: true,
                    autofilled: true,
                    show: true
                },
                {
                    id: 1,
                    name: 'All open living arrangements have been end-dated for all clients. ',
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 2,
                    name: `No clients have an open legal custody record of committed or Co-Committed to DSS, Guardianship to DSS,
         Delinquency, Voluntary Placement Agreement, Shelter Care or Committed to Another State.`,
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 3,
                    name: 'A SAFE-C or SAFE-C OHP has been completed for all applicable children and the assessment has supervisory approval',
                    isChecked: true,
                    autofilled: true,
                    show: true
                },
                {
                    id: 5,
                    name: 'There are no incomplete MFRA’s or MFRA’s pending supervisory approval',
                    isChecked: true,
                    autofilled: true,
                    show: true
                },
                {
                    id: 4,
                    name: 'All Safety Plans have Supervisory Approval(If applicable)',
                    isChecked: false,
                    autofilled: true,
                    show: true
                },
                {
                    id: 6,
                    name: 'There are no incomplete INFS Progress Reviews or INFS Progress Reviews pending supervisory approval',
                    isChecked: false,
                    autofilled: true,
                    show: true
                }
            ];
        let programarea = this._dataStoreService.getData('programarea');
        programarea = Array.isArray(programarea) ? programarea : [];
        programarea = programarea.map(item => item.programkey);
        const oohspecific = [1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14];
        const ihspecific = [1, 2, 3, 4, 5];
        const adoptioncase = [15, 9, 17, 16, 13];
        this.checklistItems.forEach(item => {
            if (this.isAdoptionCase) {
                item.show = (adoptioncase.includes(item.id)) ? true : false;
            } else {
                if (programarea.includes('OOH')) {
                    item.show = (oohspecific.includes(item.id)) ? true : false;
                } else {
                    item.show = (ihspecific.includes(item.id)) ? true : false;
                }
            }
        });
    }

    checkForARSummaryApproved() {
        this._commonHttpService.getSingle({
            where: {
                intakeserviceid: this.id
            },
            method: 'get',
            page: 1,
            limit: 10
            // caseclosuresummary/getcaseclosuresummarylist?data
        }, 'caseclosureparticipant/getcaseclosurelist?filter').subscribe((item) => {
            console.log('Get data1' + JSON.stringify(item));
        if (item) {
         // this.ARCaseSummaryForm.patchValue(item[0]);
            const summaryData =  item && item.length && Array.isArray(item) ? item[0] : null;
            if (summaryData) {
                if (summaryData.routingstatustypeid === 16) {
                    this.ARSummaryApproved = true;
                }
            }
        }});
    }

    loadSupervisor() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'CWIF' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorsList = result.data;
                this.supervisorsList = this.supervisorsList.filter(
                    users => users.rolecode === 'SP'
                );
                if (this.supervisorsList && this.supervisorsList.length) {
                    const reviewer = this.dsdsActionsSummary.da_assignedby;
                    const selectedSupervisor = this.supervisorsList.find(user => user.username === reviewer);
                    if (selectedSupervisor) {
                        this.dispositionFormGroup.patchValue({supervisorid: selectedSupervisor.userid });
                    }
                }
            });
    }

    changeSupervisor($event) {
        console.log($event);
        if ($event && $event.value) {
            this._commonHttpService.create({
                appeventcode: this.isServiceCase === 'true' ? 'SRVC' : 'INVT',
                objectid: this.id,
                fromuserid: $event.value
            }, 'routing/changereviewer')
                .subscribe(result => {
                    console.log('supervisor changed', result);
                });

        }
    }
    getAssessments() {
        let inputRequest: Object;
        if (this.isServiceCase) {
            inputRequest = {
                objecttypekey: 'servicecase',
                objectid: this.id
                // objectid: '1ab13583-6d94-4929-92e0-09723df839f3'
            };
        } else {
            inputRequest = {
                servicerequestid: this.id,
                categoryid: null,
                subcategoryid: null,
                targetid: null,
                assessmentstatus: null
            };
        }
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: this.paginationInfo.pageSize25,
                    where: inputRequest,
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.ListAssessment + '?filter'
            )
            .subscribe(result => {
                const list = result.data;
                const safec = list.find(item => item.description.toUpperCase() === 'SAFE-C');
                let safecList = safec && Array.isArray(safec.intakassessment) ? safec.intakassessment : [];
                safecList = safecList.filter(item => item.assessmentstatustypekey === 'Accepted');
                const safecohp = list.find(item => item.description.toUpperCase() === 'SAFE-C OHP');
                let safecohpList = safecohp && Array.isArray(safecohp.intakassessment) ? safecohp.intakassessment : [];
                safecohpList = safecohpList.filter(item => item.assessmentstatustypekey === 'Accepted');
                let isSafeAssDone = false;
                if (safecohpList.length > 0 || safecList.length > 0) {
                    isSafeAssDone = true;
                }
                const ismifradone = this.assessmentCompletion(list, 'MFIRA');

                this.checklistItems.forEach(item => {
                    if (item.id === 3 || item.id === 4) {
                        item.isChecked = isSafeAssDone;
                    }
                    if (item.id === 5) {
                        item.isChecked = ismifradone;
                    }
                });
                this.updateChecked();

            });
    }

    assessmentCompletion(list, assessmentname) {
        const assessment = list.find(item => item.description.toUpperCase() === assessmentname);
        let submissionlist = assessment && Array.isArray(assessment.intakassessment) ? assessment.intakassessment : [];
        submissionlist = submissionlist.filter(item => item.assessmentstatustypekey === 'Accepted');
        return (submissionlist.length > 0);
    }

    getPlacementInfoList() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 10,
                    method: 'get',
                    where: { servicecaseid: this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID) },
                }),
                'placement/getplacementbyservicecase?filter'
                // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
            ).subscribe(result => {
                const list = Array.isArray(result.data) ? result.data : [];
                if (list.length) {
                    let isLAPending = false;
                    let isPRPLpending = false;
                    let islaapprovalpending = false;
                    let isprplapprovalpending = false;
                    list.forEach(item => {
                        const placements = Array.isArray(item.placements) ? item.placements : [];
                        const ispresent = placements.some(ele => ele.livingenddate === null && ele.placementtypekey === 'LA');
                        // const isPRPLpresent = placements.some(ele => ele.revisionupdate.enddate === null && ele.placementtypekey === 'PRPL');
                        const isPRPLpresent = placements.some(ele => ele.enddate === null && ele.placementtypekey === 'PRPL');
                        const isLAapproved = placements.some(ele => ele.livingenddate !== null && ele.placementtypekey === 'LA' && ele.routingstatus !== 'Approved');
                        const isprplapproved = placements.some(ele => ele.placementtypekey === 'PRPL' && ele.routingstatus !== 'Approved');
                        if (ispresent) {
                            isLAPending = true;
                        }
                        if (isPRPLpresent) {
                            isPRPLpending = true;
                        }
                        if (isLAapproved) {
                            islaapprovalpending = true;
                        }
                        if (isprplapproved) {
                            isprplapprovalpending = true;
                        }
                    });
                    this.checklistItems.forEach(item => {
                        if (item.id === 1) {
                            item.isChecked = (isLAPending) ? false : true;
                        }
                        if (item.id === 7) {
                            item.isChecked = (isPRPLpending) ? false : true;
                        }
                        if (item.id === 14) {
                            item.isChecked = (isprplapprovalpending) ? false : true;
                        }
                    });
                } else {
                    this.checklistItems.forEach(item => {
                        if (item.id === 1) {
                            item.isChecked = true;
                        }
                        if (item.id === 7) {
                            item.isChecked = true;
                        }
                        if (item.id === 14) {
                            item.isChecked = true;
                        }
                    });
                }
                this.updateChecked();
            });
    }

    getChildRemoval(groupByPerson: number = 0) {
        const requestData = { ...this.getRequestParam(), ...{ isgroup: groupByPerson } };
        this._commonHttpService
            .getSingle(
                {
                    where: requestData,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .GetChildRemovalList + '?filter'
            ).subscribe(result => {
                const list = Array.isArray(result) ? result : [];
                if (list.length) {
                    // const notapproved = list.some(item => item.approvalstatus !== 'Approved');
                    const noexitdate = list.some(item => item.exitdate === null);
                    const isvalid = (noexitdate) ? false : true;
                    this.checklistItems.forEach(item => {
                        if (item.id === 8) {
                            item.isChecked = isvalid;
                        }
                    });
                } else {
                    this.checklistItems.forEach(item => {
                        if (item.id === 8) {
                            item.isChecked = true;
                        }
                    });
                }
                this.updateChecked();
            });
    }

    getRequestParam() {
        const intakeserviceid = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        const isServiceCase = this._dataStoreService.getData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        let requestData;
        if (isServiceCase) {
            requestData = {
                objectid: intakeserviceid,
                objecttypekey: 'servicecase'
            };

        } else {
            requestData = { intakeserviceid: intakeserviceid };
        }
        return requestData;
    }
    getInvolvedPerson() {
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 10,
                    method: 'get',
                    where: this.getRequestParam()
                }),
                'People/getpersondetailcw?filter'
            ).subscribe(result => {
                const persons = result.data;
                const personList = [];
                const personlistforservicelog = [];
                const childVictimList = [];
                const validcustodylist = ['DSSDDA', 'DSSDHMH', 'DSSDJS', 'DSSDJSDHMH', 'DSS', 'GUARDDSS', 'DJJ', 'SHLTRDSS', 'OTHRAGENCY'];
                if (persons && persons.length) {
                    // const avoidRoles = ['AV', 'AM', 'CHILD', 'RC', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC'];
                    persons.map(item => {
                        personlistforservicelog.push(item.cjamspid);
                        if (!item.rolename || item.rolename === '') {

                            if (item.roles && item.roles.length && item.roles[0].intakeservicerequestpersontypekey) {
                                item.rolename = item.roles[0].intakeservicerequestpersontypekey;
                            }
                        }
                        if (item.rolename === 'AV' || item.rolename === 'CHILD') {
                            if (!item.intakeservicerequestactorid) {
                                if (item.roles && item.roles.length && item.roles[0].intakeservicerequestactorid) {
                                    item.intakeservicerequestactorid = item.roles[0].intakeservicerequestactorid;
                                }
                            }
                            personList.push(item.intakeservicerequestactorid);
                        }
                        const roles = Array.isArray(item.roles) ? item.roles : [];
                        const isvictim = roles.some(ele => ['AV', 'CHILD'].includes(ele.intakeservicerequestpersontypekey));
                        if (isvictim) {
                            childVictimList.push(item.personid);
                        }
                    });
                    this.vendorlist = [];
                    this.agencylist = [];
                    const slsource = this.getServiceLogList(personlistforservicelog);
                    const vssource = this.getVendorList(personlistforservicelog);
                    vssource.subscribe(data => {
                        this.vendorlist = data['servicelogData'];
                        this.checkallservicelog();
                    });
                    slsource.subscribe(data => {
                        const list = Array.isArray(data['servicelogData']) ? data['servicelogData'] : [];
                        this.agencylist = list;
                        this.checkallservicelog();
                     });
                    const source = this.getLegalCustody(personList);
                    source.subscribe(res => {
                        if (res && res.length) {
                            const custodyDetails = Array.isArray(res[0].getlegalcustodymultiplepersons) ? res[0].getlegalcustodymultiplepersons : [];
                            if (custodyDetails.length) {
                                const isvalid = custodyDetails.some(ele => (validcustodylist.includes(ele.legalcustodytypekey)) && ele.todate === null);
                                this.checklistItems.forEach(item => {
                                    if (item.id === 2) {
                                        item.isChecked = !isvalid;
                                    }
                                });
                            } else {
                                this.checklistItems.forEach(item => {
                                    if (item.id === 2) {
                                        item.isChecked = true;
                                    }
                                });
                            }
                            this.updateChecked();
                        }
                    });

                    const pasource = this.getProgramAssignmentList(childVictimList);
                    pasource.subscribe((res) => {
                        if (res && Array.isArray(res) && res.length) {
                            // this.programAsssignList = result[0];
                            // this.programPersonDetails = result[0].persondetails;
                            const isexist = res.filter(item => {
                                const area = Array.isArray(item.personprogramarea) ? item.personprogramarea : [];
                                const valid = area.some(ele => ele.enddate === null);
                                return (valid) ? true : false;
                            });
                            this.checklistItems.forEach(item => {
                                if (item.id === 15) {
                                    item.isChecked = (isexist.length) ? false : true;
                                }
                            });
                            this.updateChecked();
                        }
                    });
                }
            });
    }

    checkallservicelog() {
        if (this.vendorlist && this.agencylist) {
            let agencylistcompleted = false;
            let vendorlistcompleted = false;
            if (this.agencylist.length) {
                const valid = this.agencylist.some(item => item.actual_end_date === null);
                agencylistcompleted = !valid;
            } else {
                agencylistcompleted = true;
            }

            if (this.vendorlist.length) {
                const valid = this.vendorlist.some(item => item.actual_end_date === null);
                vendorlistcompleted = !valid;
            } else {
                vendorlistcompleted = true;
            }

            this.checklistItems.forEach(item => {
                if (item.id === 9) {
                    item.isChecked = (vendorlistcompleted && agencylistcompleted);
                }
            });
            this.updateChecked();
        }
    }

    getLegalCustody(childActorIds) {
        return this._commonHttpService.getArrayList({
          method: 'get',
          where: {
            personid: childActorIds
          }
        }, 'legalcustody/getlegalcustodymultiple?filter');
      }

      getServiceLogList(cids) {
        return this._commonHttpService.getArrayList(
          {
            where: { daNumber: this.daNumber, client_id: cids },
            method: 'get',
            nolimit: true
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.servicelogmultipelist + '?filter'
        );
      }

    getAssignmentsList() {
        this._commonHttpService.getArrayList(
            {
                where: { adoptioncaseid:  this.id },
                method: 'get'
            },
            'Caseassignments/getassignmentstatus?filter'
        ).subscribe(data => {
            const list = Array.isArray(data) ? data : [];
            const valid = list.some(item => item.endate === null);
            this.checklistItems.forEach(item => {
                if (item.id === 15) {
                    item.isChecked = (valid) ? false : true;
                }
            });
        });
    }

    getAgreementListing() {
        this._commonHttpService
            .getSingle(
                new PaginationRequest({
                    where: { adoptioncaseid: this.id },
                    method: 'get',
                    page: 1,
                    limit: 10
                }),
                'adoptionagreement/adoptioncaseagreementlist?filter'
            )
            .subscribe(res => {
                if (res && res.length && Array.isArray(res)) {
                    const obcj = res[0];
                    const agreementList = obcj.getadoptioncaseagreementlist;
                    if (agreementList && agreementList.length) {
                        const length = agreementList.length - 1;
                        const agreement = agreementList[length];
                        if (agreement.agreementrate) {
                            const agreementRate = Array.isArray(agreement.agreementrate) ? agreement.agreementrate : [];
                            // const valid = agreementRate.some(item => {
                                const curr = new Date();
                                const edate = new Date(agreement.enddate);
                                let valid = false;
                                if (agreement.enddate === null || curr < edate) { valid = true; }
                            // });
                            // console.log('validity', valid);
                            this.checklistItems.forEach(item => {
                                if (item.id === 17) {
                                    item.isChecked = (valid) ? false : true;
                                }
                            });
                        }
                    }
                }
            });
    }
    getProgramAssignmentList(personlist) {
        return this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { objectid: this.id, personid: personlist },
                    method: 'get',
                    nolimit: true
                }),
                'Personprogramareas/getmultiplepersonprogramarea?filter'
            );
            // .subscribe((result) => {
                // if (result && Array.isArray(result) && result.length) {
                    // this.programAsssignList = result[0];
                    // this.programPersonDetails = result[0].persondetails;
                // }
            // });
    }

    getVendorList(personlist) {

        return this._commonHttpService.getArrayList(
            {
                where: { daNumber: this.daNumber, clientid: personlist },
                method: 'get'
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.multivendorServiceLog + '?filter'
        );
            // .map(res => {
            //     this.referredServiceList = res['servicelogData'];
            //     this.referredServiceCheck = this.referredServiceList.find(item => item.service_log_id === this.purchaseAuthorizationServiceLog);
            //     if (this.referredServiceCheck && this.purchaseAuthorizationServiceLog) {
            //         this.openpurchaseautherization(this.referredServiceCheck);
            //     }
            //     this.listCount = (res && res['servicelogData'] && res['servicelogData'].length) ? res['servicelogData'][0].totalcount : 0;
            //     return res['servicelogData'];
            // });
    }
}
