import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DispositionComponent } from './disposition.component';
const routes: Routes = [
    {
    path: '',
    component: DispositionComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DispositionRoutingModule { }
