//import { ReportSummary } from './../../../providers/_entities/provider.data.model';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DispositionComponent } from './disposition.component';
import { DispositionRoutingModule } from './disposition-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { QuillModule } from 'ngx-quill';
import { PaginationModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {InvestigationReportComponent} from '../disposition/investigation-report/investigation-report.component';
// import {CpsDocLetterComponent} from './cps-doc-letter/cps-doc-letter.component';
import { SharedComponentsModule } from '../../../../shared/shared-components/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    DispositionRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    QuillModule,
    PaginationModule,
    NgSelectModule,
    SharedComponentsModule
  ],
  declarations: [
    DispositionComponent,
    InvestigationReportComponent
    // CpsDocLetterComponent
  ],
  providers: []
})
export class DispositionModule { }
