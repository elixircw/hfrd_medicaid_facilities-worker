import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsServicePlanComponent } from './as-service-plan.component';
import { AsGoalStrategyComponent } from './as-goal-strategy/as-goal-strategy.component';
import { AsServicePlanActivityComponent } from './as-service-plan-activity/as-service-plan-activity.component';
import { ServicePlanUnmetComponent } from './service-plan-unmet/service-plan-unmet.component';

const routes: Routes = [
  {
      path: '',
      component: AsServicePlanComponent,
      children: [
          {
              path: 'goal-strategy',
              component: AsGoalStrategyComponent
          },
          {
              path: 'service-plan-activity',
              component: AsServicePlanActivityComponent
          },
         // { path: 'service-log-activity', loadChildren: './as-service-log-activity/service-log-activity.module#AsServiceLogActivityModule' },
          {
              path: '',
              redirectTo: 'goal-strategy',
              pathMatch: 'full'
          },
          {
              path: 'service-plan-unmet',
              component: ServicePlanUnmetComponent
          }
      ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsServicePlanRoutingModule { }
