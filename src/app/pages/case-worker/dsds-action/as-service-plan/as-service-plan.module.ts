import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsServicePlanRoutingModule } from './as-service-plan-routing.module';
import { AsServicePlanComponent } from './as-service-plan.component';
import { AsGoalStrategyComponent } from './as-goal-strategy/as-goal-strategy.component';
import { AsServiceLogActivityComponent } from './as-service-log-activity/as-service-log-activity.component';
import { AsServicePlanActivityComponent } from './as-service-plan-activity/as-service-plan-activity.component';
import { AsServicePlanAddEditActivityComponent } from './as-service-plan-add-edit-activity/as-service-plan-add-edit-activity.component';
import { AsServicePlanAddEditServiceComponent } from './as-service-plan-add-edit-service/as-service-plan-add-edit-service.component';
import { AsAgencyProvidedServicesComponent } from './as-service-log-activity/agency-provided-services/agency-provided-services.component';
import { AsReferredServicesComponent } from './as-service-log-activity/referred-services/referred-services.component';
import { AsServiceLogComponent } from './as-service-plan-activity/service-log/service-log.component';
import { MatTabsModule, MatSelectModule, MatDatepickerModule,
  MatTableModule, MatFormFieldModule, MatInputModule, MatCheckboxModule,
  MatAutocompleteModule, MatRadioModule, MatTooltipModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMaskModule } from 'ngx-mask';
import { AgmCoreModule } from '@agm/core';
import { ServicePlanUnmetComponent } from './service-plan-unmet/service-plan-unmet.component';

@NgModule({
  imports: [
    CommonModule,
    AsServicePlanRoutingModule,
        MatTabsModule,
        MatSelectModule,
        MatTableModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatTooltipModule,
        MatRadioModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        FormsModule,
        PaginationModule,
        A2Edatetimepicker,
        NgSelectModule,
        NgxMaskModule.forRoot(),
        AgmCoreModule
  ],
  declarations: [AsServicePlanComponent,
                 AsGoalStrategyComponent,
                 AsServiceLogActivityComponent,
                 AsServicePlanActivityComponent,
                 AsServicePlanAddEditActivityComponent,
                 AsServicePlanAddEditServiceComponent,
                 AsAgencyProvidedServicesComponent,
                 AsReferredServicesComponent,
                 AsServiceLogComponent,
                 ServicePlanUnmetComponent]
})
export class AsServicePlanModule { }
