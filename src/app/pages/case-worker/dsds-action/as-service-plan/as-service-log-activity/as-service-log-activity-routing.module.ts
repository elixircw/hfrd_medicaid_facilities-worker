import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsServiceLogActivityComponent } from '../as-service-log-activity/as-service-log-activity.component';
import { AsAgencyProvidedServicesComponent } from '../as-service-log-activity/agency-provided-services/agency-provided-services.component';
import { AsReferredServicesComponent} from '../as-service-log-activity/referred-services/referred-services.component';


const routes: Routes = [
    {
        path: '',
        component: AsServiceLogActivityComponent,
        children: [
            {
                path: 'agency-provided-services',
                component: AsAgencyProvidedServicesComponent
            },
            {
                path: 'referred-services',
                component: AsReferredServicesComponent
            },
            {
                path: '',
                redirectTo: 'agency-provided-services',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ASServiceLogActivityRoutingModule {}
