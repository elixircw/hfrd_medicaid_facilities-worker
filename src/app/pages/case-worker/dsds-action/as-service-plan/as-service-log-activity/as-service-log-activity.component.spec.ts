import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsServiceLogActivityComponent } from './as-service-log-activity.component';

describe('AsServiceLogActivityComponent', () => {
  let component: AsServiceLogActivityComponent;
  let fixture: ComponentFixture<AsServiceLogActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsServiceLogActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsServiceLogActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
