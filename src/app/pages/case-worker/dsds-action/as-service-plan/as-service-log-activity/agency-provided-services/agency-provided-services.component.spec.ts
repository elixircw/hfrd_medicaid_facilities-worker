import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsAgencyProvidedServicesComponent } from './agency-provided-services.component';

describe('AsAgencyProvidedServicesComponent', () => {
  let component: AsAgencyProvidedServicesComponent;
  let fixture: ComponentFixture<AsAgencyProvidedServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsAgencyProvidedServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsAgencyProvidedServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
