import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AppUser } from '../../../../../../@core/entities/authDataModel';
import { DropdownModel, PaginationRequest, PaginationInfo } from '../../../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService } from '../../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { AgencyCategories, AgencyServices } from '../../../../_entities/caseworker.data.model';
import value from '*.json';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'agency-provided-services',
  templateUrl: './agency-provided-services.component.html',
  styleUrls: ['./agency-provided-services.component.scss'],
  providers: [DatePipe]
})
export class AsAgencyProvidedServicesComponent implements OnInit {

  agencyServiceList$: Observable<any[]>;
  totalRecord$: Observable<number>;
  paginationInfo: PaginationInfo = new PaginationInfo();
  agencyService: any;

  addNewProviderForm: FormGroup;
  viewProviderForm: FormGroup;
  clientProgramNames$: Observable<DropdownModel[]>;
  agencyServiceNames$: Observable<DropdownModel[]>;

  frequencyCds$: Observable<DropdownModel[]>;
  durationCds$: Observable<DropdownModel[]>;

  agencyCategories$: Observable<AgencyCategories[]>;
  agencyServices$: Observable<AgencyServices[]>;
  agencyDetailsFirstDiv: boolean;
  agencyDetailsSecondDiv: boolean;
  private token: AppUser;
  a2eOptions: any;

  addEditlabel: string;
  constructor(
    private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private datePipe: DatePipe
  ) {
    this.formInitialize();
    this.token = this._authService.getCurrentUser();
    this.a2eOptions = { format: 'MM/DD/YYYY', useCurrent: false };
  }

  private formInitialize() {
    this.addNewProviderForm = this.formBuilder.group(
      {
        clientprogramnameid: [''],
        categoryid: [''],
        servicetypeid: [''],
        frequencyCdId: '',
        durationCdId: '',
        estbegindate: [''],
        actbegindate: '',
        estenddate: '',
        actenddate: '',
        actbegintime: '',
        actendtime: '',
        agencynotes: ''
      });

  }

  public selectService() {
    this.agencyDetailsFirstDiv = false;
    this.agencyDetailsSecondDiv = true;
  }

  private clientProgramDropdown() {
    // this.clientProgramNames$ = this._commonHttpService.getArrayList(
    //   new PaginationRequest({
    //     nolimit: true,
    //     method: 'get'
    //   }),
    //   //CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/4' + '?data'
    //   CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
    // );
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientProgramName + '?access_token=' + this.token.id,

      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientprogramServices + '?access_token=' + this.token.id,

      ),
      this._commonHttpService.getArrayList(
        {
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Frequency,

      ),
      this._commonHttpService.getArrayList(
        {
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Duration,

      )
    ])
      .map((result) => {

        return {
          clientprogramnameid: result[0]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: res.agency_program_nm,
                value: res.agency_program_area_id
              })
          ),
          servicetypeid: result[1]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: res.SERVICE_NM,
                value: res.SERVICE_ID
              })
          ),
          frequencyCdId: result[2]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: (res.value_tx),
                value: (res.picklist_value_cd),
              })
          ),
          durationCdId: result[3]['UserToken'].map(
            (res) =>
              new DropdownModel({
                text: (res.value_tx),
                value: (res.picklist_value_cd),
              })
          )
        };
      })
      .share();

    this.clientProgramNames$ = source.pluck('clientprogramnameid');

    this.agencyServiceNames$ = source.pluck('servicetypeid');
    this.frequencyCds$ = source.pluck('frequencyCdId');

    this.durationCds$ = source.pluck('durationCdId');
  }

  ngOnInit() {
   this.clientProgramDropdown();
    this.getList();
  }


  pageChanged(page: number) {
    this.getList(page);
  }
  getList(pageNo = 1) {

    /*
        this._commonHttpService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.agencyServiceLog + '?access_token=' + this.token.id;
        const body = new PaginationRequest({
          where: {},
          limit: 10,
          method: 'post',
          page: pageNo
        });

        const source = this._commonHttpService.getPagedArrayList(body).share();
        this.agencyServiceList$ = source.pluck('data');
        if (pageNo === 1) {
          this.totalRecord$ = source.pluck('count');
        } */

    this.agencyServiceList$ = this._commonHttpService.getArrayList(
      {
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.agencyServiceLog + '?access_token=' + this.token.id,

    )
      .map(res => {
        return res['servicelogData'];
      });
  }
  initaddNewProviderForm() {

    this.agencyDetailsFirstDiv = true;
    this.agencyDetailsSecondDiv = false;

    this.agencyService = null;
    this.addEditlabel = 'Add';
    this.addNewProviderForm.reset();

  }

  // tslint:disable-next-line:no-shadowed-variable
  formtString(value: string) {
    value = value.replace(',', '');
    value = value.replace('"', '');
    value = value.replace('(', '');
    value = value.replace(')', '');
    return value;
  }

  addAgency() {

    if (this.addNewProviderForm.valid) {
      let url = 'serviceLogs/' + (this.agencyService ? 'update' : 'save') + '?access_token=' + this.token.id;      

      const model = {
        'agencyServiceLdssId': this.addNewProviderForm.value.servicetypeid,
        'startDt': this.datePipe.transform(this.addNewProviderForm.value.actbegindate, 'yyyy-MM-dd'),
        "endDt": this.datePipe.transform(this.addNewProviderForm.value.actenddate, 'yyyy-MM-dd'),
        "descriptionTx": this.addNewProviderForm.value.agencynotes,
        "startTm": this.datePipe.transform(this.addNewProviderForm.value.actbegintime, 'hh-mm aa'),
        "endTm": this.datePipe.transform(this.addNewProviderForm.value.actendtime, 'hh-mm aa'),
        "estimatedStartDt": this.datePipe.transform(this.addNewProviderForm.value.estbegindate, 'yyyy-MM-dd'),
        "estimatedEndDt": this.datePipe.transform(this.addNewProviderForm.value.estenddate, 'yyyy-MM-dd'),
        "frequencyCd": this.addNewProviderForm.value.frequencyCdId,
        "durationCd": this.addNewProviderForm.value.durationCdId,
        "agencyProgramAreaId": this.addNewProviderForm.value.clientprogramnameid,
        "serviceLogId": this.agencyService ? this.agencyService.service_log_id : null
      };
      // var model =  this.addNewProviderForm.value;
      console.log(model);
      this._commonHttpService.create(model, url).subscribe((response) => {
        console.log(response);
        if (response) {
          this.getList();
          // if (response.ammappingid || response === 'Success') {
          //   this.closeItem();
          //   this._alertService.success('Record added successfully.');
          // } else {
          this.closeItem();
          //   this._alertService.error('Error occured, please try again');
          // }
        }
      },
        (error) => {
          this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);

          this.closeItem();
        });
    }
  }

  closeItem() {
    (<any>$('#add-newagencyprovider')).modal('hide');
  }

  view(agency) {
    this.agencyService = agency;
    (<any>$('#view-newagencyprovider')).modal('show');
  }

  delete(agencyService) {

    this.agencyService = agencyService;
    // this._commonHttpService.remove(agencyService.service_log_id, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServiceLog).subscribe(
    //   res => {
    //     this._alertService.success('Service deleted successfully');
    //     this.getList(1);
    //   },
    //   err => { }
    // );
  }



  // -------------------------------

  // delete(agencyService) {
    /* this._commonHttpService.remove(agencyService.service_log_id, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServiceLog).subscribe(
      res => {
        this._alertService.success('Service deleted successfully');
        this.getList(1);
        (<any>$('#Delete-newagencyprovider')).modal('hide');
      },
      err => {
        this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        (<any>$('#Delete-newagencyprovider')).modal('hide');
       }
    );
  } */

  // ---------------------------

  edit(agency) {

    this.agencyService = agency;
    this.updateForm(agency);
    this.addEditlabel = 'Edit';
    (<any>$('#add-newagencyprovider')).modal('show');
  }
  updateForm(agency) {
    const model = {
      clientprogramnameid: agency.agency_program_area_id,
      categoryid: 1,
      servicetypeid: agency.service_id,
      frequencyCdId: agency.frequency_cd,
      durationCdId: agency.duration_cd,
      estbegindate: new Date(agency.estimated_start_date),
      actbegindate: new Date(agency.actual_begin_date),
      estenddate: new Date(agency.estimated_end_date),
      actenddate: new Date(agency.actual_end_date),
      actbegintime: agency.actual_start_time,
      actendtime: agency.actual_end_time,
      agencynotes: agency.notes
    };
    this.addNewProviderForm.setValue(model, { emitEvent: true, onlySelf: false });
  }

  deleteItem() {
    this._commonHttpService.remove(this.agencyService.service_log_id, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServiceLog).subscribe(
      res => {
        this._alertService.success('Service deleted successfully');
        (<any>$('#Delete-newagencyprovider')).modal('hide');
        this.getList(1);
      },
      err => { }
    );

  }
}
