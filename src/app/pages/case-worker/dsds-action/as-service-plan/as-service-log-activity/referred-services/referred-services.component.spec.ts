import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsReferredServicesComponent } from './referred-services.component';

describe('AsReferredServicesComponent', () => {
  let component: AsReferredServicesComponent;
  let fixture: ComponentFixture<AsReferredServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsReferredServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsReferredServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
