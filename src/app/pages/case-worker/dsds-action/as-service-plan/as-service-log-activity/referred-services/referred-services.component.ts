import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { AlertService, AuthService, CommonHttpService, ValidationService } from '../../../../../../@core/services';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../../case-worker-url.config';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AppUser } from '../../../../../../@core/entities/authDataModel';


import { Occurence, RepeatsOn, SearchPlan, SearchPlanRes, ServicePlanConfig } from '../../_entities/as-service-plan.model';
import { DatePipe } from '@angular/common';
import { GLOBAL_MESSAGES } from '../../../../../../@core/entities/constants';
import { date } from 'ng4-validators/src/app/date/validator';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'referred-services',
    templateUrl: './referred-services.component.html',
    styleUrls: ['./referred-services.component.scss'],
    providers: [DatePipe]
})
export class AsReferredServicesComponent implements OnInit {

    referredServiceList$: Observable<any[]>;
    referredServiceForm: FormGroup;
    addNewPurchaseAuthorization: FormGroup;
    EditreferredServiceForm: FormGroup;
    purchaseServiceForm: FormGroup;

    addNewReferredServiceSecondForm: FormGroup;
    refrredSearchFormDiv: boolean;
    searchData = false;


    categoryTypes$: Observable<DropdownModel[]>;
    categorySubTypes$: Observable<DropdownModel[]>;
    paymentTypes$: Observable<DropdownModel>;
    vendorServices$: Observable<DropdownModel[]>;
    fiscalCode$: Observable<DropdownModel[]>;
    serviceEndReason$: Observable<DropdownModel[]>;
    reasonServiceNotReceived$: Observable<DropdownModel[]>;



    clientProgramNames$: Observable<DropdownModel[]>;

    frequencyCds$: Observable<DropdownModel[]>;
    durationCds$: Observable<DropdownModel[]>;

    servicePlanForm: FormGroup;
    addServicePlanForm: FormGroup;
    timesInDayHide = false;
    id: string;
    searchPlan: any;
    ReferredServices$: Observable<any[]>;
    SearchPlan$: Observable<SearchPlan[]>;
    SearchPlanRes$: Observable<SearchPlanRes>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    servicePlanCountyValuesDropdownItems$: Observable<DropdownModel[]>;
    providerId: string;
    nextDisabled = true;
    isrepeatschecked: boolean;
    isRepeats = 0;
    zoom: number;
    defaultLat: number;
    defaultLng: number;
    mapBtn = false;
    markersLocation = ([] = []);
    mindate = new Date();
    endMinDate = new Date();
    referredService: any;
    private token: AppUser;

    constructor(
        private formBuilder: FormBuilder,
        private _commonHttpService: CommonHttpService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private datePipe: DatePipe

    ) {
        this.formInitialize();
        this.token = this._authService.getCurrentUser();
    }

    private formInitialize() {

        this.purchaseServiceForm = this.formBuilder.group(
            {
                fiscalCode: [''],
                voucherRequested: '',
                costnottoexceed: '',
                justificationCode: '',
            });

        this.referredServiceForm = this.formBuilder.group(
            {
                servicetypeid: [''],
                providerid: '',
                providername: '',
                taxid: '',
                zipcode: ''
            });

        this.EditreferredServiceForm = this.formBuilder.group(
            {
                servicetypeid: [{ value: '', disabled: true }],
                serviceNotReceivedReason: [''],
                serviceReceivedReason: [''],
                providerid: [{ value: '', disabled: true }],
                providername: [{ value: '', disabled: true }],
                taxid: [{ value: '', disabled: true }],
                zipcode: [{ value: '', disabled: true }],
                clientprogramnameid: [{ value: '', disabled: true }],
                estbegindate: [''],
                actbegindate: '',
                estenddate: '',
                actenddate: '',
                actbegintime: '',
                actendtime: '',
                agencynotes: '',
                frequencyCdId: '',
                durationCdId: '',
                dateReffered: '',
                courtOrderedSw: ''
            });

        this.addNewPurchaseAuthorization = this.formBuilder.group(
            {

            });

        this.addNewReferredServiceSecondForm = this.formBuilder.group(
            {
                clientprogramnameid: [''],
                categoryid: [''],
                servicetypeid: [''],
                frequencyCdId: '',
                durationCdId: '',
                estbegindate: [''],
                actbegindate: '',
                estenddate: '',
                actenddate: '',
                dateReffered: '',
                courtOrderedSw: '',
                actbegintime: '',
                actendtime: '',
                agencynotes: ''
            });
    }

    searchReferredServices(pID) {
        this.searchData = true;

        const ptestid = this.referredServiceForm.get('providerid').value;
        const providerName = this.referredServiceForm.get('providername').value;
        const taxid = this.referredServiceForm.get('taxid').value;
        const zipcode = this.referredServiceForm.get('zipcode').value;
        const services = this.referredServiceForm.get('servicetypeid').value;

        console.log(ptestid);
        // this.ReferredServices$ = Observable.of([{
        //     id: 123456, name: 'Test Name', details: 'TAX 01', zip: 45215, paid: 'Test Service', latitude: 39.29044,
        //     longitude: -77.61233
        // }, {
        //     id: 123456, name: 'Test Name', details: 'TAX 01', zip: 45215, paid: 'Test Service', latitude: 38.29044,
        //     longitude: -78.61233
        // }, {
        //     id: 123456, name: 'Test Name', details: 'TAX 01', zip: 45215, paid: 'Test Service', latitude: 37.29044,
        //     longitude: -76.61233
        // }])

        const providerId = '5000329';

        this.ReferredServices$ = this._commonHttpService.getArrayList(
            {
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.VendorServiceSearch + '?providerId=' + ptestid +
            '&providerName=' + providerName +
            '&taxId=' + taxid +
            '&serviceId=' + services +
            '&zipCd=' + zipcode +

            '&access_token=' + this.token.id,
        )
            .map(res => {
                return res['UserToken'];
            });
    }

    ngOnInit() {
        this.refrredSearchFormDiv = true;
        this.VendorDropDown();
        this.getList();
    }
    getList(pageNo = 1) {



        this.referredServiceList$ = this._commonHttpService.getArrayList(
            {
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.vendorServiceLog + '?access_token=' + this.token.id,

        )
            .map(res => {
                return res['servicelogData'];
            });
    }
    intiReferredServiceForm() {
        this.searchData = false;
    }

    listMap() {
        this.zoom = 11;
        this.defaultLat = 39.29044;
        this.defaultLng = -76.61233;
        this.ReferredServices$.subscribe(map => {
            this.markersLocation = [];
            if (map.length > 0) {
                map.forEach(res => {
                    if (res.latitude !== null && res.longitude !== null) {
                        const mapLocation = {
                            lat: +res.latitude,
                            lng: +res.longitude,
                            draggable: +true,
                            providername: res.providername !== null ? res.providername : 'NA',
                            addressline1: res.addressline1 !== null ? res.addressline1 : 'NA',
                        };
                        this.markersLocation.push(mapLocation);
                    } else {
                        const mapLocation = {
                            lat: +this.defaultLat,
                            lng: +this.defaultLng,
                            draggable: +true,
                            providername: res.providername !== null ? res.providername : 'NA',
                            addressline1: res.addressline1 !== null ? res.addressline1 : 'NA',
                        };
                        this.markersLocation.push(mapLocation);
                    }
                });
                if (
                    !this.markersLocation[0].lat !== null &&
                    !this.markersLocation[0].lng !== null
                ) {
                    this.defaultLat = this.markersLocation[0].lat;
                    this.defaultLng = this.markersLocation[0].lng;
                } else {
                    this.defaultLat = 39.29044;
                    this.defaultLng = -76.61233;
                }
                (<any>$('#iframe-popup')).modal('show');
            }
        });
    }
    mapClose() {
        this.markersLocation = [];
    }

    selectReferredService() {

        // this.searchPlan =  this.searchPlan

        (<any>$('#select-referredservice')).modal('show');
    }

    openpurchaseautherization(referredService) {
        this.referredService = referredService;
    }

    print() {

        // (<any>$('#purchase-autherization')).modal('hide');
        // (<any>$('#print-autherization')).modal('show');


        (<any>$('#view-log')).modal('show');
        (<any>$('#purchase-autherization')).modal('hide');

    }

    // PurchaseAuthorizationSave

    savePurchaseAuth(purchaseServiceForm) {
        if (this.purchaseServiceForm.valid) {
            const url = 'purchaseAuthorizations/purchaseAuthorize' + '?access_token=' + this.token.id;



            const model = {
                'fiscalCategoryCd': this.purchaseServiceForm.value.fiscalCode,
                'voucherSw': 'Y',
                'justificationTx': this.purchaseServiceForm.value.justificationCode,
                'costNo': this.purchaseServiceForm.value.costnottoexceed,
                'startDt': '2018-12-05',
                'endDt': '2018-12-05',
                'serviceLogId': this.referredService.service_log_id

            };
            // var model =  this.addNewReferredServiceSecondForm.value;
            console.log(model);
            this._commonHttpService.create(model, url).subscribe((response) => {
                console.log(response);
                // (<any>$('#Edit-newreferredservice')).modal('hide');
                this.getList();
                if (response) {
                }
            },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        }

    }

    private VendorDropDown() {
        // this.clientProgramNames$ = this._commonHttpService.getArrayList(
        //   new PaginationRequest({
        //     nolimit: true,
        //     method: 'get'
        //   }),
        //   //CaseWorkerUrlConfig.EndPoint.DSDSAction.Attachment.AttachmentGridUrl + '/4' + '?data'
        //   CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
        // );
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ClientProgramName + '?access_token=' + this.token.id,

            ), this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.VendorServices + '?access_token=' + this.token.id,

            ),
            this._commonHttpService.getArrayList(
                {
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Frequency,

            ),
            this._commonHttpService.getArrayList(
                {
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.Duration,

            ), this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.fiscalCodes + '?access_token=' + this.token.id,

            ),
            this._commonHttpService.getArrayList(
                {
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ServiceEndReason + '?access_token=' + this.token.id,

            ),
            this._commonHttpService.getArrayList(
                {
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ReasonServiceNotReceived + '?access_token=' + this.token.id,

            ),


        ])
            .map((result) => {

                return {
                    clientprogramnameid: result[0]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: res.agency_program_nm,
                                value: res.agency_program_area_id
                            })
                    ),
                    servicetypeid: result[1]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: res.service_nm,
                                value: res.service_id
                            })
                    ),
                    frequencyCdId: result[2]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.value_tx),
                                value: (res.picklist_value_cd),
                            })
                    ),
                    durationCdId: result[3]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.value_tx),
                                value: (res.picklist_value_cd),
                            })
                    ),

                    fiscalId: result[4]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.fiscalcateforycd),
                                value: (res.fiscalcategorydesc),
                            })
                    ),
                    serviceEndReason: result[5]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.service_end_reason),
                                value: (res.service_end_reason_cd),
                            })
                    ),
                    ReasonServiceNotReceived: result[6]['UserToken'].map(
                        (res) =>
                            new DropdownModel({
                                text: (res.reason_service_not_received),
                                value: (res.service_not_received_cd),
                            })
                    )
                };
            })
            .share();

        this.vendorServices$ = source.pluck('servicetypeid');


        this.clientProgramNames$ = source.pluck('clientprogramnameid');
        this.frequencyCds$ = source.pluck('frequencyCdId');

        this.durationCds$ = source.pluck('durationCdId');

        this.fiscalCode$ = source.pluck('fiscalId');
        this.serviceEndReason$ = source.pluck('serviceEndReason');
        this.reasonServiceNotReceived$ = source.pluck('ReasonServiceNotReceived');


    }

    selectedProv(searchPlan) {
        this.searchPlan = searchPlan;
    }

    saveReferredService() {


        if (this.addNewReferredServiceSecondForm.valid) {
            const url = 'serviceLogs/vendorSaveServiceLog' + '?access_token=' + this.token.id;

            console.log('Test');
            console.log(this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actbegindate, 'yyyy-MM-dd'));
            console.log(this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actenddate, 'yyyy-MM-dd'));
            console.log(this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actbegintime, 'hh-mm aa'));
            console.log(this.addNewReferredServiceSecondForm.value.actendtime);

            console.log('testcheckbox');

            console.log(this.addNewReferredServiceSecondForm.value.courtOrderedSw);



            const model = {
                'providerServiceId': this.searchPlan.ID,
                'startDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actbegindate, 'yyyy-MM-dd'),
                'endDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actenddate, 'yyyy-MM-dd'),
                'descriptionTx': this.addNewReferredServiceSecondForm.value.agencynotes,
                'startTm': '9:10PM',
                'endTm': '9:10PM',
                'endServiceReasonCd': 'dfg',
                'noServiceReasonCd': 'abcd',
                'estimatedStartDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.estbegindate, 'yyyy-MM-dd'),
                'estimatedEndDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.estenddate, 'yyyy-MM-dd'),
                'frequencyCd': this.addNewReferredServiceSecondForm.value.frequencyCdId,
                'durationCd': this.addNewReferredServiceSecondForm.value.durationCdId,
                'courtOrderedSw': this.addNewReferredServiceSecondForm.value.courtOrderedSw ? 'Y' : 'N',
                'agencyProgramAreaId': this.addNewReferredServiceSecondForm.value.clientprogramnameid,
                'referredDt': this.addNewReferredServiceSecondForm.value.dateReffered,
                'serviceLogId': this.addNewReferredServiceSecondForm ? this.addNewReferredServiceSecondForm.value.service_log_id : null
            };
            // var model =  this.addNewReferredServiceSecondForm.value;
            console.log(model);
            this._commonHttpService.create(model, url).subscribe((response) => {
                console.log(response);
                (<any>$('#Edit-newreferredservice')).modal('hide');
                this.getList();
                if (response) {
                }
            },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        }
    }


    edit(referredService) {
        this.referredService = referredService;

        const model = {
            clientprogramnameid: referredService.agency_program_areaid,

            servicetypeid: referredService.service_id,
            frequencyCdId: referredService.frequency_cd,
            durationCdId: referredService.duration_cd,
            estbegindate: new Date(referredService.estimated_start_date),
            actbegindate: new Date(referredService.actual_start_date),
            estenddate: new Date(referredService.estimated_end_date),
            actenddate: new Date(referredService.actual_end_date),
            actbegintime: referredService.start_time,
            actendtime: referredService.end_time,
            agencynotes: referredService.notes,
            serviceNotReceivedReason: '',
            serviceReceivedReason: '',
            providerid: referredService.provider_id,
            providername: referredService.provider_nm,
            taxid: referredService.tax_id,
            zipcode: referredService.zip,
            dateReffered: new Date(referredService.referred_date),
            courtOrderedSw: referredService.courtorder
        };
        this.EditreferredServiceForm.setValue(model, { emitEvent: true, onlySelf: false });
    }

    updateReferredService() {
        if (this.EditreferredServiceForm.valid) {
            const url = 'serviceLogs/editVendorServiceLog' + '?access_token=' + this.token.id;

            const model = {
                'providerServiceId': this.referredService.provider_service_id,
                'courtOrderedSw': this.referredService.value.courtOrderedSw ? 'Y' : 'N',
                'endServiceReasonCd': 'found', // this.EditreferredServiceForm.value.serviceNotReceivedReason,

                'startDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actbegindate, 'yyyy-MM-dd'),
                'endDt': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actenddate, 'yyyy-MM-dd'),
                'descriptionTx': this.EditreferredServiceForm.value.agencynotes,
                'startTm': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actbegintime, 'hh-mm aa'),
                'endTm': this.datePipe.transform(this.addNewReferredServiceSecondForm.value.actendtime, 'hh-mm aa'),
                'estimatedStartDt': this.datePipe.transform(this.EditreferredServiceForm.value.estbegindate, 'yyyy-MM-dd'),
                'estimatedEndDt': this.datePipe.transform(this.EditreferredServiceForm.value.estenddate, 'yyyy-MM-dd'),
                'frequencyCd': this.addNewReferredServiceSecondForm.value.frequencyCdId,
                'durationCd': this.addNewReferredServiceSecondForm.value.durationCdId,
                'agencyProgramAreaId': 1, // this.addNewReferredServiceSecondForm.value.clientprogramnameid,
                'serviceLogId': this.referredService.service_log_id
            };
            // var model =  this.addNewReferredServiceSecondForm.value;
            console.log(model);
            this._commonHttpService.update(null, model, url).subscribe((response) => {
                console.log(response);
                (<any>$('#Edit-newreferredservice')).modal('hide');
                if (response) {
                }
            },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                });
        }
    }

    // tslint:disable-next-line:member-ordering
    serviceReceived: any;
    changeServiceReceived() {
        this.serviceReceived = !this.serviceReceived;
        if (!this.serviceReceived) {
            this.EditreferredServiceForm.controls['serviceReceivedReason'].disable();
            this.EditreferredServiceForm.controls['estbegindate'].disable();
            this.EditreferredServiceForm.controls['actbegindate'].disable();
            this.EditreferredServiceForm.controls['agencynotes'].disable();
        } else {
            this.EditreferredServiceForm.controls['serviceReceivedReason'].enable();
            this.EditreferredServiceForm.controls['estbegindate'].enable();
            this.EditreferredServiceForm.controls['actbegindate'].enable();
            this.EditreferredServiceForm.controls['agencynotes'].enable();
        }
    }

    // tslint:disable-next-line:member-ordering
    serviceNotReceived: any;
    changeServiceNotReceived() {


        this.serviceNotReceived = !this.serviceNotReceived;
        if (!this.serviceNotReceived) {
            this.EditreferredServiceForm.controls['serviceNotReceivedReason'].disable();
        } else {
            this.EditreferredServiceForm.controls['serviceNotReceivedReason'].enable();
        }
    }


    delete(referredService) {
        this.referredService = referredService;
    }



    deleteItem() {
        this._commonHttpService.remove(this.referredService.service_log_id, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServiceLog).subscribe(
            res => {
                this._alertService.success('Service deleted successfully');
                (<any>$('#Delete-newreferredservice')).modal('hide');
                this.getList(1);
            },
            err => { }
        );
    }
}
