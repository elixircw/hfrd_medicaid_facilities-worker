import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsServicePlanActivityComponent } from './as-service-plan-activity.component';

describe('AsServicePlanActivityComponent', () => {
  let component: AsServicePlanActivityComponent;
  let fixture: ComponentFixture<AsServicePlanActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsServicePlanActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsServicePlanActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
