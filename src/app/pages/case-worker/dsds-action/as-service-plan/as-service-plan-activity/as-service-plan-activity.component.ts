import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AlertService, CommonHttpService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { AsAddActivity, ServicePlanActivityRes, AsServicePlanActivity, ReviewCase } from '../_entities/as-service-plan.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'service-plan-activity',
    templateUrl: './as-service-plan-activity.component.html',
    styleUrls: ['./as-service-plan-activity.component.scss']
})
export class AsServicePlanActivityComponent implements OnInit {
    servicePlanActivity$: Observable<AsServicePlanActivity[]>;
    servicePlanActivityCount$: Observable<ServicePlanActivityRes>;
    reviewServicesDetails$: Observable<ReviewCase[]>;
    totalResulRecords$: Observable<number>;
    servicePlanId$ = new Subject<string>();
    isAgencyProvided$  = new Subject<boolean>();
    @Input()
    servicePlanActivityiId: string;
    @Input()
    activityPlanActivityiId: string;
    id: string;
    // @Input()
    // activityPlanActivityi: string;
    @Input()
    editASServicePlanOutputSubject$ = new Subject<AsAddActivity>();
    disableService: boolean;
    serviceType: string;

    constructor(private _httpService: CommonHttpService, private _formBuilder: FormBuilder, private _alertService: AlertService, private route: ActivatedRoute,
        private _dataStoreService: DataStoreService) {}

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.getServicePlanActivity(1);
        this.viewServiceLog(null);
    }
    servicePlane() {
        this.getServicePlanActivity(1);
    }
    updateServicePlanActivityiId(id) {
        this.servicePlanActivityiId = id;
        this.isAgencyProvided$.next(true);
    }
    getServicePlanActivity(pageNo: number) {
        const source = this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    limit: 10,
                    page: pageNo,
                    where: { objectid: this.id },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ActivityUrl + '?filter'
            )
            .map(res => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.servicePlanActivity$ = source.pluck('data');
        this.servicePlanActivityCount$ = source.pluck('count');
    }
    reviewServices() {
        const source = this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    nolimit: true,
                    page: 1,
                    where: { intakeserviceid: this.id },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.ServicePlanReviewCase + '?filter'
            )
            .map(res => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.reviewServicesDetails$ = source.pluck('data');
    }
    editServiceEmpty() {
        this.editASServicePlanOutputSubject$.next();
    }

    editServicePlan(ativity: AsAddActivity) {
        this.activityPlanActivityiId = ativity.serviceplanactivityid;
        ativity.isAddEdit = 'Edit';
        this.editASServicePlanOutputSubject$.next(ativity);
    }

    deleteServicePlan(activity) {
        this._httpService.patch(activity.serviceplanactivityid, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServicePlanUrl).subscribe(
            res => {
                this._alertService.success('Successfully deleted Activity');
                this.getServicePlanActivity(1);
            },
            err => {}
        );
    }

    viewServiceLog(serviceplanid) {
        this.servicePlanId$.next(serviceplanid);
    }
}
