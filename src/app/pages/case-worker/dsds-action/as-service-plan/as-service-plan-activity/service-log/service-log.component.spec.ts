import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsServiceLogComponent } from './service-log.component';

describe('AsServiceLogComponent', () => {
  let component: AsServiceLogComponent;
  let fixture: ComponentFixture<AsServiceLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsServiceLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsServiceLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
