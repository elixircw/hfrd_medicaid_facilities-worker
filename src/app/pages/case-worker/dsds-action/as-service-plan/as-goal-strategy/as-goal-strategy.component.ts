import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ServicePlanGoal, AddServicePlanGoal } from '../_entities/as-service-plan.model';
import { CommonHttpService, GenericService, AlertService, DataStoreService } from '../../../../../@core/services';
import { PaginationRequest, DropdownModel } from '../../../../../@core/entities/common.entities';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { ActivatedRoute } from '@angular/router';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'goal-strategy',
    templateUrl: './as-goal-strategy.component.html',
    styleUrls: ['./as-goal-strategy.component.scss']
})
export class AsGoalStrategyComponent implements OnInit {
    addServciePlanGoal: AddServicePlanGoal;
    servicePlanGoal$: Observable<ServicePlanGoal[]>;
    goalListDetails$: Observable<DropdownModel[]>;
    goalCategory$: Observable<DropdownModel[]>;
    goalDescription: string[] = [];
    totalResulRecords$: Observable<number>;
    goalStrategy: FormGroup;
    id: string;
    servicePlanGoalId: string;
    isAdded: boolean;
    addEditLabel: string;
    maxdate = new Date();
    showAdditionalGoal = false;
    constructor(
        private _httpService: CommonHttpService,
        private _addGoalService: GenericService<AddServicePlanGoal>,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _dataStoreService: DataStoreService
    ) {}
    ngOnInit() {
        this.isAdded = true;
        this.addEditLabel = 'Add New';
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.intializeForm();
        this.getServicePlanGoal(1);
        this.getGoalList();
        this.getProjectHomeGoalCategory();
    }

    intializeForm() {
        this.goalStrategy = this._formBuilder.group({
            goal: [null],
            goaldate: [null],
            isprojecthome: false,
            strategy: [null],
            goaltypekey: [null],
            plannedstartdate: [null],
            plannedenddate: [null],
            goalscategorytype: [null],
            initialcondition: [null],
            challenges: [null],
            strengths: [null]
        });
    }
    addServicePlanGoal(goal: AddServicePlanGoal) {
        this.addServciePlanGoal = Object.assign(goal);
        this.addServciePlanGoal.objecttypekey = 'ServiceRequest';
        this.addServciePlanGoal.objectid = this.id;
        this._addGoalService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.GoalUrl + '/'; // WSO2
        this._addGoalService.create(this.addServciePlanGoal).subscribe(
            response => {
                this._alertService.success('Goal added successfully');
                this.goalStrategy.reset();
                this.clearGoals();
                (<any>$('#cls-add-goal')).click();
                this.getServicePlanGoal(1);
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    editGoals(model) {
        this.isAdded = false;
        this.addEditLabel = 'Edit';
        if (model.isprojecthome) {
            this.getGoalCategory();
        }
        this.goalStrategy.patchValue(model);
        this.servicePlanGoalId = model.serviceplangoalid;
    }
    updateServicePlanGoal(item) {
        this.addServciePlanGoal = Object.assign(item);
        this.addServciePlanGoal.objecttypekey = 'ServiceRequest';
        this.addServciePlanGoal.objectid = this.id;
        this.addServciePlanGoal.serviceplangoalid = this.servicePlanGoalId;
        this._addGoalService.patchWithoutid(this.addServciePlanGoal, 'serviceplan/goal').subscribe(
            response => {
                this._alertService.success('Goal updated successfully');
                this.goalStrategy.reset();
                this.clearGoals();
                (<any>$('#cls-add-goal')).click();
                this.getServicePlanGoal(1);
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    deleteServicePlanGoal(goal) {
        this._httpService.patch(goal.serviceplangoalid, { activeflag: 0 }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.GoalUrl + '/').subscribe(
            res => {
                this._alertService.success('Goal deleted successfully');
                this.getServicePlanGoal(1);
            },
            err => {}
        );
    }
    clearGoals() {
        this.addEditLabel = 'Add New';
        this.goalStrategy.reset();
        this.intializeForm();
        this.isAdded = true;
        this.showAdditionalGoal = false;
        this.getProjectHomeGoalCategory();
    }
    getServicePlanGoal(pageNo: number) {
        this.servicePlanGoal$ = this._httpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    page: pageNo,
                    where: { objectid: this.id },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.AsServicePlan.GoalUrl + '?filter'
            )
            .map(res => {
                return res;
            });
    }
    getGoalList() {
        this.goalListDetails$ = this._httpService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        tablename: 'goals type',
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes?filter'
            )
            .map(item => {
                return item.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
        this.goalListDetails$.subscribe(item => {
            item.map(res => {
                this.goalDescription[res.value] = res.text;
            });
        });
    }

    getGoalCategory() {
        this.goalCategory$ = Observable.of([]);
        this.goalCategory$ = this._httpService
        .getArrayList(
            {
                method: 'get',
                where: {
                    tablename: 'ServiceplanGoalsCategory',
                    teamtypekey: 'AS'
                }
            },
            'referencetype/gettypes?filter'
        )
        .map(item => {
            return item.map(
                res =>
                    new DropdownModel({
                        text: res.description,
                        value: res.ref_key
                    })
            );
        });
    }

    getProjectHomeGoalCategory() {
        this.goalCategory$ = Observable.of([]);
        this.goalCategory$ = this._httpService
        .getArrayList(
            {
                method: 'get',
                where: {
                    tablename: 'ServiceplanGoalsCategoryType',
                    teamtypekey: 'AS'
                }
            },
            'referencetype/gettypes?filter'
        )
        .map(item => {
            return item.map(
                res =>
                    new DropdownModel({
                        text: res.description,
                        value: res.ref_key
                    })
            );
        });
    }

    changeGoals(event) {
        if (event.checked) {
            this.showAdditionalGoal = true;
            this.getGoalCategory();
        } else {
            this.showAdditionalGoal = false;
            this.getProjectHomeGoalCategory();
        }
    }
}
