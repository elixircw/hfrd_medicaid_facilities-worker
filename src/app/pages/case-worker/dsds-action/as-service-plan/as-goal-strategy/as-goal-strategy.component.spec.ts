import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsGoalStrategyComponent } from './as-goal-strategy.component';

describe('AsGoalStrategyComponent', () => {
  let component: AsGoalStrategyComponent;
  let fixture: ComponentFixture<AsGoalStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsGoalStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsGoalStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
