import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicePlanUnmetComponent } from './service-plan-unmet.component';

describe('ServicePlanUnmetComponent', () => {
  let component: ServicePlanUnmetComponent;
  let fixture: ComponentFixture<ServicePlanUnmetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicePlanUnmetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicePlanUnmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
