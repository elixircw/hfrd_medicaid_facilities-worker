import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { CommonHttpService } from '../../../../../@core/services/common-http.service';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Unmet, UnmetList } from '../_entities/as-service-plan.model';
import { AlertService, DataStoreService } from '../../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var $: any;

@Component({
    selector: 'service-plan-unmet',
    templateUrl: './service-plan-unmet.component.html',
    styleUrls: ['./service-plan-unmet.component.scss']
})
export class ServicePlanUnmetComponent implements OnInit {
    id: string;
    unMetFormGroup: FormGroup;
    showService = false;
    showSubtyoe = false;
    showReason = false;
    unmetListData$: Observable<UnmetList[]>;
    reasonDropdownItems$: Observable<DropdownModel[]>;
    serviceTypeDropdownItems$: Observable<DropdownModel[]>;
    subTypeDropdownItems$: Observable<DropdownModel[]>;
    unmetSubmitData: Unmet;
    activityiId: string;

    constructor(private _httpService: CommonHttpService, private _formBuilder: FormBuilder, private route: ActivatedRoute, private _alertService: AlertService,
        private _dataStoreService: DataStoreService) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    }

    ngOnInit() {
        this.getUnmetList(1);
        this.loadForm();
        this.loadDropDown();
    }

    loadForm() {
        this.unMetFormGroup = this._formBuilder.group({
            need: ['', Validators.required],
            activity: ['', Validators.required],
            servicetypekey: ['', Validators.required],
            activitysubtypekey: ['', Validators.required],
            reasontypekey: ['', Validators.required],
            otherservicetypedescription: [''],
            othersubtypedescription: [''],
            otherreason: [''],
            personinvolved: null,
            responsibleperson: null,
            reevaluationdate: null,
            completiondate: null,
            serviceplangoalid: null,
            serviceplanactivitystatustypekey: 'ACTIVE',
            objecttypekey: 'ServiceRequest',
            serviceunavailable: 'UN'
        });
    }
    loadDropDown() {
        const source = Observable.forkJoin([
            this._httpService.getArrayList({ where: { placementtypekey: 'Serviceplan' }, nolimit: true, method: 'get' }, 'servicetype/list?filter'),
            this._httpService.getArrayList(
                {
                    where: { tablename: 'serviceplanreason', teamtypekey: 'AS' },
                    nolimit: true,
                    method: 'get'
                },
                'referencetype/gettypes?filter'
            )
        ]).map(data => {
            return {
                serviceType: data[0].map(
                    result =>
                        new DropdownModel({
                            text: result.servicetypedescription,
                            value: result.servicetypekey
                        })
                ),
                reason: data[1].map(
                    result =>
                        new DropdownModel({
                            text: result.description,
                            value: result.ref_key
                        })
                )
            };
        });
        this.serviceTypeDropdownItems$ = source.pluck('serviceType');
        this.reasonDropdownItems$ = source.pluck('reason');
    }

    getUnmetList(pageNo: number) {
        const source = this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    limit: 10,
                    page: pageNo,
                    where: { objectid: this.id },
                    method: 'get'
                }),
                'serviceplan/activity/serviceplanunmetlist?filter'
            )
            .map(res => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.unmetListData$ = source.pluck('data');
    }

    otherValue(value, status) {
        if (status === 'service') {
            this.subTypeDropdownItems$ = Observable.of([]);
            this.unMetFormGroup.patchValue({ othersubtypedescription: '' });
            this.showSubtyoe = false;
            if (value === 'Othe') {
                this.showService = true;
                this.getSubType(value);
            } else {
                this.showService = false;
                this.getSubType(value);
            }
        }
        if (status === 'subtype') {
            if (value === 'Othe' || value === 'Oth') {
                this.showSubtyoe = true;
            } else {
                this.showSubtyoe = false;
            }
        }
        if (status === 'reason') {
            if (value === 'OTHER') {
                this.showReason = true;
            } else {
                this.showReason = false;
            }
        }
    }

    getSubType(serviceKey) {
        this.subTypeDropdownItems$ = this._httpService
            .getArrayList(
                {
                    where: { tablename: 'ActivitySubtype', teamtypekey: 'AS', parentkey: serviceKey },
                    method: 'get'
                },
                'referencetype/gettypes?filter'
            )
            .map(result => {
                return result.map(value => {
                    return new DropdownModel({
                        text: value.description,
                        value: value.ref_key
                    });
                });
            });
    }

    submitUnmet(unmetData) {
        if (unmetData) {
            this.unmetSubmitData = unmetData;
            this.unmetSubmitData.objectid = this.id;
            if (this.activityiId) {
                this._httpService.patch(this.activityiId, this.unmetSubmitData, 'serviceplan/activity/').subscribe(
                    res => {
                        this.clearDefaults();
                        this.getUnmetList(1);
                        this._alertService.success('Unmet/Unavailable Updated Successfully');
                    },
                    err => {}
                );
            } else {
                this._httpService.create(this.unmetSubmitData, 'serviceplan/activity/').subscribe(data => {
                    if (data) {
                        this.clearDefaults();
                        this.getUnmetList(1);
                        this._alertService.success('Unmet/Unavailable Saved Successfully');
                    }
                });
            }
        }
    }

    editUnmet(list) {
        this.activityiId = list.serviceplanactivityid;
        this.getSubType(list.servicetypekey);
        if (list.otherservicetypedescription) {
            this.showService = true;
        }
        if (list.othersubtypedescription) {
            this.showSubtyoe = true;
        }
        if (list.otherreason) {
            this.showReason = true;
        }
        this.unMetFormGroup.patchValue(list);
    }

    deleteUnmet(activity) {
        this._httpService.patch(activity.serviceplanactivityid, { activeflag: 0 }, 'serviceplan/activity/').subscribe(
            res => {
                this._alertService.success('Successfully Deleted Unmet/Unavailable');
                this.getUnmetList(1);
            },
            err => {}
        );
    }

    clearDefaults() {
        this.unMetFormGroup.reset();
        (<any>$('#add-unmet')).modal('hide');
        this.activityiId = null;
        this.loadForm();
        this.showService = false;
        this.showSubtyoe = false;
        this.showReason = false;
    }
}
