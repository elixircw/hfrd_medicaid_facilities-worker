import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsServicePlanComponent } from './as-service-plan.component';

describe('AsServicePlanComponent', () => {
  let component: AsServicePlanComponent;
  let fixture: ComponentFixture<AsServicePlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsServicePlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsServicePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
