import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsServicePlanAddEditServiceComponent } from './as-service-plan-add-edit-service.component';

describe('AsServicePlanAddEditServiceComponent', () => {
  let component: AsServicePlanAddEditServiceComponent;
  let fixture: ComponentFixture<AsServicePlanAddEditServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsServicePlanAddEditServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsServicePlanAddEditServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
