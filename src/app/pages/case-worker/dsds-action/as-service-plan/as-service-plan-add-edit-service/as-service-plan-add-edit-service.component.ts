import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';

import { DropdownModel, PaginationInfo } from '../../../../../@core/entities/common.entities';
import { AlertService, CommonHttpService, GenericService, AuthService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { Occurence, RepeatsOn, SearchPlan, SearchPlanRes, ServicePlanConfig, AsOccurence, FamliyService } from '../_entities/as-service-plan.model';
import { MatDatepickerInputEvent } from '@angular/material';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { GLOBAL_MESSAGES } from '../../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'as-service-plan-add-edit-service',
    templateUrl: './as-service-plan-add-edit-service.component.html',
    styleUrls: ['./as-service-plan-add-edit-service.component.scss']
})
export class AsServicePlanAddEditServiceComponent implements OnInit {
    caseTypes$: Observable<DropdownModel>;
    categoryTypes$: Observable<DropdownModel[]>;
    categorySubTypes$: Observable<DropdownModel[]>;
    paymentTypes$: Observable<DropdownModel>;
    servicePlanForm: FormGroup;
    addServicePlanForm: FormGroup;
    familyServiceGroup: FormGroup;
    timesInDayHide = false;
    weekDays: RepeatsOn[] = [];
    id: string;
    servicePlanAdd: ServicePlanConfig;
    SearchPlan$: Observable<SearchPlan[]>;
    SearchAgency$: Observable<any[]>;
    SearchPlanRes$: Observable<SearchPlanRes>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    servicePlanCountyValuesDropdownItems$: Observable<DropdownModel[]>;
    private pageStream$ = new Subject<number>();
    providerId: string;
    agencyId: string;
    nextDisabled = true;
    isrepeatschecked: boolean;
    isRepeats = 0;
    zoom: number;
    defaultLat: number;
    defaultLng: number;
    mapBtn = false;
    markersLocation = ([] = []);
    mindate = new Date();
    endMinDate = new Date();
    formatStartDate: string;
    formatEndDate: string;
    @Output() getServicePlan = new EventEmitter();
    @Input() servicePlanActivityiId: string;
    @Output() dateChange: EventEmitter<MatDatepickerInputEvent<Date>>;
    @Input() isAgencyProvided$ = new Subject<boolean>();
    occurence: AsOccurence[] = [];
    weekstime = ([] = []);
    token: AppUser;
    providertypekey: any;
    selectedServiceType: any;
    selectedServiceSubType: any;
    securityusersid = '';
    county: string;
    constructor(
        private _httpService: CommonHttpService,
        private _alertService: AlertService,
        private _servicePlan: GenericService<ServicePlanConfig>,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _authService: AuthService,
        private _dataStoreService: DataStoreService
    ) {}

    ngOnInit() {
        console.log('this.route...', this.route);
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.token = this._authService.getCurrentUser();
        this.servicePlanForm = this._formBuilder.group({
            jurisdiction: ['', Validators.required],
            team: [null],
            zipcode: [null],
            serviceType: [null],
            providertype: [null],
            providersubtype: [null],
            paymentType: [null],
            vendor: [null],
            agency: [null],
            childCharacteristic: [null],
            providertypekey: ['agency'],
            startdate: [''],
            enddate: [''],
            reason: [''],
            rate: [0],
            noofweeks: [''],
            noofhours: [''],
            familyworkername: ['']
        });
        this.addServicePlanForm = this._formBuilder.group({
            startdate: [null],
            enddate: [null],
            reason: [''],
            rate: [''],
            isrepeats: [false],
            repeats: [false],
            exemptions: [''],
            noofoccurence: [''],
            noofweeks: [''],
            noofhours: [''],
            occurences: this._formBuilder.array([])
        });
        this.familyServiceGroup = this._formBuilder.group({
            countyid: ['', Validators.required],
            servicecategorytypekey: ['', Validators.required],
            categorysubtypekey: ['', Validators.required],
            providercontracttypekey: [null],
            careworkername: [''],
            startdate: [null],
            enddate: [null],
            noofhours: [null],
            noofweeks: [null],
            rate: [null],
            reason: ['']
        });
        this.addServicePlanForm.get('repeats').disable();
        this.addServicePlanForm.get('noofoccurence').disable();
        this.getDefaults();
        this.getCountyDropdown();
        this.getCategoryType();
        this.isrepeatschecked = false;
        this._dataStoreService.currentStore.subscribe((item) => {
            if (item['countyid']) {
               this.county = item['countyid'];
            }
        });
        this.isAgencyProvided$.subscribe((item) => {
            if (item) {
                this.servicePlanForm.controls['providertypekey'].patchValue('agency');
                this.onCheckProvider('agency');
                this.familyServiceGroup.controls['countyid'].patchValue(this.county);
            }
        });
        this.weekDays = [
            {
                day: 'SUN',
                dayValue: 'SUN'
            },
            {
                day: 'MON',
                dayValue: 'MON'
            },
            {
                day: 'TUE',
                dayValue: 'TUE'
            },
            {
                day: 'WED',
                dayValue: 'WED'
            },
            {
                day: 'THU',
                dayValue: 'THU'
            },
            {
                day: 'FRI',
                dayValue: 'FRI'
            },
            {
                day: 'SAT',
                dayValue: 'SAT'
            }
        ];
        this.servicePlanForm.patchValue({
            providertypekey: 'agency',
            rate: 0
        });
        this.onCheckProvider(this.servicePlanForm.get('providertypekey').value);
        // this.servicePlanForm.get('rate').disable();
    }

    addFormGroup(value) {
        const timingFormArray = <FormArray>this.addServicePlanForm.controls['occurences'];
        timingFormArray.controls = [];
        for (let i = 1; i <= value; i++) {
            timingFormArray.push(this.initFormGroup());
        }
    }

    initFormGroup(): FormGroup {
        return this._formBuilder.group({
            starttime: [null],
            endtime: [null]
        });
    }

    disableRepeatsOn(event) {
        if (event === true) {
            this.addServicePlanForm.get('repeats').enable();
            this.addServicePlanForm.get('noofoccurence').enable();
            this.isRepeats = 1;
            this.isrepeatschecked = true;
        } else {
            this.addServicePlanForm.get('repeats').disable();
            this.addServicePlanForm.get('noofoccurence').disable();
            this.isRepeats = 0;
            this.isrepeatschecked = false;
            this.addServicePlanForm.patchValue({
                repeats: false
            });
        }
    }

    clearItem() {
        this.onCheckProvider(this.servicePlanForm.get('providertypekey').value);
        this.SearchPlan$ = Observable.empty();
        this.nextDisabled = true;
        this.servicePlanForm.reset();
        this.servicePlanForm.patchValue({ serviceType: '' });
        this.mapBtn = false;
        this.markersLocation = [];
        this.timesInDayHide = false;
        this.addServicePlanForm.reset();
        this.familyServiceGroup.reset();
        this.addServicePlanForm.setControl('occurences', new FormArray([]));
        this.isrepeatschecked = false;
        this.addServicePlanForm.patchValue({
            isrepeats: false
        });
        this.securityusersid = '';
    }

    startDateNotGreater(startDate) {
        this.endMinDate = new Date(startDate);
        this.addServicePlanForm.patchValue({
            enddate: null
        });
    }

    onCheckProvider(provider) {
        this.providertypekey = provider;
        if (this.providertypekey === 'vendor') {
            this.addServicePlanForm.get('rate').disable();
        } else if (this.providertypekey === 'agency') {
            this.servicePlanForm.get('rate').disable();
        }
    }
    startDate(date) {
        this.formatStartDate = moment(date.value).format();
    }
    endDate(date) {
        this.formatEndDate = moment(date.value).format();
    }
    private getDefaults() {
        const source = Observable.forkJoin([this.getCaseType(), this.getPaymentType()])
            .map(item => {
                return {
                    caseTypes: item[0].map(itm => {
                        return new DropdownModel({
                            text: itm.servicetypedescription,
                            value: itm.servicetypekey
                        });
                    }),
                    paymentTypes: item[1].map(itm => {
                        return new DropdownModel({
                            text: itm.providercontracttypename,
                            value: itm.providercontracttypekey
                        });
                    })
                };
            })
            .share();
        this.caseTypes$ = source.pluck('caseTypes');
        this.paymentTypes$ = source.pluck('paymentTypes');
    }

    getCountyDropdown() {
        this.servicePlanCountyValuesDropdownItems$ = this._httpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmCountyListUrl + '?filter'
            )
            .map(result => {
                return result.map(
                    res =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }
    daysChange(event) {
        console.log(event.source.value);
        // this.weekstime = [];
        if (event.checked) {
            // this.weekstime = event.source.value;
            this.weekstime.push(event.source.value);
            this.timesInDayHide = true;
        }
    }
    expertOnReset() {
        this.addServicePlanForm.get('exemptions').reset();
    }

    private getCaseType() {
        return this._httpService.getArrayList({ where: {}, nolimit: true, method: 'get' }, 'servicetype?filter');
    }
    getCategoryType() {
        this.categoryTypes$ = this._httpService
            .getArrayList(
                {
                    method: 'get',
                    where: { placementtypekey: 'Serviceplan' },
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.AsServicePlan.AsAddActivityUrl + '?filter'
            )
            .map(itms => {
                return itms.map(
                    res =>
                        new DropdownModel({
                            text: res.servicetypedescription,
                            value: res.servicetypekey
                        })
                );
            });
    }

    getCategorySubType(providertype: string) {
        this.categorySubTypes$ = this._httpService
            .getArrayList(
                {
                    method: 'get',
                    where: { tablename: 'ProviderSubCategory', teamtypekey: 'AS', parentkey: providertype },
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.AsServicePlan.AsAddSubService + '?filter'
            )
            .map(itm => {
                return itm.map(
                    res =>
                        new DropdownModel({
                            text: res.description,
                            value: res.ref_key
                        })
                );
            });
    }

    private getPaymentType() {
        return this._httpService.getArrayList({ where: {}, nolimit: true, method: 'get' }, 'providercontracttype?filter');
    }

    searchActivityService(model) {
        this.zoom = 11;
        this.defaultLat = 39.29044;
        this.defaultLng = -76.61233;
        const source = this._httpService
            .getPagedArrayList(
                {
                    where: {
                        servicetype: model.serviceType ? model.serviceType : null,
                        providertype: model.providertype ? model.providertype : null,
                        providersubtype: model.providersubtype ? model.providersubtype : null,
                        paymenttype: model.paymentType ? model.paymentType : null,
                        childcharacteristic: model.childCharacteristic ? model.childCharacteristic : null,
                        providercounty: model.jurisdiction ? model.jurisdiction : null,
                        zipcode: model.zipcode ? model.zipcode : null,
                        provider: model.vendor ? model.vendor : null,
                        pagenumber: null,
                        pagesize: null,
                        count: null,
                        sortorder: 'asc'
                    },
                    method: 'post'
                },
                'serviceplan/vendorsearch'
            )
            .map(res => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.SearchPlan$ = source.pluck('data');
        this.SearchPlan$.subscribe(response => {});
        this.SearchPlanRes$ = source.pluck('count');
    }

    searchAgency() {

        const source = this._httpService
            .getPagedArrayList(
                {
                    where: {
                        county: this.servicePlanForm.get('jurisdiction').value,
                        username: this.servicePlanForm.get('agency').value,
                        pagenumber: null,
                        pagesize: null
                    },
                    method: 'post'
                },
                'serviceplan/agencysearch'
            )
            .map(res => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.SearchAgency$ = source.pluck('data');
    }


    onServiceType(event) {
        console.log('event', event);
        this.selectedServiceType = event.value;
    }

    onServiceSubType(event) {
        console.log('event subtype...', event);
        this.selectedServiceSubType = event.value;
    }
    addFamliyService(famliyService: FamliyService) {
        famliyService.objecttypekey = 'ServiceRequest';
        famliyService.providertypekey = 'local';
        famliyService.objectid = this.id;
        famliyService.providerid = null;
        famliyService.serviceplanvendorid = null;
        famliyService.repeats = null;
        famliyService.securityusersid = null;
        famliyService.serviceplanactivityid = this.servicePlanActivityiId;
        famliyService.exemptions = [Object.assign({exemptiondate: null})];
        this._httpService.create(famliyService, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.AddServicePlanUrl).subscribe(() => {
            (<any>$('#add-services')).modal('hide');
            this._alertService.success('Service added successfully');
            this.clearItem();
            this.getServicePlan.emit();
        }, (error) => {
            this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        });
    }

    addServicePlan(servicePlanModel: ServicePlanConfig) {
        servicePlanModel.startdate = this.formatStartDate ? this.formatStartDate : servicePlanModel.startdate;
        servicePlanModel.enddate = this.formatEndDate ? this.formatEndDate : servicePlanModel.enddate;
        // this.getHoursandWeeks();
        this.servicePlanAdd = Object.assign(servicePlanModel);
        this.servicePlanAdd.familyworkername = servicePlanModel.familyworkername ? servicePlanModel.familyworkername : null;
        this.servicePlanAdd.noofweeks = servicePlanModel.noofweeks;
        this.servicePlanAdd.noofhours = servicePlanModel.noofhours;
        this.servicePlanAdd.objecttypekey = 'ServiceRequest';
        this.servicePlanAdd.objectid = this.id;
        this.servicePlanAdd.providerid = null;
        this.servicePlanAdd.serviceplanvendorid = this.agencyId;
        this.servicePlanAdd.providertypekey = this.providertypekey;
        this.servicePlanAdd.exemptions = Object.assign([{ exemptiondate: servicePlanModel.exemptions ? servicePlanModel.exemptions : null }]);
        this.servicePlanAdd.repeats = [];
        this.servicePlanAdd.occurences = this.occurence;
        // this.servicePlanAdd.rate = servicePlanModel.rate;
        if (this.providertypekey === 'vendor') {
            this.servicePlanAdd.rate = this.addServicePlanForm.get('rate').value;
        } else if (this.providertypekey === 'agency') {
            this.servicePlanAdd.rate = this.servicePlanForm.get('rate').value;
        }
        this.servicePlanAdd.isrepeats = this.isRepeats;
        this.servicePlanAdd.reason = servicePlanModel.reason;
        this.servicePlanAdd.serviceplanactivityid = this.servicePlanActivityiId;
        this.servicePlanAdd.servicecategorytypekey = this.selectedServiceType;
        this.servicePlanAdd.categorysubtypekey = this.selectedServiceSubType;
        this.servicePlanAdd.securityusersid = this.securityusersid ? this.securityusersid : null;
        console.log(this.servicePlanAdd);
        this._servicePlan.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.AddServicePlanUrl;
        this._servicePlan.create(this.servicePlanAdd).subscribe(
            res => {
                this.addServicePlanForm.reset();
                (<any>$('#add-services')).modal('hide');
                (<any>$('#select-services')).modal('hide');
                (<any>$('#select-agency')).modal('hide');
                this._alertService.success('Service added successfully');
                this.clearItem();
                this.getServicePlan.emit();
            },
            error => console.log(error)
        );
    }
    selectedProv(searchProvider) {
        this.nextDisabled = false;
        this.agencyId = searchProvider.serviceplanvendorid;
        this.addServicePlanForm.controls['rate'].setValue(parseFloat(searchProvider.rates).toFixed(2));
    }

    selectedAgency(searchAgency) {
        this.nextDisabled = false;
        this.securityusersid = searchAgency.securityusersid;
        this.servicePlanForm.patchValue({
            familyworkername: searchAgency.fullname
        });
    }

    changeTab() {
        (<any>$('#previous-click')).click();
    }
    // Map tab
    clickedMarker(label: string, index: number) {
        console.log(`clicked the marker: ${label || index}`);
    }
    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageStream$.next(this.paginationInfo.pageNumber);
    }
    listMap() {
        this.zoom = 11;
        this.defaultLat = 39.29044;
        this.defaultLng = -76.61233;
        this.SearchPlan$.subscribe(map => {
            this.markersLocation = [];
            if (map.length > 0) {
                map.forEach(res => {
                    if (res.latitude !== null && res.longitude !== null) {
                        const mapLocation = {
                            lat: +res.latitude,
                            lng: +res.longitude,
                            draggable: +true,
                            providername: res.providername !== null ? res.providername : 'NA',
                            addressline1: res.addressline1 !== null ? res.addressline1 : 'NA'
                        };
                        this.markersLocation.push(mapLocation);
                    } else {
                        const mapLocation = {
                            lat: +this.defaultLat,
                            lng: +this.defaultLng,
                            draggable: +true,
                            providername: res.providername !== null ? res.providername : 'NA',
                            addressline1: res.addressline1 !== null ? res.addressline1 : 'NA'
                        };
                        this.markersLocation.push(mapLocation);
                    }
                });
                if (!this.markersLocation[0].lat !== null && !this.markersLocation[0].lng !== null) {
                    this.defaultLat = this.markersLocation[0].lat;
                    this.defaultLng = this.markersLocation[0].lng;
                } else {
                    this.defaultLat = 39.29044;
                    this.defaultLng = -76.61233;
                }
                (<any>$('#iframe-popup')).modal('show');
            }
        });
    }
    mapClose() {
        this.markersLocation = [];
    }

    getHoursandWeeks() {
        this.occurence.push({
            noofweeks: this.addServicePlanForm.get('noofweeks').value,
            noofhours: this.addServicePlanForm.get('noofhours').value
        });
        this.occurence.push({
            noofweeks: this.servicePlanForm.get('noofweeks').value,
            noofhours: this.servicePlanForm.get('noofhours').value
        });
    }
    addServicePlanNoDays() {
        const weeks = this.calculateNoDays(this.addServicePlanForm.get('startdate').value, this.addServicePlanForm.get('enddate').value);
        this.addServicePlanForm.controls['noofweeks'].setValue(weeks);
    }
    servicePlanNoDays() {
        const weeks = this.calculateNoDays(this.servicePlanForm.get('startdate').value, this.servicePlanForm.get('enddate').value);
        this.servicePlanForm.controls['noofweeks'].setValue(weeks);
    }
    familyServicePlanNoDays() {
        const weeks = this.calculateNoDays(this.familyServiceGroup.get('startdate').value, this.familyServiceGroup.get('enddate').value);
        this.familyServiceGroup.controls['noofweeks'].setValue(weeks);
    }
    calculateNoDays(startdate, enddate) {
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const value = '';
        if (startdate && enddate) {
            const firstDate = new Date(startdate);
            const secondDate = new Date(enddate);
            const diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
            const noofweeks = diffDays / 7;
            const int_part = Math.trunc(noofweeks); // returns 3
            const float_part = Number((noofweeks - int_part).toFixed(2)); // return 0.2
            if (float_part < 0.5) {
                return int_part;
            } else {
                return int_part + 1;
            }

        } else {
            return value;
        }

    }
    /* changeTime(item, i) {
      const getOccurences = <FormArray>(
          this.addServicePlanForm.controls['occurences']
      );
      const format = 'hh:mm';
      this.occurence = getOccurences.value;
      this.occurence.map((items, index) => {
          if ((items.starttime && items.endtime) && index !== i) {
              const startTime = moment(items.starttime, format);
              const endTime = moment(items.endtime, format);
              const startTimeDiff = moment(item.value.starttime, format), beforeTime = startTime, afterTime = endTime;
              const endTimeDiff = moment(item.value.endtime, format), beforeStartTime = startTime, afterEndTime = endTime;

              if (startTimeDiff.isBetween(beforeTime, afterTime)) {
                  console.log('is between');
              } else {
                  if (endTimeDiff.isBetween(beforeStartTime, afterEndTime)) {
                      console.log('is between inner');
                  }
              }
          }
      });
  }*/
}
