import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsServicePlanAddEditActivityComponent } from './as-service-plan-add-edit-activity.component';

describe('AsServicePlanAddEditActivityComponent', () => {
  let component: AsServicePlanAddEditActivityComponent;
  let fixture: ComponentFixture<AsServicePlanAddEditActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsServicePlanAddEditActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsServicePlanAddEditActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
