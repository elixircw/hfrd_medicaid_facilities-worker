import { InvolvedPerson } from './../../../_entities/caseworker.data.model';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AddServicePlanActivity, ActivityStatus, InvolvedPersonConfig, AsServicePlanActivity, AsAddActivity } from '../_entities/as-service-plan.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, GenericService, ValidationService, DataStoreService } from '../../../../../@core/services';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { AuthService } from '../../../../../@core/services/auth.service';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import * as moment from 'moment';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

declare var $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'as-service-plan-add-edit-activity',
  templateUrl: './as-service-plan-add-edit-activity.component.html',
  styleUrls: ['./as-service-plan-add-edit-activity.component.scss']
})
export class AsServicePlanAddEditActivityComponent implements OnInit {

  id: string;
  activityCategoryTypes$: Observable<DropdownModel>;
  activityCategorySubTypes$: Observable<DropdownModel[]>;
  activityGoal$: Observable<AsAddActivity>;
  activityStatus$: Observable<ActivityStatus>;
  activityPersonInvloved$: Observable<DropdownModel[]>;
  activityPersonResponsible$: Observable<DropdownModel[]>;
  involvedPerson$: Observable<InvolvedPerson[]>;
  otherPersonList$: Observable<InvolvedPerson[]>;
  addServciePlanActivity: AddServicePlanActivity;
  activityStrategy: FormGroup;
  activityAdd: AsAddActivity;
  serviceTypeId: string;
  servieTypeKey: string;
  completiondate: string;
  token: AppUser;
  mindate = new Date();
  @Output()
  acitityServie = new EventEmitter();
  @Input()
  editASServicePlanOutputSubject$ = new Subject<AsAddActivity>();
  @Input()
  activityPlanActivityiId: string;
  activityPlanActivityi: string;
  updateServicePlanButton: boolean;
  SaveServicePlanButton: boolean;
  serviceTypeKey$: Observable<AsServicePlanActivity[]>;
  involvedPersons: InvolvedPerson[];
  serviceKey: any;
  roledescription: string[] = [];
  personNames: string[] = [];
  isCaseWorkerSelect: boolean;
  caseWorkerName: string;
  constructor(
      private _httpService: CommonHttpService,
      private _formBuilder: FormBuilder,
      private _alertService: AlertService,
      private _ativityService: GenericService<AsAddActivity>,
      private route: ActivatedRoute,
      private _authService: AuthService,
    //   private _addInvolvedPerson: GenericService<InvolvedPersonConfig>,
    private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService
    ) {}

  ngOnInit() {
      // this.updateServicePlanButton = false;
      this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
      this.token = this._authService.getCurrentUser();
      this.activityStrategy = this._formBuilder.group({
          activity: ['', Validators.required],
          servicetypekey: [null],
          activitysubtypekey: [null],
          personsinvolvedid: ['', Validators.required],
          responsiblepersonid: ['', Validators.required],
          reevaluationdate: [null],
          completiondate: [null],
          serviceplangoalid: [null],
          serviceplanactivitystatustypekey: ['', Validators.required],
          otherresponsibleperson:  [''],
          progressnote: [''],
          plannedstartdate: [null],
          plannedenddate: [null],
          initialcondition: ['']
      });
      this.getDefaults();
      this.getInvolvedPerson();
      this.editASServicePlanOutputSubject$.subscribe((data: AsAddActivity) => {
          if (data && data.isAddEdit === 'Edit') {
              console.log('data...', data);
              this.activityStrategy.patchValue(data);
              this.activityPlanActivityi = data.isAddEdit;
              this.updateServicePlanButton = true;
              this.onSelectServiceType(data.servicetypekey);
          } else {
              this.updateServicePlanButton = false;
          }
      });
  }
  responsiblePersonChange(personid: string) {
      if (this.token.user.securityusersid === personid) {
          this.isCaseWorkerSelect = true;
          this.caseWorkerName = this.token.user.username;
      } else {
          this.isCaseWorkerSelect = false;
          this.caseWorkerName = null;
      }
  }
  clearItem() {
      this.editASServicePlanOutputSubject$ = Object.assign({
          activity: '',
          servicetypekey: null,
          activitysubtypekey: null,
          personsinvolvedid: '',
          responsiblepersonid: '',
          reevaluationdate: null,
          completiondate: null,
          serviceplangoalid: null,
          serviceplanactivitystatustypekey: null
      });
      this.activityStrategy.reset();
      this.activityStrategy.patchValue({ serviceType: '' });
  }
  saveActivity(ativity: AsAddActivity) {
      if (ativity.responsiblepersonid === 'Other' && this.activityStrategy.controls['otherresponsibleperson'].value === '') {
          this._alertService.warn('Please fill the other responsible person');
      } else {
        ativity.completiondate = this.completiondate ? this.completiondate : ativity.completiondate;
        ativity.responsibleperson = this.isCaseWorkerSelect ? this.caseWorkerName : this.personNames[ativity.responsiblepersonid];
        ativity.personinvolved = this.personNames[ativity.personsinvolvedid];
        this.activityAdd = Object.assign(ativity);
        this.activityAdd.servicetypekey = ativity.servicetypekey;
        this.activityAdd.activitysubtypekey = ativity.activitysubtypekey;
        this.activityAdd.objecttypekey = 'ServiceRequest';
        this.activityAdd.objectid = this.id;
        // this.activityAdd.serviceplangoalid = ativity
        this._ativityService.endpointUrl = 'serviceplan/activity/';
        this._ativityService.create(this.activityAdd).subscribe(
            (res) => {
                // this.activityStrategy.reset();
                this.clearItem();
                (<any>$('#add-newactivity')).modal('hide');
                this._alertService.success('Activity added successfully');
                this.acitityServie.emit();
            },
            (error) => console.log(error)
        );
      }
  }

  UpdateActivity(ativity: AsAddActivity) {
    if (ativity.responsiblepersonid === 'Other' && this.activityStrategy.controls['otherresponsibleperson'].value === '') {
        this._alertService.warn('Please fill the other responsible person');
    } else {
      ativity.completiondate = this.completiondate ? this.completiondate : ativity.completiondate;
      ativity.responsibleperson = this.isCaseWorkerSelect ? this.caseWorkerName : this.personNames[ativity.responsiblepersonid];
      ativity.personinvolved = this.personNames[ativity.personsinvolvedid];
      this.activityAdd = Object.assign(ativity);
      this.activityAdd.objecttypekey = 'ServiceRequest';
      this.activityAdd.objectid = this.id;
      // this.activityAdd.activitysubtypekey = this.activityCategorySubTypes$.subscribe
      this._httpService.patch(this.activityPlanActivityiId, this.activityAdd, CaseWorkerUrlConfig.EndPoint.DSDSAction.ServicePlan.DelectServicePlanUrl).subscribe(
          (res) => {
              this.clearItem();
              (<any>$('#add-newactivity')).modal('hide');
              this._alertService.success('Activity Update successfully');
              this.acitityServie.emit();
          },
          (err) => {}
      );
    }
  }

  private getDefaults() {
      const source = Observable.forkJoin([this.getCategoryType(), this.getActivityGoal(), this.getStatus()])
          .map((item) => {
              return {
                  activityCategoryTypes: item[0].map((itm) => {
                      return new DropdownModel({
                          text: itm.servicetypedescription,
                          value: itm.servicetypekey
                      });
                  }),
                  activityGoal: item[1].map((itm) => {
                      return new DropdownModel({
                          text: itm.goaldescription,
                          value: itm.serviceplangoalid
                      });
                  }),
                  activityStatus: item[2].map((itm) => {
                      return new DropdownModel({
                          text: itm.description,
                          value: itm.serviceplanactivitystatustypekey
                      });
                  })
              };
          })
          .share();
      this.activityCategoryTypes$ = source.pluck('activityCategoryTypes');
      this.activityStatus$ = source.pluck('activityStatus');
      this.activityGoal$ = source.pluck('activityGoal');
  }
  private getCategoryType() {
      return this._httpService.getArrayList( { where: {placementtypekey: 'Serviceplan'}, nolimit: true, method: 'get' },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.AsServicePlan.AsAddActivityUrl + '?filter');

  }

  onSelectServiceType(items) {
      this.serviceKey = items;
      const source = Observable.forkJoin([this.getCategorySubType(this.serviceKey)])
      .map((item) => {
          return {
              activityCategorySubTypes: item[0].map((itm) => {
        return new DropdownModel({
            text: itm.description,
            value: itm.ref_key
        });
    })
    };
}).share();
    this.activityCategorySubTypes$ = source.pluck('activityCategorySubTypes');
  }

  private  getCategorySubType(serviceKey) {
    console.log('this.serviceKey...', serviceKey);
      return this._httpService.getArrayList({
        where: {tablename: 'ActivitySubtype', teamtypekey: 'AS', parentkey: serviceKey },
        method: 'get'},
        CaseWorkerUrlConfig.EndPoint.DSDSAction.AsServicePlan.AsAddSubService + '?filter');
  }
  completionDate(date: MatDatepickerInputEvent<Date>) {
    this.completiondate = moment(date.value).format();
  }
  private getActivityGoal() {
    this.serviceTypeKey$ = this._httpService.getArrayList(
        { method: 'get', where: { objectid: this.id } }, CaseWorkerUrlConfig.EndPoint.DSDSAction.AsServicePlan.GoalUrl + '?filter').share();
    this.serviceTypeKey$.subscribe((response) => {
      console.log('response... goal', response);
    });
      return this._httpService.getArrayList({ method: 'get', where: { objectid: this.id } }, CaseWorkerUrlConfig.EndPoint.DSDSAction.AsServicePlan.GoalUrl + '?filter');
  }
  private getStatus() {
      return this._httpService.getArrayList({ nolimit: true, method: 'get' }, 'serviceplan/activitystatustype?filter');
  }
 private getInvolvedPerson() {
    const source = this._commonHttpService
        .getPagedArrayList(
            new PaginationRequest({
                page: 1,
                limit: 20,
                method: 'get',
                where: { intakeserviceid: this.id }
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                .PersonList + '?filter'
        ).map((item) => {
            item['data'].map((res) => {
                this.personNames[res.personid] = res.firstname + ' ' + res.lastname;
                if (res.roles && res.roles.length) {
                    res.roles.map((role) => {
                        this.roledescription[role.intakeservicerequestpersontypekey] = role.typedescription;
                    });
                }
                return res;
            });
            return {
                RA: item['data'],
                Other: item['data'].filter((res) => res.rolename !== 'RA'),
            };
        })
        .share();
        this.involvedPerson$  = source.pluck('RA');
        this.otherPersonList$ = source.pluck('Other');
    }
}
