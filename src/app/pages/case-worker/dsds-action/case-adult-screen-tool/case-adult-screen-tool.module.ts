import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaseAdultScreenToolRoutingModule } from './case-adult-screen-tool-routing.module';
import { CaseAdultScreenToolComponent } from './case-adult-screen-tool.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    CaseAdultScreenToolRoutingModule,
    FormMaterialModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    CaseAdultScreenToolComponent
  ]
})
export class CaseAdultScreenToolModule { }
