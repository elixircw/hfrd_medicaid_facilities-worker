import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewAssessmentComponent } from './view-assessment.component';
import { ViewAssessmentRoutingModule } from './view-assessment-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { PaginationModule } from 'ngx-bootstrap';
import { AssessmentCansFComponent } from '../assessment-cans-f/assessment-cans-f.component';
import { AssessmentMfiraComponent } from '../assessment-mfira/assessment-mfira.component';
import { AssessmentCansOutComponent } from '../assessment-cans-out/assessment-cans-out.component';
import { AssessmentAodComponent } from '../assessment-aod/assessment-aod.component';
@NgModule({
  imports: [
    CommonModule,
    ViewAssessmentRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    PaginationModule
  ],
  declarations: [
    ViewAssessmentComponent,
    AssessmentAodComponent,
    AssessmentCansFComponent,
    AssessmentMfiraComponent,
    AssessmentCansOutComponent
  ],
  providers: []
})
export class ViewAssessmentModule { }
