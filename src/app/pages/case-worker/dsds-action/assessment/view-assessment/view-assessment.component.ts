import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, CommonHttpService, AlertService, DataStoreService, CommonDropdownsService } from '../../../../../@core/services';
import { HttpService } from '../../../../../@core/services/http.service';
import { AssessmentPreFill } from '../assessment-prefill';
import { InvolvedPerson, YouthInvolvedPersons } from '../../involved-persons/_entities/involvedperson.data.model';
import { DSDSActionSummary, GetintakAssessment, RoutingInfo } from '../../../_entities/caseworker.data.model';
import { AppUser } from '../../../../../@core/entities/authDataModel';


@Component({
  selector: 'view-assessment',
  templateUrl: './view-assessment.component.html',
  styleUrls: ['./view-assessment.component.scss']
})
export class ViewAssessmentComponent implements OnInit {

  id: string;
  serviceCaseId: string;
  daNumber: string;
  involvedPersons: InvolvedPerson[];
  youthInvolvedPersons: YouthInvolvedPersons[];
  routingInfo: RoutingInfo[];
  routingSupervisors: any[];
  private token: AppUser;
  selectedAssessmentName: string;
  selectedAssessment: any;

  constructor(
    private route: ActivatedRoute,
    private _authService: AuthService,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _httpService: HttpService,
    private _router: Router,
    private _dataStoreService: DataStoreService,
    private _changeDetect: ChangeDetectorRef,
    private _commonDDService: CommonDropdownsService
  ) { 
    this.id = this._commonDDService.getStoredCaseUuid();
    this.daNumber = this._commonDDService.getStoredCaseNumber();
  }

  ngOnInit() {
    this.token = this._authService.getCurrentUser();

    this.involvedPersons = this._dataStoreService.getData('CASEWORKER_INVOLVED_PERSON');
    this.routingInfo = this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') ? this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') : [];
    this.routingSupervisors = this._dataStoreService.getData('CASEWORKER_SUPERVISORS') ? this._dataStoreService.getData('CASEWORKER_SUPERVISORS') : [];

    this.selectedAssessmentName = this._dataStoreService.getData('SELECTED_ASSESSMENT_NAME')
    this.selectedAssessment = this._dataStoreService.getData('CASEWORKER_SELECTED_ASSESSMENT');

  }


  redirectToAssessment() {
    // this.isInitialized = false;
    this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment']);
  }

  private getFormPrePopulation(formName: string, submissionData: any) {
    const prefillUtil = new AssessmentPreFill(this.involvedPersons, this.routingInfo, this.routingSupervisors , this.token, this._commonHttpService, this._dataStoreService);

    switch (formName.toUpperCase()) {
      case 'AOD FORM':
        submissionData = prefillUtil.fillAOD(submissionData);
        break;
    }
  }

  printAssessment() {

    const inputRequest = {
      //'assessmentdata': this.selectedAssessment.submissiondata, 
      //get 'PRINTDATA' from datastore
      'assessmentdata': this._dataStoreService.getData('PRINTDATA'),
      'assessmentdetails': this.selectedAssessment,
      'assessmentname': this.selectedAssessmentName,
      'isheaderrequired': false,
    };
    
    const payload = {
      method: 'post',
      count: -1,
      page: 1,
      limit: 20,
      where: inputRequest,
      documntkey: [ 'oohome' ], //CHANGE THIS , Document template table needs a key
    };
    this._commonHttpService.create(payload, 'admin/assessment/generateassessmentpdf').subscribe(
      response => {
        console.log(response, 'pdf response');
        setTimeout(() => window.open(response.data.documentpath), 2000);
    });
  }


}
