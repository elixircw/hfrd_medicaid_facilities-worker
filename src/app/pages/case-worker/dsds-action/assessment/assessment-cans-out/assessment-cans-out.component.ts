import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { AssessmentService } from '../assessment.service';
import { CommonDropdownsService, AlertService, AuthService, SessionStorageService, DataStoreService, CommonHttpService, GenericService } from '../../../../../@core/services';
import { HttpService } from '../../../../../@core/services/http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Assessments } from '../../../_entities/caseworker.data.model';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { RoutingInfo } from '../../../_entities/caseworker.data.model';
import * as moment from 'moment';

@Component({
  selector: 'assessment-cans-out',
  templateUrl: './assessment-cans-out.component.html',
  styleUrls: ['./assessment-cans-out.component.scss']
})
export class AssessmentCansOutComponent implements OnInit {
  id: any;
  daNumber: any;
  ASSESSMENT_NAME = 'CANS-OUT OF HOME PLACEMENT SERVICE';
  submissiondata: any;
  currentAssessmentId: any;
  isSupervisor: any;
  assessmentStatus: any;
  routingSupervisors: any;
  isServiceCase: any;
  roleId: AppUser;
  agency: string;
  isCW: boolean;
  cansOOHData: any;
  currentSubmissionId: any;
  faceLifeForm: FormGroup;
  childform: FormGroup;
  cultureFactorForm: FormGroup;
  traumaform: FormGroup;
  caregiverstrengthform: FormGroup;
  permanencyPlanform: FormGroup;
  transitionForm: FormGroup;
  authorizationForm: FormGroup;

  // common
  involvedPerson: any[];
  legalGuardian = [];
  reportedChild = [];
  viewCansOut = false;
  routingInfo: RoutingInfo[];
  incompleteList = [];
  isapprovaltab = false;
  adultage = 0;

  // CareGiver
  ShowCareGiverForm = false;
  caregiverFormList = [];
  caregiverMode: any;
  caregiverindex: number;
  showCareGiver: boolean;
  caregiverlegalGuardian: any[];
  caregiverlegalGuardianName = [];

  //Caregivers
  caregiversInCase = [];

  assessmentType = [
    {
      text: 'Initial (60 days from placement)',
      value: 'Initial (60 days from placement)'
    },
    {
      text: 'Re-assessment (every 6 months)',
      value: 'Re-assessment (every 6 months)'
    },
    {
      text: 'Transition/Discharge (part of transition/case close)',
      value: 'Transition/Discharge (part of transition/case close)'
    }
  ];
  isCareGiver: boolean;

  constructor(private route: ActivatedRoute,
    private _service: GenericService<Assessments>,
    private _commonService: CommonHttpService,
    public sanitizer: DomSanitizer,
    private _http: HttpService,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private storage: SessionStorageService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _commonDDService: CommonDropdownsService,
    private _assessmentService: AssessmentService,
    private _formBuilder: FormBuilder
  ) {
    this.id = this._commonDDService.getStoredCaseUuid();
    this.daNumber = this._commonDDService.getStoredCaseNumber();
  }

  ngOnInit() {
    this.inintailizeForm();
    this.initializeCaregiverForm();
    this.isServiceCase = this.storage.getItem('ISSERVICECASE');
    this._assessmentService.getservicecase();
    this.roleId = this._authService.getCurrentUser();
    this.agency = this._authService.getAgencyName();
    this.isSupervisor = (this.roleId.role.name === 'apcs') ? true : false;
    if (this.agency === 'CW') {
      this.isCW = true;
    } else {
      this.isCW = false;
    }
    this.routingInfo = this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') ? this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') : [];
    this.routingSupervisors = this._dataStoreService.getData('CASEWORKER_SUPERVISORS') ? this._dataStoreService.getData('CASEWORKER_SUPERVISORS') : [];
    this.cansOOHData = this._dataStoreService.getData('CASEWORKER_SELECTED_ASSESSMENT');
    // In case of working with an existing assesment submission
    this.currentAssessmentId = this.cansOOHData.assessmentid;
    this.currentSubmissionId = this.cansOOHData.submissionid;

    //Get persons in case
    this.getInvolvedPerson();
    //Get caregivers in case
    this.getCaregiversInCase();

    this.authorizationForm.get('supervisorname').disable();

    if(this.cansOOHData.mode === 'update' || this.cansOOHData.mode === 'submit') {
      this.getSubmisionData();
    } else {
      this.patchCansOutForm();
    }
    if (this.isSupervisor) {
      this.authorizationForm.get('routingsupervisors').disable();
      this.authorizationForm.get('caseworkername').disable();
      this.authorizationForm.get('caseworkercomments').disable();
    }
  }

  tabChange(tabname) {
    if (tabname === 'caregiver') {
      this.isCareGiver = true;
    } else {
      this.isCareGiver = false;
    }
    this.isapprovaltab = tabname === 'Approval';
  }

  getSubmisionData() {
    const url = `admin/assessment/getassessmentform/${this.cansOOHData.external_templateid}/submission/${this.cansOOHData.submissionid}`;
    this._commonService.getSingle({}, url).subscribe(result => {
      console.log("SIMAR THIS IS NEW->", result);
      console.log("SIMAR THIS IS ORIGINAL ->", this.cansOOHData.submissiondata);

      //Replace submissiondata from assessment table with the parsed json from transaction table
      // this.cansFData.submissiondata = result;
      this.submissiondata = result;
      this.patchCansOutForm();
    });
  }

  patchCansOutForm() {

    //Flip for non-migrated data
    if (this.cansOOHData && this.cansOOHData.submissiondata) {
      this.submissiondata = this.cansOOHData.submissiondata;
    }

    this._dataStoreService.setData('PRINTDATA', this.submissiondata);
    
    if (this.cansOOHData && (this.cansOOHData.mode === 'update' || this.cansOOHData.mode === 'submit')) {
      this.viewCansOut = false;
      if (this.submissiondata.faceLifeForm) {
        this.faceLifeForm.patchValue(this.submissiondata.faceLifeForm);
      }
      if (this.submissiondata.faceLifeForm && this.submissiondata.faceLifeForm.datearray) {
        this.submissiondata.faceLifeForm.datearray.forEach((element) => {
          this.addmeetingdate(element);
        });
      }
      if (this.submissiondata.childform) {
        this.childform.patchValue(this.submissiondata.childform);
      }
      if (this.submissiondata.cultureFactorForm) {
        this.cultureFactorForm.patchValue(this.submissiondata.cultureFactorForm);
      }
      if (this.submissiondata.traumaform) {
        this.traumaform.patchValue(this.submissiondata.traumaform);
      }
      if (this.submissiondata.caregiverstrengthform) {
        this.caregiverstrengthform.patchValue(this.submissiondata.caregiverstrengthform);
      }
      if (this.submissiondata.permanencyPlanform) {
        this.permanencyPlanform.patchValue(this.submissiondata.permanencyPlanform);
      }
      if (this.submissiondata.transitionForm) {
        this.transitionForm.patchValue(this.submissiondata.transitionForm);
      }
      if (this.submissiondata.authorizationForm) {
        this.authorizationForm.patchValue(this.submissiondata.authorizationForm);
      }
      if (this.submissiondata.careGiver) {
        this.caregiverFormList = this.submissiondata.careGiver;
      }
    } else {
      this.viewCansOut = false;
      this.prefillauthorizationFormInfo();
    }
    if (this.cansOOHData && this.cansOOHData.mode === 'submit') {
      this.viewCansOut = true;
      this.faceLifeForm.disable();
      this.childform.disable();
      this.cultureFactorForm.disable();
      this.traumaform.disable();
      this.caregiverstrengthform.disable();
      this.permanencyPlanform.disable();
      this.transitionForm.disable();
      this.authorizationForm.disable();
    }
  }

  isReadyForApproval(){
    this.incompleteList = [];
    if (!this.faceLifeForm.valid) {
      this.incompleteList.push('FACE SHEET');
    }
    if (!this.childform.valid) {
      this.incompleteList.push('CHILD AND ENVIRONMENT STRENGTHS');
    }
    if (!this.cultureFactorForm.valid) {
      this.incompleteList.push('CULTURAL FACTORS');
    }
    if (!this.traumaform.valid) {
      this.incompleteList.push('TRAUMA');
    }
    /* if (!this.caregiverstrengthform.valid) {
      this.incompleteList.push('CURRENT CAREGIVER NEEDS AND STRENGTHS');
    } */
    if (this.caregiverFormList.length > 0) {
      const formStatus = this.caregiverFormList.filter(data => !data.formStatus);
      if (formStatus && formStatus.length > 0) {
        this.incompleteList.push('CURRENT CAREGIVER NEEDS AND STRENGTHS');
      }
    } else {
      this.incompleteList.push('CURRENT CAREGIVER NEEDS AND STRENGTHS');
    }
    if (!this.permanencyPlanform.valid) {
      this.incompleteList.push('PERMANENCY PLAN');
    }
    if (!this.transitionForm.valid && this.adultage >= 14) {
      this.incompleteList.push('TRANSITION');
    }
    if (!this.authorizationForm.valid) {
      this.incompleteList.push('APPROVAL');
    }
    if (this.incompleteList.length == 0) {
      this.submitForApproval();
    } else {
      (<any>$('#incomplete-items')).modal('show');
    }
  }

  prefillauthorizationFormInfo() {
    this.authorizationForm.patchValue({
      supervisorname: this.routingInfo[0].fromusername,
      caseworkername: this.routingInfo[0].tousername
    })
    this.faceLifeForm.patchValue({
      caseworkername: this.roleId.user.userprofile.fullname
    });
  }

  patchlegalGaurdian(model) {
    if (this.cansOOHData && (this.cansOOHData.mode === 'update' || this.cansOOHData.mode === 'submit')) {      
    } else {
      this.caregiverstrengthform.patchValue({
        firstName: model.firstname,
        lastName: model.lastname,
        relationship: model.relationship
      });
  
      this.permanencyPlanform.patchValue({
        firstName: model.firstname,
        lastName: model.lastname,
        relationship: model.relationship,
        firstNameII: model.firstname,
        lastNameII: model.lastname,
        relationshipII: model.relationship
      });
    }
  }

  /**
   * Get caregivers in the case
   */
  getCaregiversInCase() {
    this.caregiversInCase = [];
    this._commonService.getArrayList(
            new PaginationRequest({
                nolimit: true,
                method: 'get',
                where: {
                    personid: this.id
                }
            }),
            'Actorrelationships/getallcaregiversincase'+ '?filter'
        )
        .subscribe(response => {
        if (response && Array.isArray(response) && response.length && response[0].getallcaregiversincase) {
          this.caregiversInCase = response[0].getallcaregiversincase;
          }
      });
  }

  checkifcaregiver(value) {
    let cgflag = false;

    console.log("caregiversInCase ", this.caregiversInCase)

    const modal = this.involvedPerson.find(data => data.intakeservicerequestactorid === value);

    console.log("involvedPerson", this.involvedPerson);

    console.log(modal);
    if(this.caregiversInCase) {
      this.caregiversInCase.forEach(element => {
          if(element.personid == modal.personid) {
            cgflag = true;
          }
      });

      if (cgflag === false) {
        (<any>$('#not-caregiver-alert')).modal('show');
      }
    }
  }

  getInvolvedPerson() {
    this.legalGuardian = [];
    let getpersonlistreq = {};
    if (this.isCW && this.isServiceCase) {
      getpersonlistreq = { objectid: this.id, objecttypekey: 'servicecase' };
    } else {
      getpersonlistreq = { intakeserviceid: this.id };
    }
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        page: 1,
        limit: 20,
        method: 'get',
        where: getpersonlistreq
      }),
      'People/getpersondetailcw?filter'
    ).subscribe(response => {
      if (response && response.data && response.data.length) {
        this.involvedPerson = response.data;
        let  headofHouseHold = null;
        this.involvedPerson.map(item => {
          if(item.isheadofhousehold) {
            headofHouseHold = item;
          }
          const legalGurdian = item.roles.filter(roleid => roleid.intakeservicerequestpersontypekey === 'LG');
          const child = item.roles.filter(roleid => roleid.intakeservicerequestpersontypekey === 'CHILD' ||
            roleid.intakeservicerequestpersontypekey === 'OTHERCHILD');
            if (child && child.length > 0) {
              this.reportedChild.push(item);
              this.calculateAge(item.dob);
            }
          if (legalGurdian && legalGurdian.length) {
            this.legalGuardian.push(item);
            this.patchlegalGaurdian(this.legalGuardian[0]);
          }
        });
      }
    });
  }

  calculateAge(dob) {
    if (dob) {
      const age = { years: 0 };
      age.years = (moment().diff(dob, 'years', false)) ? moment().diff(dob, 'years', false) : 0;
      this.adultage = age.years;
    }
  }

  getCareGiverFormPrintData() {
    this.caregiverFormList.forEach(data => {
      data['caregivername'] = this.getCGName(data['caregiverlist']);
    })
    return this.caregiverFormList;
  }
  
  getCGName(value) {
    if(this.involvedPerson){
      const modal = this.involvedPerson.find(data => data.intakeservicerequestactorid === value);
      return modal ? modal.fullname : '' ;
    }
    return '';
    
  }
  
  getCansoutReq() {
    return {
      faceLifeForm: this.faceLifeForm.getRawValue(),
      childform: this.childform.getRawValue(),
      cultureFactorForm: this.cultureFactorForm.getRawValue(),
      traumaform: this.traumaform.getRawValue(),
      caregiverstrengthform: this.caregiverstrengthform.getRawValue(),
      careGiver: this.getCareGiverFormPrintData(),
      permanencyPlanform: this.permanencyPlanform.getRawValue(),
      transitionForm: this.transitionForm.getRawValue(),
      authorizationForm: this.authorizationForm.getRawValue(),
      supervisorname: this.authorizationForm.get('supervisorname').value,
      routingsupervisors: this.routingSupervisors,
      currentSubmissionId: this.currentSubmissionId,
      assessmentStaus: this.assessmentStatus
    }
  }

  saveForm() {
    const cansaoutData = this.getCansoutReq();
    this._assessmentService.saveAssessment(this.ASSESSMENT_NAME, this.currentAssessmentId, cansaoutData)
      .subscribe(
        (response) => {
          this._alertService.success('Form saved');
          if (response.data) {
            this.currentAssessmentId = response.data.assessmentid;
            this.currentSubmissionId = response.data.submissionid;
          } else {
            this.currentAssessmentId = response.assessmentid;
            this.currentSubmissionId = response.data.submissionid;
          }
        },
        (error) => {
          this._alertService.error('Unable to save.');
        }

      );
  }

  submitForApproval() {
      if (this.isSupervisor) {
       this.assessmentStatus = this.authorizationForm.get('assessmentstatus').value;
      } else {
       this.assessmentStatus = 'Review';
      }
       const submissionData = this.getCansoutReq();
       //console.log("SUBMISSION DATA CANSOUT",submissionData);
       this._assessmentService.saveAssessment(this.ASSESSMENT_NAME, this.currentAssessmentId, submissionData)
         .subscribe(
           (response) => {
            this._alertService.success(`${this.assessmentStatus === 'Review' ? 'Approval' : this.assessmentStatus} Submitted Successfully`);
            if (this.assessmentStatus === 'Accepted') {
              this.saveAssessmentStrengthNeeds('cansOutOfHomePlacementService');
            }
            setTimeout(() => {
               this._router.navigate(['../'], {relativeTo : this.route});
             }, 1000);
           },
           (error) => {
             this._alertService.error('Unable to submit for approval.');
           }
         );
   }

   saveAssessmentStrengthNeeds(templatename) {
    // How do we handle cans-outofhome?
    const payload = {
      'objectid': this.id,
      'templatename': templatename,
      'status': 'accepted'
    };
    this._commonService.create(payload, 'serviceplan/saveAssessmentStrengthNeeds').subscribe(
      response => {
        console.log('Strengths and Needs added successfully.');
      });
  }

   changeSupervisor(userid) {
    const user = this.routingSupervisors.find(item => item.userid === userid);
    if (user.username) {
      this.authorizationForm.patchValue({
        supervisorname: user.username
      });
    }
  }

  selectAge(value){    
    const user = this.reportedChild.find(item => item.intakeservicerequestactorid === value);
    if (user.age) {
      this.faceLifeForm.patchValue({
        age: user.age
      });
    }
  }

  addmeetingdate(modal) {
    const control = <FormArray>this.faceLifeForm.controls['datearray'];
    control.push(this.createFormGroup(modal));
  }
  deletemeetingdate(index: number) {
    const control = <FormArray>this.faceLifeForm.controls['datearray'];
    control.removeAt(index);
  }
  private createFormGroup(modal) {
      return this._formBuilder.group({
        meetingdate: modal.meetingdate ? modal.meetingdate : null
      });
  }
  inintailizeForm() {
    this.faceLifeForm = this._formBuilder.group({
      assessmenttype: null,
      childname: null,
      age: null,
      caseworkername: null,
      family_rating: null,
      family_strength: null,
      family_comments: null,
      livingsitu_rating: null,
      livingsitu_strength: null,
      livingsitu_comments: null,
      socialpeer_rating: null,
      socialpeer_strength: null,
      socialpeer_comments: null,
      socialadult_rating: null,
      socialadult_strength: null,
      socialadult_comments: null,
      medphy_rating: null,
      medphy_strength: null,
      medphy_comments: null,
      enuresis_rating: null,
      enuresis_strength: null,
      enuresis_comments: null,
      sleeping_rating: null,
      sleeping_strength: null,
      sleeping_comments: null,
      iq_rating: null,
      iq_strength: null,
      iq_comments: null,
      autism_rating: null,
      autism_strength: null,
      autism_comments: null,
      recreation_rating: null,
      recreation_strength: null,
      recreation_comments: null,
      legal_rating: null,
      legal_strength: null,
      legal_comments: null,
      judgement_rating: null,
      judgement_strength: null,
      judgement_comments: null,
      sexual_rating: null,
      sexual_strength: null,
      sexual_comments: null,
      jobfun_rating: null,
      jobfun_strength: null,
      jobfun_comments: null,
      schoolatd_rating: null,
      schoolatd_strength: null,
      schoolatd_comments: null,
      schoolacheive_rating: null,
      schoolacheive_strength: null,
      schoolacheive_comments: null,
      schoolbehave_rating: null,
      schoolbehave_strength: null,
      schoolbehave_comments: null
    });

    this.faceLifeForm.setControl('datearray', this._formBuilder.array([]));

    this.childform = this._formBuilder.group({
      familyenv_rating: null,
      familyenv_strength: null,
      familyenv_comments: null,
      eduenv_rating: null,
      eduenv_strength: null,
      eduenv_comments: null,
      religious_rating: null,
      religious_strength: null,
      religious_comments: null,
      community_rating: null,
      community_strength: null,
      community_comments: null,
      relperformance_rating: null,
      relperformance_strength: null,
      relperformance_comments: null,
      natural_rating: null,
      natural_strength: null,
      natural_comments: null,
      interperson_rating: null,
      interperson_strength: null,
      interperson_comments: null,
      interpersonnoncare_rating: null,
      interpersonnoncare_strength: null,
      interpersonnoncare_comments: null,
      optimism_rating: null,
      optimism_strength: null,
      optimism_comments: null,
      talent_rating: null,
      talent_strength: null,
      talent_comments: null,
      culture_rating: null,
      culture_strength: null,
      culture_comments: null,
      careplanning_rating: null,
      careplanning_strength: null,
      careplanning_comments: null,
      resiliency_rating: null,
      resiliency_strength: null,
      resiliency_comments: null,
      vocational_rating: null,
      vocational_strength: null,
      vocational_comments: null,
      resourcefulness_rating: null,
      resourcefulness_strength: null,
      resourcefulness_comments: null,
      psychosis_rating: null,
      psychosis_strength: null,
      psychosis_comments: null,
      impulse_rating: null,
      impulse_strength: null,
      impulse_comments: null,
      mood_rating: null,
      mood_strength: null,
      mood_comments: null,
      anxiety_rating: null,
      anxiety_strength: null,
      anxiety_comments: null,
      opposition_rating: null,
      opposition_strength: null,
      opposition_comments: null,
      conduct_rating: null,
      conduct_strength: null,
      conduct_comments: null,
      substanceabuse_rating: null,
      substanceabuse_strength: null,
      substanceabuse_comments: null,
      eating_rating: null,
      eating_strength: null,
      eating_comments: null,
      angermang_rating: null,
      angermang_strength: null,
      angermang_comments: null,
      attchment_rating: null,
      attchment_strength: null,
      attchment_comments: null,
      adjtrauma_rating: null,
      adjtrauma_strength: null,
      adjtrauma_comments: null,
      suicide_rating: null,
      suicide_strength: null,
      suicide_comments: null,
      selfinjury_rating: null,
      selfinjury_strength: null,
      selfinjury_comments: null,
      reckless_rating: null,
      reckless_strength: null,
      reckless_comments: null,
      dangertoother_rating: null,
      dangertoother_strength: null,
      dangertoother_comments: null,
      sexaggress_rating: null,
      sexaggress_strength: null,
      sexaggress_comments: null,
      sexreact_rating: null,
      sexreact_strength: null,
      sexreact_comments: null,
      runaway_rating: null,
      runaway_strength: null,
      runaway_comments: null,
      deliquent_rating: null,
      deliquent_strength: null,
      deliquent_comments: null,
      fire_rating: null,
      fire_strength: null,
      fire_comments: null,
      intentional_rating: null,
      intentional_strength: null,
      intentional_comments: null,
      bullying_rating: null,
      bullying_strength: null,
      bullying_comments: null,
      exploited_rating: null,
      exploited_strength: null,
      exploited_comments: null
    });

    this.cultureFactorForm = this._formBuilder.group({
      language_rating: null,
      language_strength: null,
      language_comments: null,
      ritual_rating: null,
      ritual_strength: null,
      ritual_comments: null,
      genderidentity_rating: null,
      genderidentity_strength: null,
      genderidentity_comments: null,
      culturestress_rating: null,
      culturestress_strength: null,
      culturestress_comments: null,
      _rating: null,
      _strength: null,
      _comments: null

    });

    this.traumaform = this._formBuilder.group({
      sexsbuse_rating: null,
      sexsbuse_strength: null,
      sexsbuse_comments: null,
      phyabuse_rating: null,
      phyabuse_strength: null,
      phyabuse_comments: null,
      emotionalabuse_rating: null,
      emotionalabuse_strength: null,
      emotionalabuse_comments: null,
      neglect_rating: null,
      neglect_strength: null,
      neglect_comments: null,
      medicaltrauma_rating: null,
      medicaltrauma_strength: null,
      medicaltrauma_comments: null,
      familywitness_rating: null,
      familywitness_strength: null,
      familywitness_comments: null,
      communityvoilance_rating: null,
      communityvoilance_strength: null,
      communityvoilance_comments: null,
      schoolvoilance_rating: null,
      schoolvoilance_strength: null,
      schoolvoilance_comments: null,
      naturaldisaster_rating: null,
      naturaldisaster_strength: null,
      naturaldisaster_comments: null,
      waraffected_rating: null,
      waraffected_strength: null,
      waraffected_comments: null,
      terroraffected_rating: null,
      terroraffected_strength: null,
      terroraffected_comments: null,
      criminalactivity_rating: null,
      criminalactivity_strength: null,
      criminalactivity_comments: null,
      disruptions_rating: null,
      disruptions_strength: null,
      disruptions_comments: null,
      traumagrief_rating: null,
      traumagrief_strength: null,
      traumagrief_comments: null,
      reexperiancing_rating: null,
      reexperiancing_strength: null,
      reexperiancing_comments: null,
      avoidance_rating: null,
      avoidance_strength: null,
      avoidance_comments: null,
      numbering_rating: null,
      numbering_strength: null,
      numbering_comments: null,
      dysregulation_rating: null,
      dysregulation_strength: null,
      dysregulation_comments: null,
      dissociation_rating: null,
      dissociation_strength: null,
      dissociation_comments: null      
    });

   
    this.permanencyPlanform = this._formBuilder.group({
      firstName: null,
      lastName: null,
      relationship: null,
      supervision_rating: null,
      supervision_strength: null,
      supervision_comments: null,
      involvement_rating: null,
      involvement_strength: null,
      involvement_comments: null,
      knowledge_rating: null,
      knowledge_strength: null,
      knowledge_comments: null,
      organization_rating: null,
      organization_strength: null,
      organization_comments: null,
      resource_rating: null,
      resource_strength: null,
      resource_comments: null,
      difficulties_rating: null,
      difficulties_strength: null,
      difficulties_comments: null,
      assebilitycare_rating: null,
      assebilitycare_strength: null,
      assebilitycare_comments: null,
      stablity_rating: null,
      stablity_strength: null,
      stablity_comments: null,
      familystress_rating: null,
      familystress_strength: null,
      familystress_comments: null,
      safety_rating: null,
      safety_strength: null,
      safety_comments: null,
      phyhealth_rating: null,
      phyhealth_strength: null,
      phyhealth_comments: null,
      mentalhealth_rating: null,
      mentalhealth_strength: null,
      mentalhealth_comments: null,
      substanceuse_rating: null,
      substanceuse_strength: null,
      substanceuse_comments: null,
      developmental_rating: null,
      developmental_strength: null,
      developmental_comments: null,
      marital_rating: null,
      marital_strength: null,
      marital_comments: null,
      traumatic_rating: null,
      traumatic_strength: null,
      traumatic_comments: null,
      criminal_rating: null,
      criminal_strength: null,
      criminal_comments: null,
      firstNameII: null,
      lastNameII: null,
      relationshipII: null,
      supervision_ratingII: null,
      supervision_strengthII: null,
      supervision_commentsII: null,
      involvement_ratingII: null,
      involvement_strengthII: null,
      involvement_commentsII: null,
      knowledge_ratingII: null,
      knowledge_strengthII: null,
      knowledge_commentsII: null,
      organization_ratingII: null,
      organization_strengthII: null,
      organization_commentsII: null,
      resource_ratingII: null,
      resource_strengthII: null,
      resource_commentsII: null,
      difficulties_ratingII: null,
      difficulties_strengthII: null,
      difficulties_commentsII: null,
      assebilitycare_ratingII: null,
      assebilitycare_strengthII: null,
      assebilitycare_commentsII: null,
      stablity_ratingII: null,
      stablity_strengthII: null,
      stablity_commentsII: null,
      familystress_ratingII: null,
      familystress_strengthII: null,
      familystress_commentsII: null,
      safety_ratingII: null,
      safety_strengthII: null,
      safety_commentsII: null,
      phyhealth_ratingII: null,
      phyhealth_strengthII: null,
      phyhealth_commentsII: null,
      mentalhealth_ratingII: null,
      mentalhealth_strengthII: null,
      mentalhealth_commentsII: null,
      substanceuse_ratingII: null,
      substanceuse_strengthII: null,
      substanceuse_commentsII: null,
      developmental_ratingII: null,
      developmental_strengthII: null,
      developmental_commentsII: null,
      marital_ratingII: null,
      marital_strengthII: null,
      marital_commentsII: null,
      traumatic_ratingII: null,
      traumatic_strengthII: null,
      traumatic_commentsII: null,
      criminal_ratingII: null,
      criminal_strengthII: null,
      criminal_commentsII: null,
      caregiver:null,
      caregiverII: null

    });

    this.transitionForm = this._formBuilder.group({
      literacy_rating: null,
      literacy_strength: null,
      literacy_comments: null,
      servlearning_rating: null,
      servlearning_strength: null,
      servlearning_comments: null,
      empoptimism_rating: null,
      empoptimism_strength: null,
      empoptimism_comments: null,
      volunterexperince_rating: null,
      volunterexperince_strength: null,
      volunterexperince_comments: null,
      knwldgeillness_rating: null,
      knwldgeillness_strength: null,
      knwldgeillness_comments: null,
      treatment_rating: null,
      treatment_strength: null,
      treatment_comments: null,
      medcompliance_rating: null,
      medcompliance_strength: null,
      medcompliance_comments: null,
      selfcare_rating: null,
      selfcare_strength: null,
      selfcare_comments: null,
      placementstab_rating: null,
      placementstab_strength: null,
      placementstab_comments: null,
      relationshippermenance_rating: null,
      relationshippermenance_strength: null,
      relationshippermenance_comments: null,

      servlearningii_rating: null,
      servlearningii_strength: null,
      servlearningii_comments: null,
      eduattainmentii_rating: null,
      eduattainmentii_strength: null,
      eduattainmentii_comments: null,
      etvii_rating: null,
      etvii_strength: null,
      etvii_comments: null,
      optimismii_rating: null,
      optimismii_strength: null,
      optimismii_comments: null,
      workexpii_rating: null,
      workexpii_strength: null,
      workexpii_comments: null,
      illnessii_rating: null,
      illnessii_strength: null,
      illnessii_comments: null,
      treatmentii_rating: null,
      treatmentii_strength: null,
      treatmentii_comments: null,
      medcomplianceii_rating: null,
      medcomplianceii_strength: null,
      medcomplianceii_comments: null,
      selfcareii_rating: null,
      selfcareii_strength: null,
      selfcareii_comments: null,
      stabilityii_rating: null,
      stabilityii_strength: null,
      stabilityii_comments: null,
      indpendentii_rating: null,
      indpendentii_strength: null,
      indpendentii_comments: null,
      consumerii_rating: null,
      consumerii_strength: null,
      consumerii_comments: null,
      budgetingii_rating: null,
      budgetingii_strength: null,
      budgetingii_comments: null,
      vitualii_rating: null,
      vitualii_strength: null,
      vitualii_comments: null,
      resourceii_rating: null,
      resourceii_strength: null,
      resourceii_comments: null,
      intimateii_rating: null,
      intimateii_strength: null,
      intimateii_comments: null,


      attainmentiii_rating: null,
      attainmentiii_strength: null,
      attainmentiii_comments: null,
      posteduiii_rating: null,
      posteduiii_strength: null,
      posteduiii_comments: null,
      optimismiii_rating: null,
      optimismiii_strength: null,
      optimismiii_comments: null,
      workexpiii_rating: null,
      workexpiii_strength: null,
      workexpiii_comments: null,
      trainingiii_rating: null,
      trainingiii_strength: null,
      trainingiii_comments: null,
      illnessiii_rating: null,
      illnessiii_strength: null,
      illnessiii_comments: null,
      treatmentiii_rating: null,
      treatmentiii_strength: null,
      treatmentiii_comments: null,
      medcomplianceiii_rating: null,
      medcomplianceiii_strength: null,
      medcomplianceiii_comments: null,
      adultserviceiii_rating: null,
      adultserviceiii_strength: null,
      adultserviceiii_comments: null,
      selfcareiii_rating: null,
      selfcareiii_strength: null,
      selfcareiii_comments: null,
      placementstabilityiii_rating: null,
      placementstabilityiii_strength: null,
      placementstabilityiii_comments: null,
      livingskillsiii_rating: null,
      livingskillsiii_strength: null,
      livingskillsiii_comments: null,
      consumeriii_rating: null,
      consumeriii_strength: null,
      consumeriii_comments: null,
      budgetingiii_rating: null,
      budgetingiii_strength: null,
      budgetingiii_comments: null,
      vitaliii_rating: null,
      vitaliii_strength: null,
      vitaliii_comments: null,
      resourceiii_rating: null,
      resourceiii_strength: null,
      resourceiii_comments: null,
      relationshipiii_rating: null,
      relationshipiii_strength: null,
      relationshipiii_comments: null,
      intimatereliii_rating: null,
      intimatereliii_strength: null,
      intimatereliii_comments: null,
      
      literacyiv_rating: null,
      literacyiv_strength: null,
      literacyiv_comments: null,
      servlearningiv_rating: null,
      servlearningiv_strength: null,
      servlearningiv_comments: null,
      attainmentiv_rating: null,
      attainmentiv_strength: null,
      attainmentiv_comments: null,
      etviv_rating: null,
      etviv_strength: null,
      etviv_comments: null,
      posteduiv_rating: null,
      posteduiv_strength: null,
      posteduiv_comments: null,
      optimismiv_rating: null,
      optimismiv_strength: null,
      optimismiv_comments: null,
      internshipiv_rating: null,
      internshipiv_strength: null,
      internshipiv_comments: null,
      workexpiv_rating: null,
      workexpiv_strength: null,
      workexpiv_comments: null,
      trainingiv_rating: null,
      trainingiv_strength: null,
      trainingiv_comments: null,
      
      healthneedsv_rating: null,
      healthneedsv_strength: null,
      healthneedsv_comments: null,
      treatcomplexv_rating: null,
      treatcomplexv_strength: null,
      treatcomplexv_comments: null,
      medicationv_rating: null,
      medicationv_strength: null,
      medicationv_comments: null,
      adultservicev_rating: null,
      adultservicev_strength: null,
      adultservicev_comments: null,
      civicengv_rating: null,
      civicengv_strength: null,
      civicengv_comments: null,
      
      selfcarevi_rating: null,
      selfcarevi_strength: null,
      selfcarevi_comments: null,
      stabilityvi_rating: null,
      stabilityvi_strength: null,
      stabilityvi_comments: null,
      livingskillsvi_rating: null,
      livingskillsvi_strength: null,
      livingskillsvi_comments: null,
      
      consumervii_rating: null,
      consumervii_strength: null,
      consumervii_comments: null,
      budgetingvii_rating: null,
      budgetingvii_strength: null,
      budgetingvii_comments: null,
      vitalvii_rating: null,
      vitalvii_strength: null,
      vitalvii_comments: null,
      resourceidentivii_rating: null,
      resourceidentivii_strength: null,
      resourceidentivii_comments: null,


      socialpeerviii_rating: null,
      socialpeerviii_strength: null,
      socialpeerviii_comments: null,
      intimateviii_rating: null,
      intimateviii_strength: null,
      intimateviii_comments: null,
      permenanceviii_rating: null,
      permenanceviii_strength: null,
      permenanceviii_comments: null
      
    });
    
    this.authorizationForm = this._formBuilder.group({
      assessmentstatus: [null],
      routingsupervisors: [null],
      supervisorname: [null],
      caseworkername: [null],
      caseworkercomments: [null]
    });
    
    this.faceLifeForm.get('caseworkername').disable();    
    // this.caregiverstrengthform.get('firstName').disable();
    // this.caregiverstrengthform.get('lastName').disable();
    // this.caregiverstrengthform.get('relationship').disable();
    this.permanencyPlanform.get('firstName').disable();
    this.permanencyPlanform.get('lastName').disable();
    this.permanencyPlanform.get('relationship').disable();
    this.permanencyPlanform.get('firstNameII').disable();
    this.permanencyPlanform.get('lastNameII').disable();
    this.permanencyPlanform.get('relationshipII').disable();
  }
  getcgFirstLastName(value) { 
    const name = this.caregiversInCase.find(item => item.personid === value);
    if (name){
      this.permanencyPlanform.patchValue({
        firstName: name.firstname,
        lastName: name.lastname
      });
    }
  }
  getcgFirstLastNameII(value) { 
    const name = this.caregiversInCase.find(item => item.personid === value);
    if (name){
      this.permanencyPlanform.patchValue({
        firstNameII: name.firstname,
        lastNameII: name.lastname
      });
    }

  }
  showEditCareGiver(mode: number, model, index?: number) {
    if (mode === 2) {
      this.caregiverFormList.splice(index, 1);
      this.removeAddedlegals();
      return true;
    }
    if (mode === 3) {
      this.removeAddedlegals();
    }
    this.caregiverstrengthform.enable();
    this.showCareGiver = true;
    this.caregiverMode = mode;
    if (model) {
      this.caregiverlegalGuardian = this.legalGuardian;
      this.caregiverstrengthform.patchValue(model);
      this.caregiverstrengthform.setControl('contactcaregivers', this._formBuilder.array([]));
      if (this.submissiondata ) { //Non-migrated data caregiver contact
      if (model.contactcaregivers && model.contactcaregivers.length > 0) {
        model.contactcaregivers.forEach(data => {
          this.addcaregiverContact(data);
        });
      }
      }
      this.caregiverindex = index;
      if (mode === 0) {
        this.caregiverstrengthform.disable();
      }
    } else {
      this.caregiverstrengthform.reset();
    }
  }

  addcaregiverContact(modal) {
    const control = <FormArray>this.caregiverstrengthform.controls['contactcaregivers'];
    control.push(this.createcareGiver(modal));
  }
  deletecaregiverContact(index: number) {
    const control = <FormArray>this.caregiverstrengthform.controls['contactcaregivers'];
    control.removeAt(index);
  }

  removeAddedlegals() {
    if (this.caregiverFormList && this.caregiverFormList.length > 0) {
      this.caregiverFormList.forEach(data => {
        this.caregiverlegalGuardian = this.caregiverlegalGuardian.filter(ele => ele.intakeservicerequestactorid !== data.caregiverlist);
      });
    } else {
      this.caregiverlegalGuardian = this.legalGuardian;
    }
  }

  private createcareGiver(modal) {
    return this._formBuilder.group({
      name: modal.name ? modal.name : null,
      address: modal.address ? modal.address : null,
      phoneno: modal.phoneno ? modal.phoneno : null,
      relationship: modal.relationship ? modal.relationship : null
    });
  }

  loadCaregiverList() {
    this.initializeCaregiverForm();
    this.caregiverlegalGuardian = this.legalGuardian;
    if (this.caregiverlegalGuardian && this.caregiverlegalGuardian.length > 0) {
      this.caregiverlegalGuardian.forEach(data => {
        this.caregiverlegalGuardianName[data.intakeservicerequestactorid] = data.fullname;
      });
    }
    this.removeAddedlegals();
    this.showCareGiver = false;
  }

  initializeCaregiverForm() {
    this.caregiverstrengthform = this._formBuilder.group({
      caregiverlist: [null],
      firstName: null,
      lastName: null,
      relationship: null,
      supervision_rating: null,
      supervision_strength: null,
      supervision_comments: null,
      involvementcare_rating: null,
      involvementcare_strength: null,
      involvementcare_comments: null,
      knowledge_rating: null,
      knowledge_strength: null,
      knowledge_comments: null,
      organization_rating: null,
      organization_strength: null,
      organization_comments: null,
      resource_rating: null,
      resource_strength: null,
      resource_comments: null,
      difficulties_rating: null,
      difficulties_strength: null,
      difficulties_comments: null,
      assessablity_rating: null,
      assessablity_strength: null,
      assessablity_comments: null,
      stability_rating: null,
      stability_strength: null,
      stability_comments: null,
      familystress_rating: null,
      familystress_strength: null,
      familystress_comments: null,
      safety_rating: null,
      safety_strength: null,
      safety_comments: null,
      phyhealth_rating: null,
      phyhealth_strength: null,
      phyhealth_comments: null,
      mentalhealth_rating: null,
      mentalhealth_strength: null,
      mentalhealth_comments: null,
      substanceuse_rating: null,
      substanceuse_strength: null,
      substanceuse_comments: null,
      developmental_rating: null,
      developmental_strength: null,
      developmental_comments: null,
      marital_rating: null,
      marital_strength: null,
      marital_comments: null,
      posttraumatic_rating: null,
      posttraumatic_strength: null,
      posttraumatic_comments: null,
      criminalbehav_rating: null,
      criminalbehav_strength: null,
      criminalbehav_comments: null
    });
    this.caregiverstrengthform.setControl('contactcaregivers', this._formBuilder.array([]));

  }
  saveCaregiver() {
    const caregiverstrengthform = this.caregiverstrengthform.getRawValue();
    caregiverstrengthform.formStatus = this.caregiverstrengthform.valid;
    if (this.caregiverMode && this.caregiverMode === 1) {
      this.caregiverFormList[this.caregiverindex] = caregiverstrengthform;
    } else {
      this.caregiverFormList.push(caregiverstrengthform);
    }
    this.showCareGiver = false;
    this.removeAddedlegals();
    this.saveForm();
  }

  cancelCareGiver() {
    this.showCareGiver = false;
  }

}
