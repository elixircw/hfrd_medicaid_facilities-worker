import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Assessments, RoutingInfo } from '../../../_entities/caseworker.data.model';
import { GenericService, CommonHttpService, DataStoreService, SessionStorageService, AuthService, AlertService, CommonDropdownsService } from '../../../../../@core/services';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpService } from '../../../../../@core/services/http.service';
import { AssessmentService } from '../assessment.service';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { DATEPICKER_CONTROL_VALUE_ACCESSOR } from 'ngx-bootstrap/datepicker/datepicker.component';
import { AppUser } from '../../../../../@core/entities/authDataModel';

@Component({
  selector: 'assessment-cans-f',
  templateUrl: './assessment-cans-f.component.html',
  styleUrls: ['./assessment-cans-f.component.scss']
})
export class AssessmentCansFComponent implements OnInit {
  ASSESSMENT_NAME = 'cans-v2';
  submissiondata: any;
  familyYouth: FormGroup;
  familyAssessmentYouth: FormGroup;
  familyCultureYouth: FormGroup;
  childFormFamilyQuestion: FormGroup;
  involvedPerson: any[];
  legalGuardian: any[];
  isServiceCase: any;
  roleId: AppUser;
  agency: string;
  isCW: boolean;
  headofhouseholdid: string;
  id: any;
  daNumber: any;
  currentAssessmentId: any;
  currentSubmissionId: string;
  timeForCompletion = [
    {
      text: 'Initial',
      value: 'Initial'
    },
    {
      text: '45 days',
      value: '45 days'
    },
    {
      text: '3 months',
      value: '3 months'
    },
    {
      text: '6 months',
      value: '6 months'
    },
    {
      text: 'Change in Family Circumstances',
      value: 'Initial'
    },
    {
      text: 'End of Service Case',
      value: 'End of Service Case'
    },
  ];
  serviceNameList: any[];
  caregiverForm: FormGroup;
  ShowCareGiverForm = false;
  cansFData: any;
  updateCansF: boolean;
  viewCansF: boolean;
  otherbox: boolean;
  ritual_strength_id: boolean;
  sexual_comments_id: boolean;
  cultural_comments_id: boolean;
  language_comments_id: boolean;
  relation_strength_id: boolean;
  extended_strength_id: boolean;
  family_strength_id: boolean;
  financial_resources_strength_id: boolean;
  residential_stability_strength_id: boolean;
  fcommunication_strength_id: boolean;
  fappropriateness_strength_id: boolean;
  safety_strength_id: boolean;
  social_strength_id: boolean;
  Provider: boolean;
  cProvider: boolean;
  familyOther: boolean;
  totalamt: number;
  // CareGiver
  caregiverFormList = [];
  caregiverMode: any;
  caregiverindex: number;
  showCareGiver: boolean;
  caregiverlegalGuardian: any[];
  caregiverDdList: any[];
  caregiverlegalGuardianName = [];
  parental_description_id: boolean;
  // Child
  childForm: FormGroup;
  childFormList = [];
  childMode: any;
  childindex: number;
  showChild: boolean;
  childList: any[];
  childListName = [];
  childListOriginal = [];
  notChildList = [];
  childListArrayList = [];

  //Caregivers
  caregiversInCase = [];

  // Approval and routing
  routingSupervisors = [];
  routingInfo: RoutingInfo[];
  authorizationApproval: FormGroup;
  isSupervisor: boolean;
  assessmentStatus: any;

  incompleteList: any[];
  caseworkersignature: any;
  supervisorsignature: any;
  headofHouseHold: any;
  notChildListOriginal = [];

  constructor(
    private route: ActivatedRoute,
    private _service: GenericService<Assessments>,
    private _commonService: CommonHttpService,
    public sanitizer: DomSanitizer,
    private _http: HttpService,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private storage: SessionStorageService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _commonDDService: CommonDropdownsService,
    private _assessmentService: AssessmentService,
    private _formBuilder: FormBuilder
  ) {
    this.id = this._commonDDService.getStoredCaseUuid();
    this.daNumber = this._commonDDService.getStoredCaseNumber();
  }

  ngOnInit() {
    this.isServiceCase = this.storage.getItem('ISSERVICECASE');
    this.roleId = this._authService.getCurrentUser();
    this.agency = this._authService.getAgencyName();
    this.isSupervisor = (this.roleId.role.name === 'apcs') ? true : false;
    if (this.agency === 'CW') {
      this.isCW = true;
    } else {
      this.isCW = false;
    }
    this.totalamt = 0;
    this.Provider = false;
    this.cProvider = false;
    this.otherbox = false;
    this.family_strength_id = false;
    this.familyYouthForm();
    this.comprehensiveFamilyAssessmentForm();
    this.initializeCaregiverForm();
    this.CultureAssessmentForm();
    this.authorizationApprovalForm();

    //Get persons in case
    this.getInvolvedPerson();
    //Get caregivers in case
    this.getCaregiversInCase();
    this.childFormFamilyQuestionInit();
    this.relation_strength_id = false;
    this.parental_description_id = false;
    this.extended_strength_id = false;
    this.fcommunication_strength_id = false;
    this.safety_strength_id = false;
    this.social_strength_id = false;
    this.ritual_strength_id = false;
    this.sexual_comments_id = false;
    this.cultural_comments_id = false;
    this.language_comments_id = false;
    this.financial_resources_strength_id = false;
    this.residential_stability_strength_id = false;
    const list = this._dataStoreService.getData('CASEWORKER_SERVICE_PLAN_LIST');
    this.cansFData = this._dataStoreService.getData('CASEWORKER_SELECTED_ASSESSMENT');
    // In case of working with an existing assesment submission
    this.currentAssessmentId = this.cansFData.assessmentid;
    this.currentSubmissionId = this.cansFData.submissionid;

    this.serviceNameList = (Array.isArray(list)) ? list.map(item => item.serviceplanname) : [];

    this._assessmentService.getservicecase();
    // Approval and routing
    this.routingInfo = this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') ? this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') : [];
    this.routingSupervisors = this._dataStoreService.getData('CASEWORKER_SUPERVISORS') ? this._dataStoreService.getData('CASEWORKER_SUPERVISORS') : [];
    this.prefillAuthorizationApprovalInfo();


    //Do this at the end to patch all the data from an existing form
    if(this.cansFData.mode === 'update' || this.cansFData.mode === 'submit') {
      this.getSubmisionData();
    } else {
      this.getCansfData();
    }

    if (this.authorizationApproval.get('caseworkersign').value) {
      this.caseworkersignature = this.authorizationApproval.get('caseworkersign').value;
    }
    if (this.authorizationApproval.get('supervisorsign').value) {
      this.supervisorsignature = this.authorizationApproval.get('supervisorsign').value;
    }
  }

  loadCaregiverList() {
    this.initializeCaregiverForm();
    this.caregiverlegalGuardian = this.caregiverDdList;
    if (this.caregiverlegalGuardian && this.caregiverlegalGuardian.length > 0) {
      this.caregiverlegalGuardian.forEach(data => {
        this.caregiverlegalGuardianName[data.intakeservicerequestactorid] = data.fullname;
      });
    }
    this.removeAddedlegals();
    this.showCareGiver = false;

  }

  caregiverRequired(value, formcontrolname) {
    if (value !== 0) {
      this.caregiverForm.get(formcontrolname).setValidators(Validators.required);
      this.caregiverForm.get(formcontrolname).updateValueAndValidity();
    } else {
      this.caregiverForm.get(formcontrolname).clearValidators();
      this.caregiverForm.get(formcontrolname).updateValueAndValidity();
    }
  }

  prefillAuthorizationApprovalInfo() {
    this.authorizationApproval.patchValue({
      supervisorname: this.routingInfo[0].fromusername,
      workername: this.routingInfo[0].tousername
    })
  }

  getSubmisionData() {
    const url = `admin/assessment/getassessmentform/${this.cansFData.external_templateid}/submission/${this.cansFData.submissionid}`;
    this._commonService.getSingle({}, url).subscribe(result => {
      console.log("SIMAR THIS IS NEW->", result);
      console.log("SIMAR THIS IS ORIGINAL ->", this.cansFData.submissiondata);

      //Replace submissiondata from assessment table with the parsed json from transaction table
      // this.cansFData.submissiondata = result;
      this.submissiondata = result;
      this.getCansfData();
    });
  }

  getCansfData() {

    //Flip for non-migrated data
    if (this.cansFData && this.cansFData.submissiondata ) {
      this.submissiondata = this.cansFData.submissiondata;
    }
    this._dataStoreService.setData('PRINTDATA', this.submissiondata);

    if (this.cansFData && (this.cansFData.mode === 'update' || this.cansFData.mode === 'submit')) {
      this.updateCansF = true;
      this.viewCansF = false;
      if (this.submissiondata.familyYouth) {
        // this.cansFData.submissiondata.familyYouth = this.submissiondata.familyYouth;
        let fydata = this.submissiondata.familyYouth;
        this.familyYouth.patchValue({
          caseheaddatetime: fydata.caseheaddatetime,
          timeCompletion: fydata.timeCompletion,
          houseHoldId: fydata.houseHoldId,
          houseHoldHead: fydata.houseHoldHead,
          houseHoldName: fydata.houseHoldName,
          servicename: fydata.servicename,
          caregivername: fydata.caregivername,
          caregiverdob: fydata.caregiverdob,
          caregiverage: fydata.caregiverage,
          caregiverrelationship: fydata.caregiverrelationship,
          childName: fydata.childName,
          childDob: fydata.childDob,
          childAge: fydata.childAge,
          childRelationship: fydata.childRelationship,
          childSchool: fydata.childSchool,
          childdescription: fydata.childdescription,
          caregiverdescription: fydata.caregiverdescription,
          referralsource: fydata.referralsource
        });
        if (this.submissiondata.familyYouth_serviceArray) {
          //@Simar: resetting the form array from the temp key
          // this.cansFData.submissiondata.familyYouth.serviceArray = this.submissiondata.familyYouth_serviceArray;
          this.submissiondata.familyYouth_serviceArray.forEach((element) => {
            this.addService(element);
          });
        }
        if (this.submissiondata.familyYouth_childArray) {
          //@Simar: resetting the form array from the temp key
          // this.cansFData.submissiondata.familyYouth.childArray = this.submissiondata.familyYouth_childArray;
          this.submissiondata.familyYouth_childArray.forEach((element, index) => {
            this.childListArrayList[index] = this.childListOriginal;
            this.addChild(element);
          });
        }
        if (this.submissiondata.familyYouth_caregiverArray) {
          //@Simar: resetting the form array from the temp key
          // this.cansFData.submissiondata.familyYouth.caregiverArray = this.submissiondata.familyYouth_caregiverArray;
          this.submissiondata.familyYouth_caregiverArray.forEach((element, index) => {
            this.notChildList[index] = this.notChildListOriginal;
            this.addLegalGuardian(element);
          });
        }
      }
      if (this.submissiondata.careGiver) {
        // this.cansFData.submissiondata.careGiver = this.submissiondata.careGiver;
        this.caregiverFormList = this.submissiondata.careGiver;
      }
      if (this.submissiondata.child) {
        // this.cansFData.submissiondata.child = this.submissiondata.child;
        this.childFormList = this.submissiondata.child;
      }
      if (this.submissiondata.familyAssessmentYouth) {
        // this.cansFData.submissiondata.familyAssessmentYouth = this.submissiondata.familyAssessmentYouth
        this.familyAssessmentYouth.patchValue(this.submissiondata.familyAssessmentYouth);
      }
      if (this.submissiondata.familyCultureYouth) {
        // this.cansFData.submissiondata.familyCultureYouth = this.submissiondata.familyCultureYouth;
        this.familyCultureYouth.patchValue(this.submissiondata.familyCultureYouth);
      }
      if(this.submissiondata.childFormFamilyQuestion){
        this.childFormFamilyQuestion.patchValue(this.submissiondata.childFormFamilyQuestion);
      }
      //currently when I patch signature it is breaking so patching data individually
      if (this.submissiondata.authorizationApproval) {
        // this.cansFData.submissiondata.authorizationApproval = this.submissiondata.authorizationApproval;
        let approvaldata = this.submissiondata.authorizationApproval;
        this.authorizationApproval.patchValue({
          routingsupervisors: approvaldata.routingsupervisors,
          supervisorname: approvaldata.supervisorname,
          workername: approvaldata.workername,
          caseworkersign: approvaldata.caseworkersign,
          caseworkersigndate: approvaldata.caseworkersigndate,
          caseworkercomments: approvaldata.caseworkercomments,
          supervisorsign: approvaldata.supervisorsign,
          supervisorsigndate: approvaldata.supervisorsigndate,
          supervisorcomments: approvaldata.supervisorcomments,
          assessmentstatus: approvaldata.assessmentstatus
        });
      }
    } else {
      this.updateCansF = false;
      this.viewCansF = false;
    }
    if (this.cansFData && this.cansFData.mode === 'submit') {
      this.updateCansF = true;
      this.viewCansF = true;
      this.familyYouth.disable();
      this.familyAssessmentYouth.disable();
      this.childFormFamilyQuestion.disable();
      this.familyCultureYouth.disable();
      this.authorizationApproval.disable();
    }
  }

  familyYouthForm() {
    this.familyYouth = this._formBuilder.group({
      caseheaddatetime: [null],
      timeCompletion: [null],
      houseHoldId: [{value: null, disabled: true}],
      houseHoldHead: [null],
      houseHoldName: [{value: null, disabled: true}],
      servicename: [null],
      caregivername: [null],
      caregiverdob: [null],
      caregiverage: [null],
      caregiverrelationship: [null],
      childName: [null],
      childDob: [null],
      childAge: [null],
      childRelationship: [null],
      childSchool: [null],
      childdescription: [null],
      caregiverdescription: [null],
      referralsource: [null]
    });
    this.familyYouth.setControl('caregiverArray', this._formBuilder.array([]));
    this.familyYouth.setControl('childArray', this._formBuilder.array([]));
    this.familyYouth.setControl('serviceArray', this._formBuilder.array([]));
    this.familyYouth.patchValue({
      caseheaddatetime: new Date(),
      houseHoldId: this.daNumber
    });
  }

  authorizationApprovalForm() {
    this.authorizationApproval = this._formBuilder.group({
      routingsupervisors: [null],
      supervisorname: [{value: '', disabled: true}],
      workername: [{value: '', disabled: true}],
      caseworkersign: [''],
      caseworkersigndate: [null],
      caseworkercomments: [''],
      supervisorsign: [''],
      supervisorsigndate: [null],
      supervisorcomments: [''],
      assessmentstatus: ['']
    });
  }

  changeSupervisor(userid) {
    const user = this.routingSupervisors.find(item => item.userid === userid);
    if (user.username) {
      this.authorizationApproval.patchValue({
        supervisorname: user.username
      });
    }
  }

  childFormFamilyQuestionInit(){
    this.childFormFamilyQuestion = this._formBuilder.group({
      recommendations: [null],
      recommendationstext:['']
    });
  }
  comprehensiveFamilyAssessmentForm() {
    this.familyAssessmentYouth = this._formBuilder.group({
      parental_strength: [''],
      parental_scale: [''],
      parental_description: [''],
      relation_strength: [''],
      relation_scale: [''],
      relation_description: [''],
      extended_strength: [''],
      extended_scale: [''],
      extended_description: [''],
      family_strength: [''],
      family_scale: [''],
      family_description: [''],
      fcommunication_strength: [''],
      fcommunication_scale: [''],
      fcommunication_description: [''],
      fappropriateness_strength: [''],
      fappropriateness_scale: [''],
      fappropriateness_description: [''],
      safety_strength: [''],
      safety_scale: [''],
      safety_description: [''],
      social_strength: [''],
      social_scale: [''],
      social_description: [''],
      emp_income_amt: [''],
      child_support_amt: [''],
      t_cash_assistance_amt: [''],
      food_stamp_amt: [''],
      social_security_benefits: [''],
      unemployment_amt: [''],
      other_amt: [''],
      unknown_amt: [''],
      monthly_total: [''],
      caregiver_insurance: [''],
      caregiver_insurance_provider: [''],
      children_insurance: [''],
      children_insurance_provider: [''],
      family_needs_assistance: [''],
      family_needs_assistance_txt: [''],
      financial_resources_strength: [''],
      financial_resources_scale: [''],
      financial_resources_description: [''],
      residential_stability_strength: [''],
      residential_stability_scale: [''],
      residential_stability_description: [''],
      family_own_rent: [''],
      family_other: [''],
      family_transport: [''],
      neighborhood: [''],
      familyFunctioning: ['']
    });
  }
  CultureAssessmentForm() {
    this.familyCultureYouth = this._formBuilder.group({
      language_strength: [''],
      language_scale: [''],
      language_comments: [''],
      cultural_strength: [''],
      cultural_scale: [''],
      cultural_comments: [''],
      sexual_identity_strength: [''],
      sexual_scale: [''],
      sexual_comments: [''],
      ritual_strength: [''],
      ritual_scale: [''],
      ritual_comments: [''],
      additional_acculturation_info: ['']
    });
  }

  caregivername(value, i) {
    this.checkifcaregiver(value);
    const modal = this.notChildListOriginal.find(data => data.intakeservicerequestactorid === value);
    const relationshipDesc = this._assessmentService.getRelationShip(this.headofhouseholdid,modal.personid, modal.relationshiparray);
   
    (<FormArray>this.familyYouth.get('caregiverArray')).controls[i].patchValue({
      caregiverdob: modal.dob ? new Date(modal.dob) : '',
      caregiverage: modal.age ? modal.age : '',
      caregiverrelationship: relationshipDesc ? relationshipDesc : ''
    });
    const control = this.familyYouth.controls['caregiverArray'].value;
    if (control && control.length > 0) {
      control.forEach((element, index) => {
        if (index !== i) {
          this.notChildList[index] = this.notChildList[index].filter(data => data.intakeservicerequestactorid !== value);
        }
      });
    }
  }

  childname(value, i) {
    const modal = this.childListOriginal.find(data => data.intakeservicerequestactorid === value);
    const relationshipDesc = this._assessmentService.getRelationShip(this.headofhouseholdid,modal.personid, modal.relationshiparray);
    (<FormArray>this.familyYouth.get('childArray')).controls[i].patchValue({
      childDob: modal.dob ? new Date(modal.dob) : '',
      childAge: modal.age ? modal.age : '',
      childRelationship: relationshipDesc ? relationshipDesc : '',
      childSchool: null
    });
    const control = this.familyYouth.controls['childArray'].value;
    if (control && control.length > 0) {
      control.forEach((element, index) => {
        if (index !== i) {
          this.childListArrayList[index] = this.childListArrayList[index].filter(data => data.intakeservicerequestactorid !== value);
        }
      });
    }
  }

  /**
   * Get caregivers in the case
   */
  getCaregiversInCase() {
    this.caregiversInCase = [];
    this._commonService.getArrayList(
            new PaginationRequest({
                nolimit: true,
                method: 'get',
                where: {
                    personid: this.id
                }
            }),
            'Actorrelationships/getallcaregiversincase'+ '?filter'
        )
        .subscribe(response => {
        if (response && Array.isArray(response) && response.length && response[0].getallcaregiversincase) {
          this.caregiversInCase = response[0].getallcaregiversincase;
          }
      });
  }

  checkifcaregiver(value) {
    let cgflag = false;

    const modal = this.involvedPerson.find(data => data.intakeservicerequestactorid === value);

    if(this.caregiversInCase && this.caregiversInCase.length) {
      this.caregiversInCase.forEach(element => {
          if(element.personid == modal.personid) {
            cgflag = true;
          }
      });

      if (cgflag === false) {
        (<any>$('#not-caregiver-alert')).modal('show');
      }
    }
  }

  getInvolvedPerson() {
    this.legalGuardian = [];
    this.caregiverDdList = [];
    let getpersonlistreq = {};
    if (this.isCW && this.isServiceCase) {
      getpersonlistreq = { objectid: this.id, objecttypekey: 'servicecase' };
    } else {
      getpersonlistreq = { intakeserviceid: this.id };
    }
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        page: 1,
        limit: 20,
        method: 'get',
        where: getpersonlistreq
      }),
      'People/getpersondetailcw?filter'
    ).subscribe(response => {
      if (response && response.data && response.data.length) {
        this.involvedPerson = response.data;
        let  headofHouseHold = null;
        this.involvedPerson.map(item => {
          if(item.isheadofhousehold) {
            this.headofhouseholdid = item.personid;
            headofHouseHold = item;
          }
          if(item.roles !== null){          
          const legalGurdian = item.roles.filter(roleid => roleid.intakeservicerequestpersontypekey === 'LG');
          const child = item.roles.filter(roleid => roleid.intakeservicerequestpersontypekey === 'CHILD' ||
            roleid.intakeservicerequestpersontypekey === 'OTHERCHILD');
          if (child && child.length) {
            this.childListOriginal.push(item);
            // if (!this.updateCansF) {
            //   if (! (this.cansFData.mode === 'update')) {
            //     this.addChild(item);
            //   }
            // }
          }
          if (!child || child.length === 0) {
            this.notChildListOriginal.push(item);
          }
          if (legalGurdian && legalGurdian.length) {
            this.legalGuardian.push(item);
          }
            // if (!this.updateCansF) {
            //   if (! (this.cansFData.mode === 'update')) {
            //   this.addLegalGuardian(item);
            //   }
            // }
          const cg = item.roles.filter( el => 
            (el.intakeservicerequestpersontypekey != 'CHILD' 
              && el.intakeservicerequestpersontypekey != 'OTHERCHILD' 
              && el.intakeservicerequestpersontypekey != 'AV')  );

          if(cg && cg.length){
            this.caregiverDdList.push(item);
          }

          if(headofHouseHold && headofHouseHold.intakeservicerequestactorid) {
            this.headofHouseHold = headofHouseHold;
            this.familyYouth.patchValue({
              houseHoldHead: headofHouseHold && headofHouseHold.intakeservicerequestactorid ? headofHouseHold.intakeservicerequestactorid : null,
              houseHoldName: headofHouseHold.firstname + ' ' + headofHouseHold.lastname
            });
          }
        }
        });
      }
      if (!(this.headofHouseHold && this.headofHouseHold.intakeservicerequestactorid)){
        setTimeout(() => {
          this.familyYouth.patchValue({
            houseHoldHead: this.legalGuardian && this.legalGuardian.length ? this.legalGuardian[0].intakeservicerequestactorid: '',
            houseHoldName: this.legalGuardian && this.legalGuardian.length ?this.legalGuardian[0].fullname : ''
          });
        }, 2000);
      }
    });
  }
  addLegalGuardian(modal) {
    if (modal) {
      const control = <FormArray>this.familyYouth.controls['caregiverArray'];
      control.push(this.createFormGroup(modal));
    } else {
      this.removeAddedCaregiver();
      const control = <FormArray>this.familyYouth.controls['caregiverArray'];
      control.push(this.createFormGroupNew());
    }
  }

  removeAddedCaregiver() {
    const control = this.familyYouth.controls['caregiverArray'].value;
    this.notChildList[control.length] = this.notChildListOriginal;
    if (control && control.length > 0) {
      control.forEach(data => {
        this.notChildList[control.length] = this.notChildList[control.length].filter(ele => ele.intakeservicerequestactorid !== data.caregivername);
      });
    } else {
      this.notChildList[0] = this.notChildListOriginal;
    }
  }

  deleteLegalGuardian(i: number) {
    const control = this.familyYouth.controls['caregiverArray'].value;
    const oridata = this.notChildListOriginal.find(data => data.intakeservicerequestactorid === control[i].caregivername);
    if (control && control.length > 0) {
      control.forEach((element, index) => {
        if (oridata) {
          if (i < control.length && index !== (control.length - 1)) {
            const oridatas = this.notChildListOriginal.find(data => data.intakeservicerequestactorid === control[index + 1].caregivername);            
            const notchildlist = oridatas ? this.notChildList[index].find(data => data.intakeservicerequestactorid === oridatas.intakeservicerequestactorid) : null;
            if (!notchildlist) {
              this.notChildList[index].push(oridatas);
            }
          } else {
            const notchildlist = this.notChildList[index].find(data => data.intakeservicerequestactorid === oridata.intakeservicerequestactorid);
            if (!notchildlist) {
              this.notChildList[index].push(oridata);
            }
          }
        }
      });
    }
    const controldata = <FormArray>this.familyYouth.controls['caregiverArray'];
    controldata.removeAt(i);
  }
  addChild(modal) {
    if (modal) {
      const control = <FormArray>this.familyYouth.controls['childArray'];
      control.push(this.createChildGroup(modal));
    } else {
      this.removeAddedChildList();
      const control = <FormArray>this.familyYouth.controls['childArray'];
      control.push(this.createChildGroupNew());
    }
  }
  removeAddedChildList() {
    const control = this.familyYouth.controls['childArray'].value;
    this.childListArrayList[control.length] = this.childListOriginal;
    if (control && control.length > 0) {
      control.forEach(data => {
        this.childListArrayList[control.length] = this.childListArrayList[control.length].filter(ele => ele.intakeservicerequestactorid !== data.childName);
      });
    } else {
      this.childListArrayList[0] = this.childListOriginal;
    }
  }
  deleteChild(i: number) {const control = this.familyYouth.controls['childArray'].value;
  const oridata = this.childListOriginal.find(data => data.intakeservicerequestactorid === control[i].childName);
  if (control && control.length > 0) {
    control.forEach((element, index) => {
      if (oridata) {
        if (i < control.length && index !== (control.length - 1)) {
          const oridatas = this.childListOriginal.find(data => data.intakeservicerequestactorid === control[index + 1].childName);
          const notchildlist = oridatas ? this.childListArrayList[index].find(data => data.intakeservicerequestactorid === oridatas.intakeservicerequestactorid) : null;
          if (!notchildlist) {
            this.childListArrayList[index].push(oridatas);
          }
        } else {
          const notchildlist = this.childListArrayList[index].find(data => data.intakeservicerequestactorid === oridata.intakeservicerequestactorid);
          if (!notchildlist) {
            this.childListArrayList[index].push(oridata);
          }
        }
      }
    });
  }
    const controldata = <FormArray>this.familyYouth.controls['childArray'];
    controldata.removeAt(i);
  }
  addService(modal?) {
    const control = <FormArray>this.familyYouth.controls['serviceArray'];
    if (modal) {
      control.push(this.createServiceGroup(modal));
    } else {
      control.push(this.createServiceGroup(null));
    }
  }
  deleteService(index: number) {
    const control = <FormArray>this.familyYouth.controls['serviceArray'];
    control.removeAt(index);
  }
  private createFormGroup(modal) {
    if (!this.updateCansF) {
      return this._formBuilder.group({
        caregivername: modal.fullname ? modal.fullname : '',
        caregiverdob: modal.dob ? new Date(modal.dob) : '',
        caregiverage: modal.age ? modal.age : '',
        caregiverrelationship: modal.relationship ? modal.relationship : ''
      });
    } else {
      return this._formBuilder.group(modal);
    }
  } 
  private createFormGroupNew() {
      return this._formBuilder.group({
        caregivername: null,
        caregiverdob: null,
        caregiverage: null,
        caregiverrelationship: null
      });
  }
  private createChildGroup(modal) {
    if (!this.updateCansF) {
      return this._formBuilder.group({
        childName: modal.fullname ? modal.fullname : '',
        childDob: modal.dob ? new Date(modal.dob) : '',
        childAge: modal.age ? modal.age : '',
        childRelationship: modal.relationship ? modal.relationship : '',
        childSchool: null
      });
    } else {
      return this._formBuilder.group(modal);
    }
  }

  private createChildGroupNew() {
    return this._formBuilder.group({
      childName: null,
      childDob: null,
      childAge: null,
      childRelationship: null,
      childSchool: null
    });
  }

  private createServiceGroup(modal) {
    if (modal) {
      return this._formBuilder.group({
        servicename: modal.servicename ? modal.servicename : null,
        currentorpast: modal.currentorpast ? modal.currentorpast : null,
        servicehelpful: modal.servicehelpful ? modal.servicehelpful : null,
      });
    } else {
      return this._formBuilder.group({
        servicename: null,
        currentorpast: null,
        servicehelpful: null,
      });
    }
  }

  getCGName(value) {
    const modal = this.notChildListOriginal.find(data => data.intakeservicerequestactorid === value);
    return modal ? modal.fullname : '' ;
  }

  getChildName(value) {
    const modal = this.childListOriginal.find(data => data.intakeservicerequestactorid === value);
    return modal ? modal.fullname : '' ;
  }

  getFYPrintData() {
    let fy = this.familyYouth.getRawValue();

    let cgarr = this.familyYouth.get('caregiverArray').value;
    if (cgarr) {
      cgarr.forEach(element => {
        element['caregivernametext'] = this.getCGName(element['caregivername']);
      });
    }
    fy['caregiverArray'] = cgarr;

    let childarr = this.familyYouth.get('childArray').value;
    if (childarr) {
      childarr.forEach(element => {
        element['childNameText'] = this.getChildName(element['childName']);
      });
    }
    fy['childArray'] = childarr;

    return fy;
  }


  getcansfPayload() {
    // const printData = {
    //   caregivernametext: this.getName(this.familyYouth.value.caregivername),
    //   childnametext: this.getName(this.familyYouth.value.childName)
    // }
    const cansFData =  {
      // printData: printData,
      familyYouth: this.getFYPrintData(),
      //Save the form array placeholder for parsed data into transaction table
      familyYouth_caregiverArray: this.familyYouth.get('caregiverArray').value,
      familyYouth_childArray: this.familyYouth.get('childArray').value,
      familyYouth_serviceArray: this.familyYouth.get('serviceArray').value,
      // this.familyYouth.setControl('caregiverArray', this._formBuilder.array([]));
      // this.familyYouth.setControl('childArray', this._formBuilder.array([]));
      // this.familyYouth.setControl('serviceArray', this._formBuilder.array([]));
      careGiver: this.caregiverFormList,
      familyAssessmentYouth: this.familyAssessmentYouth.getRawValue(),
      child: this.childFormList,
      childFormFamilyQuestion: this.childFormFamilyQuestion.getRawValue(),
      familyCultureYouth: this.familyCultureYouth.getRawValue(),
      authorizationApproval: this.authorizationApproval.getRawValue(),
      // The API expects these in addition to form data
      supervisorname: this.authorizationApproval.get('supervisorname').value,
      routingsupervisors: this.routingSupervisors, 
      currentSubmissionId: this.currentSubmissionId,
      assessmentStaus: this.assessmentStatus
    };
    return cansFData;
  }

  saveForm() {
    const cansFData = this.getcansfPayload();
    this._assessmentService.saveAssessment(this.ASSESSMENT_NAME, this.currentAssessmentId, cansFData)
      .subscribe(
        (response) => {
          this._alertService.success('Assessment data saved');
          if (response.data) {
            this.currentAssessmentId = response.data.assessmentid;
            this.currentSubmissionId = response.data.submissionid;
          } else {
            this.currentAssessmentId = response.assessmentid;
            this.currentSubmissionId = response.submissionid;
          }
        },
        (error) => {
          this._alertService.error('Unable to save.');
        }
      );
  }

  submitForApproval() {
     if (this.isSupervisor) {
      this.assessmentStatus = this.authorizationApproval.get('assessmentstatus').value;
     } else {
      this.assessmentStatus = 'Review';
     }
      const submissionData = this.getcansfPayload();
      this._assessmentService.saveAssessment(this.ASSESSMENT_NAME, this.currentAssessmentId, submissionData)
        .subscribe(
          (response) => {
            this._alertService.success(`${this.assessmentStatus === 'Review' ? 'Approval' : this.assessmentStatus} Submitted Successfully`);
            if (this.assessmentStatus === 'Accepted') {
              this.saveAssessmentStrengthNeeds('cansF');
            }
            setTimeout(() => {
              this._router.navigate(['../'], {relativeTo : this.route});
            }, 1000);
          },
          (error) => {
            this._alertService.error('Unable to submit for approval.');
          }
        );
  }

    saveAssessmentStrengthNeeds(templatename) {
      // How do we handle cans-outofhome?
      const payload = {
        'objectid': this.id,
        'templatename': templatename,
        'status': 'accepted'
      };
      this._commonService.create(payload, 'serviceplan/saveAssessmentStrengthNeeds').subscribe(
        response => {
          console.log('Strengths and Needs added successfully.');
        });
    }

  ondetectStrength(event, strength_id, comment, form) {
    if (event.value == 1) {
      if (strength_id == 'parental_strength') {
        this.parental_description_id = true;
      }
      if (strength_id == 'cultural_strength') {
        this.cultural_comments_id = true;
      }
      if (strength_id == 'language_strength') {
        this.language_comments_id = true;
      }
      if (strength_id == 'sexual_identity_strength') {
        this.sexual_comments_id = true;
      }
      if (strength_id == 'ritual_strength') {
        this.ritual_strength_id = true;
      }
      if (strength_id == 'relation_strength') {
        this.relation_strength_id = true;
      }
      if (strength_id == 'extended_strength') {
        this.extended_strength_id = true;
      }
      if (strength_id == 'social_strength') {
        this.social_strength_id = true;
      }
      if (strength_id == 'safety_strength') {
        this.safety_strength_id = true;
      }
      if (strength_id == 'fappropriateness_strength') {
        this.fappropriateness_strength_id = true;
      }
      if (strength_id == 'fcommunication_strength') {
        this.fcommunication_strength_id = true;
      }
      if (strength_id == 'financial_resources_strength') {
        this.financial_resources_strength_id = true;
      }
      if (strength_id == 'residential_stability_strength') {
        this.residential_stability_strength_id = true;
      }
      if (strength_id == 'family_strength') {
        this.family_strength_id = true;
      }
      if (form == 2) {
        this.familyAssessmentYouth.get(comment).clearValidators();
        this.familyAssessmentYouth.get(comment).updateValueAndValidity();
      } else if (form == 4) {
        this.familyCultureYouth.get(comment).clearValidators();
        this.familyCultureYouth.get(comment).updateValueAndValidity();
      }
    } else if (event.value > 1) {
      $('#' + strength_id).addClass('hide');
      if (strength_id == 'language_strength') {
        this.language_comments_id = false;
      }
      if (strength_id == 'sexual_identity_strength') {
        this.sexual_comments_id = false;
      }
      if (strength_id == 'ritual_strength') {
        this.ritual_strength_id = false;
      }
      if (strength_id == 'relation_strength') {
        this.relation_strength_id = true;
      }
      if (strength_id == 'parental_strength') {
        this.parental_description_id = false;
      }
      if (strength_id == 'relation_strength') {
        this.relation_strength_id = false;
      }
      if (strength_id == 'extended_strength') {
        this.extended_strength_id = false;
      }
      if (strength_id == 'social_strength') {
        this.social_strength_id = false;
      }
      if (strength_id == 'safety_strength') {
        this.safety_strength_id = false;
      }
      if (strength_id == 'family_strength') {
        this.family_strength_id = false;
      }
      if (strength_id == 'fappropriateness_strength') {
        this.fappropriateness_strength_id = false;
      }
      if (strength_id == 'fcommunication_strength') {
        this.fcommunication_strength_id = false;
      }
      if (strength_id == 'financial_resources_strength') {
        this.financial_resources_strength_id = false;
      }
      if (strength_id == 'residential_stability_strength') {
        this.residential_stability_strength_id = false;
      }
      if (form == 2) {
        this.familyAssessmentYouth.get(comment).setValidators([Validators.required]);
        this.familyAssessmentYouth.get(comment).updateValueAndValidity();
      } else if (form == 4) {
        this.familyCultureYouth.get(comment).setValidators([Validators.required]);
        this.familyCultureYouth.get(comment).updateValueAndValidity();
      }
    } else {
      this.parental_description_id = false;
    }
  }

  calculate() {
    const emp_income_amt = (this.familyAssessmentYouth.get('emp_income_amt').value) ? parseFloat(this.familyAssessmentYouth.get('emp_income_amt').value) : 0;
    const child_support_amt = (this.familyAssessmentYouth.get('child_support_amt').value) ? parseFloat(this.familyAssessmentYouth.get('child_support_amt').value) : 0;
    const t_cash_assistance_amt = (this.familyAssessmentYouth.get('t_cash_assistance_amt').value) ? parseFloat(this.familyAssessmentYouth.get('t_cash_assistance_amt').value) : 0;
    const food_stamp_amt = (this.familyAssessmentYouth.get('food_stamp_amt').value) ? parseFloat(this.familyAssessmentYouth.get('food_stamp_amt').value) : 0;
    const social_security_benefits = (this.familyAssessmentYouth.get('social_security_benefits').value) ? parseFloat(this.familyAssessmentYouth.get('social_security_benefits').value) : 0;
    const unemployment_amt = (this.familyAssessmentYouth.get('unemployment_amt').value) ? parseFloat(this.familyAssessmentYouth.get('unemployment_amt').value) : 0;
    const other_amt = (this.familyAssessmentYouth.get('other_amt').value) ? parseFloat(this.familyAssessmentYouth.get('other_amt').value) : 0;
    const unknown_amt = (this.familyAssessmentYouth.get('unknown_amt').value) ? parseFloat(this.familyAssessmentYouth.get('unknown_amt').value) : 0;

    this.totalamt = emp_income_amt + child_support_amt + t_cash_assistance_amt + food_stamp_amt + social_security_benefits + unemployment_amt + other_amt + unknown_amt;
    this.familyAssessmentYouth.patchValue({
        monthly_total: this.totalamt,
      });
  }

  initializeCaregiverForm() {
    this.caregiverForm = this._formBuilder.group({
      caregiverlist: [null],
      supervision: [null],
      supervisionrating: [null],
      supervisionnotes: [null],
      involvement: [null],
      involvementrating: [null],
      involvementnotes: [null],
      emotionalresp: [null],
      emotionalresprating: [null],
      emotionalrespnotes: [null],
      knowledge: [null],
      knowledgerating: [null],
      knowledgenotes: [null],
      org: [null],
      orgrating: [null],
      orgnotes: [null],
      boundaries: [null],
      boundariesrating: [null],
      boundariesnotes: [null],
      discipline: [null],
      disciplinerating: [null],
      disciplinenotes: [null],
      posttraumaticrating: [null],
      posttraumaticnotes: [null],
      phyhealthrating: [null],
      phyhealthnotes: [null],
      mentalhealthrating: [null],
      mentalhealthnotes: [null],
      developmentalrating: [null],
      developmentalnotes: [null],
      substanceuserating: [null],
      substanceusenotes: [null],
      criminalbehavrating: [null],
      criminalbehavnotes: [null],
      sexabuserating: [null],
      sexabusenotes: [null],
      phyabuserating: [null],
      phyabusenotes: [null],
      emotionalabuserating: [null],
      emotionalabusenotes: [null],
      neglectrating: [null],
      neglectnotes: [null],
      medtraumarating: [null],
      medtraumanotes: [null],
      familyvoilancerating: [null],
      familyvoilancenotes: [null],
      communityvoilancerating: [null],
      communityvoilancenotes: [null],
      schoolvoilancerating: [null],
      schoolvoilancenotes: [null],
      disasterrating: [null],
      disasternotes: [null],
      waraffectedrating: [null],
      waraffectednotes: [null],
      terroraffectedrating: [null],
      terroraffectednotes: [null],
      criminalactivityrating: [null],
      criminalactivitynotes: [null],
      disruptionrating: [null],
      disruptionnotes: [null],
      famchildneeds: [null],
      famchildneedsrating: [null],
      famchildneedsnotes: [null],
      serviceopt: [null],
      serviceoptrating: [null],
      serviceoptnotes: [null],
      responsibilities: [null],
      responsibilitiesrating: [null],
      responsibilitiesnotes: [null],
      listening: [null],
      listeningrating: [null],
      listeningnotes: [null],
      communication: [null],
      communicationrating: [null],
      communicationnotes: [null],
      naturesupport: [null],
      naturesupportrating: [null],
      naturesupportnotes: [null],
      youthliving: [null],
      youthlivingrating: [null],
      youthlivingnotes: [null],
      youtheducation: [null],
      youtheducationrating: [null],
      youtheducationnotes: [null],
      servicearrange: [null],
      servicearrangerating: [null],
      servicearrangenotes: [null],
      Psychosisrating: [null],
      Psychosisnotes: [null],
      AttnDeficitImpulseControlrating: [null],
      AttnDeficitImpulseControlnotes: [null],
      DepressionMoodDisorderrating: [null],
      DepressionMoodDisordernotes: [null],
      Anxietyrating: [null],
      Anxietynotes: [null],
      OppositionalBehaviorrating: [null],
      OppositionalBehaviornotes: [null],
      ConductAntisocialBehaviorrating :[null],
      ConductAntisocialBehaviornotes: [null],
      SubstanceAbuserating: [null],
      SubstanceAbusenotes: [null],
      EatingDisturbancerating: [null],
      EatingDisturbancenotes: [null],
      AngerControlrating: [null],
      AngerControlnotes: [null],
      AttachmentDifficultiesrating: [null],
      AttachmentDifficultiesnotes: [null],
      SuicideRiskrating: [null],
      SuicideRisknotes: [null],
      SelfInjuriousBehaviorsrating: [null],
      SelfInjuriousBehaviorsnotes: [null],
      RecklessBehaviorsrating: [null],
      RecklessBehaviorsnotes: [null],
      DangertoOthersrating: [null],
      DangertoOthersnotes: [null],
      SexualAggressionrating: [null],
      SexualAggressionnotes: [null],
      SexuallyReactiveBehaviorsrating: [null],
      SexuallyReactiveBehaviornotes: [null],
      runawayrating: [null],
      runawaynotes: [null],
      DelinquentBehaviorrating: [null],
      DelinquentBehaviornotes: [null],
      firesettingrating: [null],
      firesettingnotes: [null],
      IntentionalMisbehaviorrating: [null],
      IntentionalMisbehaviornotes: [null],
      bullyingrating: [null],
      bullyingnotes: [null],
      Exploitedrating: [null],
      Exploitednotes: [null],
      addl_crgvr_advcy_info: [''],
      addl_crgvr_info_pmsl: ['']
    });
    this.caregiverForm.setControl('contactcaregivers', this._formBuilder.array([]));
    this.caregiverForm.setControl('contactprofesssionalsupcaregivers', this._formBuilder.array([]));
  }

  showEditCareGiver(mode: number, model, index?: number) {
    if (mode === 2) {
      this.caregiverFormList.splice(index, 1);
      return true;
    }
    if (mode === 3) {
      this.removeAddedlegals();
    }
    this.caregiverForm.enable();
    this.showCareGiver = true;
    this.caregiverMode = mode;
    if (model) {
      this.caregiverlegalGuardian = this.caregiverDdList;
      this.caregiverForm.patchValue(model);
      this.caregiverForm.setControl('contactcaregivers', this._formBuilder.array([]));
      this.caregiverForm.setControl('contactprofesssionalsupcaregivers', this._formBuilder.array([]));
      if (this.cansFData && this.cansFData.submissiondata ) { //Non-migrated data caregiver contact
      if (model.contactcaregivers && model.contactcaregivers.length > 0) {
        model.contactcaregivers.forEach(data => {
          this.addcaregiverContact(data);
        });
      }
      if (model.contactprofesssionalsupcaregivers && model.contactprofesssionalsupcaregivers.length > 0) {
        model.contactprofesssionalsupcaregivers.forEach(data => {
          this.addprofesssionalsupcaregiverContact(data);
        });
      }
      }
      this.caregiverindex = index;
      if (mode === 0) {
        this.caregiverForm.disable();
      }
    } else {
      this.caregiverForm.reset();
    }
  }

  addcaregiverContact(modal) {
    const control = <FormArray>this.caregiverForm.controls['contactcaregivers'];
    control.push(this.createcareGiver(modal));
  }
  deletecaregiverContact(index: number) {
    const control = <FormArray>this.caregiverForm.controls['contactcaregivers'];
    control.removeAt(index);
  }

  addprofesssionalsupcaregiverContact(modal) {
    const control = <FormArray>this.caregiverForm.controls['contactprofesssionalsupcaregivers'];
    control.push(this.createcareGiver(modal));
  }
  deleteprofesssionalsupcaregiverContact(index: number) {
    const control = <FormArray>this.caregiverForm.controls['contactprofesssionalsupcaregivers'];
    control.removeAt(index);
  }

  private createcareGiver(modal) {
    return this._formBuilder.group({
      name: modal.name ? modal.name : null,
      address: modal.address ? modal.address : null,
      phoneno: modal.phoneno ? modal.phoneno : null,
      relationship: modal.relationship ? modal.relationship : null
    });
  }

  saveCaregiver() {
    const caregiverform = this.caregiverForm.getRawValue();
    caregiverform.formStatus = this.caregiverForm.valid;
    if (this.caregiverMode && this.caregiverMode === 1) {
      this.caregiverFormList[this.caregiverindex] = caregiverform;
    } else {
      this.caregiverFormList.push(caregiverform);
    }
    this.showCareGiver = false;
    this.removeAddedlegals();
    this.saveForm();
  }

  removeAddedlegals() {
    if (this.caregiverFormList && this.caregiverFormList.length > 0) {
      this.caregiverFormList.forEach(data => {
        this.caregiverlegalGuardian = this.caregiverlegalGuardian.filter(ele => ele.intakeservicerequestactorid !== data.caregiverlist);
      });
    } else {
      this.caregiverlegalGuardian = this.caregiverDdList;
    }
  }

  cancelCareGiver() {
    this.showCareGiver = false;
  }

  onChangeDisplay(event, type) {
    if (type == 'family_own_rent') {
      if (event.value == 3) {
        this.familyOther = true;
      } else {
        this.familyOther = false;
      }
    } else {
      if (event.value == 1) {
        if (type == 'caregiver_insurance') {
          this.Provider = true;
        }
        if (type == 'children_insurance') {
          this.cProvider = true;
        }
        if (type == 'family_needs_assistance') {
          this.otherbox = true;
        }
      } else {
        if (type == 'family_needs_assistance') {
          this.otherbox = false;
        }
        if (type == 'children_insurance') {
          this.cProvider = false;
        }
        if (type == 'caregiver_insurance') {
          this.Provider = false;
        }
      }
    }
  }

  //Move these to the common service
  getRoutingSupervisors() {
    return this.routingSupervisors; 
  }

  getRoutingInfo() {
    return this.routingInfo;
  }

  // Child
  initializeChildForm() {
    this.childForm = this._formBuilder.group({
      childlist: [null],
      mother: [null],
      motherrating: [null],
      mothernotes: [null],
      father: [null],
      fatherrating: [null],
      fathernotes: [null],
      pricaregiver: [null],
      pricaregiverrating: [null],
      pricaregivernotes: [null],
      adults: [null],
      adultsrating: [null],
      adultsnotes: [null],
      siblings: [null],
      siblingsrating: [null],
      siblingsnotes: [null],
      medical: [null],
      medicalrating: [null],
      medicalnotes: [null],
      iq: [null],
      iqrating: [null],
      iqnotes: [null],
      autism: [null],
      autismrating: [null],
      autismnotes: [null],
      speech: [null],
      speechrating: [null],
      speechnotes: [null],
      social: [null],
      socialrating: [null],
      socialnotes: [null],
      schoolatd: [null],
      schoolatdrating: [null],
      schoolatdnotes: [null],
      schoolachiv: [null],
      schoolachivrating: [null],
      schoolachivnotes: [null],
      schoolbehv: [null],
      schoolbehvrating: [null],
      schoolbehvnotes: [null],
      mentalhealthrating: [null],
      riskbehaviour: [null],
      adjtotraumarating: [null],
      adjtotraumanotes: [null],
      sexabuserating: [null],
      sexabusenotes: [null],
      phyabuserating: [null],
      phyabusenotes: [null],
      emotionalabuserating: [null],
      emotionalabusenotes: [null],
      neglectrating: [null],
      neglectnotes: [null],
      medicaltraumarating: [null],
      medicaltraumanotes: [null],
      familyvoilancerating: [null],
      familyvoilancenotes: [null],
      communityvoilancerating: [null],
      communityvoilancenotes: [null],
      schoolvoilancerating: [null],
      schoolvoilancenotes: [null],
      disasterrating: [null],
      disasternotes: [null],
      waraffectedrating: [null],
      waraffectednotes: [null],
      terroraffectedrating: [null],
      terroraffectednotes: [null],
      criminalactivityrating: [null],
      criminalactivitynotes: [null],
      Psychosisrating: [null],
      Psychosisnotes: [null],
      AttnDeficitImpulseControlrating: [null],
      AttnDeficitImpulseControlnotes: [null],
      DepressionMoodDisorderrating: [null],
      DepressionMoodDisordernotes: [null],
      Anxietyrating: [null],
      Anxietynotes: [null],
      OppositionalBehaviorrating: [null],
      OppositionalBehaviornotes: [null],
      ConductAntisocialBehaviorrating :[null],
      ConductAntisocialBehaviornotes: [null],
      SubstanceAbuserating: [null],
      SubstanceAbusenotes: [null],
      EatingDisturbancerating: [null],
      EatingDisturbancenotes: [null],
      AngerControlrating: [null],
      AngerControlnotes: [null],
      AttachmentDifficultiesrating: [null],
      AttachmentDifficultiesnotes: [null],
      SuicideRiskrating: [null],
      SuicideRisknotes: [null],
      SelfInjuriousBehaviorsrating: [null],
      SelfInjuriousBehaviorsnotes: [null],
      RecklessBehaviorsrating: [null],
      RecklessBehaviorsnotes: [null],
      DangertoOthersrating: [null],
      DangertoOthersnotes: [null],
      SexualAggressionrating: [null],
      SexualAggressionnotes: [null],
      SexuallyReactiveBehaviorsrating: [null],
      SexuallyReactiveBehaviornotes: [null],
      runawayrating: [null],
      runawaynotes: [null],
      DelinquentBehaviorrating: [null],
      DelinquentBehaviornotes: [null],
      firesettingrating: [null],
      firesettingnotes: [null],
      IntentionalMisbehaviorrating: [null],
      IntentionalMisbehaviornotes: [null],
      bullyingrating: [null],
      bullyingnotes: [null],
      Exploitedrating: [null],
      Exploitednotes: [null],
      addl_child_info:['']
    });
  }

  loadChildList() {
    this.initializeChildForm();
    this.childList = this.childListOriginal;
    if (this.childList && this.childList.length > 0) {
      this.childList.forEach(data => {
        this.childListName[data.intakeservicerequestactorid] = data.fullname;
      });
    }
    this.removeAddedchild();
    this.showChild = false;
  }

  removeAddedchild() {
    if (this.childFormList && this.childFormList.length > 0) {
      this.childFormList.forEach(data => {
        this.childList = this.childList.filter(ele => ele.intakeservicerequestactorid !== data.childlist);
      });
    } else {
      this.childList = this.childListOriginal;
    }
  }

  showEditChild(mode: number, model, index?: number) {
    if (mode === 2) {
      this.childList = this.childListOriginal;
      this.childFormList.splice(index, 1);
      return true;
    }
    if (mode === 3) {
      this.removeAddedchild();
    }
    this.childForm.enable();
    this.showChild = true;
    this.childMode = mode;
    if (model) {
      this.childList = this.childListOriginal;
      this.childForm.patchValue(model);
      this.childindex = index;
      if (mode === 0) {
        this.childForm.disable();
      }
    } else {
      this.childForm.reset();
    }
  }

  cancelChildFormFamilyQuestion(){
    this.childFormFamilyQuestion.reset();
  }

  cancelchild() {
    this.showChild = false;
  }

  saveChild() {
    const childform = this.childForm.getRawValue();
    childform.formStatus = this.childForm.valid;
    if (this.childMode && this.childMode === 1) {
      this.childFormList[this.childindex] = childform;
    } else {
      this.childFormList.push(childform);
    }
    this.showChild = false;
    this.removeAddedchild();
    this.saveForm();
  }

    // familyYouth 
    // familyAssessmentYouth
    // careGiver-- array
    // familyCultureYouth
    // child -- array
    // authorizationApproval
    //TODO: careGiver, child -- check if each form control in these array lists are also VALID 
  isReadyForApproval(){
    this.incompleteList = [];
    if (!this.familyYouth.valid) {
      this.incompleteList.push('FAMILY AND YOUTH INFORMATION');
    }
    if (!this.familyAssessmentYouth.valid) {
      this.incompleteList.push('COMPREHENSIVE FAMILY ASSESSMENT');
    }
    // Validation check added in Tab3
    if (this.caregiverFormList.length > 0) {
      const formStatus = this.caregiverFormList.filter(data => !data.formStatus);
      if (formStatus && formStatus.length > 0) {
        this.incompleteList.push('COMPREHENSIVE CAREGIVER ASSESSMENT');
      }
    } else {
      this.incompleteList.push('COMPREHENSIVE CAREGIVER ASSESSMENT');
    }
    if (!this.familyCultureYouth.valid) {
      this.incompleteList.push('FAMILY CULTURE ASSESSMENT');
    }
    // Validation check added in Tab5
    if (this.childFormList.length > 0) {
      const formStatus = this.childFormList.filter(data => !data.formStatus);
      if (formStatus && formStatus.length > 0) {
        this.incompleteList.push('COMPREHENSIVE CHILD ASSESSMENT');
      }
    } else {
      this.incompleteList.push('COMPREHENSIVE CHILD ASSESSMENT');
    }
    if (!this.authorizationApproval.valid) {
      this.incompleteList.push('APPROVAL');
    }
    if (this.incompleteList.length == 0) {
      this.submitForApproval();
    } else {
      (<any>$('#incomplete-items')).modal('show');
    }
  }
  

}
