import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseWorkerViewAssessmentComponent } from './case-worker-view-assessment.component';

describe('CaseWorkerViewAssessmentComponent', () => {
  let component: CaseWorkerViewAssessmentComponent;
  let fixture: ComponentFixture<CaseWorkerViewAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseWorkerViewAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseWorkerViewAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
