import { Location } from '@angular/common';
import * as moment from 'moment';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import FormioExport from 'formio-export';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../../../../environments/environment';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { AlertService, AuthService, CommonHttpService, DataStoreService, SessionStorageService, CommonDropdownsService } from '../../../../../@core/services';
import { HttpService } from '../../../../../@core/services/http.service';
import { DSDSActionSummary, GetintakAssessment, RoutingInfo } from '../../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { ChildRoles } from '../../child-removal/_entities/childremoval.model';
import { InvolvedPerson, YouthInvolvedPersons } from '../../involved-persons/_entities/involvedperson.data.model';
import { Placement } from '../../service-plan/_entities/service-plan.model';
import { AssessmentPreFill } from '../assessment-prefill';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var Formio: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'case-worker-view-assessment',
    templateUrl: './case-worker-view-assessment.component.html',
    styleUrls: ['./case-worker-view-assessment.component.scss']
})
export class CaseWorkerViewAssessmentComponent implements OnInit, AfterViewInit, AfterViewChecked {
    id: string;
    serviceCaseId: string;
    daNumber: string;
    assessmmentName: string;
    isAppla = false;
    isShelterForm = false;
    private token: AppUser;
    private servicereqid: string;
    private templateId: string;
    private submissionId: string;
    selectedSafeCDangerInfluence: any[] = [];
    currentTemplateId: string;
    isChildSafe = true;
    safeCKeys: string[];
    involvedPersons: InvolvedPerson[];
    youthInvolvedPersons: YouthInvolvedPersons[];
    routingInfo: RoutingInfo[];
    routingSupervisors: any[];
    placement: Placement[];
    childDetail: ChildRoles[];
    formioOptions: {
        formio: {
            ignoreLayout: true;
            emptyValue: '-';
        };
    };
    isReadOnlyForm = false;
    intakAssessment = new GetintakAssessment();
    dsdsActionsSummary = new DSDSActionSummary();
    isInitialized = false;
    private formTriggered = false;
    childRemovalInfo: any;
    baseLocation: string;
    intakeFormDRAIData = null;
    isServiceCase: string;
    placementInfo: any;
    eductionDetatils: any[];
    personRelations: any;
    removalChildList: any;
    constructor(
        private route: ActivatedRoute,
        private _authService: AuthService,
        private storage: SessionStorageService,
        private _commonService: CommonHttpService,
        private _alertService: AlertService,
        private _http: HttpService,
        private _router: Router,
        private _dataStoreService: DataStoreService,
        private _changeDetect: ChangeDetectorRef,
        private location: Location,
        private _commonDDService: CommonDropdownsService
    ) {
        this.safeCKeys = [
            'caregiverdescribes',
            'caregiverfailstoprotect',
            'caregivermadeaplausible',
            'caregiverrefuses',
            'caregiversemotionalinstability',
            'caregiversexplanation',
            'caregiversjustification',
            'caregiverssuspected',
            'childscurrentimminent',
            'childsexualabuse',
            'childswhereabouts',
            'currentactofmaltreatment',
            'domesticviolence',
            'extremelyanxious',
            'multiplereports',
            'servicestothecaregiver',
            'specialneeds',
            'unabletoprotect',
            'servicestothecaregiver2'
        ];
        this.id = this._commonDDService.getStoredCaseUuid();

        this.daNumber = this._commonDDService.getStoredCaseNumber();
        this._commonDDService.getCountyList('MD').subscribe(data => {
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.COUNTY_LIST, data);
        });
    }

    ngOnInit() {
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        if (this.isServiceCase) {
            this.serviceCaseId = this._commonDDService.getStoredCaseUuid();
        } else {
            this.serviceCaseId = null;
        }
        this.token = this._authService.getCurrentUser();
        this.isInitialized = true;
        if (this._authService.isDJS()) {
            this.intakeFormDRAIData = this._dataStoreService.getData('DRAI_PREFILL');
        }
    }

    ngAfterViewInit() {
        /*this._dataStoreService.currentStore.subscribe(store => {
           console.log('check store....', store);
       });*/
          const storeData = this._dataStoreService.getCurrentStore();
            if (this.isInitialized ) {
                this.involvedPersons = this._dataStoreService.getData('CASEWORKER_INVOLVED_PERSON');
                this.personRelations = this._dataStoreService.getData('CASEWORKER_PERSON_RELATIONS');
                this.youthInvolvedPersons = this._dataStoreService.getData('CASEWORKER_YOUTH_INVOLVED_PERSON');
                this.routingInfo = this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') ? this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') : [];
                this.routingSupervisors = this._dataStoreService.getData('CASEWORKER_SUPERVISORS') ? this._dataStoreService.getData('CASEWORKER_SUPERVISORS') : [];
                this.placement = this._dataStoreService.getData('CASEWORKER_PLACEMENT');
                this.placementInfo = this._dataStoreService.getData('CASEWORKER_PLACEMENT_INFO');
                this.intakAssessment = this._dataStoreService.getData('CASEWORKER_SELECTED_ASSESSMENT');
                this.childDetail = this._dataStoreService.getData('CASEWORKER_CHILD_DETAIL');
                this.childRemovalInfo = this._dataStoreService.getData('CHILD_REMOVAL_INFO');
                this.eductionDetatils = this._dataStoreService.getData('CASEWORKER_EDUCATION_INFO');
                this.removalChildList = this._dataStoreService.getData('REMOVAL_CHILD_LIST');
                console.log(this.removalChildList);
                if (this.intakAssessment) {
                    if (this.intakAssessment.description === 'Shelter Care Authorization and Date of Hearing' || this.intakAssessment.name === 'shelterCareAuthorizationAndDateOfHearing') {
                        this.isShelterForm = true;
                    }
                    if (this.intakAssessment.mode === 'start') {
                        if (this.childRemovalInfo) {
                            this.startAssessment(this.intakAssessment, this.childRemovalInfo);
                        } else {
                            this.startAssessment(this.intakAssessment);
                        }
                    } else if (this.intakAssessment.mode === 'submit') {
                        this.submittedAssessment(this.intakAssessment);
                    } else if (this.intakAssessment.mode === 'update') {
                        if (this.childRemovalInfo) {
                            this.updateAssessment(this.intakAssessment, this.childRemovalInfo);
                        } else {
                            this.updateAssessment(this.intakAssessment);
                        }
                    } else if (this.intakAssessment.mode === 'print') {
                        this.assessmentPrintView(this.intakAssessment);
                    }
                } else {
                    this.redirectToAssessment();
                }
                this.isInitialized = false;
            }
       // });
        const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
        if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('i')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
            (<any>$('th a')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
        }
    }
    ngAfterViewChecked(): void {
        this._changeDetect.detectChanges();
    }

    // getChildArray() {
    //     const CHILD_CATEGORIES = ['CHILD', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC', 'AV'];
    //     return this.involvedPersons.filter(child => {
    //         let childFound = false;
    //         child.roles.forEach(role => {
    //             const childCategory = CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
    //             if (childCategory) {
    //                 childFound = true;
    //                 return;
    //             }
    //         });
    //         return childFound;
    //     });
    // }
    // getRemovalChilds(childArray) {
    //     let childList = [];
    //     childList = this.involvedPersons.map(person => {
    //     childArray.forEach(child => {
    //         if (child.intakeservicerequestactorid  === person.intakeservicerequestactorid) {
    //             return {
    //                 'childname': person.firstname + ' ' + person.lastname,
    //                 'clientid': person.cjamspid,
    //                 'age': this.getAge(person.dob)
    //             };
    //         }
    //       });
    //     });
    //    return childList ? childList : [];
    // }
    startAssessment(assessment, childRemovalInfo?) {
        this.assessmmentName = assessment.description;
        this.currentTemplateId = assessment.external_templateid;
        if (assessment.description === 'APPLA') {
            this.isAppla = true;
        }

        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`, {
            hooks: {
                beforeSubmit: (submission, next) => {
                    console.log('beforeSubmit', submission);
                    if (!Formio.token) {
                        Formio.token = _self.storage.getObj('fbToken');
                    }
                    next();
                }
            }
        }).then(function (form) {
            // form.url = 'admin/assessment/Add';
            // form.url = 'https://d2n2hxi1es473f.cloudfront.net/api/admin/assessment/Add';
            // form.url = environment.formBuilderHost + `/form/${assessment.external_templateid}`;
            form.nosubmit = false;
            form.components = form.components.map(item => {
                if (item.key === 'Complete' && item.type === 'button') {
                    item.action = 'submit';
                }
                return item;
            });


            form.submission = {
                data: _self.getFormPrePopulation(_self.assessmmentName, form.data)
            };
            form.on('saveState', submission => {
                console.log('saving as draft');
            }),
            form.on('submit', submission => {
                let assessmentactor = {};
                if (_self.assessmmentName === 'SAFE-C') {
                    submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
                    submission.data['isChildSafe'] = _self.isChildSafe;
                    // submission.data['servicecaseid'] = null;
                    submission.data['assessmentactor'] = _self.processChildDetails(submission);
                    assessmentactor = _self.processChildDetails(submission);
                }
                if (assessment.description === 'APPLA') {
                    submission.data['child'] = _self._dataStoreService.getData('placed_child');
                }
                let status = '';
                let comments = '';
                if (_self.token.role.name === 'apcs') {
                    status = submission.data.assessmentstatus;
                    comments = submission.data.supervisorcomments;
                } else if (_self.token.role.name === 'field') {
                    if (_self._authService.isDJS()) {
                        if (submission.data.submit) {
                            status = 'InProcess';
                        } else {
                            status = 'Submitted';
                        }
                        comments = submission.data.caseworkercomments;
                    } else {
                        if (submission.data.submit) {
                            status = 'InProcess';
                        } else {
                            status = submission.data.assessmentreviewed;
                        }
                        comments = submission.data.caseworkercomments;
                    }
                }
                _self._http
                    .post('admin/assessment/Add', {
                        externaltemplateid: _self.currentTemplateId,
                        objectid: _self.id,
                        submissionid: submission._id,
                        submissiondata: submission.data ? submission.data : null,
                        form: submission.form ? submission.form : null,
                        score: submission.data.score ? submission.data.score : 0,
                        ischildsafe: _self.isChildSafe,
                        assessmentstatustypekey1: status,
                        assessmentactor: assessmentactor ? assessmentactor : null,
                        servicecaseid: _self.serviceCaseId,
                        comments: comments
                    })
                    .subscribe(response => {
                        _self._alertService.success(_self.assessmmentName + ' saved successfully.', true);
                        _self.redirectToAssessment();
                        /// (<any>$('#iframe-popup')).modal('hide');
                        if (_self.assessmmentName === 'SAFE-C') {
                           /*  Observable.timer(500).subscribe(() => {
                                _self._router.routeReuseStrategy.shouldReuseRoute = function () {
                                    return false;
                                };
                                _self._router.navigateByUrl(_self._router.url).then(() => {
                                    _self._router.navigated = true;
                                    _self._router.navigate([_self._router.url]);
                                });
                            }); */
                        } else {
                            if (_self.assessmmentName === 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT') {
                                const participantDetail = [];
                                const reportedChild = _self.involvedPersons.filter(item => {
                                    return item.rolename === 'RC' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RC').length);
                                });
                                let reportedChildName = '';
                                if (reportedChild.length) {
                                    reportedChildName = reportedChild[0].lastname;
                                    if (reportedChild[0].firstname) {
                                        reportedChildName   += ', ' + reportedChild[0].firstname;
                                    }
                                }
                                _self.involvedPersons.map(item => {
                                    if (item.rolename !== 'RC' && item.rolename !== 'AM') {
                                        participantDetail.push({
                                            objectid: _self.id,
                                            objecttypekey: 'servicerequest',
                                            personid: item.personid,
                                            relationship: item.relationship,
                                            email: item.email,
                                            firstname: item.firstname,
                                            lastname: item.lastname,
                                            message:
                                                'This is to notify you that the child ' +
                                                reportedChildName.toUpperCase() +
                                                ' is being enrolled in ' +
                                                submission.data.selectedcurrentschool +
                                                ' school based on the best interest assessment for education'
                                        });
                                    }
                                });
                                _self._commonService.create(participantDetail, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.Notification).subscribe(res => {
                                    console.log(res);
                                });
                            }
                        }
                        if (_self.assessmmentName === 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT') {
                            const reportedChild = _self.involvedPersons.filter(item => {
                                return item.rolename === 'RC' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RC').length);
                            });
                            _self.baseLocation = _self.location['_platformStrategy']['_platformLocation']['location'];
                            const protocol = _self.baseLocation['protocol'];
                            const host = _self.baseLocation['host'];
                            console.log(protocol + ' - ' + host);
                            const participantDetail = [];
                            participantDetail.push(
                                Object.assign({
                                    objectid: submission._id,
                                    objecttypekey: 'assessment',
                                    email: submission.data['sendemailid'],
                                    message: `${protocol}//${host}/#/external-assessment/${_self.id}/review/${_self.currentTemplateId}/${submission._id}`,
                                    url: `${protocol}//${host}/#/external-assessment/${_self.id}/review/${_self.currentTemplateId}/${submission._id}`
                                })
                            );
                            if (participantDetail) {
                                _self._commonService.create(participantDetail, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.EmailSchoolNotification).subscribe(res => {
                                    console.log(res);
                                });
                            }
                        }
                    });
            });
            form.on('change', formData => {
                _self.safeCProcess(formData);
                console.log('-----', formData);


                // Begin - D-06446 , D-06602 Fix
                if (formData.changed) {
                    /// D-10437 Start
                    const changedKey = formData.changed.component.key;

                    if (_self.assessmmentName === 'CANS-OUT OF HOME PLACEMENT SERVICE') {
                        console.log('Onchanges COOH', changedKey);
                    }
                    let childsInfo: any;
                    if (form.submission.data.childs_info_json) {
                        childsInfo   = JSON.parse(form.submission.data.childs_info_json);
                    }
                    if (_self.assessmmentName === 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT') {
                        if (changedKey === 'studentname') {
                            const studentname = form.submission.data.studentname;
                            childsInfo.forEach((childInfo) => {
                            if (childInfo.studentname === studentname) {
                                form.submission.data.studentdob = childInfo.dob;

                                if (childInfo.education) {
                                    form.submission.data.currentschool = null;
                                    form.submission.data.grade = null;
                                    form.submission.data.assignedstudent = null;
                                    form.submission.data.previousschool = null;
                                    for (let i = 0; i < childInfo.education.length; i++) {
                                        if (childInfo.education[i].schooltype === 'current') {
                                            form.submission.data.currentschool = childInfo.education[i].educationname;
                                            form.submission.data.grade = childInfo.education[i].currentgradeleveldesc;
                                            form.submission.data.assignedstudent = childInfo.education[i].sasidno;
                                        } else if (childInfo.education[i].schooltype === 'Previous') {
                                            form.submission.data.previousschool = childInfo.education[i].educationname;
                                        } else {
                                            form.submission.data.previousschool = null;
                                            form.submission.data.currentschool = null;
                                            form.submission.data.grade = null;
                                            form.submission.data.assignedstudent = null;
                                        }
                                    }
                                }  else {
                                    form.submission.data.previousschool = null;
                                    form.submission.data.currentschool = null;
                                    form.submission.data.grade = null;
                                    form.submission.data.assignedstudent = null;
                                }
                                }
                            });
                        }
                    }
                    if (_self.assessmmentName === 'SAFE-C OHP' ) {
                        if (changedKey === 'ClientName') {
                            const childName = form.submission.data.ClientName;
                            childsInfo.forEach((childInfo) => {
                                if (childInfo.childname === childName) {
                                    form.submission.data.dob = childInfo.dob;
                                    form.submission.data.clientid = childInfo.clientid;
                                    form.submission.data.currentplacement = childInfo.hasActivePlacement;
                                    const providerInfo = (childInfo.providerInfo) ? childInfo.providerInfo : null;
                                    form.submission.data['placementlivingarrangement'] = (providerInfo && providerInfo.providername) ? providerInfo.providername : null;
                                    form.submission.data['addressline1'] = (providerInfo && providerInfo.adr_street_nm) ? providerInfo.providername : null;
                                    form.submission.data['addressline2'] = (providerInfo && providerInfo.adr_street_tx) ? providerInfo.adr_street_tx : null;
                                    form.submission.data['zipcode'] = (providerInfo && providerInfo.adr_zip5_no) ? providerInfo.adr_zip5_no : null;
                                    form.submission.data['ext'] = '';
                                    form.submission.data['fax'] = '';
                                    form.submission.data['work'] = (providerInfo && providerInfo.phonenumber) ? providerInfo.phonenumber : null;
                                }
                            });
                        }

                        if (changedKey === 'currentplacement' || ( changedKey === 'ClientName' && form.submission.data.currentplacement) ) {
                            const currentStore = _self._dataStoreService.getCurrentStore();
                            if (currentStore) {
                            _self.placementInfo = currentStore['CASEWORKER_PLACEMENT_INFO'];
                            // this.placement = this.placement.filter(item => !item.placeenddatetime && item.role === 'RC');
                            }

                            console.log('current store', _self._dataStoreService.getCurrentStore());
                            console.log(_self.placementInfo);
                            const childName = form.submission.data.ClientName;
                          if (form.submission.data.currentplacement) {
                            childsInfo.forEach((childInfo) => {
                                if (childInfo.childname === childName ) {
                                    const providerInfo = childInfo.providerInfo;
                                    if (providerInfo) {
                                        form.submission.data['currentplacement'] = true;
                                        form.submission.data['placementlivingarrangement'] = providerInfo.providername;
                                        form.submission.data['addressline1'] = providerInfo.adr_street_nm;
                                        form.submission.data['addressline2'] = providerInfo.adr_street_tx;
                                        form.submission.data['zipcode'] = providerInfo.adr_zip5_no;
                                        form.submission.data['ext'] = '';
                                        form.submission.data['fax'] = '';
                                        form.submission.data['work'] = providerInfo.phonenumber;
                                    }                                  }
                              });

                          } else {
                            form.submission.data.placementlivingarrangement = null;
                            form.submission.data.addressline1 = null;
                            form.submission.data.addressline2 = null;
                            form.submission.data.zipcode = null;
                            form.submission.data.work = null;
                            form.submission.data.ext = null;
                            form.submission.data.fax = null;
                            // form.submission.data.staffmember = null;
                          }

                        }
                    }
                    if (changedKey === 'childname' && _self.assessmmentName === 'CANS-OUT OF HOME PLACEMENT SERVICE') {
                        const childName = form.submission.data.childname;
                        childsInfo.forEach((childInfo) => {
                          if (childInfo.childname === childName) {
                            form.submission.data.age = childInfo.age;
                            }
                        });
                    } else if (changedKey === 'childname') {
                        childsInfo.forEach((childInfo) => {
                            form.submission.data.childdatagrid.forEach((childdata) => {
                                if (childInfo.childname === childdata.childname) {
                                    childdata.age = childInfo.age ;
                                    childdata.clientid = childInfo.clientid ;
                                }
                            });
                        });
                    }

                    let childsInfoWithComma: any;
                    if (form.submission.data.child_info_with_comma_json) {
                        childsInfoWithComma  = JSON.parse(form.submission.data.child_info_with_comma_json);
                    }
                    if (changedKey === 'seconename') {
                        childsInfoWithComma.forEach((childInfo) => {
                            form.submission.data.addchildren.forEach((childdata) => {
                                if (childInfo.seconename === childdata.seconename) {
                                    childdata.seconeage = childInfo.seconeage;
                                }
                            });
                        });
                    }

                    if (changedKey === 'childerenname') {
                        const addedChildren  = JSON.parse(form.submission.data.childrenhiddendetails);
                        if (form.submission.data.childrendetails) {
                            form.submission.data.childrendetails.forEach(child => {
                                const changedChild = addedChildren.find(c => c.childerenname === child.childerenname);
                                if (changedChild) {
                                    child.DOB = changedChild.DOB;
                                }
                            });
                        }
                    }

                    /// D-10437 End

                    if (_self.assessmmentName === 'CANS-OUT OF HOME PLACEMENT SERVICE') {

                        form.submission = {
                            data: formData.data // _self.setChildDetails(formData)
                        };

                    }
                    if (_self.assessmmentName === 'SAFE-C OHP') {

                        form.submission = {
                            data: formData.data // _self.setChildDetails(formData)
                        };

                    }
                    if (_self.assessmmentName === 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT') {

                        form.submission = {
                            data: formData.data // _self.setChildDetails(formData)
                        };

                    }

                   if (_self.assessmmentName === 'MARYLAND FAMILY RISK REASSESSMENT') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'SILA') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'MFIRA') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'NOTIFICATION OF PLACEMENT-ENTRY AND EXIT RECEIPT') {

                        form.submission = {
                            data: formData.data
                        };
                        let childsInfo: any;
                        if (form.submission.data.child_info_json) {
                            childsInfo  = JSON.parse(form.submission.data.child_info_json);
                        }
                        if (changedKey === 'childsName') {
                            const childName = form.submission.data.childsName;
                            const child = childsInfo.find(ele => ele.childname === childName);
                            if (child) {
                                form.submission.data.placemententrydatetime = '';
                                form.submission.data.placementexitdatetime = '';
                                form.submission.data.dob = child.dob;
                                form.submission.data.childClientId = child.cjamspid;
                                if (child.gender === 'Male' || child.gender === 'M') {
                                    form.submission.data.childgender = 'male';
                                }
                                if (child.gender === 'Female' || child.gender === 'F') {
                                    form.submission.data.childgender = 'female';
                                }
                                const minDateForStart = moment(child.removalDate).format('MM/DD/YYYY') || this._dataStoreService.getData('dsdsActionsSummary').case_opendate;
                                const startDate = (<any>$(`[name="data[placemententrydatetime]"]`)[0])._flatpickr;
                                startDate.set('minDate', new Date(minDateForStart));
                                const minDateForExit = form.submission.data.placemententrydatetime ||
                                    moment(child.removalDate).format('MM/DD/YYYY') || this._dataStoreService.getData('dsdsActionsSummary').case_opendate;
                                const exitDate = (<any>$(`[name="data[placementexitdatetime]"]`)[0])._flatpickr;
                                exitDate.set('minDate', new Date(minDateForExit));
                            } else {
                                form.submission.data.dob = '';
                                form.submission.data.childClientId = '';
                                form.submission.data.childgender = '';
                                const startDate = (<any>$(`[name="data[placemententrydatetime]"]`)[0])._flatpickr;
                                startDate.set('minDate', null);
                                startDate.set('maxDate', null);
                                const exitDate = (<any>$(`[name="data[placementexitdatetime]"]`)[0])._flatpickr;
                                exitDate.set('minDate', null);
                                form.submission.data.placemententrydatetime = '';
                                form.submission.data.placementexitdatetime = '';
                            }
                        }

                        if (changedKey === 'placemententrydatetime') {
                            const minDateForExit = form.submission.data.placemententrydatetime || this._dataStoreService.getData('dsdsActionsSummary').case_opendate;
                            const exitDate = (<any>$(`[name="data[placementexitdatetime]"]`)[0])._flatpickr;
                            exitDate.set('minDate', new Date(minDateForExit));
                        }

                        if (changedKey === 'placementexitdatetime') {
                            const maxDateForStart = form.submission.data.placementexitdatetime;
                            const startDate = (<any>$(`[name="data[placemententrydatetime]"]`)[0])._flatpickr;
                            startDate.set('maxDate', new Date(maxDateForStart));
                        }

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'HOME HEALTH REPORT') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }
                    if (_self.assessmmentName === 'DOMESTIC VIOLENCE LETHALITY SCREEN FOR DHS') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'AOD Form') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'CANS-OUT OF HOME PLACEMENT SERVICE') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'CANS-F' || _self.assessmmentName === 'cans-v2') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'SAFE-C OHP') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }
                    if (_self.assessmmentName === 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT') {

                        form.submission = {
                            data: formData.data
                        };

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }


                    if (_self.assessmmentName === 'SAFE-C') {

                        form.submission = {
                            data: formData.data // _self.setChildDetails(formData)
                        };

                        if (form.submission.data.dateoflastsafetyplan === '') {
                           // form.submission.data.dateoflastsafetyplan = moment(new Date()).format('YYYY-MM-DD hh:mm a');
                        }

                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor : origsupervisor.fromusername;

                            // submission.data['supervisorid'] = '';
                            // form.submission.data = Object.assign({
                            //     supervisorid: ''
                            // })
                            // var rertsupervisorname = form.submission.data.reroutesupervisor ? form.submission.data.reroutesupervisor : '';
                            // if (rertsupervisorname !== '') {
                            //     this.routingsupervisors.forEach(sup => {
                            //         if (sup.username === rertsupervisorname){
                            //             form.submission.data.supervisorid = sup.userid;
                            //         }
                            //     });
                            // } else {
                            //     form.submission.data.supervisorid = '';
                            // }
                        }

                       /* if (changedKey === 'assessmentreviewed') {
                            if (formData.changed.value === 'Review' || formData.data[formData.changed.component.key] === 'Review') {
                                form.submission.data.safetyassessmentcompletiondate = moment(new Date()).format('YYYY-MM-DD hh:mm a');
                            }
                            if (formData.changed.value === 'InProcess' || formData.data[formData.changed.component.key] === 'InProcess') {
                                form.submission.data.safetyassessmentcompletiondate = '';
                            }
                        } */

                        if (changedKey === 'dangerInfluencesIdentified' || changedKey === 'childisunsafe1'
                            || changedKey === 'childisunsafe2' || changedKey === 'childisunsafe3'
                            || changedKey === 'childisunsafe4') {
                            // form.submission.data.dateoflastsafetyplan = moment(new Date()).format('YYYY-MM-DD hh:mm a');
                        }
                    }


                }
                // End - D-06446 , D-06602 Fix
            });
            form.on('render', formData => {
                /// (<any>$('#iframe-popup')).modal('show');
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });

            form.on('error', error => {
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
                _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
            });
        });
    }

    submittedAssessment(assessment: GetintakAssessment) {
        this.assessmmentName = assessment.titleheadertext;
        this.currentTemplateId = assessment.external_templateid;
        if (assessment.titleheadertext === 'APPLA') {
            this.isAppla = true;
        }
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        // this.assessmmentName = assessment.description;
        // this.currentTemplateId = assessment.external_templateid;
        const _self = this;
        // Formio.setToken(this.storage.getObj('fbToken'));
        // Formio.baseUrl = environment.formBuilderHost;
            const url = `admin/assessment/getassessmentform/${assessment.external_templateid}/submission/${assessment.submissionid}`;
            this._commonService.getSingle({}, url).subscribe(result => {
                /* Migration Fix */
                if (assessment.submissiondata === null) {
                    assessment.submissiondata = result;
                }
                const res = assessment.submissiondata;
                console.log(res);
                // return res;
                Formio.createForm(
                    document.getElementById('assessmentForm'),
                    environment.formBuilderHost + `/form/${assessment.external_templateid}`,
                    {
                        readOnly: true,
                        hooks: {
                            beforeSubmit: (submission, next) => {
                                console.log('beforeSubmit', submission);
                                if (!Formio.token) {
                                    Formio.token = _self.storage.getObj('fbToken');
                                }
                                next();
                            }
                        }
                    }
                ).then(function (form) {
                    form.data.childdatagrid = assessment.submissiondata.childdatagrid;
                    form.components = form.components.map(item => {
                        if (item.key === 'Complete' && item.type === 'button') {
                            item.action = 'submit';
                        }
                        return item;
                    });
                    res['userMode'] = 'view';
                    form.submission = {
                        data: res
                    };
                    form.submission.data.userMode = 'view';
                    /// (<any>$('#iframe-popup')).modal('show');
                    form.on('render', formData => {
                        setTimeout(function () {
                            $('#assessment-popup').scrollTop(0);
                        }, 200);
                    });
                });
            });
        // } else {
        //     Formio.setToken(this.storage.getObj('fbToken'));
        //     Formio.baseUrl = environment.formBuilderHost;
        //     Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}/submission/${assessment.submissionid}`, {
        //         readOnly: true,
        //         hooks: {
        //             beforeSubmit: (submission, next) => {
        //                 console.log('beforeSubmit', submission);
        //                 if (!Formio.token) {
        //                     Formio.token = _self.storage.getObj('fbToken');
        //                 }
        //                 next();
        //             }
        //         }
        //     }).then(function (submission) {
        //         submission.data.childdatagrid = assessment.submissiondata.childdatagrid;
        //         // const assessmentactor = assessment.submissiondata.assessmentactor;
        //         // submission.data.childdatagrid = this.getRemovalChilds(submission.data.submissiondata.assessmentactor);
        //         /// (<any>$('#iframe-popup')).modal('show');
        //         submission.on('render', formData => {
        //             setTimeout(function () {
        //                 $('#assessment-popup').scrollTop(0);
        //             }, 200);
        //         });
        //     });
        // }
    }

    updateAssessment(assessment: GetintakAssessment, childRemovalInfo?) {
        this.assessmmentName = assessment.titleheadertext;
        this.currentTemplateId = assessment.external_templateid;
        if (assessment.titleheadertext === 'APPLA') {
            this.isAppla = true;
        }
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        // this.assessmmentName = assessment.description;
        // this.currentTemplateId = assessment.external_templateid;
        const _self = this;
        // Formio.setToken(this.storage.getObj('fbToken'));
        // Formio.baseUrl = environment.formBuilderHost;
        const url = `admin/assessment/getassessmentform/${assessment.external_templateid}/submission/${assessment.submissionid}`;
        this._commonService.getSingle({}, url).subscribe(result => {
            const res = assessment.submissiondata;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`, {
            readOnly: false,
            hooks: {
                beforeSubmit: (submission, next) => {
                    console.log('beforeSubmit', submission);
                    if (!Formio.token) {
                        Formio.token = _self.storage.getObj('fbToken');
                    }
                    next();
                }
            }
        }).then(function (form) {
            form.components = form.components.map(item => {
                if (item.key === 'Complete' && item.type === 'button') {
                    item.action = 'submit';
                }
                return item;
            });
            form.submission = {
                data: _self.getFormPrePopulation(_self.assessmmentName, res)
            };
            if (!form.data.childdatagrid) {
                form.data.childdatagrid = assessment.submissiondata.childdatagrid;
            }
            const assessmentactor = assessment.submissiondata.assessmentactor;
            /// (<any>$('#iframe-popup')).modal('show');
            form.on('render', formData => {
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
            });
            form.on('submit', submission => {
                submission._id = assessment.submissionid;
                if (_self.assessmmentName === 'SAFE-C') {
                    submission.data['safeCDangerInfluence'] = _self.selectedSafeCDangerInfluence;
                }
                let status = '';
                let comments = '';
                if (_self.token.role.name === 'apcs') {
                    status = submission.data.assessmentstatus;
                    comments = submission.data.supervisorcomments;
                    // routing update to service
                    _self._commonService
                        .create(
                            {
                                objectid: _self.id,
                                eventcode: 'SPLR',
                                status: submission.data.assessmentstatus,
                                comments: submission.data.supervisorcomments,
                                notifymsg: submission.data.supervisorcomments,
                                routeddescription: submission.data.supervisorcomments,
                                assessmmentName: _self.assessmmentName
                            },
                            'routing/routingupdate'
                        )
                        .subscribe(
                            resp => {
                                console.log(resp);
                            },
                            err => {
                                console.log(err);
                            }
                        );
                } else if (_self.token.role.name === 'field') {
                    if (_self._authService.isDJS()) {
                        if (submission.data.submit) {
                            status = 'InProcess';
                        } else {
                            status = 'Submitted';
                        }
                        comments = submission.data.caseworkercomments;
                    } else {
                        if (submission.data.submit) {
                            status = 'InProcess';
                        } else {
                            status = submission.data.assessmentreviewed;
                            if (_self.assessmmentName === 'Risk Assessment Legacy') {
                                status = 'Review';
                            }
                        }
                        comments = submission.data.caseworkercomments;
                    }
                }
                _self._http
                    .post('admin/assessment/Add', {
                        externaltemplateid: _self.currentTemplateId,
                        objectid: _self.id,
                        submissionid: submission._id,
                        submissiondata: submission.data ? submission.data : null,
                        form: submission.form ? submission.form : null,
                        score: submission.data.score ? submission.data.score : 0,
                        assessmentstatustypekey1: status,
                        assessmentactor: assessmentactor ? assessmentactor : null,
                        servicecaseid: _self.serviceCaseId,
                        comments: comments
                    })
                    .subscribe(response => {
                        if (_self.assessmmentName === 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT') {
                            const reportedChild = _self.involvedPersons.filter(item => {
                                return item.rolename === 'RC' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RC').length);
                            });
                            _self.baseLocation = _self.location['_platformStrategy']['_platformLocation']['location'];
                            const protocol = _self.baseLocation['protocol'];
                            const host = _self.baseLocation['host'];
                            console.log(protocol + ' - ' + host);
                            const participantDetail = [];
                            participantDetail.push(
                                Object.assign({
                                    objectid: submission._id,
                                    objecttypekey: 'assessment',
                                    email: submission.data['sendemailid'],
                                    message: `${protocol}//${host}/#/external-assessment/${_self.id}/approved/${_self.currentTemplateId}/${submission._id}`,
                                    url: `${protocol}//${host}/#/external-assessment/${_self.id}/approved/${_self.currentTemplateId}/${submission._id}`
                                })
                            );
                            if (participantDetail) {
                                _self._commonService.create(participantDetail, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.EmailSchoolNotification).subscribe(resp => {
                                    console.log(resp);
                                });
                            }
                        }
                        _self._alertService.success(_self.assessmmentName + ' saved successfully.');
                        // _self.getPage(1);
                        // _self.showAssessment(
                        //     _self.showAssesment,
                        //     _self.getAsseesmentHistory
                        // );
                        _self.redirectToAssessment();
                        /// (<any>$('#iframe-popup')).modal('hide');
                        if (submission.data.assessmentstatus === 'Accepted') {
                            if (_self.assessmmentName === 'CANS-F' ||  _self.assessmmentName === 'cans-v2') {
                                _self.saveAssessmentStrengthNeeds('cansF');
                            }
                            if (_self.assessmmentName === 'CANS-OUT OF HOME PLACEMENT SERVICE') {
                                _self.saveAssessmentStrengthNeeds('cansOutOfHomePlacementService');
                            }
                        }
                        if (_self.assessmmentName === 'SAFE-C') {
                            // save safety plan service
                            if (submission.data.assessmentstatus === 'Accepted') {
                                const safeCDangerInfluence = [];
                                const safeCPlanActor = [];
                                if (submission.data.safeCDangerInfluence && submission.data.safeCDangerInfluence.length) {
                                    submission.data.safeCDangerInfluence.forEach(danger => {
                                        safeCDangerInfluence.push({
                                            dangerinfluencenumber: danger.text,
                                            dangerinfluencedesc: danger.value,
                                            completiondate: '',
                                            partiesname: '',
                                            reevaluationdate: '',
                                            actiondescription: 'no'
                                        });
                                    });
                                }
                                if (submission.data.associatedetails && submission.data.associatedetails.length) {
                                    submission.data.associatedetails.forEach(sign => {
                                        safeCPlanActor.push({
                                            intakeservicerequestactorid: sign.intakeservicerequestactorid,
                                            signimage: sign.signature ? sign.signature.replace('data:image/png;base64,', '') : ''
                                        });
                                    });
                                }
                                const safetyPlanInput = {
                                    intakeserviceid: _self.id,
                                    external_templateid: assessment.external_templateid,
                                    submissionid: assessment.submissionid,
                                    savemode: 1,
                                    dangerinfluence: safeCDangerInfluence,
                                    safetyplanactor: safeCPlanActor
                                };
                                _self._commonService.create(safetyPlanInput, 'Safetyplans/add').subscribe(
                                    resp => {
                                        console.log(resp);
                                    },
                                    err => {
                                        console.log(err);
                                    }
                                );
                                // end of save safety plan service
                                if (submission.data.dangerInfluencesIdentified === 'safetydecision2' || submission.data.dangerInfluencesIdentified === 'safetydecision3') {
                                    // email to safety plan persons
                                    _self.baseLocation = _self.location['_platformStrategy']['_platformLocation']['location'];
                                    const protocol = _self.baseLocation['protocol'];
                                    const host = _self.baseLocation['host'];
                                    console.log(protocol + ' - ' + host);
                                    const safetyPlanPersons = [];
                                    if (submission.data.associatedetails && submission.data.associatedetails.length) {
                                        submission.data.associatedetails.forEach(person => {
                                            safetyPlanPersons.push(
                                                Object.assign({
                                                    objectid: submission._id,
                                                    objecttypekey: 'assessment',
                                                    email: person.email,
                                                    message: `${protocol}//${host}/#/external-assessment/${_self.id}/${_self.currentTemplateId}/${submission._id}`,
                                                    url: `${protocol}//${host}/#/external-assessment/${_self.id}/${_self.currentTemplateId}/${submission._id}`
                                                })
                                            );
                                        });
                                    }
                                    if (safetyPlanPersons) {
                                        _self._commonService.create(safetyPlanPersons, CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.EmailSchoolNotification).subscribe(resp => {
                                            console.log(resp);
                                        });
                                    }
                                    // end of email to safety plan persons
                                }
                            }
                            Observable.timer(500).subscribe(() => {
                                _self._router.routeReuseStrategy.shouldReuseRoute = function () {
                                    return false;
                                };
                                _self._router.navigateByUrl(_self._router.url).then(() => {
                                    _self._router.navigated = true;
                                    _self._router.navigate([_self._router.url]);
                                });
                            });
                        }

                    });
            });
            form.on('change', formData => {
                _self.safeCProcess(formData);

                // Begin - D-06439 Fix
                if (formData.changed) {
                    const changedKey = formData.changed.component.key;
                    if (_self.assessmmentName === 'SAFE-C') {
                        if (changedKey === 'assessmentstatus') {
                            if (formData.changed.value === 'Accepted' || formData.data[formData.changed.component.key] === 'Accepted') {
                                form.data.safetyassessmentapprovaldate = moment(new Date()).format('YYYY-MM-DD hh:mm a');
                            }
                            if (formData.changed.value === 'Rejected' || formData.data[formData.changed.component.key] === 'Rejected') {
                                form.data.safetyassessmentapprovaldate = moment(new Date()).format('YYYY-MM-DD hh:mm a');
                            }
                        }
                        form.submission = {
                            data: formData.data // _self.setChildDetails(formData)
                        };
                // End - D-06439 Fix
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'MARYLAND FAMILY RISK REASSESSMENT') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'SILA') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'MFIRA') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'NOTIFICATION OF PLACEMENT-ENTRY AND EXIT RECEIPT') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'HOME HEALTH REPORT') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }
                    if (_self.assessmmentName === 'DOMESTIC VIOLENCE LETHALITY SCREEN FOR DHS') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'AOD Form') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'CANS-OUT OF HOME PLACEMENT SERVICE') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'CANS-F' || _self.assessmmentName === 'cans-v2') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'SAFE-C OHP') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }

                    if (_self.assessmmentName === 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT') {
                        form.submission = {
                            data: formData.data
                        };
                        // @TM: change supervisorname to re-routed supervisor
                        if (changedKey === 'reroutesupervisor') {
                            const origsupervisor = _self.routingInfo.find(item => item.fromrole === 'Supervisor');
                            form.submission.data.supervisorname = (form.submission.data.reroutesupervisor && form.submission.data.reroutesupervisor !== '')
                                ? form.submission.data.reroutesupervisor.username : origsupervisor.fromusername;
                        }
                    }
                }
            });
            // form.on('render', (formData) => {
            //     (<any>$('#iframe-popup')).modal('show');
            //     setTimeout(function () {
            //         $('#assessment-popup').scrollTop(0);
            //     }, 200);
            // });

            form.on('error', error => {
                setTimeout(function () {
                    $('#assessment-popup').scrollTop(0);
                }, 200);
                _self._alertService.error('Unable to save ' + _self.assessmmentName + '. Please try again.');
            });
        });
    });
    }

    assessmentPrintView(assessment: GetintakAssessment) {
        const _self = this;
        Formio.setToken(this.storage.getObj('fbToken'));
        Formio.baseUrl = environment.formBuilderHost;
        Formio.createForm(document.getElementById('assessmentForm'), environment.formBuilderHost + `/form/${assessment.external_templateid}`, {
            readOnly: true,
            hooks: {
                beforeSubmit: (submission, next) => {
                    console.log('beforeSubmit', submission);
                    if (!Formio.token) {
                        Formio.token = _self.storage.getObj('fbToken');
                    }
                    next();
                }
            }
        }).then(function (submission) {
            const options = {
                ignoreLayout: true
            };
            _self.viewHtml(submission._form, submission._submission, options);
        });
    }

    viewHtml(componentData, submissionData, formioOptions) {

        delete submissionData._id;
        delete submissionData.owner;
        delete submissionData.modified;
        const assessmentDateUTC = moment.utc(submissionData.data.assessmentInitiated);
        const assessmentDateESTStr = assessmentDateUTC.utcOffset(-5).format('YYYY-MM-DDThh:mm:ss-05:00');
        submissionData.data.assessmentInitiated = assessmentDateESTStr;
        submissionData.data.datetimefield01 = submissionData.data.datetimefield01 ? moment(submissionData.data.datetimefield01).format('YYYY-MM-DDThh:mm a') : submissionData.data.datetimefield01;
        submissionData.data.datetimefield1 = submissionData.data.datetimefield1 ? moment(submissionData.data.datetimefield1).format('YYYY-MM-DDThh:mm a') : submissionData.data.datetimefield1;
        const exporter = new FormioExport(componentData, submissionData, formioOptions);
        if (this._authService.isDJS() && componentData.title === 'Intake Detention Risk Assessment Instrument') {
            exporter.component.components[0].components[2].components[0]._value = ('' + exporter.component.components[0].components[2].components[0]._value);
            exporter.component.components[0].components[3].components[0]._value = ('' + exporter.component.components[0].components[3].components[0]._value);
            exporter.component.components[0].components[4].components[0]._value = ('' + exporter.component.components[0].components[4].components[0]._value);
            exporter.component.components[0].components[5].components[0]._value = ('' + exporter.component.components[0].components[5].components[0]._value);
            exporter.component.components[0].components[6].components[0]._value = ('' + exporter.component.components[0].components[6].components[0]._value);
            exporter.component.components[0].components[7].components[0]._value = ('' + exporter.component.components[0].components[7].components[0]._value);
            exporter.component.components[0].components[0].columns[0].components[1]._value =
                (moment(new Date(exporter.component.components[0].components[0].columns[0].components[1]._value.slice(0, 10))).format('MM/DD/YYYY'));
            exporter.component.components[0].components[0].columns[1].components[3]._value =
                (moment(new Date(exporter.component.components[0].components[0].columns[1].components[3]._value.slice(0, 10))).format('MM/DD/YYYY'));
        }
        const appDiv = document.getElementById('divPrintView');
        exporter.toHtml().then(html => {
            if (componentData.title === 'HOME HEALTH REPORT') {
                html.innerHTML = this.replaceAll(html.innerHTML, ' UTC</div>', '</div>');
            }
            if (this._authService.isDJS() && componentData.title === 'Intake Detention Risk Assessment Instrument') {
                html.innerHTML = this.replaceAll(html.innerHTML, ' UTC</div>', '</div>');
            }
            html.style.margin = 'auto';
            const iframe = this.createIframe(appDiv);
            const doc = iframe.contentDocument || iframe.contentWindow.document;
            doc.body.appendChild(html);
            window.frames['ifAssessmentView'].focus();
            window.frames['ifAssessmentView'].print();
        });

    }

    escapeRegExp(string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    replaceAll(str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    }

    private createIframe(el) {
        _.forEach(el.getElementsByTagName('iframe'), _iframe => {
            el.removeChild(_iframe);
        });
        const iframe = document.createElement('iframe');
        iframe.setAttribute('id', 'ifAssessmentView');
        iframe.setAttribute('name', 'ifAssessmentView');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('webkitallowfullscreen', '');
        iframe.setAttribute('mozallowfullscreen', '');
        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('style', 'width: -webkit-fill-available;height: -webkit-fill-available;');
        el.appendChild(iframe);
        return iframe;
    }

    private getFormPrePopulation(formName: string, submissionData: any) {
        const prefillUtil = new AssessmentPreFill(this.involvedPersons, this.routingInfo, this.routingSupervisors , this.token, this._commonService, this._dataStoreService);
        switch (formName.toUpperCase()) {
            case 'MFIRA':
                submissionData = prefillUtil.fillMFRA(submissionData, this.daNumber);
                break;

            case 'MARYLAND FAMILY RISK REASSESSMENT':
                submissionData = prefillUtil.fillMFRR(submissionData, this.daNumber);

                break;

            case 'SAFE-C':
                submissionData = prefillUtil.fillSafeC(submissionData, this.daNumber);
                break;

            case 'CANS-F':
                submissionData = prefillUtil.fillCansF(submissionData, this.daNumber);
                break;
            case 'SILA':
                submissionData = prefillUtil.fillSila(submissionData);
                break;

            case 'CANS-V2':
                submissionData = prefillUtil.fillCansFv2(submissionData, this.daNumber);
                break;
            case 'HOME HEALTH REPORT':
                submissionData = prefillUtil.fillHomeHealthReport(submissionData, this.daNumber);
                break;
            case 'SAFE-C OHP':
                console.log('placment info', this.placementInfo);
                this.placement = this.placement.filter(item => !item.placeenddatetime && item.role === 'RC');
                submissionData = prefillUtil.fillSafeCOHP(submissionData, this.daNumber, this.placementInfo, this.removalChildList);
                break;
            case 'TRANSPORTATION PLAN FORM ATTENDING SCHOOL OF ORIGIN FROM OUT-OF-HOME PLACEMENT':
                submissionData = prefillUtil.fillTransportationPlan(submissionData, this.daNumber, this.childDetail);
                break;
            case 'BEST INTEREST DETERMINATION FOR EDUCATIONAL PLACEMENT':
                submissionData = prefillUtil.fillBestInterestDetermination(submissionData, this.daNumber, this.token.user.userprofile.displayname, this.eductionDetatils);
                break;
            case 'CANS-OUT OF HOME PLACEMENT SERVICE':
                submissionData = prefillUtil.fillCansOutOfHomePlacement(submissionData, this.daNumber, this.token.user.userprofile.displayname, this.placement, this.intakAssessment);
                break;
            case 'CLIENT ASSESSMENT FORM - 716 A':
                // submissionData = prefillUtil.fillClientAssessmentForm716A(submissionData, this.daNumber, this.dsdsActionsSummary);
                break;
            case 'INVESTIGATION OUTCOME REPORT - 716 B':
                // submissionData = prefillUtil.fillInvestigationOutcomeReport716B(submissionData, this.daNumber, this.dsdsActionsSummary);
                break;
            case 'ADULT PROTECTIVE SERVICE PROGRAM - PROJECT HOME APPLICATION':
                // submissionData = prefillUtil.fillProjectHomeApplication(submissionData);
                break;
            case 'ADULT PROTECTIVE SERVICE PROGRAM - PROJECT HOME APPLICATION':
                submissionData = prefillUtil.fillProjectHomeApplication(submissionData);
                break;

            case 'PROJECT HOME - RESIDENT AGREEMENT':
                submissionData = prefillUtil.fillResidentAgreement(submissionData);
                break;
            // case 'NOTIFICATION OF PLACEMENT-ENTRY AND EXIT RECEIPT':
            // submissionData = prefillUtil.fillNotificationOfPlacement(submissionData, this.daNumber, this.token.user.userprofile.displayname);
            case 'CINA SHELTER PETITION REQUEST':
                submissionData = prefillUtil.fillCinaShelterPetition(submissionData, this.childRemovalInfo, this.daNumber, this.dsdsActionsSummary);
                break;

            case 'HOUSING CLASSIFICATION RE-ASSESSMENT FORM':
                submissionData = prefillUtil.housingClassificationReAssessment(submissionData);
                break;

            case 'DJS HOUSING CLASSIFICATION ASSESSMENT':
                submissionData = prefillUtil.djsHousingClassificationAssessment(submissionData);
                break;

            case 'YOUTH VULNERABILITY ASSESSMENT INSTRUMENT':
                submissionData = prefillUtil.youthVulnerabilityAssessmentInstrument(submissionData);
                break;

            case 'APPLA':
                submissionData = prefillUtil.fillAppla(submissionData);
                break;

            case 'PRE-DISCHARGE RE-ENTRY TRANSITION PLAN':
                submissionData = prefillUtil.b_02735uniform45dayspredischarge(submissionData, this.youthInvolvedPersons);
                break;

            case 'POST-DISCHARGE RE-ENTRY TRANSITION PLAN':
                submissionData = prefillUtil.b_02736postdischargere_enterytransitionplan(submissionData, this.youthInvolvedPersons);
                break;

            case 'DRAI FOLLOW UP':
                submissionData = prefillUtil.draiFollowUp(submissionData, this.youthInvolvedPersons);
                break;

            case 'INTAKE DETENTION RISK ASSESSMENT INSTRUMENT':
                submissionData = prefillUtil.intakedrai(submissionData, this.involvedPersons.find(data => data.rolename === 'Youth'), JSON.parse(JSON.stringify(this.intakeFormDRAIData)));
                break;
            case 'SHELTER CARE AUTHORIZATION AND DATE OF HEARING':
                submissionData = prefillUtil.shelterPetition(submissionData);
            break;
            case 'AOD FORM':
                submissionData = prefillUtil.fillAOD(submissionData);
                break;
            case 'AOD/PADS FORM':
                 submissionData = prefillUtil.fillAOD(submissionData);
                break;
            case 'DOMESTIC VIOLENCE LETHALITY SCREEN FOR DHS':
                submissionData = prefillUtil.fillDomesticViolence(submissionData);
                break;
            case 'NOTIFICATION OF PLACEMENT-ENTRY AND EXIT RECEIPT':
                submissionData = prefillUtil.fillNotificationOfPlacement(submissionData, this.removalChildList);
                break;
            case 'RISK ASSESSMENT LEGACY':
                submissionData = prefillUtil.fillRiskAssessmentLegacy(submissionData, this.daNumber);
                break;

        }

        return submissionData;
    }
    get getChildList() {
        const CHILD_CATEGORIES = ['CHILD', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC', 'AV'];
        return this.involvedPersons.filter(child => {
            let childFound = false;
            child.roles.forEach(role => {
                const childCategory = CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });
    }

    processChildDetails($event) {
        const assessmentactorArray = [];
        const childIsUnsafe = ($event.data.dangerInfluencesIdentified === 'childIsUnsafe');
        const issafe = childIsUnsafe ? 0 : 1;
        if (this.getChildList) {
            this.getChildList.map((data) => {
                const childname = data.firstname + ' ' + data.lastname;
                $event.data.childdatagrid.forEach(child => {
                    const formchildname = child.childname;
                    const assessmentactor = {
                        'intakeservicerequestactorid': data.intakeservicerequestactorid,
                        'issafe': issafe
                    };
                    if (childname === formchildname) {
                        assessmentactorArray.push(assessmentactor);
                    }

                });

            });
            //  $event.data.childdatagrid.forEach(child => {
            //     const  assessmentactor = {
            //         'intakeservicerequestactorid': child.cjamspid,
            //         'issafe': child.issafe };
            //         assessmentactorArray.push(assessmentactor);
            //  });
            console.log(assessmentactorArray);
            return assessmentactorArray;
        }
        return {};
    }
    private getAge(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }
    setChildDetails($event) {
        console.log(this.processChildDetails($event));
        if ($event.changed && $event.changed.component.key === 'childname') {

            if (this.getChildList) {
                this.getChildList.map((data) => {
                    const childname = data.firstname + ' ' + data.lastname;
                    $event.data.childdatagrid.forEach((child, index) => {
                        const formchildname = child.childname;
                        $event.data.childdatagrid[index].childname = childname;
                        if (childname === formchildname || $event.changed.value === '') {
                            // $event.data['childdatagrid'][0]['childname'] = formchildname;
                            $event.data.childdatagrid[index].clientid = data.cjamspid;
                            $event.data.childdatagrid[index].age = this.getAge(data.dob);
                        }
                    });

                });
            }
            return $event.data;
        } else {
            return $event.data;
        }
    }
    private safeCProcess($event) {
        if (this.assessmmentName === 'SAFE-C') {
            if ($event.changed) {
                const dangerInfluenceKey = $event.changed.component.key;
                if (dangerInfluenceKey === 'childIsUnsafe') {
                    this.isChildSafe = !$event.data[$event.changed.component.key];
                }
                if (dangerInfluenceKey && this.safeCKeys.indexOf(dangerInfluenceKey) > -1) {
                    const dangerInflunceItem = this.selectedSafeCDangerInfluence.find(item => item.value === dangerInfluenceKey);
                    if (dangerInflunceItem) {
                        if ($event.changed.value === 'no' || $event.data[$event.changed.component.key] === 'no') {
                            const itemIndex = this.selectedSafeCDangerInfluence.indexOf(dangerInflunceItem);
                            this.selectedSafeCDangerInfluence.splice(itemIndex, 1);
                        }
                        console.log(this.selectedSafeCDangerInfluence);
                    } else {
                        if ($event.changed.value === 'yes' || $event.data[$event.changed.component.key] === 'yes') {
                            this.selectedSafeCDangerInfluence.push({
                                text: $event.changed.component.label,
                                value: dangerInfluenceKey
                            });
                            console.log(this.selectedSafeCDangerInfluence);
                        }
                    }
                }
            }
        } else {
            this.selectedSafeCDangerInfluence = [];
        }
    }
    redirectToAssessment() {
        this.isInitialized = false;
        if (this.intakAssessment && this.intakAssessment.description === 'CINA Shelter Petition Request') {
            this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/child-removal']);
        } else if ((this.intakAssessment ) && (this.intakAssessment.description === 'APPLA' || this.intakAssessment.titleheadertext === 'APPLA')) {
            this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/sc-permanency-plan/placement/appla/list']);
        } else if (this.isShelterForm) {
            this.isShelterForm = false;
            this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/child-removal/details']);
        } else {
            this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/assessment']);
        }
    }
    saveAssessmentStrengthNeeds(templatename) {
        // How do we handle cans-outofhome?
        const payload = {
          'objectid': this.id,
          'templatename': templatename,
          'status': 'accepted'
        };
        this._commonService.create(payload, 'serviceplan/saveAssessmentStrengthNeeds').subscribe(
          response => {
            console.log('Strengths and Needs added successfully.');
          });
      }

      printAssessment() {
          window.print();
      }
}
