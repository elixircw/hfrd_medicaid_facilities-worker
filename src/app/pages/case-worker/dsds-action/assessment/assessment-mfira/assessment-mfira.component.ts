import { Component, OnInit } from '@angular/core';
import { PaginationRequest } from '../../../../../@core/entities/common.entities';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericService, CommonHttpService, DataStoreService, SessionStorageService, CommonDropdownsService, AlertService, AuthService } from '../../../../../@core/services';
import { HttpService } from '../../../../../@core/services/http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Assessments, RoutingInfo } from '../../../_entities/caseworker.data.model';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { AssessmentService } from '../assessment.service';

@Component({
  selector: 'assessment-mfira',
  templateUrl: './assessment-mfira.component.html',
  styleUrls: ['./assessment-mfira.component.scss']
})
export class AssessmentMfiraComponent implements OnInit {
  ASSESSMENT_NAME = 'MFIRA';
  submissiondata: any;
  involvedPerson: any[];
  isServiceCase: any;
  roleId: AppUser;
  agency: string;
  isCW: boolean;
  id: any;
  daNumber: any;
  currentAssessmentId: any;
  currentSubmissionId: string;
  mifraData: any;     
  updateMifra: boolean; 
  viewMifra: boolean;
  headofhouseholdid : string;
  
  //Approval and routing
  routingSupervisors = [];
  routingInfo: RoutingInfo[];
  authorizationApproval: FormGroup;
  familyHOUSEHOLD: FormGroup;
  supplementaryApproval: FormGroup;
  scoringandoverride: FormGroup;
  NeglectAbuse: FormGroup;
  legalGuardian = [];
  isSupervisor: boolean;
  assessmentStatus: any;
  // caseworkersignature: any;
  // supervisorsignature: any;
  incompleteList: any[];
  
  //Caregivers
  caregiversInCase = [];

  constructor(
    private route: ActivatedRoute,
    private _service: GenericService<Assessments>,
    private _commonService: CommonHttpService,
    public sanitizer: DomSanitizer,
    private _http: HttpService,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private storage: SessionStorageService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _commonDDService: CommonDropdownsService,
    private _assessmentService: AssessmentService,
    private _formBuilder: FormBuilder
  ) {
    this.id = this._commonDDService.getStoredCaseUuid();
    this.daNumber = this._commonDDService.getStoredCaseNumber();
  }

  ngOnInit() {
    this.isServiceCase = this.storage.getItem('ISSERVICECASE');
    this._assessmentService.getservicecase();
    this.roleId = this._authService.getCurrentUser();
    this.agency = this._authService.getAgencyName();
    this.isSupervisor = (this.roleId.role.name === 'apcs') ? true : false;
    if (this.agency === 'CW') {
      this.isCW = true;
    } else {
      this.isCW = false;
    }
    const list = this._dataStoreService.getData('CASEWORKER_SERVICE_PLAN_LIST');
    this.mifraData = this._dataStoreService.getData('CASEWORKER_SELECTED_ASSESSMENT');
    if (this.mifraData && (this.mifraData.mode === 'update' || this.mifraData.mode === 'submit')) {
      this.updateMifra = true;
    } else {
      this.updateMifra = false;
    }
    //In case of working with an existing assesment submission
    this.currentAssessmentId = this.mifraData.assessmentid;
    this.currentSubmissionId = this.mifraData.submissionid;
    this.routingInfo = this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') ? this._dataStoreService.getData('CASEWORKER_ROUTING_INFO') : [];
    this.routingSupervisors = this._dataStoreService.getData('CASEWORKER_SUPERVISORS') ? this._dataStoreService.getData('CASEWORKER_SUPERVISORS') : [];
    this.familyHOUSEHOLDForm();
    this.authorizationApprovalForm();
    this.supplementaryForm();
    this.scoringandoverrideForm();
    this.NeglectAbuseForm();
    this.prefillAuthorizationApprovalInfo();
    //Get persons in case
    this.getInvolvedPerson();
    //Get caregivers in case
    this.getCaregiversInCase();
    this.familyHOUSEHOLD.patchValue({ 
      assessmentInitDate: new Date(),
      serviceCaseId: this.daNumber
    });
    this.mfiraPatch();
    this.loadFinalScore();
    // if (this.authorizationApproval.get('caseworkersign').value) {
    //   this.caseworkersignature = this.authorizationApproval.get('caseworkersign').value;
    // }
    // if (this.authorizationApproval.get('supervisorsign').value) {
    //   this.supervisorsignature = this.authorizationApproval.get('supervisorsign').value;
    // }
  }

  mfiraPatch() {

 //Flip for non-migrated data
 if (this.mifraData && this.mifraData.submissiondata) {
  this.submissiondata = this.mifraData.submissiondata;
}

this._dataStoreService.setData('PRINTDATA', this.submissiondata);

    if (this.mifraData && (this.mifraData.mode === 'update' || this.mifraData.mode === 'submit')) {
      this.viewMifra = false;
      if (this.mifraData.submissiondata.familyHOUSEHOLD) {
        this.familyHOUSEHOLD.patchValue(this.mifraData.submissiondata.familyHOUSEHOLD);
        this.familyHOUSEHOLD.setControl('familyArray', this._formBuilder.array([]));
        if (this.mifraData.submissiondata.familyHOUSEHOLD.familyArray) {
          this.mifraData.submissiondata.familyHOUSEHOLD.familyArray.forEach(data => {
            this.addFamily(data);
          });
        }
      }
      if (this.mifraData.submissiondata.NeglectAbuse) {
        this.NeglectAbuse.patchValue(this.mifraData.submissiondata.NeglectAbuse);
      }
      if (this.mifraData.submissiondata.supplementaryApproval) {
        this.supplementaryApproval.patchValue(this.mifraData.submissiondata.supplementaryApproval);
      }
      if (this.mifraData.submissiondata.scoringandoverride) {
        this.scoringandoverride.patchValue(this.mifraData.submissiondata.scoringandoverride);
      }
      if (this.mifraData.submissiondata.authorizationApproval) {
        this.authorizationApproval.patchValue(this.mifraData.submissiondata.authorizationApproval);
      }
    }
    if (this.mifraData && this.mifraData.mode === 'submit') {
      this.viewMifra = true;
      this.familyHOUSEHOLD.disable();
      this.authorizationApproval.disable();
      this.supplementaryApproval.disable();
      this.scoringandoverride.disable();
      this.NeglectAbuse.disable();
    }
  }

  familyHOUSEHOLDForm() {
    this.familyHOUSEHOLD = this._formBuilder.group({
      assessmentInitDate: [null],
      houseHoldHeadName: [null],
      serviceCaseId: [null]
    });
    this.familyHOUSEHOLD.setControl('familyArray', this._formBuilder.array([]));
  }

  addFamily(model) {
    const control = <FormArray>this.familyHOUSEHOLD.controls['familyArray'];
    control.push(this.createFormGroup(model));
  }

  deleteFamily(index: number) {
    const control = <FormArray>this.familyHOUSEHOLD.controls['familyArray'];
    control.removeAt(index);
  }

  private createFormGroup(modal) {
    const relationshipDesc = this._assessmentService.getRelationShip(this.headofhouseholdid,modal.personid, modal.relationshiparray);
    if (this.updateMifra) {
      return this._formBuilder.group({
        name: modal.name ? modal.name : '',
        dob: modal.dob ? new Date(modal.dob) : '',
        caregiver: modal.caregiver ? modal.caregiver : '',
        relationship: relationshipDesc ? relationshipDesc : ''
      });
    } else {
      return this._formBuilder.group({
        name: modal.fullname ? modal.fullname : '',
        dob: modal.dob ? new Date(modal.dob) : '',
        caregiver: modal.caregiver ? modal.caregiver : '',
        relationship: relationshipDesc ? relationshipDesc : ''
      });
    }
  }

  NeglectAbuseForm() {
    this.NeglectAbuse = this._formBuilder.group({
      assessmentInitDate: [null],
      houseHoldHeadName: [null],
      serviceCaseId: [null],
      caregivername: [null],
      caregiverdob: [null],
      caregiverage: [null],
      caregiverrelationship: [null],
      currentReport: [null],
      currentNeglectScore: [0],
      currentAbuseScore: [0],
      priorCPSResponse: [null],
      priorCPSResponseNeglectScore: [0],
      priorCPSResponseAbuseScore: [0],
      priorNeglect: [null],
      priorNeglectNeglectScore: [0],
      priorNeglectAbuseScore: [0],
      priorAbuse: [null],
      priorAbuseNeglectScore: [0],
      priorAbuseAbuseScore: [0],
      ihfsohpcase: [null],
      ihfsohpcaseNeglectScore: [0],
      ihfsohpcaseAbuseScore: [0],
      cpsresponse: [null],
      cpsresponseNeglectScore: [0],
      cpsresponseAbuseScore: [0],
      priorNonAccidental: [null],
      priorNonAccidentalNeglectScore: [0],
      priorNonAccidentalAbuseScore: [0],
      youngestAge: [null],
      youngestAgeNeglectScore: [0],
      youngestAgeAbuseScore: [0],
      PrimaryCaregiver: [null],
      PrimaryCaregiverNeglectScore: [0],
      PrimaryCaregiverAbuseScore: [0],
      PrimaryCaregiverMentalHealth: [null],
      PrimaryCaregiverTreatment: [null],
      PrimaryCaregiverMentalHealthNeglectScore: [0],
      PrimaryCaregiverMentalHealthAbuseScore: [0],
      PrimaryCaregiverHistory: [null],
      PrimaryCaregiverHistoryNeglectScore: [0],
      PrimaryCaregiverHistoryAbuseScore: [0],
      housing: [null],
      housingNeglectScore: [0],
      housingAbuseScore: [0],
      totalNeglectScore: [0],
      totalAbuseScore: [0],
      medicallyfragile: [null],
      positivetoxicology: [null],
      physicaldisability: [null],
      developmentaldisability: [null],
      delinquencyhistory: [null],
      mentalhealth: [null],
      nonecharacteristics: [null],
      characteristicsabusescore: [0],
      characteristicsneglectscore: [0],
      blameschild: [null],
      justifiesmaltreatment: [null],
      noneprimary: [null],
      primaryabusescore: [0],
      primaryneglectscore: [0],
      primarycaregivercharacteristicsabusescore: [0],
      primarycaregivercharacteristicsneglectscore: [0],
      providesinsufficient: [null],
      employsexcessive: [null],
      domineeringcaregiver: [null],
      nonecaregiver: [null],
      pastorcurrentabusescore: [0],
      pastorcurrentneglectscore: [0],
      pastorcurrentdrugsselect: [null],
      pastorcurrentdrugs: [null],
      pastorcurrentalcohol: [null],
      pastorcurrentalcoholselect: [null],
      pastorcurrent: [null],
      secondarycaregiverpastorcurrentabuse: [0],
      secondarycaregiverpastorcurrentneglect: [0],
      secondarycaregiverpastorcurrentdrugs: [null],
      secondarycaregiverpastorcurrentdrugsselect: [null],
      secondarycaregiverpastorcurrentselect: [null],
      secondarycaregiverpastorcurrentalcohol: [null],
      secondarycaregiverpastorcurrent: [null],
      domesticviolenceabuse: [0],
      domesticviolenceneglect: [0],
      domesticviolencefamily: [null],
      domesticviolencehomeless: [null],
      domesticviolencecurrent: [null]
    });
  }

  getCansfData() {
    console.log(this.mifraData);
    if (this.mifraData && (this.mifraData.mode === 'update' || this.mifraData.mode === 'submit')) {
      this.updateMifra = true;
      this.viewMifra = false;
 
    } else {
      this.updateMifra = false;
      this.viewMifra = false;
    }
    if (this.mifraData && this.mifraData.mode === 'submit') {
      this.updateMifra = true;
      this.viewMifra = true;

    }
  }

  getMfiraPayload() {
    return {
      familyHOUSEHOLD: this.familyHOUSEHOLD.getRawValue(),
      NeglectAbuse: this.NeglectAbuse.getRawValue(),
      supplementaryApproval: this.supplementaryApproval.getRawValue(),
      scoringandoverride: this.scoringandoverride.getRawValue(),
      authorizationApproval: this.authorizationApproval.getRawValue(),
      // The API expects these in addition to form data
      supervisorname: this.authorizationApproval.controls['supervisorname'].value,
      routingsupervisors: this.routingSupervisors,
      currentSubmissionId: this.currentSubmissionId,
      assessmentStaus: this.assessmentStatus
    };
  }

  saveForm() {
    const mfiraData = this.getMfiraPayload();
    this._assessmentService.saveAssessment(this.ASSESSMENT_NAME, this.currentAssessmentId, mfiraData)
      .subscribe(
        (response) => {
          this._alertService.success('Form saved');
          if (response.data) {
            this.currentAssessmentId = response.data.assessmentid;
            this.currentSubmissionId = response.data.submissionid;
          } else {
            this.currentAssessmentId = response.assessmentid;
            this.currentSubmissionId = response.submissionid;
          }
        },
        (error) => {
          this._alertService.error('Unable to save.');
        }

      );
  }

  submitForApproval() {
    // if (this.familyAssessmentYouth.valid) {
      if (this.isSupervisor) {
       this.assessmentStatus = this.authorizationApproval.get('assessmentstatus').value;
      } else {
       this.assessmentStatus = 'Review';
      }
       const submissionData = this.getMfiraPayload();
       this._assessmentService.saveAssessment(this.ASSESSMENT_NAME, this.currentAssessmentId, submissionData)
         .subscribe(
           (response) => {
            this._alertService.success(`${this.assessmentStatus === 'Review' ? 'Approval' : this.assessmentStatus} Submitted Successfully`);
             setTimeout(() => {
               this._router.navigate(['../'], {relativeTo : this.route});
             }, 1000);
           },
           (error) => {
             this._alertService.error('Unable to submit for approval.');
           }
         );
    // } else {
      // this._alertService.error('Please fill mandatory fileds');
    // }
   }

  getRoutingSupervisors() {
    return this.routingSupervisors; 
  }

  getRoutingInfo() {
    return this.routingInfo;
  }
  scoringandoverrideForm() {
    this.scoringandoverride =  this._formBuilder.group({
      discretionaryreason : [null],
      //overiderisklevel: [null],
      discretionaryoverride: [null, Validators.required],
      caregiveraction: [null, Validators.required],
      severenonaccidental: [null, Validators.required],
      nonaccidental: [null, Validators.required],
      sexualabusecase: [null, Validators.required],
      finalrisklevel: [null]
    });
    this.scoringandoverride.get('finalrisklevel').disable();
  }
  supplementaryForm() {
    this.supplementaryApproval= this._formBuilder.group({
      criminalhistorywithinpastyears: [null],
      primary_isarrest: [null],
      primary_isconviction: [null],
      primary_felonyconviction: [null],
      secondary_nosecondarycaregiver: [null],
      secondary_isarrest: [null],
      secondary_isconviction: [null],
      secondary_felonyconviction: [null],

    });
  }

  authorizationApprovalForm() {
    this.authorizationApproval = this._formBuilder.group({
      routingsupervisors: [null],
      supervisorname: [''],
      workername: [''],
      assessmentstatus: [''],
      // caseworkersign: [null],
      // supervisorsign: [null],
      reviewdate: [null],
      caseworkercomment: [''],
      supervisorcomment: ['']
    });
  }

  prefillAuthorizationApprovalInfo() {
    if (Array.isArray(this.routingInfo) && this.routingInfo.length) {
      this.authorizationApproval.patchValue({
        supervisorname: this.routingInfo[0].fromusername,
        workername: this.routingInfo[0].tousername
      });
    }
  }

  onOverrideRisk(value) {
    if (value === '1') {
      this.scoringandoverride.get('discretionaryreason').setValidators(Validators.required);
      this.scoringandoverride.get('discretionaryreason').updateValueAndValidity();
      //this.scoringandoverride.get('overiderisklevel').setValidators(Validators.required);
      //this.scoringandoverride.get('overiderisklevel').updateValueAndValidity();
    } else {
      this.scoringandoverride.get('discretionaryreason').reset();
      this.scoringandoverride.get('discretionaryreason').clearValidators();
      this.scoringandoverride.get('discretionaryreason').updateValueAndValidity();
      //this.scoringandoverride.get('overiderisklevel').reset();
      //this.scoringandoverride.get('overiderisklevel').clearValidators();
      //this.scoringandoverride.get('overiderisklevel').updateValueAndValidity();
      this.loadFinalScore();
    }
  }

  /*
    onOverrideRiskLevel(value) {
      if (value) {
        this.scoringandoverride.patchValue({
          finalrisklevel: value
        });
      }
    }
  */

  /**
   * Get caregivers in the case
   */
  getCaregiversInCase() {
    this.caregiversInCase = [];
    this._commonService.getArrayList(
            new PaginationRequest({
                nolimit: true,
                method: 'get',
                where: {
                    personid: this.id
                }
            }),
            'Actorrelationships/getallcaregiversincase'+ '?filter'
        )
        .subscribe(response => {
        if (response && Array.isArray(response) && response.length && response[0].getallcaregiversincase) {
          this.caregiversInCase = response[0].getallcaregiversincase;
          }
      });
  }

  getInvolvedPerson() {
    let getpersonlistreq = {};
    if (this.isCW && this.isServiceCase) {
      getpersonlistreq = { objectid: this.id, objecttypekey: 'servicecase' };
    } else {
      getpersonlistreq = { intakeserviceid: this.id };
    }
    this._commonService.getPagedArrayList(
      new PaginationRequest({
        page: 1,
        limit: 20,
        method: 'get',
        where: getpersonlistreq
      }),
      'People/getpersondetailcw?filter'
    ).subscribe(response => {
      if (response && response.data && response.data.length) {
        this.involvedPerson = response.data;
        this.involvedPerson.forEach(ele => {

          if(ele.isheadofhousehold) {
            this.headofhouseholdid = ele.personid;
            this.familyHOUSEHOLD.patchValue({
              houseHoldHeadName: ele.fullname
            });
          }
          const legalGurdian = ele.roles.filter(roleid => roleid.intakeservicerequestpersontypekey === 'LG');          
          if (legalGurdian && legalGurdian.length) {
            this.legalGuardian.push(ele);
          
          }
          // if (!this.updateMifra) {
          //   this.addFamily(ele);
          // }
        });
        this.involvedPerson.forEach(ele => {

          // if(ele.isheadofhousehold) {
          //   this.headofhouseholdid = ele.personid;
          //   this.familyHOUSEHOLD.patchValue({
          //     houseHoldHeadName: ele.fullname
          //   });
          // }
          // const legalGurdian = ele.roles.filter(roleid => roleid.intakeservicerequestpersontypekey === 'LG');          
          // if (legalGurdian && legalGurdian.length) {
          //   this.legalGuardian.push(ele);
          
          // }
          if (!this.updateMifra) {
            this.addFamily(ele);
          }
        });
      }
    });
  }

  changeSupervisor(userid) {
    const user = this.routingSupervisors.find(item => item.userid === userid);
    if (user.username) {
      this.authorizationApproval.patchValue({
        supervisorname: user.username
      });
    }
  }

  calculateScore(value, negelct, abuse) {
    if (value === '11') {
      value = '4';
    }
    if (value === '10') {
      value = '1';
    }
    if (value === '1') {
      this.NeglectAbuse.controls[negelct].setValue(1);
      this.NeglectAbuse.controls[abuse].setValue(0);
    }
    if (value === '2') {
      if(negelct == 'PrimaryCaregiverHistoryNeglectScore'){
        this.NeglectAbuse.controls[negelct].setValue(1);
        this.NeglectAbuse.controls[abuse].setValue(1);  
      }
      else{
        this.NeglectAbuse.controls[negelct].setValue(0);
        this.NeglectAbuse.controls[abuse].setValue(1);  
      }
    }
    if (value === '3') {
      this.NeglectAbuse.controls[negelct].setValue(1);
      this.NeglectAbuse.controls[abuse].setValue(1);
    }
    if (value === '4') {
      this.NeglectAbuse.controls[negelct].setValue(0);
      this.NeglectAbuse.controls[abuse].setValue(0);
    }
    if (value === '5') {
      this.NeglectAbuse.controls[negelct].setValue(2);
      this.NeglectAbuse.controls[abuse].setValue(0);
    }
    if (value === '6') {
      this.NeglectAbuse.controls[negelct].setValue(0);
      this.NeglectAbuse.controls[abuse].setValue(2);
    }
  }

  calculateScoreOncheck(value, negelct, abuse, section) {
    if (negelct) {
      this.NeglectAbuse.controls[negelct].setValue(1);
    }
    if (abuse) {
      this.NeglectAbuse.controls[abuse].setValue(1);
    }
  }

  getCharacteristicvalue(value) {
    if (value === 1) {
      let neglectcounter = 0;
      if (this.NeglectAbuse.get('medicallyfragile').value) {
        neglectcounter++;
      } else {
        neglectcounter = neglectcounter === 0 ? 0 : neglectcounter--;
      }
      if (this.NeglectAbuse.get('positivetoxicology').value) {
        neglectcounter++;
      } else {
        neglectcounter = neglectcounter === 0 ? 0 : neglectcounter--;
      }
      if (this.NeglectAbuse.get('physicaldisability').value) {
        neglectcounter++;
      } else {
        neglectcounter = neglectcounter === 0 ? 0 : neglectcounter--;
      }
      if (this.NeglectAbuse.get('nonecharacteristics').value) {
        neglectcounter = 0;
        this.NeglectAbuse.get('medicallyfragile').reset();
        this.NeglectAbuse.get('positivetoxicology').reset();
        this.NeglectAbuse.get('physicaldisability').reset();
      }
      this.NeglectAbuse.patchValue({
        characteristicsneglectscore: neglectcounter
      });
      return neglectcounter;
    }
    if (value === 2) {
      let abusecounter = 0;
      if (this.NeglectAbuse.get('developmentaldisability').value) {
        abusecounter++;
      } else {
        abusecounter = abusecounter === 0 ? 0 : abusecounter--;
      }
      if (this.NeglectAbuse.get('delinquencyhistory').value) {
        abusecounter++;
      } else {
        abusecounter = abusecounter === 0 ? 0 : abusecounter--;
      }
      if (this.NeglectAbuse.get('mentalhealth').value) {
        abusecounter++;
      } else {
        abusecounter = abusecounter === 0 ? 0 : abusecounter--;
      }
      if (this.NeglectAbuse.get('nonecharacteristics').value) {
        abusecounter = 0;
        this.NeglectAbuse.get('mentalhealth').reset();
        this.NeglectAbuse.get('delinquencyhistory').reset();
        this.NeglectAbuse.get('developmentaldisability').reset();
      }
      this.NeglectAbuse.patchValue({
        characteristicsabusescore: abusecounter
      });
      return abusecounter;
    }
  }

  isReadyForApproval() {
    this.incompleteList = [];
    if (!this.familyHOUSEHOLD.valid) {
      this.incompleteList.push('FAMILY AND HOUSEHOLD COMPOSITION');
    }
    if (!this.NeglectAbuse.valid) {
      this.incompleteList.push('NEGLECT/ABUSE INDEX');
    }
    if (!this.scoringandoverride.valid) {
      this.incompleteList.push('SCORING AND OVERRIDES');
    }
    if (!this.supplementaryApproval.valid) {
      this.incompleteList.push('SUPPLEMENTAL QUESTION');
    }
    if (!this.authorizationApproval.valid) {
      this.incompleteList.push('APPROVAL');
    }
    if (this.incompleteList.length == 0) {
      this.submitForApproval();
    } else {
      (<any>$('#incomplete-items')).modal('show');
    }
  }


  getcaregivervalue(value) {
    if (value === 2) {
      let abusecounter = 0;
      if (this.NeglectAbuse.get('blameschild').value) {
        abusecounter++;
      } else {
        abusecounter = abusecounter === 0 ? 0 : abusecounter--;
      }
      if (this.NeglectAbuse.get('justifiesmaltreatment').value) {
        abusecounter = abusecounter + 2;
      } else {
        abusecounter = abusecounter === 0 ? 0 : (abusecounter === 3 ? (abusecounter = abusecounter - 2) : abusecounter--);
      }
      if (this.NeglectAbuse.get('noneprimary').value) {
        this.NeglectAbuse.get('blameschild').reset();
        this.NeglectAbuse.get('justifiesmaltreatment').reset();
        abusecounter = 0;
      }
      this.NeglectAbuse.patchValue({
        primaryneglectscore: 0,
        primaryabusescore: abusecounter
      });
      return abusecounter;
    }
  }

  getcaregivercharvalue(value) {
    if (value === 2) {
      let abusecounter = 0;
      if (this.NeglectAbuse.get('providesinsufficient').value) {
        abusecounter++;
      } else {
        abusecounter = abusecounter === 0 ? 0 : abusecounter--;
      }
      if (this.NeglectAbuse.get('employsexcessive').value) {
        abusecounter++;
      } else {
        abusecounter = abusecounter === 0 ? 0 : abusecounter--;
      }
      if (this.NeglectAbuse.get('domineeringcaregiver').value) {
        abusecounter++;
      } else {
        abusecounter = abusecounter === 0 ? 0 : abusecounter--;
      }
      if (this.NeglectAbuse.get('nonecaregiver').value) {
        this.NeglectAbuse.get('domineeringcaregiver').reset();
        this.NeglectAbuse.get('employsexcessive').reset();
        this.NeglectAbuse.get('providesinsufficient').reset();
        abusecounter = 0;
      }
      this.NeglectAbuse.patchValue({
        primarycaregivercharacteristicsneglectscore: 0,
        primarycaregivercharacteristicsabusescore: abusecounter
      });
      return abusecounter;
    }
  }
  /*
  getsubstanceabusevalue(value) {
    if (value === 1) {
      let neglectcounter = 0;
      if (this.NeglectAbuse.get('pastorcurrentalcohol').value) {
        neglectcounter++;
      } else {
        neglectcounter = neglectcounter === 0 ? 0 : neglectcounter--;
      }
      if (this.NeglectAbuse.get('pastorcurrentdrugs').value) {
        neglectcounter++;
      } else {
        neglectcounter = neglectcounter === 0 ? 0 : neglectcounter--;
      }
      this.NeglectAbuse.patchValue({
        pastorcurrentneglectscore: neglectcounter,
      });
      return neglectcounter;
    }
  }
  */
  getsubstanceabusevalue(value) {
    if (value === 1) {
      let abusecounter = 0;
      if (this.NeglectAbuse.get('pastorcurrentalcohol').value) {
        if (this.NeglectAbuse.get('pastorcurrentalcoholselect').value && 
        (this.NeglectAbuse.get('pastorcurrentalcoholselect').value == 2 || this.NeglectAbuse.get('pastorcurrentalcoholselect').value ==3)) {
          abusecounter++;
        }
      }
      if (this.NeglectAbuse.get('pastorcurrentdrugs').value) {
        if (this.NeglectAbuse.get('pastorcurrentdrugsselect').value && 
        (this.NeglectAbuse.get('pastorcurrentdrugsselect').value == 2 || this.NeglectAbuse.get('pastorcurrentdrugsselect').value ==3)) {
          abusecounter++;
        }
      }
      this.NeglectAbuse.patchValue({
        pastorcurrentabusescore: abusecounter,
      });
      return abusecounter;
    }
  }
  getsubstancesecondaryabusevalue(value) {
    if (value === 1) {
      let abusecounter = 0;
      if (this.NeglectAbuse.get('secondarycaregiverpastorcurrentalcohol').value) {
        if (this.NeglectAbuse.get('secondarycaregiverpastorcurrentselect').value && 
        (this.NeglectAbuse.get('secondarycaregiverpastorcurrentselect').value == 2 || this.NeglectAbuse.get('secondarycaregiverpastorcurrentselect').value ==3)) {
          abusecounter++;
        }
      }
      if (this.NeglectAbuse.get('secondarycaregiverpastorcurrentdrugs').value) {
        if (this.NeglectAbuse.get('secondarycaregiverpastorcurrentdrugsselect').value && 
        (this.NeglectAbuse.get('secondarycaregiverpastorcurrentdrugsselect').value == 2 || this.NeglectAbuse.get('secondarycaregiverpastorcurrentdrugsselect').value ==3)) {
          abusecounter++;
        }
      }
      this.NeglectAbuse.patchValue({
        secondarycaregiverpastorcurrentabuse: abusecounter,
      });
      return abusecounter;
    }
  }
  getHousing(value) {
    if (value === 2) {
      let neglectcounter = 0;
      if (this.NeglectAbuse.get('domesticviolencecurrent').value) {
        neglectcounter++;
      } else {
        neglectcounter = neglectcounter === 0 ? 0 : neglectcounter--;
      }
      if (this.NeglectAbuse.get('domesticviolencehomeless').value) {
        neglectcounter = neglectcounter + 2;
      } else {
        if (neglectcounter > 2) {
          neglectcounter = neglectcounter === 0 ? 0 : (neglectcounter = neglectcounter - 2) ;
        } else {
          neglectcounter = neglectcounter === 0 ? 0 : (neglectcounter = 2 - neglectcounter) ;
        }
      }
      this.NeglectAbuse.patchValue({
        housingNeglectScore: neglectcounter,
        housingAbuseScore: 0
      });
      return neglectcounter;
    }
  }

  calculateTotalNeglect() {
    let totalNeglectScore = 0;
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('currentNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorCPSResponseNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorNeglectNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorAbuseNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('ihfsohpcaseNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('cpsresponseNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorNonAccidentalNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('youngestAgeNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('characteristicsneglectscore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('primaryneglectscore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('PrimaryCaregiverNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('primarycaregivercharacteristicsneglectscore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('PrimaryCaregiverMentalHealthNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('pastorcurrentneglectscore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('secondarycaregiverpastorcurrentneglect').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('PrimaryCaregiverHistoryNeglectScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('domesticviolenceneglect').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('housingNeglectScore').value);
    this.NeglectAbuse.patchValue({
      totalNeglectScore : totalNeglectScore
    });
    return totalNeglectScore;
  }

  calculateTotalAbuse() {
    let totalNeglectScore = 0;
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('currentAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorCPSResponseAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorNeglectAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorAbuseAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('ihfsohpcaseAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('cpsresponseAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('priorNonAccidentalAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('youngestAgeAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('characteristicsabusescore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('primaryabusescore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('PrimaryCaregiverAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('primarycaregivercharacteristicsabusescore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('PrimaryCaregiverMentalHealthAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('pastorcurrentabusescore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('secondarycaregiverpastorcurrentabuse').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('PrimaryCaregiverHistoryAbuseScore').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('domesticviolenceabuse').value);
    totalNeglectScore += this.converttoNumber(this.NeglectAbuse.get('housingAbuseScore').value);
    this.NeglectAbuse.patchValue({
      totalAbuseScore : totalNeglectScore
    });
    return totalNeglectScore;
  }

  converttoNumber(value) {
    return (value) ? parseFloat(value) : 0;
  }

  loadFinalScore() {
    if (this.overrideExists()) {
      this.scoringandoverride.patchValue({
        finalrisklevel: '4'
      });
    } else {
      const neglect = this.converttoNumber(this.NeglectAbuse.get('totalNeglectScore').value);
      const abuse = this.converttoNumber(this.NeglectAbuse.get('totalAbuseScore').value);
      const total = neglect >= abuse ? neglect : abuse;
      if (total <= 1) {
        this.scoringandoverride.patchValue({
          finalrisklevel: '1'
        });
      } else if (total <= 4) {
        this.scoringandoverride.patchValue({
          finalrisklevel: '2'
        });
      } else if (total <= 8) {
        this.scoringandoverride.patchValue({
          finalrisklevel: '3'
        });
      } else if (total >= 8) {
        this.scoringandoverride.patchValue({
          finalrisklevel: '4'
        });
      }
      if(this.scoringandoverride.get('discretionaryoverride').value === '1'){
        const score = this.converttoNumber(this.scoringandoverride.get('finalrisklevel').value);
        if(score<4){
          const finalrisklevel = (score+1).toString();
          this.scoringandoverride.patchValue({
            finalrisklevel: finalrisklevel
          });
        }
      }
    }
    return true;
  }

  overrideExists():boolean{
    if(this.scoringandoverride && this.scoringandoverride.enabled){
      if(this.scoringandoverride.get('sexualabusecase').value == '1')
      return true;
      if(this.scoringandoverride.get('nonaccidental').value == '1')
      return true;
      if(this.scoringandoverride.get('severenonaccidental').value == '1')
      return true;
      if(this.scoringandoverride.get('caregiveraction').value == '1')
      return true;
    }
    return false;
  }
}
