import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../../../../@core/services/http.service';
import { CommonHttpService, AuthService, CommonDropdownsService, SessionStorageService } from '../../../../@core/services';


@Injectable()
export class AssessmentService {

  id: string;
  serviceCaseId: string;
  isServiceCase: string;
  daNumber: string;

  INTERNAL_ASSESSMENTS = ['AOD', 'cans-v2', 'MFIRA'];

  constructor(private _router: Router,
    private _commonHttpService: CommonHttpService,
    private _http: HttpService,
    private authService: AuthService,
    private storage: SessionStorageService,
    private _commonDDService: CommonDropdownsService
    ) {
      this.id = this._commonDDService.getStoredCaseUuid();
      this.daNumber = this._commonDDService.getStoredCaseNumber();
    }


  // Maintain mapping of assessments to their respective mappings
  internalAssessmentTemplateMapping = {
    'AOD Form' : '5b597d5ee881f068be283c9a',
    'cans-v2' : '5d26ec3d927e9405e4c99fb1',
    'MFIRA' : '5b6d56a7e881f068be283e60',
    'AOD/PADS Form' : '5b597d5ee881f068be283c9a',
    'CANS-OUT OF HOME PLACEMENT SERVICE' : '5b90c64e8c425c47f9dd4d41',
  };

  getservicecase() {
    this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        if (this.isServiceCase) {
          this.serviceCaseId = this._commonDDService.getStoredCaseUuid();
        } else {
          this.serviceCaseId = null;
        }

  }

  getRelationShip(primaryuserid, secondaryuserid, relationshiparray) {
    let relationshipdesc= 'Self';
    if(relationshiparray && relationshiparray.length) {
      const relationship = relationshiparray.filter(person => ( person.primaryuserid === primaryuserid && person.secondaryuserid === secondaryuserid ) );
      if(relationship && relationship.length) {
        relationshipdesc = relationship[0].description;
      }
    }

    return relationshipdesc;
  }


  // If the assessment record does not exist then create, else update the existing data
  saveAssessment(assessmentName, currentAssessmentId, submissionData) {
    // if (currentAssessmentId) {
    //   return this.patchAssessment(currentAssessmentId, submissionData);
    // } else {
      return this.createAssessment(assessmentName, submissionData, submissionData.currentSubmissionId, submissionData.assessmentStaus);
    // }
  }

  submitAssessmentForApproval(assessmentName, currentAssessmentId, currentSubmissionId, submissionData, assessmentStaus) {
    return this._commonHttpService.create(
      {
        method: 'post',
        internaltemplateid: this.internalAssessmentTemplateMapping[assessmentName],
        objectid: this.id,
        submissiondata : submissionData,
        assessmentid: currentAssessmentId,
        submissionid: currentSubmissionId,
        // routing is expecting this for service case!!!
        servicecaseid: this.id,
        assessmentstatustypekey1: assessmentStaus
      },
      'admin/assessment/createAssessmentInternal'
    );
  }

  // Save as draft and update once the assessment record is created
  patchAssessment(currentAssessmentId, submissionData) {
    let payload = {};
    payload['assessmentid'] = currentAssessmentId;
    payload['submissiondata'] = submissionData;
    return this._commonHttpService.patch(
      currentAssessmentId,
      payload,
      'admin/assessment'
    );
  }

  createAssessment(assessmentName, submissionData, currentSubmissionId, assessmentStatus) {
    return this._commonHttpService.create(
      {
        method: 'post',
        externaltemplateid: this.internalAssessmentTemplateMapping[assessmentName],
        form: this.internalAssessmentTemplateMapping[assessmentName],
        objectid: this.id,
        servicecaseid: this.serviceCaseId,
        submissiondata : submissionData,
        submissionid: currentSubmissionId,
        assessmentstatustypekey1: assessmentStatus,
      },
      'admin/assessment/Add'
    );
  }

  getAssessmentsByTemplate(objectid, objectname, assessmenttemplateid) {
    return this._commonHttpService.getArrayList(
      {
        method: 'get',
        where: {
          objectid: objectid,
          objectname: objectname,
          assessmenttemplateid: assessmenttemplateid
        }
      },
      'admin/assessment'
    );
  }

  // for now returnig this list from UI, can move to DB
  getInternalAssessments() {
    return this.INTERNAL_ASSESSMENTS;
  }

}
