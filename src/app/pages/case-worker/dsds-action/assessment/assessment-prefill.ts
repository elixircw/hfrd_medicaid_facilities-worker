import * as moment from 'moment';

import { titleCase } from '../../../../@core/common/initializer';
import { RoutingInfo } from '../../_entities/caseworker.data.model';
import { BehaviourHealth, InvolvedPerson, Medicalinformation, YouthInvolvedPersons } from '../involved-persons/_entities/involvedperson.data.model';
import { Placement } from '../service-plan/_entities/service-plan.model';
import { assessmentData } from './../_data/assessment';
import { NewUrlConfig } from '../../../newintake/newintake-url.config';
import { CommonHttpService, AuthService, DataStoreService } from '../../../../@core/services';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

export class AssessmentPreFill {
    householdmember: boolean;
    persondetail: InvolvedPerson[];
    educationDetails: any[];
    victimChild: boolean;
    drugList: any[];
    constructor(private involvedPersons: InvolvedPerson[], private routingInfo: RoutingInfo[], private routingSupervisors: any[], private token: any,
        private _commonHttpService?: CommonHttpService,
        private _dataStoreService?: DataStoreService) {}
    get caseHeadDetail() {
        // return this.involvedPersons.filter(item => {
        //     return item.rolename === 'CASEHD' || (item.roles && item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'CASEHD').length);
        // });
        const LEGAL_GUARDIAN = ['LG'];
        const res = this.involvedPersons.filter(child => {
            let childFound = false;
            const roles = Array.isArray(child.roles) ? child.roles : [];
            roles.forEach(role => {
                const childCategory = LEGAL_GUARDIAN.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });
        return Array.isArray(res) ? res : [];
    }

    get headofHousehold() {
        const res = this.involvedPersons.filter(person => {
            const personFound =  person.isheadofhousehold ? person.isheadofhousehold : false;
            return personFound;
        });
        return Array.isArray(res) ? res : [];
    }
    get careGiverDetails() {
        // return this.involvedPersons.filter(item => {
        //     return item.rolename === 'CARTKR' || (item.roles && item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'CARTKR').length);
        // });
        // const REPORTED_CHILD = ['CARTKR'];
        const ADULT_CATEGORIES = ['CG', 'GRD', 'LG', 'FP', 'PARENT', 'MP', 'RAS'];
        const list = this.involvedPersons.filter(child => {
            let childFound = false;
            const roles = Array.isArray(child.roles) ? child.roles : [];
            roles.forEach(role => {
                const childCategory = ADULT_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });

        return Array.isArray(list) ? list : [];
    }
    get reportedChildDetails() {
        /* return this.involvedPersons.filter(item => {
            return item.rolename === 'RC' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RC').length);
        }); */
        // return this.involvedPersons;
        const REPORTED_CHILD = ['RC', 'CHILD'];
        const res = this.involvedPersons.filter(child => {
            let childFound = false;
            if (child.dateofdeath) {
                return false;
            }
            const roles = Array.isArray(child.roles) ? child.roles : [];
            roles.forEach(role => {
                const childCategory = REPORTED_CHILD.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });
        return Array.isArray(res) ? res : [];
    }
    get getChildList() {
        const CHILD_CATEGORIES = ['CHILD', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC', 'AV'];
        const childlist = this.involvedPersons.filter(child => {
            let childFound = false;
            if (child.dateofdeath) {
                return false;
            }
            const roles = Array.isArray(child.roles) ? child.roles : [];
            roles.forEach(role => {
                const childCategory = CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });
        return Array.isArray(childlist) ? childlist : [];
    }

    get getVictimList() {
        const CHILD_CATEGORIES = ['AV'];
        const res = this.involvedPersons.filter(child => {
            let childFound = false;
            if (child.dateofdeath) {
                return false;
            }
            const roles = Array.isArray(child.roles) ? child.roles : [];
            roles.forEach(role => {
                const childCategory = CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });
        return Array.isArray(res) ? res : [];
    }

    get getChildListSafeC() {
        const CHILD_CATEGORIES = ['CHILD', 'AV'];
        const res = this.involvedPersons.filter(child => {
            let childFound = false;
            if (child.dateofdeath) {
                return false;
            }
            const roles = Array.isArray(child.roles) ? child.roles : [];
            roles.forEach(role => {
                const childCategory = CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });
        return Array.isArray(res) ? res : [];
    }

    get getChildListWithHouseHold() {
        const CHILD_CATEGORIES = ['CHILD', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC', 'AV', 'Youth', 'YOUTH'];
        const res = this.involvedPersons.filter(child => {
            let childFound = false;
            const roles = Array.isArray(child.roles) ? child.roles : [];
            roles.forEach(role => {
                const childCategory = CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory && child.ishousehold === 1) {
                    childFound = true;
                    return;
                }
            });
            return childFound;
        });
        return Array.isArray(res) ? res : [];
    }

    get getHouseHoldWithoutChild() {
        const CHILD_CATEGORIES = ['CHILD', 'OTHERCHILD', 'AV', 'AM'];
        const res = this.involvedPersons.filter(child => {
            let personFound = false;
            const roles = Array.isArray(child.roles) ? child.roles : [];
            child.roles.forEach(role => {
                const childCategory = CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (!childCategory && child.ishousehold === 1) {
                    personFound = true;
                    return;
                }
            });
            return personFound;
        });
        return Array.isArray(res) ? res : [];
    }

    get careGiverDetailsWithHouseHold() {
        const ADULT_CATEGORIES = ['CG', 'GRD', 'LG', 'FP', 'PARENT', 'MP', 'RAS', 'ADV', 'AM',
        'CASAWRKER', 'CDCP', 'COURTTERAPST', 'FN', 'LE', 'LR', 'MHP', 'OTH', 'OtherADULT', 'PRVD', 'RELATIVE', 'SSP', 'TES'];
        const res = this.involvedPersons.filter(adult => {
            let adultFound = false;
            const roles = Array.isArray(adult.roles) ? adult.roles : [];
            roles.forEach(role => {
                const adultCate = ADULT_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (adultCate && adult.ishousehold === 1) {
                    adultFound = true;
                    return;
                }
            });
            return adultFound;
        });
        return Array.isArray(res) ? res : [];
    }

    get legalGuardianCareTakerlist() {
        const ADULT_CATEGORIES = ['CACA', 'LG'];
        const res = this.involvedPersons.filter(adult => {
            let adultFound = false;
            const roles = Array.isArray(adult.roles) ? adult.roles : [];
            roles.forEach(role => {
                const adultCate = ADULT_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (adultCate) {
                    adultFound = true;
                    return;
                }
            });
            return adultFound;
        });
        return Array.isArray(res) ? res : [];
    }


    get reportedAdultDetails() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'RA' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'RA').length);
        });
    }

    get reportedDjsDetails() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'Youth' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'Youth').length);
        });
    }

    get childDetails() {
        return this.involvedPersons.filter(item => {
            return item.rolename === 'CHILD' || (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'CHILD').length);
        });
    }

    get under18childDetails() {
        const res = this.involvedPersons.filter(
            item => {
                const roles = Array.isArray(item.roles) ? item.roles : [];
                const list = ['AV', 'SIBLING', 'PAC', 'NVC', 'OTHERCHILD', 'CHILD', 'BIOCHILD'];
                return roles.some(itm => list.includes(itm.intakeservicerequestpersontypekey) && (itm.intakeservicerequestpersontypekey !== 'AV'));
            }
        );
        return Array.isArray(res) ? res : [];
        // return this.involvedPersons.filter(item => {
        //     return (
        //         (item.rolename === 'CHILD' ||
        //         (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'CHILD').length) ||
        //         item.rolename === 'BIOCHILD' ||
        //         (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'BIOCHILD').length) ||
        //         item.rolename === 'NVC' ||
        //         (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'NVC').length) ||
        //         item.rolename === 'OTHERCHILD' ||
        //         (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'OTHERCHILD').length) ||
        //         item.rolename === 'PAC' ||
        //         (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'PAC').length) ||
        //         item.rolename === 'SIBLING' ||
        //         (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey === 'SIBLING').length)) &&
        //         item.rolename !== 'AV' ||
        //         (item.roles.length && item.roles.filter(itm => itm.intakeservicerequestpersontypekey !== 'AV').length)
        //     );
        // });
    }

    get safetySignPersons() {
        const res = this.involvedPersons.filter(
            item => {
                const roles = Array.isArray(item.roles) ? item.roles : [];
                const list = ['RC', 'AV', 'CHILD', 'BIOCHILD'];
                return roles.some(itm => !list.includes(itm.intakeservicerequestpersontypekey));
            }
        );
        return Array.isArray(res) ? res : [];
    }

    get isHouseHold() {
        return this.involvedPersons.filter(item => item.ishousehold === 1);
    }

    get nonHouseHold() {
        return this.involvedPersons.filter(item => item.ishousehold === 0 && item.rolename !== 'RA');
    }

    get supervisorDetail() {
        return this.routingInfo.find(item => item.fromrole === 'Supervisor');
    }

    get allSupervisors() {
        return this.routingSupervisors;
    }

    get caseWorkerDetail() {
        return this.routingInfo.find(item => item.torole === 'Case Worker');
    }
    get intakeWorkerDetails() {
        return this.routingInfo.find(item => item.fromrole === 'Intake Worker');
    }
    get participantDetail() {
        return this.involvedPersons.filter(item => {
            if (item.roles) {
                // tslint:disable-next-line:max-line-length
                const getRole = item.roles.filter(
                    items => items.intakeservicerequestpersontypekey === 'BIOPPARNT' || items.intakeservicerequestpersontypekey === 'PARENT' || items.intakeservicerequestpersontypekey === 'LG'
                );
                if (getRole.length) {
                    return item;
                }
            }
            // return item.rolename === 'BIOPPARNT' || item.rolename === 'PARENT' || item.rolename === 'LG';
        });
    }
    get houseHoldDetail() {
        return this.involvedPersons.filter(item => {
            if (item.dateofdeath) {
                return false;
            }
            return item.ishousehold === 1;
        });
    }
    private getFullName(involvedPerson: InvolvedPerson) {
        let fullName = '';
        if (involvedPerson.fullname) {
            fullName += involvedPerson.fullname;
            return fullName.toUpperCase();
        }
        if (involvedPerson.firstname) {
            fullName += involvedPerson.firstname;
        }
        if (involvedPerson.lastname) {
            fullName += ', ' + involvedPerson.lastname;
        }
        return fullName.toUpperCase();
    }
    private getMfraFullName(involvedPerson: InvolvedPerson) {
        let fullName = '';
        if (involvedPerson.fullname) {
            fullName += involvedPerson.fullname;
        }
        return fullName.toUpperCase();
    }
    private getCaseHeadId(data) {
        return data.cjamspid;
    }
    private getAge(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }
    private getFormattedDate(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            return moment(new Date(dateValue)).format('YYYY-MM-DD');
        } else {
            return '';
        }
    }
    private getChildFormattedAddress(data) {
        const address = [`${data.address} ${data.address2}`, data.state, data.city, data.zipcode];
        return address.join(', ');
    }
    private getAddress(involvedPerson: InvolvedPerson) {
        let address = involvedPerson.address ? involvedPerson.address : '';
        if (involvedPerson.city) {
            address += ', ' + involvedPerson.city;
        } else {
            address = address;
        }
        return address.toUpperCase();
    }

    private getMedicationAddress(addressDetail: Medicalinformation) {
        let address = addressDetail.address1 ? addressDetail.address1 : '';

        if (addressDetail.city) {
            address += ' ' + addressDetail.city;
        }
        if (addressDetail.zip) {
            address += ' ' + addressDetail.zip;
        }
        return address.toUpperCase();
    }

    private getBehaviourAddress(addressDetail: BehaviourHealth) {
        let address = '';
        if (addressDetail.behavioraladdress1) {
            address += addressDetail.behavioraladdress1;
        }
        if (addressDetail.behavioralcity) {
            address += addressDetail.behavioralcity;
        }
        if (addressDetail.behavioralzip) {
            address += addressDetail.behavioralzip;
        }
        return address.toUpperCase();
    }

    public fillSila(submissionData) {
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        if (this.supervisorDetail && submissionData['supervisorname'] === '') {
            submissionData['supervisorname'] = this.supervisorDetail.fromusername;
        }
        submissionData['userrole'] = this.token.role.name;
        if (this.caseWorkerDetail && submissionData['caseworkername'] === '') {
            submissionData['caseworkername'] =  this.caseWorkerDetail.tousername;
        }
       return submissionData;
     }
    public fillSafeC(submissionData, daNumber) {
        console.log('fillSafeC', submissionData);
      /*  if (submissionData['submit'] === true) {
            return submissionData;
        } */
        const address = JSON.parse(localStorage.getItem('userProfile'));
        if (address && address.userprofileaddress && address.userprofileaddress.length > 0) {
            submissionData['ldss'] = address.userprofileaddress[0].county;
        }
        if (submissionData['dateassessmentinitiated'] === '') {
            submissionData['dateassessmentinitiated'] = moment(new Date());
        } else {
            submissionData['dateassessmentinitiated'] = moment(submissionData['dateassessmentinitiated']);
        }
        if (submissionData['dateoflastsafetyplan'] === '' && this._dataStoreService.getData('safetyassessmentcompletiondate')) {
            submissionData['dateoflastsafetyplan']  = moment(this._dataStoreService.getData('safetyassessmentcompletiondate')).format('YYYY-MM-DD hh:mm a');
         }
        let lgPersonId;
        if (this.headofHousehold.length) {
            submissionData['headofhouseholdname'] = this.headofHousehold[0].firstname + ' ' + this.headofHousehold[0].lastname;
        }
        if (this.caseHeadDetail.length) {
            lgPersonId = this.caseHeadDetail[0].personid;
            submissionData['caseheadsname'] = this.caseHeadDetail[0].firstname + ' ' + this.caseHeadDetail[0].lastname;
            // submissionData['relationship'] = titleCase(this.caseHeadDetail[0].relationship);
            if (submissionData['age'] === '') {
                submissionData['age'] = this.getAge(this.caseHeadDetail[0].dob);
            }
        }
        if (submissionData['nameofcaregiver'] = '') {
            if (this.careGiverDetails.length) {
                submissionData['nameofcaregiver'] = [];
                this.careGiverDetails.map(item => {
                    submissionData['nameofcaregiver'].push(item.firstname + ' ' + item.lastname);
                });
            }
        }

        if (submissionData['cpscaseid'] === '') {
            submissionData['cpscaseid'] = daNumber;
        }
        // if (this.reportedChildDetails.length) {
        // submissionData['childsname'] = this.getFullName(this.reportedChildDetails[0]);
        const childList = [];
        const childListWithComma = [];
        const childsNames = new Array();
        const childsNamesWithComma = new Array();
        let count = 0;
        this.getChildList.forEach(child => {
            const personid = child.personid;
            if (lgPersonId && child && child.relationshiparray && child.relationshiparray.length > 0) {
                if (count === 0) {
                    for (let i = 0; i < child.relationshiparray.length; i++) {
                        if (child.relationshiparray[i].primaryuserid === personid && child.relationshiparray[i].secondaryuserid === lgPersonId) {
                            submissionData['relationship'] = titleCase(child.relationshiparray[i].description);
                        }
                    }
                }
            }
            if (child.firstname && child.lastname) {
                childsNames.push(child.firstname + ' ' + child.lastname);
                childsNamesWithComma.push(this.getFullName(child));
            }
            count = count + 1;
        });
        submissionData['childs_names_json'] = JSON.stringify(childsNames);

        if (submissionData && submissionData.childdatagrid && submissionData.childdatagrid.length > 0) {
            if (submissionData.childdatagrid[0].age || !(submissionData.childdatagrid[0].age === '')) {
                for (let i = 0; i < submissionData.childdatagrid.length; i++) {
                    if ( submissionData.childdatagrid[i].childname) {
                        childList.push({
                            childname: submissionData.childdatagrid[i].childname,
                            clientid: submissionData.childdatagrid[i].clientid,
                            age: submissionData.childdatagrid[i].age
                        });
                        childListWithComma.push({seconename: submissionData.childdatagrid[i].childname ,
                        seconeage: this.getAge(submissionData.childdatagrid[i].dob) });
                    }
                }
            } else {
                this.getChildListSafeC.forEach(child => {
                    if (child.firstname && child.lastname) {
                        childList.push({
                            childname: child.firstname + ' ' + child.lastname,
                            clientid: child.cjamspid,
                            age: this.getAge(child.dob)
                        });
                        childListWithComma.push({seconename: child.firstname + ' ' + child.lastname , seconeage: this.getAge(child.dob) });
                    }
                });
            }
        } else {
            this.getChildListSafeC.forEach(child => {
                if (child.childname) {
                    childList.push({
                        childname: child.firstname + ' ' + child.lastname,
                        clientid: child.cjamspid,
                        age: this.getAge(child.dob)
                    });
                    childListWithComma.push({seconename: child.firstname + ' ' + child.lastname , seconeage: this.getAge(child.dob) });

                }
            });
        }
        const caregiversList = [];
        if (this.safetySignPersons && this.safetySignPersons.length) {
        this.safetySignPersons.forEach(person => {
            let rolesDesc = '';
            if (person.roles && person.roles.length) {
                person.roles.forEach((role, index) => {
                    rolesDesc =  rolesDesc +  role.typedescription + (( index !== person.roles.length - 1) ? ', ' : '');
                });
            }
            caregiversList.push(person.firstname + ' ' + person.lastname + ' (' + rolesDesc + ') ');
        });
        }
        // Check Previous completion Date
        submissionData['caregiversList'] = JSON.stringify(caregiversList);
        submissionData['childs_info_json'] = JSON.stringify(childList);
        submissionData['userrole'] = this.token.role.name;
        submissionData['child_info_with_comma_json'] = JSON.stringify(childListWithComma);
        submissionData['childdatagrid'] = childList;
        console.log('this.getChildList', childList);
        if (this.reportedChildDetails && this.reportedChildDetails.length) {
            if (submissionData['age'] === '') {
                submissionData['age'] = this.getAge(this.reportedChildDetails[0].dob);
            }
        }
        // }
        const addchildren = [];
        if (submissionData['addchildren'] && submissionData['addchildren'].length && !(submissionData['addchildren'][0].seconename === '') ) {
            for (let i = 0; i < submissionData['addchildren'].length; i++) {
                addchildren.push(submissionData['addchildren'][i]);
            }
        } else {
            if (this.under18childDetails && this.under18childDetails.length) {
                submissionData['child_names_with_comma_json'] = JSON.stringify(childsNamesWithComma);
                 const under18Years = this.under18childDetails.filter(age => this.getAge(age.dob) <= 18);
                 if (under18Years && under18Years.length) {
                     under18Years.forEach(child => {
                        this.victimChild = false;
                         if (child.dateofdeath) {
                             return false;
                         }
                         if (child.ishousehold === 1 && !child.dateofdeath && child.roles.length > 0) {
                             for (let i = 0; i < child.roles.length; i++) {
                                if (child.roles[i].intakeservicerequestpersontypekey === 'AV') {
                                    this.victimChild = true;
                                }
                             }
                            if (!this.victimChild) {
                                addchildren.push({ seconename: child.firstname + ' ' + child.lastname , seconeage: this.getAge(child.dob) });
                            }
                         }
                     });
                 }
             }
        }
        submissionData['addchildren'] = addchildren;
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        if (submissionData['supervisorname'] === '') {
            if (this.supervisorDetail) {
                submissionData['supervisorname'] = this.supervisorDetail.fromusername;
                submissionData['supervisortitle'] = this.supervisorDetail.fromrole;
                // submissionData['safetyassessmentapprovaldate'] = this.supervisorDetail.routedon ? this.supervisorDetail.routedon : '';
            }
        }
        if (submissionData['workersname'] === '') {
            if (this.caseWorkerDetail) {
                // submissionData['workersname'] = this.caseWorkerDetail.tousername;
                // @TM: setting the caseworker name to current logged in user if it is a caseworker
                submissionData['workersname'] =
                    this.token.role.name === 'field' ? this.token.user.userprofile.fullname : this.caseWorkerDetail.tousername;
                submissionData['workertitle'] = this.caseWorkerDetail.torole;
                // Begin - D-06446
                // submissionData['safetyassessmentcompletiondate'] = this.caseWorkerDetail.routedon ? this.caseWorkerDetail.routedon : '';
                // End - D-06446
            }
        }
        // safetySignPersons
        if (submissionData['associatedetails'] &&
            submissionData['associatedetails'][0].intakeservicerequestactorid === '') {
            if (this.safetySignPersons && submissionData.assessmentreviewed === 'InProcess') {
                submissionData['associatedetails'] = [];
                this.safetySignPersons.forEach(person => {
                    const personRole = [];
                    if (person.roles && person.roles.length) {
                        person.roles.forEach(role => {
                            personRole.push(role.typedescription);
                        });
                    }
                    submissionData['associatedetails'].push({
                        refusetosign: false,
                        printedname: person.firstname + ' ' + person.lastname,
                        title: personRole.join('; '),
                        intakeservicerequestactorid: person.intakeservicerequestactorid,
                        email: person.email
                    });
                });
            }
        }

        return submissionData;
    }

    public fillAOD(submissionData) {
        // if(submissionData.submit) {
        //     return submissionData;
        // }
        if (this.supervisorDetail) {
            submissionData['panel1553852294703002ColumnsChildWelfareCaseworkerssignature'] = submissionData['panel1553852294703002ColumnsChildWelfareCaseworkerssignature'] ?
            submissionData['panel1553852294703002ColumnsChildWelfareCaseworkerssignature'] : this.supervisorDetail.tousername;
            submissionData['panel58292522685816ColumnsChildWelfareCaseworker'] = submissionData['panel58292522685816ColumnsChildWelfareCaseworker'] ?
            submissionData['panel58292522685816ColumnsChildWelfareCaseworker'] : this.supervisorDetail.tousername;
            submissionData['panel58292522685816ColumnsPhoneNumber'] = submissionData['panel58292522685816ColumnsPhoneNumber'] ?
            submissionData['panel58292522685816ColumnsPhoneNumber'] :  this.supervisorDetail.phonenumber;
            // if (this.supervisorDetail) {
                submissionData['supervisorname'] =  submissionData['supervisorname'] ? submissionData['supervisorname'] : this.supervisorDetail.fromusername;
                submissionData['supervisortitle'] = submissionData['supervisortitle'] ? submissionData['supervisortitle'] : this.supervisorDetail.fromrole;
            // }
           // submissionData['supervisortitle'] = this.supervisorDetail.fromrole;
        }
        if (this.caseWorkerDetail) {
            submissionData['panel58292522685816ColumnsChildWelfareCaseworker2'] = submissionData['panel58292522685816ColumnsChildWelfareCaseworker2'] ?
                submissionData['panel58292522685816ColumnsChildWelfareCaseworker2'] : this.caseWorkerDetail.fromusername;
            submissionData['panel58292522685816ColumnsPhoneNumber2'] = submissionData['panel58292522685816ColumnsPhoneNumber2'] ?
                submissionData['panel58292522685816ColumnsPhoneNumber2'] : this.caseWorkerDetail.phonenumber;
            // submissionData['workertitle'] = this.caseWorkerDetail.fromrole;
            // submissionData['workernameandId'] = this.caseWorkerDetail.tousername;
        }

        if (this.reportedChildDetails.length) {
            submissionData['panel58292522685816ColumnsChildWelfareCaseworker3'] = submissionData['panel58292522685816ColumnsChildWelfareCaseworker3'] ?
                submissionData['panel58292522685816ColumnsChildWelfareCaseworker3'] : this.getFullName(this.reportedChildDetails[0]);
            submissionData['panel58292522685816ColumnsDate2'] = submissionData['panel58292522685816ColumnsDate2'] ?
                submissionData['panel58292522685816ColumnsDate2'] : this.getFormattedDate(this.reportedChildDetails[0].dob);
            submissionData['panel58292522685816Columns3ClientId'] = submissionData['panel58292522685816Columns3ClientId'] ?
                submissionData['panel58292522685816Columns3ClientId'] : this.reportedChildDetails[0].phonenumber; // Client Telephone Number
            submissionData['panel58292522685816Columns4McOifapplicable'] = submissionData['panel58292522685816Columns4McOifapplicable'] ?
                submissionData['panel58292522685816Columns4McOifapplicable'] : this.getChildFormattedAddress(this.reportedChildDetails[0]); // Client Address
        }
        if (this.reportedChildDetails.length) {
            const childNames = new Array();
            submissionData['clientNameList'] = (Array.isArray(submissionData['clientNameList']) && submissionData['clientNameList'].length) ?
                submissionData['clientNameList'] : [];
            submissionData['panel10805544092530561DataGrid'] = [];
            this.reportedChildDetails.forEach(child => {
                childNames.push(this.getFullName(child));
                submissionData['clientNameList'].push({
                    ClientName: this.getFullName(child),
                    ClientDOB: this.getFormattedDOB(child.dob),
                    ClientAddress: this.getFormattedAddress(child),
                    ClientContact: child.phonenumber
                });
            });
            submissionData['childList'] = JSON.stringify(childNames);
        }

             this.drugList = this._dataStoreService.getData('CASEWORKER_DRUG_LIST');
             if ( this.drugList && this.drugList.length && Array.isArray(this.drugList)) {
                submissionData['drugList'] = JSON.stringify(this.drugList);
             }
             submissionData['userrole'] = this.token.role.name;
            submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);

        return submissionData;
    }

    public fillRiskAssessmentLegacy(submissionData, daNumber) {
        console.log('Risk Assessment Form Started');
        if (this.caseHeadDetail.length) {
            submissionData['Casehead'] = this.caseHeadDetail[0].firstname + ' ' + this.caseHeadDetail[0].lastname;

        }
        if (submissionData['cpsid'] === '') {
            submissionData['cpsid'] = daNumber;
        }
        if (this.supervisorDetail) {
            submissionData['SupervisorName'] =  submissionData['SupervisorName'] ? submissionData['SupervisorName'] : this.supervisorDetail.fromusername;
        }

        const childList = [];
        const childandHouseholdList = [];
        const childcharacteristicsList = [];
        const childAgeList = [];
        const childListWithComma = [];
        const childsNames = new Array();
        const childsNamesWithComma = new Array();
        let count = 0;
        this.getChildList.forEach(child => {
            if (child.firstname && child.lastname) {
                childsNames.push(child.firstname + ' ' + child.lastname);
                childsNamesWithComma.push(this.getFullName(child));
            }
            count = count + 1;
        });
        // submissionData['childs_names_json'] = JSON.stringify(childsNames);

        if (submissionData && submissionData.CHILDRENANDFAMILYINHOUSEHOLD2 && submissionData.CHILDRENANDFAMILYINHOUSEHOLD2.length > 0) {
            if (submissionData.CHILDRENANDFAMILYINHOUSEHOLD2[0].age || !(submissionData.CHILDRENANDFAMILYINHOUSEHOLD2[0].age === '')) {
                for (let i = 0; i < submissionData.CHILDRENANDFAMILYINHOUSEHOLD2.length; i++) {
                    if ( submissionData.CHILDRENANDFAMILYINHOUSEHOLD2[i].childname) {
                        childandHouseholdList.push({
                            childname: submissionData.CHILDRENANDFAMILYINHOUSEHOLD2[i].childname,
                            age: submissionData.CHILDRENANDFAMILYINHOUSEHOLD2[i].age,
                            dob: submissionData.CHILDRENANDFAMILYINHOUSEHOLD2[i].dob
                        });
                    }
                }
            } else {

                if (this.houseHoldDetail.length) {
                    this.houseHoldDetail.forEach(child => {
                    if (child.firstname && child.lastname) {
                        childandHouseholdList.push({
                            childname: child.firstname + ' ' + child.lastname,
                            age: this.getAge(child.dob),
                            dob: this.getFormattedDOB(child.dob)
                        });
                      }
                });
              }
            }
        } else {
            if (this.houseHoldDetail.length) {
                this.houseHoldDetail.forEach(child => {
                if (child.childname) {
                    childandHouseholdList.push({
                        childname: child.firstname + ' ' + child.lastname,
                        age: this.getAge(child.dob),
                        dob: this.getFormattedDOB(child.dob)
                    });
                }
            });
           }
        }


        if (submissionData && submissionData.CHILDCHARACTERISTICS.DataGrid1 && submissionData.CHILDCHARACTERISTICS.DataGrid1.length > 0) {
            if (submissionData.CHILDCHARACTERISTICS.DataGrid1[0].AGE || !(submissionData.CHILDCHARACTERISTICS.DataGrid1[0].AGE === '')) {
                for (let i = 0; i < submissionData.CHILDCHARACTERISTICS.length; i++) {
                    if ( submissionData.CHILDCHARACTERISTICS.DataGrid1[i].ChildName) {
                        childcharacteristicsList.push({
                            ChildName: submissionData.CHILDCHARACTERISTICS.DataGrid1[i].ChildName,
                            ChildFunctioning: submissionData.CHILDCHARACTERISTICS.DataGrid1[i].ChildFunctioning,
                            RiskBasedonAGE: submissionData.CHILDCHARACTERISTICS.DataGrid1[i].RiskBasedonAGE,
                            AGE: submissionData.CHILDCHARACTERISTICS.DataGrid1[i].AGE,
                            CapacitytoSelfProtect: submissionData.CHILDCHARACTERISTICS.DataGrid1[i].CapacitytoSelfProtect
                        });
                    }
                }
            } else {

                if (this.getChildList.length) {
                    this.getChildList.forEach(child => {
                    if (child.firstname && child.lastname) {
                        childcharacteristicsList.push({
                            ChildName: child.firstname + ' ' + child.lastname,
                            AGE: this.getAge(child.dob)
                        });
                      }
                });
              }
            }
        } else {
            if (this.getChildList.length) {
                this.getChildList.forEach(child => {
                if (child.childname) {
                    childcharacteristicsList.push({
                        childname: child.firstname + ' ' + child.lastname,
                        age: this.getAge(child.dob)
                    });
                }
            });
           }
        }


        if (submissionData && submissionData.AGE2.DataGrid22 && submissionData.AGE2.DataGrid22.length > 0) {
            if (submissionData.AGE2.DataGrid22[0].AGE || !(submissionData.AGE2.DataGrid22[0].AGE === '')) {
                for (let i = 0; i < submissionData.AGE2.DataGrid22.length; i++) {
                    if ( submissionData.AGE2.DataGrid22[i].ChildName) {
                        childAgeList.push({
                            ChildName: submissionData.AGE2.DataGrid22[i].ChildName,
                            ChildFunctioning: submissionData.AGE2.DataGrid22[i].ChildFunctioning,
                            RiskBasedonAGE: submissionData.AGE2.DataGrid22[i].RiskBasedonAGE,
                            AGE: submissionData.AGE2.DataGrid22[i].AGE,
                            CapacitytoSelfProtect: submissionData.AGE2.DataGrid22[i].CapacitytoSelfProtect
                        });
                    }
                }
            } else {

                if (this.getChildList.length) {
                    this.getChildList.forEach(child => {
                    if (child.firstname && child.lastname) {
                        childAgeList.push({
                            ChildName: child.firstname + ' ' + child.lastname,
                            AGE: this.getAge(child.dob)
                        });
                      }
                });
              }
            }
        } else {
            if (this.getChildList.length) {
                this.getChildList.forEach(child => {
                if (child.childname) {
                    childAgeList.push({
                        childname: child.firstname + ' ' + child.lastname,
                        age: this.getAge(child.dob)
                    });
                }
            });
           }
        }

        const caregiversList = [];
        if (this.safetySignPersons && this.safetySignPersons.length) {
        this.safetySignPersons.forEach(person => {
            let rolesDesc = '';
            if (person.roles && person.roles.length) {
                person.roles.forEach((role, index) => {
                    rolesDesc =  rolesDesc +  role.typedescription + (( index !== person.roles.length - 1) ? ', ' : '');
                });
            }
            caregiversList.push(person.firstname + ' ' + person.lastname + ' (' + rolesDesc + ') ');
        });
        }
        // submissionData['caregiversList'] = JSON.stringify(caregiversList);
        // submissionData['panel423382371313028ChildrenandfamilyinhouseholdColumns'] = JSON.stringify(childList);
        // submissionData['userrole'] = this.token.role.name;
        // submissionData['child_info_with_comma_json'] = JSON.stringify(childListWithComma);
        submissionData['CHILDRENANDFAMILYINHOUSEHOLD2'] = childandHouseholdList;
        submissionData['CHILDCHARACTERISTICS']['DataGrid1'] = childcharacteristicsList;
        submissionData['AGE2']['DataGrid22'] = childAgeList;
        return submissionData;
    }
    private getFormattedDOB(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            return moment(new Date(dateValue)).format('MM/DD/YYYY');
        } else {
            return '';
        }
    }
    private getFormattedAddress(data) {
        const address =  (data.address ? data.address + ',' : '')
                          + (data.address2 ? data.address2  + ',' : '')
                        + (data.state ? data.state + ',' : '')
                         + (data.city ? data.city + ',' : '')
                         + (data.zipcode ? data.zipcode : '');
        return address;
    }
    private getrelationship(person1id, person2id) {
        if (person1id === person2id) {
            return 'Self';
        }
        const data = this._dataStoreService.getData('CASEWORKER_PERSON_RELATIONS');
        const personsList = (data) ? data : [];
        const person1 = personsList.find(item => item.personid === person1id);
        const person1relationList = (person1 && Array.isArray(person1.relation)) ? person1.relation : [];
        const person2inperson1 = (person1relationList) ? person1relationList.find(item => item.person2id === person2id) : null;
        return (person2inperson1) ? person2inperson1.description : '';
    }
    public fillCansF(submissionData, daNumber) {
        submissionData['userrole'] = this.token.role.name;
        submissionData['caseheaddatetime'] = submissionData['caseheaddatetime'] ? submissionData['caseheaddatetime'] : moment(new Date()).format('YYYY-MM-DD hh:mm a');
        let caseheadid = '';
        if (this.caseHeadDetail.length) {
            caseheadid = this.caseHeadDetail[0].personid;
            submissionData['caseheadname'] =  submissionData['caseheadname'] ? submissionData['caseheadname'] : this.getFullName(this.caseHeadDetail[0]);
            submissionData['caseheadid'] = submissionData['caseheadid']  ? submissionData['caseheadid']  : this.getCaseHeadId(this.caseHeadDetail[0]);
            submissionData['caregiverrelationship'] =  submissionData['caregiverrelationship'] ? submissionData['caregiverrelationship'] : titleCase(this.caseHeadDetail[0].relationship);
        }
//        if (!(submissionData['familygrid'].length) || submissionData['familygrid'][0].caregiveryouthname === '') {
            submissionData['familygrid'] = [];
            if (this.careGiverDetailsWithHouseHold.length) {
                this.careGiverDetailsWithHouseHold.forEach(child => {
                    submissionData['familygrid'].push({
                        caregiveryouthname: this.getFullName(child),
                        caregiveryouthdob: this.getFormattedDate(child.dob),
                        caregiveryouthage: this.getAge(child.dob),
                        caregiveryouthrelationship: this.getrelationship(child.personid, caseheadid) // titleCase(child.relationship)
                        // caregiveryouthschool: educationname
                    });
                });
            }
/*        } else {
            if (submissionData['familygrid'].length) {
                submissionData['familygrid'].forEach(child => {
                    submissionData['familygrid'].push({
                        caregiveryouthname: submissionData['familygrid'].caregiveryouthname,
                        caregiveryouthdob: submissionData['familygrid'].caregiveryouthdob,
                        caregiveryouthage: submissionData['familygrid'].caregiveryouthage,
                        caregiveryouthrelationship: submissionData['familygrid'].caregiveryouthrelationship
                        // caregiveryouthschool: educationname
                    });
                });
            }
        } */
        const childList = [];
//        if (!(submissionData['familygrid1'].length) || submissionData['familygrid1'][0].fg1childname === '') {
            this.getChildList.forEach(child => {
                if (child.firstname && child.lastname) {
                    childList.push({
                        fg1childname: child.firstname + ' ' + child.lastname,
                        fg1childdob: child.dob,
                        fg1childschool: child.schoolname,
                        fg1childrelationship: this.getrelationship(child.personid, caseheadid),
                        fg1childage: this.getAge(child.dob),
                    });
                }
            });
/*        } else {
            submissionData['familygrid1'].forEach(child => {
                if (child.fg1childname) {
                    childList.push({
                        fg1childname: child.fg1childname,
                        fg1childdob: child.fg1childdob,
                        fg1childschool: child.fg1childschool,
                        fg1childrelationship: child.fg1childrelationship,
                        fg1childage: child.fg1childage,
                    });
                }
            });
        } */
        submissionData['familygrid1'] = childList;
        /* cansf form FAMILY AND HOUSEHOLD COMPOSITION not having the below params so commenting this temp
        if (this.getChildListWithHouseHold.length) {
            this.getChildListWithHouseHold.forEach(child => {
                submissionData['familygrid1'].push({
                    caregiveryouthname: this.getFullName(child),
                    caregiveryouthdob: this.getFormattedDate(child.dob),
                    caregiveryouthage: this.getAge(child.dob),
                    caregiveryouthrelationship: titleCase(child.relationship)
                    // primarycaregiver: ''
                });
            });
        } */
        submissionData['cpscaseid'] = submissionData['cpscaseid'] ? submissionData['cpscaseid'] : daNumber;
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        if (this.supervisorDetail) {
            submissionData['supervisorname'] =  submissionData['supervisorname'] ? submissionData['supervisorname'] : this.supervisorDetail.fromusername;
            submissionData['supervisortitle'] = submissionData['supervisortitle'] ? submissionData['supervisortitle'] : this.supervisorDetail.fromrole;
        }
        if (this.caseWorkerDetail) {
            submissionData['workersname'] =  submissionData['workersname'] ? submissionData['workersname'] : this.caseWorkerDetail.fromusername;
            submissionData['workertitle'] = submissionData['workertitle'] ? submissionData['workertitle'] : this.caseWorkerDetail.fromrole;
            submissionData['workernameandId'] = submissionData['workernameandId'] ? submissionData['workernameandId'] : this.caseWorkerDetail.tousername;
        }
        submissionData['workerdate'] = submissionData['workerdate'] ? submissionData['workerdate'] : moment(new Date()).format('YYYY-MM-DD hh:mm a');
        // submissionData['supervisordate'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
        return submissionData;
    }
    public fillMFRR(submissionData, daNumber) {
        submissionData['userrole'] = this.token.role.name;
        submissionData['assessmentInitiated'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
        if (this.caseHeadDetail.length) {
            submissionData['caseheadname'] = this.getFullName(this.caseHeadDetail[0]);
        }
        if (!submissionData['familygrid'] || !submissionData['familygrid'].find(item => item.childrenname)) {
            submissionData['familygrid'] = [];
            if (this.houseHoldDetail.length) {
                this.houseHoldDetail.forEach(house => {
                    submissionData['familygrid'].push({
                        childrenname: this.getFullName(house),
                        childrendob: this.getFormattedDate(house.dob),
                        childrenage: this.getAge(house.dob),
                        childrenrelationship: titleCase(house.relationship)
                    });
                });
            }
        }
        if (this.caseWorkerDetail) {
            submissionData['caseworkername'] = this.caseWorkerDetail.tousername;
        }
        if (this.supervisorDetail) {
            submissionData['supervisorname'] = submissionData['supervisorname'] ? submissionData['supervisorname'] : this.supervisorDetail.fromusername;
         }
        submissionData['servicecaseID'] = daNumber;
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        return submissionData;
    }
    public fillMFRA(submissionData, daNumber) {
        submissionData['userrole'] = this.token.role.name;
        submissionData['assessmentInitiated'] =  submissionData['assessmentInitiated'] ?  submissionData['assessmentInitiated'] : moment(new Date()).format('YYYY-MM-DD hh:mm a');
        if (this.caseHeadDetail.length) {
            submissionData['caseheadname'] = submissionData['caseheadname'] ? submissionData['caseheadname'] : this.getMfraFullName(this.caseHeadDetail[0]);
        }
       /* if (submissionData['familygrid']) {
            submissionData['familygrid'] = [];
        } */
        if (submissionData['familygrid'] && submissionData['familygrid'][0].childrenname === '') {
            submissionData['familygrid'] = [];
            if (this.houseHoldDetail.length) {
                this.houseHoldDetail.forEach(house => {
                    submissionData['familygrid'].push({
                        childrenname: this.getFullName(house),
                        childrendob: this.getFormattedDate(house.dob),
                        childrenrelationship: titleCase(house.relationship)
                        // primarycaregiver: ''
                    });
                });
            }
        } else {
            if (submissionData['familygrid'] && submissionData['familygrid'].length) {
                const familyList = [];
                submissionData['familygrid'].forEach(house => {
                familyList.push (house);
                });
                submissionData['familygrid'] = familyList;
            }
        }
        if (this.caseWorkerDetail) {
            submissionData['caseworkername'] =  submissionData['caseworkername'] ?  submissionData['caseworkername'] : this.caseWorkerDetail.tousername;
        }
        submissionData['servicecaseID'] =  submissionData['servicecaseID'] ?  submissionData['servicecaseID'] : daNumber;
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        if (this.supervisorDetail) {
            submissionData['supervisorname'] = submissionData['supervisorname'] ? submissionData['supervisorname'] : this.supervisorDetail.fromusername;
           // submissionData['supervisortitle'] = this.supervisorDetail.fromrole;
            // submissionData['safetyassessmentapprovaldate'] = this.supervisorDetail.routedon ? this.supervisorDetail.routedon : '';
        }
        return submissionData;
    }
    get placementDetails() {
        const currentStore = this._dataStoreService.getCurrentStore();
        let placementInfo = [];
        if (currentStore) {
        placementInfo = currentStore['CASEWORKER_PLACEMENT_INFO'];
        // this.placement = this.placement.filter(item => !item.placeenddatetime && item.role === 'RC');
        }
        return placementInfo;
    }
    public fillSafeCOHP(submissionData, daNumber, placements: any[], removedChidren: any[]) {
        console.log('ohp' , placements);
        // if (placements.length) {
            //  submissionData['currentplacement'] = true;
            // submissionData['placementlivingarrangement'] = placement[0].providerinfo.providername;
            // submissionData['addressline1'] = placement[0].providerinfo.addressline1;
            // submissionData['addressline2'] = placement[0].providerinfo.addressline2;
            // submissionData['zipcode'] = placement[0].providerinfo.zipcode;
            // submissionData['ext'] = placement[0].providerinfo.ext;
            // if (placement[0].providerinfo && placement[0].providerinfo.phonenumber) {
            //     const fax = placement[0].providerinfo.phonenumber.filter(faxItem => faxItem.providercontactinfotypekey === 'F');
            //     if (fax.length) {
            //         submissionData['fax'] = fax[0].phonenumber;
            //     }
            //     const work = placement[0].providerinfo.phonenumber.filter(faxItem => faxItem.providercontactinfotypekey === 'P');
            //     if (work.length) {
            //         submissionData['work'] = work[0].phonenumber;
            //     }
            // }
        // }
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        if (this.supervisorDetail && submissionData['supervisorname'] === '') {
            submissionData['supervisorname'] = this.supervisorDetail.fromusername;
        }
        submissionData['userrole'] = this.token.role.name;
        submissionData['dateassessmentinitiated'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
        submissionData['approveddate'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
        submissionData['caseid'] = daNumber;
        if (this.caseHeadDetail.length) {
            submissionData['casehead'] = this.getFullName(this.caseHeadDetail[0]);
        }

        if (this.reportedChildDetails.length) {
        const childList = [];
        const childListWithComma = [];
        const childsNames = new Array();
        const childsNamesWithComma = new Array();
        this.reportedChildDetails.forEach(child => {
            if (child.firstname && child.lastname) {
                childsNames.push(child.firstname + ' ' + child.lastname);
                childsNamesWithComma.push(this.getFullName(child));
            }
        });
        submissionData['childs_names_json'] = JSON.stringify(childsNames);
        submissionData['ClientName'] = childsNames[0];
        if (submissionData['dob'] === '') {
            submissionData['dob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
        }

        this.reportedChildDetails.forEach(child => {
            if (child.firstname && child.lastname) {
                const providerdettails = this.getProviderInfo(placements, child);
                childList.push({
                    childname: child.firstname + ' ' + child.lastname,
                    clientid: child.cjamspid,
                    age: this.getAge(child.dob),
                    dob: this.getFormattedDate(child.dob),
                    providerInfo: (providerdettails && providerdettails.providerInfo) ? providerdettails.providerInfo : null,
                    hasActivePlacement: (providerdettails && providerdettails.hasActivePlacement) ? providerdettails.hasActivePlacement : null,
                    placementDetails: {}
                });
                childListWithComma.push({seconename: child.firstname + ' ' + child.lastname , seconeage: this.getAge(child.dob) });

            }
        });
        childList.forEach((child, $index) => {
            this.placementDetails.forEach(place => {
                if (child.clientid === place.cjamspid) {
                    childList[$index].placementDetails = ( place.placements && place.placements.length ) ? place.placements[0] : {};
                }
            });
        });
        if (childList && childList.length) {
            const providerInfo  = childList[0].providerInfo;
            if (providerInfo) {
                submissionData['currentplacement'] = true;
                submissionData['placementlivingarrangement'] = providerInfo.providername;
                submissionData['addressline1'] = providerInfo.adr_street_nm;
                submissionData['addressline2'] = providerInfo.adr_street_tx;
                submissionData['zipcode'] = providerInfo.adr_zip5_no;
                submissionData['ext'] = '';
                submissionData['fax'] = '';
                submissionData['work'] = providerInfo.phonenumber;
            }
        }

        submissionData['clientid'] = this.reportedChildDetails[0].cjamspid;
        console.log('placment child list', childList);
        submissionData['childs_info_json'] = JSON.stringify(childList);
        submissionData['child_info_with_comma_json'] = JSON.stringify(childListWithComma);
        }
        // if (this.reportedChildDetails.length) {
        //     submissionData['clientname'] = this.getFullName(this.reportedChildDetails[0]);
        //     submissionData['dob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
        // }
        if (this.supervisorDetail) {
            submissionData['supervisor'] = this.supervisorDetail.fromusername;
        }
        if (this.caseWorkerDetail) {
            submissionData['assessor'] = this.caseWorkerDetail.tousername;
        }
        return submissionData;
    }
    public fillHomeHealthReport(submissionData, daNumber) {
        submissionData['userrole'] = this.token.role.name;
        submissionData['casenumber'] = daNumber;
        if (this.caseHeadDetail.length) {
            submissionData['nameofcaretaker'] = this.getFullName(this.caseHeadDetail[0]);
        }
        if (this.careGiverDetails.length) {
            submissionData['nameofcaretaker'] = this.getFullName(this.careGiverDetails[0]);
            submissionData['caregiverdob'] = this.getFormattedDate(this.careGiverDetails[0].dob);
            if (submissionData['age'] === '') {
                submissionData['age'] = this.getAge(this.careGiverDetails[0].dob);
            }
            if (this.careGiverDetails[0].address && this.careGiverDetails[0].address.length) {
                submissionData['address'] = this.careGiverDetails[0].address;
                submissionData['city'] = this.careGiverDetails[0].city;
                submissionData['state'] = this.careGiverDetails[0].state;
                submissionData['zip'] = this.careGiverDetails[0].zipcode;
            }
        }
        if (this.caseWorkerDetail) {
            submissionData['workername1'] = this.caseWorkerDetail.tousername;
        }
        if (this.supervisorDetail) {
            submissionData['supervisorname'] = this.supervisorDetail.fromusername;
        }

        if (this.getChildListWithHouseHold.length) {
            const data = submissionData['sleepingarrangementsgrid'];
            if (!data) {
                submissionData['sleepingarrangementsgrid'] = [];
            }
            const children = [];
            this.getChildListWithHouseHold.forEach(child => {
                children.push({
                    childerenname: child.firstname + ' ' + child.lastname,
                    DOB: child.dob
                });
                if (!data) {
                    submissionData['sleepingarrangementsgrid'].push({
                        childnamelist: child.firstname + ' ' + child.lastname,
                        sharingbedwith : '',
                        sleepinglocation: ''
                    });
                }

            });
            submissionData['childrendetails'] = children;
            submissionData['childs_names_json'] = JSON.stringify(children.map(child => child.childerenname));
            submissionData['childrenhiddendetails'] = JSON.stringify(children);
        }
        if (this.getHouseHoldWithoutChild.length) {
            submissionData['adults_names_json'] = JSON.stringify(this.getHouseHoldWithoutChild.map(person => person.firstname + ' ' + person.lastname));
        }
        if (this.careGiverDetailsWithHouseHold.length) {
            const otherHouseHold = [];
            this.careGiverDetailsWithHouseHold.forEach(child => {
                otherHouseHold.push({
                    householdmembername: child.firstname + ' ' + child.lastname,
                    householdage: this.getAge(child.dob),
                    householdmemberrelationship: titleCase(child.relationship)
                });
            });
            submissionData['householdmemberdetail'] = otherHouseHold;
        }
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        return submissionData;
    }
    public fillTransportationPlan(submissionData, daNumber, childDetail) {
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedChildDetails.length) {
            submissionData['studentname'] = this.getFullName(this.reportedChildDetails[0]);
            submissionData['studentdob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            // if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
            //     submissionData['currentgrade'] = this.reportedChildDetails[0].school[0].typedescription;
            //      submissionData['localdepartmentofsocialservices'] = this.reportedChildDetails[0].school[0].educationname;
            // }
        }
        if (childDetail.length && childDetail[0].schoolname && childDetail[0].schoolname.length && childDetail[0].schoolname[0]) {
            submissionData['localdepartmentofsocialservices'] = childDetail[0].schoolname[0].educationname;
            submissionData['currentgrade'] = childDetail[0].schoolname[0].typedescription;
        }
        if (this.caseWorkerDetail && submissionData['caseworkername'] === '') {
            submissionData['caseworkername'] = this.caseWorkerDetail.tousername;
        }
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        if (this.supervisorDetail && submissionData['supervisorname'] === '') {
            submissionData['supervisorname'] = this.supervisorDetail.fromusername;
        }
        return submissionData;
    }
    public fillBestInterestDetermination(submissionData, daNumber, currentUser, educationDetails) {
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedChildDetails.length) {
            submissionData['date'] = moment(new Date()).format('YYYY-MM-DD');
           // submissionData['studentname'] = this.getFullName(this.reportedChildDetails[0]);
            const childNames = new Array();
            const studentList = [];
            const childListWithComma = [];
            this.persondetail = this.reportedChildDetails;
            this.educationDetails = educationDetails;
            if (this.educationDetails && this.educationDetails.length > 0) {
                for (let i = 0; i < this.persondetail.length; i++) {
                    for (let j = 0; j < this.educationDetails.length; j++) {
                        if (this.persondetail[i].personid === this.educationDetails[j].Pid) {
                            this.persondetail[i].education = this.educationDetails[j].personEducation;
                        }
                       // this.persondetail[i].education = this.educationDetails.filter ( item => item.Pid === this.persondetail[i].personid);
                    }
                }
            }
            this.persondetail.forEach(child => {
                if (child.firstname && child.lastname) {
                    childNames.push(child.firstname + ' ' + child.lastname);
                    studentList.push({
                       studentname: child.firstname + ' ' + child.lastname,
                       dob: this.getFormattedDate(child.dob),
                       personid: child.personid,
                       education: child.education
                    });
                    childListWithComma.push({seconename: child.firstname + ' ' + child.lastname , seconeage: this.getAge(child.dob) });
                }
            });
            submissionData['studentList'] = JSON.stringify(childNames);
            submissionData['studentname'] = childNames[0];
            submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
            if (this.supervisorDetail && submissionData['supervisorname'] === '') {
                submissionData['supervisorname'] = this.supervisorDetail.fromusername;
            }
            if (studentList[0].education && studentList[0].education.length > 0) {
                for (let i = 0; i < studentList[0].education.length; i++ ) {
                    if (studentList[0].education[i].schooltype === 'current') {
                        submissionData['currentschool'] = studentList[0].education[i].educationname;
                        submissionData['grade'] = studentList[0].education[i].currentgradeleveldesc;
                        submissionData['assignedstudent'] = studentList[0].education[i].sasidno;
                    } else {
                        submissionData['previousschool'] = studentList[0].education[i].educationname;
                    }
                }
            }
            submissionData['child_info_with_comma_json'] = JSON.stringify(childListWithComma);
            if (submissionData['studentdob'] === '') {
                submissionData['studentdob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            }
            // submissionData['studentdob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
                submissionData['grade'] = this.reportedChildDetails[0].school[0].typedescription;
                submissionData['currentschool'] = this.reportedChildDetails[0].school[0].educationname;
                submissionData['displaypreviousSchool'] = this.reportedChildDetails[0].school[0].educationname;
                if (this.reportedChildDetails[0].school.length > 1) {
                    submissionData['currentschool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                    submissionData['displaypreviousSchool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                    submissionData['previousschool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 2].educationname;
                }
            }
            // submissionData['saseworkername'] = currentUser;
            if (this.caseWorkerDetail) {
                submissionData['caseworkername'] = this.caseWorkerDetail.tousername;
            }
            submissionData['childs_info_json'] = JSON.stringify(studentList);
        }
        if (this.participantDetail.length) {
            submissionData['bestInterestsDeterminationMeetingParticipants'] = [];
            this.participantDetail.map(person => {
                submissionData['bestInterestsDeterminationMeetingParticipants'].push({
                    fname: person.firstname,
                    lname2: person.lastname,
                    // relationshiptostudent: person.relationshipdescription.replace(/\w\S*/g, txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase()),
                    // otherrelation: person,
                    phonenumber: person.phonenumber,
                    emailid: person.email
                });
            });
        }
        return submissionData;
    }
    public fillNotificationOfPlacement(submissionData, removalChildList) { // , daNumber, currentUser
        console.log(removalChildList);
        submissionData['userrole'] = this.token.role.name;
        // const removalChildData = Object.assign({}, removalChildList);
        if (removalChildList && removalChildList.length) {
            submissionData['childsName'] = [];
            const childlist = [];
            const childjson = [];
            removalChildList.map(item => {
                childlist.push(item.firstname + ' ' + item.lastname);
                childjson.push({
                    childname: item.firstname + ' ' + item.lastname,
                    cjamspid: item.cjamspid,
                    dob: item.dob,
                    gender: item.gender,
                    removalDate: (item.removalInfo) ? item.removalInfo.removaldate : null
                });
            });
            submissionData['childsName'] = childlist;
            submissionData['child_info_json'] = JSON.stringify(childjson);
            submissionData['dob'] = removalChildList[removalChildList.length - 1].dob;
            // submissionData['childgender'] = removalChildList[0].gender;
            submissionData['childClientId'] = removalChildList[removalChildList.length - 1].cjamspid;
            if (removalChildList[removalChildList.length - 1].gender === 'Male' || removalChildList[removalChildList.length - 1].gender === 'M') {
                submissionData['childgender'] = 'male';
            }
            if (removalChildList[removalChildList.length - 1].gender === 'Female' || removalChildList[removalChildList.length - 1].gender === 'F') {
                submissionData['childgender'] = 'female';
            }
            const minDate = (removalChildList[removalChildList.length - 1].removalInfo) ?
                            moment(removalChildList[removalChildList.length - 1].removalInfo.removaldate).format('MM/DD/YYYY') : this._dataStoreService.getData('dsdsActionsSummary').case_opendate;
            const startDate = (<any>$(`[name="data[placemententrydatetime]"]`)[0])._flatpickr;
            startDate.set('minDate', new Date(minDate));
            const exitDate = (<any>$(`[name="data[placementexitdatetime]"]`)[0])._flatpickr;
            exitDate.set('minDate', new Date(minDate));
        }
       // console.log(removalChildList);

        /* if (this.reportedChildDetails.length) {
            submissionData['date'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
            submissionData['studentsname'] = this.getFullName(this.reportedChildDetails[0]);
            submissionData['dob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
                submissionData['grade'] = this.reportedChildDetails[0].school[0].typedescription;
                submissionData['previousschool'] = this.reportedChildDetails[0].school[0].educationname;
                if (this.reportedChildDetails[0].school.length > 1) {
                    submissionData['currentschool'] = this.reportedChildDetails [0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                }
            }
            submissionData['saseworkername'] = currentUser;
        } */
        submissionData['supervisorname'] = this.supervisorDetail.fromusername;
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        return submissionData;
    }
    public fillCansOutOfHomePlacement(submissionData, daNumber, currentUser, placement, assessment) {
        if (assessment.mode === 'start') {
            submissionData['liftdomainfunction'] = assessmentData.liftdomainfunction;
            submissionData['liftdomainfunction2'] = assessmentData.liftdomainfunction2;
            submissionData['childandenvironmentstrength'] = assessmentData.childandenvironmentstrength;
            submissionData['childandenvironmentstrength2'] = assessmentData.childandenvironmentstrength2;
            submissionData['childemotinalneeds'] = assessmentData.childemotinalneeds;
            submissionData['childriskbehaviour'] = assessmentData.childriskbehaviour;
            submissionData['Surveyculturalfactors'] = assessmentData.Surveyculturalfactors;
            submissionData['surveytrauma'] = assessmentData.surveytrauma;
            submissionData['surveystress'] = assessmentData.surveystress;
        } else {
            submissionData['liftdomainfunction'] = submissionData.liftdomainfunction;
            submissionData['liftdomainfunction2'] = submissionData.liftdomainfunction2;
            submissionData['childandenvironmentstrength'] = submissionData.childandenvironmentstrength;
            submissionData['childandenvironmentstrength2'] = submissionData.childandenvironmentstrength2;
            submissionData['childemotinalneeds'] = submissionData.childemotinalneeds;
            submissionData['childriskbehaviour'] = submissionData.childriskbehaviour;
            submissionData['Surveyculturalfactors'] = submissionData.Surveyculturalfactors;
            submissionData['surveytrauma'] = submissionData.surveytrauma;
            submissionData['surveystress'] = submissionData.surveystress;
        }
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedChildDetails.length) {
            submissionData['date'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
            submissionData['assessmentdate'] = moment(new Date()).format('YYYY-MM-DD');
            // submissionData['childname'] = this.getFullName(this.reportedChildDetails[0]);
            const childList = [];
            const childListWithComma = [];
            const childsNames = new Array();
            const childsNamesWithComma = new Array();
            this.reportedChildDetails.forEach(child => {
                if (child.firstname && child.lastname) {
                    childsNames.push(child.firstname + ' ' + child.lastname);
                    childsNamesWithComma.push(this.getFullName(child));
                }
            });
            submissionData['childs_names_json'] = JSON.stringify(childsNames);
            submissionData['childname'] = childsNames[0];

            this.reportedChildDetails.forEach(child => {
                if (child.firstname && child.lastname) {
                    childList.push({
                        childname: child.firstname + ' ' + child.lastname,
                        clientid: child.cjamspid,
                        age: this.getAge(child.dob)
                    });
                    childListWithComma.push({seconename: child.firstname + ' ' + child.lastname , seconeage: this.getAge(child.dob) });

                }
            });
            submissionData['childs_info_json'] = JSON.stringify(childList);
            submissionData['child_info_with_comma_json'] = JSON.stringify(childListWithComma);
            // submissionData['assignedstudent'] = this.reportedChildDetails.Pid;
            submissionData['dob'] = this.getFormattedDate(this.reportedChildDetails[0].dob);
            const age = this.getAge(this.reportedChildDetails[0].dob);
            submissionData['validate14to21'] = (age >= 14 && age <= 21) ? true : false;
            if (submissionData['age'] === '') {
                submissionData['age'] = this.getAge(this.reportedChildDetails[0].dob);
            }
            if (this.reportedChildDetails[0].school && this.reportedChildDetails[0].school.length) {
                submissionData['grade'] = this.reportedChildDetails[0].school[0].typedescription;
                submissionData['previousschool'] = this.reportedChildDetails[0].school[0].educationname;
                if (this.reportedChildDetails[0].school.length > 1) {
                    submissionData['currentschool'] = this.reportedChildDetails[0].school[this.reportedChildDetails[0].school.length - 1].educationname;
                }
            }
            submissionData['saseworkername'] = currentUser;
        }
        if (this.caseHeadDetail.length) {
            submissionData['caseheadsname'] = this.getFullName(this.caseHeadDetail[0]);
            submissionData['relationship'] = titleCase(this.caseHeadDetail[0].relationship);
            submissionData['source'] = this.getAge(this.caseHeadDetail[0].dob);
        }
        if (this.caseWorkerDetail) {
            submissionData['columns8Columns2CaseWorkerNameField2'] = this.caseWorkerDetail.fromusername;
            submissionData['workertitle'] = this.caseWorkerDetail.fromrole;
            submissionData['score2'] = this.caseWorkerDetail.tousername;
            submissionData['columns8Columns2CaseWorkerNameField'] = this.caseWorkerDetail.tousername;
        }
        if (this.careGiverDetails.length) {
            submissionData['panel5079156779560240Columns44CaregiverFirstName'] = titleCase(this.careGiverDetails[0].firstname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName2'] = titleCase(this.careGiverDetails[0].lastname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName3'] = titleCase(this.careGiverDetails[0].relationship);
            submissionData['panel5079156779560240Columns44CaregiverFirstName4'] = titleCase(this.careGiverDetails[0].firstname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName5'] = titleCase(this.careGiverDetails[0].lastname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName6'] = titleCase(this.careGiverDetails[0].relationship);
            submissionData['panel5079156779560240Columns44CaregiverFirstName7'] = titleCase(this.careGiverDetails[0].firstname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName8'] = titleCase(this.careGiverDetails[0].lastname);
            submissionData['panel5079156779560240Columns44CaregiverFirstName9'] = titleCase(this.careGiverDetails[0].relationship);
            submissionData['nameofcaretaker'] = this.getFullName(this.careGiverDetails[0]);
            submissionData['casenumber'] = daNumber;
            if (this.careGiverDetails[0].address && this.careGiverDetails[0].address.length) {
                submissionData['address'] = this.careGiverDetails[0].address;
                submissionData['city'] = this.careGiverDetails[0].city;
                submissionData['state'] = this.careGiverDetails[0].state;
                submissionData['zip'] = this.careGiverDetails[0].zipcode;
            }
        }
        if (placement.length) {
            submissionData['panel5079156779560240Columns44CaregiverFirstName'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName2'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName3'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName4'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName5'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName6'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName7'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName8'] = 'NA';
            submissionData['panel5079156779560240Columns44CaregiverFirstName9'] = 'NA';
        }

        if (this.childDetails.length) {
            submissionData['familygrid'] = [];
            this.childDetails.forEach(child => {
                submissionData['familygrid'].push({
                    childrenname: this.getFullName(child),
                    childrendob: this.getFormattedDate(child.dob),
                    childrenage: this.getAge(child.dob),
                    childrenrelationship: titleCase(child.relationship),
                    primarycaregiver: ''
                });
            });
        }
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        return submissionData;
    }
    public fillCinaShelterPetition(submissionData, childRemovalInfo, daNumber, workerName) {
        if (childRemovalInfo) {
            if (childRemovalInfo.fatherdetails) {
                submissionData['fathername'] = childRemovalInfo.fatherdetails[0].firstname + ' ' + childRemovalInfo.fatherdetails[0].lastname;
            }
            if (childRemovalInfo.motherdetails) {
                submissionData['mothername'] = childRemovalInfo.motherdetails[0].firstname + ' ' + childRemovalInfo.motherdetails[0].lastname;
            }
            if (childRemovalInfo.fatherdetails && childRemovalInfo.fatherdetails.length && childRemovalInfo.fatherdetails[0].address && childRemovalInfo.fatherdetails[0].address.length) {
                submissionData['fatheraddress1'] = childRemovalInfo.fatherdetails[0].address[0].address;
                submissionData['fatheraddress2'] = childRemovalInfo.fatherdetails[0].address[0].address2;
                submissionData['fathercity'] = childRemovalInfo.fatherdetails[0].address[0].city;
                submissionData['fatherstate'] = childRemovalInfo.fatherdetails[0].address[0].state;
                submissionData['fatherzip'] = childRemovalInfo.fatherdetails[0].address[0].zipcode;
            }

            if (childRemovalInfo.motherdetails && childRemovalInfo.motherdetails.length && childRemovalInfo.motherdetails[0].address && childRemovalInfo.motherdetails[0].address.length) {
                submissionData['motheraddress1'] = childRemovalInfo.motherdetails[0].address[0].address;
                submissionData['motheraddress2'] = childRemovalInfo.motherdetails[0].address[0].address2;
                submissionData['mothercity'] = childRemovalInfo.motherdetails[0].address[0].city;
                submissionData['motherstate'] = childRemovalInfo.motherdetails[0].address[0].state;
                submissionData['motherzip'] = childRemovalInfo.motherdetails[0].address[0].zipcode;
            }
            submissionData['physicallyremovedfullname'] = childRemovalInfo.removerName;
        }
        if (childRemovalInfo.childdetails) {
            submissionData['childsname'] = this.getFullName(childRemovalInfo.childdetails[0]);
            submissionData['childsdob'] = this.getFormattedDate(childRemovalInfo.childdetails[0].dob);
        }
        if (childRemovalInfo.childdetails && childRemovalInfo.childdetails[0].address && childRemovalInfo.childdetails[0].address.length) {
            (submissionData['address1'] = childRemovalInfo.childdetails[0].address[0].address),
                (submissionData['address2'] = childRemovalInfo.childdetails[0].address[0].address2),
                (submissionData['city'] = childRemovalInfo.childdetails[0].address[0].city);
            submissionData['address3'] = childRemovalInfo.childdetails[0].address[0].state;
            submissionData['zip'] = childRemovalInfo.childdetails[0].address[0].zipcode;
        }
        submissionData['caseworkername'] = this.caseWorkerDetail.tousername;
        submissionData['supervisorname'] = this.supervisorDetail.fromusername;
        submissionData['userrole'] = this.token.role.name;
        submissionData['clientnumber'] = daNumber;
        submissionData['workerdate'] = moment(new Date()).format('YYYY-MM-DD');
        // submissionData['supervisordate'] = moment(new Date()).format('YYYY-MM-DD');

        if (this.reportedAdultDetails.length) {
            submissionData['clientname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['dateofreferral'] = this.getFormattedDate(this.intakeWorkerDetails.routedon);
        }

        if (this.intakeWorkerDetails) {
            submissionData['supervisorname'] = this.intakeWorkerDetails.tousername;
            submissionData['officername'] = this.intakeWorkerDetails.fromusername;
        }
        if (workerName) {
            submissionData['workername'] = workerName.da_assignedto.toUpperCase();
            submissionData['reportnumber'] = workerName.intakenumber;
        }

        return submissionData;
    }

    public fillProjectHomeApplication(submissionData) {
        submissionData['userrole'] = this.token.role.name;
        if (this.reportedAdultDetails) {
            submissionData['phonenumber'] = this.reportedAdultDetails[0].phonenumber;
            submissionData['applicantsname'] = this.getFullName(this.reportedAdultDetails[0]);
            submissionData['dob'] = this.getFormattedDate(this.reportedAdultDetails[0].dob);
            submissionData['ethnicity'] = this.reportedAdultDetails[0].ethnicgrouptypekey ? this.reportedAdultDetails[0].ethnicgrouptypekey : '';
            submissionData['secondaryphone'] = this.reportedAdultDetails[0].secondaryphone ? this.reportedAdultDetails[0].secondaryphone : '';
            submissionData['currentaddress'] = this.reportedAdultDetails[0].currentaddress ? this.reportedAdultDetails[0].currentaddress : '';
            submissionData['address'] = this.getAddress(this.reportedAdultDetails[0]);
            if (this.reportedAdultDetails[0].gender === 'M') {
                submissionData['gender'] = 'male';
            }
            if (this.reportedAdultDetails[0].gender === 'F') {
                submissionData['gender'] = 'female';
            }
            if (this.reportedAdultDetails[0].medicalinformation) {
                this.reportedAdultDetails[0].medicalinformation.map(item => {
                    submissionData['privatehealthinsuranceid'] = item.policyname;
                    if (item.ismedicaidmedicare) {
                        submissionData['privatehealthinsurance'] = 'Yes';
                        submissionData['medicare'] = 'Yes';
                    }
                    if (item.isprimaryphycisian) {
                        submissionData['primarycareprovider'] = item.name;
                        submissionData['primarycareproviderphone'] = item.phone;
                        submissionData['primarycareprovideraddress'] = this.getMedicationAddress(item);
                    }
                    if (!item.isprimaryphycisian) {
                        submissionData['medicalspecialists'] = item.name;
                        submissionData['medicalspecialistsphone'] = item.phone;
                        submissionData['medicalspecialistsaddress'] = this.getMedicationAddress(item);
                    }
                });
            }

            if (this.reportedAdultDetails[0].behavioralhealth) {
                const behaviour = this.reportedAdultDetails[0].behavioralhealth[0];
                submissionData['currentpsychiatricdiagnoses'] = behaviour.currentdiagnoses;
                submissionData['psychiatricprovider'] = behaviour.clinicianname;
                submissionData['psychiatricproviderphone'] = behaviour.behavioralphone;
                submissionData['psychiatricprovideraddress'] = this.getBehaviourAddress(behaviour);
            }

            if (this.reportedAdultDetails[0].medicalcondition) {
                let medicalCondition = '';
                this.reportedAdultDetails[0].medicalcondition.forEach(item => {
                    medicalCondition += item.description + ',';
                });
                submissionData['currentmedicaldiagnoses'] = medicalCondition;
            }
            if (this.reportedAdultDetails[0].medicationinformation) {
                submissionData['medicationinformation'] = [];
                this.reportedAdultDetails[0].medicationinformation.forEach(item => {
                    submissionData['medicationinformation'].push({
                        medication: item.medicationname,
                        dosage: item.dosage,
                        frequency: item.frequency,
                        purpose: item.compliant
                    });
                });
            }
            if (this.reportedAdultDetails[0].addendum) {
                submissionData['chemicaldependencyInformation'] = [];
                if (this.reportedAdultDetails[0].addendum[0].isusedrug) {
                    submissionData['chemicaldependencyInformation'].push({
                        substance: '5',
                        lastdateused: '',
                        howused: '',
                        frequency: this.reportedAdultDetails[0].addendum[0].drugfrequencydetails
                    });
                }
                if (this.reportedAdultDetails[0].addendum[0].isusealcohol) {
                    submissionData['chemicaldependencyInformation'].push({
                        substance: '1',
                        lastdateused: '',
                        howused: '',
                        frequency: this.reportedAdultDetails[0].addendum[0].drugfrequencydetails
                    });
                }
                if (this.reportedAdultDetails[0].addendum[0].isusetobacco) {
                    submissionData['chemicaldependencyInformation'].push({
                        substance: '16',
                        lastdateused: '',
                        howused: '',
                        frequency: this.reportedAdultDetails[0].addendum[0].drugfrequencydetails
                    });
                }
            }
        }
        if (this.intakeWorkerDetails) {
            submissionData['referralsourcename'] = this.intakeWorkerDetails.fromusername;
            submissionData['agency'] = 'Adult Service';
            submissionData['reasonforrefferal'] = this.intakeWorkerDetails.purpose;
            submissionData['agencyphone'] = this.intakeWorkerDetails.phonenumber;
            submissionData['refferaladdress'] = this.intakeWorkerDetails.address;
        }
        return submissionData;
    }

    public fillResidentAgreement(submissionData) {
        submissionData['responsiblepartydate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['providerdate'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['casemanagerdate'] = moment(new Date()).format('YYYY-MM-DD');
        return submissionData;
    }

    public housingClassificationReAssessment(submissionData) {
        if (this.reportedDjsDetails) {
            submissionData['panel4481182037352658YourName'] = this.getFullName(this.reportedDjsDetails[0]);
        }
        return submissionData;
    }

    public djsHousingClassificationAssessment(submissionData) {
        if (this.reportedDjsDetails) {
            submissionData['panel3551413008391684ColumnsTextField'] = this.reportedDjsDetails[0].lastname;
            submissionData['panel3551413008391684ColumnsTextField2'] = this.reportedDjsDetails[0].firstname;
            // submissionData['panel3551413008391684YouthPid'] = this.reportedDjsDetails[0].personid;
            submissionData['panel3551413008391684YouthDob'] = this.reportedDjsDetails[0].dob;
        }
        return submissionData;
    }

    public youthVulnerabilityAssessmentInstrument(submissionData) {
        if (this.reportedDjsDetails) {
            submissionData['panel6379524013801054ColumnsYouthsName'] = this.getFullName(this.reportedDjsDetails[0]);
            submissionData['panel6379524013801054ColumnsSex'] = this.reportedDjsDetails[0].gender;
            // submissionData['panel6379524013801054ColumnsRace'] = this.reportedDjsDetails[0].ra;
            submissionData['panel6379524013801054ColumnsDob'] = this.reportedDjsDetails[0].dob;
        }
        return submissionData;
    }

    public fillAppla(submissionData) {
        submissionData['userrole'] = this.token.role.name;
        if (this.supervisorDetail) {
            submissionData['supervisorname'] = this.supervisorDetail.fromusername;
        }
        if (this.caseWorkerDetail) {
            submissionData['caseworkername'] = this.caseWorkerDetail.tousername;
        }
        return submissionData;
    }

    public b_02735uniform45dayspredischarge(submissionData, youthInvolvedPersons) {
        submissionData['Date3'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['Youthsname3'] = youthInvolvedPersons[0].youthname;
        submissionData['Date4'] = youthInvolvedPersons[0].youthdob;
        submissionData['ASSISTPID3'] = youthInvolvedPersons[0].assistpid;
        submissionData['ASSISTPID7'] = youthInvolvedPersons[0].cjamspid;
        submissionData['YouthsCounty'] = youthInvolvedPersons[0].youthjurisdiction;
        submissionData['Facility3'] = youthInvolvedPersons[0].activefacility;
        submissionData['Entertheyouthsprojecteddischargedate3'] = youthInvolvedPersons[0].projecteddischargedate;
        submissionData['ReEntrySpecialistforthiscase3'] = youthInvolvedPersons[0].caseworkercounty;
        return submissionData;
    }

    public b_02736postdischargere_enterytransitionplan(submissionData, youthInvolvedPersons) {
        submissionData['panel3799845984923247Date'] = moment(new Date()).format('YYYY-MM-DD');
        submissionData['Youthsname'] = youthInvolvedPersons[0].youthname;
        submissionData['panel3799845984923247Date2'] = youthInvolvedPersons[0].youthdob;
        submissionData['ASSISTPID'] = youthInvolvedPersons[0].assistpid;
        submissionData['ASSISTPID2'] = youthInvolvedPersons[0].cjamspid;
        submissionData['Selectthejurisdictionofcurrentresidence'] = youthInvolvedPersons[0].youthjurisdiction;
        submissionData['Youthdischarged'] = youthInvolvedPersons[0].closedfacility;
        submissionData['panel3799845984923247Date3'] = youthInvolvedPersons[0].closeddischargedate;
        submissionData['SelectTheRegionoftheRe-EntrySpecialistforthisCase'] = youthInvolvedPersons[0].caseworkercounty;
        return submissionData;
    }

    public draiFollowUp(submissionData, youthInvolvedPersons) {
        submissionData['panel8439712137546045Dob'] = youthInvolvedPersons[0].youthdob;
        submissionData['panel8439712137546045YouthName'] = youthInvolvedPersons[0].youthname;
        submissionData['panel8439712137546045ColumnsCjamsid'] = youthInvolvedPersons[0].cjamspid;
        return submissionData;
    }

    public intakedrai(submissionData, youthInvolvedPersons, formdata) {
        switch (formdata.pend_adj) {
            case 1:
                formdata.pend_adj = 8;
                break;
            case 2:
                formdata.pend_adj = 4;
                break;
            case 3:
                formdata.pend_adj = 2;
                break;
            case 4:
                formdata.pend_adj = 0;
                break;
            default:
                formdata.pend_adj = -1;
                break;
        }
        switch (formdata.sus_adj_sup) {
            case 1:
                formdata.sus_adj_sup = 5;
                break;
            case 2:
                formdata.sus_adj_sup = 3;
                break;
            case 3:
                formdata.sus_adj_sup = 2;
                break;
            case 4:
                formdata.sus_adj_sup = 1;
                break;
            case 5:
                formdata.sus_adj_sup = 0;
                break;
            default:
                formdata.sus_adj_sup = -1;
                break;
        }
        switch (formdata.fta) {
            case 1:
                formdata.fta = 5;
                break;
            case 2:
                formdata.fta = 0;
                break;
            default:
                formdata.fta = -1;
                break;
        }
        switch (formdata.awol) {
            case 1:
                formdata.awol = 4;
                break;
            case 2:
                formdata.awol = 0;
                break;
            default:
                formdata.awol = -1;
                break;
        }
        switch (formdata.prior_det) {
            case 1:
                formdata.prior_det = 2;
                break;
            case 2:
                formdata.prior_det = 0;
                break;
            default:
                formdata.prior_det = -1;
                break;
        }
        switch (formdata.age_felony) {
            case 1:
                formdata.age_felony = 4;
                break;
            case 2:
                formdata.age_felony = 0;
                break;
            default:
                formdata.age_felony = -1;
                break;
        }
        submissionData['pc'] = formdata ? formdata.pend_adj : -1;
        submissionData['ps'] = formdata ? formdata.sus_adj_sup : -1;
        submissionData['hf'] = formdata ? formdata.fta : -1;
        submissionData['pd'] = formdata ? formdata.awol : -1;
        submissionData['he'] = formdata ? formdata.prior_det : -1;
        submissionData['aa'] = formdata ? formdata.age_felony : -1;
        submissionData['panel86083281892257Columns2YouthName'] = youthInvolvedPersons ? youthInvolvedPersons.firstname + ' ' + youthInvolvedPersons.lastname : '';
        submissionData['panel86083281892257Columns3Cjamsid'] = youthInvolvedPersons ? youthInvolvedPersons.cjamspid : '';
        submissionData['panel86083281892257Columns3DateofBirth'] = youthInvolvedPersons ? moment(youthInvolvedPersons.dob, 'MM/DD/YYYY').toDate() : '';
        submissionData['panel86083281892257Columns3Race'] = youthInvolvedPersons ? youthInvolvedPersons.racetypekey : '';
        submissionData['Workername'] = this.caseWorkerDetail.fromusername;
        submissionData['DRAICompleteddate'] = new Date();
        submissionData['Dateofdecision'] = new Date();
        if (youthInvolvedPersons && youthInvolvedPersons.race) {
            submissionData['panel86083281892257Columns3Race'] = youthInvolvedPersons.raceDescription;
        }
        if (youthInvolvedPersons && youthInvolvedPersons.racetypekey) {
            this._commonHttpService
                .getArrayList(
                    {
                        method: 'get',
                        nolimit: true,
                        activeflag: 1,
                        order: 'typedescription'
                    },
                    NewUrlConfig.EndPoint.Intake.RaceTypeUrl + '?filter'
                )
                .subscribe(result => {
                    result.map(data => {
                        if (data.racetypekey === youthInvolvedPersons.racetypekey) {
                            submissionData['panel86083281892257Columns3Race'] = data.typedescription;
                        }
                    });
                });
        }
        submissionData['gender'] = youthInvolvedPersons ? youthInvolvedPersons.gender : '';
        return submissionData;
    }

    public shelterPetition(submissionData) {
        const countyList = this._dataStoreService.getData(CASE_STORE_CONSTANTS.COUNTY_LIST);
        const dsdsaction = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DSDS_ACTIONS_SUMMARY);
        const juri = dsdsaction.countyid;
        countyList.map(county => {
            if (county.countyid === juri) {
                submissionData['CountyNameTextBox1'] = county.countyname;
                submissionData['JuvenileCourtCounty'] = county.countyname;
            }
        });

        const childList = [];
        submissionData['userrole'] = this.token.role.name;
        console.log(this.involvedPersons);
        this.getChildList.forEach(child => {
            const race = (child.race) ? child.race : [];
            if (child.firstname && child.lastname) {
                childList.push({
                    ChildName: child.firstname + ' ' + child.lastname,
                    ChildDoB: this.getFormattedDate(child.dob),
                    Sex: child.gender,
                    Race: race.map(item => item.racetypekey),
                    personid: child.personid
                });
            }
        });

        if (this.careGiverDetails) {
            const parentdetails = [];
            const parentList = [];
            for (let i = 0; i < this.careGiverDetails.length; i++) {
                let fullAddress = '';
                const phonenumber = this.careGiverDetails[i].phonenumber;
                const ParentGuardian =  this.careGiverDetails[i].firstname + ' ' + this.careGiverDetails[i].lastname;
                if (this.careGiverDetails[i].address)  {
                    fullAddress = this.careGiverDetails[i].address;
                }
                if (this.careGiverDetails[i].address2)  {
                    fullAddress = fullAddress + ' ' + this.careGiverDetails[i].address2;
                }
                if (this.careGiverDetails[i].city)  {
                    fullAddress = fullAddress + ' ' + this.careGiverDetails[i].city;
                }
                if (this.careGiverDetails[i].state) {
                    fullAddress = fullAddress + ' ' + this.careGiverDetails[i].state;
                }
                if (this.careGiverDetails[i].zipcode) {
                    fullAddress = fullAddress + ' ' + this.careGiverDetails[i].zipcode;
                }
                const Address = fullAddress;
                parentdetails.push({
                    ParentGuardian, Address
                });
                parentList.push({
                    ParentGuardian, Address, phonenumber
                });
            }
            if (this.ifEmptyArray(submissionData.parentdetails)) {
                submissionData['parentdetails'] = parentdetails;
                submissionData['parentlist'] = parentList;
            }
        }

        if (this.ifEmptyArray(submissionData.childrendatagrid)) {
            submissionData['childrendatagrid'] = childList;
        }
        return submissionData;
    }

    ifEmptyArray(list) {
        if (list.length === 0) { return true;
        } else if (list.length === 1) {
            const ele = list[0];
            let res = true;
            for (const key in ele) {
                if (ele.hasOwnProperty(key)) {
                    const val = ele[key];
                    if ((Array.isArray(val) && val.length)) {
                        res = false;
                    } else if (!Array.isArray(val) && val !== '') {
                        res = false;
                    }
                }
            }
            return res;
        } else { return false; }
    }

    public fillDomesticViolence(submissionData) {
        submissionData['userrole'] = this.token.role.name;
        if (this.caseWorkerDetail) {
            submissionData['Practitioner'] =    this.token.role.name === 'field' ? this.token.user.userprofile.fullname : this.caseWorkerDetail.tousername;
            }
            submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        submissionData['addedvictims'] = JSON.stringify(this.involvedPersons.map(victim => victim.firstname + ' ' + victim.lastname));
        return submissionData;
    }

    public fillCansFv2(submissionData, daNumber) {
        submissionData['userrole'] = this.token.role.name;
        submissionData['caseheaddatetime'] = submissionData['caseheaddatetime'] ? submissionData['caseheaddatetime'] : moment(new Date()).format('YYYY-MM-DD hh:mm a');
        submissionData['caseheadid'] = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        const list = this._dataStoreService.getData('CASEWORKER_SERVICE_PLAN_LIST');
        submissionData['servicenamelist'] = (Array.isArray(list)) ? list.map(item => item.serviceplanname) : [];
        let caseheadid = '';
        if (this.caseHeadDetail.length) {
            caseheadid = this.caseHeadDetail[0].personid;
            submissionData['caseheadname'] =  submissionData['caseheadname'] ? submissionData['caseheadname'] : this.caseHeadDetail.map(item => this.getFullName(item));
            submissionData['caregiverrelationship'] =  submissionData['caregiverrelationship'] ? submissionData['caregiverrelationship'] : titleCase(this.caseHeadDetail[0].relationship);
        }
//        if (!(submissionData['familygrid'].length) || submissionData['familygrid'][0].caregiveryouthname === '') {
            submissionData['familygrid'] = [];

            if (this.careGiverDetailsWithHouseHold.length) {
                this.careGiverDetailsWithHouseHold.forEach(child => {
                    child['personfullname'] = this.getFullName(child);
                    submissionData['familygrid'].push({
                        caregiveryouthname: this.getFullName(child),
                        caregiveryouthdob: this.getFormattedDate(child.dob),
                        caregiveryouthage: this.getAge(child.dob),
                        caregiveryouthrelationship: this.getrelationship(child.personid, caseheadid) // titleCase(child.relationship)
                        // caregiveryouthschool: educationname
                    });
                });
                submissionData['caregiverList'] = this.legalGuardianCareTakerlist.map(item => this.getFullName(item));
            }
/*        } else {
            if (submissionData['familygrid'].length) {
                submissionData['familygrid'].forEach(child => {
                    submissionData['familygrid'].push({
                        caregiveryouthname: submissionData['familygrid'].caregiveryouthname,
                        caregiveryouthdob: submissionData['familygrid'].caregiveryouthdob,
                        caregiveryouthage: submissionData['familygrid'].caregiveryouthage,
                        caregiveryouthrelationship: submissionData['familygrid'].caregiveryouthrelationship
                        // caregiveryouthschool: educationname
                    });
                });
            }
        } */
        const childList = [];
//        if (!(submissionData['familygrid1'].length) || submissionData['familygrid1'][0].fg1childname === '') {
            this.getChildList.forEach(child => {
                if (child.firstname && child.lastname) {
                    childList.push({
                        fg1childname: child.firstname + ' ' + child.lastname,
                        fg1childdob: child.dob,
                        fg1childschool: child.schoolname,
                        fg1childrelationship: this.getrelationship(child.personid, caseheadid),
                        fg1childage: this.getAge(child.dob),
                    });
                }
            });
/*        } else {
            submissionData['familygrid1'].forEach(child => {
                if (child.fg1childname) {
                    childList.push({
                        fg1childname: child.fg1childname,
                        fg1childdob: child.fg1childdob,
                        fg1childschool: child.fg1childschool,
                        fg1childrelationship: child.fg1childrelationship,
                        fg1childage: child.fg1childage,
                    });
                }
            });
        } */
        submissionData['familygrid1'] = childList;
        submissionData['childlist'] = childList.map(item => item.fg1childname);
        /* cansf form FAMILY AND HOUSEHOLD COMPOSITION not having the below params so commenting this temp
        if (this.getChildListWithHouseHold.length) {
            this.getChildListWithHouseHold.forEach(child => {
                submissionData['familygrid1'].push({
                    caregiveryouthname: this.getFullName(child),
                    caregiveryouthdob: this.getFormattedDate(child.dob),
                    caregiveryouthage: this.getAge(child.dob),
                    caregiveryouthrelationship: titleCase(child.relationship)
                    // primarycaregiver: ''
                });
            });
        } */
        submissionData['cpscaseid'] = submissionData['cpscaseid'] ? submissionData['cpscaseid'] : daNumber;
        submissionData['routingsupervisors'] = JSON.stringify(this.routingSupervisors);
        if (this.supervisorDetail) {
            submissionData['supervisorname'] =  submissionData['supervisorname'] ? submissionData['supervisorname'] : this.supervisorDetail.fromusername;
            submissionData['supervisortitle'] = submissionData['supervisortitle'] ? submissionData['supervisortitle'] : this.supervisorDetail.fromrole;
        }
        if (this.caseWorkerDetail) {
            submissionData['workersname'] =  submissionData['workersname'] ? submissionData['workersname'] : this.caseWorkerDetail.fromusername;
            submissionData['workertitle'] = submissionData['workertitle'] ? submissionData['workertitle'] : this.caseWorkerDetail.fromrole;
            submissionData['workernameandId'] = submissionData['workernameandId'] ? submissionData['workernameandId'] : this.caseWorkerDetail.tousername;
        }
        submissionData['workerdate'] = submissionData['workerdate'] ? submissionData['workerdate'] : moment(new Date()).format('YYYY-MM-DD hh:mm a');
        // submissionData['supervisordate'] = moment(new Date()).format('YYYY-MM-DD hh:mm a');
        return submissionData;
    }

    getProviderInfo(palcements, child) {
        let providerInfo = null;
        let hasActivePlacement = false;
        if (child && palcements && Array.isArray(palcements)) {
            const childPlacments = palcements.find(item => item.personid === child.personid);
            if (childPlacments && Array.isArray(childPlacments.placements))  {
                const activeChildPlacment = childPlacments.placements.find(item => item.placementtypekey === 'PRPL' && !item.enddate && item.routingstatus === 'Approved' && item.isvoided === 0);
                hasActivePlacement = (activeChildPlacment) ? true : false;
                if (activeChildPlacment && activeChildPlacment.providerdetails) {
                    providerInfo = activeChildPlacment.providerdetails;
                }
            }
        }
        return { providerInfo, hasActivePlacement };
    }

}
