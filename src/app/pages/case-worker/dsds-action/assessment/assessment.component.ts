import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as _ from 'lodash';
import { Subject } from 'rxjs/Rx';

import { AppUser } from '../../../../@core/entities/authDataModel';
import { PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService, GenericService, SessionStorageService, AlertService, CommonDropdownsService } from '../../../../@core/services';
import { AuthService } from '../../../../@core/services/auth.service';
import { HttpService } from '../../../../@core/services/http.service';
import { AppConfig } from '../../../../app.config';
import { Assessments, GetintakAssessment, InvolvedPerson, RoutingInfo } from '../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { Placement } from '../service-plan/_entities/service-plan.model';
import { ChildRoles } from '../child-removal/_entities/childremoval.model';
import { YouthInvolvedPersons } from '../involved-persons/_entities/involvedperson.data.model';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { CASE_STORE_CONSTANTS, CASE_ASSESSMENT_FORMS_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { AssessmentService } from './assessment.service';

declare var Formio: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'assessment',
    templateUrl: './assessment.component.html',
    styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent implements OnInit, AfterViewInit {
    internalAssessments = [];
    id: string;
    daNumber: string;
    assessmmentName: string;
    startAssessment$: Observable<Assessments[]>;
    data$: Observable<Assessments[]>;
    totalRecords$: Observable<number>;
    paginationInfo: PaginationInfo = new PaginationInfo();
    formBuilderUrl: string;
    safeUrl: SafeResourceUrl;
    showAssesment = -1;
    placementInfo: any;
    getAsseesmentHistory: GetintakAssessment[] = [];
    templateComponentData: any;
    templateSubmissionData: any;
    getScoreOnClose: boolean;
    refreshForm: any;
    involvedPersons: InvolvedPerson[];
    youthInvolvedPersons: YouthInvolvedPersons[];
    routingInfo: RoutingInfo[];
    routingSupervisors: any[];
    placement: Placement[];
    relationName: ChildRoles[] = [];
    childDetail: ChildRoles[] = [];
    roleId: AppUser;
    isCansfShow = false;
    dsdsActionsSummary$: Observable<any>;
    showAppla = false;
    notApplicableScore: string;
    agency: string;
    isCW: boolean;
    isDjs: boolean;
    hasMDInitialRisk = false;
    isSubmittedDrai = false;
    assessmentTemplateId: any;
    isServiceCase: string;
    assesType: string;
    hasChildRemovalHappened: any;
    isViewCase = false;
    eductionDetatils: any[];
    personRelations: any;
    drugList: any[];
    serviceplanlist: any;
    isClosed = false;
    drugListAOD: any;
    childList: any;
    CHILD_CATEGORIES = ['CHILD', 'BIOCHILD', 'NVC', 'OTHERCHILD', 'PAC', 'RC', 'AV'];
    removalChildList: any;
    childRemovalInfo: any;
    personList: any[];

    constructor(
        private route: ActivatedRoute,
        private _service: GenericService<Assessments>,
        private _commonService: CommonHttpService,
        public sanitizer: DomSanitizer,
        private _http: HttpService,
        private _dataStoreService: DataStoreService,
        private _router: Router,
        private storage: SessionStorageService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _commonDDService: CommonDropdownsService,
        private _assessmentService: AssessmentService
    ) {
        this.id = this._commonDDService.getStoredCaseUuid();
        this.daNumber = this._commonDDService.getStoredCaseNumber();
    }
    ngOnInit() {
        this.isServiceCase = this.storage.getItem('ISSERVICECASE');
        this.assesType = this._dataStoreService.getData('assesment-type');
        this.roleId = this._authService.getCurrentUser();
        this.agency = this._authService.getAgencyName();
        if (this.agency === 'CW') {
            this.isCW = true;
        } else {
            this.isCW = false;
        }
        this.isDjs = this._authService.isDJS();
       // const storeData = this._dataStoreService.getCurrentStore();
       // storeData['CASEWORKER_SELECTED_ASSESSMENT'] = '';
       // this._dataStoreService.setObject(storeData, true, 'CASEWORKER_ASSESSMENT_LOAD');
        this.getChildRemoval();
        this.getPage(1);
        this.getAssessmentPrefillData();
        this.getActionSummary();
        this.getPlacementInfoList();
        this.getEducationInfo();
        this.internalAssessments = this._assessmentService.getInternalAssessments()

       // this.getPersonsAndChildRemovalInfo();

       const da_status = this.storage.getItem('da_status');
           if (da_status) {
            if (da_status === 'Closed' || da_status === 'Completed') {
                this.isClosed = true;
            } else {
                this.isClosed = false;
            }
        }

       // this.updatePersonListWithChildRemoval();
       // this.getRemovalChildList();
    }
    ngAfterViewInit() {
        const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
        if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
            this.isViewCase = true;
            (<any>$(':button')).prop('disabled', true);
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('i')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
            (<any>$('th a')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
        }
    }

    getIntakeDRAIAssessmentDetails() {
        const involvedPersons = this.involvedPersons;
        if (involvedPersons && involvedPersons.length) {
            const personid = involvedPersons.filter(data => data.rolename === 'Youth');
            this._commonService
                .getArrayList(
                    {
                        method: 'get',
                        where: {
                            personid: personid && personid.length ? personid[0].personid : ''
                        }
                    },
                    'Intakeservicerequests/prepopasmtdrai?filter'
                )
                .subscribe((response) => {
                    this._dataStoreService.setData('DRAI_PREFILL', response);
                });
        }
    }

   filterAssessments(array) {
     const CPSIR = CASE_ASSESSMENT_FORMS_CONSTANTS.CPS_IR;
     const CPSAR = CASE_ASSESSMENT_FORMS_CONSTANTS.CPS_AR;
     const INHOME = CASE_ASSESSMENT_FORMS_CONSTANTS.SC_IN_HOME_PLAN;
     const OUTHOME = CASE_ASSESSMENT_FORMS_CONSTANTS.SC_OUT_OF_HOME;
     const SERVICECASE = CASE_ASSESSMENT_FORMS_CONSTANTS.SERVICE_CASE;
     const storeData = this._dataStoreService.getCurrentStore();
    //  console.log('Assessment Array', array);
    //  console.log('Store', storeData);
     if (storeData.teamtypekey === 'CW' || storeData.ISSERVICECASE) {
         let resArray;
         if (storeData.ISSERVICECASE) {
            resArray = array.filter(function(item) {
                return SERVICECASE.includes(item.description);
              });
            //   console.log('Result Array', resArray);
              return resArray;
         } else if (storeData.dsdsActionsSummary && storeData.dsdsActionsSummary.da_subtype && storeData.dsdsActionsSummary.da_subtype === 'CPS-IR') {
         resArray = array.filter(function(item) {
            return CPSIR.includes(item.description);
          });
        //   console.log('Result Array', resArray);
          return resArray;
        } else if (storeData.dsdsActionsSummary && storeData.dsdsActionsSummary.da_subtype && storeData.dsdsActionsSummary.da_subtype === 'CPS-AR') {
            resArray = array.filter(function(item) {
               return CPSIR.indexOf(item.description) !== -1;
             });
           //   console.log('Result Array', resArray);
             return resArray;
        } else {
            return array;
        }

     } else {
         return array;
     }

   }
    getPage(page: number) {
        let inputRequest: Object;
        this._http.overrideUrl = false;
        this._http.baseUrl = AppConfig.baseUrl;
        if (this.isServiceCase) {
            inputRequest = {
                objecttypekey: 'servicecase',
                objectid: this.id
                // objectid: '1ab13583-6d94-4929-92e0-09723df839f3'
            };
        } else {
            inputRequest = {
                servicerequestid: this.id,
                categoryid: null,
                subcategoryid: null,
                targetid: null,
                assessmentstatus: null
            };
        }
        const source = this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: page,
                    limit: this.paginationInfo.pageSize25,
                    where: inputRequest,
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.ListAssessment + '?filter'
            )
            .map(result => {
                const data = this.filterAssessments(result.data);
                return { data: data, count: data && data.length ? data : 0 };
            })
            .share();
        this.hasMDInitialRisk = false;
        this.startAssessment$ = source.pluck('data');
        console.log(this.startAssessment$);
        this.data$ = source.pluck('data');
        this.data$.subscribe(result => {
            if (result) {
               let index = 0;
               let selectedAssesment: any;

               for ( let i = 0; i < result.length; i++ ) {

                   if (result[i].description && (result[i].description === 'MFIRA'
                       || result[i].description === 'MFIRA')) {
                       if (result[i].intakassessment && result[i].intakassessment.length && result[i].intakassessment.length > 0) {
                           this.hasMDInitialRisk = true;
                       }
                   }
                //    console.log('result[i].description' + JSON.stringify(result[i]));
                //    console.log('result[i].description' + result[i].description);
                  if ( result[i].description === this.assesType ) {
                    selectedAssesment = result[i];
                    index = i;
                    break;
                  }
               }
               if (selectedAssesment) {
                this.showAssessment(index, selectedAssesment.intakassessment);
               }
            }
        });

        this.startAssessment$.subscribe(list => { 
        // Search this list based on business reqirement of the date you need
        const assessmentSafeC = list.find(element => element.description === 'SAFE-C');
        if (assessmentSafeC.intakassessment && assessmentSafeC.intakassessment.length > 0) {
            const approvedAssessment = assessmentSafeC.intakassessment.find(data => data.assessmentstatustypekey === "Accepted");
            if(approvedAssessment && approvedAssessment.submissiondata){
                const assessmentCompDate = approvedAssessment.submissiondata.safetyassessmentcompletiondate;

            // set this datastore variable
            this._dataStoreService.setData('safetyassessmentcompletiondate', assessmentCompDate);
            console.log(this._dataStoreService.getData('safetyassessmentcompletiondate'));
            }
        } 

        });


        if (this._authService.isDJS()) {
            this.startAssessment$.subscribe(list => {
                const intakeDRAI = list.find(element => element.description === 'Intake Detention Risk Assessment Instrument');
                if (intakeDRAI && intakeDRAI.intakassessment && intakeDRAI.intakassessment.length > 0) {
                    const submittedAssessment = intakeDRAI.intakassessment.find(data => data.assessmentstatustypekey === 'Submitted');
                    this.isSubmittedDrai = submittedAssessment ? true : false;
                } else {
                    this.isSubmittedDrai = false;
                }
            });
        }
        if (page === 1) {
            this.totalRecords$ = source.pluck('count');
        }
    }
    private getActionSummary() {
        this.dsdsActionsSummary$ = this._commonService
            .getById(this.daNumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl)
            .map(res => {
                return res[0];
            })
            .share();
        this.dsdsActionsSummary$.subscribe(response => {
            const dobChid = response['persondob'];
            this.isCansfShow = this.getAge(dobChid) >= 5 ? true : false;
            this.showAppla = this.getAge(dobChid) < 16 ? true : false;
        });
    }
    private getAge(dobChid) {
        dobChid = new Date(dobChid);
        const calculateYear = new Date().getFullYear();
        const calculateMonth = new Date().getMonth();
        const calculateDay = new Date().getDate();

        const birthYear = dobChid.getFullYear();
        const birthMonth = dobChid.getMonth();
        const birthDay = dobChid.getDate();

        let age = calculateYear - birthYear;
        const ageMonth = calculateMonth - birthMonth;
        const ageDay = calculateDay - birthDay;

        if (ageMonth < 0 || (ageMonth === 0 && ageDay < 0)) {
            age = age - 1;
        }
        return age;
    }

    startAssessment_v2(assessmentName, mode) {
        // assessment.mode = mode;
        // /*  const storeData = this._dataStoreService.getCurrentStore();
        //  storeData['CASEWORKER_INVOLVED_PERSON'] = this.involvedPersons;
        //  storeData['CASEWORKER_YOUTH_INVOLVED_PERSON'] = this.youthInvolvedPersons;
        //  storeData['CASEWORKER_ROUTING_INFO'] = this.routingInfo;
        //  storeData['CASEWORKER_PLACEMENT'] = this.placement;
        //  storeData['CASEWORKER_SELECTED_ASSESSMENT'] = assessment;
        //  storeData['CASEWORKER_CHILD_DETAIL'] = this.childDetail;
        //  this._dataStoreService.setObject(storeData, true, 'CASEWORKER_ASSESSMENT_LOAD'); */
          this._dataStoreService.setData('CASEWORKER_SERVICE_PLAN_LIST', this.serviceplanlist);
        //  this._dataStoreService.setData('CASEWORKER_INVOLVED_PERSON', this.involvedPersons);
        //  this._dataStoreService.setData('CASEWORKER_PERSON_RELATIONS', this.personRelations);
        //  this._dataStoreService.setData('CASEWORKER_YOUTH_INVOLVED_PERSON', this.youthInvolvedPersons);
        //  this._dataStoreService.setData('CASEWORKER_ROUTING_INFO', this.routingInfo);
        //  this._dataStoreService.setData('CASEWORKER_SUPERVISORS', this.routingSupervisors);
        //  this._dataStoreService.setData('CASEWORKER_PLACEMENT', this.placement);
        //  this._dataStoreService.setData('CASEWORKER_SELECTED_ASSESSMENT', assessment);
        //  this._dataStoreService.setData('CASEWORKER_CHILD_DETAIL', this.childDetail);
        //  this._dataStoreService.setData('CASEWORKER_DRUG_LIST', this.drugList);
        
        this._dataStoreService.setData('SELECTED_ASSESSMENT_NAME', assessmentName);
        this._router.navigate(['view-assessment'], {relativeTo : this.route});
    }

    startAssessment(assessment: GetintakAssessment, mode) {
        if (this._authService.isDJS() && assessment.description === 'DRAI Follow Up') {
            if (!this.isSubmittedDrai) {
                this._alertService.error('Please Submit Intake DRAI');
                return false;
            }
        }
        assessment.mode = mode;
       /*  const storeData = this._dataStoreService.getCurrentStore();
        storeData['CASEWORKER_INVOLVED_PERSON'] = this.involvedPersons;
        storeData['CASEWORKER_YOUTH_INVOLVED_PERSON'] = this.youthInvolvedPersons;
        storeData['CASEWORKER_ROUTING_INFO'] = this.routingInfo;
        storeData['CASEWORKER_PLACEMENT'] = this.placement;
        storeData['CASEWORKER_SELECTED_ASSESSMENT'] = assessment;
        storeData['CASEWORKER_CHILD_DETAIL'] = this.childDetail;
        this._dataStoreService.setObject(storeData, true, 'CASEWORKER_ASSESSMENT_LOAD'); */
        this._dataStoreService.setData('CASEWORKER_SERVICE_PLAN_LIST', this.serviceplanlist);
        this._dataStoreService.setData('CASEWORKER_INVOLVED_PERSON', this.involvedPersons);
        this._dataStoreService.setData('CASEWORKER_PERSON_RELATIONS', this.personRelations);
        this._dataStoreService.setData('CASEWORKER_YOUTH_INVOLVED_PERSON', this.youthInvolvedPersons);
        this._dataStoreService.setData('CASEWORKER_ROUTING_INFO', this.routingInfo);
        this._dataStoreService.setData('CASEWORKER_SUPERVISORS', this.routingSupervisors);
        this._dataStoreService.setData('CASEWORKER_PLACEMENT', this.placement);
        this._dataStoreService.setData('CASEWORKER_SELECTED_ASSESSMENT', assessment);
        this._dataStoreService.setData('CASEWORKER_CHILD_DETAIL', this.childDetail);
        this._dataStoreService.setData('CASEWORKER_DRUG_LIST', this.drugList);
        if (assessment.description === 'cans-v2' || assessment.titleheadertext === 'cans-v2') {
            this._dataStoreService.setData('SELECTED_ASSESSMENT_NAME', 'cans-v2');
            this._router.navigate(['view-assessment'], {relativeTo : this.route});
        } else if (assessment.description === 'CANS-OUT OF HOME PLACEMENT SERVICE' || assessment.titleheadertext === 'CANS-OUT OF HOME PLACEMENT SERVICE') {
            this._dataStoreService.setData('SELECTED_ASSESSMENT_NAME', 'CANS-OUT OF HOME PLACEMENT SERVICE');
            this._router.navigate(['view-assessment'], {relativeTo : this.route});
        }  else if (assessment.description === 'MFIRA' || assessment.titleheadertext === 'MFIRA') {
            this._dataStoreService.setData('SELECTED_ASSESSMENT_NAME', 'MFIRA');
            this._router.navigate(['view-assessment'], {relativeTo : this.route});
        } else if (assessment.description === 'AOD Form' || assessment.titleheadertext === 'AOD Form' || assessment.description === 'AOD/PADS Form' || assessment.titleheadertext === 'AOD/PADS Form') {
            this._dataStoreService.setData('CASEWORKER_DRUG_LIST_AOD', this.drugListAOD);
            this._dataStoreService.setData('SELECTED_ASSESSMENT_NAME', 'AOD Form');
            this._router.navigate(['view-assessment'], {relativeTo : this.route});
        } else {
            this._router.navigate(['case-worker-view-assessment'], {relativeTo : this.route});
        }
    }

    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize25 = pageInfo.itemsPerPage;
        this.getChildRemoval();
        this.getPage(this.paginationInfo.pageNumber);
    }

    showAssessment(id: number, row) {
        /* for (let j = 0; j < row.length; j++) {
             if (row[j].score === -1) {
                 this.notApplicableScore = 'N/A';
             }
         } */
        this.getAsseesmentHistory = row;
        if (this.showAssesment !== id) {
            this.showAssesment = id;
        } else {
            this.showAssesment = -1;
        }
    }
    actionIconDisplay(modal, status: string): boolean {
        if (status === 'View') {
            return modal.assessmentstatustypekey !== 'Open' && modal !== null;
        } else if (status === 'Edit') {
            const cwstatusList = ['Accepted']; // Rejected is removed based on UAT team request on 03072019
            const supStatusList = ['Open', 'Accepted'];
            return (!cwstatusList.includes(modal.assessmentstatustypekey) && modal !== null) || (this.roleId.role.name === 'apcs' && !supStatusList.includes(modal.assessmentstatustypekey));
        } else if (status === 'Print') {
            return modal.assessmentstatustypekey !== 'Open' && modal !== null;
        } else if (status === 'InProcess') {
            return modal.assessmentstatustypekey === 'InProcess' && modal !== null;
        }
        return false;
    }

    getPrintTitle(modal) {
        if (this.isDjs) {
            if (modal.assessmentstatustypekey === 'InProcess' && modal.name === 'intakeDetentionRiskAssessmentInstrument') {
                return 'Print Provisional DRAI';
            } else if (modal.assessmentstatustypekey === 'Submitted' && modal.name === 'intakeDetentionRiskAssessmentInstrument') {
                return 'Print Completed DRAI';
            } else {
                return 'Print';
            }
        } else {
            return 'Print';
        }
    }

    private getAssessmentPrefillData() {
        let involvedpersonreqobj = {};
        let getpersonlistreq = {};
        let routinginfoparam = {};
        if (this.isCW && this.isServiceCase) {
            involvedpersonreqobj = { servicecaseid: this.id };
            routinginfoparam = { servicecaseid: this.id };
            getpersonlistreq = { objectid: this.id, objecttypekey: 'servicecase' };
        } else {
            involvedpersonreqobj = { intakeservreqid: this.id };
            routinginfoparam = { intakeserviceid: this.id };
            getpersonlistreq = { intakeserviceid: this.id };
        }
        const source = forkJoin(
            this._commonService.getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: getpersonlistreq
                }),
                'People/getpersondetailcw?filter'
            ),
            this._commonService.getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 50,
                    method: 'get',
                    where: routinginfoparam
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.RoutingInfoList
            ),
            this._commonService.getPagedArrayList(
                {
                    where: {
                        intakeserviceid: this.id
                    },
                    page: 1,
                    limit: 50,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementListUrl + '?filter'
            ),
            this._commonService.getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: involvedpersonreqobj
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
            ),
            this._commonService.getById(this.id, CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.YouthInvolvedPersonListUrl),
            this._commonService.getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    method: 'get',
                    where: { intakeserviceid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.getpersonrelationbyintakeservice + '?filter'
            ),
            this._commonService.getArrayList(
                new PaginationRequest({
                    where: { appevent: 'ASST' },
                    method: 'post'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.SupervisorList
            ),
            this._commonService.getArrayList(
                {
                    where: {
                        tablename: 'substancetype',
                        teamtypekey: null
                    },
                    method: 'get',
                    nolimit: true
                },
                'referencetype/gettypes' + '?filter'
            ),
            this._commonService.getArrayList(
                {
                    where: {
                        caseid : this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID)
                    },
                    method: 'get',
                },
                'serviceplan/listbyallrelation?filter'
            ),
            this._commonService.getPagedArrayList(
                new PaginationRequest({
                page: 1,
                limit: 20,
                method: 'get',
                where: this.getChildRemovalRequestParam()
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.PersonList + '?filter'
                // CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval.InvolvedPersonList + '?filter'
            ),
            this._commonService.getSingle(
            {
                where: this.getChildRemovalRequestParam(),
                method: 'get'
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                .GetChildRemovalList + '?filter'
            )
        ).subscribe(result => {
            this.involvedPersons = result[0]['data'];
            this.routingInfo = result[1][0] ? result[1][0]['routinginfo'] : [];
            this.routingSupervisors = result[6] ? result[6]['data'] : [];
            this.placement = result[2]['data'];
            this.relationName = [];
            this.relationName = result[3].data;
            this.youthInvolvedPersons = result[4];
            this.drugList = [];
            this.serviceplanlist = result[8];
            this.personList = result[9]['data'];
            this.childRemovalInfo = result[10];
            this.updatePersonListWithChildRemoval();
            this.getRemovalChildList();
            if (result[7] && result[7].length && Array.isArray(result[7])) {
                // Sample Response
                // description: "Baby - Amphetamines"
                // ref_key: "BAS"
                // referencetypeid: "55"
                // value_text: "Baby - Amphetamines"
                this.drugList = result[7].map(drug => drug.value_text);
                this.drugListAOD = result[7];
                console.log('Drug List', this.drugList);
            }
            this.childDetail = [];
            if (this.relationName && this.relationName.length > 0) {
                this.relationName.map(res => {
                    if (res.roles) {
                        const checkRC = res.roles.filter(role => role.intakeservicerequestpersontypekey === 'RC');
                        if (checkRC.length) {
                            this.childDetail.push(res);
                        }
                    }
                });
            }
            // Get relationship from another API and override with existing object.
            const personRelations = result[5];
            this.personRelations = result[5];
            this.involvedPersons.forEach(person => {
                const filteredPerson = _.filter(personRelations, { cjamspid: person.cjamspid });
                person.relationship = _.get(filteredPerson, '0.relation.0.description');
            });

        if (this._authService.isDJS()) {
            this.getIntakeDRAIAssessmentDetails();
        }
        });
    }
    isIconDisabled(modal) {
        if (this.roleId.role.name === 'apcs') {
            if (modal.assessmentstatustypekey === 'InProcess' || modal.assessmentstatustypekey === 'Rejected' || modal.assessmentstatustypekey === 'Accepted') {
                return 'icon-disabled';
            }
        } else {
            return;
        }
    }
    confirmDelete(assessmentid, index) {
        // a.index = index;
        console.log('assessmentid....index', assessmentid, index);
        this.assessmentTemplateId = assessmentid;
        (<any>$('#delete-assessment-popup')).modal('show');
    }

    deleteAssessment() {
        this._commonService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.DeleteAssessment;
        this._commonService.create({
            assessmentid: this.assessmentTemplateId,
            // method: 'post'
        })
            .subscribe(
                response => {
                    if (response) {
                        this._alertService.success(
                            'Assessment deleted successfully'
                        );
                        (<any>$('#delete-assessment-popup')).modal('hide');
                        this.getChildRemoval();
                        // this.getPage(1);
                        this.getAsseesmentHistory = null;
                        this.getPage(1);
                        this.showAssesment = -1;
                    }
                },
                error => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
    }

    getChildRemoval() {
        this._commonService
            .getSingle(
                {
                    where: { intakeserviceid: this.id },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ChildRemoval
                    .GetChildRemovalList + '?filter'
            )
            .subscribe(result => {
                if (result && result.length) {
                    this.hasChildRemovalHappened = result.some(item => item.approvalstatus === 'Approved');
                } else {
                    this.hasChildRemovalHappened = false;
                }
            });
    }


    getPlacementInfoList() {
            return this._commonService
              .getPagedArrayList(
                new PaginationRequest({
                  page: 1,
                  limit: 20,
                  method: 'get',
                  where: { servicecaseid:  this.id  },
                }),
                'placement/getplacementbyservicecase?filter'
              ).subscribe(result => {
                if (result && result.data) {
                    this.placementInfo = result.data;
                    this._dataStoreService.setData('CASEWORKER_PLACEMENT_INFO', this.placementInfo);
                }
            });
    }

    getEducationInfo() {
        this._commonService.getArrayList({
            where: {
                object_id: this.id
            },
            method: 'get',
        }, 'personeducation/educationlistforassessment?filter').subscribe( response => {
            console.log('response...', response);
            this.eductionDetatils = response;
            this._dataStoreService.setData('CASEWORKER_EDUCATION_INFO', this.eductionDetatils);
        });
    }

    getRemovalChildList() {
     // Get Removal child list
     if (this.personList && this.personList.length) {
        this.childList = this.personList.filter(child => {
            let childFound = false;
            if (child.roles && child.roles.length) {
                child.roles.forEach(role => {
                const childCategory = this.CHILD_CATEGORIES.find(category => category === role.intakeservicerequestpersontypekey);
                if (childCategory) {
                    childFound = true;
                    return;
                }
                });
            }
            // return childFound; // removing unsafe child validation for 11/3 demo
            // return childFound && (child.issafe === UNSAFE || this.isServiceCase()); //  D-11926
            return childFound ;
            });

            console.log(this.childList);
        }
        this._dataStoreService.setData('REMOVAL_CHILD_LIST', this.childList);
    }

    getChildRemovalRequestParam() {

        let requestData;
        if (this.isServiceCase) {
          requestData = {
            objectid: this.id,
            objecttypekey: 'servicecase'
          };
    
        } else {
          requestData = { intakeserviceid: this.id };
        }
        return requestData;
    }
    
    updatePersonListWithChildRemoval() {

        if (this.childRemovalInfo && this.childRemovalInfo.length) {
          this.childRemovalInfo.forEach(removalInfo => {
            this.personList.forEach(person => {
              if (person.personid === removalInfo.personid) {
                // person.isRemoved = true;
                // if (person.servicecaseid) {
                //   person.removalStatus = 'Approved';
                // } else {
                //   person.removalStatus = 'Review';
                // }
                person.removalStatus = removalInfo.approvalstatus ? removalInfo.approvalstatus : 'Draft';
                // if record has end date then eligble for new removal
    
                person.intakeservreqchildremovalid = removalInfo.intakeservreqchildremovalid;
                if (removalInfo.exitdate && person.removalStatus === 'Approved') {
                  person.removalStatus = null;
                  person.intakeservreqchildremovalid = null;
                  person.enableNewRemoval = true;
                }
    
                if (!removalInfo.exitdate && person.removalStatus === 'Approved') {
                  person.enableEndRemoval = true;
                } else if (removalInfo.exitdate && person.removalStatus === 'Review') {
                  person.enableEndRemoval = true;
                } else {
                  person.enableEndRemoval = false;
                }
                person.removalInfo = removalInfo;
                // person.placementList = this.placementList ? this.placementList.filter(placement => placement.personid === person.personid) : null;
    
    
                person.removalHistory = JSON.parse(JSON.stringify(this.childRemovalInfo.filter(removalHistory => removalHistory.personid === person.personid)));
                person.hasHistory = person.removalHistory.length ? true : false;
              }
            });
          });
    
        }
      }
}
