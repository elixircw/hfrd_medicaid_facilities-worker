//import { ParticipationsSummary } from './../../../providers/_entities/provider.data.model';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LegalActionHistoryComponent } from './legal-action-history.component';
import { LegalActionHistoryRoutingModule } from './legal-action-history-routing.module';
import { MatRadioModule } from '@angular/material';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker/dist/datetimepicker.module';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    CommonModule,
    LegalActionHistoryRoutingModule,
    MatRadioModule,
    FormMaterialModule,
    A2Edatetimepicker,
    QuillModule
  ],
  declarations: [
    LegalActionHistoryComponent
  ],
  providers: []
})
export class LegalActionHistoryModule { }
