import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule
  } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AsOasRoutingModule } from './as-oas-routing.module';
import { AsOasComponent } from './as-oas.component';

@NgModule({
  imports: [
    CommonModule,
    AsOasRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [AsOasComponent]
})
export class AsOasModule { }
