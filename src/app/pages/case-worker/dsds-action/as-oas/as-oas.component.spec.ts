import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsOasComponent } from './as-oas.component';

describe('AsOasComponent', () => {
  let component: AsOasComponent;
  let fixture: ComponentFixture<AsOasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsOasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsOasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
