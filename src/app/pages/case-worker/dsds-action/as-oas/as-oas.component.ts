import { Component, OnInit } from '@angular/core';
import { CommonHttpService, DataStoreService, GenericService, AlertService } from '../../../../@core/services';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { PaginationRequest, DropdownModel } from '../../../../@core/entities/common.entities';
import { ActivatedRoute } from '@angular/router';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { InvolvedPerson } from '../involved-persons/_entities/involvedperson.data.model';
import { RoutingInfo, DSDSActionSummary, SaveActionLetter , ActionLetter } from '../../_entities/caseworker.data.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Role } from '../../../../@core/common/models/involvedperson.data.model';
import { Observable } from 'rxjs/Observable';
import { DropdownList } from '../involved-person/_entities/involvedperson.data.model';
import { CountyDetail } from '../as-maltreatment-information/_entities/investigation-finding.data.model';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'as-oas',
  templateUrl: './as-oas.component.html',
  styleUrls: ['./as-oas.component.scss']
})
export class AsOasComponent implements OnInit {
  actionLetterForm: FormGroup;
  id: string;
  involvedPersons: InvolvedPerson[];
  roles: Role[];
  routingInfo: RoutingInfo[];
  investigationSummary: any;
  currentDate: Date;
  dsdsActionsSummary = new DSDSActionSummary();
  caseNumber: string;
  clientName: string;
  caseSummary = [];
  summary: any;
  address = [];
  address1: any;
  address2: any;
  city: any;
  state: any;
  zipcode: any;
  selectedItems = [];
  selectedActual: any[];
  caseWorker: any;
  selectedItemsModel: any;
  involevedPerson$: Observable<InvolvedPerson[]>;
  programType$: Observable<DropdownList[]>;
  actionType$: Observable<DropdownList[]>;
  supervisorName: string;
  jurisdictionCountys$: Observable<CountyDetail[]>;
  stateCountyCode: string;
  jurisdictionState: string;
  jurisdictionZipcode: string | number;
  jurisdictionCity: string;
  juridictionAddress: string;
  juridictionPhone: string;
  jurisdictionEmail: string;
  countyName: string;
  actionSelect: any;
  actionSelectDescription: string;
  programSelect = [];
  programSelectDescription = [];
  displayProgramList$: Observable<ActionLetter[]>;
  headerText: string;
  selected: any;
  actionLetter: SaveActionLetter;
  comarValues = [];
  caseWorkerSelectedRole: any;
  CaseWorkerRole = [
    { 'id': 1, 'name': 'Family Support Worker' },
    { 'id': 2, 'name': 'Caseworker' },
    { 'id': 3, 'name': 'Social Worker' }
  ];



  constructor(
    private _service: GenericService<InvolvedPerson>, private _commonService: CommonHttpService, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private _dataStoreService: DataStoreService, private _alertService: AlertService
  ) {
    // this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
  }

  ngOnInit() {
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    console.log('id...', this.id);
    this.getActionLetterPrefillData();
    this.buildFormGroup();
    this.getActionType();
    this.getProgramType();
    this.loadJurisdiction();
    this.getActionLetter(this.actionLetter);
    this.loadActionLetter();
  }

  buildFormGroup() {
    this.actionLetterForm = this.formBuilder.group({
      /* clientName: [''],
       clientAddress1: [''],
       clientAddress2: [''],
       clientCaseNumber: [''],
       caseWorkerName: [''], */
      clientDate: [''],
      citationList: [[]],
      selectedItems: [''],
      selectedModel1: [''],
      programList: [''],
      actionList: [''],
      countyid: [''],
      caseWorkerRoleName: [''],
      immediate: [''],
      laterNotice: ['']
    });
    this.actionLetterForm.setControl('citations', this.formBuilder.array([]));
    this.updateDate();
  }
  updateDate() {
    this.currentDate = new Date();
    this._dataStoreService.currentStore.subscribe(store => {
      this.summary = this._dataStoreService.getData('dsdsActionsSummary');
      if (this.summary) {
        this.caseNumber = this.summary.da_number;
        this.clientName = this.summary.da_focus;
        this.caseWorker = this.summary.da_assignedto;
      }
    });
    this.actionLetterForm.patchValue({
      clientDate: this.currentDate
    });
    /* this.actionLetterForm.patchValue({
       clientDate: this.currentDate,
       clientName: this.clientName,
       caseNumber: this.caseNumber,
       clientAddress1: this.address1 + ',' + this.address2,
       clientAddress2: this.city + ',' + this.state + ',' + this.zipcode,
       caseWorkerName: this.caseWorker
     }); */
  }

  // tslint:disable-next-line:member-ordering
  public ProgramDropdownList1 = [
    { 'id': 4, 'name': 'The client moves to the jurisdiction of another local department;' },
    { 'id': 5, 'name': 'The client is hospitalized, placed in a long-term care facility, or is receiving community-based waiver program services;' },
    { 'id': 6, 'name': 'The client is no longer eligible' }
  ];


  onSelect(args) {
    console.log('Args... seleceted', args.value);
    this.selectedItems = args.source.triggerValue;
    let result = '';
     this.actionLetterForm.getRawValue().citationList.forEach(item => {
       result = result + '\n' + item.headerdescription + ': ' + item.programdescription;
    });
   /* this.actionLetterForm.getRawValue().citationList.map(item => item.programdescription).forEach(item => {
      result = result + '\n' + item;
    }); */
    // this.comarValues = result;
    this.actionLetterForm.patchValue({
      selectedItems: result
    });
    // this.selectedActual = Array.from(new Set(this.selectedItems));


  }

  onSelectModel(argsModel) {
    console.log('Args... seleceted', argsModel);
    this.selectedItemsModel = argsModel.source.triggerValue;
    // this.selectedActual = Array.from(new Set(this.selectedItems));

    this.actionLetterForm.patchValue({
      selectedItems: this.selectedItems + this.selectedItemsModel
    });
  }
  onProgramSelect(args) {
    this.programSelect.push({programtypekey : args.value});
    this.programSelectDescription = args.source.triggerValue;
    console.log('Args...onProgramSelect selected', this.programSelect);
  }
  onActionSelect(args) {
    this.actionSelect = args.value;
    this.actionSelectDescription = args.source.triggerValue;
    console.log('Args... onActionSelect selected', args);
    this.getSelectedProgramList(this.programSelect[0].programtypekey, this.actionSelect);
  }

  onCaseWorkerSelect(args) {
    console.log('args...', args);
    this.caseWorkerSelectedRole = args.source.triggerValue;
  }

  private getActionLetterPrefillData() {
    const source = forkJoin(
      this._commonService.getPagedArrayList(
        new PaginationRequest({
          page: 1,
          limit: 20,
          method: 'get',
          where: { intakeservreqid: this.id }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
      ),
      this._commonService.getArrayList(
        new PaginationRequest({
          page: 1,
          limit: 50,
          method: 'get',
          where: { intakeserviceid: this.id }
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Assessment.RoutingInfoList
      )
    ).subscribe((result) => {
      this.involvedPersons = result[0]['data'];
      for (let i = 0; i < this.involvedPersons.length; i++) {
        if (this.involvedPersons[i].rolename === 'RA') {
          this.address1 = this.involvedPersons[i].address;
          this.address2 = this.involvedPersons[i].address2;
          this.city = this.involvedPersons[i].city;
          this.state = this.involvedPersons[i].state;
          this.zipcode = this.involvedPersons[i].zipcode;
        }
      }
      this.routingInfo = result[1][0]['routinginfo'];
      for (let i = 0; i < this.routingInfo.length; i++) {
        this.supervisorName = this.routingInfo[0].tousername;
        break;
      }
    });
  }

  getProgramType() {
    const source = this._commonService.getArrayList(
      {
        where: { referencetypeid: 25, teamtypekey: 'AS' },
        method: 'get'
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ActionLetter.GetActionType
    ).map((result) => {
      return {
        response: result.map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.ref_key
            })
        )
      };
    }).share();
    this.programType$ = source.pluck('response');
    this.programType$.subscribe((data) => {
      console.log('data... programType', data);
    });
  }
  getActionType() {
    const source = this._commonService.getArrayList(
      {
        where: { referencetypeid: 26, teamtypekey: 'AS' },
        method: 'get'
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ActionLetter.GetActionType
    ).map((result) => {
      return {
        response: result.map(
          (res) =>
            new DropdownModel({
              text: res.description,
              value: res.ref_key
            })
        )
      };
    }).share();
    this.actionType$ = source.pluck('response');
    this.actionType$.subscribe((data) => {
      console.log('data... action', data);
    });
  }

  private getSelectedProgramList(program, action) {

    this.displayProgramList$ = this._commonService.getArrayList({
      where: { actiontypekey: action, programtypekey: program }, method: 'get'
    },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.ActionLetter.GetProgramList).map(result => {
        return result;
      });
    // this.displayProgramList$ = source.pluck('response');
    this.displayProgramList$.subscribe((data) => {
      console.log('data... displayProgramList', data);
      this.headerText = data[0].headerdescription;
    });
  }

  private loadJurisdiction() {
    // tslint:disable-next-line:max-line-length
    this.jurisdictionCountys$ = this._commonService.getArrayList({ where: { state: 'MD' }, order: 'countyname', nolimit: true, method: 'get' }, CommonUrlConfig.EndPoint.Listing.CountryListUrl + '?filter').map(result => {
      return result;
    });
  }
  onCountyChange(countyId) {
    // let address = '';
    this.jurisdictionCountys$.subscribe(item => {
      if (item) {
        const county = item.find(ca => ca.countyid === countyId);
        if (county) {
          this.countyName = county.countyid;
          // tslint:disable-next-line:max-line-length
          // this.actionLetterForm.patchValue({ address: (county.statecountycode ? county.statecountycode : '') + ' ' + (county.city ? county.city : '') + ' ' + (county.state ? county.state : '') + ' ' + (county.zipcode ? county.zipcode : '') });
          /* this.stateCountyCode = county.statecountycode ? county.statecountycode : '';
           this.jurisdictionCity = county.city ? county.city : '';
           this.jurisdictionState = county.state ? county.state : '';
           this.jurisdictionZipcode = county.zipcode ? county.zipcode : ''; */
          if (county.countyname === 'Anne Arundel County') {
            this.juridictionAddress = '80 West Street Annapolis, Maryland 21401';
            this.juridictionPhone = '410-269-4500';
            this.jurisdictionEmail = 'aacounty.dhs@maryland.gov';
          }
          if (county.countyname === 'Baltimore City') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = 'Talmadge Branch Building 1910 N. Broadway Street Baltimore, Maryland 21213';
            this.juridictionPhone = '443-423-6400';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Baltimore County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '6401 York Road Baltimore, Maryland 21212';
            this.juridictionPhone = '410-853-3000';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Calvert County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '200 Duke Street Prince Frederick, Maryland 20678';
            this.juridictionPhone = '443-550-6900';
            this.jurisdictionEmail = 'calvert.dss@maryland.gov';
          }
          if (county.countyname === 'Caroline County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '207 South Third Street Denton, Maryland 21629';
            this.juridictionPhone = '410-819-4500';
            this.jurisdictionEmail = 'caroline.dss@maryland.gov';
          }

          if (county.countyname === 'Carroll County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '1232 Tech Court Westminster, Maryland 21157';
            this.juridictionPhone = '410-386-3300';
            this.jurisdictionEmail = 'dlcarrolldept_dhr@maryland.gov';
          }
          if (county.countyname === 'Cecil County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = 'Elkton District Court/Multi Service Building 170 East Main Street Elkton,Maryland 21921';
            this.juridictionPhone = '410-996-0100';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Charles County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '200 Kent Avenue LaPlata, Maryland 20646';
            this.juridictionPhone = '301-392-6400';
            this.jurisdictionEmail = 'charles.codss@maryland.gov';
          }
          if (county.countyname === 'Dorchester County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '627 Race Street Cambridge, Maryland 21613';
            this.juridictionPhone = '410-901-4100';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Frederick County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '1888 North Market Street Frederick, Maryland 21701';
            this.juridictionPhone = '301-600-4555';
            this.jurisdictionEmail = 'FCDSS.info@maryland.gov';
          }
          if (county.countyname === 'Garrett County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '12578 Garrett Highway Oakland, Maryland 21550';
            this.juridictionPhone = '301-533-3000';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Harford County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '2 South Bond Street Suite 300 Bel Air, Maryland 21014';
            this.juridictionPhone = '410-836-4700';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Howard County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '7121 Columbia Gateway Drive Columbia, Maryland 21046';
            this.juridictionPhone = '410-872-8700';
            this.jurisdictionEmail = 'howco.dss@maryland.gov';
          }
          if (county.countyname === 'Kent County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '350 High Street P.O. Box 670 Chestertown, MD 21620';
            this.juridictionPhone = '410-810-7600';
            this.jurisdictionEmail = 'Kent.dss@maryland.gov';
          }
          if (county.countyname === 'Montgomery Count') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '401 Hungerford Drive, 5th Floor Rockville, Maryland 20850';
            this.juridictionPhone = '240-777-4513';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === "Prince George's County") {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '805 Brightseat Road Landover, Maryland 20785-4723';
            this.juridictionPhone = '301-909-7000';
            this.jurisdictionEmail = 'pgcdss@dhr.state.md.us';
          }
          if (county.countyname === "Queen Anne's County") {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '125 Comet Drive Centreville, Maryland 21617';
            this.juridictionPhone = '410-758-8000';
            this.jurisdictionEmail = 'qac.user@maryland.gov';
          }
          if (county.countyname === "Saint Mary's County") {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '23110 Leonard Hall Dr Leonardtown, MD 20650';
            this.juridictionPhone = '240-895-7000';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Somerset County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '30397 Mt. Vernon Road Princess Anne, Maryland 21853';
            this.juridictionPhone = '410-677-4200';
            this.jurisdictionEmail = 'somerset.dss@maryland.gov';
          }
          if (county.countyname === 'Washington County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '122 North Potomac Street Hagerstown, Maryland 21740';
            this.juridictionPhone = '240-420-2100';
            // this.jurisdictionEmail = '';
          }
          if (county.countyname === 'Wicomico County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '201 Baptist Street, Suite 27 Salisbury, Maryland 21801';
            this.juridictionPhone = '410-713-3900';
            this.jurisdictionEmail = 'wicodss.county@maryland.gov';
          }
          if (county.countyname === 'Worcester County') {
            console.log('county.countyname..', county.countyname);
            this.juridictionAddress = '299 Commerce Street Snow Hill, Maryland 21863';
            this.juridictionPhone = '410-677-6800';
            // this.jurisdictionEmail = '';
          }

        }
      }
    });
  }
  saveActionLetter() {
    if (this.actionLetterForm.valid) {
    this.actionLetter = Object.assign({}, new SaveActionLetter);
    this.actionLetter.intakeserviceid = this.id;
    this.actionLetter.effectivedate = this.currentDate;
    this.actionLetter.program = this.programSelect;
    this.actionLetter.actiontypekey = this.actionSelect;
    this.actionLetter.comarvalues = this.actionLetterForm.getRawValue().selectedItems;
    this.actionLetter.workername = this.caseWorkerSelectedRole;
    this.actionLetter.immediateaction = this.actionLetterForm.getRawValue().immediate;
    this.actionLetter.notice = this.actionLetterForm.getRawValue().laterNotice;
    this.actionLetter.countyid = this.countyName;
    console.log('this.actionLetter...', this.actionLetter);
    this._commonService.create(this.actionLetter, CaseWorkerUrlConfig.EndPoint.DSDSAction.ActionLetter.SaveCitation).subscribe(result => {
      if (result) {
      this._alertService.success('Action Letter added successfully');
      (<any>$('#addCitation')).modal('hide');
      }
  });
} else {
  this._alertService.warn('Please fill manditory fields');
}
 this.getActionLetter (this.actionLetter);

  }

  loadActionLetter() {
    const __this = this;
    this._commonService.getArrayList({
      where: {
        intakeserviceid: this.id
      },
      method: 'get'
    }, CaseWorkerUrlConfig.EndPoint.DSDSAction.ActionLetter.ListCitation).subscribe(
      (response) => {
        if (response) {
        console.log('response...', response);
        // console.log(this.CaseWorkerRole.find(role => role.name === response[0].workername).id);
        // console.log(__this.CaseWorkerRole.find(role => role.name === response[0].workername).id);
      this.id = response[0].intakeserviceid;
      this.actionLetterForm.patchValue({
        selectedItems: response[0].comarvalues,
        caseWorkerRoleName: __this.CaseWorkerRole.find(role => role.name === response[0].workername).id,
        immediate: response[0].immediateaction,
        programList: [response[0].oasactionletterprogramactionconfig[0].programtypekey],
        actionList: response[0].oasactionletterprogramactionconfig[0].actiontypekey,
        clientDate: response[0].effectivedate,
        laterNotice: response[0].notice,
        countyid: response[0].countyid
      });
      this.onCountyChange(response[0].countyid);
      }
      });
  }


  getActionLetter (actionLetter) {

    if (actionLetter) {
      this.currentDate = actionLetter.effectivedate;
      this.id = actionLetter.intakeserviceid;
      this.programSelect = actionLetter.program;
      this.actionSelect = actionLetter.actiontypekey;
      this.actionLetterForm.getRawValue().selectedItems = actionLetter.comarvalues;
      this.actionLetterForm.getRawValue().caseWorkerRoleName = actionLetter.workername;
      this.actionLetterForm.getRawValue().immediate = actionLetter.immediateaction;
      this.actionLetterForm.getRawValue().laterNotice = actionLetter.notice;
      this.countyName = actionLetter.countyid;
      console.log('get actionLetter...', this.actionLetter);
    }
  }
}
