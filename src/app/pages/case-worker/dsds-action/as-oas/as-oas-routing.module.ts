import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsOasComponent } from './as-oas.component';

const routes: Routes = [{
  path: '',
  component: AsOasComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsOasRoutingModule { }
