import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeLineViewDjsComponent } from './time-line-view-djs.component';

describe('TimeLineViewDjsComponent', () => {
  let component: TimeLineViewDjsComponent;
  let fixture: ComponentFixture<TimeLineViewDjsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeLineViewDjsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeLineViewDjsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
