import { NgModule } from '@angular/core';
import { QuillModule } from 'ngx-quill';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { CommonModule } from '@angular/common';
import { ControlMessagesModule } from '../../../../shared/modules/control-messages/control-messages.module';
import { NgSelectModule } from '@ng-select/ng-select';
// import { AlternativeResponseSummaryRoutingModule } from './alternative-response-summary-routing.module';
import { AlternativeResponseSummaryComponent } from './alternative-response-summary.component';
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';

import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { AlternativeResponseSummaryRoutingModule } from './alternative-response-summary-routing.module';
import { InvestigationFindingsModule } from '../investigation-findings/investigation-findings.module';

@NgModule({
  imports: [
    CommonModule,
    // AlternativeResponseSummaryRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    QuillModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    A2Edatetimepicker,
    AlternativeResponseSummaryRoutingModule,
    ControlMessagesModule,
    InvestigationFindingsModule,
    NgxfUploaderModule.forRoot(),
    NgSelectModule
  ],
  declarations: [AlternativeResponseSummaryComponent]
})
export class AlternativeResponseSummaryModule { }
