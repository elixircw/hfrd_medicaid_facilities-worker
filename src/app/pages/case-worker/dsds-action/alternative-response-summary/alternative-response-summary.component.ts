import { Component, OnInit , OnDestroy, ViewChild, ElementRef, Renderer2} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ControlUtils } from '../../../../@core/common/control-utils';
import { CheckboxModel, DropdownModel, PaginationRequest, PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, GenericService, DataStoreService, SessionStorageService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkReportSummary, Illegalactivity, ReportSummary, PersonAddress, DSDSActionSummary } from '../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { SpeechSynthesizerService } from '../../../../shared/modules/web-speech/shared/services/speech-synthesizer.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { SpeechRecognizerService } from '../../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { SpeechRecognitionService } from '../../../../@core/services/speech-recognition.service';
import * as jsPDF from 'jspdf';
import { AppConstants } from '../../../../@core/common/constants';
import { SubmitForReview, InvestigationFinding, CheckList } from '../investigation-findings/_entities/investigation-finding-data.models';
import { RoutingUser } from '../../../provider-referral/new-private-referral/_entities/existingreferralModel';
import { DispositionAddModal } from '../disposition/_entities/disposition.data.models';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'alternative-response-summary',
    templateUrl: './alternative-response-summary.component.html',
    styleUrls: ['./alternative-response-summary.component.scss']
})
export class AlternativeResponseSummaryComponent implements OnInit, OnDestroy {
    disableNo: boolean;
    disableAddress: boolean;
    downloadInProgress: boolean;
    documentsToDownload: string[] = [];
    id: string;
    isCW: boolean;
    daNumber: string;
    caseclosuresummaryid: string;
    paginationInfo: PaginationInfo = new PaginationInfo();
    possibleIllegalActivityDropdown: FormArray;
    reportSummaryForm: FormGroup;
    ARCaseSummaryForm: FormGroup;
    ARSummaryReportForm: FormGroup;
    closureSubTypeItems: DropdownModel[] = [];
    illegalActivityDd = false;
    countyList = [];
    significantEventDd = false;
    investigation: InvestigationFinding[];
    uploadedDocuments = [];
    isuploadedDocuments  = [];
    significantEventDropdownItems$: Observable<DropdownModel[]>;
    reasonDropDown: any[];
    statusDropDown: any[];
    statusTempDropDown: any[];
    serviceDropDown: any[];
    dsdsActionsSummary = new DSDSActionSummary();
    reviewCheckListForm: FormGroup;
    sourceDropdownItems$: Observable<DropdownModel[]>;
    possibleCheckboxItems: CheckboxModel[] = [];
    reportSummary: ReportSummary;
    LegalGuardian: string;
    reportSummaryDangerAddress: PersonAddress;
    private selectedIllegalActivities: string[] = [];
    private speaking = false;
    private paused = false;
    private voiceNotStarted = true;
    missinglegalRole: any[];
    involvedPersons$: Observable<any[]>;
    countyid: string;
    isLGPresent: boolean;
    clientrefrdservicesdisabled: boolean;
    statuslistdisabled: boolean;
    involevedUnkPerson: any[] = [];
    involvedChildren: any[] = [];
    involvedOthers: any[] = [];
    involvedPersons: any[] = [];
    selectedParticipant: any[];
    reviewCheckList: CheckList[];
    selectedChild: any[];
    isUnkPresent: boolean;
    userRole: AppUser;
    caseWorkerName: AppUser;
    isAs: boolean;
    isCaseWorker: boolean;
    speechRecogninitionOn: boolean;
    speechData: string;
    currentLanguage: string;
    submitForReview = new SubmitForReview();
    currentSpeechRecInput: string;
    isMandatory: any;
    notification: string;
    recognizing = false;
    ARAssessmentClosureDate: Date;
    AssessmentParticipant: any[];
    savedParticipants: any[];
    disableView = false;
    nameTest: string ;
    investigationFind: any[]= [];
    reportflag= -1;
    isapprove: boolean;
    @ViewChild('appButton')
    apButton: ElementRef;
    @ViewChild('rjButton')
    rjButton : ElementRef;

    arStatus : string = '';
    investigationAllegationList: any;

    pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
    investigationFindingForm: FormGroup;
    submitBtn= false;
    savedIndividual: any[];
    arsummaryStatus: string;
    selectedSupervisor: string;
    supervisorsList: any[];
    isClosed = false;

    dispositionFormGroup: FormGroup;
    statusDropdownItems$: Observable<DropdownModel[]>;
    dispositionDropdownItems$: Observable<DropdownModel[]>;
    private dispositionDropdownItems: DropdownModel[];
    private daType: string;
    isChildNotDead = true;
    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _authService: AuthService,
        private _alertService: AlertService,
        private _commonHttpService: CommonHttpService,
        private _reportSummaryService: GenericService<ReportSummary>,
        private _dataStoreService: DataStoreService,
        private _speechSynthesizer: SpeechSynthesizerService,
        private speechRecognizer: SpeechRecognizerService,
        private _speechRecognitionService: SpeechRecognitionService,
        private _renderer : Renderer2,
        private _dispositionAddService: GenericService<DispositionAddModal>,
        private _router: Router,
        private storage: SessionStorageService
    ) {

        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.speechRecogninitionOn = false;
        this.speechData = '';
    }
    ngOnInit() {

        this._speechSynthesizer.initSynthesis();
        this.userRole = this._authService.getCurrentUser();
        this.caseWorkerName = this._authService.getCurrentUser();
        this.speechRecognizer.initialize(this.currentLanguage);
        this.selectedParticipant = [];
        this.selectedChild = [];
        this.initARCaseSummaryForm();
        this.initARSummaryReportForm();
        this.loadReasonDropDown();
        this.loadStatusDropDown();
        this.loadServiceDropDown();
        const store = this._dataStoreService.getCurrentStore();
        if (store['dsdsActionsSummary']) {
            this.dsdsActionsSummary = store['dsdsActionsSummary'];
        this.daType = this.dsdsActionsSummary ? this.dsdsActionsSummary.da_typeid : '';
            this.loadStatuses();
            if (this.dsdsActionsSummary) {
                this.getFindingList();
                // this.getCheckList();
            }
        } else {
            this.getActionSummary();
        }
        if (this._authService.isCW()) {
            this.isCW = true;
            this.isCaseWorker = this.userRole.role.name === AppConstants.ROLES.CASE_WORKER ? true : false;
        }
        if (this.userRole.user.userprofile.teamtypekey === 'AS') {
            this.isAs = true;
            this.isCaseWorker = this.userRole.role.name === AppConstants.ROLES.CASE_WORKER ? true : false;
        }
        this.possibleIllegalActivityDropdown = this.formBuilder.array([this.formBuilder.control(false)]);
        if (this.id !== '0') {
            this.listReportSummary(this.id);
        }
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.PossibleCheckList + '?filter'
            ),
            this._commonHttpService.getArrayList(
                new PaginationRequest({
                    where: { activeflag: 1 },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.Source + '?filter'
            )
        ])
            .map(result => {
                return {
                    intakeServiceRequestIllegalActivityTypes: result[0].map(
                        res =>
                            new CheckboxModel({
                                text: res.typedescription,
                                value: res.intakeservicerequestillegalactivitytypekey,
                                isSelected: false
                            })
                    ),
                    intakeServiceRequestInputTypes: result[1].map(
                        res =>
                            new DropdownModel({
                                text: res.description,
                                value: res.intakeservreqinputtypeid
                            })
                    )
                };
            })
            .share();
        this.sourceDropdownItems$ = source.pluck('intakeServiceRequestInputTypes');
        this._dataStoreService.currentStore.subscribe(store => {
            if (store['da_status'] === 'Closed') {
                ControlUtils.disableElements($('#Involved-Persons').children());
            }
            if (store['countyid']) {
                this.countyid = store['countyid'];
                console.log(this.countyid);
            }
        });
        this.initFindingsForm();
        this.getCountyList();
        this.getARSummaryCase();
        this.loadSupervisor();

        const da_status = this.storage.getItem('da_status');
        if (da_status) {
            if (da_status === 'Closed' || da_status === 'Completed') {
                this.isClosed = true;
            } else {
                this.isClosed = false;
            }
        }
        this.dispositionFormGroup = this.formBuilder.group({
            statusid: [''],
            dispositionid: ['', Validators.required],
            closingcodetypekey: [null],
            reviewcomments: [''],
            tosecurityuserid: [null],
        });
    }

    private getActionSummary() {
        this._commonHttpService.getById(this.daNumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe(
            (response) => {
                this.dsdsActionsSummary = response[0];
                if (this.dsdsActionsSummary) {
                    // this.daType = this.dsdsActionsSummary.da_typeid;
                    this.getFindingList();
                    // this.getCheckList();
                } else {
                    this._alertService.error('DSDS Action Summary is Empty, Please Check Your Data.');
                }
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    private checkListMandatory() {
        this.isMandatory = {
            initialfacetoface: false,
            safec: false,
            cansf: false,
            mfira: false,
            personrole: false
        };
        this.reviewCheckList.map((review) => {
            if (review.taskname === 'initalfacetoface' && review.isvalid === 1) {
                this.isMandatory.initialfacetoface = true;
            } else if (review.taskname === 'Safe C Assessment' && review.isvalid === 1) {
                this.isMandatory.safec = true;
            } else if (review.taskname === 'CANS-F assessment' && review.isvalid === 1) {
                this.isMandatory.cansf = true;
            } else if (review.taskname === 'MFIRA assessment' && review.isvalid === 1) {
                this.isMandatory.mfira = true;
            } else if (review.taskname === 'Personrole' && review.isvalid === 1) {
                this.isMandatory.personrole = true;
            }
        });
    }



    conditionCheckList(): boolean {
        if (this.reviewCheckList && this.reviewCheckList.length) {
           /*  const validateSafe = this.reviewCheckList.filter(
                data => data.taskname === 'Safe C Assessment' && data.isvalid === 1 && data.status === 'OPEN'
            );
            const validateMFIRA = this.reviewCheckList.filter(
                data => data.taskname === 'MFIRA assessment' && data.isvalid === 1 && data.status === 'OPEN'
            );
            const validateCANSF = this.reviewCheckList.filter(
                data => data.taskname === 'CANS-F assessment' && data.isvalid === 1 && data.status === 'OPEN'
            );
            const validateInitial = this.reviewCheckList.filter(
                data => data.taskname === 'initalfacetoface' && data.status === 'NO'
            );
            const ValidatePersonRole = this.reviewCheckList.filter(
                data => data.taskname === 'Personrole' && data.status === 'NO'
            );
            if (validateSafe.length > 0 || validateMFIRA.length > 0 || validateCANSF.length > 0 || validateInitial.length > 0 || ValidatePersonRole.length > 0) {
                return false;
            }
            return true;*/
            const isFacetoFace = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.status === 'YES');
                    const personRole = this.reviewCheckList.find(item => item.taskname === 'Personrole' && item.status === 'YES');
                    const safec = this.reviewCheckList.find(item => item.taskname === 'SAFE-C' && item.status === 'Accepted');
                    const safecohp = this.reviewCheckList.find(item => item.taskname === 'SAFE-C OHP' && item.status === 'Accepted');
                    const mifra = this.reviewCheckList.find(item => item.taskname === 'MFIRA' && item.status === 'Accepted');
                    const cansf = this.reviewCheckList.find(item => item.taskname === 'cans-v2' && item.status === 'Accepted');
                    const isChildDead = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.isvalid === 0);
            if (isFacetoFace && personRole && isChildDead) {
                return true;
            } else if (isFacetoFace && personRole && !isChildDead && (safec || safecohp) && mifra && cansf) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    reviewCheck() {
        // const isValid = this.maltreatmentValidation();
        // if(!isValid) {
        //     (<any>$('#maltreatment-validation-modal')).modal('show');
        //     return;
        // }
        this.commarValidation();
        this._commonHttpService.getArrayList(
            new PaginationRequest({
                where: { intakeserviceid: this.id },
                method: 'get'
            }),
            'Investigationfindings/getfacetofacedetails?filter'
                ).subscribe((result) => {
                    this.reviewCheckList = result;                    
                    /* this.reviewCheckList.map((review) => {
                    if (review.taskname === 'initalfacetoface' && review.status === 'YES') {
                        this.reviewCheckListForm.patchValue({ initalfacetoface: true });
                    } else if (review.taskname === 'Safe C Assessment' && review.status === 'CLOSED') {
                        this.reviewCheckListForm.patchValue({ safec: true });
                    } else if (review.taskname === 'CANS-F assessment' && review.status === 'CLOSED') {
                        this.reviewCheckListForm.patchValue({ canf: true });
                    } else if (review.taskname === 'MFIRA assessment' && review.status === 'CLOSED') {
                        this.reviewCheckListForm.patchValue({ mfira: true });
                    } else if (review.taskname === 'Personrole' && review.status === 'YES') {
                        this.reviewCheckListForm.patchValue({ personrole: true });
                    } 
                });*/
                    const isFacetoFace = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.status === 'YES');
                    const personRole = this.reviewCheckList.find(item => item.taskname === 'Personrole' && item.status === 'YES');
                    const safec = this.reviewCheckList.find(item => item.taskname === 'SAFE-C' && item.status === 'Accepted');
                    const safecohp = this.reviewCheckList.find(item => item.taskname === 'SAFE-C OHP' && item.status === 'Accepted');
                    const mifra = this.reviewCheckList.find(item => item.taskname === 'MFIRA' && item.status === 'Accepted');
                    const cansf = this.reviewCheckList.find(item => item.taskname === 'cans-v2' && item.status === 'Accepted');
                    const isChildDead = this.reviewCheckList.find(item => item.taskname === 'initalfacetoface' && item.isvalid === 0);
                    if (isChildDead) {
                        this.isChildNotDead = false;
                    } else {
                        this.isChildNotDead = true;
                    }
                    if (isFacetoFace) {
                        this.reviewCheckListForm.patchValue({ initalfacetoface: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ initalfacetoface: false });
                    }
                    if (personRole) {
                        this.reviewCheckListForm.patchValue({ personrole: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ personrole: false });
                    }
                    if (safec || safecohp ) {
                        this.reviewCheckListForm.patchValue({ safec: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ safec: false });
                    }
                    if (mifra) {
                        this.reviewCheckListForm.patchValue({ mfira: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ mfira: false });
                    }
                    if (cansf) {
                        this.reviewCheckListForm.patchValue({ canf: true });
                    } else {
                        this.reviewCheckListForm.patchValue({ canf: false });
                    }



                const commar = this.submitForReview.allegedperson.filter((data) => data.investigationfindings === null);
                if (commar.length > 0) {
                    this._alertService.warn('Please select investigation finding');
                } else {
                    // this.checkListMandatory();
                    (<any>$('#checklist-investigation-findings-review')).modal('show');
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    initARCaseSummaryForm() {
      this.ARCaseSummaryForm = this.formBuilder.group({
        neglect: [''],
        physical_abuse: [''],
        sexual_abuse: [''],
        mental_injury: [''],
        reason: [null],
        caseclosuresummaryid: [null],
        intakeserviceid : [null],
        referralreason: [null, Validators.required],
        riskissues: [null],
        recommendation: [null],
        interventionissues: [null],
        statusList: this.formBuilder.array([]),
        clientrefrdservices: [null],
        ARAssessmentClosureDate:[null],
        closuretypekey: [null, Validators.required],
        closuresubtypekey: [null],
        notes: [null],
        participants: [null]
     });

    }

    initARSummaryReportForm() {
        this.ARSummaryReportForm = this.formBuilder.group({
            personid: [null],
            allegation: [[]],
            investigationallegationid: [''],
            victim_explanation: [''] ,
            sibling_explanation: [''] ,
            guardian_explanation: [''],
            maltreator_explanation: [''],
            med_assessmnts: [''],
            expert_assessmnts: [''],
            collateral_interviews: [''],
            criminal_history_inv: [''],
            home_conditions: [''],
       });
      }
      private initFindingsForm() {
        this.investigationFindingForm = this.formBuilder.group({
            jointinvestigation: false,
            summary: ['', Validators.required],
            remarks: [''],
            investigationid: [''],
            allegedperson: this.formBuilder.array([])
        });
        this.reviewCheckListForm = this.formBuilder.group({
            initalfacetoface: false,
            safec: false,
            canf: false,
            mfira: false,
            personrole: false
        });
    }

    getFindingList() {
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: this.paginationInfo.pageNumber,
                    limit: this.paginationInfo.pageSize,
                    where: {
                        investigationid: this.dsdsActionsSummary.da_investigationid
                    },
                    method: 'get'
                }),
                'Investigationallegations/getmaltreatmentfinding?filter'
            )
            .subscribe((res) => {
                if (res) {
                    this.investigation = res;
                    if (this.investigation.length > 0) {
                        this.submitBtn = true;
                    }
                    this.investigationAllegationList = res;
                    this.setFormValues();
                    if (this.investigation && Array.isArray(this.investigation) && this.investigation[0]) {
                        this.investigationFindingForm.patchValue({
                            jointinvestigation: this.investigation[0].jointinvestigation,
                            summary: this.investigation[0].investigationsummary,
                            remarks: this.investigation[0].notes

                        });
                    }
                }
            });
    }

    setFormValues() {
        const control = new FormArray([]);
        this.investigationFindingForm.removeControl('allegedperson');
        this.clearFormArray(control);
        this.investigation.forEach((x, index) => {
            control.push(this.buildInvestigationForm(x, index));
        });
        this.investigationFindingForm.addControl('allegedperson', control);
    }

    clearFormArray = (formArray: FormArray) => {
        formArray = this.formBuilder.array([]);
    }

    private buildInvestigationForm(x, index): FormGroup {
        const investFind = this.formBuilder.group({
            personid: x.personid ? x.personid : '',
            investigationallegationid: x.investigationallegationid ? x.investigationallegationid : null,
            personname: x.personname ? x.personname : '',
            name: x.name ? x.name : '',
            relationship: x.relationship ? x.relationship : '',
            maltreatmentkey: x.findings ? x.findings.map((item) => item.investigationfindingtypekey) : '',
            investigationfindingtypekey: '',
            allegation: x.allegation ? x.allegation : '',
            intentionalinjurydesc: '',
            findingcomments: '',
            isharm: '',
            isharmsubstantial: '',
            harmdesc: '',
            investigationfindings: '',
            ischildfatality: x.ischildfatality ? x.ischildfatality : 0,
            victim_explanation: x.victim_explanation ? x.victim_explanation : '',
            sibling_explanation: x.sibling_explanation ? x.sibling_explanation : '',
            guardian_explanation: x.guardian_explanation ? x.guardian_explanation : '',
            maltreator_explanation: x.maltreator_explanation ? x.maltreator_explanation : '',
            med_assessmnts: x.med_assessmnts ? x.med_assessmnts : '',
            expert_assessmnts: x.expert_assessmnts ? x.expert_assessmnts : '',
            collateral_interviews: x.collateral_interviews ? x.collateral_interviews : '',
            criminal_history_inv: x.criminal_history_inv ? x.criminal_history_inv : '',
            home_conditions: x.home_conditions ? x.home_conditions : '',
            intakeservicerequestactorid: null,
            isAppealDone: false,
            isFinalizeDone: false,
            maltreatmentid: null
        });
        const investFindType = x.findings ? x.findings.map((item) => item.investigationfindingtypekey) : null;
        investFind.controls['investigationfindingtypekey'].patchValue(investFindType);
        const intentionalInjuryDesc = x.findings ? x.findings.map((item) => item.intentionalinjurydesc) : null;
        investFind.controls['intentionalinjurydesc'].patchValue(intentionalInjuryDesc);
        const findingComments = x.findings ? x.findings.map((item) => item.findingcomments) : null;
        investFind.controls['findingcomments'].patchValue(findingComments);
        const isharm = x.findings ? x.findings.map((item) => item.isharm) : null;
        investFind.controls['isharm'].patchValue(isharm);
        const isHarmSubstantial = x.findings ? x.findings.map((item) => item.isharmsubstantial) : null;
        investFind.controls['isharmsubstantial'].patchValue(isHarmSubstantial);
        const harmDesc = x.findings ? x.findings.map((item) => item.harmdesc) : null;
        investFind.controls['harmdesc'].patchValue(harmDesc);
        const relationShip = x.maltreators ? x.maltreators.map((item) => item.relationship) : null;
        investFind.controls['relationship'].patchValue(relationShip);
        const displayname = x.maltreators ? x.maltreators.map((item) => item.displayname) : null;
        investFind.controls['allegation'].patchValue(displayname);
        const intakeservicerequestactorid = x.maltreators ? x.maltreators.map((item) => item.intakeservicerequestactorid) : null;
        investFind.controls['intakeservicerequestactorid'].patchValue(intakeservicerequestactorid);
        const maltreatmentid = x.maltreatmentid ? x.maltreatmentid : null;
        investFind.controls['maltreatmentid'].patchValue(maltreatmentid);
        const alleagation = this.investigationAllegationList.find(al => al.maltreatmentid === maltreatmentid);

        let appealData = null;
        if (alleagation) {
            const maltreators = alleagation.maltreators ? alleagation.maltreators : null;
            appealData = maltreators.find(mal => intakeservicerequestactorid.includes(mal.intakeservicerequestactorid));
            if (appealData) {
                const hasHeader = !!appealData.scdecisiontypekey;
                const hasOverride = !!appealData.overridefindingtypekey;
                investFind.controls['isAppealDone'].patchValue(hasHeader);
                investFind.controls['isFinalizeDone'].patchValue(hasOverride);
                console.log(appealData);
            }
        }

        return investFind;
    }

    openReportDialog(index): void {
        this.reportflag = index;
    }

      commarValidation() {
        this.submitForReview = Object.assign(new SubmitForReview(), this.investigationFindingForm.value);
        this.submitForReview.investigationid = this.dsdsActionsSummary.da_investigationid;
        if (this.investigation && Array.isArray(this.investigation) && this.investigation.length) {
            this.submitForReview.maltreatmentid = this.investigation[0].maltreatmentid;
        }

        if (this.submitForReview.allegedperson) {
            this.submitForReview.allegedperson = this.submitForReview.allegedperson.map((data) => {
                return Object.assign({
                    personid: data.personid,
                    investigationallegationid: data.investigationallegationid,
                    ischildfatality: Number(data.ischildfatality),
                    victim_explanation: data.victim_explanation,
                    sibling_explanation: data.sibling_explanation,
                    guardian_explanation: data.guardian_explanation,
                    maltreator_explanation: data.maltreator_explanation,
                    med_assessmnts: data.med_assessmnts,
                    expert_assessmnts: data.expert_assessmnts,
                    collateral_interviews: data.collateral_interviews,
                    criminal_history_inv: data.criminal_history_inv,
                    home_conditions: data.home_conditions,
                });
            });
        }
    }

      saveInvestigation() {
        this.commarValidation();
        if (this.ARSummaryReportForm.valid) {
            this._commonHttpService.create(this.submitForReview, 'investigationmaltreatment/addfindings').subscribe(
                (result) => {
                    this._alertService.success('Investigation findings saved successfully!');
                    // this.closePopup();
                    this.getFindingList();
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }

    getCountyList() {
        this._commonHttpService.getArrayList({
            where: {
                activeflag: '1',
                state: 'MD'
            },
            order: 'countyname asc',
            method: 'get',
            nolimit: true
        }, 'admin/county?filter').subscribe((item) => {
            if (item && item.length) {
                this.countyList = item.filter((res) => res.countyid === this.countyid);
            }
        });
    }

    async downloadCasePdf(val) {
        const source = document.getElementById(val);
        const pages = document.getElementsByClassName('pdf-page');
        let pageImages = [];
        for (let i = 0; i < pages.length; i++) {
          const pageName = pages.item(i).getAttribute('data-page-name');
          const isPageEnd = pages.item(i).getAttribute('data-page-end');
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
                const img = canvas.toDataURL('image/png');
                pageImages.push(img);
                if (isPageEnd === 'true') {
                    this.pdfFiles.push({ fileName: pageName, images: pageImages });
                    pageImages = [];
                }
              });
        }
         this.convertImageToPdf();
    }

    convertImageToPdf() {
        this.pdfFiles.forEach((pdfFile) => {
            // const doc = new jsPDF('landscape');
            let doc = null;
            if (pdfFile.fileName === 'AR Summary Report') {
                doc = new jsPDF('landscape');
            } else {
                doc = new jsPDF();
            }

            const width = doc.internal.pageSize.getWidth() - 10;
            const heigth = doc.internal.pageSize.getHeight() - 10;
            pdfFile.images.forEach((image, index) => {

                doc.addImage(image, 'JPEG', 3, 5, width, heigth);
                if (pdfFile.images.length > index + 1) {
                    doc.addPage();
                }
            });
            doc.save(pdfFile.fileName);
        });
        (<any>$('#docu-View')).modal('hide');
        this.pdfFiles = [];
        this.downloadInProgress = false;
    }

    arSummaryPrint() {
        this.downloadInProgress = true;
        this.nameTest = 'TEST';
        const pdfList = this.documentsToDownload;
        pdfList.forEach((element) => {
            this.downloadCasePdf(element);
        });
        (<any>$('#arSummaryPrint')).modal('show');
    }

    closeView() {
    }

    onChangeIllegalActivityMain($event) {
        this.illegalActivityDd = $event.target.checked;
        if (this.illegalActivityDd) {
            this.buildCheckBox();
        } else {
            this.selectedIllegalActivities = [];
        }
    }
    changeSignificantEvent($event) {
        this.significantEventDd = $event.target.checked;
        if (this.significantEventDd) {
            this.reportSummaryForm.patchValue({
                significantkey: this.reportSummary.servicerequestincidenttypekey ? this.reportSummary.servicerequestincidenttypekey : ''
            });
        } else {
            this.reportSummaryForm.value.significantkey = null;
        }
    }
    onChangeIllegalActivity($event) {
        if ($event.target.checked) {
            this.selectedIllegalActivities.push($event.target.value);
        } else {
            this.selectedIllegalActivities.splice($event.target.value, 1);
        }
    }
    saveReportSummary() {
        const userId = this._authService.getCurrentUser().userId;
        const caseWorkReportSummary = new CaseWorkReportSummary();
        caseWorkReportSummary.servicerequestincidenttypekey = this.reportSummaryForm.value.significantkey ? this.reportSummaryForm.value.significantkey : null;
        const addedItems = this.selectedIllegalActivities.map(id => {
            const illegalactivity = this.reportSummary.intakeservicerequestillegalactivity.filter((item, index, array) => {
                return item.intakeservicerequestillegalactivitytypekey === id;
            });
            if (!illegalactivity.length) {
                return new Illegalactivity({
                    activeflag: 1,
                    effectivedate: new Date(),
                    insertedby: userId,
                    intakeservicerequestillegalactivitytypekey: id,
                    intakeservicerequestid: this.reportSummary.intakeserviceid,
                    updatedby: userId
                });
            } else {
                return new Illegalactivity({
                    activeflag: 1,
                    effectivedate: new Date(),
                    insertedby: userId,
                    intakeservicerequestillegalactivitytypekey: id,
                    intakeservicerequestid: this.reportSummary.intakeserviceid,
                    intakeservicerequestillegalactivityid: this.reportSummary.intakeservicerequestillegalactivity[0].intakeservicerequestillegalactivityid,
                    updatedby: userId
                });
            }
        });
        caseWorkReportSummary.intakeservicerequestillegalactivity = [];
        caseWorkReportSummary.intakeservicerequestillegalactivity.push(...addedItems);
        caseWorkReportSummary.suspiciousdeath = this.reportSummaryForm.value.suspiciousdeath;
        caseWorkReportSummary.missingpersons = this.reportSummaryForm.value.missingpersons;
        caseWorkReportSummary.effectivedate = new Date();
        this._commonHttpService.update(this.reportSummary.intakeserviceid, caseWorkReportSummary, CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.UpdateReportSummary).subscribe(
            responce => {
                this._alertService.success('Report summary updated successfully');
                this.listReportSummary(this.id);
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }

    readNarrative() {
        if (this.voiceNotStarted) {
            const narrativeText = $('#reportSummaryNarrative').text();
            this._speechSynthesizer.speak(narrativeText, 'en-US');
            this.voiceNotStarted = false;
            this.paused = false;
            this.speaking = true;
        } else if (this.speaking) {
            this._speechSynthesizer.pause();
            this.paused = true;
            this.speaking = false;
        } else if (this.paused) {
            this._speechSynthesizer.resume();
            this.paused = false;
            this.speaking = true;
        }
    }

    private formInitilizer(reportSummary: ReportSummary) {
        this.reportSummaryForm = this.formBuilder.group({
            suspiciousdeath: [reportSummary.suspiciousdeath ? reportSummary.suspiciousdeath : false],
            missingpersons: [reportSummary.missingpersons ? reportSummary.missingpersons : false],
            significantkey: [reportSummary.servicerequestincidenttypekey ? reportSummary.servicerequestincidenttypekey : '']
        });
    }
    private listReportSummary(id: string) {
        this._reportSummaryService.getSingle(new PaginationRequest({}), CaseWorkerUrlConfig.EndPoint.DSDSAction.ReportSummary.ReportSummary + this.id).subscribe(result => {
            this.reportSummary = result;
            if (this.reportSummary.narrative) {
                this.reportSummary.narrative = this.reportSummary.narrative.replace('<a', '<a target="_blank"');
            }
            const reportSummaryDanger = this.reportSummary.intakeservicerequestactor.filter(item => {
                if (item.actor) {
                    if (item.actor.Person) {
                        return item.actor.Person.dangerlevel === 1;
                    }
                }
            });
            if (reportSummaryDanger.length !== 0) {
                this.disableNo = true;
            } else {
                this.disableNo = false;
            }
            const reportSummaryDangerAddress = this.reportSummary.intakeservicerequestactor.filter(item => {
                if (item.actor) {
                    return (
                        item.actor.Person.personaddress.filter(res => {
                            return res.danger === true;
                        }).length !== 0
                    );
                }
            });
            if (reportSummaryDangerAddress.length !== 0) {
                this.disableAddress = true;
            } else {
                this.disableAddress = false;
            }
            if (result.servicerequestincidenttypekey) {
                this.significantEventDd = true;
            }
            if (result.intakeservicerequestillegalactivity.length) {
                this.illegalActivityDd = true;
            }
            // this.formInitilizer(result);
            this.possibleIllegalActivityDropdown = this.buildCheckBox();
        });
        if (this._authService.isCW()) {
            this.isCW = true;
            this.legalGuardianCheck();
            this.getInvolvedUnkPerson();
        }
    }

    legalGuardianCheck() {
        this.isLGPresent = false;
        this.missinglegalRole = [];
        this.involvedPersons$ = this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeservreqid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.InvolvedPersonListUrl + '?data'
            )
            .share()
            .pluck('data');
        this.involvedPersons$.subscribe((items) => {
            if (items) {
                this.getInvolvedChildrenAndOthers(items);
                const person = items.filter((item) => {
                    const roles = item.roles;
                    roles.forEach(element => {
                        if (element.intakeservicerequestpersontypekey === 'LG') {
                            this.missinglegalRole.push(item);
                        }
                    });

                });
            }
            if (this.missinglegalRole && this.missinglegalRole.length > 0) {
            } else {
                this.isLGPresent = true;
                (<any>$('#legal-guardian-role')).modal('show');
            }
        });
    }

    getInvolvedUnkPerson() {
        this.isUnkPresent = false;
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: { intakeserviceid: this.id }
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .UnkPersonList + '?filter'
            )
            .subscribe(data => {
                this.involevedUnkPerson = data;
                if (this.involevedUnkPerson) {
                    const unkPersonList = this.involevedUnkPerson.filter(item => item.isNew);
                    if (unkPersonList && unkPersonList.length > 0) {
                        this.isUnkPresent = true;
                        (<any>$('#legal-guardian-role')).modal('show');
                    }
                 }
                });
    }



    private buildCheckBox(): FormArray {
        let selectedIllActs: string[] = [];
        if (this.reportSummary.intakeservicerequestillegalactivity && this.reportSummary.intakeservicerequestillegalactivity.length) {
            selectedIllActs = this.reportSummary.intakeservicerequestillegalactivity.map(item => {
                return item.intakeservicerequestillegalactivitytypekey;
            });
        }
        this.selectedIllegalActivities = selectedIllActs;
        return ControlUtils.buildCheckBoxGroup(this.formBuilder, this.possibleCheckboxItems, selectedIllActs);
    }


    getInvolvedChildrenAndOthers(invlovedPersonsList) {
        invlovedPersonsList.map((item, $index) => {
            if (
                item.rolename !== 'AV' &&
                item.rolename !== 'AM' &&
                item.rolename !== 'CHILD' &&
                item.rolename !== 'RC' &&
                item.rolename !== 'BIOCHILD' &&
                item.rolename !== 'NVC' &&
                item.rolename !== 'OTHERCHILD' &&
                item.rolename !== 'PAC'
                // (item.roles.length &&
                //     item.roles.filter(
                //         itm =>
                //             itm.rolename !== 'AV' &&
                //             itm.rolename !== 'AM' &&
                //             itm.rolename !== 'CHILD' &&
                //             itm.rolename !== 'RC' &&
                //             itm.rolename !== 'BIOCHILD' &&
                //             itm.rolename !== 'NVC' &&
                //             itm.rolename !== 'OTHERCHILD' &&
                //             itm.rolename !== 'PAC'
                //     ))
            ) {
                this.involvedOthers.push(item);
                this.involvedPersons.push(item);
            } else {
                this.involvedChildren.push(item);
                this.involvedPersons.push(item);
            }

            if (item.rolename === 'LG') {
                this.LegalGuardian = ( this.LegalGuardian ? this.LegalGuardian : '' ) + ($index !== 0 ? ', ' : '') + item.firstname + ' ' + item.lastname  ;
            }
            // });
        });
        this.getARSummaryCase();
    }

    loadReasonDropDown() {

        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 101, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            ).subscribe ( (data) => {
                this.reasonDropDown = data;
                console.log(data);
            });
    }

    loadStatusDropDown() {

        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 102, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            ).subscribe ( (data) => {

                this.closureSubTypeItems = data.map(
                    res =>
                      new DropdownModel({
                        text: res.value_text,
                        value: res.ref_key
                      })
                  );

                const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
                this.closureSubTypeItems.map(c => {
                    status.push(new FormControl(false));
                });
                this.statusDropDown = data;
            });
    }

    unchecksubtypes() {
        const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
        this.closureSubTypeItems.map(function (c, index) {
            status.removeAt(0);
        });
        this.closureSubTypeItems.map(function (c, index) {
            status.push(new FormControl(false));
        });
    }

    loadServiceDropDown() {

        this._commonHttpService
            .getArrayList(
                {
                    where: { referencetypeid: 103, teamtypekey: 'CW' },
                    method: 'get'
                },
                'referencetype/gettypes' + '?filter'
            ).subscribe ( (data) => {
                this.serviceDropDown = data;
                console.log(data);
            });

    }

    getRiskDropDown(array) {
        const riskDropDown = [];
        if ( array && array.length) {
        array.forEach( data => {
            const obj =  data;
            riskDropDown.push(obj);
        }); }
        return riskDropDown ? riskDropDown : [];
    }


    setRiskDropDown(array) {
        const riskDropDown = [];
        if ( array && array.length) {
        const arryriskdropdown = array.split(',');
        arryriskdropdown.forEach( data => {
            riskDropDown.push(data);
        }); }
        return riskDropDown ? riskDropDown : [];
    }

    chooseAssessmentParticipant(event, child, isChild) {
        let participantObj;
        if (event.checked) {
            participantObj = { intakeservicerequestactorid: child.intakeservicerequestactorid,
                ischild: isChild
            };
        } else {
            if (this.AssessmentParticipant && this.AssessmentParticipant.length) {
                for (let i = 0; i < this.AssessmentParticipant.length; i++) {
                    if (this.AssessmentParticipant[i].intakeservicerequestactorid === child.intakeservicerequestactorid) {
                        this.AssessmentParticipant.splice(i, 2);
                    }
                }
            }
        }
      if (this.AssessmentParticipant && this.AssessmentParticipant.length) {
           // AssessmentParticipant Array Null Check
      } else {
        this.AssessmentParticipant = [];
      }
      if (event.checked) {
        this.AssessmentParticipant.push(participantObj);
      } else {
        const index = this.AssessmentParticipant.findIndex( p => p.intakeservicerequestactorid === child.intakeservicerequestactorid);
        this.selectedChild.splice(index, 1);
      }
    }
    chooseChildren(event, child) {
        if (event.checked) {
        this.selectedChild.push(child.intakeservicerequestactorid);
         } else {
            const selectedItem = child.intakeservicerequestactorid;
            const index = this.selectedParticipant.indexOf(selectedItem);
            this.selectedChild.splice(index, 1);
         }
    }
    chooseParticipant(event, participant) {
        if (event.checked) {
            this.selectedParticipant.push(participant.intakeservicerequestactorid);
             }    else {
                 const selectedItem = participant.intakeservicerequestactorid;
                const index = this.selectedParticipant.indexOf(selectedItem);
                this.selectedParticipant.splice(index, 1);
             }
    }

    saveARCAseSummary(arStatus: string, caseCreatedDate?) {


        if (!this.ARCaseSummaryForm.controls['referralreason'].valid ) {
            this._alertService.error('Referral reason is required');
            return;
        }
        if (!this.ARCaseSummaryForm.controls['closuretypekey'].valid && arStatus  !== 'summarySave') {
            this._alertService.error('Closure status is required');
            return;
        }

        if (!this.AssessmentParticipant) {
            this._alertService.error('Please select a participant');
            return;
        }
        if (this.AssessmentParticipant && this.AssessmentParticipant.length === 0) {
            this._alertService.error('Please select a participant');
            return;
        }
        this.ARCaseSummaryForm.patchValue({
            participants: this.AssessmentParticipant ? this.AssessmentParticipant : [],
            caseclosuresummaryid: this.caseclosuresummaryid
          });

        const ARCaseSummaryData = this.ARCaseSummaryForm.getRawValue();
        if (arStatus  === 'summarySave') {
            ARCaseSummaryData.isapprove = false;
        } else if (arStatus === 'Submitted') {
            ARCaseSummaryData.assignsecurityuserid = this.selectedSupervisor;
            ARCaseSummaryData.isapprove = true;
        } else if ( arStatus === 'accepted') {
            ARCaseSummaryData.isapprove = true;
        } else if ( arStatus === 'Rejected') {
            ARCaseSummaryData.isapprove = true;
        }
        ARCaseSummaryData.arStatus = arStatus;
        this.statusTempDropDown = [];
        ARCaseSummaryData.statusList.forEach((item, index) => {
            if (item) {
                this.statusTempDropDown.push(this.closureSubTypeItems[index].value);
            }
        });
        ARCaseSummaryData.closuresubtypekey = this.statusTempDropDown;
        ARCaseSummaryData.intakeserviceid = this.id;
        ARCaseSummaryData.interventionissues = this.getRiskDropDown(ARCaseSummaryData.interventionissues);
        ARCaseSummaryData.intakeserreqstatustypeid = this.dispositionFormGroup.getRawValue().statusid.split('~')[1],
        ARCaseSummaryData.dispostionid = this.dispositionFormGroup.getRawValue().dispositionid;
        console.log('ARCaseSummaryData' + JSON.stringify(ARCaseSummaryData));
        if (ARCaseSummaryData) {
          this._commonHttpService
            .create(ARCaseSummaryData, 'caseclosuresummary/addupdate')
            .subscribe(response => {
              (<any>$('#list-supervisor')).modal('hide');
              //this.arsummaryStatus = 'Draft';

              //this.disableView = response.disableArSummary;
              //this.apButton.nativeElement.disabled = true;
              this.getARSummaryCase();
              if ('Submitted' === arStatus ) {
                this.arStatus = 'Submitted';
                this._alertService.success('AR case summary submitted for approval');
              } else if ('summarySave' === arStatus) {
                this.arStatus = 'Saved';
                this._alertService.success('AR case summary saved successfully');
              } else if ('accepted' === arStatus) {
                this.arStatus = 'Approved';
                this._alertService.success('AR case summary Approved successfully');
                this.closeCase();
                //this.rjButton.nativeElement.disabled = true;
              } else if('Rejected' === arStatus){
                this.arStatus = 'Rejected';
                this._alertService.success('AR case summary Rejected successfully');
                //this.rjButton.nativeElement.disabled = true;
              }
            });
         }

        // this.apButton.nativeElement.disabled = true;
    }



    activateSpeechToText(type): void {
        this.currentSpeechRecInput = type;
        this.recognizing = type;
        this.speechRecogninitionOn = !this.speechRecogninitionOn;
        if (this.speechRecogninitionOn) {
          this._speechRecognitionService.record().subscribe(
            // listener
            (value) => {
              this.speechData = value;
              switch (type) {
                case 'q1':
                  const comments = this.ARCaseSummaryForm.getRawValue().serviceProvidedAddress;
                  this.ARCaseSummaryForm.patchValue({ serviceProvidedAddress: comments + ' ' + this.speechData });
                  break;
                case 'q2':
                  const comments1 = this.ARCaseSummaryForm.getRawValue().issuesRequiring;
                  this.ARCaseSummaryForm.patchValue({ issuesRequiring: comments1 + ' ' + this.speechData });
                  break;
                case 'q3':
                  const comments2 = this.ARCaseSummaryForm.getRawValue().recommendationforFamily;
                  this.ARCaseSummaryForm.patchValue({ recommendationforFamily: comments2 + ' ' + this.speechData });
                  break;
                default: break;
              }
            },
            // errror
            (err) => {
              console.log(err);
              this.recognizing = false;
              if (err.error === 'no-speech') {
                this.notification = `No speech has been detected. Please try again.`;
                this._alertService.warn(this.notification);
                this.activateSpeechToText(type);
              } else if (err.error === 'not-allowed') {
                this.notification = `Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.`;
                this._alertService.warn(this.notification);
                // this.activateSpeechToText(type);
              } else if (err.error === 'not-microphone') {
                this.notification = `Microphone is not available. Please verify the connection of your microphone and try again.`;
                this._alertService.warn(this.notification);
                // this.activateSpeechToText(type);
              }
            },
            // completion
            () => {
              this.speechRecogninitionOn = true;
              console.log('--complete--');
              this.activateSpeechToText(type);
            }
          );
        } else {
          this.recognizing = false;
          this.deActivateSpeechRecognition();
        }
      }

      deActivateSpeechRecognition() {
        this.speechRecogninitionOn = false;
        this._speechRecognitionService.destroySpeechObject();
      }

      ngOnDestroy(): void {
        this._speechRecognitionService.destroySpeechObject();
      }
      isChildParticipantSelected(childActorId) {
          const form = this.ARCaseSummaryForm.getRawValue();
          if ( this.savedParticipants && this.savedParticipants.length) {
            this.savedParticipants.forEach((data, $index) => {
                if (data.intakeservicerequestactorid === childActorId ) {
                    return true;
                }
            });
        //   const selected = this.savedParticipants.indexOf(p => p.intakeservicerequestactorid === childActorId)
        //   const Index =  selected ? selected : -1;
        //   return (Index !== -1) ? true : false; }
        }
        return false;
      }

    onChangeClosureStatus(value) {
        if (value === 'ONGS') {
            this.clientrefrdservicesdisabled = true;
            this.ARCaseSummaryForm.patchValue({ clientrefrdservices: '' });
            this.statuslistdisabled = true;
            this.unchecksubtypes();
        } else if (value === 'CONS') {
            this.clientrefrdservicesdisabled = false;
            this.statuslistdisabled = true;
            this.unchecksubtypes();
        } else if (value === 'LDSS') {
            this.clientrefrdservicesdisabled = true;
            this.ARCaseSummaryForm.patchValue({ clientrefrdservices: '' });
            this.statuslistdisabled = false;
        } else if (value === 'RRMS') {
            this.clientrefrdservicesdisabled = true;
            this.ARCaseSummaryForm.patchValue({ clientrefrdservices: '' });
            this.statuslistdisabled = true;
            this.unchecksubtypes();
        }
    }



      getARSummaryCase() {
          console.log('Get data');
          this.savedParticipants = [];
          this.savedIndividual = [];
          this._commonHttpService.getSingle({
            where: {
                intakeserviceid: this.id
            },
            method: 'get',
            page: 1,
            limit: 10
            // caseclosuresummary/getcaseclosuresummarylist?data
        }, 'caseclosureparticipant/getcaseclosurelist?filter').subscribe((item) => {
            console.log('Get data1' + JSON.stringify(item));
        if (item) {
         // this.ARCaseSummaryForm.patchValue(item[0]);
            const summaryData =  item && item.length && Array.isArray(item) ? item[0] : null;
            if (summaryData) {

         this.ARCaseSummaryForm = this.formBuilder.group({
            reason: summaryData.reason,
            caseclosuresummaryid: summaryData.caseclosuresummaryid,
            intakeserviceid : summaryData.intakeserviceid,
            referralreason: summaryData.referralreason,
            riskissues: summaryData.riskissues,
            recommendation: summaryData.recommendation,
            statusList: this.formBuilder.array([]),
            clientrefrdservices: null,
            closuretypekey: summaryData.closuretypekey,
            // closuresubtypekey: item[0].closuresubtypekey,
            ARAssessmentClosureDate: summaryData.closuredate,
            interventionissues: null,
            notes: summaryData.notes,
            participants: summaryData.participants ? summaryData.participants : null
            });
         }

         this.ARAssessmentClosureDate = summaryData.closuredate;

         this.caseclosuresummaryid = summaryData.caseclosuresummaryid;
        // this.
        if(summaryData.childparticipant){
        for (let i = 0; i < summaryData.childparticipant.length; i++) {
            if(summaryData.childparticipant[i]){
         this.savedParticipants.push(summaryData.childparticipant[i]);
                }
            }
        }
        if(summaryData.individualparticipant){
         for (let i = 0; i < summaryData.individualparticipant.length; i++) {
             if(summaryData.individualparticipant[i]){
         this.savedIndividual.push(summaryData.individualparticipant[i]);
             }
            }
         }
         this.AssessmentParticipant = [];
         if (this.savedParticipants && this.savedParticipants.length && this.involvedChildren && this.involvedChildren.length) {
            for (let i = 0; i < this.involvedChildren.length; i++) {
                if (this.savedParticipants[0] && this.savedParticipants[0].intakeservicerequestactorid) {
                    for (let j = 0; j < this.savedParticipants.length; j++) {
                        if (this.involvedChildren[i].intakeservicerequestactorid === this.savedParticipants[j].intakeservicerequestactorid) {
                            this.involvedChildren[i].isChecked = true;
                            const participantObj = { intakeservicerequestactorid: this.involvedChildren[i].intakeservicerequestactorid,
                                ischild: 1
                            };
                            this.AssessmentParticipant.push(participantObj);
                        }
                    }
                }
            }
        }
            if (this.savedIndividual && this.savedIndividual.length && this.involvedPersons && this.involvedPersons.length) {
                for (let i = 0; i < this.involvedPersons.length; i++) {
                    if (this.savedIndividual[0] && this.savedIndividual[0].intakeservicerequestactorid) {
                        for (let j = 0; j < this.savedIndividual.length; j++) {
                            if (this.involvedPersons[i].intakeservicerequestactorid === this.savedIndividual[j].intakeservicerequestactorid) {
                                this.involvedPersons[i].isChecked = true;
                                const participantObj = { intakeservicerequestactorid: this.involvedPersons[i].intakeservicerequestactorid,
                                    ischild: 0
                                };
                                this.AssessmentParticipant.push(participantObj);
                            }
                        }
                        const val = this.savedIndividual.find(data => this.involvedPersons[i].intakeservicerequestactorid.includes(data.intakeservicerequestactorid));
                        console.log('-----> ' + val);
                    }
                }
            }

            if (summaryData.closuretypekey === 'CONS') {
                this.statuslistdisabled = true;
            } else if (summaryData.closuretypekey === 'ONGS') {
                this.statuslistdisabled = true;
            } else if (summaryData.closuretypekey === 'LDSS') {
                this.statuslistdisabled = false;
            } else if (summaryData.closuretypekey === 'RRMS') {
                this.statuslistdisabled = true;
            }

            if (summaryData.routingstatustypeid === 15) {
                this.arsummaryStatus = 'Review';
            } else if (summaryData.routingstatustypeid === 16) {
                this.arsummaryStatus = 'Approved';
            } else if (summaryData.routingstatustypeid === 17) {
                this.arsummaryStatus = 'Rejected';
            } else {
                this.arsummaryStatus = 'Draft';
            }
           // this.isChildParticipantSelected(summaryData.childparticipant.intakeservicerequestactorid);
        // }
         if (summaryData.interventionissues) {
         const riskData = this.setRiskDropDown(summaryData.interventionissues);
         this.ARCaseSummaryForm.patchValue({interventionissues : riskData});
         }

         if (summaryData.clientrefrdservices) {
            const clientrefrdservicesdata = this.setRiskDropDown(summaryData.clientrefrdservices);
            this.ARCaseSummaryForm.patchValue({clientrefrdservices : clientrefrdservicesdata});
        }


        setTimeout(()=>{
            if(summaryData.disableArSummary && this.isCaseWorker){
                this.apButton.nativeElement.disabled = true;
                this._renderer.addClass(this.apButton.nativeElement, 'disabled');
                this._renderer.addClass(this.apButton.nativeElement, 'btn');
                this._renderer.removeClass(this.apButton.nativeElement, 'btn-pri');
                if(summaryData.routingstatustypeid){
                    if(summaryData.routingstatustypeid === 15){
                        this.arStatus = 'Submitted';
                     }else if(summaryData.routingstatustypeid === 16){
                        this.arStatus = 'Approved';

                    } else if(summaryData.routingstatustypeid === 17){
                       this.arStatus = 'Rejected';
                    }

                }


            }


        },0)

        setTimeout(()=>{

            if(summaryData.disableArSummary && !this.isCaseWorker){

                if(summaryData.routingstatustypeid){
                    if(summaryData.routingstatustypeid === 15){
                        this.arStatus = 'Submitted';
                     }else if(summaryData.routingstatustypeid === 16){
                        this.apButton.nativeElement.disabled = true;
                        this.rjButton.nativeElement.disabled = true;
                        this.arStatus = 'Approved';
                        this._renderer.addClass(this.apButton.nativeElement, 'disabled');
                        this._renderer.addClass(this.apButton.nativeElement, 'btn');
                        this._renderer.removeClass(this.apButton.nativeElement, 'btn-pri');
                        this._renderer.addClass(this.rjButton.nativeElement, 'disabled');
                        this._renderer.addClass(this.rjButton.nativeElement, 'btn');
                        this._renderer.removeClass(this.rjButton.nativeElement, 'btn-pri');
                    } else if(summaryData.routingstatustypeid === 17){
                       this.arStatus = 'Rejected';
                       this.apButton.nativeElement.disabled = true;
                        this.rjButton.nativeElement.disabled = true;
                        this._renderer.addClass(this.apButton.nativeElement, 'disabled');
                        this._renderer.addClass(this.apButton.nativeElement, 'btn');
                        this._renderer.removeClass(this.apButton.nativeElement, 'btn-pri');
                        this._renderer.addClass(this.rjButton.nativeElement, 'disabled');
                        this._renderer.addClass(this.rjButton.nativeElement, 'btn');
                        this._renderer.removeClass(this.rjButton.nativeElement, 'btn-pri');
                    }

                }


            }

        },0)



        //  const closuresubstypearry = item[0].closuresubtypekey.split(",");
        //  const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
        //  this.closureSubTypeItems.map(function(closuresubtype){
        //      if(item[0].closuresubtypekey.indexOf(closuresubtype.value)){
        //         status.push(new FormControl(false));
        //      }else{
        //         status.push(new FormControl(false));
        //      }
        //  });



        //  this.closureSubTypeItems.map(closuresubtype => {

        //     if(item[0].closuresubtypekey.indexOf(closuresubtype.value)){
        //        status.push(new FormControl(false));
        //     }else{
        //        status.push(new FormControl(false));
        //     }
        // });

                const status = <FormArray>this.ARCaseSummaryForm.controls['statusList'];
                this.closureSubTypeItems.map(function (closuresubtype, index) {
                    let resp = summaryData.closuresubtypekey;
                    if (resp === null || resp === undefined) {
                        resp = '';
                    }
                    const temp = resp.split(',');
                    temp.map(function (sub, subindex) {
                        if (closuresubtype.value === sub) {
                            status.push(new FormControl(true));
                        }
                    });
                    if (status.length === index) {
                        status.push(new FormControl(false));
                    }
                });

                // if (item[0].closuretypekey) {
                //     this.onChangeClosureStatus(item[0].closuretypekey);
                // }
                //  const ARCaseForData = this.ARCaseSummaryForm.getRawValue();
                //  this.ARAssessmentClosureDate = ARCaseForData.ARAssessmentClosureDate;
                //  this.selectedChild = ARCaseForData.selectedChildren;
                //  this.selectedParticipant = ARCaseForData.selectedParticipants;
            }
        });

      }

      private loadSupervisor() {
        this.selectedSupervisor = '';
        let appEvent = 'INTR';
        this._commonHttpService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorsList = result.data;
               /* this.supervisorsList = this.supervisorsList.filter(
                    res => res.agencykey ===
                ); */
                    // (<any>$('#list-supervisor')).modal('show');
            });
            if (this.reviewCheckList && this.reviewCheckList.length > 0) {
                const checkList = this.conditionCheckList();
                (<any>$('#checklist-investigation-findings-review')).modal('hide');
                if (checkList) {
                    this.loadStatuses();
                    (<any>$('#intake-caseassign')).modal('show');
                    // (<any>$('#list-supervisor')).modal('show');
                }
            } else {
                (<any>$('#checklist-investigation-findings-review')).modal('hide');
            }
    }
    onChangeSupervisor(supervisor: RoutingUser) {
        console.log(supervisor);
        this.selectedSupervisor = supervisor.userid;
    }

    saveDisposition() {
        const dispositionModal = new DispositionAddModal();
        dispositionModal.disposition = Object.assign({
            intakeserviceid: this.id,
            intakeserreqstatustypeid: this.dispositionFormGroup.getRawValue().statusid.split('~')[1],
            dispostionid: this.dispositionFormGroup.value.dispositionid,
            reviewcomments: this.dispositionFormGroup.value.reviewcomments,
            closingcodetypekey: this.dispositionFormGroup.value.closingcodetypekey,
            supervisorid: this.dispositionFormGroup.getRawValue().tosecurityuserid,
            dateseen: new Date()
        });
        dispositionModal.investigation = {
            summary: '',
            filelocdesc: null,
            appevent: 'INVR'
        };
        this._dispositionAddService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.DispositionAddUrl;
        this._dispositionAddService.create(dispositionModal).subscribe(
            (response) => {
                this.dispositionFormGroup.reset();
                (<any>$('#intake-caseassign')).modal('hide');
                this._alertService.success('Disposition updated successfully');
                Observable.timer(2000).subscribe(() => {
                    this._router.routeReuseStrategy.shouldReuseRoute = function () {
                        return false;
                    };
                    const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition';
                    this._router.navigateByUrl(currentUrl).then(() => {
                        this._router.navigated = false;
                        this._router.navigate([currentUrl]);
                    });
                });
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    private loadStatuses() {
        this.statusDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        intakeservreqtypeid: this.daType,
                        servicerequestsubtypeid: this.daType
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.StatusUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.intakeserreqstatustypekey + '~' + res.intakeserreqstatustypeid
                        })
                );
            });
            this.statusDropdownItems$.subscribe( data => {
                const completeStatus = data.find(item => item.text === 'Completed');
                this.dispositionFormGroup.patchValue({
                    statusid : completeStatus.value
                });
                this.loadDispositon(completeStatus.value);
                this.dispositionFormGroup.get('statusid').disable();
            });
    }
    loadDispositon(statusId) {
        const statusKey = statusId.split('~')[0];
        this.dispositionDropdownItems$ = this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: {
                        statuskey: statusKey,
                        intakeservreqtypeid: this.daType,
                        servicerequestsubtypeid: this.daType
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Disposition.DispositionUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.servicerequesttypeconfigiddispostionid
                        })
                );
            });
        this.dispositionDropdownItems$.subscribe(data => {
            this.dispositionDropdownItems = data;
            if (data && data.length) {
                const closuerDispositon = data.find(item => item.text === 'Recommend for closure');
                if (closuerDispositon) {
                    this.dispositionFormGroup.patchValue({
                        dispositionid : closuerDispositon.value
                    });
                }
                }
        });
    }
    confirmDisposition() {
        const disposition = this.dispositionDropdownItems.find(item => item.value === this.dispositionFormGroup.value.dispositionid);
        this.saveDisposition();

    }
    cancelConfirmDisposition() {
        (<any>$('#status-disposition')).modal('show');
    }

    closeCase() {
        const intakeserreqstatustypeid = this.dispositionFormGroup.getRawValue().statusid.split('~')[1];
        const dispostionid = this.dispositionFormGroup.getRawValue().dispositionid;
        const closeCase = {
            intakeserreqstatustypeid : intakeserreqstatustypeid,
            dispostionid: dispostionid,
            intakeserviceid : this.id,
            caseclosuresummaryid: this.caseclosuresummaryid
        };
        this._commonHttpService
            .getArrayList(
                new PaginationRequest({
                    nolimit: true,
                    where: closeCase,
                    method: 'get'
                }), 'caseclosuresummary/updatedisposition?filter'
            ).subscribe(result => {
                this._alertService.success('Investigation closed successfully!', true);
                const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/disposition';
                this._router.navigate([currentUrl]);
            });
       /*  this._commonHttpService.create(closeCase, 'caseclosuresummary/updatedisposition').subscribe(
            (result) => {
                this._alertService.success('Investigation closed successfully!');
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        ); */
    }
}
