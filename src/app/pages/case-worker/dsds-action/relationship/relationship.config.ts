export class Relationslist {
    public static grandparentsrelationlist = ['Maternal Grandparent', 'Maternal Great Grandparent',
        'Parental Grandparent', 'Parental Great Grandparent', 'Step-Maternal Grandparent', 'Step-Parental Grandparent'];

    public static familyrelationList = ['Biological Father', 'Biological Mother', 'Boyfriend', 'Brother-in-law', 'Child care centre employee',
        'Child care home provider', 'Child care worker', 'Child Therapeutic Foster', 'Co-Worker', 'Custodian(Legal)', 'Father-in-law',
        'Father Therapeutic Foster', 'Foster Father', 'Foster Mother', 'Foster Parent', 'Friend', 'Girlfriend', 'Girlfriend-EX',
        'GodFather', 'GodMother', 'Guardian(Legal)', 'Husband', 'Husband-Ex', 'Legal Father', 'Legal Mother',
        'Maternal Aunt', 'Maternal Great Uncle', 'Maternal Uncle', 'Mother Therapeutic Foster', 'Neighbor',
        'Non-Custodial parent', 'No Relation', 'Other', 'Parent', 'Parental Aunt', 'Parental Cousin',
        'Parental Great Aunt', 'Parental Great Uncle', 'Parental Uncle', 'Resident', 'Residential Facility Staff',
        'School Counselor', 'Significant Other/Partner', 'Sister-in-law', 'Social Worker', 'Spouse', 'Spouse- Ex',
        'Step Brother', 'Step Father', 'Step Mother', 'Support Staff', 'Teacher', 'Unknown Father', 'Unknown Mother',
        'Wife', 'Wife-Ex'];

    public static childrelationshipList = ['Biological Brother', 'Biological Child', 'Biological Sister',
        'Daycare Child', 'Foster Child', 'Half Brother', 'Half Sister', 'Legal Brother', 'Legal Child',
        'Legal Sister', 'Maternal Cousin', 'Maternal GrandChild', 'Maternal Great GrandChild', 'Maternal Nephew',
        'Maternal Niece', 'Maternal Step GrandChild', 'Parental GrandChild', 'Parental Great GrandChild',
        'Parental Nephew', 'Parental Niece', 'Parental Step GrandChild', 'Putative Child', 'Putative Father',
        'Putative Mother', 'Relative(Other)', 'Self', 'Step Child', 'Step Sister', 'Student', 'Child'];

    public gp = ['MTNLGGPRNT', 'PRNTLGPRNT', 'PRNTLGGPRNT', 'STMATRNLGPRNT', 'STPRNTLGPRNT', 'MATRNLGPRNT'];
    public fm = ['MTNLGUE', 'MATNLUE', 'MTHRILW', 'PRNTLGAT', 'PRNTLGUE', 'STMTHR', 'BGFTHR', 'BGMTHR', 'BFRND', 'BFRND',
        'MTHRTHF', 'NBHR', 'NCUSPRNT', 'NORLTN', 'OTHER', 'PRNT', 'PRNTLAT', 'PRNTLCN', 'BROINLW', 'CCCE', 'CCHP', 'CCW',
        'CHTHF', 'COWR', 'CUSLG', 'FTHRINLW', 'FTHRRHF', 'FSTRFTHR', 'FSTRMTHR', 'fosterparent', 'FRND', 'GFRND', 'GFRNDX',
        'PRNTLUE', 'RESDNT', 'RESFS', 'SHLCR', 'SNFOPAR', 'SISTRINLW', 'SOWORKR', 'spouse', 'SPSEEX', 'STPBR', 'STPFTHR', 'SUPRTSTF',
        'TECHR', 'UKNFTHR', 'UKMTHR', 'WIFE', 'WIFEX', 'GDFTHR', 'GDMTHR', 'GDNLGL', 'HSBND', 'HSBNDX', 'LGLFTHR', 'LGLMTHR', 'MTNLAT', 'MTNLGA'
    ];
    public cld = ['MATNLNPW', 'MATNLNC', 'MTRLSGCD', 'PRNTLGCHLD', 'PRNTLGGCHLD', 'PRNTLNPW', 'PRNTLNC', 'PRNTLSGCD',
        'PUTCHLD', 'PUTFATHR', 'PTMTHR', 'RELOTHR', 'SELF', 'stepchild', 'STPSISTR', 'STUDNT', 'BIOBR',
        'BGCHLD', 'BGSISTR', 'DACRCHLD', 'fosterchild', 'HLFBR', 'HLFSISTR', 'LEGLBR', 'LGLCHLD', 'LGLSISTR',
        'MTNLCN', 'MRTNLGCHLD', 'MTNLGGCHLD'
    ];
}
