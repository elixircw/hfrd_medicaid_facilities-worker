import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArCaseClosureRoutingModule } from './ar-case-closure-routing.module';
import { ArCaseClosureComponent } from './ar-case-closure.component';
import { MatSelectModule, MatCheckboxModule, MatDatepickerModule, MatRadioModule, MatNativeDateModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        ArCaseClosureRoutingModule,
        MatSelectModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatNativeDateModule,
        MatRadioModule,
        ReactiveFormsModule
        ],
    declarations: [ArCaseClosureComponent]
})
export class ArCaseClosureModule {}
