import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Rx';

import { DropdownModel } from '../../../../@core/entities/common.entities';
import { CommonHttpService, DataStoreService } from '../../../../@core/services';
import { InvolvedPerson, Participant } from '../../_entities/caseworker.data.model';
import { AlertService } from './../../../../@core/services/alert.service';
import { Task } from './../../_entities/caseworker.data.model';
import { DropdownList } from './../involved-person/_entities/involvedperson.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'ar-case-closure',
    templateUrl: './ar-case-closure.component.html',
    styleUrls: ['./ar-case-closure.component.scss']
})
export class ArCaseClosureComponent implements OnInit {
    closerFormGroup: FormGroup;
    id: string;
    daNumber: string;
    isLDSS: boolean;
    isCONS: boolean;
    childs = [];
    individual = [];
    childrenParticipateItems$: Observable<InvolvedPerson[]>;
    individualParticipateItems$: Observable<InvolvedPerson[]>;
    closerTypeDropdownItems$: Observable<DropdownList[]>;
    validateResponseList: Task[] = [];
    communityServiceItems$: Observable<DropdownList[]>;
    childrenParticipateList: Participant[] = [];
    individualParticipate: Participant[] = [];
    isInvolvedPerson: boolean;
    discloserList = [];
    serviceNInter: { text: string; value: string; }[];
    constructor(private _formBuilder: FormBuilder, private route: ActivatedRoute, private _commonHttpService: CommonHttpService, private _alertService: AlertService,
        private _dataStoreService: DataStoreService) { }

    ngOnInit() {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);;
        this.closureType();
        this.validateCaseclosure();
        this.initializeFormGroup();
        this.arCaseCloserList();
        this.getInvolvedPerson();
        this.getServiceNInter();
    }
    initializeFormGroup() {
        this.closerFormGroup = this._formBuilder.group({
            reason: [''],
            individualparticipants: ['', Validators.required],
            childparticipate: ['', Validators.required],
            riskissues: [''],
            referralreason: [''],
            interventionissues: [''],
            recommendation: [''],
            closuretypekey: ['', Validators.required],
            closuresubtypekey: [null]
        });
    }
    arCaseCloserList() {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: { intakeserviceid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ArCaseClosure.CaseClosureListUrl + '?filter'
            )
            .subscribe((res) => {
                if (res) {
                    this.discloserList = res;
                    res.map((item) => {
                        this.changeClosureStatus(item.closuretypekey);
                        this.closerFormGroup.patchValue(item);
                        this.closerFormGroup.disable();
                        if (item && item.individualparticipant) {
                            this.individual.push({
                                firstname: item.individualparticipant.displayname ? item.individualparticipant.displayname.split(' ')[0] : '',
                                lastname: item.individualparticipant.displayname ? item.individualparticipant.displayname.split(' ')[1] : '',
                                roles: item.individualparticipant.roles
                            });
                        }
                        if (item && item.childparticipant) {
                            this.childs.push({
                                firstname: item.childparticipant.displayname ? item.childparticipant.displayname.split(' ')[0] : '',
                                lastname: item.childparticipant.displayname ? item.childparticipant.displayname.split(' ')[1] : '',
                                roles: item.childparticipant.roles ? item.childparticipant.roles : []
                            });
                        }
                    });
                }
            });
    }
    personChange(model, type) {
        if (type === 'child') {
            this.childs = model.length === 0 ? [] : model;
            this.childrenParticipateList = [];
            if (model && model.length) {
                model.map((res) => {
                    if (res.roles) {
                        res.roles.map((role) => {
                            this.childrenParticipateList.push({
                                intakeservicerequestactorid: role.intakeservicerequestactorid,
                                ischild: 1
                            });
                        });
                    }
                });
            }
        }
        if (type === 'individual') {
            this.individual = model.length === 0 ? [] : model;
            this.individualParticipate = [];
            if (model && model.length) {
                model.map((res) => {
                    if (res.roles) {
                        res.roles.map((role) => {
                            this.individualParticipate.push({
                                intakeservicerequestactorid: role.intakeservicerequestactorid,
                                ischild: 0
                            });
                        });
                    }
                });
            }
        }
    }
    changeClosureStatus(statustypekey: string) {
        this.isLDSS = statustypekey === 'LDSS' ? true : false;
        this.isCONS = statustypekey === 'CONS' ? true : false;
        this.communityServiceItems$ = this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        closuretypekey: statustypekey
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ArCaseClosure.CaseClosureServiceSubTypeUrl + '?filter'
            )
            .map((result) => {
                return result.map((item) => {
                    return new DropdownModel({
                        text: item.typedescription,
                        value: item.closuresubtypekey
                    });
                });
            });
    }
    submitCaseCloser(model) {
        if (this.validateTask(this.validateResponseList)) {
            if (this.discloserList.length < 1) {
                const caseCloserDetails = Object.assign({
                    intakeserviceid: this.id,
                    reason: model.reason,
                    referralreason: model.referralreason,
                    riskissues: model.riskissues,
                    recommendation: model.recommendation,
                    interventionissues: model.interventionissues,
                    closuretypekey: model.closuretypekey,
                    closuresubtypekey: model.closuresubtypekey,
                    notes: null,
                    participants: this.individualParticipate.concat(this.childrenParticipateList)
                });
                this._commonHttpService.create(caseCloserDetails, CaseWorkerUrlConfig.EndPoint.DSDSAction.ArCaseClosure.CaseClosureStatusAddUpdateUrl).subscribe((res) => {
                    this._alertService.success('Ar case closure status submitted successfully');
                    this.closerFormGroup.reset();
                    this.arCaseCloserList();
                    this.clearPersonList();
                });
            } else {
                this._alertService.warn('Ar case closure status submitted already exists ');
            }
        } else {
            this._alertService.warn('Please completed the check list');
        }
    }
    clearPersonList() {
        this.childs = [];
        this.individual = [];
    }
    private closureType() {
        this.closerTypeDropdownItems$ = this._commonHttpService.getArrayList({}, CaseWorkerUrlConfig.EndPoint.DSDSAction.ArCaseClosure.CaseClosureServiceTypeUrl).map((result) => {
            return result.map((item) => {
                return new DropdownModel({
                    text: item.typedescription,
                    value: item.closuretypekey
                });
            });
        });
    }

    getServiceNInter() {
        this.serviceNInter = [
            { text: 'Burial', value: 'Burial' },
            { text: 'Child Care', value: 'Child Care' },
            { text: 'Clothing & Hardware', value: 'Clothing & Hardware' },
            { text: 'Counselling', value: 'Counselling' },
            { text: 'Educational Services', value: 'Educational Services' },
            { text: 'Employment Services', value: 'Employment Services' },
            { text: 'Housing Assistance', value: 'Housing Assistance' },
            { text: 'Legal Services', value: 'Legal Services' },
            { text: 'Medical Services', value: 'Medical Services' },
            { text: 'Mental Health Services', value: 'Mental Health Services' },
            { text: 'Referral', value: 'Referral' },
            { text: 'Respite Care', value: 'Respite Care' },
            { text: 'Special Services', value: 'Special Services' },
            { text: 'Stipend', value: 'Stipend' },
            { text: 'Substance - Abuse Services', value: 'Substance - Abuse Services' },
            { text: 'Transportation', value: 'Transportation' },
            { text: 'Basic Living Skill', value: 'Basic Living Skill' }
        ];
    }
    private validateCaseclosure() {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: { intakeserviceid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ArCaseClosure.SummaryValidationUrl + '?filter'
            )
            .subscribe((res) => {
                res.map((item) => {
                    const validationResponse = {
                        task: item.task ? item.task : [],
                        assessment: item.assessment ? item.assessment : []
                    };
                    this.validateResponseList = validationResponse.assessment.concat(validationResponse.task);
                    if (this.validateResponseList.length !== 0) {
                        (<any>$('#checklist')).modal('show');
                    }
                });
            });
    }
    validateTask(taskData): boolean {
        const validateStatus = taskData.filter((item) => item.status === 'Open');
        if (validateStatus.length) {
            return false;
        } else {
            return true;
        }
    }
    private getAge(dateValue) {
        if (dateValue && moment(new Date(dateValue), 'MM/DD/YYYY', true).isValid()) {
            const rCDob = moment(new Date(dateValue), 'MM/DD/YYYY').toDate();
            return moment().diff(rCDob, 'years');
        } else {
            return '';
        }
    }
    private getInvolvedPerson() {
        const source = this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: { intakeserviceid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.ArCaseClosure.InvolvedPersonListUrl + '?filter'
            )
            .map((res) => {
                return {
                    childrenParticipates: res['data'].filter((item) => this.getAge(item.dob) <= 18),
                    individualParticipates: res['data'].filter((item) => this.getAge(item.dob) > 18)
                };
            })
            .share();
        this.childrenParticipateItems$ = source.pluck('childrenParticipates');
        this.individualParticipateItems$ = source.pluck('individualParticipates');
    }
}
