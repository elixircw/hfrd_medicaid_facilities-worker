import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArCaseClosureComponent } from './ar-case-closure.component';

describe('ArCaseClosureComponent', () => {
  let component: ArCaseClosureComponent;
  let fixture: ComponentFixture<ArCaseClosureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArCaseClosureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArCaseClosureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
