import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ArCaseClosureComponent } from './ar-case-closure.component';

const routes: Routes = [
    {
        path: '',
        component: ArCaseClosureComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ArCaseClosureRoutingModule {}
