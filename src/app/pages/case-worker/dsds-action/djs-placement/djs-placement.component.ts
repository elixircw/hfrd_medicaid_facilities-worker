import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';

import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonHttpService } from '../../../../@core/services';
import { AlertService } from '../../../../@core/services/alert.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import { InvolvedPerson } from '../involved-persons/_entities/involvedperson.data.model';
import { Placement, SearchPlan, SearchPlanRes } from '../service-plan/_entities/service-plan.model';
import { PlacementPlanConfig } from '../placement/_entities/placement.model';
import { AuthService } from '../../../../@core/services/auth.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import value from '*.json';

declare var $: any;


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'djs-placement',
    templateUrl: './djs-placement.component.html',
    styleUrls: ['./djs-placement.component.scss']
})
export class DjsPlacementComponent implements OnInit {
    id: string;
    caseTypes$: Observable<DropdownModel>;
    categoryTypes$: Observable<DropdownModel>;
    categorySubTypes$: Observable<DropdownModel[]>;
    paymentTypes$: Observable<DropdownModel>;
    exitTypes$: Observable<DropdownModel>;
    courtApproved$: Observable<DropdownModel>;
    admissionClassification$: Observable<DropdownModel>;
    admissionType$: Observable<DropdownModel>;
    admissionAuthorization$: Observable<DropdownModel>;
    admissionReason$: Observable<DropdownModel>;
    searchPlacementForm: FormGroup;
    addPlacementPlanForm: FormGroup;
    addPlacementForm: FormGroup;
    PlacementPlanFormExit: FormGroup;
    placementForm: FormGroup;
    reportedChildDob: string;
    involevedPerson$: Observable<InvolvedPerson[]>;
    SearchPlacement$: Observable<SearchPlan[]>;
    SearchPlacementRes$: Observable<SearchPlanRes>;
    placement$: Observable<Placement[]>;
    placementCount$: Observable<number>;
    countyDropdown$: Observable<DropdownModel[]>;
    childCharacteristicDropdown$: Observable<DropdownModel[]>;
    childDropdown = [];
    placementAdd: any;
    providerId: string;
    placementid: string;
    intakeservicerequestactorid: string;
    nextDisabled = true;
    startdatetime: string;
    enddatetime: string;
    Exitdatetime: string;
    Exittime: string;
    exitplacementid: string;
    markersLocation = ([] = []);
    zoom: number;
    defaultLat: number;
    defaultLng: number;
    maxDate = new Date();
    startDate: Date;
    isAddPlacement = false;
    providerInfo: { 'providerId': number; 'name': string; };
    paginationInfo: PaginationInfo = new PaginationInfo();
    isEnablePlacement: Placement[];
    selectedProvider: any;
    userInfo: AppUser;
    showCop = false;
    involvedYouth: any;
    reportMode: string;
    constructor(
        private _httpService: CommonHttpService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _servicePlan: GenericService<PlacementPlanConfig>,
        private _alertService: AlertService,
        private _authService: AuthService,
        private router: Router
    ) { }

    panelconfig = {
        panels: [
            {
                id: 1,
                name: 'Profile',
                selector: 'profile-info'
            },
            {
                id: 2,
                name: 'Contract',
                selector: 'contract-info'
            },
            {
                id: 3,
                name: 'Additional Information',
                selector: 'additional-info'
            }
        ]
    };

    ngOnInit() {
        // this.reportMode = 'add';
        // this.userInfo = this._authService.getCurrentUser();
        // console.log('userInfo', this.userInfo);
        // this.id = this.this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        // this.searchPlacementForm = this._formBuilder.group({
        //     jurisdiction: [null],
        //     zipcode: [null],
        //     serviceType: [null],
        //     categoryType: [null],
        //     categorySubType: [null],
        //     paymentType: [null],
        //     vendor: [null],
        //     providerTaxId: [null],
        //     distance: [null]
        // });

        // this.addPlacementPlanForm = this._formBuilder.group({
        //     startdatetime: [null, Validators.required],
        //     enddatetime: [null, Validators.required],
        //     remarks: ['', Validators.required]
        // });

        // this.addPlacementForm = this._formBuilder.group({
        //     providername: [null],
        //     providerid: [null],
        //     providerunit: [null],
        //     placementadmissionclassificationkey: [null],
        //     PlacementAdmissionTypekey: [null],
        //     parentorg: [null],
        //     addate: [null],
        //     adtime: [null],
        //     releasedate: [null, Validators.required],
        //     detainer: [null],
        //     PlacementAdmissionAuthorizationTypekey: [null],
        //     PlacementPrimaryAdmissionReasonTypekey: [null],
        //     PlacementPrimaryApprovedAltTypekey: [null],
        //     county: [null],
        //     jlocation: [null],
        //     jcounty: [null],
        //     fieldworker: [null],
        //     cop: [null],
        //     certifiedAd: [null],
        //     resourceworker: [null]
        // });

        // this.PlacementPlanFormExit = this._formBuilder.group({
        //     enddatetime: [null, Validators.required],
        //     exittime: [null, Validators.required],
        //     exitremarks: ['', Validators.required]
        // });
        // this.placementForm = this._formBuilder.group({
        //     choosechild: ['']
        // });
        // this.getDefaults();
        // this.getPlacement(1);
        // this.getInvolvedPerson();
        // this.getCountyDropdown();
        // this.childCharacteristic();
        // this.loadDropdown();
    }

    loadDropdown() {
        const source = Observable.forkJoin([this.getCourtApproved(), this.getAdmissionClassification(), this.getAdmissionType(), this.getAdmissionAuthorization(), this.getPrimaryAdmissionReason()])
            .map((item) => {
                return {
                    courtApproved: item[0].map((res) => {
                        return new DropdownModel({
                            text: res.description,
                            value: res.placementprimaryapprovedalttypekey
                        });
                    }),
                    admissionClassification: item[1].map((itm) => {
                        return new DropdownModel({
                            text: itm.description,
                            value: itm.placementadmissionclassificationkey
                        });
                    }),
                    admissionType: item[2].map((itm) => {
                        return new DropdownModel({
                            text: itm.description,
                            value: itm.placementadmissiontypekey
                        });
                    }),
                    admissionAuthorization: item[3].map((itm) => {
                        return new DropdownModel({
                            text: itm.description,
                            value: itm.placementadmissionauthorizationtypekey
                        });
                    }),
                    admissionReason: item[4].map((itm) => {
                        return new DropdownModel({
                            text: itm.description,
                            value: itm.placementprimaryadmissionreasontypekey
                        });
                    })
                };
            })
            .share();
        this.courtApproved$ = source.pluck('courtApproved');
        this.admissionClassification$ = source.pluck('admissionClassification');
        this.admissionType$ = source.pluck('admissionType');
        this.admissionAuthorization$ = source.pluck('admissionAuthorization');
        this.admissionReason$ = source.pluck('admissionReason');
    }

    getProvider(providerInformation) {
        console.log('Get Provider' + providerInformation);
        this.providerInfo = providerInformation;
    }

    countyPatchValue(event) {
        if (event) {
            this.countyDropdown$.subscribe((data) => {
                data.map((list) => {
                    if (list.value === event.value) {
                        this.addPlacementForm.patchValue({
                            jlocation: 'Head Quarters',
                            jcounty: list.text
                        });
                    }
                });
            });
        }
    }

    showCOP(event) {
        if (event.checked) {
            this.showCop = true;
            this.addPlacementForm.get('certifiedAd').setValidators([Validators.required]);
            this.addPlacementForm.get('resourceworker').setValidators([Validators.required]);
            this.addPlacementForm.updateValueAndValidity();
        } else {
            this.showCop = false;
            this.addPlacementForm.get('certifiedAd').clearValidators();
            this.addPlacementForm.get('resourceworker').clearValidators();
            this.addPlacementForm.updateValueAndValidity();
        }
    }

    showCOPs(chevalue) {
        if (chevalue) {
            this.showCop = true;
            this.addPlacementForm.get('certifiedAd').setValidators([Validators.required]);
            this.addPlacementForm.get('resourceworker').setValidators([Validators.required]);
            this.addPlacementForm.updateValueAndValidity();
        } else {
            this.showCop = false;
            this.addPlacementForm.get('certifiedAd').clearValidators();
            this.addPlacementForm.get('resourceworker').clearValidators();
            this.addPlacementForm.updateValueAndValidity();
        }
    }

    getInvolvedPerson() {
        this.involevedPerson$ = this._httpService
            .getArrayList(
                {
                    method: 'get',
                    where: { intakeservreqid: this.id }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .InvolvedPersonListUrl +
                '?data'
            )
            .map((res) => {
                return res['data'].filter((item) => item.rolename === 'Youth');
            });

        this.involevedPerson$.subscribe((data) => {
            this.involvedYouth = data[0];
        });
    }
    pesonDateOfBirth(item) {
        this.reportedChildDob = item.value.dob;
        this.intakeservicerequestactorid =
            item.value.intakeservicerequestactorid;
    }
    searchClearPlacement() {
        // this.SearchPlacement$ = Observable.empty();
        // this.nextDisabled = true;
        // this.searchPlacementForm.reset();
        // this.childDropdown = [];
        // this.searchPlacementForm.patchValue({ serviceType: '' });
        this.router.navigate(['provider-search'], { relativeTo: this.route });
    }
    getPlacement(page: number) {
        const source = this._httpService
            .getPagedArrayList(
                new PaginationRequest({
                    page: page,
                    limit: this.paginationInfo.pageSize,
                    where: {
                        intakeserviceid: this.id
                    },
                    method: 'get'
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementListUrl + '?filter'
            )
            .map((res) => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.placement$ = source.pluck('data');
        if (page === 1) {
            this.placementCount$ = source.pluck('count');
        }
        this.placement$.subscribe((data) => {
            data.map((list) => {
                if (data.length === 1 && !list.placeenddatetime) {
                    this.isAddPlacement = true;
                } else {
                    this.isAddPlacement = false;
                }
                if (data.length >= 2) {
                    if (!data[0].placeenddatetime) {
                        this.isAddPlacement = true;
                    } else {
                        this.isAddPlacement = false;
                    }
                }
            });
        });
    }
    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.placementForm.patchValue({ choosechild: '' });
        this.isAddPlacement = true;
        this.getPlacement(this.paginationInfo.pageNumber);
    }

    child(childvalues) {
        this.childDropdown = childvalues.value;
    }

    searchPlacement(model) {
        this.zoom = 11;
        this.defaultLat = 39.29044;
        this.defaultLng = -76.61233;

        const source = this._httpService
            .getPagedArrayList(
                {
                    where: {
                        servicetype: model.serviceType ? model.serviceType : null,
                        service: model.categoryType ? model.categoryType : null,
                        servicesubtype: model.categorySubType ? model.categorySubType : null,
                        county: model.jurisdiction ? model.jurisdiction : null,
                        zipcode: model.zipcode ? model.zipcode : null,
                        provider: model.vendor ? model.vendor : null,
                        providerTaxId: model.providerTaxId ? model.providerTaxId : null,
                        distance: model.distance ? model.distance : null,
                        pagenumber: null,
                        pagesize: null,
                        // from: 'Placement',
                        count: null,
                        childcharacteristic: this.childDropdown.length >= 1 ? this.childDropdown : null
                    },
                    method: 'post'
                },
                'provider/search'
            )
            .map((res) => {
                return {
                    data: res.data,
                    count: res.count
                };
            })
            .share();
        this.SearchPlacement$ = source.pluck('data');
        this.SearchPlacementRes$ = source.pluck('count');
        // this.childDropdown = [];
    }
    private getDefaults() {
        const source = Observable.forkJoin([this.getCaseType(), this.getCategoryType(), this.getPaymentType(), this.ExitReason()])
            .map((item) => {
                return {
                    caseTypes: item[0].map((res) => {
                        return new DropdownModel({
                            text: res.servicetypedescription,
                            value: res.servicetypekey
                        });
                    }),
                    categoryTypes: item[1].map((itm) => {
                        return new DropdownModel({
                            text: itm.description,
                            value: itm.serviceid
                        });
                    }),
                    paymentTypes: item[2].map((itm) => {
                        return new DropdownModel({
                            text: itm.providercontracttypename,
                            value: itm.providercontracttypekey
                        });
                    }),
                    exitTypes: item[3].map((itm) => {
                        return new DropdownModel({
                            text: itm.description,
                            value: itm.exitreasontypekey
                        });
                    })
                };
            })
            .share();
        this.caseTypes$ = source.pluck('caseTypes');
        this.categoryTypes$ = source.pluck('categoryTypes');
        this.paymentTypes$ = source.pluck('paymentTypes');
        this.exitTypes$ = source.pluck('exitTypes');
    }

    getCountyDropdown() {
        this.countyDropdown$ = this._httpService
            .getArrayList(
                {
                    where: {
                        activeflag: '1',
                        state: 'MD'
                    },
                    order: 'countyname asc',
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmCountyListUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.countyname,
                            value: res.countyid
                        })
                );
            });
    }

    private getCaseType() {
        return this._httpService.getArrayList({ where: {}, nolimit: true, method: 'get' }, 'servicetype/list?filter');
    }

    private getCategoryType() {
        return this._httpService.getArrayList(
            { where: { servicetypekey: 'PS' }, nolimit: true, method: 'get' },
            'Services?filter'
        );
    }

    childCharacteristic() {
        this.childCharacteristicDropdown$ = this._httpService
            .getArrayList(
                {
                    order: 'childcharacteristicdescription asc',
                    method: 'get',
                    nolimit: true
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.childcharacteristicUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.childcharacteristicdescription,
                            value: res.childcharacteristictypekey
                        })
                );
            });
    }

    getCategorySubType(categoryType: string) {
        this.categorySubTypes$ = this._httpService
            .getArrayList(
                {
                    method: 'get',
                    where: { serviceid: categoryType },
                    nolimit: true
                },
                'servicesubtype?filter'
            )
            .map((itm) => {
                return itm.map(
                    (res) =>
                        new DropdownModel({
                            text: res.servicesubtypedescription,
                            value: res.servicesubtypekey
                        })
                );
            });
    }

    private getPaymentType() {
        return this._httpService.getArrayList({ where: {}, nolimit: true, method: 'get' }, 'providercontracttype?filter');
    }

    private ExitReason() {
        return this._httpService.getAll(
            'exitreasontype/'
        );

    }

    private getCourtApproved() {
        return this._httpService.getAll('placement/primaryapproved');
    }

    private getAdmissionClassification() {
        return this._httpService.getAll('placement/admissionclassification');
    }

    private getAdmissionType() {
        return this._httpService.getAll('placement/admissiontype');
    }

    private getAdmissionAuthorization() {
        return this._httpService.getAll('placement/admissionauthorization');
    }

    private getPrimaryAdmissionReason() {
        return this._httpService.getAll('placement/admissionreason');
    }

    selectedProv(provId) {
        this.nextDisabled = false;
        this.selectedProvider = provId;
        this.providerId = provId.providerid;
        this.addPlacementForm.patchValue({
            providername: this.selectedProvider.providername,
            providerid: this.selectedProvider.providercode,
            providerunit: this.selectedProvider.providerunit,
            parentorg: this.selectedProvider.providerorganizationtype,
            placementadmissionclassificationkey: this.selectedProvider.placementadmissionclassificationkey,
            PlacementAdmissionTypekey: this.selectedProvider.placementadmissiontypekey,
            fieldworker: this.userInfo.user.userprofile.displayname
        });
    }

    clearForm() {
        this.addPlacementForm.reset();
        this.addPlacementForm.clearValidators();
    }

    placementSearchAdd(modal) {
        modal.providerid = this.providerId;
        modal.intakeserviceid = this.id;
        modal.remarks = 'djs placement';
        modal.addate = moment(modal.addate).format('YYYY-MM-DD').toString();
        modal.adtime = moment.utc(modal.adtime).format('HH:mm').toString();
        if (modal.adtime) {
            const timeSplit = modal.adtime.split(':');
            if (!(modal.addate instanceof Date)) {
                modal.addate = new Date(modal.addate);
            }
            modal.addate.setHours(timeSplit[0]);
            modal.addate.setMinutes(timeSplit[1]);
        }
        modal.releasedate = moment(modal.releasedate).format('YYYY-MM-DD').toString();
        this.placementAdd = Object.assign(modal);
        this._httpService.create(this.placementAdd, 'placement/addjs').subscribe(
            result => {
                this._alertService.success('Placement Addedd Successfully');
                this.addPlacementForm.reset();
                this.searchPlacementForm.reset();
                (<any>$('#select-placement')).modal('hide');
                // this.isAddPlacement = true;
                this.getPlacement(1);
                this.getInvolvedPerson();
                this.clearForm();
                this.reportedChildDob = null;
                this.showCop = false;
            }, error => {
                this._alertService.error('Unable to process request');
            });
    }

    placementUpdate(modal) {
        modal.placementid = this.placementid;
        modal.providerid = this.providerId;
        modal.intakeserviceid = this.id;
        modal.remarks = 'djs placement';
        modal.addate = moment(modal.addate).format('YYYY-MM-DD').toString();
        modal.adtime = moment.utc(modal.adtime).format('HH:mm').toString();
        modal.releasedate = moment(modal.releasedate).format('YYYY-MM-DD').toString();
        if (modal.adtime) {
            const timeSplit = modal.adtime.split(':');
            if (!(modal.addate instanceof Date)) {
                modal.addate = new Date(modal.addate);
            }
            modal.addate.setHours(timeSplit[0]);
            modal.addate.setMinutes(timeSplit[1]);
        }
        this.placementAdd = Object.assign(modal);
        this.placementAdd.placementadmissionclassificationkey = modal.placementadmissionclassificationkey;
        this.placementAdd.placementadmissiontypekey = modal.PlacementAdmissionTypekey;
        this.placementAdd.placementadmissionauthorizationtypekey = modal.PlacementAdmissionAuthorizationTypekey;
        this.placementAdd.placementprimaryadmissionreasontypekey = modal.PlacementPrimaryAdmissionReasonTypekey;
        this.placementAdd.placementprimaryapprovedalttypekey = modal.PlacementPrimaryApprovedAltTypekey;
        this.placementAdd.county = modal.county;
        this.placementAdd.certifiedad = modal.certifiedAd;
        this.placementAdd.jcounty = modal.county;
        this._httpService.create(this.placementAdd, 'placement/updateplacement').subscribe(result => {
            this._alertService.success('Placement Updated Successfully');
            this.addPlacementForm.reset();
            this.searchPlacementForm.reset();
            (<any>$('#select-placement')).modal('hide');
            // this.isAddPlacement = true;
            this.getPlacement(1);
            this.getInvolvedPerson();
            this.clearForm();
            this.reportedChildDob = null;
            this.showCop = false;
            this.reportMode = 'add';
        }, error => {
            this._alertService.error('Unable to process request');
        });
    }

    listMap() {
        this.zoom = 11;
        this.defaultLat = 39.29044;
        this.defaultLng = -76.61233;
        this.SearchPlacement$.subscribe((map) => {
            this.markersLocation = [];
            if (map.length > 0) {
                map.forEach((res) => {
                    const mapLocation = {
                        lat: +res.latitude,
                        lng: +res.longitude,
                        draggable: +true,
                        providername: res.providername,
                        addressline1: res.addressline1
                    };
                    this.markersLocation.push(mapLocation);
                    this.defaultLat = this.markersLocation[0].lat;
                    this.defaultLng = this.markersLocation[0].lng;
                });
                (<any>$('#iframe-popup')).modal('show');
            }
        });
    }
    mapClose() {
        this.markersLocation = [];
        // this.zoom = 11;
        // this.defaultLat = 39.219236;
        // this.defaultLng = -76.662834;
    }

    Exitbutton(placement) {
        this.PlacementPlanFormExit.reset();
        this.exitplacementid = placement.placementid;
        this.startDate = placement.placedatetime;
    }

    ExitSave(Exit) {
        this.Exitdatetime = moment(Exit.enddatetime).format('YYYY-MM-DD');
        this.Exittime = moment.utc(Exit.exittime).format('HH:mm');

        console.log(this.Exitdatetime + ' ' + this.Exittime);

        this._httpService
            .create(
                {
                    placementid: this.exitplacementid,
                    exitreasontypekey: Exit.exitremarks,
                    enddatetime: this.Exitdatetime + ' ' + this.Exittime
                },
                'placement/updateplacement'
            )
            .subscribe(
                (res) => {
                    (<any>$('#exit-popup')).modal('hide');
                    this._alertService.success('Placement Update successfully');
                    this.getPlacement(1);
                    this.getInvolvedPerson();
                    this.reportedChildDob = null;
                },
                (err) => { }
            );
    }

    SelectedForModify(model) {
        console.log(model);
        this.reportMode = 'update';
        this.selectedProvider = model;
        this.providerId = model.providerid;
        this.placementid = model.placementid;
        this.addPlacementForm.patchValue({
            providername: this.selectedProvider.providerinfo.providername,
            providerid: this.selectedProvider.providerinfo.providercode,
            providerunit: this.selectedProvider.providerunit,
            parentorg: this.selectedProvider.parentorg,
            addate: this.selectedProvider.addate,
            adtime: moment(this.selectedProvider.addate).format('HH:mm'),
            releasedate: this.selectedProvider.releasedate,
            PlacementAdmissionAuthorizationTypekey: this.selectedProvider.placementadmissionauthorizationtypekey,
            PlacementPrimaryAdmissionReasonTypekey: this.selectedProvider.placementprimaryadmissionreasontypekey,
            PlacementPrimaryApprovedAltTypekey: this.selectedProvider.placementprimaryapprovedalttypekey,
            county: this.selectedProvider.countyid,
            jlocation: this.selectedProvider.jlocation,
            jcounty: this.selectedProvider.county,
            cop: this.selectedProvider.certifiedad ? true : false,
            certifiedAd: this.selectedProvider.certifiedad,
            resourceworker: this.selectedProvider.resourceworker,
            placementadmissionclassificationkey: this.selectedProvider.placementadmissionclassificationtypekey,
            PlacementAdmissionTypekey: this.selectedProvider.placementadmissiontypekey,
            fieldworker: this.userInfo.user.userprofile.displayname,
            detainer: this.selectedProvider.detainer
        });
        this.showCOPs(this.selectedProvider.certifiedad ? true : false);
    }

}
