import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsProviderSearchComponent } from './djs-provider-search.component';

describe('DjsProviderSearchComponent', () => {
  let component: DjsProviderSearchComponent;
  let fixture: ComponentFixture<DjsProviderSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsProviderSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsProviderSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
