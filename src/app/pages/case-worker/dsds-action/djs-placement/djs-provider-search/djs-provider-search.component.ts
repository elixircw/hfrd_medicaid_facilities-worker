import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SearchPlan, SearchPlanRes } from '../../service-plan/_entities/service-plan.model';
import { DropdownModel, PaginationInfo, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, GenericService, AlertService, AuthService, DataStoreService } from '../../../../../@core/services';
import { PlacementPlanConfig } from '../../placement/_entities/placement.model';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DSDS_STORE_CONSTANTS } from '../../dsds-action.constants';
import { AppConstants } from '../../../../../@core/common/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';

declare var $: any;
const FOLDER_TYPE_COMMITED = 'Committed';
const FOLDER_TYPE_DETTENTION = 'Detention';
const FOLDER_TYPE_ATD = 'ATD';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'djs-provider-search',
  templateUrl: './djs-provider-search.component.html',
  styleUrls: ['./djs-provider-search.component.scss']
})
export class DjsProviderSearchComponent implements OnInit {
  id: string;
  searchPlacementForm: FormGroup;
  SearchPlacement$: Observable<SearchPlan[]>;
  countyDropdown$: Observable<DropdownModel[]>;
  SearchPlacementRes$: Observable<number>;
  providerInfo: { 'providerId': number; 'name': string; };
  nextDisabled = true;
  selectedProvider: any;
  providerId: string;
  userInfo: AppUser;
  paginationInfo: PaginationInfo = new PaginationInfo();
  folderTypeKey: string;
  resedentialType: string;
  needOperatedBy = false;
  transferProviderId: string;
  resedentialStatus = false;
  constructor(
    private _httpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private _authService: AuthService,
    private _dataStoreService: DataStoreService
  ) {
    this.route.queryParams.subscribe(result => {
      this.resedentialStatus = result['resedentialStatus'];
    });
   }

  panelconfig = {
    panels: [
      {
        id: 1,
        name: 'Profile',
        selector: 'profile-info'
      },
      {
        id: 2,
        name: 'Contract',
        selector: 'contract-info'
      },
      {
        id: 3,
        name: 'Additional Information',
        selector: 'additional-info'
      }
    ]
  };

  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.folderTypeKey = this._dataStoreService.getData('da_foldertypekey');
    // this.folderTypeKey = 'Commited';
    this.searchPlacementForm = this._formBuilder.group({
      jurisdiction: [null],
      zipcode: [null],
      serviceType: [null],
      categoryType: [null],
      categorySubType: [null],
      paymentType: [null],
      vendor: [null],
      providerTaxId: [null],
      distance: [null],
      isoperatedbydjs: [null]
    });
    this.getCountyDropdown();
    const searchForLeaveTransfer = this._dataStoreService.getData(DSDS_STORE_CONSTANTS.LEAVE_AND_TANSFER);
    const tempLeaveDate = this._dataStoreService.getData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA);
    if (searchForLeaveTransfer && tempLeaveDate) {
      const searchParams = this.searchPlacementForm.getRawValue();
      const folderTypeKey = this._dataStoreService.getData('da_foldertypekey');
      if (folderTypeKey === FOLDER_TYPE_COMMITED) {
        this.folderTypeKey = `${folderTypeKey},Detention`;
      } else {
        this.folderTypeKey = `Committed,${folderTypeKey}`;
      }
      searchParams.isoperatedbydjs = searchForLeaveTransfer.isoperatedbydjs ? searchForLeaveTransfer.isoperatedbydjs : false;
      this.searchPlacementForm.patchValue(searchParams);
      this.searchPlacementForm.get('isoperatedbydjs').disable();
      this.searchPlacement(this.searchPlacementForm.getRawValue(), 1);
      this.transferProviderId = tempLeaveDate.providerid;
    } else {
      if (this.folderTypeKey === FOLDER_TYPE_COMMITED || this.folderTypeKey === FOLDER_TYPE_DETTENTION) {
        if (this.folderTypeKey === FOLDER_TYPE_COMMITED) {
          this.searchPlacementForm.patchValue({ isoperatedbydjs: true });
          this.needOperatedBy = true;
        }
        this.searchPlacement(this.searchPlacementForm.getRawValue(), 1);
      } else {
        const prevSearch = this._dataStoreService.getData(DSDS_STORE_CONSTANTS.PROVIDER_SEARCH_MODEL);
        if (prevSearch) {
          this.searchPlacementForm.patchValue(prevSearch);
          this.searchPlacement(prevSearch, 1);
        }
      }
    }
    if (this._dataStoreService.getData('placement_intake')) {
      if (this.userInfo.role.name === AppConstants.ROLES.DJS_PLACEMENT_WORKER) {
        this.folderTypeKey = FOLDER_TYPE_DETTENTION;
      } else {
        this.folderTypeKey = FOLDER_TYPE_ATD;
      }
    }
  }

  getCountyDropdown() {
    this.countyDropdown$ = this._httpService
      .getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmCountyListUrl + '?filter'
      )
      .map((result) => {
        return result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyid
            })
        );
      });
  }

  otherPlacements(page: number) {
    // Need to call /api/foldertypeproviderconfig/listProviderByFoldertype? for only
    // commited and detention placement othre placement can be searched
    this.searchPlacement(this.searchPlacementForm.getRawValue(), 1);
  }

  searchPlacement(model, page: number) {
    this._dataStoreService.setData(DSDS_STORE_CONSTANTS.PROVIDER_SEARCH_MODEL, model);
    let folderTypeKey;
    if (this.folderTypeKey) {
      folderTypeKey = `{${this.folderTypeKey}}`;
    }

    const source = this._httpService
      .getPagedArrayList(
        new PaginationRequest({

          where: {
            servicetype: model.serviceType ? model.serviceType : null,
            service: model.categoryType ? model.categoryType : null,
            servicesubtype: model.categorySubType ? model.categorySubType : null,
            county: model.jurisdiction ? model.jurisdiction : null,
            zipcode: model.zipcode ? model.zipcode : null,
            provider: model.vendor ? model.vendor : null,
            providerTaxId: model.providerTaxId ? model.providerTaxId : null,
            distance: model.distance ? model.distance : null,
            foldertype: folderTypeKey,
            isoperatedbydjs: model.isoperatedbydjs,
            pagenumber: page,
            pagesize: this.paginationInfo.pageSize,
          },
          method: 'post'
        }),
        'provider/search'
      )
      .map((res) => {
        return {
          data: res.data,
          count: res.count
        };
      })
      .share();
    this.SearchPlacement$ = source.pluck('data');
    if (page === 1) {
      this.SearchPlacementRes$ = source.pluck('count');
    }
    // this.childDropdown = [];
  }

  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.searchPlacement(this.searchPlacementForm.getRawValue(), pageInfo.page);
  }

  searchClearPlacement() {
    this.searchPlacementForm.reset();
    this.SearchPlacement$ = Observable.empty();
    this.SearchPlacementRes$ = Observable.empty();
  }

  getProvider(providerInformation) {
    console.log('Get Provider' + providerInformation);
    this.providerInfo = providerInformation;
  }

  selectedProv(provId) {
    this.nextDisabled = false;
    this.selectedProvider = provId;
    this._dataStoreService.setData('SELECTED_PROVIDER', {
      providername: this.selectedProvider.providername,
      providerid: this.selectedProvider.providerid,
      providercode: this.selectedProvider.providercode,
      providerunit: this.selectedProvider.providerunit,
      parentorg: this.selectedProvider.providerorganizationtype,
      placementadmissionclassificationkey: this.selectedProvider.placementadmissionclassificationkey,
      PlacementAdmissionTypekey: this.selectedProvider.placementadmissiontypekey,
      fieldworker: this.userInfo.user.userprofile.displayname,
      placementadmissionclassificationdesc: this.selectedProvider.placementadmissionclassificationdesc,
      placementadmissiontypedesc: this.selectedProvider.placementadmissiontypedesc
    });
  }

  navigateToAddPlacement() {
    this.router.navigate(['../add'], { relativeTo: this.route });
  }

  ResidentialTypeChange(value) {
    this.resedentialType = value;
  }
}
