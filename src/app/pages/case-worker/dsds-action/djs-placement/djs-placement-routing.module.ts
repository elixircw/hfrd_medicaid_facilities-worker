import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DjsPlacementComponent } from './djs-placement.component';
import { DjsProviderSearchComponent } from './djs-provider-search/djs-provider-search.component';
import { DjsPlacementListComponent } from './djs-placement-list/djs-placement-list.component';
import { DjsPlacementAddEditComponent } from './djs-placement-add-edit/djs-placement-add-edit.component';
import { DjsPlacementService } from './djs-placement.service';
import { DjsPlacementReleaseComponent } from './djs-placement-release/djs-placement-release.component';
import { DjsPlacementLeavereturnComponent } from './djs-placement-leavereturn/djs-placement-leavereturn.component';

const routes: Routes = [{
  path: '',
  component: DjsPlacementComponent,
  children: [
    {
      path: 'provider-search',
      component: DjsProviderSearchComponent
    },
    {
      path: 'list',
      component: DjsPlacementListComponent
    },
    {
      path: 'release/:placementId',
      component: DjsPlacementReleaseComponent
    },
    {
      path: 'leavereturn/:placementId',
      component: DjsPlacementLeavereturnComponent
    },
    {
      path: 'view-leave/:placementId',
      component: DjsPlacementLeavereturnComponent
    },
    {
      path: ':operation',
      component: DjsPlacementAddEditComponent
    },
    {
      path: ':operation/:placementid',
      component: DjsPlacementAddEditComponent
    },
    {
      path: '**',
      redirectTo: 'list'
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [DjsPlacementService]
})
export class DjsPlacementRoutingModule { }
