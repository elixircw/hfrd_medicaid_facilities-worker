import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../../@core/services/data-store.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DSDS_STORE_CONSTANTS } from '../../dsds-action.constants';

@Component({
  selector: 'djs-placement-admissiondetails',
  templateUrl: './djs-placement-admissiondetails.component.html',
  styleUrls: ['./djs-placement-admissiondetails.component.scss']
})
export class DjsPlacementAdmissiondetailsComponent implements OnInit {

  involvedYouth: any;
  placementDetails: any;
  leaveTypeDesc: string;
  constructor(private _dataStoreService: DataStoreService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    if (this._dataStoreService.getData('PLACEMENT_PROVIDER') && this._dataStoreService.getData('PLACEMENT_INVOLVED_YOUTH')) {
      this.involvedYouth = this._dataStoreService.getData('PLACEMENT_INVOLVED_YOUTH');
      this.placementDetails = this._dataStoreService.getData('PLACEMENT_PROVIDER');
      if (this._dataStoreService.getData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA)) {
        const leaveModel = this._dataStoreService.getData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA);
        if (this.placementDetails.placementid === leaveModel.placementid) {
          this.leaveTypeDesc = leaveModel.leavetypekeydesc;
        }
      }
    } else {
      this.router.navigate(['../../list'], { relativeTo: this.route });
    }
  }

}
