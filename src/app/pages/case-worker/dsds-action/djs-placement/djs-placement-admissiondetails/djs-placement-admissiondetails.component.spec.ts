import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPlacementAdmissiondetailsComponent } from './djs-placement-admissiondetails.component';

describe('DjsPlacementAdmissiondetailsComponent', () => {
  let component: DjsPlacementAdmissiondetailsComponent;
  let fixture: ComponentFixture<DjsPlacementAdmissiondetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPlacementAdmissiondetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPlacementAdmissiondetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
