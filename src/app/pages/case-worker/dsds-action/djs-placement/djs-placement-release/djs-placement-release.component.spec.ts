import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPlacementReleaseComponent } from './djs-placement-release.component';

describe('DjsPlacementReleaseComponent', () => {
  let component: DjsPlacementReleaseComponent;
  let fixture: ComponentFixture<DjsPlacementReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPlacementReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPlacementReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
