import { Component, OnInit, Input, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService, CommonHttpService, DataStoreService, CommonDropdownsService } from '../../../../../@core/services';
import { DjsPlacementService } from '../djs-placement.service';
import * as moment from 'moment';

declare var $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'djs-placement-release',
  templateUrl: './djs-placement-release.component.html',
  styleUrls: ['./djs-placement-release.component.scss']
})
export class DjsPlacementReleaseComponent implements OnInit {

  releaseReason$: Observable<any>;
  Whereabouts$: Observable<any>;
  Whereaboutssub$: Observable<any>;
  releaseCategory$: Observable<any>;
  transfertoOtherFacility$: Observable<any>;
  cdUnSuccessful$: Observable<any>;
  erccdUnSuccessful$: Observable<any>;
  releaseReasonStatus$: Observable<any>;
  releaseReasonSub$: Observable<any>;
  releaseReasonUnderSub$: Observable<any>;
  PlacementPlanFormReason: FormGroup;
  startDate: Date = new Date();
  placementId: string;
  data: any;
  placement: any;
  minDate = new Date();

  constructor(private _formBuilder: FormBuilder,
    private _alertService: AlertService,
    private _placementService: DjsPlacementService,
    private _datastoreService: DataStoreService,
    private router: Router,
    private route: ActivatedRoute,
    private _dropDownService: CommonDropdownsService) { }

  ngOnInit() {
    this.PlacementPlanFormReason = this._formBuilder.group({
      releasedate: [null, Validators.required],
      releasetime: [null, Validators.required],
      releasetoname: ['', Validators.required],
      releasereason: [''],
      releasereasonstatus: [''],
      releasereasonsubstatus: [''],
      whereabouts: [''],
      wrstatus: [''],
      releasecategory: [''],
      releasecomments: ['']
    });
    this.placement = this._datastoreService.getData('PLACEMENT_PROVIDER');
    this.minDate = this.placement.addate;
    this.placementId = this.route.snapshot.paramMap.get('placementId');
    this.getDefaults();
  }

  private getDefaults() {
    // const source = this._placementService.getDefaultsList();
    this.releaseReason$ = this._dropDownService.getDropownsByTable('plcmtrlsrsn').map((data) => {
      return data.map((res) => {
        return new DropdownModel({
          text: res.description,
          value: res.ref_key
        });
      });
    });
    this.Whereabouts$ = this._dropDownService.getDropownsByTable('plcmttwhrabt').map((data) => {
      return data.map((res) => {
        return new DropdownModel({
          text: res.description,
          value: res.ref_key
        });
      });
    });
    this.releaseCategory$ = this._dropDownService.getDropownsByTable('plcmtrlscatgry').map((data) => {
      return data.map((res) => {
        return new DropdownModel({
          text: res.description,
          value: res.ref_key
        });
      });
    });
    this.transfertoOtherFacility$ = this._dropDownService.getDropownsByTable('plcmttothf').map((data) => {
      return data.map((res) => {
        return new DropdownModel({
          text: res.description,
          value: res.ref_key
        });
      });
    });
    this.releaseReasonStatus$ = this._dropDownService.getDropownsByTable('plcmtrlsrsnsts').map((data) => {
      return data.map((res) => {
        return new DropdownModel({
          text: res.description,
          value: res.ref_key
        });
      });
    });
    this.cdUnSuccessful$ = this._dropDownService.getDropownsByTable('plcmtunsucc').map((data) => {
      return data.filter((itm) => itm.ref_key !== 'NC').map((res) => {
        return new DropdownModel({
          text: res.description,
          value: res.ref_key
        });
      });
    });
    this.erccdUnSuccessful$ = this._dropDownService.getDropownsByTable('plcmtunsucc').map((data) => {
      return data.filter((itm) => itm.ref_key !== 'SC' && itm.ref_key !== 'FTA' && itm.ref_key !== 'CS')
        .map((res) => {
          return new DropdownModel({
            text: res.description,
            value: res.ref_key
          });
        });
    });
  }

  ReleaseSave(releaseModel) {
    releaseModel.placementid = this.placementId;
    releaseModel.releasedate = moment(releaseModel.releasedate).format('MM/DD/YYYY').toString();
    if (moment.isMoment(releaseModel.releasetime)) {
      releaseModel.releasetime = releaseModel.releasetime.format('HH:mm').toString();
    }
    if (releaseModel.releasetime) {
      const timeSplit = releaseModel.releasetime.split(':');
      if (!(releaseModel.releasedate instanceof Date)) {
        releaseModel.releasedate = new Date(releaseModel.releasedate);
      }
      releaseModel.releasedate.setHours(timeSplit[0]);
      releaseModel.releasedate.setMinutes(timeSplit[1]);
    }
    releaseModel.releasedate = moment(releaseModel.releasedate).format('MM/DD/YYYY').toString();
    this._placementService.addUpdateRelease(releaseModel).subscribe(
      (res) => {
        this._alertService.success('Placement Release Saved successfully');
        this.exit();
      },
      (err) => { this._alertService.error('Unable to process your request!'); }
    );
  }

  exit() {
    this.router.navigate(['../../list'], { relativeTo: this.route });
  }

  onReasonChanged(value) {
    this.PlacementPlanFormReason.patchValue({
      releasereasonstatus: ''
    });
    if (value === 'TOF') {
      this.releaseReasonSub$ = this.transfertoOtherFacility$;
    } else if (value === 'CD' || value === 'ERC' || value === 'EM') {
      this.releaseReasonSub$ = this.releaseReasonStatus$;
    } else {
      this.releaseReasonSub$ = Observable.empty();
    }
    this.releaseReasonUnderSub$ = Observable.empty();
  }

  onReasonStatusChanged(value) {
    this.releaseReasonUnderSub$ = Observable.empty();
    this.PlacementPlanFormReason.patchValue({
      releasereasonsubstatus: ''
    });
    if (value === 'USUC') {
      if (this.PlacementPlanFormReason.get('releasereason').value === 'ERC') {
        this.releaseReasonUnderSub$ = this.erccdUnSuccessful$;
      } else {
        this.releaseReasonUnderSub$ = this.cdUnSuccessful$;
      }
    } else {
      this.releaseReasonUnderSub$ = Observable.empty();
    }
  }

  onWhareaboutsChanged(value) {
    this.Whereaboutssub$ = Observable.empty();
    if (value === 'TOF') {
      this.Whereaboutssub$ = this.transfertoOtherFacility$;
    } else {
      this.Whereaboutssub$ = Observable.empty();
    }
  }

}
