import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPermanencyPlanComponent } from './djs-permanency-plan.component';

describe('DjsPermanencyPlanComponent', () => {
  let component: DjsPermanencyPlanComponent;
  let fixture: ComponentFixture<DjsPermanencyPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPermanencyPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPermanencyPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
