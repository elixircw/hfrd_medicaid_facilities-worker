import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DjsPlacementRoutingModule } from './djs-placement-routing.module';
import { DjsPlacementComponent } from './djs-placement.component';
import { DjsPermanencyPlanComponent } from './djs-permanency-plan/djs-permanency-plan.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatExpansionModule, MatTooltipModule } from '@angular/material';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { AgmCoreModule } from '@agm/core';
import { PaginationModule } from 'ngx-bootstrap';
import { MatRadioModule } from '@angular/material/radio';
import { DjsProviderSearchComponent } from './djs-provider-search/djs-provider-search.component';
import { DjsPlacementListComponent } from './djs-placement-list/djs-placement-list.component';
import { DjsPlacementAddEditComponent } from './djs-placement-add-edit/djs-placement-add-edit.component';
import { DjsPlacementReleaseComponent } from './djs-placement-release/djs-placement-release.component';
import { DjsPlacementLeavereturnComponent } from './djs-placement-leavereturn/djs-placement-leavereturn.component';
import { DjsPlacementAdmissiondetailsComponent } from './djs-placement-admissiondetails/djs-placement-admissiondetails.component';
import { SharedDirectivesModule } from '../../../../@core/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    DjsPlacementRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatExpansionModule,
    A2Edatetimepicker,
    AgmCoreModule,
    PaginationModule,
    MatRadioModule,
    MatTooltipModule,
    SharedDirectivesModule
  ],
  declarations: [DjsPlacementComponent, DjsPermanencyPlanComponent, DjsProviderSearchComponent,
    DjsPlacementListComponent, DjsPlacementAddEditComponent, DjsPlacementReleaseComponent, DjsPlacementLeavereturnComponent, DjsPlacementAdmissiondetailsComponent]
})
export class DjsPlacementModule { }
