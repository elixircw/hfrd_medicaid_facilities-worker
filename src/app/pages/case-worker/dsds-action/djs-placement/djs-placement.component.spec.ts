import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPlacementComponent } from './djs-placement.component';

describe('DjsPlacementComponent', () => {
  let component: DjsPlacementComponent;
  let fixture: ComponentFixture<DjsPlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
