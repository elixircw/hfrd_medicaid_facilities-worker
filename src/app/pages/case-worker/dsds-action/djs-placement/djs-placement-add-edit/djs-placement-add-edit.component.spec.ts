import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPlacementAddEditComponent } from './djs-placement-add-edit.component';

describe('DjsPlacementAddEditComponent', () => {
  let component: DjsPlacementAddEditComponent;
  let fixture: ComponentFixture<DjsPlacementAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPlacementAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPlacementAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
