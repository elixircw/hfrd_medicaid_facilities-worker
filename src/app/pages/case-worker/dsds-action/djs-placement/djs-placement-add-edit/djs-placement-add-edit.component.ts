import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationRequest } from '../../../../../@core/entities/common.entities';
import { CommonHttpService, GenericService, AlertService, AuthService, DataStoreService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { DSDS_STORE_CONSTANTS } from '../../dsds-action.constants';
import { DjsPlacementService } from '../djs-placement.service';
import { IntakeStoreConstants } from '../../../../newintake/my-newintake/my-newintake.constants';
import { CommonUrlConfig } from '../../../../../@core/common/URLs/common-url.config';
import { AppConstants } from '../../../../../@core/common/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';



@Component({
  selector: 'djs-placement-add-edit',
  templateUrl: './djs-placement-add-edit.component.html',
  styleUrls: ['./djs-placement-add-edit.component.scss']
})
export class DjsPlacementAddEditComponent implements OnInit {
  id: string;
  userInfo: AppUser;
  caseTypes$: Observable<DropdownModel>;
  categoryTypes$: Observable<DropdownModel>;
  categorySubTypes$: Observable<DropdownModel[]>;
  paymentTypes$: Observable<DropdownModel>;
  exitTypes$: Observable<DropdownModel>;
  courtApproved$: Observable<DropdownModel>;
  admissionClassification$: Observable<DropdownModel>;
  admissionType$: Observable<DropdownModel>;
  admissionAuthorization$: Observable<DropdownModel>;
  admissionReason$: Observable<DropdownModel>;
  addPlacementForm: FormGroup;
  reportMode: string;
  showCop = false;
  countyDropdown$: Observable<DropdownModel[]>;
  providerId: string;
  placementAdd: any;
  provider: any;
  placement: any;
  placementid: string;
  isTempPlacement: boolean;
  maxDate = new Date();
  ProjmaxDate = new Date();
  intakeJSON: any;
  isDJSPlacementWorkPage: any;
  intakenumber: any;
  personid: any;
  involvedYouth: any;
  constructor(
    private _httpService: CommonHttpService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _authService: AuthService,
    private router: Router,
    private _dataStoreService: DataStoreService,
    private _DjsPlacementService: DjsPlacementService
  ) { }

  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    this.provider = this._dataStoreService.getData('SELECTED_PROVIDER');
    this.placementid = this.route.snapshot.paramMap.get('placementid');
    this.personid = this._dataStoreService.getData('da_personid');
    if (this._dataStoreService.getData(DSDS_STORE_CONSTANTS.IS_TEMP_PLACEMENT)) {
      this.isTempPlacement = this._dataStoreService.getData(DSDS_STORE_CONSTANTS.IS_TEMP_PLACEMENT);
    } else {
      this.isTempPlacement = false;
    }
    this.addPlacementForm = this._formBuilder.group({
      providername: new FormControl({ value: null, disabled: true }),
      providerid: new FormControl({ value: null, disabled: true }),
      providerunit: new FormControl({ value: null, disabled: true }),
      placementadmissionclassificationkey: new FormControl({ value: null, disabled: true }),
      PlacementAdmissionTypekey: new FormControl({ value: null, disabled: true }),
      parentorg: new FormControl({ value: null, disabled: true }),
      addate: [null],
      adtime: [null],
      releasedate: [null, Validators.required],
      detainer: [null],
      PlacementAdmissionAuthorizationTypekey: [null],
      PlacementPrimaryAdmissionReasonTypekey: [null],
      PlacementPrimaryApprovedAltTypekey: [null],
      county: [null],
      jlocation: new FormControl({ value: null, disabled: true }),
      jcounty: new FormControl({ value: null, disabled: true }),
      fieldworker: new FormControl({ value: null, disabled: true }),
      cop: [null],
      certifiedAd: [null],
      resourceworker: [null],
      effectivedatetime: [{ value: null, disabled: true }]
    });
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.reportMode = this.route.snapshot.paramMap.get('operation');
    console.log(this.reportMode);
    this.intakeJSON = this._dataStoreService.getData('placement_intake');
    if (this.intakeJSON && this.intakeJSON.intakenumber) {
      this.intakenumber = this.intakeJSON.intakenumber;
      this.getInvolvedPersonFromIntake();
      this.isDJSPlacementWorkPage = true;
    } else {
      this.isDJSPlacementWorkPage = false;
    }
    if (this.reportMode === 'update' && this.placementid) {
      this._httpService
        .getPagedArrayList(
          {
            where: {
              placementid: this.placementid
            },
            method: 'get'
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementSearch + '?filter'
        ).subscribe(res => {
          this.placement = res.data[0];
          this.providerId = this.placement.providerinfo.providerid;
          this.placement.providerid = this.placement.providerinfo.providercode;
          this.placementid = this.placement.placementid;
          const effDateTime = this.placement.updatedon;
          this.ProjmaxDate = new Date(this.placement.releasedate);
          this.addPlacementForm.patchValue(this.placement);
          this.addPlacementForm.patchValue({
            providername: this.placement.providerinfo.providername,
            providerid: this.placement.providerinfo.providercode,
            providerunit: this.placement.providerunit,
            parentorg: this.placement.parentorg,
            addate: this.placement.addate,
            adtime: this.placement.adtime,
            releasedate: this.placement.releasedate,
            PlacementAdmissionAuthorizationTypekey: this.placement.placementadmissionauthorizationtypekey,
            PlacementPrimaryAdmissionReasonTypekey: this.placement.placementprimaryadmissionreasontypekey,
            PlacementPrimaryApprovedAltTypekey: this.placement.placementprimaryapprovedalttypekey,
            county: this.placement.countyid,
            jlocation: this.placement.jlocation,
            jcounty: this.placement.county,
            cop: this.placement.certifiedad ? true : false,
            certifiedAd: this.placement.certifiedad,
            resourceworker: this.placement.resourceworker,
            placementadmissionclassificationkey: this.placement.placementadmissionclassificationtypedesc,
            PlacementAdmissionTypekey: this.placement.placementadmissiontypedesc,
            fieldworker: this.userInfo.user.userprofile.displayname,
            detainer: this.placement.detainer
          });
          if (effDateTime) {
            this.addPlacementForm.patchValue({ effectivedatetime: moment(effDateTime).format('MM/DD/YYYY hh:mm a') });
          }
          this.showCop = this.placement.certifiedad ? true : false;
          this.addPlacementForm.get('certifiedAd').disable();
          this.addPlacementForm.get('resourceworker').disable();
          this.addPlacementForm.get('cop').disable();
          this.getCountyDropdown();
          this.loadDropdown();
        });
    } else if (this.reportMode === 'add' && this.provider) {
      this.getPersonDetainerList();
      this.providerId = this.provider.providerid;
      this.provider.providerid = this.provider.providercode;
      this.placementid = this.provider.placementid;
      this.addPlacementForm.patchValue(this.provider);
      this.addPlacementForm.patchValue({
        placementadmissionclassificationkey: this.provider.placementadmissionclassificationdesc,
        PlacementAdmissionTypekey: this.provider.placementadmissiontypedesc
      });
      this.getCountyDropdown();
      this.loadDropdown();
    } else {
      this.router.navigate(['../list'], { relativeTo: this.route });
    }
  }

  getInvolvedPersonFromIntake() {
    const involved = this.intakeJSON.jsondata.persons;
    if (involved && involved.length > 0) {
      const involvedYouth = involved.filter(data => data.Role === 'Youth').map(data => {
        this.personid = data.Pid;
        data.firstname = data.Firstname;
        data.lastname = data.Lastname;
        data.dob = data.Dob;
        return data;
      });
      if (involvedYouth && involvedYouth.length > 0) {
        this.involvedYouth = involvedYouth[0];
      }
    }
  }

  countyPatchValue(event) {
    if (event) {
      this.countyDropdown$.subscribe((data) => {
        data.map((list) => {
          if (list.value === event.value) {
            this.addPlacementForm.patchValue({
              jlocation: 'Head Quarters',
              jcounty: list.text
            });
          }
        });
      });
    }
  }

  getPersonDetainerList() {
    this._httpService
      .getPagedArrayList(
        new PaginationRequest({
          nolimit: true,
          where: {
            personid: this.personid,
            status: 'Open'
          },
          method: 'get'
        }),
        CommonUrlConfig.EndPoint.Intake.personStatus
      )
      .subscribe((result: any) => {
        if (result.length > 0) {
          const isDetention = result.filter(status => status.focuspersonstatustypekey === 'DET');
          if (isDetention.length > 0) {
            this.addPlacementForm.patchValue({
              detainer: 'Yes'
            });
          }
        }
      });
  }

  loadDropdown() {
    const source = Observable.forkJoin([this.getCourtApproved(), this.getAdmissionAuthorization(), this.getPrimaryAdmissionReason()])
      .map((item: any) => {
        return {
          courtApproved: item[0].map((res) => {
            return new DropdownModel({
              text: res.description,
              value: res.placementprimaryapprovedalttypekey
            });
          }),
          admissionAuthorization: item[1].map((itm) => {
            return new DropdownModel({
              text: itm.description,
              value: itm.placementadmissionauthorizationtypekey
            });
          }),
          admissionReason: item[2].map((itm) => {
            return new DropdownModel({
              text: itm.description,
              value: itm.placementprimaryadmissionreasontypekey
            });
          })
        };
      })
      .share();
    this.courtApproved$ = source.pluck('courtApproved');
    // this.admissionClassification$ = source.pluck('admissionClassification');
    // this.admissionType$ = source.pluck('admissionType');
    this.admissionAuthorization$ = source.pluck('admissionAuthorization');
    this.admissionReason$ = source.pluck('admissionReason');
  }

  showCOP(event) {
    console.log(event.checked);
    if (event.checked) {
      this.showCop = true;
      this.addPlacementForm.get('certifiedAd').setValidators([Validators.required]);
      this.addPlacementForm.get('resourceworker').setValidators([Validators.required]);
      this.addPlacementForm.updateValueAndValidity();
    } else {
      this.showCop = false;
      this.addPlacementForm.get('certifiedAd').clearValidators();
      this.addPlacementForm.get('resourceworker').clearValidators();
      this.addPlacementForm.updateValueAndValidity();
      this.addPlacementForm.enable();
      this.addPlacementForm.get('providername').disable();
      this.addPlacementForm.get('providerid').disable();
      this.addPlacementForm.get('providerunit').disable();
      this.addPlacementForm.get('placementadmissionclassificationkey').disable();
      this.addPlacementForm.get('PlacementAdmissionTypekey').disable();
      this.addPlacementForm.get('jlocation').disable();
      this.addPlacementForm.get('jcounty').disable();
      this.addPlacementForm.get('fieldworker').disable();
    }
  }

  getCountyDropdown() {
    this.countyDropdown$ = this._httpService
      .getArrayList(
        {
          where: {
            activeflag: '1',
            state: 'MD'
          },
          order: 'countyname asc',
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.SdmCountyListUrl + '?filter'
      )
      .map((result) => {
        return result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyid
            })
        );
      });
  }

  private getCourtApproved() {
    return this._httpService.getAll('placement/primaryapproved');
  }

  // private getAdmissionClassification() {
  //   return this._httpService.getAll('placement/admissionclassification');
  // }

  // private getAdmissionType() {
  //   return this._httpService.getAll('placement/admissiontype');
  // }

  private getAdmissionAuthorization() {
    return this._httpService.getAll('placement/admissionauthorization');
  }

  private getPrimaryAdmissionReason() {
    return this._httpService.getAll('placement/admissionreason');
  }

  placementSearchAdd(modal) {
    modal.providerid = this.providerId;
    if (this.isDJSPlacementWorkPage) {
      modal.intakenumber = this.intakenumber;
      modal.placementworkertype = (this.userInfo.role.name !== AppConstants.ROLES.DJS_PLACEMENT_WORKER) ? 'ATD' : 'DET';
    } else {
      modal.intakeserviceid = this.id;
    }
    modal.remarks = 'djs placement';
    modal.addate = moment(modal.addate).format('MM/DD/YYYY').toString();
    if (moment.isMoment(modal.adtime)) {
      modal.adtime = modal.adtime.format('HH:mm').toString();
    }
    // if (modal.adtime) {
    //   const timeSplit = modal.adtime.split(':');
    //   if (!(modal.addate instanceof Date)) {
    //     modal.addate = new Date(modal.addate);
    //   }
    //   modal.addate.setHours(timeSplit[0]);
    //   modal.addate.setMinutes(timeSplit[1]);
    // }
    modal.releasedate = moment(modal.releasedate).format('MM/DD/YYYY').toString();
    this.placementAdd = Object.assign(modal);
    modal.placementadmissionclassificationkey = this.provider.placementadmissionclassificationkey;
    modal.PlacementAdmissionTypekey = this.provider.PlacementAdmissionTypekey;
    modal.placementadmissiontypekey = this.provider.PlacementAdmissionTypekey;
    modal.istempplacement = this.isTempPlacement;
    if (this.isTempPlacement && this._dataStoreService.getData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA)) {
      const leaveModal = this._dataStoreService.getData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA);
      this._dataStoreService.setData(DSDS_STORE_CONSTANTS.LEAVE_AND_TANSFER, null);
      this._dataStoreService.setData(DSDS_STORE_CONSTANTS.IS_TEMP_PLACEMENT, null);
      this._DjsPlacementService.addUpdateLR(leaveModal).subscribe(result => {
        if (this.isDJSPlacementWorkPage) {
          this.addPlacementFromIntake();
        } else {
          this.addPlacement();
        }
      }, error => {
        this._alertService.error('Unable to process request');
      });
    } else {
      if (this.isDJSPlacementWorkPage) {
        this.addPlacementFromIntake();
      } else {
        this.addPlacement();
      }
    }
  }

  addPlacement() {
    this._httpService.create(this.placementAdd, CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementSearchAdd).subscribe(
      result => {
        this._alertService.success('Placement Added Successfully');
        this._dataStoreService.setData(DSDS_STORE_CONSTANTS.PROVIDER_SEARCH_MODEL, null);
        this._dataStoreService.setData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA, null);
        setTimeout(() => {
          this.router.navigate(['../list'], { relativeTo: this.route });
        }, 2000);
      }, error => {
        this._alertService.error('Unable to process request');
      });
  }

  addPlacementFromIntake() {
    this._httpService.create(this.placementAdd, CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementSearchAddFromIntake).subscribe(
      result => {
        this._alertService.success('Placement Added Successfully');
        this._dataStoreService.setData(DSDS_STORE_CONSTANTS.PROVIDER_SEARCH_MODEL, null);
        this._dataStoreService.setData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA, null);
        setTimeout(() => {
          this.router.navigate(['../list'], { relativeTo: this.route });
        }, 2000);
      }, error => {
        this._alertService.error('Unable to process request');
      });
  }


  placementUpdate(modal) {
    modal.placementid = this.placementid;
    modal.providerid = this.providerId;
    if (this.isDJSPlacementWorkPage) {
      modal.intakenumber = this.intakenumber;
    } else {
      modal.intakeserviceid = this.id;
    }
    modal.remarks = 'djs placement';
    modal.addate = moment(modal.addate).format('MM/DD/YYYY').toString();
    if (moment.isMoment(modal.adtime)) {
      modal.adtime = modal.adtime.format('HH:mm').toString();
    }
    modal.releasedate = moment(modal.releasedate).format('MM/DD/YYYY').toString();
    // if (modal.adtime) {
    //   const timeSplit = modal.adtime.split(':');
    //   if (!(modal.addate instanceof Date)) {
    //     modal.addate = new Date(modal.addate);
    //     modal.addate.setHours(timeSplit[0]);
    //     modal.addate.setMinutes(timeSplit[1]);
    //   }
    // }
    this.placementAdd = Object.assign(modal);
    this.placementAdd.placementadmissionclassificationkey = this.placement.placementadmissionclassificationtypekey;
    this.placementAdd.placementadmissiontypekey = this.placement.placementadmissiontypekey;
    this.placementAdd.placementadmissionauthorizationtypekey = modal.PlacementAdmissionAuthorizationTypekey;
    this.placementAdd.placementprimaryadmissionreasontypekey = modal.PlacementPrimaryAdmissionReasonTypekey;
    this.placementAdd.placementprimaryapprovedalttypekey = modal.PlacementPrimaryApprovedAltTypekey;
    this.placementAdd.county = modal.county;
    this.placementAdd.certifiedad = modal.certifiedAd;
    this.placementAdd.jcounty = modal.county;
    delete this.placementAdd.certifiedAd;
    this._httpService.create(this.placementAdd,
      CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementUpdate).subscribe(result => {
      this._alertService.success('Placement Updated Successfully');
      this._dataStoreService.setData(DSDS_STORE_CONSTANTS.PROVIDER_SEARCH_MODEL, null);
      setTimeout(() => {
        this.router.navigate(['../../list'], { relativeTo: this.route });
      }, 2000);
    }, error => {
      this._alertService.error('Unable to process request');
    });
  }

  goToProviderSearchResult() {
    this.router.navigate(['../provider-search'], { relativeTo: this.route });
  }

  cancel() {
    if (this.reportMode === 'update') {
      this.router.navigate(['../../list'], { relativeTo: this.route });
    } else {
      this.router.navigate(['../list'], { relativeTo: this.route });
    }
  }
}
