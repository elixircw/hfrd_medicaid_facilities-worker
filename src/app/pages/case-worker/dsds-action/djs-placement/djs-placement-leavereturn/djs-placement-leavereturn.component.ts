import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, AuthService, DataStoreService, CommonDropdownsService } from '../../../../../@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DropdownModel } from '../../../../../@core/entities/common.entities';
import * as moment from 'moment';
import { DjsPlacementService } from '../djs-placement.service';
import { DSDS_STORE_CONSTANTS } from '../../dsds-action.constants';
const TRANSFER_TO_DJS = 'TDF';
const TRANSFER_TO_NON_DJS = 'TNF';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'djs-placement-leavereturn',
  templateUrl: './djs-placement-leavereturn.component.html',
  styleUrls: ['./djs-placement-leavereturn.component.scss']
})
export class DjsPlacementLeavereturnComponent implements OnInit {
  id: string;
  placementid: string;
  lrPlacementForm: FormGroup;
  reportMode: string;
  providerId: string;
  leaveType$: Observable<any>;
  resourceId: string;
  lrList$ = new Observable<any[]>();
  transferFacility: boolean;
  noteshow$: Observable<boolean>;
  leaveReturnDate$: Observable<Date>;
  CheckDate = new Date();
  startDate = new Date();
  admissionDate: Date;
  lrdays = '';
  submitButton = 'Save';
  transferName: string;

  constructor(
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private router: Router,
    private _dataStoreService: DataStoreService,
    private _DjsPlacementService: DjsPlacementService,
    private _dropDownService: CommonDropdownsService) { }

  ngOnInit() {
    this.lrPlacementForm = this._formBuilder.group({
      departeddate: null,
      departedtime: null,
      leavetypekey: null,
      releasedby: null,
      releasedto: null,
      projectedreturndate: null,
      leavenotes: null,
      leavecomments: null,
      actualreturndate: null,
      actualreturntime: null,
      returnby: null,
      returncomments: null,
      lrdays: null
    });
    this.loadDropdown();
    this.noteshow$ = Observable.of(false);
    this.leaveReturnDate$ = Observable.of(new Date());
    this.transferFacility = false;
    const lrstatus = this._dataStoreService.getData('PROVIDER_LR_STATUS');
    this.providerId = this._dataStoreService.getData('PLACEMENT_PROVIDER').providerinfo.providerid;
    this.placementid = this.route.snapshot.paramMap.get('placementId');
    if (lrstatus === 'leave' || lrstatus === 'view') {
      const source = this._DjsPlacementService.getLRList(this.placementid);
      this.lrList$ = source.pluck('data');
      this.lrList$.subscribe(data => {
        console.log(data);
        if (data && data.length > 0) {
          this.reportMode = lrstatus === 'view' ? lrstatus : 'Return';
          this.resourceId = data[0].placementleavereturnid;
          this.lrPlacementForm.patchValue(data[0]);
          this.lrPlacementForm.patchValue({
            departeddate: data[0].departeddate ? new Date(moment(data[0].departeddate).utcOffset(5).format('MM/DD/YYYY')) : new Date(),
            projectedreturndate: data[0].projectedreturndate ? new Date(moment(data[0].projectedreturndate).utcOffset(5).format('MM/DD/YYYY')) : new Date()
          });
          this.leaveTypeChanged(data[0].leavetypekey, 2);
          this.leaveReturnDate$ = Observable.of(data[0].departeddate);
          this.lrPlacementForm.get('departeddate').disable();
          this.lrPlacementForm.get('departedtime').disable();
          this.lrPlacementForm.get('leavetypekey').disable();
          this.lrPlacementForm.get('releasedby').disable();
          this.lrPlacementForm.get('releasedto').disable();
          this.lrPlacementForm.get('projectedreturndate').disable();
          this.lrPlacementForm.get('leavecomments').disable();
          this.lrPlacementForm.get('leavenotes').disable();
          this.lrPlacementForm.get('lrdays').disable();
        } else {
          this.reportMode = 'Leave';
          this.resourceId = null;
        }
      });
    } else {
      this.admissionDate = this._dataStoreService.getData('PLACEMENT_PROVIDER') ? this._dataStoreService.getData('PLACEMENT_PROVIDER').addate : new Date();
      this.reportMode = 'Leave';
      this.resourceId = null;
    }
    this.lrPlacementForm.get('actualreturntime').valueChanges.subscribe(res => {
      this.timeDiff();
    });
  }

  loadDropdown() {

    this.leaveType$ = this._dropDownService.getDropownsByTable('plcmtleave');
  }

  LRSave(modal) {
    modal.placementid = this.placementid;
    modal.providerid = this.providerId;
    delete modal.lrdays;
    if (this.reportMode === 'Leave') {
      if (modal.leavetypekey === TRANSFER_TO_DJS || modal.leavetypekey === TRANSFER_TO_NON_DJS) {
        this.transferFacility = true;
      }
      modal.departeddate = moment(modal.departeddate).format('MM/DD/YYYY').toString();
      if (moment.isMoment(modal.departedtime)) {
        modal.departedtime = modal.departedtime.format('HH:mm').toString();
      }
      modal.projectedreturndate = moment(modal.projectedreturndate).format('MM/DD/YYYY').toString();
    } else {
      modal.placementleavereturnid = this.resourceId;
      modal.actualreturndate = moment(modal.actualreturndate).format('MM/DD/YYYY').toString();
      if (moment.isMoment(modal.actualreturntime)) {
        modal.actualreturntime = modal.actualreturntime.format('HH:mm').toString();
      }
    }


    if (this.transferFacility) {
      const searchParam = this.getTranserSearchParam(modal);
      this._dataStoreService.setData(DSDS_STORE_CONSTANTS.LEAVE_AND_TANSFER, searchParam);
      this._dataStoreService.setData(DSDS_STORE_CONSTANTS.IS_TEMP_PLACEMENT, true);
      modal.leavetypekeydesc = this.transferName;
      this._dataStoreService.setData(DSDS_STORE_CONSTANTS.TEMP_LEAVE_DATA, modal);
      this._dataStoreService.setData('PLACEMENT_INVOLVED_YOUTH', this._dataStoreService.getData('PLACEMENT_INVOLVED_YOUTH'));
      this._dataStoreService.setData('PLACEMENT_PROVIDER', this._dataStoreService.getData('PLACEMENT_PROVIDER'));
      this.router.navigate(['../../provider-search'], { relativeTo: this.route });
    } else {
      this._DjsPlacementService.addUpdateLR(modal).subscribe(result => {
        this._alertService.success('Placement ' + this.reportMode + ' Saved Successfully');
        setTimeout(() => {
          this.router.navigate(['../../list'], { relativeTo: this.route });
        }, 2000);
      }, error => {
        this._alertService.error('Unable to process request');
      });
    }

  }
  getTranserSearchParam(leaveForm) {
    const searchParam = { 'isoperatedbydjs': false };
    if (leaveForm.leavetypekey === TRANSFER_TO_DJS) {
      searchParam.isoperatedbydjs = true;
    }
    if (leaveForm.leavetypekey === TRANSFER_TO_NON_DJS) {
      searchParam.isoperatedbydjs = false;
    }
    return searchParam;
  }

  leaveTypeChanged(value, key) {
    if (key === 2) {
      if (value === 'ONF') {
        this.noteshow$ = Observable.of(true);
      } else {
        this.noteshow$ = Observable.of(false);
      }
    }
    if (key === 1) {
      if (value.value === 'ONF') {
        this.noteshow$ = Observable.of(true);
      } else {
        this.noteshow$ = Observable.of(false);
      }
      if (value.value === TRANSFER_TO_DJS || value.value === TRANSFER_TO_NON_DJS) {
        this.submitButton = 'Transfer';
      } else {
        this.submitButton = 'Save';
      }
      this.transferName = value.source.selected._element.nativeElement.innerText.trim();
    }
  }

  departedDateChanged(value) {
    if (value) {
      this.leaveReturnDate$ = Observable.of(value);
    }
  }

  // CalculateLeave(value) {
  //   const departdate = this.lrPlacementForm.get('departeddate').value;
  //   const element = document.getElementById('leaveCalc') as HTMLElement;
  //   this.lrdays = Math.round((new Date(value).getTime() - new Date(departdate).getTime()) / (1000 * 60 * 60 * 24)) + ' days';
  //   this.lrPlacementForm.get('lrdays').setValue(this.lrdays);
  // }

  timeDiff() {
    const departformat = moment(this.lrPlacementForm.get('departeddate').value).format('YYYY-MM-DD');
    const returnformat = moment(this.lrPlacementForm.get('actualreturndate').value).format('YYYY-MM-DD');
    const departtimeformat = moment(this.lrPlacementForm.get('departedtime').value).format('HH:mm:ss');
    const returntimeformat = moment(this.lrPlacementForm.get('actualreturntime').value).format('HH:mm:ss');
    const departtime = moment(`${departformat} ${departtimeformat}`, 'YYYY-MM-DD HH:mm:ss').format();
    const returndatetime = moment(`${returnformat} ${returntimeformat}`, 'YYYY-MM-DD HH:mm:ss').format();
    const days = returndatetime ? moment(returndatetime).diff(departtime, 'days', false) : moment().diff(returndatetime, 'days', false);
    const totalhours = returndatetime ? moment(returndatetime).diff(departtime, 'hours', false) : moment().diff(returndatetime, 'hours', false);
    const totalminutes = returndatetime ? moment(returndatetime).diff(departtime, 'minutes', false) : moment().diff(returndatetime, 'minutes', false);
    const mins = totalminutes - (totalhours * 60);
    const hours = totalhours - (days * 24);
    if (!isNaN(days) && !isNaN(hours) && !isNaN(mins)) {
      this.lrdays = days + 'days ' + hours + 'hours ' + mins + 'min';
      this.lrPlacementForm.get('lrdays').setValue(this.lrdays);
    }
  }

  cancel() {
    this.router.navigate(['../../list'], { relativeTo: this.route });
  }

}
