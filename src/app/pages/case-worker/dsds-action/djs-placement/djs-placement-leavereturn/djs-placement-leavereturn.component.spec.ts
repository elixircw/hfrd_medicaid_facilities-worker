import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPlacementLeavereturnComponent } from './djs-placement-leavereturn.component';

describe('DjsPlacementLeavereturnComponent', () => {
  let component: DjsPlacementLeavereturnComponent;
  let fixture: ComponentFixture<DjsPlacementLeavereturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPlacementLeavereturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPlacementLeavereturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
