import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjsPlacementListComponent } from './djs-placement-list.component';

describe('DjsPlacementListComponent', () => {
  let component: DjsPlacementListComponent;
  let fixture: ComponentFixture<DjsPlacementListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjsPlacementListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjsPlacementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
