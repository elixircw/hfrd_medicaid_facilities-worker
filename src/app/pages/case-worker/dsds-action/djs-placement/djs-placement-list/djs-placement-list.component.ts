import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PaginationInfo, PaginationRequest, DropdownModel } from '../../../../../@core/entities/common.entities';
import { Placement } from '../../service-plan/_entities/service-plan.model';
import { InvolvedPerson } from '../../involved-persons/_entities/involvedperson.data.model';
import { CommonHttpService, DataStoreService, AuthService, AlertService } from '../../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../case-worker-url.config';
import * as moment from 'moment';
import { AppUser } from '../../../../../@core/entities/authDataModel';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AppConstants } from '../../../../../@core/common/constants';
import { CASE_STORE_CONSTANTS } from '../../../_entities/caseworker.data.constants';
declare var $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'djs-placement-list',
  templateUrl: './djs-placement-list.component.html',
  styleUrls: ['./djs-placement-list.component.scss']
})
export class DjsPlacementListComponent implements OnInit, AfterViewInit {
  id: string;
  involvedYouth: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  reportMode: string;
  placementCount$: Observable<number>;
  placement$: Observable<Placement[]>;
  isAddPlacement = false;
  involevedPerson$: Observable<InvolvedPerson[]>;
  selectedProvider: any;
  userInfo: AppUser;
  PlacementPlanFormExit: FormGroup;
  exitplacementid: string;
  startDate: Date;
  Exitdatetime: string;
  Exittime: string;
  exitTypes$: Observable<DropdownModel>;
  detainerFlag: boolean;
  resedentialStatus: boolean;
  personId: string;
  isBlockParentPlacement = false;
  isDJSPlacementWorkPage = false;
  intakenumber: string;
  intakeJSON: any;
  isDjs: boolean;
  constructor(
    private _httpService: CommonHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private _dataStoreService: DataStoreService,
    private _authService: AuthService,
    private _alertService: AlertService,
    private _formBuilder: FormBuilder
  ) {
    this.intakenumber =  this._dataStoreService.getData('da_intakenumber');
   // this.intakeNumber = route.snapshot.parent.parent.parent.params['intakenumber'];
    this.route.data.subscribe(response => {
      const pageSource = response.pageSource;
      this.isDJSPlacementWorkPage = pageSource === AppConstants.PAGES.DJS_PLACEMENT_WORKER;
    });
    if (this.intakenumber) {
      this.isDJSPlacementWorkPage = true;
    }
  }

  ngOnInit() {
    this.isDjs = this._authService.isDJS();
    this.userInfo = this._authService.getCurrentUser();
    this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
    this.getPlacement(1);
    if (this.isDJSPlacementWorkPage) {
      this.intakeJSON = this._dataStoreService.getData('placement_intake');
      this.getInvolvedPersonFromIntake();
    } else {
      this.personId = this._dataStoreService.getData('da_personid');
      this.getInvolvedPerson();
    }

    this.PlacementPlanFormExit = this._formBuilder.group({
      enddatetime: [null, Validators.required],
      exittime: [null, Validators.required],
      exitremarks: ['', Validators.required]
    });

  }
  ngAfterViewInit() {
      const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
      if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
          (<any>$(':button')).prop('disabled', true);
          (<any>$('span')).css({'pointer-events': 'none',
                      'cursor': 'default',
                      'opacity': '0.5',
                      'text-decoration': 'none'});
          (<any>$('i')).css({'pointer-events': 'none',
                                  'cursor': 'default',
                                  'opacity': '0.5',
                                  'text-decoration': 'none'});
          (<any>$('th a')).css({'pointer-events': 'none',
                                  'cursor': 'default',
                                  'opacity': '0.5',
                                  'text-decoration': 'none'});
      }
  }

  private getDefaults() {
    this.getResedentialStatus().subscribe(element => {
      this.resedentialStatus = element[0].residentialexists;
      this.detainerFlag = element[0].detainerflag;
    });
  }

  getResedentialStatus() {
    return this._httpService.getArrayList({ where: { personid: this.personId }, nolimit: true, method: 'get' }, 'placement/placementresidential?filter');
  }

  searchClearPlacement() {
    this.router.navigate(['../provider-search'], { relativeTo: this.route, queryParams: { resedentialStatus : this.resedentialStatus } });
  }

  getPlacement(page: number) {
    const source = this._httpService
      .getPagedArrayList(
        new PaginationRequest({
          page: page,
          limit: this.paginationInfo.pageSize,
          where: this.isDJSPlacementWorkPage ? {
            intakenumber: this.intakenumber ? this.intakenumber : null
          } : {
              intakeserviceid: this.id ? this.id : null
            },
          method: 'get'
        }),
        CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementListUrl + '?filter'
      )
      .map((res) => {
        return {
          data: res.data,
          count: res.count
        };
      })
      .share();
    this.placement$ = source.pluck('data');
    if (page === 1) {
      this.placementCount$ = source.pluck('count');
    }
    this.placement$.subscribe(data => {
      if (data && data.length > 0) {
        const placement = data.find(placementObj => placementObj.istempplacement);
        if (placement && (placement.istempplacement === true) && (!placement.placeenddatetime)) {
          this.isBlockParentPlacement = true;
        }
      }
    });
  }
  pageChanged(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.isAddPlacement = true;
    this.getPlacement(this.paginationInfo.pageNumber);
  }
  getInvolvedPerson() {
    this.involevedPerson$ = this._httpService
      .getArrayList(
        {
          method: 'get',
          where: { intakeservreqid: this.id }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
          .InvolvedPersonListUrl +
        '?data'
      )
      .map((res) => {
        return res['data'].filter((item) => item.rolename === 'Youth');
      });

    this.involevedPerson$.subscribe((data) => {
      this.involvedYouth = data[0];
      this.personId = this._dataStoreService.getData('da_personid') ? this._dataStoreService.getData('da_personid') : data[0].personid;
      this.getDefaults();
    });
  }
  getInvolvedPersonFromIntake() {
    const involved = this.intakeJSON.jsondata.persons;
    if (involved && involved.length > 0) {
      const involvedYouth = involved.filter(data => data.Role === 'Youth').map(data => {
        this.personId = data.Pid;
        data.firstname = data.Firstname;
        data.lastname = data.Lastname;
        data.dob = data.Dob;
        return data;
      });
      if (involvedYouth && involvedYouth.length > 0) {
        this.involvedYouth = involvedYouth[0];
        this.getDefaults();
      }
    }
  }

  SelectedForModify(model) {
    this.reportMode = 'update';
    this.selectedProvider = model;
    this.router.navigate(['../update/' + this.selectedProvider.placementid], { relativeTo: this.route });
  }

  releasePlacement(placement) {
    this._dataStoreService.setData('PLACEMENT_PROVIDER', placement);
    this._dataStoreService.setData('PLACEMENT_INVOLVED_YOUTH', this.involvedYouth);
    this.router.navigate(['../release/' + placement.placementid], { relativeTo: this.route });
  }

  SelectedForLeaveReturn(placement) {
    this._dataStoreService.setData('PLACEMENT_PROVIDER', placement);
    this._dataStoreService.setData('PLACEMENT_INVOLVED_YOUTH', this.involvedYouth);
    this._dataStoreService.setData('PROVIDER_LR_STATUS', placement.lrstatus);
    if (this.isBlockParentPlacement) {
      this._dataStoreService.setData('PROVIDER_LR_STATUS', 'view');
      this.router.navigate(['../view-leave/' + placement.placementid], { relativeTo: this.route });
    } else {
      this.router.navigate(['../leavereturn/' + placement.placementid], { relativeTo: this.route });
    }
  }
}
