import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { DropdownModel, PaginationRequest } from '../../../../@core/entities/common.entities';
import { CommonUrlConfig } from '../../../../@core/common/URLs/common-url.config';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';

@Injectable()
export class DjsPlacementService {

    listsDropdownSource;

    constructor(private _httpService: CommonHttpService) { }

    addUpdateLR(modal) {
        return this._httpService.create(modal, CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementLRAddUpdate);
    }

    addUpdateRelease(releaseModel) {
       return this._httpService
            .create(releaseModel,
                CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementReleaseAddUpdate
            );
    }

    getLRList(placementid) {
        const source = this._httpService.getArrayList(
            new PaginationRequest(
                {
                    method: 'get',
                    where: { placementid: placementid }
                }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Placement.PlacementLRList + '?filter'
        ).map((result: any) => {
            return {
                data: result
            };
        }).share();
        return source;
    }

}
