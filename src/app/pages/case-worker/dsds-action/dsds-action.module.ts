import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxMaskModule } from 'ngx-mask';
import { QuillModule } from 'ngx-quill';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { MatRadioModule } from '@angular/material/radio';

import { environment } from '../../../../environments/environment';
import { SharedDirectivesModule } from '../../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../../@core/pipes/shared-pipes.module';
import { ControlMessagesModule } from '../../../shared/modules/control-messages/control-messages.module';
import { SpeechSynthesizerService } from '../../../shared/modules/web-speech/shared/services/speech-synthesizer.service';
//import { AllegationComponent } from './allegation/allegation.component';
import { CaseWorkerDocumentCreatorComponent } from './case-worker-document-creator/case-worker-document-creator.component';
//import {CaseWorkerViewAssessmentComponent } from './assessment/case-worker-view-assessment/case-worker-view-assessment.component';
//import { EditChildAttachmentComponent } from './child-removal/edit-child-attachment/edit-child-attachment.component';
//import { UploadChildAttachmentComponent } from './child-removal/upload-child-attachment/upload-child-attachment.component';
//import { InvestigationReportComponent } from './disposition/investigation-report/investigation-report.component';
import { DsdsActionRoutingModule } from './dsds-action-routing.module';
import { DsdsActionComponent } from './dsds-action.component';
//import { InvolvedEntityComponent } from './involved-entity/involved-entity.component';
import { SpeechRecognitionService } from '../../../@core/services/speech-recognition.service';
import { SpeechRecognizerService } from '../../../shared/modules/web-speech/shared/services/speech-recognizer.service';
import { DsdsService } from './_services/dsds.service';
import { CommonControlsModule } from '../../../shared/modules/common-controls/common-controls.module';
import { FolderChangeComponent } from './folder-change/folder-change.component';
import { IntakeConfigService } from '../../newintake/my-newintake/intake-config.service';
import { RelationshipNewModule } from '../../shared-pages/relationship-new/relationship-new.module';
import { SharedComponentsModule } from '../../../shared/shared-components/shared-components.module';
import { NarrativeModule } from './narrative/narrative.module';
import { AdoptionPersonsModule } from './adoption-persons/adoption-persons.module';
//import { AssessmentAodComponent } from './assessment/assessment-aod/assessment-aod.component';
import { AssessmentService } from './assessment/assessment.service';
// import { AlternativeResponseSummaryComponent } from './alternative-response-summary/alternative-response-summary.component';
// tslint:disable-next-line:max-line-length
@NgModule({
    imports: [
        CommonModule,
        DsdsActionRoutingModule,
        SharedDirectivesModule,
        ReactiveFormsModule,
        FormsModule,
        MatCardModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatExpansionModule,
        MatFormFieldModule,
        RelationshipNewModule,
        MatInputModule,
        MatListModule,
        MatNativeDateModule,
        MatRadioModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        PaginationModule,
        BsDatepickerModule,
        ControlMessagesModule,
        NgSelectModule,
        SharedDirectivesModule,
        SharedPipesModule,
        A2Edatetimepicker,
        NgxMaskModule.forRoot(),
        NgxfUploaderModule.forRoot(),
        QuillModule,
        AgmCoreModule.forRoot({
            apiKey: environment.googleMapApi
        }),
        CommonControlsModule,
        SharedComponentsModule,
        NarrativeModule,
        AdoptionPersonsModule
    ],
    exports: [
        DsdsActionComponent
        //InvolvedEntityComponent,
        //AllegationComponent,
       //CaseWorkerDocumentCreatorComponent
        ],
    // tslint:disable-next-line:max-line-length
    declarations: [
        DsdsActionComponent,
        // InvolvedEntityComponent,
        // AllegationComponent,
        // EditChildAttachmentComponent,
        // UploadChildAttachmentComponent,
         CaseWorkerDocumentCreatorComponent,
         //CaseWorkerViewAssessmentComponent,
        // InvestigationReportComponent,
         FolderChangeComponent,
        // AssessmentAodComponent,
        // AssessmentCansFComponent,
        // AssessmentMfiraComponent,
        // AssessmentCansOutComponent

    ],
    providers: [
        SpeechSynthesizerService,
        SpeechRecognitionService,
        SpeechRecognizerService,
        DsdsService,
        IntakeConfigService,
        AssessmentService
    ]
})
export class DsdsActionModule { }
