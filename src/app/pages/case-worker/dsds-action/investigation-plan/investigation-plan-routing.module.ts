import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvestigationPlanComponent } from './investigation-plan.component';
import { InvestigationPlanSearchComponent } from './investigation-plan-search/investigation-plan-search.component';
import { InvestigationPlanActivityComponent } from './investigation-plan-search/investigation-plan-activity/investigation-plan-activity.component';
import { InvestigationPlanSuggestedTaskComponent } from './investigation-plan-search/investigation-plan-suggested-task/investigation-plan-suggested-task.component';
import { InvestigationPlanDeleteActivityComponent } from './investigation-plan-search/investigation-plan-delete-activity/investigation-plan-delete-activity.component';
import { InvestigationPlanTaskComponent } from './investigation-plan-search/investigation-plan-task/investigation-plan-task.component';

const routes: Routes = [{
  path: '',
  component: InvestigationPlanComponent
},
{
  path: 'plan-search',
  component: InvestigationPlanSearchComponent
},
{
  path: 'plan-activity/:investigationID',
  component: InvestigationPlanActivityComponent
},
{
  path: 'plan-suggested/:investigationID',
  component: InvestigationPlanSuggestedTaskComponent
},
{
  path: 'plan-delete/:investigationID',
  component: InvestigationPlanDeleteActivityComponent
},
{
  path: 'plan-task/:investigationID/:loadNumber',
  component: InvestigationPlanTaskComponent
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvestigationPlanRoutingModule { }
