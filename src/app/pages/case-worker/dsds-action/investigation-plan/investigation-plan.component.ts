import { DatePipe } from '@angular/common';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { ControlUtils } from '../../../../@core/common/control-utils';
import { ObjectUtils } from '../../../../@core/common/initializer';
import { DropdownModel, PaginationInfo } from '../../../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';
import { AlertService, AuthService, CommonHttpService, DataStoreService, GenericService, SessionStorageService } from '../../../../@core/services';
import { DSDSActionSummary } from '../../_entities/caseworker.data.model';
import { CaseWorkerUrlConfig } from '../../case-worker-url.config';
import * as status from './_configurations/status.json';
import { ActivityGoalModal, ActivityTaskModal, IntakeActivityTask, InvestigationPlanCaseCount, InvestigationPlanSummary } from './_entities/investigationplan.data.models';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { DsdsService } from '../_services/dsds.service';
import { CASE_STORE_CONSTANTS } from '../../_entities/caseworker.data.constants';
import { MatRadioChange } from '@angular/material';

declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'investigation-plan',
    templateUrl: './investigation-plan.component.html',
    styleUrls: ['./investigation-plan.component.scss'],
    providers: [DatePipe]
})
export class InvestigationPlanComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {
    id: string;
    daNumber: string;
    investigationId: string;
    startdate: string;
    starttime: string;
    starttimeformat: string;
    enddate: string;
    endtime: string;
    endtimeformat: string;
    showTime: boolean;
    isServiceCase: boolean;
    statusSearchForTask = false;
    taskstatus: string;
    editLabel: string;
    caseStatus: string;
    userType = false;
    activityTask = new ActivityTaskModal();
    taskUpdate = new IntakeActivityTask();
    activityGoal = new ActivityGoalModal();
    paginationInfo: PaginationInfo = new PaginationInfo();
    activityGoal$: Observable<ActivityGoalModal[]>;
    activityTask$: Observable<ActivityTaskModal[]>;
    activityTasks: ActivityTaskModal[] = [];
    dsdsActionsSummary = new DSDSActionSummary();
    investigationPlanCaseCount$: Observable<InvestigationPlanCaseCount>;
    totalRecords$: Observable<number>;
    canDisplayPager$: Observable<boolean>;
    dsdsActionsSummary$: Observable<any>;
    statusDropdown: DropdownModel[];
    goalTypeDropdownItems$: Observable<DropdownModel[]>;
    goalStatusTypeDropdownItems$: Observable<DropdownModel[]>;
    dispositionDropdownItems$: Observable<DropdownModel[]>;
    taskDispositionDropdownItems$: Observable<DropdownModel[]>;
    activityDropdownItems$: Observable<DropdownModel[]>;
    taskDropdownItems$: Observable<DropdownModel[]>;
    usersProfileDropdownItems$: Observable<DropdownModel[]>;
    taskTypeStatusDropdownItems$: Observable<DropdownModel[]>;
    activityGoalsFormGroup: FormGroup;
    activityTaskFormGroup: FormGroup;
    investigationPlanSearchFormGroup: FormGroup;
    activityTaskStatusFormGroup: FormGroup;
    forkedResult$: Observable<{}[]>;
    maxDate = new Date();
    private pageSubject$ = new Subject<number>();
    agency = '';
    userInfo: AppUser;
    dispositionStatusDetails = [];
    isAS: boolean;
    isDJS = false;
    isInHomeservice = false;
    isInHomeActive: boolean;
    sortBy = false;
    sortCategoryForm: FormGroup;
    sortDropDown: { 'value': string; 'text': string; }[];
    selectedActionType: string;
    isClosed = false;
    constructor(
        private route: ActivatedRoute,
        private _router: Router,
        private formBuilder: FormBuilder,
        private _formBuilder: FormBuilder,
        private _service: GenericService<IntakeActivityTask>,
        private _commonHttpService: CommonHttpService,
        private _planSummaryService: GenericService<InvestigationPlanSummary>,
        private _taskService: GenericService<ActivityTaskModal>,
        private _goalsService: GenericService<ActivityGoalModal>,
        private _authService: AuthService,
        private _alertService: AlertService,
        private datePipe: DatePipe,
        private _changeDetect: ChangeDetectorRef,
        private _dataStoreService: DataStoreService,
        private _dsdsService: DsdsService,
        private storage: SessionStorageService

    ) {
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
    }
    ngOnInit() {
        this.isServiceCase = this._dsdsService.isServiceCase();
        this.initiateFormGroup();
        this.statusDropdown = <any>status;
        this.getActionSummary();
        this.loadDropdown();
        this.getActivityDropdown();
        this.getDispositionStatus();
        // this.searchByActivity();
        this.activityTaskFormGroup.get('loadnumber').disable();
        this.activityTaskFormGroup.get('assignedon').disable();
        this.activityTaskFormGroup.get('activitytasktypekey').disable();
        /* this._dataStoreService.currentStore.subscribe((data) => {
            if (data['dsdsActionsSummary']) {
                this.dsdsActionsSummary = data['dsdsActionsSummary'];
                if (this.dsdsActionsSummary && this.dsdsActionsSummary.da_subtype && this.dsdsActionsSummary.da_subtype === 'IHS'
                    && this.dsdsActionsSummary.teamtypekey && this.dsdsActionsSummary.teamtypekey === 'CW') {
                    this.isInHomeservice = true;
                }
            }
        }); */
        this.dsdsActionsSummary = this._dataStoreService.getData('dsdsActionsSummary');
        if (this.dsdsActionsSummary && this.dsdsActionsSummary.da_subtype && this.dsdsActionsSummary.da_subtype === 'IHS'
            && this.dsdsActionsSummary.teamtypekey && this.dsdsActionsSummary.teamtypekey === 'CW') {
            this.isInHomeservice = true;
        }
        this.agency = this._authService.getAgencyName();
        this.userInfo = this._authService.getCurrentUser();
        if (this.userInfo.user.userprofile.teamtypekey === 'AS') {
            this.isAS = true;
        }
        if (this.userInfo.user.userprofile.teamtypekey === 'DJS') {
            this.isDJS = true;
        }
        const da_status = this.storage.getItem('da_status');
        if (da_status) {
        if (da_status === 'Closed' || da_status === 'Completed') {
            this.isClosed = true;
        } else {
            this.isClosed = false;
        }
        }
    }
    ngAfterViewInit() {
        const intakeCaseStore = this._dataStoreService.getData('IntakeCaseStore');
        if (this._authService.isDJS() && intakeCaseStore && intakeCaseStore.action === 'view') {
            (<any>$(':button')).prop('disabled', true);
            (<any>$('.hidefromview')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('span')).css({'pointer-events': 'none',
                        'cursor': 'default',
                        'opacity': '0.5',
                        'text-decoration': 'none'});
            (<any>$('i')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
            (<any>$('th a')).css({'pointer-events': 'none',
                                    'cursor': 'default',
                                    'opacity': '0.5',
                                    'text-decoration': 'none'});
        }
    }
    ngAfterViewChecked() {
        this._changeDetect.detectChanges();
    }
    ngOnDestroy() {
        if (this.activityTaskStatusFormGroup.dirty) {
            this.saveTask();
        }
    }
    initiateFormGroup() {
        this.activityGoalsFormGroup = this.formBuilder.group({
            activitygoalid: [''],
            goal: [''],
            goalType: [''],
            duedate: ['', [Validators.required, Validators.minLength(1)]],
            status: [''],
            disposition: [''],
            completiondate: [null, [Validators.required, Validators.minLength(1)]]
        });

        this.activityTaskFormGroup = this.formBuilder.group(
            {
                task: [''],
                activitytaskid: [''],
                taskdescription: [''],
                activitytasktypekey: [''],
                insertedon: new Date(),
                required: [true],
                duedate: new Date(),
                loadnumber: [''],
                assignedon: new Date(),
                location: [''],
                outofoffice: [''],
                activitytaskstatustypekey: ['', Validators.required],
                completeddate: [new Date()],
                activitytaskdispositiontypekey: ['', Validators.required],
                starttimeformat: [''],
                startdate: [''],
                starttime: [''],
                enddate: [''],
                endtime: [''],
                endtimeformat: [''],
                tasktype: ['']
            }
            // { validator: [this.checkDateRange, this.checkCompletedDate] }
        );
        this.investigationPlanSearchFormGroup = this.formBuilder.group({
            invesplansearch: [''],
            activityname: [''],
            taskname: [''],
            statustype: [''],
            taskagainst: ['']
        });
        // this.taskTypeStatusDropdown('Investigation');
        this.activityTaskStatusFormGroup = this._formBuilder.group({
            task: this._formBuilder.array([this.createTaskForm()])
        });
        this.sortCategoryForm = this.formBuilder.group({
        sortBy: [''],
        sortDir: ['']
        });
    }
    createTaskForm() {
        return this._formBuilder.group({
            assignedon: [''],
            activitytypekey: [''],
            taskdescription: [''],
            duedate: [''],
            activitytaskid: [''],
            activitytaskstatustypekey: [''],
            activitytaskdispositiontypekey: null,
            completeddate: [''],
            tasktype: [''],
            taskdispositiontypekey: [''],
            iseditable: [''],
            notes: ['']
        });
    }
    addNewTask() {
        const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];
        control.push(this.createTaskForm());
    }

    setFormValues(isInHome: number) {
        this.activityTaskStatusFormGroup.setControl('task', this._formBuilder.array([]));
        const control = <FormArray>this.activityTaskStatusFormGroup.controls['task'];

        if (this.isServiceCase || this.isInHomeservice) {
            let filterActivityTasks = [];
            if (isInHome === 1) {
                filterActivityTasks = this.activityTasks.filter(task => task.checklisttype === 'IHM');
            } else {
                filterActivityTasks = this.activityTasks.filter(task => task.checklisttype !== 'IHM');
            }
            filterActivityTasks.forEach((x) => {
                control.push(this.buildTaskForm(x));
            });
        } else {
            this.activityTasks.forEach((x) => {
                control.push(this.buildTaskForm(x));
            });
        }
    }

    changeSelectedActionType(event: MatRadioChange) {
        if (event.value === 'SORT') {
            this.sortBy = true;
         }
    }

    private buildTaskForm(x): FormGroup {
        return this._formBuilder.group({
            assignedon: x.assignedon,
            activitytypekey: x.activitytypekey,
            actvityname: x.actvityname ? x.actvityname : '',
            taskdescription: x.task,
            duedate: x.duedate,
            completeddate: x.completeddate,
            activitytaskid: x.activitytaskid,
            activitytaskstatustypekey: x.activitytaskstatustypekey,
            activitytaskdispositiontypekey: x.activitytaskdispositiontypekey,
            tasktype: x.tasktype,
            taskdispositiontypekey: x.taskdispositiontypekey,
            iseditable: x.iseditable,
            isstatustype: x.isstatustype,
            notes: x.notes
        });
    }

    addTask(i: number) {
        const val = this.activityTaskStatusFormGroup.value.task[i];
        if (val.activitytaskstatustypekey === 'InvOpen') {
            this._alertService.warn('Please fill Details');
            return true;
        } else {
            if (!val.notes) {
                this._alertService.warn('Please fill Notes');
                return true;
            }
            if (!val.completeddate) {
                this._alertService.warn('Please fill completed date');
                return true;
            }
            this._service.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivitySaveUrl;
            let requestData = this.activityTaskStatusFormGroup.value.task
                .filter((item) => item.activitytaskid === val.activitytaskid)
                .map((item) => {
                    return {
                        activitytaskid: item.activitytaskid,
                        activitytaskstatustypekey: item.activitytaskstatustypekey,
                        activitytaskdispositiontypekey: item.activitytaskdispositiontypekey,
                        completeddate: item.completeddate,
                        taskdispositiontypekey: item.taskdispositiontypekey,
                        duedate: item.duedate,
                        notes: this.isDJS ? item.notes : ''
                    };
                });
            requestData = {
                intakeserviceid: this.id,
                task: requestData
            };
            this._service.create(requestData).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Task updated successfully');
                        this.getInvestigationPlanSummary(this.investigationId);
                        this.getActivityTask(1, this.investigationId);
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        }
    }

    saveTask() {
        const validateCompleteDate = this.activityTaskStatusFormGroup.value.task.filter((res) => res.completeddate);
        let isvalidCompleteDate = true;
        let isvalidDispostionClosedScenario = true;
        validateCompleteDate.forEach((element) => {
            var completedate = new Date(element.completeddate).setHours(0, 0, 0, 0);
            var Assigneddate = new Date(element.assignedon).setHours(0, 0, 0, 0);
            if (completedate < Assigneddate) {
                isvalidCompleteDate = false;
            }
            if (element.taskdispositiontypekey == "Completed") {
                if (element.activitytaskstatustypekey != "InvClosed") {
                    isvalidDispostionClosedScenario = false;
                }
            }
        });
        if (!isvalidCompleteDate) {
            this._alertService.warn('Please provide valid completed date.');
            return;
        }
        if (!isvalidDispostionClosedScenario) {
            this._alertService.warn('Please provide valid status and disposition.');
            return;
        }

        const agency = this._authService.getAgencyName();
        let toSaveTasks = false;
        if (agency === 'CW') {
            toSaveTasks = validateCompleteDate.length > 0 ? true : false;
        } else if (agency === 'AS') {
            toSaveTasks = true;
        }

        if (toSaveTasks) {
            this._service.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivitySaveUrl;
            let requestData = this.activityTaskStatusFormGroup.value.task
                .filter((item) => (item.activitytaskstatustypekey === 'InvClosed' && item.completeddate) || item.activitytaskstatustypekey !== 'InvClosed')
                .map((item) => {
                    return {
                        activitytaskid: item.activitytaskid,
                        activitytaskstatustypekey: item.activitytaskstatustypekey,
                        activitytaskdispositiontypekey: item.activitytaskdispositiontypekey,
                        completeddate: item.completeddate,
                        taskdispositiontypekey: item.taskdispositiontypekey,
                        duedate: item.duedate,
                        notes: this.isDJS ? item.notes : ''
                    };
                });
            requestData = {
                intakeserviceid: this.id,
                task: requestData
            };
            this._service.create(requestData).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Task updated successfully');
                        this.getInvestigationPlanSummary(this.investigationId);
                        this.getActivityTask(1, this.investigationId);
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._alertService.warn('Please fill completed date');
        }
    }

    checkDateRange(activityTaskFormGroup) {
        if (activityTaskFormGroup.controls.duedate.value) {
            if (activityTaskFormGroup.controls.insertedon.value > activityTaskFormGroup.controls.duedate.value) {
                return { notValid: true };
            }
            return null;
        }
    }
    checkCompletedDate(activityTaskFormGroup) {
        if (new Date(activityTaskFormGroup.controls.assignedon.value) > new Date(activityTaskFormGroup.controls.completeddate.value)) {
            return { notValidCompletedDate: true };
        }
    }
    searchByActivity() {
        this.statusSearchForTask = false;
        this.investigationPlanSearchFormGroup.get('taskname').disable();
        this.investigationPlanSearchFormGroup.get('taskagainst').disable();
        this.investigationPlanSearchFormGroup.patchValue({ taskname: '' });
        this.investigationPlanSearchFormGroup.get('activityname').enable();
    }
    searchByTask() {
        this.statusSearchForTask = true;
        this.investigationPlanSearchFormGroup.get('activityname').disable();
        this.investigationPlanSearchFormGroup.patchValue({ activityname: '' });
        this.investigationPlanSearchFormGroup.get('taskname').enable();
        this.investigationPlanSearchFormGroup.get('taskagainst').enable();
        this.getTaskTypeDropdown('Investigation');
        this.taskTypeStatusDropdown('Investigation');
    }
    taskSearchBy(tasksearchby) {
        this.getTaskTypeDropdown(tasksearchby);
        this.taskTypeStatusDropdown(tasksearchby);
    }
    clearInvestigationSearch() {
        this.investigationPlanSearchFormGroup.patchValue({ invesplansearch: 'Activity', taskname: '', activityname: '', statustype: '' });
        this.searchByActivity();
        this.statusSearchForTask = false;
        this.getActivityTask(1, this.investigationId);
        this.getActivityGoal(this.investigationId);
    }
    searchInvestigationPlan() {
        this.getActivityTask(1, this.investigationId);
        this.getActivityGoal(this.investigationId);
    }
    private getActionSummary() {
        this.dsdsActionsSummary$ = this._commonHttpService
            .getById(this.daNumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl)
            .map((res) => {
                return res[0];
            })
            .share();
        this.dsdsActionsSummary$.subscribe((response) => {
            // this.investigationId = response['da_investigationid'];
            this.investigationId = response['da_investigationid']  ? response['da_investigationid'] : this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
            this.caseStatus = response['da_status'] === 'Closed' ? 'hide' : '';
            this.pageSubject$.subscribe((pageNumber) => {
                this.paginationInfo.pageNumber = pageNumber;
                this.getActivityTask(this.paginationInfo.pageNumber, this.investigationId);
            });
            this.getActivityTask(1, this.investigationId);
            this.getActivityGoal(this.investigationId);
            this.getInvestigationPlanSummary(this.investigationId);
            return response;
        });
    }

    getInvestigationPlanSummary(investigationid) {
        this.investigationPlanCaseCount$ = this._planSummaryService
            .getPagedArrayList(
                {
                    method: 'get',
                    where: {
                        investigationid: this.isServiceCase ?  ( this.dsdsActionsSummary.da_investigationid ? this.dsdsActionsSummary.da_investigationid : investigationid )  : investigationid
                    }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.PlanSummaryUrl + '?data'
            )
            .map((result) => {
                return result.data[0];
            })
            .share();
    }
    serachByActivityId(goal) {
        this.clearInvestigationSearch();
        this.activityGoal = Object.assign({}, goal);
        this.investigationPlanSearchFormGroup.patchValue({
            activityname: this.activityGoal.activityid
        });
        this.getActivityTask(1, this.investigationId);
    }
    sortactivity() {
        this.getActivityTask(1,this.investigationId);
    }

    resetSelectedActionType() {
        this.sortBy = false;
        this.selectedActionType = null;
        this.sortCategoryForm.controls['sortBy'].reset();
        this.getActivityTask(1,this.investigationId);
    }
    getActivityTask(page: number, investigationid) {
        this.paginationInfo.pageNumber = page;
        this.taskstatus = null;
        if (this.investigationPlanSearchFormGroup.value.invesplansearch === 'Task') {
            if (this.investigationPlanSearchFormGroup.value.statustype === '') {
                if (this.investigationPlanSearchFormGroup.value.taskagainst === 'Allegation') {
                    this.taskstatus = 'alleg';
                } else {
                    this.taskstatus = 'inv';
                }
            } else {
                this.taskstatus = this.investigationPlanSearchFormGroup.value.statustype;
            }
        } else {
            this.taskstatus = this.investigationPlanSearchFormGroup.value.statustype ? this.investigationPlanSearchFormGroup.value.statustype : null;
        }
        let requestParam;
        if (this.isServiceCase) {
            requestParam = { objectid: this.id, objecttypekey: 'servicecacse', sortBy: this.sortCategoryForm.value.sortBy,
            sortDir: this.sortCategoryForm.value.sortDir };
        } else {
            requestParam = {
                investigationid: this.isServiceCase ? this.dsdsActionsSummary.da_investigationid : investigationid,
                activitiesid: this.investigationPlanSearchFormGroup.value.activityname ? this.investigationPlanSearchFormGroup.value.activityname : null,
                taskid: this.investigationPlanSearchFormGroup.value.taskname ? this.investigationPlanSearchFormGroup.value.taskname : null,
                taskstatus: this.taskstatus,
                sortBy: this.sortCategoryForm.value.sortBy,
                sortDir: this.sortCategoryForm.value.sortDir
            };
        }
        const source = this._taskService
            .getPagedArrayList(
                {
                    method: 'get',
                    where: requestParam,
                    limit: 15,
                    page: this.paginationInfo.pageNumber
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivityTaskUrl + '?data'
            )
            .map((result) => {
                return {
                    data: result.data,
                    count: result.count,
                    canDisplayPager: result.count > this.paginationInfo.pageSize
                };
            })
            .share();
        this.activityTask$ = source.pluck('data');
        this.activityTask$.subscribe((res) => {
            if (res && res.length) {
                res.map((item) => {
                    if (item.activitytaskstatustypekey === 'InvClosed') {
                        item.isstatustype = false;
                    } else {
                        item.isstatustype = true;
                    }
                    return res;
                });
            }
            if (res && res.length) {
                this.activityTasks = res.map((item) => {
                    if (item.taskdescription === 'Project Home Registered Nurse Consultant Assessment  Quarterly/Interim Review') {
                        item.iseditable = true;
                    }
                    return item;
                });
            } else {
                this.activityTasks = [];
            }
            this._dataStoreService.setData('ACTIVITY_TASK', this.activityTasks, true, 'ACTIVITY_TASK_LOAD');
            this.setFormValues(1);
            console.log(res);
            const statusCalls = [];
            for (const id of res) {
                statusCalls.push(this.taskTypeStatusDropdown(id.activitytypekey).share());
            }
            this.forkedResult$ = Observable.forkJoin(statusCalls);
        });
        if (page === 1) {
            this.totalRecords$ = source.pluck('count');
            this.canDisplayPager$ = source.pluck('canDisplayPager');
        }
    }
    pageChanged(pageInfo: any) {
        this.paginationInfo.pageNumber = pageInfo.page;
        this.paginationInfo.pageSize = pageInfo.itemsPerPage;
        this.pageSubject$.next(this.paginationInfo.pageNumber);
    }

    getActivityGoal(investigationid) {
        const source = this._goalsService
            .getPagedArrayList(
                {
                    method: 'get',
                    where: {
                        investigationid: investigationid,
                        activitiesid: this.investigationPlanSearchFormGroup.value.activityname ? this.investigationPlanSearchFormGroup.value.activityname : null,
                        goalid: null
                    },
                    page: 1,
                    limit: 10
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivityGoalUrl + '?data'
            )
            .share();
        this.activityGoal$ = source.pluck('data');
    }

    loadDropdown() {
      this.sortDropDown = [ { 'value': 'task', 'text': 'Sort By Task' },
                            // { 'value': 'assignedon', 'text': 'Sort By Assigned Date' },
                            { 'value': 'duedate', 'text': 'Sort By Due Date' }
                            ];
        this.activityDropdownItems$ = Observable.empty();
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1, activitytypekey: 'Investigation' },
                    order: 'typedescription asc',
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivityGoalTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    where: { activeflag: 1, activitytypekey: 'Investigation' },
                    order: 'typedescription asc',
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivityGoalStatusTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    nolimit: true,
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.UserProfilekUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    goaltype: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.activitygoaltypekey
                            })
                    ),
                    goalstatus: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.activitygoalstatustypekey
                            })
                    ),
                    usersProfile: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.displayname,
                                value: res.loadnumber
                            })
                    )
                };
            })
            .share();
        this.goalTypeDropdownItems$ = source.pluck('goaltype');
        this.goalStatusTypeDropdownItems$ = source.pluck('goalstatus');
        this.activityDropdownItems$ = source.pluck('activity');
        this.usersProfileDropdownItems$ = source.pluck('usersProfile');
    }
    getActivityDropdown() {
        this.activityDropdownItems$ = Observable.empty();
        this.activityDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.InvestigationActivitiesUrl + '/' + this.id + '?filter'
            )
            .map((result) => {
                return result['investigation'][0]['activity'].map(
                    (res) =>
                        new DropdownModel({
                            text: res.description,
                            value: res.activityid
                        })
                );
            });
    }
    getDispositionStatus() {
        this._commonHttpService
            .getArrayList(
                {
                    method: 'get',
                    where: {
                        referencetypeid: 24,
                        teamtypekey: 'AS'
                    }
                },
                'referencetype/gettypes' + '?filter'
            )
            .subscribe((item) => {
                this.dispositionStatusDetails = item;
            });
    }
    getTaskTypeDropdown(activitytypekey) {
        this.taskDropdownItems$ = Observable.empty();
        this.taskDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    where: { activeflag: 1, activitytypekey: activitytypekey },
                    order: 'typedescription asc',
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivityAllTaskUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.activitytasktypekey
                        })
                );
            })
            .share();
    }
    taskTypeStatusDropdown(activitytypekey) {
        console.log(activitytypekey);
        return this._commonHttpService
            .getArrayList(
                {
                    where: { activeflag: 1, activitytypekey: activitytypekey },
                    order: 'typedescription asc',
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.TaskStatusTypeUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.activitytaskstatustypekey
                        })
                );
            });
    }

    getGoalDisposition(goaltypekey, goalstatustypekey) {
        this.activityGoalsFormGroup.patchValue({
            disposition: ''
        });
        this.dispositionDropdownItems$ = this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activitygoaltypekey: goaltypekey,
                        activitygoalstatustypekey: goalstatustypekey,
                        activitytypekey: 'Investigation'
                    },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.GoalDispositionUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.activitygoaldispositiontypekey
                        })
                );
            });
    }
    changeGoalStatusType() {
        this.getGoalDisposition(this.activityGoalsFormGroup.value.goalType, this.activityGoalsFormGroup.value.status);
    }
    getTaskDisposition(tasktypekey, taskstatustypekey) {
        return this._commonHttpService
            .getArrayList(
                {
                    where: {
                        activitytasktypekey: tasktypekey,
                        activitytaskstatustypekey: taskstatustypekey,
                        activitytypekey: 'Investigation'
                    },
                    method: 'get'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.InvestigationDispositionUrl + '?filter'
            )
            .map((result) => {
                return result.map(
                    (res) =>
                        new DropdownModel({
                            text: res.typedescription,
                            value: res.activitytaskdispositiontypekey
                        })
                );
            });
    }
    changeTaskStatusType() {
        this.getTaskDisposition(this.activityTask.activitytasktypekey, this.activityTaskFormGroup.value.activitytaskstatustypekey);
    }

    editGoal(currentGoal, editStatus) {
        this.activityGoal = Object.assign({}, currentGoal);
        if (editStatus === 1) {
            this.editLabel = 'Edit';
            this.populateActivityGoal(currentGoal);
            ControlUtils.enableElements($('#myModal-conclude-investigation').children());
        } else {
            this.editLabel = 'View';
            this.populateActivityGoal(currentGoal);
            ControlUtils.disableElements($('#myModal-conclude-investigation').children(), ['btnClose']);
        }
    }
    populateActivityGoal(goal) {
        this.getGoalDisposition(this.activityGoal.activitygoaltypekey, this.activityGoal.activitygoalstatustypekey);
        this.activityGoalsFormGroup.patchValue({
            goal: this.activityGoal.goal,
            activitygoalid: this.activityGoal.activitygoalid,
            goalType: this.activityGoal.activitygoaltypekey,
            required: this.activityGoal.required,
            duedate: new Date(this.activityGoal.duedate),
            status: this.activityGoal.activitygoalstatustypekey,
            disposition: this.activityGoal.activitygoaldispositiontypekey,
            completiondate: new Date(this.activityGoal.completiondate)
        });
        if (!this.activityGoal.completiondate) {
            this.activityGoalsFormGroup.patchValue({
                completiondate: this.activityGoal.completiondate
            });
        }
    }
    updateGoal() {
        this.activityGoal = Object.assign({}, new ActivityGoalModal());
        this.activityGoal.activitygoalid = this.activityGoalsFormGroup.value.activitygoalid;
        this.activityGoal.completiondate = this.activityGoalsFormGroup.value.completiondate;
        this.activityGoal.activitygoalstatustypekey = this.activityGoalsFormGroup.value.status;
        this.activityGoal.activitygoaldispositiontypekey = this.activityGoalsFormGroup.value.disposition;
        this.activityGoal.duedate = this.activityGoalsFormGroup.value.duedate;
        this._goalsService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.GoalUpdateUrl;
        this._goalsService.patch(this.activityGoal.activitygoalid, this.activityGoal).subscribe(
            (response) => {
                if (response) {
                    this._alertService.success('Goal updated successfully');
                    (<any>$('#myModal-conclude-investigation')).modal('hide');
                    this.getActivityTask(1, this.investigationId);
                    this.getActivityGoal(this.investigationId);
                }
            },
            (error) => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    enableAssignedTo() {
        this.activityTaskFormGroup.get('loadnumber').enable();
        this.activityTaskFormGroup.get('assignedon').enable();
    }
    editActivityTask(currentTask, editStatus) {
        this.activityTask = Object.assign({}, currentTask);
        if (editStatus === 1) {
            this.editLabel = 'Edit';
            this.populateActivityTask(currentTask);
            ControlUtils.enableElements($('#myModal-edit-goal-details').children());
            this.activityTaskFormGroup.get('activitytaskstatustypekey').enable();
            this.activityTaskFormGroup.get('activitytaskdispositiontypekey').enable();
        } else {
            this.editLabel = 'View';
            this.populateActivityTask(currentTask);
            ControlUtils.disableElements($('#myModal-edit-goal-details').children(), ['btnClose']);
            this.activityTaskFormGroup.get('activitytaskstatustypekey').disable();
            this.activityTaskFormGroup.get('activitytaskdispositiontypekey').disable();
        }
    }
    populateActivityTask(currentTask) {
        if (currentTask.tasktype === 'Contact') {
            this.activityTaskFormGroup.get('loadnumber').disable();
            this.showTime = true;
            if (this.activityTask.startdatetime !== null) {
                (this.startdate = new Date(this.activityTask.startdatetime).toLocaleDateString()),
                    (this.starttime = new Date(this.activityTask.startdatetime).getHours() + ':' + new Date(this.activityTask.startdatetime).getMinutes()),
                    (this.starttimeformat = new Date(this.activityTask.startdatetime).getHours() >= 12 ? 'PM' : 'AM');
            }
            if (this.activityTask.enddatetime !== null) {
                (this.enddate = new Date(this.activityTask.enddatetime).toLocaleDateString()),
                    (this.endtime = new Date(this.activityTask.enddatetime).getHours() + ':' + new Date(this.activityTask.enddatetime).getMinutes()),
                    (this.endtimeformat = new Date(this.activityTask.enddatetime).getHours() >= 12 ? 'PM' : 'AM');
            }
        } else {
            this.showTime = false;
        }
        this.getTaskTypeDropdown(this.activityTask.activitytypekey);
        this.taskTypeStatusDropdown(this.activityTask.activitytypekey);
        this.getTaskDisposition(this.activityTask.activitytasktypekey, this.activityTask.activitytaskstatustypekey);
        this.activityTaskFormGroup.patchValue(this.activityTask);
        if (currentTask.tasktype === 'Contact') {
            this.activityTaskFormGroup.patchValue({
                startdate: currentTask.tasktype === 'Contact' ? this.startdate : null,
                starttime: currentTask.tasktype === 'Contact' ? this.starttime : null,
                starttimeformat: currentTask.tasktype === 'Contact' ? this.starttimeformat : null,
                enddate: currentTask.tasktype === 'Contact' ? this.enddate : null,
                endtime: currentTask.tasktype === 'Contact' ? this.endtime : null,
                endtimeformat: currentTask.tasktype === 'Contact' ? this.endtimeformat : null,
                insertedon: new Date(this.activityTask.insertedon)
            });
        }
    }
    updateActivityTask() {
        if (this.activityTaskFormGroup.dirty && this.activityTaskFormGroup.valid) {
            this.activityTask = Object.assign({}, this.activityTaskFormGroup.value);
            this.activityTask.assignedto = this.activityTaskFormGroup.value.loadnumber;
            if (this.activityTaskFormGroup.value.startdate) {
                this.activityTask.startdatetime = this.datePipe.transform(this.activityTaskFormGroup.value.startdate, 'yyyy-MM-dd') + ' ' + this.activityTaskFormGroup.value.starttime;
                this.activityTask.enddatetime = this.datePipe.transform(this.activityTaskFormGroup.value.enddate, 'yyyy-MM-dd') + ' ' + this.activityTaskFormGroup.value.endtime;
            }
            ObjectUtils.removeEmptyProperties(this.activityTask);
            this.activityTask.required = this.activityTaskFormGroup.get('required').value;
            this._taskService.endpointUrl = CaseWorkerUrlConfig.EndPoint.DSDSAction.InvestigationPlan.ActivityTaskUpdateUrl;
            this._taskService.patch(this.activityTaskFormGroup.value.activitytaskid, this.activityTask).subscribe(
                (response) => {
                    if (response) {
                        this._alertService.success('Activity Task updated successfully');
                        (<any>$('#myModal-edit-goal-details')).modal('hide');
                        this.getActivityTask(this.paginationInfo.pageNumber, this.investigationId);
                        this.getActivityGoal(this.investigationId);
                        this.getInvestigationPlanSummary(this.investigationId);
                    }
                },
                (error) => {
                    this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                }
            );
        } else {
            this._alertService.error('Please fill mandatory fields');
        }
    }
    routeToChecklist(da_investigationid: any) {
        const currentUrl = '/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/investigation-plan/plan-suggested/' + da_investigationid;
        this._router.navigate([currentUrl]);
    }
    navigateToTask(type) {
        if (type === 'Assessment') {
            $('#adultassessmentsTab').click();
            // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/adult-assessment']);
        }
        if (type === 'Placement') {
            $('#placementTab').click();
            // this._router.navigate(['/pages/case-worker/' + this.id + '/' + this.daNumber + '/dsds-action/adult-placement']);
        }
    }
}
