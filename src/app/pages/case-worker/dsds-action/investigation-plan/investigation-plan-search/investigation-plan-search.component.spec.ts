import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaginationModule } from 'ngx-bootstrap';

import { CoreModule } from '../../../../../@core/core.module';
import { ControlMessagesModule } from '../../../../../shared/modules/control-messages/control-messages.module';
import { InvestigationPlanSearchComponent } from './investigation-plan-search.component';

describe('InvestigationPlanSearchComponent', () => {
    let component: InvestigationPlanSearchComponent;
    let fixture: ComponentFixture<InvestigationPlanSearchComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, CoreModule.forRoot(), HttpClientModule, FormsModule, ReactiveFormsModule, ControlMessagesModule, PaginationModule, NgSelectModule],
            declarations: [InvestigationPlanSearchComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(InvestigationPlanSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
