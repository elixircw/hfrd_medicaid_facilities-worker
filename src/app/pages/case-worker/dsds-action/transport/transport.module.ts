import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatTabsModule,
  MatCheckboxModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatExpansionModule,
  MatAutocompleteModule
} from '@angular/material';

import { TransportRoutingModule } from './transport-routing.module';
import { TransportComponent } from './transport.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TransportAddEditComponent } from './transport-add-edit/transport-add-edit.component';
import { CommonControlsModule } from '../../../../shared/modules/common-controls/common-controls.module';

@NgModule({
  imports: [
    CommonModule,
    TransportRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    CommonControlsModule
  ],
  declarations: [TransportComponent, TransportAddEditComponent]
})
export class TransportModule { }
