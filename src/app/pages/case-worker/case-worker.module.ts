import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { A2Edatetimepicker } from 'ng2-eonasdan-datetimepicker';
import { SharedDirectivesModule } from '../../@core/directives/shared-directives.module';
import { SharedPipesModule } from '../../@core/pipes/shared-pipes.module';
import { SortTableModule } from '../../shared/modules/sortable-table/sortable-table.module';
import { CaseWorkerRoutingModule } from './case-worker-routing.module';
import { CaseWorkerComponent } from './case-worker.component';
import { DashboardComponent } from './dashboard.component';
import { FormLetterComponent } from './form-letter/form-letter.component';
import { NotificationComponent } from './notification/notification.component';
import { RoutingComponent } from './routing/routing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxfUploaderService, NgxfUploaderModule } from 'ngxf-uploader';
import { QuillModule } from 'ngx-quill';
import { IntakeUtils } from '../_utils/intake-utils.service';
import { CaseWorkerResolverService } from './case-worker-resolver.service';
import { DsdsService } from './dsds-action/_services/dsds.service';
import { FormMaterialModule } from '../../@core/form-material.module';
import { CaseConnectedComponent } from './dsds-action/case-connected/case-connected.component'

@NgModule({
    imports: [CommonModule,
        CaseWorkerRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        SortTableModule,
        A2Edatetimepicker,
        SharedPipesModule,
        SharedDirectivesModule,
        QuillModule,
        FormMaterialModule,
        NgxfUploaderModule.forRoot()],
    declarations: [CaseWorkerComponent, RoutingComponent, NotificationComponent, FormLetterComponent, DashboardComponent, CaseConnectedComponent],
    providers: [NgxfUploaderService, IntakeUtils, CaseWorkerResolverService, DsdsService]
})
export class CaseWorkerModule { }
