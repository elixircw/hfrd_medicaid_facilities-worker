import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService, CommonHttpService, DataStoreService, SessionStorageService } from '../../@core/services';
import { CaseWorkerUrlConfig } from './case-worker-url.config';
import { GLOBAL_MESSAGES } from '../../@core/entities/constants';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from './_entities/caseworker.data.constants';

@Injectable()
export class CaseWorkerResolverService {
  daNumber: string;
  caseuid: string;
  isServiceCase: any;
  isAdoptionCase: any;
  constructor(private _authService: AuthService,
     private _commonHttpService: CommonHttpService,
      private _dataStoreService: DataStoreService, private _session: SessionStorageService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.daNumber = route.paramMap.get('daNumber');
    this.caseuid = route.paramMap.get('id');
    this.isServiceCase = this._session.getItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
    this._dataStoreService.setData(CASE_STORE_CONSTANTS.DA_NUMBER, this.daNumber);
    this._dataStoreService.setData(CASE_STORE_CONSTANTS.CASE_UID, this.caseuid);
    this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, this.isServiceCase);
    if (this.isServiceCase === 'true') {
      this._dataStoreService.setData(CASE_STORE_CONSTANTS.CASE_TYPE, CASE_TYPE_CONSTANTS.SERVICE_CASE);
    }
    const caseType = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_TYPE);
    if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
        this.isAdoptionCase = true;
    }
    console.log(this.daNumber, this.caseuid);
    return this.getActionSummary();
  }

  private getActionSummary() {
    let url = '';
    console.log('cwr getActionSummary',this.caseuid );
    if (this.isServiceCase || this.isAdoptionCase) {
      url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + this.caseuid + '/casetype';
    } else {
      url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + this.daNumber;
    }
    return this._commonHttpService.getAll(url);
  }
}
