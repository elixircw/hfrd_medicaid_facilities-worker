import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CommonHttpService } from '../../@core/services/common-http.service';
import { CaseWorkerUrlConfig } from './case-worker-url.config';
import { DSDSActionSummary, ServiceCaseResponsbility } from './_entities/caseworker.data.model';
import {
    InvolvedPerson,
} from './dsds-action/involved-persons/_entities/involvedperson.data.model';
import * as _ from 'lodash';

import { ActivatedRoute, Router } from '@angular/router';
import { AlertService, AuthService, CommonDropdownsService } from '../../@core/services';
import { GLOBAL_MESSAGES } from '../../@core/entities/constants';
import { DataStoreService } from '../../@core/services/data-store.service';
import { CommonUrlConfig } from '../../@core/common/URLs/common-url.config';
import { IntakeStoreConstants } from '../newintake/my-newintake/my-newintake.constants';
import { IntakeUtils } from '../_utils/intake-utils.service';
import { GenericService } from '../../@core/services/generic.service';
import { Subscription } from 'rxjs/Subscription';
import { SessionStorageService } from '../../@core/services/storage.service';
import { DsdsService } from './dsds-action/_services/dsds.service';
import { PaginationRequest } from '../../@core/entities/common.entities';
import { CASE_STORE_CONSTANTS, CASE_TYPE_CONSTANTS } from './_entities/caseworker.data.constants';
import { AppConstants } from '../../@core/common/constants';
import { RoutingUser } from '../cjams-dashboard/_entities/dashBoard-datamodel';
import { AppUser } from '../../@core/entities/authDataModel';

declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'case-worker',
    templateUrl: './case-worker.component.html',
    styleUrls: ['./case-worker.component.scss']
})
export class CaseWorkerComponent implements OnInit, AfterViewInit, OnDestroy {
    dsdsActionsSummary = new DSDSActionSummary();
    id: string;
    daNumber: string;
    reporterName: string;
    youthStatus: string;
    isDjs: boolean;
    legalGuardian: string;
    involvedPerson$: Observable<InvolvedPerson[]>;
    involvedPerson: any[];
    store: any;
    isCW: boolean;
    datastoreSubscription: Subscription;
    isServiceCase: string;
    serviceprogramkey: any[] = [];
    familyworker: ServiceCaseResponsbility;
    childworker: ServiceCaseResponsbility;
    isrestricteditem = false;
    isAdoptionCase = false;
    servicedays: number;
    selectedServiceCaseId: string = null;
    serviceCaseNumber: any;
    exitingServiceCaseList: Array<any> = [];
    adoptionStartDate: any;
    isCaseWorker = false;
    isSupervisor = false;
    canUserRestrictItems = false;
    supervisorsList: any[];
    selectedSupervisor: string;
    caseworkerlist: any[];
    roleId: AppUser;

    cpsResponseOffset = 0;
    sdmInfo: any;
    selectedteamid: string;
    selectedcaseworkerlist: any[];
    administrativeWorker: ServiceCaseResponsbility;
    headofHousehold: string;
    workerPhoneNumber: string;
    valueCaseStatusDate='';
    labelCaseStatusDate='Opened On :';
    valueCaseCloseDate='';
    constructor(
        private _service: GenericService<InvolvedPerson>,
        private route: ActivatedRoute,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _dsdsActionService: CommonHttpService,
        private _dataStoreService: DataStoreService,
        private _intakeService: IntakeUtils,
        private storage: SessionStorageService,
        private _dsdsService: DsdsService,
        private _router: Router,
        private _commonHttpService: CommonHttpService,
        private _intakeUtils: IntakeUtils,
        private _commonDDService: CommonDropdownsService
    ) {
        this.route.data.subscribe(data => {
            console.log('case worker resover componet ts ', data);
            this.isServiceCase = this.storage.getItem('ISSERVICECASE');
            this.setActionSummary(data.intakesummary);
            this.isCaseWorker = this._authService.selectedRoleIs(AppConstants.ROLES.CASE_WORKER);
            this.isSupervisor = this._authService.selectedRoleIs(AppConstants.ROLES.SUPERVISOR);
            const intakesummary = (Array.isArray(data.intakesummary) && data.intakesummary.length) ? data.intakesummary[0] : null;
            if (intakesummary) {
                this._dataStoreService.setData(CASE_STORE_CONSTANTS.CPS_CASE_ID, intakesummary.intakeserviceid);
            }
        });

    }

    ngOnInit() {
        this.selectedSupervisor = null;
        this.selectedcaseworkerlist = [];
        this.isDjs = this._authService.isDJS();
        this.isCW = this._authService.isCW();
        this.roleId = this._authService.getCurrentUser();
        this.selectedteamid = this.roleId.user.userprofile.teammemberassignment.teammember.teamid;
        this.datastoreSubscription = this._dataStoreService.currentStore.subscribe(storeObj => {
            if (storeObj[IntakeStoreConstants.statusChanged] && this.isDjs) {
                this._dataStoreService.setData(IntakeStoreConstants.statusChanged, false);
                this.getYouthStaus();
            }
            if (storeObj[CASE_STORE_CONSTANTS.FOLDER_CHANGED] && this.isDjs) {
                this._dataStoreService.setData(CASE_STORE_CONSTANTS.FOLDER_CHANGED, false);
                this.getActionSummary();
            }
            if (storeObj['DSDS_ACTION_UPDATE']) {
                this._dataStoreService.setData('DSDS_ACTION_UPDATE', false);
                this.getActionSummary();
            }
        });
        this.id = this._dataStoreService.getData(CASE_STORE_CONSTANTS.CASE_UID);
        this.daNumber = this._dataStoreService.getData(CASE_STORE_CONSTANTS.DA_NUMBER);
        const caseType = this.storage.getItem(CASE_STORE_CONSTANTS.CASE_TYPE);
        if (caseType === CASE_TYPE_CONSTANTS.ADOPTION) {
            this.isAdoptionCase = true;
            this.adoptionStartDate = this.storage.getItem(CASE_STORE_CONSTANTS.Adoption_START_DATE);
            this.calculatedays(this.adoptionStartDate);
        }
        if (this.daNumber) {
            this.getActionSummary();
        }

        // Decide who can restrict items
        if (this.isSupervisor) {
            this.canUserRestrictItems = true;
        }

        if (this.id) {
            this.getIsRestrictItem();
            this.restrictedItemAuditLog();
        }
        this._intakeUtils.notesUpdated$.subscribe(_ => {
            this.processRecordingsList();
        });
    }

    ngAfterViewInit() {
        if (this._authService.isDevice()) {
            $('#cw-profile-section').hide();
            $('#cw-groupid-display').hide();
            $('.scrtabs-tab-container').hide();
            $('#header').hide();
        }
    }

    getYouthStaus() {
        this._intakeService.getFocusPersonStatus(this.dsdsActionsSummary.personid, this.id, this.dsdsActionsSummary.intakenumber)
            .subscribe(statuses => {
                const status = statuses.map(s => s.description);
                this.youthStatus = status.toString();
                this._dataStoreService.setData(IntakeStoreConstants.youthStatus, status);
            });
    }

    changeFolder() {
        this._dataStoreService.setData(CASE_STORE_CONSTANTS.FOLDER_CHANGE_INITIATED, true);
        (<any>$('#folder-change')).modal('show');
    }

    getcaseworkerlist() {
        let appEvent = 'INVR';
        if (this.roleId.role.name === AppConstants.ROLES.KINSHIP_INTAKE_WORKER || this.roleId.role.name === AppConstants.ROLES.KINSHIP_SUPERVISOR) {
            appEvent = 'KINR';
        }
        this._service
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: appEvent , teamid: this.selectedteamid || null},
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe((result) => {
                this.caseworkerlist = result.data;
                this.caseworkerlist = this.caseworkerlist.filter((res) => {
                    if (res.issupervisor === false) {
                        // this.isSupervisor = false;
                        return res;
                    }
                });
            });
    }

    private getInvolvedPerson() {
        let inputRequest: Object;
        if (this._dsdsService.isServiceCase()) {
            inputRequest = {
                objectid: this.id,
                objecttypekey: 'servicecase'
            };
        } else {
            inputRequest = {
                intakeserviceid: this.id
            };
        }
        this.involvedPerson$ = this._service
            .getPagedArrayList(
                new PaginationRequest({
                    page: 1,
                    limit: 20,
                    method: 'get',
                    where: inputRequest
                }),
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson
                    .PersonList + '?filter'
            )
            .share()
            .pluck('data');
        this.involvedPerson$.subscribe((data) => {
            this.involvedPerson = data;
            this.processLegalGuardian();
            this.loadSDMData();

        });
    }

    private getActionSummary() {
        this.serviceprogramkey = [];
        let url = '';
        if (this.isServiceCase && this.isServiceCase !== 'false') {
            url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + this.id + '/casetype';
        } else if(this.isAdoptionCase) {
            url = CaseWorkerUrlConfig.EndPoint.Dashboard.AdoptionActionSummary + '/' + this.id ;
        } else {
            url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + this.daNumber;
        }
        this._dsdsActionService.getAll(url).subscribe(
            (response) => {
                this.dsdsActionsSummary = response[0];
                //console.log("DSDSACTION SUMMARY", this.dsdsActionsSummary);
                if (this.dsdsActionsSummary) {
                    if (this.dsdsActionsSummary.intake_jsondata && this.dsdsActionsSummary.intake_jsondata.narrative &&
                        this.dsdsActionsSummary.intake_jsondata.narrative.length) {
                        this.reporterName = this.dsdsActionsSummary.intake_jsondata.narrative[0].Firstname + ' ' + this.dsdsActionsSummary.intake_jsondata.narrative[0].Lastname;
                        if (!this.reporterName) {
                            this.reporterName = '';
                        } else if (!this.reporterName.replace(/\s/g, '').length) {
                            this.reporterName = '';
                        }
                    }
                    if (this.dsdsActionsSummary.programarea && this.dsdsActionsSummary.programarea.length) {
                        this.dsdsActionsSummary.programarea.forEach(element => {
                            this.serviceprogramkey.push(element);
                        });
                        // this.programkey = this.serviceprogramkey ? this.serviceprogramkey.toString() : '';
                    }
                    
                    if (_.has(this.dsdsActionsSummary,'case_closedate') && _.has(this.dsdsActionsSummary,'case_opendate') ) {
                        if(this.dsdsActionsSummary['case_closedate'] != null) {
                            var open = (new Date(this.dsdsActionsSummary['case_opendate'])).getTime();
                            var close = (new Date(this.dsdsActionsSummary['case_closedate'])).getTime();
                            console.log("open",open);
                            console.log("close",close);
                            if( open > close)
                            {
                                this.labelCaseStatusDate = 'Reopened On :'
                                this.valueCaseStatusDate = this.dsdsActionsSummary['case_opendate'];
                                this.valueCaseCloseDate = '';
                            }
                            else{
                                this.labelCaseStatusDate = 'Opened On :'
                                this.valueCaseStatusDate = this.dsdsActionsSummary['case_opendate'];
                                this.valueCaseCloseDate =  this.dsdsActionsSummary['case_closedate'];
                            }
                        }
                        else{
                            this.labelCaseStatusDate = 'Opened On :'
                            this.valueCaseStatusDate = this.dsdsActionsSummary['case_opendate'];
                            this.valueCaseCloseDate =  '';
                        }
                    }
                    if (this.dsdsActionsSummary.responsibleworkers && Array.isArray(this.dsdsActionsSummary.responsibleworkers) && this.dsdsActionsSummary.responsibleworkers.length) {
                        this.familyworker = this.dsdsActionsSummary.responsibleworkers.find(worker => worker.responsibilitytypekey === 'family');
                        this.childworker = this.dsdsActionsSummary.responsibleworkers.find(worker => worker.responsibilitytypekey === 'child');
                        this.administrativeWorker = this.dsdsActionsSummary.responsibleworkers.find(worker => worker.responsibilitytypekey === 'administrative');
                        this.workerPhoneNumber = this.dsdsActionsSummary.responsibleworkers[0].phonenumber;
                    }
                    this.getLegalGuardian();
                    this._dataStoreService.setData('object', this.dsdsActionsSummary);
                    this._dataStoreService.setData('da_status', this.dsdsActionsSummary.da_status);
                    this.storage.setItem('da_status', this.dsdsActionsSummary.da_status)
                    this._dataStoreService.setData('teamtypekey', this.dsdsActionsSummary.teamtypekey);
                    this._dataStoreService.setData('teamid', this.dsdsActionsSummary.teamid);
                    this._dataStoreService.setData('dsdsActionsSummary', this.dsdsActionsSummary);
                    this._dataStoreService.setData('youthFullName', this.dsdsActionsSummary.da_lastname + ' ' + this.dsdsActionsSummary.da_suffix + ', ' + this.dsdsActionsSummary.da_firstname);
                    this._dataStoreService.setData('da_legalGuardian', this.dsdsActionsSummary.da_legalGuardian);
                    this._dataStoreService.setData('da_focus', this.dsdsActionsSummary.da_focus);
                    this._dataStoreService.setData('da_personid', this.dsdsActionsSummary.personid);
                    this._dataStoreService.setData('da_persondob', this.dsdsActionsSummary.persondob);
                    this._dataStoreService.setData('da_foldertypedecription', this.dsdsActionsSummary.foldertypedescription);
                    this._dataStoreService.setData('da_foldertypekey', this.dsdsActionsSummary.foldertypekey);
                    this._dataStoreService.setData('da_intakenumber', this.dsdsActionsSummary.intakenumber);
                    this._dataStoreService.setData('countyid', this.dsdsActionsSummary.countyid);
                    this._dataStoreService.setData('programarea', this.dsdsActionsSummary.programarea);
                    this._dataStoreService.setData(CASE_STORE_CONSTANTS.CPS_CASE_ID, this.dsdsActionsSummary.intakeserviceid);
                    if (this.isDjs) {
                        this.getYouthStaus();
                    }
                } else {
                    this._alertService.error('DSDS Action Summary is Empty, Please Check Your Data.');
                }
            },
            error => {
                this._alertService.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
            }
        );
    }
    loadSupervisor() {
        this._dsdsActionService
            .getPagedArrayList(
                new PaginationRequest({
                    where: { appevent: 'INTR' },
                    method: 'post'
                }),
                'Intakedastagings/getroutingusers'
            )
            .subscribe(result => {
                this.supervisorsList = result.data;
                (<any>$('#list-supervisor')).modal('show');
            });
    }
    getAppEventCode() {
        let appeventcode = 'INVT';
        if (this.isAdoptionCase) {
            appeventcode = 'ADPC'
        }
        if (this.isServiceCase) {
            appeventcode = 'SRVC'
        }
        return appeventcode;
    }
    updateReviewer() {
        if (this.selectedSupervisor) {
            this._dsdsActionService.create({
                appeventcode: this.getAppEventCode(),
                objectid: this.id,
                fromuserid: this.selectedSupervisor
            },
            'routing/changereviewer'
        )
        .subscribe(result => {
            this.selectedSupervisor = null;
            this._alertService.success('Case Assign Successfully Completed');
            (<any>$('#list-supervisor')).modal('hide');
            this.getActionSummary();
        });
    } else {
        this._alertService.warn('Please Select Supervisor');
    }
}
    onChangeSupervisor(supervisor: RoutingUser) {
        console.log(supervisor);
        this.selectedSupervisor = supervisor.userid;
    }
    private setActionSummary(intakesummary) {
        if (intakesummary && intakesummary.length) {
            const data = intakesummary[0];
            this.getLegalGuardian();
            this._dataStoreService.setData('da_status', data.da_status);
            this._dataStoreService.setData('teamtypekey', data.teamtypekey);
            this._dataStoreService.setData('dsdsActionsSummary', data);
            this._dataStoreService.setData('da_focus', data.da_focus);
            this._dataStoreService.setData('da_legalGuardian', data.da_legalGuardian);
            this._dataStoreService.setData('da_personid', data.personid);
            this._dataStoreService.setData('da_persondob', data.persondob);
            this._dataStoreService.setData('da_foldertypedecription', data.foldertypedescription);
            this._dataStoreService.setData('da_foldertypekey', data.foldertypekey);
            this._dataStoreService.setData('da_intakenumber', data.intakenumber);
            this._dataStoreService.setData('da_typeid', data.da_typeid);
            this._dataStoreService.setData('da_type', data.da_type);
            this._dataStoreService.setData('da_subtypeid', data.da_typeid);
            this._dataStoreService.setData('da_receiveddate', data.da_receiveddate);
            this._dataStoreService.setData('programarea', data.programarea);
            if (this.isServiceCase) {
                this.calculatedays(data.da_receiveddate);
            }
            if (this.isDjs) {
                this.getYouthStaus();
            }
        }
    }
    calculatedays(recieveddate) {
        const date2 = new Date(recieveddate);
        const date1 = new Date();
        const diff = Math.abs(date1.getTime() - date2.getTime());
        this.servicedays = Math.ceil(diff / (1000 * 3600 * 24));
    }

    private getLegalGuardian() {
        if (this.involvedPerson && this.involvedPerson.length > 0) {
            this.processLegalGuardian();
        } else {
            this.getInvolvedPerson();
        }
    }

    processLegalGuardian() {
        const addedPersons = this.involvedPerson;
        let personFullName = '';
        if (addedPersons && addedPersons.length > 0) {
            addedPersons.map(item => {
                if (item.rolename === 'LG' && personFullName === '') {
                    personFullName = item.firstname + ' ' + item.lastname;
                    this.dsdsActionsSummary.da_legalGuardian = personFullName;
                    this._dataStoreService.setData('dsdsActionsSummary', this.dsdsActionsSummary);
                    this._dataStoreService.setData('da_legalGuardian', personFullName);
                    // return personFullName;
                }
                if (item.isheadofhousehold === true) {
                    this.headofHousehold = item.fullname;
                }
            });

        }
    }

    ngOnDestroy(): void {
        // this._sessionStorage.removeItem('intake');
        if (this.datastoreSubscription) {
            this.datastoreSubscription.unsubscribe();
        }
    }
    linkToServiceCase() {
        this._dsdsActionService
            .getArrayList(
                {
                    where: { intakeserviceid: this.id, source: 'cps' },
                    method: 'get'
                },
                'intakeservreqchildremoval/servicecasevalidation?filter'
            ).subscribe((result: any) => {
                if (result.isavailable === 1) {
                    this.openHistoryOfFamilyCase(result.data);
                    // let servicecaseno = '';
                    // result.data.forEach(element => {
                    //     if (servicecaseno === '') {
                    //         servicecaseno = element.servicecasenumber;
                    //     } else {
                    //         servicecaseno = servicecaseno + ', ' + element.servicecasenumber;
                    //     }
                    // });
                    // this.getActionSummary();
                    // this._alertService.success('Already you linked to ' + servicecaseno);
                } else {
                    this.getActionSummary();
                    this._alertService.success('Successfully Linked to Service Case');
                    const routingObject = {
                        objectid: this.id,
                        eventcode: 'SCCR',
                        status: 'Accepted',
                        intakeserviceid: this.id
                    };

                    this._dsdsActionService.create(routingObject, 'routing/routingupdate').subscribe((res) => {
                        this._alertService.success('Service case created: ' + this.serviceCaseNumber);
                        (<any>$('#confirm-navigation-assign-service-case')).modal('show');
                    }, (error) => {
                        console.log(error);
                    });
                }
            });
    }

    // Saran

    selectCase(selectedcase,isRedirect) {
        if (selectedcase === 'NEW_CASE') {
            this.selectedServiceCaseId = 'NEW_CASE';
        } else {
            this.selectedServiceCaseId = selectedcase.servicecaseid;

            this.storage.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
            if (selectedcase) {
                this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, selectedcase.objecttypekey);
            }
            const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + selectedcase.servicecaseid + '/casetype';
            this._dsdsActionService.getAll(url).subscribe((response) => {
                const dsdsActionsSummary = response[0];
                if (dsdsActionsSummary) {
                    this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                    this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                    // common person for cw
                    const currentUrl = '#/pages/case-worker/' + selectedcase.servicecaseid + '/' + selectedcase.servicecasenumber + '/dsds-action/person-cw';
                    if(isRedirect) {
                    window.open(currentUrl);
                    }
                    this.storage.removeItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE);
                    this._dataStoreService.clearStore();
                    this._dataStoreService.clearStore();
                    this._dataStoreService.clearStore();
                }
            });
        }
    }

    createOrMergeServiceCase() {
        const serviceCaseData = {
            servicecaseid: this.selectedServiceCaseId === 'NEW_CASE' ? null : this.selectedServiceCaseId,
            intakeserviceid: this.id,
            isnewcase: this.selectedServiceCaseId === 'NEW_CASE' ? 1 : 0,
            subtypekey: 'IHM', // INHOME SERVICES,
            source: 'intake'
        };
        this._dsdsActionService
            .create(serviceCaseData, 'servicecase/createservicecase')
            .subscribe(response => {
                console.log('service case create', response);
                this.serviceCaseNumber = response[0].servicecaseno;

                // not sure what these are doing
                this.hideHistoryOfFmailyCase();
                this.getActionSummary();
                // end : not sure what these are doing

                // this._alertService.success('Service case created: ' + this.serviceCaseNumber);

                const routingObject = {
                    objectid: this.id,
                    eventcode: 'SCCR',
                    status: 'Accepted',
                    intakeserviceid: this.id
                };

                this._dsdsActionService.create(routingObject, 'routing/routingupdate').subscribe((res) => {

                    if (this.selectedServiceCaseId === 'NEW_CASE') {
                        this._alertService.success('Service case created: ' + this.serviceCaseNumber);
                        (<any>$('#confirm-navigation-assign-service-case')).modal('show');
                    } else {
                        this._alertService.success('Service case connected: ' + this.serviceCaseNumber);
                    }
                }, (error) => {
                    console.log(error);
                });


                // this.openServiceCaseAcknowledge(response[0].servicecaseno);
            });

    }

    openServiceCaseAcknowledge(serviceCaseNumber) {
        this.serviceCaseNumber = serviceCaseNumber;
        (<any>$('#approve-intake-ackmt-service-case')).modal('show');
    }
    openHistoryOfFamilyCase(caseList) {
        this.exitingServiceCaseList = caseList;
        (<any>$('#serviceCaseHistory')).modal('show');
    }

    hideHistoryOfFmailyCase() {
        (<any>$('#serviceCaseHistory')).modal('hide');
    }
    // (click)="routToCaseWorker1(dsdsActionsSummary)"
    routToCaseWorker1(item) {
        // console.log(item);
        this.storage.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        if (item) {
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, item.objecttypekey);
        }
        const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.servicecaseid + '/casetype';
        this._dsdsActionService.getAll(url).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
                this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                this._dataStoreService.setData('dsdsActionsSummary', dsdsActionsSummary);
                this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                const currentUrl = '/pages/case-worker/' + item.servicecaseid + '/' + item.servicecasenumber + '/dsds-action/person-cw';
                // this._router.navigate([currentUrl]);
                this._router.navigateByUrl('/', { skipLocationChange: true }).then(() => this._router.navigate([currentUrl]));
            }
        });
    }

    getIsRestrictItem() {
        this._intakeService.isRestrictedItem(this.id)
            .subscribe(
                (response) => {
                    if (response.length > 0) {
                        this.isrestricteditem = true;
                    }
                }
            );
    }

    restrictedItemAuditLog() {
        this._intakeService.restrictedItemAuditLog(this.id)
            .subscribe(
                (response) => {

                }
            );
    }

    confirmRestrictItem() {
        const userinfo = this._authService.getCurrentUser();
        let activeflag = 0 ;
        if (!this.isrestricteditem) {
        activeflag = 1;
        }
        this.selectedcaseworkerlist.push(userinfo.user.securityusersid);
        this._intakeService.createRestrictedItem(this.id, 'SERVICE', this.selectedcaseworkerlist, activeflag)
            .subscribe(
                (response) => {
                    // If originally false then we just successfully applied restriction
                    if (!this.isrestricteditem) {
                        this._alertService.success('Restriction applied successfully.');
                    } else {
                        this._alertService.success('Restriction removed successfully.');
                    }
                    // Revert the restriction flag
                    this.isrestricteditem = !this.isrestricteditem;
                    (<any>$('#confirm-restrict-item')).modal('hide');
                    this.selectedcaseworkerlist = [];
                },
                (error) => {
                    this._alertService.warn('Error in updating restriction.');
                }
            );
    }


    cancelRestrictItem() {
        // this.returnFormGroup.reset();
        (<any>$('#confirm-restrict-item')).modal('hide');
        this.selectedcaseworkerlist = [];
    }

    cancelRequestCase() {
        (<any>$('#confirm-request-case')).modal('hide');
    }
    confirmRequestCase() {
        console.log('TODO: Call to request a case for supervisor');
        const routingObject = {
            objectid: this.id,
            eventcode: 'SCCR',
            status: 'Review',
            intakeserviceid: this.id
        };

        /*
        comments: "Case worker has requested to open a in-home service case.",
        notifymsg: 'Notify message',
        routeddescription: 'routing description'
        */
        this._dsdsActionService.create(routingObject, 'routing/routingupdate').subscribe((res) => {
            this._alertService.success('Successfully Requested to Open a Service Case.');
            this.routeIntakeServiceRequestToCaseWorker1(this.daNumber);
        }, (error) => {
            console.log(error);
        });


        (<any>$('#confirm-request-case')).modal('hide');
        this.hideHistoryOfFmailyCase();
    }

    closeRequestCaseAndRoute(event: any) {
        if (event.selecteditemtype === 'SERVICE_CASE') {
            this.routeServiceCaseToCaseWorker(event);
        } else if (event.selecteditemtype === 'INTAKE_SERVICE_REQUEST') {
            this.routeToIntakeServiceRequestFromRelatedCases(event);
        } else if (event.selecteditemtype === 'INTAKE') {
            this.routeToIntakeFromRelatedCases(event);
        }
    }

    routeToIntakeFromRelatedCases(item: any) {
        (<any>$('#confirm-request-case')).modal('hide');
        this._intakeService.redirectIntake(item.intakenumber);
    }

    routeToIntakeServiceRequestFromRelatedCases(item: any) {
        (<any>$('#confirm-request-case')).modal('hide');
        this._dsdsActionService.getById(item.servicerequestnumber, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
                this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                const currentUrl = '/pages/case-worker/' + item.intakeserviceid + '/' + item.servicerequestnumber + '/dsds-action/report-summary';
                // this._router.navigate([currentUrl]);
                this._router.navigateByUrl('/', { skipLocationChange: true }).then(() => this._router.navigate([currentUrl]));
            }
        });
    }

    routeIntakeServiceRequestToCaseWorker1(item: any) {
        (<any>$('#confirm-request-case')).modal('hide');
        this._dsdsActionService.getById(item, CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
                this.dsdsActionsSummary.caseconnectsent = dsdsActionsSummary.caseconnectsent;
            }
        });
    }

    routeServiceCaseToCaseWorker(item: any) {
        (<any>$('#serviceCaseHistory')).modal('hide');
        (<any>$('#confirm-request-case')).modal('hide');
        this.storage.setItem(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, true);
        if (item) {
            this._dataStoreService.setData(CASE_STORE_CONSTANTS.IS_SERVICE_CASE, item.objecttypekey);
        }
        const url = CaseWorkerUrlConfig.EndPoint.Dashboard.DsdsActionSummaryUrl + '/' + item.servicecaseid + '/casetype';
        this._dsdsActionService.getAll(url).subscribe((response) => {
            const dsdsActionsSummary = response[0];
            if (dsdsActionsSummary) {
                this._dataStoreService.setData('da_status', dsdsActionsSummary.da_status);
                this._dataStoreService.setData('teamtypekey', dsdsActionsSummary.teamtypekey);
                const currentUrl = '/pages/case-worker/' + item.servicecaseid + '/' + item.servicecasenumber + '/dsds-action/person-cw';
                // const currentUrl = '/pages/case-worker/' + item.caseid + '/' + item.servicecasenumber + '/dsds-action/involved-person';
                // this._router.navigate([currentUrl]);
                this._router.navigateByUrl('/', { skipLocationChange: true }).then(() => this._router.navigate([currentUrl]));
            }
        });
    }

    confirmNavigationAssignServiceCase() {
        (<any>$('#confirm-navigation-assign-service-case')).modal('hide');
        (<any>$('#confirm-request-case')).modal('hide');
        const currentUrl = 'pages/cjams-dashboard/cw-assign-service-case';
        this._router.navigate([currentUrl]);
        // this._router.navigateByUrl('/', { skipLocationChange: true }).then(() => this._router.navigate([currentUrl]));
    }

    cancelNavigationAssignServiceCase() {
        (<any>$('#confirm-navigation-assign-service-case')).modal('hide');
    }

    confirmIntakeNavigation() {
        (<any>$('#confirm-intake-navigation')).modal('hide');
        this._intakeService.redirectIntake(this.dsdsActionsSummary.intakenumber);
    }

    cancelIntakeNavigation() {
        (<any>$('#confirm-intake-navigation')).modal('hide');
    }

    calculateResponseOffset(sdmInfo) {
        if (sdmInfo) {
            /* SDM Counter info
             isnoimmed_physicalabuse-- Physical abuse-response within 24 hours
             isnoimmed_sexualabuse -- Sexual abuse-response within 24 hours
             isnoimmed_neglectresponse --- Neglect-response within 5 days
             isnoimmed_mentalinjury --- Mental injury-response within 5 days
             isnoimmed_screeninoverride --- Screen-in Override
             isnoimmed_risk_harm --There is Risk ofHarm. Response within 5 days.
             isnoimmed_substantial_risk -- There is Risk of Harm for a Substance Exposed Newborn. Response within 48 hours.
             isnoimmed_risk_harm -- There is Risk of Harm. Response within 5 days.
             */

            if (sdmInfo.isnoimmed_physicalabuse || sdmInfo.isnoimmed_sexualabuse) {
                return 24;
            }

            if (sdmInfo.isnoimmed_substantial_risk) {
                return 48;
            }

            if (sdmInfo.isnoimmed_neglectresponse || sdmInfo.isnoimmed_mentalinjury || sdmInfo.isnoimmed_risk_harm) {
                return 24 * 5;  // 5 days
            }
        }
        return 0;
    }

    processRecordingsList() {
        const neededRoles = ['AV', 'CHILD', 'LG'];
        if (this.involvedPerson && Array.isArray(this.involvedPerson)) {
            const needToContactPersons = this.involvedPerson.filter(person => {
                if (person.roles) {
                    if (person.roles.find(role => neededRoles.indexOf(role.intakeservicerequestpersontypekey) !== -1)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                
            }).map(filteredPerson => filteredPerson.roles[0].intakeservicerequestactorid);
            console.log('need to contacts', needToContactPersons);
            this._commonHttpService
                .getPagedArrayList(
                    new PaginationRequest({
                        page: 1,
                        limit: 100,
                        where: { intakeservicerequestactorids: needToContactPersons },
                        method: 'get'
                    }),
                    CaseWorkerUrlConfig.EndPoint.DSDSAction.Recording.GetAllDaRecordingUrl + '/' + this.id + '?data'
                )
                .subscribe((result) => {
                    if (result && Array.isArray(result.data)) {
                        const acceptedRecordings = result.data.filter(recording => {
                            const isAccepted =  (recording.recordingtype === 'Face To Face' ||
                                recording.recordingtype === 'Initialfacetoface' ||
                                recording.recordingtype === 'Phone')
                            && (recording.attemptind !== null);
                            console.log('rt', (recording.recordingtype === 'Face To Face' ||
                            recording.recordingtype === 'Initialfacetoface' ||
                            recording.recordingtype === 'Phone'));

                            console.log('ate-com', recording.attemptind !== null );
                            console.log( recording.recordingtype, recording.attemptind,  'isAccepted', isAccepted );
                            return isAccepted;
                         }
                        );
                        const recordingInfo = needToContactPersons.map(requestactorid => {
                            let isCompleted = false;
                            for (const recodringInfo of acceptedRecordings) {
                                const found = recodringInfo.contactparticipant.filter(participant => participant.intakeservicerequestactorid === requestactorid);
                                if (found && found.length > 0) {
                                    isCompleted = true;
                                    break;
                                } else {
                                    isCompleted = false;
                                }
                            }
                            return { requestactorid: requestactorid, completed: isCompleted };
                        });

                        if (needToContactPersons.length && needToContactPersons.length === recordingInfo.filter(info => info.completed).length) {
                            this.cpsResponseOffset = 0;
                        } else {
                            this.cpsResponseOffset = this.calculateResponseOffset(this.sdmInfo);
                        }

                        console.log('recordingInfo', recordingInfo);

                        console.log('acceptedRecordings', acceptedRecordings);

                    }
                });
        }

    }

    loadSDMData() {
        this._commonHttpService.getArrayList(
            new PaginationRequest({
                where: { servicerequestid: this.id, intakenumber: null },
                method: 'get'
            }),
            CaseWorkerUrlConfig.EndPoint.DSDSAction.Sdmcaseworker.PathwaySdmListUrl
        ).subscribe(response => {
            if (response && Array.isArray(response) && response.length) {
                const sdmInfoHistory = response[0].getintakeservicerequestsdm;
                if (sdmInfoHistory && Array.isArray(sdmInfoHistory) && sdmInfoHistory.length) {
                    this.sdmInfo = sdmInfoHistory[0];
                    this.cpsResponseOffset = this.calculateResponseOffset(sdmInfoHistory[0]);
                    this.processRecordingsList();
                }
            }
        });
    }

    formatPhoneNumber(phoneNumber) {
        if (phoneNumber) {
            return this._commonDDService.formatPhoneNumber(phoneNumber);
        }
    }



}
