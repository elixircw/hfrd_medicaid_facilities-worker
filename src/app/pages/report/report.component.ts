import { Component, OnInit } from '@angular/core';
import * as reports from './_configurations/reports.json';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
    // reports: any;
    safeUrl: SafeResourceUrl;
    constructor(public sanitizer: DomSanitizer) {}

    ngOnInit() {
        // this.reports = <any>reports;
        // this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('http://52.230.81.18:8080/SpagoBI/servlet/AdapterHTTP?PAGE=LoginPage&userID=biadmin&password=biadmin&NEW_SESSION=TRUE');
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            'https://welfare.myhcue.co/SpagoBI/servlet/AdapterHTTP?PAGE=LoginPage&userID=biadmin&password=biadmin&NEW_SESSION=TRUE'
        );
    }

    openReport(url) {
        // const reportUrl = environment.reportHost + `/` + url;
        // this.reports = this.reports.map((group) => {
        //     group.items = group.items.map((item) => {
        //         item.selected = url.indexOf(item.path) !== -1;
        //         return item;
        //     });
        //     return group;
        // });
        // this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('http://52.230.81.18:8080/SpagoBI/servlet/AdapterHTTP?PAGE=LoginPage&userID=biadmin&password=biadmin&NEW_SESSION=TRUE');
    }
}
