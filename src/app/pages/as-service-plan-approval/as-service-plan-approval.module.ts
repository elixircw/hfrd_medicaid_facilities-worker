import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsServicePlanApprovalRoutingModule } from './as-service-plan-approval-routing.module';
import { AsServicePlanApprovalComponent } from './as-service-plan-approval.component';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    AsServicePlanApprovalRoutingModule,
    MatTooltipModule
  ],
  declarations: [AsServicePlanApprovalComponent]
})
export class AsServicePlanApprovalModule { }
