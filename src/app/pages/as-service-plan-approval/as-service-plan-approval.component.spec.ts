import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsServicePlanApprovalComponent } from './as-service-plan-approval.component';

describe('AsServicePlanApprovalComponent', () => {
  let component: AsServicePlanApprovalComponent;
  let fixture: ComponentFixture<AsServicePlanApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsServicePlanApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsServicePlanApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
