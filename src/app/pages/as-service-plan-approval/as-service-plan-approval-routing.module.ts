import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsServicePlanApprovalComponent } from './as-service-plan-approval.component';

const routes: Routes = [{
  path: '',
  component: AsServicePlanApprovalComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsServicePlanApprovalRoutingModule { }
