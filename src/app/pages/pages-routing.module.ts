import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleGuard } from '../@core/guard';
import { PagesComponent } from './pages.component';
import { PersonInfoService } from './shared-pages/person-info/person-info.service';
import { NavigationUtils } from './_utils/navigation-utils.service';
import { IntakeUtils } from './_utils/intake-utils.service';

import { ProgramParticipationComponent } from './../lib/programParticipation/example/programParticipation.component';
import { ProgramParticipationModule } from './../lib/programParticipation/programParticipation.module';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivate: [RoleGuard],
        children: [
            // {
            //     path: 'testing/pp', component: ProgramParticipationComponent
            // },


            //   { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
            //  { path: 'newintake', loadChildren: './newintake/newintake.module#NewintakeModule' },
            //  { path: 'manage', loadChildren: './manage/manage.module#ManageModule' },
             { path: 'provider-management', loadChildren: './provider-management/provider-management.module#ProviderManagementModule' },
             { path: 'provider-referral', loadChildren: './provider-referral/provider-referral.module#ProviderReferralModule' },
             { path: 'provider-applicant', loadChildren: './provider-applicant/provider-applicant.module#ProviderApplicantModule' },
             { path: 'provider-portal-temp', loadChildren: './provider-portal-temp/provider-portal-temp.module#ProviderPortalTempModule' },
            //  { path: 'find', loadChildren: './find/find.module#FindModule' },
            //  { path: 'finance', loadChildren: './finance/finance.module#FinanceModule' },
             { path: 'case-worker', loadChildren: './case-worker/case-worker.module#CaseWorkerModule' },
            //  { path: 'case-search', loadChildren: './case-search/case-search.module#CaseSearchModule' },
             { path: 'home-dashboard', loadChildren: './home-dashboard/home-dashboard.module#HomeDashboardModule' },
            //  { path: 'provider-dashboard', loadChildren: './home-dashboard/home-dashboard.module#HomeDashboardModule' },
            //  { path: 'transport-dboard', loadChildren: './transport-dboard/transport-dboard.module#TransportDboardModule' },
            //  { path: 'cjams-dashboard', loadChildren: './cjams-dashboard/cjams-dashboard.module#CjamsDashboardModule' },
             { path: 'calendar', loadChildren: './user-calendar/user-calendar.module#UserCalendarModule' },
             { path: 'notification', loadChildren: './notification/notification.module#NotificationModule' },
            //  { path: 'report', loadChildren: './report/report.module#ReportModule' },
            //  { path: 'resource', loadChildren: './resource/resource.module#ResourceModule' },
            //  { path: 'help', loadChildren: './help/help.module#HelpModule' },
            //  { path: 'sao-dashboard', loadChildren: './sao-dashboard/sao-dashboard.module#SaoDashboardModule' },
            //  { path: 'services', loadChildren: './requested-service/requested-service.module#RequestedServiceModule' },
            //  { path: 'practitioners', loadChildren: './medical-practitioner/medical-practitioner.module#MedicalPractitionerModule' },
            //  { path: 'as-dss-admin', loadChildren: './as-dss-admin/as-dss-admin.module#AsDssAdminModule' },
            //  { path: 'case-folders', loadChildren: './case-folders/case-folders.module#CaseFoldersModule' },
            //  { path: 'person-search', loadChildren: './person-search/person-search.module#PersonSearchModule' },
            //  { path: 'person-details', loadChildren: './person-details/person-details.module#PersonDetailsModule' },
            //  { path: 'person-info-cw', loadChildren: './shared-pages/person-info/person-info.module#PersonInfoModule' },
            //  { path: 'vendor-management', loadChildren: './shared-pages/vendor-management/vendor-management.module#VendorManagementModule'},
            //  { path: 'placement-validation', loadChildren: './placement-validations/placement-validations.module#PlacementValidationsModule' },
            //  { path: 'title4e', loadChildren: './title4e/title4e.module#Title4eModule' },
            //  { path: 'as-service-plan-approval', loadChildren: './as-service-plan-approval/as-service-plan-approval.module#AsServicePlanApprovalModule' },
            //  { path: 'restitution', loadChildren: 'app/pages/restitution/restitution.module#RestitutionModule' },
            //  { path: 'reports', loadChildren: 'app/pages/generate-reports/generate-reports.module#GenerateReportsModule' },
             { path: 'provider', loadChildren: './providers/providers.module#ProvidersModule' },
             { path: 'provider-search', loadChildren: './provider-search/provider-search.module#ProviderSearchModule' },
             { path: 'default-dashboard', loadChildren: './default-dashboard/default-dashboard.module#DefaultDashboardModule' },
            //  { path: 'reports/nytd', loadChildren: './../lib/nytd/nytd.module#NytdModule' }
        ],
        data: {
            screen: { modules: ['pages', 'menus'], skip: false }
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        ProgramParticipationModule
    ],
    exports: [RouterModule],
    providers : [PersonInfoService, NavigationUtils, IntakeUtils],
})
export class PagesRoutingModule { }
