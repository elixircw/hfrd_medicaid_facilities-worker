import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { ProviderSearchResult } from '../../_entities/find-entity.module';

@Component({
  selector: 'app-dsds-provider',
  templateUrl: './dsds-provider.component.html',
  styleUrls: ['./dsds-provider.component.scss']
})
export class DsdsProviderComponent implements OnInit {

  @Input() showTable: boolean;
  @Input() pageNumberSubject$: Subject<number>;
  @Input() searchList$: Observable<ProviderSearchResult[]>;
  @Input() totalRecords$: Observable<number>;

  paginationInfo: PaginationInfo = new PaginationInfo();
  constructor() { this.showTable = false; }

  ngOnInit() {
  }
  pageChanged(page: number) {
    this.paginationInfo.pageNumber = page;
    this.pageNumberSubject$.next(page);
  }
}
