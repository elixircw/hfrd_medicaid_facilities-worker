import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { ProviderSearchResult } from '../../_entities/find-entity.module';

@Component({
  selector: 'app-home-health-hospital',
  templateUrl: './home-health-hospital.component.html',
  styleUrls: ['./home-health-hospital.component.scss']
})
export class HomeHealthHospitalComponent implements OnInit {
  @Input() showTable: boolean;
  @Input() pageNumberSubject$: Subject<number>;
  @Input() searchList$: Observable<ProviderSearchResult[]>;
  @Input() totalRecords$: Observable<number>;

  paginationInfo: PaginationInfo = new PaginationInfo();
  selectedProvider: ProviderSearchResult;

  constructor() {
    this.selectedProvider = new ProviderSearchResult();
    this.showTable = false;
  }

  ngOnInit() {
  }
  selectProvider(item: ProviderSearchResult) {
    this.selectedProvider = item;
  }
  pageChanged(page: number) {
    this.paginationInfo.pageNumber = page;
    this.pageNumberSubject$.next(page);
  }

}
