import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FindComponent } from './find.component';
import { RoleGuard } from '../../@core/guard';

const routes: Routes = [
    {
        path: '',
        component: FindComponent,
        canActivate: [RoleGuard],
        children: [
            { path: 'provider', loadChildren: './provider/provider.module#ProviderModule' },
            { path: 'person', loadChildren: './persons/persons.module#PersonsModule' },
            // { path: 'accountspayable', loadChildren: './accountspayable/accountspayable.module#AccountsPayableModule' },
            { path: 'dsds-actions', loadChildren: './dsds-actions/dsds-actions.module#DsdsActionsModule'  },
            { path: 'entities', loadChildren: './entities/entities.module#EntitiesModule' },
            { path: 'documents', loadChildren: './documents/documents.module#DocumentsModule' },
            { path: 'find-regulation-library', loadChildren: './find-regulation-library/find-regulation-library.module#FindRegulationLibraryModule' },
            { path: 'staff-n-team', loadChildren: './staff-and-team/staff-and-team.module#StaffAndTeamModule' }
        ],
        data: { roles: ['admin', 'intakeuser', 'caseworker', 'reviewer'] }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FindRoutingModule { }
