import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';
import { PaginationInfo } from '../../../../@core/entities/common.entities';
import { EntitiesSearchEntity } from '../../_entities/find-entity.module';

@Component({
  selector: 'entities-results',
  templateUrl: './entities-results.component.html',
  styleUrls: ['./entities-results.component.scss']
})
export class EntitiesResultsComponent implements OnInit {

  @Input() resultsPageNumber$: Subject<number>;
  @Input() resultsEntitiesList$: Observable<EntitiesSearchEntity[]>;
  @Input() resultsTotalRecords$: Observable<number>;

  selectedEntity$: Subject<EntitiesSearchEntity>;
  paginationInfo: PaginationInfo = new PaginationInfo();
  @Input() showTable: boolean;

  constructor() {
    this.selectedEntity$ = new Subject<EntitiesSearchEntity>();
    this.showTable = false;
  }

  ngOnInit() {
  }

  pageChanged(pageNumber: number) {
    this.paginationInfo.pageNumber = pageNumber;
    this.resultsPageNumber$.next(pageNumber);
  }

  entitySelected(entity: EntitiesSearchEntity) {
    this.selectedEntity$.next(entity);
  }
}
