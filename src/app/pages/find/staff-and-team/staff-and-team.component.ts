import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';

import { UsersSearchResult } from '../_entities/find-entity.module';

declare var $: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'staff-and-team',
    templateUrl: './staff-and-team.component.html',
    styleUrls: ['./staff-and-team.component.scss']
})
export class StaffAndTeamComponent implements OnInit {

    searchListSubject$: Subject<Observable<UsersSearchResult[]>>;
    totalRecordsSubject$: Subject<Observable<number>>;
    pageNumberSubject$: Subject<number>;
    userIDSubject$: Subject<UsersSearchResult>;

    searchList$: Observable<UsersSearchResult[]>;
    totalRecords$: Observable<number>;

    constructor() {
        this.searchListSubject$ = new Subject<Observable<UsersSearchResult[]>>();
        this.totalRecordsSubject$ = new Subject<Observable<number>>();
        this.pageNumberSubject$ = new Subject<number>();
        this.userIDSubject$ = new Subject<UsersSearchResult>();
    }

    ngOnInit() {
        this.searchListSubject$.subscribe(data => {
            this.searchList$ = data;
        });

        this.totalRecordsSubject$.subscribe(data => {
            this.totalRecords$ = data;
        });

        $('.collapse.in').each(function () {
            $(this).siblings('.panel-heading').find('.glyphicon').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        });

        // Toggle plus minus icon on show hide of collapse element
        $('.collapse').on('show.bs.collapse', function () {
            $(this).parent().find('.glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }).on('hide.bs.collapse', function () {
            $(this).parent().find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        });
    }

}
