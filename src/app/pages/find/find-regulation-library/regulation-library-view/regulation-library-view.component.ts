import { RegulationLibraryResult } from '../../_entities/find-entity.module';
import { Component, OnInit, Input } from '@angular/core';
@Component({
    selector: 'regulation-library-view',
    templateUrl: './regulation-library-view.component.html',
    styleUrls: ['./regulation-library-view.component.scss']
})
export class RegulationLibraryViewComponent implements OnInit {
    @Input() selectedRegulation: RegulationLibraryResult;
    constructor() {
    }
    ngOnInit() {
    }
}
