import { Component, OnInit, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { Http } from '../../../../node_modules/@angular/http';
import { ErrorInfo } from '../../@core/common/errorDisplay';
import { UserLogin } from '../../@core/entities/authDataModel';
import { AlertService } from '../../@core/services';
import { AuthService } from '../../@core/services/auth.service';
import { config } from '../../../environments/config';
import { AppConstants } from '../../@core/common/constants';


@Component({
    // moduleId: module.id,
    // tslint:disable-next-line:component-selector
    selector: 'login',
    styleUrls: ['./login.component.scss'],
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    show = false;
    user: UserLogin = new UserLogin();
    error: ErrorInfo = new ErrorInfo();
    openam_token: any;
    workEnv: boolean;

    @ViewChild('showhideinput')
    input;
    constructor(private authService: AuthService, private _alertService: AlertService, private _CookieService: CookieService, private _http: Http) {
        this.openam_token = this._CookieService.get('iPlanetDirectoryPro');
        this.openam_token = this.openam_token.replace(/^"(.*)"$/, '$1');
        this.getheaders(this.openam_token);
    }

    ngOnInit() {
        if (config.workEnvironment === 'local') {
            this.workEnv = false;
        } else {
            this.workEnv = true;
        }
    }

    getheaders(token) {
        const uid = '';
        const role_am = 'superuser';
        if (this.openam_token !== '') {
            this.authService.openAMlogin('', '').subscribe((response: any) => {
                if (response && response.id && response.role) {
                    localStorage.setItem('userProfile', JSON.stringify(response.user.userprofile));
                        this.authService.roleBasedRoute(response.role.name.toLowerCase());
                } else {
                    this._alertService.error('User does not exist to access this application. Please contact your site admin.');
                }
            });
        }
    }
    // get headers from openAM

    login() {
        if (this.user.email && this.user.password) {
            this.authService.login(this.user.email, this.user.password).subscribe(
                (response) => {
                    console.log('login user:', response );
                    if (response && response.id && response.role) {
                        if (response.role.name === 'Provider_Staff_Admin') {
                            this.getScript('../../../assets/js/app.js', () => {});
                        }
                        localStorage.setItem('userProfile', JSON.stringify(response.user.userprofile));
                        localStorage.setItem('userProfile', JSON.stringify(response.user.userprofile));
                        if (response.user && response.user.userprofile && response.user.userprofile.teamtypekey === AppConstants.AGENCY.PRIVATE_PROVIDER) {
                            this.authService.routeToProviderPortal();
                        } else {
                            this.authService.roleBasedRoute(response.role.name.toLowerCase());
                        }
                    } else {
                        this._alertService.error('User roles are not mapped for this user to access this application. Please contact your site admin.');
                    }
                },
                (err) => {
                    if (err.error && err.error.error && err.error.error.code) {
                        if (err.error.error.code === 'LOGIN_FAILED') {
                            this._alertService.warn('Invalid email or password.');
                        } else if (err.error.error.code === 'USERNAME_EMAIL_REQUIRED') {
                            this._alertService.warn('Email or password should not be empty!');
                        } else {
                            this._alertService.error('Unable to login, please try again.');
                            this.error.error(err);
                        }
                    } else {
                        this._alertService.error('Unable to login, please try again.');
                    }
                }
            );
        } else {
            this._alertService.warn('Email or password should not be empty!');
        }
    }

    toggleShow() {
        this.show = !this.show;
        if (this.show) {
            this.input.nativeElement.type = 'text';
        } else {
            this.input.nativeElement.type = 'password';
        }
    }
    getScript(src: string, callback: Function) {
        const d = document;
        const o = { callback: callback || function() {} };
        let s, t;

        if (typeof src === 'undefined') {
            return;
        }

        s = d.createElement('script');
        s.language = 'javascript';
        s.type = 'text/javascript';
        s.async = 1;
        s.charset = 'utf-8';
        s.src = src;
        if (typeof o.callback === 'function') {
            if (d.addEventListener) {
                s.addEventListener(
                    'load',
                    function() {
                        o.callback();
                    },
                    false
                );
            } else {
                // old IE support
                s.onreadystatechange = function() {
                    if (
                        this.readyState === 'complete' ||
                        this.readyState === 'loaded'
                    ) {
                        this.onreadystatechange = null;
                        o.callback();
                    }
                };
            }
        }
        t = d.getElementsByTagName('script')[0];
        t.parentNode.insertBefore(s, t);
    }
}
