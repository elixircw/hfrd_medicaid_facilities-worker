import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard, SeoGuard } from './@core/guard';


const routes: Routes = [
    {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
        canActivate: [SeoGuard],
        data: {
            title: ['MDTHINK Login'],
            desc: 'Maryland department of human services'
        }
    },
    {
        path: 'devices/:token/:returnUrl',
        component: AppComponent
    },
    {
        path: 'external-assessment/:servicereqid/:isApproved/:templateId/:submissionId',
        loadChildren: './external-assessment/external-assessment.module#ExternalAssessmentModule',
    },
    {
        path: 'external-assessment/:servicereqid/:isApproved/:templateId/:submissionId/:intakeservicerequestactorid',
        loadChildren: './external-assessment/external-assessment.module#ExternalAssessmentModule',
    },
    {
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule',
        canActivate: [AuthGuard, SeoGuard],
        data: {
            title: ['MDTHINK Home'],
            desc: 'Maryland department of human services'
        }
    },
    {
        path: 'login',
        loadChildren: './auth/login/login.module#LoginModule',
        canActivate: [SeoGuard],
        data: {
            title: ['MDTHINK Login'],
            desc: 'Maryland department of human services'
        }
    },
    {
        path: 'provider-portal',
        loadChildren: './provider-portal/provider-portal.module#ProviderPortalModule',
        data: {
            title: ['PROVIDER Home'],
            desc: 'Provider services'
        }
    },
    { path: 'error', loadChildren: './shared/pages/server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './shared/pages/access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './shared/pages/not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            useHash: true,
            enableTracing: false
        })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}
