import { Injectable } from '@angular/core';
import { CommonControlsModule } from '../shared/modules/common-controls/common-controls.module';
import { CommonHttpService, AuthService } from '../@core/services';
import { PaginationRequest } from '../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { UirConstants } from './constants';
import { AppUser } from '../@core/entities/authDataModel';


@Injectable()
export class ProviderPortalService {

  roleName: string;
  applications = [];
  configuration = {};
  providerId: string;
  role: AppUser;

  constructor(private _commonHttpService: CommonHttpService,
    private _authService: AuthService) {
      this.role = this._authService.getCurrentUser();
      if(this.role && this.role.role && this.role.role.name) {
        this.roleName = this.role.role.name;
      }

  }

  getProviderConfiguration() {
    const currentUser = this._authService.getCurrentUser();
    let userId = null;
    if (currentUser && currentUser.user && currentUser.user.securityusersid) {
      userId = currentUser.user.securityusersid;
    }

    this.setProviderId(userId);
    
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        nolimit: true,
        method: 'get',
        where: { securityusersid: userId }
      }),
      'tb_provider/providerapplicantsearch?filter'
    ).map(response => {
      return response.data;
    });
  }

  setProviderId(userId: string) {
    this._commonHttpService.getSingle(
      {
        where: { securityusers_id: userId },
        method: 'get',
        count: -1 
      }, 
      'Tb_provider_userconfig?filter'
    ).subscribe(
      (response) => {
        this.providerId = response[0].provider_id;
      });
  }
  setAgencyProviderId(providerId: any) {
    this.providerId = providerId;
}
  getProviderId() {
    return this.providerId;
  }

  getApplications() {
    return this.applications;
  }

  setApplications(applications) {
    return this.applications = applications;
  }

  getConfiguration() {
    return this.applications;
  }

  setConfiguration(configuration) {
    return this.configuration = configuration;
  }

  getUirTabs(){
   const tabs = UirConstants.UIR_TABS.filter(tab => {

      //@Simar: I'm not sure what role the new dynamic users are set-up with for Portal access
      // so for now just selecting all roles to see UIR tabs
      // this needs to be changed once we have the roleName added in the uirconstants 
      // comment this return statement for below filtering condition to work
      return true;
      // if (tab.roles.indexOf(this.roleName) !== -1) {
      //   return true;
      // } else {
      //   return false;
      // }
    });
    return tabs;
  }


}
