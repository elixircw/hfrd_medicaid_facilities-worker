import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationTabComponent } from './navigation-tab/navigation-tab.component';
import { RouterModule } from '@angular/router';
import { AttachmentRoutingModule } from './attachment/attachment-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AttachmentRoutingModule
  ],
  declarations: [NavigationTabComponent],
  exports: [NavigationTabComponent]
})
export class ProviderPortalUtilsModule { }
