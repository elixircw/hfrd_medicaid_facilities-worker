import { Component, OnInit } from '@angular/core';
//import { ProviderAuthorizationService } from '../../../provider-authorization.service';
//import { ComplaintDetailsService } from '../complaint-details.service';
import { ProviderPortalService } from '../../provider-portal.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navigation-tab',
  templateUrl: './navigation-tab.component.html',
  styleUrls: ['./navigation-tab.component.scss']
})
export class NavigationTabComponent implements OnInit {

  tabs = [];
  constructor(private providerSerivcie: ProviderPortalService) {
    //const status = this.complaintSerivcie.getCurrentStatus();
    this.tabs = this.providerSerivcie.getUirTabs();
  }


  ngOnInit() {
  }

}
