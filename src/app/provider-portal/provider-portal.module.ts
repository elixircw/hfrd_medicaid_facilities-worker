import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderPortalRoutingModule } from './provider-portal-routing.module';
import { ProviderPortalComponent } from './provider-portal.component';
import { FormMaterialModule } from '../@core/form-material.module';
import { ProviderNavMenuModule } from './provider-nav-menu/provider-nav-menu.module';
import { AlertModule } from '../shared/modules/alert/alert.module';
import { ProviderPortalService } from './provider-portal.service';
import { ProviderPortalResolverService } from './provider-portal-resolver.service';
import { ProviderStaffComponent } from './provider-staff/provider-staff.component';

@NgModule({
  imports: [
    CommonModule,
    ProviderPortalRoutingModule,
    FormMaterialModule,
    ProviderNavMenuModule,
    AlertModule
  ],
  declarations: [ProviderPortalComponent, ProviderStaffComponent],
  providers: [ProviderPortalService, ProviderPortalResolverService]
})
export class ProviderPortalModule { }
