import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderPortalComponent } from './provider-portal.component';
import { ProviderPortalResolverService } from './provider-portal-resolver.service';
import { ProviderStaffComponent } from './provider-staff/provider-staff.component';

const routes: Routes = [{
  path: '',
  component: ProviderPortalComponent,
  children: [
    {
      path: 'dashboard',
      loadChildren: './provider-dashboard/provider-dashboard.module#ProviderDashboardModule'
    },
    {
      path: 'incident-report',
      loadChildren: './provider-incident-report/provider-incident-report.module#ProviderIncidentReportModule'
    },
    {
      path: 'staff',
      component: ProviderStaffComponent
    }
  ],
  resolve: {
    configurations: ProviderPortalResolverService
  },

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderPortalRoutingModule { }
