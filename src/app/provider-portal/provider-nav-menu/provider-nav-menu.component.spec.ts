import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderNavMenuComponent } from './provider-nav-menu.component';

describe('ProviderNavMenuComponent', () => {
  let component: ProviderNavMenuComponent;
  let fixture: ComponentFixture<ProviderNavMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderNavMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderNavMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
