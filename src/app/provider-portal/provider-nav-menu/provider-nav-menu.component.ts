import { Component, OnInit } from '@angular/core';
import { AppUser } from '../../@core/entities/authDataModel';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService, CommonHttpService, DataStoreService, SessionStorageService } from '../../@core/services';
import { Observable } from 'rxjs/Observable';
import { hasMatch } from '../../@core/common/initializer';
import { ProviderPortalService } from '../provider-portal.service';

@Component({
  selector: 'provider-nav-menu',
  templateUrl: './provider-nav-menu.component.html',
  styleUrls: ['./provider-nav-menu.component.scss']
})
export class ProviderNavMenuComponent implements OnInit {

  userInfo: AppUser;
  pushRightClass = 'push-right';
  totalNotificationCount = 10;
  showNotification = false;
  today = Date.now();
  applications = [];
  constructor(public router: Router,
    private _authService: AuthService,
    private _service: CommonHttpService,
    private _providerPortalService: ProviderPortalService,
    private _sessionStorage: SessionStorageService, ) {
      this.applications = this._providerPortalService.getApplications();
    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
        this.toggleSidebar();
       ;
      }
    });
  }

  ngOnInit() {
    this.userInfo = this._authService.getCurrentUser();
    this.getNotificationCount();
  }

  isToggled(): boolean {
    const dom: Element = document.querySelector('body');
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle(this.pushRightClass);
  }

  onLoggedout() {
    this._authService.logout();
  }

  hasAccess(key: string): Observable<boolean> {
    return this._authService.currentUser.map(user => {
      if (key && user.resources) {
        if (!hasMatch([key], user.resources.map(item => (item ? item.name : '')))) {
          return false;
        }
        return true;
      }
      return true;
    });
  }
  clearCount() {
    this.showNotification = false;
  }

  getNotificationCount() {
    this._service.endpointUrl = 'Usernotifications/getUserNotificationCount';
    this._service.getArrayList({}).subscribe(data => {
      this.totalNotificationCount = data['count'];
      if (data['count'] !== '0') {
        this.showNotification = true;
      }
    });
  }

}
