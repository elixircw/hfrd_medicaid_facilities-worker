import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProviderNavMenuComponent } from './provider-nav-menu.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [ProviderNavMenuComponent],
  exports: [ProviderNavMenuComponent]
})
export class ProviderNavMenuModule { }
