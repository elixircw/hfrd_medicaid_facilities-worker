import { Component, OnInit } from '@angular/core';
import { ProviderPortalService } from '../provider-portal.service';
import { CommonHttpService, AlertService } from '../../@core/services';

@Component({
  selector: 'provider-staff',
  templateUrl: './provider-staff.component.html',
  styleUrls: ['./provider-staff.component.scss']
})
export class ProviderStaffComponent implements OnInit {
  providerId: string;
  staffInformation: any[];

  constructor(
    private _commonHttpService: CommonHttpService,
    private _providerPortalService: ProviderPortalService,
    private _alertService: AlertService
  ) { }

  ngOnInit() {
    this.providerId = this._providerPortalService.getProviderId();
    this.getStaffInformation();
  }

  getStaffInformation() {
    console.log('Provider Id in staff', this.providerId);
    this._commonHttpService.getArrayList({
      method: 'get',
      nolimit: true,
      where: {
        object_id: this.providerId,
      },
    },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.staffInformation = response;
      },
      (error) => {
        this._alertService.error("Unable to retrieve information");
      }
    );
  }

}
