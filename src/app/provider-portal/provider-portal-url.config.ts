export class ProviderPortalUrlConfig {
    public static EndPoint = {
        Uir: {
            GetNextNumberUrl: `Nextnumbers/getNextNumber?apptype=providerIncident`,
            // CREATE_UPDATE_URL : `tb_provider_complaint/addupdate`,
             DETAIL_URL : `tb_provider_complaint/providercomplaintdetail?filter`,
             CREATE_UPDATE_URL: `provider_uir/addupdate`,
             CREATE_UPDATE_INCIDENT: `provider_uir_classincident/addupdate`,
             CREATE_UPDATE_WITNESS: `provider_uir_witness/addupdate`,
             CREATE_UPDATE_DECISION: `provider_uir/routingupdate`,
             CREATE_UPDATE_YOUTHFOSTER: `provider_uir_actor_detail/addupdate`,
             CREATE_UPDATE_CLASS3_INCIDENT: `provider_uir_incident_detail/addupdate`,
             CREATE_UPDATE_CONTACT: `provider_uir_contact_detail/addupdate`,
             GET_YOUTH_LIST: 'provider_uir_youth_detail/getassignedyouth?filter',
             GET_ASSIGED_YOUTH_LIST: 'provider_uir_youth_detail/getyouthlistbyprovider?filter',
             YOUTH_IN_PROVIDER: 'prov_monitoring/youthinprovider',
             ASSIGN_YOUTH_LIST: 'provider_uir_youth_detail/assignyouth',
             CREATE_UPDATE_LAW_ENFORCEMENT: `provider_uir_lawenforcement_detail/addupdate`,
             CREATE_UPDATE_SUPERVISORY: `provider_uir_supervisory/addupdate`,
             CREATE_UPDATE_YOUTHINVOLVED: `provider_uir_actor_involved/addupdate`,
             CREATE_UPDATE_NOTIFICATION: `provider_uir_notification/addupdate`,
             LIST_NOTIFICATION: `provider_uir_notification`,
             LIST_URL : `provider_uir/listdashboard`,
             GET_PROGRAM_DETAILS: `provider_uir/uir_update_list`,
             GET_UIR_DETAILS : `provider_uir/list?filter`,
             GET_SUPERVISORY_DETAILS: `provider_uir_supervisory/list?filter`,
             GET_CLASSINCIDENT_DETAILS: `provider_uir_classincident/list?filter`,
             GET_CLASS3_INCIDENT_DETAILS: `provider_uir_incident_detail/list?filter`,
             GET_NARRATIVE_DETAILS : `provider_uir_witness/list?filter`,
             GET_YOUTH_INVOLVED_DETAILS: `provider_uir_actor_involved/list?filter`,
             GET_YOUTH_FOSTER_LIST: `provider_uir_actor_detail/list?filter`,
             GET_CONTACT_LIST: `provider_uir_contact_detail/list?filter`,
             GET_LAW_ENFORCEMENT_LIST: `provider_uir_lawenforcement_detail/list?filter`,
             LIST_PROGRAMMES_URL: 'providerreferral/listprogramnames',
             PROVIDER_PROGRAM_NAMES_URL: 'provider_uir/getproviderprogramsinformation',
             GET_SELECTED_PROGRAM_INFO: 'providerlicense/getproviderlicensing',
             GET_SELECTED_SITE_INFO: 'provider_uir/site_list'
            // DECISION_URL: `tb_provider_complaint/routingupdate`,
            // DECISION_LIST_URL: `tb_provider_complaint/routinglist?filter`,
            // DEFIICENCY_URL : `tb_provider_complaint_deficiency/list?filter`,
            // CREATE_UPDATE_DEFIICENCY_URL : `tb_provider_complaint_deficiency/addupdate`,
            // CONTACT_URL : `tb_provider_complaint_contacts/addupdate`,
            // CONTACT_NOTE_CREATE_UPDATE_URL : `tb_provider_complaint_notes/addupdate`,
            // CONTACT_LIST_URL : `tb_provider_complaint_contacts/list?filter`,
            // CONTACT_NOTES_LIST_URL : `tb_provider_complaint_notes/list?filter`,
        },
        DSDSAction: {
            Attachment: {
                UploadAttachmentUrl: 'attachments/uploadsFile',
                AttachmentGridUrl: 'Documentproperties/getintakeattachments',
                AttachmentUploadUrl: 'attachments/uploadsFile',
                AttachmentTypeUrl: 'Intake/attachmenttype/',
                AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype/',
                SaveAttachmentUrl: 'Documentproperties/addintakeattchment',
                DeleteAttachmentUrl: 'Documentproperties/delete'
            }
        }
    };
}

// export class ProviderUrlConfig {
//     public static EndPoint = {
//         DSDSAction: {
//             Attachment: {
//                 UploadAttachmentUrl: 'attachments/uploadsFile',
//                 AttachmentGridUrl: 'Documentproperties/getintakeattachments',
//                 AttachmentUploadUrl: 'attachments/uploadsFile',
//                 AttachmentTypeUrl: 'Intake/attachmenttype/',
//                 AttachmentClassificationTypeUrl: 'Intake/attachmentclassificationtype/',
//                 SaveAttachmentUrl: 'Documentproperties/addintakeattchment',
//                 DeleteAttachmentUrl: 'Documentproperties/delete'
//             }
//         }
//     };
// }
