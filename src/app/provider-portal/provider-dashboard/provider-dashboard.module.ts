import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderDashboardRoutingModule } from './provider-dashboard-routing.module';
import { ProviderDashboardComponent } from './provider-dashboard.component';
import { FormMaterialModule } from '../../@core/form-material.module';
import { ProviderDashboardService } from './provider-dashboard.service';
import { ReferralApplicationsComponent } from './referral-applications/referral-applications.component';
import { ReferralApplicationsService } from './referral-applications/referral-applications.service';
import { ProviderRequestsComponent } from './provider-requests/provider-requests.component';

@NgModule({
  imports: [
    CommonModule,
    ProviderDashboardRoutingModule,
    FormMaterialModule
  ],
  declarations: [ProviderDashboardComponent, ReferralApplicationsComponent, ProviderRequestsComponent],
  providers: [ProviderDashboardService, ReferralApplicationsService]
})
export class ProviderDashboardModule { }
