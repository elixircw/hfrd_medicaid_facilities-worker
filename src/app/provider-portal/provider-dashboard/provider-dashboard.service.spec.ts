import { TestBed, inject } from '@angular/core/testing';

import { ProviderDashboardService } from './provider-dashboard.service';

describe('ProviderDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProviderDashboardService]
    });
  });

  it('should be created', inject([ProviderDashboardService], (service: ProviderDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
