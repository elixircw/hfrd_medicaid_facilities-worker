import { Injectable } from '@angular/core';
import { CommonHttpService, AuthService } from '../../../@core/services';
import { PaginationRequest } from '../../../@core/entities/common.entities';

@Injectable()
export class ReferralApplicationsService {

  constructor(private _commonHttpService: CommonHttpService,
    private _authService: AuthService) { }

  getApplications() {
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        nolimit: true,
        method: 'get',
        where: { securityusersid: this._authService.getCurrentUser().user.securityusersid }
      }),
      'tb_provider/providerapplicantsearch?filter'
    ).map(response => {
      return response.data;
    });
  }

}
