import { Component, OnInit } from '@angular/core';
import { ReferralApplicationsService } from './referral-applications.service';
import { DataStoreService } from '../../../@core/services';

@Component({
  selector: 'referral-applications',
  templateUrl: './referral-applications.component.html',
  styleUrls: ['./referral-applications.component.scss']
})
export class ReferralApplicationsComponent implements OnInit {

  referralApplicationList = [];
  constructor(private _service: ReferralApplicationsService,
    private _dataStoreService: DataStoreService) { }

  ngOnInit() {
    this.loadApplications();
  }

  loadApplications() {
    this._service.getApplications().subscribe(response => {
      this.referralApplicationList = response;
      if(this.referralApplicationList && this.referralApplicationList.length) {
        this._dataStoreService.setData('ProviderPortalID', this.referralApplicationList[0].provider_id);
      }
    });
  }

}
