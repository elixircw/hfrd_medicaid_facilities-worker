import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralApplicationsComponent } from './referral-applications.component';

describe('ReferralApplicationsComponent', () => {
  let component: ReferralApplicationsComponent;
  let fixture: ComponentFixture<ReferralApplicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralApplicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
