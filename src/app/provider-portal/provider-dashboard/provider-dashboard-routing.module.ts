import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderDashboardComponent } from './provider-dashboard.component';

const routes: Routes = [{
  path: '',
  component: ProviderDashboardComponent
  },
  {
    path: '',
    loadChildren: '../../pages/provider-portal-temp/provider-portal-temp.module#ProviderPortalTempModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderDashboardRoutingModule { }
